import { CustomerTableItemView, CustomerTableListView } from '../viewModel';
import { SearchResult } from './searchParamModel';

export class CustomerTableStoreModel {
  public list: SearchResult<CustomerTableListView> = new SearchResult<CustomerTableListView>();
  public items: CustomerTableItemView[] = [];
  public item: CustomerTableItemView = new CustomerTableItemView();
}
