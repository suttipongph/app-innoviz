import { BracketType, ColumnType, Operators } from 'shared/constants';

export class SearchParameter {
  public tableKey?: string;
  public urlPath?: string;
  public conditions?: SearchCondition[] = [];
  public paginator?: Paginator = null;
  public refTable?: string = null;
  public branchFilterMode?: string = null;
  public companyBaseGUID?: string;
  public branchBaseGUID?: string;
  public isAscs?: boolean[] = [];
  public sortColumns?: string[] = [];
  public excludeRowData?: boolean;
}

export class SearchCondition {
  public columnName: string = null;
  public subColumnName?: string;
  public parameterName?: string = null;
  public value?: string = null;
  public values?: string[] = null;
  public dateOfValues?: any[] = null;
  public type: ColumnType = null;
  public operator?: string = Operators.AND;
  public isParentKey?: boolean = false;
  public equalityOperator?: string = Operators.EQUAL;
  public mockValues?: string[] = null;
  public bracket?: number = BracketType.None;
}

export class SearchResult<T> {
  public results: T[] = [];
  public paginator: Paginator = null;
}

export class SearchResultK2<T> {
  public results: T[] = [];
  public paginator: Paginator = null;
  public errorCode: string = null;
  public errorMessage: string = null;
}

export class Paginator {
  public page: number;
  public first: number;
  public rows: number;
  public pageCount: number;
  public totalRecord?: number;
}
