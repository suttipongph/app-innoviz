import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ServiceFeeCategory, RefType, ProductType } from 'shared/constants';

export class ServiceFeeConditionTransItemView extends ViewCompanyBaseEntity {
  public serviceFeeConditionTransGUID: string = null;
  public amountBeforeTax: number = 0;
  public creditAppRequestTableGUID: string = null; //ผิด format แต่ไม่ลบเพราะกลัวกระทบ
  public creditAppRequestTable_CreditAppRequestTableGUID: string = null;
  public description: string = null;
  public inactive: boolean = false;
  public invoiceRevenueTypeGUID: string = null;
  public ordering: number = 0;
  public refGUID: string = null;
  public refId: string = null;
  public refServiceFeeConditionTransGUID: string = null;
  public creditAppRequestTable_CreditAppRequestId: string = null;
  public refType: RefType = null;
  public invoiceRevenueType_ServiceFeeCategory: ServiceFeeCategory = null;
  public invoiceRevenueType_ProductType: ProductType = null;
}
