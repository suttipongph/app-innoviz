import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ApprovalDecision } from 'shared/constants';

export class CreditAppRequestLineListView extends ViewCompanyBaseEntity {
  public creditAppRequestLineGUID: string = null;
  public approvalDecision: ApprovalDecision = null;
  public approvedCreditLimitLineRequest: number = 0;
  public businessSegmentId: string = null;
  public buyerTableGUID: string = null;
  public creditLimitLineRequest: number = 0;
  public lineNum: number = 0;
  public creditapprequesttableGUID: string = null;
  public buyerTable_Values: string = null;
  public buyerTable_BuyerId: string = null;
  public businessSegmentGUID: string = null;
  public businessSegment_Values: string = null;
  public businessSegment_BusinessSegmentId: string = null;
}
