import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType } from 'shared/constants';

export class NumberSeqSetupByProductTypeListView extends ViewCompanyBaseEntity {
  public numberSeqSetupByProductTypeGUID: string = null;
  public creditAppNumberSeqGUID: string = null;
  public creditAppRequestNumberSeqGUID: string = null;
  public guarantorAgreementNumberSeqGUID: string = null;
  public internalGuarantorAgreementNumberSeqGUID: string = null;
  public internalMainAgreementNumberSeqGUID: string = null;
  public mainAgreementNumberSeqGUID: string = null;
  public taxInvoiceNumberSeqGUID: string = null;
  public taxCreditNoteNumberSeqGUID: string = null;
  public receiptNumberSeqGUID: string = null;
  public productType: ProductType = null;
  public creditAppRequestNumber_Values: string = null;
  public creditAppNumberSeq_Values: string = null;
  public internalMainAgreementNumber_Values: string = null;
  public mainAgreementNumber_Values: string = null;
  public internalGuarantorAgreementNumber_Values: string = null;
  public guarantorAgreementNumber_Values: string = null;
  public taxInvoiceNumberSeq_Values: string = null;
  public taxCreditNoteNumberSeq_Values: string = null;
  public receiptNumberSeq_Values: string = null;
  public numberSeqCode: string = null;
}
