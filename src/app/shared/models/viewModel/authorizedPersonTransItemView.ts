import { ViewCompanyBaseEntity } from 'shared/models/base';
import { IdentificationType, RefType } from 'shared/constants';

export class AuthorizedPersonTransItemView extends ViewCompanyBaseEntity {
  public authorizedPersonTransGUID: string = null;
  public authorizedPersonTypeGUID: string = null;
  public relatedPersonTable_BackgroundSummary: string = null;
  public inActive: boolean = false;
  public ncbCheckedBy: string = null;
  public ncbCheckedDate: string = null;
  public ordering: number = 0;
  public refAuthorizedPersonTransGUID: string = null;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
  public relatedPersonTableGUID: string = null;
  public relatedPersonTable_Age: number = 0;
  public relatedPersonTable_NationalityId: string = null;
  public relatedPersonTable_RaceId: string = null;
  public relatedPersonTable_Name: string = null;
  public relatedPersonTable_IdentificationType: IdentificationType = null;
  public relatedPersonTable_TaxId: string = null;
  public relatedPersonTable_PassportId: string = null;
  public relatedPersonTable_WorkPermitId: string = null;
  public relatedPersonTable_DateOfBirth: string = null;
  public relatedPersonTable_Phone: string = null;
  public relatedPersonTable_Extension: string = null;
  public relatedPersonTable_Fax: string = null;
  public relatedPersonTable_Mobile: string = null;
  public relatedPersonTable_LineId: string = null;
  public relatedPersonTable_Email: string = null;
  public relatedPersonTable_RelatedPersonId: string = null;
  public replace: boolean = false;
  public replacedAuthorizedPersonTransGUID: string = null;
}
