import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class ServiceFeeTemplateLineItemView extends ViewBranchCompanyBaseEntity {
  public serviceFeeTemplateLineGUID: string = null;
  public amountBeforeTax: number = 0;
  public description: string = null;
  public invoiceRevenueTypeGUID: string = null;
  public serviceFeeTemplateTableGUID: string = null;
}
