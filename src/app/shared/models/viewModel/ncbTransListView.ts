import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class NCBTransListView extends ViewCompanyBaseEntity {
  public ncbTransGUID: string = null;
  public bankGroupGUID: string = null;
  public creditLimit: number = 0;
  public creditTypeGUID: string = null;
  public endDate: string = null;
  public monthlyRepayment: number = 0;
  public ncbAccountStatusGUID: string = null;
  public outstandingAR: number = 0;
  public creditType_Values: string = null;
  public creditType_CreditTypeId: string = null;
  public bankGroup_Values: string = null;
  public bankGroup_BankGroupId: string = null;
  public ncbAccountStatus_Values: string = null;
  public ncbAccountStatus_NCBAccStatusId: string = null;
  public refGUID: RefType = null;
  public refType: string = null;
}
