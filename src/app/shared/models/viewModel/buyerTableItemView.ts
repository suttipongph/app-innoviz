import { ViewCompanyBaseEntity } from 'shared/models/base';
import { IdentificationType } from 'shared/constants';

export class BuyerTableItemView extends ViewCompanyBaseEntity {
  public buyerTableGUID: string = null;
  public altName: string = null;
  public blacklistStatusGUID: string = null;
  public businessSegmentGUID: string = null;
  public businessSizeGUID: string = null;
  public businessTypeGUID: string = null;
  public buyerId: string = null;
  public buyerName: string = null;
  public creditScoringGUID: string = null;
  public currencyGUID: string = null;
  public dateOfEstablish: string = null;
  public dateOfExpiry: string = null;
  public dateOfIssue: string = null;
  public documentStatusGUID: string = null;
  public fixedAssets: number = 0;
  public identificationType: IdentificationType = null;
  public issuedBy: string = null;
  public kycSetupGUID: string = null;
  public labor: number = 0;
  public lineOfBusinessGUID: string = null;
  public paidUpCapital: number = 0;
  public passportId: string = null;
  public referenceId: string = null;
  public registeredCapital: number = 0;
  public taxId: string = null;
  public workPermitId: string = null;
  //#region  Unbound
  public blacklistStatus_Values: string = null;
  public businessSegment_Values: string = null;
  public businessType_Values: string = null;
  public lineOfBusiness_Values: string = null;
  public creditAppTable_CreditAppTableGUID: string = null;
  public buyerAgreementTable_Description: string = null;
  public buyerAgreementTable_ReferenceAgreementID: string = null;
  public creditAppLine_ExpiryDate: string = null;
  public creditAppLine_CreditAppLineGUID: string = null;
  public methodOfPayment_MethodOfPaymentGUID: string = null;
  public methodOfPayment_Values: string = null;
  public purchaseLine_PurchaseFeeCalculateBase: number = 0;
  public creditAppLine_Values: string = null;
  public creditAppLine_MaxPurchasePct: number = 0;
  public creditAppLine_PurchaseFeePct: number = 0;

  //#endregion
}
