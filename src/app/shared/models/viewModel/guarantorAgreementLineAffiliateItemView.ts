import { ViewCompanyBaseEntity } from 'shared/models/base';

export class GuarantorAgreementLineAffiliateItemView extends ViewCompanyBaseEntity {
  public guarantorAgreementLineAffiliateGUID: string = null;
  public customerTableGUID: string = null;
  public guarantorAgreementLineGUID: string = null;
  public mainAgreementTableGUID: string = null;
  public ordering: number = 0;
  public guarantorAgreementLine_Values: string = null;
  public documentStatus_StatusId: string = null;
  public customer_CustomerGUID: string = null;
  public mainAgreement_ProductType: string = null;
  public guarantorAgreementTable_ParentCompanyGUID: string = null;
}
