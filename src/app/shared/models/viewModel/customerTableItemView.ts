import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RecordType, IdentificationType, DayOfMonth } from 'shared/constants';

export class CustomerTableItemView extends ViewCompanyBaseEntity {
  public customerTableGUID: string = null;
  public age: number = 0;
  public altName: string = null;
  public blacklistStatusGUID: string = null;
  public botRating: string = null;
  public businessSegmentGUID: string = null;
  public businessSizeGUID: string = null;
  public businessTypeGUID: string = null;
  public companyName: string = null;
  public companyWebsite: string = null;
  public creditScoringGUID: string = null;
  public currencyGUID: string = null;
  public custGroupGUID: string = null;
  public customerId: string = null;
  public dateOfBirth: string = null;
  public dateOfEstablish: string = null;
  public dateOfExpiry: string = null;
  public dateOfIssue: string = null;
  public dimension1GUID: string = null;
  public dimension2GUID: string = null;
  public dimension3GUID: string = null;
  public dimension4GUID: string = null;
  public dimension5GUID: string = null;
  public documentStatusGUID: string = null;
  public dueDay: DayOfMonth = null;
  public exposureGroupGUID: string = null;
  public fixedAsset: number = 0;
  public genderGUID: string = null;
  public gradeClassificationGUID: string = null;
  public identificationType: IdentificationType = null;
  public income: number = 0;
  public introducedByGUID: string = null;
  public introducedByRemark: string = null;
  public invoiceIssuingDay: DayOfMonth = null;
  public issuedBy: string = null;
  public kycSetupGUID: string = null;
  public labor: number = 0;
  public lineOfBusinessGUID: string = null;
  public maritalStatusGUID: string = null;
  public methodOfPaymentGUID: string = null;
  public name: string = null;
  public nationalityGUID: string = null;
  public ncbAccountStatusGUID: string = null;
  public occupationGUID: string = null;
  public otherIncome: number = 0;
  public otherSourceIncome: string = null;
  public paidUpCapital: number = 0;
  public parentCompanyGUID: string = null;
  public passportID: string = null;
  public position: string = null;
  public privateARPct: number = 0;
  public publicARPct: number = 0;
  public raceGUID: string = null;
  public recordType: RecordType = null;
  public referenceId: string = null;
  public registeredCapital: number = 0;
  public registrationTypeGUID: string = null;
  public responsibleByGUID: string = null;
  public signingCondition: string = null;
  public spouseName: string = null;
  public taxID: string = null;
  public territoryGUID: string = null;
  public vendorTableGUID: string = null;
  public withholdingTaxGroupGUID: string = null;
  public workExperienceMonth: number = 0;
  public workExperienceYear: number = 0;
  public workPermitID: string = null;
  public lineOfBusiness_Values: string = null;
  public introducedBy_Values: string = null;
  public businessType_Values: string = null;
  public customerTable_CustomerId: string = null;
  public creditLimitType_CreditLimitTypeId: string = null;
  public passportDuplicateEmployeeName: string = null;
  public isPassportIdDuplicate: boolean = false;
  public isTaxIdDuplicate: boolean = false;
}
