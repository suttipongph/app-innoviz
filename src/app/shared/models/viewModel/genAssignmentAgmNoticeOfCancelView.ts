import { ViewCompanyBaseEntity } from 'shared/models/base';
import { AssignmentAgreementTableItemView } from '.';

export class GenAssignmentAgmNoticeOfCancelView extends ViewCompanyBaseEntity {
  public assignmentAgreementTableGUID: string = null;
  public noticeOfCancellationInternalAssignmentAgreementId: string = null;
  public description: string = null;
  public cancelDate: string = null;
  public documentReasonGUID: string = null;
  public reasonRemark: string = null;
  public resultLabel: string = null;
  public assignmentAgreementTableItemView: AssignmentAgreementTableItemView;
}
