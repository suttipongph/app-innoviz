import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class ReceiptLineItemView extends ViewBranchCompanyBaseEntity {
  public receiptLineGUID: string = null;
  public dueDate: string = null;
  public invoiceAmount: number = 0;
  public invoiceTableGUID: string = null;
  public lineNum: number = 0;
  public receiptTableGUID: string = null;
  public settleAmount: number = 0;
  public settleAmountMST: number = 0;
  public settleBaseAmount: number = 0;
  public settleBaseAmountMST: number = 0;
  public settleTaxAmount: number = 0;
  public settleTaxAmountMST: number = 0;
  public taxAmount: number = 0;
  public taxTableGUID: string = null;
  public whtAmount: number = 0;
  public withholdingTaxTableGUID: string = null;
  public receiptTable_Values: string = null;
  public invoiceTable_Values: string = null;
  public taxTable_Code: string = null;
  public withholdingTaxTable_Code: string = null;
}
