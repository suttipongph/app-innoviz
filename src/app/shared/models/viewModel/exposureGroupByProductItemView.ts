import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType } from 'shared/constants';

export class ExposureGroupByProductItemView extends ViewCompanyBaseEntity {
  public exposureGroupByProductGUID: string = null;
  public exposureAmount: number = 0;
  public exposureGroupGUID: string = null;
  public exposureOutstanding: number = 0;
  public productType: ProductType = null;
  public exposureGroup_Values: string = null;
}
