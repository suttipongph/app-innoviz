import { ViewBaseEntity } from '../base';

export class CompanyListView extends ViewBaseEntity {
  public companyId: string = null;
  public companyGUID: string = null;
  public name: string = null;
  public companyLogo: string = null;
  public secondName: string = null;
  public altName: string = null;
  public taxId: string = null;
  public defaultBranchGUID: string = null;
  public branch_Values: string = null;
}
