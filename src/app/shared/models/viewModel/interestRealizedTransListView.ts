import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, RefType } from 'shared/constants';

export class InterestRealizedTransListView extends ViewCompanyBaseEntity {
  public interestRealizedTransGUID: string = null;
  public accInterestAmount: number = 0;
  public accountingDate: string = null;
  public endDate: string = null;
  public interestDay: number = 0;
  public lineNum: number = 0;
  public startDate: string = null;
  public refGUID: string = null;
  public documentId: string = null;
  public accrued: boolean = false;
  public cancelled: boolean = false;
  public refProcessTransGUID: string = null;
  public processTrans_Value: string = null;
}
