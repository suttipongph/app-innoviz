import { ViewCompanyBaseEntity } from 'shared/models/base';

export class AssignmentAgreementLineListView extends ViewCompanyBaseEntity {
  public assignmentAgreementLineGUID: string = null;
  public buyerAgreementTableGUID: string = null;
  public referenceAgreementID: string = null;
}
