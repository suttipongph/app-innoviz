import { ViewCompanyBaseEntity } from 'shared/models/base';

export class MessengerTableItemView extends ViewCompanyBaseEntity {
  public messengerTableGUID: string = null;
  public address: string = null;
  public driverLicenseId: string = null;
  public name: string = null;
  public phone: string = null;
  public plateNumber: string = null;
  public taxId: string = null;
  public vendorTableGUID: string = null;
  public vendorTable_VendorId: string = null;
}
