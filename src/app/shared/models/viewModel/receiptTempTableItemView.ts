import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, ReceivedFrom, ReceiptTempRefType } from 'shared/constants';

export class ReceiptTempTableItemView extends ViewCompanyBaseEntity {
  public receiptTempTableGUID: string = null;
  public buyerReceiptTableGUID: string = null;
  public buyerTableGUID: string = null;
  public creditAppTableGUID: string = null;
  public currencyGUID: string = null;
  public customerTableGUID: string = null;
  public dimension1GUID: string = null;
  public dimension2GUID: string = null;
  public dimension3GUID: string = null;
  public dimension4GUID: string = null;
  public dimension5GUID: string = null;
  public documentReasonGUID: string = null;
  public documentStatusGUID: string = null;
  public exchangeRate: number = 0;
  public mainReceiptTempGUID: string = null;
  public productType: ProductType = null;
  public receiptAmount: number = 0;
  public receiptDate: string = null;
  public receiptTempId: string = null;
  public receiptTempRefType: ReceiptTempRefType = null;
  public receivedFrom: ReceivedFrom = null;
  public refReceiptTempGUID: string = null;
  public remark: string = null;
  public settleAmount: number = 0;
  public settleFeeAmount: number = 0;
  public suspenseAmount: number = 0;
  public suspenseInvoiceTypeGUID: string = null;
  public transDate: string = null;

  public documentReason_Values: string = null;
  public mainReceiptTemp_Values: string = null;
  public refReceiptTemp_Values: string = null;
  public documentStatus_Values: string = null;
  public invoiceType_ValidateDirectReceiveByCA: boolean = false;
  public documentStatus_StatusId: string = null;

  public customerTable_Values: string = null;
  public buyerTable_Values: string = null;
  public suspenseInvoiceType_Values: string = null;
  public creditAppTable_Values: string = null;
  public process_Id: string = null;
}
