import { ViewCompanyBaseEntity } from 'shared/models/base';

export class AdjustIntercompanyInvoiceView extends ViewCompanyBaseEntity {
  public adjustIntercompanyInvoiceGUID: string = null;
  public adjustment: number = 0;
  public balance: number = 0;
  public documentReasonGUID: string = null;
  public intercompanyInvoiceId: string = null;
  public originalInvoiceAmount: number = 0;
  public origInvoiceAmount: number = 0;

  public intercompanyInvoiceTableGUID: string = null;
}
