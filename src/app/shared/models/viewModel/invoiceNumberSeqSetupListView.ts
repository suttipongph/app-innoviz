import { ViewBranchCompanyBaseEntity } from '../base';

export class InvoiceNumberSeqSetupListView extends ViewBranchCompanyBaseEntity {
  public branchGUID: string = null;
  public branch_BranchId: string = null;
  public branch_Values: string = null;
  public invoiceNumberSeqSetupGUID: string = null;
  public invoiceTypeGUID: string = null;
  public leaseTypeGUID: string = null;
  public numberSeqTableGUID: string = null;
  public invoiceType_Values: string = null;
  public invoiceType_InvoiceTypeId: string = null;
  public invoiceType_Description: string = null;
  public numberSeqTable_Values: string = null;
  public numberSeqTable_NumberSeqCode: string = null;
  public numberSeqTable_Description: string = null;
  public productType: string = null;
}
