import { ViewReportBaseEntity } from '../base';

export class RptPrintVerificationReportView extends ViewReportBaseEntity {
  public buyerId: string = null;
  public creditAppId: string = null;
  public customerId: string = null;
  public documentStatus: string = null;
  public verificationDate: string = null;
  public verificationTableGUID: string = null;
  public verificationId: string = null;
}
