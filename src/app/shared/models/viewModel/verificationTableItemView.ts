import { ViewCompanyBaseEntity } from 'shared/models/base';

export class VerificationTableItemView extends ViewCompanyBaseEntity {
  public verificationTableGUID: string = null;
  public buyerTableGUID: string = null;
  public buyerTable_Values: string = null;
  public creditAppTableGUID: string = null;
  public customerTableGUID: string = null;
  public customerTable_Values: string = null;
  public description: string = null;
  public documentStatusGUID: string = null;
  public remark: string = null;
  public verificationDate: string = null;
  public verificationId: string = null;
  public documentStatus_Values: string = null;
  public documentStatus_StatusId: string = null;
  public totalVericationLine: number = 0;
  public creditAppTable_Values: string = null;
}
