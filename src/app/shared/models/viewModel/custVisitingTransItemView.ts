import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class CustVisitingTransItemView extends ViewCompanyBaseEntity {
  public custVisitingTransGUID: string = null;
  public custVisitingDate: string = null;
  public custVisitingMemo: string = null;
  public description: string = null;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
  public responsibleByGUID: string = null;
}
