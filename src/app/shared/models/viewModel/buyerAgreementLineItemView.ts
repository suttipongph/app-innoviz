import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BuyerAgreementLineItemView extends ViewCompanyBaseEntity {
  public buyerAgreementLineGUID: string = null;
  public alreadyInvoiced: boolean = false;
  public amount: number = 0;
  public buyerAgreementTableGUID: string = null;
  public description: string = null;
  public dueDate: string = null;
  public period: number = 0;
  public remark: string = null;
  public buyerAgreementTable_Values: string = null;
}
