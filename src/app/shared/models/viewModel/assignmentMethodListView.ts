import { ViewCompanyBaseEntity } from 'shared/models/base';

export class AssignmentMethodListView extends ViewCompanyBaseEntity {
  public assignmentMethodGUID: string = null;
  public assignmentMethodId: string = null;
  public description: string = null;
  public validateAssignmentBalance: boolean = false;
}
