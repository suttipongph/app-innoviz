import { ViewCompanyBaseEntity } from 'shared/models/base';

export class InquiryPurchaseLineOutstandingItemView extends ViewCompanyBaseEntity {
  public purchaseLineGUID: string = null;
  public purchaseTable_Values: string = null;
  public customerTable_Values: string = null;
  public buyerTable_Values: string = null;
  public buyerInvoiceTable_BuyerInvoiceId: string = null;
  public invoiceAmount: number = 0;
  public purchaseAmount: number = 0;
  public outstanding: number = 0;
  public dueDate: string = null;
  public chequeTable_ChequeDate: string = null;
  public billingDate: string = null;
  public collectionDate: string = null;
  public methodOfPayment_Values: string = null;
  public creditAppTableBankAccountControl_BankAccountName: string = null;
  public creditAppTablePDCBank_BankAccountName: string = null;
  public LineNum: number = 0;
  public creditAppLine_Values: string = null;
}
