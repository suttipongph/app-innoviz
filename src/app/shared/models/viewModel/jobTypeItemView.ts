import { ShowDocConVerifyType } from 'shared/constants';
import { ViewCompanyBaseEntity } from 'shared/models/base';

export class JobTypeItemView extends ViewCompanyBaseEntity {
  public jobTypeGUID: string = null;
  public description: string = null;
  public jobTypeId: string = null;
  public showDocConVerifyType: ShowDocConVerifyType = null;
  public assignment: boolean = false;
}
