import { ViewCompanyBaseEntity } from './viewCompanyBaseEntity';
export class CustomerNameHistoryListView extends ViewCompanyBaseEntity {
  public origName: string = null;
  public newName: string = null;
  public origAltName: string = null;
  public newAltName: string = null;
  public customerNameHistoryGUID: string = null;
  public customerTableGUID: string = null;
}

export class CustomerNameHistoryItemView extends ViewCompanyBaseEntity {
  public origName: string = null;
  public newName: string = null;
  public origAltName: string = null;
  public newAltName: string = null;
  public customerNameHistoryGUID: string = null;
  public customerTableGUID: string = null;
}
