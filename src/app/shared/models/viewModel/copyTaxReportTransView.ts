import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CopyTaxReportTransView extends ViewCompanyBaseEntity {
  public refType: number = 0;
  public refGUID: string = null;
  public month: number = 0;
  public resultLabel: string = null;
  public customerTableGUID:string = null;
}
