import { ContactTo } from 'shared/constants';
import { ViewCompanyBaseEntity } from 'shared/models/base';

export class DocumentReturnLineItemView extends ViewCompanyBaseEntity {
  public documentReturnLineGUID: string = null;
  public address: string = null;
  public addressTransGUID: string = null;
  public buyerTableGUID: string = null;
  public contactPersonName: string = null;
  public documentNo: string = null;
  public documentReturnTableGUID: string = null;
  public documentTypeGUID: string = null;
  public lineNum: number = 0;
  public remark: string = null;
  public documentReturnTable_ContactTo: ContactTo = null;
  public documentReturnTable_DocumentStatus_StatusId: string = null;
  public documentReturnTable_DocumentReturnId: string = null;
}
