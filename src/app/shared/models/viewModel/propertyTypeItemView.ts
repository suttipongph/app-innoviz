import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class PropertyTypeItemView extends ViewBranchCompanyBaseEntity {
  public propertyTypeGUID: string = null;
  public description: string = null;
  public propertyTypeId: string = null;
}
