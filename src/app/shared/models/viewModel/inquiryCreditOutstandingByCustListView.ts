import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType } from 'shared/constants';

export class InquiryCreditOutstandingByCustListView extends ViewCompanyBaseEntity {
  public inquiryCreditOutstandingByCustGUID: string = null;
  public accumRetentionAmount: number = 0;
  public approvedCreditLimit: number = 0;
  public arBalance: number = 0;
  public asOfDate: string = null;
  public creditAppId: string = null;
  public creditLimitBalance: number = 0;
  public creditLimitTypeId: string = null;
  public customerId: string = null;
  public productType: ProductType = null;
  public reserveToBeRefund: number = 0;
  public customerTable_Values: string = null;
  public creditLimitType_Values: string = null;
  public creditAppTable_Values: string = null;
  public creditAppTable_CreditAppId: string = null;
  public creditLimitType_CreditLimitTypeId: string = null;
  public customerTable_CustomerId: string = null;
  public customerTableGUID: string = null;
  public creditLimitTypeGUID: string = null;
  public creditAppTableGUID: string = null;
}
