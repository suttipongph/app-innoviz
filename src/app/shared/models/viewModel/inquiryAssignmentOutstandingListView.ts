import { ViewCompanyBaseEntity } from 'shared/models/base';

export class InquiryAssignmentOutstandingListView extends ViewCompanyBaseEntity {
  public inquiryAssignmentOutstandingGUID: string = null;
  public assignmentAgreementAmount: number = 0;
  public buyerId: string = null;
  public customerId: string = null;
  public internalAssignmentAgreementId: string = null;
  public remainingAmount: number = 0;
  public settleAmount: number = 0;
  public customerTable_Values: string = null;
  public buyerTable_Values: string = null;
  public customerTable_CustomerId: string = null;
  public buyerTable_BuyerId: string = null;
}
