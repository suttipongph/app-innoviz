import { ViewCompanyBaseEntity } from 'shared/models/base';

export class ExposureGroupListView extends ViewCompanyBaseEntity {
  public exposureGroupGUID: string = null;
  public description: string = null;
  public exposureGroupId: string = null;
}
