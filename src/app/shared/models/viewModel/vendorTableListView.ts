import { ViewCompanyBaseEntity } from '../base';

export class VendorTableListView extends ViewCompanyBaseEntity {
  public vendorId: string = null;
  public name: string = null;
  public altName: string = null;
  public recordType: number = null;
  public taxId: string = null;
  public externalCode: string = null;
  public vendorTableGUID: string = null;
  public currencyGUID: string = null;
  public languageGUID: string = null;
  public vendGroupGUID: string = null;
  public sentToOtherSystem: boolean = false;
  public currency_CurrencyId: string = null;
  public vendGroup_VendGroupId: string = null;
  public vendGroup_Values: string = null;
  public currency_Values: string = null;
}
