import { ViewCompanyBaseEntity } from 'shared/models/base';

export class ProjectProgressTableListView extends ViewCompanyBaseEntity {
  public projectProgressTableGUID: string = null;
  public description: string = null;
  public projectProgressId: string = null;
  public transDate: string = null;
  public withdrawalTableGUID: string = null;
}
