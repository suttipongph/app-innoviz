import { ViewCompanyBaseEntity } from 'shared/models/base';

export class GuarantorAgreementLineAffiliateListView extends ViewCompanyBaseEntity {
  public guarantorAgreementLineAffiliateGUID: string = null;
  public customerTableGUID: string = null;
  public ordering: number = 0;
  public customer_Values: string = null;
  public guarantorAgreementLineGUID: string = null;
}
