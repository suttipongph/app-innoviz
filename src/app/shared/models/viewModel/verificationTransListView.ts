import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class VerificationTransListView extends ViewCompanyBaseEntity {
  public verificationTransGUID: string = null;
  public buyerId: string = null;
  public verificationTable_VerificationDate: string = null;
  public verificationTableGUID: string = null;
  public verificationTable_Values: string = null;
  public verificationTable_VerificationId: string = null;
  public refGUID: string = null;
}
