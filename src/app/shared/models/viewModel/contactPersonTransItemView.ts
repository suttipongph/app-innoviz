import { ViewCompanyBaseEntity } from 'shared/models/base';
import { IdentificationType, RefType } from 'shared/constants';

export class ContactPersonTransItemView extends ViewCompanyBaseEntity {
  public contactPersonTransGUID: string = null;
  public relatedPersonTable_Email: string = null;
  public relatedPersonTable_Extension: string = null;
  public relatedPersonTable_Fax: string = null;
  public relatedPersonTable_IdentificationType: IdentificationType = null;
  public inActive: boolean = false;
  public relatedPersonTable_LineId: string = null;
  public relatedPersonTable_Mobile: string = null;
  public relatedPersonTable_Name: string = null;
  public relatedPersonTable_PassportId: string = null;
  public relatedPersonTable_Phone: string = null;
  public relatedPersonTable_Position: string = null;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
  public relatedPersonTableGUID: string = null;
  public relatedPersonTable_TaxId: string = null;
  public relatedPersonTable_WorkPermitId: string = null;
  public relatedPersonTable_DateOfBirth: string = null;
  public contactPersonTrans_Values: string = null;
}
