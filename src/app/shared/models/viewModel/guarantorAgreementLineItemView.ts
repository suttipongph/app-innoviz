import { AgreementDocType } from 'shared/constants';
import { ViewCompanyBaseEntity } from 'shared/models/base';

export class GuarantorAgreementLineItemView extends ViewCompanyBaseEntity {
  public guarantorAgreementLineGUID: string = null;
  public affiliate: boolean = false;
  public age: number = 0;
  public dateOfBirth: string = null;
  public dateOfIssue: string = null;
  public guarantorAgreementTableGUID: string = null;
  public guarantorTransGUID: string = null;
  public guaranteeLand: string = null;
  public name: string = null;
  public nationalityGUID: string = null;
  public ordering: number = 0;
  public passportId: string = null;
  public primaryAddress1: string = null;
  public primaryAddress2: string = null;
  public raceGUID: string = null;
  public taxId: string = null;
  public workPermitId: string = null;
  public guarantorAgreementTable_InternalGuarantorAgreementId: string = null;
  public mainAgreement_CreditAppTableGUID: string = null;
  public mainagreement_AgreementDoctype: AgreementDocType = 0;
  public maximumGuaranteeAmount: number = 0;
  public creditAppTableGUID: string = null;
  public customerTableGUID: string = null;
  public approvedCreditLimit: number = 0;
  public documentStatus_StatusId: string = null;
  public operatedBy: string = null;
}
