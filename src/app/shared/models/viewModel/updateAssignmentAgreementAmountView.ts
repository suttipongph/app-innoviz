import { StringMap } from '@angular/compiler/src/compiler_facade_interface';
import { ViewCompanyBaseEntity } from 'shared/models/base';

export class UpdateAssignmentAgreementAmountView extends ViewCompanyBaseEntity {
  public updateAssignmentAgreementAmountGUID: string = null;
  public agreementDate: string = null;
  public assignmentAgreementAmount: number = 0;
  public assignmentAgreementId: string = null;
  public assignmentMethodId: string = null;
  public buyerName: string = null;
  public buyerTableGUID: string = null;
  public customerName: string = null;
  public customerTableGUID: string = null;
  public description: string = null;
  public documentReasonGUID: string = null;
  public internalAssignmentAgreementId: string = null;
  public newAssignmentAgreementAmount: number = 0;
  public reasonRemark: string = null;
  public customerTable_Values: string = null;
  public buyerTable_Values: string = null;
}
