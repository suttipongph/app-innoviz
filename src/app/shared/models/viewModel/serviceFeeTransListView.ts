import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class ServiceFeeTransListView extends ViewCompanyBaseEntity {
  public serviceFeeTransGUID: string = null;
  public amountBeforeTax: number = 0;
  public description: string = null;
  public feeAmount: number = 0;
  public includeTax: boolean = false;
  public invoiceRevenueTypeGUID: string = null;
  public ordering: number = 0;
  public invoiceRevenueType_Values: string = null;
  public invoiceRevenueType_RevenueTypeId: string = null;
  public refGUID: string = null;
}
