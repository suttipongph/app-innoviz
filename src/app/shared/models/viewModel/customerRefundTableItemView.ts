import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, SuspenseInvoiceType, RetentionCalculateBase } from 'shared/constants';
import { SelectItems } from '../systemModel';

export class CustomerRefundTableItemView extends ViewCompanyBaseEntity {
  public customerRefundTableGUID: string = null;
  public creditAppTableGUID: string = null;
  public currencyGUID: string = null;
  public customerRefundId: string = null;
  public customerTableGUID: string = null;
  public description: string = null;
  public dimension1GUID: string = null;
  public dimension2GUID: string = null;
  public dimension3GUID: string = null;
  public dimension4GUID: string = null;
  public dimension5GUID: string = null;
  public documentReasonGUID: string = null;
  public documentReason_Values: string = null;
  public documentStatusGUID: string = null;
  public documentStatus_StatusId: string = null;
  public exchangeRate: number = 0;
  public operReportSignatureGUID: string = null;
  public netAmount: number = 0;
  public paymentAmount: number = 0;
  public productType: ProductType = null;
  public retentionAmount: number = 0;
  public retentionCalculateBase: RetentionCalculateBase = null;
  public retentionPct: number = 0;
  public serviceFeeAmount: number = 0;
  public settleAmount: number = 0;
  public suspenseAmount: number = 0;
  public suspenseInvoiceType: SuspenseInvoiceType = null;
  public transDate: string = null;
  public remark: string = null;
}
