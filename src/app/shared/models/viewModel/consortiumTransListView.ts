import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class ConsortiumTransListView extends ViewCompanyBaseEntity {
  public consortiumTransGUID: string = null;
  public authorizedPersonTypeGUID: string = null;
  public refGUID: string = null;
  public customerName: string = null;
  public operatedBy: string = null;
  public authorizedPersonType_Values: string = null;
  public authorizedPersonType_AuthorizedPersonTypeId: string = null;
  public ordering: number = 0;
}
