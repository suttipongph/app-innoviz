import { RecordType } from 'shared/constants';
import { ViewCompanyBaseEntity } from 'shared/models/base';

export class StagingTableVendorInfoListView extends ViewCompanyBaseEntity {
  public stagingTableVendorInfoGUID: string = null;
  public bankAccount: string = null;
  public bankAccountName: string = null;
  public bankGroupId: string = null;
  public currencyId: string = null;
  public name: string = null;
  public recordType: RecordType = null;
  public vendGroupId: string = null;
  public vendorId: string = null;
  public processTransGUID: string = null;
}
