import { ViewCompanyBaseEntity } from 'shared/models/base';
import { AgreementDocType, ProductType } from 'shared/constants';

export class ManageAgreementView extends ViewCompanyBaseEntity {
  public manageAgreementGUID: string = null;
  public affiliate: boolean = false;
  public agreementDate: string = null;
  public agreementDocType: AgreementDocType = null;
  public agreementId: string = null;
  public amount: number = 0;
  public assignmentMethodId: string = null;
  public buyerId: string = null;
  public buyerName: string = null;
  public customerId: string = null;
  public customerName: string = null;
  public description: string = null;
  public documentReasonGUID: string = null;
  public expiryDate: string = null;
  public internalAgreementId: string = null;
  public signingDate: string = null;
  public customerTable_Values: string = null;
  public buyerTable_Values: string = null;
  public label: string = null;
  public refType: number = 0;
  public refGUID: string = null;
  public manageAgreementAction: number = null;
  public documentStatusGUID: string = null;
  public productType: ProductType = null;
}
