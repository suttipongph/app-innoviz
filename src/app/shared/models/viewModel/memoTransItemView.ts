import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class MemoTransItemView extends ViewCompanyBaseEntity {
  public memoTransGUID: string = null;
  public memo: string = null;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
  public topic: string = null;
}
