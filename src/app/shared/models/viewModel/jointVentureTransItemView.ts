import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class JointVentureTransItemView extends ViewCompanyBaseEntity {
  public jointVentureTransGUID: string = null;
  public address: string = null;
  public authorizedPersonTypeGUID: string = null;
  public name: string = null;
  public operatedBy: string = null;
  public ordering: number = 0;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
  public remark: string = null;
  public documentStatus_StatusCode: string = null;
}
