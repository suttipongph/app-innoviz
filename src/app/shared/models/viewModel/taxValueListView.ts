import { ViewDateEffectiveBaseEntity } from '../base/viewDateEffectiveBaseEntity';

export class TaxValueListView extends ViewDateEffectiveBaseEntity {
  public value: number = 0;
  public taxValueGUID: string = null;
  public taxTableGUID: string = null;

  public taxTable_TaxCode: string = null;
  public taxTable_Description: string = null;
}
