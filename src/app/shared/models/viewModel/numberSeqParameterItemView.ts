import { ViewCompanyBaseEntity } from '../base';

export class NumberSeqParameterItemView extends ViewCompanyBaseEntity {
  public referenceId: string = null;
  public numberSeqParameterGUID: string = null;
  public numberSeqTableGUID: string = null;
  public numberSeqTable_Description: string = null;
  public numberSeqTable_NumberSeqCode: string = null;
}
