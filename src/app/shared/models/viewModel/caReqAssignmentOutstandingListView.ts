import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CAReqAssignmentOutstandingListView extends ViewCompanyBaseEntity {
  public caReqAssignmentOutstandingGUID: string = null;
  public assignmentAgreementAmount: number = 0;
  public assignmentAgreementTableGUID: string = null;
  public buyerTableGUID: string = null;
  public customerTableGUID: string = null;
  public remainingAmount: number = 0;
  public settleAmount: number = 0;
  public creditAppRequestTableGUID: string = null;
  public customerTable_Values: string = null;
  public customerTable_CustomerId: string = null;
  public assignmentAgreement_Values: string = null;
  public assignmentAgreement_InternalAssignmentAgreementId: string = null;
  public buyerTable_Values: string = null;
  public buyerTable_BuyerId: string = null;
}
