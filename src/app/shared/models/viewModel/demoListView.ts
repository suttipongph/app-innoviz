import { ViewCompanyBaseEntity } from 'shared/models/base';

export class DemoListView extends ViewCompanyBaseEntity {
  public demoGUID?: string = null;
  public demoId?: string = null;
  public description?: string = null;
  public branch_Values?: string = null;
  public accoutingDate?: string = null;
}

export class DemoSearchParam{
  public demoId?: string = null;
  public description?: string = null;
  public documentProcessGUID: string = null;
}