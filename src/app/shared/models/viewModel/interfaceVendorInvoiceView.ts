import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProcessTransType, InterfaceStatus } from 'shared/constants';

export class InterfaceVendorInvoiceView extends ViewCompanyBaseEntity {
  public interfaceVendorInvoiceGUID: string = null;
  public interfaceStatus: InterfaceStatus[] = null;
  public processTransType: ProcessTransType[] = null;
}
