import { ViewCompanyBaseEntity } from 'shared/models/base';

export class UpdateCustomerTableStatusParamView extends ViewCompanyBaseEntity {
  public customerTableGUID: string = null;

  public documentStatusGUID: string = null;
}
export class UpdateCustomerTableStatusResultView extends ViewCompanyBaseEntity {
  public customerTableGUID: string = null;

  public documentStatusGUID: string = null;
  public originalDocumentStatus: string = null;
  public originalDocumentStatusGUID: string = null;
  public customerTableId: string = null;
  public resultLabel: string = null;
}
