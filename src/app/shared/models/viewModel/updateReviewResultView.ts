import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ApprovalDecision } from 'shared/constants';

export class UpdateReviewResultView extends ViewCompanyBaseEntity {
  public creditAppRequestTableGUID: string = null;
  public approvalDecision: ApprovalDecision = null;
  public creditAppRequestTable_Values: string = null;
  public documentRemark: string = null;
  public documentReasonGUID: string = null;
  public reviewDate: string = null;
}
