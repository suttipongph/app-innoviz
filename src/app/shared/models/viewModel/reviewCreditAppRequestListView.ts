import { ViewCompanyBaseEntity } from 'shared/models/base';
import { CreditAppRequestType } from 'shared/constants';

export class ReviewCreditAppRequestListView extends ViewCompanyBaseEntity {
  public reviewCreditAppRequestGUID: string = null;
}
