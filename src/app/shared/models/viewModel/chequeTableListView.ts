import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ReceivedFrom, RefType } from 'shared/constants';

export class ChequeTableListView extends ViewCompanyBaseEntity {
  public chequeTableGUID: string = null;
  public amount: number = 0;
  public buyerTableGUID: string = null;
  public chequeBankAccNo: string = null;
  public chequeDate: string = null;
  public chequeNo: string = null;
  public customerTableGUID: string = null;
  public documentStatusGUID: string = null;
  public issuedName: string = null;
  public pdc: boolean = false;
  public recipientName: string = null;
  public customerTable_Values: string = null;
  public buyerTable_Values: string = null;
  public documentStatus_Values: string = null;
  public documentStatus_StatusCode: string = null;
  public refGUID: string = null;
  public documentStatus_StatusId: string = null;
  public refType: number = 0;
  public purchaseLine_Value: string = null;
  public withdrawalLine_Value: string = null;
}
