import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ReceivedFrom, CollectionFollowUpResult, Result, RefType } from 'shared/constants';

export class CollectionFollowUpListView extends ViewCompanyBaseEntity {
  public collectionFollowUpGUID: string = null;
  public buyerTableGUID: string = null;
  public collectionAmount: number = 0;
  public collectionDate: string = null;
  public collectionFollowUpResult: CollectionFollowUpResult = null;
  public customerTableGUID: string = null;
  public description: string = null;
  public methodOfPaymentGUID: string = null;
  public newCollectionDate: string = null;
  public receivedFrom: ReceivedFrom = null;
  public customerTable_Values: string = null;
  public buyerTable_Values: string = null;
  public methodOfPayment_Values: string = null;
  public customerTable_CustomerId: string = null;
  public buyerTable_BuyerId: string = null;
  public methodOfPayment_MethodOfPaymentId: string = null;
  public refGUID: string = null;
}
