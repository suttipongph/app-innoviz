import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class AddressDistrictListView extends ViewBranchCompanyBaseEntity {
  public districtId: string = null;
  public name: string = null;
  public addressDistrictGUID: string = null;
  public addressProvinceGUID: string = null;
  public provinceId: string = null;
  public addressProvince_Name: string = null;
  public addressProvince_Values: string = null;
}
