import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BlacklistStatusListView extends ViewCompanyBaseEntity {
  public blacklistStatusGUID: string = null;
  public blacklistStatusId: string = null;
  public description: string = null;
}
