import { ViewCompanyBaseEntity } from 'shared/models/base';
export class BranchItemView extends ViewCompanyBaseEntity {
  public branchId: string = null;
  public name: string = null;
  public branchGUID: string = null;
  public taxBranchGUID: string = null;
  public taxBranch_Values: string = null;
}
