import { ViewCompanyBaseEntity } from 'shared/models/base';
export class TaxTableItemView extends ViewCompanyBaseEntity {
  public taxCode: string = null;
  public description: string = null;
  public paymentTaxTableGUID: string = null;
  public outputTaxLedgerAccount: string = null;
  public inputTaxLedgerAccount: string = null;
  public taxTableGUID: string = null;
}
