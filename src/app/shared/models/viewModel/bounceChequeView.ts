import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BounceChequeView extends ViewCompanyBaseEntity {
  public bounceChequeGUID: string = null;
  public amount: number = 0;
  public buyerTableGUID: string = null;
  public chequeBankAccNo: string = null;
  public chequeBankGroupGUID: string = null;
  public chequeBranch: string = null;
  public chequeDate: string = null;
  public chequeNo: string = null;
  public customerTableGUID: string = null;
  public documentStatusGUID: string = null;
  public issuedName: string = null;
  public pdc: boolean = false;
  public receivedFrom: number = 0;
  public recipientName: string = null;
  public remark: string = null;
  public documentStatus_Values: string = null;
  public customerTable_Value: string = null;
  public buyerTable_Value: string = null;
  public chequeBankGroup_Value: string = null;
}
