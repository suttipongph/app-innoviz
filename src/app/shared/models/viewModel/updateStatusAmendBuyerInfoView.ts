import { ViewCompanyBaseEntity } from 'shared/models/base';
import { CreditAppRequestType, ApprovalDecision } from 'shared/constants';

export class UpdateStatusAmendBuyerInfoView extends ViewCompanyBaseEntity {
  public updateStatusAmendBuyerInfoGUID: string = null;
  public approvalDecision: ApprovalDecision = null;
  public approvedDate: string = null;
  public creditAppRequestId: string = null;
  public creditAppRequestType: CreditAppRequestType = null;
  public customerId: string = null;
  public description: string = null;
  public creditAppRequestTableGUID: string = null;
}
