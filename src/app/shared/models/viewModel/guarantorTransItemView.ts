import { ViewCompanyBaseEntity } from 'shared/models/base';
import { IdentificationType, RefType } from 'shared/constants';

export class GuarantorTransItemView extends ViewCompanyBaseEntity {
  public guarantorTransGUID: string = null;
  public affiliate: boolean = false;
  public age: number = 0;
  public relatedPersonTable_BackgroundSummary: string = null;
  public relatedPersonTable_DateOfBirth: string = null;
  public relatedPersonTable_Email: string = null;
  public relatedPersonTable_Extension: string = null;
  public relatedPersonTable_Fax: string = null;
  public guarantorTypeGUID: string = null;
  public relatedPersonTable_IdentificationType: IdentificationType = null;
  public inActive: boolean = false;
  public relatedPersonTable_LineId: string = null;
  public relatedPersonTable_Mobile: string = null;
  public relatedPersonTable_Name: string = null;
  public relatedPersonTable_NationalityId: string = null;
  public ordering: number = 0;
  public relatedPersonTable_PassportId: string = null;
  public relatedPersonTable_Phone: string = null;
  public relatedPersonTable_RaceId: string = null;
  public relatedPersonTable_RaceGUID: string = null;
  public relatedPersonTable_NationalityGUID: string = null;
  public relatedPersonTable_OperatedBy: string = null;
  public refGuarantorTransGUID: string = null;
  public refGUID: string = null;
  public refID: string = null;
  public refType: RefType = null;
  public relatedPersonTableGUID: string = null;
  public relatedPersonTable_TaxId: string = null;
  public relatedPersonTable_WorkPermitId: string = null;
  public relatedPersonTable_Address1: string = null;
  public relatedPersonTable_Address2: string = null;
  public relatedPersonTable_DateOfIssue: string = null;
  public maximumGuaranteeAmount: number = 0;
}
