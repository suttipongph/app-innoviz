import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType } from 'shared/constants';

export class FreeTextInvoiceTableListView extends ViewCompanyBaseEntity {
  public freeTextInvoiceTableGUID: string = null;
  public creditAppTableGUID: string = null;
  public customerTableGUID: string = null;
  public freeTextInvoiceId: string = null;
  public invoiceTypeGUID: string = null;
  public issuedDate: string = null;
  public customerTable_CustomerId: string = null;
  public invoiceType_InvoiceTypeId: string = null;
  public creditAppTable_CreditAppId: string = null;
  public documentStatus_StatusId: string = null;
  public documentStatus_Values: string = null;
  public documentStatusGUID: string = null;
  public invoiceAmount: number = 0;
}
