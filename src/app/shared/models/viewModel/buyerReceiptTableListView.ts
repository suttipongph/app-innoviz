import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class BuyerReceiptTableListView extends ViewCompanyBaseEntity {
  public buyerReceiptTableGUID: string = null;
  public buyerReceiptAmount: number = 0;
  public buyerReceiptDate: string = null;
  public buyerReceiptId: string = null;
  public buyerTableGUID: string = null;
  public cancel: boolean = false;
  public customerTableGUID: string = null;
  public buyerTable_Values: string = null;
  public customerTable_Values: string = null;
  public refGUID: string = null;
  public buyerTable_BuyerId: string = null;
  public customerTable_CustomerId: string = null;
}
