import { ViewCompanyBaseEntity } from '../base';

export class WithholdingTaxGroupItemView extends ViewCompanyBaseEntity {
  public whtGroupId: string = null;
  public description: string = null;
  public withholdingTaxGroupGUID: string = null;
}
