import { ViewCompanyBaseEntity } from 'shared/models/base';

export class NationalityListView extends ViewCompanyBaseEntity {
  public nationalityGUID: string = null;
  public description: string = null;
  public nationalityId: string = null;
}
