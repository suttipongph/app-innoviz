import { ViewCompanyBaseEntity } from 'shared/models/base';

export class AgreementTableInfoTextItemView extends ViewCompanyBaseEntity {
  public agreementTableInfoTextGUID: string = null;
  public agreementTableInfoGUID: string = null;
  public text1: string = null;
  public text2: string = null;
  public text3: string = null;
  public text4: string = null;
}
