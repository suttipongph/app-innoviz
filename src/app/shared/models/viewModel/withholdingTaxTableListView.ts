import { ViewCompanyBaseEntity } from '../base';

export class WithholdingTaxTableListView extends ViewCompanyBaseEntity {
  public whtCode: string = null;
  public description: string = null;
  public ledgerAccount: string = null;
  public withholdingTaxTableGUID: string = null;
}
