import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class CustTransItemView extends ViewBranchCompanyBaseEntity {
  public custTransGUID: string = null;
  public buyerTableGUID: string = null;
  public creditAppTableGUID: string = null;
  public currencyGUID: string = null;
  public customerTableGUID: string = null;
  public custTransStatus: number = 0;
  public description: string = null;
  public dueDate: string = null;
  public exchangeRate: number = 0;
  public invoiceTableGUID: string = null;
  public invoiceTypeGUID: string = null;
  public lastSettleDate: string = null;
  public productType: number = 0;
  public settleAmount: number = 0;
  public settleAmountMST: number = 0;
  public transAmount: number = 0;
  public transAmountMST: number = 0;
  public transDate: string = null;
  public customerTable_Values: string = null;
  public buyerTable_Values: string = null;
  public invoiceType_Values: string = null;
  public currency_Values: string = null;
  public creditApp_Value: string = null;
  public invoiceTable_Values: string = null;
  public creditAppLineGUID: string = null;
  public creditAppLine_Value: string = null;
}
