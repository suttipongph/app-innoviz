import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, DayOfMonth, RetentionCalculateBase } from 'shared/constants';

export class WithdrawalTableListView extends ViewCompanyBaseEntity {
  public withdrawalTableGUID: string = null;
  public creditTermGUID: string = null;
  public customerTableGUID: string = null;
  public description: string = null;
  public documentStatusGUID: string = null;
  public dueDate: string = null;
  public numberOfExtension: number = 0;
  public termExtension: boolean = false;
  public withdrawalDate: string = null;
  public withdrawalId: string = null;
  public customerTable_Values: string = null;
  public customerTable_CustomerId: string = null;
  public creditTerm_CreditTermId: string = null;
  public documentStatus_StatusId: string = null;
}
