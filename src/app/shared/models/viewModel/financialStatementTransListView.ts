import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class FinancialStatementTransListView extends ViewCompanyBaseEntity {
  public financialStatementTransGUID: string = null;
  public amount: number = 0;
  public description: string = null;
  public ordering: number = 0;
  public year: number = 0;
}
