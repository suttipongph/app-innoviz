import { ViewCompanyBaseEntity } from 'shared/models/base';
import { DocumentTemplateType } from 'shared/constants';

export class BookmarkDocumentTemplateLineItemView extends ViewCompanyBaseEntity {
  public bookmarkDocumentTemplateLineGUID: string = null;
  public bookmarkDocumentGUID: string = null;
  public bookmarkDocumentTemplateTableGUID: string = null;
  public documentTemplateTableGUID: string = null;
  public documentTemplateType: DocumentTemplateType = null;
  public documentTemplateTable_Values: string = null;
}
