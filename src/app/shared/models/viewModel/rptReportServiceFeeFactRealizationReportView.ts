import { ViewReportBaseEntity } from 'shared/models/base';

export class RptReportServiceFeeFactRealizationReportView extends ViewReportBaseEntity {
  public reportServiceFeeFactRealizationGUID: string = null;
  public buyerTableGUID: string[] = null;
  public customerTableGUID: string[] = null;
  public fromCollectionDate: string = null;
  public toCollectionDate: string = null;
}
