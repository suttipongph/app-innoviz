import { ViewCompanyBaseEntity } from 'shared/models/base';

export class GuarantorAgreementLineListView extends ViewCompanyBaseEntity {
  public guarantorAgreementLineGUID: string = null;
  public guarantorTransGUID: string = null;
  public name: string = null;
  public ordering: number = 0;
  public passportId: string = null;
  public taxId: string = null;
  public guarantorAgreementTable_InternalGuarantorAgreementId: string = null;
  public guarantorAgreementTableGUID: string = null;
  public isAffiliateCreated: boolean = true;
  public operatedBy: string = null;
}
