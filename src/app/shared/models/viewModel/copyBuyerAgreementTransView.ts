import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CopyBuyerAgreementTransView extends ViewCompanyBaseEntity {
  public customerTableGUID: string = null;
  public buyerTableGUID: string = null;
  public buyerAgreementTableGUID: string = null;
  public refType: number = 0;
  public refGUID: string = null;
  public resultLabel: string[] = [];
}
