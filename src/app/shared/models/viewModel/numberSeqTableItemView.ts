import { ViewCompanyBaseEntity } from '../base';

export class NumberSeqTableItemView extends ViewCompanyBaseEntity {
  public numberSeqTableGUID: string = null;
  public description: string = null;
  public largest: number = 0;
  public manual: boolean = false;
  public next: number = 0;
  public numberSeqCode: string = null;
  public smallest: number = 0;
  public rowVersion: object[] = null;
}
