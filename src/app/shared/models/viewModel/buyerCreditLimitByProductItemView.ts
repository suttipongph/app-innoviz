import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType } from 'shared/constants';

export class BuyerCreditLimitByProductItemView extends ViewCompanyBaseEntity {
  public buyerCreditLimitByProductGUID: string = null;
  public buyerTableGUID: string = null;
  public creditLimit: number = 0;
  public creditLimitBalance: number = 0;
  public productType: ProductType = null;
}
