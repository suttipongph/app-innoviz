import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CancelBuyerReceiptTableView extends ViewCompanyBaseEntity {
  public buyerReceiptTableGUID: string = null;
  public buyerTable_Values: string = null;
  public buyerReceiptAmount: number = 0;
  public buyerReceiptDate: string = null;
  public buyerReceiptTable_Values: string = null;
  public cancel: boolean = false;
  public customerTable_Values: string = null;
}

export class CancelBuyerReceiptTableResultView {}
