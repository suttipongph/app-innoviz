import { NotificationResponse } from '../systemModel/miscellaneousModel';

export class RecalWithdrawalInterestParamView {
  public withdrawalTableGUID: string = null;
  public creditTermGUID: string = null;
  public dueDate: string = null;
  public totalInterestPct: number = 0;
  public withdrawalAmount: number = 0;
  public withdrawalDate: string = null;
  public withdrawalId: string = null;
}
export class RecalWithdrawalInterestResultView extends NotificationResponse {}
