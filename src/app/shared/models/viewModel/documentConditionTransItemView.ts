import { ViewCompanyBaseEntity } from 'shared/models/base';
import { DocConVerifyType, RefType } from 'shared/constants';

export class DocumentConditionTransItemView extends ViewCompanyBaseEntity {
  public documentConditionTransGUID: string = null;
  public docConVerifyType: DocConVerifyType = null;
  public documentTypeGUID: string = null;
  public mandatory: boolean = false;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
  public refDocumentConditionTransGUID: string = null;
  public inactive: boolean = false;
}
