import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CreditAppReqAssignmentInfoItemView extends ViewCompanyBaseEntity {
  public creditAppReqAssignmentInfoGUID: string = null;
  public creditAppRequestTable_Values: string = null;
  public customerTable_Values: string = null;
}
