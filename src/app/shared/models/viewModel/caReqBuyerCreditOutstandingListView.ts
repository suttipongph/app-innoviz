import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CAReqBuyerCreditOutstandingListView extends ViewCompanyBaseEntity {
  public caReqBuyerCreditOutstandingGUID: string = null;
  public approvedCreditLimitLine: number = 0;
  public arBalance: number = 0;
  public assignmentMethodGUID: string = null;
  public billingResponsibleByGUID: string = null;
  public buyerTableGUID: string = null;
  public lineNum: number = 0;
  public maxPurchasePct: number = 0;
  public methodOfPaymentGUID: string = null;
  public status: string = null;
  public buyerTable_Values: string = null;
  public assignmentMethod_Values: string = null;
  public billingResponsibleBy_Values: string = null;
  public methodOfPayment_Values: string = null;
  public buyerTable_BuyerId: string = null;
  public assignmentMethod_AssignmentMethodId: string = null;
  public billingResponsibleBy_BillingResponsibleById: string = null;
  public methodOfPayment_MethodOfPaymentId: string = null;
  public creditAppRequestTableGUID: string = null;
  public creditAppTable_CreditAppId: string = null;
}
