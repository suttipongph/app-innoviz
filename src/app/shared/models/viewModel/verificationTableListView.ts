import { ViewCompanyBaseEntity } from 'shared/models/base';

export class VerificationTableListView extends ViewCompanyBaseEntity {
  public verificationTableGUID: string = null;
  public buyerTableGUID: string = null;
  public customerTableGUID: string = null;
  public description: string = null;
  public documentStatusGUID: string = null;
  public verificationDate: string = null;
  public verificationId: string = null;
  public documentStatus_Values: string = null;
  public documentStatus_StatusId: string = null;
  public creditAppTableGUID: string = null;
}
