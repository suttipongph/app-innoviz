import { ViewCompanyBaseEntity } from 'shared/models/base';
import { CreditAppRequestType } from 'shared/constants';
import { WorkflowInstanceView } from './workflowView';

export class WorkFlowCreditAppRequestView extends ViewCompanyBaseEntity {
  public workflowInstance: WorkflowInstanceView = null;
  public workFlowCreditAppRequestGUID: string = null;
  public actionName: string = null;
  public actionRemark: string = null;
  public assistMD: string = null;
  public creditAppRequestId: string = null;
  public creditAppRequestType: CreditAppRequestType = null;
  public customerTable_Values: string = null;
  public description: string = null;
  public documentStatus_StatusId: string = null;
  public creditAppRequestTableGUID: string = null;
  public marketingHead: string = null;
  public productType: number = 0;
  public parmProductType: string = null;
  public refCreditAppTableGUID: string = null;
  public parmFolio: string = null;
  public processInstanceId: number = 0;
  public refCreditAppLineGUID: string = null;
  public customerEmail: string = null;
  public rowVersion: object[] = null;
}
