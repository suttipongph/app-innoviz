import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class TaxInvoiceLineListView extends ViewBranchCompanyBaseEntity {
  public taxInvoiceLineGUID: string = null;
  public invoiceRevenueTypeGUID: string = null;
  public lineNum: number = 0;
  public taxAmount: number = 0;
  public taxInvoiceTableGUID: string = null;
  public taxTableGUID: string = null;
  public totalAmount: number = 0;
  public totalAmountBeforeTax: number = 0;
  public invoiceRevenueType_Values: string = null;
  public taxInvoiceTable_Values: string = null;
  public invoiceRevenueType_InvoiceRevenueTypeId: string = null;
  public taxTable_TaxId: string = null;
}
