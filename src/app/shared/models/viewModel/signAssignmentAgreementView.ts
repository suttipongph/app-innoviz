import { ViewCompanyBaseEntity } from 'shared/models/base';

export class SignAssignmentAgreementView extends ViewCompanyBaseEntity {
  public signAssignmentAgreementGUID: string = null;
  public agreementDate: string = null;
  public assignmentAgreementAmount: number = 0;
  public assignmentAgreementId: string = null;
  public assignmentAgreementTableGUID: string = null;
  public assignmentMethodGUID: string = null;
  public buyerName: string = null;
  public buyerTableGUID: string = null;
  public customerName: string = null;
  public customerTableGUID: string = null;
  public description: string = null;
  public signingDate: string = null;
}
