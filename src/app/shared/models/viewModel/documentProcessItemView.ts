import { ViewBaseEntity } from '../base';

export class DocumentProcessItemView extends ViewBaseEntity {
  public documentProcessGUID: string = null;
  public description: string = null;
  public processId: string = null;
}
