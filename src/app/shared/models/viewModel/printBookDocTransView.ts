import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class PrintBookDocTransView extends ViewCompanyBaseEntity {
  public bookmarkDocument_Values: string = null;
  public documentTemplate_Values: string = null;
  public documentStatus_Values: string = null;
  public referenceExternalId: string = null;
  public refId: string = null;
  public refType: RefType = null;
  public refGUID: string = null;
  public documentTemplateTableGUID: string = null;
}
