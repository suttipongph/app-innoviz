import { ViewCompanyBaseEntity } from 'shared/models/base';

export class IntercompanyItemView extends ViewCompanyBaseEntity {
  public intercompanyGUID: string = null;
  public description: string = null;
  public intercompanyId: string = null;
}
