import { ViewCompanyBaseEntity } from 'shared/models/base';
import { AccessMode } from 'shared/constants';

export class SysFeatureRoleView extends ViewCompanyBaseEntity {
  public accessRight: AccessMode = AccessMode.noAccess;
  public sysFeatureRoleGUID: string = null;
  public sysFeatureTableGUID: string = null;
  public roleGUID: string = null;

  public sysFeatureTable_Path = null;
  public sysFeatureTable_FeatureId = null;
  public sysFeatureTable_ParentFeatureId = null;

  public role_DisplayName = null;

  public company_Name: string = null;
}
