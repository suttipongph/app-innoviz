export class PrintPurchaseReportView {
  public printPurchaseGUID: string = null;
  public additionalPurchase: boolean = false;
  public customerId: string = null;
  public documentStatus: string = null;
  public purchaseDate: string = null;
  public purchaseId: string = null;
  public rollbill: boolean = false;
}
