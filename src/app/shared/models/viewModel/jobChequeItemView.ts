import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ChequeSource } from 'shared/constants';

export class JobChequeItemView extends ViewCompanyBaseEntity {
  public jobChequeGUID: string = null;
  public amount: number = 0;
  public chequeBankAccNo: string = null;
  public chequeBankGroupGUID: string = null;
  public chequeBranch: string = null;
  public chequeDate: string = null;
  public chequeNo: string = null;
  public chequeSource: ChequeSource = null;
  public chequeTableGUID: string = null;
  public collectionFollowUpGUID: string = null;
  public messengerJobTableGUID: string = null;
  public recipientName: string = null;
  public messengerJobTable_CustomerTableGUID: string = null;
  public messengerJobTable_BuyerTableGUID: string = null;
  public messengerJobTable_RefGUID: string = null;
}
