import { ViewCompanyBaseEntity } from 'shared/models/base';

export class ProjectProgressTableItemView extends ViewCompanyBaseEntity {
  public projectProgressTableGUID: string = null;
  public description: string = null;
  public projectProgressId: string = null;
  public remark: string = null;
  public transDate: string = null;
  public withdrawalTableGUID: string = null;
  public refId: string = null;
  public withdrawal_Value: string = null;
}
