import { ViewBranchCompanyBaseEntity } from 'shared/models/base';
import { ProductType, ReceivedFrom, RefType } from 'shared/constants';

export class PaymentHistoryListView extends ViewBranchCompanyBaseEntity {
  public paymentHistoryGUID: string = null;
  public creditAppTableGUID: string = null;
  public customerTableGUID: string = null;
  public invoiceTableGUID: string = null;
  public paymentAmount: number = 0;
  public paymentBaseAmount: number = 0;
  public paymentDate: string = null;
  public paymentTaxAmount: number = 0;
  public productType: ProductType = null;
  public receivedFrom: ReceivedFrom = null;
  public receivedDate: string = null;
  public whtAmount: number = 0;
  public customerTable_Values: string = null;
  public creditAppTable_Values: string = null;
  public invoiceTable_Values: string = null;
  public whtSlipReceivedByCustomer: boolean = false;
  public customerTable_CustomerId: string = null;
  public creditAppTable_CreditAppTableId: string = null;
  public invoiceTable_InvoiceTableId: string = null;
}
