import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class AddressSubDistrictItemView extends ViewBranchCompanyBaseEntity {
  public subDistrictId: string = null;
  public name: string = null;
  public addressSubDistrictGUID: string = null;
  public addressDistrictGUID: string = null;
  public addressDistrict_DistrictId: string = null;
  public addressDistrict_Name: string = null;
  public addressProvince_ProvinceId: string = null;
  public addressProvinceGUID: string = null;
}
