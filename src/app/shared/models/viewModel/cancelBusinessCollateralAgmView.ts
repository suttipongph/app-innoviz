import { ViewCompanyBaseEntity } from 'shared/models/base';
import { AgreementDocType } from 'shared/constants';

export class CancelBusinessCollateralAgmView extends ViewCompanyBaseEntity {
  public cancelBusinessCollateralAgmGUID: string = null;
  public agreementDate: string = null;
  public agreementDocType: AgreementDocType = null;
  public businessCollateralAgmDescription: string = null;
  public businessCollateralAgmId: string = null;
  public buyerName: string = null;
  public buyerTableGUID: string = null;
  public cancelDate: string = null;
  public customerTableGUID: string = null;
  public customerId: string = null;
  public customerName: string = null;
  public description: string = null;
  public documentReasonGUID: string = null;
  public internalBusinessCollateralAgmId: string = null;
  public noticeOfCancellationInternalBusineeCollateralId: string = null;
  public BusinessCollateralAgmId: string = null;
  public reasonRemark: string = null;
  public BuyerTable_Values: string = null;
  public customerTable_Values: string = null;
  public DocumentReason_Values: string = null;
}
