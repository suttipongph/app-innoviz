import { ViewCompanyBaseEntity } from 'shared/models/base';
import { WorkflowInstanceView } from './workflowView';

export class WorkflowPurchaseView extends ViewCompanyBaseEntity {
  public workflowInstance: WorkflowInstanceView = null;
  public purchaseTableGUID: string = null;
  public actionName: string = null;
  public actionRemark: string = null;
  public assistMD: string = null;
  public parmFolio: string = null;
  public parmCreditAppTableGUID: string = null;
  public processInstanceId: number = 0;
  public netPaid: number = 0;
  public parmSumPurchaseAmount: number = 0;
  public parmOverCreditBuyer: number = 0;
  public parmOverCreditCA: number = 0;
  public parmOverCreditCALine: number = 0;
  public parmProductType: string = null;
  public parmDocID: string = null;
  public parmRollbill: boolean = false;
  public parmDiffChequeIssuedName: boolean = false;
  public parmDocumentReasonGUID: string = null;

  public creditApplicationId: string = null;
  public customerId: string = null;
  public purchaseId: string = null;
  public documentStatus_StatusId: string = null;
  public rollbill: boolean = false;
  public documentReasonGUID: string = null;
  public rowVersion: object[] = null;
}
