import { ViewCompanyBaseEntity } from '../base';

export class CurrencyListView extends ViewCompanyBaseEntity {
  public currencyId: string = null;
  public name: string = null;
  public currencyGUID: string = null;
}
