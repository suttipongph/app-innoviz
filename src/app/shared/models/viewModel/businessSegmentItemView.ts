import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BusinessSegmentItemView extends ViewCompanyBaseEntity {
  public businessSegmentGUID: string = null;
  public businessSegmentId: string = null;
  public description: string = null;
  public businessSegmentType: number = null;
}
