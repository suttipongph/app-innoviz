import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType } from 'shared/constants';

export class InquiryRetentionOutstandingItemView extends ViewCompanyBaseEntity {
  public inquiryRetentionOutstandingGUID: string = null;
  public accumRetentionAmount: number = 0;
  public creditAppId: string = null;
  public customerId: string = null;
  public maximumRetention: number = 0;
  public productType: ProductType = null;
  public remainingAmount: number = 0;
}
