import { ViewCompanyBaseEntity } from "../base/viewCompanyBaseEntity";
import { NotificationResponse } from "../systemModel/miscellaneousModel";

export class RecalProductSettledTransView extends ViewCompanyBaseEntity {
  public productSettledTransGUID: string = null;
  public documentId: string = null;
  public invoiceSettlementDetailGUID: string = null;
  public invoiceTable_Values: string = null; 
}

export class RecalProductSettledResultView extends NotificationResponse {}