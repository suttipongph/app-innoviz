import { ViewCompanyBaseEntity } from 'shared/models/base';

export class IntercompanyInvoiceAdjustmentItemView extends ViewCompanyBaseEntity {
  public intercompanyInvoiceAdjustmentGUID: string = null;
  public adjustment: number = 0;
  public balance: number = 0;
  public documentReasonGUID: string = null;
  public intercompanyInvoiceTableGUID: string = null;
  public originalAmount: number = 0;
  public documentReason_Values: string = null;
  public intercompanyInvoice_Values: string = null;
}
