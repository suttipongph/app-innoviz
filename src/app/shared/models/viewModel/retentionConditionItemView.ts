import { ViewBranchCompanyBaseEntity } from 'shared/models/base';
import { ProductType, RetentionDeductionMethod, RetentionCalculateBase } from 'shared/constants';

export class RetentionConditionItemView extends ViewBranchCompanyBaseEntity {
  public retentionConditionGUID: string = null;
  public productType: ProductType = null;
  public retentionCalculateBase: RetentionCalculateBase = null;
  public retentionDeductionMethod: RetentionDeductionMethod = null;
}
