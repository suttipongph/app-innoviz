import { ViewCompanyBaseEntity } from 'shared/models/base';

export class ConsortiumLineListView extends ViewCompanyBaseEntity {
  public consortiumLineGUID: string = null;
  public customerName: string = null;
  public isMain: boolean = false;
  public operatedBy: string = null;
  public position: string = null;
  public consortiumTableGUID: string = null;
  public proportionOfShareholderPct: number = 0;
  public ordering: number = 0;
}
