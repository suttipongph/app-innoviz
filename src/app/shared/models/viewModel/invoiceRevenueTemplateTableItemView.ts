import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class InvoiceRevenueTemplateTableItemView extends ViewBranchCompanyBaseEntity {
  public invoiceRevenueTemplateTableGUID: string = null;
  public description: string = null;
  public invoiceRevenueTemplateTableID: string = null;
}
