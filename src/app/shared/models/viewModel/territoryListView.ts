import { ViewCompanyBaseEntity } from 'shared/models/base';
export class TerritoryListView extends ViewCompanyBaseEntity {
  public territoryId: string = null;
  public description: string = null;
  public territoryGUID: string = null;
}
