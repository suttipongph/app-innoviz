import { ViewCompanyBaseEntity } from 'shared/models/base';
import { IdentificationType, RefType } from 'shared/constants';

export class OwnerTransItemView extends ViewCompanyBaseEntity {
  public ownerTransGUID: string = null;
  public age: number = 0;
  public relatedPersonTable_BackgroundSummary: string = null;
  public relatedPersonTable_DateOfBirth: string = null;
  public relatedPersonTable_Email: string = null;
  public relatedPersonTable_Extension: string = null;
  public relatedPersonTable_Fax: string = null;
  public relatedPersonTable_IdentificationType: IdentificationType = null;
  public inActive: boolean = false;
  public relatedPersonTable_LineId: string = null;
  public relatedPersonTable_Mobile: string = null;
  public relatedPersonTable_Name: string = null;
  public ordering: number = 0;
  public relatedPersonTable_PassportId: string = null;
  public relatedPersonTable_Phone: string = null;
  public propotionOfShareholderPct: number = 0;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
  public relatedPersonTableGUID: string = null;
  public relatedPersonTable_TaxId: string = null;
  public relatedPersonTable_WorkPermitId: string = null;
}
