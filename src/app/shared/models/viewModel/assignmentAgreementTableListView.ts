import { ViewCompanyBaseEntity } from 'shared/models/base';
import { AgreementDocType } from 'shared/constants';

export class AssignmentAgreementTableListView extends ViewCompanyBaseEntity {
  public assignmentAgreementTableGUID: string = null;
  public agreementDate: string = null;
  public agreementDocType: AgreementDocType = null;
  public assignmentAgreementAmount: number = 0;
  public assignmentAgreementId: string = null;
  public assignmentMethodGUID: string = null;
  public buyerTableGUID: string = null;
  public customerTableGUID: string = null;
  public documentStatusGUID: string = null;
  public internalAssignmentAgreementId: string = null;
  public documentStatus_Values: string = null;
  public documentStatus_StatusId: string = null;
  public refCreditAppRequestTableGUID: string = null;
  public creditAppReqAssignmentGUID: string = null;
  public creditAppRequestTable_Values: string = null;
  public creditAppRequestTable_CreditAppRequestId: string = null;
  public description: string = null;
}
