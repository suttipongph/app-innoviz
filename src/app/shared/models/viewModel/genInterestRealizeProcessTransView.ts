import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType } from 'shared/constants';

export class GenInterestRealizeProcessTransView extends ViewCompanyBaseEntity {
  public asofDate: string = null;
  public productType: ProductType[] = null;
  public transDate: string = null;
}
