import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class AgreementTableInfoListView extends ViewCompanyBaseEntity {
  public agreementTableInfoGUID: string = null;
}
