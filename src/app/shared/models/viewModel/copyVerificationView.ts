import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CopyVerificationView extends ViewCompanyBaseEntity {
  public copyVerificationGUID: string = null;
  public buyerTableGUID: string = null;
  public creditAppTableGUID: string = null;
  public customerTableGUID: string = null;
  public documentStatusGUID: string = null;
  public verificationDate: string = null;
  public verificationTableGUID: string = null;
  public creditAppTable_Values: string = null;
  public customerTable_Values: string = null;
  public buyerTable_Values: string = null;
  public documentStatus_Values: string = null;
  public haveLine: boolean = false;
}
