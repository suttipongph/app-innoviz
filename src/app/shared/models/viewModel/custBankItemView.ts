import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CustBankItemView extends ViewCompanyBaseEntity {
  public custBankGUID: string = null;
  public accountNumber: string = null;
  public bankAccountControl: boolean = false;
  public bankAccountName: string = null;
  public bankBranch: string = null;
  public bankGroupGUID: string = null;
  public bankTypeGUID: string = null;
  public customerTableGUID: string = null;
  public inActive: boolean = false;
  public pdc: boolean = false;
  public primary: boolean = false;
  public customerTable_Values: string = null;
}
