import { ViewReportBaseEntity } from '../base';

export class PrintReceiptReportView extends ViewReportBaseEntity {
  public receiptTableGUID: string = null;
  public printReceiptGUID: string = null;
  public customerId: string = null;
  public receiptDate: string = null;
  public receiptId: string = null;
  public settleAmount: number = 0;
  public settleBaseAmount: number = 0;
  public settleTaxAmount: number = 0;
  public transDate: string = null;
  public isCopy: boolean = false;
}
