import { PaymentType, ProductType, ReceivedFrom } from 'shared/constants';
import { ViewCompanyBaseEntity } from 'shared/models/base';

export class ReceiptTempPaymDetailItemView extends ViewCompanyBaseEntity {
  public receiptTempPaymDetailGUID: string = null;
  public chequeBankGroupGUID: string = null;
  public chequeBranch: string = null;
  public chequeTableGUID: string = null;
  public companyBank: string = null;
  public methodOfPaymentGUID: string = null;
  public receiptAmount: number = 0;
  public receiptTempTableGUID: string = null;
  public transferReference: string = null;
  public methodOfPayment_PaymentType: PaymentType = null;
  public receiptTempTable_CustomerTableGUID: string = null;
  public receiptTempTable_BuyerTableGUID: string = null;
  public companyBank_Values: string = null;
  public receiptTempTable_ReceivedFrom: ReceivedFrom = null;
  public receiptTempTable_ProductType: ProductType = null;
  public receiptTempTable_ReceiptTempId: string = null;
}
