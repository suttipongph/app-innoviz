import { ViewCompanyBaseEntity } from '../base';

export class VendGroupListView extends ViewCompanyBaseEntity {
  public vendGroupId: string = null;
  public description: string = null;
  public vendGroupGUID: string = null;
}
