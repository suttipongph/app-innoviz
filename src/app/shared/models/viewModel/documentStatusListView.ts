import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class DocumentStatusListView extends ViewBranchCompanyBaseEntity {
  public documentStatusGUID: string = null;
  public documentProcessGUID: string = null;
  public description: string = null;
  public documentStatusID: string = null;
}
