import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CreditAppTransItemView extends ViewCompanyBaseEntity {
  public creditAppTransGUID: string = null;
  public buyerTableGUID: string = null;
  public creditAppLineGUID: string = null;
  public creditAppTableGUID: string = null;
  public creditDeductAmount: number = 0;
  public customerTableGUID: string = null;
  public documentId: string = null;
  public invoiceAmount: number = 0;
  public productType: number = 0;
  public refGUID: string = null;
  public refType: number = 0;
  public transDate: string = null;
  public productType_Values: string = null;
  public refType_Values: string = null;
  public creditAppTable_Values: string = null;
  public customerTable_Values: string = null;
  public buyerTable_Values: string = null;
  public creditAppLine_Values: string = null;
}
