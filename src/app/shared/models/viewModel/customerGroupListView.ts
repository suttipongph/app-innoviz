import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CustGroupListView extends ViewCompanyBaseEntity {
  public custGroupId: string = null;
  public description: string = null;
  public custGroupGUID: string = null;
  public numberSeqTableGUID: string = null;
  public numberSeqTable_Value: string = null;
  public numberSeqCode: string = null;
}
