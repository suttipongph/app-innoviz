import { ViewCompanyBaseEntity } from 'shared/models/base';

export class VerificationLineListView extends ViewCompanyBaseEntity {
  public verificationLineGUID: string = null;
  public pass: boolean = false;
  public verificationTypeGUID: string = null;
  public verificationType_Values: string = null;
  public verificationTableGUID: string = null;
}
