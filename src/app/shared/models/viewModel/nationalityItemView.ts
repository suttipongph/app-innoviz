import { ViewCompanyBaseEntity } from 'shared/models/base';

export class NationalityItemView extends ViewCompanyBaseEntity {
  public nationalityGUID: string = null;
  public description: string = null;
  public nationalityId: string = null;
}
