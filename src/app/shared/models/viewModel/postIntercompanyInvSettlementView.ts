import { ViewCompanyBaseEntity } from 'shared/models/base';

export class PostIntercompanyInvSettlementView extends ViewCompanyBaseEntity {
  public intercompanyInvoiceSettlementGUID: string = null;
  public documentStatus_Values: string = null;
  public intercompanyInvoice_Values: string = null;
  public methodOfPayment_Values: string = null;
  public settleInvoiceAmount: number = 0;
  public transDate: string = null;
}
export class PostIntercompanyInvSettlementResultView extends ViewCompanyBaseEntity {}
