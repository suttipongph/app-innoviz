import { ShowDocConVerifyType } from 'shared/constants';
import { ViewCompanyBaseEntity } from 'shared/models/base';

export class JobTypeListView extends ViewCompanyBaseEntity {
  public jobTypeGUID: string = null;
  public description: string = null;
  public jobTypeId: string = null;
  public assignment: boolean = false;
  public showDocConVerifyType: ShowDocConVerifyType = null;
}
