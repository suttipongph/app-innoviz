import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ServiceFeeCategory, RefType, ProductType } from 'shared/constants';

export class ServiceFeeConditionTransListView extends ViewCompanyBaseEntity {
  public serviceFeeConditionTransGUID: string = null;
  public amountBeforeTax: number = 0;
  public description: string = null;
  public inactive: boolean = false;
  public invoiceRevenueTypeGUID: string = null;
  public ordering: number = 0;
  public invoiceRevenueType_ServiceFeeCategory: ServiceFeeCategory = null;
  public invoiceRevenueType_Values: string = null;
  public invoiceRevenueType_revenueTypeId: string = null;
  public refGUID: string = null;
  public creditAppRequestTable_CreditAppRequestId: string = null;
  public invoiceRevenueType_ProductType: ProductType = null;
  public creditAppRequestTable_CreditAppRequestTableGUID: string = null;
}
