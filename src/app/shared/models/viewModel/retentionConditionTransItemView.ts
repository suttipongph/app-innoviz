import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, RetentionDeductionMethod, RetentionCalculateBase, RefType } from 'shared/constants';

export class RetentionConditionTransItemView extends ViewCompanyBaseEntity {
  public retentionConditionTransGUID: string = null;
  public productType: ProductType = null;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
  public retentionAmount: number = 0;
  public retentionCalculateBase: RetentionCalculateBase = null;
  public retentionDeductionMethod: RetentionDeductionMethod = null;
  public retentionPct: number = 0;
}
