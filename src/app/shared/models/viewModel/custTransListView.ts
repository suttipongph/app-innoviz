import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class CustTransListView extends ViewBranchCompanyBaseEntity {
  public custTransGUID: string = null;
  public buyerTableGUID: string = null;
  public currencyGUID: string = null;
  public customerTableGUID: string = null;
  public custTransStatus: number = 0;
  public dueDate: string = null;
  public invoiceTableGUID: string = null;
  public invoiceTypeGUID: string = null;
  public productType: number = 0;
  public settleAmount: number = 0;
  public transAmount: number = 0;
  public customerId: string = null;
  public customerName: string = null;
  public customerTable_Values: string = null;
  public buyerTable_Values: string = null;
  public invoiceType_Values: string = null;
  public currency_Values: string = null;
  public creditApp_Value: string = null;
  public invoiceTable_Values: string = null;
  public customerTable_CustomerId: string = null;
  public buyerTable_BuyerId: string = null;
  public invoiceType_InvoiceTypeId: string = null;
  public currency_CurrencyId: string = null;
  public creditApp_CreditAppId: string = null;
  public invoiceTable_InvoiceId: string = null;
}
