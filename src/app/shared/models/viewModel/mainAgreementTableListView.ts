import { ViewCompanyBaseEntity } from 'shared/models/base';
import { AgreementDocType, ProductType } from 'shared/constants';

export class MainAgreementTableListView extends ViewCompanyBaseEntity {
  public mainAgreementTableGUID: string = null;
  public agreementDate: string = null;
  public agreementDocType: AgreementDocType = null;
  public buyerTableGUID: string = null;
  public creditAppTableGUID: string = null;
  public creditLimitTypeGUID: string = null;
  public customerTableGUID: string = null;
  public documentStatusGUID: string = null;
  public internalMainAgreementId: string = null;
  public mainAgreementId: string = null;
  public productType: ProductType = null;
  public revolving: boolean = false;
  public documentStatus_Values: string = null;
  public customerTable_Values: string = null;
  public customerTable_CustomerId: string = null;
  public buyerTable_BuyerId: string = null;
  public creditLimitType_CreditLimitTypeId: string = null;
  public refMainAgreementTableGUID: string = null;
  public creditAppTable_Values: string = null;
  public creditAppTable_creditAppId: string = null;
  public documentStatus_StatusId: string = null;
  public description: string = null;
}
