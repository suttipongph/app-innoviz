import { ViewCompanyBaseEntity } from 'shared/models/base';

export class InquiryAssignmentOutstandingItemView extends ViewCompanyBaseEntity {
  public inquiryAssignmentOutstandingGUID: string = null;
  public assignmentAgreementAmount: number = 0;
  public buyerId: string = null;
  public customerId: string = null;
  public internalAssignmentAgreementId: string = null;
  public remainingAmount: number = 0;
  public settleAmount: number = 0;
}
