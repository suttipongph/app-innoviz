import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CopyFinancialStatementTransView extends ViewCompanyBaseEntity {
  public financialStatementTransGUID: string = null;
  public year: number = 0;
  public refGUID: string = null;
  public refType: number = 0;
  public resultLabel: string = null;
}
