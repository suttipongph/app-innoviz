import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CreditTermItemView extends ViewCompanyBaseEntity {
  public creditTermGUID: string = null;
  public creditTermId: string = null;
  public description: string = null;
  public numberOfDays: number = 0;
}
