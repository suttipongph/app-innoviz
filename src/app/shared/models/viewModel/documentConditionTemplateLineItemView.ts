import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class DocumentConditionTemplateLineItemView extends ViewBranchCompanyBaseEntity {
  public documentConditionTemplateLineGUID: string = null;
  public documentConditionTemplateTableGUID: string = null;
  public documentTypeGUID: string = null;
  public mandatory: boolean = false;
  public documentType_Values: string = null;
}
