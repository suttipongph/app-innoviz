import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CopyGuarantorFromCAView extends ViewCompanyBaseEntity {
  public copyGuarantorFromCAGUID: string = null;
  public affiliate: boolean = false;
  public creditAppRequestTable_creditAppRequestTableGUID: string = null;
  public creditAppTable_CreditAppTableGUID: string = null;
  public customerTable_CustomerTableGUID: string = null;
  public description: string = null;
  public guarantorAgreementId: string = null;
  public internalGuarantorAgreementId: string = null;
  public creditApp_ProductType: number;
  public customerTable_Value: string = null;
  public haveLine: number = 0;
  public maximumGuaranteeAmount: string = null;
  public mainAgreementTable_ProductType: number;
}
