import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType, BookmarkDocumentRefType, DocumentTemplateType } from 'shared/constants';

export class BookmarkDocumentTransItemView extends ViewCompanyBaseEntity {
  public bookmarkDocumentTransGUID: string = null;
  public bookmarkDocumentGUID: string = null;
  public documentStatusGUID: string = null;
  public documentTemplateTableGUID: string = null;
  public referenceExternalId: string = null;
  public referenceExternalDate: string = null;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
  public remark: string = null;
  public bookmarkDocument_BookmarkDocumentRefType: BookmarkDocumentRefType = null;
  public bookmarkDocument_DocumentTemplateType: DocumentTemplateType = null;
  public documentStatus_StatusId: string = null;
}
