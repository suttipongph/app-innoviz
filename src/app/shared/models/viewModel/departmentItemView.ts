import { ViewCompanyBaseEntity } from 'shared/models/base';

export class DepartmentItemView extends ViewCompanyBaseEntity {
  public departmentGUID: string = null;
  public departmentId: string = null;
  public description: string = null;
}
