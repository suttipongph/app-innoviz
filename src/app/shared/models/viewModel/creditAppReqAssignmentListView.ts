import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CreditAppReqAssignmentListView extends ViewCompanyBaseEntity {
  public creditAppReqAssignmentGUID: string = null;
  public assignmentAgreementAmount: number = 0;
  public assignmentAgreementTableGUID: string = null;
  public assignmentMethodGUID: string = null;
  public buyerAgreementAmount: number = 0;
  public buyerAgreementTableGUID: string = null;
  public buyerTableGUID: string = null;
  public isNew: boolean = false;
  public remainingAmount: number = 0;
  public creditAppRequestTableGUID: string = null;
  public assignmentAgreementTable_Values: string = null;
  public buyerTable_Values: string = null;
  public assignmentMethod_Values: string = null;
  public buyerAgreementTable_Values: string = null;
  public assignmentAgreementTable_InternalAssignmentAgreementId: string = null;
  public buyerTable_BuyerId: string = null;
  public assignmentMethod_AssignmentMethodId: string = null;
  public buyerAgreementTable_BuyerAgreementId: string = null;
}
