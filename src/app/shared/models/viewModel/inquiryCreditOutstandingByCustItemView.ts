import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType } from 'shared/constants';

export class InquiryCreditOutstandingByCustItemView extends ViewCompanyBaseEntity {
  public inquiryCreditOutstandingByCustGUID: string = null;
  public accumRetentionAmount: number = 0;
  public approvedCreditLimit: number = 0;
  public arBalance: number = 0;
  public asOfDate: string = null;
  public creditAppId: string = null;
  public creditLimitBalance: number = 0;
  public creditLimitTypeId: string = null;
  public customerId: string = null;
  public productType: ProductType = null;
  public reserveToBeRefund: number = 0;
}
