import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class DocumentConditionTemplateLineListView extends ViewBranchCompanyBaseEntity {
  public documentConditionTemplateLineGUID: string = null;
  public documentTypeGUID: string = null;
  public mandatory: boolean = false;
  public documentTypeId: string = null;
  public documentType_Values: string = null;
}
