import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BlacklistStatusItemView extends ViewCompanyBaseEntity {
  public blacklistStatusGUID: string = null;
  public blacklistStatusId: string = null;
  public description: string = null;
}
