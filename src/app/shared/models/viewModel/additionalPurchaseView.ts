import { NotificationResponse } from '../systemModel/miscellaneousModel';

export class AdditionalPurchaseParamView {
  public description: string = null;
  public dueDate: string = null;
  public linePurchaseAmount: number = 0;
  public linePurchasePct: number = 0;
  public purchaseAmount: number = 0;
  public purchaseDate: string = null;
  public purchasePct: number = 0;
  public refDueDate: string = null;
  public refPurchaseAmount: number = 0;
  public refPurchaseDate: string = null;
  public refPurchaseId: string = null;
  public refPurchasePct: number = 0;
  public purchaseLineGUID: string = null;
  public refBuyerInvoiceAmount: number = 0;
}
export class AdditionalPurchaseResultView extends NotificationResponse {}
