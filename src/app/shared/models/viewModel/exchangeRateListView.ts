import { ViewDateEffectiveBaseEntity } from '../base';

export class ExchangeRateListView extends ViewDateEffectiveBaseEntity {
  public exchangeRateTransId: string = null;
  public rate: number = 0;
  public exchangeRateGUID: string = null;
  public currencyGUID: string = null;
  public homeCurrencyGUID: string = null;
  public homeCurrencyId: string = null;
  public homeCurrency_CurrencyId: string = null;
  public currency_CurrencyId: string = null;
}
