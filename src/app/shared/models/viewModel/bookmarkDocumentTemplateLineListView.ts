import { ViewCompanyBaseEntity } from 'shared/models/base';
import { DocumentTemplateType } from 'shared/constants';

export class BookmarkDocumentTemplateLineListView extends ViewCompanyBaseEntity {
  public bookmarkDocumentTemplateLineGUID: string = null;
  public bookmarkDocumentGUID: string = null;
  public documentTemplateTableGUID: string = null;
  public bookmarkDocumentTemplateTableGUID: string = null;
  public documentTemplateType: DocumentTemplateType = null;
  public bookmarkDocument_Values: string = null;
  public documentTemplateTable_Values: string = null;
  public templateId: string = null;
  public bookmarkDocumentId: string = null;
}
