import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType } from 'shared/constants';

export class FreeTextInvoiceTableItemView extends ViewCompanyBaseEntity {
  public freeTextInvoiceTableGUID: string = null;
  public cnReasonGUID: string = null;
  public creditAppTableGUID: string = null;
  public currencyGUID: string = null;
  public customerName: string = null;
  public customerTableGUID: string = null;
  public dimension1GUID: string = null;
  public dimension2GUID: string = null;
  public dimension3GUID: string = null;
  public dimension4GUID: string = null;
  public dimension5GUID: string = null;
  public documentStatusGUID: string = null;
  public dueDate: string = null;
  public exchangeRate: number = 0;
  public freeTextInvoiceId: string = null;
  public invoiceAddress1: string = null;
  public invoiceAddress2: string = null;
  public invoiceAddressGUID: string = null;
  public invoiceAmount: number = 0;
  public invoiceAmountBeforeTax: number = 0;
  public invoiceRevenueTypeGUID: string = null;
  public invoiceTableGUID: string = null;
  public invoiceText: string = null;
  public invoiceTypeGUID: string = null;
  public issuedDate: string = null;
  public mailingInvoiceAddress1: string = null;
  public mailingInvoiceAddress2: string = null;
  public mailingInvoiceAddressGUID: string = null;
  public origInvoice: string = null;
  public origInvoiceAmount: number = 0;
  public origInvoiceId: string = null;
  public origTaxInvoiceAmount: number = 0;
  public origTaxInvoiceId: string = null;
  public productType: ProductType = null;
  public remark: string = null;
  public taxAmount: number = 0;
  public taxBranchId: string = null;
  public taxBranchName: string = null;
  public taxTableGUID: string = null;
  public unboundInvoiceAddress: string = null;
  public unboundMailingInvoiceAddress: string = null;
  public whtAmount: number = 0;
  public withholdingTaxTableGUID: string = null;
  public documentStatus_StatusId: string = null;
  public documentStatus_Description: string = null;

  public taxValueError: string = null;
  public withholdingTaxValueError: string = null;
  public invoiceTable_Values: string = null;
}
export class CustomFreeTextInvoiceTableItemView {
  public invoiceAmount: number = 0;
  public taxAmount: number = 0;
  public whtAmount: number = 0;
  public invoiceAmountBeforeTax: number = 0;

  public withholdingTaxTableGUID: string = null;
  public taxTableGUID: string = null;
  public issuedDate: string = null;

  public taxValueError: string = null;
  public withholdingTaxValueError: string = null;
}
