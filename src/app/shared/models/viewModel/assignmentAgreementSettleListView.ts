import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class AssignmentAgreementSettleListView extends ViewCompanyBaseEntity {
  public assignmentAgreementSettleGUID: string = null;
  public buyerAgreementTableGUID: string = null;
  public settledAmount: number = 0;
  public settledDate: string = null;
  public buyerAgreementTable_Values: string = null;
  public assignmentAgreementTableGUID: string = null;
}
