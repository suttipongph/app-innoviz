import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType } from 'shared/constants';

export class CAReqRetentionOutstandingListView extends ViewCompanyBaseEntity {
  public caReqRetentionOutstandingGUID: string = null;
  public accumRetentionAmount: number = 0;
  public creditAppTableGUID: string = null;
  public customerTableGUID: string = null;
  public maximumRetention: number = 0;
  public productType: ProductType = null;
  public remainingAmount: number = 0;
  public creditAppTable_Values: string = null;
  public creditAppTable_CreditAppId: string = null;
  public customerTable_Values: string = null;
  public customerTable_CustomerId: string = null;
  public creditAppRequestTableGUID: string = null;
}
