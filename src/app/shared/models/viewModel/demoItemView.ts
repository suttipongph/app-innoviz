import { ViewCompanyBaseEntity } from 'shared/models/base';

export class DemoItemView extends ViewCompanyBaseEntity {
  public demoGUID: string = null;
  public demoId: string = null;
  public description: string = null;
  public documentProcessGUID: string = null;
  public documentStatusGUID: string = null;
  public employeeTableGUID: string = null;
  public demoDate: string = null;
  public demoAmount: number = 0;
  public demoPeriod: number = 0;
  public demoFact: boolean = false;
  public demoDocument: string = null;
}
