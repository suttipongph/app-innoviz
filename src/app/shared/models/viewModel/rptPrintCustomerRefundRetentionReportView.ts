import { ViewReportBaseEntity } from '../base';

export class PrintCustomerRefundRetentionReportView extends ViewReportBaseEntity {
  public printCustomerRefundRetentionGUID: string = null;
  public customerId: string = null;
  public customerRefundId: string = null;
  public documentStatus: string = null;
  public netAmount: number = 0;
  public paymentAmount: number = 0;
  public retentionAmount: number = 0;
  public serviceFeeAmount: number = 0;
  public settleAmount: number = 0;
  public suspenseAmount: number = 0;
  public transDate: string = null;
  public customerRefundTableGUID: string = null;
}
