import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RecordType, InterfaceStatus } from 'shared/constants';

export class StagingTableIntercoInvSettleListView extends ViewCompanyBaseEntity {
  public stagingTableIntercoInvSettleGUID: string = null;
  public customerId: string = null;
  public dueDate: string = null;
  public feeLedgerAccount: string = null;
  public intercompanyInvoiceSettlementId: string = null;
  public interfaceStagingBatchId: string = null;
  public interfaceStatus: InterfaceStatus = null;
  public invoiceDate: string = null;
  public methodOfPaymentId: string = null;
  public name: string = null;
  public settleInvoiceAmount: number = 0;
  public intercompanyInvoiceSettlementGUID: string = null;
}
