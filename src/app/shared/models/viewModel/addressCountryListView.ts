import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class AddressCountryListView extends ViewBranchCompanyBaseEntity {
  public addressCountryGUID: string = null;
  public countryId: string = null;
  public name: string = null;
}
