export class Product {
  public code: string;
  public name: string;
  public category: string;
  public quantity: number;
}
