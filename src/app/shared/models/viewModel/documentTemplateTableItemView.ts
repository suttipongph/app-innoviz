import { DocumentTemplateType } from 'shared/constants';
import { ViewCompanyBaseEntity } from '../base';
import { FileInformation } from '../systemModel';

export class DocumentTemplateTableItemView extends ViewCompanyBaseEntity {
  public fileInfo: FileInformation = null;
  public documentTemplateTableGUID: string = null;
  public templateId: string = null;
  public description: string = null;
  public documentTemplateType: DocumentTemplateType = 0;
  public fileName: string = null;
  public base64Data: string = null;
  public filePath: string = null;
}
