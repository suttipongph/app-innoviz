import { ViewCompanyBaseEntity } from '../base';

export class VendBankListView extends ViewCompanyBaseEntity {
  public bankAccount: string = null;
  public bankAccountName: string = null;
  public bankBranch: string = null;
  public vendBankGUID: string = null;
  public bankGroupGUID: string = null;
  public vendorTableGUID: string = null;

  public bankGroup_BankGroupId: string = null;
  public bankGroup_Description: string = null;

  public vendorTable_VendorId: string = null;
  public vendorTable_Name: string = null;
  public primary: boolean = false;
}
