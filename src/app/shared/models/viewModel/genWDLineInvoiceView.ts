import { ViewCompanyBaseEntity } from 'shared/models/base';

export class GenWDLineInvoiceView extends ViewCompanyBaseEntity {
  public documentStatusGUID: string = null;
  public withdrawalLineInvoiceTableGUID: string = null;
}
export class GenWDLineInvoiceResultView extends ViewCompanyBaseEntity {}
