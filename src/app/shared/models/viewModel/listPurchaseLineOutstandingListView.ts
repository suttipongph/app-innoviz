import { ViewCompanyBaseEntity } from 'shared/models/base';

export class ListPurchaseLineOutstandingListView extends ViewCompanyBaseEntity {
  public listPurchaseLineOutstandingGUID: string = null;
  public billingDate: string = null;
  public buyerId: string = null;
  public collectionDate: string = null;
  public customerId: string = null;
  public dueDate: string = null;
  public invoiceAmount: number = 0;
  public methodOfPaymentId: string = null;
  public outstanding: number = 0;
  public purchaseAmount: number = 0;
  public purchaseId: string = null;
  public customerTableGUID: string = null;
  public methodOfPaymentGUID: string = null;
  public purchaseTableGUID: string = null;
  public buyerTableGUID: string = null;
  public purchaseTable_Values: string = null;
  public customerTable_Values: string = null;
  public buyerTable_Values: string = null;
  public methodOfPayment_Values: string = null;
}
