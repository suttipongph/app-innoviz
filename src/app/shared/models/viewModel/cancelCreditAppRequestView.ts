import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CancelCreditAppRequestParamView extends ViewCompanyBaseEntity {
  public creditAppRequestGUID: string = null;
  public documentReasonGUID: string = null;
  public refLabel: string = null;
}
export class CancelCreditAppRequestResultView extends ViewCompanyBaseEntity {
  public creditAppRequestGUID: string = null;
  public creditAppRequestId: string = null;
  public customerId: string = null;
  public customerName: string = null;
  public description: string = null;
  public documentReasonGUID: string = null;
  public documentStatus_Description: string = null;
}
