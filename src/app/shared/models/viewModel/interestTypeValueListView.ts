import { ViewCompanyBaseEntity } from 'shared/models/base';

export class InterestTypeValueListView extends ViewCompanyBaseEntity {
  public interestTypeValueGUID: string = null;
  public effectiveFrom: string = null;
  public effectiveTo: string = null;
  public value: number = 0;
}
