import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class ProjectStatusListView extends ViewBranchCompanyBaseEntity {
  public projectStatusGUID: string = null;
  public description: string = null;
  public projectStatusId: string = null;
}
