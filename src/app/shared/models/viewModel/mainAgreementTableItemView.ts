import { ViewCompanyBaseEntity } from 'shared/models/base';
import { AgreementDocType, ProductType } from 'shared/constants';

export class MainAgreementTableItemView extends ViewCompanyBaseEntity {
  public mainAgreementTableGUID: string = null;
  public agreementDate: string = null;
  public agreementDocType: AgreementDocType = null;
  public agreementextension: number = 0;
  public agreementYear: number = 0;
  public approvedCreditLimit: number = 0;
  public approvedCreditLimitLine: number = 0;
  public buyerAgreementDescription: string = null;
  public buyerAgreementReferenceId: string = null;
  public buyerName: string = null;
  public buyerTableGUID: string = null;
  public consortiumTableGUID: string = null;
  public creditAppRequestTableGUID: string = null;
  public creditAppTableGUID: string = null;
  public creditLimitTypeGUID: string = null;
  public customerAltName: string = null;
  public customerName: string = null;
  public customerTableGUID: string = null;
  public description: string = null;
  public documentReasonGUID: string = null;
  public documentStatusGUID: string = null;
  public expiryDate: string = null;
  public internalMainAgreementId: string = null;
  public mainAgreementId: string = null;
  public maxInterestPct: number = 0;
  public productType: ProductType = null;
  public refMainAgreementId: string = null;
  public refMainAgreementTableGUID: string = null;
  public remark: string = null;
  public creditLimitType_Revolving: boolean = false;
  public signingDate: string = null;
  public startDate: string = null;
  public totalInterestPct: number = 0;
  public withdrawalTable_DueDate: string = null;
  public withdrawalTableGUID: string = null;
  public customerTable_Values: string = null;
  public documentStatus_Values: string = null;
  public documentStatus_StatusId: string = null;
  public creditLimitType_Values: string = null;
  public buyerTable_Values: string = null;
  public customerTable_CustomerId: string = null;
  public buyerTable_BuyerId: string = null;
  public creditLimitType_CreditLimitTypeId: string = null;
  public creditAppRequestTable_CreditAppRequestId: string = null;
  public creditAppRequestTable_Values: string = null;
  public consortiumTable_Values: string = null;
  public mainAgreementTable_Values: string = null;
  public mainAgreementTable_AgreementDocType: AgreementDocType = null;
  public documentReason_Values: string = null;
}
