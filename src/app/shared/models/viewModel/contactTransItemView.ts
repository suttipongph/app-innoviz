import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ContactType, RefType } from 'shared/constants';

export class ContactTransItemView extends ViewCompanyBaseEntity {
  public contactTransGUID: string = null;
  public contactType: ContactType = null;
  public contactValue: string = null;
  public description: string = null;
  public extension: string = null;
  public primaryContact: boolean = false;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
}
