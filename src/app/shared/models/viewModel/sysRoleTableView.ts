import { ViewCompanyBaseEntity } from 'shared/models/base';
import { SysFeatureRoleView } from './sysFeatureRoleView';

export class SysRoleTableView extends ViewCompanyBaseEntity {
  public id: string = null;
  public name: string = null;
  public normalizedName: string = null;
  public displayName: string = null;
  public company_Name: string = null;

  public siteLoginType: number = null;

  public sysFeatureRoleViewList: SysFeatureRoleView[] = null;
}
