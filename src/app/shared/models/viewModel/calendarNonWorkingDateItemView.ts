import { ViewCompanyBaseEntity } from 'shared/models/base';
import { HolidayType } from 'shared/constants';

export class CalendarNonWorkingDateItemView extends ViewCompanyBaseEntity {
  public calendarNonWorkingDateGUID: string = null;
  public calendarDate: string = null;
  public calendarGroupGUID: string = null;
  public description: string = null;
  public holidayType: HolidayType = null;
}
