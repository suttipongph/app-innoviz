import { ViewCompanyBaseEntity } from 'shared/models/base';

export class ReceiptTempPaymDetailListView extends ViewCompanyBaseEntity {
  public receiptTempPaymDetailGUID: string = null;
  public chequeTableGUID: string = null;
  public methodOfPaymentGUID: string = null;
  public methodOfPayment_Values: string = null;
  public chequeTable_Values: string = null;
  public methodOfPayment_MethodOfPaymentId: string = null;
  public chequeTable_ChequeNo: string = null;
  public invoiceSettlementDetail_RefReceiptTempPaymDetailGUID: string = null;
  public receiptAmount: number = 0;
}
