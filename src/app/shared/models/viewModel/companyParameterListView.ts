import {
  CollectionFeeMethod,
  ReceiptGenBy,
  InvoiceIssuing,
  PenaltyBase,
  PaymStructureRefNum,
  NPLMethod,
  ProvisionMethod,
  ProvisionRateBy,
  PenaltyCalculationMethod,
  OverdueType
} from 'shared/constants';
import { ViewCompanyBaseEntity } from '../base';

export class CompanyParameterListView extends ViewCompanyBaseEntity {
  public calcSheetIncTax: boolean = false;
  public vehicleTaxServiceFee: number = 0;
  public calcSheetIncAdditionalCost: boolean = false;
  public collectionFeeMethod: CollectionFeeMethod = null;
  public collectionFeeMinBalCal: number = 0;
  public numberSeqVariable: string = null;
  public receiptGenBy: ReceiptGenBy = null;
  public invoiceIssuingDay: number = 0;
  public invoiceIssuing: InvoiceIssuing = null;
  public penaltyBase: PenaltyBase = null;
  public autoSettleToleranceLedgerAcc: string = null;
  public paymStructureRefNum: PaymStructureRefNum = null;
  public earlyToleranceOverLedgerAcc: string = null;
  public earlyToleranceUnderLedgerAcc: string = null;
  public earlyToleranceOverAmount: number = 0;
  public earlyToleranceUnderAmount: number = 0;
  public nplMethod: NPLMethod = null;
  public botRealization: boolean = false;
  public provisionMethod: ProvisionMethod = null;
  public provisionRateBy: ProvisionRateBy = null;
  public realizedGainExchRateLedgerAcc: string = null;
  public realizedLossExchRateLedgerAcc: string = null;
  public companyParameterGUID: string = null;
  public homeCurrencyGUID: string = null;
  public penaltyInvTypeGUID: string = null;
  public vehicleTaxInvTypeGUID: string = null;
  public initInvAdvInvTypeGUID: string = null;
  public initInvDownInvTypeGUID: string = null;
  public installInvTypeGUID: string = null;
  public insuranceInvTypeGUID: string = null;
  public compulsoryInvTypeGUID: string = null;
  public collectionInvTypeGUID: string = null;
  public installAdvInvTypeGUID: string = null;
  public initInvDepInvTypeGUID: string = null;
  public depositReturnInvTypeGUID: string = null;
  public discountMethodOfPaymentGUID: string = null;
  public waiveMethodOfPaymentGUID: string = null;
  public compulsoryTaxGUID: string = null;
  public insuranceTaxGUID: string = null;
  public maintenanceTaxGUID: string = null;
  public vehicleTaxServiceFeeTaxGUID: string = null;
  public penaltyCalculationMethod: PenaltyCalculationMethod = null;
  public settlementGraceDay: number = 0;
  public settleByPeriod: boolean = false;
  public collectionOverdueDay: number = 0;
  public leaseType: boolean = false;
  public leaseSubType: boolean = false;
  public agreementType: boolean = false;
  public recordType: boolean = false;
  public productGroup: boolean = false;
  public product: boolean = false;
  public overdueType: OverdueType = null;
  public earlyPayOffToleranceInvTypeGUID: string = null;
  public pennyDifferenceLedgerAcc: string = null;
  public maximumPennyDifference: number = 0;
  public earlyToleranceMethodOfPaymentGUID: string = null;

  // currency
  public homeCurrency_CurrencyId: string = null;
  public homeCurrency_Name: string = null;

  // invoiceType
  public penaltyInvType_InvoiceId: string = null;
  public penaltyInvType_Description: string = null;
  public vehicleTaxInvType_InvoiceId: string = null;
  public vehicleTaxInvType_Description: string = null;
  public initInvAdvInvType_InvoiceId: string = null;
  public initInvAdvInvType_Description: string = null;
  public initInvDownInvType_InvoiceId: string = null;
  public initInvDownInvType_Description: string = null;
  public installInvType_InvoiceId: string = null;
  public installInvType_Description: string = null;
  public insuranceInvType_InvoiceId: string = null;
  public insuranceInvType_Description: string = null;
  public compulsoryInvType_InvoiceId: string = null;
  public compulsoryInvType_Description: string = null;
  public collectionInvType_InvoiceId: string = null;
  public collectionInvType_Description: string = null;
  public installAdvInvType_InvoiceId: string = null;
  public installAdvInvType_Description: string = null;
  public initInvDepInvType_InvoiceId: string = null;
  public initInvDepInvType_Description: string = null;
  public depositReturnInvType_InvoiceId: string = null;
  public depositReturnInvType_Description: string = null;

  // methodOfPaymentGUID
  public discountMethodOfPayment_MethodOfPaymentId: string = null;
  public discountMethodOfPayment_Description: string = null;
  public waiveMethodOfPayment_MethodOfPaymentId: string = null;
  public waiveMethodOfPayment_Description: string = null;

  // taxTable
  public compulsoryTax_TaxCode: string = null;
  public compulsoryTax_Description: string = null;
  public insuranceTax_TaxCode: string = null;
  public insuranceTax_Description: string = null;
  public maintenanceTax_TaxCode: string = null;
  public maintenanceTax_Description: string = null;
  public vehicleTaxServiceFeeTax_TaxCode: string = null;
  public vehicleTaxServiceFeeTax_Description: string = null;
}
