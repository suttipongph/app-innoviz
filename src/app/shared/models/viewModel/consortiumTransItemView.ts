import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class ConsortiumTransItemView extends ViewCompanyBaseEntity {
  public consortiumTransGUID: string = null;
  public address: string = null;
  public authorizedPersonTypeGUID: string = null;
  public consortiumLineGUID: string = null;
  public customerName: string = null;
  public operatedBy: string = null;
  public ordering: number = 0;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
  public remark: string = null;
  public mainAgreementTable_ConsortiumTableGUID: string = null;
}
