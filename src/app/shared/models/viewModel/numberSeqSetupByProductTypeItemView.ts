import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType } from 'shared/constants';

export class NumberSeqSetupByProductTypeItemView extends ViewCompanyBaseEntity {
  public numberSeqSetupByProductTypeGUID: string = null;
  public creditAppNumberSeqGUID: string = null;
  public creditAppRequestNumberSeqGUID: string = null;
  public guarantorAgreementNumberSeqGUID: string = null;
  public internalGuarantorAgreementNumberSeqGUID: string = null;
  public internalMainAgreementNumberSeqGUID: string = null;
  public mainAgreementNumberSeqGUID: string = null;
  public taxInvoiceNumberSeqGUID: string = null;
  public taxCreditNoteNumberSeqGUID: string = null;
  public receiptNumberSeqGUID: string = null;
  public productType: ProductType = null;
}
