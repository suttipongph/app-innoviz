import { ViewCompanyBaseEntity } from 'shared/models/base';

export class ParentCompanyListView extends ViewCompanyBaseEntity {
  public parentCompanyGUID: string = null;
  public description: string = null;
  public parentCompanyId: string = null;
}
