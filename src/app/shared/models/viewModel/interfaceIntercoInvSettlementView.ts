import { ViewCompanyBaseEntity } from 'shared/models/base';
import { InterfaceStatus } from 'shared/constants';

export class InterfaceIntercoInvSettlementView extends ViewCompanyBaseEntity {
  public interfaceIntercoInvSettlementGUID: string = null;
  public interfaceStatus: number[] = [];
}
