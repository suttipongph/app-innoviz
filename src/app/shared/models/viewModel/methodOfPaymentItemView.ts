import { ViewCompanyBaseEntity } from 'shared/models/base';
import { PaymentType, AccountType } from 'shared/constants';

export class MethodOfPaymentItemView extends ViewCompanyBaseEntity {
  public methodOfPaymentGUID: string = null;
  public companyBankGUID: string = null;
  public description: string = '';
  public methodOfPaymentId: string = null;
  public paymentRemark: string = null;
  public paymentType: PaymentType = null;
  public accountNum: string = null;
  public accountType: AccountType = null;
}
