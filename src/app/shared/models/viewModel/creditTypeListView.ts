import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class CreditTypeListView extends ViewBranchCompanyBaseEntity {
  public creditTypeGUID: string = null;
  public creditTypeId: string = null;
  public description: string = null;
}
