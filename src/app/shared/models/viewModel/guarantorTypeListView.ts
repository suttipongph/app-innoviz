import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class GuarantorTypeListView extends ViewBranchCompanyBaseEntity {
  public guarantorTypeGUID: string = null;
  public description: string = null;
  public guarantorTypeId: string = null;
}
