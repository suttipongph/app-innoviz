import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, CreditLimitConditionType, CreditLimitExpiration } from 'shared/constants';

export class CreditLimitTypeListView extends ViewCompanyBaseEntity {
  public creditLimitTypeGUID: string = null;
  public closeDay: number = 0;
  public creditLimitExpiration: CreditLimitExpiration = null;
  public creditLimitExpiryDay: number = 0;
  public creditLimitTypeId: string = null;
  public description: string = null;
  public inactiveDay: number = 0;
  public productType: ProductType = null;
  public revolving: boolean = false;
  public bookmarkOrdering: number = 0;
}
