import { CompanySignatureRefType } from 'shared/constants';
import { ViewBranchCompanyBaseEntity } from '../base';
import { SelectItems } from '../systemModel';

export class CompanySignatureItemView extends ViewBranchCompanyBaseEntity {
  public companySignatureGUID: string = null;
  public refType: CompanySignatureRefType = null;
  public ordering: number = 0;
  public branchGUID: string = null;
  public employeeTableGUID: string = null;
  public employeeTable_EmployeeId: string = null;
  public employeeTable_Name: string = null;
  public branch_Values: string = null;
}
