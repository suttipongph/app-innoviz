import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class FinancialCreditTransItemView extends ViewCompanyBaseEntity {
  public financialCreditTransGUID: string = null;
  public amount: number = 0;
  public bankGroupGUID: string = null;
  public creditTypeGUID: string = null;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
}
