import { ViewCompanyBaseEntity } from '../base';

export class OccupationItemView extends ViewCompanyBaseEntity {
  public occupationId: string = null;
  public description: string = null;
  public occupationGUID: string = null;
}
