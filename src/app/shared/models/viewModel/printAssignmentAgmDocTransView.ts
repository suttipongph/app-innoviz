import { ViewCompanyBaseEntity } from 'shared/models/base';
import { AgreementDocType } from 'shared/constants';

export class PrintAssignmentAgmDocTransView extends ViewCompanyBaseEntity {
  public printAssignmentAgmDocTransGUID: string = null;
  public agreementDate: string = null;
  public agreementDocType: AgreementDocType = null;
  public assignmentAgreementId: string = null;
  public assignmentAgreementTableGUID: string = null;
  public assignmentMethodGUID: string = null;
  public description: string = null;
}
