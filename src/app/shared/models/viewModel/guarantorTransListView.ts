import { ViewCompanyBaseEntity } from 'shared/models/base';
import { IdentificationType, RefType } from 'shared/constants';

export class GuarantorTransListView extends ViewCompanyBaseEntity {
  public guarantorTransGUID: string = null;
  public affiliate: boolean = false;
  public guarantorTypeGUID: string = null;
  public inActive: boolean = false;
  public name: string = null;
  public ordering: number = 0;
  public passportId: string = null;
  public taxId: string = null;
  public refGUID: string = null;
  public refType: RefType = null;
  public refGuarantorTransGUID: string = null;
  public guarantorType_Values: string = null;
  public guarantorType_GuarantorTypeId: string = null;
}
