import { ViewBaseEntity } from '../base/viewBaseEntity';
import { FileInformation } from '../systemModel';

export class AttachmentListView extends ViewBaseEntity {
  public attachmentGUID: string = null;
  public refType: number = null;
  public refGUID: string = null;
  public fileName: string = null;
  public fileDescription: string = null;
}
