import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CustBankListView extends ViewCompanyBaseEntity {
  public custBankGUID: string = null;
  public accountNumber: string = null;
  public bankAccountControl: boolean = false;
  public bankAccountName: string = null;
  public bankGroupGUID: string = null;
  public bankTypeGUID: string = null;
  public inActive: boolean = false;
  public pdc: boolean = false;
  public primary: boolean = false;
  public bankGroup_Values: string = null;
  public bankGroup_BankGroupId: string = null;
  public bankType_Values: string = null;
  public bankType_BankTypeId: string = null;
  public customerTableGUID: string = null;
}
