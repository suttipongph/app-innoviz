import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, ReceivedFrom } from 'shared/constants';

export class CancelReceiptTempView extends ViewCompanyBaseEntity {
  public receiptTempTableGUID: string = null;
  public documentReasonGUID: string = null;
  public productType: ProductType = null;
  public reasonRemark: string = null;
  public receiptAmount: number = 0;
  public receiptDate: string = null;
  public receivedFrom: ReceivedFrom = null;
  public settleAmount: number = 0;
  public settleFeeAmount: number = 0;
  public suspenseAmount: number = 0;
  public transDate: string = null;
  public receiptTempTable_Values: string = null;
  public customerTable_Values: string = null;
  public buyerTable_Values: string = null;
  public suspenseInvoiceType_Values: string = null;
  public creditAppTable_Values: string = null;
}
export class CancelReceiptTempResultView {}
