import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ChequeSource } from 'shared/constants';

export class JobChequeListView extends ViewCompanyBaseEntity {
  public jobChequeGUID: string = null;
  public amount: number = 0;
  public chequeBankGroupGUID: string = null;
  public chequeBranch: string = null;
  public chequeDate: string = null;
  public chequeNo: string = null;
  public chequeSource: ChequeSource = null;
  public chequeTableGUID: string = null;
  public collectionFollowUpGUID: string = null;
  public messengerJobTableGUID: string = null;
  public collectionFollowUp_Values: string = null;
  public chequeTable_Values: string = null;
  public bankGroup_Values: string = null;
}
