import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProcessTransType } from 'shared/constants';

export class StagingTransTextItemView extends ViewCompanyBaseEntity {
  public stagingTransTextGUID: string = null;
  public processTransType: ProcessTransType = null;
  public transText: string = null;
}
