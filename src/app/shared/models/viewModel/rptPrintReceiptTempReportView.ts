import { ProductType, ReceivedFrom } from 'shared/constants';
import { ViewReportBaseEntity } from '../base';

export class PrintReceiptTempReportView extends ViewReportBaseEntity {
  public printReceiptTempGUID: string = null;
  public buyerId: string = null;
  public creditAppTableGUID: string = null;
  public customerId: string = null;
  public productType: ProductType = null;
  public receiptAmount: number = 0;
  public receiptDate: string = null;
  public receiptTempId: string = null;
  public receivedFrom: ReceivedFrom = null;
  public settleAmount: number = 0;
  public settleFeeAmount: number = 0;
  public suspenseAmount: number = 0;
  public suspenseInvoiceTypeGUID: string = null;
  public transDate: string = null;
  public suspenseInvoiceType_Values: string = null;
  public creditAppTable_Values: string = null;
}
