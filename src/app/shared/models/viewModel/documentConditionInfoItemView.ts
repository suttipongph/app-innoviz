import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class DocumentConditionInfoItemView extends ViewCompanyBaseEntity {
  public documentConditionInfoGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
}
