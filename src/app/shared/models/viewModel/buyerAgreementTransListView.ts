import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class BuyerAgreementTransListView extends ViewCompanyBaseEntity {
  public buyerAgreementTransGUID: string = null;
  public buyerAgreementLineGUID: string = null;
  public buyerAgreementTableGUID: string = null;
  public buyerAgreementTable_Values: string = null;
  public buyerAgreementLine_Values: string = null;
  public buyerAgreementTable_BuyerAgreementId: string = null;
  public buyerAgreementLine_Period: number = 0;
  public buyerAgreementLine_DueDate: string = null;
  public buyerAgreementLine_Amount: number = 0;
  public refGUID: string = null;
}
