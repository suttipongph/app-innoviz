import { ViewBranchCompanyBaseEntity, ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, SuspenseInvoiceType, CustTransStatus, RefType } from 'shared/constants';

export class InvoiceTableListView extends ViewBranchCompanyBaseEntity {
  public invoiceTableGUID: string = null;
  public buyerInvoiceTableGUID: string = null;
  public buyerInvoiceTable_BuyerInvoiceId: string = null;
  public buyerInvoiceTable_Values: string = null;
  public buyerTableGUID: string = null;
  public buyerTable_BuyerId: string = null;
  public buyerTable_Values: string = null;
  public creditAppTableGUID: string = null;
  public creditAppTableId: string = null;
  public creditAppTable_Values: string = null;
  public customerTableGUID: string = null;
  public customerTable_Values: string = null;
  public customerTable_CustomerId: string = null;
  public documentStatusGUID: string = null;
  public documentStatus_Values: string = null;
  public documentId: string = null;
  public invoiceType_Values: string = null;
  public invoiceType_InvoiceTypeId: string = null;
  public invoiceTypeGUID: string = null;
  public issuedDate: string = null;
  public custTransStatus: CustTransStatus = null;
  public dueDate: string = null;
  public refGUID: string = null;
  public refType: RefType = null;
}
