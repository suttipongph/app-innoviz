import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CreditAppReqBusinessCollateralListView extends ViewCompanyBaseEntity {
  public creditAppReqBusinessCollateralGUID: string = null;
  public businessCollateralSubTypeGUID: string = null;
  public businessCollateralTypeGUID: string = null;
  public businessCollateralValue: number = 0;
  public capitalValuation: number = 0;
  public creditAppRequestTableGUID: string = null;
  public custBusinessCollateralGUID: string = null;
  public customerTableGUID: string = null;
  public description: string = null;
  public isNew: boolean = false;
  public refAgreementDate: string = null;
  public refAgreementId: string = null;
  public customerTable_CustomerId: string = null;
  public creditAppRequestTable_CreditAppRequestId: string = null;
  public custBusinessCollateral_CustBusinessCollateralId: string = null;
  public businessCollateralType_BusinessCollateralTypeId: string = null;
  public businessCollateralSubType_BusinessCollateralSubTypeId: string = null;
  public creditAppRequestTable_Values: string = null;
  public custBusinessCollateral_Values: string = null;
  public businessCollateralType_Values: string = null;
  public businessCollateralSubType_Values: string = null;
  public customerTable_Values: string = null;
}
