import { ViewReportBaseEntity } from '../base';

export class PrintBuyerReceiptReportView extends ViewReportBaseEntity {
  public printBuyerReceiptGUID: string = null;
  public buyerId: string = null;
  public buyerReceiptAmount: number = 0;
  public buyerReceiptDate: string = null;
  public buyerReceiptId: string = null;
  public cancel: boolean = false;
  public customerId: string = null;
}
