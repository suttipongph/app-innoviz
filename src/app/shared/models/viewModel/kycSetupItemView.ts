import { ViewCompanyBaseEntity } from 'shared/models/base';

export class KYCSetupItemView extends ViewCompanyBaseEntity {
  public kycSetupGUID: string = null;
  public description: string = null;
  public kycId: string = null;
}
