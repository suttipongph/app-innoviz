import { ViewCompanyBaseEntity } from 'shared/models/base';
import { MonthOfYear, RefType } from 'shared/constants';

export class TaxReportTransItemView extends ViewCompanyBaseEntity {
  public taxReportTransGUID: string = null;
  public amount: number = 0;
  public description: string = null;
  public month: MonthOfYear = null;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
  public year: number = 0;
}
