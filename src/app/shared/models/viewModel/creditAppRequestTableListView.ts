import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, CreditAppRequestType, CreditLimitExpiration, PurchaseFeeCalculateBase } from 'shared/constants';

export class CreditAppRequestTableListView extends ViewCompanyBaseEntity {
  public creditAppRequestTableGUID: string = null;
  public approvedCreditLimitRequest: number = 0;
  public creditAppRequestId: string = null;
  public creditAppRequestType: CreditAppRequestType = null;
  public creditLimitRequest: number = 0;
  public creditLimitTypeGUID: string = null;
  public customerTableGUID: string = null;
  public documentStatusGUID: string = null;
  public productType: ProductType = null;
  public requestDate: string = null;
  public creditLimitType_Values: string = null;
  public documentStatus_Values: string = null;
  public customerTable_Values: string = null;
  public customerTable_CustomerId: string = null;
  public creditLimitType_CreditLimitTypeId: string = null;
  public documentStatus_StatusId: string = null;
  public refCreditAppTableGUID: string = null;
  // #region For ReviewCreditAppRequest
  public creditAppLineGUID: string = null;
  public buyerTable_Values: string = null;
  public creditAppRequestLine_BuyerId: string = null;
  public creditAppRequestLine_BuyerTableGUID: string = null;
  public refCreditAppTable_CreditAppId: string = null;
  public refCreditAppTable_Values: string = null;
  public buyerCreditLimit: string = null;
  public creditLimitLineRequest: string = null;

  // #endregion For ReviewCreditAppRequest
}
