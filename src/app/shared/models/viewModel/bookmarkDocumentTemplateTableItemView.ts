import { ViewCompanyBaseEntity } from 'shared/models/base';
import { BookmarkDocumentRefType } from 'shared/constants';

export class BookmarkDocumentTemplateTableItemView extends ViewCompanyBaseEntity {
  public bookmarkDocumentTemplateTableGUID: string = null;
  public bookmarkDocumentRefType: BookmarkDocumentRefType = null;
  public bookmarkDocumentTemplateId: string = null;
  public description: string = null;
}
