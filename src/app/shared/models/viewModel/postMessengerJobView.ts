import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ContactTo, Result } from 'shared/constants';

export class PostMessengerJobView extends ViewCompanyBaseEntity {
  public postMessengerJobGUID: string = null;
  public contactPersonName: string = null;
  public contactTo: ContactTo = null;
  public documentStatusGUID: string = null;
  public feeAmount: number = 0;
  public jobDate: string = null;
  public jobDetail: string = null;
  public jobEndTime: string = null;
  public jobStartTime: string = null;
  public jobTypeGUID: string = null;
  public messengerJobTableGUID: string = null;
  public messengerTableGUID: string = null;
  public result: Result = null;
  public resultRemark: string = null;
  public documentStatus_StatusId: string = null;
  public jobId: string = null;
  public jobType_Value: string = null;
  public messengerTable_Value: string = null;

  public messengerJob_Value: string = null;
  public documentStatus_Value: string = null;
  public serviceFeeAmount: number = 0;
}
