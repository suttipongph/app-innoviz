import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, RefType } from 'shared/constants';

export class InterestRealizedTransItemView extends ViewCompanyBaseEntity {
  public interestRealizedTransGUID: string = null;
  public accInterestAmount: number = 0;
  public accountingDate: string = null;
  public accrued: boolean = false;
  public documentId: string = null;
  public endDate: string = null;
  public interestDay: number = 0;
  public interestPerDay: number = 0;
  public lineNum: number = 0;
  public productType: ProductType = null;
  public refGUID: string = null;
  public refID: string = null;
  public refType: RefType = null;
  public startDate: string = null;
  public cancelled: boolean = false;
  public refProcessTransGUID: string = null;
  public processTrans_Value: string = null;
  public processTrans_TransDate: string = null;

  public dimension1GUID: string = null;
  public dimension2GUID: string = null;
  public dimension3GUID: string = null;
  public dimension4GUID: string = null;
  public dimension5GUID: string = null;

  public dimension1_Values: string = null;
  public dimension2_Values: string = null;
  public dimension3_Values: string = null;
  public dimension4_Values: string = null;
  public dimension5_Values: string = null;
}
