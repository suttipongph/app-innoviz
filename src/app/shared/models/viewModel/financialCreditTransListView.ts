import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class FinancialCreditTransListView extends ViewCompanyBaseEntity {
  public financialCreditTransGUID: string = null;
  public amount: number = 0;
  public bankGroupGUID: string = null;
  public creditTypeGUID: string = null;
  public bankGroup_Values: string = null;
  public creditType_Values: string = null;
  public bankGroup_BankGroupId: string = null;
  public creditType_CreditTypeId: string = null;
  public refGUID: string = null;
}
