import { ViewCompanyBaseEntity } from 'shared/models/base';

export class MaritalStatusItemView extends ViewCompanyBaseEntity {
  public maritalStatusGUID: string = null;
  public maritalStatusId: string = null;
  public description: string = null;
}
