import { ViewCompanyBaseEntity } from 'shared/models/base';
import { IdentificationType, RefType } from 'shared/constants';

export class OwnerTransListView extends ViewCompanyBaseEntity {
  public ownerTransGUID: string = null;
  public inActive: boolean = false;
  public name: string = null;
  public ordering: number = 0;
  public passportId: string = null;
  public taxId: string = null;
  public relatedPersonTable_PassportId: string = null;
  public relatedPersonTable_Name: string = null;
  public relatedPersonTable_TaxId: string = null;
}
