import { ViewCompanyBaseEntity } from 'shared/models/base';

export class UpdateExpectedAgmSignDateView extends ViewCompanyBaseEntity {
  public creditAppTableGUID: string = null;
  public creditAppId: string = null;
  public customerTable_Values: string = null;
  public expectedAgmSigningDate: string = null;
}
