import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BuyerInvoiceTableItemView extends ViewCompanyBaseEntity {
  public buyerInvoiceTableGUID: string = null;
  public amount: number = 0;
  public buyerAgreementLineGUID: string = null;
  public buyerAgreementTableGUID: string = null;
  public buyerId: string = null;
  public buyerInvoiceId: string = null;
  public creditAppId: string = null;
  public creditAppLineGUID: string = null;
  public dueDate: string = null;
  public invoiceDate: string = null;
  public lineNum: string = null;
  public remark: string = null;
  public purchaseLineGUID: string = null;
  public purchaseTableGUID: string = null;
  public documentStatusGUID: string = null;
  public documentStatus_StatusId: string = null;
  public creditAppLine_CreditAppTableGUID: string = null;
  public creditAppLine_BuyerTableGUID: string = null;
  public creditAppTable_CustomerTableGUID: string = null;
}
