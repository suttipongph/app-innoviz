import { AgreementDocType, RefType } from 'shared/constants';
import { ViewCompanyBaseEntity } from '../base';

export class AssignmentAgreementOutstandingView extends ViewCompanyBaseEntity {
  public internalAssignmentAgreementId: string = null;
  public customerTableGUID: string = null;
  public assignmentAgreementTableGUID: string = null;
  public buyerTableGUID: string = null;
  public assignmentAgreementAmount: number = 0;
  public settledAmount: number = 0;
  public remainingAmount: number = 0;
  public customerName: string = null;
  public buyerName: string = null;
  public customerTable_CustomerId: string = null;
  public buyerTable_BuyerId: string = null;
  public customerTable_Values: string = null;
  public buyerTable_Values: string = null;
  public refType: RefType = null;
  public assignmentAgreementId: string = null;
  public documentStatusGUID: string = null;
  public documentStatus_Values: string = null;
  public documentStatus_StatusId: string = null;
  public assignmentMethodGUID: string = null;
  public assignmentMethod_Values: string = null;
  public assignmentMethod_InternalAssignmentAgreementId: string = null;
  public agreementDocType: AgreementDocType = null;
  public agreementDate: string = null;
  public description: string = null;
  public referenceAgreementId: string = null;
}
