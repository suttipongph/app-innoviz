import { ViewCompanyBaseEntity } from 'shared/models/base';

export class InterestTypeListView extends ViewCompanyBaseEntity {
  public interestTypeGUID: string = null;
  public description: string = null;
  public interestTypeId: string = null;
}
