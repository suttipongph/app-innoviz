import { ViewCompanyBaseEntity } from 'shared/models/base';
import { StagingBatchStatus } from 'shared/constants';

export class IntercompanyInvoiceSettlementItemView extends ViewCompanyBaseEntity {
  public intercompanyInvoiceSettlementGUID: string = null;
  public cnReasonGUID: string = null;
  public documentStatusGUID: string = null;
  public intercompanyInvoiceTableGUID: string = null;
  public settleAmount: number = 0;
  public settleInvoiceAmount: number = 0;
  public settleWHTAmount: number = 0;
  public intercompanyInvoiceSettlementId: string = null;
  public origInvoiceAmount: number = 0;
  public origInvoiceId: string = null;
  public origTaxInvoiceAmount: number = 0;
  public origTaxInvoiceId: string = null;
  public refIntercompanyInvoiceSettlementGUID: string = null;
  public transDate: string = null;
  public whtSlipReceivedByCustomer: boolean = false;
  public intercompanyInvoice_Values: string = null;
  public documentStatus_Values: string = null;
  public documentStatus_StatusId: string = null;
  public methodOfPaymentGUID: string = null;
  public methodOfPayment_Values: string = null;
  public intercomapnyInvoice_IssuedDate: string = null;

  public intercomapnyInvoice_InvoiceAmount: number = 0;
  public cnReason_Values: string = null;
  public refIntercompanyInvoiceSettlement_IntercompanyInvoiceSettlementId: string = null;
}
