import { ViewCompanyBaseEntity } from 'shared/models/base';
import { PaymentType, AccountType } from 'shared/constants';

export class MethodOfPaymentListView extends ViewCompanyBaseEntity {
  public methodOfPaymentGUID: string = null;
  public companyBankGUID: string = null;
  public description: string = null;
  public methodOfPaymentId: string = null;
  public paymentRemark: string = null;
  public paymentType: PaymentType = null;
  public companyBank_Values: string = null;
  public companyBank_AccountNumber: string = null;
  public accountNum: string = null;
  public accountType: AccountType = null;
}
