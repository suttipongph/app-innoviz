import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CloseCAView extends ViewCompanyBaseEntity {
  public closeCAGUID: string = null;
  public creditApplicationId: string = null;
  public customerTableGUID: string = null;
  public documentReasonGUID: string = null;
  public remark: string = null;
}
