import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CopyCustVisitingView extends ViewCompanyBaseEntity {
  public customerTableGUID: string = null;
  public custVisitingTransGUID: string = null;
  public refGUID: string = null;
  public refType: number = 0;
  public resultLabel: string = null;
}
