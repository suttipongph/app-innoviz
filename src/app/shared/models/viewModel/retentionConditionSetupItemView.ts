import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, RetentionDeductionMethod, RetentionCalculateBase } from 'shared/constants';

export class RetentionConditionSetupItemView extends ViewCompanyBaseEntity {
  public retentionConditionSetupGUID: string = null;
  public productType: ProductType = null;
  public retentionCalculateBase: RetentionCalculateBase = null;
  public retentionConditionGUID: string = null;
  public retentionDeductionMethod: RetentionDeductionMethod = null;
}
