import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, ReceivedFrom, ReceiptTempRefType } from 'shared/constants';

export class ReceiptTempTableListView extends ViewCompanyBaseEntity {
  public receiptTempTableGUID: string = null;
  public buyerTableGUID: string = null;
  public currencyGUID: string = null;
  public customerTableGUID: string = null;
  public documentStatusGUID: string = null;
  public productType: ProductType = null;
  public receiptTempRefType: ReceiptTempRefType = null;
  public receiptAmount: number = 0;
  public receiptDate: string = null;
  public receiptTempId: string = null;
  public receivedFrom: ReceivedFrom = null;
  public transDate: string = null;

  public customerTable_CustomerId: string = null;
  public buyerTable_BuyerId: string = null;
  public currency_CurrencyId: string = null;
  public documentStatus_StatusId: string = null;
  public customerTable_Values: string = null;
  public buyerTable_Values: string = null;
  public currency_Values: string = null;
  public documentStatus_Values: string = null;
}
