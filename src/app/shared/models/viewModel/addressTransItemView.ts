import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class AddressTransItemView extends ViewCompanyBaseEntity {
  public addressTransGUID: string = null;
  public address1: string = '';
  public address2: string = '';
  public addressCountryGUID: string = null;
  public addressDistrictGUID: string = null;
  public addressPostalCodeGUID: string = null;
  public addressProvinceGUID: string = null;
  public addressSubDistrictGUID: string = null;
  public currentAddress: boolean = false;
  public isTax: boolean = false;
  public name: string = null;
  public ownershipGUID: string = null;
  public primary: boolean = false;
  public propertyTypeGUID: string = null;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
  public taxBranchId: string = null;
  public taxBranchName: string = null;
  public unboundAddress: string = null;
  public altAddress: string = null;
}
