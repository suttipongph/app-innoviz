import { ViewBranchCompanyBaseEntity } from 'shared/models/base';
import { DocumentTemplateType, AgreementRefType } from 'shared/constants';

export class AgreementDocumentListView extends ViewBranchCompanyBaseEntity {
  public agreementDocumentGUID: string = null;
  public agreementDocumentId: string = null;
  public agreementRefType: AgreementRefType = null;
  public description: string = null;
  public documentTemplateType: DocumentTemplateType = null;
}
