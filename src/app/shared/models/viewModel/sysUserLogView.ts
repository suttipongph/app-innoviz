export class SysUserLogView {
  public createdDateTime: string = null;
  public computerName: string = null;
  public sessionId: string = null;
  public userName: string = null;
  public logOffTime: string = null;
  public ipAddress: string = null;
}
