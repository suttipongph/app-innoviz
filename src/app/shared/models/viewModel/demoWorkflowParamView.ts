export class DemoWorkflowParamView {
  public demoId: string = null;
  public parmDocGUID: string = null;
  public parmOwner: string = null;
  public parmOwnerBusinessUnitGUID: string = null;
}
