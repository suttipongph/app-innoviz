import { RefType } from 'shared/constants';
import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BuyerAgreementTableItemView extends ViewCompanyBaseEntity {
  public buyerAgreementTableGUID: string = null;
  public buyerAgreementAmount: number = 0;
  public buyerAgreementId: string = null;
  public buyerTableGUID: string = null;
  public customerTableGUID: string = null;
  public description: string = null;
  public endDate: string = null;
  public maximumCreditLimit: number = 0;
  public penalty: string = null;
  public referenceAgreementID: string = null;
  public remark: string = null;
  public startDate: string = null;
  public buyerAgreementTable_Values: string = null;
  public buyerAgreementTrans_RefGUID: string = null;
  public creditAppTable_CreditAppTableGUID: string = null;
  public buyerAgreementLine_Description: string = null;
  public buyerAgreementTrans_RefType: RefType = 0;
}
