import { ViewCompanyBaseEntity } from 'shared/models/base';

export class UpdateBuyerTableBlacklistStatusParamView extends ViewCompanyBaseEntity {
  public buyerTableGUID: string = null;
  public blacklistStatusGUID: string = null;
}

export class UpdateBuyerTableBlacklistStatusResultView extends ViewCompanyBaseEntity {
  public buyerTableGUID: string = null;
  public blacklistStatusGUID: string = null;
  public originalBlacklistStatus: string = null;
  public originalBlacklistStatusGUID: string = null;
  public buyerId: string = null;
  public resultLabel: string = null;
}
