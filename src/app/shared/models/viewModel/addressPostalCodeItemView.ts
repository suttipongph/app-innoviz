import { ViewCompanyBaseEntity } from '../base';

export class AddressPostalCodeItemView extends ViewCompanyBaseEntity {
  public postalCode: string = null;
  public addressPostalCodeGUID: string = null;
  public addressSubDistrictGUID: string = null;
  public addressSubDistrict_Value: string = null;
}
