import { ViewCompanyBaseEntity } from 'shared/models/base';

export class DocumentReturnMethodListView extends ViewCompanyBaseEntity {
  public documentReturnMethodGUID: string = null;
  public description: string = null;
  public documentReturnMethodId: string = null;
}
