import { ViewCompanyBaseEntity } from '../base';

export class OwnershipItemView extends ViewCompanyBaseEntity {
  public ownershipId: string = null;
  public description: string = null;
  public ownershipGUID: string = null;
}
