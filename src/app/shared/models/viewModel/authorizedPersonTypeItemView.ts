import { ViewCompanyBaseEntity } from 'shared/models/base';

export class AuthorizedPersonTypeItemView extends ViewCompanyBaseEntity {
  public authorizedPersonTypeGUID: string = null;
  public authorizedPersonTypeId: string = null;
  public description: string = null;
}
