import { ViewDateEffectiveBaseEntity } from '../base';

export class WithholdingTaxValueItemView extends ViewDateEffectiveBaseEntity {
  public value: number = 0;
  public withholdingTaxValueGUID: string = null;
  public withholdingTaxTableGUID: string = null;
  public withholdingTaxTable_WHTCode: string = null;
  public withholdingTaxTable_Description: string = null;
  public withholdingTaxTable_Values: string = null;
}
