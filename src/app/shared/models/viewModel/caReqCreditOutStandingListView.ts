import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType } from 'shared/constants';

export class CAReqCreditOutStandingListView extends ViewCompanyBaseEntity {
  public caReqCreditOutStandingGUID: string = null;
  public accumRetentionAmount: number = 0;
  public approvedCreditLimit: number = 0;
  public arBalance: number = 0;
  public asOfDate: string = null;
  public creditAppTableGUID: string = null;
  public creditLimitBalance: number = 0;
  public creditLimitTypeGUID: string = null;
  public customerTableGUID: string = null;
  public productType: ProductType = null;
  public reserveToBeRefund: number = 0;
  public creditAppRequestTableGUID: string = null;
  public creditLimitType_Values: string = null;
  public creditAppTable_Values: string = null;
  public customerTable_Values: string = null;
  public creditLimitType_CreditLimitTypeId: string = null;
  public creditAppTable_CreditAppId: string = null;
  public customerTable_CustomerId: string = null;
}
