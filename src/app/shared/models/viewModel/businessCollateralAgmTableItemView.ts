import { ViewCompanyBaseEntity } from 'shared/models/base';
import { AgreementDocType, ProductType } from 'shared/constants';

export class BusinessCollateralAgmTableItemView extends ViewCompanyBaseEntity {
  public businessCollateralAgmTableGUID: string = null;
  public agreementDate: string = null;
  public agreementDocType: AgreementDocType = null;
  public agreementExtension: number = 0;
  public businessCollateralAgmId: string = null;
  public creditAppRequestTableGUID: string = null;
  public creditAppTableGUID: string = null;
  public creditLimitTypeGUID: string = null;
  public consortiumTableGUID: string = null;
  public customerAltName: string = null;
  public customerName: string = null;
  public customerTableGUID: string = null;
  public description: string = null;
  public documentReasonGUID: string = null;
  public documentStatusGUID: string = null;
  public internalBusinessCollateralAgmId: string = null;
  public productType: ProductType = null;
  public refBusinessCollateralAgmTableGUID: string = null;
  public signingDate: string = null;
  public documentStatus_StatusId: string = null;
  public documentStatus_Values: string = null;
  public totalBusinessCollateralValue: number = 0;
  public creditAppTable_Values: string = null;
  public creditLimitType_Values: string = null;
  public consortiumTable_Values: string = null;
  public businessCollateralAgmTable_Values: string = null;
  public customerTable_Values: string = null;
  public dbdRegistrationAmount: number = 0;
  public dbdRegistrationDate: string = null;
  public dbdRegistrationDescription: string = null;
  public dbdRegistrationId: string = null;
  public attachmentRemark: string = null;
  public remark: string = null;
  public documentReason_Values: string = null;
}
