import { ViewCompanyBaseEntity } from 'shared/models/base';

export class KYCSetupListView extends ViewCompanyBaseEntity {
  public kycSetupGUID: string = null;
  public description: string = null;
  public kycId: string = null;
}
