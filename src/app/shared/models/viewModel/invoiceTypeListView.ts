import { ProductType, SuspenseInvoiceType } from 'shared/constants';
import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class InvoiceTypeListView extends ViewBranchCompanyBaseEntity {
  public invoiceTypeGUID: string = null;
  public description: string = null;
  public invoiceTypeId: string = null;
  public directReceipt: boolean = false;
  public productType: ProductType = null;
  public productInvoice: boolean = false;
  public validateDirectReceiveByCA: boolean = false;
  public suspenseInvoiceType: SuspenseInvoiceType = null;
  public autoGenInvoiceRevenueTypeGUID: string = null;
  public autoGenInvoiceRevenueType_values: string = null;
}
