import { BatchEndDateType, BatchIntervalType } from 'shared/constants';

export class BatchObjectView {
  public isBatch: boolean = false;
  public taskDescription: string = null;
  public batchApiFunctionMappingKey: string = null;
  public parameters: string = null;
  public startDateTime: DateTimeInput = new DateTimeInput();
  public endDateModel: EndDateModel = new EndDateModel();
  public recurringPatternModel: RecurringPatternModel = new RecurringPatternModel();
  public isEndOfMonth: boolean = false;
  public companyGUID: string = null;
}

export class DateTimeInput {
  public dateInput: string = null;
  public timeInput: string = null;
}

export class EndDateModel {
  public endDateOption: BatchEndDateType = 0;
  public recurringCount: number = 0;
  public endBy: string = null;
}

export class RecurringPatternModel {
  public intervalType: BatchIntervalType = null;
  public intervalCount: string = null;
}
