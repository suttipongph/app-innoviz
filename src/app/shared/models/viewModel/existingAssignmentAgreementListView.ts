import { ViewCompanyBaseEntity } from 'shared/models/base';

export class ExistingAssignmentAgreementListView extends ViewCompanyBaseEntity {
  public existingAssignmentAgreementGUID: string = null;
  public agreementDate: string = null;
  public agreementDocType: number = 0;
  public assignmentAgreementAmount: number = 0;
  public assignmentAgreementId: string = null;
  public assignmentMethodId: string = null;
  public buyerId: string = null;
  public documentStatusId: string = null;
  public internalAssignmentAgreementId: string = null;
  public remainingAmount: number = 0;
}
