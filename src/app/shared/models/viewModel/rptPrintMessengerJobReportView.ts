import { ContactTo } from 'shared/constants';

export class PrintMessengerJobReportView {
  public printMessengerJobGUID: string = null;
  public contactPersonName: string = null;
  public contactTo: ContactTo = null;
  public documentStatusGUID: string = null;
  public documentStatus_Values: string = null;
  public jobDate: string = null;
  public jobDetail: string = null;
  public jobEndTime: string = null;
  public jobStartTime: string = null;
  public jobTime: string = null;
  public jobTypeGUID: string = null;
  public jobType_Values: string = null;
  public messengerJobTableGUID: string = null;
  public messengerJobTable_Values: string = null;
  public messengerTableGUID: string = null;
  public messengerTable_Values: string = null;
}
