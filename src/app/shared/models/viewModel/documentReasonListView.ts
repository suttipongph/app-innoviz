import { RefType } from 'shared/constants';
import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class DocumentReasonListView extends ViewBranchCompanyBaseEntity {
  public documentReasonGUID: string = null;
  public description: string = null;
  public reasonId: string = null;
  public refType: RefType = 0;
}
