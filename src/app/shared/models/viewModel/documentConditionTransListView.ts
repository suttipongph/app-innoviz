import { ViewCompanyBaseEntity } from 'shared/models/base';
import { DocConVerifyType, RefType } from 'shared/constants';

export class DocumentConditionTransListView extends ViewCompanyBaseEntity {
  public documentConditionTransGUID: string = null;
  public documentTypeGUID: string = null;
  public mandatory: boolean = false;
  public documentType_DocumentTypeId: string = null;
  public documentType_Values: string = null;
  public refGUID: string = null;
  public refId: string = null;
  public docConVerifyType: number = 0;
  public refDocumentConditionTransGUID: string = null;
}
