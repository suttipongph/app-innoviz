import { ViewCompanyBaseEntity } from 'shared/models/base';

export class EmployeeTableListView extends ViewCompanyBaseEntity {
  public employeeId: string = null;
  public name: string = null;
  public companyGUID: string = null;
  public employeeTableGUID: string = null;
  public departmentGUID: string = null;
  public assistMD: boolean = false;
  public emplTeamGUID: string = null;
  public dimension5GUID: string = null;
  public dimension4GUID: string = null;
  public dimension3GUID: string = null;
  public dimension2GUID: string = null;
  public dimension1GUID: string = null;
  public inActive: boolean = false;
  public signature: string = null;
  public userId: string = null;

  public department_Values: string = null;
  public department_DepartmentId: string = null;
  public emplTeam_TeamId: string = null;
  public emplTeam_Name: string = null;
  public dimension1_DimensionCode: string = null;
  public dimension1_Description: string = null;
  public dimension2_DimensionCode: string = null;
  public dimension2_Description: string = null;
  public dimension3_DimensionCode: string = null;
  public dimension3_Description: string = null;
  public dimension4_DimensionCode: string = null;
  public dimension4_Description: string = null;
  public dimension5_DimensionCode: string = null;
  public dimension5_Description: string = null;
  public sysuserTable_UserName: string = null;
  public businessUnit_Values: string = null;
  public businessUnit_BusinessUnitId: string = null;
  public businessUnitGUID: string = null;
}
