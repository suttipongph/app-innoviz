import { ViewCompanyBaseEntity } from 'shared/models/base';
export class TaxTableListView extends ViewCompanyBaseEntity {
  public taxCode: string = null;
  public description: string = null;
  public taxTableGUID: string = null;
}
