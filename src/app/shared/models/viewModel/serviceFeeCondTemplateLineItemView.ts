import { ProductType } from 'shared/constants';
import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class ServiceFeeCondTemplateLineItemView extends ViewBranchCompanyBaseEntity {
  public serviceFeeCondTemplateLineGUID: string = null;
  public amountBeforeTax: number = 0;
  public description: string = null;
  public invoiceRevenueTypeGUID: string = null;
  public ordering: number = 0;
  public serviceFeeCondTemplateTableGUID: string = null;
  public serviceFeeCondTemplateTable_Value: string = null;
  public invoiceRevenueType_Value: string = null;
  public productType: ProductType = null;
}
