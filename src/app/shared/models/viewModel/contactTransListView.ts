import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ContactType, RefType } from 'shared/constants';

export class ContactTransListView extends ViewCompanyBaseEntity {
  public contactTransGUID: string = null;
  public contactType: ContactType = null;
  public contactValue: string = null;
  public description: string = null;
  public primaryContact: boolean = false;
}
