import { ViewCompanyBaseEntity } from 'shared/models/base';

export class AgreementTableInfoTextListView extends ViewCompanyBaseEntity {
  public agreementTableInfoTextGUID: string = null;
}
