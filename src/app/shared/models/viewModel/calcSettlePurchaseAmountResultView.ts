export class CalcSettlePurchaseAmountResultView {
  public balanceAmount: number = 0;
  public purchaseAmount: number = 0;
  public purchaseAmountBalance: number = 0;
  public settleReserveAmount: number = 0;
  public settlePurchaseAmount: number = 0;
  public settleInvoiceAmount: number = 0;
  public reserveToBeRefund: number = 0;
  public whtAmount: number = 0;
  public settleTaxAmount: number = 0;
}
