import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BusinessSegmentListView extends ViewCompanyBaseEntity {
  public businessSegmentGUID: string = null;
  public businessSegmentId: string = null;
  public description: string = null;
  public businessSegmentType: number = null;
}
