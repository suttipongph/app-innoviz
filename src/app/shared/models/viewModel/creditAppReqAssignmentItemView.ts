import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CreditAppReqAssignmentItemView extends ViewCompanyBaseEntity {
  public creditAppReqAssignmentGUID: string = null;
  public assignmentAgreementAmount: number = 0;
  public assignmentAgreementTableGUID: string = null;
  public assignmentMethodGUID: string = null;
  public buyerAgreementAmount: number = 0;
  public buyerAgreementTableGUID: string = null;
  public buyerTableGUID: string = null;
  public creditAppRequestTableGUID: string = null;
  public customerTableGUID: string = null;
  public description: string = null;
  public isNew: boolean = false;
  public referenceAgreementId: string = null;
  public remainingAmount: number = 0;
  public creditAppRequestTable_Values: string = null;
  public customerTable_Values: string = null;
}
