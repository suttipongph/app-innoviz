import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class NCBTransItemView extends ViewCompanyBaseEntity {
  public ncbTransGUID: string = null;
  public bankGroupGUID: string = null;
  public creditLimit: number = 0;
  public creditTypeGUID: string = null;
  public endDate: string = null;
  public monthlyRepayment: number = 0;
  public ncbAccountStatusGUID: string = null;
  public outstandingAR: number = 0;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
}
