import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CreditAppTransListView extends ViewCompanyBaseEntity {
  public creditAppTransGUID: string = null;
  public buyerTableGUID: string = null;
  public creditAppLineGUID: string = null;
  public creditAppTableGUID: string = null;
  public creditDeductAmount: number = 0;
  public customerTableGUID: string = null;
  public documentId: string = null;
  public invoiceAmount: number = 0;
  public productType: number = 0;
  public productType_Values: string = null;
  public refType: number = 0;
  public refType_Values: string = null;
  public transDate: string = null;
  public creditAppTable_Values: string = null;
  public customerTable_Values: string = null;
  public buyerTable_Values: string = null;
  public creditAppLine_Values: string = null;
  public creditAppTable_CreditAppId: string = null;
  public customerTable_CustomerId: string = null;
  public buyerTable_BuyerId: string = null;
  public creditAppLine_LineNum: string = null;
}
