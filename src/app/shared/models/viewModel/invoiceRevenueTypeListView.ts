import { AssetFeeType } from 'shared/constants';
import { ViewCompanyBaseEntity } from '../base';

export class InvoiceRevenueTypeListView extends ViewCompanyBaseEntity {
  public invoiceRevenueTypeGUID: string = null;
  public assetFeeType: AssetFeeType = null;
  public description: string = null;
  public feeAmount: number = 0;
  public feeTaxAmount: number = 0;
  public feeTaxGUID: string = null;
  public feeWHTGUID: string = null;
  public revenueTypeId: string = null;
  public serviceFeeRevenueTypeGUID: string = null;
  public withholdingTaxTable_Values: string = null;
  public withholdingTaxTable_WHTCode: string = null;
  public taxTable_Values: string = null;
  public taxTable_TaxCode: string = null;
  public invoiceRevenueType_Values: string = null;
  public invoiceRevenueType_revenueTypeId: string = null;
}
