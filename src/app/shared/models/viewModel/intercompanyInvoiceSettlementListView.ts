import { ViewCompanyBaseEntity } from 'shared/models/base';
import { StagingBatchStatus } from 'shared/constants';

export class IntercompanyInvoiceSettlementListView extends ViewCompanyBaseEntity {
  public intercompanyInvoiceSettlementGUID: string = null;
  public documentStatusGUID: string = null;
  public settleAmount: number = 0;
  public settleInvoiceAmount: number = 0;
  public settleWHTAmount: number = 0;

  public stagingIntercompanyBatchId: string = null;
  public transDate: string = null;
  public whtSlipReceivedByCustomer: boolean = false;
  public intercompanyInvoiceTableGUID: string = null;
  public documentStatus_Values: string = null;
  public documentStatus_StatusId: string = null;
  public methodOfPayment_Values: string = null;
  public methodOfPayment_MethodOfPaymentId: string = null;
  public methodOfPaymentGUID: string = null;
}
