import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BuyerAgreementTableListView extends ViewCompanyBaseEntity {
  public buyerAgreementTableGUID: string = null;
  public buyerAgreementAmount: number = 0;
  public buyerAgreementId: string = null;
  public customerTableGUID: string = null;
  public description: string = null;
  public endDate: string = null;
  public referenceAgreementID: string = null;
  public startDate: string = null;
  public customerTable_CustomerId: string = null;

  public buyerTableGUID: string = null;
}
