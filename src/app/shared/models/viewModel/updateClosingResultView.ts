import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ApprovalDecision } from 'shared/constants';

export class UpdateClosingResultView extends ViewCompanyBaseEntity {
  public creditAppRequestTableGUID: string = null;
  public approvalDecision: ApprovalDecision = null;
  public closingDate: string = null;
  public creditAppRequestTable_Values: string = null;
  public documentReasonGUID: string = null;
  public documentRemark: string = null;
}
