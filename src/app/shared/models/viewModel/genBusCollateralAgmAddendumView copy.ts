import { ViewCompanyBaseEntity } from 'shared/models/base';
import { AgreementDocType, CreditAppRequestType } from 'shared/constants';

export class GenBusCollateralAgmAddendumView extends ViewCompanyBaseEntity {
  public genBusCollateralAgmAddendumGUID: string = null;
  public addendumDate: string = null;
  public addendumInternalBusineeCollateralId: string = null;
  public agreementDate: string = null;
  public agreementDocType: AgreementDocType = null;
  public businessCollateralAgmDescription: string = null;
  public businessCollateralAgmId: string = null;
  public creditAppRequestDescription: string = null;
  public creditAppRequestTableGUID: string = null;
  public creditAppRequestType: CreditAppRequestType = null;
  public creditLimitTypeGUID: string = null;
  public customerId: string = null;
  public customerName: string = null;
  public description: string = null;
  public internalBusineeCollateralId: string = null;
  public requestDate: string = null;
}
