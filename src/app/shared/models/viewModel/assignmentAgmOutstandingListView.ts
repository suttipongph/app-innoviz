import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class AssignmentAgmOutstandingListView extends ViewCompanyBaseEntity {
  public assignmentAgmOutstandingGUID: string = null;
  public assignmentAgreementAmount: number = 0;
  public assignmentAgreementTableGUID: string = null;
  public buyerTableGUID: string = null;
  public customerTableGUID: string = null;
  public settledAmount: number = 0;
}
