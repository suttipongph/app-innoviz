import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class ContactPersonTransListView extends ViewCompanyBaseEntity {
  public contactPersonTransGUID: string = null;
  public inActive: boolean = false;
  public name: string = null;
  public passportId: string = null;
  public phone: string = null;
  public taxId: string = null;
}
