import { ViewDateEffectiveBaseEntity } from '../base/viewDateEffectiveBaseEntity';

export class TaxValueItemView extends ViewDateEffectiveBaseEntity {
  public value: number = 0;
  public taxValueGUID: string = null;
  public taxTableGUID: string = null;
  public taxTable_Values: string = null;
}
