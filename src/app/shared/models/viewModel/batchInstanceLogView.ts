import { BatchResultStatus } from 'shared/constants';

export class BatchInstanceLogView {
  public id: number = null;
  public status: BatchResultStatus = 0;
  public timeStamp: string = null;
  public message: string = null;
  public values: string = null;
  public stackTrace: string = null;
  public instanceHistoryId: string = null;
  public reference: string = null;
}
