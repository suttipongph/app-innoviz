import { ViewCompanyBaseEntity } from 'shared/models/base';

export class ServiceFeeTransCalculateFieldView extends ViewCompanyBaseEntity {
  public amountBeforeTax: number = 0;
  public amountIncludeTax: number = 0;
  public settleInvoiceAmount: number = 0;
  public settleWHTAmount: number = 0;
  public taxAmount: number = 0;
  public whtAmount: number = 0;
  public taxTableGUID: string = null;
  public withholdingTaxTableGUID: string = null;
}
