import { ViewBaseEntity } from 'shared/models/base';

export class MigrationTableItemView extends ViewBaseEntity {
  public migrationTableGUID: string = null;
  public groupName: string = null;
  public groupOrder: number = null;
  public migrationName: string = null;
  public migrationOrder: number = null;
  public migrationResult: string = null;
  public packageName: string = null;
}
