import { Dimension } from 'shared/constants';
import { ViewCompanyBaseEntity } from '../base';

export class LedgerDimensionItemView extends ViewCompanyBaseEntity {
  public dimensionCode: string = null;
  public description: string = null;
  public dimension: Dimension = null;
  public ledgerDimensionGUID: string = null;
}
