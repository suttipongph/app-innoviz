import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class VendorPaymentTransListView extends ViewCompanyBaseEntity {
  public vendorPaymentTransGUID: string = null;
  public amountBeforeTax: number = 0;
  public creditAppTableGUID: string = null;
  public documentStatusGUID: string = null;
  public offsetAccount: string = null;
  public paymentDate: string = null;
  public taxAmount: number = 0;
  public totalAmount: number = 0;
  public vendorTableGUID: string = null;
  public refGUID: string = null;
  public creditAppTable_CreditAppId: string = null;
  public vendorTable_VendorId: string = null;
  public documentStatus_StatusId: string = null;
  public creditAppTable_Values: string = null;
  public vendorTable_Values: string = null;
  public documentStatus_Values: string = null;
}
