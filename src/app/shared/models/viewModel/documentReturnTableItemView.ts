import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ContactTo } from 'shared/constants';

export class DocumentReturnTableItemView extends ViewCompanyBaseEntity {
  public documentReturnTableGUID: string = null;
  public actualReturnDate: string = null;
  public address: string = null;
  public addressTransGUID: string = null;
  public buyerTableGUID: string = null;
  public contactPersonName: string = null;
  public contactTo: ContactTo = null;
  public customerTableGUID: string = null;
  public documentReturnId: string = null;
  public documentReturnMethodGUID: string = null;
  public documentStatusGUID: string = null;
  public expectedReturnDate: string = null;
  public postAcknowledgementNo: string = null;
  public postNumber: string = null;
  public receivedBy: string = null;
  public remark: string = null;
  public requestorGUID: string = null;
  public returnByGUID: string = null;
  public documentStatus_StatusId: string = null;
  public documentStatus_Values: string = null;
  public originalDocumentStatus: string = null;
  public originalDocumentStatusGUID: string = null;
}
