import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class DocumentConditionTemplateTableItemView extends ViewBranchCompanyBaseEntity {
  public documentConditionTemplateTableGUID: string = null;
  public description: string = null;
  public documentConditionTemplateTableId: string = null;
}
