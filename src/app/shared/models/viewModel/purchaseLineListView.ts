import { ViewCompanyBaseEntity } from 'shared/models/base';
import { PurchaseFeeCalculateBase } from 'shared/constants';

export class PurchaseLineListView extends ViewCompanyBaseEntity {
  public purchaseLineGUID: string = null;
  public purchaseTableGUID: string = null;
  public billingDate: string = null;
  public buyerInvoiceAmount: number = 0;
  public buyerInvoiceTableGUID: string = null;
  public buyerTableGUID: string = null;
  public collectionDate: string = null;
  public dueDate: string = null;
  public lineNum: number = 0;
  public methodOfPaymentGUID: string = null;
  public purchaseAmount: number = 0;
  public buyerTable_Values: string = null;
  public buyerTable_BuyerId: string = null;
  public methodOfPayment_Values: string = null;
  public methodOfPayment_MethodOfPaymentId: string = null;
  public buyerInvoiceTable_Values: string = null;
  public buyerInvoiceTable_BuyerInvoiceId: string = null;
}
