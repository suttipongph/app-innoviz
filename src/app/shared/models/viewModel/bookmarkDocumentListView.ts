import { ViewCompanyBaseEntity } from 'shared/models/base';
import { DocumentTemplateType, BookmarkDocumentRefType } from 'shared/constants';

export class BookmarkDocumentListView extends ViewCompanyBaseEntity {
  public bookmarkDocumentGUID: string = null;
  public bookmarkDocumentId: string = null;
  public bookmarkDocumentRefType: BookmarkDocumentRefType = null;
  public description: string = null;
  public documentTemplateType: DocumentTemplateType = null;
}
