import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class CollateralStatusListView extends ViewBranchCompanyBaseEntity {
  public collateralStatusGUID: string = null;
  public collateralStatusID: string = null;
  public description: string = null;
}
