import { ViewCompanyBaseEntity } from 'shared/models/base';
import { CreditAppRequestType } from 'shared/constants';

export class CreditAppRequestLineAmendListView extends ViewCompanyBaseEntity {
  public creditAppRequestLineAmendGUID: string = null;
  public creditAppRequestTable_CreditAppRequestId: string = null;
  public creditAppRequestTable_Description: string = null;
  public creditAppRequestTable_CreditAppRequestType: CreditAppRequestType = null;
  public creditAppRequestTable_RequestDate: string = null;
  public documentStatus_Values: string = null;
  public documentStatus_StatusId: string = null;
  public customerTable_Values: string = null;
  public customerTable_CustomerId: string = null;
  public buyerTable_Values: string = null;
  public buyerTable_BuyerId: string = null;
  public creditAppRequestTable_DocumentStatusGUID: string = null;
  public creditAppRequestTable_CustomerTableGUID: string = null;
  public creditAppRequestTable_BuyerTableGUID: string = null;
  public creditAppRequestLine_RefCreditAppLineGUID: string = null;
}
