import { ViewBranchCompanyBaseEntity } from 'shared/models/base';
import { ProductType } from 'shared/constants';

export class ServiceFeeCondTemplateTableItemView extends ViewBranchCompanyBaseEntity {
  public serviceFeeCondTemplateTableGUID: string = null;
  public description: string = null;
  public productType: ProductType = null;
  public serviceFeeCondTemplateId: string = null;
}
