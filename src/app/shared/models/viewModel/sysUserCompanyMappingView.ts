import { ViewBaseEntity } from 'shared/models/base';

export class SysUserCompanyMappingView extends ViewBaseEntity {
  public companyGUID: string = null;
  public branchGUID: string = null;
  public userGUID: string = null;

  public company_Name: string = null;
  public company_CompanyId: string = null;
  public branch_Name: string = null;
  public branch_BranchId: string = null;
  public sysUserTable_UserName: string = null;
  public sysUserTable_Name: string = null;
  public branch_Values: string = null;
  public company_Values: string = null;
}
