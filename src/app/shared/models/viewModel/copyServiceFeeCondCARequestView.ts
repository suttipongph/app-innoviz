import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ServiceFeeCategory } from 'shared/constants';

export class CopyServiceFeeCondCARequestView extends ViewCompanyBaseEntity {
  public copyServiceFeeCondCARequestGUID: string = null;
  public creditAppRequestTableGUID: string = null;
  public creditAppTableGUID: string = null;
  public mainAgreementTableGUID: string = null;
}
