export class CleanUpBatchHistoryView {
  public jobId: string = null;
  public fromCreatedDate: string = null;
  public toCreatedDate: string = null;
  public instanceState: number[] = null;
}
