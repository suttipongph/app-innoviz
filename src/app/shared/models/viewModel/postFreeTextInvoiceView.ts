import { ViewCompanyBaseEntity } from 'shared/models/base';

export class PostFreeTextInvoiceView extends ViewCompanyBaseEntity {
  public freeTextInvoiceTableGUID: string = null;
  public currency_Values: string = null;
  public customerTable_Values: string = null;
  public customerName: string = null;
  public dueDate: string = null;
  public freeTextInvoiceId: string = null;
  public invoiceAmount: number = 0;
  public invoiceAmountBeforeTax: number = 0;
  public invoiceRevenueType_Values: string = null;
  public invoiceType_Values: string = null;
  public issuedDate: string = null;
  public taxAmount: number = 0;
  public whtAmount: number = 0;
}
export class PostFreeTextInvoiceResultView extends ViewCompanyBaseEntity {}
