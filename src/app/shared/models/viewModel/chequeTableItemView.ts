import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ReceivedFrom, RefType } from 'shared/constants';

export class ChequeTableItemView extends ViewCompanyBaseEntity {
  public chequeTableGUID: string = null;
  public amount: number = 0;
  public buyerTableGUID: string = null;
  public chequeBankAccNo: string = null;
  public chequeBankGroupGUID: string = null;
  public chequeBranch: string = null;
  public chequeDate: string = null;
  public chequeNo: string = null;
  public completedDate: string = null;
  public customerTableGUID: string = null;
  public documentStatusGUID: string = null;
  public expectedDepositDate: string = null;
  public issuedName: string = null;
  public pdc: boolean = false;
  public receivedFrom: ReceivedFrom = null;
  public recipientName: string = null;
  public refGUID: string = null;
  public refID: string = null;
  public refPDCGUID: string = null;
  public refType: RefType = null;
  public remark: string = null;
  public documentStatus_StatusId: string = null;
  public documentStatus_Values: string = null;
  public document_StatusId: string = null;
  public purchaseTable_Value: string = null;
  public refPDC_Values: string = null;
}
