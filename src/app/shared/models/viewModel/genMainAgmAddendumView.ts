import { ViewCompanyBaseEntity } from 'shared/models/base';
import { AgreementDocType, CreditAppRequestType } from 'shared/constants';

export class GenMainAgmAddendumView extends ViewCompanyBaseEntity {
  public mainAgreementTableGUID: string = null;
  public addendumInternalMainAgreementId: string = null;
  public agreementDate: string = null;
  public agreementDocType: AgreementDocType = null;
  public buyerId: string = null;
  public buyerName: string = null;
  public creditAppRequestDescription: string = null;
  public creditAppRequestTableGUID: string = null;
  public creditAppRequestType: CreditAppRequestType = null;
  public creditLimitTypeGUID: string = null;
  public customerId: string = null;
  public customerName: string = null;
  public description: string = null;
  public internalMainAgreementId: string = null;
  public mainAgreementDescription: string = null;
  public mainAgreementId: string = null;
  public requestDate: string = null;
  public mainAgreementTable_Values: string = null;
  public creditAppRequestTable_Values: string = null;
  public creditLimitType_Values: string = null;
  public customer_Values: string = null;
  public buyer_Values: string = null;
  public creditLimitType_Revolving: boolean = false;
}
