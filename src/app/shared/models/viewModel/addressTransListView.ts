import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class AddressTransListView extends ViewCompanyBaseEntity {
  public addressTransGUID: string = null;
  public address1: string = null;
  public currentAddress: boolean = false;
  public name: string = null;
  public primary: boolean = false;
  public refGUID: string = null;
}
