import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, RetentionDeductionMethod, RetentionCalculateBase } from 'shared/constants';

export class RetentionConditionSetupListView extends ViewCompanyBaseEntity {
  public retentionConditionSetupGUID: string = null;
  public productType: ProductType = null;
  public retentionCalculateBase: RetentionCalculateBase = null;
  public retentionDeductionMethod: RetentionDeductionMethod = null;
}
