import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class ContactTypeItemView extends ViewBranchCompanyBaseEntity {
  public contactTypeGUID: string = null;
  public contactTypeID: string = null;
  public description: string = null;
}
