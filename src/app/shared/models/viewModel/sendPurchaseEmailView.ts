import { ViewCompanyBaseEntity } from 'shared/models/base';
import { WorkflowInstanceView } from './workflowView';

export class SendPurchaseEmailView extends ViewCompanyBaseEntity {
  public additionalPurchase: boolean = false;
  public customerId: string = null;
  public documentStatus: string = null;
  public purchaseDate: string = null;
  public purchaseId: string = null;
  public purchaseTableGUID: string = null;
  public workflowInstance: WorkflowInstanceView = null;
  public rowVersion: object[] = null;

  // workflow
  public parmFolio: string = null;
  public procName: string = null;
  public parmCompany: string = null;
  public parmDocGUID: string = null;
  public parmDocID: string = null;
  public parmCustomerEmail: string = null;
  public parmMarketingUser: string = null;
  public parmSubmitPurchaseUser: string = null;
  public parmPurchaseDate: string = null;
  public parmCustomerName: string = null;
  public parmPurchaseId: string = null;
  public activityName: string = null;
  public actionName: string = null;
}
