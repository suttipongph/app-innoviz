import { BookmarkDocumentRefType, DocumentTemplateType, RefType } from 'shared/constants';
import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BookmarkDocumentTransListView extends ViewCompanyBaseEntity {
  public bookmarkDocumentTransGUID: string = null;
  public bookmarkDocumentGUID: string = null;
  public documentStatusGUID: string = null;
  public documentTemplateTableGUID: string = null;
  public referenceExternalId: string = null;
  public bookmarkDocument_BookmarkDocumentId: string = null;
  public documentTemplateTable_TemplateId: string = null;
  public documentStatus_StatusId: string = null;
  public documentStatus_Description: string = null;
  public refGUID: string = null;
  public refType: RefType = null;
  public bookmarkDocument_BookmarkDocumentRefType: BookmarkDocumentRefType = null;
  public bookmarkDocument_DocumentTemplateType: DocumentTemplateType = null;
  public bookmarkDocumentId: string = null;
}
