import { FileInformation } from '../systemModel';

export class ImportAccessRightView {
  public fileInfo: FileInformation = null;
  public roleGUID: string = null;
  public pathMenu: string = null;
  public groupId: string = null;
  public action: number = 0;
  public read: number = 0;
  public update: number = 0;
  public create: number = 0;
  public delete: number = 0;
}
