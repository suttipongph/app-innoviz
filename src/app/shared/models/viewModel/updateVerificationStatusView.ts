import { ViewCompanyBaseEntity } from 'shared/models/base';

export class UpdateVerificationStatusParamView extends ViewCompanyBaseEntity {
  public updateVerificationStatusGUID: string = null;
  public documentReasonGUID: string = null;
  public documentStatusGUID: string = null;
  public originalDocumentStatusGUID: string = null;
}
export class UpdateVerificationStatusView extends ViewCompanyBaseEntity {
  public updateVerificationStatusGUID: string = null;
  public documentReasonGUID: string = null;
  public documentStatusGUID: string = null;
  public originalDocumentStatusGUID: string = null;
  public verificationTableGUID: string = null;
  public verificationId: string = null;
  public documentStatus_Values: string = null;
}
