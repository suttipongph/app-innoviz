import { ViewCompanyBaseEntity } from 'shared/models/base';

export class AssignmentMethodItemView extends ViewCompanyBaseEntity {
  public assignmentMethodGUID: string = null;
  public assignmentMethodId: string = null;
  public description: string = '';
  public validateAssignmentBalance: boolean = false;
}
