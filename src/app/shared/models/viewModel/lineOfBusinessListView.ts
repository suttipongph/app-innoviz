import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class LineOfBusinessListView extends ViewBranchCompanyBaseEntity {
  public lineOfBusinessId: string = null;
  public description: string = null;
  public lineOfBusinessGUID: string = null;
}
