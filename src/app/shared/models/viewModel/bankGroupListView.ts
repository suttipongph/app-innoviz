import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BankGroupListView extends ViewCompanyBaseEntity {
  public bankGroupGUID: string = null;
  public bankGroupId: string = null;
  public description: string = null;
  public alias: string = null;
}
