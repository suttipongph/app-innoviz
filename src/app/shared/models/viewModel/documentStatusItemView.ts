import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class DocumentStatusItemView extends ViewBranchCompanyBaseEntity {
  public documentStatusGUID: string = null;
  public documentProcessGUID: string = null;
  public description: string = null;
  public statusId: string = null;
  public statusCode: string = null;
  // tslint:disable-next-line:variable-name
  public documentProcess_Values: string = null;
}
