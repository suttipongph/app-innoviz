import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BusinessCollateralSubTypeListView extends ViewCompanyBaseEntity {
  public businessCollateralSubTypeGUID: string = null;
  public agreementOrdering: number = 0;
  public businessCollateralSubTypeId: string = null;
  public businessCollateralTypeGUID: string = null;
  public description: string = null;
  public businessCollateralType_Values: string = null;
  public businessCollateralTypeId: string = null;
  public debtorClaims: boolean = false;
}
