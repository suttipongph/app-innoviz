import { ViewCompanyBaseEntity } from 'shared/models/base';

export class ExtendExpiryDateParamView extends ViewCompanyBaseEntity {
  public creditAppTableGUID: string = null;
  public creditAppId: string = null;
  public customerTable_Values: string = null;
  public originalExpiryDate: string = null;
  public expiryDate: string = null;
  public creditLimitExpiration: number = 0;
}

export class ExtendExpiryDateResultView extends ViewCompanyBaseEntity {
  public creditAppTableGUID: string = null;
  public originalExpiryDate: string = null;
  public expiryDate: string = null;
  public creditLimitExpiration: number = 0;
  public resultLabel: string = null;
}
