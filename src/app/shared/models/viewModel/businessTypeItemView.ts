import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BusinessTypeItemView extends ViewCompanyBaseEntity {
  public businessTypeGUID: string = null;
  public businessTypeId: string = null;
  public description: string = null;
}
