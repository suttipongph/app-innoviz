import { ViewCompanyBaseEntity } from '../base';

export class VendGroupItemView extends ViewCompanyBaseEntity {
  public vendGroupId: string = null;
  public description: string = null;
  public vendGroupGUID: string = null;
}
