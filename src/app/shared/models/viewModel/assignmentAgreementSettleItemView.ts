import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class AssignmentAgreementSettleItemView extends ViewCompanyBaseEntity {
  public assignmentAgreementSettleGUID: string = null;
  public assignmentAgreementTableGUID: string = null;
  public buyerAgreementTableGUID: string = null;
  public documentReasonGUID: string = null;
  public refAssignmentAgreementSettleGUID: string = null;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
  public settledAmount: number = 0;
  public settledDate: string = null;
  public assignmentAgreementTable_Value: string = null;
  public assignmentAgreementTable_AssignmentAgreementId: string = null;
  public buyerTable_Value: string = null;
  public documentReason_ReasonId: string = null;

  public invoiceTableGUID: string = null;
  public documentId: string = null;
  public invoiceTable_Values: string = null;
  //public refAssignTable_Value: string = null;
}
