import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ApprovalDecision } from 'shared/constants';

export class UpdateStatusRollbillPurchaseView extends ViewCompanyBaseEntity {
  public updateStatusRollbillPurchaseGUID: string = null;
  public approvedResult: ApprovalDecision = null;
  public purchaseId: string = null;
  public purchaseTableGUID: string = null;
}
