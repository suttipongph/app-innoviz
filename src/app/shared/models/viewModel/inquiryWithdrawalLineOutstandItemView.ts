import { ViewCompanyBaseEntity } from 'shared/models/base';

export class InquiryWithdrawalLineOutstandItemView extends ViewCompanyBaseEntity {
  public withdrawalLineGUID: string = null;
  public withdrawalTable_Values: string = null;
  public customerTable_Values: string = null;
  public buyerTable_Values: string = null;
  public buyerAgreementTrans_Values: string = null;
  public withdrawalAmount: number = 0;
  public outstanding: number = 0;
  public methodOfPayment_Values: string = null;
  public dueDate: string = null;
  public chequeTable_ChequeDate: string = null;
  public billingDate: string = null;
  public collectionDate: string = null;
  public creditAppTableBankAccountControl_BankAccountName: string = null;
  public creditAppTablePDCBank_BankAccountName: string = null;
  public withdrawalLineType: number = 0;
  public creditAppLine_Values: string = null;
}
