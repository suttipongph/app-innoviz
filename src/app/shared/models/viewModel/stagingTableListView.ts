import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType, ProductType, ProcessTransType, AccountType, InterfaceStatus } from 'shared/constants';

export class StagingTableListView extends ViewCompanyBaseEntity {
  public stagingTableGUID: string = null;
  public accountNum: string = null;
  public accountType: AccountType = null;
  public amountMST: number = 0;
  public interfaceStagingBatchId: string = null;
  public interfaceStatus: InterfaceStatus = null;
  public processTransGUID: string = null;
  public processTransType: ProcessTransType = null;
  public refId: string = null;
  public refType: RefType = null;
  public stagingBatchId: string = null;
  public transDate: string = null;
  public processTrans_Values: string = null;
  public sourceRefType: string = null;
  public sourceRefId: string = null;
  public invoiceTableGUID: string = null;
  public invoiceTable_Values: string = null;
  public invoiceTable_invoiceId: string = null;
}
