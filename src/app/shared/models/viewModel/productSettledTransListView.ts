import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, RefType } from 'shared/constants';

export class ProductSettledTransListView extends ViewCompanyBaseEntity {
  public productSettledTransGUID: string = null;
  public documentId: string = null;
  public interestCalcAmount: number = 0;
  public interestDate: string = null;
  public interestDay: number = 0;
  public invoiceSettlementDetailGUID: string = null;
  public productType: ProductType = null;
  public settledDate: string = null;
  public settledInvoiceAmount: number = 0;
  public settledPurchaseAmount: number = 0;
  public settledReserveAmount: number = 0;
  public invoiceSettlementDetail_Values: string = null;
  public invoiceTable_InvoiceId: string = null;
  public originalRefGUID: string = null;
  public refType: number = 0;
  public withdrawalTable_WithdrawalTableGUID: string = null;
}
