import { ViewCompanyBaseEntity } from '../base';

export class CurrencyItemView extends ViewCompanyBaseEntity {
  public currencyId: string = null;
  public name: string = null;
  public currencyGUID: string = null;
  public exchangeRate_Rate: number = null;
}
