import { ViewBaseEntity } from "../base/viewBaseEntity";
import { FileInformation } from "../systemModel";

export class ImportTaxReportItemView extends ViewBaseEntity {
    public fileInfo: FileInformation = null;
    public importTaxReportGUID: string = null;
    public refType: number = null;
    public refGUID: string = null;
    public fileName: string = null;
    public fileDescription: string = null;
    public contentType: string = null;
    public base64Data: string = null;
    public filePath: string = null;
    public refId: string = null;
    public name: string = null;
    public customerId: string = null;
    public customerTableGUID: string = null;
}