import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, RetentionDeductionMethod, RetentionCalculateBase, RefType } from 'shared/constants';

export class RetentionConditionTransListView extends ViewCompanyBaseEntity {
  public retentionConditionTransGUID: string = null;
  public productType: ProductType = null;
  public retentionAmount: number = 0;
  public retentionCalculateBase: RetentionCalculateBase = null;
  public retentionDeductionMethod: RetentionDeductionMethod = null;
  public retentionPct: number = 0;
  public refGUID: string = null;
}
