import { ViewBranchCompanyBaseEntity } from 'shared/models/base';
import { ProductType } from 'shared/constants';

export class CustomerCreditLimitByProductItemView extends ViewBranchCompanyBaseEntity {
  public customerCreditLimitByProductGUID: string = null;
  public creditLimit: number = 0;
  public creditLimitBalance: number = 0;
  public customerTableGUID: string = null;
  public productType: ProductType = null;
  public customerTable_Values: string = null;
}
