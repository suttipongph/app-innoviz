import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class GuarantorTypeItemView extends ViewBranchCompanyBaseEntity {
  public guarantorTypeGUID: string = null;
  public description: string = null;
  public guarantorTypeId: string = null;
}
