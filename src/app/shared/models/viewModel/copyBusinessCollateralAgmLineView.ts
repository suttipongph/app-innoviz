import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CopyBusinessCollateralAgmLineView extends ViewCompanyBaseEntity {
  public copyBusinessCollateralAgmLineGUID: string = null;
  public internalBusineeCollateralId: string = null;
}
