import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class ReceiptTableListView extends ViewBranchCompanyBaseEntity {
  public receiptTableGUID: string = null;
  public currencyGUID: string = null;
  public customerTableGUID: string = null;
  public documentStatusGUID: string = null;
  public receiptDate: string = null;
  public receiptId: string = null;
  public settleAmount: number = 0;
  public settleBaseAmount: number = 0;
  public settleTaxAmount: number = 0;
  public transDate: string = null;
  public customerTable_Values: string = null;
  public receiptTempTable_Values: string = null;
  public currency_Values: string = null;
  public documentStatus_Values: string = null;
  public methodopayment_Values: string = null;
  public refGUID: string = null;
  public invoiceSettlementDetail_Values: string = null;
}
