import { ViewReportBaseEntity } from 'shared/models/base';
import { ContactTo } from 'shared/constants';

export class RptPrintDocumentReturnLabelLetterReportView extends ViewReportBaseEntity {
  public printDocumentReturnLabelLetterGUID: string = null;
  public address: string = null;
  public contactPersonName: string = null;
  public contactTo: ContactTo = null;
  public documentReturnId: string = null;
}
