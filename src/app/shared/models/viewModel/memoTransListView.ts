import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class MemoTransListView extends ViewCompanyBaseEntity {
  public memoTransGUID: string = null;
  public memo: string = null;
  public topic: string = null;
  public refGUID: string = null;
}
