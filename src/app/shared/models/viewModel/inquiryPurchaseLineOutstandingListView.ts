import { ViewCompanyBaseEntity } from 'shared/models/base';

export class InquiryPurchaseLineOutstandingListView extends ViewCompanyBaseEntity {
  public purchaseLineGUID: string = null;
  public purchaseTable_Values: string = null;
  public purchaseTableGUID: string = null;
  public purchaseTable_PurchaseId: string = null;
  public customerTable_Values: string = null;
  public customerTableGUID: string = null;
  public customerTable_CustomerTableId: string = null;
  public buyerTable_Values: string = null;
  public buyerTableGUID: string = null;
  public buyerTable_BuyerId: string = null;
  public invoiceAmount: number = 0;
  public purchaseAmount: number = 0;
  public outstanding: number = 0;
  public dueDate: string = null;
  public billingDate: string = null;
  public collectionDate: string = null;
  public methodOfPayment_Values: string = null;
  public methodOfPaymentGUID: string = null;
  public methodOfPayment_MethodOfPaymentId: string = null;
}
