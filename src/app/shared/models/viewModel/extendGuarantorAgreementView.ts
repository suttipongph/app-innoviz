import { ViewCompanyBaseEntity } from 'shared/models/base';
import { AgreementDocType } from 'shared/constants';

export class ExtendGuarantorAgreementView extends ViewCompanyBaseEntity {
  public guarantorAgreementTableGUID: string = null;
  public affiliate: boolean = false;
  public agreementDate: string = null;
  public agreementDocType: AgreementDocType = null;
  public agreementExtension: number = 0;
  public description: string = null;
  public documentReasonGUID: string = null;
  public expiryDate: string = null;
  public extendInternalGuarantorAgreementId: string = null;
  public guarantorAgreementId: string = null;
  public internalGuarantorAgreementId: string = null;
  public internalMainAgreementId: string = null;
  public newAgreementDate: string = null;
  public newDescription: string = null;
  public reasonRemark: string = null;
  public extendGuarantorAgreement_Values: string = null;
  public documentReason_Values: string = null;
  public mainAgreementTable_MainAgreementId: string = null;
  public documentStatusGUID: string = null;
  public productType: number = null;
}

export class ExtendGuarantorAgreementResultView extends ViewCompanyBaseEntity {
  public guarantorAgreementTableGUID: string = null;
  public affiliate: boolean = false;
  public agreementDate: string = null;
  public agreementDocType: AgreementDocType = null;
  public agreementExtension: number = 0;
  public description: string = null;
  public documentReasonGUID: string = null;
  public expiryDate: string = null;
  public extendInternalGuarantorAgreementId: string = null;
  public guarantorAgreementId: string = null;
  public internalGuarantorAgreementId: string = null;
  public internalMainAgreementId: string = null;
  public newAgreementDate: string = null;
  public newDescription: string = null;
  public reasonRemark: string = null;
  public documentStatusGUID: string = null;
  public productType: number = null;
}
