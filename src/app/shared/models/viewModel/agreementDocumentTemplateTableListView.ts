import { ViewBranchCompanyBaseEntity } from 'shared/models/base';
import { AgreementRefType } from 'shared/constants';

export class AgreementDocumentTemplateTableListView extends ViewBranchCompanyBaseEntity {
  public agreementDocumentTemplateTableGUID: string = null;
  public agreementDocumentTemplateId: string = null;
  public agreementRefType: AgreementRefType = null;
  public description: string = null;
}
