import { ViewCompanyBaseEntity } from 'shared/models/base';

export class VerificationLineItemView extends ViewCompanyBaseEntity {
  public verificationLineGUID: string = null;
  public pass: boolean = false;
  public remark: string = null;
  public vendorTableGUID: string = null;
  public verificationTableGUID: string = null;
  public verificationTypeGUID: string = null;
  public documentStatus_Values: string = null;
  public verificationTable_Values: string = null;
}
