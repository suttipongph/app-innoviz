export class GetTaxValueAndWHTValueParm {
  public taxTableGUID: string = null;
  public withholdingTaxTableGUID: string = null;
  public taxDate: string = null;
}
