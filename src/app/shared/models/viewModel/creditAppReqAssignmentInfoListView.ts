import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CreditAppReqAssignmentInfoListView extends ViewCompanyBaseEntity {
  public creditAppReqAssignmentInfoGUID: string = null;
}
