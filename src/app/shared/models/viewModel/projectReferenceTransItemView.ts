import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class ProjectReferenceTransItemView extends ViewCompanyBaseEntity {
  public projectReferenceTransGUID: string = null;
  public projectCompanyName: string = null;
  public projectCompletion: number = 0;
  public projectName: string = null;
  public projectStatus: string = null;
  public projectValue: number = 0;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
  public remark: string = null;
}
