import { ViewCompanyBaseEntity } from 'shared/models/base';

export class InquiryRollbillPurchaseLineItemView extends ViewCompanyBaseEntity {
  public inquiryRollbillPurchaseLineGUID: string = null;
  public dueDate: string = null;
  public interestDate: string = null;
  public lineNum: number = 0;
  public linePurchaseAmount: number = 0;
  public numberOfRollbill: number = 0;
  public outstandingBuyerInvoiceAmount: number = 0;
  public purchaseLineGUID: string = null;
  public originalPurchaseLineGUID: string = null;
  public purchaseTable_PurchaseDate: string = null;
  public purchaseTable_PurchaseId: string = null;
  public documentStatus_Description: string = null;
}
