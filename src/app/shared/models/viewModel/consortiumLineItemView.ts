import { ViewCompanyBaseEntity } from 'shared/models/base';

export class ConsortiumLineItemView extends ViewCompanyBaseEntity {
  public consortiumLineGUID: string = null;
  public address: string = null;
  public authorizedPersonTypeGUID: string = null;
  public consortiumTableGUID: string = null;
  public customerName: string = null;
  public isMain: boolean = false;
  public operatedBy: string = null;
  public position: string = null;
  public proportionOfShareholderPct: number = 0;
  public remark: string = null;
  public consortiumTable_Values: string = null;
  public ordering: number = 0;
}
