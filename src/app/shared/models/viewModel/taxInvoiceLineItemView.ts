import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class TaxInvoiceLineItemView extends ViewBranchCompanyBaseEntity {
  public taxInvoiceLineGUID: string = null;
  public dimension1GUID: string = null;
  public dimension2GUID: string = null;
  public dimension3GUID: string = null;
  public dimension4GUID: string = null;
  public dimension5GUID: string = null;
  public invoiceRevenueTypeGUID: string = null;
  public invoiceText: string = null;
  public lineNum: number = 0;
  public prodUnitGUID: string = null;
  public qty: number = 0;
  public taxAmount: number = 0;
  public taxInvoiceTableGUID: string = null;
  public taxTableGUID: string = null;
  public totalAmount: number = 0;
  public totalAmountBeforeTax: number = 0;
  public unitPrice: number = 0;
  public taxInvoiceTable_Values: string = null;
  public invoiceRevenueType_Values: string = null;
  public prodUnit_Values: string = null;
  public taxTable_Values: string = null;
  public dimension1_Values: string = null;
  public dimension2_Values: string = null;
  public dimension3_Values: string = null;
  public dimension4_Values: string = null;
  public dimension5_Values: string = null;
}
