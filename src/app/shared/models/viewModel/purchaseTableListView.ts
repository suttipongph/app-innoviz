import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, RetentionCalculateBase } from 'shared/constants';

export class PurchaseTableListView extends ViewCompanyBaseEntity {
  public purchaseTableGUID: string = null;
  public customerTableGUID: string = null;
  public description: string = null;
  public purchaseDate: string = null;
  public purchaseId: string = null;
  public creditAppTableGUID: string = null;
  public customerTable_Values: string = null;
  public customerTable_CustomerId: string = null;
  public rollBill: boolean = false;

  public documentStatus_Values: string = null;
  public documentStatusGUID: string = null;
  public documentStatus_StatusId: string = null;
}
