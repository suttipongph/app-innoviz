import { ViewCompanyBaseEntity } from 'shared/models/base';
import { CalcInterestDayMethod, CalcInterestMethod, ProductType } from 'shared/constants';

export class ProductSubTypeListView extends ViewCompanyBaseEntity {
  public productSubTypeGUID: string = null;
  public description: string = null;
  public guarantorAgreementYear: number = 0;
  public maxInterestFeePct: number = 0;
  public productSubTypeId: string = null;
  public productType: ProductType = null;
  public calcInterestMethod: CalcInterestMethod = null;
  public calcInterestDayMethod: CalcInterestDayMethod = null;
}
