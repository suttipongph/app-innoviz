import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType } from 'shared/constants';

export class IntercompanyInvoiceTableListView extends ViewCompanyBaseEntity {
  public intercompanyInvoiceTableGUID: string = null;
  public balance: number = 0;
  public creditAppTableGUID: string = null;
  public customerTableGUID: string = null;
  public documentId: string = null;
  public dueDate: string = null;
  public intercompanyInvoiceId: string = null;
  public invoiceAmount: number = 0;
  public invoiceRevenueTypeGUID: string = null;
  public issuedDate: string = null;
  public productType: ProductType = null;
  public settleAmount: number = 0;
  public customerTable_Values: string = null;
  public creditAppTable_Values: string = null;
  public invoiceRevenueType_Values: string = null;
  public InvoiceRevenueType_InvoiceRevenueTypeId: string = null;
  public CreditAppTable_CreditAppId: string = null;
  public CustomerTable_CustomerId: string = null;
}
