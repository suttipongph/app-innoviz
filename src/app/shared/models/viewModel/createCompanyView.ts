import { ViewBaseEntity } from 'shared/models/base';

export class CreateCompanyParamView extends ViewBaseEntity {
  //company
  public companyGUID: string = null;
  public companyId: string = null;
  public company_Name: string = null;
  public secondName: string = null;

  //branch
  public branchGUID: string = null;
  public branchId: string = null;
  public branch_Name: string = null;

  //user
  public userId: string = null;
  public userName: string = null;
  public sysUserTable_Name: string = null;
  public employeeTable_EmployeeId: string = null;
  public businessUnit_BusinessUnitId: string = null;
}
