import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, SuspenseInvoiceType, RetentionCalculateBase } from 'shared/constants';

export class CustomerRefundTableListView extends ViewCompanyBaseEntity {
  public customerRefundTableGUID: string = null;
  public currencyGUID: string = null;
  public currency_CurrencyId: string = null;
  public currency_Values: string = null;
  public customerRefundId: string = null;
  public description: string = null;
  public documentStatusGUID: string = null;
  public documentStatus_StatusId: string = null;
  public documentStatus_Values: string = null;
  public netAmount: number = 0;
  public paymentAmount: number = 0;
  public suspenseAmount: number = 0;
  public transDate: string = null;
  public suspenseInvoiceType: SuspenseInvoiceType = null;
}
