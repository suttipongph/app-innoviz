import { ViewCompanyBaseEntity } from 'shared/models/base';

export class DocumentReturnLineListView extends ViewCompanyBaseEntity {
  public documentReturnLineGUID: string = null;
  public buyerTableGUID: string = null;
  public contactPersonName: string = null;
  public documentNo: string = null;
  public documentTypeGUID: string = null;
  public lineNum: number = 0;
  public buyerTable_Values: string = null;
  public documentType_Values: string = null;
  public buyerTable_BuyerId: string = null;
  public documentType_DocumentTypeId: string = null;
}
