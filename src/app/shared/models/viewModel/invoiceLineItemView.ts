import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class InvoiceLineItemView extends ViewBranchCompanyBaseEntity {
  public invoiceLineGUID: string = null;
  public dimension1GUID: string = null;
  public dimension2GUID: string = null;
  public dimension3GUID: string = null;
  public dimension4GUID: string = null;
  public dimension5GUID: string = null;
  public invoiceRevenueTypeGUID: string = null;
  public invoiceTableGUID: string = null;
  public invoiceText: string = null;
  public lineNum: number = 0;
  public prodUnitGUID: string = null;
  public qty: number = 0;
  public taxAmount: number = 0;
  public taxTableGUID: string = null;
  public totalAmount: number = 0;
  public totalAmountBeforeTax: number = 0;
  public unitPrice: number = 0;
  public whtAmount: number = 0;
  public whtBaseAmount: number = 0;
  public withholdingTaxTableGUID: string = null;
}
