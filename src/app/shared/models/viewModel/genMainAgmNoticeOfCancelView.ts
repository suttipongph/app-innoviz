import { ViewCompanyBaseEntity } from 'shared/models/base';
import { AgreementDocType } from 'shared/constants';

export class GenMainAgmNoticeOfCancelView extends ViewCompanyBaseEntity {
  public mainAgreementTableGUID: string = null;
  public agreementDate: string = null;
  public agreementDocType: AgreementDocType = null;
  public buyerName: string = null;
  public buyerTableGUID: string = null;
  public cancelDate: string = null;
  public customerGUID: string = null;
  public customerName: string = null;
  public description: string = null;
  public documentReasonGUID: string = null;
  public internalMainAgreementId: string = null;
  public mainAgreementDescription: string = null;
  public mainAgreementId: string = null;
  public noticeOfCancellationInternalMainAgreementId: string = null;
  public reasonRemark: string = null;
  public buyerTable_Values: string = null;
  public customerTable_Values: string = null;
  public documentReason_Values: string = null;
  public creditLimitType_Revolving: boolean = false;
}

export class GenMainAgmNoticeOfCancelResultView extends ViewCompanyBaseEntity {
  public mainAgreementTableGUID: string = null;
  public agreementDate: string = null;
  public agreementDocType: AgreementDocType = null;
  public buyerName: string = null;
  public buyerTableGUID: string = null;
  public cancelDate: string = null;
  public customerGUID: string = null;
  public customerName: string = null;
  public description: string = null;
  public documentReasonGUID: string = null;
  public internalMainAgreementId: string = null;
  public mainAgreementDescription: string = null;
  public mainAgreementId: string = null;
  public noticeOfCancellationInternalMainAgreementId: string = null;
  public reasonRemark: string = null;
  public buyerTable_Values: string = null;
  public customerTable_Values: string = null;
  public documentReason_Values: string = null;
  public creditLimitType_Revolving: boolean = false;
}
