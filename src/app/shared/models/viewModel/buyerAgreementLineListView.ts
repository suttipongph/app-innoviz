import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BuyerAgreementLineListView extends ViewCompanyBaseEntity {
  public buyerAgreementLineGUID: string = null;
  public amount: number = 0;
  public description: string = null;
  public period: number = 0;
  public buyerAgreementTableGUID: string = null;
}
