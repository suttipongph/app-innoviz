import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType, CreditAppRequestType } from 'shared/constants';

export class PrintSetBookDocTransView extends ViewCompanyBaseEntity {
  public agreementDescription: string = null;
  public agreementId: string = null;
  public creditAppRequestDescription: string = null;
  public creditAppRequestId: string = null;
  public creditAppRequestType: CreditAppRequestType = null;
  public creditLimitTypeId: string = null;
  public internalAgreementId: string = null;
  public refType: RefType = null;
  public refGUID: string = null;
  public documentTemplateTableGUID:string = null;
  public bookmarkDocumentTransGUID:string = null;
}
