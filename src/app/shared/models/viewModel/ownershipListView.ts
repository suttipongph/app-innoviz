import { ViewCompanyBaseEntity } from '../base';

export class OwnershipListView extends ViewCompanyBaseEntity {
  public ownershipId: string = null;
  public description: string = null;
  public ownershipGUID: string = null;
}
