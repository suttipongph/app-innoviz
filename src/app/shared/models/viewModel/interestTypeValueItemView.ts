import { ViewCompanyBaseEntity } from 'shared/models/base';

export class InterestTypeValueItemView extends ViewCompanyBaseEntity {
  public interestTypeValueGUID: string = null;
  public effectiveFrom: string = null;
  public effectiveTo: string = null;
  public interestTypeGUID: string = null;
  public value: number = 0;
  public interestType_Values: string = null;
}
