import { ViewCompanyBaseEntity } from 'shared/models/base';
import { WorkingDay } from 'shared/constants';

export class CalendarGroupListView extends ViewCompanyBaseEntity {
  public calendarGroupGUID: string = null;
  public calendarGroupId: string = null;
  public description: string = null;
  public workingDay: WorkingDay = null;
}
