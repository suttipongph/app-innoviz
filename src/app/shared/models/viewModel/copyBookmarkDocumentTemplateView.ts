import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CopyBookmarkDocumentTemplateView extends ViewCompanyBaseEntity {
  public copyBookmarkDocumentTemplateGUID: string = null;
  public bookmarkDocumentTemplateTableGUID: string = null;
  public refGUID: string = null;
  public refType: string = null;
}
