import { ViewReportBaseEntity } from 'shared/models/base';

export class RptReportWithdrawalTermExtensionReportView extends ViewReportBaseEntity {
  public reportWithdrawalTermExtensionGUID: string = null;
  public buyerTableGUID: string[] = null;
  public customerTableGUID: string[] = null;
  public documentStatusGUID: string[] = null;
  public fromDueDate: string = null;
  public fromWithdrawalDate: string = null;
  public showOutstandingOnly: boolean = false;
  public toDueDate: string = null;
  public toWithdrawalDate: string = null;
}
