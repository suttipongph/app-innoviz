import { ViewCompanyBaseEntity } from 'shared/models/base';

export class IntercompanyInvoiceAdjustmentListView extends ViewCompanyBaseEntity {
  public intercompanyInvoiceAdjustmentGUID: string = null;
  public adjustment: number = 0;
  public documentReasonGUID: string = null;
  public originalAmount: number = 0;
  public documentReason_Values: string = null;
  public createdDateTime: string = null;
  public intercompanyInvoiceTableGUID: string = null;
  public documentReason_DocumentReasonId: string = null;
}
