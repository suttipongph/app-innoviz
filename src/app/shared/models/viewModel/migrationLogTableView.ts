import { ViewBaseEntity } from 'shared/models/base';

export class MigrationLogTableView extends ViewBaseEntity {
  public id: number = null;
  public migrationName: string = null;
  public logStartDateTime: string = null;
  public logEndDateTime: string = null;
  public logMessage: string = null;
}
