import { DocumentTemplateType } from 'shared/constants';
import { ViewCompanyBaseEntity } from '../base';
import { FileInformation } from '../systemModel';

export class DocumentTemplateTableListView extends ViewCompanyBaseEntity {
  public fileInfo: FileInformation = null;
  public documentTemplateTableGUID: string = null;
  public templateId: string = null;
  public description: string = null;
  public documentTemplateType: DocumentTemplateType = 0;
  public fileName: string = null;
}
