import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BankTypeListView extends ViewCompanyBaseEntity {
  public bankTypeGUID: string = null;
  public bankTypeId: string = null;
  public description: string = null;
}
