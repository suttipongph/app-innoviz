import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BranchListView extends ViewCompanyBaseEntity {
  public branchGUID: string = null;
  public branchId: string = null;
  public name: string = null;
  public taxBranchGUID: string = null;
  public taxBranch_Values: string = null;
}
