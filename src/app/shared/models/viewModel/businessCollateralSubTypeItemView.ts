import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BusinessCollateralSubTypeItemView extends ViewCompanyBaseEntity {
  public businessCollateralSubTypeGUID: string = null;
  public agreementOrdering: number = 0;
  public agreementRefText: string = null;
  public businessCollateralSubTypeId: string = null;
  public businessCollateralTypeGUID: string = null;
  public businessCollateralTypeId: string = null;
  public BusinessCollateralType_Values: string = null;
  public description: string = null;
  public debtorClaims: boolean = false;
}
