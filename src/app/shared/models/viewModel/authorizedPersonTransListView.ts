import { ViewCompanyBaseEntity } from 'shared/models/base';
import { IdentificationType, RefType } from 'shared/constants';

export class AuthorizedPersonTransListView extends ViewCompanyBaseEntity {
  public authorizedPersonTransGUID: string = null;
  public authorizedPersonTypeGUID: string = null;
  public inActive: boolean = false;
  public relatedPersonTable_Name: string = null;
  public ordering: number = 0;
  public relatedPersonTable_PassportId: string = null;
  public relatedPersonTable_TaxId: string = null;
  public authorizedPersonType_Values: string = null;
  public authorizedPersonType_AuthorizedPersonTypeId: string = null;
  public refAuthorizedPersonTransGUID: string = null;
  public refGUID: string = null;
  public refType: RefType = null;
}
