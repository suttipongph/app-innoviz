import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CreditScoringListView extends ViewCompanyBaseEntity {
  public creditScoringGUID: string = null;
  public creditScoringId: string = null;
  public description: string = null;
}
