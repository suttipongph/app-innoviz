export class ManageAssignAgreementValidationResult {
  public isValid: boolean = false;
  public hasLine: boolean = false;
  public refType: number = 0;
  public refGUID: string = null;
}
