import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, SuspenseInvoiceType, InterestCalculationMethod, RefType } from 'shared/constants';

export class InvoiceSettlementDetailListView extends ViewCompanyBaseEntity {
  public invoiceSettlementDetailGUID: string = null;
  public balanceAmount: number = 0;
  public documentId: string = null;
  public invoiceAmount: number = 0;
  public invoiceDueDate: string = null;
  public invoiceTableGUID: string = null;
  public invoiceTypeGUID: string = null;
  public productType: ProductType = null;
  public settleAmount: number = 0;
  public settleInvoiceAmount: number = 0;
  public whtAmount: number = 0;
  public whtSlipReceivedByBuyer: boolean = false;
  public whtSlipReceivedByCustomer: boolean = false;
  public refType: RefType = null;
  public refGUID: string = null;
  public invoiceTable_Values: string = null;
  public invoiceType_Values: string = null;
  public invoiceTable_InvoiceId: string = null;
  public invoiceType_InvoiceTypeId: string = null;
  public invoiceTable_InvoiceTypeGUID: string = null;
  public suspenseInvoiceType: SuspenseInvoiceType = null;
}
