import { ViewBranchCompanyBaseEntity } from 'shared/models/base';
import { TaxInvoiceRefType } from 'shared/constants';

export class TaxInvoiceTableListView extends ViewBranchCompanyBaseEntity {
  public taxInvoiceTableGUID: string = null;
  public customerName: string = null;
  public customerTableGUID: string = null;
  public documentStatusGUID: string = null;
  public dueDate: string = null;
  public invoiceAmount: number = 0;
  public invoiceAmountBeforeTax: number = 0;
  public issuedDate: string = null;
  public taxAmount: number = 0;
  public taxInvoiceId: string = null;
  public customerTable_Values: string = null;
  public documentStatus_Values: string = null;
}
