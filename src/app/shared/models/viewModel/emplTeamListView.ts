import { ViewCompanyBaseEntity } from '../base';

export class EmplTeamListView extends ViewCompanyBaseEntity {
  public teamId: string = null;
  public name: string = null;
  public emplTeamGUID: string = null;
  public companyGUID: string = null;
}
