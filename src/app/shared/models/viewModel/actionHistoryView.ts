import { ViewCompanyBaseEntity } from '../base';

export class ActionHistoryListView extends ViewCompanyBaseEntity {
  public serialNo: string = null;
  public refGUID: string = null;
  public workflowName: string = null;
  public activityName: string = null;
  public actionName: string = null;
  public comment: string = null;
  public actionHistoryGUID: string = null;

  public displayContent: string = null;
}
