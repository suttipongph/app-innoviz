import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CustBusinessCollateralListView extends ViewCompanyBaseEntity {
  public custBusinessCollateralGUID: string = null;
  public businessCollateralSubTypeGUID: string = null;
  public businessCollateralTypeGUID: string = null;
  public businessCollateralValue: number = 0;
  public buyerName: string = null;
  public cancelled: boolean = false;
  public custBusinessCollateralId: string = null;
  public dbdRegistrationDate: string = null;
  public description: string = null;
  public businessCollateralType_values: string = null;
  public businessCollateralSubType_values: string = null;
  public businessCollateralType_BusinessCollateralTypeId: string = null;
  public businessCollateralSubType_BusinessCollateralSubTypeId: string = null;
  public customerTableGUID: string = null;
}
