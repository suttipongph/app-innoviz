import { AssetFeeType, ProductType, ServiceFeeCategory } from 'shared/constants';
import { ViewCompanyBaseEntity } from 'shared/models/base';

export class InvoiceRevenueTypeItemView extends ViewCompanyBaseEntity {
  public invoiceRevenueTypeGUID: string = null;
  public assetFeeType: AssetFeeType = null;
  public description: string = null;
  public feeAmount: number = 0;
  public feeInvoiceText: string = null;
  public feeLedgerAccount: string = null;
  public feeTaxAmount: number = 0;
  public feeTaxGUID: string = null;
  public feeWHTGUID: string = null;
  public intercompanyTableGUID: string = null;
  public productType: ProductType = null;
  public revenueTypeId: string = null;
  public serviceFeeCategory: ServiceFeeCategory = null;
  public serviceFeeRevenueTypeGUID: string = null;
}
