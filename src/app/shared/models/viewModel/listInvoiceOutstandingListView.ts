import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, ReceivedFrom, RefType, SuspenseInvoiceType } from 'shared/constants';

export class ListInvoiceOutstandingListView extends ViewCompanyBaseEntity {
  public balanceAmount: number = 0;
  public buyerAgreementTableGUID: string = null;
  public buyerInvoiceTableGUID: string = null;
  public buyerTableGUID: string = null;
  public documentId: string = null;
  public dueDate: string = null;
  public invoiceAmount: number = 0;
  public invoiceTableGUID: string = null;
  public invoiceTypeGUID: string = null;
  public productInvoice: boolean = false;
  public productType: ProductType = null;
  public refGUID: string = null;
  public invoiceTable_Values: string = null;
  public invoiceTable_InvoiceId: string = null;
  public suspenseInvoiceType: SuspenseInvoiceType = null;
  public invoiceType_Values: string = null;
  public invoiceType_InvoiceTypeId: string = null;
  public invoiceTable_CustomerTableGUID: string = null;
  public refReceiptTempPaymDetailGUID: string = null;
  public creditAppTable_Values: string = null;
  public currency_Values: string = null;
  public buyerInvoiceTable_Values: string = null;
  public buyerInvoiceTable_BuyerInvoiceId: string = null;
  public buyerAgreementTable_Values: string = null;
  public buyerAgreementTable_BuyerAgreementId: string = null;
  public buyerTable_Values: string = null;
  public buyerTable_BuyerId: string = null;
  public refType: RefType = null;
  public receivedFrom: ReceivedFrom = null;
  public invoiceSettlementDetails: string = null;
  public exitsInvoice: boolean = false;
}
