import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ReceivedFrom, CollectionFollowUpResult, Result, RefType } from 'shared/constants';

export class CollectionFollowUpItemView extends ViewCompanyBaseEntity {
  public collectionFollowUpGUID: string = null;
  public buyerTableGUID: string = null;
  public chequeAmount: number = 0;
  public chequeBankAccNo: string = null;
  public chequeBankGroupGUID: string = null;
  public chequeBranch: string = null;
  public chequeCollectionResult: Result = null;
  public chequeDate: string = null;
  public chequeNo: string = null;
  public collectionAmount: number = 0;
  public collectionDate: string = null;
  public collectionFollowUpResult: CollectionFollowUpResult = null;
  public creditAppLineGUID: string = null;
  public customerTableGUID: string = null;
  public description: string = null;
  public documentId: string = null;
  public methodOfPaymentGUID: string = null;
  public newCollectionDate: string = null;
  public receivedFrom: ReceivedFrom = null;
  public recipientName: string = null;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
  public remark: string = null;
  public customerTable_Values: string = null;
  public buyerTable_Values: string = null;
  public creditAppLine_Values: string = null;
}
