import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CompanyBankItemView extends ViewCompanyBaseEntity {
  public companyBankGUID: string = null;
  public accountNumber: string = null;
  public bankAccountName: string = null;
  public bankBranch: string = null;
  public bankGroupGUID: string = null;
  public bankTypeGUID: string = null;
  public inActive: boolean = false;
  public primary: boolean = false;
}
