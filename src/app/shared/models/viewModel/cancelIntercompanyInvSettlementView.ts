import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CancelIntercompanyInvSettlementView extends ViewCompanyBaseEntity {
  public cancelDate: string = null;
  public cnReasonGUID: string = null;
  public documentStatus_Values: string = null;
  public intercompanyInvoice_Values: string = null;
  public intercompanyInvoiceSettlementGUID: string = null;
  public intercompanyInvoiceSettlementId: string = null;
  public methodOfPayment_Values: string = null;
  public origInvoiceAmount: number = 0;
  public origInvoiceId: string = null;
  public origTaxInvoiceAmount: number = 0;
  public origTaxInvoiceId: string = null;
  public settleAmount: number = 0;
  public settleInvoiceAmount: number = 0;
  public settleWHTAmount: number = 0;
}
export class CancelIntercompanyInvSettlementResultView extends ViewCompanyBaseEntity {}
