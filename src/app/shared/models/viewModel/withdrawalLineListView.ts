import { ViewCompanyBaseEntity } from 'shared/models/base';
import { WithdrawalLineType } from 'shared/constants';

export class WithdrawalLineListView extends ViewCompanyBaseEntity {
  public lineNum: number = 0;
  public withdrawalLineGUID: string = null;
  public dueDate: string = null;
  public interestAmount: number = 0;
  public interestDate: string = null;
  public interestDay: number = 0;
  public startDate: string = null;
  public withdrawalLineType: WithdrawalLineType = null;
  public creditAppTableGUID: string = null;
  public withdrawalTableGUID: string = null;
  public withdrawalLine_Value: string = null;
}
