import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class VendorPaymentTransItemView extends ViewCompanyBaseEntity {
  public vendorPaymentTransGUID: string = null;
  public amountBeforeTax: number = 0;
  public creditAppTableGUID: string = null;
  public dimension1: string = null;
  public dimension2: string = null;
  public dimension3: string = null;
  public dimension4: string = null;
  public dimension5: string = null;
  public documentStatusGUID: string = null;
  public offsetAccount: string = null;
  public paymentDate: string = null;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
  public taxAmount: number = 0;
  public taxTableGUID: string = null;
  public totalAmount: number = 0;
  public vendorTableGUID: string = null;
  public creditAppTable_Values: string = null;
  public documentStatus_Values: string = null;
  public vendorTable_Values: string = null;
  public dimension1_Values: string = null;
  public dimension2_Values: string = null;
  public dimension3_Values: string = null;
  public dimension4_Values: string = null;
  public dimension5_Values: string = null;
  public vendorTaxInvoiceId: string = null;
}
