import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CancelAssignmentAgreementView extends ViewCompanyBaseEntity {
  public cancelAssignmentAgreementGUID: string = null;
  public agreementDate: string = null;
  public assignmentAgreementAmount: number = 0;
  public assignmentAgreementId: string = null;
  public assignmentAgreementTableGUID: string = null;
  public assignmentMethodGUID: string = null;
  public buyerName: string = null;
  public buyerTableGUID: string = null;
  public customerName: string = null;
  public customerTableGUID: string = null;
  public description: string = null;
  public documentReasonGUID: string = null;
  public reasonRemark: string = null;
}
