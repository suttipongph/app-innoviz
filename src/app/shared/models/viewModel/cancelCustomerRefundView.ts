import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CancelCustomerRefundView extends ViewCompanyBaseEntity {
  public customerRefundTableGUID: string = null;
  public customerTable_Values: string = null;
  public customerRefundTable_Values: string = null;
  public documentReasonGUID: string = null;
  public documentStatus_Values: string = null;
  public netAmount: number = 0;
  public paymentAmount: number = 0;
  public retentionAmount: number = 0;
  public serviceFeeAmount: number = 0;
  public settleAmount: number = 0;
  public suspenseAmount: number = 0;
  public transDate: string = null;
}
