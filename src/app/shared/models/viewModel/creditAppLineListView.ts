import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CreditAppLineListView extends ViewCompanyBaseEntity {
  public creditAppLineGUID: string = null;
  public approvedCreditLimitLine: number = 0;
  public businessSegmentGUID: string = null;
  public buyerTableGUID: string = null;
  public lineNum: number = 0;
  public reviewDate: string = null;
  public expiryDate: string = null;
  public buyerTable_Values: string = null;
  public businessSegment_Values: string = null;
  public buyerTable_BuyerId: string = null;
  public businessSegment_BusinessSegmentId: string = null;
  public creditAppTable_Values: string = null;
  public customerTable_Values: string = null;
}
