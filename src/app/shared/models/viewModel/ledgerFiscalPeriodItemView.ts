import { LedgerFiscalPeriodStatus } from 'shared/constants';
import { ViewCompanyBaseEntity } from '../base';
export class LedgerFiscalPeriodItemView extends ViewCompanyBaseEntity {
  public startDate: string = null;
  public endDate: string = null;
  public periodStatus: LedgerFiscalPeriodStatus = null;
  public ledgerFiscalPeriodGUID: string = null;
  public ledgerFiscalYearGUID: string = null;
  public ledgerFiscalYear_FiscalYearId: string = null;
  public ledgerFiscalYear_Description: string = null;
}
