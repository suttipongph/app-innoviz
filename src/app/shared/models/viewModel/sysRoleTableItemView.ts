import { ViewCompanyBaseEntity } from 'shared/models/base';
import { SysFeatureRoleView } from './sysFeatureRoleView';

export class SysRoleTableItemView extends ViewCompanyBaseEntity {
  public id: string = null;
  public name: string = null;
  public normalizedName: string = null;
  public displayName: string = null;
  public siteLoginType: number = null;

  public sysFeatureGroupRoleViewList: SysFeatureGroupRoleView[] = null;
}

export class SysFeatureGroupRoleView {
  public sysFeatureGroupRoleGUID: string = null;
  public sysFeatureGroupGUID: string = null;
  public groupId: string = null;
  public create: number = 0;
  public read: number = 0;
  public update: number = 0;
  public delete: number = 0;
  public action: number = 0;
  public roleGUID: string = null;
  public sysRoleTable_CompanyGUID: string = null;
  public sysRoleTable_DisplayName: string = null;
  public sysRoleTable_SiteLoginType: number = 0;
  public sysFeatureGroup_FeatureType: number = 0;
}
