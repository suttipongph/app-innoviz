import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class ServiceFeeCondTemplateLineListView extends ViewBranchCompanyBaseEntity {
  public serviceFeeCondTemplateLineGUID: string = null;
  public amountBeforeTax: number = 0;
  public description: string = null;
  public invoiceRevenueTypeGUID: string = null;
  public ordering: number = 0;
  public invoiceRevenueType_Value: string = null;
  public serviceFeeCondTemplateTableGUID: string = null;
}
