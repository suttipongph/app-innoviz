import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CompanyBankListView extends ViewCompanyBaseEntity {
  public companyBankGUID: string = null;
  public accountNumber: string = null;
  public bankAccountName: string = null;
  public bankGroupGUID: string = null;
  public bankTypeGUID: string = null;
  public inActive: boolean = false;
  public primary: boolean = false;
  public bankGroup_Values: string = null;
  public bankType_Values: string = null;
  public bankGroup_BankGroupId: string = null;
  public bankType_BankTypeId: string = null;
}
