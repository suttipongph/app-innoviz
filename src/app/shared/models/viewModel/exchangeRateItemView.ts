import { ViewDateEffectiveBaseEntity } from '../base';

export class ExchangeRateItemView extends ViewDateEffectiveBaseEntity {
  public exchangeRateTransId: string = null;
  public rate: number = 0;
  public exchangeRateGUID: string = null;
  public currencyGUID: string = null;
  public homeCurrencyGUID: string = null;
  public currency_Values: string = null;
  public homeCurrency_Values: string = null;
}
