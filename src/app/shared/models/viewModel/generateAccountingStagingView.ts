import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProcessTransType, StagingBatchStatus } from 'shared/constants';

export class GenerateAccountingStagingView extends ViewCompanyBaseEntity {
  public generateAccountingStagingGUID: string = null;
  public listProcessTransType: number[] = [];
  public stagingBatchStatus: number[] = [];
}
