import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class GenPULineInvoiceView extends ViewCompanyBaseEntity {
  public documentStatusGUID: string = null;
  public purchaseLineInvoiceTableGUID: string = null;
}
export class GenPULineInvoiceResultView extends ViewCompanyBaseEntity {}
