import { AccessLevel } from 'shared/constants';
import { SysUserSettingsItemView } from './sysUserSettingsView';

export class AccessRightView {
  // accessRight: number = 0;
  companyGUID: string = null;
  // roleGUID: string = null;
  sysFeatureTableGUID: string = null;
  sysFeatureTable_FeatureId: string = null;
  sysFeatureTable_ParentFeatureId: string = null;
  sysFeatureTable_Path: string = null;
  sysRoleTable_SiteLoginType: number = null;
  accessLevels: AccessLevelModel = null; // action, read, update, create, delete
}

export class UserLogonView {
  initDataCheck: SystemInitDataCheck = null;
  user: SysUserSettingsItemView = null;
  companyBranch: CompanyBranchView = null;
}

export class SystemInitDataCheck {
  hasAnyCompany: boolean = false;
  hasAnySysUserTable: boolean = false;
}

export class CompanyBranchView {
  companyGUID: string = null;
  companyId: string = null;
  branchGUID: string = null;
  branchId: string = null;
}

export class AccessRightWithCompanyBranchView {
  accessRight: AccessRightView[] = null;
  companyBranch: CompanyBranchView = null;
}

export class AccessLevelModel {
  action: AccessLevel = 0;
  read: AccessLevel = 0;
  update: AccessLevel = 0;
  create: AccessLevel = 0;
  delete: AccessLevel = 0;
}

export class AccessRightBU {
  accessRight: AccessRightView[] = null;
  parentChildBU: string[] = null;
  businessUnitGUID: string = null;
  businessUnitId: string = null;
}

export class UserAccessLevelParm {
  owner: string = null;
  businessUnitGUID: string = null;
  parentChildBU: string[] = null;
}
