import { ViewCompanyBaseEntity } from 'shared/models/base';

export class DepartmentListView extends ViewCompanyBaseEntity {
  public departmentGUID: string = null;
  public departmentId: string = null;
  public description: string = null;
}
