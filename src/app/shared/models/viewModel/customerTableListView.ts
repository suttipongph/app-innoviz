import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RecordType, IdentificationType } from 'shared/constants';

export class CustomerTableListView extends ViewCompanyBaseEntity {
  public customerTableGUID: string = null;
  public customerId: string = null;
  public documentStatusGUID: string = null;
  public name: string = null;
  public passportID: string = null;
  public recordType: RecordType = null;
  public taxID: string = null;
  public documentStatus_StatusId: string = null;
  public documentStatus_Values: string = null;
}
