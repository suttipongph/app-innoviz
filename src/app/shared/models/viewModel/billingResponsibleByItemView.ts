import { ViewCompanyBaseEntity } from 'shared/models/base';
import { BillingBy } from 'shared/constants';

export class BillingResponsibleByItemView extends ViewCompanyBaseEntity {
  public billingResponsibleByGUID: string = null;
  public billingBy: BillingBy = null;
  public billingResponsibleById: string = null;
  public description: string = null;
}
