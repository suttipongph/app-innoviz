import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class CustVisitingTransListView extends ViewCompanyBaseEntity {
  public custVisitingTransGUID: string = null;
  public custVisitingDate: string = null;
  public description: string = null;
  public responsibleByGUID: string = null;
  public refGUID: string = null;
  public employeeTable_EmployeeId: string = null;
  public employeeTable_Values: string = null;
}
