import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RecordType, IdentificationType } from 'shared/constants';

export class RelatedPersonTableListView extends ViewCompanyBaseEntity {
  public relatedPersonTableGUID: string = null;
  public companyName: string = null;
  public name: string = null;
  public operatedBy: string = null;
  public passportId: string = null;
  public position: string = null;
  public recordType: RecordType = null;
  public relatedPersonId: string = null;
  public taxId: string = null;
}
