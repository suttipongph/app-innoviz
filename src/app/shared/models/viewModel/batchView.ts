import { BatchIntervalType, BatchJobStatus } from 'shared/constants';

export class BatchView {
  public jobId: string = null;
  public triggerId: string = null;

  public startDateTime: string = null;
  public endDateTime: string = null;
  public nextScheduledDateTime: string = null;
  public lastExecutedDateTime: string = null;

  public isFinalized: boolean = false;
  public intervalType: BatchIntervalType = null;
  public intervalCount: string = null;
  public recurringCount: number = null;

  public isEndOfMonth: boolean = false;

  public controllerUrl: string = null;
  public description: string = null;
  public jobStatus: BatchJobStatus = null;
  public paramValues: string = null;

  public lastExecutedInstanceState: number = null;

  public companyGUID: string = null;
  public companyId: string = null;

  public createdBy: string = null;
  public createdDateTime: string = null;
  public modifiedBy: string = null;
  public modifiedDateTime: string = null;
  rowAuthorize: number = null;
}
