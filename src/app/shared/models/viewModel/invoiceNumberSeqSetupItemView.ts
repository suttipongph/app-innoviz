import { ProductType } from 'shared/constants';
import { ViewBranchCompanyBaseEntity } from '../base';

export class InvoiceNumberSeqSetupItemView extends ViewBranchCompanyBaseEntity {
  public branchGUID: string = null;
  public invoiceNumberSeqSetupGUID: string = null;
  public invoiceTypeGUID: string = null;
  public productType: ProductType = null;
  public numberSeqTableGUID: string = null;
  public invoiceType_InvoiceTypeId: string = null;
  public invoiceType_Description: string = null;
  public leaseType_LeaseTypeId: string = null;
  public leaseType_Description: string = null;
  public numberSeqTable_NumberSeqCode: string = null;
  public numberSeqTable_Description: string = null;
}
