import { ViewCompanyBaseEntity } from 'shared/models/base';
import { MonthOfYear, RefType } from 'shared/constants';

export class TaxReportTransListView extends ViewCompanyBaseEntity {
  public taxReportTransGUID: string = null;
  public amount: number = 0;
  public description: string = null;
  public month: MonthOfYear = null;
  public year: number = 0;
}
