import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class IntroducedByItemView extends ViewBranchCompanyBaseEntity {
  public introducedById: string = null;
  public description: string = null;
  public introducedByGUID: string = null;
  public vendorTableGUID: string = null;
  public vendorTable_VendorId: string = null;
  public vendorTable_Name: string = null;
  public vendorTable_Values: string = null;
}
