import { ViewCompanyBaseEntity } from 'shared/models/base';

export class RaceItemView extends ViewCompanyBaseEntity {
  public raceGUID: string = null;
  public description: string = null;
  public raceId: string = null;
}
