import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CreditAppReqBusCollaterlInfoListView extends ViewCompanyBaseEntity {
  public creditAppReqBusCollaterlInfoGUID: string = null;
  public creditApplicationRequestId: string = null;
}
