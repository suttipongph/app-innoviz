import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType } from 'shared/constants';

export class InquiryCALineOutstandingByCAListView extends ViewCompanyBaseEntity {
  public inquiryCALineOutstandingByCAGUID: string = null;
  public creditAppTableGUID: string = null;
  public approvedCreditLimitLine: number = 0;
  public arBalance: number = 0;
  public buyerId: string = null;
  public creditAppId: string = null;
  public creditLimitBalance: number = 0;
  public creditLimitTypeId: string = null;
  public customerId: string = null;
  public expiryDate: string = null;
  public lineNum: number = 0;
  public productType: ProductType = null;
  public startDate: string = null;
  public expiryDate_Values: string = null;
  public startDate_Values: string = null;
  public buyerTable_Values: string = null;
  public creditAppTable_Values: string = null;
  public customerTable_Values: string = null;
  public creditLimitType_Values: string = null;
  public maxPurchasePct: number = 0;
  public assignmentMethodGUID: string = null;
  public billingResponsibleByGUID: string = null;
  public methodOfPaymentGUID: string = null;
  public status: string = null;
}
