import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class ReceiptLineListView extends ViewBranchCompanyBaseEntity {
  public receiptLineGUID: string = null;
  public invoiceAmount: number = 0;
  public invoiceTableGUID: string = null;
  public lineNum: number = 0;
  public settleAmount: number = 0;
  public settleBaseAmount: number = 0;
  public settleTaxAmount: number = 0;
  public whtAmount: number = 0;
  public invoiceTable_Values: string = null;
  public receiptTableGUID: string = null;

  public invoiceTable_InvoiceId: string = null;
}
