import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ContactTo } from 'shared/constants';

export class DocumentReturnTableListView extends ViewCompanyBaseEntity {
  public documentReturnTableGUID: string = null;
  public contactPersonName: string = null;
  public contactTo: ContactTo = null;
  public documentReturnId: string = null;
  public documentReturnMethodGUID: string = null;
  public documentStatusGUID: string = null;
  public expectedReturnDate: string = null;
  public requestorGUID: string = null;
  public documentStatus_StatusId: string = null;
  public documentStatus_Values: string = null;
  public documentReturnMethod_Values: string = null;
  public employeeTable_Values: string = null;
  public  documentReturnMethod_DocumentReturnMethodId: string = null;
		public  employeeTable_EmployeeTableId: string = null;
}
