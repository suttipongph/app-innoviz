import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, SuspenseInvoiceType, RefType } from 'shared/constants';

export class ReportAgingView extends ViewCompanyBaseEntity {
  public asOfDate: string = null;
  public buyerTableGUID: string[] = null;
  public customerTableGUID: string[] = null;
  public productType: ProductType = null;
  public refType: RefType[] = null;
  public suspenseInvoiceType: SuspenseInvoiceType = null;
  public agingReportSetup_Description_LineNum1: string = null;
  public agingReportSetup_Description_LineNum2: string = null;
  public agingReportSetup_Description_LineNum3: string = null;
  public agingReportSetup_Description_LineNum4: string = null;
  public agingReportSetup_Description_LineNum5: string = null;
  public agingReportSetup_Description_LineNum6: string = null;
}
