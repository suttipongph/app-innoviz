import { ViewCompanyBaseEntity } from 'shared/models/base';

export class ConsortiumTableItemView extends ViewCompanyBaseEntity {
  public consortiumTableGUID: string = null;
  public consortiumId: string = null;
  public description: string = null;
  public documentStatusGUID: string = null;
}
