import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BusinessSizeItemView extends ViewCompanyBaseEntity {
  public businessSizeGUID: string = null;
  public businessSizeId: string = null;
  public description: string = null;
}
