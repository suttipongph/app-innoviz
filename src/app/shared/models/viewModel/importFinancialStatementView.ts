import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';
import { FileInformation } from '../systemModel';

export class ImportFinancialStatementView extends ViewCompanyBaseEntity {
  public fileInfo: FileInformation = null;
  public importFinancialStatementGUID: string = null;
  public file: string = null;
  public name: string = null;
  public refId: string = null;  
  public refType: RefType = null;
  public refGUID: string = null;
}
