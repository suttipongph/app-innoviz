import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class InterestRateListView extends ViewBranchCompanyBaseEntity {
  public interestRateGUID: string = null;
  public description: string = null;
  public interestRateId: string = null;
}
