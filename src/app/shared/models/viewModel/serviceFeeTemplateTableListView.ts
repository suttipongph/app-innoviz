import { ViewBranchCompanyBaseEntity } from 'shared/models/base';
import { ProductType } from 'shared/constants';

export class ServiceFeeTemplateTableListView extends ViewBranchCompanyBaseEntity {
  public serviceFeeTemplateTableGUID: string = null;
  public description: string = null;
  public serviceFeeTemplateTableId: string = null;
}
