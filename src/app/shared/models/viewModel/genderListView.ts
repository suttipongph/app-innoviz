import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class GenderListView extends ViewBranchCompanyBaseEntity {
  public genderGUID: string = null;
  public description: string = null;
  public genderId: string = null;
}
