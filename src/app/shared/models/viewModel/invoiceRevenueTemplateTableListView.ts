import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class InvoiceRevenueTemplateTableListView extends ViewBranchCompanyBaseEntity {
  public invoiceRevenueTemplateTableGUID: string = null;
  public description: string = null;
  public invoiceRevenueTemplateTableID: string = null;
}
