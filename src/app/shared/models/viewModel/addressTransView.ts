import { ViewCompanyBaseEntity } from '../base';

export class AddressTransListView extends ViewCompanyBaseEntity {
  public addressTransGUID: string = null;
  public address1: string = null;
  public address2: string = null;
  public addressCountryGUID: string = null;
  public addressDistrictGUID: string = null;
  public addressPostalCodeGUID: string = null;
  public addressProvinceGUID: string = null;
  public addressSubDistrictGUID: string = null;
  public isTax: boolean = false;
  public languageGUID: string = null;
  public name: string = null;
  public primary: boolean = false;
  public refGUID: string = null;
  public refType: number = 0;
  public taxBranchId: string = null;
  public taxBranchName: string = null;
  public taxId: string = null;
  public refId: string = null;
  public propertyTypeGUID: string = null;
  public ownershipGUID: string = null;
  public currentAddress: boolean = false;

  public addressCountry_CountryId: string = null;
  public addressCountry_Name: string = null;
  public addressDistrict_DistrictId: string = null;
  public addressDistrict_Name: string = null;
  public addressPostalCode_PostalCode: string = null;
  public addressProvince_ProvinceId: string = null;
  public addressProvince_Name: string = null;
  public addressSubDistrict_SubDistrictId: string = null;
  public addressSubDistrict_Name: string = null;
  public language_LanguageId: string = null;
}
export class AddressTransItemView extends ViewCompanyBaseEntity {
  public addressTransGUID: string = null;
  public address1: string = null;
  public address2: string = null;
  public addressCountryGUID: string = null;
  public addressDistrictGUID: string = null;
  public addressPostalCodeGUID: string = null;
  public addressProvinceGUID: string = null;
  public addressSubDistrictGUID: string = null;
  public isTax: boolean = false;
  public languageGUID: string = null;
  public name: string = null;
  public primary: boolean = false;
  public refGUID: string = null;
  public refType: number = 0;
  public taxBranchId: string = null;
  public taxBranchName: string = null;
  public taxId: string = null;
  public refId: string = null;
  public propertyTypeGUID: string = null;
  public ownershipGUID: string = null;
  public currentAddress: boolean = false;

  public addressCountry_CountryId: string = null;
  public addressCountry_Name: string = null;
  public addressDistrict_DistrictId: string = null;
  public addressDistrict_Name: string = null;
  public addressPostalCode_PostalCode: string = null;
  public addressProvince_ProvinceId: string = null;
  public addressProvince_Name: string = null;
  public addressSubDistrict_SubDistrictId: string = null;
  public addressSubDistrict_Name: string = null;
  public language_LanguageId: string = null;
}
