import { ViewCompanyBaseEntity } from 'shared/models/base';
import { AgreementDocType } from 'shared/constants';

export class GuarantorAgreementTableListView extends ViewCompanyBaseEntity {
  public guarantorAgreementTableGUID: string = null;
  public affiliate: boolean = false;
  public agreementDate: string = null;
  public agreementDocType: AgreementDocType = null;
  public description: string = null;
  public documentStatusGUID: string = null;
  public expiryDate: string = null;
  public guarantorAgreementId: string = null;
  public internalGuarantorAgreementId: string = null;
  public parentCompanyGUID: string = null;
  public mainAgreementTable_MainAgreementId: string = null;
  public mainAgreementTable_MainAgreementGUID: string = null;
  public documentStatus_StatusId: string = null;
  public refGuarantorAgreementTableGUID: string = null;
}
