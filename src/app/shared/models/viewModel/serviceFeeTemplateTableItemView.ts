import { ViewBranchCompanyBaseEntity } from 'shared/models/base';
import { ProductType } from 'shared/constants';

export class ServiceFeeTemplateTableItemView extends ViewBranchCompanyBaseEntity {
  public serviceFeeTemplateTableGUID: string = null;
  public description: string = null;
  public productType: ProductType = null;
  public serviceFeeTemplateTableId: string = null;
  public serviceFeeTemplateTableTableGUID: string = null;
}
