import { ViewCompanyBaseEntity } from 'shared/models/base';

export class AssignmentAgreementLineItemView extends ViewCompanyBaseEntity {
  public assignmentAgreementLineGUID: string = null;
  public assignmentAgreementTableGUID: string = null;
  public buyerAgreementAmount: number = 0;
  public buyerAgreementTableGUID: string = null;
  public endDate: string = null;
  public referenceAgreementID: string = null;
  public startDate: string = null;
  public assignmentAgreementId: string = null;
  public lineNum: number = 0;
  public assignmentAgreementTable_StatusId: string = null;
  public assignmentAgreementTable_InternalAssignmentAgreementId: string = null;
  public assignmentAgreementTable_AssignmentAgreementId: string = null;
  public buyerTableGUID: string = null;
  public assignmentAgreementTable_CustomerTableGUID: string = null;
  public assignmentAgreementTable_BuyerTableGUID: string = null;
  public assignmentAgreementTable_Values: string = null;
}
