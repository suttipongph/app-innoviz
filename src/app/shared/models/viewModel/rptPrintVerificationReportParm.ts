import { ViewReportBaseEntity } from '../base';

export class RptPrintVerificationReportParm extends ViewReportBaseEntity {
  public verificationTableGUID: string = null;
}
