import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class AddressCountryItemView extends ViewBranchCompanyBaseEntity {
  public addressCountryGUID: string = null;
  public countryId: string = null;
  public name: string = null;
}
