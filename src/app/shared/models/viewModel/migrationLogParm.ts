export class MigrationLogParm {
  public migrationFromDate: string = null;
  public migrationToDate: string = null;
  public migrationTable: string = null;
}
