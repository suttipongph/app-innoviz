import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class PropertyTypeListView extends ViewBranchCompanyBaseEntity {
  public propertyTypeGUID: string = null;
  public description: string = null;
  public propertyTypeId: string = null;
}
