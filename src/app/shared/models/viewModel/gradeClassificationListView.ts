import { ViewCompanyBaseEntity } from 'shared/models/base';

export class GradeClassificationListView extends ViewCompanyBaseEntity {
  public gradeClassificationGUID: string = null;
  public description: string = null;
  public gradeClassificationId: string = null;
}
