import { RefType } from 'shared/constants';

export class PrintSetBookDocTransParm {
  public refType: RefType = null;
  public refGUID: string = null;  
  public documentTemplateTableGUID:string = null;
  public bookmarkDocumentTransGUID:string = null;
}
