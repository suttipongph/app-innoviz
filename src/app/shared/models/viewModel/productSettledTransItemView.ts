import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, RefType } from 'shared/constants';

export class ProductSettledTransItemView extends ViewCompanyBaseEntity {
  public productSettledTransGUID: string = null;
  public documentId: string = null;
  public interestCalcAmount: number = 0;
  public interestDate: string = null;
  public interestDay: number = 0;
  public invoiceSettlementDetailGUID: string = null;
  public lastReceivedDate: string = null;
  public originalDocumentId: string = null;
  public originalInterestCalcAmount: number = 0;
  public originalRefGUID: string = null;
  public originalRefId: string = null;
  public pdcInterestOutstanding: number = 0;
  public postedInterestAmount: number = 0;
  public productType: ProductType = null;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
  public reserveToBeRefund: number = 0;
  public settledDate: string = null;
  public settledInvoiceAmount: number = 0;
  public settledPurchaseAmount: number = 0;
  public settledReserveAmount: number = 0;
  public totalRefundAdditionalIntAmount: number = 0;
  public invoiceSettlementDetail_Values: string = null;
}
