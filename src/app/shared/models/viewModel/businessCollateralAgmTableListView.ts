import { ViewCompanyBaseEntity } from 'shared/models/base';
import { AgreementDocType, ProductType } from 'shared/constants';

export class BusinessCollateralAgmTableListView extends ViewCompanyBaseEntity {
  public businessCollateralAgmTableGUID: string = null;
  public agreementDocType: AgreementDocType = null;
  public businessCollateralAgmId: string = null;
  public creditAppRequestTableGUID: string = null;
  public customerTableGUID: string = null;
  public description: string = null;
  public documentStatusGUID: string = null;
  public internalBusinessCollateralAgmId: string = null;
  public signingDate: string = null;
  public productType: ProductType = null;

  public customerTable_Values: string = null;
  public creditAppRequestTable_Values: string = null;
  public customerTable_CustomerId: string = null;
  public creditAppRequestTable_CreditAppRequestId: string = null;
  public documentStatus_StatusId: string = null;
  public creditAppTableGUID: string = null;
  public creditAppTable_Values: string = null;
  public creditAppTable_CreditAppId: string = null;
}
