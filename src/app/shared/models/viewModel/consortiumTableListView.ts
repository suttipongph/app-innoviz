import { ViewCompanyBaseEntity } from 'shared/models/base';

export class ConsortiumTableListView extends ViewCompanyBaseEntity {
  public consortiumTableGUID: string = null;
  public consortiumId: string = null;
  public description: string = null;
}
