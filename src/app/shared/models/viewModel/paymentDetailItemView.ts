import { ViewCompanyBaseEntity } from 'shared/models/base';
import { PaidToType, RefType } from 'shared/constants';

export class PaymentDetailItemView extends ViewCompanyBaseEntity {
  public paymentDetailGUID: string = null;
  public customerTableGUID: string = null;
  public invoiceTypeGUID: string = null;
  public paidToType: PaidToType = null;
  public paymentAmount: number = 0;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
  public suspenseTransfer: boolean = false;
  public totalPaymentAmount: number = 0;
  public vendorTableGUID: string = null;
}
