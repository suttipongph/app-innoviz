import { ViewCompanyBaseEntity } from 'shared/models/base';

export class IntercompanyListView extends ViewCompanyBaseEntity {
  public intercompanyGUID: string = null;
  public description: string = null;
  public intercompanyId: string = null;
}
