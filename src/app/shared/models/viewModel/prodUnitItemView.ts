import { ViewCompanyBaseEntity } from 'shared/models/base';
export class ProdUnitItemView extends ViewCompanyBaseEntity {
  public unitId: string = null;
  public description: string = null;
  public prodUnitGUID: string = null;
}
