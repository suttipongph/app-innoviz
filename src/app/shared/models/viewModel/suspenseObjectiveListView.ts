import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class SuspenseObjectiveListView extends ViewBranchCompanyBaseEntity {
  public suspenseObjectiveGUID: string = null;
  public description: string = null;
  public suspenseObjectiveId: string = null;
}
