import { ViewReportBaseEntity } from '../base';

export class PrintWithdrawalReportView extends ViewReportBaseEntity {
  public printWithdrawalGUID: string = null;
  public buyerId: string = null;
  public creditTermId: string = null;
  public customerId: string = null;
  public documentStatus: string = null;
  public dueDate: string = null;
  public numberOfExtension: number = 0;
  public termExtension: boolean = false;
  public totalInterestPct: number = 0;
  public withdrawalAmount: number = 0;
  public withdrawalDate: string = null;
  public withdrawalId: string = null;
  public creditTerm_Values: string = null;
  public withdrawalTableGUID: string = null;
}
