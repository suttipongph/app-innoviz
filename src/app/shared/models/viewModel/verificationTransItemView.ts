import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class VerificationTransItemView extends ViewCompanyBaseEntity {
  public verificationTransGUID: string = null;
  public buyerTable_Values: string = null;
  public documentId: string = null;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
  public remark: string = null;
  public verificationDate: string = null;
  public verificationTableGUID: string = null;
  public buyerTableGUID: string = null;
  public creditAppTableGUID: string = null;
}
