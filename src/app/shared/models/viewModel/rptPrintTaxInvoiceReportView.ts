import { ViewReportBaseEntity } from '../base/viewReportBaseEntity';

export class PrintTaxInvoiceReportView extends ViewReportBaseEntity {
  public taxInvoiceTableGUID: string = null;
  public printTaxInvoiceGUID: string = null;
  public customerId: string = null;
  public documentId: string = null;
  public dueDate: string = null;
  public invoiceAmount: number = 0;
  public invoiceAmountBeforeTax: number = 0;
  public invoiceId: string = null;
  public invoiceTypeId: string = null;
  public issuedDate: string = null;
  public taxAmount: number = 0;
  public taxInvoiceId: string = null;
  public isCopy: boolean = false;
}
