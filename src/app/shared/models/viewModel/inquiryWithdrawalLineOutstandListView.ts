import { ViewCompanyBaseEntity } from 'shared/models/base';

export class InquiryWithdrawalLineOutstandListView extends ViewCompanyBaseEntity {
  public withdrawalLineGUID: string = null;
  public billingDate: string = null;
  public buyerTableGUID: string = null;
  public buyerTable_BuyerId: string = null;
  public buyerTable_Values: string = null;
  public collectionDate: string = null;
  public customerTableGUID: string = null;
  public customerTable_CustomerId: string = null;
  public customerTable_Values: string = null;
  public dueDate: string = null;
  public methodOfPaymentGUID: string = null;
  public methodOfPayment_MethodOfPaymentId: string = null;
  public methodOfPayment_Values: string = null;
  public outstanding: number = 0;
  public withdrawalAmount: number = 0;
  public withdrawalTableGUID: string = null;
  public withdrawalTable_WithdrawalId: string = null;
  public withdrawalTable_Values: string = null;
}
