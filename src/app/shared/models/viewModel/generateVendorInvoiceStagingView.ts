import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProcessTransType, StagingBatchStatus } from 'shared/constants';

export class GenerateVendorInvoiceStagingView extends ViewCompanyBaseEntity {
  public generateVendorInvoiceStagingGUID: string = null;
  public listProcessTransType: number[] = [];
  public stagingBatchStatus: number[] = [];
}
