import { ViewCompanyBaseEntity } from 'shared/models/base';
import { IdentificationType } from 'shared/constants';

export class BuyerTableListView extends ViewCompanyBaseEntity {
  public buyerTableGUID: string = null;
  public buyerId: string = null;
  public buyerName: string = null;
  public passportId: string = null;
  public taxId: string = null;
  public documentStatus_Values: string = null;
  public documentStatus_StatusId: string = null;
  public documentStatusGUID: string = null;
}
