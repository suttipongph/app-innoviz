import { ViewCompanyBaseEntity } from 'shared/models/base';
import { CreditAppRequestType } from 'shared/constants';

export class CreditAppRequestTableAmendListView extends ViewCompanyBaseEntity {
  public creditAppRequestTableAmendGUID: string = null;
  public customerTable_CustomerId: string = null;
  public documentStatus_Values: string = null;
  public creditAppRequestTable_RequestDate: string = null;
  public creditAppRequestTable_RefCreditAppTableGUID: string = null;
  public creditAppRequestTable_CreditAppRequestId: string = null;
  public creditAppRequestTable_CreditAppRequestType: CreditAppRequestType = null;
}
