import { ViewCompanyBaseEntity } from 'shared/models/base';

export class RegistrationTypeListView extends ViewCompanyBaseEntity {
  public registrationTypeGUID: string = null;
  public description: string = null;
  public registrationTypeId: string = null;
}
