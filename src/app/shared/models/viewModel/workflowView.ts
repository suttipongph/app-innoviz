import { SearchParameter } from '../systemModel';

export class Action {
  public batchable: boolean = false;
  public metadata: string = null;
  public name: string = null;
}

export class DataField {
  public category: string = null;
  public name: string = null;
  public value: string = null;
}
export class CommentView {
  public user: string = null;
  public message: string = null;
  public createdDate: string = null;
  public createdBy: string = null;
}
export class WorkflowInstanceView {
  public id: number = 0;
  public serialNumber: string = null;
  public name: string = null;
  public folio: string = null;
  public action: string = null;
  public impersonateUser: string = null;
  public comments: CommentView[] = null;
  public dataFields: DataField[] = null;
  public actInstDestName: string = null;
  public actInstDestDisplayName: string = null;
  public allocatedUser: string = null;
  public worklistItemId: number = -1;
}

export class TaskListParm {
  public search: SearchParameter = null;
  public workflow: WorkflowInstanceView = null;
}

export class TaskItemView {
  public id: number = null;
  public name: string = null;
  public description: string = null;
  public folio: string = null;
  public serialNumber: string = null;
  public originator: string = null;
  public startDate: string = null;
  public viewflow: string = null;
  public status: string = null;
  public actInstDestId: number = null;
  public actInstDestName: string = null;
  public actInstDestDescription: string = null;
  public actInstDestStartDate: string = null;
  public actInstDestDataFields: DataField[] = null;
  public eventName: string = null;
  public eventDescription: string = null;

  public actions: Action[] = null;
  public comments: Comment[] = null;
  public dataFields: DataField[] = null;

  public formURL: string = null;
  public actInstDestDisplayName: string = null;
  public allocatedUser: string = null;
  public worklistItemId: number = null;
}
