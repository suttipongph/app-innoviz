import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class MessengerRequestTypeItemView extends ViewBranchCompanyBaseEntity {
  public messengerRequestTypeGUID: string = null;
  public description: string = null;
  public messengerRequestTypeId: string = null;
}
