import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ContactTo } from 'shared/constants';

export class ManageDocumentReturnView extends ViewCompanyBaseEntity {
  public manageDocumentReturnGUID: string = null;
  public contactPersonName: string = null;
  public contactTo: ContactTo = null;
  public documentReturnId: string = null;
  public documentStatusGUID: string = null;
  public originalDocumentStatus: string = null;
}
