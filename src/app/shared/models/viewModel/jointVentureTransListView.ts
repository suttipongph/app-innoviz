import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class JointVentureTransListView extends ViewCompanyBaseEntity {
  public jointVentureTransGUID: string = null;
  public authorizedPersonTypeGUID: string = null;
  public name: string = null;
  public operatedBy: string = null;
  public ordering: number = 0;
  public authorizedPersonType_Values: string = null;
  public authorizedPersonType_AuthorizedPersonTypeId: string = null;
  public documentStatus_StatusCode: string = null;
}
