import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BuyerInvoiceTableListView extends ViewCompanyBaseEntity {
  public buyerInvoiceTableGUID: string = null;
  public amount: number = 0;
  public buyerAgreementLineGUID: string = null;
  public buyerAgreementTableGUID: string = null;
  public buyerInvoiceId: string = null;
  public dueDate: string = null;
  public invoiceDate: string = null;
  public buyerAgreementLine_Values: string = null;
  public buyerAgreementTable_Values: string = null;
  public accessModeCanDelete: boolean = null;
}
