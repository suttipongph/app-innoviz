import { ViewCompanyBaseEntity } from 'shared/models/base';
import { Priority, ProductType, ContactTo, Result, RefType } from 'shared/constants';

export class MessengerJobTableListView extends ViewCompanyBaseEntity {
  public messengerJobTableGUID: string = null;
  public buyerTableGUID: string = null;
  public contactName: string = null;
  public customerTableGUID: string = null;
  public documentStatusGUID: string = null;
  public jobDate: string = null;
  public jobEndTime: string = null;
  public jobId: string = null;
  public jobStartTime: string = null;
  public jobTypeGUID: string = null;
  public messengerTableGUID: string = null;
  public priority: Priority = null;
  public requestorGUID: string = null;
  public result: Result = null;
  public customerTable_Values: string = null;

  public buyerTable_Values: string = null;
  public jobType_Values: string = null;
  public messengerTable_Values: string = null;
  public documentStatus_Values: string = null;

  public employeeTable_Values: string = null;
  public customerTable_CustomerId: string = null;
  public buyerTable_BuyerId: string = null;
  public jobType_JobTypeId: string = null;
  public messengerTable_MessengerId: string = null;
  public documentStatus_StatusId: string = null;
  public employeeTable_EmployeeId: string = null;
}
