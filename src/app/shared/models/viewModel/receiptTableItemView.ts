import { RefType } from 'shared/constants';
import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class ReceiptTableItemView extends ViewBranchCompanyBaseEntity {
  public receiptTableGUID: string = null;
  public chequeBankGroupGUID: string = null;
  public chequeBranch: string = null;
  public chequeDate: string = null;
  public chequeNo: string = null;
  public currencyGUID: string = null;
  public customerTableGUID: string = null;
  public documentStatusGUID: string = null;
  public exchangeRate: number = 0;
  public methodOfPaymentGUID: string = null;
  public overUnderAmount: number = 0;
  public receiptAddress1: string = null;
  public receiptAddress2: string = null;
  public receiptDate: string = null;
  public receiptId: string = null;
  public remark: string = null;
  public settleAmount: number = 0;
  public settleAmountMST: number = 0;
  public settleBaseAmount: number = 0;
  public settleBaseAmountMST: number = 0;
  public settleTaxAmount: number = 0;
  public settleTaxAmountMST: number = 0;
  public transDate: string = null;
  public unboundReceiptAddress: string = null;
  public customerTable_Values: string = null;
  public receiptTempTable_Values: string = null;
  public currency_Values: string = null;
  public documentStatus_Values: string = null;
  public documentStatus_Description: string = null;
  public methodopayment_Values: string = null;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
  public invoiceSettlementDetailGUID: string = null;
  public invoiceSettlementDetail_Values: string = null;

  public receiptAddress_Values: string = null;
}
