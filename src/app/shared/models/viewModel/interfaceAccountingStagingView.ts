import { ProcessTransType, InterfaceStatus } from 'shared/constants';

export class InterfaceAccountingStagingView {
  public interfaceStatus: InterfaceStatus[] = null;
  public processTransType: ProcessTransType[] = null;
}
