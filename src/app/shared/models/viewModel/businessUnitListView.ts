import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BusinessUnitListView extends ViewCompanyBaseEntity {
  public businessUnitGUID: string = null;
  public businessUnitId: string = null;
  public description: string = null;
  public parentBusinessUnitGUID: string = null;
  public parentBusinessUnit_Values: string = null;
  public parentBusinessUnit_BusinessUnitId: string = null;
}
