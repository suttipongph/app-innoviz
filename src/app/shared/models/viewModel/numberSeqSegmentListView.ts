import { NumberSeqSegmentType } from 'shared/constants';
import { ViewCompanyBaseEntity } from '../base';

export class NumberSeqSegmentListView extends ViewCompanyBaseEntity {
  public numberSeqSegmentGUID: string = null;
  public numberSeqTableGUID: string = null;
  public ordering: number = 0;
  public segmentType: NumberSeqSegmentType;
  public segmentValue: string = null;

  public numberSeqTable_Values: string = null;
}
