import { ProductType, SettleInvoiceCriteria, SuspenseInvoiceType } from 'shared/constants';
import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class InvoiceTypeItemView extends ViewBranchCompanyBaseEntity {
  public invoiceTypeGUID: string = null;
  public invoiceTypeId: string = null;
  public description: string = null;
  public autoGen: boolean = false;
  public directReceipt: boolean = false;
  public arLedgerAccount: string = null;
  public productType: ProductType = null;
  public productInvoice: boolean = false;
  public validateDirectReceiveByCA: boolean = false;
  public suspenseInvoiceType: SuspenseInvoiceType = null;
  public autoGenInvoiceRevenueTypeGUID: string = null;
  public invoiceRevenueType_Values: string = null;
}
