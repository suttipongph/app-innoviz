import { ViewCompanyBaseEntity } from 'shared/models/base';
import { CreditAppRequestType, PurchaseFeeCalculateBase } from 'shared/constants';

export class CreditAppRequestLineAmendItemView extends ViewCompanyBaseEntity {
  public creditAppRequestLineAmendGUID: string = null;
  public amendAssignmentMethod: boolean = false;
  public amendBillingDocumentCondition: boolean = false;
  public amendBillingInformation: boolean = false;
  public amendRate: boolean = false;
  public amendReceiptDocumentCondition: boolean = false;
  public amendReceiptInformation: boolean = false;
  public creditAppRequestLine_ApprovedCreditLimitLineRequest: number = 0;
  public creditAppRequestTable_ApprovedDate: string = null;
  public creditAppRequestLine_ApproverComment: string = null;
  public creditAppRequestLine_BuyerCreditLimit: number = 0;
  public buyerTable_Values: string = null;
  public creditAppRequestLine_BuyerTableGUID: string = null;
  public creditAppRequestTable_CreditAppRequestId: string = null;
  public creditAppRequestTable_CustomerTableGUID: string = null;
  public creditAppRequestTable_CreditAppRequestTableGUID: string = null;
  public creditAppRequestLineGUID: string = null;
  public creditAppRequestTable_CreditAppRequestType: CreditAppRequestType = null;
  public creditAppRequestLine_CreditComment: string = null;
  public creditAppRequestTable_CustomerCreditLimit: number = 0;
  public customerTable_Values: string = null;
  public creditAppRequestTable_Description: string = null;
  public documentStatus_Values: string = null;
  public documentStatus_StatusId: string = null;
  public creditAppRequestTable_DocumentStatusGUID: string = null;
  public creditAppRequestLine_MarketingComment: string = null;
  public creditAppRequestLine_AssignmentMethodGUID: string = null;
  public creditAppRequestLine_NewAssignmentMethodRemark: string = null;
  public creditAppRequestLine_NewBillingAddressGUID: string = null;
  public creditAppRequestLine_NewCreditLimitLineRequest: number = 0;
  public creditAppRequestLine_NewMaxPurchasePct: number = 0;
  public creditAppRequestLine_NewMethodOfPaymentGUID: string = null;
  public creditAppRequestLine_NewPurchaseFeeCalculateBase: PurchaseFeeCalculateBase = null;
  public creditAppRequestLine_NewPurchaseFeePct: number = 0;
  public creditAppRequestLine_NewReceiptAddressGUID: string = null;
  public creditAppRequestLine_NewReceiptRemark: string = null;
  public creditAppRequestLine_RefCreditAppLineGUID: string = null;
  public originalAssignmentMethodGUID: string = null;
  public originalAssignmentMethod_Values: string = null;
  public originalAssignmentMethodRemark: string = null;
  public originalBillingAddressGUID: string = null;
  public originalBillingAddress_Values: string = null;
  public originalCreditLimitLineRequest: number = 0;
  public originalMaxPurchasePct: number = 0;
  public originalMethodOfPaymentGUID: string = null;
  public originalMethodOfPayment_Values: string = null;
  public originalPurchaseFeeCalculateBase: PurchaseFeeCalculateBase = null;
  public originalPurchaseFeePct: number = 0;
  public originalReceiptAddressGUID: string = null;
  public originalReceiptAddress_Values: string = null;
  public originalReceiptRemark: string = null;
  public refCreditAppTable_Values: string = null;
  public creditAppRequestTable_Remark: string = null;
  public creditAppRequestTable_RequestDate: string = null;
  public unboundNewBillingAddress: string = null;
  public unboundNewReceiptAddress: string = null;
  public unboundOriginalBillingAddress: string = null;
  public unboundOriginalReceiptAddress: string = null;
  public processInstanceId: number = 0;
  public creditAppRequestLine_BlacklistStatusGUID: string = null;
  public creditAppRequestLine_CreditScoringGUID: string = null;
  public creditAppRequestLine_KYCSetupGUID: string = null;
  public creditAppRequestLine_CustomerBuyerOutstanding: number = 0;
  public creditAppRequestLine_AllCustomerBuyerOutstanding: number = 0;
  public creditAppRequestTable_CACondition: string = null;
}
