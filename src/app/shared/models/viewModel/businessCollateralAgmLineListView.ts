import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BusinessCollateralAgmLineListView extends ViewCompanyBaseEntity {
  public businessCollateralAgmLineGUID: string = null;
  public businessCollateralAgmTableGUID: string = null;
  public businessCollateralSubTypeGUID: string = null;
  public businessCollateralTypeGUID: string = null;
  public businessCollateralValue: number = 0;
  public buyerTableGUID: string = null;
  public description: string = null;
  public businessCollateralType_BusinessCollateralTypeId: string = null;
  public businessCollateralSubType_BusinessCollateralSubTypeId: string = null;
  public buyerTable_BuyerId: string = null;
  public businessCollateralType_Values: string = null;
  public businessCollateralSubType_Values: string = null;
  public buyerTable_Values: string = null;
  public lineNum: number = 0;
  public cancelled: boolean = false;
}
