import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class UpdateAssignmentAgreementSettleView extends ViewCompanyBaseEntity {
  public updateAssignmentAgreementSettleGUID: string = null;
  public assignmentAgreementSettleGUID: string = null;
  public assignmentAgreementId: string = null;
  public buyerAgreementId: string = null;
  public documentReasonGUID: string = null;
  public internalAssignmentAgreementId: string = null;
  public newSettledAmount: number = 0;
  public newSettledDate: string = null;
  public settledAmount: number = 0;
  public settledDate: string = null;
}
