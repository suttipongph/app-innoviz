import { ViewCompanyBaseEntity } from 'shared/models/base';
import { DayOfMonth, MethodOfBilling, PurchaseFeeCalculateBase, ApprovalDecision } from 'shared/constants';

export class CreditAppRequestLineItemView extends ViewCompanyBaseEntity {
  public creditAppRequestLineGUID: string = null;
  public acceptanceDocument: boolean = false;
  public acceptanceDocumentDescription: string = null;
  public approvalDecision: ApprovalDecision = null;
  public approvedCreditLimitLineRequest: number = 0;
  public approverComment: string = null;
  public assignmentAgreementTableGUID: string = null;
  public assignmentMethodGUID: string = null;
  public assignmentMethodRemark: string = null;
  public billingAddressGUID: string = null;
  public billingContactPersonGUID: string = null;
  public billingDay: DayOfMonth = null;
  public billingDescription: string = null;
  public billingRemark: string = null;
  public billingResponsibleByGUID: string = null;
  public blacklistStatusGUID: string = null;
  public buyerCreditLimit: number = 0;
  public buyerCreditTermGUID: string = null;
  public buyerProduct: string = null;
  public buyerTableGUID: string = null;
  public creditAppRequestTableGUID: string = null;
  public creditComment: string = null;
  public creditLimitLineRequest: number = 0;
  public creditScoringGUID: string = null;
  public creditTermDescription: string = null;
  public customerContactBuyerPeriod: number = 0;
  public customerRequest: number = 0;
  public dateOfEstablish: string = null;
  public insuranceCreditLimit: number = 0;
  public invoiceAddressGUID: string = null;
  public kycSetupGUID: string = null;
  public lineNum: number = 0;
  public mailingReceiptAddressGUID: string = null;
  public marketingComment: string = null;
  public maxPurchasePct: number = 0;
  public methodOfBilling: MethodOfBilling = null;
  public methodOfPaymentGUID: string = null;
  public paymentCondition: string = null;
  public purchaseFeeCalculateBase: PurchaseFeeCalculateBase = null;
  public purchaseFeePct: number = 0;
  public receiptAddressGUID: string = null;
  public receiptContactPersonGUID: string = null;
  public receiptDay: DayOfMonth = null;
  public receiptDescription: string = null;
  public receiptRemark: string = null;
  public remainingBuyerCreditLimit: number = 0;
  //#region  Unbound
  public unboundBillingAddress: string = null;
  public unboundInvoiceAddress: string = null;
  public unboundMailingReceiptAddress: string = null;
  public unboundReceiptAddress: string = null;
  public blacklistStatus_Values: string = null;
  public creditAppRequestTable_ProductType: number = 0;
  public creditAppRequestTable_Values: string = null;
  public creditAppRequestTable_StatusId: string = null;
  public businessSegment_Values: string = null;
  public businessType_Values: string = null;
  public lineOfBusiness_Values: string = null;
  // R02
  public remainingCreditLoanRequest: number = 0;
  public allCustomerBuyerOutstanding: number = 0;
  public customerBuyerOutstanding: number = 0;
  public refCreditAppLineGUID: string = null;
  public refCreditAppLine_Values: string = null;
  public refCreditAppTable_Values: string = null;
  public lineCondition: string = null;

  //#endregion
}
