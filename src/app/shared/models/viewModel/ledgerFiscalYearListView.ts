import { LedgerFiscalPeriodUnit } from 'shared/constants';
import { ViewCompanyBaseEntity } from '../base';

export class LedgerFiscalYearListView extends ViewCompanyBaseEntity {
  public fiscalYearId: string = null;
  public description: string = null;
  public periodUnit: LedgerFiscalPeriodUnit;
  public lengthOfPeriod: number = 0;
  public ledgerFiscalYearGUID: string = null;
  public startDate: string = null;
  public endDate: string = null;
}
