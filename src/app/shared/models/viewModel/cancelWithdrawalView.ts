import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CancelWithdrawalView extends ViewCompanyBaseEntity {
  public withdrawalTableGUID: string = null;
  public withdrawalTable_Values: string = null;
  public buyerTable_Values: string = null;
  public creditTerm_Values: string = null;
  public customerTable_Values: string = null;
  public documentStatus_Values: string = null;
  public dueDate: string = null;
  public totalInterestPct: number = 0;
  public withdrawalAmount: number = 0;
  public withdrawalDate: string = null;
  public documentReasonGUID: string = null;
}

export class CancelWithdrawalResultView extends ViewCompanyBaseEntity {}
