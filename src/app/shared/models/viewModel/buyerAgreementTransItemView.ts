import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class BuyerAgreementTransItemView extends ViewCompanyBaseEntity {
  public buyerAgreementTransGUID: string = null;
  public buyerAgreementLineGUID: string = null;
  public buyerAgreementTableGUID: string = null;
  public refGUID: string = null;
  public refId: string = null;
  public buyerAgreementLine_DueDate: string = null;
  public buyerAgreementLine_Amount: number = 0;
  public buyerAgreementLine_Period: string = null;
  public refType: RefType = null;
}
