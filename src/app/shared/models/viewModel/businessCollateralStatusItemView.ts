import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BusinessCollateralStatusItemView extends ViewCompanyBaseEntity {
  public businessCollateralStatusGUID: string = null;
  public businessCollateralStatusId: string = null;
  public description: string = null;
}
