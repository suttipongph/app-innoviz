import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, CreditLimitExpiration, PurchaseFeeCalculateBase } from 'shared/constants';

export class CreditAppTableListView extends ViewCompanyBaseEntity {
  public creditAppTableGUID: string = null;
  public approvedCreditLimit: number = 0;
  public approvedDate: string = null;
  public creditAppId: string = null;
  public creditLimitTypeGUID: string = null;
  public customerTableGUID: string = null;
  public expiryDate: string = null;
  public inactiveDate: string = null;
  public productType: ProductType = null;
  public refCreditAppRequestTableGUID: string = null;
  public startDate: string = null;
  public creditLimitType_Values: string = null;
  public creditAppRequestTable_Values: string = null;
  public customerTable_Values: string = null;
  public refCreditAppRequestTable_Values: string = null;
  public refCreditAppRequestTable_CreditAppRequestId: string = null;
  public creditLimitType_CreditLimitTypeId: string = null;
  public customerTable_CustomerId: string = null;
}
