import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class AssignmentAgmOutstandingItemView extends ViewCompanyBaseEntity {
  public assignmentAgmOutstandingGUID: string = null;
  public assignmentAgreementAmount: number = 0;
  public assignmentAgreementTableGUID: string = null;
  public buyerTableGUID: string = null;
  public customerTableGUID: string = null;
  public refGUID: string = null;
  public refID: string = null;
  public refType: RefType = null;
  public remainingAmount: number = 0;
  public settledAmount: number = 0;
}
