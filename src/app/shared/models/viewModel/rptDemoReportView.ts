import { ViewReportBaseEntity } from 'shared/models/base';

export class RptDemoReportView extends ViewReportBaseEntity {
  public recordType: number[] = null;
  public fromDate: string = null;
  public customerTableGUID: string[] = null;
}
