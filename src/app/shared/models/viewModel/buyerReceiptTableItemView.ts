import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class BuyerReceiptTableItemView extends ViewCompanyBaseEntity {
  public buyerReceiptTableGUID: string = null;
  public assignmentAgreementTableGUID: string = null;
  public buyerAgreementTableGUID: string = null;
  public buyerReceiptAddress: string = null;
  public buyerReceiptAmount: number = 0;
  public buyerReceiptDate: string = null;
  public buyerReceiptId: string = null;
  public buyerTableGUID: string = null;
  public cancel: boolean = false;
  public chequeDate: string = null;
  public chequeNo: string = null;
  public companyBankId: string = null;
  public customerTableGUID: string = null;
  public methodOfPaymentGUID: string = null;
  public refGUID: string = null;
  public refId: string = null;
  public refType: RefType = null;
  public remark: string = null;
  public showRemarkOnly: boolean = false;
  public buyerTable_Values: string = null;
  public customerTable_Values: string = null;
  public assignmentAgreementTable_Values: string = null;
  public buyerAgreementTable_Values: string = null;
}
