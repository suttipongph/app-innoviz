import { ViewCompanyBaseEntity } from 'shared/models/base';

export class DocumentTypeItemView extends ViewCompanyBaseEntity {
  public documentTypeGUID: string = null;
  public description: string = null;
  public documentTypeId: string = null;
}
