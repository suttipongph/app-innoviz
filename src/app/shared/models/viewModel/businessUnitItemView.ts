import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BusinessUnitItemView extends ViewCompanyBaseEntity {
  public businessUnitGUID: string = null;
  public businessUnitId: string = null;
  public description: string = null;
  public parentBusinessUnitGUID: string = null;
}
