import { ViewCompanyBaseEntity } from 'shared/models/base';

export class UpdateCustomerTableBlacklistStatusParamView extends ViewCompanyBaseEntity {
  public customerTableGUID: string = null;
  public blacklistStatusGUID: string = null;
}

export class UpdateCustomerTableBlacklistStatusResultView extends ViewCompanyBaseEntity {
  public customerTableGUID: string = null;
  public blacklistStatusGUID: string = null;
  public originalBlacklistStatus: string = null;
  public originalBlacklistStatusGUID: string = null;
  public customerTableId: string = null;
  public resultLabel: string = null;
}
