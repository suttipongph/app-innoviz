import { ViewBranchCompanyBaseEntity } from 'shared/models/base';
import { DocumentTemplateType } from 'shared/constants';

export class AgreementDocumentTemplateLineListView extends ViewBranchCompanyBaseEntity {
  public agreementDocumentTemplateLineGUID: string = null;
  public agreementDocumentGUID: string = null;
  public documentTemplateTableGUID: string = null;
  public documentTemplateType: DocumentTemplateType = null;
}
