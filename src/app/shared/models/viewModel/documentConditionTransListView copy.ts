import { ViewCompanyBaseEntity } from 'shared/models/base';
import { DocConVerifyType, RefType } from 'shared/constants';

export class DocumentConditionTransListView extends ViewCompanyBaseEntity {
  public documentConditionTransGUID: string = null;
  public documentTypeGUID: string = null;
  public mandatory: boolean = false;
}
