import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType } from 'shared/constants';

export class BuyerCreditLimitByProductListView extends ViewCompanyBaseEntity {
  public buyerCreditLimitByProductGUID: string = null;
  public creditLimit: number = 0;
  public creditLimitBalance: number = 0;
  public productType: ProductType = null;
  public buyerTableGUID: string = null;
}
