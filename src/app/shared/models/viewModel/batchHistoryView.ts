import { BatchInstanceState } from 'shared/constants';
export class BatchHistoryView {
  public jobId: string = null;
  public triggerId: string = null;
  public instanceHistoryId: string = null;
  public scheduledDateTime: string = null;
  public actualStartDateTime: string = null;
  public instanceState: BatchInstanceState = 0;
  public paramValues: string = null;
  public controllerUrl: string = null;
  public finishedDateTime: string = null;

  public anyFailedLogStatus: boolean = false;

  public createdBy: string = null;
  public createdDateTime: string = null;
  public modifiedBy: string = null;
  public modifiedDateTime: string = null;
}
