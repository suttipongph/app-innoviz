export class CalcSettleInvoiceAmountResultView {
  public balanceAmount: number = 0;
  public whtBalance: number = 0;
  public settleTaxBalance: number = 0;
  public maxSettleAmount: number = 0;
  public whtAmount: number = 0;
  public settleInvoiceAmount: number = 0;
  public settleTaxAmount: number = 0;
  public settleAmount: number = 0;
}
