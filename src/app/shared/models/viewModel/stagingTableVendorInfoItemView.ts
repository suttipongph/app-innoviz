import { RecordType } from 'shared/constants';
import { ViewCompanyBaseEntity } from 'shared/models/base';

export class StagingTableVendorInfoItemView extends ViewCompanyBaseEntity {
  public stagingTableVendorInfoGUID: string = null;
  public address: string = null;
  public addressName: string = null;
  public altName: string = null;
  public bankAccount: string = null;
  public bankAccountName: string = null;
  public bankBranch: string = null;
  public bankGroupId: string = null;
  public stagingTableCompanyId: string = null;
  public countryId: string = null;
  public currencyId: string = null;
  public districtId: string = null;
  public name: string = null;
  public postalCode: string = null;
  public processTransGUID: string = null;
  public provinceId: string = null;
  public recordType: RecordType = null;
  public subDistrictId: string = null;
  public taxBranchId: string = null;
  public vendGroupId: string = null;
  public vendorId: string = null;
  public vendorPaymentTransGUID: string = null;
  public vendorTaxId: string = null;
}
