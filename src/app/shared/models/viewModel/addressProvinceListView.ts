import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class AddressProvinceListView extends ViewBranchCompanyBaseEntity {
  public provinceId: string = null;
  public name: string = null;
  public addressProvinceGUID: string = null;
  public addressCountryGUID: string = null;
  public addressCountry_CountryId: string = null;
  public addressCountry_Name: string = null;
  public addressProvince_Values: string = null;
  public addressProvince_ProvinceId: string = null;
}
