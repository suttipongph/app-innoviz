import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CancelPurchaseView extends ViewCompanyBaseEntity {
  public purchaseTableGUID: string = null;
  public purchase_Values: string = null;
  public additionalPurchase: boolean = false;
  public customerTable_Values: string = null;
  public documentReasonGUID: string = null;
  public documentStatus_Values: string = null;
  public purchaseDate: string = null;
  public reasonRemark: string = null;
  public rollbill: boolean = false;
}
export class CancelPurchaseResultView extends ViewCompanyBaseEntity {}
