import { ProductType, RefType, SuspenseInvoiceType } from 'shared/constants';
import { ViewCompanyBaseEntity } from 'shared/models/base';

export class ReportAccruedInterestView extends ViewCompanyBaseEntity {
  public asOfDate: string = null;
  public buyerTableGUID: string = null;
  public customerTableGUID: string = null;
  public invoiceTypeGUID: string = null;
  public productType: ProductType[] = null;
  public refType: RefType[] = null;
  public suspenseInvoiceType: SuspenseInvoiceType = null;
}
