import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, CreditLimitConditionType, CreditLimitExpiration } from 'shared/constants';

export class CreditLimitTypeItemView extends ViewCompanyBaseEntity {
  public creditLimitTypeGUID: string = null;
  public allowAddLine: boolean = false;
  public buyerMatchingAndLoanRequest: boolean = false;
  public closeDay: number = 0;
  public creditLimitConditionType: CreditLimitConditionType = null;
  public creditLimitExpiration: CreditLimitExpiration = null;
  public creditLimitExpiryDay: number = 0;
  public creditLimitRemark: string = null;
  public creditLimitTypeId: string = null;
  public description: string = null;
  public inactiveDay: number = 0;
  public parentCreditLimitTypeGUID: string = null;
  public productType: ProductType = null;
  public revolving: boolean = false;
  public validateBuyerAgreement: boolean = false;
  public validateCreditLimitType: boolean = false;
  public bookmarkOrdering: number = 0;
}
