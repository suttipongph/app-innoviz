import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, SuspenseInvoiceType, InterestCalculationMethod, RefType, ReceivedFrom } from 'shared/constants';

export class InvoiceSettlementDetailItemView extends ViewCompanyBaseEntity {
  public invoiceSettlementDetailGUID: string = null;
  public assignmentAgreementTableGUID: string = null;
  public balanceAmount: number = 0;
  public buyerAgreementTableGUID: string = null;
  public creditAppTableGUID: string = null;
  public currencyGUID: string = null;
  public documentId: string = null;
  public interestCalculationMethod: InterestCalculationMethod = null;
  public invoiceAmount: number = 0;
  public invoiceDueDate: string = null;
  public invoiceTableGUID: string = null;
  public invoiceTypeGUID: string = null;
  public productType: ProductType = null;
  public purchaseAmount: number = 0;
  public purchaseAmountBalance: number = 0;
  public refGUID: string = null;
  public refReceiptTempPaymDetailGUID: string = null;
  public refType: RefType = null;
  public refId: string = null;
  public retentionAmountAccum: number = 0;
  public settleAmount: number = 0;
  public settleAssignmentAmount: number = 0;
  public settleInvoiceAmount: number = 0;
  public settlePurchaseAmount: number = 0;
  public settleReserveAmount: number = 0;
  public settleTaxAmount: number = 0;
  public suspenseInvoiceType: SuspenseInvoiceType = null;
  public whtAmount: number = 0;
  public whtSlipReceivedByBuyer: boolean = false;
  public invoiceType_Values: string = null;
  public buyerAgreementTable_Values: string = null;
  public assignmentAgreementTable_Values: string = null;
  //#region  InvoiceTable
  public invoiceTable_Values: string = null;
  public currency_Values: string = null;
  public creditAppTable_Values: string = null;
  public invoiceTable_WHTBalance: number = 0;
  public invoiceTable_SettleTaxBalance: number = 0;
  public invoiceTable_WHTAmount: number = 0;
  public invoiceTable_ProductType: number = 0;
  public invoiceTable_ProductInvoice: boolean = false;

  //#endregion InvoiceTable
  public receivedFrom: ReceivedFrom = null;
  public whtSlipReceivedByCustomer: boolean = false;
  // #region Unbound
  public maxSettleAmount: number = 0;
  //#endregion Unbound
}
