export class SysUserRolesView {
  public userId: string = null;
  public roleId: string = null;
  public sysUserTable_UserName: string = null;
  public sysRoleTable_Name: string = null;
  public sysRoleTable_DisplayName: string = null;
  public sysRoleTable_CompanyGUID: string = null;
  public sysRoleTable_Company_CompanyId: string = null;
  public sysRoleTable_Company_Name: string = null;
  public sysRoleTable_SiteLoginType: number = null;
}
