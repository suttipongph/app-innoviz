import { ViewCompanyBaseEntity } from 'shared/models/base';

export class BusinessCollateralTypeItemView extends ViewCompanyBaseEntity {
  public businessCollateralTypeGUID: string = null;
  public agreementOrdering: number = 0;
  public businessCollateralTypeId: string = null;
  public description: string = null;
}
