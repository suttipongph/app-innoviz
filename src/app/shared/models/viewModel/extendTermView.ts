import { ViewCompanyBaseEntity } from 'shared/models/base';

export class ExtendTermParamView extends ViewCompanyBaseEntity {
  public withdrawalTableGUID: string = null;
  public withdrawalId: string = null;
  public documentStatus: string = null;
  public documentStatusGUID: string = null;
  public customerId: string = null;
  public buyerId: string = null;
  public termExtension: boolean = false;
  public numberOfExtension: number = 0;
  public withdrawalDate: string = null;
  public dueDate: string = null;
  public creditTermId: string = null;
  public totalInterestPct: number = 0;
  public withdrawalAmount: number = 0;
  public outstanding: number = 0;
  public extendWithdrawalDate: string = null;
  public extendCreditTermGUID: string = null;
  public description: string = null;
  public companyGUID: string = null;
}
export class ExtendTermResultView extends ViewCompanyBaseEntity {
  public withdrawalTableGUID: string = null;
}