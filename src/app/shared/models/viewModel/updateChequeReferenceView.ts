import { ViewCompanyBaseEntity } from 'shared/models/base';
import { RefType } from 'shared/constants';

export class UpdateChequeReferenceView extends ViewCompanyBaseEntity {
  public updateChequeReferenceGUID: string = null;
  public chequeRefGUID: string = null;
  public listChequeRefType: RefType[] = null;
}
export class UpdateChequeReferenceResultView extends ViewCompanyBaseEntity {}
