import { ViewReportBaseEntity } from '../base/viewReportBaseEntity';

export class PrintPurchaseRollbillReportView extends ViewReportBaseEntity {
  public printPurchaseRollbillGUID: string = null;
  public additionalPurchase: boolean = false;
  public customerId: string = null;
  public documentStatus: string = null;
  public purchaseDate: string = null;
  public purchaseId: string = null;
  public rollbill: boolean = false;
}
