import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType } from 'shared/constants';

export class ExposureGroupByProductListView extends ViewCompanyBaseEntity {
  public exposureGroupByProductGUID: string = null;
  public exposureAmount: number = 0;
  public exposureGroupGUID: string = null;
  public exposureGroup_ExposureGroupId: string = null;
  public productType: ProductType = null;
}
