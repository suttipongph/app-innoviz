import { ViewCompanyBaseEntity } from '../base';

export class NCBAccountStatusItemView extends ViewCompanyBaseEntity {
  public ncbAccStatusId: string = null;
  public code: string = null;
  public description: string = null;
  public ncbAccountStatusGUID: string = null;
}
