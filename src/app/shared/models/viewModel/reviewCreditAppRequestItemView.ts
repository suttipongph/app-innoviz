import { ViewCompanyBaseEntity } from 'shared/models/base';
import { CreditAppRequestType } from 'shared/constants';

export class ReviewCreditAppRequestItemView extends ViewCompanyBaseEntity {
  public reviewCreditAppRequestGUID: string = null;
  public allCustomerBuyerOutstanding: number = 0;
  public approvedDate: string = null;
  public approverComment: string = null;
  public buyerCreditLimit: number = 0;
  public buyerId: string = null;
  public creditAppRequestId: string = null;
  public creditAppRequestType: CreditAppRequestType = null;
  public creditComment: string = null;
  public creditLimitLineRequest: number = 0;
  public customerBuyerOutstanding: number = 0;
  public customerId: string = null;
  public description: string = null;
  public documentReasonGUID: string = null;
  public documentRemark: string = null;
  public documentStatusGUID: string = null;
  public marketingComment: string = null;
  public refCreditAppLineGUID: string = null;
  public refCreditAppTableGUID: string = null;
  public requestDate: string = null;
}
