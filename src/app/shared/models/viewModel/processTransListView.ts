import { ViewCompanyBaseEntity } from 'shared/models/base';

export class ProcessTransListView extends ViewCompanyBaseEntity {
  public processTransGUID: string = null;
  public amount: number = 0;
  public creditAppTableGUID: string = null;
  public customerTableGUID: string = null;
  public processTransType: number = 0;
  public productType: number = 0;
  public revert: boolean = false;
  public stagingBatchId: string = null;
  public stagingBatchStatus: number = 0;
  public taxAmount: number = 0;
  public transDate: string = null;
  public customerTable_Values: string = null;
  public creditAppTable_Values: string = null;
  public currencyTable_Values: string = null;
  public reasonTable_Values: string = null;
  public refTexTable_Values: string = null;
  public texCode_Values: string = null;
  public originalTax_Values: string = null;
  public refProcessTrans_Values: string = null;
  public history_Values: string = null;
  public invoiceTableGUID: string = null;
  public invoiceTable_Values: string = null;
  public refType: string = null;
  public refId: string = null;
  public invoiceTable_InvoiceId: string = null;
  public creditAppTable_CreditappId: string = null;
  public customerTable_CustomerId: string = null;
  public refGUID: string = null;
}
