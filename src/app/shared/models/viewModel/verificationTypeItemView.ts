import { VerifyType } from 'shared/constants';
import { ViewCompanyBaseEntity } from 'shared/models/base';

export class VerificationTypeItemView extends ViewCompanyBaseEntity {
  public verificationTypeGUID: string = null;
  public description: string = null;
  public verificationTypeId: string = null;
  public verifyType: VerifyType = null;
}
