import { ViewCompanyBaseEntity } from 'shared/models/base';

export class SendVendorInvoiceStagingView extends ViewCompanyBaseEntity {
  public refGUID: string = null;
  public refType: number = 0;
  public documentId: string = null;
  public number: number = 0;
  public callerTableLabel: string = null;
}
