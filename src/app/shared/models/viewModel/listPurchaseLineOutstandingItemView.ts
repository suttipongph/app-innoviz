import { ViewCompanyBaseEntity } from 'shared/models/base';

export class ListPurchaseLineOutstandingItemView extends ViewCompanyBaseEntity {
  public listPurchaseLineOutstandingGUID: string = null;
  public billingDate: string = null;
  public buyerId: string = null;
  public buyerInvoiceId: string = null;
  public collectionDate: string = null;
  public creditAppTableBankAccountControlId: string = null;
  public creditAppTablePDCBankId: string = null;
  public customerId: string = null;
  public customerPDCDate: string = null;
  public dueDate: string = null;
  public invoiceAmount: number = 0;
  public methodOfPaymentId: string = null;
  public outstanding: number = 0;
  public purchaseAmount: number = 0;
  public purchaseId: string = null;
  public purchaseLineGUID: string = null;
}
