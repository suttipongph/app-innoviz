import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CreditAppReqBusCollaterlInfoItemView extends ViewCompanyBaseEntity {
  public creditAppReqBusCollaterlInfoGUID: string = null;
  public creditApplicationRequestId: string = null;
  public creditAppRequestTable_Values: string = null;
  public customerTable_Values: string = null;
}
