import { ViewBranchCompanyBaseEntity } from 'shared/models/base';
import { ProductType, ReceivedFrom, RefType } from 'shared/constants';

export class PaymentHistoryItemView extends ViewBranchCompanyBaseEntity {
  public paymentHistoryGUID: string = null;
  public cancel: boolean = null;
  public creditAppTableGUID: string = null;
  public currencyGUID: string = null;
  public customerTableGUID: string = null;
  public custTransGUID: string = null;
  public dueDate: string = null;
  public exchangeRate: number = 0;
  public invoiceTableGUID: string = null;
  public paymentAmount: number = 0;
  public paymentAmountMST: number = 0;
  public paymentBaseAmount: number = 0;
  public paymentBaseAmountMST: number = 0;
  public paymentDate: string = null;
  public paymentTaxAmount: number = 0;
  public paymentTaxAmountMST: number = 0;
  public PaymentTaxBaseAmount: number = 0;
  public PaymentTaxBaseAmountMST: number = 0;
  public productType: ProductType = null;
  public receiptTableGUID: string = null;
  public receivedFrom: ReceivedFrom = null;
  public receivedDate: string = null;
  public refGUID: string = null;
  public refID: string = null;
  public refType: RefType = null;
  public whtAmount: number = 0;
  public whtAmountMST: number = 0;
  public withholdingTaxTableGUID: string = null;
  public creditAppTable_Values: string = null;
  public currency_Values: string = null;

  public customerTable_Values: string = null;

  public invoiceTable_Values: string = null;

  public withholdingTaxTable_Values: string = null;
  public receiptTable_Values: string = null;
  public whtSlipReceivedByCustomer: boolean = false;
  public invoiceSettlementDetailGUID: string = null;
  public invoiceSettlementDetail_Values: string = null;
  public origTaxTableGUID: string = null;
  public origTaxTable_Values: string = null;
}
