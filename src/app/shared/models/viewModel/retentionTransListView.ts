import { ViewCompanyBaseEntity } from 'shared/models/base';
import { ProductType, RefType } from 'shared/constants';

export class RetentionTransListView extends ViewCompanyBaseEntity {
  public retentionTransGUID: string = null;
  public amount: number = 0;
  public buyerAgreementTableGUID: string = null;
  public buyerTableGUID: string = null;
  public creditAppTableGUID: string = null;
  public documentId: string = null;
  public productType: ProductType = null;
  public refType: RefType = null;
  public transDate: string = null;
  public creditAppTable_Values: string = null;
  public creditAppTable_CreditAppId: string = null;
  public buyerTable_Values: string = null;
  public buyerAgreementTable_Values: string = null;
  public refGUID: string;
  public buyerTable_BuyerId: string = null;
  public buyerAgreementTable_ReferenceAgreementID: string = null;
}
