import { ViewCompanyBaseEntity } from 'shared/models/base';

export class CopyDocumentConditionTemplateParamView extends ViewCompanyBaseEntity {
  public documentConditionTemplateTableGUID: string = null;
  public RefGUID: string = null;
  public RefType: number = 0;
  public DocConVerifyType: number = 0;
  public CallerLabel: string = null;
  public DocConVerifyTypeLabel: string = null;
}
export class CopyDocumentConditionTemplateResultView extends ViewCompanyBaseEntity {
  public documentConditionTemplateTableGUID: string = null;
  public RefGUID: string = null;
}
