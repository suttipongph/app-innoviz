import { ViewCompanyBaseEntity } from 'shared/models/base';
import { PaidToType, RefType } from 'shared/constants';

export class PaymentDetailListView extends ViewCompanyBaseEntity {
  public paymentDetailGUID: string = null;
  public customerTableGUID: string = null;
  public invoiceTypeGUID: string = null;
  public paidToType: PaidToType = null;
  public paymentAmount: number = 0;
  public suspenseTransfer: boolean = false;
  public vendorTableGUID: string = null;
  public refGUID: string = null;
  public invoiceType_InvoiceTypeId: string = null;
  public customerTable_CustomerId: string = null;
  public vendorTable_VendorId: string = null;
}
