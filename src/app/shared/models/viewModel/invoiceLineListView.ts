import { ViewBranchCompanyBaseEntity } from 'shared/models/base';

export class InvoiceLineListView extends ViewBranchCompanyBaseEntity {
  public invoiceTableGUID: string = null;
  public invoiceLineGUID: string = null;
  public invoiceRevenueTypeGUID: string = null;
  public invoiceText: string = null;
  public lineNum: number = 0;
  public taxAmount: number = 0;
  public taxTableGUID: string = null;
  public totalAmount: number = 0;
  public totalAmountBeforeTax: number = 0;
  public InvoiceRevenueType_RevenueTypeId: string = null;
  public InvoiceRevenueType_Values: string = null;
  public taxTable_taxTabled: string = null;
  public taxTable_Values: string = null;
}
