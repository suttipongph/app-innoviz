import { CompanySignatureRefType } from 'shared/constants';
import { ViewBranchCompanyBaseEntity } from '../base';

export class CompanySignatureListView extends ViewBranchCompanyBaseEntity {
  public companySignatureGUID: string = null;
  public refType: CompanySignatureRefType = null;
  public ordering: number = 0;
  public employeeTableGUID: string = null;
  public employeeTable_EmployeeId: string = null;
  public employeeTable_Name: string = null;
  public branchGUID: string = null;
  public branch_Values: string = null;
  public branch_BranchId: string = null;
}
