import { ViewCompanyBaseEntity } from 'shared/models/base';
import { WithdrawalLineType } from 'shared/constants';

export class WithdrawalLineItemView extends ViewCompanyBaseEntity {
  public lineNum: number = 0;
  public withdrawalLineGUID: string = null;
  public billingDate: string = null;
  public buyerPDCTableGUID: string = null;
  public closedForTermExtension: boolean = false;
  public collectionDate: string = null;
  public customerPDCAmount: number = 0;
  public customerPDCDate: string = null;
  public customerPDCTableGUID: string = null;
  public dimension1GUID: string = null;
  public dimension2GUID: string = null;
  public dimension3GUID: string = null;
  public dimension4GUID: string = null;
  public dimension5GUID: string = null;
  public dueDate: string = null;
  public interestAmount: number = 0;
  public interestDate: string = null;
  public interestDay: number = 0;
  public startDate: string = null;
  public withdrawalLineInvoiceTableGUID: string = null;
  public withdrawalLineType: WithdrawalLineType = null;
  public withdrawalTableGUID: string = null;
  public withdrawalTable_CustomerTableGUID: string = null;
  public withdrawalTable_BuyerTableGUID: string = null;
  public withdrawalTable_WithdrawalDate: string = null;
  public withdrawalTable_Values: string = null;
  public withdrawalTable_StatusId: string = null;
  public invoiceTable_Values: string = null;
}
