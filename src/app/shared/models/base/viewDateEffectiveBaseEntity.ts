import { AccessMode } from 'shared/constants';

export class ViewDateEffectiveBaseEntity {
  public createdBy: string = null;
  public createdDateTime: string = null;
  public modifiedBy: string = null;
  public modifiedDateTime: string = null;
  public companyGUID: string = null;
  public companyId: string = null;
  public effectiveFrom: string = null;
  public effectiveTo: string = null;
  public rowAuthorize?: AccessMode = AccessMode.noAccess;
  public branchId?: string = null;
  public owner: string = null;
  public ownerBusinessUnitGUID: string = null;
  public ownerBusinessUnitId: string = null;
  public rowVersion: object[] = null;
}
