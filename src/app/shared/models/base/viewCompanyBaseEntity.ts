import { AccessMode } from 'shared/constants';
import { AccessModeView } from '../viewModel';
export class ViewCompanyBaseEntity {
  public createdBy: string = null;
  public createdDateTime: string = null;
  public modifiedBy: string = null;
  public modifiedDateTime: string = null;
  public companyGUID: string = null;
  public companyId: string = null;
  public rowAuthorize?: AccessMode = AccessMode.noAccess;
  public guidStamp?: string = null;
  public owner: string = null;
  public ownerBusinessUnitGUID: string = null;
  public ownerBusinessUnitId: string = null;
  public accessModeView: AccessModeView = null;
  public rowVersion: object[] = null;
}
