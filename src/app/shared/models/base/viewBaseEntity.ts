import { AccessMode } from 'shared/constants';
export class ViewBaseEntity {
  public createdBy: string = null;
  public createdDateTime: string = null;
  public modifiedBy: string = null;
  public modifiedDateTime: string = null;
  public rowAuthorize?: AccessMode = AccessMode.noAccess;
  public rowVersion: object[] = null;
}
