export class ViewReportBaseEntity {
  public reportName: string = null;
  public reportFormat: string = null;
  public companyGUID: string = null;
  public servicePath: string = null;
  // public owner: string = null;
  // public ownerBusinessUnitGUID: string = null;
  // public parentChildBusinessUnitGUID: string[] = null;
}
