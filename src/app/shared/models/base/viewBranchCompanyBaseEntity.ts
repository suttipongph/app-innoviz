import { AccessMode } from 'shared/constants';
export class ViewBranchCompanyBaseEntity {
  public createdBy: string = null;
  public createdDateTime: string = null;
  public modifiedBy: string = null;
  public modifiedDateTime: string = null;
  public branchGUID: string = null;
  public companyGUID: string = null;
  public branchId: string = null;
  public companyId: string = null;
  public rowAuthorize?: AccessMode = AccessMode.noAccess;
  public owner: string = null;
  public ownerBusinessUnitGUID: string = null;
  public businessUnitId: string = null;
  public ownerBusinessUnitId: string = null;
  public rowVersion: object[] = null;
}
