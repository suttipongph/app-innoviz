import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormatConfigModel } from 'shared/models/systemModel';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { NumberMode } from 'shared/constants';

@Component({
  selector: 'hoc-inputnumber',
  templateUrl: './hoc-inputnumber.component.html',
  styleUrls: ['./hoc-inputnumber.component.scss']
})
export class HocInputnumberComponent implements OnInit {
  @Input() set ctModel(num: number) {
    if (isNullOrUndefined(num)) {
      this.modelNum = 0;
    } else {
      this.modelNum = num;
    }
  }
  get ctModel(): number {
    return this.modelNum;
  }
  @Input() set config(con: FormatConfigModel) {
    if (!isNullOrUndefined(con)) {
      this.modelConfig = con;
      this.setConfig();
    }
  }
  @Input() inputId: string;
  modelNum: number;
  suffix: string;
  modelConfig: FormatConfigModel;
  fractionDigits = 0;
  mode = 'decimal';
  min = 0;
  max = 9999999;
  currency = 'THB';
  locale = 'th-TH';
  grouping = false;
  @Output() ctModelChange = new EventEmitter<number>();

  val = 0;
  constructor() {}

  ngOnInit(): void {}
  setConfig(): void {
    this.mode = this.modelConfig.type;
    this.min = this.modelConfig.min;
    this.max = this.modelConfig.max;
    this.fractionDigits = this.modelConfig.fractionDigits;
    this.locale = this.modelConfig.locale;
    this.currency = this.modelConfig.currency;
    this.suffix = this.modelConfig.suffix;
    this.grouping = this.modelConfig.grouping;
    // if (this.modelConfig.type === NumberMode.DECIMAL) {

    // }
  }
}
