import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HocInputnumberComponent } from './hoc-inputnumber.component';

describe('HocInputnumberComponent', () => {
  let component: HocInputnumberComponent;
  let fixture: ComponentFixture<HocInputnumberComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HocInputnumberComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HocInputnumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
