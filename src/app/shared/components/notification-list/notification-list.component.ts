import { Component, OnInit, EventEmitter } from '@angular/core';
import { formatDate } from '@angular/common';
import { LoggingModel } from 'shared/models/systemModel';
import { isUndefinedOrZeroLength } from 'shared/functions/value.function';
import { AppConst, LogType } from 'shared/constants';
import { LoggingService } from 'core/services/logging.service';
import { NotificationService } from 'core/services/notification.service';
import { UserDataService } from 'core/services/user-data.service';

@Component({
  selector: 'app-notification-list',
  templateUrl: './notification-list.component.html',
  styles: [``]
})
export class NotificationListComponent implements OnInit {
  messages: LoggingModel[] = [];
  isUndefinedOrZeroLength = isUndefinedOrZeroLength;
  LogType = LogType;

  userEnabledSystemLog: boolean = false;
  showSystemLogLevel: string;
  isLogAvailable: boolean = false;

  currentDetailIndex: number = null;

  showSystemLogDetail = new EventEmitter<any>();

  constructor(private loggingService: LoggingService, private notificationService: NotificationService, private userDataService: UserDataService) {
    this.notificationService.showNotificationMsgsSubject.subscribe((value) => {
      if (value) {
        this.setNotiMsgsList();
      }
    });
  }

  ngOnInit() {}
  setNotiMsgsList() {
    this.userEnabledSystemLog = this.userDataService.getShowSystemLog() === AppConst.TRUE;

    let notilist: LoggingModel[] = this.loggingService.getAppLogs();
    if (!isUndefinedOrZeroLength(notilist)) {
      notilist.sort(function (a, b) {
        let datetimeA: string = a.timestamp;
        let datetimeB: string = b.timestamp;
        let arrA: string[] = datetimeA.split(' ');
        let arrB: string[] = datetimeB.split(' ');
        let dateA = arrA[0].split('-');
        let dateB = arrB[0].split('-');
        let timeA = arrA[1].split(':');
        let timeB = arrB[1].split(':');
        let dateObjA = new Date(
          parseInt(dateA[0]),
          parseInt(dateA[1]),
          parseInt(dateA[2]),
          parseInt(timeA[0]),
          parseInt(timeA[1]),
          parseInt(timeA[2])
        );
        let dateObjB = new Date(
          parseInt(dateB[0]),
          parseInt(dateB[1]),
          parseInt(dateB[2]),
          parseInt(timeB[0]),
          parseInt(timeB[1]),
          parseInt(timeB[2])
        );

        // dsc
        return dateObjA > dateObjB ? -1 : dateObjA < dateObjB ? 1 : 0;
      });

      if (this.userEnabledSystemLog) {
        this.isLogAvailable = true;
      } else {
        this.isLogAvailable = notilist.some((item) => item.logType === LogType.NORMAL);
      }
    } else {
      this.isLogAvailable = false;
    }

    this.messages = notilist;
  }
  onToggleDetail(e, index) {
    if (!isUndefinedOrZeroLength(this.currentDetailIndex)) {
      if (this.currentDetailIndex === index) {
        let currentVal = this.messages[index].systemLogDetail.displayDetail;
        this.messages[this.currentDetailIndex].systemLogDetail.displayDetail = !currentVal;
        this.showSystemLogDetail.emit(!currentVal);
      }
    }
    if (
      isUndefinedOrZeroLength(this.currentDetailIndex) ||
      (!isUndefinedOrZeroLength(this.messages[index].systemLogDetail) && this.currentDetailIndex !== index)
    ) {
      const startTime = this.messages[index].systemLogDetail.startTime;
      const finishTime = this.messages[index].systemLogDetail.finishTime;
      this.messages[index].systemLogDetail.displayStartTime = formatDate(startTime, 'yyyy-MM-dd HH:mm:ss', 'EN-US');
      this.messages[index].systemLogDetail.displayFinishTime = formatDate(finishTime, 'yyyy-MM-dd HH:mm:ss', 'EN-US');
      this.messages[index].systemLogDetail.displayDetail = true;

      if (!isUndefinedOrZeroLength(this.currentDetailIndex)) {
        this.messages[this.currentDetailIndex].systemLogDetail.displayDetail = false;
      }

      this.showSystemLogDetail.emit(true);
    }

    this.currentDetailIndex = index;
  }
}
