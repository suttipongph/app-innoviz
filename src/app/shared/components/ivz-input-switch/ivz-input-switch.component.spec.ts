import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IvzInputSwitchComponent } from './ivz-input-switch.component';

describe('IvzInputSwitchComponent', () => {
  let component: IvzInputSwitchComponent;
  let fixture: ComponentFixture<IvzInputSwitchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IvzInputSwitchComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IvzInputSwitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
