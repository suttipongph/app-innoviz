import { Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { ColumnType } from '../../constants';
import { Guid, isNullOrUndefined } from '../../functions/value.function';
import { InputSwitchConfigModel } from '../../models/systemModel/ivzComponentModel';
import { IvzBaseComponent } from 'shared/components/ivz-base/ivz-base.component';
import { interval } from 'rxjs';

@Component({
  selector: 'ivz-input-switch',
  templateUrl: './ivz-input-switch.component.html',
  styleUrls: ['./ivz-input-switch.component.scss']
})
export class IvzInputSwitchComponent extends IvzBaseComponent implements OnInit {
  bindingModel: boolean;
  @Input() set ivzModel(param: boolean) {
    this.bindingModel = param;
    if (this.bindingModel !== undefined && param !== undefined) {
      this.getBindingEmitter(param);
    }
  }
  get ivzModel(): boolean {
    return this.bindingModel;
  }

  bindingConfig: InputSwitchConfigModel;
  @Input() set ivzConfig(param: InputSwitchConfigModel) {
    this.bindingConfig = isNullOrUndefined(param) ? new InputSwitchConfigModel() : param;
    this.bindingInputId = isNullOrUndefined(this.bindingInputId) ? Guid.newGuid() : this.bindingInputId;
  }

  @Output() ivzModelChange = new EventEmitter<boolean>();
  @Output() ivzOnChange = new EventEmitter<boolean>();
  @Output() ivzBindingChange = new EventEmitter<any>();
  constructor(el: ElementRef, renderer: Renderer2) {
    super(el, renderer);
  }

  ngOnInit(): void {
    super.setGetwayFieldType(this.bindingInputId, ColumnType.BOOLEAN);
  }
  private getBindingEmitter(param: any): void {
    const $interval = interval(500).subscribe((val) => {
      this.ivzBindingChange.next(param);
      $interval.unsubscribe();
    });
  }
}
