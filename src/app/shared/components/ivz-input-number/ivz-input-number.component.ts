import { Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { AppConst, ColumnType } from 'shared/constants';
import { Guid, isNullOrUndefined, isNullOrUndefOrEmpty } from 'shared/functions/value.function';
import { TranslateModel } from 'shared/models/systemModel';
import { InputNumberConfigModel } from 'shared/models/systemModel';
import { IvzBaseComponent } from 'shared/components/ivz-base/ivz-base.component';

@Component({
  selector: 'ivz-input-number',
  templateUrl: './ivz-input-number.component.html',
  styleUrls: ['./ivz-input-number.component.scss']
})
export class IvzInputNumberComponent extends IvzBaseComponent implements OnInit {
  //#region variable
  bindingModel: number;
  bindingValidation: TranslateModel;
  bindingConfig: InputNumberConfigModel;
  bindingMode: string;
  bindingMax: number;
  bindingMin: number;
  bindingMinFractionDigits: number;
  bindingMaxFractionDigits: number;
  bindingUseGrouping: boolean;
  bindingSuffix: string;
  bindingDisabled: boolean;
  bindingIgnoreDefault: boolean = false;
  bindingPlaceholder: string;
  //#endregion
  //#region input props

  @Input() set ignoreDefault(param: boolean) {
    this.bindingIgnoreDefault = param;
  }
  @Input() set placeholder(param: string) {
    this.bindingPlaceholder = isNullOrUndefOrEmpty(param) ? null : param;
  }
  @Input() set ivzModel(param: number) {
    this.bindingModel = isNullOrUndefOrEmpty(param) ? (this.bindingIgnoreDefault ? null : 0) : param;
    this.validateModel(this.bindingModel, this.bindingValidation);
  }
  get ivzModel(): number {
    return this.bindingModel;
  }

  @Input() set ivzValidation(param: TranslateModel) {
    this.bindingValidation = param;
    this.validationNotification(param);
  }
  @Input() set ivzConfig(param: InputNumberConfigModel) {
    this.bindingConfig = isNullOrUndefined(param) ? new InputNumberConfigModel() : param;
    this.bindingInputId = isNullOrUndefined(this.bindingInputId) ? Guid.newGuid() : this.bindingInputId;
    this.bindingMode = isNullOrUndefined(this.bindingConfig.mode) ? this.bindingMode : this.bindingConfig.mode;
    this.bindingMax = isNullOrUndefined(this.bindingConfig.max) ? this.bindingMax : this.bindingConfig.max;
    this.bindingMin = isNullOrUndefined(this.bindingConfig.min) ? this.bindingMin : this.bindingConfig.min;
    this.bindingMaxFractionDigits = isNullOrUndefined(this.bindingConfig.fractionDigits)
      ? this.bindingMaxFractionDigits
      : this.bindingConfig.fractionDigits;
    this.bindingMinFractionDigits = isNullOrUndefined(this.bindingConfig.fractionDigits)
      ? this.bindingMinFractionDigits
      : this.bindingConfig.fractionDigits;
    this.bindingUseGrouping = isNullOrUndefined(this.bindingConfig.grouping) ? this.bindingUseGrouping : this.bindingConfig.grouping;
    this.bindingSuffix = isNullOrUndefined(this.bindingConfig.suffix) ? this.bindingSuffix : this.bindingConfig.suffix;
  }
  @Input() set mode(param: string) {
    this.bindingMode = isNullOrUndefined(param) ? this.bindingConfig.mode : param;
  }
  @Input() set max(param: number) {
    this.bindingMax = isNullOrUndefined(param) ? this.bindingConfig.max : param;
  }
  @Input() set min(param: number) {
    this.bindingMin = isNullOrUndefined(param) ? this.bindingConfig.min : param;
  }
  @Input() set minFractionDigits(param: number) {
    this.bindingMinFractionDigits = isNullOrUndefined(param) ? this.bindingConfig.fractionDigits : param;
  }
  @Input() set maxFractionDigits(param: number) {
    this.bindingMaxFractionDigits = isNullOrUndefined(param) ? this.bindingConfig.fractionDigits : param;
  }
  @Input() set useGrouping(param: boolean) {
    this.bindingUseGrouping = isNullOrUndefined(param) ? this.bindingConfig.grouping : param;
  }
  @Input() set suffix(param: string) {
    this.bindingSuffix = isNullOrUndefined(param) ? this.bindingConfig.suffix : param;
  }
  @Input() set disabled(param: boolean) {
    this.bindingDisabled = isNullOrUndefined(param) ? false : param;
  }
  //#endregion
  //#region output props
  @Output() ivzModelChange = new EventEmitter<number>();
  //#endregion
  constructor(el: ElementRef, renderer: Renderer2) {
    super(el, renderer);
    this.$subscripion.subscribe((data) => {
      if (!AppConst.SYSTEM_FIELD.some((s) => s === this.bindingInputId)) {
        this.validateModel(this.ivzModel, this.bindingValidation, true);
      }
    });
  }

  ngOnInit(): void {
    super.setGetwayFieldType(this.bindingInputId, ColumnType.INT);
  }
  isEmitModelChange(): void {
    if (!this.getReadonly(this.bindingInputId)) this.ivzModelChange.emit(this.ivzModel);
  }
}
