import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IvzInputNumberComponent } from './ivz-input-number.component';

describe('IvzInputNumberComponent', () => {
  let component: IvzInputNumberComponent;
  let fixture: ComponentFixture<IvzInputNumberComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IvzInputNumberComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IvzInputNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
