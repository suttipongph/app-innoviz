import { Component, ElementRef, EventEmitter, Input, Output, Renderer2 } from '@angular/core';
import { Guid, isNullOrUndefined, isUndefinedOrZeroLength, replaceAll, switchClass } from 'shared/functions/value.function';
import { ButtonConfigModel } from 'shared/models/systemModel';
import { IvzBaseComponent } from '../ivz-base/ivz-base.component';
import { IvzButtonComponent } from '../ivz-button/ivz-button.component';

@Component({
  selector: 'ivz-multi-state-button',
  templateUrl: './ivz-multi-state-button.component.html',
  styleUrls: ['./ivz-multi-state-button.component.scss']
})
export class IvzMultiStateButtonComponent extends IvzBaseComponent {
  //#region variable
  bindingDisabled: boolean;
  defaultConfig: ButtonConfigModel = new ButtonConfigModel();
  bindingMaxStateSize: number = 1;
  bindingStateStyles: ButtonConfigModel[] = [];
  currentState: number = 0;
  currentStateClass: string = 'ui-button-info ui-button-rounded';
  currentStateIcon: string;
  currentStateIconPos: string = 'left';
  currentStateLabel: string = '.';
  ngClassObj: any;
  bindingModel: number;
  //#endregion

  //#region input props
  @Input() set ivzModel(param: number) {
    this.bindingModel = isNullOrUndefined(param) ? 0 : param;
    this.currentState = this.bindingModel;

    if (!isUndefinedOrZeroLength(this.bindingStateStyles) && this.bindingModel > 0 && this.bindingModel <= this.bindingMaxStateSize) {
      this.onStateChange(this.currentState);
    }
  }
  get ivzModel(): number {
    return this.bindingModel;
  }
  @Input() set disabled(param: boolean) {
    this.bindingDisabled = isNullOrUndefined(param) ? false : param;
    if (this.bindingDisabled) {
      switchClass(this.el.nativeElement, 'no-click', this.bindingDisabled);
    } else {
      switchClass(this.el.nativeElement, 'no-click', false);
    }
  }

  @Input() set maxStateSize(size: number) {
    if (!isUndefinedOrZeroLength(size)) {
      this.bindingMaxStateSize = size;
    }
  }
  @Input() set stateStyles(styles: ButtonConfigModel[]) {
    this.bindingStateStyles = styles;
    if (!isUndefinedOrZeroLength(this.bindingStateStyles)) {
      const currentSlot = this.bindingStateStyles[this.currentState];
      this.setCurrentStateStyles(currentSlot);
    }
  }
  //#endregion

  //#region output props
  @Output() ivzModelChange = new EventEmitter<number>();
  //#endregion
  constructor(el: ElementRef, renderer: Renderer2) {
    super(el, renderer);
  }
  ngOnInit(): void {}
  onClick($event): void {
    this.onStateChange(this.currentState + 1);
  }
  onStateChange(e: number): void {
    this.removeButtonClasses();

    this.currentState = e % this.bindingMaxStateSize;

    this.bindingModel = this.currentState;
    const currentStateSlot = this.bindingStateStyles[this.currentState];
    this.setCurrentStateStyles(currentStateSlot);
    this.addButtonClasses();
    this.ivzModelChange.emit(this.bindingModel);
  }
  setCurrentStateStyles(currentSlot: ButtonConfigModel): void {
    this.currentStateClass =
      !isUndefinedOrZeroLength(currentSlot) && !isUndefinedOrZeroLength(currentSlot.className)
        ? currentSlot.className
        : 'ui-button-info ui-button-rounded';
    this.currentStateIcon = !isUndefinedOrZeroLength(currentSlot) && !isUndefinedOrZeroLength(currentSlot.icon) ? currentSlot.icon : undefined;
    this.currentStateIconPos = !isUndefinedOrZeroLength(currentSlot) && !isUndefinedOrZeroLength(currentSlot.iconPos) ? currentSlot.iconPos : 'left';
    this.currentStateLabel = !isUndefinedOrZeroLength(currentSlot) ? currentSlot.label : '';
    this.setNgClass(this.currentState);
  }
  removeButtonClasses(): void {
    // this.removeIconClasses();
    const classes = this.currentStateClass.split(' ');
    while (classes.length > 0) {
      const pop = classes.pop();
      switchClass(this.el.nativeElement, pop, false);
      this.el.nativeElement.classList['value'] = this.el.nativeElement.classList['value'].replace(pop, '').trim();
    }
  }
  removeIconClasses(): void {
    const ele = this.el.nativeElement.querySelectorAll(`span.p-button-icon`)[0];
    const currentIconPos = `p-putton-icon-${this.currentStateIconPos}`;
    switchClass(ele, currentIconPos, false);
    ele.classList['value'] = ele.classList['value'].replace(currentIconPos, '').trim();

    const classes = this.currentStateIcon.split(' ');
    while (classes.length > 0) {
      const pop = classes.pop();
      switchClass(ele, pop, false);
      ele.classList['value'] = ele.classList['value'].replace(pop, '').trim();
    }
  }
  addIconClasses(): void {
    const ele = this.el.nativeElement.querySelectorAll(`span.p-button-icon`)[0];
    const currentIconPos = `p-putton-icon-${this.currentStateIconPos}`;
    switchClass(ele, currentIconPos, true);
    const classes = this.currentStateIcon.split(' ');
    while (classes.length > 0) {
      const pop = classes.pop();
      switchClass(ele, pop, true);
    }
  }

  addButtonClasses(): void {
    const classes = this.currentStateClass.split(' ');
    while (classes.length > 0) {
      const pop = classes.pop();
      switchClass(this.el.nativeElement, pop, true);
    }
    //this.addIconClasses();
  }
  setNgClass(state: number): any {
    if (!isUndefinedOrZeroLength(this.bindingStateStyles)) {
      const result = {};
      for (let i = 0; i < this.bindingMaxStateSize; i++) {
        const states = this.bindingStateStyles[i].className.split(' ');
        for (let j = 0; j < states.length; j++) {
          result[states[j]] = false;
        }
      }
      if (!isUndefinedOrZeroLength(this.bindingStateStyles[state]) && !isUndefinedOrZeroLength(this.bindingStateStyles[state])) {
        const currentStateStyle = this.bindingStateStyles[state].className.split(' ');
        for (let i = 0; i < currentStateStyle.length; i++) {
          result[currentStateStyle[i]] = true;
        }
      }
      this.ngClassObj = result;
      return result;
    } else {
      return {};
    }
  }
  setIcon(state: number): string {
    return this.currentStateIcon;
  }
}
