import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IvzMultiStateButtonComponent } from './ivz-multi-state-button.component';

describe('IvzMultiStateButtonComponent', () => {
  let component: IvzMultiStateButtonComponent;
  let fixture: ComponentFixture<IvzMultiStateButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IvzMultiStateButtonComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IvzMultiStateButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
