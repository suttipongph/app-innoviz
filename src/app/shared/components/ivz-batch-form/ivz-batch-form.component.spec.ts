import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IvzBatchFormComponent } from './ivz-batch-form.component';

describe('IvzBatchFormComponent', () => {
  let component: IvzBatchFormComponent;
  let fixture: ComponentFixture<IvzBatchFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IvzBatchFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IvzBatchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
