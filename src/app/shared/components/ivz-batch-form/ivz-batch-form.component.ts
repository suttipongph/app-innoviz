import { formatDate } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BatchService } from 'core/services/batch.service';
import { forkJoin, Observable, of } from 'rxjs';
import { BatchEndDateType, BatchIntervalType, BATCH_DAYS, BATCH_HOURS, BATCH_MINUTES, BATCH_MONTHS, BATCH_YEARS } from 'shared/constants';
import { datetoString, stringToDate } from 'shared/functions/date.function';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUndefinedOrZeroLength } from 'shared/functions/value.function';
import {
  BatchFormConfigModel,
  FieldAccessing,
  FormValidationModel,
  PageInformationModel,
  SelectItems,
  TranslateModel
} from 'shared/models/systemModel';
import { BatchObjectView, DateTimeInput, EndDateModel, RecurringPatternModel } from 'shared/models/viewModel';

@Component({
  selector: 'ivz-batch-form',
  templateUrl: './ivz-batch-form.component.html',
  styleUrls: ['./ivz-batch-form.component.scss']
})
export class IvzBatchFormComponent extends BaseItemComponent<BatchObjectView> {
  BatchIntervalType = BatchIntervalType;
  BatchEndDateType = BatchEndDateType;
  dayOfWeekOption: SelectItems[] = [];
  intervalTypeOption: SelectItems[] = [];
  intervalValueOption: SelectItems[] = [];
  minuteOption: SelectItems[] = [];
  hourOption: SelectItems[] = [];
  dayOption: SelectItems[] = [];
  monthOption: SelectItems[] = [];
  yearOption: SelectItems[] = [];

  //#region binding fields
  bindingConfig: BatchFormConfigModel;
  bindingDefaultDescription: string;
  bindingDefaultIsBatch: boolean;
  bindingRunOnce: boolean;
  //#endregion
  model: BatchObjectView = new BatchObjectView();
  startDateTimeInput: DateTimeInput = new DateTimeInput();
  endDateTimeInput: EndDateModel = new EndDateModel();
  recurringPatternInput: RecurringPatternModel = new RecurringPatternModel();
  singleIntervalValue: number = 0;
  multipleIntervalValues: number[] = [];

  // visible/hide flags validate tooltip at field container
  validateRecurringCountFlag: boolean = true;
  validateRecurringTypeFlag: boolean = true;
  validateEndDateTypeFlag: boolean = true;
  validateEndDateFlag: boolean = true;
  validateIntervalCountFlag: boolean = true;

  //#region @Input
  @Input() set parameters(param: any) {
    if (!isUndefinedOrZeroLength(param)) {
      if (!isUndefinedOrZeroLength(param['batchApiFunctionMappingKey'])) {
        this.setParamForSubmit(param);
      }
    }
  }
  @Input() set ivzConfig(param: BatchFormConfigModel) {
    this.bindingConfig = isNullOrUndefined(param) ? new BatchFormConfigModel() : param;
    this.bindingRunOnce = isNullOrUndefined(this.bindingConfig.runOnce) ? this.bindingRunOnce : this.bindingConfig.runOnce;
    this.bindingDefaultDescription = isNullOrUndefined(this.bindingConfig.defaultDescription)
      ? this.bindingDefaultDescription
      : this.bindingConfig.defaultDescription;
    this.bindingDefaultIsBatch = isNullOrUndefined(this.bindingConfig.defaultIsBatch)
      ? this.bindingDefaultIsBatch
      : this.bindingConfig.defaultIsBatch;
  }
  @Input() set defaultDescription(param: string) {
    this.bindingDefaultDescription = isNullOrUndefined(param) ? this.bindingConfig.defaultDescription : param;
  }
  @Input() set defaultIsBatch(param: boolean) {
    this.bindingDefaultIsBatch = isNullOrUndefined(param) ? this.bindingConfig.defaultIsBatch : param;
  }
  @Input() set runOnce(param: boolean) {
    this.bindingRunOnce = isNullOrUndefined(param) ? this.bindingConfig.runOnce : param;
  }
  //#endregion
  //#region @Output
  @Output() batchSuccess = new EventEmitter<boolean>();
  //#endregion
  //#region field accessing
  group01: string[] = [
    'BATCH_TASK_DESCRIPTION',
    'BATCH_UNIT',
    'BATCH_REPEAT_EVERY',
    'BATCH_START_DATE',
    'BATCH_START_TIME',
    'BATCH_END_AFTER',
    'BATCH_COUNT',
    'BATCH_END_BY',
    'BATCH_END_DATE',
    'BATCH_NO_END_DATE'
  ];
  group02: string[] = ['BATCH_COUNT'];
  group03: string[] = ['BATCH_END_DATE'];
  group04: string[] = [
    'BATCH_TASK_DESCRIPTION',
    'BATCH_UNIT',
    'BATCH_REPEAT_EVERY',
    'BATCH_END_AFTER',
    'BATCH_COUNT',
    'BATCH_END_BY',
    'BATCH_END_DATE',
    'BATCH_NO_END_DATE'
  ];
  group05: string[] = ['BATCH_START_DATE', 'BATCH_START_TIME'];
  group06: string[] = ['BATCH_END_AFTER', 'BATCH_COUNT'];
  group07: string[] = ['BATCH_IS_BATCH'];
  //#endregion
  constructor(private batchService: BatchService, private currentActivatedRoute: ActivatedRoute) {
    super();
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.onEnumLoader();
    this.getInitialData();
  }
  onEnumLoader(): void {
    this.baseDropdown.getBatchIntervalTypeEnumDropDown(this.intervalTypeOption);
    this.baseDropdown.getBatchDayOfWeekEnumDropDown(this.dayOfWeekOption);
    this.minuteOption = BATCH_MINUTES;
    this.hourOption = BATCH_HOURS;
    this.dayOption = BATCH_DAYS;
    this.monthOption = BATCH_MONTHS;
    this.yearOption = BATCH_YEARS;

    this.intervalValueOption = this.dayOption;
  }
  getInitialData(): void {
    this.model.isBatch = this.bindingDefaultIsBatch;
    this.endDateTimeInput.endDateOption = BatchEndDateType.RECURRING;
    this.endDateTimeInput.recurringCount = 1;
    this.recurringPatternInput.intervalType = BatchIntervalType.DAYS;

    this.singleIntervalValue = this.intervalValueOption[0]['value'];
    this.multipleIntervalValues = [];

    const date: Date = new Date();
    this.startDateTimeInput.dateInput = datetoString(date);
    this.startDateTimeInput.timeInput = formatDate(date, 'HH:mm', 'EN-US');

    this.model.taskDescription = this.bindingDefaultDescription;
    this.setDefaultFieldAccessing();
  }
  setDefaultFieldAccessing(): void {
    this.isView = false;
    const fieldAccessing: FieldAccessing[] = this.getDefaultFieldAccessing();
    super.setBaseFieldAccessing(fieldAccessing);
  }
  // onSave(validation: FormValidationModel): void {}
  // onSaveAndClose(validation: FormValidationModel): void {
  onSubmitBatch(): void {
    const intervalType = this.model.recurringPatternModel.intervalType;
    if (intervalType === BatchIntervalType.MONTHS && !this.model.isEndOfMonth) {
      const februaryCondition = this.getBatchFebCondition();
      if (februaryCondition) {
        const header: TranslateModel = { code: 'Confirm: Submit Batch' };
        const message: TranslateModel = { code: 'Batch will not run in February with current criteria. Are you sure?' };
        this.notificationService.showManualConfirmDialog(header, message);
        this.notificationService.isAccept.subscribe((isConfirm) => {
          if (isConfirm) {
            this.onSubmitPreparedParams();
          }
          this.notificationService.isAccept.observers = [];
        });
      } else {
        this.onSubmitPreparedParams();
      }
    } else {
      this.onSubmitPreparedParams();
    }
  }
  onSubmitPreparedParams(): void {
    this.getDataValidation().subscribe((res) => {
      if (res) {
        this.onSubmit(true, this.model);
      }
    });
  }
  onSubmit(isColsing: boolean, model: BatchObjectView): void {
    // super.onExecuteFunction(this.batchService.createScheduledJob(model));
    this.batchService.createScheduledJob(model).subscribe(
      (res) => {
        this.batchSuccess.emit(true);
      },
      (error) => {
        this.batchSuccess.emit(false);
      }
    );
  }
  onClose(): void {}
  getDataValidation(): Observable<boolean> {
    // const formValid = this.getFormValidation();
    // return of(formValid.invalidId.length === 0);
    return of(true);
  }

  //#region set values ========================================
  setParamForSubmit(param: any): void {
    if (!isUndefinedOrZeroLength(this.multipleIntervalValues)) {
      this.multipleIntervalValues.sort((a, b) => {
        return a - b;
      });
      this.recurringPatternInput.intervalCount = this.multipleIntervalValues.join(',');
    } else {
      this.recurringPatternInput.intervalCount = Number(this.singleIntervalValue).toString();
    }

    this.model.batchApiFunctionMappingKey = param['batchApiFunctionMappingKey'];
    this.model.companyGUID = param['companyGUID'];

    this.model.parameters = this.getBatchParameters(param);

    if (!isUndefinedOrZeroLength(this.startDateTimeInput) && !isUndefinedOrZeroLength(this.startDateTimeInput.timeInput)) {
      const splitTime = this.startDateTimeInput.timeInput.split(':');
      const hrStr = isUndefinedOrZeroLength(splitTime[0])
        ? '00'
        : Number(splitTime[0]).toString().length === 1
        ? `0${Number(splitTime[0])}`
        : splitTime[0];
      const minStr = isUndefinedOrZeroLength(splitTime[1])
        ? '00'
        : Number(splitTime[1]).toString().length === 1
        ? `0${Number(splitTime[1])}`
        : splitTime[1];
      const secStr = isUndefinedOrZeroLength(splitTime[2])
        ? '00'
        : Number(splitTime[2]).toString().length === 1
        ? `0${Number(splitTime[2])}`
        : splitTime[2];
      const timeStr = `${hrStr}:${minStr}:${secStr}`;
      this.startDateTimeInput.timeInput = timeStr;
    }

    this.model.startDateTime = this.startDateTimeInput;
    this.model.endDateModel = this.endDateTimeInput;
    this.model.recurringPatternModel = this.recurringPatternInput;

    this.batchService.setPathFromPathString(param['servicePath']);

    this.onSubmitBatch();
  }
  setEndDateOptionUI(e: BatchEndDateType): void {
    switch (e) {
      case BatchEndDateType.NO_ENDDATE:
        this.endDateTimeInput.recurringCount = 1;
        this.endDateTimeInput.endBy = null;
        break;

      case BatchEndDateType.RECURRING:
        this.endDateTimeInput.endBy = null;
        break;

      case BatchEndDateType.END_BY:
        this.endDateTimeInput.recurringCount = 1;
        break;
    }
    this.setFieldAccessingBatchEndDateOption(e);
  }

  setIntervalValueOptionUI(e: BatchIntervalType): void {
    switch (e) {
      case BatchIntervalType.MINUTES:
        this.setDefaultOptionForNotDayOfWeekType(this.minuteOption);
        break;
      case BatchIntervalType.HOURS:
        this.setDefaultOptionForNotDayOfWeekType(this.hourOption);
        break;
      case BatchIntervalType.DAYS:
        this.setDefaultOptionForNotDayOfWeekType(this.dayOption);
        break;
      case BatchIntervalType.MONTHS:
        this.setDefaultOptionForNotDayOfWeekType(this.monthOption);
        break;
      case BatchIntervalType.DAYOFWEEK:
        this.intervalValueOption = this.dayOfWeekOption;
        this.endDateTimeInput.endDateOption = BatchEndDateType.END_BY;
        this.setEndDateOptionUI(this.endDateTimeInput.endDateOption);
        break;
      case BatchIntervalType.YEARS:
        this.setDefaultOptionForNotDayOfWeekType(this.yearOption);
        break;
      default:
        this.intervalValueOption = [];
        break;
    }
    if (!isNullOrUndefined(e) && e !== BatchIntervalType.DAYOFWEEK) {
      this.singleIntervalValue = this.intervalValueOption[0]['value'];
    }
    this.multipleIntervalValues = [];
    this.model.isEndOfMonth = false;

    this.setFieldAccessingBatchIntervalType(e);
  }

  private setDefaultOptionForNotDayOfWeekType(intervalValueOption: SelectItems[]): void {
    this.intervalValueOption = intervalValueOption;
    this.endDateTimeInput.endDateOption = BatchEndDateType.RECURRING;
    this.setEndDateOptionUI(this.endDateTimeInput.endDateOption);
  }
  getBatchParameters(param: any): string {
    if (!isUndefinedOrZeroLength(param)) {
      const keys = Object.keys(param);
      if (!isUndefinedOrZeroLength(keys)) {
        const p = {};
        let hasCompany: boolean;
        keys.forEach((f) => {
          if (f === 'hasCompanyGUID') {
            hasCompany = !isUndefinedOrZeroLength(param['hasCompanyGUID']) ? (param['hasCompanyGUID'] as boolean) : false;
          }
          if (f !== 'batchApiFunctionMappingKey' && f !== 'hasCompanyGUID' && f !== 'servicePath' && f !== 'companyGUID') {
            const value = param[f];
            if (value !== undefined) {
              p[f] = value;
            }
          }
        });
        if (hasCompany === true) {
          p['companyGUID'] = param['companyGUID'];
        }
        return JSON.stringify(p);
      }
    }
    return '';
  }
  private getBatchFebCondition(): boolean {
    const startDate: Date = stringToDate(this.model.startDateTime.dateInput);
    const month = startDate.getMonth();
    const date = startDate.getDate();
    return (
      (date === 29 && month !== 1) ||
      (date === 30 && (month === 0 || month === 2 || month === 4 || month === 6 || month === 7 || month === 9 || month === 11))
    );
  }

  //#endregion
  //#region get/set field FieldAccessing ========================================
  getDefaultFieldAccessing(): FieldAccessing[] {
    const fieldAccessing: FieldAccessing[] = [];
    if (!this.bindingDefaultIsBatch) {
      fieldAccessing.push({ filedIds: this.group01, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: this.group01, readonly: false });
      fieldAccessing.push({ filedIds: this.group07, readonly: true });
    }

    if (this.bindingRunOnce && this.bindingDefaultIsBatch) {
      fieldAccessing.push({ filedIds: this.group04, readonly: true });
      fieldAccessing.push({ filedIds: this.group05, readonly: false });
    } else {
      fieldAccessing.push(...this.getFieldAccessingBatchEndDateOption(this.endDateTimeInput.endDateOption));
    }
    return fieldAccessing;
  }
  getFieldAccessingBatchEndDateOption(value: BatchEndDateType): FieldAccessing[] {
    switch (value) {
      case BatchEndDateType.NO_ENDDATE:
        return this.getFieldAccessingBatchNoEndDate();

      case BatchEndDateType.RECURRING:
        return this.getFieldAccessingBatchRecurring();

      case BatchEndDateType.END_BY:
        return this.getFieldAccessingBatchEndBy();
      default:
        return [];
    }
  }
  getFieldAccessingBatchNoEndDate(): FieldAccessing[] {
    return [
      { filedIds: this.group02, readonly: true },
      { filedIds: this.group03, readonly: true }
    ];
  }
  getFieldAccessingBatchRecurring(): FieldAccessing[] {
    return [
      { filedIds: this.group02, readonly: false },
      { filedIds: this.group03, readonly: true }
    ];
  }
  getFieldAccessingBatchEndBy(): FieldAccessing[] {
    return [
      { filedIds: this.group02, readonly: true },
      { filedIds: this.group03, readonly: false }
    ];
  }
  getFieldAccessingBatchIntervalType(val: BatchIntervalType): FieldAccessing[] {
    switch (val) {
      case BatchIntervalType.MINUTES:
      case BatchIntervalType.HOURS:
      case BatchIntervalType.DAYS:
      case BatchIntervalType.MONTHS:
      case BatchIntervalType.YEARS:
        return [{ filedIds: this.group06, readonly: false }];

      case BatchIntervalType.DAYOFWEEK:
        return [{ filedIds: this.group06, readonly: true }];

      default:
        return [];
    }
  }
  getFieldAccessingIsBatchChange(isBatch: boolean): FieldAccessing[] {
    const fieldAccessing: FieldAccessing[] = [];
    if (isBatch) {
      if (this.bindingRunOnce) {
        fieldAccessing.push({ filedIds: this.group01, readonly: false });
        fieldAccessing.push({ filedIds: this.group04, readonly: true });
        fieldAccessing.push({ filedIds: this.group05, readonly: false });
      } else {
        fieldAccessing.push({ filedIds: this.group01, readonly: false });
      }
    } else {
      fieldAccessing.push({ filedIds: this.group01, readonly: true });
    }
    return fieldAccessing;
  }
  setFieldAccessingBatchEndDateOption(e: BatchEndDateType): void {
    const fieldAccessing: FieldAccessing[] = this.getFieldAccessingBatchEndDateOption(e);
    super.setBaseFieldAccessing(fieldAccessing);
  }
  setFieldAccessingBatchIntervalType(e: BatchIntervalType): void {
    const fieldAccessing: FieldAccessing[] = this.getFieldAccessingBatchIntervalType(e);
    super.setBaseFieldAccessing(fieldAccessing);
  }
  setFieldAccessingIsBatchChange(isBatch: boolean): void {
    const fieldAccessing: FieldAccessing[] = this.getFieldAccessingIsBatchChange(isBatch);
    super.setBaseFieldAccessing(fieldAccessing);
  }
  //#endregion
  //#region batch field validation ========================================
  validateIntervalValue(): TranslateModel {
    if (this.validateRecurringTypeFlag) {
      return null;
    } else {
      return this.validateIntervalLimit();
    }
    // return this.validateIntervalLimit();
  }
  // only set flag
  validateIntervalType(): void {
    const validateCount = this.validateIntervalLimit();
    if (!isUndefinedOrZeroLength(validateCount)) {
      this.validateRecurringTypeFlag = false;
    } else {
      this.validateRecurringTypeFlag = true;
    }
  }
  // only set flag
  validateEndDateType(): void {
    const enddateoption = !isUndefinedOrZeroLength(this.endDateTimeInput.endDateOption) ? Number(this.endDateTimeInput.endDateOption) : -1;
    const recurringCount = !isUndefinedOrZeroLength(this.endDateTimeInput.recurringCount) ? Number(this.endDateTimeInput.recurringCount) : 0;

    if (enddateoption === BatchEndDateType.RECURRING) {
      if (recurringCount > 1) {
        this.validateEndDateFlag = true;
      } else {
        this.validateEndDateFlag = false;
      }
    } else {
      this.validateEndDateFlag = false;
    }
  }
  validateEndDate(): TranslateModel {
    const enddateoption = !isUndefinedOrZeroLength(this.endDateTimeInput.endDateOption) ? Number(this.endDateTimeInput.endDateOption) : -1;
    const endby = this.endDateTimeInput.endBy;

    if (this.validateEndDateFlag) {
      return null;
    } else {
      if (enddateoption === BatchEndDateType.END_BY && isUndefinedOrZeroLength(endby)) {
        return {
          code: 'ERROR.MANDATORY_FIELD'
        };
      } else {
        return null;
      }
    }
  }
  validateRecurringCountGreaterThanZero(): TranslateModel {
    const recurringCount = !isUndefinedOrZeroLength(this.endDateTimeInput.recurringCount) ? Number(this.endDateTimeInput.recurringCount) : 0;
    if (recurringCount < 1) {
      return {
        code: 'ERROR.GREATER_THAN_ZERO',
        parameters: [this.translate.instant('LABEL.COUNT')]
      };
    } else {
      return null;
    }
  }
  validateIntervalLimit(): TranslateModel {
    const intervalType = !isUndefinedOrZeroLength(this.recurringPatternInput.intervalType) ? Number(this.recurringPatternInput.intervalType) : -1;
    const intervalCount = this.singleIntervalValue;
    const intervalTypeLabel = this.intervalTypeOption.find((item) => item.value === this.recurringPatternInput.intervalType)['label'];

    let isError: boolean = false;
    switch (intervalType) {
      case BatchIntervalType.MINUTES:
        if (intervalCount !== 5 && intervalCount !== 10 && intervalCount !== 20 && intervalCount !== 30) {
          isError = true;
        }
        break;
      case BatchIntervalType.HOURS:
        if (
          intervalCount !== 1 &&
          intervalCount !== 2 &&
          intervalCount !== 3 &&
          intervalCount !== 4 &&
          intervalCount !== 6 &&
          intervalCount !== 8 &&
          intervalCount !== 12
        ) {
          isError = true;
        }
        break;
      case BatchIntervalType.DAYS:
        if (intervalCount !== 1) {
          isError = true;
        }
        break;
      case BatchIntervalType.MONTHS:
        if (intervalCount !== 1 && intervalCount !== 2 && intervalCount !== 3 && intervalCount !== 4 && intervalCount !== 6) {
          isError = true;
        }
        break;
      case BatchIntervalType.YEARS:
        if (intervalCount !== 1) {
          isError = true;
        }
        break;
      case BatchIntervalType.DAYOFWEEK:
        if (isUndefinedOrZeroLength(this.multipleIntervalValues)) {
          return {
            code: 'ERROR.MANDATORY_FIELD'
          };
        } else {
          return null;
        }

      default:
        return {
          code: 'ERROR.MANDATORY_FIELD'
        };
    }
    if (isError) {
      return {
        code: '"{{0}}" is not allowed for interval type {{1}}',
        parameters: [intervalCount.toString(), this.translate.instant(intervalTypeLabel)]
      };
    } else {
      return null;
    }
  }
  //#endregion
  //#region on field change ========================================
  onIsBatchChange(e: boolean): void {
    this.setFieldAccessingIsBatchChange(e);
    this.validateEndDateType();
    this.validateIntervalType();
  }
  onStartDateTimeChange(): void {
    this.validateEndDateType();
    this.validateIntervalType();
  }
  onEndDateOptionChange(e: BatchEndDateType): void {
    this.setEndDateOptionUI(e);
    // toggle validation tooltip
    this.validateEndDateType();
    this.validateIntervalType();
  }
  onEndDateChange(e: any): void {
    // this.endDateTimeInput.endBy = e;
    // this.validateEndDate();
    this.validateEndDateType();
    this.validateIntervalType();
  }
  onEndCountChange(e: number): void {
    this.validateEndDateType();
    this.validateIntervalType();
  }

  onIntervalTypeChange(e: BatchIntervalType): void {
    // this.recurringPatternInput.intervalType = e;
    this.setIntervalValueOptionUI(e);
    // toggle validation tooltip
    this.validateEndDateType();
    this.validateIntervalType();
  }
  onIntervalValueChange(e: any): void {
    // if (this.recurringPatternInput.intervalType === BatchIntervalType.DAYOFWEEK) {
    //   this.multipleIntervalValues = e;
    // } else {
    //   this.singleIntervalValue = e;
    // }
    this.validateEndDateType();
    this.validateIntervalType();
  }
  //#endregion
}
