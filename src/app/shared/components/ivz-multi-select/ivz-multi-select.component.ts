import { Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { ColumnType } from 'shared/constants';
import { Guid, isNullOrUndefined, isUndefinedOrZeroLength, setBindingReady, setOptionReady, switchClass } from 'shared/functions/value.function';
import { TranslateModel } from 'shared/models/systemModel';
import { MultipleSelectConfigModel } from 'shared/models/systemModel';
import { IvzBaseComponent } from 'shared/components/ivz-base/ivz-base.component';
import { interval } from 'rxjs';

@Component({
  selector: 'ivz-multi-select',
  templateUrl: './ivz-multi-select.component.html',
  styleUrls: ['./ivz-multi-select.component.scss']
})
export class IvzMultiSelectComponent extends IvzBaseComponent implements OnInit {
  //#region variable
  bindingModel: any[];
  bindingValidation: TranslateModel;
  bindingConfig: MultipleSelectConfigModel;
  bindingVirtualScroll: boolean;
  bindingItemSize: number;
  bindingFilter: boolean;
  bindingPlaceholder: string;
  bindingAutoDisplayFirst: boolean;
  bindingShowClear: boolean;
  bindingOptions: any[];
  bindingDefaultLabel: string;
  bindingPanelStyle: string;
  bindingMaxSelectedLabels: number;
  bindingDisabled: boolean;
  //#endregion
  //#region input props
  @Input() set ivzModel(param: any[]) {
    this.bindingModel = isNullOrUndefined(param) ? null : param;
    this.setMultiSelectDataInfo();
    this.validateModel(this.bindingModel, this.bindingValidation);
    if (isNullOrUndefined(param)) {
      this.getBindingEmitter(param);
    } else {
      this.setClassReady();
    }
  }
  get ivzModel(): any[] {
    return this.bindingModel;
  }
  @Input() set ivzValidation(param: TranslateModel) {
    this.bindingValidation = param;
    this.validationNotification(param);
  }
  @Input() set ivzConfig(param: MultipleSelectConfigModel) {
    this.bindingConfig = isNullOrUndefined(param) ? new MultipleSelectConfigModel() : param;
    this.bindingInputId = isNullOrUndefined(this.bindingInputId) ? Guid.newGuid() : this.bindingInputId;
    this.bindingVirtualScroll = isNullOrUndefined(this.bindingConfig.virtualScroll) ? this.bindingVirtualScroll : this.bindingConfig.virtualScroll;
    this.bindingItemSize = isNullOrUndefined(this.bindingConfig.itemSize) ? this.bindingItemSize : this.bindingConfig.itemSize;
    this.bindingPlaceholder = isNullOrUndefined(this.bindingConfig.placeholder) ? this.bindingPlaceholder : this.bindingConfig.placeholder;
    this.bindingAutoDisplayFirst = isNullOrUndefined(this.bindingConfig.autoDisplayFirst)
      ? this.bindingAutoDisplayFirst
      : this.bindingConfig.autoDisplayFirst;
    this.bindingShowClear = isNullOrUndefined(this.bindingConfig.showClear) ? this.bindingShowClear : this.bindingConfig.showClear;
    this.bindingOptions = isNullOrUndefined(this.bindingConfig.options) ? this.bindingOptions : this.bindingConfig.options;
    this.bindingDefaultLabel = isNullOrUndefined(this.bindingConfig.defaultLabel) ? this.bindingDefaultLabel : this.bindingConfig.defaultLabel;
    this.bindingPanelStyle = isNullOrUndefined(this.bindingConfig.panelStyle) ? this.bindingPanelStyle : this.bindingConfig.panelStyle;
    this.bindingMaxSelectedLabels = isNullOrUndefined(this.bindingConfig.maxSelectedLabels)
      ? this.bindingMaxSelectedLabels
      : this.bindingConfig.maxSelectedLabels;
    this.bindingFilter = isNullOrUndefined(this.bindingConfig.filter) ? this.bindingFilter : this.bindingConfig.filter;
  }
  @Input() set virtualScroll(param: boolean) {
    this.bindingVirtualScroll = isNullOrUndefined(param) ? this.bindingConfig.virtualScroll : param;
  }
  @Input() set itemSize(param: number) {
    this.bindingItemSize = isNullOrUndefined(param) ? this.bindingConfig.itemSize : param;
  }
  @Input() set filter(param: boolean) {
    this.bindingFilter = isNullOrUndefined(param) ? this.bindingConfig.filter : param;
  }
  @Input() set placeholder(param: string) {
    this.bindingPlaceholder = isNullOrUndefined(param) ? this.bindingConfig.placeholder : param;
  }
  @Input() set autoDisplayFirst(param: boolean) {
    this.bindingAutoDisplayFirst = isNullOrUndefined(param) ? this.bindingConfig.autoDisplayFirst : param;
  }
  @Input() set showClear(param: boolean) {
    this.bindingShowClear = isNullOrUndefined(param) ? this.bindingConfig.showClear : param;
  }
  @Input() set options(param: any[]) {
    this.bindingOptions = isNullOrUndefined(param) ? this.bindingConfig.options : param;
    this.setMultiSelectDataInfo();
    this.setOptionReady();
  }
  @Input() set defaultLabel(param: string) {
    this.bindingDefaultLabel = isNullOrUndefined(param) ? this.bindingConfig.defaultLabel : param;
  }
  @Input() set panelStyle(param: string) {
    this.bindingPanelStyle = isNullOrUndefined(param) ? this.bindingConfig.panelStyle : param;
  }
  @Input() set maxSelectedLabels(param: number) {
    this.bindingMaxSelectedLabels = isNullOrUndefined(param) ? this.bindingConfig.maxSelectedLabels : param;
  }
  @Input() set disabled(param: boolean) {
    this.bindingDisabled = isNullOrUndefined(param) ? false : param;
    if (this.bindingDisabled) {
      switchClass(this.el.nativeElement, 'no-click', this.bindingDisabled);
    } else {
      switchClass(this.el.nativeElement, 'no-click', false);
    }
  }
  //#endregion
  //#region output props
  @Output() ivzModelChange = new EventEmitter<any>();
  @Output() ivzBindingChange = new EventEmitter<any>();

  //#endregion
  constructor(el: ElementRef, renderer: Renderer2) {
    super(el, renderer);
    this.validateService.$formValidate.subscribe((data) => {
      this.validateModel(this.bindingModel, this.bindingValidation, true);
    });
  }
  ngOnInit(): void {
    super.setGetwayFieldType(this.bindingInputId, ColumnType.MASTER);
  }
  setMultiSelectDataInfo(): void {
    super.setMultiSelectDataInfo(this.bindingInputId, this.bindingOptions);
  }
  private getBindingEmitter(param: number[]): void {
    const ele = document.querySelector(`ivz-multi-select[inputId=${this.bindingInputId}]`);
    const $interval = interval(500).subscribe((val) => {
      if (!isUndefinedOrZeroLength(this.bindingOptions)) {
        setOptionReady(ele, true);
        if (!isUndefinedOrZeroLength(param)) {
          if (param.every((e) => this.bindingOptions.some((s) => s.value === e))) {
            setBindingReady(ele, true);
            ele.setAttribute('value', this.setValue());
            this.ivzBindingChange.next(param);
            this.bindingModel = null;
            setTimeout(() => {
              this.bindingModel = param;
            }, 0);
            $interval.unsubscribe();
          } else if (this.bindingModel.every((e) => this.bindingOptions.some((s) => s.value === e))) {
            setBindingReady(ele, true);
            $interval.unsubscribe();
          } else {
            setBindingReady(ele, false);
            ele.setAttribute('value', '');
          }
        } else {
          ele.setAttribute('value', '');
        }
      } else {
        setOptionReady(ele, false);
        ele.setAttribute('value', '');
      }
    });
  }
  setValue(): string {
    let listProcessTransTypeString = '';
    this.bindingModel
      .sort(function (a, b) {
        return a - b;
      })
      .forEach((f, idx) => {
        listProcessTransTypeString += this.bindingOptions.find((fi) => fi.value === f).label;
        if (idx < this.bindingModel.length - 1) {
          listProcessTransTypeString += ', ';
        }
      });
    return listProcessTransTypeString;
  }
  private setClassReady(): void {
    const ele = document.querySelector(`ivz-multi-select[inputId=${this.bindingInputId}]`);
    const $interval = interval(500).subscribe((val) => {
      if (!isUndefinedOrZeroLength(this.bindingOptions)) {
        setOptionReady(ele, true);
        if (this.bindingModel.every((e) => this.bindingOptions.some((s) => s.value === e))) {
          setBindingReady(ele, true);
          $interval.unsubscribe();
        } else {
          setBindingReady(ele, false);
          $interval.unsubscribe();
        }
      }
    });
  }
  private setOptionReady(): void {
    const ele = document.querySelector(`ivz-multi-select[inputId=${this.bindingInputId}]`);
    setOptionReady(ele, !isUndefinedOrZeroLength(this.bindingOptions));
  }
}
