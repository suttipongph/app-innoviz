import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IvzMultiSelectComponent } from './ivz-multi-select.component';

describe('IvzMultiSelectComponent', () => {
  let component: IvzMultiSelectComponent;
  let fixture: ComponentFixture<IvzMultiSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IvzMultiSelectComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IvzMultiSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
