import { Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { Guid, isNullOrUndefined, switchClass } from 'shared/functions/value.function';
import { ButtonConfigModel } from 'shared/models/systemModel';
import { IvzBaseComponent } from 'shared/components/ivz-base/ivz-base.component';
import { setBodyFunctionMode } from 'shared/functions/routing.function';

@Component({
  selector: 'ivz-button',
  templateUrl: './ivz-button.component.html',
  styleUrls: ['./ivz-button.component.scss']
})
export class IvzButtonComponent extends IvzBaseComponent implements OnInit {
  //#region variable
  bindingConfig: ButtonConfigModel;
  bindingLabel: string;
  bindingIcon: string;
  bindingClassName: string;
  bindingDisabled: boolean;
  bindingIconPos: string;
  bindingFunctionMode: boolean;
  //#endregion
  //#region input props
  @Input() set ivzConfig(param: ButtonConfigModel) {
    this.bindingConfig = isNullOrUndefined(param) ? new ButtonConfigModel() : param;
    this.bindingInputId = isNullOrUndefined(this.bindingInputId) ? Guid.newGuid() : this.bindingInputId;
    this.bindingLabel = isNullOrUndefined(this.bindingConfig.label) ? this.bindingLabel : this.bindingConfig.label;
    this.bindingIcon = isNullOrUndefined(this.bindingConfig.icon) ? this.bindingIcon : this.bindingConfig.icon;
    this.bindingClassName = isNullOrUndefined(this.bindingConfig.className) ? this.bindingClassName : this.bindingConfig.className;
    this.bindingDisabled = isNullOrUndefined(this.bindingConfig.disabled) ? this.bindingDisabled : this.bindingConfig.disabled;
    this.bindingIconPos = isNullOrUndefined(this.bindingConfig.iconPos) ? this.bindingIconPos : this.bindingConfig.iconPos;
  }
  @Input() set label(param: string) {
    this.bindingLabel = isNullOrUndefined(param) ? this.bindingConfig.label : param;
  }
  @Input() set icon(param: string) {
    this.bindingIcon = isNullOrUndefined(param) ? this.bindingConfig.icon : param;
  }
  @Input() set className(param: string) {
    this.bindingClassName = isNullOrUndefined(param) ? this.bindingConfig.className : param;
  }
  @Input() set disabled(param: boolean) {
    this.bindingDisabled = isNullOrUndefined(param) ? this.bindingConfig.disabled : param;
    if (this.bindingDisabled) {
      switchClass(this.el.nativeElement, 'no-click', this.bindingDisabled);
    } else {
      switchClass(this.el.nativeElement, 'no-click', false);
    }
  }
  @Input() set iconPos(param: string) {
    this.bindingIconPos = isNullOrUndefined(param) ? this.bindingConfig.iconPos : param;
  }
  @Input() set setBodyFunctionMode(isFn: boolean) {
    this.bindingFunctionMode = isFn;
    setBodyFunctionMode(isFn);
  }
  //#endregion
  //#region output props
  @Output() ivzClick = new EventEmitter<any>();
  //#endregion
  constructor(el: ElementRef, renderer: Renderer2) {
    super(el, renderer);
  }

  ngOnInit(): void {}
  ngOnDestroy(): void {
    if (this.bindingFunctionMode) {
      setBodyFunctionMode(!this.bindingFunctionMode);
    }
    super.ngOnDestroy();
  }
}
