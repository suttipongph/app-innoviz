import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IvzButtonComponent } from './ivz-button.component';

describe('IvzButtonComponent', () => {
  let component: IvzButtonComponent;
  let fixture: ComponentFixture<IvzButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IvzButtonComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IvzButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
