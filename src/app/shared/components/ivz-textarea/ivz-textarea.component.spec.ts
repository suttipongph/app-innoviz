import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IvzTextareaComponent } from './ivz-textarea.component';

describe('IvzTextareaComponent', () => {
  let component: IvzTextareaComponent;
  let fixture: ComponentFixture<IvzTextareaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IvzTextareaComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IvzTextareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
