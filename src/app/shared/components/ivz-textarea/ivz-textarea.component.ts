import { Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { Guid, isNullOrUndefined } from 'shared/functions/value.function';
import { TranslateModel } from 'shared/models/systemModel';
import { TextareaConfigModel } from 'shared/models/systemModel';
import { IvzBaseComponent } from 'shared/components/ivz-base/ivz-base.component';
import { ColumnType } from 'shared/constants';

@Component({
  selector: 'ivz-textarea',
  templateUrl: './ivz-textarea.component.html',
  styleUrls: ['./ivz-textarea.component.scss']
})
export class IvzTextareaComponent extends IvzBaseComponent implements OnInit {
  //#region variable
  bindingModel: string;
  bindingValidation: TranslateModel;
  bindingConfig: TextareaConfigModel;
  bindingMaxlength: number;
  //#endregion
  //#region input props
  @Input() set ivzModel(param: string) {
    this.bindingModel = isNullOrUndefined(param) ? null : param;
    this.validateModel(this.bindingModel, this.bindingValidation);
  }
  get ivzModel(): string {
    return this.bindingModel;
  }
  @Input() set ivzValidation(param: TranslateModel) {
    this.bindingValidation = param;
    this.validationNotification(param);
  }
  @Input() set ivzConfig(param: TextareaConfigModel) {
    this.bindingConfig = isNullOrUndefined(param) ? new TextareaConfigModel() : param;
    this.bindingInputId = isNullOrUndefined(this.bindingInputId) ? Guid.newGuid() : this.bindingInputId;
    this.bindingMaxlength = isNullOrUndefined(this.bindingMaxlength) ? this.bindingConfig.maxlength : this.bindingMaxlength;
  }
  @Input() set maxlength(param: number) {
    this.bindingMaxlength = isNullOrUndefined(param) ? this.bindingConfig.maxlength : param;
  }
  //#endregion
  //#region output props
  @Output() ivzModelChange = new EventEmitter<string>();
  //#endregion
  constructor(el: ElementRef, renderer: Renderer2) {
    super(el, renderer);
    this.validateService.$formValidate.subscribe((data) => {
      this.validateModel(this.bindingModel, this.bindingValidation, true);
    });
  }
  ngOnInit(): void {
    super.setGetwayFieldType(this.bindingInputId, ColumnType.STRING);
  }
}
