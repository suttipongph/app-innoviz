import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IvzBaseComponent } from './ivz-base.component';

describe('IvzBaseComponent', () => {
  let component: IvzBaseComponent;
  let fixture: ComponentFixture<IvzBaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IvzBaseComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IvzBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
