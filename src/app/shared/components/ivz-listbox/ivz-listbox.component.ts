import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { isNullOrUndefined } from '../../functions/value.function';
import { SelectItem } from '../../models/primeModel';

@Component({
  selector: 'ivz-listbox',
  templateUrl: './ivz-listbox.component.html',
  styleUrls: ['./ivz-listbox.component.scss']
})
export class IvzListboxComponent implements OnInit {
  bindingOptions: SelectItem[];
  @Input() set options(param: SelectItem[]) {
    this.bindingOptions = param;
    // param.forEach(item => {
    //   this.translate.getTranslation(item.label).subscribe(res => {
    //     this.bindingOptions.push({ value: item.value, label: res });
    //   });

    // });
  }
  constructor(private translate: TranslateService) {}

  ngOnInit(): void {}
}
