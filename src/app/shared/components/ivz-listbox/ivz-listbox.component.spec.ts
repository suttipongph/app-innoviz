import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IvzListboxComponent } from './ivz-listbox.component';

describe('IvzListboxComponent', () => {
  let component: IvzListboxComponent;
  let fixture: ComponentFixture<IvzListboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IvzListboxComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IvzListboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
