import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IvzBase2Component } from './ivz-base2.component';

describe('IvzBaseComponent', () => {
  let component: IvzBase2Component;
  let fixture: ComponentFixture<IvzBase2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IvzBase2Component]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IvzBase2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
