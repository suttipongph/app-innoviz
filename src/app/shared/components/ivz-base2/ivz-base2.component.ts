import { Component, ElementRef, OnInit, Renderer2 } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppInjector } from 'app-injector';
import { UIControllerService } from 'core/services/uiController.service';
import { ElementType, Ordinal } from '../../constants';
import { isNullOrUndefined, isNullOrUndefOrEmpty, isUndefinedOrZeroLength, replaceFormat } from '../../functions/value.function';
import { OrdinalModel, TranslateModel } from '../../models/systemModel';

@Component({
  selector: 'ivz-base2',
  templateUrl: './ivz-base2.component.html',
  styleUrls: ['./ivz-base2.component.scss']
})
export class IvzBase2Component implements OnInit {
  public uiService: UIControllerService;

  public translate: TranslateService;
  groups: OrdinalModel[] = [];
  divMessage;
  divParent;
  messageFn: string;
  elementText: string;

  // private el: ElementRef, private renderer: Renderer2,
  //   private translate: TranslateService
  constructor(public el: ElementRef, public renderer: Renderer2) {
    this.uiService = AppInjector.get(UIControllerService);
    // this.el = AppInjector.get(ElementRef);
    // this.renderer = AppInjector.get(Renderer2);
    this.translate = AppInjector.get(TranslateService);
    const enums = Object.keys(Ordinal).filter((key) => !isNaN(Number(Ordinal[key])));
    enums.forEach((en, i) => {
      this.groups.push({ value: i, fact: false });
    });
    this.uiService.readonlySubject.subscribe((data: OrdinalModel) => {
      this.setBaseReadonly(data);
    });
  }

  ngOnInit(): void {}
  setBaseReadonly(data: OrdinalModel): void {
    // this.setElementVisibilty();
    if (data.value === Ordinal.Clear) {
      this.groups.forEach((g) => {
        g.fact = false;
      });
    } else {
      this.groups[data.value].fact = data.fact;
    }
  }

  getReadonly(group: number[], id = null): boolean {
    const factors: OrdinalModel[] = [];
    group.forEach((g, i) => {
      factors.push({ value: i, fact: this.groups[g].fact });
    });
    if (group.length > 0 && this.groups[Ordinal.ViewOnly].fact && !isNullOrUndefined(id)) {
      const ele = document.getElementById(id);
      if (!isNullOrUndefined(ele.closest('p-panel.view-only'))) {
        return true;
      } else {
        return group.every((s) => s === Ordinal.ViewOnly) ? false : factors.some((s) => s.fact) ? true : false;
      }
    } else {
      return factors.some((s) => s.fact) ? true : false;
    }
  }
  validationNotification(label?: TranslateModel): void {
    this.divParent = this.el.nativeElement.parentNode;
    if (this.divParent.querySelector('div.validated-tooltip') === null) {
      this.divMessage = this.renderer.createElement('div');
      this.renderer.addClass(this.divMessage, 'validated-tooltip');
    }
    if (!isNullOrUndefined(label)) {
      this.messageFn = this.translate.instant(`${label.code}`);
      if (!isUndefinedOrZeroLength(label.parameters)) {
        this.messageFn = replaceFormat(this.messageFn, label.parameters);
      }
      if (this.divParent.querySelector('div.validated-tooltip-error') === null) {
        const message = this.renderer.createText(this.messageFn);
        this.renderer.appendChild(this.divMessage, message);
        this.renderer.appendChild(this.divParent, this.divMessage);
        this.renderer.addClass(this.divMessage, 'validated-tooltip-error');
        if (this.el.nativeElement.tagName === ElementType.P_DROPDOWN) {
          this.renderer.addClass(this.divParent.querySelector('p-dropdown'), 'input-rerender');
        } else if (this.el.nativeElement.tagName === ElementType.P_MULTISELECT) {
          this.renderer.addClass(this.divParent.querySelector('p-multiselect'), 'input-rerender');
        } else if (this.el.nativeElement.tagName === ElementType.P_INPUTSWITCH) {
          this.renderer.addClass(this.divParent.querySelector('p-inputSwitch'), 'input-rerender');
        } else if (this.el.nativeElement.tagName === ElementType.P_SELECTBUTTON) {
          this.renderer.addClass(this.divParent.querySelector('p-selectbutton'), 'input-rerender');
        }
      }
      this.renderer.addClass(this.el.nativeElement, 'invalidated');
      this.renderer.removeClass(this.el.nativeElement, 'validated');
    } else {
      if (this.el.nativeElement.tagName === ElementType.P_DROPDOWN) {
        this.renderer.removeClass(this.divParent.querySelector('p-dropdown'), 'input-rerender');
      } else if (this.el.nativeElement.tagName === ElementType.P_MULTISELECT) {
        this.renderer.removeClass(this.divParent.querySelector('p-multiselect'), 'input-rerender');
      } else if (this.el.nativeElement.tagName === ElementType.P_INPUTSWITCH) {
        this.renderer.removeClass(this.divParent.querySelector('p-inputSwitch'), 'input-rerender');
      } else if (this.el.nativeElement.tagName === ElementType.P_SELECTBUTTON) {
        this.renderer.removeClass(this.divParent.querySelector('p-selectbutton'), 'input-rerender');
      }
      this.renderer.removeChild(this.divParent, this.divMessage);
      this.renderer.removeClass(this.el.nativeElement, 'invalidated');
      this.renderer.addClass(this.el.nativeElement, 'validated');
    }
  }

  textRequiredValidation(model: any): void {
    if (this.el.nativeElement.hasAttribute('required') && isNullOrUndefOrEmpty(model)) {
      const label: TranslateModel = { code: 'ERROR.MANDATORY_FIELD' };
      this.validationNotification(label);
    } else {
      this.validationNotification();
    }
  }
}
