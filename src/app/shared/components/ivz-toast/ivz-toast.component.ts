import { Component } from '@angular/core';
import { MessageService, ConfirmationService, Confirmation } from 'shared/models/primeModel';
import { NotificationService } from 'core/services/notification.service';
import { UserDataService } from 'core/services/user-data.service';
import { MessageModel, IConfirmation } from 'shared/models/systemModel';
import { isUndefinedOrZeroLength, isNullOrUndefOrEmpty } from '../../functions/value.function';
import { AppConst } from 'shared/constants';

@Component({
  selector: 'ivz-toast',
  templateUrl: './ivz-toast.component.html',
  styles: [``]
})
// tslint:disable-next-line:component-class-suffix
export class IvzToastComponent {
  isUndefinedOrZeroLength = isUndefinedOrZeroLength;
  AppConst = AppConst;

  showUnauthoirzed: boolean = false;
  showBranchCompany: boolean = false;
  visibleCompanyBranchSelector: boolean = false;
  currentCompanyGUID: string;
  loginSite: string;
  message = new MessageModel();
  confirmation: Confirmation;

  toastStyle: string = 'custom-toast';

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private notiService: NotificationService,
    public userDataService: UserDataService
  ) {
    notiService.confirmModalSubject.subscribe((data) => {
      this.message = data;
    });
    notiService.confirmDialogSubject.subscribe((data) => {
      this.confirmation = data;
      this.onConfirmDialog(this.confirmation);
    });
    notiService.showUnauthorizedSubject.subscribe((data) => {
      this.showUnauthoirzed = data;
    });
    notiService.showBranchCompanySelectorSubject.subscribe((data) => {
      if (data) {
        if (!this.visibleCompanyBranchSelector) {
          this.visibleCompanyBranchSelector = true;
        } else {
          this.showBranchCompany = data;
        }
      } else {
        this.showBranchCompany = data;
      }
    });
    this.userDataService.staticCompanyBranchSelectorLoadedSubject.subscribe((componentReady) => {
      if (componentReady && this.visibleCompanyBranchSelector) {
        this.showBranchCompany = true;
      }
    });
  }
  onConfirm() {
    this.messageService.clear('error');
  }
  onReject() {}
  onConfirmDialog(conf: Confirmation): void {
    this.confirmationService.confirm({
      message: conf.message,
      header: conf.header,
      icon: conf.icon,
      accept: () => {
        if (isNullOrUndefOrEmpty(conf.key)) {
          if (this.notiService.isAccept.observers.length > 1) {
            this.notiService.isAccept.observers.splice(0, this.notiService.isAccept.observers.length - 1);
          }
          this.notiService.isAccept.next(true);
        } else {
          const confirm: IConfirmation = { key: conf.key, isAccept: true };
          this.notiService.isAcceptWithKey.next(confirm);
        }
      },
      reject: () => {
        if (isNullOrUndefOrEmpty(conf.key)) {
          if (this.notiService.isAccept.observers.length > 1) {
            this.notiService.isAccept.observers.splice(0, this.notiService.isAccept.observers.length - 1);
          }
          this.notiService.isAccept.next(false);
        } else {
          const confirm: IConfirmation = { key: conf.key, isAccept: false };
          this.notiService.isAcceptWithKey.next(confirm);
        }
      },
      acceptLabel: conf.acceptLabel,
      rejectLabel: conf.rejectLabel
    });
  }
  onClose401() {
    this.showUnauthoirzed = false;
  }
  onCloseBranchCompany() {
    this.showBranchCompany = false;
    sessionStorage.removeItem('companyFromResponse');
  }
  onShowBranchCompany() {
    this.currentCompanyGUID = this.userDataService.getCurrentCompanyGUID();
    this.loginSite = this.userDataService.getSiteLogin();
  }
}
