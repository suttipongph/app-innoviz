import { Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { Guid, isNullOrUndefined } from 'shared/functions/value.function';
import { CheckboxConfigModel } from 'shared/models/systemModel';
import { IvzBaseComponent } from 'shared/components/ivz-base/ivz-base.component';
import { ColumnType } from 'shared/constants';

@Component({
  selector: 'ivz-checkbox',
  templateUrl: './ivz-checkbox.component.html',
  styleUrls: ['./ivz-checkbox.component.scss']
})
export class IvzCheckboxComponent extends IvzBaseComponent implements OnInit {
  //#region variable
  bindingConfig: CheckboxConfigModel;
  bindingModel: boolean = false;
  bindingName: string;
  bindingValue: string;
  bindingLabel: string;
  bindingDisabled: boolean;
  bindingBinary: boolean;
  bindingRequired: boolean;
  //#endregion
  //#region input props
  @Input() set ivzModel(param: any) {
    this.bindingModel = isNullOrUndefined(param) ? null : param;
  }
  get ivzModel(): any {
    return this.bindingModel;
  }
  @Input() set ivzConfig(param: CheckboxConfigModel) {
    this.bindingConfig = isNullOrUndefined(param) ? new CheckboxConfigModel() : param;
    this.bindingInputId = isNullOrUndefined(this.bindingInputId) ? Guid.newGuid() : this.bindingInputId;
    this.bindingName = isNullOrUndefined(this.bindingConfig.name) ? this.bindingName : this.bindingConfig.name;
    this.bindingValue = isNullOrUndefined(this.bindingConfig.value) ? this.bindingValue : this.bindingConfig.value;
    this.bindingLabel = isNullOrUndefined(this.bindingConfig.label) ? this.bindingLabel : this.bindingConfig.label;
    this.bindingDisabled = isNullOrUndefined(this.bindingConfig.disabled) ? this.bindingDisabled : this.bindingConfig.disabled;
    this.bindingBinary = isNullOrUndefined(this.bindingConfig.binary) ? this.bindingBinary : this.bindingConfig.binary;
    this.bindingRequired = isNullOrUndefined(this.bindingConfig.required) ? this.bindingRequired : this.bindingConfig.required;
  }
  @Input() set name(param: string) {
    this.bindingName = isNullOrUndefined(param) ? this.bindingConfig.name : param;
  }
  @Input() set value(param: string) {
    this.bindingValue = isNullOrUndefined(param) ? this.bindingConfig.value : param;
  }
  @Input() set label(param: string) {
    this.bindingLabel = isNullOrUndefined(param) ? this.bindingConfig.label : param;
  }
  @Input() set disabled(param: boolean) {
    this.bindingDisabled = isNullOrUndefined(param) ? this.bindingConfig.disabled : param;
  }
  @Input() set binary(param: boolean) {
    this.bindingBinary = isNullOrUndefined(param) ? this.bindingConfig.binary : param;
  }
  @Input() set required(param: boolean) {
    this.bindingRequired = isNullOrUndefined(param) ? this.bindingConfig.required : param;
  }
  //#endregion
  //#region output props
  @Output() ivzModelChange = new EventEmitter<any>();
  //#endregion
  constructor(el: ElementRef, renderer: Renderer2) {
    super(el, renderer);
  }

  ngOnInit(): void {
    super.setGetwayFieldType(this.bindingInputId, ColumnType.BOOLEAN);
  }
}
