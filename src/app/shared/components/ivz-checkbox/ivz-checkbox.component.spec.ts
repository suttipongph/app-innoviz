import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IvzCheckboxComponent } from './ivz-checkbox.component';

describe('IvzCheckboxComponent', () => {
  let component: IvzCheckboxComponent;
  let fixture: ComponentFixture<IvzCheckboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IvzCheckboxComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IvzCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
