import { Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2, ViewChild } from '@angular/core';
import { Guid, isNullOrUndefined, isNullOrUndefOrEmpty, switchClass } from 'shared/functions/value.function';
import { TranslateModel } from 'shared/models/systemModel';
import { AppConst, ColumnType } from 'shared/constants';
import { CalendarConfigModel } from 'shared/models/systemModel';
import { IvzBaseComponent } from 'shared/components/ivz-base/ivz-base.component';
import { getTimeStringToDate } from 'shared/functions/date.function';

@Component({
  selector: 'ivz-calendar',
  templateUrl: './ivz-calendar.component.html',
  styleUrls: ['./ivz-calendar.component.scss']
})
export class IvzCalendarComponent extends IvzBaseComponent implements OnInit {
  //#region variable
  bindingMinDate: Date;
  bindingMaxDate: Date;
  bindingModel: any;
  bindingConfig: CalendarConfigModel;
  bindingDateFormat: string;
  bindingShowIcon: boolean;
  bindingValidation: TranslateModel;
  bindingDataType: string;
  bindingSelectionMode: string;
  bindingHourFormat: string;
  bindingShowTime: boolean;
  bindingShowSeconds: boolean;
  bindingTimeOnly: boolean;
  bindingDisabled: boolean;
  date8: Date;
  @ViewChild('myCalendar') datePicker;
  //#endregion
  //#region input props
  @Input() set ivzModel(param: any) {
    if (this.bindingTimeOnly) {
      this.bindingModel = getTimeStringToDate(param);
    } else {
      this.bindingModel = isNullOrUndefined(param) ? null : param;
    }
    this.validateModel(this.bindingModel, this.bindingValidation);
    if (!isNullOrUndefOrEmpty(param) && param instanceof Array) {
      if (!param.some((s) => isNullOrUndefOrEmpty(s))) {
        const date = document.getElementById('closeCalendar');
        if (!isNullOrUndefOrEmpty(date)) {
          date.click();
        }
      }
    }
  }
  get ivzModel(): any {
    return this.bindingModel;
  }

  @Input() set ivzValidation(param: TranslateModel) {
    this.bindingValidation = param;
    this.validationNotification(param);
  }

  @Input() set ivzConfig(param: CalendarConfigModel) {
    this.bindingConfig = isNullOrUndefined(param) ? new CalendarConfigModel() : param;
    this.bindingInputId = isNullOrUndefined(this.bindingInputId) ? Guid.newGuid() : this.bindingInputId;
    this.bindingDateFormat = isNullOrUndefined(this.bindingConfig.dateFormat) ? this.bindingDateFormat : this.bindingConfig.dateFormat;
    this.bindingShowIcon = isNullOrUndefined(this.bindingConfig.showIcon) ? this.bindingShowIcon : this.bindingConfig.showIcon;
    this.bindingDataType = isNullOrUndefined(this.bindingConfig.dataType) ? this.bindingDataType : this.bindingConfig.dataType;
    this.bindingSelectionMode = isNullOrUndefined(this.bindingConfig.selectionMode) ? this.bindingSelectionMode : this.bindingConfig.selectionMode;
    this.bindingShowTime = isNullOrUndefined(this.bindingConfig.showTime) ? this.bindingShowTime : this.bindingConfig.showTime;
    this.bindingHourFormat = isNullOrUndefined(this.bindingConfig.hourFormat) ? this.bindingHourFormat : this.bindingConfig.hourFormat;
    this.bindingShowSeconds = isNullOrUndefined(this.bindingConfig.showSeconds) ? this.bindingShowSeconds : this.bindingConfig.showSeconds;
    this.bindingTimeOnly = isNullOrUndefined(this.bindingConfig.timeOnly) ? this.bindingTimeOnly : this.bindingConfig.timeOnly;
    this.bindingMinDate = isNullOrUndefined(this.bindingConfig.minDate) ? this.bindingMinDate : this.bindingConfig.minDate;
    this.bindingMaxDate = isNullOrUndefined(this.bindingConfig.maxDate) ? this.bindingMaxDate : this.bindingConfig.maxDate;
  }
  @Input() set minDate(param: Date) {
    this.bindingMinDate = isNullOrUndefined(param) ? this.bindingConfig.minDate : param;
  }
  @Input() set maxDate(param: Date) {
    this.bindingMaxDate = isNullOrUndefined(param) ? this.bindingConfig.maxDate : param;
  }
  @Input() set dateFormat(param: string) {
    this.bindingDateFormat = isNullOrUndefined(param) ? this.bindingConfig.dateFormat : param;
  }

  @Input() set showIcon(param: boolean) {
    this.bindingShowIcon = isNullOrUndefined(param) ? this.bindingConfig.showIcon : param;
  }

  @Input() set dataType(param: string) {
    this.bindingDataType = isNullOrUndefined(param) ? this.bindingConfig.dataType : param;
  }

  @Input() set selectionMode(param: string) {
    this.bindingSelectionMode = isNullOrUndefined(param) ? this.bindingConfig.selectionMode : param;
  }

  @Input() set showTime(param: boolean) {
    this.bindingShowTime = isNullOrUndefined(param) ? this.bindingConfig.showTime : param;
  }

  @Input() set hourFormat(param: string) {
    this.bindingHourFormat = isNullOrUndefined(param) ? this.bindingConfig.hourFormat : param;
  }

  @Input() set showSeconds(param: boolean) {
    this.bindingShowSeconds = isNullOrUndefined(param) ? this.bindingConfig.showSeconds : param;
  }

  @Input() set timeOnly(param: boolean) {
    this.bindingTimeOnly = isNullOrUndefined(param) ? this.bindingConfig.timeOnly : param;
  }
  @Input() set disabled(param: boolean) {
    this.bindingDisabled = isNullOrUndefined(param) ? false : param;
    if (this.bindingDisabled) {
      switchClass(this.el.nativeElement, 'no-click', this.bindingDisabled);
    } else {
      switchClass(this.el.nativeElement, 'no-click', false);
    }
  }

  //#endregion
  //#region output props
  @Output() ivzModelChange = new EventEmitter<any>();
  //#endregion
  constructor(el: ElementRef, renderer: Renderer2) {
    super(el, renderer);
    this.$subscripion.subscribe((data) => {
      if (!AppConst.SYSTEM_FIELD.some((s) => s === this.bindingInputId)) {
        this.validateModel(this.ivzModel, this.bindingValidation, true);
      }
    });
  }
  ngOnInit(): void {
    super.setGetwayFieldType(this.bindingInputId, ColumnType.DATE);
  }
  onModelChange(ivzModel: any) {
    this.bindingTimeOnly && !isNullOrUndefined(ivzModel)
      ? this.ivzModelChange.next(`${ivzModel.getHours()}:${ivzModel.getMinutes()}:`)
      : this.ivzModelChange.next(ivzModel);
  }
  showOnFocus(): boolean {
    return !this.getReadonly(this.bindingInputId);
  }
}
