import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IvzCalendarComponent } from './ivz-calendar.component';

describe('IvzCalendarComponent', () => {
  let component: IvzCalendarComponent;
  let fixture: ComponentFixture<IvzCalendarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IvzCalendarComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IvzCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
