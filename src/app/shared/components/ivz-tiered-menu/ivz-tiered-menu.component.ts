// import { Component, Input, OnInit } from '@angular/core';
// import { TranslateService } from '@ngx-translate/core';
// import { Ordinal } from '../../constants';
// import { isNullOrUndefined } from '../../functions/value.function';
// import { MenuItem } from '../../models/primeModel';
// import { TextareaConfigModel } from '../../models/systemModel/ivzComponentModel';
// import { IvzBaseComponent } from 'shared/components/ivz-base/ivz-base.component';

// @Component({
//     selector: 'ivz-tiered-menu',
//     templateUrl: './ivz-tiered-menu.component.html'
// })
// export class IvzTieredMenuComponent extends IvzBaseComponent implements OnInit {
//     model: string;
//     bindingConfig: TextareaConfigModel;
//     bindingModel: any[];
//     public relatedInfoItems: MenuItem[];

//     @Input() set ivzModel(param: any[]) {
//         this.bindingModel = isNullOrUndefined(param) ? null : param;
//     }
//     get ivzModel(): any[] { return this.bindingModel; }

//     @Input() set ivzConfig(param: TextareaConfigModel) {
//         this.bindingConfig = isNullOrUndefined(param) ? new TextareaConfigModel() : param;
//         this.bindingInputId = this.bindingConfig.inputId;
//         this.bindingReadonly = [Ordinal.ViewOnly];
//         this.bindingRows = this.bindingConfig.rows;
//         this.bindingCols = this.bindingConfig.cols;
//         this.bindingMaxlength = this.bindingConfig.maxlength;
//         this.bindingAutoResize = this.bindingConfig.autoResize;
//         this.bindingDisabled = this.bindingConfig.disabled;
//     }

//
//     @Input() set inputId(param: string) {
//         this.bindingInputId = isNullOrUndefined(param) ? this.bindingConfig.inputId : param;
//     }

//     bindingReadonly: number[];
//     @Input() set ivzReadonly(param: number[]) {
//         this.bindingReadonly = isNullOrUndefined(param) ? [Ordinal.ViewOnly] : param;
//     }

//     bindingRows: number;
//     @Input() set rows(param: number) {
//         this.bindingRows = isNullOrUndefined(param) ? this.bindingConfig.rows : param;
//     }

//     bindingCols: number;
//     @Input() set cols(param: number) {
//         this.bindingCols = isNullOrUndefined(param) ? this.bindingConfig.cols : param;
//     }

//     bindingMaxlength: number;
//     @Input() set maxlength(param: number) {
//         this.bindingMaxlength = isNullOrUndefined(param) ? this.bindingConfig.maxlength : param;
//     }

//     bindingAutoResize: boolean;
//     @Input() set autoResize(param: boolean) {
//         this.bindingAutoResize = isNullOrUndefined(param) ? this.bindingConfig.autoResize : param;
//     }

//     bindingDisabled: boolean;
//     @Input() set disabled(param: boolean) {
//         this.bindingDisabled = isNullOrUndefined(param) ? this.bindingConfig.disabled : param;
//     }
//     constructor(public translate: TranslateService) {
//         super();
this.custom.baseService = this.baseService;
//         this.relatedInfoItems = [
//             {
//                 label: 'LABEL.AGREEMENT')
//             },
//             {
//                 label: 'LABEL.AGREEMENT_OUTSTANDING_BALANCE'),
//             }];
//     }

//     ngOnInit(): void {
//     }

// }
