import { Component, OnInit, ViewChild, Input, OnDestroy, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { TreeNode } from 'shared/models/primeModel';
import { AuthService } from 'core/services/auth.service';
import { NotificationService } from 'core/services/notification.service';
import { UserDataService } from 'core/services/user-data.service';
import { OverlayPanel } from 'primeng/overlaypanel/public_api';
import { AccessRightService } from 'core/services/access-right.service';
import { TranslateService } from '@ngx-translate/core';
import { isUndefinedOrZeroLength, getQueryParamObject } from 'shared/functions/value.function';
import { Router } from '@angular/router';
import { SysUserCompanyMappingView, SysUserSettingsItemView } from 'shared/models/viewModel';
import { AppConst } from 'shared/constants';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'cpn-company-branch-selector',
  templateUrl: './company-branch-selector.component.html',
  styleUrls: ['./company-branch-selector.component.scss']
})
export class CompanyBranchSelectorComponent implements OnInit, AfterViewInit, OnDestroy {
  companyMappings: any = [];

  popupCompanyMapping: boolean = false;
  titleList = 'Select Company - Branch';

  treeSourceDataSource: TreeNode[] = [];
  @Input() isStatic: boolean = false;
  @ViewChild('op1') op1: OverlayPanel;

  isLoggedInSubscription: Subscription;

  @Output() initComponent = new EventEmitter<any>();
  @Output() nodeSelected = new EventEmitter<any>();

  constructor(
    private authService: AuthService,
    private userDataService: UserDataService,
    private accessService: AccessRightService,
    private notificationService: NotificationService,
    private translateService: TranslateService,
    private router: Router
  ) {
    this.userDataService.showCompanyBranchSelectorSubject.subscribe((event) => {
      if (!this.isStatic) {
        if (this.accessService.isPermittedBySite() && this.accessService.isPermittedByAccessRight()) {
          this.op1.toggle(event);
        }
      }
    });

    if (!isUndefinedOrZeroLength(this.isLoggedInSubscription)) {
      this.isLoggedInSubscription.unsubscribe();
    }
  }

  ngOnInit(): void {
    if (this.isStatic) {
      const loginSite = this.userDataService.getSiteLogin();
      if (!isUndefinedOrZeroLength(loginSite)) {
        this.setCompanyBranchSelectorData(loginSite);
      }
    }
  }

  ngAfterViewInit(): void {}

  nodeSelect(event): void {
    const currentCompany: string = this.userDataService.getCurrentCompanyGUID();
    const loginSite = this.userDataService.getSiteLogin();
    // let data = { companyGUID: '', companyId: '', branchGUID: '', branchId: '' };
    // if (loginSite !== AppConst.BACK && loginSite !== AppConst.OB) {
    //   data = {
    //     companyGUID: event.node.parent.data.companyGUID,
    //     companyId: event.node.parent.data.company_CompanyId,
    //     branchGUID: event.node.data.branchGUID,
    //     branchId: event.node.data.branch_BranchId
    //   };
    // } else {
    const data = {
      companyGUID: event.node.data.companyGUID,
      companyId: event.node.data.company_CompanyId,
      branchGUID: event.node.data.branchGUID,
      branchId: event.node.data.branch_BranchId
    };
    // }
    this.userDataService.setCurrentCompany(data);

    if (currentCompany !== data.companyGUID) {
      const siteLogin = this.userDataService.getSiteLogin();
      if (siteLogin === AppConst.CASHIER) {
        this.accessService
          .loadUserAccessRightAtLogin(this.userDataService.getUserIdFromToken(), siteLogin)
          .pipe(
            finalize(() => {
              // clear dropdown data
              this.userDataService.clearUsageData();
              this.authService.setDisplaySubject.next(true);
              this.accessService.siteAndCompanyBranchSelected.next(true);

              this.routeToPageAfterLogin();
            })
          )
          .subscribe();
      } else {
        this.accessService.loadUserAccessRight(this.userDataService.getUserIdFromToken()).subscribe(() => {
          // clear dropdown data
          this.userDataService.clearUsageData();

          if (!this.isStatic) {
            this.router.navigate([`/`]);
          } else {
            const companyFromResponse = sessionStorage.getItem('companyFromResponse');
            if (isUndefinedOrZeroLength(companyFromResponse)) {
              // this.routeToPageAfterLogin();
            } else {
              const currentCompanyGUID = this.userDataService.getCurrentCompanyGUID();
              if (currentCompanyGUID !== companyFromResponse) {
                this.router.navigate([`/`]);
              }
            }
            this.notificationService.hideBranchCompanySelector();
          }
        });
      }
    } else {
      this.notificationService.hideBranchCompanySelector();
    }

    this.userDataService.companyChangeSubject.next('change');
    this.nodeSelected.emit(currentCompany);
    if (!this.isStatic) {
      this.op1.hide();
    }
  }

  nodeUnselect(event): void {}

  onShow(event): void {
    const loginSite = this.userDataService.getSiteLogin();
    this.setCompanyBranchSelectorData(loginSite);
  }

  onHide(event): void {}

  setCompanyBranchSelectorData(loginSite: string): void {
    if (this.authService.isLoggedIn()) {
      this.userDataService.loadCompanyMapping().subscribe((res) => {
        const user: SysUserSettingsItemView = res;
        // set user language
        if (!isUndefinedOrZeroLength(user.languageId)) {
          this.userDataService.setAppLanguage(user.languageId);
          this.translateService.use(user.languageId);
        }

        // sort user-company mapping list
        user.sysUserCompanyMappings.sort((a, b) => {
          if (a.company_CompanyId === b.company_CompanyId) {
            return a.branch_BranchId < b.branch_BranchId ? -1 : a.branch_BranchId > b.branch_BranchId ? 1 : 0;
          } else {
            return a.company_CompanyId < b.company_CompanyId ? -1 : 1;
          }
        });

        // set company branch selector tree data
        this.companyMappings = user.sysUserCompanyMappings;
        const treeSource = this.setCompanyBranchTreeData(this.companyMappings, loginSite);

        this.treeSourceDataSource = treeSource;

        // set default value only in front and back site
        if (loginSite !== AppConst.CASHIER) {
          this.setUserDefaultCompanyBranch(user, loginSite);

          const currentCompany = this.userDataService.getCurrentCompanyGUID();
          const currentBranch = this.userDataService.getCurrentBranchGUID();
          if (
            !this.isStatic ||
            (this.isStatic &&
              ((!isUndefinedOrZeroLength(currentCompany) && !isUndefinedOrZeroLength(currentBranch)) ||
                (!isUndefinedOrZeroLength(currentCompany) && isUndefinedOrZeroLength(currentBranch) && loginSite === AppConst.BACK)))
          ) {
            this.authService.setDisplaySubject.next();
          }
        }

        if (this.isStatic && loginSite !== AppConst.CASHIER) {
          this.userDataService.staticCompanyBranchSelectorLoadedSubject.next(true);
        }
      });
    } else {
      this.authService.onInvalidToken();
    }
  }

  setCompanyBranchTreeData(userCompanyMapping: SysUserCompanyMappingView[], loginSite: string): TreeNode[] {
    const treeSource = [];
    for (let i = 0; i < userCompanyMapping.length; i++) {
      let exitIdx = -1;
      // set parent node
      const parent: TreeNode = {};
      parent.label = '[' + userCompanyMapping[i].company_CompanyId + '] ' + userCompanyMapping[i].company_Name;
      parent.data = userCompanyMapping[i];
      parent.selectable = loginSite === AppConst.BACK;
      parent.expanded = true;
      parent.children = [];

      const currentCompany = userCompanyMapping[i].companyGUID;
      for (let j = i; j < userCompanyMapping.length; j++) {
        if (userCompanyMapping[j].companyGUID == currentCompany) {
          // not populating branch in 'back' site
          if (loginSite !== AppConst.BACK) {
            const child: TreeNode = {};
            child.data = userCompanyMapping[j];
            child.label = '[' + userCompanyMapping[j].branch_BranchId + '] ' + userCompanyMapping[j].branch_Name;
            parent.children.push(child);
          }
          if (j === userCompanyMapping.length - 1) {
            exitIdx = j;
            i = exitIdx;

            break;
          }
        } else {
          exitIdx = j;
          i = exitIdx - 1;

          break;
        }
      }

      treeSource.push(parent);
    }

    return treeSource;
  }
  setUserDefaultCompanyBranch(user: SysUserSettingsItemView, loginSite: string): void {
    // check if company/branch already selected by user
    const currentCompany = this.userDataService.getCurrentCompanyGUID();
    const currentBranch = this.userDataService.getCurrentBranchGUID();
    if (isUndefinedOrZeroLength(currentCompany) || (loginSite === AppConst.FRONT && isUndefinedOrZeroLength(currentBranch))) {
      // get default mapped company-branch
      let defaultBC;
      if (
        isUndefinedOrZeroLength(user.defaultCompanyGUID) ||
        (isUndefinedOrZeroLength(user.defaultCompanyGUID) && isUndefinedOrZeroLength(user.defaultBranchGUID))
      ) {
        defaultBC = null;
      } else if (isUndefinedOrZeroLength(user.defaultBranchGUID)) {
        if (loginSite === AppConst.FRONT || loginSite === AppConst.BACK || loginSite === AppConst.CASHIER) {
          defaultBC = null;
        } else {
          const findCompany = user.sysUserCompanyMappings.find((item) => item.companyGUID.toUpperCase() === user.defaultCompanyGUID.toUpperCase());
          if (!isUndefinedOrZeroLength(findCompany)) {
            findCompany.branchGUID = null;
            findCompany.branch_BranchId = null;
            defaultBC = findCompany;
          } else {
            defaultBC = null;
          }
        }
      } else {
        defaultBC = user.sysUserCompanyMappings.find(
          (item) =>
            item.companyGUID.toUpperCase() === user.defaultCompanyGUID.toUpperCase() &&
            item.branchGUID.toUpperCase() === user.defaultBranchGUID.toUpperCase()
        );
      }
      // prepare default company-branch data
      let data = {};
      if (!isUndefinedOrZeroLength(defaultBC)) {
        data = {
          companyGUID: defaultBC.companyGUID,
          companyId: defaultBC.company_CompanyId,
          // branchGUID: loginSite === AppConst.BACK ? null : defaultBC.branchGUID,
          // branchId: loginSite === AppConst.BACK ? null : defaultBC.branch_BranchId
          branchGUID: defaultBC.branchGUID,
          branchId: defaultBC.branch_BranchId
        };
        // set to storage/display
        this.userDataService.setCurrentCompany(data);
        this.accessService.loadUserAccessRight(user.id).subscribe();
      } else {
        data = {
          companyGUID: '',
          companyId: 'N/A',
          branchGUID: '',
          branchId: 'N/A'
        };
        // set to storage/display
        this.userDataService.setCurrentCompany(data);
      }
    }
  }

  routeToPageAfterLogin(): void {
    // redirect to page before login
    let redirect = sessionStorage.getItem('redirect');
    console.log(redirect);
    if (!isUndefinedOrZeroLength(redirect)) {
      sessionStorage.removeItem('redirect');
      if (redirect.includes('siteselect')) {
        this.router.navigate([redirect], { skipLocationChange: true });
      } else {
        const path_decoded = decodeURIComponent(redirect);
        if (redirect.includes(':') || redirect.includes('!')) {
          this.router.navigate([path_decoded]);
        } else {
          const splits = path_decoded.split('?');
          console.log(splits);
          if (splits.length > 1) {
            this.router.navigate([splits[0]], {
              queryParams: getQueryParamObject(path_decoded)
            });
          } else {
            this.router.navigate([path_decoded]);
          }
        }
      }
    }
  }
  ngOnDestroy(): void {
    if (!isUndefinedOrZeroLength(this.isLoggedInSubscription)) {
      this.isLoggedInSubscription.unsubscribe();
    }
  }
}
