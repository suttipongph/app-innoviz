import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyBranchSelectorComponent } from './company-branch-selector.component';

describe('CompanyBranchSelectorComponent', () => {
  let component: CompanyBranchSelectorComponent;
  let fixture: ComponentFixture<CompanyBranchSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CompanyBranchSelectorComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyBranchSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
