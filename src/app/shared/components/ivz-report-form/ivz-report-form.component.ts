import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FileService } from 'core/services/file.service';
import { ReportService } from 'core/services/report.service';
import { isUndefinedOrZeroLength } from 'shared/functions/value.function';
import { MenuItem } from 'shared/models/primeModel';
import { FileInformation } from 'shared/models/systemModel';

@Component({
  selector: 'ivz-report-form',
  templateUrl: './ivz-report-form.component.html',
  styleUrls: ['./ivz-report-form.component.scss']
})
export class IvzReportFormComponent implements OnInit {
  parm: any;
  toolBar: boolean = false;
  menuBarItems: MenuItem[];

  currentPage: number = 1;
  totalPages: number;

  firstMakeMenu: boolean = true;
  reportViewer: boolean = false;

  @Input() isExportToExcel: boolean = false;
  @Input()
  set parameter(parameter: any) {
    if (!isUndefinedOrZeroLength(parameter)) {
      if (!isUndefinedOrZeroLength(parameter['reportName'])) {
        this.parm = parameter;
        this.renderReport();
      }
    }
  }

  @Output() reportReady = new EventEmitter<boolean>();
  theHtmlString: string = '';
  constructor(private reportService: ReportService, private fileService: FileService, private translate: TranslateService) {}

  ngOnInit(): void {}

  renderReport(): void {
    this.reportService.renderReport(this.parm).subscribe((file: FileInformation) => {
      const blob = this.fileService.getPreviewBlob(file);
      this.theHtmlString = window.URL.createObjectURL(blob);

      this.reportViewer = true;
      this.toolBar = true;
      this.reportReady.emit(true);
      if (this.firstMakeMenu) {
        this.getMenuBarItem();
        this.firstMakeMenu = false;
      }
    });
  }

  exportReport(fileType: string): void {
    this.parm['reportFormat'] = fileType;
    this.reportService.exportReport(this.parm).subscribe((file: FileInformation) => {
      this.download(file);
    });
  }

  download(file: FileInformation): void {
    if (window.navigator.msSaveOrOpenBlob) {
      // IE10+
      const blob = this.fileService.getPreviewBlob(file);
      window.navigator.msSaveOrOpenBlob(blob, file.fileName);
    } else {
      // Others
      this.fileService.saveAsFile(file.base64, file.fileName);
    }
  }

  getMenuBarItem(): void {
    this.menuBarItems = [
      {
        label: this.translate.instant('LABEL.EXPORT_TO_EXCEL'),
        icon: 'pi pi-share-alt',
        visible: this.isExportToExcel,
        command: () => {
          this.exportReport('EXCELOPENXML');
        }
      },
      {
        label: this.translate.instant('LABEL.REFRESH'),
        icon: 'pi pi-fw pi-refresh',
        visible: true,
        command: () => {
          this.renderReport();
        }
      },
      {
        label: this.translate.instant('LABEL.PARAMETER'),
        icon: 'pi pi-fw pi-filter',
        visible: true,
        command: () => {
          this.reportViewer = false;
          this.parm = null;
          this.theHtmlString = null;
          this.toolBar = false;
          this.reportReady.emit(false);
        }
      }
    ];
  }
}
