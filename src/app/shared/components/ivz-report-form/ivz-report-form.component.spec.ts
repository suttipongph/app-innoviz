import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IvzReportFormComponent } from './ivz-report-form.component';

describe('IvzReportFormComponent', () => {
  let component: IvzReportFormComponent;
  let fixture: ComponentFixture<IvzReportFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IvzReportFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IvzReportFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
