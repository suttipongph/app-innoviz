import { Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { ColumnType, Ordinal } from '../../constants';
import { Guid, isNullOrUndefined } from '../../functions/value.function';
import { RadioButtonConfigModel } from '../../models/systemModel/ivzComponentModel';
import { IvzBaseComponent } from 'shared/components/ivz-base/ivz-base.component';

@Component({
  selector: 'ivz-radio-button',
  templateUrl: './ivz-radio-button.component.html',
  styleUrls: ['./ivz-radio-button.component.scss']
})
export class IvzRadioButtonComponent extends IvzBaseComponent implements OnInit {
  //#region variable
  bindingModel: string;
  bindingConfig: RadioButtonConfigModel;
  bindingName: string;
  bindingValue: string;
  bindingLabel: string;
  //#endregion
  //#region input props
  @Input() set ivzModel(param: string) {
    this.bindingModel = isNullOrUndefined(param) ? '' : param;
  }
  get ivzModel(): string {
    return this.bindingModel;
  }
  @Input() set ivzConfig(param: RadioButtonConfigModel) {
    this.bindingConfig = isNullOrUndefined(param) ? new RadioButtonConfigModel() : param;
    this.bindingInputId = isNullOrUndefined(this.bindingInputId) ? Guid.newGuid() : this.bindingInputId;
    this.bindingName = this.bindingConfig.name;
    this.bindingValue = this.bindingConfig.value;
    this.bindingLabel = this.bindingConfig.label;
  }
  @Input() set name(param: string) {
    this.bindingName = isNullOrUndefined(param) ? this.bindingConfig.name : param;
  }
  @Input() set value(param: string) {
    this.bindingValue = isNullOrUndefined(param) ? this.bindingConfig.value : param;
  }
  @Input() set label(param: string) {
    this.bindingLabel = isNullOrUndefined(param) ? this.bindingConfig.label : param;
  }
  //#endregion
  //#region output props
  @Output() ivzModelChange = new EventEmitter<string>();
  @Output() ivzOnChange = new EventEmitter<string>();
  //#endregion
  constructor(el: ElementRef, renderer: Renderer2) {
    super(el, renderer);
  }

  ngOnInit(): void {
    super.setGetwayFieldType(this.bindingInputId, ColumnType.BOOLEAN);
  }
}
