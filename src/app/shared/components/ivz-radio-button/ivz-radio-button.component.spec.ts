import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IvzRadioButtonComponent } from './ivz-radio-button.component';

describe('IvzRadioButtonComponent', () => {
  let component: IvzRadioButtonComponent;
  let fixture: ComponentFixture<IvzRadioButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IvzRadioButtonComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IvzRadioButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
