import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IvzDropdownComponent } from './ivz-dropdown.component';

describe('IvzDropdownComponent', () => {
  let component: IvzDropdownComponent;
  let fixture: ComponentFixture<IvzDropdownComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IvzDropdownComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IvzDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
