import { Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { AppConst, ColumnType } from 'shared/constants';
import {
  Guid,
  isFalse,
  isNullOrUndefined,
  isNullOrUndefOrEmpty,
  isUndefinedOrZeroLength,
  replaceAll,
  setBindingReady,
  setOptionReady,
  switchClass
} from 'shared/functions/value.function';
import { IvzDropdownOnFocusModel, TranslateModel } from 'shared/models/systemModel';
import { DropdownConfigModel } from 'shared/models/systemModel';
import { IvzBaseComponent } from 'shared/components/ivz-base/ivz-base.component';
import { interval } from 'rxjs';

@Component({
  selector: 'ivz-dropdown',
  templateUrl: './ivz-dropdown.component.html',
  styleUrls: ['./ivz-dropdown.component.scss']
})
export class IvzDropdownComponent extends IvzBaseComponent implements OnInit {
  //#region variable
  bindingConfig: DropdownConfigModel;
  bindingModel: any;
  bindingValidation: TranslateModel;
  bindingVirtualScroll: boolean;
  bindingItemSize: number;
  bindingFilter: boolean;
  bindingPlaceholder: string;
  bindingShowClear: boolean;
  bindingAutoDisplayFirst: boolean;
  bindingOptions: any[];
  bindingDisabled: boolean;
  isLoadedOnFirstFocus: boolean = false;
  //#endregion
  //#region input props
  @Input() set ivzModel(param: any) {
    if (this.bindingModel === undefined && param !== undefined) {
      this.getBindingEmitter(param);
    } else {
      this.bindingModel = param;
      this.setBindingReady();
    }
    this.getRequiredValidation(param);
  }
  get ivzModel(): any {
    return this.bindingModel;
  }
  @Input() set ivzValidation(param: TranslateModel) {
    this.bindingValidation = param;
    this.validationNotification(param);
  }
  @Input() set ivzConfig(param: DropdownConfigModel) {
    this.bindingConfig = isNullOrUndefined(param) ? new DropdownConfigModel() : param;
    this.bindingInputId = isNullOrUndefined(this.bindingInputId) ? Guid.newGuid() : this.bindingInputId;
    this.bindingVirtualScroll = isNullOrUndefined(this.bindingConfig.virtualScroll) ? this.bindingVirtualScroll : this.bindingConfig.virtualScroll;
    this.bindingItemSize = isNullOrUndefined(this.bindingConfig.itemSize) ? this.bindingItemSize : this.bindingConfig.itemSize;
    this.bindingPlaceholder = isNullOrUndefined(this.bindingConfig.placeholder) ? this.bindingPlaceholder : this.bindingConfig.placeholder;
    this.bindingAutoDisplayFirst = isNullOrUndefined(this.bindingConfig.autoDisplayFirst)
      ? this.bindingAutoDisplayFirst
      : this.bindingConfig.autoDisplayFirst;
    this.bindingFilter = isNullOrUndefined(this.bindingConfig.filter) ? this.bindingFilter : this.bindingConfig.filter;
    this.bindingShowClear = isNullOrUndefined(this.bindingConfig.showClear)
      ? this.bindingShowClear === false
        ? undefined
        : this.bindingShowClear
      : this.bindingConfig.showClear === false
      ? undefined
      : this.bindingConfig.showClear;
    this.bindingOptions = isNullOrUndefined(this.bindingConfig.options) ? this.bindingOptions : this.bindingConfig.options;
  }
  @Input() set virtualScroll(param: boolean) {
    this.bindingVirtualScroll = isNullOrUndefined(param) ? this.bindingConfig.virtualScroll : param;
  }
  @Input() set itemSize(param: number) {
    this.bindingItemSize = isNullOrUndefined(param) ? this.bindingConfig.itemSize : param;
  }
  @Input() set filter(param: boolean) {
    this.bindingFilter = isNullOrUndefined(param) ? this.bindingConfig.filter : param;
  }
  @Input() set placeholder(param: string) {
    this.bindingPlaceholder = isNullOrUndefined(param) ? this.bindingConfig.placeholder : param;
  }
  @Input() set autoDisplayFirst(param: boolean) {
    this.bindingAutoDisplayFirst = isNullOrUndefined(param) ? this.bindingConfig.autoDisplayFirst : param;
  }
  @Input() set showClear(param: boolean) {
    this.bindingShowClear = isNullOrUndefined(param)
      ? this.bindingConfig.showClear === false
        ? undefined
        : this.bindingConfig.showClear
      : isFalse(param)
      ? undefined
      : param;
  }
  @Input() set options(param: any[]) {
    this.bindingOptions = isNullOrUndefined(param) ? this.bindingConfig.options : param;
    this.setOptionReady();
  }
  @Input() set disabled(param: boolean) {
    this.bindingDisabled = isNullOrUndefined(param) ? false : param;
    if (this.bindingDisabled) {
      switchClass(this.el.nativeElement, 'no-click', this.bindingDisabled);
    } else {
      switchClass(this.el.nativeElement, 'no-click', false);
    }
  }
  //#endregion
  //#region output props
  @Output() ivzModelChange = new EventEmitter<any>();
  @Output() ivzBindingChange = new EventEmitter<any>();
  @Output() ivzOnFocus = new EventEmitter<any>();
  //#endregion
  constructor(el: ElementRef, renderer: Renderer2) {
    super(el, renderer);
    this.$subscripion.subscribe((data) => {
      if (!AppConst.SYSTEM_FIELD.some((s) => s === this.bindingInputId)) {
        this.validateModel(this.ivzModel, this.bindingValidation, true);
      }
    });
    this.uiService.ddlLoadFirstFocusSubject.subscribe((vals) => {
      this.setIsLoadedOnFirstFocus(vals);
    });
  }
  ngOnInit(): void {
    super.setGetwayFieldType(this.bindingInputId, ColumnType.MASTER);
  }
  private getBindingEmitter(param: any): void {
    const ele = document.querySelector(`ivz-dropdown[inputId=${this.bindingInputId}]`);
    const $interval = interval(500).subscribe((val) => {
      if (!isUndefinedOrZeroLength(this.bindingOptions)) {
        setOptionReady(ele, true);
        if (this.bindingOptions.some((s) => s.value === param)) {
          setBindingReady(ele, true);
          this.ivzBindingChange.next(param);
          this.bindingModel = null;
          setTimeout(() => {
            this.bindingModel = param;
          }, 0);
          $interval.unsubscribe();
        } else if (this.bindingOptions.some((s) => s.value === this.bindingModel)) {
          setBindingReady(ele, true);
          $interval.unsubscribe();
        } else {
          setBindingReady(ele, false);
        }
      } else {
        setOptionReady(ele, false);
      }
    });
  }
  private setBindingReady(): void {
    const $interval = interval(100).subscribe((val) => {
      const ele = document.querySelector(`ivz-dropdown[inputId=${this.bindingInputId}]`);
      if (!isUndefinedOrZeroLength(this.bindingOptions) && !isNullOrUndefOrEmpty(ele)) {
        if (this.bindingOptions.some((s) => s.value === this.bindingModel)) {
          setBindingReady(ele, true);
          $interval.unsubscribe();
        } else {
          setBindingReady(ele, false);
          $interval.unsubscribe();
        }
      }
    });
  }

  onFocus(): void {
    const emitValue: IvzDropdownOnFocusModel = {
      inputId: this.bindingInputId,
      ivzModel: this.bindingModel,
      readonly: this.getReadonly(this.bindingInputId),
      isLoadedOnFirstFocus: this.isLoadedOnFirstFocus,
      options: this.bindingOptions
    };
    this.ivzOnFocus.next(emitValue);
  }
  private setIsLoadedOnFirstFocus(vals: IvzDropdownOnFocusModel[]): void {
    if (!isUndefinedOrZeroLength(vals)) {
      const inputId = replaceAll(this.bindingInputId, '_', '').toUpperCase();
      vals.forEach((val) => {
        if (!isUndefinedOrZeroLength(val.inputId) && val.inputId.toUpperCase() === inputId) {
          this.isLoadedOnFirstFocus = val.isLoadedOnFirstFocus;
        }
      });
    }
  }
  private setOptionReady(): void {
    const $interval = interval(100).subscribe((val) => {
      const ele = document.querySelector(`ivz-dropdown[inputId=${this.bindingInputId}]`);
      if (!isUndefinedOrZeroLength(this.bindingOptions) && !isNullOrUndefOrEmpty(ele)) {
        setOptionReady(ele, !isUndefinedOrZeroLength(this.bindingOptions));
        $interval.unsubscribe();
      }
      if (isUndefinedOrZeroLength(this.bindingOptions)) {
        $interval.unsubscribe();
      }
    });
  }
}
