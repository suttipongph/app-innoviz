import { Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { IvzBaseComponent } from 'shared/components/ivz-base/ivz-base.component';
import { ColumnType } from 'shared/constants';
import { Guid, isNullOrUndefined } from 'shared/functions/value.function';
import { TranslateModel } from 'shared/models/systemModel';
import { SelectButtonConfigModel } from 'shared/models/systemModel/ivzComponentModel';

@Component({
  selector: 'ivz-select-button',
  templateUrl: './ivz-select-button.component.html',
  styleUrls: ['./ivz-select-button.component.scss']
})
export class IvzSelectButtonComponent extends IvzBaseComponent implements OnInit {
  //#region variable
  bindingModel: any;
  bindingConfig: SelectButtonConfigModel;
  bindingOptions: any[];
  bindingIsMultiple: boolean;
  bindingValidation: TranslateModel;
  //#endregion
  //#region input props
  @Input() set ivzModel(param: any) {
    this.bindingModel = param;
    this.validateModel(this.bindingModel, this.bindingValidation);
  }
  get ivzModel(): any {
    return this.bindingModel;
  }
  @Input() set ivzValidation(param: TranslateModel) {
    this.bindingValidation = param;
    this.validationNotification(param);
  }
  @Input() set ivzConfig(param: SelectButtonConfigModel) {
    this.bindingConfig = isNullOrUndefined(param) ? new SelectButtonConfigModel() : param;
    this.bindingInputId = isNullOrUndefined(this.bindingInputId) ? Guid.newGuid() : this.bindingInputId;
    this.bindingOptions = isNullOrUndefined(this.bindingConfig.options) ? this.bindingOptions : this.bindingConfig.options;
    this.bindingIsMultiple = isNullOrUndefined(this.bindingConfig.isMultiple) ? this.bindingIsMultiple : this.bindingConfig.isMultiple;
  }
  @Input() set options(param: any[]) {
    this.bindingOptions = isNullOrUndefined(param) ? this.bindingConfig.options : param;
  }
  @Input() set multiple(param: boolean) {
    this.bindingIsMultiple = isNullOrUndefined(param) ? this.bindingConfig.isMultiple : param;
  }

  //#endregion
  //#region output props
  @Output() ivzModelChange = new EventEmitter<any>();
  //#endregion
  constructor(el: ElementRef, renderer: Renderer2) {
    super(el, renderer);
    this.validateService.$formValidate.subscribe((data) => {
      this.validateModel(this.bindingModel, this.bindingValidation, true);
    });
  }

  ngOnInit(): void {
    super.setGetwayFieldType(this.bindingInputId, ColumnType.MASTER);
  }
  ngOnDestroy(): void {}
}
