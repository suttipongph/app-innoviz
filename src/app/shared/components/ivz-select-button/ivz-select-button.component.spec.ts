import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IvzSelectButtonComponent } from './ivz-select-button.component';

describe('IvzSelectButtonComponent', () => {
  let component: IvzSelectButtonComponent;
  let fixture: ComponentFixture<IvzSelectButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IvzSelectButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IvzSelectButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
