import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IvzInputComponent } from './ivz-input-text.component';

describe('IvzInputComponent', () => {
  let component: IvzInputComponent;
  let fixture: ComponentFixture<IvzInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IvzInputComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IvzInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
