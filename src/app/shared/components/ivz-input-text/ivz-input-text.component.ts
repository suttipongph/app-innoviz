import { Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { getJelly, Guid, isNullOrUndefined, isNullOrUndefOrEmpty, isUndefinedOrZeroLength } from 'shared/functions/value.function';
import { InputTextConfigModel } from 'shared/models/systemModel/ivzComponentModel';
import { AppConst, ColumnType } from 'shared/constants';
import { TranslateModel } from 'shared/models/systemModel';
import { IvzBaseComponent } from 'shared/components/ivz-base/ivz-base.component';
import { takeWhile } from 'rxjs/operators';
import { interval } from 'rxjs';

@Component({
  selector: 'ivz-input-text',
  templateUrl: './ivz-input-text.component.html',
  styleUrls: ['./ivz-input-text.component.scss']
})
export class IvzInputTextComponent extends IvzBaseComponent implements OnInit {
  //#region variable
  bindingModel: string;
  bindingValidation: TranslateModel;
  bindingMaxlength: number;
  bindingConfig: InputTextConfigModel;
  bindingDisabled: boolean;
  //#endregion
  //#region input props
  @Input() set ivzModel(param: string) {
    this.bindingModel = isNullOrUndefined(param) ? '' : param;
    this.validateModel(this.bindingModel, this.bindingValidation);
  }
  get ivzModel(): string {
    return this.bindingModel;
  }
  @Input() set ivzValidation(param: TranslateModel) {
    this.bindingValidation = param;
    this.validationNotification(param);
  }
  @Input() set maxlength(param: number) {
    this.bindingMaxlength = isNullOrUndefined(param) ? this.bindingConfig.maxlength : param;
  }
  @Input() set ivzConfig(param: InputTextConfigModel) {
    this.bindingConfig = isNullOrUndefined(param) ? new InputTextConfigModel() : param;
    this.bindingInputId = isNullOrUndefined(this.bindingInputId) ? Guid.newGuid() : this.bindingInputId;
    this.bindingMaxlength = isNullOrUndefined(this.bindingMaxlength) ? param.maxlength : this.bindingMaxlength;
  }
  @Input() set disabled(param: boolean) {
    this.bindingDisabled = isNullOrUndefined(param) ? false : param;
  }
  //#endregion
  //#region output props
  @Output() ivzModelChange = new EventEmitter<string>();
  //#endregion
  constructor(el: ElementRef, renderer: Renderer2) {
    super(el, renderer);
    this.$subscripion.subscribe((data) => {
      if (!AppConst.SYSTEM_FIELD.some((s) => s === this.bindingInputId)) {
        this.validateModel(this.ivzModel, this.bindingValidation, true);
      }
    });
  }

  ngOnInit(): void {
    super.setGetwayFieldType(this.bindingInputId, ColumnType.STRING);
  }
  onBlur(e: any): void {
    const btn = e['relatedTarget'];
    if (!isNullOrUndefOrEmpty(btn)) {
      if (btn.id === 'SAVE' || btn.id === 'SAVE_AND_CLOSE') {
        setTimeout(() => {
          let hasRequest = !isUndefinedOrZeroLength(this.uiService.httpTrack);
          if (hasRequest) {
            interval(100)
              .pipe(takeWhile((it) => hasRequest === true))
              .subscribe((val) => {
                hasRequest = !isUndefinedOrZeroLength(this.uiService.httpTrack);
                if (!hasRequest) {
                  btn.click();
                }
              });
          }
        }, 100);
      }
    }
    this.ivzModelChange.emit(this.ivzModel);
  }
}
