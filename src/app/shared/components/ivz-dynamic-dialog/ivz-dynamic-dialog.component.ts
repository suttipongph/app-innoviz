import { Component, Input, OnInit, Type } from '@angular/core';
import { DialogService, DynamicDialogRef, MessageService } from 'shared/models/primeModel';

@Component({
  selector: 'ivz-dynamic-dialog',
  templateUrl: './ivz-dynamic-dialog.component.html',
  styleUrls: ['./ivz-dynamic-dialog.component.scss']
})
export class IvzDynamicDialogComponent implements OnInit {
  bindingComponent: Type<any>;
  @Input() set component(param: Type<any>) {
    this.bindingComponent = param;
  }
  constructor(public dialogService: DialogService, public messageService: MessageService) {}
  ref: DynamicDialogRef;
  ngOnInit(): void {}
  show() {
    this.ref = this.dialogService.open(this.bindingComponent, {
      header: 'Choose a Product',
      width: '70%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      baseZIndex: 10000
    });

    this.ref.onClose.subscribe((product: any) => {
      console.log('closed');

      // if (product) {
      //     this.messageService.add({severity:'info', summary: 'Product Selected', detail: product.name});
      // }
    });
  }

  ngOnDestroy() {
    if (this.ref) {
      this.ref.close();
    }
  }
}
