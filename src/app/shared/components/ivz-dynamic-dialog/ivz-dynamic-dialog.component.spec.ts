import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IvzDynamicDialogComponent } from './ivz-dynamic-dialog.component';

describe('IvzDynamicDialogComponent', () => {
  let component: IvzDynamicDialogComponent;
  let fixture: ComponentFixture<IvzDynamicDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IvzDynamicDialogComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IvzDynamicDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
