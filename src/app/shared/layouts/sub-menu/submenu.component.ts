import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, Input } from '@angular/core';
import { AppComponent } from 'app.component';
import { MenuItem } from 'shared/models/primeModel';
// import { AppMenuComponent } from 'shared/layouts/menu/app.menu.component';

@Component({
  selector: '[app-submenu]',
  templateUrl: './submenu.component.html',
  animations: [
    trigger('children', [
      state(
        'hiddenAnimated',
        style({
          height: '0px'
        })
      ),
      state(
        'visibleAnimated',
        style({
          height: '*'
        })
      ),
      state(
        'visible',
        style({
          height: '*',
          'z-index': 100
        })
      ),
      state(
        'hidden',
        style({
          height: '0px',
          'z-index': '*'
        })
      ),
      transition('visibleAnimated => hiddenAnimated', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
      transition('hiddenAnimated => visibleAnimated', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
    ])
  ]
})
export class AppSubMenuComponent {
  @Input() item: MenuItem;

  @Input() root: boolean;

  @Input() visible: boolean;

  _parentActive: boolean;

  _reset: boolean;

  activeIndex: number;

  animating: boolean;

  active = false;

  constructor(public app: AppComponent) {}

  ngOnInit() {
    // setTimeout(() => {
    //     console.log('item', this.item);
    // }, 2000);
    // if (!this.app.isHorizontal() && this.item.routerLink) {
    //     this.updateActiveStateFromRoute();
    // }
    // this.key = this.parentKey ? this.parentKey + '-' + this.index : String(this.index);
  }
  isActive(index: number): boolean {
    return this.activeIndex === index;
  }
  updateActiveStateFromRoute() {
    // this.active = this.router.isActive(this.item.routerLink[0], this.item.items ? false : true);
  }

  itemClick(event: Event, item: MenuItem, index: number) {
    // avoid processing disabled items
    if (item.disabled) {
      event.preventDefault();
      return true;
    }

    // navigate with hover in horizontal mode
    if (this.root) {
      this.app.menuHoverActive = !this.app.menuHoverActive;
    }

    // notify other items
    // this.menuService.onMenuStateChange(this.key);
    this.activeIndex = this.activeIndex === index ? null : index;
    // execute command
    if (item.command) {
      item.command({ originalEvent: event, item });
    }

    // toggle active state
    // if (item.items) {
    //     this.active = !this.active;
    //     this.animating = true;
    // } else {
    //     // activate item
    //     this.active = true;

    //     // hide overlay menus
    //     this.app.overlayMenuActive = false;
    //     this.app.staticMenuMobileActive = false;
    //     this.app.menuHoverActive = !this.app.menuHoverActive;

    //     // reset horizontal menu
    //     if (this.app.isHorizontal() || this.app.isSlim()) {
    //         this.app.resetMenu = true;
    //     } else {
    //         this.app.resetMenu = false;
    //     }
    // }
  }

  onMouseEnter(index: number) {
    // activate item on hover
    if (this.root && this.app.menuHoverActive && (this.app.isHorizontal() || this.app.isSlim()) && this.app.isDesktop()) {
      // this.menuService.onMenuStateChange(this.key);
      this.active = true;
    }
  }

  onAnimationDone() {
    this.animating = false;
  }

  ngOnDestroy() {
    // if (this.menuSourceSubscription) {
    //     this.menuSourceSubscription.unsubscribe();
    // }
    // if (this.menuResetSubscription) {
    //     this.menuResetSubscription.unsubscribe();
    // }
  }

  @Input() get reset(): boolean {
    return this._reset;
  }

  set reset(val: boolean) {
    this._reset = val;

    if (this._reset && (this.app.isHorizontal() || this.app.isSlim())) {
      this.activeIndex = null;
    }
  }

  @Input() get parentActive(): boolean {
    return this._parentActive;
  }

  set parentActive(val: boolean) {
    this._parentActive = val;

    if (!this._parentActive) {
      this.activeIndex = null;
    }
  }
}
