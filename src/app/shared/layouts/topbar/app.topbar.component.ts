import { Component, HostListener, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'core/services/auth.service';
import { HttpClientService } from 'core/services/httpclient.service';
import { LoggingService } from 'core/services/logging.service';
import { NotificationService } from 'core/services/notification.service';
import { UserDataService } from 'core/services/user-data.service';
import { Dialog } from 'primeng/dialog';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { NotificationListComponent } from 'shared/components/notification-list/notification-list.component';
import { ButtonConfig } from 'shared/config/format.config';
import { AppConst, LogType, ROUTE_MASTER_GEN } from 'shared/constants';
import { isUndefinedOrZeroLength } from 'shared/functions/value.function';
import { LoggingModel, TranslateModel } from 'shared/models/systemModel';
import { AppComponent } from '../../../app.component';

@Component({
  selector: 'app-topbar',
  templateUrl: './app.topbar.component.html'
})
export class AppTopBarComponent {
  @ViewChild('notificationdialog') notiDialog: Dialog;
  @ViewChild('notificationlist') notificationlist: NotificationListComponent;
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (this.showNotiList) {
      this.setNotiDialogSize(event.target.innerWidth, event.target.innerHeight);
    }
  }
  notiDialogStyle: any;
  notiDialogContentStyle: any;
  isMaximized: boolean = false;

  displayName: string = '';
  unseenCount: number = 0;
  showNotiList: boolean = false;
  messages = [];

  version: string;
  commit: string;
  api_version: string;
  api_commit: string;
  auth_version: string;
  auth_commit: string;

  defaultUserImg: string = 'assets/layout/images/avatar.png';
  imgContent: string;
  buttonConfig = ButtonConfig.SAVE;
  constructor(
    public app: AppComponent,
    private authService: AuthService,
    private router: Router,
    private userDataService: UserDataService,
    private notificationService: NotificationService,
    private loggingService: LoggingService,
    private http: HttpClientService
  ) {
    this.authService.setDisplaySubject.subscribe((event) => {
      this.displayName = this.authService.getDisplayName();
      this.setUserImage();
    });
    this.loggingService.unseenNotiCountSubject.subscribe((count) => {
      this.unseenCount = count;
    });

    this.notificationService.showNotificationMsgsSubject.subscribe((value) => {
      this.showNotiList = value;
    });
  }
  isLoggedIn() {
    return this.authService.isLoggedIn();
  }
  onLogOutClick(event): void {
    event.preventDefault();

    if (this.authService.isLoggedIn()) {
      this.authService.logOut();
    } else {
      this.router.navigate(['/']);
    }
  }
  onLoginClick(event) {
    event.preventDefault();
    this.authService.login();
  }
  onUserSettingsClick(event) {
    event.preventDefault();
    let site = this.userDataService.getSiteLogin();
    this.router.navigate([`/${site}/${ROUTE_MASTER_GEN.USERSETTINGS}`]);
  }
  onNotificationClick(event) {
    event.preventDefault();
    this.notificationService.showNotificationMsgs();
  }
  setUserImage() {
    let userImg = this.userDataService.getUserImageContent();
    if (isUndefinedOrZeroLength(userImg)) {
      this.imgContent = this.defaultUserImg;
    } else {
      this.imgContent = userImg;
    }
  }
  setNotiLogSeen() {
    let notiloglist: LoggingModel[] = this.loggingService.getAppLogs();
    let userEnabledSystemLog: boolean = this.userDataService.getShowSystemLog() === AppConst.TRUE;
    if (!isUndefinedOrZeroLength(notiloglist)) {
      if (userEnabledSystemLog) {
        for (let i = 0; i < notiloglist.length; i++) {
          if (notiloglist[i].seen === false) {
            notiloglist[i].seen = true;
          }
        }
      } else {
        for (let i = 0; i < notiloglist.length; i++) {
          if (notiloglist[i].logType === LogType.NORMAL && notiloglist[i].seen === false) {
            notiloglist[i].seen = true;
          }
        }
      }
    }
    this.loggingService.setAppLogsToStorage(notiloglist);
  }
  setNotiDialogSize(windowWidth: number, windowHeight: number) {
    let notiWindowWidth = windowWidth * 0.8;
    let notiWindowHeight = windowHeight * 0.9;
    let diff = windowWidth - notiWindowWidth;
    let notiWindowMinX = diff / 2;

    let notiwindowMinY = windowHeight * 0.1 * 0.6;

    this.notiDialogStyle = {
      width: `${notiWindowWidth}px`,
      height: `${notiWindowHeight}px`
    };

    this.notiDialogContentStyle = {
      'max-height': `${notiWindowHeight * 0.8}px`,
      'max-width': `auto`,
      'overflow-y': `auto`,
      height: `100%`
    };
    window.resizeBy(0, 0.1);
  }
  setDialogWindowState() {
    this.notificationlist.currentDetailIndex = null;
    this.isMaximized = isUndefinedOrZeroLength(this.notiDialog) ? false : this.notiDialog.maximized;
    this.notiDialogContentStyle = {
      width: '0px',
      height: '0px'
    };
    this.notiDialogStyle = {
      width: '0px',
      height: '0px'
    };
  }
  onShowSystemLogDetail(e) {
    this.setNotiDialogSize(window.innerWidth, window.innerHeight);
  }
  onShowNotiDialog() {
    this.setNotiDialogSize(window.innerWidth, window.innerHeight);
  }
  onHideNotiDialog() {
    this.setNotiLogSeen();
  }
  onNotificationListClose() {
    this.setDialogWindowState();
    this.showNotiList = false;
  }
  getApiAssemblyVersion(): Observable<string> {
    const url = `assembly/GetAssemblyVersion`;
    const result = this.http.Get(url);
    return result;
  }

  onAbout(e) {
    this.onGetAssembly().subscribe(() => {
      const topic: TranslateModel = { code: 'Application version' };
      const contents: TranslateModel[] = [];
      contents.push({ code: `App version ${this.version}` });
      contents.push({ code: `Api version ${this.api_version}` });
      this.notificationService.showInfoMultiple({ topic: topic, contents: contents }, false);
    });
  }

  onGetAssembly(): Observable<any> {
    this.version = '24.12.2021T18:00';
    this.commit = '06315afe5797097d25998316718d17abca93db1b';
    return this.getApiAssemblyVersion().pipe(
      tap((res) => {
        const arr = res.split(';');
        this.api_version = arr[1];
        this.api_commit = arr[0];
      })
      // switchMap((result): Observable<any> => {
      //   return this.getAuthAssemblyVersion().pipe(
      //     tap(res => {
      //       const arr = res.split(';');
      //       this.auth_version = arr[1];
      //       this.auth_commit = arr[0];
      //     })
      //   )
      // })
    );
  }
}
