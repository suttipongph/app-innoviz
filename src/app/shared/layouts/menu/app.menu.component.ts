import { Component, Input, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { AppComponent } from 'app.component';
import { UserDataService } from 'core/services/user-data.service';
import { AuthService } from 'core/services/auth.service';
import { UIControllerService } from 'core/services/uiController.service';
import { TranslateService } from '@ngx-translate/core';
import { isNullOrUndefined, isUndefinedOrZeroLength, transformLabel } from 'shared/functions/value.function';
import { AccessRightService } from 'core/services/access-right.service';
import { isInterfacedWithAX } from 'shared/config/globalvar.config';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import {
  ACTION_PATH,
  AppConst,
  MENU_ROUTE_MAPPING,
  MENU_ROUTE_PREFIX_MAPPING,
  ROUTE_FUNCTION_GEN,
  ROUTE_MASTER_GEN,
  ROUTE_REPORTS,
  ROUTE_REPORT_GEN
} from 'shared/constants';
import { setRoutingGateway } from 'shared/functions/routing.function';
import { PathParamModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-menu',
  templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit, AfterViewInit {
  displayCompany: string = '[Select Company][Select Branch]';

  @Input() reset: boolean;

  modelGrouped: MenuItem[];

  modelUngrouped: MenuItem[];

  site: string;

  cashierMenu: MenuItem[];

  frontMenu: MenuItem[];

  backMenu: MenuItem[];

  disableMenu: boolean = false;
  initMenu: boolean = false;
  accessRightLoaded: boolean = false;
  constructor(
    public app: AppComponent,
    private uiService: UIControllerService,
    private userDataService: UserDataService,
    private accessService: AccessRightService,
    private translate: TranslateService,
    private authService: AuthService
  ) {
    this.accessService.siteAndCompanyBranchSelected.subscribe((e) => {
      this.accessRightLoaded = e;
    });
    this.userDataService.companyChangeSubject.subscribe((e) => {
      this.setDisplayCompany();
    });
    this.authService.setDisplaySubject.subscribe((e) => {
      this.setDisplayCompany();

      if (!isUndefinedOrZeroLength(this.userDataService.getSiteLogin())) {
        this.site = this.userDataService.getSiteLogin();
        if (isUndefinedOrZeroLength(this.modelGrouped) && isUndefinedOrZeroLength(this.modelUngrouped)) {
          this.loadMenuStructure(this.site).subscribe((result) => {
            this.setMenuModel();
          });
        }
        if (this.accessRightLoaded && !this.initMenu) {
          this.modelGrouped = null;
          this.modelUngrouped = null;
          this.loadMenuStructure(this.site).subscribe((result) => {
            this.setMenuModel();

            this.initMenu = true;
          });
        }
      }
    });
    this.userDataService.languageChangeSubject.subscribe((data) => {
      if (data) {
        if (isUndefinedOrZeroLength(this.site)) {
          this.site = this.userDataService.getSiteLogin();
        }
        this.modelGrouped = null;
        this.modelUngrouped = null;
        this.loadMenuStructure(this.site).subscribe((result) => {
          this.setMenuModel();
        });
      }
    });
    this.accessService.secretSiteAcessRightLoadedSubject.subscribe((data) => {
      if (data) {
        this.site = this.userDataService.getSiteLogin();
        this.modelGrouped = null;
        this.modelUngrouped = null;
        this.loadMenuStructure(this.site).subscribe((result) => {
          this.setMenuModel();
        });
      }
    });
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {}
  isLoggedInAndSiteSelected(): boolean {
    const isLoggedIn = this.authService.isLoggedIn();
    const siteLogin = this.userDataService.getSiteLogin();
    return isLoggedIn && !isUndefinedOrZeroLength(siteLogin);
  }

  showCompanyMapping(e): void {
    const loginSite = this.userDataService.getSiteLogin();
    if (loginSite !== AppConst.CASHIER) {
      this.userDataService.showCompanyBranchSelectorSubject.next(e);
    } else {
      // not showing in cashier site
    }
  }
  setDisplayCompany(): void {
    // set display group if not permitted
    if (this.accessRightLoaded && (!this.accessService.isPermittedBySite() || !this.accessService.isPermittedByAccessRight())) {
      this.disableMenu = true;
    } else {
      if (!this.accessRightLoaded && !this.isLoggedInAndSiteSelected()) {
        this.disableMenu = true;
      } else if (this.isLoggedInAndSiteSelected() && !this.accessService.isPermittedByAccessRight()) {
        this.disableMenu = true;
      } else {
        this.disableMenu = false;
      }
    }

    const companyGUID = this.userDataService.getCurrentCompanyGUID();
    const companyId = this.userDataService.getCurrentCompanyId();

    const loginSite = this.userDataService.getSiteLogin();
    this.displayCompany = loginSite.toUpperCase() + ' / ';
    if (!isNullOrUndefined(companyId)) {
      this.displayCompany += companyId;
    } else {
      if (!isNullOrUndefined(companyGUID)) {
        this.displayCompany += companyGUID;
      } else {
        this.displayCompany += 'N/A';
      }
    }

    // display branch only in front and cashier site
    if (loginSite !== AppConst.BACK && loginSite !== AppConst.OB) {
      const branchId = this.userDataService.getCurrentBranchId();
      const branchGUID = this.userDataService.getCurrentBranchGUID();
      if (!isNullOrUndefined(branchId)) {
        this.displayCompany += ' / ' + branchId;
      } else {
        if (!isNullOrUndefined(branchGUID)) {
          this.displayCompany += ' / [' + branchGUID + ']';
        } else {
          this.displayCompany += ' / N/A';
        }
      }
    }
  }
  setMenuLabels(menuItems: any[]): void {
    if (!isUndefinedOrZeroLength(menuItems)) {
      menuItems = transformLabel(menuItems);
      for (let i = 0; i < menuItems.length; i++) {
        if (!isUndefinedOrZeroLength(menuItems[i].items)) {
          this.setMenuLabels(menuItems[i].items);
        }
      }
    }
  }

  /*
   * ============================
   *   prime ng layout behavior
   * ============================
   */

  setMenuModel(): void {
    this.modelUngrouped = null;
    this.modelUngrouped = [
      {
        label: '',
        icon: 'pi pi-fw pi-home',
        items: this.modelGrouped
      }
    ];
  }
  changeTheme(theme: string, scheme: string): void {
    const layoutLink: HTMLLinkElement = document.getElementById('layout-css') as HTMLLinkElement;
    layoutLink.href = 'assets/layout/css/layout-' + theme + '.css';

    const themeLink: HTMLLinkElement = document.getElementById('theme-css') as HTMLLinkElement;
    themeLink.href = 'assets/theme/' + theme + '/theme-' + scheme + '.css';

    const topbarLogo: HTMLImageElement = document.getElementById('layout-topbar-logo') as HTMLImageElement;

    const menuLogo: HTMLImageElement = document.getElementById('layout-menu-logo') as HTMLImageElement;

    if (theme === 'yellow' || theme === 'lime') {
      topbarLogo.src = 'assets/layout/images/logo-black.png';
      menuLogo.src = 'assets/layout/images/logo-black.png';
    } else {
      topbarLogo.src = 'assets/layout/images/logo-white.png';
      menuLogo.src = 'assets/layout/images/logo-white.png';
    }

    if (scheme === 'dark') {
      this.app.darkMenu = true;
    } else if (scheme === 'light') {
      this.app.darkMenu = false;
    }
  }

  setOpenFunction(path: string): void {
    this.uiService.setInitStored('fn');
    this.uiService.getFunctionMode(path);
  }

  setInitialFunction(param = ''): void {
    this.uiService.setRemovePrimaryUrl();
    this.uiService.getNavigator(param);
    this.uiService.setInitStored(param);
  }
  setRoutingGateway(path: string, action: string, pathPrefix: string): void {
    const param: PathParamModel = { path, action, pathPrefix };
    setRoutingGateway(param, null, [], null, false);
    this.uiService.setInitStored(path);
  }
  loadMenuStructure(site: string): Observable<MenuItem[]> {
    return this.accessService.loadMenuStructure(site).pipe(
      tap((result) => {
        const menus = result;
        this.setMenuItemValues(menus);
        this.modelGrouped = menus;
      }),
      catchError((error) => {
        console.error('failed loading menu structure: ' + JSON.stringify(error, null, 3));
        return of([]);
      })
    );
  }
  setMenuItemValues(menuItems: any[]): void {
    if (!isUndefinedOrZeroLength(menuItems)) {
      menuItems.map((item) => {
        item.disabled = this.disableMenu;
        this.setMenuCommand(item);
        this.setVisibleMenu(item);
        return item;
      });
      menuItems = transformLabel(menuItems);

      for (let i = 0; i < menuItems.length; i++) {
        if (!isUndefinedOrZeroLength(menuItems[i].items)) {
          this.setMenuItemValues(menuItems[i].items);
        }
      }
    }
  }
  setMenuCommand(item: MenuItem): void {
    const routeMapping = MENU_ROUTE_MAPPING[item.label];
    if (!isUndefinedOrZeroLength(routeMapping)) {
      if (Object.values(ROUTE_FUNCTION_GEN).findIndex((itm) => itm === routeMapping) > -1) {
        const pathPrefix = MENU_ROUTE_PREFIX_MAPPING[item.label];
        item.command = this.getInitialRouting(routeMapping, ACTION_PATH.MENUTOFUN, pathPrefix);
      } else if (Object.values(ROUTE_REPORT_GEN).findIndex((itm) => itm === routeMapping) > -1) {
        const pathPrefix = MENU_ROUTE_PREFIX_MAPPING[item.label];
        item.command = this.getInitialRouting(routeMapping, ACTION_PATH.MENUTOREPORT, pathPrefix);
      } else {
        item.command = this.getInitialRouting(routeMapping, ACTION_PATH.MENUTOLIST);
      }
    }
  }
  getInitialRouting(routeMapping, action: string = '', pathPrefix: string = ''): any {
    const caseCompany = [ROUTE_MASTER_GEN.COMPANYPARAMETER];
    if (caseCompany.some((s) => s === routeMapping)) {
      return () => this.setForCompanyParameter(routeMapping);
    } else {
      return () => this.setRoutingGateway(routeMapping, action, pathPrefix);
    }
  }
  setForCompanyParameter(routeMapping): any {
    return this.setInitialFunction(`${routeMapping}/${this.userDataService.getCurrentCompanyGUID()}`);
  }
  setVisibleMenu(item: MenuItem): void {
    // to be hidden
    const hide_menus = [];
    if (hide_menus.includes(item.label)) {
      item.visible = false;
    }

    // special condition
    if (item.label === 'MENU.SUB_MENU_INTERFACE_ACCOUNTING' || item.label === 'MENU.SUB_MENU_INTERFACE_VENDOR_INVOICE') {
      item.visible = isInterfacedWithAX();
    }
  }
}
