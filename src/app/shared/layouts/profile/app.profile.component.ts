import { Component } from '@angular/core';
import { trigger, state, transition, style, animate } from '@angular/animations';
import { Router } from '@angular/router';
import { NotificationService } from 'core/services/notification.service';
import { UIControllerService } from 'core/services/uiController.service';
import { UserDataService } from 'core/services/user-data.service';
import { ROUTE_SYSTEM } from 'shared/constants';

@Component({
  selector: 'app-inline-profile',
  templateUrl: './app.profile.component.html',
  animations: [
    trigger('menu', [
      state(
        'hidden',
        style({
          height: '0px'
        })
      ),
      state(
        'visible',
        style({
          height: '*'
        })
      ),
      transition('visible => hidden', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
      transition('hidden => visible', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
    ])
  ]
})
export class AppProfileComponent {
  active: boolean;

  constructor(private router: Router, private userDataService: UserDataService) {}

  onClick(event): void {
    this.active = !this.active;
    event.preventDefault();
  }

  onLogOutClick(event): void {
    event.preventDefault();
    this.router.navigate(['/Logout']);
  }
  onUserSettingsClick(event): void {
    event.preventDefault();
    const site = this.userDataService.getSiteLogin();
    this.router.navigate([`/${site}/${ROUTE_SYSTEM.USERSETTINGS}`]);
  }
}
