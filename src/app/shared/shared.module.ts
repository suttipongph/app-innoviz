import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PanelModule } from 'primeng/panel';
import { ButtonModule } from 'primeng/button';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { from } from 'rxjs';
import { TranslateModule } from '@ngx-translate/core';
import { IvzDataGridComponent } from './components/ivz-dataGrid/ivz-dataGrid.component';
import { TableModule } from 'primeng/table';
import { DataGridColumnPipe } from './pipes/dgColumn.pipe';
import { CheckboxModule } from 'primeng/checkbox';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { HocInputnumberComponent } from './components/hoc-inputnumber/hoc-inputnumber.component';
import { InputNumberModule } from 'primeng/inputnumber';
import { InputTextModule } from 'primeng/inputtext';
import { TabViewModule } from 'primeng/tabview';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FieldsetModule } from 'primeng/fieldset';
import { ListboxModule } from 'primeng/listbox';
import { ToastModule } from 'primeng/toast';
import { CardModule } from 'primeng/card';
import { TreeModule } from 'primeng/tree';
import { CompanyBranchSelectorComponent } from './components/company-branch-selector/company-branch-selector.component';
import { AppTopBarComponent } from './layouts/topbar/app.topbar.component';
import { AppFooterComponent } from './layouts/footer/app.footer.component';
import { AppMenuComponent } from './layouts/menu/app.menu.component';
import { AppSubMenuComponent } from './layouts/sub-menu/submenu.component';
import { AppBreadcrumbComponent } from './layouts/beadcrumb/app.breadcrumb.component';
import { AppProfileComponent } from './layouts/profile/app.profile.component';
import { IvzInputTextComponent } from './components/ivz-input-text/ivz-input-text.component';
import { IvzTextareaComponent } from './components/ivz-textarea/ivz-textarea.component';
import { IvzCalendarComponent } from './components/ivz-calendar/ivz-calendar.component';
import { IvzDropdownComponent } from './components/ivz-dropdown/ivz-dropdown.component';
import { IvzInputSwitchComponent } from './components/ivz-input-switch/ivz-input-switch.component';
import { IvzRadioButtonComponent } from './components/ivz-radio-button/ivz-radio-button.component';
import { IvzMultiSelectComponent } from './components/ivz-multi-select/ivz-multi-select.component';
import { IvzCheckboxComponent } from './components/ivz-checkbox/ivz-checkbox.component';
import { IvzInputNumberComponent } from './components/ivz-input-number/ivz-input-number.component';
import { IvzBaseComponent } from './components/ivz-base/ivz-base.component';
import { MenubarModule } from 'primeng/menubar';
import { DataDisplay } from './pipes/dataDisplay.pipe';
import { CalendarModule } from 'primeng/calendar';
import { InputSwitchModule } from 'primeng/inputswitch';
import { RadioButtonModule } from 'primeng/radiobutton';
import { IvzButtonComponent } from './components/ivz-button/ivz-button.component';
import { ReadonlyDirective } from './directives/readonly.directive';
import { ViewOnlyDirective } from './directives/viewOnly.directive';
import { RequiredDirective } from './directives/required.directive';
import { FormValidateDirective } from './directives/formValidate.directive';
import { IvzToastComponent } from './components/ivz-toast/ivz-toast.component';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { IvzListboxComponent } from './components/ivz-listbox/ivz-listbox.component';
import { ValidationDirective } from './directives/validation.directive';
import { IvzBase2Component } from './components/ivz-base2/ivz-base2.component';
import { ChartModule } from 'primeng/chart';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { IVZFileUploadComponent } from './components/ivz-file-upload/ivz-file-upload.component';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { DialogModule } from 'primeng/dialog';
import { IvzDynamicDialogComponent } from './components/ivz-dynamic-dialog/ivz-dynamic-dialog.component';
// import { IvzTieredMenuComponent } from './components/ivz-tiered-menu/ivz-tiered-menu.component';
import { PickListModule } from 'primeng/picklist';
import { StepsModule } from 'primeng/steps';
import { NotificationListComponent } from './components/notification-list/notification-list.component';
import { IvzMultiStateButtonComponent } from './components/ivz-multi-state-button/ivz-multi-state-button.component';
import { TreeTableModule } from 'primeng/treetable';
import { ToolbarModule } from 'primeng/toolbar';
import { IvzReportFormComponent } from './components/ivz-report-form/ivz-report-form.component';
import { SafePipe } from './pipes/urlSafe.pipe';
import { IvzBatchFormComponent } from './components/ivz-batch-form/ivz-batch-form.component';
import { IvzSelectButtonComponent } from './components/ivz-select-button/ivz-select-button.component';
import { SelectButtonModule } from 'primeng/selectbutton';

const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule];

const SHARED_MODULES = [TranslateModule];
const COMPONENTS = [
  IvzDataGridComponent,
  HocInputnumberComponent,
  CompanyBranchSelectorComponent,
  IvzInputTextComponent,
  IvzTextareaComponent,
  IvzCalendarComponent,
  IvzDropdownComponent,
  IvzInputSwitchComponent,
  IvzRadioButtonComponent,
  IvzMultiSelectComponent,
  IvzCheckboxComponent,
  IvzInputNumberComponent,
  IvzButtonComponent,
  IvzToastComponent,
  IvzBaseComponent,
  IvzListboxComponent,
  IvzBase2Component,
  IVZFileUploadComponent,
  IvzDynamicDialogComponent,
  NotificationListComponent,
  IvzReportFormComponent,
  IvzBatchFormComponent,
  IvzSelectButtonComponent,
  IvzMultiStateButtonComponent
];

const LAYOUTS = [AppBreadcrumbComponent, AppFooterComponent, AppMenuComponent, AppProfileComponent, AppTopBarComponent, AppSubMenuComponent];

const PIPES = [DataGridColumnPipe, DataDisplay, SafePipe];
const DIRECTIVE = [ReadonlyDirective, ViewOnlyDirective, RequiredDirective, FormValidateDirective, ValidationDirective];

const PRIMENG_MODULES = [
  PanelModule,
  ButtonModule,
  OverlayPanelModule,
  TableModule,
  CheckboxModule,
  TieredMenuModule,
  InputNumberModule,
  InputTextModule,
  DropdownModule,
  MultiSelectModule,
  FieldsetModule,
  ListboxModule,
  ToastModule,
  CardModule,
  TreeModule,
  MenubarModule,
  CalendarModule,
  InputSwitchModule,
  RadioButtonModule,
  ConfirmDialogModule,
  ChartModule,
  InputTextareaModule,
  DynamicDialogModule,
  DialogModule,
  TabViewModule,
  PickListModule,
  StepsModule,
  TreeTableModule,
  ToolbarModule,
  SelectButtonModule
];

@NgModule({
  imports: [...BASE_MODULES, ...PRIMENG_MODULES, ...SHARED_MODULES],
  exports: [...PRIMENG_MODULES, ...SHARED_MODULES, ...PIPES, ...COMPONENTS, ...DIRECTIVE, ...LAYOUTS],
  declarations: [...COMPONENTS, ...PIPES, ...DIRECTIVE, ...LAYOUTS],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
// @NgModule({
//     imports: [
//         BrowserModule,
//         BrowserAnimationsModule,
//         FormsModule,
//     ],
//     declarations: [],
//     bootstrap: []
// })
export class SharedModule {}
