import { AppInjector } from 'app-injector';
import { ExternalDataService } from 'shared/services/external-data.service';

let enumData: any;
export function setEnumData(): void {
  const enumService = AppInjector.get(ExternalDataService);
  enumService
    .loadEnumData()
    .then((response: Response) => {
      enumData = <any>response;
    })
    .catch((error) => {});
}

export function getEnumData(): any {
  return enumData;
}
