import { isNullOrUndefined, Guid, isNullOrUndefOrEmpty, popIfExist, popsIfExist } from './value.function';
import { ColumnExportModel, ColumnModel, Deletion, OptionExportModel, OptionModel, RowIdentity } from 'shared/models/systemModel';
import { IKey } from 'shared/models/systemModel';
import { AppInjector } from 'app-injector';
import { TranslateService } from '@ngx-translate/core';
import { ColumnType, ElementType, Format } from 'shared/constants';
import { Options } from 'selenium-webdriver';
import { stringToDate, stringToDateTime } from './date.function';

let models: any[];
export function modelRegister(model: any): void {
  if (isNullOrUndefined(models)) {
    models = [];
  }
  let guid = Guid.newGuid();
  let updatingIndex = -1;
  if (!isNullOrUndefined(model) && !isNullOrUndefined(model.guidStamp)) {
    updatingIndex = models.findIndex((f) => f.guidStamp === model.guidStamp);
    if (updatingIndex > -1) {
      guid = models[updatingIndex].guidStamp;
      models.splice(updatingIndex, models.length - updatingIndex);
    }
  }
  const freshModel = Object.assign({}, model);
  model.guidStamp = guid;
  freshModel.guidStamp = guid;
  models.push(freshModel);
  if (models.length > 20) {
    models.splice(0, 1);
  }
}
export function modelGetDirty(model: any): string[] {
  const keys = Object.keys(model);
  const conflicts = [];
  if (isNullOrUndefined(models)) {
    return [];
  }
  const oldModel = models.filter((m) => m.guidStamp === model.guidStamp)[0];
  if (isNullOrUndefined(oldModel)) {
    return keys;
  } else {
    keys.forEach((key) => {
      if (model[key] !== oldModel[key] && !isNullOrUndefOrEmpty(oldModel[key]) && !isNullOrUndefOrEmpty(model[key])) {
        conflicts.push(key);
      }
    });
    const system = ['createdBy', 'createdDateTime', 'modifiedBy', 'modifiedDateTime', 'companyGUID', 'companyId', 'owner', 'ownerBusinessUnitGUID'];
    popsIfExist(conflicts, system);
    return conflicts;
  }
}
export function getModelRegistered(guidStamp: string): any {
  return models.find((f) => f.guidStamp === guidStamp);
}

export function setOrderingJson(json: any[], option: OptionModel): any[] {
  const translate = AppInjector.get(TranslateService);
  let result = [];
  const newKeys = [];
  const colMappers: IKey[] = [];
  if (!isNullOrUndefined(option)) {
    option.columns.forEach((col) => {
      if (!isNullOrUndefined(col.label)) {
        translate.get(col.label).subscribe((res) => {
          col.label = res;
        });
      }
    });
    const config: ColumnModel[] = option.columns;
    config.forEach((col) => {
      if (!isNullOrUndefined(col.label)) {
        colMappers.push({
          oldValue: col.textKey,
          newValue: col.label
        });
        newKeys.push(col.label);
      }
    });
    for (let index = 0; index < json.length; index++) {
      const row = json[index];
      const newRow: any = {};
      newKeys.forEach((key) => {
        const mapper = colMappers.find((f) => f.newValue === key);
        const keyOld = isNullOrUndefined(mapper) ? key : mapper.oldValue;
        const col = config.find((f) => f.textKey === keyOld);
        if (!isNullOrUndefOrEmpty(row[keyOld])) {
          switch (col.type) {
            case ColumnType.ENUM:
              if (!isNullOrUndefined(col.masterList)) {
                const data = col.masterList.find((f) => f.value === row[keyOld]);
                if (!isNullOrUndefined(data)) {
                  newRow[key] = col.type === ColumnType.ENUM ? data.label : data;
                } else {
                  newRow[key] = '';
                }
              } else {
                newRow[key] = row[keyOld];
              }
              break;
            case ColumnType.DATETIME:
              const dateTime = stringToDateTime(row[keyOld]);
              dateTime.setSeconds(dateTime.getSeconds() - 4);
              newRow[key] = dateTime;
              break;
            case ColumnType.DATE:
              const date = stringToDate(row[keyOld]);
              date.setSeconds(date.getSeconds() - 4);
              newRow[key] = date;
              break;
            case ColumnType.DATERANGE:
              switch (col.format) {
                case Format.DATE_TIME:
                  const dateRangeDateTime = stringToDateTime(row[keyOld]);
                  dateRangeDateTime.setSeconds(dateRangeDateTime.getSeconds() - 4);
                  newRow[key] = dateRangeDateTime;
                  break;
                case Format.DATE:
                default:
                  const dateRangeDate = stringToDate(row[keyOld]);
                  dateRangeDate.setSeconds(dateRangeDate.getSeconds() - 4);
                  newRow[key] = dateRangeDate;
                  break;
              }
              break;
            default:
              newRow[key] = row[keyOld];
          }
        } else {
          newRow[key] = '';
        }
      });
      result.push(newRow);
    }
  } else {
    result = json;
  }

  return result;
}

export function toRowIdentity(del: Deletion): RowIdentity {
  const row: RowIdentity = { guid: del.guid, rowIndex: -1 };
  return row;
}

export function toDeletion(row: RowIdentity): Deletion {
  const del: Deletion = { guid: row.guid, searchParams: null };
  return row;
}

export function toMapModel(src: any, des: any) {
  const keys = Object.keys(des);
  keys.forEach((k) => {
    des[k] = src[k];
  });
}
export function focusInValidated(invalidFields: string[]): void {
  setTimeout(() => {
    if (invalidFields.length !== 0) {
      const container = document.querySelector('.invalidated');
      if (!isNullOrUndefOrEmpty(container)) {
        const ele: NodeListOf<HTMLInputElement> = container.querySelectorAll(`${ElementType.INPUT},${ElementType.P_CALENDAR},${ElementType.P_CHECKBOX},
        ${ElementType.P_DROPDOWN},${ElementType.P_INPUTSWITCH},${ElementType.P_MULTISELECT},${ElementType.P_RADIOBUTTON}
        ,${ElementType.TEXTAREA}`);
        if (!isNullOrUndefOrEmpty(ele)) {
          if (ele[0].tagName === ElementType.TEXTAREA || ele[0].tagName === ElementType.INPUT) {
            ele[0].focus();
          } else {
            const input: NodeListOf<HTMLInputElement> = ele[0].querySelectorAll(`${ElementType.INPUT}`);
            input[0].focus();
            if (ele[0].tagName === ElementType.P_CALENDAR) {
              setTimeout(() => {
                const date = document.getElementById('closeCalendar');
                if (!isNullOrUndefOrEmpty(date)) {
                  date.click();
                }
              }, 0);
            }
          }
        }
      }
    }
  }, 0);
}
