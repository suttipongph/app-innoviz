export enum DocumentProcessStatus {
  Application = '10',
  ApplicationLine = '20',
  Agreement = '30',
  AgreementAssetFees = '31',
  LetterOfRenewalJournal = '32',
  AgentJobJournal = '33',
  AgreementEarlyPayoff = '34',
  LetterOfExpirationJournal = '35',
  AgreementClassificationJournal = '36',
  ProvisionJournal = '37',
  DutyStampJournal = '38',
  RepossessionAsset = '39',
  PurchaseAgreement = '40',
  PurchaseAgreementConfirmation = '41',
  TemporaryReceipt = '50',
  Receipt = '51',
  ImportReceiptHeader = '52',
  ImportReceiptLine = '53',
  Invoice = '60',
  TaxInvoice = '61',

  Waive = '70',
  CollectionActivities = '75',
  LetterOfOverdueJournal = '76',
  CollectionFeeTable = '77',
  Collateral = '80',
  CollateralMovementJournal = '81',
  PaymentStructure = '90',
  Consortium = '102',
  CreditAppRequestTable = '110',
  AssignmentAgreement = '120',
  Cheque = '130',
  Job = '140',
  Verification = '150',
  Purchase = '160',
  MainAgreement = '170',
  GuarantorAgreement = '180',
  BusinessCollateralAgreement = '190',
  AgreementDocument = '200',
  VendorPayment = '210',
  Withdrawal = '220',
  CustomerRefund = '230',
  BookmarkDocument = '240',
  DocumentReturn = '250'
}

export enum ApplicationStatus {
  Draft = '10100',
  Submitted = '10110',
  Approved = '10120',
  Confirmed = '10130',
  Rejected = '10140',
  Cancelled = '10150'
}

export enum ApplicationLineStatus {
  Draft = '20100',
  Confirmed = '20110',
  Cancelled = '20120'
}

export enum AgreementStatus {
  Draft = '30100',
  Confirmed = '30110',
  Executed = '30120',
  Closed = '30130',
  Terminated = '30140',
  WrieOff = '30150',
  Cancelled = '30180'
}

export enum AgreementAssetFeesStatus {
  Open = '31100',
  Closed = '31110'
}

export enum LetterOfRenewalJournalStatus {
  Draft = '32100',
  Posted = '32110',
  Cancelled = '32120'
}

export enum AgentJobJournalStatus {
  Draft = '33100',
  Assign = '33110',
  Posted = '33120',
  Cancelled = '33130'
}

export enum AgreementEarlyPayoffStatus {
  Draft = '34100',
  Posted = '34110',
  Closed = '34120',
  Cancelled = '34130'
}

export enum LetterOfExpirationJournalStatus {
  Draft = '35100',
  Posted = '35110',
  Cancelled = '35120'
}

export enum AgreementClassificationJournalStatus {
  Draft = '36100',
  Posted = '36110',
  Cancelled = '36120'
}

export enum ProvisionJournalStatus {
  Draft = '37100',
  Posted = '37110',
  Cancelled = '37120'
}

export enum DutyStampJournalStatus {
  Draft = '38100',
  Posted = '38110',
  Cancelled = '38120'
}

export enum PurchaseAgreementStatus {
  Draft = '40100',
  Confirmed = '40110',
  Cancelled = '40120'
}

export enum PurchaseAgreementConfirmationStatus {
  Confirmed = '41100',
  Cancelled = '41110'
}

export enum TemporaryReceiptStatus {
  Draft = '50100',
  Posted = '50110',
  Cancelled = '50120'
}

export enum ReceiptStatus {
  Posted = '51100',
  Cancelled = '51110'
}

export enum InvoiceStatus {
  Draft = '60100',
  Posted = '60110',
  Cancelled = '60120'
}

export enum TaxInvoiceStatus {
  Posted = '61100',
  Cancelled = '61110'
}

export enum WaiveStatus {
  Draft = '70100',
  Submitted = '70110',
  Approved = '70120',
  Closed = '70130',
  Cancelled = '70140'
}

export enum CollectionActivitiesStatus {
  Open = '75100',
  Assigned = '75110',
  Closed = '75120'
}

export enum LetterOfOverdueJournalStatus {
  Draft = '76100',
  Posted = '76110',
  Cancelled = '76120'
}

export enum CollectionFeeStatus {
  Open = '77100',
  Closed = '77110',
  Cancelled = '77120'
}

export enum CollateralStatus {
  Open = '80100',
  Closed = '80110',
  Cancelled = '80120'
}

export enum CollateralMovementJournalStatus {
  Draft = '81100',
  Intransit = '81110',
  Posted = '81120'
}

export enum PaymentStructureStatus {
  Draft = '90100',
  Active = '90110',
  Inactive = '90120'
}

export enum ImportReceiptHeaderStatus {
  Draft = '52100',
  Pass = '52110',
  Error = '52120',
  Posted = '52130',
  Cancelled = '52140'
}

export enum ImportReceiptLineStatus {
  Draft = '53100',
  Pass = '53110',
  Error = '53120'
}

export enum RepossessionAssetStatus {
  WaitingForRedemption = '39100',
  WaitingForSale = '39110',
  Sold = '39120'
}

export enum CustomerStatus {
  Lead = '100100',
  Prospect = '100110',
  Customer = '100120'
}
export enum CreditAppRequestStatus {
  Draft = '110100',
  WaitingForMarketingStaff = '110110',
  WaitingForMarketingHead = '110120',
  WaitingForCreditStaff = '110130',
  WaitingForCreditHead = '110140',
  WaitingForAssistMD = '110150',
  WaitingForBoard2Of3 = '110160',
  WaitingForBoard3Of4 = '110170',
  WaitingForRetry = '110180',
  Approved = '110190',
  Rejected = '110200',
  Cancelled = '110210'
}
export enum AssignmentAgreementStatus {
  Draft = '120100',
  Posted = '120110',
  Sent = '120120',
  Signed = '120130',
  Closed = '120140',
  Cancelled = '120150'
}
export enum MainAgreementStatus {
  Draft = '170100',
  Posted = '170110',
  Sent = '170120',
  Signed = '170130',
  Closed = '170140',
  Cancelled = '170150'
}
export enum GuarantorAgreementStatus {
  Draft = '180100',
  Posted = '180110',
  Sent = '180120',
  Signed = '180130',
  Closed = '180140',
  Cancelled = '180150'
}
export enum PurchaseStatus {
  Draft = '160100',
  WaitingForOperationStaff = '160110',
  WaitingForOperationFunctionalHead = '160120',
  WaitingForAssistMD = '160130',
  WaitingForMD = '160140',
  WaitingForRetry = '160145',
  Approved = '160150',
  Rejected = '160160',
  Posted = '160170',
  Closed = '160180',
  Cancelled = '160190'
}
export enum BookMarkDocumentStatus {
  Draft = '240100',
  InProgress = '240110',
  Completed = '240120',
  Rejected = '240130'
}
export enum BusinessCollateralAgreementStatus {
  Draft = '190100',
  Posted = '190110',
  Sent = '190120',
  Signed = '190130',
  Closed = '190140',
  Cancelled = '190150'
}

export enum ChequeStatus {
  Created = '130100',
  Deposit = '130110',
  Completed = '130120',
  Bounced = '130130',
  Replaced = '130140',
  Returned = '130150',
  Cancelled = '130160'
}
export enum VerificationStatus {
  Draft = '150100',
  Approved = '150110',
  Rejected = '150120',
  Cancelled = '150130'
}
export enum ChequeDocumentStatus {
  Created = '130100',
  Deposit = '130110',
  Completed = '130120',
  Bounced = '130130',
  Replaced = '130140',
  Returned = '130150',
  Cancelled = '130160'
}
export enum FreeTextInvoiceDocumentStatus {
  Draft = '280100',
  Posted = '280110',
  Cancelled = '280120'
}
export enum ReceivedForm {
  Customer = 0,
  Buyer = 1
}
export enum WithdrawalStatus {
  Draft = '220100',
  Posted = '220110',
  Cancelled = '220120'
}
export enum MessengerJobStatus {
  Draft = '140100',
  Assigned = '140110',
  Posted = '140120',
  Cancelled = '140130'
}
export enum CustomerRefundStatus {
  Draft = '230100',
  Posted = '230110',
  Cancelled = '230120'
}
export enum VendorPaymentStatus {
  Confirmed = '210100',
  Cancelled = '210110'
}
export enum DocumentReturnStatus {
  Draft = '250100',
  Posted = '250110',
  Closed = '250120'
}
export enum IntercompanyStatus {
  Draft = '260100',
  Posted = '260110',
  Cancelled = '260120'
}
export enum IntercompanyInvoiceSettlementStatus {
  Draft = '270100',
  Posted = '270110',
  Cancelled = '270120'
}
export enum FreeTextInvoiceStatus {
  Draft = '280100',
  Posted = '280110',
  Cancelled = '280120'
}
