import { getEnumData } from 'shared/functions/external-data.function';
export const CRITERIA = getEnumData();
export const CRITERIA2 = getEnumData();
export const DETAIL_LEVEL = getEnumData();

export const ACCOUNT_TYPE = [
  { label: 'ENUM.LEDGER', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BANK', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.VENDOR', value: 2, styleClass: null, icon: null, title: null, disabled: false }
];
export const AGREEMENT_DOC_TYPE = [
  { label: 'ENUM.NEW', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.EXTEND', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.ADDENDUM', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.NOTICE_OF_CANCELLATION', value: 3, styleClass: null, icon: null, title: null, disabled: false }
];
export const AGREEMENT_REF_TYPE = [
  { label: 'ENUM.NONE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.MAIN_AGREEMENT', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.GUARANTOR_AGREEMENT', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.ASSIGNMENT_AGREEMENT', value: 3, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.COLLATERAL_AGREEMENT', value: 4, styleClass: null, icon: null, title: null, disabled: false }
];
export const APPROVAL_DECISION = [
  { label: 'ENUM.NONE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.APPROVED', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.REJECTED', value: 2, styleClass: null, icon: null, title: null, disabled: false }
];
export const ASSET_FEE_TYPE = [
  { label: 'ENUM.NONE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.VEHICLE_TAX', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.COMPULSORY', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.INSURANCE', value: 3, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.OTHER', value: 4, styleClass: null, icon: null, title: null, disabled: false }
];
export const BILLING_BY = [
  { label: 'ENUM.COMPANY', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CUSTOMER', value: 1, styleClass: null, icon: null, title: null, disabled: false }
];
export const CHEQUE_SOURCE = [
  { label: 'ENUM.NONE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.COLLECTION_FOLLOW_UP', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CHEQUE', value: 2, styleClass: null, icon: null, title: null, disabled: false }
];
export const COLLECTION_FOLLOW_UP_RESULT = [
  { label: 'ENUM.NONE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CONFIRMED', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.POSTPONE', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.EXTEND', value: 3, styleClass: null, icon: null, title: null, disabled: false }
];
export const COLLECTION_TYPE = [
  { label: 'ENUM.PRODUCT', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.OTHER', value: 1, styleClass: null, icon: null, title: null, disabled: false }
];
export const CONTACT_TO = [
  { label: 'ENUM.CUSTOMER', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BUYER', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.SPECIFIC', value: 2, styleClass: null, icon: null, title: null, disabled: false }
];
export const CONTACT_TYPE = [
  { label: 'ENUM.PHONE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.EMAIL', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.URL', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.FAX', value: 3, styleClass: null, icon: null, title: null, disabled: false }
];
export const CREDIT_APP_REQUEST_TYPE = [
  { label: 'ENUM.MAIN_CREDIT_LIMIT', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BUYER_MATCHING', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.LOAN_REQUEST', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.ACTIVE_AMEND_CUSTOMER_CREDIT_LIMIT', value: 3, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.AMEND_CUSTOMER_INFO', value: 4, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.AMEND_CUSTOMER_BUYER_CREDIT_LIMIT', value: 5, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.AMEND_BUYER_INFO', value: 6, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.REVIEW_CUSTOMER_BUYER_CREDIT_LIMIT', value: 7, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CLOSE_CUSTOMER_CREDIT_LIMIT', value: 8, styleClass: null, icon: null, title: null, disabled: false }
];
export const CREDIT_LIMIT_CONDITION_TYPE = [
  { label: 'ENUM.BY_CUSTOMER', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BY_CREDIT_APPLICATION', value: 1, styleClass: null, icon: null, title: null, disabled: false }
];
export const CREDIT_LIMIT_EXPIRATION = [
  { label: 'ENUM.BY_TRANSACTION', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BY_CREDIT_APPLICATION', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BY_BUYER_AGREEMENT', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BY_SPECIFIC', value: 3, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.NO_EXPIRATION', value: 4, styleClass: null, icon: null, title: null, disabled: false }
];
export const DIMENSION = [
  {
    label: 'ENUM.DIMENSION_DIMENSION1',
    value: 0,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.DIMENSION_DIMENSION2',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.DIMENSION_DIMENSION3',
    value: 2,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.DIMENSION_DIMENSION4',
    value: 3,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.DIMENSION_DIMENSION5',
    value: 4,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
export const DOC_CON_VERIFY_TYPE = [
  { label: 'ENUM.BILLING', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.RECEIPT', value: 1, styleClass: null, icon: null, title: null, disabled: false }
];
export const DOCUMENT_TEMPLATE_TYPE = [
  { label: 'ENUM.NONE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.MAIN_AGREEMENT_SHARED', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.MAIN_AGREEMENT_CONSORTIUM', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.MAIN_AGREEMENT_JV', value: 3, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.MAIN_AGREEMENT_PF', value: 4, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.MAIN_AGREEMENT_EXPENSE_NOTICE', value: 5, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.ADDENDUM_MAIN_AGREEMENT', value: 6, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.ASSIGNMENT_AGREEMENT', value: 7, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CANCEL_ASSIGNMENT_AGREEMENT', value: 8, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BUSINESS_COLLATERAL_AGREEMENT', value: 9, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.EXTEND_NOTES_BUS_COLLATERAL_AGM', value: 10, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BUS_COLLATERAL_AGM_EXPENSE_NOTICE', value: 11, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.LETTER_REQ_CANCEL_BUS_COLLATERAL_AGM', value: 12, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.GUARANTOR_AGREEMENT_THAI_PERSON', value: 13, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CROSS_GUARANTOR_AGREEMENT', value: 14, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CUSTOMER_AMEND_CREDIT_INFORMATION', value: 15, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CUSTOMER_AMEND_CREDIT_LIMIT', value: 16, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CUSTOMER_CREDIT_APPLICATION_SHARED', value: 17, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CUSTOMER_CREDIT_APPLICATION_PF', value: 18, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CREDIT_APPLICATION_CONSIDERATION', value: 19, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BUYER_AMEND_CREDIT_INFORMATION', value: 20, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BUYER_AMEND_CREDIT_LIMIT', value: 21, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BUYER_CREDIT_APPLICATION_PRIVATE', value: 22, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BUYER_REVIEW_CREDIT_LIMIT', value: 23, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CREDIT_LIMIT_CLOSING', value: 24, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.GUARANTOR_AGREEMENT_FOREIGNER', value: 25, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.GUARANTOR_AGREEMENT_ORGANIZATION', value: 26, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BUYER_CREDIT_APPLICATION_GOVERNMENT', value: 27, styleClass: null, icon: null, title: null, disabled: false }
];
export const IDENTIFICATION_TYPE = [
  { label: 'ENUM.TAX_ID', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.PASSPORT_ID', value: 1, styleClass: null, icon: null, title: null, disabled: false }
];
export const INTEREST_CALCULATION_METHOD = [
  { label: 'ENUM.MINIMUM', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.ACTUAL', value: 1, styleClass: null, icon: null, title: null, disabled: false }
];
export const METHOD_OF_BILLING = [
  { label: 'ENUM.BEFORE_PURCHASE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.AFTER_PURCHASE', value: 1, styleClass: null, icon: null, title: null, disabled: false }
];
export const PAID_TO_TYPE = [
  { label: 'ENUM.CUSTOMER', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.VENDOR', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.SUSPENSE', value: 2, styleClass: null, icon: null, title: null, disabled: false }
];
export const PAYMENT_TYPE = [
  { label: 'ENUM.LEDGER', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.TRANSFER', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CHEQUE', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.SUSPENSE_ACCOUNT', value: 3, styleClass: null, icon: null, title: null, disabled: false }
];
export const PRIORITY = [
  { label: 'ENUM.NORMAL', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.HIGH', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CRITICAL', value: 2, styleClass: null, icon: null, title: null, disabled: false }
];
export const PRODUCT_TYPE = [
  { label: 'ENUM.NONE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.FACTORING', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.PROJECT_FINANCE', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.LEASING', value: 3, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.HIRE_PURCHASE', value: 4, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BOND', value: 5, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.LC_DLC', value: 6, styleClass: null, icon: null, title: null, disabled: false }
];
export const PURCHASE_FEE_CALCULATE_BASE = [
  { label: 'ENUM.INVOICE_AMOUNT', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.PURCHASE_AMOUNT', value: 1, styleClass: null, icon: null, title: null, disabled: false }
];
export const RECEIPT_TEMP_REF_TYPE = [
  { label: 'ENUM.NORMAL', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.IMPORTED', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.WAIVE', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.DISCOUNT', value: 3, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.PRODUCT', value: 4, styleClass: null, icon: null, title: null, disabled: false }
];
export const RECEIVED_FROM = [
  { label: 'ENUM.CUSTOMER', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BUYER', value: 1, styleClass: null, icon: null, title: null, disabled: false }
];
export const RECORD_TYPE = [
  { label: 'ENUM.PERSON', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.ORGANIZATION', value: 1, styleClass: null, icon: null, title: null, disabled: false }
];
export const REF_TYPE = [
  { label: 'ENUM.COMPANY', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.PROSPECT', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CUSTOMER', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.GUARANTOR', value: 3, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.VENDOR', value: 4, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.APPLICATION', value: 5, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.AGREEMENT', value: 7, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.INVOICE', value: 8, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.PAYMENT', value: 9, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.PURCHASE_AGREEMENT_CONFIRM', value: 10, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.AGREEMENT_CLASSIFICATION_JOURNAL', value: 11, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.RECEIPT_TEMP', value: 12, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.WAIVE', value: 13, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.APPLICATION_ASSET', value: 14, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.AGREEMENT_ASSET', value: 15, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.AGREEMENT_ASSET_FEES', value: 16, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.COLLATERAL', value: 17, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.PRODUCT', value: 18, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.ADDRESS', value: 19, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CUST_BANK', value: 20, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CONTACT', value: 21, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.AUTHORIZED_PERSON_TRANS', value: 22, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.GUARANTOR_TRANS', value: 23, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.WITNESS_TRANS', value: 24, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CUST_TRANS', value: 25, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.COLLATERAL_MOVEMENT_JOURNAL', value: 26, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.PROVISION_JOURNAL', value: 28, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.AGREEMENT_TRANS_INCOME_REALIZATION', value: 29, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.AGREEMENT_EARLY_PAYOFF', value: 30, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BRANCH', value: 31, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.DOCUMENT_CHECK_LIST', value: 32, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.COLLECTION_ACTIVITY_TRANS', value: 33, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.DUTY_STAMP_JOURNAL', value: 34, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.ALL_AGREEMENT_EARLY_PAY_OFF', value: 35, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.PURCH_AGREEMENT_TABLE', value: 36, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.VEND_BANK', value: 37, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BUYER', value: 38, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CREDIT_APP_REQUEST_TABLE', value: 39, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CREDIT_APP_TABLE', value: 40, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CREDIT_APP_REQUEST_LINE', value: 41, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CREDIT_APP_LINE', value: 42, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.ASSIGNMENT_AGREEMENT', value: 43, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CHEQUE', value: 44, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.PURCHASE_TABLE', value: 45, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.PURCHASE_LINE', value: 46, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.MAIN_AGREEMENT', value: 47, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.GUARANTOR_AGREEMENT', value: 48, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BUSINESS_COLLATERAL_AGREEMENT', value: 49, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BUYER_AGREEMENT', value: 50, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.PAYMENT_DETAIL', value: 51, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.COLLECTION_FOLLOW_UP', value: 53, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.RELATED_PERSON', value: 54, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.INVOICE_SETTLEMENT_DETAIL', value: 55, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.WITHDRAWAL_TABLE', value: 56, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.WITHDRAWAL_LINE', value: 57, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CUSTOMER_REFUND', value: 58, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BLACKLIST', value: 59, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.VERIFICATION', value: 60, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.EMPLOYEE', value: 61, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.SERVICE_FEE_TRANS', value: 62, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.MESSENGER_JOB', value: 63, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.VENDOR_PAYMENT_TRANS', value: 64, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.PROJECT_PROGRESS', value: 65, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.DOCUMENT_RETURN', value: 66, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.INTEREST_REALIZED_TRANS', value: 67, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.FREE_TEXT_INVOICE', value: 68, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.INTERCOMPANY_INVOICE_ADJUSTMENT', value: 69, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.ASSIGNMENT_AGREEMENT_SETTLE', value: 70, styleClass: null, icon: null, title: null, disabled: false }
];
export const RESULT = [
  { label: 'ENUM.NONE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.COMPLETED', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.FAILED', value: 2, styleClass: null, icon: null, title: null, disabled: false }
];
export const RETENTION_CALCULATE_BASE = [
  { label: 'ENUM.NONE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.INVOICE_AMOUNT', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.PURCHASE_AMOUNT', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.FIXED_AMOUNT', value: 3, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.NET_RESERVE_AMOUNT', value: 4, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.WITHDRAWAL_AMOUNT', value: 5, styleClass: null, icon: null, title: null, disabled: false }
];
export const RETENTION_DEDUCTION_METHOD = [
  { label: 'ENUM.PURCHASE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.WITHDRAWAL', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.RESERVE_REFUND', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CUSTOMER_SPECIFIC', value: 3, styleClass: null, icon: null, title: null, disabled: false }
];
export const SERVICE_FEE_CATEGORY = [
  { label: 'ENUM.NONE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.AGREEMENT', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BILLING', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.RECEIPT', value: 3, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.RETENTION', value: 4, styleClass: null, icon: null, title: null, disabled: false }
];
export const SUSPENSE_INVOICE_TYPE = [
  { label: 'ENUM.NONE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.SUSPENSE_ACCOUNT', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.TO_BE_REFUNDED', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.RETENTION', value: 3, styleClass: null, icon: null, title: null, disabled: false }
];
export const DEFAULT_BIT = [
  { label: 'ENUM.DEFAULT_BIT_ALL', value: null, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.DEFAULT_BIT_TRUE', value: true, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.DEFAULT_BIT_FALSE', value: false, styleClass: null, icon: null, title: null, disabled: false }
];
export const NUMBER_SEQ_SEGMENT_TYPE = [
  { label: 'ENUM.NUMBER_SEQ_SEGMENT_TYPE_CONSTANT', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.NUMBER_SEQ_SEGMENT_TYPE_NUMBER', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.NUMBER_SEQ_SEGMENT_TYPE_VARIABLE1', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.NUMBER_SEQ_SEGMENT_TYPE_VARIABLE2', value: 3, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.NUMBER_SEQ_SEGMENT_TYPE_VARIABLE3', value: 4, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.NUMBER_SEQ_SEGMENT_TYPE_COMPANY_VARIABLE', value: 5, styleClass: null, icon: null, title: null, disabled: false }
];
export const BOOKMARK_DOCUMENT_REF_TYPE = [
  { label: 'ENUM.NONE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.MAIN_AGREEMENT', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.GUARANTOR_AGREEMENT', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.ASSIGNMENT_AGREEMENT', value: 3, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BUSINESS_COLLATERAL_AGREEMENT', value: 4, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CREDIT_APP_REQUEST', value: 5, styleClass: null, icon: null, title: null, disabled: false }
];
export const DAY_OF_MONTH = [
  { label: 'ENUM.NONE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D01', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D02', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D03', value: 3, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D04', value: 4, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D05', value: 5, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D06', value: 6, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D07', value: 7, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D08', value: 8, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D09', value: 9, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D10', value: 10, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D11', value: 11, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D12', value: 12, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D13', value: 13, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D14', value: 14, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D15', value: 15, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D16', value: 16, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D17', value: 17, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D18', value: 18, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D19', value: 19, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D20', value: 20, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D21', value: 21, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D22', value: 22, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D23', value: 23, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D24', value: 24, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D25', value: 25, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D26', value: 26, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D27', value: 27, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D28', value: 28, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D29', value: 29, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D30', value: 30, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.D31', value: 31, styleClass: null, icon: null, title: null, disabled: false }
];
export const MONTH_OF_YEAR = [
  { label: 'ENUM.NONE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.M01', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.M02', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.M03', value: 3, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.M04', value: 4, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.M05', value: 5, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.M06', value: 6, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.M07', value: 7, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.M08', value: 8, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.M09', value: 9, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.M10', value: 10, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.M11', value: 11, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.M12', value: 12, styleClass: null, icon: null, title: null, disabled: false }
];
export const WORKING_DAY = [
  { label: 'ENUM.MON_TO_FRI', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.MON_TO_SAT', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.MON_TO_SUN', value: 2, styleClass: null, icon: null, title: null, disabled: false }
];
export const HOLIDAY_TYPE = [
  { label: 'ENUM.PUBLIC_HOLIDAY', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.COMPANY_HOLIDAY', value: 1, styleClass: null, icon: null, title: null, disabled: false }
];

export const CALC_INTEREST_METHOD = [
  { label: 'ENUM.NONE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.NEXT_COMPANY_CALENDAR', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.NEXT_BOT_CALENDAR', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.NEXT_MIN_CALENDAR', value: 3, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.NEXT_MAX_CALENDAR', value: 4, styleClass: null, icon: null, title: null, disabled: false }
];
export const CALC_INTEREST_DAY_METHOD = [
  { label: 'ENUM.NONE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.NEXT_DAY', value: 1, styleClass: null, icon: null, title: null, disabled: false }
];
export const INVOICE_ISSUING = [
  {
    label: 'ENUM.INVOICE_ISSUING_COMPANY',
    value: 0,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.INVOICE_ISSUING_CUSTOMER',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.INVOICE_ISSUING_AGREEMENT',
    value: 2,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
export const PAYM_STRUCTURE_REF_NUM = [
  {
    label: 'ENUM.PAYM_STRUCTURE_REF_NUM_NONE',
    value: 0,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.PAYM_STRUCTURE_REF_NUM_REF_NUM1',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.PAYM_STRUCTURE_REF_NUM_REF_NUM2',
    value: 2,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
export const PENALTY_BASE = [
  {
    label: 'ENUM.PENALTY_BASE_PAYMENT',
    value: 0,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.PENALTY_BASE_PAYMENT_INC_TAX',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
export const RECEIPT_GEN_BY = [
  {
    label: 'ENUM.RECEIPT_GEN_BY_AGREEMENT',
    value: 0,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.RECEIPT_GEN_BY_AGREEMENT_INVOICE_TYPE',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.RECEIPT_GEN_BY_AGREEMENT_INVOICE',
    value: 2,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
export const COLLECTION_FEE_METHOD = [
  {
    label: 'ENUM.COLLECTION_FEE_METHOD_DAY',
    value: 0,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.COLLECTION_FEE_METHOD_PERIOD',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
export const NPL_METHOD = [
  {
    label: 'ENUM.NPL_METHOD_DAY',
    value: 0,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.NPL_METHOD_PERIOD',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
export const PROVISION_METHOD = [
  {
    label: 'ENUM.PROVISION_METHOD_DAY',
    value: 0,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.PROVISION_METHOD_PERIOD',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
export const PROVISION_RATE_BY = [
  {
    label: 'ENUM.PROVISION_RATE_BY_CUSTOMER',
    value: 0,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.PROVISION_RATE_BY_AGREEMENT',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
export const OVERDUE_TYPE = [
  {
    label: 'ENUM.OVERDUE_TYPE_DAY',
    value: 0,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.OVERDUE_TYPE_PERIOD',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
export const PENALTY_CALCULATION_METHOD = [
  {
    label: 'ENUM.PENALTY_CALCULATION_METHOD_OUTSTANDING_BALANCE_OF_INSTALLMENT_INVOICE',
    value: 0,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.PENALTY_CALCULATION_METHOD_ALL_INSTALLMENT_INVOICE',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
export const COMPANY_SIGNATURE_REF_TYPE = [
  {
    label: 'ENUM.AUTHORIZED_PERSON',
    value: 0,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.WITNESS',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
export const SETTLE_INVOICE_CRITERIA = [
  {
    label: 'ENUM.SETTLE_INVOICE_CRITERIA_NONE',
    value: 0,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.SETTLE_INVOICE_CRITERIA_SETTLEMENT_BY_MONTH',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.SETTLE_INVOICE_CRITERIA_SETTLEMENT_BY_DAY',
    value: 2,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
export const LEDGER_FISCAL_PERIOD_STATUS = [
  {
    label: 'ENUM.LEDGER_FISCAL_PERIOD_STATUS_OPEN',
    value: 0,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.LEDGER_FISCAL_PERIOD_STATUS_CLOSED',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
export const LEDGER_FISCAL_PERIOD_UNIT = [
  {
    label: 'ENUM.LEDGER_FISCAL_PERIOD_UNIT_MONTH',
    value: 0,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.LEDGER_FISCAL_PERIOD_UNIT_YEAR',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
export const SYS_LANGUAGE = [
  {
    label: 'ENUM.SYS_LANGUAGE_EN',
    value: 'en',
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.SYS_LANGUAGE_TH',
    value: 'th',
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
export const CUST_TRANS_STATUS = [
  { label: 'ENUM.OPEN', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  {
    label: 'ENUM.CLOSED',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
export const STAGING_BATCH_STATUS = [
  { label: 'ENUM.NONE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.SUCCESS', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.FAILED', value: 2, styleClass: null, icon: null, title: null, disabled: false }
];
export const PROCESS_TRANS_TYPE = [
  { label: 'ENUM.DOWN_PAYM', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.ADVANCE_PAYM', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.DEPOSIT', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.DEPOSIT_RETURN', value: 3, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.EXECUTION', value: 4, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.INSTALLMENT', value: 5, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.INVOICE', value: 6, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.PENALTY', value: 7, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.COLLECTION', value: 8, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.TAX_REALIZATION', value: 9, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.INCOME_REALIZATION', value: 10, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.INCOME_REALIZATION_NPL', value: 11, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.INCOME_REALIZATION_TERMINATE', value: 12, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.INCOME_REALIZATION_WRITE_OFF', value: 13, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.PAYMENT', value: 14, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.CLOSING', value: 15, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.TERMINATION', value: 16, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.WRITE_OFF', value: 17, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.SELLING', value: 18, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.DISPOSAL', value: 19, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.PURCHASE_AGREEMENT', value: 20, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.VENDOR_PAYMENT', value: 21, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.PAYMENT_DETAIL', value: 22, styleClass: null, icon: null, title: null, disabled: false }
];
export const TAX_INVOICE_REF_TYPE = [
  { label: 'ENUM.NONE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.INVOICE', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.RECEIPT', value: 2, styleClass: null, icon: null, title: null, disabled: false }
];
export const WITHDRAWAL_LINE_TYPE = [
  { label: 'ENUM.PRINCIPAL', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.INTEREST', value: 1, styleClass: null, icon: null, title: null, disabled: false }
];
export const VERIFY_TYPE = [
  { label: 'ENUM.NONE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.ASSIGNMENT', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.BILLING', value: 2, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.RECEIPT', value: 3, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.SUPPLIER', value: 4, styleClass: null, icon: null, title: null, disabled: false }
];
export const BUSINESS_SEGMENT_TYPE = [
  { label: 'ENUM.PERSON', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.PRIVATE', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.GOVERNMENT', value: 2, styleClass: null, icon: null, title: null, disabled: false }
];
export const INTERFACE_STATUS = [
  { label: 'ENUM.NONE', value: 0, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.SUCCESS', value: 1, styleClass: null, icon: null, title: null, disabled: false },
  { label: 'ENUM.FAILED', value: 2, styleClass: null, icon: null, title: null, disabled: false }
];
export const SITE_LOGIN_TYPE = [
  {
    label: 'ENUM.SITE_LOGIN_TYPE_FRONT',
    value: 0,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.SITE_LOGIN_TYPE_BACK',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.SITE_LOGIN_TYPE_CASHIER',
    value: 2,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
export const BATCH_MINUTES = [
  { label: '5', value: 5 },
  { label: '10', value: 10 },
  { label: '20', value: 20 },
  { label: '30', value: 30 }
];
export const BATCH_HOURS = [
  { label: '1', value: 1 },
  { label: '2', value: 2 },
  { label: '3', value: 3 },
  { label: '4', value: 4 },
  { label: '6', value: 6 },
  { label: '8', value: 8 },
  { label: '12', value: 12 }
];
export const BATCH_DAYS = [{ label: '1', value: 1 }];
export const BATCH_MONTHS = [
  { label: '1', value: 1 },
  { label: '2', value: 2 },
  { label: '3', value: 3 },
  { label: '4', value: 4 },
  { label: '6', value: 6 }
];
export const BATCH_YEARS = [{ label: '1', value: 1 }];
export const BATCH_DAY_OF_WEEK = [
  {
    label: 'ENUM.BATCH_DAY_OF_WEEK_SUN',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_DAY_OF_WEEK_MON',
    value: 2,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_DAY_OF_WEEK_TUE',
    value: 3,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_DAY_OF_WEEK_WED',
    value: 4,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_DAY_OF_WEEK_THU',
    value: 5,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_DAY_OF_WEEK_FRI',
    value: 6,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_DAY_OF_WEEK_SAT',
    value: 7,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
export const BATCH_INSTANCE_STATE = [
  {
    label: 'ENUM.BATCH_INSTANCE_STATE_EXECUTING',
    value: 0,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_INSTANCE_STATE_COMPLETED',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_INSTANCE_STATE_FAILED',
    value: 2,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_INSTANCE_STATE_RETRIED',
    value: 3,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_INSTANCE_STATE_CANCELLED',
    value: 4,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_INSTANCE_STATE_RETRYING',
    value: 5,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_INSTANCE_STATE_RETRY_COMPLETED',
    value: 6,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_INSTANCE_STATE_RETRY_FAILED',
    value: 7,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
export const BATCH_INTERVAL_TYPE = [
  {
    label: 'ENUM.BATCH_INTERVAL_TYPE_MINUTES',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_INTERVAL_TYPE_HOURS',
    value: 2,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_INTERVAL_TYPE_DAYS',
    value: 3,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_INTERVAL_TYPE_DAY_OF_WEEK',
    value: 6,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_INTERVAL_TYPE_MONTHS',
    value: 4,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_INTERVAL_TYPE_YEARS',
    value: 5,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
export const BATCH_JOB_STATUS = [
  {
    label: 'ENUM.BATCH_JOB_STATUS_WAITING',
    value: 0,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_JOB_STATUS_EXECUTED',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_JOB_STATUS_ENDED',
    value: 2,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_JOB_STATUS_ON_HOLD',
    value: 3,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_JOB_STATUS_CANCELLED',
    value: 4,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];

export const BATCH_RESULT_STATUS = [
  {
    label: 'ENUM.BATCH_RESULT_STATUS_FAIL',
    value: 0,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BATCH_RESULT_STATUS_SUCCESS',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
export const SHOW_DOC_CON_VERIFY_TYPE = [
  {
    label: 'ENUM.NONE',
    value: 0,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.BILLING',
    value: 1,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  },
  {
    label: 'ENUM.RECEIPT',
    value: 2,
    styleClass: null,
    icon: null,
    title: null,
    disabled: false
  }
];
