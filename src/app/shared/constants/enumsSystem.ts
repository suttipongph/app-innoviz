export enum itemViewMode {
  create = 0,
  update = 1,
  view = 2
}

export enum ColumnType {
  STRING = 'STRING',
  INT = 'INT',
  DATE = 'DATE',
  DATERANGE = 'DATERANGE',
  BOOLEAN = 'BOOLEAN',
  MASTER = 'MASTER',
  ENUM = 'ENUM',
  DECIMAL = 'DECIMAL',
  VARIABLE = 'VARIABLE',
  VARIABLES = 'VARIABLES',
  MASTERSINGLE = 'MASTERSINGLE',
  CONTROLLER = 'CONTROLLER',
  HIDDEN = 'HIDDEN',
  DATETIME = 'DATETIME',
  NONE = 'NONE'
}

export enum SortType {
  ASC = 1,
  DESC = 0,
  NONE = null
}
export enum BracketType {
  None = 0,
  SingleStart = 1,
  SingleEnd = 2,
  DoubleStart = 3,
  DoubleEnd = 4
}
export enum PaginationSeatType {
  Begin,
  First,
  Second,
  Third,
  Fourth,
  Fifth,
  Less,
  More,
  End
}

export enum Ordinal {
  Clear,
  System,
  First,
  Second,
  Third,
  Fourth,
  Fifth,
  Sixth,
  Seventh,
  Eighth,
  Ninth,
  Tenth,
  E11,
  E12,
  E13,
  E14,
  E15,
  E16,
  E17,
  E18,
  E19,
  E20,
  Batch01,
  Batch02,
  Batch03,
  Batch04,
  Batch05,
  Batch06,
  Batch07,
  ViewOnly
}
export enum ElementType {
  P_DROPDOWN = 'P-DROPDOWN',
  P_CALENDAR = 'P-CALENDAR',
  INPUT = 'INPUT',
  TEXTAREA = 'TEXTAREA',
  BUTTON = 'BUTTON',
  P_RADIOBUTTON = 'P-RADIOBUTTON',
  P_CHECKBOX = 'P-CHECKBOX',
  IV_PERCENT_INPUT = 'IV-PERCENT-INPUT',
  P_INPUTSWITCH = 'P-INPUTSWITCH',
  P_MULTISELECT = 'P-MULTISELECT',
  P_SELECTBUTTON = 'P-SELECTBUTTON',
  COMMENT = '#comment',
  P_TIEREDMENU = 'P-TIEREDMENU',
  A = 'A',
  SPAN = 'SPAN',
  P_INPUTNUMBER = 'P-INPUTNUMBER',
  LABEL = 'LABEL'
}

export enum AccessMode {
  noAccess,
  viewer,
  editor,
  creator,
  full
}

export enum PathType {
  LIST = 'list',
  ITEM = 'item'
}
export enum MaskType {
  NUMBER,
  CURRENCY,
  PERCENT
}

export enum BranchFilterType {
  BYSPECIFICBRANCH = 'BySpecificBranch',
  BYALLBRANCH = 'ByAllBranch',
  BYCOMPANY = 'ByCompany'
}

export enum LogType {
  SYSTEM,
  NORMAL
}

export enum ACTIONTYPE {
  HTTP_GETLIST
}
export enum HTTPTYPE {
  REQUESTING,
  RESPONSED,
  CLIENTPROCESS
}
export enum AccessLevel {
  None,
  User,
  BusinessUnit,
  ParentChildBU,
  Company
}
export enum SysFeatureType {
  primary,
  relatedInfo,
  function,
  report,
  workflow,
  workflowActionHistory
}
export enum K2WorklistStatus {
  Available = 'Available',
  Open = 'Open',
  Allocated = 'Allocated',
  Sleep = 'Sleep',
  Completed = 'Completed'
}
export enum BatchDayOfWeek {
  SUN = 1,
  MON = 2,
  TUE = 3,
  WED = 4,
  THU = 5,
  FRI = 6,
  SAT = 7
}
export enum BatchEndDateType {
  NO_ENDDATE,
  RECURRING,
  END_BY
}
export enum BatchInstanceState {
  EXECUTING,
  COMPLETED,
  FAILED,
  RETRIED,
  CANCELLED,
  RETRYING,
  RETRY_COMPLETED,
  RETRY_FAILED,
  FAILED_SERVER_RESTARTED,
  RETRY_FAILED_SERVER_RESTARTED
}
export enum BatchIntervalType {
  SECONDS = 0,
  MINUTES = 1,
  HOURS = 2,
  DAYS = 3,
  MONTHS = 4,
  YEARS = 5,
  DAYOFWEEK = 6
}
export enum BatchJobStatus {
  WAITING,
  EXECUTING,
  ENDED,
  ON_HOLD,
  CANCELLED
}
export enum BatchResultStatus {
  FAIL,
  SUCCESS
}
export enum Format {
  DATE = 'DATE',
  DATE_TIME = 'DATETIME'
}
