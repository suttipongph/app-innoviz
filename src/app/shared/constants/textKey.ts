export const ADDRESS_COUNTRY = {
  PK: 'countryId',
  FK: 'addressCountry_CountryId'
};
export const ADDRESS_DISTRICT = {
  PK: 'districtId',
  FK: 'addressDistrictGUID'
};
export const ADDRESS_POSTAL_CODE = {
  PK: 'postalCode',
  FK: 'addressPostalCode_PostalCode'
};
export const ADDRESS_PROVINCE = {
  PK: 'provinceId',
  FK: 'addressProvinceGUID'
};
export const ADDRESS_SUB_DISTRICT = {
  PK: 'subDistrictId',
  FK: 'addressSubDistrictGUID'
};
export const ADDRESS_TRANS = {
  PK: 'name',
  FK: 'addressTrans_Name'
};
export const AGENT_JOB_JOURNAL = {
  PK: 'journalId',
  FK: 'agentJobJournal_JournalId'
};
export const AGREEMENT_ASSET = {
  PK: 'agreementAssetId',
  FK: 'agreementAsset_AgreementAssetId'
};
export const AGREEMENT_ASSET_FEES = {
  PK: 'agreementAssetFeesId',
  FK: 'agreementAssetFees_AgreementAssetFeesId'
};
export const AGREEMENT_CLASSIFICATION_JOURNAL = {
  PK: 'journalId',
  FK: 'agreementClassificationJournal_JournalId'
};
export const AGREEMENT_OVERRULE_PAYM_STRUCTURE = {
  PK: 'autoSettlementBatchId',
  FK: 'agreementOverrulePaymStructure_AutoSettlementBatchId'
};
export const AGREEMENT_REVISION_HISTORY = {
  PK: 'fieldName',
  FK: 'agreementRevisionHistory_FieldName'
};
export const AGREEMENT_TABLE = {
  PK: 'agreementId',
  FK: 'agreementTable_AgreementId'
};
export const AGREEMENT_TRANS = {
  PK: 'autoSettlementBatchId',
  FK: 'agreementTrans_AutoSettlementBatchId'
};
export const AGREEMENT_TYPE = {
  PK: 'agreementTypeId',
  FK: 'agreementType_AgreementTypeId'
};
export const APPLICATION_TABLE = {
  PK: 'applicationId',
  FK: 'applicationTable_ApplicationId'
};
export const ASSET_FLEX_SETUP = {
  PK: 'assetFlexId',
  FK: 'assetFlexSetupGUID'
};
export const ASSET_GROUP = {
  PK: 'assetGroupId',
  FK: 'assetGroupGUID'
};
export const AUTHORIZED_PERSON_TRANS = {
  PK: 'name',
  FK: 'authorizedPersonTrans_Name'
};
export const BANK_GROUP = {
  PK: 'bankGroupId',
  FK: 'bankGroup_BankGroupId'
};
export const BILLING_NOTES_TABLE = {
  PK: 'billingNotesId',
  FK: 'billingNotesTable_BillingNotesId'
};
export const BRANCH = {
  PK: 'branchId',
  FK: 'branchGUID'
};
export const CAMPAIGN_TABLE = {
  PK: 'campaignId',
  FK: 'campaignTableGUID'
};
export const COLLATERAL_LOCATION = {
  PK: 'collateralLocationId',
  FK: 'collateralLocationGUID'
};
export const COLLATERAL_MOVEMENT_JOURNAL = {
  PK: 'journalId',
  FK: 'collateralMovementJournal_JournalId'
};
export const COLLATERAL_TABLE = {
  PK: 'collateralId',
  FK: 'collateralTable_CollateralId'
};
export const COLLATERAL_TYPE = {
  PK: 'collateralTypeId',
  FK: 'collateralTypeGUID'
};
export const COLLECTION_ACTIVITY = {
  PK: 'collectionActivityId',
  FK: 'collectionActivity_CollectionActivityId'
};
export const COLLECTION_ACTIVITY_RESULT = {
  PK: 'resultId',
  FK: 'collectionActivityResult_ResultId'
};
export const COLLECTION_ACTIVITY_TRANS = {
  PK: 'processInstanceId',
  FK: 'collectionActivityTrans_ProcessInstanceId'
};
export const COLLECTION_FEE_GROUP = {
  PK: 'collectionFeeGroupId',
  FK: 'collectionFeeGroupGUID'
};
export const COLLECTION_FEE_TABLE = {
  PK: 'collectionFeeId',
  FK: 'collectionFeeTable_CollectionFeeId'
};
export const COLLECTION_GROUP = {
  PK: 'collectionGroupId',
  FK: 'collectionGroupGUID'
};
export const COMPANY = {
  PK: 'companyId',
  FK: 'company_CompanyId'
};
export const COST_TYPE = {
  PK: 'costTypeId',
  FK: 'costTypeGUID'
};
export const CREDIT_APP_TABLE = {
  PK: 'creditAppId',
  FK: 'creditAppTable_CreditAppId'
};
export const CREDIT_RESULT = {
  PK: 'creditResultId',
  FK: 'creditResult_CreditResultId'
};
export const CURRENCY = {
  PK: 'currencyId',
  FK: 'currency_CurrencyId'
};
export const CUST_BANK = {
  PK: 'bankAccountName',
  FK: 'custBank_BankAccountName'
};
export const CUST_CREDIT_GROUP = {
  PK: 'creditGroupId',
  FK: 'custCreditGroupGUID'
};
export const CUST_GROUP = {
  PK: 'custGroupId',
  FK: 'custGroup_CustGroupId'
};
export const CUSTOMER_NAME_HISTORY = {
  PK: 'origName',
  FK: 'customerNameHistory_OrigName'
};
export const CUSTOMER_TABLE = {
  PK: 'customerId',
  FK: 'customerTable_CustomerId'
};
export const CUST_TRANS = {
  PK: 'lastSettleId',
  FK: 'custTrans_LastSettleId'
};
export const DOCUMENT_CHECK_LIST_TEMPLATE_TABLE = {
  PK: 'documentCheckListTemplateId',
  FK: 'documentCheckListTemplateTable_DocumentCheckListTemplateId'
};
export const DOCUMENT_CHECK_LIST_TYPE = {
  PK: 'documentTypeId',
  FK: 'documentCheckListType_DocumentTypeId'
};
export const DOCUMENT_PROCESS = {
  PK: 'processId',
  FK: 'documentProcess_ProcessId'
};
export const DOCUMENT_REASON = {
  PK: 'reasonId',
  FK: 'documentReasonGUID'
};
export const DOCUMENT_STATUS = {
  PK: 'statusId',
  FK: 'documentStatus_Description'
};
export const EMPLOYEE_TABLE = {
  PK: 'employeeId',
  FK: 'employeeTable_EmployeeId'
};
export const EMPL_TEAM = {
  PK: 'teamId',
  FK: 'emplTeam_TeamId'
};
export const FLEX_SETUP = {
  PK: 'flexId',
  FK: 'flexSetupGUID'
};
export const GUARANTOR_TABLE = {
  PK: 'guarantorId',
  FK: 'guarantorTable_GuarantorId'
};
export const IMPORT_RECEIPT_HEADER = {
  PK: 'fileName',
  FK: 'importReceiptHeader_FileName'
};
export const IMPORT_RECEIPT_LINE = {
  PK: 'agreementId',
  FK: 'importReceiptLine_AgreementId'
};
export const IMPORT_RECEIPT_LOG_HEADER = {
  PK: 'jobId',
  FK: 'importReceiptLogHeader_JobId'
};
export const IMPORT_RECEIPT_LOG_LINE = {
  PK: 'agreementId',
  FK: 'importReceiptLogLine_AgreementId'
};
export const INSTITUTE = {
  PK: 'instituteId',
  FK: 'instituteGUID'
};
export const INTRODUCED_BY = {
  PK: 'introducedById',
  FK: 'introducedByGUID'
};
export const INVOICE_REVENUE_TYPE = {
  PK: 'revenueTypeId',
  FK: 'invoiceRevenueTypeGUID'
};
export const INVOICE_TABLE = {
  PK: 'invoiceId',
  FK: 'invoiceTable_InvoiceId'
};
export const INVOICE_TYPE = {
  PK: 'invoiceTypeId',
  FK: 'invoiceTypeGUID'
};
export const LANGUAGE = {
  PK: 'languageId',
  FK: 'language_LanguageId'
};
export const LEASE_SUB_TYPE = {
  PK: 'leaseSubTypeId',
  FK: 'leaseSubType_LeaseSubTypeId'
};
export const LEASE_TYPE = {
  PK: 'leaseTypeId',
  FK: 'leaseType_LeaseTypeId'
};
export const LEDGER_DIMENSION = {
  PK: 'dimensionCode',
  FK: 'ledgerDimension_DimensionCode'
};
export const LEDGER_FISCAL_YEAR = {
  PK: 'fiscalYearId',
  FK: 'ledgerFiscalYear_FiscalYearId'
};
export const LETTER_OF_EXPIRATION_JOURNAL = {
  PK: 'journalId',
  FK: 'letterOfExpirationJournal_JournalId'
};
export const LETTER_OF_EXPIRATION_TRANS = {
  PK: 'customerName',
  FK: 'letterOfExpirationTrans_CustomerName'
};
export const LETTER_OF_OVERDUE_JOURNAL = {
  PK: 'journalId',
  FK: 'letterOfOverdueJournal_JournalId'
};
export const LETTER_OF_OVERDUE_TRANS = {
  PK: 'customerName',
  FK: 'letterOfOverdueTrans_CustomerName'
};
export const LETTER_OF_OVERDUE_TYPE = {
  PK: 'letterOfOverdueTypeId',
  FK: 'letterOfOverdueType_LetterOfOverdueTypeId'
};
export const LETTER_OF_RENEWAL_JOURNAL = {
  PK: 'journalId',
  FK: 'letterOfRenewalJournal_JournalId'
};
export const LETTER_OF_RENEWAL_TRANS = {
  PK: 'customerName',
  FK: 'letterOfRenewalTrans_CustomerName'
};
export const LINE_OF_BUSINESS = {
  PK: 'lineOfBusinessId',
  FK: 'lineOfBusinessGUID'
};
export const METHOD_OF_CLOSING = {
  PK: 'methodOfClosingId',
  FK: 'methodOfClosing_MethodOfClosingId'
};
export const METHOD_OF_PAYMENT = {
  PK: 'methodOfPaymentId',
  FK: 'methodOfPayment_MethodOfPaymentId'
};
export const NCB_ACCOUNT_STATUS = {
  PK: 'ncbAccStatusId',
  FK: 'ncbAccountStatus_NCBAccStatusId'
};
export const NPL_RATE = {
  PK: 'rateId',
  FK: 'nplRate_RateId'
};
export const NUMBER_SEQ_PARAMETER = {
  PK: 'referenceId',
  FK: 'numberSeqParameter_ReferenceId'
};
export const NUMBER_SEQ_TABLE = {
  PK: 'numberSeqCode',
  FK: 'numberSeqTableGUID'
};
export const PAYMENT_FREQUENCY = {
  PK: 'paymentFrequencyId',
  FK: 'paymentFrequency_PaymentFrequencyId'
};
export const PAYMENT_HISTORY = {
  PK: 'autoSettlementBatchId',
  FK: 'paymentHistory_AutoSettlementBatchId'
};
export const PAYMENT_STRUCTURE_TABLE = {
  PK: 'paymStructureId',
  FK: 'paymentStructureTable_PaymStructureId'
};
export const PENALTY_SETUP = {
  PK: 'penaltyRateId',
  FK: 'penaltySetup_PenaltyRateId'
};
export const PENALTY_TABLE = {
  PK: 'customerName',
  FK: 'penaltyTable_CustomerName'
};
export const PROD_GROUP = {
  PK: 'prodGroupId',
  FK: 'prodGroupGUID'
};
export const PRODUCT_TABLE = {
  PK: 'productId',
  FK: 'productTable_ProductId'
};
export const PROD_UNIT = {
  PK: 'unitId',
  FK: 'prodUnit_UnitId'
};
export const PROSPECT_TABLE = {
  PK: 'prospectId',
  FK: 'prospectTable_ProspectId'
};
export const PROS_SEGMENT = {
  PK: 'segmentId',
  FK: 'prosSegment_SegmentId'
};
export const PROVISION_JOURNAL = {
  PK: 'journalId',
  FK: 'provisionJournal_JournalId'
};
export const PROVISION_RATE = {
  PK: 'rateId',
  FK: 'provisionRate_RateId'
};
export const PROVISION_TRANS = {
  PK: 'parameterInvalid',
  FK: 'provisionTrans_ParameterInvalid'
};
export const PURCH_AGREEMENT_CONFIRM_TABLE = {
  PK: 'purchAgreementConfirmId',
  FK: 'purchAgreementConfirmTable_PurchAgreementConfirmId'
};
export const PURCH_AGREEMENT_DEDUCTION_SETUP = {
  PK: 'deductionId',
  FK: 'purchAgreementDeductionSetup_DeductionId'
};
export const PURCH_AGREEMENT_TABLE = {
  PK: 'purchAgreementId',
  FK: 'purchAgreementTable_PurchAgreementId'
};
export const RECEIPT_TABLE = {
  PK: 'receiptId',
  FK: 'receiptTable_ReceiptId'
};
export const RECEIPT_TEMP_TABLE = {
  PK: 'receiptTempId',
  FK: 'receiptTempTable_ReceiptTempId'
};
export const REPOSSESSION_STATUS = {
  PK: 'statusId',
  FK: 'repossessionStatus_StatusId'
};
export const STAGING_TABLE = {
  PK: 'autoSettlementBatchId',
  FK: 'stagingTable_AutoSettlementBatchId'
};
export const TAX_INVOICE_TABLE = {
  PK: 'taxInvoiceId',
  FK: 'taxInvoiceTable_TaxInvoiceId'
};
export const TAX_TABLE = {
  PK: 'taxCode',
  FK: 'taxTable_TaxCode'
};
export const TERM = {
  PK: 'termId',
  FK: 'term_TermId'
};
export const TERRITORY = {
  PK: 'territoryId',
  FK: 'territoryGUID'
};
export const VEHICLE_ENGINE_SIZE = {
  PK: 'vehicleEngineSizeId',
  FK: 'vehicleEngineSizeGUID'
};
export const VEHICLE_SUB_TYPE = {
  PK: 'vehicleSubTypeId',
  FK: 'vehicleSubTypeGUID'
};
export const VEHICLE_TYPE = {
  PK: 'vehicleTypeId',
  FK: 'vehicleTypeGUID'
};
export const VEND_GROUP = {
  PK: 'vendGroupId',
  FK: 'vendGroup_VendGroupId'
};
export const VENDOR_TABLE = {
  PK: 'vendorId',
  FK: 'vendorTable_VendorId'
};
export const WAIVE_TABLE = {
  PK: 'waiveId',
  FK: 'waiveTable_WaiveId'
};
export const WITHHOLDING_TAX_GROUP = {
  PK: 'whtGroupId',
  FK: 'withholdingTaxGroup_WHTGroupId'
};
export const WITHHOLDING_TAX_TABLE = {
  PK: 'whtCode',
  FK: 'withholdingTaxTable_WHTCode'
};
export const WITNESS_TRANS = {
  PK: 'name',
  FK: 'witnessTrans_Name'
};
export const SELLING_TYPE = {
  PK: 'sellingTypeId',
  FK: 'sellingTypeGUID'
};

export const DUTY_STAMP = {
  PK: 'dutyStampId',
  FK: 'dutyStampSetupGUID'
};
export const DUTY_STAMP_JOURNAL = {
  PK: 'journalId',
  FK: 'dutyStampJournal_JournalId'
};
