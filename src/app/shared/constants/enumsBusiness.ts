export enum AccountType {
  Ledger = 0,
  Bank = 1,
  Vendor = 2
}
export enum AgreementDocType {
  New = 0,
  Extend = 1,
  Addendum = 2,
  NoticeOfCancellation = 3
}
export enum AgreementRefType {
  None = 0,
  MainAgreement = 1,
  GuarantorAgreement = 2,
  AssignmentAgreement = 3,
  CollateralAgreement = 4
}
export enum ApprovalDecision {
  None = 0,
  Approved = 1,
  Rejected = 2
}
export enum AssetFeeType {
  None = 0,
  VehicleTax = 1,
  Compulsory = 2,
  Insurance = 3,
  Other = 4
}
export enum BillingBy {
  Company = 0,
  Customer = 1
}
export enum ChequeSource {
  None = 0,
  CollectionFollowUp = 1,
  Cheque = 2
}
export enum CollectionFollowUpResult {
  None = 0,
  Confirmed = 1,
  Postpone = 2,
  Extend = 3
}
export enum CollectionType {
  Product = 0,
  Other = 1
}
export enum ContactTo {
  Customer = 0,
  Buyer = 1,
  Specific = 2
}
export enum ContactType {
  Phone = 0,
  Email = 1,
  URL = 2,
  Fax = 3
}
export enum CreditAppRequestType {
  MainCreditLimit = 0,
  BuyerMatching = 1,
  LoanRequest = 2,
  ActiveAmendCustomerCreditLimit = 3,
  AmendCustomerInfo = 4,
  AmendCustomerBuyerCreditLimit = 5,
  AmendBuyerInfo = 6,
  ReviewBuyerCreditLimit = 7,
  CloseCustomerCreditLimit = 8
}
export enum CreditLimitConditionType {
  ByCustomer = 0,
  ByCreditApplication = 1
}
export enum CreditLimitExpiration {
  ByTransaction = 0,
  ByCreditApplication = 1,
  ByBuyerAgreement = 2,
  BySpecific = 3,
  NoExpiration = 4
}
export enum Dimension {
  Dimension1 = 0,
  Dimension2 = 1,
  Dimension3 = 2,
  Dimension4 = 3,
  Dimension5 = 4
}
export enum DocConVerifyType {
  Billing = 0,
  Receipt = 1
}
export enum DocumentTemplateType {
  None = 0,
  MainAgreementShared = 1,
  MainAgreementConsortium = 2,
  MainAgreementJV = 3,
  MainAgreementPF = 4,
  MainAgreementExpenseNotice = 5,
  AddendumMainAgreement = 6,
  AssignmentAgreement = 7,
  CancelAssignmentAgreement = 8,
  BusinessCollateralAgreement = 9,
  ExtendNotesBusCollateralAgm = 10,
  BusCollateralAgmExpenseNotice = 11,
  LetterReqCancelBusCollateralAgm = 12,
  GuarantorAgreement_ThaiPerson = 13,
  CrossGuarantorAgreement = 14,
  CustomerAmendCreditInformation = 15,
  CustomerAmendCreditLimit = 16,
  CustomerCreditApplicationShared = 17,
  CustomerCreditApplicationPF = 18,
  CreditApplicationConsideration = 19,
  BuyerAmendCreditInformation = 20,
  BuyerAmendCreditLimit = 21,
  BuyerCreditApplication_Private = 22,
  BuyerReviewCreditLimit = 23,
  CreditLimitClosing = 24,
  GuarantorAgreement_Foreigner = 25,
  GuarantorAgreement_Organization = 26,
  BuyerCreditApplication_Government = 27
}
export enum IdentificationType {
  TaxId = 0,
  PassportId = 1
}
export enum InterestCalculationMethod {
  Minimum = 0,
  Actual = 1
}
export enum InterfaceStatus {
  None = 0,
  Success = 1,
  Failed = 2
}
export enum MethodOfBilling {
  BeforePurchase = 0,
  AfterPurchase = 1
}
export enum PaidToType {
  Customer = 0,
  Vendor = 1,
  Suspense = 2
}
export enum PaymentType {
  Ledger = 0,
  Transfer = 1,
  Cheque = 2,
  SuspenseAccount = 3
}
export enum Priority {
  Normal = 0,
  High = 1,
  Critical = 2
}
export enum ProductType {
  None = 0,
  Factoring = 1,
  ProjectFinance = 2,
  Leasing = 3,
  HirePurchase = 4,
  Bond = 5,
  LC_DLC = 6
}
export enum PurchaseFeeCalculateBase {
  InvoiceAmount = 0,
  PurchaseAmount = 1
}
export enum ReceiptTempRefType {
  Normal = 0,
  Imported = 1,
  Waive = 2,
  Discount = 3,
  Product = 4,
  Refund = 5
}
export enum ReceivedFrom {
  Customer = 0,
  Buyer = 1
}
export enum RecordType {
  Person = 0,
  Organization = 1
}
export enum RefType {
  Company = 0,
  Prospect = 1,
  Customer = 2,
  Guarantor = 3,
  Vendor = 4,
  Application = 5,
  Agreement = 7,
  Invoice = 8,
  Payment = 9,
  PurchaseAgreementConfirm = 10,
  AgreementClassificationJournal = 11,
  ReceiptTemp = 12,
  Waive = 13,
  ApplicationAsset = 14,
  AgreementAsset = 15,
  AgreementAssetFees = 16,
  Collateral = 17,
  Product = 18,
  Address = 19,
  CustBank = 20,
  Contact = 21,
  AuthorizedPersonTrans = 22,
  GuarantorTrans = 23,
  WitnessTrans = 24,
  CustTrans = 25,
  CollateralMovementJournal = 26,
  ProvisionJournal = 28,
  AgreementTransIncomeRealization = 29,
  AgreementEarlyPayoff = 30,
  Branch = 31,
  DocumentCheckList = 32,
  CollectionActivityTrans = 33,
  DutyStampJournal = 34,
  AllAgreementEarlyPayOff = 35,
  PurchAgreementTable = 36,
  VendBank = 37,
  Buyer = 38,
  CreditAppRequestTable = 39,
  CreditAppTable = 40,
  CreditAppRequestLine = 41,
  CreditAppLine = 42,
  AssignmentAgreement = 43,
  Cheque = 44,
  PurchaseTable = 45,
  PurchaseLine = 46,
  MainAgreement = 47,
  GuarantorAgreement = 48,
  BusinessCollateralAgreement = 49,
  BuyerAgreement = 50,
  PaymentDetail = 51,
  CollectionFollowUp = 53,
  RelatedPerson = 54,
  InvoiceSettlementDetail = 55,
  WithdrawalTable = 56,
  WithdrawalLine = 57,
  CustomerRefund = 58,
  Blacklist = 59,
  Verification = 60,
  Employee = 61,
  ServiceFeeTrans = 62,
  MessengerJob = 63,
  VendorPaymentTrans = 64,
  ProjectProgress = 65,
  DocumentReturn = 66,
  InterestRealizedTrans = 67,
  FreeTextInvoice = 68,
  IntercompanyInvoiceAdjustment = 69,
  AssignmentAgreementSettle = 70
}
export enum Result {
  None = 0,
  Completed = 1,
  Failed = 2
}
export enum RetentionCalculateBase {
  None = 0,
  InvoiceAmount = 1,
  PurchaseAmount = 2,
  FixedAmount = 3,
  NetReserveAmount = 4,
  WithdrawalAmount = 5
}
export enum RetentionDeductionMethod {
  Purchase = 0,
  Withdrawal = 1,
  ReserveRefund = 2,
  CustomerSpecific = 3
}
export enum ServiceFeeCategory {
  None = 0,
  Agreement = 1,
  Billing = 2,
  Receipt = 3,
  Retention = 4
}
export enum SuspenseInvoiceType {
  None = 0,
  SuspenseAccount = 1,
  ToBeRefunded = 2,
  RetentionReturn = 3
}

export enum NumberSeqSegmentType {
  Constant = 0,
  Number = 1,
  Variable1 = 2,
  Variable2 = 3,
  Variable3 = 4,
  CompanyVariable = 5
}
export enum BookmarkDocumentRefType {
  None = 0,
  MainAgreement = 1,
  GuarantorAgreement = 2,
  AssignmentAgreement = 3,
  BusinessCollateralAgreement = 4,
  CreditAppRequest = 5
}
export enum CustTransStatus {
  Open = 0,
  Closed = 1
}
export enum DayOfMonth {
  None = 0,
  D01 = 1,
  D02 = 2,
  D03 = 3,
  D04 = 4,
  D05 = 5,
  D06 = 6,
  D07 = 7,
  D08 = 8,
  D09 = 9,
  D10 = 10,
  D11 = 11,
  D12 = 12,
  D13 = 13,
  D14 = 14,
  D15 = 15,
  D16 = 16,
  D17 = 17,
  D18 = 18,
  D19 = 19,
  D20 = 20,
  D21 = 21,
  D22 = 22,
  D23 = 23,
  D24 = 24,
  D25 = 25,
  D26 = 26,
  D27 = 27,
  D28 = 28,
  D29 = 29,
  D30 = 30,
  D31 = 31
}
export enum MonthOfYear {
  None = 0,
  M01 = 1,
  M02 = 2,
  M03 = 3,
  M04 = 4,
  M05 = 5,
  M06 = 6,
  M07 = 7,
  M08 = 8,
  M09 = 9,
  M10 = 10,
  M11 = 11,
  M12 = 12
}
export enum WorkingDay {
  MonToFri = 0,
  MonToSat = 1,
  MonToSun = 2
}
export enum HolidayType {
  PublicHoliday = 0,
  CompanyHoliday = 1
}
export enum CalcInterestMethod {
  None = 0,
  NextCompanyCalendar = 1,
  NextBOTCalendar = 2,
  NextMinCalendar = 3,
  NextMaxCalendar = 4
}
export enum CalcInterestDayMethod {
  None = 0,
  NextDay = 1
}
export enum PenaltyCalculationMethod {
  OutstandingBalanceOfInstallmentInvoice = 0,
  AllInstallmentInvoice = 1
}
export enum OverdueType {
  Day = 0,
  Period = 1
}
export enum ProvisionRateBy {
  Customer = 0,
  Agreement = 1
}
export enum ProvisionMethod {
  Day = 0,
  Period = 1
}
export enum NPLMethod {
  Day = 0,
  Period = 1
}
export enum CollectionFeeMethod {
  Day = 0,
  Period = 1
}
export enum ReceiptGenBy {
  Agreement = 0,
  AgreementInvoiceType = 1,
  AgreementInvoice = 2
}
export enum PenaltyBase {
  Payment = 0,
  PaymentIncTax = 1
}
export enum PaymStructureRefNum {
  None = 0,
  RefNum1 = 1,
  RefNum2 = 2
}
export enum InvoiceIssuing {
  Company = 0,
  Customer = 1,
  Agreement = 2
}
export enum CompanySignatureRefType {
  AuthorizedPerson = 0,
  Witness = 1
}
export enum SettleInvoiceCriteria {
  NONE = 0,
  SettlementByMonth = 1,
  SettlementByDay = 2
}
export enum LedgerFiscalPeriodStatus {
  Open = 0,
  Closed = 1
}

export enum LedgerFiscalPeriodUnit {
  Month = 0,
  Year = 1
}

export enum ManageAgreementAction {
  Post = 0,
  Send = 1,
  Sign = 2,
  Close = 3,
  Cancel = 4
}
export enum CustTransStatus {
  OPEN = 0,
  CLOSED = 1
}
export enum StagingBatchStatus {
  None = 0,
  Success = 1,
  Failed = 2
}
export enum ProcessTransType {
  DownPaym = 0,
  AdvancePaym = 1,
  Deposit = 2,
  DepositReturn = 3,
  Execution = 4,
  Installment = 5,
  Invoice = 6,
  Penalty = 7,
  Collection = 8,
  TaxRealization = 9,
  IncomeRealization = 10,
  IncomeRealizationNPL = 11,
  IncomeRealizationTerminate = 12,
  IncomeRealizationWriteOff = 13,
  Payment = 14,
  Closing = 15,
  Termination = 16,
  WriteOff = 17,
  Selling = 18,
  Disposal = 19,
  PurchaseAgreement = 20,
  VendorPayment = 21,
  PaymentDetail = 22
}
export enum TaxInvoiceRefType {
  None = 0,
  Invoice = 1,
  Receipt = 2
}
export enum WithdrawalLineType {
  Principal = 0,
  Interest = 1
}
export enum VerifyType {
  None = 0,
  Assignment = 1,
  Billing = 2,
  Receipt = 3,
  Supplier = 4
}
export enum ShowDocConVerifyType {
  None = 0,
  Billing = 1,
  Receipt = 2
}
