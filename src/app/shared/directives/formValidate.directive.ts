import { Directive, HostListener, ElementRef, OnInit, Input, Renderer2, Output, EventEmitter } from '@angular/core';
import { interval, timer } from 'rxjs';
import { takeWhile } from 'rxjs/operators';
import { UIControllerService } from 'core/services/uiController.service';
import { FormValidationModel } from '../models/systemModel';
import { ValidationService } from '../services/validation.service';
import { ElementType } from 'shared/constants';
import { isNullOrUndefOrEmpty } from 'shared/functions/value.function';
import { focusInValidated } from 'shared/functions/model.function';

@Directive({ selector: '[formValidate]' })
export class FormValidateDirective implements OnInit {
  @Output() formValidate = new EventEmitter<FormValidationModel>();
  isShowLog: boolean;
  constructor(
    private elementRef: ElementRef,
    private renderer: Renderer2,
    private uiService: UIControllerService,
    private validateService: ValidationService
  ) {
    this.isShowLog = false;
  }
  @HostListener('click', ['$event.target'])
  onClick(btn): void {
    this.uiService.setIncreaseReqCounter('formvalidate');
    this.validateService.clearInvalidFiled();
    this.validateService.$formValidate.emit({ isValid: true });
    this.setFocus();
    this.formValidate.emit({
      isValid: this.validateService.invalidFields.length === 0,
      invalidId: this.validateService.invalidFields
    });

    this.uiService.setDecreaseReqCounter('formvalidate');
  }
  setResult(): void {}
  setClearClass(): void {}
  setFocus(): void {
    focusInValidated(this.validateService.invalidFields);
  }
  getValidation(): void {}
  ngOnInit(): void {}
  setCalendar(el: any): void {
    this.renderer.addClass(el.parentNode, 'hide-datepicker');
    const input = el.querySelectorAll('input');
    input[0].focus();
    setTimeout(() => {
      this.renderer.removeClass(el.parentNode, 'hide-datepicker');
    }, 100);
  }
}
