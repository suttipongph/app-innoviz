import { Directive, ElementRef, OnInit, Input, Renderer2 } from '@angular/core';
import { isNullOrUndefined, switchClass } from '../functions/value.function';
import { interval } from 'rxjs';
import { UIControllerService } from 'core/services/uiController.service';
import { AppConst, Ordinal } from '../constants';
import { FieldAccessing } from 'shared/models/systemModel';

@Directive({ selector: '[viewonly]' })
export class ViewOnlyDirective implements OnInit {
  isView = false;
  header: any;
  body: any;
  save: any;
  saveNclose: any;
  isSeted = false;
  inputs: any;
  ddls: any;
  calendars: any;
  textAreas: any;
  switch: any;
  multi: any;
  fieldSets: any;
  inVisibility = false;
  isShowLog = false;
  factors: boolean[] = [];
  ordinal = Ordinal;
  btn: any;
  @Input()
  set viewonly(_isview: boolean) {
    const self = this;
    if (!isNullOrUndefined(_isview)) {
      switchClass(this.elementRef.nativeElement, 'view-only', _isview);
      if (_isview) {
        let subscription = interval(300).subscribe((val) => {
          self.btn = document.querySelectorAll('ivz-button');
          if (!isNullOrUndefined(self.btn)) {
            [].forEach.call(self.btn, function (ele) {
              const icon = ele.getAttribute('icon');
              if (icon === 'pi pi-file-excel' || icon === 'pi pi-table') {
                self.renderer.removeAttribute(ele.children[0], 'disabled');
              }
            });
            subscription.unsubscribe();
          }
        });
        const fields: FieldAccessing[] = [{ filedIds: [AppConst.VIEWMODE_ID], readonly: true }];
        this.uiService.setUIFieldsAccessing(fields);
      } else {
        const fields: FieldAccessing[] = [{ filedIds: [AppConst.CLEAR_ID], readonly: false }];
        this.uiService.setUIFieldsAccessing(fields);
      }
    }
  }
  constructor(private elementRef: ElementRef, private renderer: Renderer2, private uiService: UIControllerService) {}
  ngOnInit(): void {}
}
