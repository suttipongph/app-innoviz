import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { ConfigurationModel } from 'shared/models/systemModel';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ExternalDataService {
  constructor(private location: Location, private http: HttpClient) {}

  loadEnumData(): Promise<any> {
    return (
      this.http
        // .get(this.location.prepareExternalUrl('/assets/enum.json'))
        .get(this.location.prepareExternalUrl('/app/shared/constants/enum.json'))
        .toPromise()
        .then((response: Response) => {
          return Promise.resolve(response);
        })
        .catch((error) => {
          return Promise.reject(error);
        })
    );
  }
  // get config(): ConfigurationModel {
  //     return this.enumData;
  // }
}
