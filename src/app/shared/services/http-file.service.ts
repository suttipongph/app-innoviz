import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClientService } from 'core/services/httpclient.service';
import { ImportBankFile } from 'shared/models/systemModel';

@Injectable()
export class HttpFileService {
  constructor(private http: HttpClientService) {}
  setImportExcel(data): Observable<any> {
    const url = 'File/GetImportExcel';
    return this.http.Post(url, data);
  }
  setImportBackFile(data: ImportBankFile): Observable<any> {
    const url = 'File/GetImportBankFile';
    return this.http.Post(url, data);
  }
}
