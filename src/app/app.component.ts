import { isPlatformBrowser, LocationStrategy } from '@angular/common';
import { Component, HostListener, Inject, OnInit, PLATFORM_ID, Renderer2, ViewChild } from '@angular/core';
import { ActivationStart, NavigationEnd, RouteConfigLoadEnd, RouteConfigLoadStart, Router, NavigationStart } from '@angular/router';
import { TranslateService, TranslationChangeEvent } from '@ngx-translate/core';
import { AuthConfig, OAuthService } from 'angular-oauth2-oidc';
import { JwksValidationHandler } from 'angular-oauth2-oidc-jwks';
import { MessageService } from 'primeng/api';
import { MenuService } from './app.menu.service';
import { AccessRightService } from './core/services/access-right.service';
import { AuthService } from './core/services/auth.service';
import { ConfigurationService } from './core/services/configuration.service';
import { UIControllerService } from './core/services/uiController.service';
import { UserDataService } from './core/services/user-data.service';
import { getEnvironment, setAppSetting } from './shared/config/globalvar.config';
import { AppConst, FUNCTION_PATHS, RELETEDINFO_PATHS } from './shared/constants';
import { isUndefinedOrZeroLength } from './shared/functions/value.function';
import { ScrollPanel } from 'primeng/scrollpanel/public_api';
import {
  calendarListener,
  reCalcFunctionAndRelatedDialogPosition,
  recalcMainMenur,
  reCalcMutipleSelectionDialogSize
} from './shared/functions/event.function';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @HostListener('document:click', ['$event'])
  clickout(event): void {
    recalcMainMenur(event); // or recalc menu scroll
    reCalcFunctionAndRelatedDialogPosition(event);
    reCalcMutipleSelectionDialogSize(event);
    calendarListener(event);
  }

  layoutMode = 'static';

  darkMenu = false;

  profileMode = 'popup';

  rotateMenuButton: boolean;

  topbarMenuActive: boolean;

  overlayMenuActive: boolean;

  staticMenuDesktopInactive: boolean;

  staticMenuMobileActive: boolean;

  menuClick: boolean;

  topbarItemClick: boolean;

  configClick: boolean;

  activeTopbarItem: any;

  menuHoverActive: boolean;

  grouped = false;

  configActive: boolean;

  inlineMenuActive: boolean;

  inlineMenuClick: boolean;
  authConfig: AuthConfig;
  isShowLog: boolean;
  resetMenu: boolean;
  lang = 'en';
  isActivated = false;

  constructor(
    public renderer: Renderer2,
    private menuService: MenuService,
    public translate: TranslateService,
    private uiService: UIControllerService,
    private messageService: MessageService,
    private userDataService: UserDataService,
    private configService: ConfigurationService,
    @Inject(PLATFORM_ID) private platformId: any,
    private locationStrategy: LocationStrategy,
    private oAuthService: OAuthService,
    private authService: AuthService,
    private accessService: AccessRightService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {
    // translate.addLangs(['en', 'fr']);
    // translate.setDefaultLang('en');
    this.isShowLog = false;
    setAppSetting();
    const appLanguage = this.userDataService.getAppLanguage();
    const defaultLang = this.configService.config.defaultLanguage.toLowerCase();

    translate.setDefaultLang(defaultLang);
    if (appLanguage === undefined || appLanguage === null || appLanguage === '') {
      translate.use(defaultLang);
    } else {
      translate.use(appLanguage);
    }
    const env = getEnvironment();
    this.authConfig = {
      issuer: env.authURL,
      redirectUri: window.location.origin + this.locationStrategy.getBaseHref() + 'Login',
      clientId: env.clientId,
      scope: env.scope,
      logoutUrl: env.authURL + '/connect/endsession',
      postLogoutRedirectUri: window.location.origin + this.locationStrategy.getBaseHref() + 'Logout'
    };

    this.configureOAuthAuthentication();
    this.router.events.subscribe((event) => {
      if (event instanceof RouteConfigLoadStart) {
        if (FUNCTION_PATHS.findIndex((f) => f === event.route.path) > -1) {
          this.uiService.setIncreaseReqCounter('app router');
        }
        if (RELETEDINFO_PATHS.findIndex((f) => f === event.route.path) > -1) {
          this.uiService.setIncreaseReqCounter('app router');
        }
      } else if (event instanceof RouteConfigLoadEnd) {
        if (FUNCTION_PATHS.findIndex((f) => f === event.route.path) > -1) {
          if (this.isShowLog) {
            console.log('fullPageSubject.next 19');
          }
          this.uiService.fullPageSubject.next(true);
          this.uiService.setDecreaseReqCounter('app router');
        }
        if (RELETEDINFO_PATHS.findIndex((f) => f === event.route.path) > -1) {
          this.uiService.setDecreaseReqCounter('app router');
        }
      }
      if (event instanceof ActivationStart) {
        this.uiService.setHistSnapshot(event.snapshot);
      }
      if (event instanceof NavigationEnd) {
        this.uiService.setInitialMode(event);
      }
    });
    // this.uiService.fullPageSubject.subscribe(data => {
    //     this.isFunctionPage = data;
    // });
    this.accessService.siteAndCompanyBranchSelected.subscribe((data) => {
      this.isActivated = true;
    });
  }
  ngOnInit(): void {
    const siteLogin = this.userDataService.getSiteLogin();
    if (
      isUndefinedOrZeroLength(siteLogin) ||
      (siteLogin === AppConst.CASHIER && isUndefinedOrZeroLength(this.userDataService.getCurrentCompanyGUID()))
    ) {
      this.isActivated = false;
    } else {
      this.isActivated = true;
      this.authService.setDisplaySubject.next(true);
    }
  }
  isLoggedIn(): boolean {
    return this.authService.hasToken();
  }
  onSwitch(): void {
    this.lang = this.lang === 'en' ? 'th' : 'en';
    this.translate.use(this.lang);
  }
  configureOAuthAuthentication(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.oAuthService.configure(this.authConfig);
      if (this.oAuthService.hasValidAccessToken() || this.oAuthService.hasValidIdToken()) {
        this.oAuthService.loadDiscoveryDocument();
      } else {
        this.oAuthService.loadDiscoveryDocumentAndLogin();
      }
      this.oAuthService.setupAutomaticSilentRefresh();

      this.oAuthService.events.subscribe((e) => {
        if (e.type === 'token_expires') {
          this.authService.isTokenExpiredSubject.next(true);
        }
        // subscibe to invalid token event
        // redirect to login
        // silent refresh token error >> re-login
        if (
          e.type === 'token_error' ||
          e.type === 'silent_refresh_error' ||
          e.type === 'silent_refresh_timeout' ||
          e.type === 'session_terminated' ||
          e.type === 'session_error'
        ) {
          this.authService.onInvalidToken();
          this.authService.initImplicitFlow();
        }
      });
      this.router.events.subscribe((e) => {
        if (e instanceof NavigationStart) {
          this.authService.isAuthNavigationDoneSubject.next(false);
        }
        if (e instanceof NavigationEnd) {
          if (e.url === '/siteselect') {
            this.authService.isAuthNavigationDoneSubject.next(true);
          }
        }
      });

      window.addEventListener('storage', (event) => {
        if (event.key === 'access_token' || event.key === null) {
          if (this.authService.isLoggedIn()) {
            const siteLogin = this.userDataService.getSiteLogin();
            if (!isUndefinedOrZeroLength(siteLogin) && (!this.accessService.isPermittedBySite() || !this.accessService.isPermittedByAccessRight())) {
              this.accessService.setErrorCode('ERROR.00807');
              this.router.navigate([`/${siteLogin}/401`]);
            }
          }
        }
      });
    }
  }
  onLayoutClick(): void {
    if (!this.topbarItemClick) {
      this.activeTopbarItem = null;
      this.topbarMenuActive = false;
    }

    if (!this.menuClick || (this.inlineMenuClick && this.isSlim())) {
      if (this.isHorizontal() || this.isSlim()) {
        this.menuService.reset();
      }

      if (this.overlayMenuActive || this.staticMenuMobileActive) {
        this.hideOverlayMenu();
      }

      this.menuHoverActive = false;
    }

    if (this.configActive && !this.configClick) {
      this.configActive = false;
    }

    if (this.inlineMenuActive && !this.inlineMenuClick) {
      this.inlineMenuActive = false;
    }

    this.inlineMenuClick = false;
    this.configClick = false;
    this.topbarItemClick = false;
    this.menuClick = false;
  }

  onMenuButtonClick(event): void {
    this.menuClick = true;
    this.rotateMenuButton = !this.rotateMenuButton;
    this.topbarMenuActive = false;

    if (this.layoutMode === 'overlay' && !this.isMobile() && !this.isTablet()) {
      this.overlayMenuActive = !this.overlayMenuActive;
    } else {
      if (this.isDesktop()) {
        this.staticMenuDesktopInactive = !this.staticMenuDesktopInactive;
      } else {
        this.staticMenuMobileActive = !this.staticMenuMobileActive;
      }
    }

    event.preventDefault();
  }

  onMenuClick($event): void {
    this.menuClick = true;

    if (this.inlineMenuActive && !this.inlineMenuClick) {
      this.inlineMenuActive = false;
    }
  }

  onInlineMenuClick(event): void {
    this.inlineMenuActive = !this.inlineMenuActive;
    this.inlineMenuClick = true;
  }

  onTopbarMenuButtonClick(event): void {
    this.topbarItemClick = true;
    this.topbarMenuActive = !this.topbarMenuActive;

    this.hideOverlayMenu();

    event.preventDefault();
  }

  onTopbarItemClick(event, item): void {
    this.topbarItemClick = true;

    if (this.activeTopbarItem === item) {
      this.activeTopbarItem = null;
    } else {
      this.activeTopbarItem = item;
    }

    event.preventDefault();
  }

  onTopbarSubItemClick(event): void {
    event.preventDefault();
  }

  onConfigClick(event): void {
    this.configClick = true;
  }

  hideOverlayMenu(): void {
    this.rotateMenuButton = false;
    this.overlayMenuActive = false;
    this.staticMenuMobileActive = false;
  }

  isTablet(): boolean {
    const width = window.innerWidth;
    return width <= 1024 && width > 640;
  }

  isDesktop(): boolean {
    return window.innerWidth > 1024;
  }

  isMobile(): boolean {
    return window.innerWidth <= 640;
  }

  isOverlay(): boolean {
    return this.layoutMode === 'overlay';
  }

  isHorizontal(): boolean {
    return this.layoutMode === 'horizontal';
  }

  isSlim(): boolean {
    return this.layoutMode === 'slim';
  }

  isStatic(): boolean {
    return this.layoutMode === 'static';
  }
}
