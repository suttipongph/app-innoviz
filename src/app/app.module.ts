import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, Injector, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MenuService } from './app.menu.service';
import { AppRoutingModule } from './app-routing.module';
import { BaseListComponent } from './core/components/base-list/base-list.component';
import { BaseComponent } from './core/components/base/base.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';
import { SharedModule } from './shared/shared.module';
import { DatePipe } from '@angular/common';
import { BaseItemComponent } from './core/components/base-item/base-item.component';
import { setAppInjector } from './app-injector';
import { ConfigurationService } from './core/services/configuration.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { OAuthModule, OAuthStorage, ValidationHandler } from 'angular-oauth2-oidc';
import { JwksValidationHandler } from 'angular-oauth2-oidc-jwks';
import { FallbackComponent } from './core/auth/fallback.component';
import { LoginComponent } from './core/auth/login.component';
import { LogOutComponent } from './core/auth/logout.component';
import { SmartAppNotFoundComponent } from './core/auth/not-found.component';
import { SiteSelectComponent } from './core/auth/site-select/site-select.component';
import { HttpHeaderDataInterceptor } from './core/services/http-header-data.interceptor';
import { LoggingInterceptor } from './core/services/logging.interceptor';
import { SmartAppUnAuthorizedComponent } from './core/auth/unauthorized.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { StoreModule } from '@ngrx/store';
import { reducer } from './core/store/reducer/dummy.reducer';
import { DialogService } from 'primeng/dynamicdialog';
import { BaseWorkflowComponent } from './core/components/base-workflow/base-workflow.component';
import { BaseReportComponent } from 'core/components/base-report/base-report.component';
import { BaseBatchFunctionComponent } from './core/components/base-batch-function/base-batch-function.component';

export function HttpLoaderFactory(http: HttpClient): any {
  return new MultiTranslateHttpLoader(http, [
    { prefix: './assets/i18n/', suffix: '.json' },
    { prefix: './assets/i18n/custom/', suffix: '.json' }
  ]);
}
export function OAuthStorageFactory(): OAuthStorage {
  return localStorage;
}

@NgModule({
  declarations: [
    AppComponent,
    BaseListComponent,
    BaseComponent,
    BaseItemComponent,
    FallbackComponent,
    LoginComponent,
    LogOutComponent,
    SmartAppNotFoundComponent,
    SiteSelectComponent,
    SmartAppUnAuthorizedComponent,
    BaseWorkflowComponent,
    BaseReportComponent,
    BaseBatchFunctionComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule,

    NgxSpinnerModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: [],
        sendAccessToken: true
      }
    }),
    StoreModule.forRoot({
      DummyStore: reducer
    })
  ],
  providers: [
    DatePipe,
    ConfigurationService,
    {
      provide: APP_INITIALIZER,
      useFactory: (configService: ConfigurationService) => () => configService.loadConfigurationData(),
      deps: [ConfigurationService],
      multi: true
    },
    {
      provide: ValidationHandler,
      useClass: JwksValidationHandler
    },
    {
      provide: OAuthStorage,
      useFactory: OAuthStorageFactory
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpHeaderDataInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoggingInterceptor,
      multi: true
    },
    MenuService,
    MessageService,
    ConfirmationService,
    DialogService
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
  constructor(injector: Injector) {
    setAppInjector(injector);
  }
}
