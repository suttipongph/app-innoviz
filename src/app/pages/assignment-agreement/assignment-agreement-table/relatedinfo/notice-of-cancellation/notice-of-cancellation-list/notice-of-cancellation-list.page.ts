import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { AgreementDocType, ColumnType, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'notice-of-cancellation-list-page',
  templateUrl: './notice-of-cancellation-list.page.html',
  styleUrls: ['./notice-of-cancellation-list.page.scss']
})
export class NoticeOfCancellationListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute(1);
  parentId = this.uiService.getRelatedInfoParentTableKey();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'assignmentAgreementTableGUID';
    this.option.canCreate = false;
    this.option.canDelete = false;
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'agreementDocType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: AgreementDocType.NoticeOfCancellation.toString()
      },
      {
        label: null,
        textKey: 'refAssignmentAgreementTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION,
      servicePath: `AssignmentAgreementTable/AssignmentAgreement/${this.masterRoute}/RelatedInfo/NoticeOfCancellation`
    };
  }
}
