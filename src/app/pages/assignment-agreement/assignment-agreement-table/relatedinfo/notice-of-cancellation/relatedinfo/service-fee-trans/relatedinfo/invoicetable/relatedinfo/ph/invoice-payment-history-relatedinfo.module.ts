import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoicePaymentHistoryRelatedinfoRoutingModule } from './invoice-payment-history-relatedinfo-routing.module';
import { PaymentHistoryListPage } from './ph-list/payment-history-list.page';
import { PaymentHistoryItemPage } from './ph-item/payment-history-item.page';
import { SharedModule } from 'shared/shared.module';
import { PaymentHistoryComponentModule } from 'components/payment-history/payment-history/payment-history.module';

@NgModule({
  declarations: [PaymentHistoryListPage, PaymentHistoryItemPage],
  imports: [CommonModule, SharedModule, InvoicePaymentHistoryRelatedinfoRoutingModule, PaymentHistoryComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InvoicePaymentHistoryRelatedinfoModule {}
