import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { CopyBookmarkDocumentTemplateComponentModule } from 'components/bookmark-document-trans/copy-bookmark-document-template/copy-bookmark-document-template.module';
import { CopyBookmarkDocumentFromTemplatePage } from './copy-bd.page';
import { CopyBookmarkDocumentFromTemplateFunctionRoutingModule } from './copy-bd-function-routing.module';

@NgModule({
  declarations: [CopyBookmarkDocumentFromTemplatePage],
  imports: [CommonModule, SharedModule, CopyBookmarkDocumentFromTemplateFunctionRoutingModule, CopyBookmarkDocumentTemplateComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CopyBookmarkDocumentFromTemplateFunctionModule {}
