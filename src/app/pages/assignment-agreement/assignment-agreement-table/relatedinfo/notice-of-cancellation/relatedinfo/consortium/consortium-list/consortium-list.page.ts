import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'consortium-list-page',
  templateUrl: './consortium-list.page.html',
  styleUrls: ['./consortium-list.page.scss']
})
export class ConsortiumTransListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'consortiumGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.CONSORTIUM_TRANS,
      servicePath: `AssignmentAgreementTable/AssignmentAgreement/${this.masterRoute}/RelatedInfo/NoticeOfCancellation/RelatedInfo/ConsortiumTrans`
    };
  }
}
