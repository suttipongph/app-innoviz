import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgreementTableInfoListPage } from './agreement-information-list/agreement-information-list.page';
import { AgreementTableInfoItemPage } from './agreement-information-item/agreement-information-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: AgreementTableInfoListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: AgreementTableInfoItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssignmentAgreementAgreementInformationRelatedinfoRoutingModule {}
