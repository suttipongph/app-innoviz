import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'taxinvoice',
    loadChildren: () => import('./ti/tax-invoice-relatedinfo.module').then((m) => m.TaxInvoiceRelatedinfoModule)
  },
  {
    path: 'custtransaction',
    loadChildren: () => import('./ct/customer-transaction-relatedinfo.module').then((m) => m.CustomerTransactionRelatedinfoModule)
  },
  {
    path: 'paymenthistory',
    loadChildren: () => import('./ph/invoice-payment-history-relatedinfo.module').then((m) => m.InvoicePaymentHistoryRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoiceAndCollectionRelatedinfoRoutingModule {}
