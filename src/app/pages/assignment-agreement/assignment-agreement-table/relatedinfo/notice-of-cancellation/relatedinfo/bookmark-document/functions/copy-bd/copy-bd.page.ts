import { Component, OnInit } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { UIControllerService } from 'core/services/uiController.service';
import { getProductType } from 'shared/functions/value.function';

@Component({
  selector: 'copy-bd-page',
  templateUrl: './copy-bd.page.html',
  styleUrls: ['./copy-bd.page.scss']
})
export class CopyBookmarkDocumentFromTemplatePage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE,
      servicePath: `AssignmentAgreementTable/AssignmentAgreement/${this.masterRoute}/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate`
    };
  }
}
