import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { PrintTaxInvoiceCopyRoutingModule } from './tax-invoice-copy-report-routing.module';
import { PrintTaxInvoiceCopyPage } from './ti-copy/print-tax-invoice-copy.page';
import { PrintTaxInvoiceComponentModule } from 'components/tax-invoice/print-tax-invoice/print-tax-invoice.module';

@NgModule({
  declarations: [PrintTaxInvoiceCopyPage],
  imports: [CommonModule, SharedModule, PrintTaxInvoiceCopyRoutingModule, PrintTaxInvoiceComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PrintTaxInvoiceCopyReportModule {}
