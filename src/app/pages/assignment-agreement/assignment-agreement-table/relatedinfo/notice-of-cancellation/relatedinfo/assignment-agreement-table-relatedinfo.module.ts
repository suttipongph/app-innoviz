import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AssignmentAgreementTableRelatedinfoRoutingModule } from './assignment-agreement-table-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, AssignmentAgreementTableRelatedinfoRoutingModule]
})
export class AssignmentAgreementTableRelatedinfoModule {}
