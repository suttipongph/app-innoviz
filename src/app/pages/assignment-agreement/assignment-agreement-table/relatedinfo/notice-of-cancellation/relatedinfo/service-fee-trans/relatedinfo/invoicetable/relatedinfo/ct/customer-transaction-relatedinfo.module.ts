import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerTransactionRelatedinfoRoutingModule } from './customer-transaction-relatedinfo-routing.module';
import { CustomerTransactionListPage } from './ct-list/customer-transaction-list.page';
import { CustomerTransactionItemPage } from './ct-item/customer-transaction-item.page';
import { SharedModule } from 'shared/shared.module';
import { CustTransComponentModule } from 'components/cust-trans/cust-trans.module';

@NgModule({
  declarations: [CustomerTransactionListPage, CustomerTransactionItemPage],
  imports: [CommonModule, SharedModule, CustomerTransactionRelatedinfoRoutingModule, CustTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerTransactionRelatedinfoModule {}
