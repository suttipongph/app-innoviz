import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'printtaxinvoicecopy',
    loadChildren: () => import('./ti-copy/tax-invoice-report-copy.module').then((m) => m.PrintTaxInvoiceCopyReportModule)
  },
  {
    path: 'printtaxinvoiceoriginal',
    loadChildren: () => import('./ti-original/tax-invoice-original-report.module').then((m) => m.PrintTaxInvoiceOriginalReportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaxInvoiceCopyReportRoutingModule {}
