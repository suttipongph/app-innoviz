import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssignmentAgreementTableServiceFeeTransRelatedinfoRelatedInfoRoutingModule } from './assignment-agreement-table-service-fee-trans-relatedinfo-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, AssignmentAgreementTableServiceFeeTransRelatedinfoRelatedInfoRoutingModule]
})
export class AssignmentAgreementTableServiceFeeTransRelatedinfoRelatedInfoModule {}
