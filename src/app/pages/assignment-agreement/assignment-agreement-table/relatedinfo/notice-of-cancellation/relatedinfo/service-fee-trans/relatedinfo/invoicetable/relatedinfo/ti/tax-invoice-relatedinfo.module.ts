import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaxInvoiceRelatedinfoRoutingModule } from './tax-invoice-relatedinfo-routing.module';
import { TaxInvoiceListPage } from './ti-list/tax-invoice-list.page';
import { TaxInvoiceItemPage } from './ti-item/tax-invoice-item.page';
import { SharedModule } from 'shared/shared.module';
import { TaxInvoiceTableComponentModule } from 'components/tax-invoice/tax-invoice-table/tax-invoice-table.module';
import { TaxInvoiceLineComponentModule } from 'components/tax-invoice/tax-invoice-line/tax-invoice-line.module';
import { TaxInvoiceLineItemPage } from './ti-line-item/tax-invoice-line-item.page';

@NgModule({
  declarations: [TaxInvoiceListPage, TaxInvoiceItemPage, TaxInvoiceLineItemPage],
  imports: [CommonModule, SharedModule, TaxInvoiceRelatedinfoRoutingModule, TaxInvoiceTableComponentModule, TaxInvoiceLineComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TaxInvoiceRelatedinfoModule {}
