import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ConsortiumTransListPage } from './consortium-list.page';

describe('ConsortiumTransListPage', () => {
  let component: ConsortiumTransListPage;
  let fixture: ComponentFixture<ConsortiumTransListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConsortiumTransListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsortiumTransListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
