import { ComponentFixture, TestBed } from '@angular/core/testing';
import { JointVentureListPage } from './joint-venture-list.page';

describe('JointVentureListPage', () => {
  let component: JointVentureListPage;
  let fixture: ComponentFixture<JointVentureListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [JointVentureListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JointVentureListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
