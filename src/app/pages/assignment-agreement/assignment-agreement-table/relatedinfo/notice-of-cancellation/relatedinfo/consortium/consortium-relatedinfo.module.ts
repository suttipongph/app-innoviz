import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssignmentAgreementConsortiumRelatedinfoRoutingModule } from './consortium-relatedinfo-routing.module';
import { ConsortiumTransListPage } from './consortium-list/consortium-list.page';
import { ConsortiumTransItemPage } from './consortium-item/consortium-item.page';
import { SharedModule } from 'shared/shared.module';
import { ConsortiumTransComponentModule } from 'components/consortium-trans/consortium-trans.module';

@NgModule({
  declarations: [ConsortiumTransListPage, ConsortiumTransItemPage],
  imports: [CommonModule, SharedModule, AssignmentAgreementConsortiumRelatedinfoRoutingModule, ConsortiumTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AssignmentAgreementConsortiumRelatedinfoModule {}
