import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'bookmarkdocumenttrans',
    loadChildren: () =>
      import('./bookmark-document/bookmark-document-relatedinfo.module').then((m) => m.AssignmentAgreementBookmarkDocumentRelatedinfoModule)
  },
  {
    path: 'consortiumtrans',
    loadChildren: () => import('./consortium/consortium-relatedinfo.module').then((m) => m.AssignmentAgreementConsortiumRelatedinfoModule)
  },
  {
    path: 'agreementtableinfo',
    loadChildren: () =>
      import('./agreement-information/agreement-information-relatedinfo.module').then(
        (m) => m.AssignmentAgreementAgreementInformationRelatedinfoModule
      )
  },
  {
    path: 'jointventuretrans',
    loadChildren: () => import('./joint-venture/joint-venture-relatedinfo.module').then((m) => m.AssignmentAgreementJointVentureRelatedinfoModule)
  },
  {
    path: 'servicefeetrans',
    loadChildren: () =>
      import('./service-fee-trans/assignment-agreement-table-service-fee-trans-relatedinfo.module').then(
        (m) => m.AssignmentAgreementTableServiceFeeTransRelatedinfoModule
      )
  },
  {
    path: 'memotrans',
    loadChildren: () => import('./memo/memo-relatedinfo.module').then((m) => m.MemoRelatedinfoModule)
  },
  {
    path: 'attachment',
    loadChildren: () => import('./attachment/attachment-relatedinfo.module').then((m) => m.AttachmentRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssignmentAgreementTableRelatedinfoRoutingModule {}
