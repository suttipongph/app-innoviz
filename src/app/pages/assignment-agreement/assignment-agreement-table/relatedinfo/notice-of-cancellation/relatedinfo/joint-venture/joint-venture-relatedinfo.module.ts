import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssignmentAgreementJointVentureRelatedinfoRoutingModule } from './joint-venture-relatedinfo-routing.module';
import { JointVentureTransListPage } from './joint-venture-list/joint-venture-list.page';
import { JointVentureTransItemPage } from './joint-venture-item/joint-venture-item.page';
import { SharedModule } from 'shared/shared.module';
import { JointVentureTransComponentModule } from 'components/joint-venture-trans/joint-venture-trans.module';

@NgModule({
  declarations: [JointVentureTransListPage, JointVentureTransItemPage],
  imports: [CommonModule, SharedModule, AssignmentAgreementJointVentureRelatedinfoRoutingModule, JointVentureTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AssignmentAgreementJointVentureRelatedinfoModule {}
