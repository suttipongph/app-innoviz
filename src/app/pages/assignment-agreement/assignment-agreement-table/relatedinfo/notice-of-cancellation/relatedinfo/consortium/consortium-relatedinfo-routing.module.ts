import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConsortiumTransListPage } from './consortium-list/consortium-list.page';
import { ConsortiumTransItemPage } from './consortium-item/consortium-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: ConsortiumTransListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ConsortiumTransItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssignmentAgreementConsortiumRelatedinfoRoutingModule {}
