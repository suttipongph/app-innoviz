import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CopyBookmarkDocumentFromTemplatePage } from './copy-bd.page';

describe('CopyCustVisitingPage', () => {
  let component: CopyBookmarkDocumentFromTemplatePage;
  let fixture: ComponentFixture<CopyBookmarkDocumentFromTemplatePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CopyBookmarkDocumentFromTemplatePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyBookmarkDocumentFromTemplatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
