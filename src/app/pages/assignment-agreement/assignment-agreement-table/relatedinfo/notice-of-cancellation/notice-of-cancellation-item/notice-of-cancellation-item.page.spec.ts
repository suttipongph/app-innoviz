import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoticeOfCancellationItemPage } from './notice-of-cancellation-item.page';

describe('NoticeOfCancellationItemPage', () => {
  let component: NoticeOfCancellationItemPage;
  let fixture: ComponentFixture<NoticeOfCancellationItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NoticeOfCancellationItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoticeOfCancellationItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
