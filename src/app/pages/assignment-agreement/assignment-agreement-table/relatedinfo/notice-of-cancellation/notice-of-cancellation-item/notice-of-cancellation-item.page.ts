import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { MenuItem } from 'shared/models/primeModel';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'notice-of-cancellation-item.page',
  templateUrl: './notice-of-cancellation-item.page.html',
  styleUrls: ['./notice-of-cancellation-item.page.scss']
})
export class NoticeOfCancellationItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute(1);
  relatedInfoItems: MenuItem[];
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION,
      servicePath: `AssignmentAgreementTable/AssignmentAgreement/${this.masterRoute}/RelatedInfo/NoticeOfCancellation`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION_LINE,
          servicePath: `AssignmentAgreementTable/AssignmentAgreement/${this.masterRoute}/RelatedInfo/NoticeOfCancellation`
        }
      ]
    };
  }
}
