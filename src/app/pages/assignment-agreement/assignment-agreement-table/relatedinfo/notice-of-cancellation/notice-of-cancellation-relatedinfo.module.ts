import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssignmentAgreementNoticeOfCancellationRelatedinfoRoutingModule } from './notice-of-cancellation-relatedinfo-routing.module';
import { NoticeOfCancellationListPage } from './notice-of-cancellation-list/notice-of-cancellation-list.page';
import { NoticeOfCancellationItemPage } from './notice-of-cancellation-item/notice-of-cancellation-item.page';
import { SharedModule } from 'shared/shared.module';
import { AssignmentAgreementTableComponentModule } from 'components/assignment-agreement/assignment-agreement-table/assignment-agreement-table.module';
import { NoticeOfCancellationLineItemPage } from './notice-of-cancellation-line-item/notice-of-cancellation-line-item.page';
import { AssignmentAgreementLineComponentModule } from 'components/assignment-agreement/assignment-agreement-line/assignment-agreement-line.module';

@NgModule({
  declarations: [NoticeOfCancellationListPage, NoticeOfCancellationItemPage, NoticeOfCancellationLineItemPage],
  imports: [
    CommonModule,
    SharedModule,
    AssignmentAgreementNoticeOfCancellationRelatedinfoRoutingModule,
    AssignmentAgreementTableComponentModule,
    AssignmentAgreementLineComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AssignmentAgreementNoticeOfCancellationRelatedinfoModule {}
