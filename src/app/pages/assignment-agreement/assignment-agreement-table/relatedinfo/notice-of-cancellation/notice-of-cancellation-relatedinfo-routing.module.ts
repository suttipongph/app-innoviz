import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NoticeOfCancellationListPage } from './notice-of-cancellation-list/notice-of-cancellation-list.page';
import { NoticeOfCancellationItemPage } from './notice-of-cancellation-item/notice-of-cancellation-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { NoticeOfCancellationLineItemPage } from './notice-of-cancellation-line-item/notice-of-cancellation-line-item.page';

const routes: Routes = [
  {
    path: '',
    component: NoticeOfCancellationListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: NoticeOfCancellationItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/noticeofcancellation-child/:id',
    component: NoticeOfCancellationLineItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'function/generatenoticeofcancellation',
    loadChildren: () =>
      import('./functions/notice-of-cancellation/assignment-agreement-table-notice-of-cancellation-function.module').then(
        (m) => m.AssignmentAgreementTableNoticeOfCancellationFunctionModule
      )
  },
  {
    path: ':id/function',
    loadChildren: () =>
      import('./functions/assignment-agreement-table-notice-of-cancellation.module').then((m) => m.AssignmentAgreementTableNoticeOfCancellationModule)
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/assignment-agreement-table-relatedinfo.module').then((m) => m.AssignmentAgreementTableRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssignmentAgreementNoticeOfCancellationRelatedinfoRoutingModule {}
