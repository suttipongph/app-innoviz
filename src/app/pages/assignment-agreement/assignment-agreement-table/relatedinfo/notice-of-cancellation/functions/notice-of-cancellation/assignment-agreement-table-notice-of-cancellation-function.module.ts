import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { GenAssignmentAgmNoticeOfCancelComponentModule } from 'components/assignment-agreement/gen-assignment-agm-notice-of-cancel/gen-assignment-agm-notice-of-cancel.module';
import { AssignmentAgreementNoticeOfCancellationFunctionRoutingModule } from './assignment-agreement-table-notice-of-cancellation-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, AssignmentAgreementNoticeOfCancellationFunctionRoutingModule, GenAssignmentAgmNoticeOfCancelComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AssignmentAgreementTableNoticeOfCancellationFunctionModule {}
