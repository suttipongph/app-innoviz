import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel, PathParamModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-notice-of-cancellation',
  templateUrl: './notice-of-cancellation.page.html',
  styleUrls: ['./notice-of-cancellation.page.scss']
})
export class GenerateNoticeOfCancellationPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.GENERATE_NOTICE_OF_CANCELLATION,
      // servicePath: `AssignmentAgreementTable/AssignmentAgreement/${this.masterRoute}/Function/GenerateNoticeOfCancellation`
      servicePath: `AssignmentAgreementTable/AssignmentAgreement/${this.masterRoute}/Relatedinfo/Noticeofcancellation/Function/GenerateNoticeOfCancellation`
    };
  }
}
