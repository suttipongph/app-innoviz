import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { GenAssignmentAgmNoticeOfCancelComponentModule } from 'components/assignment-agreement/gen-assignment-agm-notice-of-cancel/gen-assignment-agm-notice-of-cancel.module';
import { AssignmentAgreementTableNoticeOfCancellationRoutingModule } from './assignment-agreement-table-notice-of-cancellation-routing.module';
import { GenerateNoticeOfCancellationPage } from './notice-of-cancellation/notice-of-cancellation/notice-of-cancellation.page';

@NgModule({
  declarations: [GenerateNoticeOfCancellationPage],
  imports: [CommonModule, SharedModule, AssignmentAgreementTableNoticeOfCancellationRoutingModule, GenAssignmentAgmNoticeOfCancelComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AssignmentAgreementTableNoticeOfCancellationModule {}
