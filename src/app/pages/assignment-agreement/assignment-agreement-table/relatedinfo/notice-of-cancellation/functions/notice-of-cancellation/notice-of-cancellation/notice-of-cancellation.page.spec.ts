import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GenerateNoticeOfCancellationPage } from './notice-of-cancellation.page';

describe('GenerateNoticeOfCancellationPage', () => {
  let component: GenerateNoticeOfCancellationPage;
  let fixture: ComponentFixture<GenerateNoticeOfCancellationPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GenerateNoticeOfCancellationPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateNoticeOfCancellationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
