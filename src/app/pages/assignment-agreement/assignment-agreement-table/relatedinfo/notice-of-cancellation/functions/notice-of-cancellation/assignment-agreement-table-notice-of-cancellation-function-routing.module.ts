import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { GenerateNoticeOfCancellationPage } from './notice-of-cancellation/notice-of-cancellation.page';

const routes: Routes = [
  {
    path: '',
    component: GenerateNoticeOfCancellationPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssignmentAgreementNoticeOfCancellationFunctionRoutingModule {}
