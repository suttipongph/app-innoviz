import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { GenerateNoticeOfCancellationPage } from './notice-of-cancellation/notice-of-cancellation/notice-of-cancellation.page';

const routes: Routes = [
  {
    path: '',
    component: GenerateNoticeOfCancellationPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'cancelagreement',
    loadChildren: () => import('./cancel/cancel-function.module').then((m) => m.AssignmentAgreementCancelFunctionModule)
  },
  {
    path: 'closeagreement',
    loadChildren: () => import('./close/close-function.module').then((m) => m.AssignmentAgreementCloseFunctionModule)
  },
  {
    path: 'postagreement',
    loadChildren: () => import('./post/post-function.module').then((m) => m.AssignmentAgreementPostFunctionModule)
  },
  {
    path: 'sendagreement',
    loadChildren: () => import('./send/send-function.module').then((m) => m.AssignmentAgreementSendFunctionModule)
  },
  {
    path: 'signagreement',
    loadChildren: () => import('./sign/sign-function.module').then((m) => m.AssignmentAgreementSignFunctionModule)
  },
  {
    path: 'printsetbookdoctrans',
    loadChildren: () => import('./print-book-doc/print-book-doc-function.module').then((m) => m.PrintBookDocFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssignmentAgreementTableNoticeOfCancellationRoutingModule {}
