import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'notice-of-cancellation-line-item-page',
  templateUrl: './notice-of-cancellation-line-item.page.html',
  styleUrls: ['./notice-of-cancellation-line-item.page.scss']
})
export class NoticeOfCancellationLineItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION_LINE,
      servicePath: `AssignmentAgreementTable/AssignmentAgreement/${this.masterRoute}`
    };
  }
}
