import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoticeOfCancellationLineItemPage } from './notice-of-cancellation-line-item.page';

describe('NoticeOfCancellationLineItemPage', () => {
  let component: NoticeOfCancellationLineItemPage;
  let fixture: ComponentFixture<NoticeOfCancellationLineItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NoticeOfCancellationLineItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoticeOfCancellationLineItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
