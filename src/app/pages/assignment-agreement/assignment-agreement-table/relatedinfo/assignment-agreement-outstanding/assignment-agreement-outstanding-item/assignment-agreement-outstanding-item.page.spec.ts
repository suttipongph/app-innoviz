import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AssignmentAgreementOutstandingItemPage } from './assignment-agreement-outstanding-item.page';

describe('AssignmentAgreementOutstandingItemPage', () => {
  let component: AssignmentAgreementOutstandingItemPage;
  let fixture: ComponentFixture<AssignmentAgreementOutstandingItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AssignmentAgreementOutstandingItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentAgreementOutstandingItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
