import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'assignment-agreement-outstanding-item.page',
  templateUrl: './assignment-agreement-outstanding-item.page.html',
  styleUrls: ['./assignment-agreement-outstanding-item.page.scss']
})
export class AssignmentAgreementOutstandingItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.ASSIGNMENT_AGREEMENT_TABLE,
      servicePath: 'AssignmentAgreementTable/RelatedInfo/AssignmentAgreementOutstanding'
    };
  }
}
