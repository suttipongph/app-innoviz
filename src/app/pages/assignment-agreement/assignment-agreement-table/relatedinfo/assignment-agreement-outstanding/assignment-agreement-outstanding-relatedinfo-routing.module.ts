import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AssignmentAgreementOutstandingListPage } from './assignment-agreement-outstanding-list/assignment-agreement-outstanding-list.page';
import { AssignmentAgreementOutstandingItemPage } from './assignment-agreement-outstanding-item/assignment-agreement-outstanding-item.page';

const routes: Routes = [
  {
    path: 'assignmentagreementoutstanding',
    component: AssignmentAgreementOutstandingListPage
  },
  {
    path: 'assignmentagreementoutstanding/:id',
    component: AssignmentAgreementOutstandingItemPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssignmentAgreementOutstandingRelatedinfoRoutingModule {}
