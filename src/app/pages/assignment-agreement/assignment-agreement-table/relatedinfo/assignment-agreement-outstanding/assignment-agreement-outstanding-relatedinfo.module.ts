import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssignmentAgreementOutstandingRelatedinfoRoutingModule } from './assignment-agreement-outstanding-relatedinfo-routing.module';
import { AssignmentAgreementOutstandingListPage } from './assignment-agreement-outstanding-list/assignment-agreement-outstanding-list.page';
import { AssignmentAgreementOutstandingItemPage } from './assignment-agreement-outstanding-item/assignment-agreement-outstanding-item.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [AssignmentAgreementOutstandingListPage, AssignmentAgreementOutstandingItemPage],
  imports: [CommonModule, SharedModule, AssignmentAgreementOutstandingRelatedinfoRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AssignmentAgreementOutstandingRelatedinfoModule {}
