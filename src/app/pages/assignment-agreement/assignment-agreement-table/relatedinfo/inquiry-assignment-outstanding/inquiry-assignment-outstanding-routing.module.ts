import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { InquiryAssignmentOutstandingListPage } from './inquiry-assignment-outstanding-list/inquiry-assignment-outstanding-list.page';

const routes: Routes = [
  {
    path: '',
    component: InquiryAssignmentOutstandingListPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InquiryAssignmentOutstandingRoutingModule {}
