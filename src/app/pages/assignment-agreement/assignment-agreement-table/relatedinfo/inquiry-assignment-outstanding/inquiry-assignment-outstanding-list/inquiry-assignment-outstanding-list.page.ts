import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, RefType, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'inquiry-assignment-outstanding-list.page',
  templateUrl: './inquiry-assignment-outstanding-list.page.html',
  styleUrls: ['./inquiry-assignment-outstanding-list.page.scss']
})
export class InquiryAssignmentOutstandingListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  parentId;
  masterRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService, public uiControllerService: UIControllerService) {
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'inquiryAssignmentOutstandingGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'assignmentAgreementTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.INQUIRY_ASSIGNMENT_AGREEMENT_OUTSTANDING,
      servicePath: `AssignmentAgreementTable/AssignmentAgreement/${this.masterRoute}/RelatedInfo/Inquiryassignmentagreementoutstanding`
    };
  }
}
