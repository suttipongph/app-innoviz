import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PrintBookDocTransPage } from './print-book-doc-trans.page';

describe('AddendumPage', () => {
  let component: PrintBookDocTransPage;
  let fixture: ComponentFixture<PrintBookDocTransPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PrintBookDocTransPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintBookDocTransPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
