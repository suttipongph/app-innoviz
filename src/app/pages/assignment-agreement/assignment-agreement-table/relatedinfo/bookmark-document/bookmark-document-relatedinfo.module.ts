import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssignmentAgreementBookmarkDocumentRelatedinfoRoutingModule } from './bookmark-document-relatedinfo-routing.module';
import { BookmarkDocumentTransListPage } from './bookmark-document-list/bookmark-document-list.page';
import { BookmarkDocumentTransItemPage } from './bookmark-document-item/bookmark-document-item.page';
import { SharedModule } from 'shared/shared.module';
import { BookmarkDocumentTransComponentModule } from 'components/bookmark-document-trans/bookmark-document-trans/bookmark-document-trans.module';

@NgModule({
  declarations: [BookmarkDocumentTransListPage, BookmarkDocumentTransItemPage],
  imports: [CommonModule, SharedModule, AssignmentAgreementBookmarkDocumentRelatedinfoRoutingModule, BookmarkDocumentTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AssignmentAgreementBookmarkDocumentRelatedinfoModule {}
