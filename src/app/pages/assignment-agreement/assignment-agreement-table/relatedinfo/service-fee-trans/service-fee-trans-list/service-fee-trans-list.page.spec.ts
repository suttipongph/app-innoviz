import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ServiceFeeTransListPage } from './service-fee-trans-list.page';

describe('ServiceFeeListPage', () => {
  let component: ServiceFeeTransListPage;
  let fixture: ComponentFixture<ServiceFeeTransListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ServiceFeeTransListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceFeeTransListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
