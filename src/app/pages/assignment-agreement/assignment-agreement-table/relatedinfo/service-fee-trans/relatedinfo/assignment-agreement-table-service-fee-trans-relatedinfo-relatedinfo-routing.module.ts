import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'invoicetable',
    loadChildren: () =>
      import('./invoicetable/assignment-agreement-table-service-fee-trans-invoicetable-relatedinfo.module').then(
        (m) => m.AssignmentAgreementTableServiceFeeTransInvoiceTableRelatedinfoModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssignmentAgreementTableServiceFeeTransRelatedinfoRelatedInfoRoutingModule {}
