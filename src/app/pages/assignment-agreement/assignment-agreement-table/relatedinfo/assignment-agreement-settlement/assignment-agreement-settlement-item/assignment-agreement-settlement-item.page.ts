import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'assignment-agreement-settlement-item.page',
  templateUrl: './assignment-agreement-settlement-item.page.html',
  styleUrls: ['./assignment-agreement-settlement-item.page.scss']
})
export class AssignmentAgreementSettlementItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.ASSIGNMENT_AGREEMENT_SETTLEMENT,
      servicePath: `AssignmentAgreementTable/AssignmentAgreement/${this.masterRoute}/RelatedInfo/AssignmentAgreementSettlement`
    };
  }
}
