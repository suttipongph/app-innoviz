import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AssignmentAgreementSettlementItemPage } from './assignment-agreement-settlement-item.page';

describe('AssignmentAgreementSettlementItemPage', () => {
  let component: AssignmentAgreementSettlementItemPage;
  let fixture: ComponentFixture<AssignmentAgreementSettlementItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AssignmentAgreementSettlementItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentAgreementSettlementItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
