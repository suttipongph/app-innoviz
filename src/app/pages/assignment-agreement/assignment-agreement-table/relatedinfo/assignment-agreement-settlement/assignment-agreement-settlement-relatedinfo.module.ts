import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssignmentAgreementSettlementRelatedinfoRoutingModule } from './assignment-agreement-settlement-relatedinfo-routing.module';
import { AssignmentAgreementSettlementListPage } from './assignment-agreement-settlement-list/assignment-agreement-settlement-list.page';
import { AssignmentAgreementSettlementItemPage } from './assignment-agreement-settlement-item/assignment-agreement-settlement-item.page';
import { SharedModule } from 'shared/shared.module';
import { AssignmentAgreementTableComponentModule } from 'components/assignment-agreement/assignment-agreement-table/assignment-agreement-table.module';
import { AssignmentAgreementSettleComponentModule } from 'components/assignment-agreement/assignment-agreement-settle/assignment-agreement-settle.module';
// import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [AssignmentAgreementSettlementListPage, AssignmentAgreementSettlementItemPage],
  imports: [CommonModule, SharedModule, AssignmentAgreementSettlementRelatedinfoRoutingModule, AssignmentAgreementSettleComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AssignmentAgreementSettlementRelatedinfoModule {}
