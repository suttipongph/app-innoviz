import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'assignment-agreement-settlement-list-page',
  templateUrl: './assignment-agreement-settlement-list.page.html',
  styleUrls: ['./assignment-agreement-settlement-list.page.scss']
})
export class AssignmentAgreementSettlementListPage implements OnInit {
  option: OptionModel;
  masterRoute = this.uiService.getMasterRoute(1);
  pageInfo: PageInformationModel;
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'assignmentAgreementSettleGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.ASSIGNMENT_AGREEMENT_SETTLEMENT,
      servicePath: `AssignmentAgreementTable/AssignmentAgreement/${this.masterRoute}/RelatedInfo/AssignmentAgreementSettlement`
    };
  }
}
