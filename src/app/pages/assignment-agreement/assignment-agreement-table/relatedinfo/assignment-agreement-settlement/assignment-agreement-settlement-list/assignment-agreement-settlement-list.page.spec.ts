import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AssignmentAgreementSettlementListPage } from './assignment-agreement-settlement-list.page';

describe('AssignmentAgreementSettlementListPage', () => {
  let component: AssignmentAgreementSettlementListPage;
  let fixture: ComponentFixture<AssignmentAgreementSettlementListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AssignmentAgreementSettlementListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentAgreementSettlementListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
