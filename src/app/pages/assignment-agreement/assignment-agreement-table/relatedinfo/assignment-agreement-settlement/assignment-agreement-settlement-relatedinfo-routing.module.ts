import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { AssignmentAgreementSettlementItemPage } from './assignment-agreement-settlement-item/assignment-agreement-settlement-item.page';
import { AssignmentAgreementSettlementListPage } from './assignment-agreement-settlement-list/assignment-agreement-settlement-list.page';

const routes: Routes = [
  {
    path: '',
    component: AssignmentAgreementSettlementListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: AssignmentAgreementSettlementItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/function',
    loadChildren: () =>
      import('./functions/assignment-agreement-settlement-function.module').then((m) => m.AssignmentAgreementSettlementFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssignmentAgreementSettlementRelatedinfoRoutingModule {}
