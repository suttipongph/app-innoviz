import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UpdateAssignmentAgreementSettlePage } from './update-assignment-agreement-settle.page';

describe('UpdateAssignmentAgreementSettlePage', () => {
  let component: UpdateAssignmentAgreementSettlePage;
  let fixture: ComponentFixture<UpdateAssignmentAgreementSettlePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UpdateAssignmentAgreementSettlePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAssignmentAgreementSettlePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
