import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AssignmentAgreementSettlementFunctionRoutingModule } from './assignment-agreement-settlement-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, AssignmentAgreementSettlementFunctionRoutingModule]
})
export class AssignmentAgreementSettlementFunctionModule {}
