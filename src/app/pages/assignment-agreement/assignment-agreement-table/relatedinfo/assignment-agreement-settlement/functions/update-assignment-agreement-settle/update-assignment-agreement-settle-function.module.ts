import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { UpdateAssignmentAgreementSettlePage } from './update-assignment-agreement-settle/update-assignment-agreement-settle.page';
import { UpdateAssignmentAgreementSettleFunctionRoutingModule } from './update-assignment-agreement-settle-function-routing.module';
import { UpdateAssignmentAgreementSettleComponentModule } from 'components/assignment-agreement/update-assignment-agreement-settle/update-assignment-agreement-settle.module';

@NgModule({
  declarations: [UpdateAssignmentAgreementSettlePage],
  imports: [CommonModule, SharedModule, UpdateAssignmentAgreementSettleFunctionRoutingModule, UpdateAssignmentAgreementSettleComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UpdateAssignmentAgreementSettleFunctionModule {}
