import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { UpdateAssignmentAgreementSettlePage } from './update-assignment-agreement-settle/update-assignment-agreement-settle.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateAssignmentAgreementSettlePage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpdateAssignmentAgreementSettleFunctionRoutingModule {}
