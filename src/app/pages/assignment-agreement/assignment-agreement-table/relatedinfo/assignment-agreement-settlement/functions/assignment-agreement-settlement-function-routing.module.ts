import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'updateassignmentagreementsettle',
    loadChildren: () =>
      import('./update-assignment-agreement-settle/update-assignment-agreement-settle-function.module').then(
        (m) => m.UpdateAssignmentAgreementSettleFunctionModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssignmentAgreementSettlementFunctionRoutingModule {}
