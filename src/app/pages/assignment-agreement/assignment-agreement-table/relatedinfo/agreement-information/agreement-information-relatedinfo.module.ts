import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssignmentAgreementAgreementInformationRelatedinfoRoutingModule } from './agreement-information-relatedinfo-routing.module';
import { AgreementTableInfoListPage } from './agreement-information-list/agreement-information-list.page';
import { AgreementTableInfoItemPage } from './agreement-information-item/agreement-information-item.page';
import { SharedModule } from 'shared/shared.module';
import { AgreementTableInfoComponentModule } from 'components/agreement-table-info/agreement-table-info.module';

@NgModule({
  declarations: [AgreementTableInfoListPage, AgreementTableInfoItemPage],
  imports: [CommonModule, SharedModule, AssignmentAgreementAgreementInformationRelatedinfoRoutingModule, AgreementTableInfoComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AssignmentAgreementAgreementInformationRelatedinfoModule {}
