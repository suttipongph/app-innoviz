import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AssignmentAgreementTableListPage } from './assignment-agreement-table-list.page';

describe('AssignmentAgreementTableListPage', () => {
  let component: AssignmentAgreementTableListPage;
  let fixture: ComponentFixture<AssignmentAgreementTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AssignmentAgreementTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentAgreementTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
