import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { AgreementDocType, ColumnType, ROUTE_MASTER_GEN, SortType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'assignment-agreement-table-list-page',
  templateUrl: './assignment-agreement-table-list.page.html',
  styleUrls: ['./assignment-agreement-table-list.page.scss']
})
export class AssignmentAgreementTableListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'assignmentAgreementTableGUID';
    const columns: ColumnModel[] = [];
    this.setColumnOptionByPath(columns);
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.ASSIGNMENT_AGREEMENT_TABLE,
      servicePath: `AssignmentAgreementTable/AssignmentAgreement/${this.masterRoute}`
    };
  }
  setColumnOptionByPath(columns: ColumnModel[]) {
    if (this.masterRoute === 'new') {
      columns.push({
        label: null,
        textKey: 'agreementDocType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: AgreementDocType.New.toString()
      });
    } else if (this.masterRoute === 'all') {      
      this.option.canCreate = false;
    }
  }
}
