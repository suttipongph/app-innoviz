import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { AssignmentAgreementLineItemPage } from './assignment-agreement-line-item/assignment-agreement-line-item.page';
import { AssignmentAgreementTableItemPage } from './assignment-agreement-table-item/assignment-agreement-table-item.page';
import { AssignmentAgreementTableListPage } from './assignment-agreement-table-list/assignment-agreement-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: AssignmentAgreementTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: AssignmentAgreementTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/assignmentagreementline-child/:id',
    component: AssignmentAgreementLineItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/assignment-agreement-table-relatedinfo.module').then((m) => m.AssignmentAgreementTableRelatedinfoModule)
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/assignment-agreement-table-function.module').then((m) => m.AssignmentAgreementTableFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssignmentAgreementTableRoutingModule {}
