import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssignmentAgreementTableRoutingModule } from './assignment-agreement-table-routing.module';
import { AssignmentAgreementTableListPage } from './assignment-agreement-table-list/assignment-agreement-table-list.page';
import { AssignmentAgreementTableItemPage } from './assignment-agreement-table-item/assignment-agreement-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { AssignmentAgreementTableComponentModule } from 'components/assignment-agreement/assignment-agreement-table/assignment-agreement-table.module';
import { AssignmentAgreementLineComponentModule } from 'components/assignment-agreement/assignment-agreement-line/assignment-agreement-line.module';
import { AssignmentAgreementLineItemPage } from './assignment-agreement-line-item/assignment-agreement-line-item.page';

@NgModule({
  declarations: [AssignmentAgreementTableListPage, AssignmentAgreementTableItemPage, AssignmentAgreementLineItemPage],
  imports: [
    CommonModule,
    SharedModule,
    AssignmentAgreementTableRoutingModule,
    AssignmentAgreementTableComponentModule,
    AssignmentAgreementLineComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AssignmentAgreementTableModule {}
