import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AssignmentAgreementTableItemPage } from './assignment-agreement-table-item.page';

describe('AssignmentAgreementTableItemPage', () => {
  let component: AssignmentAgreementTableItemPage;
  let fixture: ComponentFixture<AssignmentAgreementTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AssignmentAgreementTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentAgreementTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
