import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'assignment-agreement-table-item-page',
  templateUrl: './assignment-agreement-table-item.page.html',
  styleUrls: ['./assignment-agreement-table-item.page.scss']
})
export class AssignmentAgreementTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.ASSIGNMENT_AGREEMENT_TABLE,
      servicePath: `AssignmentAgreementTable/AssignmentAgreement/${this.masterRoute}`,
      childPaths: [
        { pagePath: ROUTE_MASTER_GEN.ASSIGNMENT_AGREEMENT_LINE, servicePath: `AssignmentAgreementTable/AssignmentAgreement/${this.masterRoute}` }
      ]
    };
  }
}
