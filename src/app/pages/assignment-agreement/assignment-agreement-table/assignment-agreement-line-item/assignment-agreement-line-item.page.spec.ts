import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AssignmentAgreementLineItemPage } from './assignment-agreement-line-item.page';

describe('AssignmentAgreementLineItemPage', () => {
  let component: AssignmentAgreementLineItemPage;
  let fixture: ComponentFixture<AssignmentAgreementLineItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AssignmentAgreementLineItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentAgreementLineItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
