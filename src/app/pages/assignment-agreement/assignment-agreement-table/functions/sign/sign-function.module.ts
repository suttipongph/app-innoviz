import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssignmentAgreementSignFunctionRoutingModule } from './sign-function-routing.module';
import { SignAgreementPage } from './sign/sign.page';
import { SharedModule } from 'shared/shared.module';
import { ManageAgreementComponentModule } from 'components/function/manage-agreement/manage-agreement.module';

@NgModule({
  declarations: [SignAgreementPage],
  imports: [CommonModule, SharedModule, AssignmentAgreementSignFunctionRoutingModule, ManageAgreementComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AssignmentAgreementSignFunctionModule {}
