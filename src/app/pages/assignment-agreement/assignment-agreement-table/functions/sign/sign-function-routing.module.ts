import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { SignAgreementPage } from './sign/sign.page';

const routes: Routes = [
  {
    path: '',
    component: SignAgreementPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssignmentAgreementSignFunctionRoutingModule {}
