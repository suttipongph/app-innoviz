import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AssignmentAgreementTableFunctionRoutingModule } from './assignment-agreement-table-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, AssignmentAgreementTableFunctionRoutingModule]
})
export class AssignmentAgreementTableFunctionModule {}
