import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel, PathParamModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-close',
  templateUrl: './close.page.html',
  styleUrls: ['./close.page.scss']
})
export class CloseAgreementPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.CLOSE_AGREEMENT,
      servicePath: `AssignmentAgreementTable/AssignmentAgreement/${this.masterRoute}/Function/CloseAgreement`
    };
  }
}
