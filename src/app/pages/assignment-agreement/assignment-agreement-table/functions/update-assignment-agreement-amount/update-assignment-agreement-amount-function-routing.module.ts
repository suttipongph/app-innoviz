import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { UpdateAssignmentAgreementAmountPage } from './update-assignment-agreement-amount/update-assignment-agreement-amount.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateAssignmentAgreementAmountPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpdateAssignmentAgreementAmountFunctionRoutingModule {}
