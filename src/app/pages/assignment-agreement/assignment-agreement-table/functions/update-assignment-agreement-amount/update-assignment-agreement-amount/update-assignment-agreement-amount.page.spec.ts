import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UpdateAssignmentAgreementAmountPage } from './update-assignment-agreement-amount.page';

describe('UpdateAssignmentAgreementAmountPage', () => {
  let component: UpdateAssignmentAgreementAmountPage;
  let fixture: ComponentFixture<UpdateAssignmentAgreementAmountPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UpdateAssignmentAgreementAmountPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAssignmentAgreementAmountPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
