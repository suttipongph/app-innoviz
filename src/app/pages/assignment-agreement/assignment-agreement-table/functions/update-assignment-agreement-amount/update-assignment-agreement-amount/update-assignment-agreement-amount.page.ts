import { Component } from '@angular/core';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel, PathParamModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-update-assignment-agreement-amount',
  templateUrl: './update-assignment-agreement-amount.page.html',
  styleUrls: ['./update-assignment-agreement-amount.page.scss']
})
export class UpdateAssignmentAgreementAmountPage extends BaseItemComponent<any> {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute(1);
  constructor() {
    super();
    this.path = ROUTE_FUNCTION_GEN.UPDATE_ASSIGNMENT_AGREEMENT_AMOUNT;
  }
  onRelatedMenu(): void {
    const path: PathParamModel = {};
    this.toRelatedInfo(path);
  }
  toRelatedInfo(param: PathParamModel): void {
    super.toRelatedInfo(param);
  }
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.UPDATE_ASSIGNMENT_AGREEMENT_AMOUNT,
      servicePath: `AssignmentAgreementTable/AssignmentAgreement/${this.masterRoute}/Function/UpdateAssignmentAgreementAmount`
    };
  }
}
