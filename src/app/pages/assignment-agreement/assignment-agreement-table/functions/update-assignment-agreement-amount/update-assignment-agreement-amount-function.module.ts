import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateAssignmentAgreementAmountFunctionRoutingModule } from './update-assignment-agreement-amount-function-routing.module';
import { UpdateAssignmentAgreementAmountPage } from './update-assignment-agreement-amount/update-assignment-agreement-amount.page';
import { UpdateAssignmentAgreementAmountComponentModule } from 'components/assignment-agreement/update-assignment-agreement-amount/update-assignment-agreement-amount.module';
import { SharedModule } from 'shared/shared.module';

@NgModule({
  declarations: [UpdateAssignmentAgreementAmountPage],
  imports: [CommonModule, SharedModule, UpdateAssignmentAgreementAmountFunctionRoutingModule, UpdateAssignmentAgreementAmountComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UpdateAssignmentAgreementAmountFunctionModule {}
