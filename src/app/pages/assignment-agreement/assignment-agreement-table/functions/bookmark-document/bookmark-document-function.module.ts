import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookmarkDocumentFunctionRoutingModule } from './bookmark-document-function-routing.module';
import { BookmarkDocumentPage } from './bookmark-document/bookmark-document.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [BookmarkDocumentPage],
  imports: [CommonModule, SharedModule, BookmarkDocumentFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BookmarkDocumentFunctionModule {}
