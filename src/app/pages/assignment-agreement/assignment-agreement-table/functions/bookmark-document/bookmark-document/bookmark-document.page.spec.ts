import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BookmarkDocumentPage } from './bookmark-document.page';

describe('BookmarkDocumentPage', () => {
  let component: BookmarkDocumentPage;
  let fixture: ComponentFixture<BookmarkDocumentPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BookmarkDocumentPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkDocumentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
