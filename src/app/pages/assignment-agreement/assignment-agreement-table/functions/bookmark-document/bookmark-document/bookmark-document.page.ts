import { Component } from '@angular/core';
import { BaseItemComponent } from 'src/app/core/components/base-item/base-item.component';
import { ROUTE_FUNCTION_GEN } from 'src/app/shared/constants';
import { PathParamModel } from 'src/app/shared/models/systemModel';

@Component({
  selector: 'app-bookmark-document',
  templateUrl: './bookmark-document.page.html',
  styleUrls: ['./bookmark-document.page.scss']
})
export class BookmarkDocumentPage extends BaseItemComponent<any> {
  constructor() {
    super();
    this.path = ROUTE_FUNCTION_GEN.ASSIGNMENT_AGREEMENT_TABLE_BOOKMARK_DOCUMENT;
  }
  onRelatedMenu(): void {
    const path: PathParamModel = {};
    this.toRelatedInfo(path);
  }
  toRelatedInfo(param: PathParamModel): void {
    super.toRelatedInfo(param);
  }
  ngOnInit(): void {}
}
