import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookmarkDocumentPage } from './bookmark-document/bookmark-document.page';

const routes: Routes = [
  {
    path: 'bookmarkdocument/:id',
    component: BookmarkDocumentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookmarkDocumentFunctionRoutingModule {}
