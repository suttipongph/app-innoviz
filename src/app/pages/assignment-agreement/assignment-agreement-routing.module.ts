import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  // {
  //   path: 'assignmentagmoutstanding',
  //   loadChildren: () => import('./assignment-agm-outstanding/assignment-agm-outstanding.module').then((m) => m.AssignmentAgmOutstandingModule)
  // },
  // {
  //   path: 'assignmentagreementsettle',
  //   loadChildren: () => import('./assignment-agreement-settle/assignment-agreement-settle.module').then((m) => m.AssignmentAgreementSettleModule)
  // },
  {
    path: 'new/assignmentagreementtable',
    loadChildren: () => import('./assignment-agreement-table/assignment-agreement-table.module').then((m) => m.AssignmentAgreementTableModule)
  },
  {
    path: 'all/assignmentagreementtable',
    loadChildren: () => import('./assignment-agreement-table/assignment-agreement-table.module').then((m) => m.AssignmentAgreementTableModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssignmentAgreementRoutingModule {}
