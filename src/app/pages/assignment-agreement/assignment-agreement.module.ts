import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssignmentAgreementRoutingModule } from './assignment-agreement-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, AssignmentAgreementRoutingModule]
})
export class AssignmentAgreementModule {}
