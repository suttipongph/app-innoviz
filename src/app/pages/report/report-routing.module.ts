import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'outstanding/report/reportwithdrawaltermextension',
    loadChildren: () =>
      import('./report-withdrawal-term-extension/report-withdrawal-term-extension.module').then((m) => m.ReportWithdrawalTermExtensionModule)
  },
  {
    path: 'outstanding/report/reportservicefeefactrealization',
    loadChildren: () =>
      import('./report-service-fee-fact-realization/report-service-fee-fact-realization.module').then((m) => m.ReportServiceFeeFactRealizationModule)
  },
  {
    path: 'outstanding/report/exportagingfactoring',
    loadChildren: () => import('./export-aging-factoring/export-aging-factoring.module').then((m) => m.ExportAgingFactoringModule)
  },
  {
    path: 'outstanding/report/exportagingprojectfinance',
    loadChildren: () => import('./export-aging-project-finance/export-aging-project-finance.module').then((m) => m.ExportAgingProjectFinanceModule)
  },
  {
    path: 'outstanding/report/exportaccruedinterest',
    loadChildren: () => import('./export-accrued-interest/export-accrued-interest.module').then((m) => m.ExportAccruedInterestModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule {}
