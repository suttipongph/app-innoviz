import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportWithdrawalTermExtensionRoutingModule } from './report-withdrawal-term-extension-routing.module';
import { ReportWithdrawalTermExtensionPage } from './report-withdrawal-term-extension/report-withdrawal-term-extension.page';
import { SharedModule } from 'shared/shared.module';
import { ReportWithdrawalTermExtensionComponentModule } from 'components/report/report-withdrawal-term-extension/report-withdrawal-term-extension.module';

@NgModule({
  declarations: [ReportWithdrawalTermExtensionPage],
  imports: [CommonModule, SharedModule, ReportWithdrawalTermExtensionRoutingModule, ReportWithdrawalTermExtensionComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReportWithdrawalTermExtensionModule {}
