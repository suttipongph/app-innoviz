import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReportWithdrawalTermExtensionPage } from './report-withdrawal-term-extension.page';

describe('ReportWithdrawalTermExtensionItemPage', () => {
  let component: ReportWithdrawalTermExtensionPage;
  let fixture: ComponentFixture<ReportWithdrawalTermExtensionPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReportWithdrawalTermExtensionPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportWithdrawalTermExtensionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
