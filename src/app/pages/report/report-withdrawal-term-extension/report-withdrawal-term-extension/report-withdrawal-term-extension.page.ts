import { Component, OnInit } from '@angular/core';
import { ROUTE_REPORT_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'report-withdrawal-term-extension-page',
  templateUrl: './report-withdrawal-term-extension.page.html',
  styleUrls: ['./report-withdrawal-term-extension.page.scss']
})
export class ReportWithdrawalTermExtensionPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_REPORT_GEN.REPORT_WITHDRAWAL_TERM_EXTENSION,
      servicePath: `Report/Report/${ROUTE_REPORT_GEN.REPORT_WITHDRAWAL_TERM_EXTENSION}`
    };
  }
}
