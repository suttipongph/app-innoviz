import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ReportWithdrawalTermExtensionPage } from './report-withdrawal-term-extension/report-withdrawal-term-extension.page';

const routes: Routes = [
  {
    path: '',
    component: ReportWithdrawalTermExtensionPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportWithdrawalTermExtensionRoutingModule {}
