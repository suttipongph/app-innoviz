import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ReportServiceFeeFactRealizationPage } from './report-service-fee-fact-realization/report-service-fee-fact-realization.page';

const routes: Routes = [
  {
    path: '',
    component: ReportServiceFeeFactRealizationPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportServiceFeeFactRealizationRoutingModule {}
