import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportServiceFeeFactRealizationRoutingModule } from './report-service-fee-fact-realization-routing.module';
import { ReportServiceFeeFactRealizationPage } from './report-service-fee-fact-realization/report-service-fee-fact-realization.page';
import { SharedModule } from 'shared/shared.module';
import { ReportServiceFeeFactRealizationComponentModule } from 'components/report/report-service-fee-fact-realization/report-service-fee-fact-realization.module';

@NgModule({
  declarations: [ReportServiceFeeFactRealizationPage],
  imports: [CommonModule, SharedModule, ReportServiceFeeFactRealizationRoutingModule, ReportServiceFeeFactRealizationComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReportServiceFeeFactRealizationModule {}
