import { Component, OnInit } from '@angular/core';
import { ROUTE_REPORT_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'report-service-fee-fact-realization-page',
  templateUrl: './report-service-fee-fact-realization.page.html',
  styleUrls: ['./report-service-fee-fact-realization.page.scss']
})
export class ReportServiceFeeFactRealizationPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_REPORT_GEN.REPORT_SERVICE_FEE_FACT_REALIZATION,
      servicePath: `Report/Report/${ROUTE_REPORT_GEN.REPORT_SERVICE_FEE_FACT_REALIZATION}`
    };
  }
}
