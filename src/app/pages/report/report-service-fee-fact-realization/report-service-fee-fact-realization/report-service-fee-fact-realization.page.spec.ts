import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReportServiceFeeFactRealizationItemPage } from './report-service-fee-fact-realization-item.page';

describe('ReportServiceFeeFactRealizationItemPage', () => {
  let component: ReportServiceFeeFactRealizationItemPage;
  let fixture: ComponentFixture<ReportServiceFeeFactRealizationItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReportServiceFeeFactRealizationItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportServiceFeeFactRealizationItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
