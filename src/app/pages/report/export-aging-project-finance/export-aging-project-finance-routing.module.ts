import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ExportAgingProjectFinancePage } from './export-aging-project-finance/export-aging-project-finance.page';

const routes: Routes = [
  {
    path: '',
    component: ExportAgingProjectFinancePage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExportAgingProjectFinanceRoutingModule {}
