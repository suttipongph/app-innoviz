import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { ReportAgingComponentModule } from 'components/report/report-aging/report-aging.module';
import { ExportAgingProjectFinanceRoutingModule } from './export-aging-project-finance-routing.module';
import { ExportAgingProjectFinancePage } from './export-aging-project-finance/export-aging-project-finance.page';

@NgModule({
  declarations: [ExportAgingProjectFinancePage],
  imports: [CommonModule, SharedModule, ExportAgingProjectFinanceRoutingModule, ReportAgingComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExportAgingProjectFinanceModule {}
