import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ExportAgingProjectFinancePage } from './export-aging-project-finance.page';

describe('ExportAgingProjectFinancePage', () => {
  let component: ExportAgingProjectFinancePage;
  let fixture: ComponentFixture<ExportAgingProjectFinancePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExportAgingProjectFinancePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportAgingProjectFinancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
