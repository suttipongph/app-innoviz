import { Component, OnInit } from '@angular/core';
import { ROUTE_REPORT_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'export-aging-project-finance-page',
  templateUrl: './export-aging-project-finance.page.html',
  styleUrls: ['./export-aging-project-finance.page.scss']
})
export class ExportAgingProjectFinancePage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_REPORT_GEN.EXPORT_AGING_PROJECT_FINANCE,
      servicePath: `Report/Report/${ROUTE_REPORT_GEN.EXPORT_AGING_PROJECT_FINANCE}`
    };
  }
}
