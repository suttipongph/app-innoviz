import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ExportAccruedInterestPage } from './export-accrued-interest/export-accrued-interest.page';

const routes: Routes = [
  {
    path: '',
    component: ExportAccruedInterestPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExportAccruedInterestRoutingModule {}
