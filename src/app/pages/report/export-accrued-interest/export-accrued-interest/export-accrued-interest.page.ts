import { Component, OnInit } from '@angular/core';
import { ROUTE_REPORT_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'export-accrued-interest-page',
  templateUrl: './export-accrued-interest.page.html',
  styleUrls: ['./export-accrued-interest.page.scss']
})
export class ExportAccruedInterestPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_REPORT_GEN.EXPORT_ACCRUED_INTEREST,
      servicePath: `Report/Report/${ROUTE_REPORT_GEN.EXPORT_ACCRUED_INTEREST}`
    };
  }
}
