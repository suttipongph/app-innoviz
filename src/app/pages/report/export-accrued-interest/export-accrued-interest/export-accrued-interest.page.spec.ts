import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ExportAccruedInterestPage } from './export-accrued-interest.page';

describe('ExportAccruedInterestPage', () => {
  let component: ExportAccruedInterestPage;
  let fixture: ComponentFixture<ExportAccruedInterestPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExportAccruedInterestPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportAccruedInterestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
