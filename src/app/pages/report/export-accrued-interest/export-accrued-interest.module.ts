import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExportAccruedInterestRoutingModule } from './export-accrued-interest-routing.module';
import { SharedModule } from 'shared/shared.module';
import { ExportAccruedInterestPage } from './export-accrued-interest/export-accrued-interest.page';
import { ReportAccruedInterestComponentModule } from 'components/report/report-accrued-interest/report-accrued-interest.module';

@NgModule({
  declarations: [ExportAccruedInterestPage],
  imports: [CommonModule, SharedModule, ExportAccruedInterestRoutingModule, ReportAccruedInterestComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExportAccruedInterestModule {}
