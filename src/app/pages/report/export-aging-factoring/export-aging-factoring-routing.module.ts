import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ExportAgingFactoringPage } from './export-aging-factoring/export-aging-factoring.page';

const routes: Routes = [
  {
    path: '',
    component: ExportAgingFactoringPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExportAgingFactoringRoutingModule {}
