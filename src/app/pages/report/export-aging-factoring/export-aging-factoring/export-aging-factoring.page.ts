import { Component, OnInit } from '@angular/core';
import { ROUTE_REPORT_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'export-aging-factoring-page',
  templateUrl: './export-aging-factoring.page.html',
  styleUrls: ['./export-aging-factoring.page.scss']
})
export class ExportAgingFactoringPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_REPORT_GEN.EXPORT_AGING_FACTORING,
      servicePath: `Report/Report/${ROUTE_REPORT_GEN.EXPORT_AGING_FACTORING}`
    };
  }
}
