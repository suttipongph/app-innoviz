import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ExportAgingFactoringPage } from './export-aging-factoring.page';

describe('ExportAgingFactoringPage', () => {
  let component: ExportAgingFactoringPage;
  let fixture: ComponentFixture<ExportAgingFactoringPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExportAgingFactoringPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportAgingFactoringPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
