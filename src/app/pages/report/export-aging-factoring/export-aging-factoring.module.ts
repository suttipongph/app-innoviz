import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExportAgingFactoringRoutingModule } from './export-aging-factoring-routing.module';
import { ExportAgingFactoringPage } from './export-aging-factoring/export-aging-factoring.page';
import { SharedModule } from 'shared/shared.module';
import { ReportWithdrawalTermExtensionComponentModule } from 'components/report/report-withdrawal-term-extension/report-withdrawal-term-extension.module';
import { ReportAgingComponentModule } from 'components/report/report-aging/report-aging.module';

@NgModule({
  declarations: [ExportAgingFactoringPage],
  imports: [CommonModule, SharedModule, ExportAgingFactoringRoutingModule, ReportAgingComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExportAgingFactoringModule {}
