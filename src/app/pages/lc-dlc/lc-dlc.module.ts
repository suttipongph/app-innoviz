import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LCDLCRoutingModule } from './lc-dlc-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, LCDLCRoutingModule]
})
export class LCDLCModule {}
