import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'migrationtable',
    loadChildren: () => import('./migration-table/migration-table.module').then((m) => m.MigrationTableModule)
  },
  {
    path: 'migrationlogtable',
    loadChildren: () => import('./migration-log-table/migration-log-table.module').then((m) => m.MigrationLogTableModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OBFrameworkRoutingModule {}
