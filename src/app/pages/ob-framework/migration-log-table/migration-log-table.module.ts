import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { MigrationLogTablePage } from './migration-log-table/migration-log-table.page';
import { MigrationLogRoutingModule } from './migration-log-table-routing.module';
import { MigrationLogTableComponentModule } from 'components/framework/migration-log-table/migration-log-table.module';

@NgModule({
  declarations: [MigrationLogTablePage],
  imports: [CommonModule, SharedModule, MigrationLogRoutingModule, MigrationLogTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MigrationLogTableModule {}
