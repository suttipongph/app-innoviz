import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_OBFRAMEWORK } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'migration-log-table-page',
  templateUrl: './migration-log-table.page.html',
  styleUrls: ['./migration-log-table.page.scss']
})
export class MigrationLogTablePage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'id';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_OBFRAMEWORK.MIGRATION_LOG, servicePath: 'MigrationLogTable' };
  }
}
