import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MigrationLogTablePage } from './migration-log-table.page';

describe('MigrationLogTablePageComponent', () => {
  let component: MigrationLogTablePage;
  let fixture: ComponentFixture<MigrationLogTablePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MigrationLogTablePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MigrationLogTablePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
