import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_OBFRAMEWORK } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'migration-table-item.page',
  templateUrl: './migration-table-item.page.html',
  styleUrls: ['./migration-table-item.page.scss']
})
export class MigrationTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_OBFRAMEWORK.MIGRATION, servicePath: 'MigrationTable' };
  }
}
