import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MigrationTableItemPage } from './migration-table-item.page';

describe('MigrationTableItemPageComponent', () => {
  let component: MigrationTableItemPage;
  let fixture: ComponentFixture<MigrationTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MigrationTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MigrationTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
