import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MigrationTableRoutingModule } from './migration-table-routing.module';
import { SharedModule } from 'shared/shared.module';
import { MigrationTableItemPage } from './migration-table-item/migration-table-item.page';
import { MigrationTableListPage } from './migration-table-list/migration-table-list.page';
import { MigrationTableComponentModule } from 'components/framework/migration-table/migration-table.module';

@NgModule({
  declarations: [MigrationTableListPage, MigrationTableItemPage],
  imports: [CommonModule, SharedModule, MigrationTableRoutingModule, MigrationTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MigrationTableModule {}
