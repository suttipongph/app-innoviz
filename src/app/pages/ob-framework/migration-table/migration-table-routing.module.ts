import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { MigrationTableItemPage } from './migration-table-item/migration-table-item.page';
import { MigrationTableListPage } from './migration-table-list/migration-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: MigrationTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: MigrationTableItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MigrationTableRoutingModule {}
