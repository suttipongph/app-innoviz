import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_OBFRAMEWORK } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'migration-table-list-page',
  templateUrl: './migration-table-list.page.html',
  styleUrls: ['./migration-table-list.page.scss']
})
export class MigrationTableListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'migrationTableGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_OBFRAMEWORK.MIGRATION, servicePath: 'MigrationTable' };
  }
}
