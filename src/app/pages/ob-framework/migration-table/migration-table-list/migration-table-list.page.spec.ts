import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MigrationTableListPage } from './migration-table-list.page';

describe('MigrationTableListPageComponent', () => {
  let component: MigrationTableListPage;
  let fixture: ComponentFixture<MigrationTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MigrationTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MigrationTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
