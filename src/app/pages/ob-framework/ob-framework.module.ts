import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { OBFrameworkRoutingModule } from './ob-framework-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, OBFrameworkRoutingModule]
})
export class OBFrameworkModule {}
