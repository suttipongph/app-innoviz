import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { GenWithdrawalLineInvoiceComponentModule } from 'components/framework/migration-function/gen-withdrawal-line-invoice/gen-withdrawal-line-invoice.module';
import { GenWithdrawalLineInvoicePage } from './gen-withdrawal-line-invoice/gen-withdrawal-line-invoice.page';
import { GenWithdrawalLineInvoiceFunctionRoutingModule } from './gen-withdrawal-line-invoice-function-routing.module';

@NgModule({
  declarations: [GenWithdrawalLineInvoicePage],
  imports: [CommonModule, SharedModule, GenWithdrawalLineInvoiceFunctionRoutingModule, GenWithdrawalLineInvoiceComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GenWithdrawalLineInvoiceFunctionModule {}
