import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GenWithdrawalLineInvoicePage } from './gen-withdrawal-line-invoice.page';

describe('PostPage', () => {
  let component: GenWithdrawalLineInvoicePage;
  let fixture: ComponentFixture<GenWithdrawalLineInvoicePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GenWithdrawalLineInvoicePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenWithdrawalLineInvoicePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
