import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN, ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-gen-withdrawal-line-invoice',
  templateUrl: './gen-withdrawal-line-invoice.page.html',
  styleUrls: ['./gen-withdrawal-line-invoice.page.scss']
})
export class GenWithdrawalLineInvoicePage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.GEN_WITHDRAWAL_LINE_INVOICE,
      servicePath: `MigrationTable/Function/GenWithdrawalLineInvoice`
    };
  }
}
