import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { GenWithdrawalLineInvoicePage } from './gen-withdrawal-line-invoice/gen-withdrawal-line-invoice.page';

const routes: Routes = [
  {
    path: '',
    component: GenWithdrawalLineInvoicePage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GenWithdrawalLineInvoiceFunctionRoutingModule {}
