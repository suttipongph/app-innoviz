import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GenPurchaseLineInvoicePage } from './gen-purchase-line-invoice.page';

describe('PostPage', () => {
  let component: GenPurchaseLineInvoicePage;
  let fixture: ComponentFixture<GenPurchaseLineInvoicePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GenPurchaseLineInvoicePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenPurchaseLineInvoicePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
