import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { GenPurchaseLineInvoiceComponentModule } from 'components/framework/migration-function/gen-purchase-line-invoice/gen-purchase-line-invoice.module';
import { GenPurchaseLineInvoicePage } from './gen-purchase-line-invoice/gen-purchase-line-invoice.page';
import { GenPurchaseLineInvoiceFunctionRoutingModule } from './gen-purchase-line-invoice-function-routing.module';

@NgModule({
  declarations: [GenPurchaseLineInvoicePage],
  imports: [CommonModule, SharedModule, GenPurchaseLineInvoiceFunctionRoutingModule, GenPurchaseLineInvoiceComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GenPurchaseLineInvoiceFunctionModule {}
