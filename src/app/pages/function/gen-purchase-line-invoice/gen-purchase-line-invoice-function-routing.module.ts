import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { GenPurchaseLineInvoicePage } from './gen-purchase-line-invoice/gen-purchase-line-invoice.page';

const routes: Routes = [
  {
    path: '',
    component: GenPurchaseLineInvoicePage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GenPurchaseLineInvoiceFunctionRoutingModule {}
