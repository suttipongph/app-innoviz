import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateChequeReferenceFunctionRoutingModule as UpdateChequeReferenceFunctionRoutingModule } from './update-cheque-reference-function-routing.module';
import { SharedModule } from 'shared/shared.module';
import { UpdateChequeReferencePage } from './update-cheque-reference/update-cheque-reference.page';
import { UpdateChequeReferenceComponentModule } from 'components/framework/migration-function/update-cheque-reference/update-cheque-reference.module';

@NgModule({
  declarations: [UpdateChequeReferencePage],
  imports: [CommonModule, SharedModule, UpdateChequeReferenceFunctionRoutingModule, UpdateChequeReferenceComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UpdateChequeReferenceFunctionModule {}
