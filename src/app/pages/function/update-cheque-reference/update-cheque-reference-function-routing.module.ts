import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { UpdateChequeReferencePage } from './update-cheque-reference/update-cheque-reference.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateChequeReferencePage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpdateChequeReferenceFunctionRoutingModule {}
