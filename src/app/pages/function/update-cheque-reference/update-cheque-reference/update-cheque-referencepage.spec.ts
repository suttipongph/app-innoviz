import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UpdateChequeReferencePage } from './update-cheque-reference.page';

describe('PostPage', () => {
  let component: UpdateChequeReferencePage;
  let fixture: ComponentFixture<UpdateChequeReferencePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UpdateChequeReferencePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateChequeReferencePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
