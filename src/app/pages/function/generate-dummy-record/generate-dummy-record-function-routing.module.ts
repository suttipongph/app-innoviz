import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { GenerateDummyRecordPage } from './generate-dummy-record/generate-dummy-record.page';

const routes: Routes = [
  {
    path: '',
    component: GenerateDummyRecordPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GenerateDummyRecordFunctionRoutingModule {}
