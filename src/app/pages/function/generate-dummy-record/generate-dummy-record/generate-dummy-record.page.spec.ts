import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GenerateDummyRecordPage } from './generate-dummy-record.page';

describe('PostPage', () => {
  let component: GenerateDummyRecordPage;
  let fixture: ComponentFixture<GenerateDummyRecordPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GenerateDummyRecordPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateDummyRecordPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
