import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenerateDummyRecordFunctionRoutingModule } from './generate-dummy-record-function-routing.module';
import { SharedModule } from 'shared/shared.module';
import { GenerateDummyRecordPage } from './generate-dummy-record/generate-dummy-record.page';
import { GenerateDummyRecordComponentModule } from 'components/framework/migration-function/generate-dummy-record/generate-dummy-record.module';

@NgModule({
  declarations: [GenerateDummyRecordPage],
  imports: [CommonModule, SharedModule, GenerateDummyRecordFunctionRoutingModule, GenerateDummyRecordComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GenerateDummyRecordFunctionModule {}
