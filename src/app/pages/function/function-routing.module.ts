import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'generatedummyrecord',
    loadChildren: () => import('./generate-dummy-record/generate-dummy-record-function.module').then((m) => m.GenerateDummyRecordFunctionModule)
  },
  
  {
    path: 'updatechequereference',
    loadChildren: () => import('./update-cheque-reference/update-cheque-reference-function.module').then((m) => m.UpdateChequeReferenceFunctionModule)
  },
  {
    path: 'genpurchaselineinvoice',
    loadChildren: () =>
      import('./gen-purchase-line-invoice/gen-purchase-line-invoice-function.module').then((m) => m.GenPurchaseLineInvoiceFunctionModule)
  },
  {
    path: 'genwithdrawallineinvoice',
    loadChildren: () =>
      import('./gen-withdrawal-line-invoice/gen-withdrawal-line-invoice-function.module').then((m) => m.GenWithdrawalLineInvoiceFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FunctionRoutingModule {}
