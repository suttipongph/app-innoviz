import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserSettingsRoutingModule } from './user-settings-routing.module';
import { SharedModule } from 'shared/shared.module';
import { UserSettingsItemPage } from './user-settings-item.page';
import { UserSettingsComponentModule } from 'components/user-settings/user-settings.module';

@NgModule({
  declarations: [UserSettingsItemPage],
  imports: [CommonModule, SharedModule, UserSettingsRoutingModule, UserSettingsComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UserSettingsModule {}
