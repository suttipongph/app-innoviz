import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'user-settings-item.page',
  templateUrl: './user-settings-item.page.html',
  styleUrls: ['./user-settings-item.page.scss']
})
export class UserSettingsItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.USERSETTINGS, servicePath: 'UserSettings' };
  }
}
