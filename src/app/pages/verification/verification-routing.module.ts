import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'verificationtable', loadChildren: () => import('./verification-table/verification-table.module').then((m) => m.VerificationTableModule) },
  {
    path: 'memotrans',
    loadChildren: () =>
      import('./verification-table/relatedinfo/memo/memo/memo-relatedinfo-routing.module').then((m) => m.MemoRelatedinfoRoutingModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VerificationRoutingModule {}
