import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VerificationRoutingModule } from './verification-routing.module';
import { MemoListPage } from './verification-table/relatedinfo/memo/memo-list/memo-list.page';

@NgModule({
  declarations: [],
  imports: [CommonModule, VerificationRoutingModule, MemoListPage]
})
export class VerificationModule {}
