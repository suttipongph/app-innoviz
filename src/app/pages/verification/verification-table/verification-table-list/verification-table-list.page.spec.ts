import { ComponentFixture, TestBed } from '@angular/core/testing';
import { VerificationTableListPage } from './verification-table-list.page';

describe('VerificationTableListPage', () => {
  let component: VerificationTableListPage;
  let fixture: ComponentFixture<VerificationTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VerificationTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
