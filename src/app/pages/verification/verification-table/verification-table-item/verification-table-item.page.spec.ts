import { ComponentFixture, TestBed } from '@angular/core/testing';
import { VerificationTableItemPage } from './verification-table-item.page';

describe('VerificationTableItemPage', () => {
  let component: VerificationTableItemPage;
  let fixture: ComponentFixture<VerificationTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VerificationTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
