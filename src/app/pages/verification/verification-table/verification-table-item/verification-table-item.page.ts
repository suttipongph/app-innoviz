import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'verification-table-item-page',
  templateUrl: './verification-table-item.page.html',
  styleUrls: ['./verification-table-item.page.scss']
})
export class VerificationTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.VERIFICATION_TABLE, servicePath: 'VerificationTable' };
  }
}
