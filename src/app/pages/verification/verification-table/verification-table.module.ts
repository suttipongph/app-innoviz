import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VerificationTableRoutingModule } from './verification-table-routing.module';
import { VerificationTableListPage } from './verification-table-list/verification-table-list.page';
import { VerificationTableItemPage } from './verification-table-item/verification-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { VerificationTableComponentModule } from 'components/verification/verification-table/verification-table.module';
import { MemoTransComponentModule } from 'components/memo-trans/memo-trans/memo-trans.module';

@NgModule({
  declarations: [VerificationTableListPage, VerificationTableItemPage],
  imports: [CommonModule, SharedModule, VerificationTableRoutingModule, VerificationTableComponentModule, MemoTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class VerificationTableModule {}
