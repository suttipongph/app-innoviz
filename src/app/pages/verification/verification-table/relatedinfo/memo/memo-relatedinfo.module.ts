import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MemoRelatedinfoRoutingModule } from './memo-relatedinfo-routing.module';
import { MemoListPage } from './memo-list/memo-list.page';
import { MemoItemPage } from './memo-item/memo-item.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [MemoListPage, MemoItemPage],
  imports: [CommonModule, SharedModule, MemoRelatedinfoRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MemoRelatedinfoModule {}
