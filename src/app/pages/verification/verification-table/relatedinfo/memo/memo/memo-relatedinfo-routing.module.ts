import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MemoListPage } from './memo-list/memo-list.page';
import { MemoItemPage } from './memo-item/memo-item.page';

const routes: Routes = [
  {
    path: '',
    component: MemoListPage
  },
  {
    path: ':id',
    component: MemoItemPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MemoRelatedinfoRoutingModule {}
