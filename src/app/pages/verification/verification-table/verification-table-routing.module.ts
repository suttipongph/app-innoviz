import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { VerificationTableItemPage } from './verification-table-item/verification-table-item.page';
import { VerificationTableListPage } from './verification-table-list/verification-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: VerificationTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: VerificationTableItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VerificationTableRoutingModule {}
