import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MasterInformationRoutingModule } from './master-information-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, MasterInformationRoutingModule]
})
export class MasterInformationModule {}
