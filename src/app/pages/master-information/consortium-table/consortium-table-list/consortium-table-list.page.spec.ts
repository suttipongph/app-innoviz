import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ConsortiumTableListPage } from './consortium-table-list.page';

describe('ConsortiumTableListPage', () => {
  let component: ConsortiumTableListPage;
  let fixture: ComponentFixture<ConsortiumTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConsortiumTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsortiumTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
