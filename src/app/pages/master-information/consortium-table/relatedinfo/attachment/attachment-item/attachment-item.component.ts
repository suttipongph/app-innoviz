import { Component } from '@angular/core';
import { BaseItemComponent } from 'src/app/core/components/base-item/base-item.component';
import { ROUTE_RELATED_GEN } from 'src/app/shared/constants';
import { PathParamModel } from 'src/app/shared/models/systemModel';

@Component({
  selector: 'app-attachment-item',
  templateUrl: './attachment-item.component.html',
  styleUrls: ['./attachment-item.component.scss']
})
export class AttachmentItemComponent extends BaseItemComponent<any> {
  constructor() {
    super();
    this.path = ROUTE_RELATED_GEN.CONSORTIUM_TABLE_ATTACHMENT;
  }
  onRelatedMenu(): void {
    const path: PathParamModel = {};
    this.toRelatedInfo(path);
  }
  toRelatedInfo(param: PathParamModel): void {
    super.toRelatedInfo(param);
  }
  ngOnInit(): void {}
}
