import { Component } from '@angular/core';
import { BaseListComponent } from 'src/app/core/components/base-list/base-list.component';
import { EmptyGuid, ROUTE_RELATED_GEN } from 'src/app/shared/constants';
import { RowIdentity } from 'src/app/shared/models/systemModel';

@Component({
  selector: 'app-attachment-list',
  templateUrl: './attachment-list.component.html',
  styleUrls: ['./attachment-list.component.scss']
})
export class AttachmentListComponent extends BaseListComponent<any> {
  constructor() {
    super();
    this.path = ROUTE_RELATED_GEN.CONSORTIUM_TABLE_ATTACHMENT;
  }
  onDataGridView(): void {
    const row: RowIdentity = { guid: EmptyGuid };
    this.onView(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  ngOnInit(): void {}
}
