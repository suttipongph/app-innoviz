import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AttachmentListComponent } from './attachment-list/attachment-list.component';
import { AttachmentItemComponent } from './attachment-item/attachment-item.component';

const routes: Routes = [
  {
    path: 'attachment',
    component: AttachmentListComponent
  },
  {
    path: 'attachment/:id',
    component: AttachmentItemComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AttachmentRelatedinfoRoutingModule {}
