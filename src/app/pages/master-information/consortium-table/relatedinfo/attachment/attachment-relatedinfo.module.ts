import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AttachmentRelatedinfoRoutingModule } from './attachment-relatedinfo-routing.module';
import { AttachmentListComponent } from './attachment-list/attachment-list.component';
import { AttachmentItemComponent } from './attachment-item/attachment-item.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [AttachmentListComponent, AttachmentItemComponent],
  imports: [CommonModule, SharedModule, AttachmentRelatedinfoRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AttachmentRelatedinfoModule {}
