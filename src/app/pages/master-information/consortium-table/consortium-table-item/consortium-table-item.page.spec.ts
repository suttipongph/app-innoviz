import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ConsortiumTableItemPage } from './consortium-table-item.page';

describe('ConsortiumTableItemPage', () => {
  let component: ConsortiumTableItemPage;
  let fixture: ComponentFixture<ConsortiumTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConsortiumTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsortiumTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
