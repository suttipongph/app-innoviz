import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'consortium-table-item-page',
  templateUrl: './consortium-table-item.page.html',
  styleUrls: ['./consortium-table-item.page.scss']
})
export class ConsortiumTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.CONSORTIUM_TABLE,
      servicePath: 'ConsortiumTable',
      childPaths: [{ pagePath: ROUTE_MASTER_GEN.CONSORTIUM_LINE, servicePath: 'ConsortiumTable' }]
    };
  }
}
