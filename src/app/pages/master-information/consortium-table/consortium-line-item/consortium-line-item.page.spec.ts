import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ConsortiumLineItemPage } from './consortium-line-item.page';

describe('ConsortiumLineItemPage', () => {
  let component: ConsortiumLineItemPage;
  let fixture: ComponentFixture<ConsortiumLineItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConsortiumLineItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsortiumLineItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
