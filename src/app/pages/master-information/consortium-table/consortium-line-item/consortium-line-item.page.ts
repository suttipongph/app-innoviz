import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'consortium-line-item-page',
  templateUrl: './consortium-line-item.page.html',
  styleUrls: ['./consortium-line-item.page.scss']
})
export class ConsortiumLineItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.CONSORTIUM_TABLE, servicePath: 'ConsortiumTable' };
  }
}
