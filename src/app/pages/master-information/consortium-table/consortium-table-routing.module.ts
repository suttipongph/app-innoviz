import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ConsortiumLineItemPage } from './consortium-line-item/consortium-line-item.page';
import { ConsortiumTableItemPage } from './consortium-table-item/consortium-table-item.page';
import { ConsortiumTableListPage } from './consortium-table-list/consortium-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: ConsortiumTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ConsortiumTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/consortiumline-child/:id',
    component: ConsortiumLineItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsortiumTableRoutingModule {}
