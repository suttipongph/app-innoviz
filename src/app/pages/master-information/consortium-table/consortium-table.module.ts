import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsortiumTableRoutingModule } from './consortium-table-routing.module';
import { ConsortiumTableListPage } from './consortium-table-list/consortium-table-list.page';
import { ConsortiumTableItemPage } from './consortium-table-item/consortium-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { ConsortiumTableComponentModule } from 'components/consortium/consortium-table/consortium-table.module';
import { ConsortiumLineItemPage } from './consortium-line-item/consortium-line-item.page';
import { ConsortiumLineComponentModule } from 'components/consortium/consortium-line/consortium-line.module';

@NgModule({
  declarations: [ConsortiumTableListPage, ConsortiumTableItemPage, ConsortiumLineItemPage],
  imports: [CommonModule, SharedModule, ConsortiumTableRoutingModule, ConsortiumTableComponentModule, ConsortiumLineComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ConsortiumTableModule {}
