import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { inquiryCALineOutstandingByCARoutingModule } from './inquiry-ca-line-outstanding-by-ca-routing.module';
import { inquiryCALineOutstandingByCAListPage } from './inquiry-ca-line-outstanding-by-ca-list/inquiry-ca-line-outstanding-by-ca-list.page';
import { InquiryCALineOutstandingByCAComponentModule } from 'components/inquiry/inquiry-ca-line-outstanding-by-ca/inquiry-ca-line-outstanding-by-ca.module';

@NgModule({
  declarations: [inquiryCALineOutstandingByCAListPage],
  imports: [CommonModule, SharedModule, inquiryCALineOutstandingByCARoutingModule, InquiryCALineOutstandingByCAComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InquiryCreditOutstandingByCustModule {}
