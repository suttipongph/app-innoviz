import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { inquiryCALineOutstandingByCAListPage } from './inquiry-ca-line-outstanding-by-ca-list/inquiry-ca-line-outstanding-by-ca-list.page';

const routes: Routes = [
  {
    path: '',
    component: inquiryCALineOutstandingByCAListPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class inquiryCALineOutstandingByCARoutingModule {}
