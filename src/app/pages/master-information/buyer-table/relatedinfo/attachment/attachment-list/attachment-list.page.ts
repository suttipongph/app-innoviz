import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'attachment-list-page',
  templateUrl: './attachment-list.page.html',
  styleUrls: ['./attachment-list.page.scss']
})
export class AttachmentListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'attachmentGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.ATTACHMENT, servicePath: 'BuyerTable/RelatedInfo/Attachment' };
  }
}
