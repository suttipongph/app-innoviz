import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RelatedinfoRoutingModule } from './buyer-table-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, RelatedinfoRoutingModule]
})
export class BuyerTableRelatedinfoModule {}
