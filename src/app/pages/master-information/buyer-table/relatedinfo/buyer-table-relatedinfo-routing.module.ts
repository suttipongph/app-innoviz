import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'buyercreditlimitbyproduct',
    loadChildren: () =>
      import('./buyer-credit-limit-by-product/buyer-table-buyer-credit-limit-by-product-relatedinfo.module').then(
        (m) => m.BuyerTableBuyerCreditLimitByProductRelatedinfoModule
      )
  },
  {
    path: 'memotrans',
    loadChildren: () => import('./memo/buyer-table-memo-trans-relatedinfo.module').then((m) => m.BuyerTableMemoTransRelatedinfoModule)
  },
  {
    path: 'contactpersontrans',
    loadChildren: () =>
      import('./contact-person/buyer-table-contact-person-trans-relatedinfo.module').then((m) => m.BuyerTableContactPersonTransRelatedinfoModule)
  },
  {
    path: 'contacttrans',
    loadChildren: () => import('./contact/buyer-table-contact-trans-relatedinfo.module').then((m) => m.BuyerTableContactTransRelatedinfoModule)
  },
  {
    path: 'authorizedpersontrans',
    loadChildren: () =>
      import('./authorized-person-trans/buyer-table-authorized-person-trans.module').then((m) => m.BuyerTableAuthorizedPersonTransModule)
  },
  {
    path: 'buyeragreementtable',
    loadChildren: () =>
      import('./buyer-agreement/buyer-table-buyer-agreement-table-relatedinfo.module').then((m) => m.BuyerTableBuyerAgreementTableRelatedinfoModule)
  },
  {
    path: 'addresstrans',
    loadChildren: () => import('./address-trans/buyer-table-address-trans.module').then((m) => m.BuyerTableAddressTransModule)
  },
  {
    path: 'documentconditiontrans',
    loadChildren: () => import('./document-condition-info/document-condition-info.module').then((m) => m.DocumentConditionInfoModule)
  },
  {
    path: 'financialstatementtrans',
    loadChildren: () =>
      import('./financial-statement-trans/buyer-table-financial-statement-trans.module').then((m) => m.BuyerTableFinancialStatementTransModule)
  },
  {
    path: 'buyertransaction',
    loadChildren: () => import('./buyer-transaction/buyer-transaction-relatedinfo.module').then((m) => m.BuyerTransactionRelatedinfoModule)
  },
  {
    path: 'attachment',
    loadChildren: () => import('./attachment/buyer-table-attachment.module').then((m) => m.BuyerTableAttachmentModule)
  },
  {
    path: 'inquiryassignmentagreementoutstanding',
    loadChildren: () =>
      import('./inquiry-assignment-outstanding/inquiry-assignment-outstanding.module').then((m) => m.InquiryAssignmentOutstandingModule)
  },
  {
    path: 'inquirybuyercreditofallcustomer',
    loadChildren: () =>
      import('./inquiry-ca-line-outstanding-by-ca/inquiry-ca-line-outstanding-by-ca.module').then((m) => m.InquiryCreditOutstandingByCustModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RelatedinfoRoutingModule {}
