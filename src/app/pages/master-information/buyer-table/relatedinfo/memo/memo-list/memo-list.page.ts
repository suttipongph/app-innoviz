import { Component, OnInit } from '@angular/core';
import { EmptyGuid, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel, RowIdentity } from 'shared/models/systemModel';

@Component({
  selector: 'app-memo-list',
  templateUrl: './memo-list.page.html',
  styleUrls: ['./memo-list.page.scss']
})
export class MemoListPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.MEMO, servicePath: 'BuyerTable/RelatedInfo/MemoTrans' };
  }
}
