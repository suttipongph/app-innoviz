import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BuyerAgreementListPage } from './buyer-agreement-list.page';

describe('BuyerAgreementListComponent', () => {
  let component: BuyerAgreementListPage;
  let fixture: ComponentFixture<BuyerAgreementListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BuyerAgreementListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerAgreementListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
