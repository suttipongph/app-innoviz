import { Component } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-buyer-agreement-list',
  templateUrl: './buyer-agreement-list.page.html',
  styleUrls: ['./buyer-agreement-list.page.scss']
})
export class BuyerAgreementListPage {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.BUYER_AGREEMENT, servicePath: 'BuyerTable/RelatedInfo/BuyerAgreementTable' };
  }
}
