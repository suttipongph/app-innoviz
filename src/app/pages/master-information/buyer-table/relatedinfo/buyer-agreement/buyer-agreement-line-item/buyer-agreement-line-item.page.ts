import { Component } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-buyer-agreement-line-item',
  templateUrl: './buyer-agreement-line-item.page.html',
  styleUrls: ['./buyer-agreement-line-item.page.scss']
})
export class BuyerAgreementLineItemPage {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.BUYER_AGREEMENT_LINE,
      servicePath: 'BuyerTable/RelatedInfo/BuyerAgreementTable'
    };
  }
}
