import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BuyerAgreementLineItemPage } from './buyer-agreement-line-item.page';

describe('BuyerAgreementItemComponent', () => {
  let component: BuyerAgreementLineItemPage;
  let fixture: ComponentFixture<BuyerAgreementLineItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BuyerAgreementLineItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerAgreementLineItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
