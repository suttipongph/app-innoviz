import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BuyerAgreementItemPage } from './buyer-agreement-item.page';

describe('BuyerAgreementItemComponent', () => {
  let component: BuyerAgreementItemPage;
  let fixture: ComponentFixture<BuyerAgreementItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BuyerAgreementItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerAgreementItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
