import { Component } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-buyer-agreement-item',
  templateUrl: './buyer-agreement-item.page.html',
  styleUrls: ['./buyer-agreement-item.page.scss']
})
export class BuyerAgreementItemPage {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.BUYER_AGREEMENT,
      servicePath: 'BuyerTable/RelatedInfo/BuyerAgreementTable',
      childPaths: [{ pagePath: ROUTE_RELATED_GEN.BUYER_AGREEMENT_LINE, servicePath: 'BuyerTable/RelatedInfo/BuyerAgreementTable' }]
    };
  }
}
