import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BuyerAgreementListPage } from './buyer-agreement-list/buyer-agreement-list.page';
import { BuyerAgreementItemPage } from './buyer-agreement-item/buyer-agreement-item.page';
import { BuyerAgreementLineItemPage } from './buyer-agreement-line-item/buyer-agreement-line-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: BuyerAgreementListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BuyerAgreementItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/buyeragreementline-child/:id',
    component: BuyerAgreementLineItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyerTableBuyerAgreementTableRelatedinfoRoutingModule {}
