import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuyerTableBuyerAgreementTableRelatedinfoRoutingModule } from './buyer-table-buyer-agreement-table-relatedinfo-routing.module';
import { BuyerAgreementListPage } from './buyer-agreement-list/buyer-agreement-list.page';
import { BuyerAgreementItemPage } from './buyer-agreement-item/buyer-agreement-item.page';
import { SharedModule } from 'shared/shared.module';
import { BuyerAgreementTableComponentModule } from 'components/buyer-agreement-table/buyer-agreement-table/buyer-agreement-table.module';
import { BuyerAgreementLineComponentModule } from 'components/buyer-agreement-table/buyer-agreement-line/buyer-agreement-line.module';
import { BuyerAgreementLineItemPage } from './buyer-agreement-line-item/buyer-agreement-line-item.page';

@NgModule({
  declarations: [BuyerAgreementListPage, BuyerAgreementItemPage, BuyerAgreementLineItemPage],
  imports: [
    CommonModule,
    SharedModule,
    BuyerTableBuyerAgreementTableRelatedinfoRoutingModule,
    BuyerAgreementTableComponentModule,
    BuyerAgreementLineComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BuyerTableBuyerAgreementTableRelatedinfoModule {}
