import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddressTransListPage } from './address-trans-list/address-trans-list.page';
import { AddressTransItemPage } from './address-trans-item/address-trans-item.page';
import { SharedModule } from 'shared/shared.module';
import { AddressTransComponentModule } from 'components/address-trans/address-trans/address-trans.module';
import { BuyerTableAddressTransRoutingModule } from './buyer-table-address-trans-routing.module';

@NgModule({
  declarations: [AddressTransListPage, AddressTransItemPage],
  imports: [CommonModule, SharedModule, BuyerTableAddressTransRoutingModule, AddressTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BuyerTableAddressTransModule {}
