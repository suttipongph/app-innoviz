import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'address-trans-item.page',
  templateUrl: './address-trans-item.page.html',
  styleUrls: ['./address-trans-item.page.scss']
})
export class AddressTransItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.ADDRESS_TRANS, servicePath: 'BuyerTable/RelatedInfo/AddressTrans' };
  }
}
