import { Component } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-buyer-credit-limit-by-product-item',
  templateUrl: './buyer-credit-limit-by-product-item.page.html',
  styleUrls: ['./buyer-credit-limit-by-product-item.page.scss']
})
export class BuyerCreditLimitByProductItemPage {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.BUYER_CREDIT_LIMIT_BY_PRODUCT, servicePath: 'BuyerTable/RelatedInfo/buyercreditlimitbyproduct' };
  }
}
