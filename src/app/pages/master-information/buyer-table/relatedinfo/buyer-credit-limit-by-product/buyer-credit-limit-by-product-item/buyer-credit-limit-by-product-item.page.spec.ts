import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BuyerCreditLimitByProductItemPage } from './buyer-credit-limit-by-product-item.page';

describe('BuyerCreditLimitByProductItemComponent', () => {
  let component: BuyerCreditLimitByProductItemPage;
  let fixture: ComponentFixture<BuyerCreditLimitByProductItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BuyerCreditLimitByProductItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerCreditLimitByProductItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
