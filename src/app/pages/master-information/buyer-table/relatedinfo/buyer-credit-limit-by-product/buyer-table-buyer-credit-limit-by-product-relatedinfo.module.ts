import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuyerTableBuyerCreditLimitByProductRelatedinfoRoutingModule } from './buyer-table-buyer-credit-limit-by-product-relatedinfo-routing.module';
import { BuyerCreditLimitByProductListPage } from './buyer-credit-limit-by-product-list/buyer-credit-limit-by-product-list.page';
import { BuyerCreditLimitByProductItemPage } from './buyer-credit-limit-by-product-item/buyer-credit-limit-by-product-item.page';
import { SharedModule } from 'shared/shared.module';
import { BuyerCreditLimitByProductComponentModule } from 'components/buyer-credit-limit-by-product/buyer-credit-limit-by-product.module';

@NgModule({
  declarations: [BuyerCreditLimitByProductListPage, BuyerCreditLimitByProductItemPage],
  imports: [CommonModule, SharedModule, BuyerTableBuyerCreditLimitByProductRelatedinfoRoutingModule, BuyerCreditLimitByProductComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BuyerTableBuyerCreditLimitByProductRelatedinfoModule {}
