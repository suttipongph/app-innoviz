import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BuyerCreditLimitByProductListPage } from './buyer-credit-limit-by-product-list/buyer-credit-limit-by-product-list.page';
import { BuyerCreditLimitByProductItemPage } from './buyer-credit-limit-by-product-item/buyer-credit-limit-by-product-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: BuyerCreditLimitByProductListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BuyerCreditLimitByProductItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyerTableBuyerCreditLimitByProductRelatedinfoRoutingModule {}
