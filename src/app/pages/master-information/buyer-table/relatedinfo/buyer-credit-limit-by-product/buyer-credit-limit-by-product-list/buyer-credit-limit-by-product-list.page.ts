import { Component } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-buyer-credit-limit-by-product-list',
  templateUrl: './buyer-credit-limit-by-product-list.page.html',
  styleUrls: ['./buyer-credit-limit-by-product-list.page.scss']
})
export class BuyerCreditLimitByProductListPage {
  option: OptionModel;
  pageInfo: PageInformationModel;
  parentId: string;
  constructor(public uiService: UIControllerService) {
    this.parentId = uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'buyerCreditLimitByProductGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'buyerTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.BUYER_CREDIT_LIMIT_BY_PRODUCT, servicePath: 'BuyerTable/RelatedInfo/buyercreditlimitbyproduct' };
  }
}
