import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BuyerCreditLimitByProductListPage } from './buyer-credit-limit-by-product-list.page';

describe('BuyerCreditLimitByProductListComponent', () => {
  let component: BuyerCreditLimitByProductListPage;
  let fixture: ComponentFixture<BuyerCreditLimitByProductListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BuyerCreditLimitByProductListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerCreditLimitByProductListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
