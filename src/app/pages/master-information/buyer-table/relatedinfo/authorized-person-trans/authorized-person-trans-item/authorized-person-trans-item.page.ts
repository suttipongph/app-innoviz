import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel, PathParamModel } from 'shared/models/systemModel';

@Component({
  selector: 'authorized-person-trans-item.page',
  templateUrl: './authorized-person-trans-item.page.html',
  styleUrls: ['./authorized-person-trans-item.page.scss']
})
export class AuthorizedPersonTransItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.AUTHORIZED_PERSON_TRANS, servicePath: 'BuyerTable/RelatedInfo/AuthorizedPersonTrans' };
  }
}
