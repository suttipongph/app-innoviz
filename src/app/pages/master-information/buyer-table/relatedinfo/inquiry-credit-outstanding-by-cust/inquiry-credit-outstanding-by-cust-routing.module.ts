import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { inquiryCreditOutstandingByCustListPage } from './inquiry-credit-outstanding-by-cust-list/inquiry-credit-outstanding-by-cust-list.page';

const routes: Routes = [
  {
    path: '',
    component: inquiryCreditOutstandingByCustListPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class inquiryCreditOutstandingByCustRoutingModule {}
