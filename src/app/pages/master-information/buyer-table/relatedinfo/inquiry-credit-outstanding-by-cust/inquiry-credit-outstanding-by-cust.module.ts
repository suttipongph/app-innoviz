import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { inquiryCreditOutstandingByCustRoutingModule } from './inquiry-credit-outstanding-by-cust-routing.module';
import { inquiryCreditOutstandingByCustListPage } from './inquiry-credit-outstanding-by-cust-list/inquiry-credit-outstanding-by-cust-list.page';
import { InquiryCreditOutstandingByCustComponentModule } from 'components/inquiry/inquiry-credit-outstanding-by-cust/inquiry-credit-outstanding-by-cust.module';

@NgModule({
  declarations: [inquiryCreditOutstandingByCustListPage],
  imports: [CommonModule, SharedModule, inquiryCreditOutstandingByCustRoutingModule, InquiryCreditOutstandingByCustComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InquiryCreditOutstandingByCustModule {}
