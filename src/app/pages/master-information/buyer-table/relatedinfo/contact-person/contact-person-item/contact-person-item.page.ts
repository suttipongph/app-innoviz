import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel, PathParamModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-contact-person-item',
  templateUrl: './contact-person-item.page.html',
  styleUrls: ['./contact-person-item.page.scss']
})
export class ContactPersonItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.CONTACT_PERSON, servicePath: 'BuyerTable/RelatedInfo/ContactPersonTrans' };
  }
}
