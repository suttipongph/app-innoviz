import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ContactPersonItemComponent } from './contact-person-item.component';

describe('ContactPersonItemComponent', () => {
  let component: ContactPersonItemComponent;
  let fixture: ComponentFixture<ContactPersonItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ContactPersonItemComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactPersonItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
