import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuyerTableContactPersonTransRelatedinfoRoutingModule } from './buyer-table-contact-person-trans-relatedinfo-routing.module';
import { ContactPersonListPage } from './contact-person-list/contact-person-list.page';
import { ContactPersonItemPage } from './contact-person-item/contact-person-item.page';
import { SharedModule } from 'shared/shared.module';
import { ContactPersonTransComponentModule } from 'components/contact-person-trans/contact-person-trans/contact-person-trans.module';

@NgModule({
  declarations: [ContactPersonListPage, ContactPersonItemPage],
  imports: [CommonModule, SharedModule, BuyerTableContactPersonTransRelatedinfoRoutingModule, ContactPersonTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BuyerTableContactPersonTransRelatedinfoModule {}
