import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactPersonListPage } from './contact-person-list/contact-person-list.page';
import { ContactPersonItemPage } from './contact-person-item/contact-person-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: ContactPersonListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ContactPersonItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyerTableContactPersonTransRelatedinfoRoutingModule {}
