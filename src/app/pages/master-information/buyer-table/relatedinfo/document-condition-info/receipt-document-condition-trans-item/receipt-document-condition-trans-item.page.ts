import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'receipt-document-condition-trans-item.page',
  templateUrl: './receipt-document-condition-trans-item.page.html',
  styleUrls: ['./receipt-document-condition-trans-item.page.scss']
})
export class ReceiptDocumentConditionTransItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.RECEIPT_DOCUMENT_CONDITION_TRANS_RECEIPT,
      servicePath: 'BuyerTable/RelatedInfo/DocumentConditionTrans'
    };
  }
}
