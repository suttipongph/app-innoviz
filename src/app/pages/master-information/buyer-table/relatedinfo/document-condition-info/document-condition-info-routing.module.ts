import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { DocumentConditionInfoItemPage } from './document-condition-info-item/document-condition-info-item.page';
import { BillingDocumentConditionTransItemPage } from './billing-document-condition-trans-item/billing-document-condition-trans-item.page';
import { ReceiptDocumentConditionTransItemPage } from './receipt-document-condition-trans-item/receipt-document-condition-trans-item.page';

const routes: Routes = [
  {
    path: '',
    component: DocumentConditionInfoItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'billingdocumentconditiontrans-child/:id',
    component: BillingDocumentConditionTransItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'receiptdocumentconditiontrans-child/:id',
    component: ReceiptDocumentConditionTransItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'function',
    loadChildren: () => import('./functions/document-condition-info-function.module').then((m) => m.DocumentConditionTemplateFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentConditionInfoRoutingModule {}
