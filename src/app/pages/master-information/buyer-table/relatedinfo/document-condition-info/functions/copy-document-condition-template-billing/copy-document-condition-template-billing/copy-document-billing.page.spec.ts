import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CopyDocumentConditionTemplateBillingComponentPage } from './copy-document-billing.page';

describe('CopyDocumentConditionTemplateBillingComponentPage', () => {
  let component: CopyDocumentConditionTemplateBillingComponentPage;
  let fixture: ComponentFixture<CopyDocumentConditionTemplateBillingComponentPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CopyDocumentConditionTemplateBillingComponentPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyDocumentConditionTemplateBillingComponentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
