import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'primeng/api';
import { DocumentConditionInfoFunctionRoutingModule } from './document-condition-info-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, DocumentConditionInfoFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DocumentConditionTemplateFunctionModule {}
