import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CopyDocumentConditionTemplateReceiptPage } from './copy-document-condition-template-receipt/copy-document-receipt.page';

const routes: Routes = [
  {
    path: '',
    component: CopyDocumentConditionTemplateReceiptPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CopyDocumentConditionTemplateReceiptFunctionRoutingModule {}
