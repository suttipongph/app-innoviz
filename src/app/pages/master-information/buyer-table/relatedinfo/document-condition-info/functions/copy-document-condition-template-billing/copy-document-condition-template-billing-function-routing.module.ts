import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CopyDocumentConditionTemplateBillingPage } from './copy-document-condition-template-billing/copy-document-billing.page';

const routes: Routes = [
  {
    path: '',
    component: CopyDocumentConditionTemplateBillingPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CopyDocumentConditionTemplateBillingFunctionRoutingModule {}
