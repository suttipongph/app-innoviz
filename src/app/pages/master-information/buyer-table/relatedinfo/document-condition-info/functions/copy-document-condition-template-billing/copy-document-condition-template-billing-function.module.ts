import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CopyDocumentConditionTemplateBillingFunctionRoutingModule } from './copy-document-condition-template-billing-function-routing.module';
import { CopyDocumentConditionTemplateBillingPage } from './copy-document-condition-template-billing/copy-document-billing.page';
import { SharedModule } from 'shared/shared.module';
import { CopyDocumentConditionTemplateComponentModule } from 'components/document-condition-trans/copy-document-condition-template/copy-document-condition-template.module';

@NgModule({
  declarations: [CopyDocumentConditionTemplateBillingPage],
  imports: [CommonModule, SharedModule, CopyDocumentConditionTemplateBillingFunctionRoutingModule, CopyDocumentConditionTemplateComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CopyDocumentConditionTemplateBillingFunctionModule {}
