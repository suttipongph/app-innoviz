import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'copydocumentconditiontemplatebilling',
    loadChildren: () =>
      import('./copy-document-condition-template-billing/copy-document-condition-template-billing-function.module').then(
        (m) => m.CopyDocumentConditionTemplateBillingFunctionModule
      )
  },
  {
    path: 'copydocumentconditiontemplatereceipt',
    loadChildren: () =>
      import('./copy-document-condition-template-receipt/copy-document-condition-template-receipt-function.module').then(
        (m) => m.CopyDocumentConditionTemplateReceiptFunctionModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentConditionInfoFunctionRoutingModule {}
