import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CopyDocumentConditionTemplateReceiptFunctionRoutingModule } from './copy-document-condition-template-receipt-function-routing.module';
import { CopyDocumentConditionTemplateReceiptPage } from './copy-document-condition-template-receipt/copy-document-receipt.page';
import { SharedModule } from 'shared/shared.module';
import { CopyDocumentConditionTemplateComponentModule } from 'components/document-condition-trans/copy-document-condition-template/copy-document-condition-template.module';

@NgModule({
  declarations: [CopyDocumentConditionTemplateReceiptPage],
  imports: [CommonModule, SharedModule, CopyDocumentConditionTemplateReceiptFunctionRoutingModule, CopyDocumentConditionTemplateComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CopyDocumentConditionTemplateReceiptFunctionModule {}
