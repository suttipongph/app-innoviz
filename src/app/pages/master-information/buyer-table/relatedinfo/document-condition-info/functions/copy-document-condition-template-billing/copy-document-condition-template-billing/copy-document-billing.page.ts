import { Component, OnInit } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';

@Component({
  selector: 'copy-document-billing-page',
  templateUrl: './copy-document-billing.page.html',
  styleUrls: ['./copy-document-billing.page.scss']
})
export class CopyDocumentConditionTemplateBillingPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.COPY_DOCUMENT_CONDITION_TEMPLATE_BILLING,
      servicePath: 'BuyerTable/RelatedInfo/DocumentConditionTrans/Function/CopyDocumentConditionTemplateBilling'
    };
  }
}
