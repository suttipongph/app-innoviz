import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CopyDocumentConditionTemplateComponentPage } from './copy-document-condition-template.page';

describe('CopyDocumentConditionTemplateComponentPage', () => {
  let component: CopyDocumentConditionTemplateComponentPage;
  let fixture: ComponentFixture<CopyDocumentConditionTemplateComponentPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CopyDocumentConditionTemplateComponentPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyDocumentConditionTemplateComponentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
