import { Component, OnInit } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';

@Component({
  selector: 'copy-document-receipt-page',
  templateUrl: './copy-document-receipt.page.html',
  styleUrls: ['./copy-document-receipt.page.scss']
})
export class CopyDocumentConditionTemplateReceiptPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.COPY_DOCUMENT_CONDITION_TEMPLATE_RECEIPT,
      servicePath: 'BuyerTable/RelatedInfo/DocumentConditionTrans/Function/CopyDocumentConditionTemplateReceipt'
    };
  }
}
