import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentConditionInfoRoutingModule } from './document-condition-info-routing.module';
import { SharedModule } from 'shared/shared.module';
import { DocumentConditionTransComponentModule } from 'components/document-condition-trans/document-condition-trans/document-condition-trans.module';
import { DocumentConditionInfoItemPage } from './document-condition-info-item/document-condition-info-item.page';
import { DocumentConditionInfoComponentModule } from 'components/document-condition-trans/document-condition-info/document-condition-info.module';
import { BillingDocumentConditionTransItemPage } from './billing-document-condition-trans-item/billing-document-condition-trans-item.page';
import { ReceiptDocumentConditionTransItemPage } from './receipt-document-condition-trans-item/receipt-document-condition-trans-item.page';

@NgModule({
  declarations: [DocumentConditionInfoItemPage, BillingDocumentConditionTransItemPage, ReceiptDocumentConditionTransItemPage],
  imports: [
    CommonModule,
    SharedModule,
    DocumentConditionInfoRoutingModule,
    DocumentConditionTransComponentModule,
    DocumentConditionInfoComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DocumentConditionInfoModule {}
