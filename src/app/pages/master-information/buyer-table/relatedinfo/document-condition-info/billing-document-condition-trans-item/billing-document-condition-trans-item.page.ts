import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'billing-document-condition-trans-item.page',
  templateUrl: './billing-document-condition-trans-item.page.html',
  styleUrls: ['./billing-document-condition-trans-item.page.scss']
})
export class BillingDocumentConditionTransItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.BILLING_DOCUMENT_CONDITION_TRANS_BILLING,
      servicePath: 'BuyerTable/RelatedInfo/DocumentConditionTrans'
    };
  }
}
