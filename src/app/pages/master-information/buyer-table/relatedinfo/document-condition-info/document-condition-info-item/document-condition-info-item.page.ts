import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'document-condition-info-item.page',
  templateUrl: './document-condition-info-item.page.html',
  styleUrls: ['./document-condition-info-item.page.scss']
})
export class DocumentConditionInfoItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.BUYERTABLE_DOCUMENT_CONDITION_INFO,
      servicePath: 'BuyerTable/RelatedInfo/DocumentConditionTrans',
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.BILLING_DOCUMENT_CONDITION_TRANS_BILLING,
          servicePath: 'BuyerTable/RelatedInfo/DocumentConditionTrans'
        },
        {
          pagePath: ROUTE_RELATED_GEN.RECEIPT_DOCUMENT_CONDITION_TRANS_RECEIPT,
          servicePath: 'BuyerTable/RelatedInfo/DocumentConditionTrans'
        }
      ]
    };
  }
}
