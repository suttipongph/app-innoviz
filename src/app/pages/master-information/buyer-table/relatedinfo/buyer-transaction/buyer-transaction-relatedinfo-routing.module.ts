import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BuyerTransactionListPage } from './buyer-transaction-list/buyer-transaction-list.page';
import { BuyerTransactionItemPage } from './buyer-transaction-item/buyer-transaction-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: BuyerTransactionListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BuyerTransactionItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyerTransactionRelatedinfoRoutingModule {}
