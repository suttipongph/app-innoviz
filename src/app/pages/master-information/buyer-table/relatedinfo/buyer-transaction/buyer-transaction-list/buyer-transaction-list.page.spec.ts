import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BuyerTransactionListPage } from './buyer-transaction-list.page';

describe('BuyerTransactionListPage', () => {
  let component: BuyerTransactionListPage;
  let fixture: ComponentFixture<BuyerTransactionListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BuyerTransactionListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerTransactionListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
