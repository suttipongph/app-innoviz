import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuyerTransactionRelatedinfoRoutingModule } from './buyer-transaction-relatedinfo-routing.module';
import { BuyerTransactionListPage } from './buyer-transaction-list/buyer-transaction-list.page';
import { BuyerTransactionItemPage } from './buyer-transaction-item/buyer-transaction-item.page';
import { SharedModule } from 'shared/shared.module';
import { CustTransComponentModule } from 'components/cust-trans/cust-trans.module';
import { CustomerTransactionRelatedinfoRoutingModule } from 'pages/master-information/customer-table/relatedinfo/customer-transaction/customer-transaction-relatedinfo-routing.module';

@NgModule({
  declarations: [BuyerTransactionListPage, BuyerTransactionItemPage],
  imports: [CommonModule, SharedModule, BuyerTransactionRelatedinfoRoutingModule, CustTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BuyerTransactionRelatedinfoModule {}
