import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BuyerTransactionItemPage } from './buyer-transaction-item.page';

describe('BuyerTransactionItemPage', () => {
  let component: BuyerTransactionItemPage;
  let fixture: ComponentFixture<BuyerTransactionItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BuyerTransactionItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerTransactionItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
