import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'buyer-transaction-item.page',
  templateUrl: './buyer-transaction-item.page.html',
  styleUrls: ['./buyer-transaction-item.page.scss']
})
export class BuyerTransactionItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.BUYER_TABLE, servicePath: 'BuyerTable/RelatedInfo/BuyerTransaction' };
  }
}
