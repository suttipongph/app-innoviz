import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'financial-statement-trans-item.page',
  templateUrl: './financial-statement-trans-item.page.html',
  styleUrls: ['./financial-statement-trans-item.page.scss']
})
export class FinancialStatementTransItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.FINANCIAL_STATEMENT_TRANS, servicePath: 'BuyerTable/RelatedInfo/FinancialStatementTrans' };
  }
}
