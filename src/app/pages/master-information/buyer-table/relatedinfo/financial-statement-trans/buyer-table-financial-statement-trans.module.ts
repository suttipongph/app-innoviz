import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FinancialStatementTransListPage } from './financial-statement-trans-list/financial-statement-trans-list.page';
import { FinancialStatementTransItemPage } from './financial-statement-trans-item/financial-statement-trans-item.page';
import { SharedModule } from 'shared/shared.module';
import { FinancialStatementTransComponentModule } from 'components/financial-statement-trans/financial-statement-trans/financial-statement-trans.module';
import { BuyerTableFinancialStatementTransRoutingModule } from './buyer-table-financial-statement-trans-routing.module';

@NgModule({
  declarations: [FinancialStatementTransListPage, FinancialStatementTransItemPage],
  imports: [CommonModule, SharedModule, BuyerTableFinancialStatementTransRoutingModule, FinancialStatementTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BuyerTableFinancialStatementTransModule {}
