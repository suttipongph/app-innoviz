import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FinancialStatementTransListPage } from './financial-statement-trans-list/financial-statement-trans-list.page';
import { FinancialStatementTransItemPage } from './financial-statement-trans-item/financial-statement-trans-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: FinancialStatementTransListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: FinancialStatementTransItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'function',
    loadChildren: () => import('./functions/buyer-table-function.module').then((m) => m.BuyerTableFinancialRelatedinfoFunctionModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyerTableFinancialStatementTransRoutingModule {}
