import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { ImportFinancialStatementTransPage } from './import-financial-statement/import-financial-statement-trans.page';
import { ImportFinancialStatementComponentModule } from 'components/financial-statement-trans/import-financial-statement/import-financial-statement.module';
import { BuyerTableFinancialRelatedinfoImportFinancialFunctionRoutingModule } from './import-financial-statement-trans-function-routing.module';

@NgModule({
  declarations: [ImportFinancialStatementTransPage],
  imports: [
    CommonModule,
    SharedModule,
    BuyerTableFinancialRelatedinfoImportFinancialFunctionRoutingModule,
    ImportFinancialStatementComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BuyerTableFinancialRelatedinfoImportFinancialFunctionModule {}
