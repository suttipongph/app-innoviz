import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'primeng/api';
import { BuyerTableFinancialRelatedinfoFunctionRoutingModule } from './buyer-table-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, BuyerTableFinancialRelatedinfoFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BuyerTableFinancialRelatedinfoFunctionModule {}
