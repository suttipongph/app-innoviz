import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'importfinancialstatementtrans',
    loadChildren: () =>
      import('./import-financial-statement/import-financial-statement-trans-function.module').then(
        (m) => m.BuyerTableFinancialRelatedinfoImportFinancialFunctionModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyerTableFinancialRelatedinfoFunctionRoutingModule {}
