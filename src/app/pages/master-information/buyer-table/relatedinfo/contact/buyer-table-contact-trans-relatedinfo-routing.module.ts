import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactListPage } from './contact-list/contact-list.page';
import { ContactItemPage } from './contact-item/contact-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: ContactListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ContactItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyerTableContactTransRelatedinfoRoutingModule {}
