import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { BuyerTableItemPage } from './buyer-table-item/buyer-table-item.page';
import { BuyerTableListPage } from './buyer-table-list/buyer-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: BuyerTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BuyerTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/buyer-table-relatedinfo.module').then((m) => m.BuyerTableRelatedinfoModule)
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/buyer-table-function.module').then((m) => m.BuyerTableFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyerTableRoutingModule {}
