import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BuyerTableListPage } from './buyer-table-list.page';

describe('BuyerTableListPage', () => {
  let component: BuyerTableListPage;
  let fixture: ComponentFixture<BuyerTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BuyerTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
