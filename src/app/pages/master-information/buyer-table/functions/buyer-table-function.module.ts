import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'primeng/api';
import { BuyerTableFunctionRoutingModule } from './buyer-table-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, BuyerTableFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BuyerTableFunctionModule {}
