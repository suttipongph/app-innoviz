import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { UpdateBuyerTableBlacklistAndWatchlistStatusPage } from './update-buyer-table-blacklist-and-watchlist-status/update-buyer-table-blacklist-and-watchlist-status.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateBuyerTableBlacklistAndWatchlistStatusPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpdateBuyerTableBlacklistAndWatchlistStatusFunctionRoutingModule {}
