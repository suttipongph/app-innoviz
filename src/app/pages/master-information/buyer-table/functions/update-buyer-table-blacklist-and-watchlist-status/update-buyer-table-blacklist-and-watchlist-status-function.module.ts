import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateBuyerTableBlacklistAndWatchlistStatusFunctionRoutingModule } from './update-buyer-table-blacklist-and-watchlist-status-function-routing.module';
import { UpdateBuyerTableBlacklistAndWatchlistStatusPage } from './update-buyer-table-blacklist-and-watchlist-status/update-buyer-table-blacklist-and-watchlist-status.page';
import { SharedModule } from 'shared/shared.module';
import { UpdateBuyerTableBlacklistStatusComponentModule } from 'components/buyer-table/update-buyer-table-blacklist-status/update-buyer-table-blacklist-status.module';

@NgModule({
  declarations: [UpdateBuyerTableBlacklistAndWatchlistStatusPage],
  imports: [
    CommonModule,
    SharedModule,
    UpdateBuyerTableBlacklistAndWatchlistStatusFunctionRoutingModule,
    UpdateBuyerTableBlacklistStatusComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UpdateBuyerTableBlacklistAndWatchlistStatusFunctionModule {}
