import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UpdateBuyerTableBlacklistAndWatchlistStatusPage } from './update-buyer-table-blacklist-and-watchlist-status.page';

describe('UpdateBuyerTableBlacklistAndWatchlistStatusPage', () => {
  let component: UpdateBuyerTableBlacklistAndWatchlistStatusPage;
  let fixture: ComponentFixture<UpdateBuyerTableBlacklistAndWatchlistStatusPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UpdateBuyerTableBlacklistAndWatchlistStatusPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateBuyerTableBlacklistAndWatchlistStatusPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
