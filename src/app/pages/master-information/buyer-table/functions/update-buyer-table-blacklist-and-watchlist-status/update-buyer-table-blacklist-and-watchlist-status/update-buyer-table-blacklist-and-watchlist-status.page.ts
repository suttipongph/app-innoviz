import { Component, OnInit } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';

@Component({
  selector: 'update-buyer-table-blacklist-and-watchlist-status',
  templateUrl: './update-buyer-table-blacklist-and-watchlist-status.page.html',
  styleUrls: ['./update-buyer-table-blacklist-and-watchlist-status.page.scss']
})
export class UpdateBuyerTableBlacklistAndWatchlistStatusPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.BUYERTABLE_UPDATE_BLACKLIST_AND_WATCHLIST_STATUS,
      servicePath: 'BuyerTable/Function/UpdateBuyerTableBlacklistAndWatchlistStatus'
    };
  }
}
