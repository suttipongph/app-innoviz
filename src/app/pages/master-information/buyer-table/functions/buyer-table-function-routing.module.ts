import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'updatebuyertableblacklistandwatchliststatus',
    loadChildren: () =>
      import('./update-buyer-table-blacklist-and-watchlist-status/update-buyer-table-blacklist-and-watchlist-status-function.module').then(
        (m) => m.UpdateBuyerTableBlacklistAndWatchlistStatusFunctionModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyerTableFunctionRoutingModule {}
