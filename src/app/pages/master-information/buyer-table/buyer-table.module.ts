import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuyerTableRoutingModule } from './buyer-table-routing.module';
import { BuyerTableListPage } from './buyer-table-list/buyer-table-list.page';
import { BuyerTableItemPage } from './buyer-table-item/buyer-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { ComponentBuyerTableModule } from 'components/buyer-table/buyer-table/buyer-table.module';

@NgModule({
  declarations: [BuyerTableListPage, BuyerTableItemPage],
  imports: [CommonModule, SharedModule, BuyerTableRoutingModule, ComponentBuyerTableModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BuyerTableModule {}
