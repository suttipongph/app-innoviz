import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BuyerTableItemPage } from './buyer-table-item.page';

describe('BuyerTableItemPage', () => {
  let component: BuyerTableItemPage;
  let fixture: ComponentFixture<BuyerTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BuyerTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
