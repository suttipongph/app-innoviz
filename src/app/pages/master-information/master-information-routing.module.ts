import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'customertable',
    loadChildren: () => import('./customer-table/customer-table.module').then((m) => m.CustomerTableModule)
  },
  {
    path: 'buyertable',
    loadChildren: () => import('./buyer-table/buyer-table.module').then((m) => m.BuyerTableModule)
  },
  { path: 'buyertable', loadChildren: () => import('./buyer-table/buyer-table.module').then((m) => m.BuyerTableModule) },
  {
    path: 'relatedpersontable',
    loadChildren: () => import('./related-person-table/related-person-table.module').then((m) => m.RelatedPersonTableModule)
  },
  {
    path: 'vendortable',
    loadChildren: () => import('./vendor-table/vendor-table.module').then((m) => m.VendorTableModule)
  },
  {
    path: 'consortiumtable',
    loadChildren: () => import('./consortium-table/consortium-table.module').then((m) => m.ConsortiumTableModule)
  },
  {
    path: 'messengertable',
    loadChildren: () => import('./messenger-table/messenger-table.module').then((m) => m.MessengerTableModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MasterInformationRoutingModule {}
