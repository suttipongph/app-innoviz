import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'contacttrans',
    loadChildren: () => import('./contact/vendor-table-contact-relatedinfo.module').then((m) => m.VendorTableContactRelatedinfoModule)
  },
  {
    path: 'vendbank',
    loadChildren: () => import('./vend-bank/vendor-table-vend-bank-relatedinfo.module').then((m) => m.VendorTableVendBankRelatedinfoModule)
  },
  {
    path: 'addresstrans',
    loadChildren: () => import('./address-trans/address-trans.module').then((m) => m.AddressTransModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorTableRelatedinfoRoutingModule {}
