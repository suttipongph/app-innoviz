import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddressTransListPage } from './address-trans-list/address-trans-list.page';
import { AddressTransItemPage } from './address-trans-item/address-trans-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: AddressTransListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: AddressTransItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddressTransRoutingModule {}
