import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorTableRelatedinfoRoutingModule } from './vendor-table-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, VendorTableRelatedinfoRoutingModule]
})
export class VendorTableRelatedinfoModule {}
