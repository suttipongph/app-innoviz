import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorTableVendBankRelatedinfoRoutingModule } from './vendor-table-vend-bank-relatedinfo-routing.module';
import { VendBankListPage } from './vend-bank-list/vend-bank-list.page';
import { VendBankItemPage } from './vend-bank-item/vend-bank-item.page';
import { SharedModule } from 'shared/shared.module';
import { VendBankComponentModule } from 'components/vend-bank/vend-bank.module';

@NgModule({
  declarations: [VendBankListPage, VendBankItemPage],
  imports: [CommonModule, SharedModule, VendorTableVendBankRelatedinfoRoutingModule, VendBankComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class VendorTableVendBankRelatedinfoModule {}
