import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VendBankListPage } from './vend-bank-list/vend-bank-list.page';
import { VendBankItemPage } from './vend-bank-item/vend-bank-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: VendBankListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: VendBankItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorTableVendBankRelatedinfoRoutingModule {}
