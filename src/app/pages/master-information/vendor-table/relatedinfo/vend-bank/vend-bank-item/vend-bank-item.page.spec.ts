import { ComponentFixture, TestBed } from '@angular/core/testing';
import { VendBankItemPage } from './vend-bank-item.page';

describe('VendBankItemPage', () => {
  let component: VendBankItemPage;
  let fixture: ComponentFixture<VendBankItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VendBankItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VendBankItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
