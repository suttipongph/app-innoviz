import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'vend-bank-item-page',
  templateUrl: './vend-bank-item.page.html',
  styleUrls: ['./vend-bank-item.page.scss']
})
export class VendBankItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.VEND_BANK, servicePath: 'VendorTable/RelatedInfo/VendBank' };
  }
}
