import { ComponentFixture, TestBed } from '@angular/core/testing';
import { VendBankListPage } from './vend-bank-list.page';

describe('VendBankListPage', () => {
  let component: VendBankListPage;
  let fixture: ComponentFixture<VendBankListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VendBankListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VendBankListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
