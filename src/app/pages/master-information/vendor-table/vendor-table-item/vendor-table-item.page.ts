import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'vendor-table-item.page',
  templateUrl: './vendor-table-item.page.html',
  styleUrls: ['./vendor-table-item.page.scss']
})
export class VendorTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.VENDOR_TABLE, servicePath: 'VendorTable' };
  }
}
