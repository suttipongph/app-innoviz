import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VendorTableListPage } from './vendor-table-list/vendor-table-list.page';
import { VendorTableItemPage } from './vendor-table-item/vendor-table-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: VendorTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: VendorTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/vendor-table-relatedinfo.module').then((m) => m.VendorTableRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorTableRoutingModule {}
