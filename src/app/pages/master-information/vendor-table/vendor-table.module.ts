import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorTableRoutingModule } from './vendor-table-routing.module';
import { VendorTableListPage } from './vendor-table-list/vendor-table-list.page';
import { VendorTableItemPage } from './vendor-table-item/vendor-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { VendorTableComponentModule } from 'components/vendor-table/vendor-table.module';

@NgModule({
  declarations: [VendorTableListPage, VendorTableItemPage],
  imports: [CommonModule, SharedModule, VendorTableRoutingModule, VendorTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class VendorTableModule {}
