import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { MessengerTableItemPage } from './messenger-table-item/messenger-table-item.page';
import { MessengerTableListPage } from './messenger-table-list/messenger-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: MessengerTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: MessengerTableItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessengerTableRoutingModule {}
