import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessengerTableRoutingModule } from './messenger-table-routing.module';
import { MessengerTableListPage } from './messenger-table-list/messenger-table-list.page';
import { MessengerTableItemPage } from './messenger-table-item/messenger-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { MessengerTableComponentModule } from 'components/messenger-table/messenger-table.module';

@NgModule({
  declarations: [MessengerTableListPage, MessengerTableItemPage],
  imports: [CommonModule, SharedModule, MessengerTableRoutingModule, MessengerTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MessengerTableModule {}
