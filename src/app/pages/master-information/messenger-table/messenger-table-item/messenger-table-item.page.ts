import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'messenger-table-item-page',
  templateUrl: './messenger-table-item.page.html',
  styleUrls: ['./messenger-table-item.page.scss']
})
export class MessengerTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.MESSENGER_TABLE, servicePath: 'MessengerTable' };
  }
}
