import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MessengerTableItemPage } from './messenger-table-item.page';

describe('MessengerTableItemPage', () => {
  let component: MessengerTableItemPage;
  let fixture: ComponentFixture<MessengerTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MessengerTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MessengerTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
