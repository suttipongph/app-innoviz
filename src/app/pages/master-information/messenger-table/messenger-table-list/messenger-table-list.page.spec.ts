import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MessengerTableListPage } from './messenger-table-list.page';

describe('MessengerTableListPage', () => {
  let component: MessengerTableListPage;
  let fixture: ComponentFixture<MessengerTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MessengerTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MessengerTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
