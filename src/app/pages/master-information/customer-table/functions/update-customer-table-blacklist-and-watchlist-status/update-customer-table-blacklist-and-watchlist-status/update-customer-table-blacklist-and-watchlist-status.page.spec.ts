import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UpdateCustomerTableBlacklistAndWatchlistStatusPage } from './update-customer-table-blacklist-and-watchlist-status.page';

describe('UpdateCustomerTableBlacklistAndWatchlistStatusPage', () => {
  let component: UpdateCustomerTableBlacklistAndWatchlistStatusPage;
  let fixture: ComponentFixture<UpdateCustomerTableBlacklistAndWatchlistStatusPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UpdateCustomerTableBlacklistAndWatchlistStatusPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCustomerTableBlacklistAndWatchlistStatusPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
