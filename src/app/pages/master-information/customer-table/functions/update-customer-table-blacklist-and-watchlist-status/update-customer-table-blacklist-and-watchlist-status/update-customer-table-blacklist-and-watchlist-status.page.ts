import { Component, OnInit } from '@angular/core';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel, PathParamModel } from 'shared/models/systemModel';

@Component({
  selector: 'update-customer-table-blacklist-and-watchlist-status',
  templateUrl: './update-customer-table-blacklist-and-watchlist-status.page.html',
  styleUrls: ['./update-customer-table-blacklist-and-watchlist-status.page.scss']
})
export class UpdateCustomerTableBlacklistAndWatchlistStatusPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.CUSTOMERTABLE_UPDATE_BLACKLIST_AND_WATCHLIST_STATUS,
      servicePath: 'CustomerTable/Function/UpdateCustomerTableBlacklistAndWatchlistStatus'
    };
  }
}
