import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { UpdateCustomerTableBlacklistAndWatchlistStatusPage } from './update-customer-table-blacklist-and-watchlist-status/update-customer-table-blacklist-and-watchlist-status.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateCustomerTableBlacklistAndWatchlistStatusPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpdateCustomerTableBlacklistAndWatchlistStatusFunctionRoutingModule {}
