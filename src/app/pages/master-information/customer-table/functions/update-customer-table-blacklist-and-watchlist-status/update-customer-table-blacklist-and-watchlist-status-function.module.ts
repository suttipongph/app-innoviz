import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateCustomerTableBlacklistAndWatchlistStatusFunctionRoutingModule } from './update-customer-table-blacklist-and-watchlist-status-function-routing.module';
import { UpdateCustomerTableBlacklistAndWatchlistStatusPage } from './update-customer-table-blacklist-and-watchlist-status/update-customer-table-blacklist-and-watchlist-status.page';
import { SharedModule } from 'shared/shared.module';
import { UpdateCustomerTableBlacklistStatusComponentModule } from 'components/customer-table/update-customer-table-blacklist-status/update-customer-table-blacklist-status.module';

@NgModule({
  declarations: [UpdateCustomerTableBlacklistAndWatchlistStatusPage],
  imports: [
    CommonModule,
    SharedModule,
    UpdateCustomerTableBlacklistAndWatchlistStatusFunctionRoutingModule,
    UpdateCustomerTableBlacklistStatusComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UpdateBlacklistAndWatchlistStatusFunctionModule {}
