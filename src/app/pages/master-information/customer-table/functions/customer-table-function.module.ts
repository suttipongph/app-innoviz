import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'primeng/api';
import { CustomerTableFunctionRoutingModule } from './customer-table-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, CustomerTableFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerTableFunctionModule {}
