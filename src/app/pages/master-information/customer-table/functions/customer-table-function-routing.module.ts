import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'updatecustomertablestatus',
    loadChildren: () =>
      import('./update-customer-status/update-customer-table-status-function.module').then((m) => m.UpdateCustomerTableStatusFunctionModule)
  },
  {
    path: 'updatecustomertableblacklistandwatchliststatus',
    loadChildren: () =>
      import('./update-customer-table-blacklist-and-watchlist-status/update-customer-table-blacklist-and-watchlist-status-function.module').then(
        (m) => m.UpdateBlacklistAndWatchlistStatusFunctionModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerTableFunctionRoutingModule {}
