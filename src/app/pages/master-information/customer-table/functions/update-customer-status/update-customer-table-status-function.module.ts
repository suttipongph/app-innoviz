import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateCustomerTableStatusFunctionRoutingModule } from './update-customer-table-status-function-routing.module';
import { UpdateCustomerTableStatusPage } from './update-customer-table-status/update-customer-table-status.page';
import { SharedModule } from 'shared/shared.module';
import { UpdateCustomerTableStatusComponentModule } from 'components/customer-table/update-customer-table-status/update-customer-table-status.module';

@NgModule({
  declarations: [UpdateCustomerTableStatusPage],
  imports: [CommonModule, SharedModule, UpdateCustomerTableStatusFunctionRoutingModule, UpdateCustomerTableStatusComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UpdateCustomerTableStatusFunctionModule {}
