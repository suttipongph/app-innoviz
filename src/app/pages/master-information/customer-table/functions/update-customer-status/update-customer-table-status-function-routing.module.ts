import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { UpdateCustomerTableStatusPage } from './update-customer-table-status/update-customer-table-status.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateCustomerTableStatusPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpdateCustomerTableStatusFunctionRoutingModule {}
