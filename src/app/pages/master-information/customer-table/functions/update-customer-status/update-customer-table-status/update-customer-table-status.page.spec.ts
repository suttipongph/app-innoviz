import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UpdateCustomerTableStatusPage } from './update-customer-table-status.page';

describe('UpdateCustomerTableStatusPage', () => {
  let component: UpdateCustomerTableStatusPage;
  let fixture: ComponentFixture<UpdateCustomerTableStatusPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UpdateCustomerTableStatusPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCustomerTableStatusPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
