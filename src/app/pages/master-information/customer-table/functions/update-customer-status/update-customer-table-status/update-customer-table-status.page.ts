import { Component, OnInit } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';

@Component({
  selector: 'app-update-customer-table-status',
  templateUrl: './update-customer-table-status.page.html',
  styleUrls: ['./update-customer-table-status.page.scss']
})
export class UpdateCustomerTableStatusPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.CUSTOMERTABLE_UPDATE_STATUS,
      servicePath: 'CustomerTable/Function/UpdateCustomerTableStatus'
    };
  }
}
