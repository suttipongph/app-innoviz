import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CustomerTableItemPage } from './customer-table-item.page';

describe('CustomerTableItemPage', () => {
  let component: CustomerTableItemPage;
  let fixture: ComponentFixture<CustomerTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CustomerTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
