import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'customer-table-item-page',
  templateUrl: './customer-table-item.page.html',
  styleUrls: ['./customer-table-item.page.scss']
})
export class CustomerTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.CUSTOMER_TABLE, servicePath: 'CustomerTable' };
  }
}
