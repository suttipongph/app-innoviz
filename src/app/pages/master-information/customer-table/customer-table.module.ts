import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerTableRoutingModule } from './customer-table-routing.module';
import { CustomerTableListPage } from './customer-table-list/customer-table-list.page';
import { CustomerTableItemPage } from './customer-table-item/customer-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { CustomerTableComponentModule } from 'components/customer-table/customer-table/customer-table.module';

@NgModule({
  declarations: [CustomerTableListPage, CustomerTableItemPage],
  imports: [CommonModule, SharedModule, CustomerTableRoutingModule, CustomerTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerTableModule {}
