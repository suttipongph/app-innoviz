import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CustomerTableListPage } from './customer-table-list.page';

describe('CustomerTableListPage', () => {
  let component: CustomerTableListPage;
  let fixture: ComponentFixture<CustomerTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CustomerTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
