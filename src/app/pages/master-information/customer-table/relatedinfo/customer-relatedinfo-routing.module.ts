import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'taxreporttrans',
    loadChildren: () => import('./tax-report-trans/customer-table-tax-report-trans.module').then((m) => m.CustomerTableTaxReportTransModule)
  },
  {
    path: 'guarantortrans',
    loadChildren: () => import('./guarantor-trans/guarantor-trans.module').then((m) => m.CustomerTableGuarantorTransModule)
  },
  {
    path: 'jointventuretrans',
    loadChildren: () => import('./joint-venture-trans/joint-venture-trans.module').then((m) => m.CustomerTableJointVentureTransModule)
  },
  {
    path: 'custvisitingtrans',
    loadChildren: () => import('./cust-visiting-trans/customer-table-cust-visiting-trans.module').then((m) => m.CustomerTableCustVisitingTransModule)
  },
  {
    path: 'ownertrans',
    loadChildren: () => import('./owner-trans/customer-table-owner-trans.module').then((m) => m.CustomerTableOwnerTransModule)
  },
  {
    path: 'authorizedpersontrans',
    loadChildren: () =>
      import('./authorized-person-trans/customer-table-authorized-person-trans.module').then((m) => m.CustomerTableAuthorizedPersonTransModule)
  },
  {
    path: 'contactpersontrans',
    loadChildren: () =>
      import('./contact-person/customer-table-contact-person-relatedinfo.module').then((m) => m.CustomerTableContactPersonRelatedinfoModule)
  },
  {
    path: 'contacttrans',
    loadChildren: () => import('./contact/customer-table-contact-relatedinfo.module').then((m) => m.CustomerTableContactRelatedinfoModule)
  },
  {
    path: 'memotrans',
    loadChildren: () => import('./memo/customer-table-memo-relatedinfo.module').then((m) => m.CustomerTableMemoRelatedinfoModule)
  },
  {
    path: 'projectreferencetrans',
    loadChildren: () =>
      import('./project-reference/customer-table-project-reference-relatedinfo.module').then((m) => m.CustomerTableProjectReferenceRelatedinfoModule)
  },
  {
    path: 'customercreditlimitbyproduct',
    loadChildren: () =>
      import('./customer-credit-limit-by-product/customer-credit-limit-by-product-relatedinfo.module').then(
        (m) => m.CustomertableCustomerCreditLimitByProductModule
      )
  },
  {
    path: 'addresstrans',
    loadChildren: () => import('./address-trans/customer-table-address-trans.module').then((m) => m.CustomerTableAddressTransModule)
  },
  {
    path: 'financialstatementtrans',
    loadChildren: () =>
      import('./financial-statement-trans/customer-table-financial-statement-trans.module').then((m) => m.CustomerTableFinancialStatementTransModule)
  },
  {
    path: 'custbank',
    loadChildren: () => import('./cust-bank/customer-table-cust-bank.module').then((m) => m.CustomerTableCustBankModule)
  },
  {
    path: 'businesscollateral',
    loadChildren: () =>
      import('./business-collateral/customer-table-business-collateral-relatedinfo.module').then(
        (m) => m.CustomerTableBusinessCollateralRelatedinfoModule
      )
  },
  {
    path: 'attachment',
    loadChildren: () => import('./attachment/customer-table-attachment.module').then((m) => m.CustomerTableAttachmentModule)
  },
  {
    path: 'custtransaction',
    loadChildren: () => import('./customer-transaction/customer-transaction-relatedinfo.module').then((m) => m.CustomerTransactionRelatedinfoModule)
  },
  {
    path: 'retentiontransaction',
    loadChildren: () =>
      import('./retention-transaction/retention-transaction-relatedinfo.module').then((m) => m.RetentionTransactionRelatedinfoModule)
  },
  {
    path: 'retentionoutstanding',
    loadChildren: () =>
      import('./retention-outstanding/retention-outstanding-relatedinfo.module').then((m) => m.RetentionOutstandingRelatedinfoModule)
  },
  {
    path: 'inquiryassignmentagreementoutstanding',
    loadChildren: () => import('./inquiry-assignment-outstanding/inquiry-assignment-outstanding.module').then((m) => m.InquiryAssignmentOutstandingModule)
  },
  {
    path: 'creditoutstanding',
    loadChildren: () =>
      import('./credit-outstanding/customer-table-credit-outstanding-relatedinfo.module').then((m) => m.CreditAppRequestTableCreditOutstandingRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRelatedinfoRoutingModule {}
