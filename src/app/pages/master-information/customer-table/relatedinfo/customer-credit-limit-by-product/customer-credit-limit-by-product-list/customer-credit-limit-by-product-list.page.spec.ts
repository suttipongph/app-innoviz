import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CustomerCreditLimitByProductListPage } from './customer-credit-limit-by-product-list.page';

describe('CustomerCreditLimitByProductListComponent', () => {
  let component: CustomerCreditLimitByProductListPage;
  let fixture: ComponentFixture<CustomerCreditLimitByProductListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CustomerCreditLimitByProductListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerCreditLimitByProductListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
