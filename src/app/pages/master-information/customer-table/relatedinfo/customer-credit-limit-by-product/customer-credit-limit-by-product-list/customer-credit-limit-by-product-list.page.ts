import { Component } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-customer-credit-limit-by-product-list',
  templateUrl: './customer-credit-limit-by-product-list.page.html',
  styleUrls: ['./customer-credit-limit-by-product-list.page.scss']
})
export class CustomerCreditLimitByProductListPage {
  option: OptionModel;
  pageInfo: PageInformationModel;

  constructor() {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.CUSTOMER_CREDIT_LIMIT_BY_PRODUCT,
      servicePath: 'CustomerTable/RelatedInfo/CustomerCreditLimitByProduct'
    };
  }
}
