import { Component } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-customer-credit-limit-by-product-item',
  templateUrl: './customer-credit-limit-by-product-item.page.html',
  styleUrls: ['./customer-credit-limit-by-product-item.page.scss']
})
export class CustomerCreditLimitByProductItemPage {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.CUSTOMER_CREDIT_LIMIT_BY_PRODUCT,
      servicePath: 'CustomerTable/RelatedInfo/CustomerCreditLimitByProduct'
    };
  }
}
