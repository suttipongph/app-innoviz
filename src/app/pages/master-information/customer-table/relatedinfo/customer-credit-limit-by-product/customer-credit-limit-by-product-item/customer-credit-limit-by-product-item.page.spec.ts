import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CustomerCreditLimitByProductItemPage } from './customer-credit-limit-by-product-item.page';

describe('CustomerCreditLimitByProductItemComponent', () => {
  let component: CustomerCreditLimitByProductItemPage;
  let fixture: ComponentFixture<CustomerCreditLimitByProductItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CustomerCreditLimitByProductItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerCreditLimitByProductItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
