import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerCreditLimitByProductRelatedinfoRoutingModule } from './customer-credit-limit-by-product-relatedinfo-routing.module';
import { CustomerCreditLimitByProductListPage } from './customer-credit-limit-by-product-list/customer-credit-limit-by-product-list.page';
import { CustomerCreditLimitByProductItemPage } from './customer-credit-limit-by-product-item/customer-credit-limit-by-product-item.page';
import { SharedModule } from 'shared/shared.module';
import { CustomerCreditLimitByProductComponentModule } from 'components/customer-credit-limit-by-product/customer-credit-limit-by-product.module';
@NgModule({
  declarations: [CustomerCreditLimitByProductListPage, CustomerCreditLimitByProductItemPage],
  imports: [CommonModule, SharedModule, CustomerCreditLimitByProductRelatedinfoRoutingModule, CustomerCreditLimitByProductComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomertableCustomerCreditLimitByProductModule {}
