import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerCreditLimitByProductListPage } from './customer-credit-limit-by-product-list/customer-credit-limit-by-product-list.page';
import { CustomerCreditLimitByProductItemPage } from './customer-credit-limit-by-product-item/customer-credit-limit-by-product-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: CustomerCreditLimitByProductListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CustomerCreditLimitByProductItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerCreditLimitByProductRelatedinfoRoutingModule {}
