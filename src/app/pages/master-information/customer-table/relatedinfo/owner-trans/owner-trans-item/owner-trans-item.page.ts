import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'owner-trans-item.page',
  templateUrl: './owner-trans-item.page.html',
  styleUrls: ['./owner-trans-item.page.scss']
})
export class OwnerTransItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.JOINT_VENTURE_TRANS, servicePath: 'CustomerTable/RelatedInfo/OwnerTrans' };
  }
}
