import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'cust-bank-item.page',
  templateUrl: './cust-bank-item.page.html',
  styleUrls: ['./cust-bank-item.page.scss']
})
export class CustBankItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.CUST_BANK, servicePath: 'CustomerTable/RelatedInfo/CustBank' };
  }
}
