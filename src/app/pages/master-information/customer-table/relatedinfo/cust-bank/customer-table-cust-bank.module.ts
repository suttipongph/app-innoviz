import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustBankRoutingModule } from './customer-table-cust-bank-routing.module';
import { CustBankListPage } from './cust-bank-list/cust-bank-list.page';
import { CustBankItemPage } from './cust-bank-item/cust-bank-item.page';
import { SharedModule } from 'shared/shared.module';
import { CustBankComponentModule } from 'components/cust-bank/cust-bank.module';

@NgModule({
  declarations: [CustBankListPage, CustBankItemPage],
  imports: [CommonModule, SharedModule, CustBankRoutingModule, CustBankComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerTableCustBankModule {}
