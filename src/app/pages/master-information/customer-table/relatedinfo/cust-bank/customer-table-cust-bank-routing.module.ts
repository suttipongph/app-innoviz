import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustBankListPage } from './cust-bank-list/cust-bank-list.page';
import { CustBankItemPage } from './cust-bank-item/cust-bank-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: CustBankListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CustBankItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustBankRoutingModule {}
