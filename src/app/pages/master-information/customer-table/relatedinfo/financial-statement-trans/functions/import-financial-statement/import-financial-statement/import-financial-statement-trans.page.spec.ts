import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ImportFinancialStatementTransPage } from './import-financial-statement-trans.page';

describe('ImportFinancialStatementTransPage', () => {
  let component: ImportFinancialStatementTransPage;
  let fixture: ComponentFixture<ImportFinancialStatementTransPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ImportFinancialStatementTransPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportFinancialStatementTransPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
