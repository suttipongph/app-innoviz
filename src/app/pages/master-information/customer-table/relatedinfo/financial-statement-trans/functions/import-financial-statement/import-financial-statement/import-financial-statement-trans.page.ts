import { Component, OnInit } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { UIControllerService } from 'core/services/uiController.service';
import { getProductType } from 'shared/functions/value.function';

@Component({
  selector: 'import-financial-statement-trans-page',
  templateUrl: './import-financial-statement-trans.page.html',
  styleUrls: ['./import-financial-statement-trans.page.scss']
})
export class ImportFinancialStatementTransPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.IMPORT_FINANCIAL_STATEMENT_TRANS,
      servicePath: `CustomerTable/RelatedInfo/FinancialStatementTrans/Function/ImportFinancialStatementTrans`
    };
  }
}

