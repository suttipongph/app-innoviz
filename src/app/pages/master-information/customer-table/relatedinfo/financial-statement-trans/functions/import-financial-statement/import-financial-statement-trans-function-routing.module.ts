import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ImportFinancialStatementTransPage } from './import-financial-statement/import-financial-statement-trans.page';

const routes: Routes = [
  {
    path: '',
    component: ImportFinancialStatementTransPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerTableFinancialRelatedinfoImportFinancialFunctionRoutingModule {}
