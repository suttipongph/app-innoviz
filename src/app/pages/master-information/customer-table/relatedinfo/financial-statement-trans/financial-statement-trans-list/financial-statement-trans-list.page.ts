import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'financial-statement-trans-list-page',
  templateUrl: './financial-statement-trans-list.page.html',
  styleUrls: ['./financial-statement-trans-list.page.scss']
})
export class FinancialStatementTransListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.FINANCIAL_STATEMENT_TRANS, servicePath: 'CustomerTable/RelatedInfo/FinancialStatementTrans' };
  }
}
