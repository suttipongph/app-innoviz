import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'guarantor-trans-item.page',
  templateUrl: './guarantor-trans-item.page.html',
  styleUrls: ['./guarantor-trans-item.page.scss']
})
export class GuarantorTransItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.GUARANTOR_TRANS, servicePath: 'CustomerTable/RelatedInfo/GuarantorTrans' };
  }
}
