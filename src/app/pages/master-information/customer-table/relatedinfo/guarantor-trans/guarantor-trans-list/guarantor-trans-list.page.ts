import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'guarantor-trans-list-page',
  templateUrl: './guarantor-trans-list.page.html',
  styleUrls: ['./guarantor-trans-list.page.scss']
})
export class GuarantorTransListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.GUARANTOR_TRANS, servicePath: 'CustomerTable/RelatedInfo/GuarantorTrans' };
  }
}
