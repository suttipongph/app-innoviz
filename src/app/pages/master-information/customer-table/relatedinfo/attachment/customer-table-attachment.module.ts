import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AttachmentListPage } from './attachment-list/attachment-list.page';
import { AttachmentItemPage } from './attachment-item/attachment-item.page';
import { CustomerTableAttachmentRoutingModule } from './customer-table-attachment-routing.module';
import { SharedModule } from 'shared/shared.module';
import { AttachmentComponentModule } from 'components/attachment/attachment.module';

@NgModule({
  declarations: [AttachmentListPage, AttachmentItemPage],
  imports: [CommonModule, SharedModule, CustomerTableAttachmentRoutingModule, AttachmentComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerTableAttachmentModule {}
