import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'primeng/api';
import { CustomerRelatedinfoRoutingModule } from './customer-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, CustomerRelatedinfoRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerRelatedinfoModule {}
