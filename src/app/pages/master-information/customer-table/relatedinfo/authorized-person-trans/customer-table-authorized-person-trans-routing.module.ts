import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthorizedPersonTransListPage } from './authorized-person-trans-list/authorized-person-trans-list.page';
import { AuthorizedPersonTransItemPage } from './authorized-person-trans-item/authorized-person-trans-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: AuthorizedPersonTransListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: AuthorizedPersonTransItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerTableAuthorizedPersonTransRoutingModule {}
