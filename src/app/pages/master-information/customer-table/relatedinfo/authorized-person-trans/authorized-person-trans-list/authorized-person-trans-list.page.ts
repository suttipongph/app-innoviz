import { Component, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { EmptyGuid, ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel, RowIdentity } from 'shared/models/systemModel';

@Component({
  selector: 'authorized-person-trans-list-page',
  templateUrl: './authorized-person-trans-list.page.html',
  styleUrls: ['./authorized-person-trans-list.page.scss']
})
export class AuthorizedPersonTransListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'authorizedPersonTransGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.AUTHORIZED_PERSON_TRANS, servicePath: 'CustomerTable/RelatedInfo/AuthorizedPersonTrans' };
  }
}
