import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthorizedPersonTransListPage } from './authorized-person-trans-list/authorized-person-trans-list.page';
import { AuthorizedPersonTransItemPage } from './authorized-person-trans-item/authorized-person-trans-item.page';
import { SharedModule } from 'shared/shared.module';
import { AuthorizedPersonTransComponentModule } from 'components/authorized-person-trans/authorized-person-trans.module';
import { CustomerTableAuthorizedPersonTransRoutingModule } from './customer-table-authorized-person-trans-routing.module';

@NgModule({
  declarations: [AuthorizedPersonTransListPage, AuthorizedPersonTransItemPage],
  imports: [CommonModule, SharedModule, CustomerTableAuthorizedPersonTransRoutingModule, AuthorizedPersonTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerTableAuthorizedPersonTransModule {}
