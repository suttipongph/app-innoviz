import { AuthGuardService } from 'core/services/auth-guard.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RetentionOutstandingListPage } from './retention-outstanding-list/retention-outstanding-list.page';
import { RetentionOutstandingItemPage } from './retention-outstanding-item/retention-outstanding-item.page';

const routes: Routes = [
  {
    path: '',
    component: RetentionOutstandingListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: RetentionOutstandingItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RetentionOutstandingRelatedinfoRoutingModule {}
