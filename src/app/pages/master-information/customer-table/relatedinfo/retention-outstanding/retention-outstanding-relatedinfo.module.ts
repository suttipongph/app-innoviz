import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RetentionOutstandingRelatedinfoRoutingModule } from './retention-outstanding-relatedinfo-routing.module';
import { RetentionOutstandingListPage } from './retention-outstanding-list/retention-outstanding-list.page';
import { RetentionOutstandingItemPage } from './retention-outstanding-item/retention-outstanding-item.page';
import { SharedModule } from 'shared/shared.module';
import { InquiryRetentionOutstandingComponentModule } from 'components/inquiry/inquiry-retention-outstanding/inquiry-retention-outstanding.module';

@NgModule({
  declarations: [RetentionOutstandingListPage, RetentionOutstandingItemPage],
  imports: [CommonModule, SharedModule, RetentionOutstandingRelatedinfoRoutingModule, InquiryRetentionOutstandingComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RetentionOutstandingRelatedinfoModule {}
