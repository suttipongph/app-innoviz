import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'retention-outstanding-item.page',
  templateUrl: './retention-outstanding-item.page.html',
  styleUrls: ['./retention-outstanding-item.page.scss']
})
export class RetentionOutstandingItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.RETENTION_OUTSTANDING, servicePath: 'CustomerTable/RelatedInfo/RetentionOutstanding' };
  }
}
