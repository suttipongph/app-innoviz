import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RetentionOutstandingItemPage } from './retention-outstanding-item.page';

describe('RetentionOutstandingItemPage', () => {
  let component: RetentionOutstandingItemPage;
  let fixture: ComponentFixture<RetentionOutstandingItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RetentionOutstandingItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RetentionOutstandingItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
