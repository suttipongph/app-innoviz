import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerTableContactRelatedinfoRoutingModule } from './customer-table-contact-relatedinfo-routing.module';
import { ContactListPage } from './contact-list/contact-list.page';
import { ContactItemPage } from './contact-item/contact-item.page';
import { SharedModule } from 'shared/shared.module';
import { ContactTransComponentModule } from 'components/contact-trans/contact-trans/contact-trans.module';

@NgModule({
  declarations: [ContactListPage, ContactItemPage],
  imports: [CommonModule, SharedModule, CustomerTableContactRelatedinfoRoutingModule, ContactTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerTableContactRelatedinfoModule {}
