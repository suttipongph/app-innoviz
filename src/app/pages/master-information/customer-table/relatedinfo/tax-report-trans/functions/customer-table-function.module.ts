import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'primeng/api';
import { CustomerTableFinancialRelatedinfoFunctionRoutingModule } from './customer-table-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, CustomerTableFinancialRelatedinfoFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerTableFinancialRelatedinfoFunctionModule {}
