import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ImportTaxReportPage } from './import-tax-report/import-tax-report.page';

const routes: Routes = [
  {
    path: '',
    component: ImportTaxReportPage,
    canActivate: [AuthGuardService]
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerTableFinancialRelatedinfoImportTaxReportFunctionRoutingModule {}
