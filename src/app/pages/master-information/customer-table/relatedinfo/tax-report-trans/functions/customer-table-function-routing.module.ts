import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'importtaxreport',
    loadChildren: () =>
      import('./import-tax-report/import-tax-report-function.module').then(
        (m) => m.CustomerTableFinancialRelatedinfoImportTaxReportFunctionModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerTableFinancialRelatedinfoFunctionRoutingModule {}
