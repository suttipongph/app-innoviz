import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportTaxReportPage } from './import-tax-report.page';

describe('ImportTaxReportComponent', () => {
  let component: ImportTaxReportPage;
  let fixture: ComponentFixture<ImportTaxReportPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImportTaxReportPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportTaxReportPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
