import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { ImportTaxReportPage } from './import-tax-report/import-tax-report.page';
import { CustomerTableFinancialRelatedinfoImportTaxReportFunctionRoutingModule } from './import-tax-report-function-routing.module';
import { ImportTaxReportComponentModule } from 'components/tax-report-trans/import-tax-report/import-tax-report.module';

@NgModule({
  declarations: [ImportTaxReportPage],
  imports: [
    CommonModule,
    SharedModule,
    CustomerTableFinancialRelatedinfoImportTaxReportFunctionRoutingModule,
    ImportTaxReportComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerTableFinancialRelatedinfoImportTaxReportFunctionModule {}
