import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-import-tax-report',
  templateUrl: './import-tax-report.page.html',
  styleUrls: ['./import-tax-report.page.scss']
})
export class ImportTaxReportPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.IMPORT_TAX_REPORT,
      servicePath: `CustomerTable/RelatedInfo/TaxReportTrans/Function/ImportTaxReport`
    };
  }
}
