import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaxReportTransListPage } from './tax-report-trans-list/tax-report-trans-list.page';
import { TaxReportTransItemPage } from './tax-report-trans-item/tax-report-trans-item.page';
import { SharedModule } from 'shared/shared.module';
import { TaxReportTransComponentModule } from 'components/tax-report-trans/tax-report-trans/tax-report-trans.module';
import { CustomerTableTaxReportTransRoutingModule } from './customer-table-tax-report-trans-routing.module';

@NgModule({
  declarations: [TaxReportTransListPage, TaxReportTransItemPage],
  imports: [CommonModule, SharedModule, CustomerTableTaxReportTransRoutingModule, TaxReportTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerTableTaxReportTransModule {}
