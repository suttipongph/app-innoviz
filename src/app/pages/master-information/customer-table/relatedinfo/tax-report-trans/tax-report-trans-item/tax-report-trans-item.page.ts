import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'tax-report-trans-item.page',
  templateUrl: './tax-report-trans-item.page.html',
  styleUrls: ['./tax-report-trans-item.page.scss']
})
export class TaxReportTransItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.TAX_REPORT_TRANS, servicePath: 'CustomerTable/RelatedInfo/TaxReportTrans' };
  }
}
