import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerTableBusinessCollateralRelatedinfoRoutingModule } from './customer-table-business-collateral-relatedinfo-routing.module';
import { BusinessCollateralListPage } from './business-collateral-list/business-collateral-list.page';
import { BusinessCollateralItemPage } from './business-collateral-item/business-collateral-item.page';
import { SharedModule } from 'shared/shared.module';
import { CustBusinessCollateralComponentModule } from 'components/cust-business-collateral/cust-business-collateral.module';

@NgModule({
  declarations: [BusinessCollateralListPage, BusinessCollateralItemPage],
  imports: [CommonModule, SharedModule, CustomerTableBusinessCollateralRelatedinfoRoutingModule, CustBusinessCollateralComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerTableBusinessCollateralRelatedinfoModule {}
