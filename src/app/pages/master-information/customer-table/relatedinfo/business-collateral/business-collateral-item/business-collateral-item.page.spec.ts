import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessCollateralItemPage } from './business-collateral-item.page';

describe('BusinessCollateralItemPage', () => {
  let component: BusinessCollateralItemPage;
  let fixture: ComponentFixture<BusinessCollateralItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BusinessCollateralItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCollateralItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
