import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'business-collateral-item.page',
  templateUrl: './business-collateral-item.page.html',
  styleUrls: ['./business-collateral-item.page.scss']
})
export class BusinessCollateralItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.BUSINESS_COLLATERAL, servicePath: 'CustomerTable/RelatedInfo/BusinessCollateral' };
  }
}
