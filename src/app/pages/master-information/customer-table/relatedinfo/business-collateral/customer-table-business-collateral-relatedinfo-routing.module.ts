import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BusinessCollateralListPage } from './business-collateral-list/business-collateral-list.page';
import { BusinessCollateralItemPage } from './business-collateral-item/business-collateral-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: BusinessCollateralListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BusinessCollateralItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerTableBusinessCollateralRelatedinfoRoutingModule {}
