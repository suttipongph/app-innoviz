import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessCollateralListPage } from './business-collateral-list.page';

describe('BusinessCollateralListPage', () => {
  let component: BusinessCollateralListPage;
  let fixture: ComponentFixture<BusinessCollateralListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BusinessCollateralListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCollateralListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
