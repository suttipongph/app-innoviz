import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'business-collateral-list-page',
  templateUrl: './business-collateral-list.page.html',
  styleUrls: ['./business-collateral-list.page.scss']
})
export class BusinessCollateralListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  parentId: string;
  constructor(private uiService: UIControllerService) {
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'custBusinessCollateralGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.BUSINESS_COLLATERAL, servicePath: 'CustomerTable/RelatedInfo/BusinessCollateral' };
  }
}
