import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'retention-transaction-item.page',
  templateUrl: './retention-transaction-item.page.html',
  styleUrls: ['./retention-transaction-item.page.scss']
})
export class RetentionTransactionItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.RETENTION_TRANSACTION, servicePath: 'CustomerTable/RelatedInfo/RetentionTransaction' };
  }
}
