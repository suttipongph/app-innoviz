import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'retention-transaction-list-page',
  templateUrl: './retention-transaction-list.page.html',
  styleUrls: ['./retention-transaction-list.page.scss']
})
export class RetentionTransactionListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  parentId = this.uiService.getRelatedInfoParentTableKey();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'retentionTransGUID';
    const columns: ColumnModel[] = [];
    this.setColumnOptionByPath(columns);
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.RETENTION_TRANSACTION, servicePath: 'CustomerTable/RelatedInfo/RetentionTransaction' };
  }
  setColumnOptionByPath(columns: ColumnModel[]) {
    columns.push({
      label: null,
      textKey: 'customerTableGUID',
      type: ColumnType.MASTER,
      visibility: true,
      sorting: SortType.NONE,
      parentKey: this.parentId
    });
  }
}
