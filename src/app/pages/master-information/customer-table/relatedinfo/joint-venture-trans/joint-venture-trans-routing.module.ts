import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JointVentureTransListPage } from './joint-venture-trans-list/joint-venture-trans-list.page';
import { JointVentureTransItemPage } from './joint-venture-trans-item/joint-venture-trans-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: JointVentureTransListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: JointVentureTransItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerTableJointVentureTransRoutingModule {}
