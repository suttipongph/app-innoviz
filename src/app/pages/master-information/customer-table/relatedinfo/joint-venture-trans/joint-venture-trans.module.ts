import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JointVentureTransListPage } from './joint-venture-trans-list/joint-venture-trans-list.page';
import { JointVentureTransItemPage } from './joint-venture-trans-item/joint-venture-trans-item.page';
import { SharedModule } from 'shared/shared.module';
import { JointVentureTransComponentModule } from 'components/joint-venture-trans/joint-venture-trans.module';
import { CustomerTableJointVentureTransRoutingModule } from './joint-venture-trans-routing.module';

@NgModule({
  declarations: [JointVentureTransListPage, JointVentureTransItemPage],
  imports: [CommonModule, SharedModule, CustomerTableJointVentureTransRoutingModule, JointVentureTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerTableJointVentureTransModule {}
