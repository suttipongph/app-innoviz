import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'cust-visiting-trans-list-page',
  templateUrl: './cust-visiting-trans-list.page.html',
  styleUrls: ['./cust-visiting-trans-list.page.scss']
})
export class CustVisitingTransListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.CUST_VISITING_TRANS, servicePath: 'CustomerTable/RelatedInfo/CustVisitingTrans' };
  }
}
