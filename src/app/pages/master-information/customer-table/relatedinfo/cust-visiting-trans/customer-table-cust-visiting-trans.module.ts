import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustVisitingTransListPage } from './cust-visiting-trans-list/cust-visiting-trans-list.page';
import { CustVisitingTransItemPage } from './cust-visiting-trans-item/cust-visiting-trans-item.page';
import { SharedModule } from 'shared/shared.module';
import { CustVisitingTransComponentModule } from 'components/cust-visiting-trans/cust-visiting-trans/cust-visiting-trans.module';
import { CustomerTableCustVisitingTransRoutingModule } from './customer-table-cust-visiting-trans-routing.module';

@NgModule({
  declarations: [CustVisitingTransListPage, CustVisitingTransItemPage],
  imports: [CommonModule, SharedModule, CustomerTableCustVisitingTransRoutingModule, CustVisitingTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerTableCustVisitingTransModule {}
