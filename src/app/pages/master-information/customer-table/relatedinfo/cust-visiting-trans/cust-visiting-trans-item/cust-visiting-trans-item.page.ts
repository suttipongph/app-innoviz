import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'cust-visiting-trans-item.page',
  templateUrl: './cust-visiting-trans-item.page.html',
  styleUrls: ['./cust-visiting-trans-item.page.scss']
})
export class CustVisitingTransItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.JOINT_VENTURE_TRANS, servicePath: 'CustomerTable/RelatedInfo/CustVisitingTrans' };
  }
}
