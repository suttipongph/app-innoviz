import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ProjectReferenceItemPage } from './project-reference-item.page';

describe('ProjectReferenceItemComponent', () => {
  let component: ProjectReferenceItemPage;
  let fixture: ComponentFixture<ProjectReferenceItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProjectReferenceItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectReferenceItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
