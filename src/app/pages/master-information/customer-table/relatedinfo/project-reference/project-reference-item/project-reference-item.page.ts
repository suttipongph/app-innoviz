import { Component } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-project-reference-item',
  templateUrl: './project-reference-item.page.html',
  styleUrls: ['./project-reference-item.page.scss']
})
export class ProjectReferenceItemPage {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.PROJECT_REFERENCE, servicePath: 'CustomerTable/RelatedInfo/ProjectReferenceTrans' };
  }
}
