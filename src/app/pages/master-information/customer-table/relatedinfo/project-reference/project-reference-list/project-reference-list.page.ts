import { Component } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, RefType, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-project-reference-list',
  templateUrl: './project-reference-list.page.html',
  styleUrls: ['./project-reference-list.page.scss']
})
export class ProjectReferenceListPage {
  option: OptionModel;
  pageInfo: PageInformationModel;
  parentKey: string = null;
  constructor(private uiService: UIControllerService) {
    this.parentKey = this.uiService.getRelatedInfoParentTableKey();
  }

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'projectReferenceTransGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: false,
        sorting: SortType.NONE,
        parentKey: this.parentKey
      },
      {
        label: null,
        textKey: 'reftype',
        type: ColumnType.INT,
        visibility: false,
        sorting: SortType.NONE,
        parentKey: RefType.Customer.toString()
      }
    ];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.PROJECT_REFERENCE, servicePath: 'CustomerTable/RelatedInfo/ProjectReferenceTrans' };
  }
}
