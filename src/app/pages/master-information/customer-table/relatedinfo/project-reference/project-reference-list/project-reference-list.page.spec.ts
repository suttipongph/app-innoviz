import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ProjectReferenceListPage } from './project-reference-list.page';

describe('ProjectReferenceListComponent', () => {
  let component: ProjectReferenceListPage;
  let fixture: ComponentFixture<ProjectReferenceListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProjectReferenceListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectReferenceListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
