import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectReferenceListPage } from './project-reference-list/project-reference-list.page';
import { ProjectReferenceItemPage } from './project-reference-item/project-reference-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: ProjectReferenceListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ProjectReferenceItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerTableProjectReferenceRelatedinfoRoutingModule {}
