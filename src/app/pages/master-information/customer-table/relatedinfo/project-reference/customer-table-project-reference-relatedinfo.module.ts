import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectReferenceListPage } from './project-reference-list/project-reference-list.page';
import { ProjectReferenceItemPage } from './project-reference-item/project-reference-item.page';
import { SharedModule } from 'shared/shared.module';
import { ProjectReferenceTransComponentModule } from 'components/project-reference-trans/project-reference-trans/project-reference-trans.module';
import { CustomerTableProjectReferenceRelatedinfoRoutingModule } from './customer-table-project-reference-relatedinfo-routing.module';

@NgModule({
  declarations: [ProjectReferenceListPage, ProjectReferenceItemPage],
  imports: [CommonModule, SharedModule, CustomerTableProjectReferenceRelatedinfoRoutingModule, ProjectReferenceTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerTableProjectReferenceRelatedinfoModule {}
