import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { InquiryAssignmentOutstandingRoutingModule } from './inquiry-assignment-outstanding-routing.module';
import { InquiryAssignmentOutstandingListPage } from './inquiry-assignment-outstanding-list/inquiry-assignment-outstanding-list.page';
import { InquiryAssignmentOutstandingComponentModule } from 'components/inquiry/inquiry-assignment-outstanding/inquiry-assignment-outstanding.module';

@NgModule({
  declarations: [InquiryAssignmentOutstandingListPage],
  imports: [CommonModule, SharedModule, InquiryAssignmentOutstandingRoutingModule, InquiryAssignmentOutstandingComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InquiryAssignmentOutstandingModule {}
