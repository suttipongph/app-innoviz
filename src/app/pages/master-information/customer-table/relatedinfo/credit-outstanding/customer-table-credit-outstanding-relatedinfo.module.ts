import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppRequestTableCreditOutstandingRelatedinfoRoutingModule } from './customer-table-credit-outstanding-relatedinfo-routing.module';
import { CreditOutstandingListPage } from './credit-outstanding-list/credit-outstanding-list.page';
import { SharedModule } from 'shared/shared.module';
import { InquiryCreditOutstandingByCustComponentModule } from 'components/inquiry/inquiry-credit-outstanding-by-cust/inquiry-credit-outstanding-by-cust.module';

@NgModule({
  declarations: [CreditOutstandingListPage],
  imports: [CommonModule, SharedModule, CreditAppRequestTableCreditOutstandingRelatedinfoRoutingModule , InquiryCreditOutstandingByCustComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppRequestTableCreditOutstandingRelatedinfoModule {}
