import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CustomerTableItemPage } from './customer-table-item/customer-table-item.page';
import { CustomerTableListPage } from './customer-table-list/customer-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: CustomerTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CustomerTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/customer-relatedinfo.module').then((m) => m.CustomerRelatedinfoModule)
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/customer-table-function.module').then((m) => m.CustomerTableFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerTableRoutingModule {}
