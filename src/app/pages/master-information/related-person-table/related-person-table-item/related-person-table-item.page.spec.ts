import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RelatedPersonTableItemPage } from './related-person-table-item.page';

describe('RelatedPersonTableItemPage', () => {
  let component: RelatedPersonTableItemPage;
  let fixture: ComponentFixture<RelatedPersonTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RelatedPersonTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatedPersonTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
