import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'related-person-table-item-page',
  templateUrl: './related-person-table-item.page.html',
  styleUrls: ['./related-person-table-item.page.scss']
})
export class RelatedPersonTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.RELATED_PERSON_TABLE, servicePath: 'RelatedPersonTable' };
  }
}
