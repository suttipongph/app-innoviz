import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RelatedPersonTableRoutingModule } from './related-person-table-routing.module';
import { RelatedPersonTableListPage } from './related-person-table-list/related-person-table-list.page';
import { RelatedPersonTableItemPage } from './related-person-table-item/related-person-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { RelatedPersonTableComponentModule } from 'components/related-person-table/related-person-table/related-person-table.module';

@NgModule({
  declarations: [RelatedPersonTableListPage, RelatedPersonTableItemPage],
  imports: [CommonModule, SharedModule, RelatedPersonTableRoutingModule, RelatedPersonTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RelatedPersonTableModule {}
