import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { RelatedPersonTableItemPage } from './related-person-table-item/related-person-table-item.page';
import { RelatedPersonTableListPage } from './related-person-table-list/related-person-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: RelatedPersonTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: RelatedPersonTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/related-person-table-relatedinfo.module').then((m) => m.RelatedPersonTableRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RelatedPersonTableRoutingModule {}
