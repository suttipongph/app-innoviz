import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { MemoItemPage } from './memo-item/memo-item.page';
import { MemoListPage } from './memo-list/memo-list.page';

const routes: Routes = [
  {
    path: '',
    component: MemoListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: MemoItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RelatedPersonTableMemoRelatedinfoRoutingModule {}
