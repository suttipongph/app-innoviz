import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel, PathParamModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-memo-item',
  templateUrl: './memo-item.page.html',
  styleUrls: ['./memo-item.page.scss']
})
export class MemoItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.MEMO, servicePath: 'RelatedPersonTable/RelatedInfo/MemoTrans' };
  }
}
