import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RelatedPersonTableRelatedinfoRoutingModule } from './related-person-table-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, RelatedPersonTableRelatedinfoRoutingModule]
})
export class RelatedPersonTableRelatedinfoModule {}
