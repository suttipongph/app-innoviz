import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'memotrans',
    loadChildren: () => import('./memo/related-person-table-memo-relatedinfo.module').then((m) => m.RelatedPersonTableMemoRelatedinfoModule)
  },
  {
    path: 'addresstrans',
    loadChildren: () => import('./address-trans/related-person-table-address-trans.module').then((m) => m.RelatedPersonAddressTransModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RelatedPersonTableRelatedinfoRoutingModule {}
