import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RelatedPersonTableListPage } from './related-person-table-list.page';

describe('RelatedPersonTableListPage', () => {
  let component: RelatedPersonTableListPage;
  let fixture: ComponentFixture<RelatedPersonTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RelatedPersonTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatedPersonTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
