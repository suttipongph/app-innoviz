import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectFinanceRoutingModule } from './project-finance-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, ProjectFinanceRoutingModule]
})
export class ProjectFinanceModule {}
