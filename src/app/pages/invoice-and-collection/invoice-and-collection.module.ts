import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvoiceAndCollectionRoutingModule } from './invoice-and-collection-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, InvoiceAndCollectionRoutingModule]
})
export class InvoiceAndCollectionModule {}
