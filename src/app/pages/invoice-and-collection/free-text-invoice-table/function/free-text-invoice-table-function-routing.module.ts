import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'postfreetextinvoice',
    loadChildren: () =>
      import('./post-free-text-invoice/free-text-invoice-table-post-free-text-invoice-function.module').then(
        (m) => m.FreeTextInvoiceTablePostFreeTextInvoiceFunctionModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FreeTextInvoiceTableFunctionRoutingModule {}
