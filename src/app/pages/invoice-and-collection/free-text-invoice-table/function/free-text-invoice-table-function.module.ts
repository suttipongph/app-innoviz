import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FreeTextInvoiceTableFunctionRoutingModule } from './free-text-invoice-table-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, FreeTextInvoiceTableFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FreeTextInvoiceTableFunctionModule {}
