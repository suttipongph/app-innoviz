import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { PostFreeTextInvoicePage } from './post-free-text-invoice/post-free-text-invoice.page';
import { FreeTextInvoiceTablePostFreeTextInvoiceFunctionRoutingModule } from './free-text-invoice-table-post-free-text-invoice-function-routing.module';
import { PostFreeTextInvoiceComponentModule } from 'components/free-text-invoice-table/post-free-text-invoice/post-free-text-invoice.module';

@NgModule({
  declarations: [PostFreeTextInvoicePage],
  imports: [CommonModule, SharedModule, FreeTextInvoiceTablePostFreeTextInvoiceFunctionRoutingModule, PostFreeTextInvoiceComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FreeTextInvoiceTablePostFreeTextInvoiceFunctionModule {}
