import { Component, OnInit } from '@angular/core';
import { ROUTE_FUNCTION_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'post-free-text-invoice-page',
  templateUrl: './post-free-text-invoice.page.html',
  styleUrls: ['./post-free-text-invoice.page.scss']
})
export class PostFreeTextInvoicePage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.POST_FREE_TEXT_INVOICE,
      servicePath: `FreeTextInvoiceTable/Function/PostFreeTextInvoice`
    };
  }
}
