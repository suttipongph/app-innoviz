import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostFreeTextInvoicePage } from './post-free-text-invoice.page';

describe('PostFreeTextInvoicePage', () => {
  let component: PostFreeTextInvoicePage;
  let fixture: ComponentFixture<PostFreeTextInvoicePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PostFreeTextInvoicePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostFreeTextInvoicePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
