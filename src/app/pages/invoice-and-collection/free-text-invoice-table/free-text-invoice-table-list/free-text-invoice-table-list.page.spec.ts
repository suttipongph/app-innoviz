import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FreeTextInvoiceTableListPage } from './free-text-invoice-table-list.page';

describe('FreeTextInvoiceTableListPage', () => {
  let component: FreeTextInvoiceTableListPage;
  let fixture: ComponentFixture<FreeTextInvoiceTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FreeTextInvoiceTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FreeTextInvoiceTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
