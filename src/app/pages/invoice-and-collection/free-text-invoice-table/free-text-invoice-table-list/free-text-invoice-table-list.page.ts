import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'free-text-invoice-table-list-page',
  templateUrl: './free-text-invoice-table-list.page.html',
  styleUrls: ['./free-text-invoice-table-list.page.scss']
})
export class FreeTextInvoiceTableListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'freeTextInvoiceTableGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.FREE_TEXT_INVOICE_TABLE, servicePath: 'FreeTextInvoiceTable' };
  }
}
