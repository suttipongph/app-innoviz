import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'tax-invoice-item.page',
  templateUrl: './tax-invoice-item.page.html',
  styleUrls: ['./tax-invoice-item.page.scss']
})
export class TaxInvoiceItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.TAX_INVOICE,
      servicePath: 'FreeTextInvoiceTable/RelatedInfo/TaxInvoice',
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.TAX_INVOICE_LINE,
          servicePath: 'FreeTextInvoiceTable/RelatedInfo/TaxInvoice'
        }
      ]
    };
  }
}
