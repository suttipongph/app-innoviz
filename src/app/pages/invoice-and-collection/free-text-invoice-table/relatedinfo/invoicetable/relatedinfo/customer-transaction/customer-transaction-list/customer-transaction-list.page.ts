import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'customer-transaction-list-page',
  templateUrl: './customer-transaction-list.page.html',
  styleUrls: ['./customer-transaction-list.page.scss']
})
export class CustomerTransactionListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute(1);
  pathName = this.uiService.getRelatedInfoOriginTableName();
  parentName = this.uiService.getRelatedInfoActiveTableName();

  parentId = this.uiService.getRelatedInfoParentTableKey();

  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'custTransGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.CUSTOMER_TRANSACTION,
      servicePath: 'InvoiceTable/RelatedInfo/CustTransaction'
    };
  }
}
