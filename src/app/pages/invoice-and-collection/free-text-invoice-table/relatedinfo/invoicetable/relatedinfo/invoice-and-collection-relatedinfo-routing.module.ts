import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'taxinvoice',
    loadChildren: () => import('./tax-invoice/tax-invoice-relatedinfo.module').then((m) => m.TaxInvoiceRelatedinfoModule)
  },
  {
    path: 'custtransaction',
    loadChildren: () => import('./customer-transaction/customer-transaction-relatedinfo.module').then((m) => m.CustomerTransactionRelatedinfoModule)
  },
  {
    path: 'paymenthistory',
    loadChildren: () => import('./payment-history/invoice-payment-history-relatedinfo.module').then((m) => m.InvoicePaymentHistoryRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FreeTextInvoiceRelatedinfoRoutingModule {}
