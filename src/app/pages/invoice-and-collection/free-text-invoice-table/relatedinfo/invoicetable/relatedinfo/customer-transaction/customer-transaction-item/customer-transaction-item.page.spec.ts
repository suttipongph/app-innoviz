import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CustomerTransactionItemPage } from './customer-transaction-item.page';

describe('CustomerTransactionItemPage', () => {
  let component: CustomerTransactionItemPage;
  let fixture: ComponentFixture<CustomerTransactionItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CustomerTransactionItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerTransactionItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
