import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FreeTextInvoiceRelatedinfoRoutingModule } from './free-text-invoice-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, FreeTextInvoiceRelatedinfoRoutingModule]
})
export class FreeTextInvoiceRelatedinfoModule {}
