import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'attachment',
    loadChildren: () => import('./attachment/free-text-invoice-attachment.module').then((m) => m.FreeTextInvoiceAttachmentModule)
  },
  {
    path: 'taxinvoice',
    loadChildren: () => import('./tax-invoice/tax-invoice-relatedinfo.module').then((m) => m.TaxInvoiceRelatedinfoModule)
  },
  {
    path: 'invoicetable',
    loadChildren: () => import('./invoicetable/free-text-invoice-relatedinfo.module').then((m) => m.FreeTextInvoiceTableRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FreeTextInvoiceRelatedinfoRoutingModule {}
