import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { FreeTextInvoiceTableItemPage } from './free-text-invoice-table-item/free-text-invoice-table-item.page';
import { FreeTextInvoiceTableListPage } from './free-text-invoice-table-list/free-text-invoice-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: FreeTextInvoiceTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: FreeTextInvoiceTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/free-text-invoice-relatedinfo.module').then((m) => m.FreeTextInvoiceRelatedinfoModule)
  },
  {
    path: ':id/function',
    loadChildren: () => import('./function/free-text-invoice-table-function.module').then((m) => m.FreeTextInvoiceTableFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FreeTextInvoiceTableRoutingModule {}
