import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FreeTextInvoiceTableItemPage } from './free-text-invoice-table-item.page';

describe('FreeTextInvoiceTableItemPage', () => {
  let component: FreeTextInvoiceTableItemPage;
  let fixture: ComponentFixture<FreeTextInvoiceTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FreeTextInvoiceTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FreeTextInvoiceTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
