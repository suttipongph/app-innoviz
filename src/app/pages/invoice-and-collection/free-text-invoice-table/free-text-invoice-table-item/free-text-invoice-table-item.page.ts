import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'free-text-invoice-table-item-page',
  templateUrl: './free-text-invoice-table-item.page.html',
  styleUrls: ['./free-text-invoice-table-item.page.scss']
})
export class FreeTextInvoiceTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.FREE_TEXT_INVOICE_TABLE, servicePath: 'FreeTextInvoiceTable' };
  }
}
