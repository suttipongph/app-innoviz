import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FreeTextInvoiceTableRoutingModule } from './free-text-invoice-table-routing.module';
import { FreeTextInvoiceTableListPage } from './free-text-invoice-table-list/free-text-invoice-table-list.page';
import { FreeTextInvoiceTableItemPage } from './free-text-invoice-table-item/free-text-invoice-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { FreeTextInvoiceTableComponentModule } from 'components/free-text-invoice-table/free-text-invoice-table/free-text-invoice-table.module';

@NgModule({
  declarations: [FreeTextInvoiceTableListPage, FreeTextInvoiceTableItemPage],
  imports: [CommonModule, SharedModule, FreeTextInvoiceTableRoutingModule, FreeTextInvoiceTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FreeTextInvoiceTableModule {}
