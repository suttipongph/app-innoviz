import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DocumentReturnLineItemPage } from './document-return-line-item.page';

describe('DocumentReturnTableItemPage', () => {
  let component: DocumentReturnLineItemPage;
  let fixture: ComponentFixture<DocumentReturnLineItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DocumentReturnLineItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentReturnLineItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
