import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentReturnTableRoutingModule } from './document-return-table-routing.module';
import { DocumentReturnTableListPage } from './document-return-table-list/document-return-table-list.page';
import { DocumentReturnTableItemPage } from './document-return-table-item/document-return-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { DocumentReturnTableComponentModule } from 'components/document-return/document-return-table/document-return-table.module';
import { DocumentReturnLineItemPage } from './document-return-line-item/document-return-line-item.page';
import { DocumentReturnLineComponentModule } from 'components/document-return/document-return-line/document-return-line.module';

@NgModule({
  declarations: [DocumentReturnTableListPage, DocumentReturnTableItemPage, DocumentReturnLineItemPage],
  imports: [CommonModule, SharedModule, DocumentReturnTableRoutingModule, DocumentReturnTableComponentModule, DocumentReturnLineComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DocumentReturnTableModule {}
