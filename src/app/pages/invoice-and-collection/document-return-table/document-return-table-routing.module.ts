import { DocumentReturnLineItemPage } from './document-return-line-item/document-return-line-item.page';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { DocumentReturnTableItemPage } from './document-return-table-item/document-return-table-item.page';
import { DocumentReturnTableListPage } from './document-return-table-list/document-return-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: DocumentReturnTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: DocumentReturnTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/documentreturntable-child/:id',
    component: DocumentReturnLineItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/document-retunr-table-relatedinfo.module').then((m) => m.DocumentRetunrTableRelatedInfoModule)
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/document-return-table-function.module').then((m) => m.DocumentReturnFunctionModule)
  },
  {
    path: ':id/report',
    loadChildren: () => import('./report/document-return-table-report.module').then((m) => m.DocumentReturnReportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentReturnTableRoutingModule {}
