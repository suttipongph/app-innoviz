import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { DocumentRetunrTableRelatedInfoRoutingModule } from './document-retunr-table-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, DocumentRetunrTableRelatedInfoRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DocumentRetunrTableRelatedInfoModule {}
