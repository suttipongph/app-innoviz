import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: 'attachment',
    loadChildren: () => import('./attachment/document-retunr-table-attachment.module').then((m) => m.DocumentRetunrTableAttachmentModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRetunrTableRelatedInfoRoutingModule {}
