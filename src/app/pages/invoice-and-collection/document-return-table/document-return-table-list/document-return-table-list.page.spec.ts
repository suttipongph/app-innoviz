import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DocumentReturnTableListPage } from './document-return-table-list.page';

describe('DocumentReturnTableListPage', () => {
  let component: DocumentReturnTableListPage;
  let fixture: ComponentFixture<DocumentReturnTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DocumentReturnTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentReturnTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
