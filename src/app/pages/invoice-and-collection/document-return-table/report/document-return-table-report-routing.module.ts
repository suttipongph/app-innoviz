import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: 'printdocumentreturnlabelletter',
    loadChildren: () => import('./document-return-report/document-return-report.module').then((m) => m.PrintDocumentReturnLabelLetterReportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentReturnReportRoutingModule {}
