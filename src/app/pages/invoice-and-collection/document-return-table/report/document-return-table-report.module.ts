import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';

import { DocumentReturnReportRoutingModule } from './document-return-table-report-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, DocumentReturnReportRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DocumentReturnReportModule {}
