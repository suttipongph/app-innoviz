import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_REPORT_GEN } from 'shared/constants/constantGen';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'print-purchase-page',
  templateUrl: './print-doc-re-label-letter.page.html',
  styleUrls: ['./print-doc-re-label-letter.page.scss']
})
export class PrintDocumentReturnLabelLetterPage implements OnInit {
  pageInfo: PageInformationModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_REPORT_GEN.PRINT_DOCUMENT_RETURE_LABER_LETTER,
      servicePath: `DocumentReturnTable/Report/PrintDocumentReturnLabelLetter`
    };
  }
}
