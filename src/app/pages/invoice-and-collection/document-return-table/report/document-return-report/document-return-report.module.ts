import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { PrintDocumentReturnLabelLetterComponentModule } from 'components/document-return/print-document-return-label-letter/print-document-return-label-letter.module';
import { PrintDocumentReturnLabelLetterPage } from './print-doc-re-label-letter/print-doc-re-label-letter.page';
import { PrintDucumentReturnLabelLetterReportRoutingModule } from './document-return-report-routing.module';

@NgModule({
  declarations: [PrintDocumentReturnLabelLetterPage],
  imports: [CommonModule, SharedModule, PrintDucumentReturnLabelLetterReportRoutingModule, PrintDocumentReturnLabelLetterComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PrintDocumentReturnLabelLetterReportModule {}
