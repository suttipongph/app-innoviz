import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PrintDocumentReturnLabelLetterPage } from './print-doc-re-label-letter/print-doc-re-label-letter.page';

const routes: Routes = [
  {
    path: '',
    component: PrintDocumentReturnLabelLetterPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrintDucumentReturnLabelLetterReportRoutingModule {}
