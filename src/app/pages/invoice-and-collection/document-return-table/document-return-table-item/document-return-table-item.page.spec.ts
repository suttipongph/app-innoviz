import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DocumentReturnTableItemPage } from './document-return-table-item.page';

describe('DocumentReturnTableItemPage', () => {
  let component: DocumentReturnTableItemPage;
  let fixture: ComponentFixture<DocumentReturnTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DocumentReturnTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentReturnTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
