import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'document-return-table-item-page',
  templateUrl: './document-return-table-item.page.html',
  styleUrls: ['./document-return-table-item.page.scss']
})
export class DocumentReturnTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.DOCUMENT_RETURN,
      servicePath: 'DocumentReturnTable',
      childPaths: [{ pagePath: ROUTE_RELATED_GEN.DOCUMENT_RETURN_LINE, servicePath: 'DocumentReturnTable' }]
    };
  }
}
