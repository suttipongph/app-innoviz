import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PostDocumentReturnPage } from './post.page';

describe('PostDocumentReturnPage', () => {
  let component: PostDocumentReturnPage;
  let fixture: ComponentFixture<PostDocumentReturnPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PostDocumentReturnPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostDocumentReturnPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
