import { ManageDocumentReturnComponentModule } from './../../../../../components/document-return/manage-document-return/manage-document-return.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostFunctionRoutingModule } from './post-function-routing.module';
import { PostDocumentReturnPage } from './post/post.page';
import { SharedModule } from 'shared/shared.module';

@NgModule({
  declarations: [PostDocumentReturnPage],
  imports: [CommonModule, SharedModule, PostFunctionRoutingModule, ManageDocumentReturnComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PostFunctionModule {}
