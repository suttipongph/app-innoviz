import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: 'postdocumentreturn',
    loadChildren: () => import('./post/post-function.module').then((m) => m.PostFunctionModule)
  },
  {
    path: 'closedocumentreturn',
    loadChildren: () => import('./close/close-function.module').then((m) => m.CloseFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentReturnFunctionRoutingModule {}
