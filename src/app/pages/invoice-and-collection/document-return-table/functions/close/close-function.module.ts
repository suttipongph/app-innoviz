import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CloseFunctionRoutingModule } from './close-function-routing.module';
import { ClosePage } from './close/close.page';
import { SharedModule } from 'shared/shared.module';
import { ManageDocumentReturnComponentModule } from 'components/document-return/manage-document-return/manage-document-return.module';

@NgModule({
  declarations: [ClosePage],
  imports: [CommonModule, SharedModule, CloseFunctionRoutingModule, ManageDocumentReturnComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CloseFunctionModule {}
