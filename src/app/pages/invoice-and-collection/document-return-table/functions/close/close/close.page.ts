import { Component } from '@angular/core';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel, PathParamModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-close',
  templateUrl: './close.page.html',
  styleUrls: ['./close.page.scss']
})
export class ClosePage extends BaseItemComponent<any> {
  pageInfo: PageInformationModel;
  constructor() {
    super();
    this.path = ROUTE_FUNCTION_GEN.CLOSE_DOCUMENT_RETURN;
  }
  onRelatedMenu(): void {
    const path: PathParamModel = {};
    this.toRelatedInfo(path);
  }
  toRelatedInfo(param: PathParamModel): void {
    super.toRelatedInfo(param);
  }
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.POST_DOCUMENT_RETURN,
      servicePath: 'DocumentReturnTable/Function/Closedocumentreturn'
    };
  }
}
