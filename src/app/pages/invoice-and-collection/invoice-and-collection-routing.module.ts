import { FreeTextInvoiceTableComponentModule } from 'components/free-text-invoice-table/free-text-invoice-table/free-text-invoice-table.module';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'inquiry',
    loadChildren: () => import('./inquiry/inquiry.module').then((m) => m.InquiryModule)
  },
  {
    path: 'messenger',
    loadChildren: () => import('./messenger/messenger.module').then((m) => m.MessengerModule)
  },
  {
    path: 'documentreturntable',
    loadChildren: () => import('./document-return-table/document-return-table.module').then((m) => m.DocumentReturnTableModule)
  },
  {
    path: 'receipttemptable',
    loadChildren: () => import('./receipt-temp-table/receipt-temp-table.module').then((m) => m.ReceiptTempTableModule)
  },
  { path: 'chequetable', loadChildren: () => import('./cheque-table/cheque-table.module').then((m) => m.CollectionChequeTableModule) },
  {
    path: 'refund',
    loadChildren: () => import('./refund/refund.module').then((m) => m.RefundModule)
  },
  {
    path: 'intercompanyinvoice',
    loadChildren: () => import('./intercompany/intercompany.module').then((m) => m.IntercompanyModule)
  },
  {
    path: 'freetextinvoicetable',
    loadChildren: () => import('./free-text-invoice-table/free-text-invoice-table.module').then((m) => m.FreeTextInvoiceTableModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoiceAndCollectionRoutingModule {}
