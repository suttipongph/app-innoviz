import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'suspenserefund',
    loadChildren: () => import('./customer-refund-table/cusotmer-refund-table.module').then((m) => m.CustomerRefundTableModule)
  },
  {
    path: 'reserverefund',
    loadChildren: () => import('./customer-refund-table/cusotmer-refund-table.module').then((m) => m.CustomerRefundTableModule)
  },
  {
    path: 'retentionrefund',
    loadChildren: () => import('./customer-refund-table/cusotmer-refund-table.module').then((m) => m.CustomerRefundTableModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RefundRoutingModule {}
