import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CustomerRefundTableItemPage } from './customer-refund-table-item/customer-refund-table-item.page';
import { CustomerRefundTableListPage } from './customer-refund-table-list/customer-refund-table-list.page';
import { SuspenseSettlementItemPage } from './suspense-settlement-item/suspense-settlement-item.page';

const routes: Routes = [
  {
    path: '',
    component: CustomerRefundTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CustomerRefundTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/suspensesettlement-child/:id',
    component: SuspenseSettlementItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/customer-refund-table-relatedinfo.module').then((m) => m.CustomerRefundTableRelatedinfoModule)
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/customer-refund-table-function.module').then((m) => m.CustomerRefundTableFunctionModule)
  },
  {
    path: ':id/report',
    loadChildren: () => import('./report/refund-report.module').then((m) => m.RefundReportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRefundTableRoutingModule {}
