import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerRefundTableAttachmentRelatedinfoRoutingModule } from './customer-refund-table-attachment-relatedinfo-routing.module';
import { AttachmentListPage } from './attachment-list/attachment-list.page';
import { AttachmentItemPage } from './attachment-item/attachment-item.page';
import { SharedModule } from 'shared/shared.module';
import { AttachmentComponentModule } from 'components/attachment/attachment.module';

@NgModule({
  declarations: [AttachmentListPage, AttachmentItemPage],
  imports: [CommonModule, SharedModule, CustomerRefundTableAttachmentRelatedinfoRoutingModule, AttachmentComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerRefundTableAttachmentRelatedinfoModule {}
