import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'invoicesettlement',
    loadChildren: () =>
      import('./invoice-settlement/suspense-refund-invoice-settlement-relatedinfo.module').then(
        (m) => m.SuspenseRefundInvoiceSettlementRelatedinfoModule
      )
  },
  {
    path: 'memotrans',
    loadChildren: () => import('./memo/suspense-refund-memo-relatedinfo.module').then((m) => m.ReceiptTempTableMemoRelatedinfoModule)
  },
  {
    path: 'vendorpaymenttrans',
    loadChildren: () => import('./vendor-payment-trans/vendor-payment-trans-relatedinfo.module').then((m) => m.VendorPaymentTransRelatedinfoModule)
  },
  {
    path: 'paymentdetail',
    loadChildren: () =>
      import('./payment-detail/customer-refund-table-payment-detail-relatedinfo.module').then(
        (m) => m.CustomerRefundTablePaymentDetailRelatedinfoModule
      )
  },
  {
    path: 'servicefeetrans',
    loadChildren: () =>
      import('./service-fee-trans/customer-refund-table-service-fee-trans-relatedinfo.module').then(
        (m) => m.CustomerRefundTableServiceFeeTransRelatedinfoModule
      )
  },
  {
    path: 'attachment',
    loadChildren: () => import('./attachment/customer-refund-table-attachment-relatedinfo.module').then((m) => m.CustomerRefundTableAttachmentRelatedinfoModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRefundTableRelatedinfoRoutingModule {}
