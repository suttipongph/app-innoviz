import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SendVendorInvoiceStagingPage } from './send-vendor-invoice.page';

describe('SendVendorInvoiceStagingPage', () => {
  let component: SendVendorInvoiceStagingPage;
  let fixture: ComponentFixture<SendVendorInvoiceStagingPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SendVendorInvoiceStagingPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SendVendorInvoiceStagingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
