import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { SendVendorInvoiceStagingPage } from './send-vendor-invoice/send-vendor-invoice.page';
import { CustomerRefundTableVendorPaymentTransSendVendorInvoiceStagingFunctionRoutingModule } from './send-vendor-invoice-staging-function-routing.module';
import { SendVendorInvoiceStagingComponentModule } from 'components/vendor-payment-trans/send-vendor-invoice-staging/send-vendor-invoice-staging.module';

@NgModule({
  declarations: [SendVendorInvoiceStagingPage],
  imports: [
    CommonModule,
    SharedModule,
    CustomerRefundTableVendorPaymentTransSendVendorInvoiceStagingFunctionRoutingModule,
    SendVendorInvoiceStagingComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerRefundTableVendorPaymentTransSendVendorInvoiceStagingFunctionModule {}
