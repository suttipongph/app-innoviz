import { ComponentFixture, TestBed } from '@angular/core/testing';
import { VendorPaymentTransItemPage } from './vendor-payment-trans-item.page';

describe('VendorPaymentItemPage', () => {
  let component: VendorPaymentTransItemPage;
  let fixture: ComponentFixture<VendorPaymentTransItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VendorPaymentTransItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorPaymentTransItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
