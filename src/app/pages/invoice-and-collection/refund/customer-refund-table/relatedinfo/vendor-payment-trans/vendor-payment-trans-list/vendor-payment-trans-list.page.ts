import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'vendor-payment-trans-list.page',
  templateUrl: './vendor-payment-trans-list.page.html',
  styleUrls: ['./vendor-payment-trans-list.page.scss']
})
export class VendorPaymentTransListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'vendorPaymentTransGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.VENDOR_PAYMENT_TRANS,
      servicePath: `CustomerRefundTable/RelatedInfo/${ROUTE_RELATED_GEN.VENDOR_PAYMENT_TRANS}`
    };
  }
}
