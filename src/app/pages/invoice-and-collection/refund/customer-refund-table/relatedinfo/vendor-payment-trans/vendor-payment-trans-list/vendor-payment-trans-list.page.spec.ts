import { ComponentFixture, TestBed } from '@angular/core/testing';
import { VendorPaymentTransListPage } from './vendor-payment-trans-list.page';

describe('VendorPaymentListPage', () => {
  let component: VendorPaymentTransListPage;
  let fixture: ComponentFixture<VendorPaymentTransListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VendorPaymentTransListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorPaymentTransListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
