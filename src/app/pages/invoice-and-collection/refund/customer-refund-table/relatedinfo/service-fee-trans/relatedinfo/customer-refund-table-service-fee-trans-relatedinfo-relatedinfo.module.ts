import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerRefundTableServiceFeeTransRelatedinfoRelatedInfoRoutingModule } from './customer-refund-table-service-fee-trans-relatedinfo-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, CustomerRefundTableServiceFeeTransRelatedinfoRelatedInfoRoutingModule]
})
export class CustomerRefundTableServiceFeeTransRelatedinfoRelatedInfoModule {}
