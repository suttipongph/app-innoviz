import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CustomerTransactionListPage } from './customer-transaction-list.page';

describe('CustomerTransactionListPage', () => {
  let component: CustomerTransactionListPage;
  let fixture: ComponentFixture<CustomerTransactionListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CustomerTransactionListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerTransactionListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
