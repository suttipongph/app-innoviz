import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'invoicetable',
    loadChildren: () =>
      import('./invoicetable/customer-refund-table-service-fee-trans-invoicetable-relatedinfo.module').then(
        (m) => m.CustomerRefundTableServiceFeeTransInvoiceTableRelatedinfoModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRefundTableServiceFeeTransRelatedinfoRelatedInfoRoutingModule {}
