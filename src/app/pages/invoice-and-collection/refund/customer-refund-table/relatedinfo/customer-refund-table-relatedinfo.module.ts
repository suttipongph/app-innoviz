import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerRefundTableRelatedinfoRoutingModule } from './customer-refund-table-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, CustomerRefundTableRelatedinfoRoutingModule]
})
export class CustomerRefundTableRelatedinfoModule {}
