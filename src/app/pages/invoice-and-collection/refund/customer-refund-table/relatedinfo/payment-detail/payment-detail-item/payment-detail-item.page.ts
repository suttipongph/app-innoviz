import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'payment-detail-item.page',
  templateUrl: './payment-detail-item.page.html',
  styleUrls: ['./payment-detail-item.page.scss']
})
export class PaymentDetailItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.PAYMENT_DETAIL,
      servicePath: `CustomerRefundTable/RelatedInfo/${ROUTE_RELATED_GEN.PAYMENT_DETAIL}`
    };
  }
}
