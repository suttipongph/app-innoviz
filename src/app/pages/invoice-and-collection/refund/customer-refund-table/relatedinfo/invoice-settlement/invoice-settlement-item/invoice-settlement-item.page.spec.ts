import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InvoiceSettlementItemPage } from './invoice-settlement-item.page';

describe('InvoiceSettlementItemPage', () => {
  let component: InvoiceSettlementItemPage;
  let fixture: ComponentFixture<InvoiceSettlementItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InvoiceSettlementItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceSettlementItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
