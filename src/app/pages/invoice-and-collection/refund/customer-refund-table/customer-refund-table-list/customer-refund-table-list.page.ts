import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, ROUTE_MASTER_GEN, SortType, SuspenseInvoiceType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';
const RESERVE_REFUND = 'reserverefund';
const RETENTION_REFUND = 'retentionrefund';
const SUSPENSE_REFUND = 'suspenserefund';

@Component({
  selector: 'customer-refund-table-list-page',
  templateUrl: './customer-refund-table-list.page.html',
  styleUrls: ['./customer-refund-table-list.page.scss']
})
export class CustomerRefundTableListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute(2);
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'customerRefundTableGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'suspenseInvoiceType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: SuspenseInvoiceType.None.toString()
      }
    ];
    this.option.columns = columns;
  }
  setPath(): void {
    switch (this.masterRoute) {
      case SUSPENSE_REFUND:
        this.setPathSuspenseRefund();
        break;
      case RESERVE_REFUND:
        this.setPathReserveRefund();
        break;
      case RETENTION_REFUND:
        this.setPathRetentionRefund();
        break;
    }
  }
  setPathSuspenseRefund(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.SUSPENSE_REFUND, servicePath: 'CustomerRefundTable' };
  }
  setPathReserveRefund(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.RESERVE_REFUND, servicePath: 'CustomerRefundTable' };
  }
  setPathRetentionRefund(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.RETENTION_REFUND, servicePath: 'CustomerRefundTable' };
  }
}
