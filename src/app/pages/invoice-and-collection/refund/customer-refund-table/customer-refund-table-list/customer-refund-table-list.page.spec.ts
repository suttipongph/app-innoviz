import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CustomerRefundTableListPage } from './customer-refund-table-list.page';

describe('MessengerJobTableListPage', () => {
  let component: CustomerRefundTableListPage;
  let fixture: ComponentFixture<CustomerRefundTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CustomerRefundTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerRefundTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
