import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CustomerRefundTableItemPage } from './customer-refund-table-item.page';

describe('MessengerJobTableItemPage', () => {
  let component: CustomerRefundTableItemPage;
  let fixture: ComponentFixture<CustomerRefundTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CustomerRefundTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerRefundTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
