import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';
const RESERVE_REFUND = 'reserverefund';
const RETENTION_REFUND = 'retentionrefund';
const SUSPENSE_REFUND = 'suspenserefund';

@Component({
  selector: 'customer-refund-table-item-page',
  templateUrl: './customer-refund-table-item.page.html',
  styleUrls: ['./customer-refund-table-item.page.scss']
})
export class CustomerRefundTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute(2);
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    switch (this.masterRoute) {
      case SUSPENSE_REFUND:
        this.setPathSuspenseRefund();
        break;
      case RESERVE_REFUND:
        this.setPathReserveRefund();
        break;
      case RETENTION_REFUND:
        this.setPathRetentionRefund();
        break;
    }
  }
  setPathSuspenseRefund(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.SUSPENSE_REFUND,
      servicePath: 'CustomerRefundTable',
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.SUSPENSE_SETTLEMENT_CHILD,
          servicePath: 'CustomerRefundTable'
        }
      ]
    };
  }
  setPathReserveRefund(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.RESERVE_REFUND,
      servicePath: 'CustomerRefundTable',
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.SUSPENSE_SETTLEMENT_CHILD,
          servicePath: 'CustomerRefundTable'
        }
      ]
    };
  }
  setPathRetentionRefund(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.RETENTION_REFUND,
      servicePath: 'CustomerRefundTable',
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.SUSPENSE_SETTLEMENT_CHILD,
          servicePath: 'CustomerRefundTable'
        }
      ]
    };
  }
}
