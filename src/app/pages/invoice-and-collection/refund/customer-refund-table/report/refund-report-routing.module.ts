import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'printcustomerrefundreserve',
    loadChildren: () => import('./print-reserve-refund/print-reserve-refund-report.module').then((m) => m.PrintReserveRefundReportModule)
  },
  {
    path: 'printcustomerrefundretention',
    loadChildren: () => import('./print-retention-refund/print-retention-refund-report.module').then((m) => m.PrintRetentionRefundReportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RefundReportRoutingModule {}
