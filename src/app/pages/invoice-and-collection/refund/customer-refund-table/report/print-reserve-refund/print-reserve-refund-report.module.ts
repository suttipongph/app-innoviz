import { PrintCustomerRefundReserveComponentModule } from 'components/customer-retund-table/print-customer-refund-reserve/print-customer-refund-reserve.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { PrintReserveRefundPage } from './print-reserve-refund/print-reserve-refund.page';
import { PrintReserveRefundRoutingModule } from './print-reserve-refund-report-routing.module';

@NgModule({
  declarations: [PrintReserveRefundPage],
  imports: [CommonModule, SharedModule, PrintReserveRefundRoutingModule, PrintCustomerRefundReserveComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PrintReserveRefundReportModule {}
