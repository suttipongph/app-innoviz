import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PrintReserveRefundPage } from './print-reserve-refund/print-reserve-refund.page';

const routes: Routes = [
  {
    path: '',
    component: PrintReserveRefundPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrintReserveRefundRoutingModule {}
