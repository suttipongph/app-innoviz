import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { RefundReportRoutingModule } from './refund-report-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, RefundReportRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RefundReportModule {}
