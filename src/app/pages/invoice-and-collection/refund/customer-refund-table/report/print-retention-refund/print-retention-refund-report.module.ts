import { PrintCustomerRefundRetentionComponentModule } from 'components/customer-retund-table/print-customer-refund-retention/print-customer-refund-retention.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { PrintRetentionRefundPage } from './print-retention-refund/print-retention-refund.page';
import { PrintRetentionRefundRoutingModule } from './print-retention-refund-report-routing.module';

@NgModule({
  declarations: [PrintRetentionRefundPage],
  imports: [CommonModule, SharedModule, PrintRetentionRefundRoutingModule, PrintCustomerRefundRetentionComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PrintRetentionRefundReportModule {}
