import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_REPORT_GEN } from 'shared/constants/constantGen';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'print-tax-invoice-original-page',
  templateUrl: './print-retention-refund.page.html',
  styleUrls: ['./print-retention-refund.page.scss']
})
export class PrintRetentionRefundPage implements OnInit {
  pageInfo: PageInformationModel;
  pageLabel: string;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_REPORT_GEN.PRINT_CUSTOMER_REFUND_RETENTION,
      servicePath: 'CustomerRefundTable/Report/printcustomerrefundretention'
    };
  }
}
