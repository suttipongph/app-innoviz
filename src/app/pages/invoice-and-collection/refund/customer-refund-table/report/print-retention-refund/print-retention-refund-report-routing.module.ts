import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PrintRetentionRefundPage } from './print-retention-refund/print-retention-refund.page';

const routes: Routes = [
  {
    path: '',
    component: PrintRetentionRefundPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrintRetentionRefundRoutingModule {}
