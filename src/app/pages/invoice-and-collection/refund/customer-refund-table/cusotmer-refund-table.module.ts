import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerRefundTableRoutingModule } from './customer-refund-table-routing.module';
import { CustomerRefundTableListPage } from './customer-refund-table-list/customer-refund-table-list.page';
import { CustomerRefundTableItemPage } from './customer-refund-table-item/customer-refund-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { CustomerRefundTableComponentModule } from 'components/customer-retund-table/customer-refund-table/customer-refund-table.module';
import { InvoiceSettlementDetailComponentModule } from 'components/invoice-settlement-detail/invoice-settlement-detail/invoice-settlement-detail.module';
import { SuspenseSettlementItemPage } from './suspense-settlement-item/suspense-settlement-item.page';

@NgModule({
  declarations: [CustomerRefundTableListPage, CustomerRefundTableItemPage, SuspenseSettlementItemPage],
  imports: [CommonModule, SharedModule, CustomerRefundTableRoutingModule, CustomerRefundTableComponentModule, InvoiceSettlementDetailComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerRefundTableModule {}
