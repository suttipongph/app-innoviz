import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'postcustomerrefund',
    loadChildren: () =>
      import('./post-customer-refund-table/post-customer-refund-table-function.module').then((m) => m.PostCustomerRefundTableFunctionModule)
  },
  {
    path: 'cancelcustomerrefund',
    loadChildren: () =>
      import('./cancel-customer-refund-table/cancel-customer-refund-table-function.module').then((m) => m.CancelCustomerRefundTableFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRefundTableFunctionRoutingModule {}
