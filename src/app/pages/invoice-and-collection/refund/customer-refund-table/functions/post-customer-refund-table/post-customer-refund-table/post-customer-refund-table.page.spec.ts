import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostCustomerRefundTablePage } from './post-customer-refund-table.page';

describe('PostCustomerRefundTablePage', () => {
  let component: PostCustomerRefundTablePage;
  let fixture: ComponentFixture<PostCustomerRefundTablePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PostCustomerRefundTablePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostCustomerRefundTablePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
