import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { PostCustomerRefundTableFunctionRoutingModule } from './post-customer-refund-table-function-routing.module';
import { PostCustomerRefundTablePage } from './post-customer-refund-table/post-customer-refund-table.page';
import { PostCustomerRefundComponentModule } from 'components/customer-retund-table/post-customer-refund/post-customer-refund.module';

@NgModule({
  declarations: [PostCustomerRefundTablePage],
  imports: [CommonModule, SharedModule, PostCustomerRefundTableFunctionRoutingModule, PostCustomerRefundComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PostCustomerRefundTableFunctionModule {}
