import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PostCustomerRefundTablePage } from './post-customer-refund-table/post-customer-refund-table.page';

const routes: Routes = [
  {
    path: '',
    component: PostCustomerRefundTablePage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostCustomerRefundTableFunctionRoutingModule {}
