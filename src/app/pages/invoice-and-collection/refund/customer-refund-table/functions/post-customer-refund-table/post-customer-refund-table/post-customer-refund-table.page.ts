import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'post-customer-refund-table-page',
  templateUrl: './post-customer-refund-table.page.html',
  styleUrls: ['./post-customer-refund-table.page.scss']
})
export class PostCustomerRefundTablePage implements OnInit {
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  pageInfo: PageInformationModel;
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.POST_CUSTOMER_REFUND,
      servicePath: `CustomerRefundTable/function/${ROUTE_FUNCTION_GEN.POST_CUSTOMER_REFUND}`
    };
  }
}
