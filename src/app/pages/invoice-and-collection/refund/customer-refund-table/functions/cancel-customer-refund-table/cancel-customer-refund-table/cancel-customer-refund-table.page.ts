import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'cancel-customer-refund-table-page',
  templateUrl: './cancel-customer-refund-table.page.html',
  styleUrls: ['./cancel-customer-refund-table.page.scss']
})
export class CancelCustomerRefundTablePage implements OnInit {
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  pageInfo: PageInformationModel;
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.CANCEL_CUSTOMER_REFUND,
      servicePath: `CustomerRefundTable/function/${ROUTE_FUNCTION_GEN.CANCEL_CUSTOMER_REFUND}`
    };
  }
}
