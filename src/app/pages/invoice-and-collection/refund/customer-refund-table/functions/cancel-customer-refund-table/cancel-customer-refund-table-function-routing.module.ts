import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CancelCustomerRefundTablePage } from './cancel-customer-refund-table/cancel-customer-refund-table.page';

const routes: Routes = [
  {
    path: '',
    component: CancelCustomerRefundTablePage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CancelCustomerRefundTableFunctionRoutingModule {}
