import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelCustomerRefundTablePage } from './cancel-customer-refund-table.page';

describe('CancelCustomerRefundTablePage', () => {
  let component: CancelCustomerRefundTablePage;
  let fixture: ComponentFixture<CancelCustomerRefundTablePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CancelCustomerRefundTablePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelCustomerRefundTablePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
