import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { CancelCustomerRefundTableFunctionRoutingModule } from './cancel-customer-refund-table-function-routing.module';
import { CancelCustomerRefundTablePage } from './cancel-customer-refund-table/cancel-customer-refund-table.page';
import { CancelCustomerRefundComponentModule } from 'components/customer-retund-table/cancel-customer-refund/cancel-customer-refund.module';

@NgModule({
  declarations: [CancelCustomerRefundTablePage],
  imports: [CommonModule, SharedModule, CancelCustomerRefundTableFunctionRoutingModule, CancelCustomerRefundComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CancelCustomerRefundTableFunctionModule {}
