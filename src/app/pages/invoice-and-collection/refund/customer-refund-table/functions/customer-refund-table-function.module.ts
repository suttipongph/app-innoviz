import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { CustomerRefundTableFunctionRoutingModule } from './customer-refund-table-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, CustomerRefundTableFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerRefundTableFunctionModule {}
