import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'invoice-line-item.page',
  templateUrl: './invoice-line-item.page.html',
  styleUrls: ['./invoice-line-item.page.scss']
})
export class InvoiceLineItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
      this.pageInfo = { pagePath: ROUTE_MASTER_GEN.INVOICELINE, servicePath: 'InvoiceTable' };
  }
}
