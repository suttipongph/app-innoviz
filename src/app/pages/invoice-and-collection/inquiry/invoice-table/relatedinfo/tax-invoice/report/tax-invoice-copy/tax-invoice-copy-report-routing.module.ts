import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PrintTaxInvoiceCopyPage } from './tax-invoice-copy/print-tax-invoice-copy.page';

const routes: Routes = [
  {
    path: '',
    component: PrintTaxInvoiceCopyPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrintTaxInvoiceCopyRoutingModule {}
