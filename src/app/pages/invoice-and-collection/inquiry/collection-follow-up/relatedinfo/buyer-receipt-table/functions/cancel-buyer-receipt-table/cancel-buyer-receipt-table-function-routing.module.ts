import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CancelBuyerReceiptTablePage } from './cancel-buyer-receipt-table/cancel-buyer-receipt-table.page';

const routes: Routes = [
  {
    path: '',
    component: CancelBuyerReceiptTablePage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CancelBuyerReceiptTableFunctionRoutingModule {}
