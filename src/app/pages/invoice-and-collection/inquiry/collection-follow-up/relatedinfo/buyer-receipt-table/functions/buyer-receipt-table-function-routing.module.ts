import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'cancelbuyerreceipttable',
    loadChildren: () =>
      import('./cancel-buyer-receipt-table/cancel-buyer-receipt-table-function.module').then((m) => m.CancelBuyerReceiptTableFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyerReceiptTableFunctionRoutingModule {}
