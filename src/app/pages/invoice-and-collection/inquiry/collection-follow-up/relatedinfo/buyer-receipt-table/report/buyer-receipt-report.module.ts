import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { BuyerReceiptReportRoutingModule } from './buyer-receipt-report-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, BuyerReceiptReportRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BuyerReceiptReportModule {}
