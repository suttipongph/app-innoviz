import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { BuyerReceiptTableFunctionRoutingModule } from './buyer-receipt-table-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, BuyerReceiptTableFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BuyerReceiptTableFunctionModule {}
