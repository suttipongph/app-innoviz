import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuyerReceiptTableComponentModule } from 'components/buyer-receipt-table/buyer-receipt-table/buyer-receipt-table.module';
import { BuyerReceiptTableRoutingModule } from './buyer-receipt-table-routing.module';
import { BuyerReceiptTableItemPage } from './buyer-receipt-item/buyer-receipt-item.page';
import { BuyerReceiptTableListPage } from './buyer-receipt-list/buyer-receipt-list.page';

@NgModule({
  declarations: [BuyerReceiptTableListPage, BuyerReceiptTableItemPage],
  imports: [CommonModule, BuyerReceiptTableRoutingModule, BuyerReceiptTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BuyerReceiptTableModule {}
