import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CollectionFllowUpItemPage } from './collection-follow-up-item/collection-follow-up-item.page';
import { CollectionFllowUpListPage } from './collection-follow-up-list/collection-follow-up-list.page';

const routes: Routes = [
  {
    path: '',
    component: CollectionFllowUpListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CollectionFllowUpItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/collection-follow-up-relatedinfo.module').then((m) => m.CollectionFollowUpRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CollectionFllowUpRoutingModule {}
