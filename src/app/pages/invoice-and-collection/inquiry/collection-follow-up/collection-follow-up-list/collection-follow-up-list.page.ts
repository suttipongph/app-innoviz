import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'collection-follow-up-list-page',
  templateUrl: './collection-follow-up-list.page.html',
  styleUrls: ['./collection-follow-up-list.page.scss']
})
export class CollectionFllowUpListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'CollectionFollowUpGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.INQUIRY_COLLECTION_FOLLOW_UP,
      servicePath: `collectionfollowup`
    };
  }
}
