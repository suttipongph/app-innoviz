import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { CollectionFllowUpRoutingModule } from './collection-follow-up-routing.module';
import { CollectionFollowUpComponentModule } from 'components/collection-follow-up/collection-follow-up/collection-follow-up.module';
import { CollectionFllowUpListPage } from './collection-follow-up-list/collection-follow-up-list.page';
import { CollectionFllowUpItemPage } from './collection-follow-up-item/collection-follow-up-item.page';

@NgModule({
  declarations: [CollectionFllowUpListPage, CollectionFllowUpItemPage],
  imports: [CommonModule, SharedModule, CollectionFllowUpRoutingModule, CollectionFollowUpComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CollectionFollowUpModule {}
