import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [
  { path: 'invoicetable', loadChildren: () => import('./invoice-table/invoice-table.module').then((m) => m.InvoiceTableModule) },
  {
    path: 'inquirypurchaselineoutstanding',
    loadChildren: () =>
      import('./inquiry-purchase-line-outstanding/inquiry-purchase-line-outstanding.module').then((m) => m.InquiryPurchaseLineOutstandingModule)
  },
  { path: 'receipttable', loadChildren: () => import('./receipt-table/receipt-table.module').then((m) => m.ReceiptTableModule) },
  {
    path: 'inquirywithdrawallineoutstand',
    loadChildren: () =>
      import('./inquiry-withdrawal-line-outstand/inquiry-withdrawal-line-outstand.module').then((m) => m.InquiryWithdrawalLineOutstandModule)
  },
  {
    path: 'paymenthistory',
    loadChildren: () => import('./payment-history/payment-history-relatedinfo.module').then((m) => m.PaymentHistoryRelatedinfoModule)
  },
  {
    path: 'collectionfollowup',
    loadChildren: () => import('./collection-follow-up/collection-follow-up.module').then((m) => m.CollectionFollowUpModule)
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InquiryRoutingModule {}
