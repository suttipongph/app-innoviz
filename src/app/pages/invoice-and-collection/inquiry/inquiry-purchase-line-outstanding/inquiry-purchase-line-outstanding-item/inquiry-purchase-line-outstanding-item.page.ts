import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'inquiry-purchase-line-outstanding-item.page',
  templateUrl: './inquiry-purchase-line-outstanding-item.page.html',
  styleUrls: ['./inquiry-purchase-line-outstanding-item.page.scss']
})
export class InquiryPurchaseLineOutstandingItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.INQUIRYPURCHASELINEOUTSTANDING,
      servicePath: 'InquiryPurchaseLineOutstanding'
    };
  }
}
