import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { InquiryPurchaseLineOutstandingItemPage } from './inquiry-purchase-line-outstanding-item/inquiry-purchase-line-outstanding-item.page';
import { InquiryPurchaseLineOutstandingListPage } from './inquiry-purchase-line-outstanding-list/inquiry-purchase-line-outstanding-list.page';

const routes: Routes = [
  {
    path: '',
    component: InquiryPurchaseLineOutstandingListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: InquiryPurchaseLineOutstandingItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () =>
      import('./relatedinfo/inquiry-purchase-line-outstanding-relatedinfo.module').then((m) => m.InquiryPurchaseLineOutstandingRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InquiryPurchaseLineOutstandingRoutingModule {}
