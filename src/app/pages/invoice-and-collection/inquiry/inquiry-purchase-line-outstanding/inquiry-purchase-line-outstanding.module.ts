import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InquiryPurchaseLineOutstandingRoutingModule } from './inquiry-purchase-line-outstanding-routing.module';
import { InquiryPurchaseLineOutstandingItemPage } from './inquiry-purchase-line-outstanding-item/inquiry-purchase-line-outstanding-item.page';
import { SharedModule } from 'shared/shared.module';
import { InquiryPurchaseLineOutstandingListPage } from './inquiry-purchase-line-outstanding-list/inquiry-purchase-line-outstanding-list.page';
import { InquiryPurchaseLineOutstandingComponentModule } from 'components/inquiry/inquiry-purchase-line-outstanding/inquiry-purchase-line-outstanding.module';

@NgModule({
  declarations: [InquiryPurchaseLineOutstandingListPage, InquiryPurchaseLineOutstandingItemPage],
  imports: [CommonModule, SharedModule, InquiryPurchaseLineOutstandingRoutingModule, InquiryPurchaseLineOutstandingComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InquiryPurchaseLineOutstandingModule {}
