import { ChequeTableComponentModule } from 'components/cheque-table/cheque-table/cheque-table.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PdcRelatedinfoRoutingModule } from './pdc-relatedinfo-routing.module';
import { PdcListPage } from './pdc-list/pdc-list.page';
import { PdcItemPage } from './pdc-item/pdc-item.page';
import { SharedModule } from 'shared/shared.module';

@NgModule({
  declarations: [PdcListPage, PdcItemPage],
  imports: [CommonModule, SharedModule, PdcRelatedinfoRoutingModule, ChequeTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PdcRelatedinfoModule {}
