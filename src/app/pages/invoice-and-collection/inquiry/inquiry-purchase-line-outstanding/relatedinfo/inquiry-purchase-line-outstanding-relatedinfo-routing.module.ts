import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'collectionfollowup',
    loadChildren: () =>
      import('./collection-follow-up/inquiry-purchase-line-outstanding-collection-follow-up.module').then(
        (m) => m.InquiryPurchaseLineOutstandingCollectionFollowUpModule
      )
  },
  {
    path: 'pdc',
    loadChildren: () => import('./pdc/pdc-relatedinfo.module').then((m) => m.PdcRelatedinfoModule)
  },
  {
    path: 'messengerrequest',
    loadChildren: () =>
      import('./messenger-request/purchase-line-messenger-request.module').then((m) => m.InquiryPurchaseLineOutstandMessengerRequestModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InquiryPurchaseLineOutstandingRelatedinfoRoutingModule {}
