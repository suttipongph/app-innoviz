import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InquiryPurchaseLineOutstandingRelatedinfoRoutingModule } from './inquiry-purchase-line-outstanding-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, InquiryPurchaseLineOutstandingRelatedinfoRoutingModule]
})
export class InquiryPurchaseLineOutstandingRelatedinfoModule {}
