import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'printtaxinvoicecopy',
    loadChildren: () => import('./tax-inv-copy/tax-inv-report-copy.module').then((m) => m.PrintTaxInvoiceCopyReportModule)
  },
  {
    path: 'printtaxinvoiceoriginal',
    loadChildren: () => import('./tax-inv-orig/tax-inv-orig-report.module').then((m) => m.PrintTaxInvoiceOriginalReportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaxInvoiceCopyReportRoutingModule {}
