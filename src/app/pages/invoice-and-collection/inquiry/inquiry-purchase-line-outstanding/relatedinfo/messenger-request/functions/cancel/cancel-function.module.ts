import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CancelFunctionRoutingModule } from './cancel-function-routing.module';
import { CancelPage } from './cancel/cancel.page';
import { SharedModule } from 'shared/shared.module';
import { CancelMessengerJobComponentModule } from 'components/messenger-job-table/messenger-job-table/cancel-messenger-job/cancel-messenger-job.module';

@NgModule({
  declarations: [CancelPage],
  imports: [CommonModule, SharedModule, CancelFunctionRoutingModule, CancelMessengerJobComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CancelFunctionModule {}
