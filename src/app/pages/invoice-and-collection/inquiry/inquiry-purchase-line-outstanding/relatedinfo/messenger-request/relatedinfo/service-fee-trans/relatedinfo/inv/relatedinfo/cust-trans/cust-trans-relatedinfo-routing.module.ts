import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerTransactionListPage } from './cust-trans-list/cust-trans-list.page';
import { CustomerTransactionItemPage } from './cust-trans-item/cust-trans-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: CustomerTransactionListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CustomerTransactionItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerTransactionRelatedinfoRoutingModule {}
