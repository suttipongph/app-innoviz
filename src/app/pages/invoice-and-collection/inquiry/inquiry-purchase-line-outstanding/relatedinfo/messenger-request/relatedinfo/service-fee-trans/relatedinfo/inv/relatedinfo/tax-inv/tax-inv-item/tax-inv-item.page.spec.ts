import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TaxInvoiceItemPage } from './tax-inv-item.page';

describe('TaxInvoiceItemPage', () => {
  let component: TaxInvoiceItemPage;
  let fixture: ComponentFixture<TaxInvoiceItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TaxInvoiceItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxInvoiceItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
