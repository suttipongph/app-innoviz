import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, RefType, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'purchase-line-messenger-request-list-page',
  templateUrl: './purchase-line-messenger-request-list.page.html',
  styleUrls: ['./purchase-line-messenger-request-list.page.scss']
})
export class PurchaseLineMessengerRequestListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  parentId;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'messengerJobTableGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'refType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: RefType.PurchaseLine
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.MESSENGER_REQUEST,
      servicePath: `InquiryPurchaseLineOutstanding/RelatedInfo/${ROUTE_RELATED_GEN.MESSENGER_REQUEST}`
    };
  }
}
