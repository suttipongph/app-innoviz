import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PurchaseLineMessengerRequestListPage } from './purchase-line-messenger-request-list.page';

describe(' PurchaseLineMessengerRequestListPage', () => {
  let component: PurchaseLineMessengerRequestListPage;
  let fixture: ComponentFixture<PurchaseLineMessengerRequestListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PurchaseLineMessengerRequestListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseLineMessengerRequestListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
