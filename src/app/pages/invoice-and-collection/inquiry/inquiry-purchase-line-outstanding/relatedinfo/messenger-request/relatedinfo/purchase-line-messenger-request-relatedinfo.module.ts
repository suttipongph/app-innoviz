import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { InquiryPurchaseLineOutstandMessengerRequestRelatedInfoRoutingModule } from './purchase-line-messenger-request-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, InquiryPurchaseLineOutstandMessengerRequestRelatedInfoRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InquiryPurchaseLineOutstandMessengerRequestRelatedInfoModule {}
