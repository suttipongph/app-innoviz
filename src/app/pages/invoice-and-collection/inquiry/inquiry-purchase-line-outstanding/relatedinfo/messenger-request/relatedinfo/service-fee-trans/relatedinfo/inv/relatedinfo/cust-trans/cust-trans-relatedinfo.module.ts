import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerTransactionRelatedinfoRoutingModule } from './cust-trans-relatedinfo-routing.module';
import { CustomerTransactionListPage } from './cust-trans-list/cust-trans-list.page';
import { CustomerTransactionItemPage } from './cust-trans-item/cust-trans-item.page';
import { SharedModule } from 'shared/shared.module';
import { CustTransComponentModule } from 'components/cust-trans/cust-trans.module';

@NgModule({
  declarations: [CustomerTransactionListPage, CustomerTransactionItemPage],
  imports: [CommonModule, SharedModule, CustomerTransactionRelatedinfoRoutingModule, CustTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerTransactionRelatedinfoModule {}
