import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'cust-trans-item.page',
  templateUrl: './cust-trans-item.page.html',
  styleUrls: ['./cust-trans-item.page.scss']
})
export class CustomerTransactionItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.CUSTOMER_TRANSACTION, servicePath: 'CustomerTable/RelatedInfo/CustTransaction' };
  }
}
