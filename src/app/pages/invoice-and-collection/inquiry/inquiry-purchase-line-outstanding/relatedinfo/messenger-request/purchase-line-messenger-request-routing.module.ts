import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PurchaseLineMessengerRequestItemPage } from './purchase-line-messenger-request-item/purchase-line-messenger-request-item.page';
import { PurchaseLineMessengerRequestListPage } from './purchase-line-messenger-request-list/purchase-line-messenger-request-list.page';

const routes: Routes = [
  {
    path: '',
    component: PurchaseLineMessengerRequestListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: PurchaseLineMessengerRequestItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () =>
      import('./relatedinfo/purchase-line-messenger-request-relatedinfo.module').then(
        (m) => m.InquiryPurchaseLineOutstandMessengerRequestRelatedInfoModule
      )
  },
  {
    path: ':id/function',
    loadChildren: () =>
      import('./functions/purchase-line-messenger-request-function.module').then((m) => m.InquiryPurchaseLineOutstandMessengerRequestFunctionModule)
  },
  {
    path: ':id/report',
    loadChildren: () => import('./report/print-messenger-job-print-report.module').then((m) => m.MessengerJobTablePrintMessengerJobReportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InquiryPurchaseLineOutstandMessengerRequestRoutingModule {}
