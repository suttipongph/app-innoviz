import { MessengerJobTableComponentModule } from 'components/messenger-job-table/messenger-job-table/messenger-job-table.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { InquiryPurchaseLineOutstandMessengerRequestRoutingModule } from './purchase-line-messenger-request-routing.module';
import { PurchaseLineMessengerRequestItemPage } from './purchase-line-messenger-request-item/purchase-line-messenger-request-item.page';
import { PurchaseLineMessengerRequestListPage } from './purchase-line-messenger-request-list/purchase-line-messenger-request-list.page';

@NgModule({
  declarations: [PurchaseLineMessengerRequestListPage, PurchaseLineMessengerRequestItemPage],
  imports: [CommonModule, SharedModule, InquiryPurchaseLineOutstandMessengerRequestRoutingModule, MessengerJobTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InquiryPurchaseLineOutstandMessengerRequestModule {}
