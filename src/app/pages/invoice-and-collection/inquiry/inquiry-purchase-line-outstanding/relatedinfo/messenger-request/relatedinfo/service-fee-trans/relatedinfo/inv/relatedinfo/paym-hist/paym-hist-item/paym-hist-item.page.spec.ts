import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PaymentHistoryItemPage } from './paym-hist-item.page';

describe('PaymentHistoryItemPage', () => {
  let component: PaymentHistoryItemPage;
  let fixture: ComponentFixture<PaymentHistoryItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PaymentHistoryItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentHistoryItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
