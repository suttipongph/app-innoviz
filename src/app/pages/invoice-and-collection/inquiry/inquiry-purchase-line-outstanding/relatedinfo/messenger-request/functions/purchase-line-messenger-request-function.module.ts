import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InquiryPurchaseLineOutstandMessengerRequestFunctionRoutingModule } from './purchase-line-messenger-request-function-routing.module';
import { SharedModule } from 'shared/shared.module';
import { MessengerJobTableComponentModule } from 'components/messenger-job-table/messenger-job-table/messenger-job-table.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, InquiryPurchaseLineOutstandMessengerRequestFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InquiryPurchaseLineOutstandMessengerRequestFunctionModule {}
