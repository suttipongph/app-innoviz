import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaxInvoiceRelatedinfoRoutingModule } from './tax-inv-relatedinfo-routing.module';
import { TaxInvoiceListPage } from './tax-inv-list/tax-inv-list.page';
import { TaxInvoiceItemPage } from './tax-inv-item/tax-inv-item.page';
import { SharedModule } from 'shared/shared.module';
import { TaxInvoiceTableComponentModule } from 'components/tax-invoice/tax-invoice-table/tax-invoice-table.module';
import { TaxInvoiceLineComponentModule } from 'components/tax-invoice/tax-invoice-line/tax-invoice-line.module';
import { TaxInvoiceLineItemPage } from './tax-inv-line-item/tax-inv-line-item.page';

@NgModule({
  declarations: [TaxInvoiceListPage, TaxInvoiceItemPage, TaxInvoiceLineItemPage],
  imports: [CommonModule, SharedModule, TaxInvoiceRelatedinfoRoutingModule, TaxInvoiceTableComponentModule, TaxInvoiceLineComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TaxInvoiceRelatedinfoModule {}
