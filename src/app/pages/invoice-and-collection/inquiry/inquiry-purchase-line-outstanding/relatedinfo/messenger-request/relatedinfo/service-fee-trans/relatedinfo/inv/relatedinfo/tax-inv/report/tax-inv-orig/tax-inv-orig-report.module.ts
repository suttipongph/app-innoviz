import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { PrintTaxInvoiceOriginalPage } from './tax-inv-orig/print-tax-inv-orig.page';
import { PrintTaxInvoiceOriginalRoutingModule } from './tax-inv-orig-report-routing.module';
import { PrintTaxInvoiceComponentModule } from 'components/tax-invoice/print-tax-invoice/print-tax-invoice.module';

@NgModule({
  declarations: [PrintTaxInvoiceOriginalPage],
  imports: [CommonModule, SharedModule, PrintTaxInvoiceOriginalRoutingModule, PrintTaxInvoiceComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PrintTaxInvoiceOriginalReportModule {}
