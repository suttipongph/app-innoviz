import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: 'jobchequetable',
    loadChildren: () => import('./cheque/cheque-relatedinfo.module').then((m) => m.ChequeRelatedinfoModule)
  },
  {
    path: 'servicefeetrans',
    loadChildren: () =>
      import('./service-fee-trans/messenger-job-table-service-fee-trans-relatedinfo.module').then(
        (m) => m.MessengerJobTableServiceFeeTransRelatedinfoModule
      )
  },
  {
    path: 'attachment',
    loadChildren: () => import('./attachment/messenger-job-table-attachment.module').then((m) => m.MessengerJobTableAttachmentModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InquiryPurchaseLineOutstandMessengerRequestRelatedInfoRoutingModule {}
