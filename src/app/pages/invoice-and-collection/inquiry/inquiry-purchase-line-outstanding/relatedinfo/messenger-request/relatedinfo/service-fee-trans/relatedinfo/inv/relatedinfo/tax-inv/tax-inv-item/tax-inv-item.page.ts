import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'tax-inv-item.page',
  templateUrl: './tax-inv-item.page.html',
  styleUrls: ['./tax-inv-item.page.scss']
})
export class TaxInvoiceItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.TAX_INVOICE,
      servicePath: 'InvoiceTable/RelatedInfo/TaxInvoice',
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.TAX_INVOICE_LINE,
          servicePath: 'InvoiceTable/RelatedInfo/TaxInvoice'
        }
      ]
    };
  }
}
