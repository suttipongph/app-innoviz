import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoicePaymentHistoryRelatedinfoRoutingModule } from './paym-hist-relatedinfo-routing.module';
import { PaymentHistoryListPage } from './paym-hist-list/paym-hist-list.page';
import { PaymentHistoryItemPage } from './paym-hist-item/paym-hist-item.page';
import { SharedModule } from 'shared/shared.module';
import { PaymentHistoryComponentModule } from 'components/payment-history/payment-history/payment-history.module';

@NgModule({
  declarations: [PaymentHistoryListPage, PaymentHistoryItemPage],
  imports: [CommonModule, SharedModule, InvoicePaymentHistoryRelatedinfoRoutingModule, PaymentHistoryComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InvoicePaymentHistoryRelatedinfoModule {}
