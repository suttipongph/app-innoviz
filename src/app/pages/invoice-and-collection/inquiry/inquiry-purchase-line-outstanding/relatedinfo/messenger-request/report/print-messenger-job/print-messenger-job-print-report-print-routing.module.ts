import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PrintMessengerJobPage } from './print-messenger-job/print-messenger-job.page';

const routes: Routes = [
  {
    path: '',
    component: PrintMessengerJobPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrintMessengerJobReportRoutingModule {}
