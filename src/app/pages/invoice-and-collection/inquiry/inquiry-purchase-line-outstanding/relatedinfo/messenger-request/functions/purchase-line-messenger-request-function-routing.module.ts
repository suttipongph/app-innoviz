import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: 'assignmessengerjobtable',
    loadChildren: () => import('./assign/assign-function.module').then((m) => m.AssignFunctionModule)
  },
  {
    path: 'cancelmessengerjobtable',
    loadChildren: () => import('./cancel/cancel-function.module').then((m) => m.CancelFunctionModule)
  },
  {
    path: 'postmessengerjobtable',
    loadChildren: () => import('./post/post-function.module').then((m) => m.PostFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InquiryPurchaseLineOutstandMessengerRequestFunctionRoutingModule {}
