import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PurchaseLineMessengerRequestItemPage } from './purchase-line-messenger-request-item.page';

describe('CollectionFllowUpItemPage', () => {
  let component: PurchaseLineMessengerRequestItemPage;
  let fixture: ComponentFixture<PurchaseLineMessengerRequestItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PurchaseLineMessengerRequestItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseLineMessengerRequestItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
