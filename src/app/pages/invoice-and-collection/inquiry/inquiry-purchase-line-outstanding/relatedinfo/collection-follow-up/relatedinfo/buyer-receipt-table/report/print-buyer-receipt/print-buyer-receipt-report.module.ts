import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { PrintBuyerReceiptComponentModule } from 'components/buyer-receipt-table/print-buyer-receipt/print-buyer-receipt.module';
import { PrintBuyerReceiptPage } from './print-buyer-receipt/print-buyer-receipt.page';
import { PrintBuyerReceiptReportRoutingModule } from './print-buyer-receipt-report-routing.module';

@NgModule({
  declarations: [PrintBuyerReceiptPage],
  imports: [CommonModule, SharedModule, PrintBuyerReceiptReportRoutingModule, PrintBuyerReceiptComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PrintBuyerReceiptReportModule {}
