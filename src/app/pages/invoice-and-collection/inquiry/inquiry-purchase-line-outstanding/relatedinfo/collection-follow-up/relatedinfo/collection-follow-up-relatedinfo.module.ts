import { CommonModule } from '@angular/common';
import { CollectionFollowUpRelatedinfoRoutingModule } from './collection-follow-up-relatedinfo-routing.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
@NgModule({
  declarations: [],
  imports: [CommonModule, CollectionFollowUpRelatedinfoRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CollectionFollowUpRelatedinfoModule {}
