import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { CancelBuyerReceiptTableFunctionRoutingModule } from './cancel-buyer-receipt-table-function-routing.module';
import { CancelBuyerReceiptTableComponentModule } from 'components/buyer-receipt-table/cancel-buyer-receipt-table/cancel-buyer-receipt-table.module';
import { CancelBuyerReceiptTablePage } from './cancel-buyer-receipt-table/cancel-buyer-receipt-table.page';

@NgModule({
  declarations: [CancelBuyerReceiptTablePage],
  imports: [CommonModule, SharedModule, CancelBuyerReceiptTableFunctionRoutingModule, CancelBuyerReceiptTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CancelBuyerReceiptTableFunctionModule {}
