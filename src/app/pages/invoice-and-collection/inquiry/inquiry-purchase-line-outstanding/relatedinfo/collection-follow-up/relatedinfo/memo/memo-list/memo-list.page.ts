import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, EmptyGuid, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel, RowIdentity } from 'shared/models/systemModel';

@Component({
  selector: 'app-memo-list',
  templateUrl: './memo-list.page.html',
  styleUrls: ['./memo-list.page.scss']
})
export class MemoListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  parentId = this.uiService.getRelatedInfoParentTableKey();
  currentRoute = this.uiService.getMasterRoute(1);

  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
    this.setOption();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'memoTransGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.MEMO,
      servicePath: `InquiryPurchaseLineOutstanding/RelatedInfo/CollectionFollowUp/RelatedInfo/MemoTrans`
    };
  }
}
