import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CollectionFllowUpItemPage } from './collection-follow-up-item.page';

describe('CollectionFllowUpItemPage', () => {
  let component: CollectionFllowUpItemPage;
  let fixture: ComponentFixture<CollectionFllowUpItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CollectionFllowUpItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectionFllowUpItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
