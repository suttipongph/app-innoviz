import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { BuyerReceiptTableItemPage } from './buyer-receipt-item/buyer-receipt-item.page';
import { BuyerReceiptTableListPage } from './buyer-receipt-list/buyer-receipt-list.page';

const routes: Routes = [
  {
    path: '',
    component: BuyerReceiptTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BuyerReceiptTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/buyer-receipt-table-function.module').then((m) => m.BuyerReceiptTableFunctionModule)
  },
  {
    path: ':id/report',
    loadChildren: () => import('./report/buyer-receipt-report.module').then((m) => m.BuyerReceiptReportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyerReceiptTableRoutingModule {}
