import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN, ROUTE_REPORT_GEN } from 'shared/constants/constantGen';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'print-purchase-page',
  templateUrl: './print-buyer-receipt.page.html',
  styleUrls: ['./print-buyer-receipt.page.scss']
})
export class PrintBuyerReceiptPage implements OnInit {
  pageInfo: PageInformationModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_REPORT_GEN.PRINT_BUYER_RECEIPT,
      servicePath: `InquiryPurchaseLineOutstanding/RelatedInfo/${ROUTE_RELATED_GEN.COLLECTION_FOLLOW_UP}/RelatedInfo/${ROUTE_RELATED_GEN.BUYER_RECEIPT_TABLE}/Report/PrintBuyerReceipt`
    };
  }
}
