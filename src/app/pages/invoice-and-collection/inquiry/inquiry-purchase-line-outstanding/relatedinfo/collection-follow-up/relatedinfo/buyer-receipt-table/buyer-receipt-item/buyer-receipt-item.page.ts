import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'collection-follow-up-item.page',
  templateUrl: './buyer-receipt-item.page.html',
  styleUrls: ['./buyer-receipt-item.page.scss']
})
export class BuyerReceiptTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.BUYER_RECEIPT_TABLE,
      servicePath: `InquiryPurchaseLineOutstanding/RelatedInfo/${ROUTE_RELATED_GEN.COLLECTION_FOLLOW_UP}/RelatedInfo/${ROUTE_RELATED_GEN.BUYER_RECEIPT_TABLE}`
    };
  }
}
