import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BuyerReceiptTableListPage } from './buyer-receipt-list.page';

describe('CollectionFllowUpListPage', () => {
  let component: BuyerReceiptTableListPage;
  let fixture: ComponentFixture<BuyerReceiptTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BuyerReceiptTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerReceiptTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
