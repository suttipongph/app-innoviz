import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel, PathParamModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-memo-item',
  templateUrl: './memo-item.page.html',
  styleUrls: ['./memo-item.page.scss']
})
export class MemoItemPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  parentId = this.uiService.getRelatedInfoParentTableKey();

  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
    this.setOption();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'MemoTransGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.MEMO,
      servicePath: `InquiryPurchaseLineOutstanding/RelatedInfo/CollectionFollowUp/RelatedInfo/MemoTrans`
    };
  }
}
