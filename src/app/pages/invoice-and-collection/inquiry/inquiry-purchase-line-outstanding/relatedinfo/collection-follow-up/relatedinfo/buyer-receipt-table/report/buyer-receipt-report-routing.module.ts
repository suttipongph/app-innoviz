import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'printbuyerreceipt',
    loadChildren: () => import('./print-buyer-receipt/print-buyer-receipt-report.module').then((m) => m.PrintBuyerReceiptReportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyerReceiptReportRoutingModule {}
