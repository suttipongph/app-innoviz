import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'inquiry-purchase-line-outstanding-list-page',
  templateUrl: './inquiry-purchase-line-outstanding-list.page.html',
  styleUrls: ['./inquiry-purchase-line-outstanding-list.page.scss']
})
export class InquiryPurchaseLineOutstandingListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'purchaseLineGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.INQUIRYPURCHASELINEOUTSTANDING, servicePath: 'InquiryPurchaseLineOutstanding' };
  }
}
