import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InquiryWithdrawalLineOutstandRoutingModule } from './inquiry-withdrawal-line-outstand-routing.module';
import { InquiryWithdrawalLineOutstandItemPage } from './inquiry-withdrawal-line-outstand-item/inquiry-withdrawal-line-outstand-item.page';
import { SharedModule } from 'shared/shared.module';
import { InquiryWithdrawalLineOutstandListPage } from './inquiry-withdrawal-line-outstand-list/inquiry-withdrawal-line-outstand-list.page';
import { InquiryWithdrawalLineOutstandComponentModule } from 'components/inquiry/inquiry-withdrawal-line-outstand/inquiry-withdrawal-line-outstand.module';

@NgModule({
  declarations: [InquiryWithdrawalLineOutstandListPage, InquiryWithdrawalLineOutstandItemPage],
  imports: [CommonModule, SharedModule, InquiryWithdrawalLineOutstandRoutingModule, InquiryWithdrawalLineOutstandComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InquiryWithdrawalLineOutstandModule {}
