import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'inquiry-withdrawal-line-outstand-item.page',
  templateUrl: './inquiry-withdrawal-line-outstand-item.page.html',
  styleUrls: ['./inquiry-withdrawal-line-outstand-item.page.scss']
})
export class InquiryWithdrawalLineOutstandItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.INQUIRYWITHDRAWALLINEOUTSTAND,
      servicePath: 'InquiryWithdrawalLineOutstand'
    };
  }
}
