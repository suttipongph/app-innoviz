import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { InquiryWithdrawalLineOutstandItemPage } from './inquiry-withdrawal-line-outstand-item/inquiry-withdrawal-line-outstand-item.page';
import { InquiryWithdrawalLineOutstandListPage } from './inquiry-withdrawal-line-outstand-list/inquiry-withdrawal-line-outstand-list.page';

const routes: Routes = [
  {
    path: '',
    component: InquiryWithdrawalLineOutstandListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: InquiryWithdrawalLineOutstandItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () =>
      import('./relatedinfo/inquiry-withdrawal-line-outstand-relatedinfo.module').then((m) => m.InquiryWithdrawalLineOutstandRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InquiryWithdrawalLineOutstandRoutingModule {}
