import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InquiryWithdrawalLineOutstandRelatedinfoRoutingModule } from './inquiry-withdrawal-line-outstand-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, InquiryWithdrawalLineOutstandRelatedinfoRoutingModule]
})
export class InquiryWithdrawalLineOutstandRelatedinfoModule {}
