import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PdcListPage } from './pdc-list.page';

describe('PdcListPage', () => {
  let component: PdcListPage;
  let fixture: ComponentFixture<PdcListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PdcListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PdcListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
