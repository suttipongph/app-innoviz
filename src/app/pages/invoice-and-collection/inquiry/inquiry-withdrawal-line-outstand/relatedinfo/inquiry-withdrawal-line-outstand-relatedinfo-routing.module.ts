import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'collectionfollowup',
    loadChildren: () =>
      import('./collection-follow-up/inquiry-withdrawal-line-outstand-collection-follow-up.module').then(
        (m) => m.InquiryWithdrawalLineOutstandCollectionFllowUpModule
      )
  },
  {
    path: 'pdc',
    loadChildren: () => import('./pdc/withdrawal-line-pdc-relatedinfo.module').then((m) => m.WithdrawalLinePdcRelatedinfoModule)
  },
  {
    path: 'messengerrequest',
    loadChildren: () =>
      import('./messenger-request/inquiry-withdrawal-line-outstand-messenger-request.module').then(
        (m) => m.InquiryWithdrawalLineOutstandMessengerRequestModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InquiryWithdrawalLineOutstandRelatedinfoRoutingModule {}
