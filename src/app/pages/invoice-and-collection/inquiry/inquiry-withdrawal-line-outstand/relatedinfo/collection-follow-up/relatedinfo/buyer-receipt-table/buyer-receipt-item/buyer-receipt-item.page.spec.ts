import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BuyerReceiptTableItemPage } from './buyer-receipt-item.page';

describe('CollectionFllowUpItemPage', () => {
  let component: BuyerReceiptTableItemPage;
  let fixture: ComponentFixture<BuyerReceiptTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BuyerReceiptTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerReceiptTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
