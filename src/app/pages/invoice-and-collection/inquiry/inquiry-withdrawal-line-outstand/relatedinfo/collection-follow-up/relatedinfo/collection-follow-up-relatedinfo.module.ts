import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CollectionFollowUpRelatedinfoRoutingModule } from './collection-follow-up-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, CollectionFollowUpRelatedinfoRoutingModule]
})
export class CollectionFollowUpMemoTransRelatedinfoModule {}
