import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CollectionFllowUpListPage } from './collection-follow-up-list/collection-follow-up-list.page';
import { CollectionFllowUpItemPage } from './collection-follow-up-item/collection-follow-up-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: CollectionFllowUpListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CollectionFllowUpItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/collection-follow-up-relatedinfo.module').then((m) => m.CollectionFollowUpMemoTransRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InquiryWithdrawalLineOutstandCollectionFllowUpRoutingModule {}
