import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CollectionFllowUpListPage } from './collection-follow-up-list.page';

describe('CollectionFllowUpListPage', () => {
  let component: CollectionFllowUpListPage;
  let fixture: ComponentFixture<CollectionFllowUpListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CollectionFllowUpListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectionFllowUpListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
