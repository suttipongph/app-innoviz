import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'collection-follow-up-list-page',
  templateUrl: './buyer-receipt-list.page.html',
  styleUrls: ['./buyer-receipt-list.page.scss']
})
export class BuyerReceiptTableListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'buyerReceiptTableGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.BUYER_RECEIPT_TABLE,
      servicePath: `InquiryWithdrawalLineOutstand/RelatedInfo/${ROUTE_RELATED_GEN.COLLECTION_FOLLOW_UP}/RelatedInfo/${ROUTE_RELATED_GEN.BUYER_RECEIPT_TABLE}`
    };
  }
}
