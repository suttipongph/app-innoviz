import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'cancel-buyer-receipt-table-page',
  templateUrl: './cancel-buyer-receipt-table.page.html',
  styleUrls: ['./cancel-buyer-receipt-table.page.scss']
})
export class CancelBuyerReceiptTablePage implements OnInit {
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  pageInfo: PageInformationModel;
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.CANCEL_BUYER_RECEIPT_TABLE,
      servicePath: `InquiryWithdrawalLineOutstand/RelatedInfo/${ROUTE_RELATED_GEN.COLLECTION_FOLLOW_UP}/RelatedInfo/${ROUTE_RELATED_GEN.BUYER_RECEIPT_TABLE}/function/${ROUTE_FUNCTION_GEN.CANCEL_BUYER_RECEIPT_TABLE}`
    };
  }
}
