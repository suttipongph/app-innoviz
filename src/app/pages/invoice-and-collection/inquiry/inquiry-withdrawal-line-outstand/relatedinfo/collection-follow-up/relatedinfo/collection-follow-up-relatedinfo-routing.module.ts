import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'memotrans',
    loadChildren: () =>
      import('./memo/collection-follow-up-memo-trans-relatedinfo.module').then((m) => m.CollectionFollowUpMemoTransRelatedinfoModule)
  },
  {
    path: 'attachment',
    loadChildren: () => import('./attachment/attachment.module').then((m) => m.AttachmentModule)
  },
  {
    path: 'buyerreceipttable',
    loadChildren: () => import('./buyer-receipt-table/buyer-receipt-table.module').then((m) => m.BuyerReceiptTableModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CollectionFollowUpRelatedinfoRoutingModule {}
