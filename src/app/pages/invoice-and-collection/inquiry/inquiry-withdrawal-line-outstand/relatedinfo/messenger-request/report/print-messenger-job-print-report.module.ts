import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { MessengerJobTablePrintMessengerJobReportRoutingModule } from './print-messenger-job-print-report-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, MessengerJobTablePrintMessengerJobReportRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MessengerJobTablePrintMessengerJobReportModule {}
