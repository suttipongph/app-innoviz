import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';

import { PrintMessengerJobReportRoutingModule } from './print-messenger-job-print-report-print-routing.module';
import { PrintMessengerJobPage } from './print-messenger-job/print-messenger-job.page';
import { PrintMessengerJobComponentModule } from 'components/messenger-job-table/messenger-job-table/print-messenger-job/print-messenger-job.module';

@NgModule({
  declarations: [PrintMessengerJobPage],
  imports: [CommonModule, SharedModule, PrintMessengerJobReportRoutingModule, PrintMessengerJobComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PrintMessengerJobReportModule {}
