import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MessengerRequestItemPage } from './messenger-request-item.page';

describe('CollectionFllowUpItemPage', () => {
  let component: MessengerRequestItemPage;
  let fixture: ComponentFixture<MessengerRequestItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MessengerRequestItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MessengerRequestItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
