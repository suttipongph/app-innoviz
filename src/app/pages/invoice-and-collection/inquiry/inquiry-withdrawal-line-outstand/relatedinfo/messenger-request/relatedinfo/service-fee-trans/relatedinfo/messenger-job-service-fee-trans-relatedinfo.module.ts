import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessengerJobServiceFeeTransRelatedinfoRelatedinfoRoutingModule } from './messenger-job-service-fee-trans-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, MessengerJobServiceFeeTransRelatedinfoRelatedinfoRoutingModule]
})
export class MessengerJobServiceFeeTransRelatedinfoRelatedinfoModule {}
