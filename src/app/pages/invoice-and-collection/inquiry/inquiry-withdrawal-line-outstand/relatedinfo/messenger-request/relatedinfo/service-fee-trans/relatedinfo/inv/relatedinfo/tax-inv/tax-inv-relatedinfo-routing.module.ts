import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TaxInvoiceListPage } from './tax-inv-list/tax-inv-list.page';
import { TaxInvoiceItemPage } from './tax-inv-item/tax-inv-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { TaxInvoiceLineItemPage } from './tax-inv-line-item/tax-inv-line-item.page';

const routes: Routes = [
  {
    path: '',
    component: TaxInvoiceListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: TaxInvoiceItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/taxinvoiceline-child/:id',
    component: TaxInvoiceLineItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/report',
    loadChildren: () => import('./report/tax-inv-report.module').then((m) => m.TaxInvoiceCopyReportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaxInvoiceRelatedinfoRoutingModule {}
