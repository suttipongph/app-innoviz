import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'invoice-line-item.page',
  templateUrl: './invoice-line-item.page.html',
  styleUrls: ['./invoice-line-item.page.scss']
})
export class InvoiceLineItemPage implements OnInit {
  pageInfo: PageInformationModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.INVOICE_LINE,
      servicePath: `InquiryWithdrawalLineOutstand/RelatedInfo/${ROUTE_RELATED_GEN.MESSENGER_REQUEST}/RelatedInfo/${ROUTE_RELATED_GEN.SERVICE_FEE_TRANS}/RelatedInfo/${ROUTE_RELATED_GEN.INVOICE_TABLE}`
    };
  }
}
