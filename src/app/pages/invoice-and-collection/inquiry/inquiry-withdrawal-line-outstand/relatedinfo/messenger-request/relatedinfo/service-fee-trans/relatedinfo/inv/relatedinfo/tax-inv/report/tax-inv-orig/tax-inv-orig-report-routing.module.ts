import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PrintTaxInvoiceOriginalPage } from './tax-inv-orig/print-tax-inv-orig.page';

const routes: Routes = [
  {
    path: '',
    component: PrintTaxInvoiceOriginalPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrintTaxInvoiceOriginalRoutingModule {}
