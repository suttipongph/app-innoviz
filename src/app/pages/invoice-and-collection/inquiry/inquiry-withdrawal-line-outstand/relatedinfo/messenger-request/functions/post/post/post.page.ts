import { Component } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { ROUTE_FUNCTION_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PathParamModel } from 'shared/models/systemModel';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';

@Component({
  selector: 'app-post',
  templateUrl: './post.page.html',
  styleUrls: ['./post.page.scss']
})
export class PostPage extends BaseItemComponent<any> {
  pageInfo: PageInformationModel;
  constructor() {
    super();
    this.path = ROUTE_FUNCTION_GEN.MESSENGER_JOB_TABLE_POST;
  }
  onRelatedMenu(): void {
    const path: PathParamModel = {};
    this.toRelatedInfo(path);
  }
  toRelatedInfo(param: PathParamModel): void {
    super.toRelatedInfo(param);
  }
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.MESSENGER_JOB_TABLE_POST,
      servicePath: `InquiryWithdrawalLineOutstand/RelatedInfo/${ROUTE_RELATED_GEN.MESSENGER_REQUEST}/Function/PostMessengerJobTable`
    };
  }
}
