import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'taxinvoice',
    loadChildren: () => import('./tax-inv/tax-inv-relatedinfo.module').then((m) => m.TaxInvoiceRelatedinfoModule)
  },
  {
    path: 'custtransaction',
    loadChildren: () => import('./cust-trans/cust-trans-relatedinfo.module').then((m) => m.CustomerTransactionRelatedinfoModule)
  },
  {
    path: 'paymenthistory',
    loadChildren: () => import('./paym-hist/paym-hist-relatedinfo.module').then((m) => m.InvoicePaymentHistoryRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoiceAndCollectionRelatedinfoRoutingModule {}
