import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ColumnType, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';
import { UIControllerService } from 'core/services/uiController.service';

@Component({
  selector: 'cheque-list-page',
  templateUrl: './cheque-list.page.html',
  styleUrls: ['./cheque-list.page.scss']
})
export class ChequeListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  parentId;
  constructor(public uiService: UIControllerService) {
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'jobChequeGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'messengerJobTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.CHEQUE,
      servicePath: `InquiryWithdrawalLineOutstand/RelatedInfo/${ROUTE_RELATED_GEN.MESSENGER_REQUEST}/RelatedInfo/JobChequeTable`
    };
  }
}
