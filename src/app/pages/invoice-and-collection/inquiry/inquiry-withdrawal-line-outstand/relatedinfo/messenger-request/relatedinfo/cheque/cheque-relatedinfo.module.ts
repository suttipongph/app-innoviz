import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChequeRelatedinfoRoutingModule } from './cheque-relatedinfo-routing.module';
import { ChequeListPage } from './cheque-list/cheque-list.page';
import { ChequeItemPage } from './cheque-item/cheque-item.page';
import { SharedModule } from 'shared/shared.module';
import { JobChequeComponentModule } from 'components/job-cheque/job-cheque.module';

@NgModule({
  declarations: [ChequeListPage, ChequeItemPage],
  imports: [CommonModule, SharedModule, ChequeRelatedinfoRoutingModule, JobChequeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ChequeRelatedinfoModule {}
