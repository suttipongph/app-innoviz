import { MessengerJobTableComponentModule } from 'components/messenger-job-table/messenger-job-table/messenger-job-table.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { CollectionFollowUpComponentModule } from 'components/collection-follow-up/collection-follow-up/collection-follow-up.module';
import { MessengerRequestItemPage } from './messenger-request-item/messenger-request-item.page';
import { MessengerRequestListPage } from './messenger-request-list/messenger-request-list.page';
import { InquiryWithdrawalLineOutstandMessengerRequestRoutingModule } from './inquiry-withdrawal-line-outstand-messenger-request-routing.module';

@NgModule({
  declarations: [MessengerRequestListPage, MessengerRequestItemPage],
  imports: [CommonModule, SharedModule, InquiryWithdrawalLineOutstandMessengerRequestRoutingModule, MessengerJobTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InquiryWithdrawalLineOutstandMessengerRequestModule {}
