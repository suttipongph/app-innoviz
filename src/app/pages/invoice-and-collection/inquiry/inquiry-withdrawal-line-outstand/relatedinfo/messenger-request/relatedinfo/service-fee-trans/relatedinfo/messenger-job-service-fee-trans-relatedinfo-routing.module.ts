import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'invoicetable',
    loadChildren: () => import('./inv/invoicetable-relatedinfo.module').then((m) => m.MessengerJobServiceFeeTransInvoiceTableRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessengerJobServiceFeeTransRelatedinfoRelatedinfoRoutingModule {}
