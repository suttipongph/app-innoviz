import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MessengerRequestListPage } from './messenger-request-list.page';

describe(' MessengerRequestListPage', () => {
  let component: MessengerRequestListPage;
  let fixture: ComponentFixture<MessengerRequestListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MessengerRequestListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MessengerRequestListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
