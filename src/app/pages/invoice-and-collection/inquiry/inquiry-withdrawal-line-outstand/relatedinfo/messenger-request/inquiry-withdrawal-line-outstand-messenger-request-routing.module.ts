import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { MessengerRequestItemPage } from './messenger-request-item/messenger-request-item.page';
import { MessengerRequestListPage } from './messenger-request-list/messenger-request-list.page';

const routes: Routes = [
  {
    path: '',
    component: MessengerRequestListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: MessengerRequestItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () =>
      import('./relatedinfo/withdrawal-line-messenger-request-relatedinfo.module').then(
        (m) => m.InquiryWithdrawalLineOutstandMessengerRequestRelatedInfoModule
      )
  },
  {
    path: ':id/function',
    loadChildren: () =>
      import('./functions/withdrawal-line-messenger-request-function.module').then(
        (m) => m.InquiryWithdrawalLineOutstandMessengerRequestFunctionModule
      )
  },
  {
    path: ':id/report',
    loadChildren: () => import('./report/print-messenger-job-print-report.module').then((m) => m.MessengerJobTablePrintMessengerJobReportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InquiryWithdrawalLineOutstandMessengerRequestRoutingModule {}
