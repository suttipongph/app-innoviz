import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { InquiryWithdrawalLineOutstandMessengerRequestRelatedInfoRoutingModule } from './withdrawal-line-messenger-request-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, InquiryWithdrawalLineOutstandMessengerRequestRelatedInfoRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InquiryWithdrawalLineOutstandMessengerRequestRelatedInfoModule {}
