import { RefType } from './../../../../../../../shared/constants/enumsBusiness';
import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'messenger-request-list-page',
  templateUrl: './messenger-request-list.page.html',
  styleUrls: ['./messenger-request-list.page.scss']
})
export class MessengerRequestListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  parentId;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
    console.log('parentId', this.parentId);
  }
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'messengerJobTableGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'refType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: RefType.WithdrawalLine
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.MESSENGER_REQUEST,
      servicePath: `InquiryWithdrawalLineOutstand/RelatedInfo/${ROUTE_RELATED_GEN.MESSENGER_REQUEST}`
    };
  }
}
