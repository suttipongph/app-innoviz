import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'cust-trans-list-page',
  templateUrl: './cust-trans-list.page.html',
  styleUrls: ['./cust-trans-list.page.scss']
})
export class CustomerTransactionListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute(1);
  pathName = this.uiService.getRelatedInfoOriginTableName();

  parentId = this.uiService.getRelatedInfoParentTableKey();

  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'custTransGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.CUSTOMER_TRANSACTION,
      servicePath: 'InvoiceTable/RelatedInfo/CustTransaction'
    };
  }
}
