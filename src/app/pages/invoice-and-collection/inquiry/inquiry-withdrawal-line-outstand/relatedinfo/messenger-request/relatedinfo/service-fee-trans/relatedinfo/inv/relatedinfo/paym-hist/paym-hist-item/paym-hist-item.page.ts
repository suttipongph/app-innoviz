import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'paym-hist-item.page',
  templateUrl: './paym-hist-item.page.html',
  styleUrls: ['./paym-hist-item.page.scss']
})
export class PaymentHistoryItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.PAYMENT_HISTORY, servicePath: 'InvoiceTable/RelatedInfo/PaymentHistory' };
  }
}
