import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'inquiry-withdrawal-line-outstand-list-page',
  templateUrl: './inquiry-withdrawal-line-outstand-list.page.html',
  styleUrls: ['./inquiry-withdrawal-line-outstand-list.page.scss']
})
export class InquiryWithdrawalLineOutstandListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'withdrawalLineGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.INQUIRYWITHDRAWALLINEOUTSTAND, servicePath: 'InquiryWithdrawalLineOutstand' };
  }
}
