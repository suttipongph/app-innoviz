import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { ReceiptCopyReportRoutingModule } from './receipt-table-report-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, ReceiptCopyReportRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReceiptCopyReportModule {}
