import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReceipttableRelatedinfoRoutingModule } from './receipt-table-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, ReceipttableRelatedinfoRoutingModule]
})
export class ReceipttableRelatedinfoModule {}
