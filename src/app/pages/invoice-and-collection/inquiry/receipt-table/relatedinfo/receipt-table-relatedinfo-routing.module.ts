import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'taxinvoice',
    loadChildren: () => import('./tax-invoice/tax-invoice-relatedinfo.module').then((m) => m.TaxInvoiceRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceipttableRelatedinfoRoutingModule {}
