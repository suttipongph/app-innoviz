import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TaxInvoiceListPage } from './tax-invoice-list.page';

describe('TaxInvoiceListPage', () => {
  let component: TaxInvoiceListPage;
  let fixture: ComponentFixture<TaxInvoiceListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TaxInvoiceListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxInvoiceListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
