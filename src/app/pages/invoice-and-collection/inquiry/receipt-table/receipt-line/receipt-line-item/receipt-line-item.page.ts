import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'receipt-line-item.page',
  templateUrl: './receipt-line-item.page.html',
  styleUrls: ['./receipt-line-item.page.scss']
})
export class ReceiptLineItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.RECEIPTLINE, servicePath: 'ReceiptTable' };
  }
}
