import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReceiptLineRoutingModule } from './receipt-line-routing.module';
import { ReceiptLineListPage } from './receipt-line-list/receipt-line-list.page';
import { ReceiptLineItemPage } from './receipt-line-item/receipt-line-item.page';
import { SharedModule } from 'shared/shared.module';
import { ReceiptLineComponentModule } from 'components/receipt-table/receipt-line/receipt-line.module';

@NgModule({
  declarations: [ReceiptLineListPage, ReceiptLineItemPage],
  imports: [CommonModule, SharedModule, ReceiptLineRoutingModule, ReceiptLineComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReceiptLineModule {}
