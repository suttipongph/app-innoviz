import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentHistoryRelatedinfoRoutingModule } from './payment-history-relatedinfo-routing.module';
import { PaymentHistoryListPage } from './payment-history-list/payment-history-list.page';
import { PaymentHistoryItemPage } from './payment-history-item/payment-history-item.page';
import { SharedModule } from 'shared/shared.module';
import { PaymentHistoryComponentModule } from 'components/payment-history/payment-history/payment-history.module';

@NgModule({
  declarations: [PaymentHistoryListPage, PaymentHistoryItemPage],
  imports: [CommonModule, SharedModule, PaymentHistoryRelatedinfoRoutingModule, PaymentHistoryComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PaymentHistoryRelatedinfoModule {}
