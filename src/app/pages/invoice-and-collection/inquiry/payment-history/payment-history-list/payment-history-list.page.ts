import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'payment-history-list-page',
  templateUrl: './payment-history-list.page.html',
  styleUrls: ['./payment-history-list.page.scss']
})
export class PaymentHistoryListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'paymentHistoryGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.PAYMENT_HISTORY, servicePath: 'PaymentHistory' };
  }
}
