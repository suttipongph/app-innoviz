import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BouncePage } from './bounce.page';

describe('BouncePage', () => {
  let component: BouncePage;
  let fixture: ComponentFixture<BouncePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BouncePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BouncePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
