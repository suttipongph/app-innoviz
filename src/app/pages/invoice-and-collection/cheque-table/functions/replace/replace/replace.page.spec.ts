import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReplacePage } from './replace.page';

describe('ReplacePage', () => {
  let component: ReplacePage;
  let fixture: ComponentFixture<ReplacePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReplacePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplacePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
