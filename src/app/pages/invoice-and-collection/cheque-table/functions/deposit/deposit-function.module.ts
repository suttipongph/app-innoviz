import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepositFunctionRoutingModule } from './deposit-function-routing.module';
import { DepositPage } from './deposit/deposit.page';
import { SharedModule } from 'shared/shared.module';
import { DepositChequeComponentModule } from 'components/cheque-table/cheque-table/deposit-cheque/deposit-cheque.module';

@NgModule({
  declarations: [DepositPage],
  imports: [CommonModule, SharedModule, DepositFunctionRoutingModule, DepositChequeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DepositFunctionModule {}
