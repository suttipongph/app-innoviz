import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CancelFunctionRoutingModule } from './cancel-function-routing.module';
import { CancelPage } from './cancel/cancel.page';
import { SharedModule } from 'shared/shared.module';
import { CancelChequeComponentModule } from 'components/cheque-table/cheque-table/cancel-cheque/cancel-cheque.module';

@NgModule({
  declarations: [CancelPage],
  imports: [CommonModule, SharedModule, CancelFunctionRoutingModule, CancelChequeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CancelFunctionModule {}
