import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChequeTableRoutingModule } from './cheque-table-routing.module';
import { ChequeTableListPage } from './cheque-table-list/cheque-table-list.page';
import { ChequeTableItemPage } from './cheque-table-item/cheque-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { ChequeTableComponentModule } from 'components/cheque-table/cheque-table/cheque-table.module';

@NgModule({
  declarations: [ChequeTableListPage, ChequeTableItemPage],
  imports: [CommonModule, SharedModule, ChequeTableRoutingModule, ChequeTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CollectionChequeTableModule {}
