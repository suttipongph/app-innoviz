import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChequeTableListPage } from './cheque-table-list.page';

describe('ChequeTableListPage', () => {
  let component: ChequeTableListPage;
  let fixture: ComponentFixture<ChequeTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChequeTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChequeTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
