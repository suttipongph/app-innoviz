import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ChequeTableItemPage } from './cheque-table-item/cheque-table-item.page';
import { ChequeTableListPage } from './cheque-table-list/cheque-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: ChequeTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ChequeTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/cheque-table-function.module').then((m) => m.ChequeTableFunctionModule)

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChequeTableRoutingModule {}
