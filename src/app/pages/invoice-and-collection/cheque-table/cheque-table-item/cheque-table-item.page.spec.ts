import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChequeTableItemPage } from './cheque-table-item.page';

describe('ChequeTableItemPage', () => {
  let component: ChequeTableItemPage;
  let fixture: ComponentFixture<ChequeTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChequeTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChequeTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
