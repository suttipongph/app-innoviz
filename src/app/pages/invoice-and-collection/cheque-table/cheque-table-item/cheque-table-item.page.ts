import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'cheque-table-item-page',
  templateUrl: './cheque-table-item.page.html',
  styleUrls: ['./cheque-table-item.page.scss']
})
export class ChequeTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.CHEQUE_TABLE, servicePath: 'ChequeTable' };
  }
}
