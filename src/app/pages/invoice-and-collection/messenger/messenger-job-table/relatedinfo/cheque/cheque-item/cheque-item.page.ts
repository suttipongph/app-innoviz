import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'cheque-item.page',
  templateUrl: './cheque-item.page.html',
  styleUrls: ['./cheque-item.page.scss']
})
export class ChequeItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.CHEQUE, servicePath: 'MessengerJobTable/RelatedInfo/JobChequeTable' };
  }
}
