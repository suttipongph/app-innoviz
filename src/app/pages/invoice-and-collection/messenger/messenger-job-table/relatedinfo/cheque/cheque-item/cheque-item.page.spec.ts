import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChequeItemPage } from './cheque-item.page';

describe('ChequeItemPage', () => {
  let component: ChequeItemPage;
  let fixture: ComponentFixture<ChequeItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChequeItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChequeItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
