import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChequeListPage } from './cheque-list.page';

describe('ChequeListPage', () => {
  let component: ChequeListPage;
  let fixture: ComponentFixture<ChequeListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChequeListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChequeListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
