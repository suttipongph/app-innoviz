import { AuthGuardService } from 'core/services/auth-guard.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChequeListPage } from './cheque-list/cheque-list.page';
import { ChequeItemPage } from './cheque-item/cheque-item.page';

const routes: Routes = [
  {
    path: '',
    component: ChequeListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ChequeItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChequeRelatedinfoRoutingModule {}
