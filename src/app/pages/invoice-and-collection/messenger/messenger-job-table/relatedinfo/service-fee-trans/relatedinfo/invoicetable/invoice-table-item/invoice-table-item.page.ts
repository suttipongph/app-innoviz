import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'invoice-table-item.page',
  templateUrl: './invoice-table-item.page.html',
  styleUrls: ['./invoice-table-item.page.scss']
})
export class InvoiceTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor(public uiService: UIControllerService) {}
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.INVOICE_TABLE,
      servicePath: `MessengerJobTable/RelatedInfo/${ROUTE_RELATED_GEN.SERVICE_FEE_TRANS}/RelatedInfo/${ROUTE_RELATED_GEN.INVOICE_TABLE}`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.INVOICE_LINE,
          servicePath: `MessengerJobTable/RelatedInfo/${ROUTE_RELATED_GEN.SERVICE_FEE_TRANS}/RelatedInfo/${ROUTE_RELATED_GEN.INVOICE_TABLE}`
        }
      ]
    };
  }
}
