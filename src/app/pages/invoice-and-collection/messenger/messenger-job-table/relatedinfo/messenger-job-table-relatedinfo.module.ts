import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { MessengerJobTableRelatedInfoRoutingModule } from './messenger-job-table-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, MessengerJobTableRelatedInfoRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MessengerJobTableRelatedInfoModule {}
