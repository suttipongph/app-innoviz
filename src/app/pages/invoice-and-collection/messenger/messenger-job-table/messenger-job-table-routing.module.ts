import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { MessengerJobTableItemPage } from './messenger-job-table-item/messenger-job-table-item.page';
import { MessengerJobTableListPage } from './messenger-job-table-list/messenger-job-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: MessengerJobTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: MessengerJobTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/messenger-job-table-relatedinfo.module').then((m) => m.MessengerJobTableRelatedInfoModule)
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/messenger-job-table-function.module').then((m) => m.MessengerJobTableFunctionModule)
  },
  {
    path: ':id/report',
    loadChildren: () => import('./report/print-messenger-job-print-report.module').then((m) => m.MessengerJobTablePrintMessengerJobReportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessengerJobTableRoutingModule {}
