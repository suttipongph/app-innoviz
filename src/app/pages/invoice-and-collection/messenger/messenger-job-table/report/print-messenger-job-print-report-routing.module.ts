import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'printmessengerjob',
    loadChildren: () =>
      import('./print-messenger-job/print-messenger-job-print-report-print-report.module').then((m) => m.PrintMessengerJobReportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessengerJobTablePrintMessengerJobReportRoutingModule {}
