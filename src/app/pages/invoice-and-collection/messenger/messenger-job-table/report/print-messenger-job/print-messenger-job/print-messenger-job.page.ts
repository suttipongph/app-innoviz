import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_REPORT_GEN } from 'shared/constants/constantGen';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'print-messenger-job',
  templateUrl: './print-messenger-job.page.html',
  styleUrls: ['./print-messenger-job.page.scss']
})
export class PrintMessengerJobPage implements OnInit {
  pageInfo: PageInformationModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_REPORT_GEN.PRINT_MESSENGER_JOB,
      servicePath: `MessengerJobTable/Report/PrintMessengerJob`
    };
  }
}
