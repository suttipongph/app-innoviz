import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'messenger-job-table-item-page',
  templateUrl: './messenger-job-table-item.page.html',
  styleUrls: ['./messenger-job-table-item.page.scss']
})
export class MessengerJobTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.MESSENGER_JOB_TABLE, servicePath: 'MessengerJobTable' };
  }
}
