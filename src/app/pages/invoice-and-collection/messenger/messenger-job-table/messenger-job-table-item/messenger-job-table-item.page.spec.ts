import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MessengerJobTableItemPage } from './messenger-job-table-item.page';

describe('MessengerJobTableItemPage', () => {
  let component: MessengerJobTableItemPage;
  let fixture: ComponentFixture<MessengerJobTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MessengerJobTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MessengerJobTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
