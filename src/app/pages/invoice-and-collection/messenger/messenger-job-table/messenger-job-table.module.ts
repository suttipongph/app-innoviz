import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessengerJobTableRoutingModule } from './messenger-job-table-routing.module';
import { MessengerJobTableListPage } from './messenger-job-table-list/messenger-job-table-list.page';
import { MessengerJobTableItemPage } from './messenger-job-table-item/messenger-job-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { MessengerJobTableComponentModule } from 'components/messenger-job-table/messenger-job-table/messenger-job-table.module';

@NgModule({
  declarations: [MessengerJobTableListPage, MessengerJobTableItemPage],
  imports: [CommonModule, SharedModule, MessengerJobTableRoutingModule, MessengerJobTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MessengerJobTableModule {}
