import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MessengerJobTableListPage } from './messenger-job-table-list.page';

describe('MessengerJobTableListPage', () => {
  let component: MessengerJobTableListPage;
  let fixture: ComponentFixture<MessengerJobTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MessengerJobTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MessengerJobTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
