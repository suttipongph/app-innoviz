import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssignFunctionRoutingModule } from './assign-function-routing.module';
import { AssignPage } from './assign/assign.page';
import { SharedModule } from 'shared/shared.module';
import { AssignMessengerJobComponent } from 'components/messenger-job-table/messenger-job-table/assign-messenger-job/assign-messenger-job/assign-messenger-job.component';
import { AssignMessengerJobComponentModule } from 'components/messenger-job-table/messenger-job-table/assign-messenger-job/assign-messenger-job.module';

@NgModule({
  declarations: [AssignPage],
  imports: [CommonModule, SharedModule, AssignFunctionRoutingModule, AssignMessengerJobComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AssignFunctionModule {}
