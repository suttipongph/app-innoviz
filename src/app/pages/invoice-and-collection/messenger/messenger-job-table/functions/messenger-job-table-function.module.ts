import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessengerJobTableFunctionRoutingModule } from './messenger-job-table-function-routing.module';
import { SharedModule } from 'shared/shared.module';
import { MessengerJobTableComponentModule } from 'components/messenger-job-table/messenger-job-table/messenger-job-table.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, MessengerJobTableFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MessengerJobTableFunctionModule {}
