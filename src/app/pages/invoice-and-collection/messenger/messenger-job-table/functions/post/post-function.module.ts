import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostFunctionRoutingModule } from './post-function-routing.module';
import { PostPage } from './post/post.page';
import { SharedModule } from 'shared/shared.module';
import { PostMessengerJobComponentModule } from 'components/messenger-job-table/messenger-job-table/post-messenger-job/post-messenger-job.module';

@NgModule({
  declarations: [PostPage],
  imports: [CommonModule, SharedModule, PostFunctionRoutingModule, PostMessengerJobComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PostFunctionModule {}
