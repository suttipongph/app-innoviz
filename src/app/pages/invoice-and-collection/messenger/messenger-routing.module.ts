import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'messengerjobtable', loadChildren: () => import('./messenger-job-table/messenger-job-table.module').then((m) => m.MessengerJobTableModule) }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessengerRoutingModule {}
