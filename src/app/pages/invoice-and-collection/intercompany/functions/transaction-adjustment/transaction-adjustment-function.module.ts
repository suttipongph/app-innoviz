import { AdjustIntercompanyInvoiceComponentModule } from '../../../../../components/intercompany-invoice-table/intercompany-invoice-table/adjust-intercompany-invoice/adjust-intercompany-invoice.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionAdjustmentPage } from './transaction-adjustment/transaction-adjustment.page';
import { TransactionAdjustmentFunctionRoutingModule } from './transaction-adjustment-function-routing.module';
import { SharedModule } from 'shared/shared.module';

@NgModule({
  declarations: [TransactionAdjustmentPage],
  imports: [CommonModule, SharedModule, TransactionAdjustmentFunctionRoutingModule, AdjustIntercompanyInvoiceComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TransactionAdjustmentFunctionModule {}
