import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { TransactionAdjustmentPage } from './transaction-adjustment/transaction-adjustment.page';

const routes: Routes = [
  {
    path: '',
    component: TransactionAdjustmentPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransactionAdjustmentFunctionRoutingModule {}
