import { IntercompanyInvoiceAdjustmentComponentModule } from 'components/intercompany-invoice-adjustment/intercompany-invoice-adjustment/intercompany-invoice-adjustment.module';
import { Component } from '@angular/core';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel, PathParamModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-transaction-adjustment',
  templateUrl: './transaction-adjustment.page.html',
  styleUrls: ['./transaction-adjustment.page.scss']
})
export class TransactionAdjustmentPage extends BaseItemComponent<any> {
  pageInfo: PageInformationModel;
  constructor() {
    super();
    this.path = ROUTE_FUNCTION_GEN.TRANSACTION_ADJUSTMENT;
  }
  onRelatedMenu(): void {
    const path: PathParamModel = {};
    this.toRelatedInfo(path);
  }
  toRelatedInfo(param: PathParamModel): void {
    super.toRelatedInfo(param);
  }
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.TRANSACTION_ADJUSTMENT,
      servicePath: 'IntercompanyInvoiceTable/Function/Transactionadjustment'
    };
  }
}
