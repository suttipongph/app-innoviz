import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TransactionAdjustmentPage } from './transaction-adjustment.page';

describe('TransactionAdjustmentPage', () => {
  let component: TransactionAdjustmentPage;
  let fixture: ComponentFixture<TransactionAdjustmentPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TransactionAdjustmentPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionAdjustmentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
