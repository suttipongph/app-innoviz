import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: 'transactionadjustment',
    loadChildren: () => import('./transaction-adjustment/transaction-adjustment-function.module').then((m) => m.TransactionAdjustmentFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InterCompanyFunctionRoutingModule {}
