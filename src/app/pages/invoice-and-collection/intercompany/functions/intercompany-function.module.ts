import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { MessengerJobTableComponentModule } from 'components/messenger-job-table/messenger-job-table/messenger-job-table.module';
import { InterCompanyFunctionRoutingModule } from './intercompany-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, InterCompanyFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InterCompanyFunctionModule {}
