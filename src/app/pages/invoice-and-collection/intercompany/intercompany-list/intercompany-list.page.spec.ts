import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IntercompanyListPage } from './intercompany-list.page';

describe('IntercompanyListPage', () => {
  let component: IntercompanyListPage;
  let fixture: ComponentFixture<IntercompanyListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IntercompanyListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntercompanyListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
