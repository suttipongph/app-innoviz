import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: 'intercompanyinvoicesettlement',
    loadChildren: () =>
      import('./intercompany-invoice-settlement/intercompany-invoice-settlement.module').then((m) => m.IntercompanyInvoiceSettlementModule)
  },
  {
    path: 'intercompanyinvoiceadjustment',
    loadChildren: () =>
      import('./intercompany-invoice-adjustment/intercompany-invoice-adjustment.module').then((m) => m.IntercompanyInvoiceAdjustmentModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntercompanyInvoiceRelatedInfoRoutingModule {}
