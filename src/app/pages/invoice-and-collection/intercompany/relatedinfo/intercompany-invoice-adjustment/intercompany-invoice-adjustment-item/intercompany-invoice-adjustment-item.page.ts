import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'intercompany-item-page',
  templateUrl: './intercompany-invoice-adjustment-item.page.html',
  styleUrls: ['./intercompany-invoice-adjustment-item.page.scss']
})
export class IntercompanyInvoiceAdjustmentItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.INTERCOMPANY_INVOICE_ADJUSTMENT,
      servicePath: 'IntercompanyInvoiceTable/RelatedInfo/intercompanyinvoiceadjustment'
    };
  }
}
