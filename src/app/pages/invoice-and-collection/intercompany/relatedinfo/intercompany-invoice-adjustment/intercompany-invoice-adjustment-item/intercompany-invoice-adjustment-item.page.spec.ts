import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IntercompanyItemPage } from './intercompany-item.page';

describe('IntercompanyItemPage', () => {
  let component: IntercompanyItemPage;
  let fixture: ComponentFixture<IntercompanyItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IntercompanyItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntercompanyItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
