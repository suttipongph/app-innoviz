import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IntercompanyInvoiceAdjustmentRoutingModule } from './intercompany-invoice-adjustment-routing.module';
import { IntercompanyInvoiceAdjustmentItemPage } from './intercompany-invoice-adjustment-item/intercompany-invoice-adjustment-item.page';
import { IntercompanyInvoiceAdjustmentListPage } from './intercompany-invoice-adjustment-list/intercompany-invoice-adjustment-list.page';
import { SharedModule } from 'shared/shared.module';
import { IntercompanyInvoiceAdjustmentComponentModule } from 'components/intercompany-invoice-adjustment/intercompany-invoice-adjustment/intercompany-invoice-adjustment.module';

@NgModule({
  declarations: [IntercompanyInvoiceAdjustmentListPage, IntercompanyInvoiceAdjustmentItemPage],
  imports: [CommonModule, SharedModule, IntercompanyInvoiceAdjustmentRoutingModule, IntercompanyInvoiceAdjustmentComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IntercompanyInvoiceAdjustmentModule {}
