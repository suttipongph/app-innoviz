import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, ROUTE_MASTER_GEN, ROUTE_RELATED, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'intercompany-list-page',
  templateUrl: './intercompany-invoice-adjustment-list.page.html',
  styleUrls: ['./intercompany-invoice-adjustment-list.page.scss']
})
export class IntercompanyInvoiceAdjustmentListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;

  parentId;
  constructor(public uiService: UIControllerService) {
    this.parentId = this.uiService.getRelatedInfoOriginTableKey();
  }

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'intercompanyInvoiceAdjustmentGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'intercompanyInvoiceTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.INTERCOMPANY_INVOICE_ADJUSTMENT,
      servicePath: 'IntercompanyInvoiceTable/RelatedInfo/intercompanyinvoiceadjustment'
    };
  }
}
