import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { IntercompanyInvoiceAdjustmentItemPage } from './intercompany-invoice-adjustment-item/intercompany-invoice-adjustment-item.page';
import { IntercompanyInvoiceAdjustmentListPage } from './intercompany-invoice-adjustment-list/intercompany-invoice-adjustment-list.page';

const routes: Routes = [
  {
    path: '',
    component: IntercompanyInvoiceAdjustmentListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: IntercompanyInvoiceAdjustmentItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntercompanyInvoiceAdjustmentRoutingModule {}
