import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PostIntercompanyInvSettlementPage } from './post-intercompany-inv-settlement/post-intercompany-inv-settlement.page';

const routes: Routes = [
  {
    path: '',
    component: PostIntercompanyInvSettlementPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostIntercompanyInvSettlementFunctionRoutingModule {}
