import { Component, OnInit } from '@angular/core';
import { ROUTE_FUNCTION_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'cancel-intercompany-inv-settlement-page',
  templateUrl: './cancel-intercompany-inv-settlement.page.html',
  styleUrls: ['./cancel-intercompany-inv-settlement.page.scss']
})
export class CancelIntercompanyInvSettlementPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.CANCEL_INTERCOMPANY_INV_SETTLEMENT,
      servicePath: `IntercompanyInvoiceTable/RelatedInfo/intercompanyinvoicesettlement/Function/CancelIntercompanyInvSettlement`
    };
    console.log(this.pageInfo);
  }
}
