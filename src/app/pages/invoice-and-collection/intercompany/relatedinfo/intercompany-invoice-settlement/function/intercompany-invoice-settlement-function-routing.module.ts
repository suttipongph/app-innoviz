import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'postintercompanyinvsettlement',
    loadChildren: () =>
      import('./post-intercompany-inv-settlement/post-intercompany-inv-settlement-function.module').then(
        (m) => m.PostIntercompanyInvSettlementFunctionModule
      )
  },
  {
    path: 'cancelintercompanyinvsettlement',
    loadChildren: () =>
      import('./cancel-intercompany-inv-settlement/cancel-intercompany-inv-settlement-function.module').then(
        (m) => m.CancelIntercompanyInvSettlementFunctionModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntercompanyInvoiceSettlementFunctionRoutingModule {}
