import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { PostIntercompanyInvSettlementPage } from './post-intercompany-inv-settlement/post-intercompany-inv-settlement.page';
import { PostIntercompanyInvSettlementComponentModule } from 'components/intercompany-invoice-settlement/post-intercompany-inv-settlement/post-intercompany-inv-settlement.module';
import { PostIntercompanyInvSettlementFunctionRoutingModule } from './post-intercompany-inv-settlement-function-routing.module';

@NgModule({
  declarations: [PostIntercompanyInvSettlementPage],
  imports: [CommonModule, SharedModule, PostIntercompanyInvSettlementFunctionRoutingModule, PostIntercompanyInvSettlementComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PostIntercompanyInvSettlementFunctionModule {}
