import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { CancelIntercompanyInvSettlementPage } from './cancel-intercompany-inv-settlement/cancel-intercompany-inv-settlement.page';
import { CancelIntercompanyInvSettlementComponentModule } from 'components/intercompany-invoice-settlement/cancel-intercompany-inv-settlement/cancel-intercompany-inv-settlement.module';
import { CancelIntercompanyInvSettlementFunctionRoutingModule } from './cancel-intercompany-inv-settlement-function-routing.module';

@NgModule({
  declarations: [CancelIntercompanyInvSettlementPage],
  imports: [CommonModule, SharedModule, CancelIntercompanyInvSettlementFunctionRoutingModule, CancelIntercompanyInvSettlementComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CancelIntercompanyInvSettlementFunctionModule {}
