import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { IntercompanyInvoiceSettlementFunctionRoutingModule } from './intercompany-invoice-settlement-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, IntercompanyInvoiceSettlementFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IntercompanyInvoiceSettlementFunctionModule {}
