import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostIntercompanyInvSettlementPage } from './post-intercompany-inv-settlement.page';

describe('PostIntercompanyInvSettlementPage', () => {
  let component: PostIntercompanyInvSettlementPage;
  let fixture: ComponentFixture<PostIntercompanyInvSettlementPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PostIntercompanyInvSettlementPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostIntercompanyInvSettlementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
