import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CancelIntercompanyInvSettlementPage } from './cancel-intercompany-inv-settlement/cancel-intercompany-inv-settlement.page';

const routes: Routes = [
  {
    path: '',
    component: CancelIntercompanyInvSettlementPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CancelIntercompanyInvSettlementFunctionRoutingModule {}
