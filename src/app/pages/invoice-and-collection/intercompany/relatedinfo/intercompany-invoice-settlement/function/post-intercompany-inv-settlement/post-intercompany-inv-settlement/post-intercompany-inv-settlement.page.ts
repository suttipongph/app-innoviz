import { Component, OnInit } from '@angular/core';
import { ROUTE_FUNCTION_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'post-intercompany-inv-settlement-page',
  templateUrl: './post-intercompany-inv-settlement.page.html',
  styleUrls: ['./post-intercompany-inv-settlement.page.scss']
})
export class PostIntercompanyInvSettlementPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.POST_INTERCOMPANY_INV_SETTLEMENT,
      servicePath: `IntercompanyInvoiceTable/RelatedInfo/intercompanyinvoicesettlement/Function/PostIntercompanyInvSettlement`
    };
    console.log(this.pageInfo);
  }
}
