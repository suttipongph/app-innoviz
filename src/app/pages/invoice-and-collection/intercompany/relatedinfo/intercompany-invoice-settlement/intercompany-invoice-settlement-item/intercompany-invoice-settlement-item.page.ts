import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'intercompany-item-page',
  templateUrl: './intercompany-invoice-settlement-item.page.html',
  styleUrls: ['./intercompany-invoice-settlement-item.page.scss']
})
export class IntercompanyInvoiceSettlementItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.INTERCOMPANY_INVOICE_SETTLEMENT,
      servicePath: 'IntercompanyInvoiceTable/RelatedInfo/intercompanyinvoicesettlement'
    };
  }
}
