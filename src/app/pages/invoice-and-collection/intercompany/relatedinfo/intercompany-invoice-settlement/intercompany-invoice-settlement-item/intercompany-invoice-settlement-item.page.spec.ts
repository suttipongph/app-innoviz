import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IntercompanyInvoiceSettlementItemPage } from './intercompany-invoice-settlement-item.page';

describe('IntercompanyInvoiceSettlementItemPage', () => {
  let component: IntercompanyInvoiceSettlementItemPage;
  let fixture: ComponentFixture<IntercompanyInvoiceSettlementItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IntercompanyInvoiceSettlementItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntercompanyInvoiceSettlementItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
