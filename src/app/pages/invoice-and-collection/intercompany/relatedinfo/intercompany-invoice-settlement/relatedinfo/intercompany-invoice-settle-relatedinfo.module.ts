import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { IntercompanyInvoiceSettlementRelatedInfoRoutingModule } from './intercompany-invoice-settle-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, IntercompanyInvoiceSettlementRelatedInfoRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IntercompanyInvoiceSettlementRelatedInfoModule {}
