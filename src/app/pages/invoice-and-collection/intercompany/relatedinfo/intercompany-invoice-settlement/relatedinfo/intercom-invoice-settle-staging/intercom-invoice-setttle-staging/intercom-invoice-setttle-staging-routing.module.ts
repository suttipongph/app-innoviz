import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IntercompanyInvoiceSettlementStagingListPage } from './intercom-invoice-setttle-staging-list/intercom-invoice-setttle-staging-list.page';
import { IntercompanyInvoiceSettlementStagingItemPage } from './intercom-invoice-setttle-staging-item/intercom-invoice-setttle-staging-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: IntercompanyInvoiceSettlementStagingListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: IntercompanyInvoiceSettlementStagingItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntercompanyInvoiceSettlementStagingRoutingModule {}
