import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: 'intercompanyinvoicesettlementstaging',
    loadChildren: () =>
      import('./intercom-invoice-settle-staging/intercom-invoice-setttle-staging/intercom-invoice-setttle-staging.module').then(
        (m) => m.IntercompanyInvoiceSettlementStagingModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntercompanyInvoiceSettlementRelatedInfoRoutingModule {}
