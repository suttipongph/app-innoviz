import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN, SortType, StagingBatchStatus } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'intercom-invoice-setttle-staging-list-page',
  templateUrl: './intercom-invoice-setttle-staging-list.page.html',
  styleUrls: ['./intercom-invoice-setttle-staging-list.page.scss']
})
export class IntercompanyInvoiceSettlementStagingListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  parentId = this.uiService.getRelatedInfoParentTableKey();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'stagingTableIntercoInvSettleGUID';
    console.log('pa=' + this.parentId);
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'intercompanyInvoiceSettlementId',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: null,
        textKey: 'interfaceStagingBatchId',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: null,
        textKey: 'intercompanyInvoiceSettlementGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.INTERCOMPANY_INVOICE_SETTLEMENT_STAGING,
      servicePath: `IntercompanyInvoiceTable/RelatedInfo/intercompanyinvoicesettlement/RelatedInfo/IntercompanyInvoiceSettlementStaging`
    };
  }
}
