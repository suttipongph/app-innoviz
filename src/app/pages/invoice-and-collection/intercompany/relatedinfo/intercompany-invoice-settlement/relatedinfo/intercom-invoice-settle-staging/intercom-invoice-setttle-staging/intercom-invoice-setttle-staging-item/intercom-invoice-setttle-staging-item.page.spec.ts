import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ProcessTransItemPage } from './process-transaction-item.page';

describe('ProcessTransactionItemPage', () => {
  let component: ProcessTransItemPage;
  let fixture: ComponentFixture<ProcessTransItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProcessTransItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessTransItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
