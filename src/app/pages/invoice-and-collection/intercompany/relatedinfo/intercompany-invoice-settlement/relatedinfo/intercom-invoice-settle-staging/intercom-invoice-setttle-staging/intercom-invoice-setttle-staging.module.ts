import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IntercompanyInvoiceSettlementStagingRoutingModule } from './intercom-invoice-setttle-staging-routing.module';
import { IntercompanyInvoiceSettlementStagingListPage } from './intercom-invoice-setttle-staging-list/intercom-invoice-setttle-staging-list.page';
import { IntercompanyInvoiceSettlementStagingItemPage } from './intercom-invoice-setttle-staging-item/intercom-invoice-setttle-staging-item.page';
import { SharedModule } from 'shared/shared.module';
import { StagingTableIntercoInvSettleComponentModule } from 'components/staging-table-interco-inv-settle/staging-table-interco-inv-settle.module';

@NgModule({
  declarations: [IntercompanyInvoiceSettlementStagingListPage, IntercompanyInvoiceSettlementStagingItemPage],
  imports: [CommonModule, SharedModule, IntercompanyInvoiceSettlementStagingRoutingModule, StagingTableIntercoInvSettleComponentModule],

  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IntercompanyInvoiceSettlementStagingModule {}
