import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { IntercompanyInvoiceSettlementItemPage } from './intercompany-invoice-settlement-item/intercompany-invoice-settlement-item.page';
import { IntercompanyInvoiceSettlementListPage } from './intercompany-invoice-settlement-list/intercompany-invoice-settlement-list.page';

const routes: Routes = [
  {
    path: '',
    component: IntercompanyInvoiceSettlementListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: IntercompanyInvoiceSettlementItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () =>
      import('./relatedinfo/intercompany-invoice-settle-relatedinfo.module').then((m) => m.IntercompanyInvoiceSettlementRelatedInfoModule)
  },
  {
    path: ':id/function',
    loadChildren: () =>
      import('./function/intercompany-invoice-settlement-function.module').then((m) => m.IntercompanyInvoiceSettlementFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntercompanyInvoiceSettlementRoutingModule {}
