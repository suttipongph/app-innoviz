import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IntercompanyInvoiceSettlementRoutingModule } from './intercompany-invoice-settlement-routing.module';
import { IntercompanyInvoiceSettlementItemPage } from './intercompany-invoice-settlement-item/intercompany-invoice-settlement-item.page';
import { IntercompanyInvoiceSettlementListPage } from './intercompany-invoice-settlement-list/intercompany-invoice-settlement-list.page';
import { SharedModule } from 'shared/shared.module';
import { IntercompanyInvoiceSettlementComponentModule } from 'components/intercompany-invoice-settlement/intercompany-invoice-settlement/intercompany-invoice-settlement.module';

@NgModule({
  declarations: [IntercompanyInvoiceSettlementListPage, IntercompanyInvoiceSettlementItemPage],
  imports: [CommonModule, SharedModule, IntercompanyInvoiceSettlementRoutingModule, IntercompanyInvoiceSettlementComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IntercompanyInvoiceSettlementModule {}
