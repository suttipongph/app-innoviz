import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { IntercompanyItemPage } from './intercompany-item/intercompany-item.page';
import { IntercompanyListPage } from './intercompany-list/intercompany-list.page';

const routes: Routes = [
  {
    path: '',
    component: IntercompanyListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: IntercompanyItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/intercompany-invoice-relatedinfo.module').then((m) => m.IntercompanyInvoiceRelatedInfoModule)
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/intercompany-function.module').then((m) => m.InterCompanyFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntercompanyRoutingModule {}
