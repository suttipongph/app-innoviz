import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReceiptTempTableListPage } from './receipt-temp-table-list.page';

describe('ReceiptTempTableListPage', () => {
  let component: ReceiptTempTableListPage;
  let fixture: ComponentFixture<ReceiptTempTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReceiptTempTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptTempTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
