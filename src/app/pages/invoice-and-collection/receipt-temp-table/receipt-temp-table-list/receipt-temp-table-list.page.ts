import { Component, OnInit } from '@angular/core';
import { ColumnType, ReceiptTempRefType, ROUTE_MASTER_GEN, SortType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'receipt-temp-table-list-page',
  templateUrl: './receipt-temp-table-list.page.html',
  styleUrls: ['./receipt-temp-table-list.page.scss']
})
export class ReceiptTempTableListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'receiptTempTableGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'receiptTempRefType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: ReceiptTempRefType.Normal.toString()
      }
    ];
    this.option.columns = columns;
  }

  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.COLLECTION, servicePath: 'ReceiptTempTable' };
  }
}
