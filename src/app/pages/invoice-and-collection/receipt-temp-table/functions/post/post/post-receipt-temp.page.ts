import { Component } from '@angular/core';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel, PathParamModel } from 'shared/models/systemModel';

@Component({
  selector: 'post-receipt-temp-page',
  templateUrl: './post-receipt-temp.page.html',
  styleUrls: ['./post-receipt-temp.page.scss']
})
export class PostReceiptTempPage extends BaseItemComponent<any> {
  pageInfo: PageInformationModel;
  constructor() {
    super();
    this.path = ROUTE_FUNCTION_GEN.POST_RECEIPT_TEMP;
  }
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.POST_RECEIPT_TEMP,
      servicePath: `ReceiptTempTable/Function/PostReceiptTemp`
    };
  }
}
