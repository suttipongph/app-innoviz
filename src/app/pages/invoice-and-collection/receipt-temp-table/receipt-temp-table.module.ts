import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReceiptTempTableRoutingModule } from './receipt-temp-table-routing.module';
import { ReceiptTempTableListPage } from './receipt-temp-table-list/receipt-temp-table-list.page';
import { ReceiptTempTableItemPage } from './receipt-temp-table-item/receipt-temp-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { ReceiptTempTableComponentModule } from 'components/receipt-temp-table/receipt-temp-table/receipt-temp-table.module';
import { ReceiptTempPaymDetailItemPage } from './receipt-temp-paym-detail-item/receipt-temp-paym-detail-item.page';
import { ReceiptTempPaymDetailComponentModule } from 'components/receipt-temp-table/receipt-temp-paym-detail/receipt-temp-paym-detail.module';

@NgModule({
  declarations: [ReceiptTempTableListPage, ReceiptTempTableItemPage, ReceiptTempPaymDetailItemPage],
  imports: [CommonModule, SharedModule, ReceiptTempTableRoutingModule, ReceiptTempTableComponentModule, ReceiptTempPaymDetailComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReceiptTempTableModule {}
