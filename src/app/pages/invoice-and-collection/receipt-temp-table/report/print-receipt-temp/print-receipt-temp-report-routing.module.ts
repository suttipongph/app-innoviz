import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PrintReceiptTempPage } from './print-receipt-temp/print-receipt-temp.page';

const routes: Routes = [
  {
    path: '',
    component: PrintReceiptTempPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrintReceiptTempReportRoutingModule {}
