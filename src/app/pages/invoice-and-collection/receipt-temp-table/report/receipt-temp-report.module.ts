import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { ReceiptTempReportRoutingModule } from './receipt-temp-report-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, ReceiptTempReportRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReceiptTempReportModule {}
