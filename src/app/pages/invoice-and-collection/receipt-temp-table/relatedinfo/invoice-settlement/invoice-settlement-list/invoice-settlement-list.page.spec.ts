import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InvoiceSettlementListPage } from './invoice-settlement-list.page';

describe('InvoiceSettlementListPage', () => {
  let component: InvoiceSettlementListPage;
  let fixture: ComponentFixture<InvoiceSettlementListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InvoiceSettlementListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceSettlementListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
