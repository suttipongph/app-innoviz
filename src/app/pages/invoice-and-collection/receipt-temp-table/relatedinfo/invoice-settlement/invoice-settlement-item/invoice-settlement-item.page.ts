import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'invoice-settlement-item.page',
  templateUrl: './invoice-settlement-item.page.html',
  styleUrls: ['./invoice-settlement-item.page.scss']
})
export class InvoiceSettlementItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.INVOICE_SETTLEMENT,
      servicePath: `ReceiptTempTable/RelatedInfo/InvoiceSettlement`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.INVOICE_SETTLEMENT,
          servicePath: `ReceiptTempTable/RelatedInfo/InvoiceSettlement`
        }
      ]
    };
  }
}
