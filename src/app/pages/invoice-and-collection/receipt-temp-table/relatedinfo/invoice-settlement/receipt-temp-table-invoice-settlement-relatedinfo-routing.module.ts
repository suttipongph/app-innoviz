import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InvoiceSettlementListPage } from './invoice-settlement-list/invoice-settlement-list.page';
import { InvoiceSettlementItemPage } from './invoice-settlement-item/invoice-settlement-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: InvoiceSettlementListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: InvoiceSettlementItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/invoice-settlement-relatedinfo.module').then((m) => m.InvoiceSettlementRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceiptTempTableInvoiceSettlementRelatedinfoRoutingModule {}
