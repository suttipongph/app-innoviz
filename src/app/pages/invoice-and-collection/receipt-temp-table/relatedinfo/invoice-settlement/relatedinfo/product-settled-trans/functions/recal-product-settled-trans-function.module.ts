import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecalProductSettledTransFunctionRoutingModule } from './recal-product-settled-trans-function-routing.module';
import { SharedModule } from 'shared/shared.module';
import { RecalProductSettledTransComponentModule } from 'components/product-settled-trans/product-settled-trans/recal-product-settled-trans/recal-product-settled-trans.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedModule,
    RecalProductSettledTransFunctionRoutingModule,
    RecalProductSettledTransComponentModule
  ],
})
export class RecalProductSettledTransFunctionModule {}
