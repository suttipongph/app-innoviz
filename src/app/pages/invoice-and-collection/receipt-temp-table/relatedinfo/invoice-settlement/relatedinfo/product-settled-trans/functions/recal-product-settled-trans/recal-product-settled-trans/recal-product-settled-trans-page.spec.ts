import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecalProductSettledTransPage } from './recal-product-settled-trans.-page';

describe('RecalProductSettledTransPage', () => {
  let component: RecalProductSettledTransPage;
  let fixture: ComponentFixture<RecalProductSettledTransPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecalProductSettledTransPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecalProductSettledTransPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
