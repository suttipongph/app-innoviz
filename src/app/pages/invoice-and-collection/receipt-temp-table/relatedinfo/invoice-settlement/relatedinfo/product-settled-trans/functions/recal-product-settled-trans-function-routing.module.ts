import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'recalculate',
    loadChildren: () => import('./recal-product-settled-trans/recal-product-settled-trans.module').then((m) => m.RecalProductSettledTransModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecalProductSettledTransFunctionRoutingModule {}
