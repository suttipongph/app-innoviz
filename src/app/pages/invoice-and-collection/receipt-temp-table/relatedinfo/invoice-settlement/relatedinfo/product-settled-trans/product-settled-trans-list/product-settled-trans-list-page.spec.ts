import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductSettledTransListPageComponent } from './product-settled-trans-list-page.component';

describe('ProductSettledTransListPageComponent', () => {
  let component: ProductSettledTransListPageComponent;
  let fixture: ComponentFixture<ProductSettledTransListPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductSettledTransListPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSettledTransListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
