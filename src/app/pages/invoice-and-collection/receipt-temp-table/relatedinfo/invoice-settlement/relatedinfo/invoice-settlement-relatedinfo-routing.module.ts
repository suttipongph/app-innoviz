import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'productsettlement',
    loadChildren: () =>
      import('./product-settled-trans/invoice-settlement-product-settled-trans.module').then((m) => m.InvoiceSettlementProductSettledTransModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoiceSettlementRelatedinfoRoutingModule {}
