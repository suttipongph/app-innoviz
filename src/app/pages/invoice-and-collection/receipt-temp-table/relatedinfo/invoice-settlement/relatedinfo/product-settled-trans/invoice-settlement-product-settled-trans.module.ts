import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvoiceSettlementProductSettledTransRoutingModule } from './invoice-settlement-product-settled-trans-routing.module';
import { ProductSettledTransListPage } from './product-settled-trans-list/product-settled-trans-list-page';
import { ProductSettledTransItemPage } from './product-settled-trans-item/product-settled-trans-item-page';
import { ProductSettledTransComponentModule } from 'components/product-settled-trans/product-settled-trans/product-settled-trans.module';

@NgModule({
  declarations: [ProductSettledTransListPage, ProductSettledTransItemPage],
  imports: [CommonModule, InvoiceSettlementProductSettledTransRoutingModule, ProductSettledTransComponentModule]
})
export class InvoiceSettlementProductSettledTransModule {}
