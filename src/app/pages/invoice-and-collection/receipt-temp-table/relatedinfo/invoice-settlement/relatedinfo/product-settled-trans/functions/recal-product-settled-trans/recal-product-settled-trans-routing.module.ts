import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { RecalProductSettledTransPage } from './recal-product-settled-trans/recal-product-settled-trans.-page';

const routes: Routes = [
  {
    path: '',
    component: RecalProductSettledTransPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecalProductSettledTransRoutingModule {}
