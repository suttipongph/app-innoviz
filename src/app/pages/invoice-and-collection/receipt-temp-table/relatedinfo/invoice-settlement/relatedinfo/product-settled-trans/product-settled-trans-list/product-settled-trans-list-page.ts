import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-product-settled-trans-list-page',
  templateUrl: './product-settled-trans-list-page.html',
  styleUrls: ['./product-settled-trans-list-page.scss']
})
export class ProductSettledTransListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'productSettledTransGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }

  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.PRODUCT_SETTLEMENT,
      servicePath: `ReceiptTempTable/RelatedInfo/InvoiceSettlement/RelatedInfo/ProductSettlement`
    };
  }
}
