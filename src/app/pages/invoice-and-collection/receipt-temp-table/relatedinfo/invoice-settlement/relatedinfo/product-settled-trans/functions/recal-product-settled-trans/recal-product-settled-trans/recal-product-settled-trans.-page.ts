import { Component, OnInit } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';

@Component({
  selector: 'recal-product-settled-trans-page',
  templateUrl: './recal-product-settled-trans-page.html',
  styleUrls: ['./recal-product-settled-trans-page.scss']
})
export class RecalProductSettledTransPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.RECALCULATE,
      servicePath: `ReceiptTempTable/RelatedInfo/InvoiceSettlement/RelatedInfo/ProductSettlement/Function/Recalculate`
    };
  }
}
