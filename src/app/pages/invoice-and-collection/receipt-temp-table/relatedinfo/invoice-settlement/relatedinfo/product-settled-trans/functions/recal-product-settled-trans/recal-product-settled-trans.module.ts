import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { RecalProductSettledTransComponentModule } from 'components/product-settled-trans/product-settled-trans/recal-product-settled-trans/recal-product-settled-trans.module';
import { RecalProductSettledTransPage } from './recal-product-settled-trans/recal-product-settled-trans.-page';
import { RecalProductSettledTransRoutingModule } from './recal-product-settled-trans-routing.module';

@NgModule({
  declarations: [RecalProductSettledTransPage],
  imports: [
    CommonModule,
    SharedModule,
    RecalProductSettledTransRoutingModule,
    RecalProductSettledTransComponentModule
  ],
})
export class RecalProductSettledTransModule {}
