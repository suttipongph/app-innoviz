import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvoiceSettlementRelatedinfoRoutingModule } from './invoice-settlement-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, InvoiceSettlementRelatedinfoRoutingModule]
})
export class InvoiceSettlementRelatedinfoModule {}
