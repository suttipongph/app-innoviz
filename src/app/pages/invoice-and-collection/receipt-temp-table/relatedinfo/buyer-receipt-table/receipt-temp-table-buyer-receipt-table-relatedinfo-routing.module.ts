import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { BuyerReceiptTableItemPage } from './buyer-receipt-table-item/buyer-receipt-table-item.page';
import { BuyerReceiptTableListPage } from './buyer-receipt-table-list/buyer-receipt-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: BuyerReceiptTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BuyerReceiptTableItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceiptTempTableBuyerReceiptTableRelatedinfoRoutingModule {}
