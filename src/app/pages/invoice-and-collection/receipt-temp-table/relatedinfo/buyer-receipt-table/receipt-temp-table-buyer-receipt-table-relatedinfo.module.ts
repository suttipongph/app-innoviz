import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { BuyerReceiptTableComponentModule } from 'components/buyer-receipt-table/buyer-receipt-table/buyer-receipt-table.module';
import { BuyerReceiptTableListPage } from './buyer-receipt-table-list/buyer-receipt-table-list.page';
import { BuyerReceiptTableItemPage } from './buyer-receipt-table-item/buyer-receipt-table-item.page';
import { ReceiptTempTableBuyerReceiptTableRelatedinfoRoutingModule } from './receipt-temp-table-buyer-receipt-table-relatedinfo-routing.module';

@NgModule({
  declarations: [BuyerReceiptTableListPage, BuyerReceiptTableItemPage],
  imports: [CommonModule, SharedModule, ReceiptTempTableBuyerReceiptTableRelatedinfoRoutingModule, BuyerReceiptTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReceiptTempTableMemoRelatedinfoModule {}
