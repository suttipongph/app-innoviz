import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'buyer-receipt-table-item.page',
  templateUrl: './buyer-receipt-table-item.page.html',
  styleUrls: ['./buyer-receipt-table-item.page.scss']
})
export class BuyerReceiptTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.BUYER_RECEIPT_TABLE,
      servicePath: `ReceiptTempTable/RelatedInfo/buyerreceipttable`
    };
  }
}
