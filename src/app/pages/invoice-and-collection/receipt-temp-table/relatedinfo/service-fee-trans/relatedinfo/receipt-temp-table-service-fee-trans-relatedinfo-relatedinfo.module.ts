import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReceiptTempTableServiceFeeTransRelatedinfoRelatedInfoRoutingModule } from './receipt-temp-table-service-fee-trans-relatedinfo-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, ReceiptTempTableServiceFeeTransRelatedinfoRelatedInfoRoutingModule]
})
export class ReceiptTempTableServiceFeeTransRelatedinfoRelatedInfoModule {}
