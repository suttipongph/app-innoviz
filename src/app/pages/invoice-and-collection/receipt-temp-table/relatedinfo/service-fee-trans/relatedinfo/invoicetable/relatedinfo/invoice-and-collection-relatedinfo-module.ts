import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvoiceAndCollectionRelatedinfoRoutingModule } from './invoice-and-collection-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, InvoiceAndCollectionRelatedinfoRoutingModule]
})
export class InvoiceAndCollectionRelatedinfoModule {}
