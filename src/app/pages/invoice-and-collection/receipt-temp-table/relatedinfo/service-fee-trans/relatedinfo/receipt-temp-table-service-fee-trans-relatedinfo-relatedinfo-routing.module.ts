import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'invoicetable',
    loadChildren: () =>
      import('./invoicetable/receipt-temp-table-service-fee-trans-invoicetable-relatedinfo.module').then(
        (m) => m.ReceiptTempTableServiceFeeTransInvoiceTableRelatedinfoModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceiptTempTableServiceFeeTransRelatedinfoRelatedInfoRoutingModule {}
