import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'invoicesettlement',
    loadChildren: () =>
      import('./invoice-settlement/receipt-temp-table-invoice-settlement-relatedinfo.module').then(
        (m) => m.ReceiptTempTableInvoiceSettlementRelatedinfoModule
      )
  },
  {
    path: 'receipttable',
    loadChildren: () =>
      import('./receipt-table/receipt-temp-table-receipt-table-relatedinfo.module').then((m) => m.ReceiptTempTableReceiptTableRelatedinfoModule)
  },
  {
    path: 'attachment',
    loadChildren: () => import('./attachment/receipt-temp-table-attachment.module').then((m) => m.ReceiptTempTableAttachmentModule)
  },
  {
    path: 'memotrans',
    loadChildren: () => import('./memo/receipt-temp-table-memo-relatedinfo.module').then((m) => m.ReceiptTempTableMemoRelatedinfoModule)
  },
  {
    path: 'memotrans',
    loadChildren: () => import('./memo/receipt-temp-table-memo-relatedinfo.module').then((m) => m.ReceiptTempTableMemoRelatedinfoModule)
  },
  {
    path: 'servicefeetrans',
    loadChildren: () =>
      import('./service-fee-trans/receipt-temp-table-service-fee-trans-relatedinfo.module').then(
        (m) => m.ReceiptTempTableServiceFeeTransRelatedinfoModule
      )
  },
  {
    path: 'buyerreceipttable',
    loadChildren: () =>
      import('./buyer-receipt-table/receipt-temp-table-buyer-receipt-table-relatedinfo.module').then((m) => m.ReceiptTempTableMemoRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceiptTempTableRelatedinfoRoutingModule {}
