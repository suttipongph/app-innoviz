import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TaxInvoiceLineItemPage } from './tax-invoice-line-item.page';

describe('TaxInvoiceLineItemPage', () => {
  let component: TaxInvoiceLineItemPage;
  let fixture: ComponentFixture<TaxInvoiceLineItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TaxInvoiceLineItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxInvoiceLineItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
