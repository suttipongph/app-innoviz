import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'tax-invoice-line-item-page',
  templateUrl: './tax-invoice-line-item.page.html',
  styleUrls: ['./tax-invoice-line-item.page.scss']
})
export class TaxInvoiceLineItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.TAX_INVOICE_LINE,
      servicePath: 'ReceiptTable/relatedinfo/taxinvoice'
    };
  }
}
