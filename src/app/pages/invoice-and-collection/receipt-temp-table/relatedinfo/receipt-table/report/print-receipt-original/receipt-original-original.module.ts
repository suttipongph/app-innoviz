import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { PrintReceiptComponentModule } from 'components/receipt-table/print-receipt/print-receipt.module';
import { PrintReceiptOriginalPage } from './print-receipt-original/print-receipt-original.page';
import { PrintReceiptOriginalRoutingModule } from './receipt-original-report-routing.module';

@NgModule({
  declarations: [PrintReceiptOriginalPage],
  imports: [CommonModule, SharedModule, PrintReceiptOriginalRoutingModule, PrintReceiptComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PrintReceiptOriginalReportModule {}
