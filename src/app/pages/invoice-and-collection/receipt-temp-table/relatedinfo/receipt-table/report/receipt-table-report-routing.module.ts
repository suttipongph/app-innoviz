import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'printreceiptcopy',
    loadChildren: () => import('./print-receipt-copy/receipt-report-copy.module').then((m) => m.PrintReceiptCopyReportModule)
  },
  {
    path: 'printreceiptoriginal',
    loadChildren: () => import('./print-receipt-original/receipt-original-original.module').then((m) => m.PrintReceiptOriginalReportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceiptCopyReportRoutingModule {}
