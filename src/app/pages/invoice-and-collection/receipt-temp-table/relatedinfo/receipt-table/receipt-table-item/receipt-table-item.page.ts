import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'receipt-table-item.page',
  templateUrl: './receipt-table-item.page.html',
  styleUrls: ['./receipt-table-item.page.scss']
})
export class ReceiptTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.RECEIPT_TABLE,
      servicePath: `ReceiptTempTable/RelatedInfo/receipttable`,
      childPaths: [
        {
          pagePath: ROUTE_MASTER_GEN.RECEIPTLINE,
          servicePath: `ReceiptTempTable/RelatedInfo/receipttable`
        }
      ]
    };
  }
}
