import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReceiptTableListPage } from './receipt-table-list/receipt-table-list.page';
import { ReceiptTableItemPage } from './receipt-table-item/receipt-table-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ReceiptLineItemPage } from './receipt-line-item/receipt-line-item.page';
const routes: Routes = [
  {
    path: '',
    component: ReceiptTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ReceiptTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/receiptline-child/:id',
    component: ReceiptLineItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/receipt-table-relatedinfo-module').then((m) => m.ReceipttableRelatedinfoModule)
  },
  {
    path: ':id/report',
    loadChildren: () => import('./report/receipt-table-report.module').then((m) => m.ReceiptCopyReportModule)
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceiptTempTableReceiptTableRelatedinfoRoutingModule {}
