import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReceiptTempPaymDetailSuspenseSettlementRelatedinfoRoutingModule } from './receipt-temp-paym-detail-suspense-settlement-relatedinfo-routing.module';
import { SuspenseSettlementListPage } from './suspense-settlement-list/suspense-settlement-list.page';
import { SuspenseSettlementItemPage } from './suspense-settlement-item/suspense-settlement-item.page';
import { InvoiceSettlementDetailComponentModule } from 'components/invoice-settlement-detail/invoice-settlement-detail/invoice-settlement-detail.module';
import { SharedModule } from 'shared/shared.module';

@NgModule({
  declarations: [SuspenseSettlementListPage, SuspenseSettlementItemPage],
  imports: [CommonModule, SharedModule, ReceiptTempPaymDetailSuspenseSettlementRelatedinfoRoutingModule, InvoiceSettlementDetailComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReceiptTempPaymDetailSuspenseSettlementRelatedinfoModule {}
