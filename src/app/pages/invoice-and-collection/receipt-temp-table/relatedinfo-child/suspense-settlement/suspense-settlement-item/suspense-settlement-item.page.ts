import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'suspense-settlement-item.page',
  templateUrl: './suspense-settlement-item.page.html',
  styleUrls: ['./suspense-settlement-item.page.scss']
})
export class SuspenseSettlementItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.INVOICE_SETTLEMENT,
      servicePath: `ReceiptTempTable/${ROUTE_RELATED_GEN.RECEIPT_TEMP_PAYM_DETAIL}/RelatedInfo/SuspenseSettlement`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.INVOICE_SETTLEMENT,
          servicePath: `ReceiptTempTable/${ROUTE_RELATED_GEN.RECEIPT_TEMP_PAYM_DETAIL}/RelatedInfo/SuspenseSettlement`
        }
      ]
    };
  }
}
