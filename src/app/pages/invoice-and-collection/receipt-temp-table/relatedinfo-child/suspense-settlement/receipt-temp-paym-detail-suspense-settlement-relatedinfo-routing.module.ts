import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SuspenseSettlementListPage } from './suspense-settlement-list/suspense-settlement-list.page';
import { SuspenseSettlementItemPage } from './suspense-settlement-item/suspense-settlement-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: SuspenseSettlementListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: SuspenseSettlementItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceiptTempPaymDetailSuspenseSettlementRelatedinfoRoutingModule {}
