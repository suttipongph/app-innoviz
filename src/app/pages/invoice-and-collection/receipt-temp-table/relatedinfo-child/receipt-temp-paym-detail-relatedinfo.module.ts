import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReceiptTempPaymDetailRelatedinfoRoutingModule } from './receipt-temp-paym-detail-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, ReceiptTempPaymDetailRelatedinfoRoutingModule]
})
export class ReceiptTempPaymDetailRelatedinfoModule {}
