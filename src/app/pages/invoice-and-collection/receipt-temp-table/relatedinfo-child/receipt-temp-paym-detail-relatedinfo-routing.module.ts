import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'suspensesettlement',
    loadChildren: () =>
      import('./suspense-settlement/receipt-temp-paym-detail-suspense-settlement-relatedinfo.module').then(
        (m) => m.ReceiptTempPaymDetailSuspenseSettlementRelatedinfoModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceiptTempPaymDetailRelatedinfoRoutingModule {}
