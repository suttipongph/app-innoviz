import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'receipt-temp-paym-detail-item-page',
  templateUrl: './receipt-temp-paym-detail-item.page.html',
  styleUrls: ['./receipt-temp-paym-detail-item.page.scss']
})
export class ReceiptTempPaymDetailItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.RECEIPT_TEMP_PAYM_DETAIL, servicePath: 'ReceiptTempTable'
    };
  }
}
