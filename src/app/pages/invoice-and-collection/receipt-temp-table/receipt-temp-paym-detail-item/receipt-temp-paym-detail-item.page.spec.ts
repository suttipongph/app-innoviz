import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReceiptTempPaymDetailItemPage } from './receipt-temp-paym-detail-item.page';

describe('ReceiptTempPaymDetailItemPage', () => {
  let component: ReceiptTempPaymDetailItemPage;
  let fixture: ComponentFixture<ReceiptTempPaymDetailItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReceiptTempPaymDetailItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptTempPaymDetailItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
