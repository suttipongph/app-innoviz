import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'receipt-temp-table-item-page',
  templateUrl: './receipt-temp-table-item.page.html',
  styleUrls: ['./receipt-temp-table-item.page.scss']
})
export class ReceiptTempTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.COLLECTION,
      servicePath: 'ReceiptTempTable',
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.RECEIPT_TEMP_PAYM_DETAIL,
          servicePath: `ReceiptTempTable`
        }
      ]
    };
  }
}
