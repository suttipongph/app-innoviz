import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReviewCreditAppRequestRoutingModule } from './review-credit-app-request-routing.module';
import { ReviewCreditAppRequestComponentModule } from 'components/review-credit-app-request/review-credit-app-request/review-credit-app-request.module';
import { ReviewCreditAppRequestItemPage } from './review-credit-app-request-item-page/review-credit-app-reuqest-item-page';
import { ReviewCreditAppRequestListPage } from './review-credit-app-request-list-page/review-credit-app-request-list-page';

@NgModule({
  declarations: [ReviewCreditAppRequestItemPage, ReviewCreditAppRequestListPage],
  imports: [CommonModule, ReviewCreditAppRequestRoutingModule, ReviewCreditAppRequestComponentModule]
})
export class ReviewCreditAppRequestModule {}
