import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-review-credit-app-reuqest-item-page',
  templateUrl: './review-credit-app-reuqest-item-page.html',
  styleUrls: ['./review-credit-app-reuqest-item-page.scss']
})
export class ReviewCreditAppRequestItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.CREDIT_APP_LINE,
      servicePath: `ReviewActiveCreditAppLine/${getProductType(this.masterRoute, true)}/RelatedInfo/ReviewCreditAppRequest`
    };
  }
}
