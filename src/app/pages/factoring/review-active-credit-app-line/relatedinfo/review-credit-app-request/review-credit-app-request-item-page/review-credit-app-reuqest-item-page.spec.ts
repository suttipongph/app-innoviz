import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewCreditAppRequestItemPage } from './review-credit-app-reuqest-item-page';

describe('ReviewCreditAppReuqestItemPageComponent', () => {
  let component: ReviewCreditAppRequestItemPage;
  let fixture: ComponentFixture<ReviewCreditAppRequestItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReviewCreditAppRequestItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewCreditAppRequestItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
