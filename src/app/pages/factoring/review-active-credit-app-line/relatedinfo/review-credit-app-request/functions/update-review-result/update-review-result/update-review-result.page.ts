import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel, PathParamModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-update-review-result-page',
  templateUrl: './update-review-result.page.html',
  styleUrls: ['./update-review-result.page.scss']
})
export class UpdateReviewResultPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }

  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.UPDATE_REVIEW_RESULT,
      servicePath: `ReviewActiveCreditAppLine/${getProductType(
        this.masterRoute,
        true
      )}/RelatedInfo/ReviewCreditAppRequest/Function/updatereviewresult`
    };
  }
}
