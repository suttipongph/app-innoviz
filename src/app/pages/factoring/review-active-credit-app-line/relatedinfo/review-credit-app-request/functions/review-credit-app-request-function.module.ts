import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReviewCreditAppFunctionRoutingModule } from './review-credit-app-request-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, ReviewCreditAppFunctionRoutingModule]
})
export class ReviewCreditAppFunctionModule {}
