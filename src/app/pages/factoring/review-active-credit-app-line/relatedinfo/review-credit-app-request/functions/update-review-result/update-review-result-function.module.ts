import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { UpdateReviewResultPage } from './update-review-result/update-review-result.page';
import { UpdateReviewResultFunctionRoutingModule } from './update-review-result-function-routing.module';
import { UpdateReviewResultComponentModule } from 'components/review-credit-app-request/update-review-result/update-review-result.module';

@NgModule({
  declarations: [UpdateReviewResultPage],
  imports: [CommonModule, SharedModule, UpdateReviewResultFunctionRoutingModule, UpdateReviewResultComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UpdateReviewResultFunctionModule {}
