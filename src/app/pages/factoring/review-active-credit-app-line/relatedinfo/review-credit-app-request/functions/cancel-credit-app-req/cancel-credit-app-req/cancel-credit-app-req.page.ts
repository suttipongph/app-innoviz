import { Component, OnInit } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';

@Component({
  selector: 'cancel-credit-app-req',
  templateUrl: './cancel-credit-app-req.page.html',
  styleUrls: ['./cancel-credit-app-req.page.scss']
})
export class CancelCreditAppRequestPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.UPDATE_REVIEW_RESULT,
      servicePath: `ReviewActiveCreditAppLine/Factoring/RelatedInfo/ReviewCreditAppRequest/Function/CancelCreditAppRequest`
    };
  }
}
