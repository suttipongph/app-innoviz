import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'updatereviewresult',
    loadChildren: () => import('./update-review-result/update-review-result-function.module').then((m) => m.UpdateReviewResultFunctionModule)
  },
  {
    path: 'cancelcreditapprequest',
    loadChildren: () =>
      import('./cancel-credit-app-req/cancel-credit-app-req.page-function.module').then((m) => m.CancelCreditAppRequestFunctionModule)
  },
  {
    path: 'printsetbookdoctrans',
    loadChildren: () => import('./print-book-doc/print-book-doc-function.module').then((m) => m.PrintBookDocFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReviewCreditAppFunctionRoutingModule {}
