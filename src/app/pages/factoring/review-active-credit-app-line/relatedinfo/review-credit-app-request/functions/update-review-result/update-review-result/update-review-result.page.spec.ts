import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UpdateReviewResultPage } from './update-review-result.page';

describe('AddendumPage', () => {
  let component: UpdateReviewResultPage;
  let fixture: ComponentFixture<UpdateReviewResultPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UpdateReviewResultPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateReviewResultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
