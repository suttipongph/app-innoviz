import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { UpdateReviewResultPage } from './update-review-result/update-review-result.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateReviewResultPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpdateReviewResultFunctionRoutingModule {}
