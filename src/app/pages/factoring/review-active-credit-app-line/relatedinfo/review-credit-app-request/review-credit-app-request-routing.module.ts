import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ReviewCreditAppRequestItemPage } from './review-credit-app-request-item-page/review-credit-app-reuqest-item-page';
import { ReviewCreditAppRequestListPage } from './review-credit-app-request-list-page/review-credit-app-request-list-page';

const routes: Routes = [
  {
    path: '',
    component: ReviewCreditAppRequestListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ReviewCreditAppRequestItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/review-credit-app-request-relatedinfo.module').then((m) => m.ReviewCreditAppRequestRelatedinfoModule)
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/review-credit-app-request-function.module').then((m) => m.ReviewCreditAppFunctionModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReviewCreditAppRequestRoutingModule {}
