import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { ReviewCreditAppRequestRelatedinfoRoutingModule } from './review-credit-app-request-relatedinfo-routing.module';

@NgModule({
  //declarations: [BookmarkDocumentTransItemComponent, BookmarkDocumentTransListComponent],
  imports: [CommonModule, SharedModule, ReviewCreditAppRequestRelatedinfoRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReviewCreditAppRequestRelatedinfoModule {}
