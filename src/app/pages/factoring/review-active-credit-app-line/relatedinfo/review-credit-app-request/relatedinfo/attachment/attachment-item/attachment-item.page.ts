import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'attachment-item.page',
  templateUrl: './attachment-item.page.html',
  styleUrls: ['./attachment-item.page.scss']
})
export class AttachmentItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.ATTACHMENT,
      servicePath: `ReviewActiveCreditAppLine/${getProductType(this.masterRoute, true)}/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/Attachment`
    };
  }
}
