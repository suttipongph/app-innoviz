import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'review-credit-app-request-memo-item.page',
  templateUrl: './review-credit-app-request-memo-item.page.html',
  styleUrls: ['./review-credit-app-request-memo-item.page.scss']
})
export class CreditAppRequestMemoItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();

  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: '/memotrans',
      servicePath: `ReviewActiveCreditAppLine/${getProductType(this.masterRoute, true)}/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/memotrans`
    };
  }
}
