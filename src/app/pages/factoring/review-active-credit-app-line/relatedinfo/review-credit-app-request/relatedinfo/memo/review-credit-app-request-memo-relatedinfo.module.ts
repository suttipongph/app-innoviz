import { MemoTransComponentModule } from './../../../../../../../components/memo-trans/memo-trans/memo-trans.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppRequestMemoRelatedinfoRoutingModule } from './review-credit-app-request-memo-relatedinfo-routing.module';
import { CreditAppRequestMemoListPage } from './review-credit-app-request-memo-list/review-credit-app-request-memo-list.page';
import { CreditAppRequestMemoItemPage } from './review-credit-app-request-memo-item/review-credit-app-request-memo-item.page';
import { SharedModule } from 'shared/shared.module';

@NgModule({
  declarations: [CreditAppRequestMemoListPage, CreditAppRequestMemoItemPage],
  imports: [CommonModule, SharedModule, CreditAppRequestMemoRelatedinfoRoutingModule, MemoTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppRequestMemoRelatedinfoModule {}
