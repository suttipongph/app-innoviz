import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { FinancialStatementTransItemPage } from './financial-statement-trans-item/financial-statement-trans-item.page';
import { FinancialStatementTransListPage } from './financial-statement-trans-list/financial-statement-trans-list.page';

const routes: Routes = [
  {
    path: '',
    component: FinancialStatementTransListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: FinancialStatementTransItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'function', 
    loadChildren: () =>  import('./functions/review-credit-app-request-function.module').then((m)=>m.ReviewCreditAppRequestFunctionFunctionModule) 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReviewCreditAppRequestFinancialStatementTransRelatedinfoRoutingModule {}
