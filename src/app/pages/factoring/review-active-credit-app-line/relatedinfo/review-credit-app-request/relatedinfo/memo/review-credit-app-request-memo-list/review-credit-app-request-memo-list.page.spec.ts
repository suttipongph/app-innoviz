import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditAppRequestMemoListPage } from './review-credit-app-request-memo-list.page';

describe('MemoListPage', () => {
  let component: CreditAppRequestMemoListPage;
  let fixture: ComponentFixture<CreditAppRequestMemoListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditAppRequestMemoListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppRequestMemoListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
