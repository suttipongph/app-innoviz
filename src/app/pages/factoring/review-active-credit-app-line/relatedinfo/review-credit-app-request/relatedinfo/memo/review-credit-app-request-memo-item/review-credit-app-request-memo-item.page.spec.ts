import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditAppRequestMemoItemPage } from './review-credit-app-request-memo-item.page';

describe('MemoItemPage', () => {
  let component: CreditAppRequestMemoItemPage;
  let fixture: ComponentFixture<CreditAppRequestMemoItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditAppRequestMemoItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppRequestMemoItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
