import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants/constantGen';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'financial-statement-trans-item-page',
  templateUrl: './financial-statement-trans-item.page.html',
  styleUrls: ['./financial-statement-trans-item.page.scss']
})
export class FinancialStatementTransItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.FINANCIAL_STATEMENT_TRANS,
      servicePath: `ReviewActiveCreditAppLine/${getProductType(this.masterRoute, true)}/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/FinancialStatementTrans`
    };
  }
}