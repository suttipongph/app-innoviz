import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReviewCreditAppBookmarkFunctionRoutingModule } from './review-credit-app-request-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, ReviewCreditAppBookmarkFunctionRoutingModule]
})
export class ReviewCreditAppBookmarkFunctionModule {}
