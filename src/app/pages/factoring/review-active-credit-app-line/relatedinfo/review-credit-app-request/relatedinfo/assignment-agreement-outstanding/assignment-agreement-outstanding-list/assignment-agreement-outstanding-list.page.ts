import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'assignment-agreement-outstanding-list-page',
  templateUrl: './assignment-agreement-outstanding-list.page.html',
  styleUrls: ['./assignment-agreement-outstanding-list.page.scss']
})
export class AssignmentAgreementOutstandingListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'assignmentAgreementOutstandingGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.BOOKMARK_DOCUMENT_TRANS,
      servicePath: `ReviewActiveCreditAppLine/Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/AssignmentAgreementOutstanding`
    };
  }
}
