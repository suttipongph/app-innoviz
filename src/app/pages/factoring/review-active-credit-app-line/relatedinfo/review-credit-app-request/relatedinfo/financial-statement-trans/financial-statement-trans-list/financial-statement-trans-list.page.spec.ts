import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FinancialStatementTransListPage } from './financial-statement-trans-list.page';

describe('FinancialStatementTransListPage', () => {
  let component: FinancialStatementTransListPage;
  let fixture: ComponentFixture<FinancialStatementTransListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinancialStatementTransListPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialStatementTransListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
