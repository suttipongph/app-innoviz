import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReviewCreditAppRequestRelatedinfoRoutingModule } from './review-credit-app-request-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, ReviewCreditAppRequestRelatedinfoRoutingModule]
})
export class ReviewCreditAppRequestRelatedInfoModule {}
