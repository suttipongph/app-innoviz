import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookmarkDocumentListPage } from './bookmark-document-list.page';

describe('BookmarkDocumentListPage', () => {
  let component: BookmarkDocumentListPage;
  let fixture: ComponentFixture<BookmarkDocumentListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookmarkDocumentListPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkDocumentListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
