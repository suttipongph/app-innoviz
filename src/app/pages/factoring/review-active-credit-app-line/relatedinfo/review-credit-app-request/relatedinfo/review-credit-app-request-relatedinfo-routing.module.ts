import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'bookmarkdocumenttrans',
    loadChildren: () =>
      import('./bookmark-document-trans/review-credit-app-request-bookmark-document-trans-relatedinfo.module').then(
        (m) => m.ReviewCreditAppRequestBookmarkDocumentRelatedinfoModule
      )
  },
  {
    path: 'financialstatementtrans',
    loadChildren: () =>
      import('./financial-statement-trans/review-credit-app-request-financial-statement-trans-relatedinfo.module').then(
        (m) => m.ReviewCreditAppRequestFinancialStatementTransRelatedinfoModule
      )
  },
  {
    path: 'memotrans',
    loadChildren: () => import('./memo/review-credit-app-request-memo-relatedinfo.module').then((m) => m.CreditAppRequestMemoRelatedinfoModule)
  },
  {
    path: 'attachment',
    loadChildren: () => import('./attachment/review-credit-app-request-attachment.module').then((m) => m.ReviewCreditAppRequestAttachmentModule)
  },
  {
    path: 'assignmentagreementoutstanding',
    loadChildren: () =>
      import('./assignment-agreement-outstanding/credit-app-table-assignment-agreement-outstanding-relatedinfo.module').then(
        (m) => m.CreditAppTableAssignmentAgreementOutstandingRelatedinfoModule
      )
  },
  {
    path: 'creditoutstanding',
    loadChildren: () =>
      import('./credit-outstanding/credit-app-request-credit-outstanding-relatedinfo.module').then(
        (m) => m.CreditAppRequestTableCreditOutstandingRelatedinfoModule
      )
  },
  {
    path: 'cabuyercreditoutstanding',

    loadChildren: () =>
      import('./ca-buyer-credit-outstanding/amend-ca-ca-buyer-credit-outstanding-relatedinfo.module').then(
        (m) => m.AmendCCreditAppRequestTableCaBuyerCreditOutstandingRelatedinfoModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReviewCreditAppRequestRelatedinfoRoutingModule {}
