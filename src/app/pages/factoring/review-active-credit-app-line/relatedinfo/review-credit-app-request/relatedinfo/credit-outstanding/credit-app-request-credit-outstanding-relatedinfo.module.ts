import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppRequestTableCreditOutstandingRelatedinfoRoutingModule } from './credit-app-request-credit-outstanding-relatedinfo-routing.module';
import { CreditOutstandingListPage } from './credit-outstanding-list/credit-outstanding-list.page';
import { SharedModule } from 'shared/shared.module';
import { CAReqCreditOutStandingComponentModule } from 'components/ca-req-credit-outstanding/ca-req-credit-out-standing/ca-req-credit-out-standing.module';

@NgModule({
  declarations: [CreditOutstandingListPage],
  imports: [CommonModule, SharedModule, CreditAppRequestTableCreditOutstandingRelatedinfoRoutingModule, CAReqCreditOutStandingComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppRequestTableCreditOutstandingRelatedinfoModule {}
