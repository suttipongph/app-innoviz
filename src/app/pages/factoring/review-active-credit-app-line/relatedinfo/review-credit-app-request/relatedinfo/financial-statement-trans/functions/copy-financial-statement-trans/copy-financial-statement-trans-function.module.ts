import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppReqTableFinancialRelatedinfoCopyFinancialFunctionRoutingModule } from './copy-financial-statement-trans-function-routing.module';
import { CopyFinancialStatementTransPage } from './copy-financial-statement-trans/copy-financial-statement-trans.page';
import { SharedModule } from 'shared/shared.module';
import { CopyFinancialStatementTransComponentModule } from 'components/financial-statement-trans/copy-financial-statement-trans/copy-financial-statement-trans.module';

@NgModule({
  declarations: [CopyFinancialStatementTransPage],
  imports: [
    CommonModule,
    SharedModule,
    CreditAppReqTableFinancialRelatedinfoCopyFinancialFunctionRoutingModule,
    CopyFinancialStatementTransComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppReqTableFinancialRelatedinfoCopyFinancialFunctionModule {}
