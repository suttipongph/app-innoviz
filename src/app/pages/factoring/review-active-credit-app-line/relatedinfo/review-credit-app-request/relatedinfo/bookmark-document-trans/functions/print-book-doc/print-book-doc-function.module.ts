import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrintBookDocFunctionRoutingModule } from './print-book-doc-function-routing.module';
import { SharedModule } from 'shared/shared.module';
import { PrintBookDocPage } from './print-book-doc/print-book-doc.page';
import { PrintBookDocTransComponentModule } from 'components/bookmark-document-trans/print-book-doc-trans/print-book-doc-trans.module';

@NgModule({
  declarations: [PrintBookDocPage],
  imports: [CommonModule, SharedModule, PrintBookDocFunctionRoutingModule, PrintBookDocTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PrintBookDocFunctionModule {}
