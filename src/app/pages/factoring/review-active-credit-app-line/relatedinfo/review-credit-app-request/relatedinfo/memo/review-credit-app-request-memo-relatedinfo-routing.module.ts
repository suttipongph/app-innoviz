import { AuthGuardService } from './../../../../../../../core/services/auth-guard.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreditAppRequestMemoListPage } from './review-credit-app-request-memo-list/review-credit-app-request-memo-list.page';
import { CreditAppRequestMemoItemPage } from './review-credit-app-request-memo-item/review-credit-app-request-memo-item.page';

const routes: Routes = [
  {
    path: '',
    component: CreditAppRequestMemoListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CreditAppRequestMemoItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppRequestMemoRelatedinfoRoutingModule {}
