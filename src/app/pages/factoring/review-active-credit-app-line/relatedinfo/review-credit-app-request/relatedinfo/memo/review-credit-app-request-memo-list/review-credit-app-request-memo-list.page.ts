import { ROUTE_RELATED_GEN } from './../../../../../../../../shared/constants/constantGen';
import { Component, OnInit } from '@angular/core';
import { ColumnType, ROUTE_MASTER_GEN, SortType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';
import { UIControllerService } from 'core/services/uiController.service';
import { getProductType } from 'shared/functions/value.function';

@Component({
  selector: 'review-credit-app-request-memo-list-page',
  templateUrl: './review-credit-app-request-memo-list.page.html',
  styleUrls: ['./review-credit-app-request-memo-list.page.scss']
})
export class CreditAppRequestMemoListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();

  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'memoTransGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: '/memotrans',
      servicePath: `ReviewActiveCreditAppLine/${getProductType(this.masterRoute, true)}/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/memotrans`
    };
  }
}
