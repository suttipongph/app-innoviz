import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CopyFinancialStatementTransPage } from './copy-financial-statement-trans.page';

describe('CopyFinancialStatementTransPage', () => {
  let component: CopyFinancialStatementTransPage;
  let fixture: ComponentFixture<CopyFinancialStatementTransPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CopyFinancialStatementTransPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyFinancialStatementTransPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
