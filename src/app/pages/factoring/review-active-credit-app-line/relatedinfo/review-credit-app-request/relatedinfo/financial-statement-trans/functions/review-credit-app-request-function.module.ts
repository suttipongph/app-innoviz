import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'primeng/api';
import {  ReviewCreditAppRequestFunctionRoutingModule } from './review-credit-app-request-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule,  ReviewCreditAppRequestFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReviewCreditAppRequestFunctionFunctionModule {}
