import { Component, OnInit } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { UIControllerService } from 'core/services/uiController.service';
import { getProductType } from 'shared/functions/value.function';

@Component({
  selector: 'copy-financial-statement-trans-page',
  templateUrl: './copy-financial-statement-trans.page.html',
  styleUrls: ['./copy-financial-statement-trans.page.scss']
})
export class CopyFinancialStatementTransPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.COPY_FINANCIAL_STATEMENT_TRANS,
      servicePath: `ReviewActiveCreditAppLine/${getProductType(
        this.masterRoute,
        true
      )}/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans`
    };
  }
}
