import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FinancialStatementTransItemPage } from './financial-statement-trans-item.page';

describe('FinancialStatementTransItemPage', () => {
  let component: FinancialStatementTransItemPage;
  let fixture: ComponentFixture<FinancialStatementTransItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinancialStatementTransItemPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialStatementTransItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
