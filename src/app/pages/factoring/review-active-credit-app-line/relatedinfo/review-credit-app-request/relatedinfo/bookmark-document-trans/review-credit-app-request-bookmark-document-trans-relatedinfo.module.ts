import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReviewCreditAppRequestComponentModule } from 'components/review-credit-app-request/review-credit-app-request/review-credit-app-request.module';
import { BookmarkDocumentListPage } from './bookmark-document-list/bookmark-document-list.page';
import { BookmarkDocumentItemPage } from './bookmark-document-item/bookmark-document-item.page';
import { ReviewCreditAppRequestBookmarkDocumentRelatedinfoRoutingModule } from './review-credit-app-request-bookmark-document-trans-relatedinfo-routing.module';
import { SharedModule } from 'shared/shared.module';
import { BookmarkDocumentTransComponentModule } from 'components/bookmark-document-trans/bookmark-document-trans/bookmark-document-trans.module';

@NgModule({
  declarations: [BookmarkDocumentListPage, BookmarkDocumentItemPage],
  imports: [CommonModule, SharedModule, ReviewCreditAppRequestBookmarkDocumentRelatedinfoRoutingModule, BookmarkDocumentTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReviewCreditAppRequestBookmarkDocumentRelatedinfoModule {}
