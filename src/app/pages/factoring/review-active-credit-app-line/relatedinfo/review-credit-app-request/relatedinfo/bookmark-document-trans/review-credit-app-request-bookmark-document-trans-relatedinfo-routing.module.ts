import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { BookmarkDocumentItemPage } from './bookmark-document-item/bookmark-document-item.page';
import { BookmarkDocumentListPage } from './bookmark-document-list/bookmark-document-list.page';

const routes: Routes = [
  {
    path: '',
    component: BookmarkDocumentListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BookmarkDocumentItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/review-credit-app-request-function.module').then((m) => m.ReviewCreditAppBookmarkFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReviewCreditAppRequestBookmarkDocumentRelatedinfoRoutingModule {}
