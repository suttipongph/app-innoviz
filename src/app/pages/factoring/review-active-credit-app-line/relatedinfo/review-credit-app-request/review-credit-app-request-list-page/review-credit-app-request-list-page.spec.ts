import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewCreditAppRequestListPage } from './review-credit-app-request-list-page';

describe('ReviewCreditAppReuqestListPageComponent', () => {
  let component: ReviewCreditAppRequestListPage;
  let fixture: ComponentFixture<ReviewCreditAppRequestListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReviewCreditAppRequestListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewCreditAppRequestListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
