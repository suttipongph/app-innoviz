import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-review-credit-app-request-list-page',
  templateUrl: './review-credit-app-request-list-page.html',
  styleUrls: ['./review-credit-app-request-list-page.scss']
})
export class ReviewCreditAppRequestListPage implements OnInit {
  pageInfo: PageInformationModel;
  option: OptionModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'creditAppRequestTableGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: '/reviewcreditapprequest',
      servicePath: `ReviewActiveCreditAppLine/${getProductType(this.masterRoute, true)}/RelatedInfo/ReviewCreditAppRequest`
    };
  }
}
