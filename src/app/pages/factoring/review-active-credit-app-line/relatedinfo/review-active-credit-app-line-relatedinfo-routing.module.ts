import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'reviewcreditapprequest',
    loadChildren: () => import('./review-credit-app-request/review-credit-app-request.module').then((m) => m.ReviewCreditAppRequestModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReviewActiveCreditAppLineRelatedinfoRoutingModule {}
