import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReviewActiveCreditAppLineRelatedinfoRoutingModule } from './review-active-credit-app-line-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, ReviewActiveCreditAppLineRelatedinfoRoutingModule]
})
export class ReviewActiveCreditAppLineRelatedinfoModule {}
