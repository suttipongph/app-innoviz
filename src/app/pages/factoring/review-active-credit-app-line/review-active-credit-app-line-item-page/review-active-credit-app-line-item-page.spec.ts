import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewActiveCreditAppLineItemPage } from './review-active-credit-app-line-item-page';

describe('ReviewActiveCreditAppLineItemPageComponent', () => {
  let component: ReviewActiveCreditAppLineItemPage;
  let fixture: ComponentFixture<ReviewActiveCreditAppLineItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReviewActiveCreditAppLineItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewActiveCreditAppLineItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
