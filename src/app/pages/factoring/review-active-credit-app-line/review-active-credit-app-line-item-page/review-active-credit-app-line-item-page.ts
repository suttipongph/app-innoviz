import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-review-active-credit-app-line-item-page',
  templateUrl: './review-active-credit-app-line-item-page.html',
  styleUrls: ['./review-active-credit-app-line-item-page.scss']
})
export class ReviewActiveCreditAppLineItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.CREDIT_APP_LINE,
      servicePath: `ReviewActiveCreditAppLine/${getProductType(this.masterRoute, true)}`
    };
  }
}
