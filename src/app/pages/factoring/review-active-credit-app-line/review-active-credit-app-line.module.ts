import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReviewActiveCreditAppLineRoutingModule } from './review-active-credit-app-line-routing.module';
import { ReviewActiveCreditAppLineListPage } from './review-active-credit-app-line-list-page/review-active-credit-app-line-list-page';
import { ReviewActiveCreditAppLineItemPage } from './review-active-credit-app-line-item-page/review-active-credit-app-line-item-page';
import { CreditAppLineComponentModule } from 'components/credit-app-table/credit-app-line/credit-app-line.module';

@NgModule({
  declarations: [ReviewActiveCreditAppLineListPage, ReviewActiveCreditAppLineItemPage],
  imports: [CommonModule, ReviewActiveCreditAppLineRoutingModule, CreditAppLineComponentModule]
})
export class ReviewActiveCreditAppLineModule {}
