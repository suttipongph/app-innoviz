import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewActiveCreditAppLineListPage } from './review-active-credit-app-line-list-page';

describe('ReviewActiveCreditAppLineListPageComponent', () => {
  let component: ReviewActiveCreditAppLineListPage;
  let fixture: ComponentFixture<ReviewActiveCreditAppLineListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReviewActiveCreditAppLineListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewActiveCreditAppLineListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
