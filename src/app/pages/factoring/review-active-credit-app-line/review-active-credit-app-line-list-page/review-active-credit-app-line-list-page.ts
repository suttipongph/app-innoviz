import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, Operators, ProductType, ROUTE_MASTER_GEN, SortType } from 'shared/constants';
import { datetoString } from 'shared/functions/date.function';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-review-active-credit-app-line-list-page',
  templateUrl: './review-active-credit-app-line-list-page.html',
  styleUrls: ['./review-active-credit-app-line-list-page.scss']
})
export class ReviewActiveCreditAppLineListPage implements OnInit {
  pageInfo: PageInformationModel;
  option: OptionModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'creditAppLineGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.CREDIT_APPLICATION_ID',
        textKey: 'creditAppTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        order: 1,
        sortingKey: 'creditAppTable_creditAppId',
        searchingKey: 'creditAppTableGUID'
      },
      {
        label: 'LABEL.LINE',
        textKey: 'lineNum',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.ASC,
        order: 2
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'customerTable_CustomerTableGUID',
        sortingKey: 'customerTable_CustomerId',
        order: 3
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'buyerTableGUID',
        sortingKey: 'buyerTable_BuyerId',
        order: 4
      },

      {
        label: 'LABEL.APRROVED_CREDIT_LIMIT_LINE',
        textKey: 'approvedCreditLimitLine',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        // format: this.CURRENCY_13_2,
        order: 5
      },
      {
        label: 'LABEL.REVIEWED_DATE',
        textKey: 'reviewDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE,
        order: 6
      },
      {
        label: 'LABEL.EXPIRY_DATE',
        textKey: 'expiryDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE,
        order: 7,
        values: [datetoString(new Date()), null],
        minDate: new Date()
      },
      {
        label: 'LABEL.BUSINESS_SEGMENT_ID',
        textKey: 'businessSegment_Values',
        type: ColumnType.MASTER,
        visibility: false,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'creditapptableGUID',
        type: ColumnType.MASTER,
        visibility: false,
        sorting: SortType.NONE,
        parentKey: null
      },
      {
        label: null,
        textKey: 'productType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: ProductType.Factoring.toString()
      }
    ];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.CREDIT_APP_LINE,
      servicePath: `ReviewActiveCreditAppLine/${getProductType(this.masterRoute, true)}`
    };
  }
}
