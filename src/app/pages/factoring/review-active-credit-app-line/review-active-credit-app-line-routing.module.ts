import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ReviewActiveCreditAppLineItemPage } from './review-active-credit-app-line-item-page/review-active-credit-app-line-item-page';
import { ReviewActiveCreditAppLineListPage } from './review-active-credit-app-line-list-page/review-active-credit-app-line-list-page';

const routes: Routes = [
  {
    path: '',
    component: ReviewActiveCreditAppLineListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ReviewActiveCreditAppLineItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () =>
      import('./relatedinfo/review-active-credit-app-line-relatedinfo.module').then((m) => m.ReviewActiveCreditAppLineRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReviewActiveCreditAppLineRoutingModule {}
