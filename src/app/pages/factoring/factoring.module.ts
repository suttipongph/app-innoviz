import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FactoringRoutingModule } from './factoring-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, FactoringRoutingModule]
})
export class FactoringModule {}
