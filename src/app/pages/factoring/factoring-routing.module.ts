import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'creditapprequesttable',
    loadChildren: () =>
      import('pages/credit-app-request/credit-app-request-table/credit-app-request-table.module').then((m) => m.CreditAppRequestTableModule)
  },
  {
    path: 'creditapptable',
    loadChildren: () => import('pages/credit-app/credit-app-table/credit-app-table.module').then((m) => m.CreditAppTableModule)
  },
  {
    path: 'allmainagreementtable/mainagreementtable',
    loadChildren: () => import('pages/main-agreement-table/main-agreement-table.module').then((m) => m.MainAgreementTableModule)
  },
  {
    path: 'newmainagreementtable/mainagreementtable',
    loadChildren: () => import('pages/main-agreement-table/main-agreement-table.module').then((m) => m.MainAgreementTableModule)
  },
  {
    path: 'allbusinesscollateralagmtable/businesscollateralagmtable',
    loadChildren: () =>
      import('pages/business-collateral-agm-table/business-collateral-agm-table.module').then((m) => m.BusinessCollateralAgmTableModule)
  },
  {
    path: 'newbusinesscollateralagmtable/businesscollateralagmtable',
    loadChildren: () =>
      import('pages/business-collateral-agm-table/business-collateral-agm-table.module').then((m) => m.BusinessCollateralAgmTableModule)
  },
  {
    path: 'reviewactivecreditappline',
    loadChildren: () =>
      import('pages/factoring/review-active-credit-app-line/review-active-credit-app-line.module').then((m) => m.ReviewActiveCreditAppLineModule)
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FactoringRoutingModule {}
