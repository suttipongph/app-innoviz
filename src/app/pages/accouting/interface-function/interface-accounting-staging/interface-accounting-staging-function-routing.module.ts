import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { InterfaceAccountingStagingPage } from './interface-accounting-staging/interface-accounting-staging.page';

const routes: Routes = [
  {
    path: '',
    component: InterfaceAccountingStagingPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InterfaceAccountingStagingFunctionRoutingModule {}
