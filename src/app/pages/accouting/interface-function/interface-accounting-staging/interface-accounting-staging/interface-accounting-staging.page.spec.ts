import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InterfaceAccountingStagingPage } from './interface-accounting-staging.page';

describe('PostPage', () => {
  let component: InterfaceAccountingStagingPage;
  let fixture: ComponentFixture<InterfaceAccountingStagingPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InterfaceAccountingStagingPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InterfaceAccountingStagingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
