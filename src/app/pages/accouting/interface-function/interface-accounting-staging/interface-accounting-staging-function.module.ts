import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InterfaceAccountingStagingFunctionRoutingModule } from './interface-accounting-staging-function-routing.module';
import { SharedModule } from 'shared/shared.module';
import { InterfaceAccountingStagingPage } from './interface-accounting-staging/interface-accounting-staging.page';
import { InterfaceAccountingStagingComponentModule } from 'components/function/interface-accounting-staging/interface-accounting-staging.module';

@NgModule({
  declarations: [InterfaceAccountingStagingPage],
  imports: [CommonModule, SharedModule, InterfaceAccountingStagingFunctionRoutingModule, InterfaceAccountingStagingComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InterfaceAccountingStagingFunctionModule {}
