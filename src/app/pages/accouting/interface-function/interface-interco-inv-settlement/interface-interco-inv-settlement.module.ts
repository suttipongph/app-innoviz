import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InterfaceIntercoInvSettlementRoutingModule } from './interface-interco-inv-settlement-routing.module';
import { InterfaceIntercoInvSettlementComponentModule } from 'components/function/interface-interco-inv-settlement/interface-interco-inv-settlement.module';
import { InterfaceIntercoInvSettlementPage } from './interface-interco-inv-settlement/interface-interco-inv-settlement-page.component';

@NgModule({
  declarations: [InterfaceIntercoInvSettlementPage],
  imports: [CommonModule, InterfaceIntercoInvSettlementRoutingModule, InterfaceIntercoInvSettlementComponentModule]
})
export class InterfaceIntercoInvSettlementModule {}
