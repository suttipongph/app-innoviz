import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { InterfaceIntercoInvSettlementPage } from './interface-interco-inv-settlement/interface-interco-inv-settlement-page.component';
const routes: Routes = [
  {
    path: '',
    component: InterfaceIntercoInvSettlementPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InterfaceIntercoInvSettlementRoutingModule {}
