import { Component, OnInit } from '@angular/core';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-interface-interco-inv-settlement-page',
  templateUrl: './interface-interco-inv-settlement-page.component.html',
  styleUrls: ['./interface-interco-inv-settlement-page.component.scss']
})
export class InterfaceIntercoInvSettlementPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.INTERFACE_INTERCO_INV_SETTLEMENT,
      servicePath: `Accounting/Function/InterfaceIntercoInvSettlement`
    };
  }
}
