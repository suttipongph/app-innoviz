import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InterfaceIntercoInvSettlementPage } from './interface-interco-inv-settlement-page.component';

describe('InterfaceIntercoInvSettlementPageComponent', () => {
  let component: InterfaceIntercoInvSettlementPage;
  let fixture: ComponentFixture<InterfaceIntercoInvSettlementPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InterfaceIntercoInvSettlementPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InterfaceIntercoInvSettlementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
