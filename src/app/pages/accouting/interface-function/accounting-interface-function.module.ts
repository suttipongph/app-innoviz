import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountingInterfaceFunctionRoutingModule } from './accounting-interface-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, AccountingInterfaceFunctionRoutingModule]
})
export class AccountingInterfaceFunctionModule {}
