import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InterfaceVendorInvoiceStagingFunctionRoutingModule } from './interface-vendor-invoice-staging-function-routing.module';
import { SharedModule } from 'shared/shared.module';
import { InterfaceVendorInvoiceStagingPage } from './interface-vendor-invoice-staging/interface-vendor-invoice-staging.page';
import { InterfaceVendorInvoiceStagingComponentModule } from 'components/function/interface-vendor-invoice/interface-vendor-invoice.module';

@NgModule({
  declarations: [InterfaceVendorInvoiceStagingPage],
  imports: [CommonModule, SharedModule, InterfaceVendorInvoiceStagingFunctionRoutingModule, InterfaceVendorInvoiceStagingComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InterfaceVendorInvoiceStagingFunctionModule {}
