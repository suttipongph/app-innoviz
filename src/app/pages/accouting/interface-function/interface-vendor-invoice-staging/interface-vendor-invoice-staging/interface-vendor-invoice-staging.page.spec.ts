import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InterfaceVendorInvoiceStagingPage } from './interface-vendor-invoice-staging.page';

describe('PostPage', () => {
  let component: InterfaceVendorInvoiceStagingPage;
  let fixture: ComponentFixture<InterfaceVendorInvoiceStagingPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InterfaceVendorInvoiceStagingPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InterfaceVendorInvoiceStagingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
