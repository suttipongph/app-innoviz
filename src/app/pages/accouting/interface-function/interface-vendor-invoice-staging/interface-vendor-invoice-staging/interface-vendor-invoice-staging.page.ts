import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN, ROUTE_MASTER_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'interface-vendor-invoice-staging-page',
  templateUrl: './interface-vendor-invoice-staging.page.html',
  styleUrls: ['./interface-vendor-invoice-staging.page.scss']
})
export class InterfaceVendorInvoiceStagingPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.GENERATE_DUMMY_RECORD,
      servicePath: `Accounting/Function/InterfaceVendorInvoiceStaging`
    };
  }
}
