import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'function/interfaceaccountingstaging',
    loadChildren: () =>
      import('./interface-accounting-staging/interface-accounting-staging-function.module').then((m) => m.InterfaceAccountingStagingFunctionModule)
  },
  {
    path: 'function/interfacevendorinvoicestaging',
    loadChildren: () =>
      import('./interface-vendor-invoice-staging/interface-vendor-invoice-staging-function.module').then(
        (m) => m.InterfaceVendorInvoiceStagingFunctionModule
      )
  },
  {
    path: 'function/interfaceintercoinvsettlement',
    loadChildren: () =>
      import('./interface-interco-inv-settlement/interface-interco-inv-settlement.module').then((m) => m.InterfaceIntercoInvSettlementModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountingInterfaceFunctionRoutingModule {}
