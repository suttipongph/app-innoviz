import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ProcessTransListPage } from './process-transaction-list.page';

describe('ProcessTransactionListPage', () => {
  let component: ProcessTransListPage;
  let fixture: ComponentFixture<ProcessTransListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProcessTransListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessTransListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
