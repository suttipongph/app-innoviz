import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';

import { OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'intercom-invoice-setttle-staging-item.page',
  templateUrl: './intercom-invoice-setttle-staging-item.page.html',
  styleUrls: ['./intercom-invoice-setttle-staging-item.page.scss']
})
export class IntercompanyInvoiceSettlementStagingItemPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.INTERCOMPANY_INVOICE_SETTLEMENT_STAGING,
      servicePath: `StagingTableIntercoInvSettle`
    };
  }
}
