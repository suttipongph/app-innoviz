import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProcessTransactionRoutingModule } from './process-transaction-routing.module';
import { ProcessTransListPage } from './process-transaction-list/process-transaction-list.page';
import { ProcessTransItemPage } from './process-transaction-item/process-transaction-item.page';
import { SharedModule } from 'shared/shared.module';
import { ProcessTransComponentModule } from 'components/process-trans/process-trans.module';

@NgModule({
  declarations: [ProcessTransListPage, ProcessTransItemPage],
  imports: [CommonModule, SharedModule, ProcessTransactionRoutingModule, ProcessTransComponentModule],

  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProcessTransactionModule {}
