import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, Operators, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN, SortType, StagingBatchStatus } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'process-transaction-list-page',
  templateUrl: './process-transaction-list.page.html',
  styleUrls: ['./process-transaction-list.page.scss']
})
export class ProcessTransListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;

  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'processTransGUID';

    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.PROCESS_TRANSACTION,
      servicePath: `ProcessTrans`
    };
  }
}
