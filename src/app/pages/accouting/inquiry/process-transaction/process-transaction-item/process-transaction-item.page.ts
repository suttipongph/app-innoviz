import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';

import { OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'process-transaction-item.page',
  templateUrl: './process-transaction-item.page.html',
  styleUrls: ['./process-transaction-item.page.scss']
})
export class ProcessTransItemPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.PROCESS_TRANSACTION,
      servicePath: `ProcessTrans`
    };
  }
}
