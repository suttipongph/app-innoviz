import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StagingTableVendorInfoListPage } from './staging-table-vendor-info-list/staging-table-vendor-info-list.page';
import { StagingTableVendorInfoItemPage } from './staging-table-vendor-info-item/staging-table-vendor-info-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: StagingTableVendorInfoListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: StagingTableVendorInfoItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StagingTableStagingTableVendorInfoRoutingModule {}
