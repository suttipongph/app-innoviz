import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'staging-table-vendor-info-list-page',
  templateUrl: './staging-table-vendor-info-list.page.html',
  styleUrls: ['./staging-table-vendor-info-list.page.scss']
})
export class StagingTableVendorInfoListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canDelete = false;
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.STAGING_TABLE_VENDOR_INFO,
      servicePath: `StagingTable/RelatedInfo/StagingTableVendorInfo`
    };
  }
}
