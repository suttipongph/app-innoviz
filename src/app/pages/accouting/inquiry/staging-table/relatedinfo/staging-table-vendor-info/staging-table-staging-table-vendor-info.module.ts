import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StagingTableVendorInfoListPage } from './staging-table-vendor-info-list/staging-table-vendor-info-list.page';
import { StagingTableVendorInfoItemPage } from './staging-table-vendor-info-item/staging-table-vendor-info-item.page';
import { SharedModule } from 'shared/shared.module';
import { StagingTableStagingTableVendorInfoRoutingModule } from './staging-table-staging-table-vendor-info-routing.module';
import { StagingTableVendorInfoComponentModule } from 'components/staging-table-vendor-info/staging-table-vendor-info.module';

@NgModule({
  declarations: [StagingTableVendorInfoListPage, StagingTableVendorInfoItemPage],
  imports: [CommonModule, SharedModule, StagingTableStagingTableVendorInfoRoutingModule, StagingTableVendorInfoComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StagingTableStagingTableVendorInfoModule {}
