import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'staging-table-vendor-info-item.page',
  templateUrl: './staging-table-vendor-info-item.page.html',
  styleUrls: ['./staging-table-vendor-info-item.page.scss']
})
export class StagingTableVendorInfoItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.STAGING_TABLE_VENDOR_INFO,
      servicePath: `StagingTable/RelatedInfo/StagingTableVendorInfo`
    };
  }
}
