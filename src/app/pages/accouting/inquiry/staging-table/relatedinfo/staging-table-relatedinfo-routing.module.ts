import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'stagingtablevendorinfo',
    loadChildren: () =>
      import('./staging-table-vendor-info/staging-table-staging-table-vendor-info.module').then((m) => m.StagingTableStagingTableVendorInfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StagingTableRelatedinfoRoutingModule {}
