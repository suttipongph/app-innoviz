import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StagingTableRelatedinfoRoutingModule } from './staging-table-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, StagingTableRelatedinfoRoutingModule]
})
export class StagingTableRelatedinfoModule {}
