import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'staging-table-item.page',
  templateUrl: './staging-table-item.page.html',
  styleUrls: ['./staging-table-item.page.scss']
})
export class StagingTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.STAGINGTABLE, servicePath: 'StagingTable' };
  }
}
