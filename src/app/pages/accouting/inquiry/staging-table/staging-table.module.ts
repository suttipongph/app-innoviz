import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StagingTableRoutingModule } from './staging-table-routing.module';
import { StagingTableListPage } from './staging-table-list/staging-table-list.page';
import { StagingTableItemPage } from './staging-table-item/staging-table-item.page';
import { StagingTableComponentModule } from 'components/staging-table/staging-table.module';
import { SharedModule } from 'shared/shared.module';

@NgModule({
  declarations: [StagingTableListPage, StagingTableItemPage],
  imports: [CommonModule, SharedModule, StagingTableRoutingModule, StagingTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StagingTableModule {}
