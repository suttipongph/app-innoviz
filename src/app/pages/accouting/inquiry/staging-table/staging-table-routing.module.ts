import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StagingTableListPage } from './staging-table-list/staging-table-list.page';
import { StagingTableItemPage } from './staging-table-item/staging-table-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: StagingTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: StagingTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/staging-table-relatedinfo.module').then((m) => m.StagingTableRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StagingTableRoutingModule {}
