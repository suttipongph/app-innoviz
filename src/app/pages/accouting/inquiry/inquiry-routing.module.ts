import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [
  { path: 'stagingtable', loadChildren: () => import('./staging-table/staging-table.module').then((m) => m.StagingTableModule) },
  {
    path: 'processtransaction',
    loadChildren: () => import('./process-transaction/process-transaction.module').then((m) => m.ProcessTransactionModule)
  },
  {
    path: 'intercompanyinvoicesettlementstaging',
    loadChildren: () =>
      import('./intercom-invoice-setttle-staging/intercom-invoice-setttle-staging.module').then((m) => m.IntercompanyInvoiceSettlementStagingModule)
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InquiryRoutingModule {}
