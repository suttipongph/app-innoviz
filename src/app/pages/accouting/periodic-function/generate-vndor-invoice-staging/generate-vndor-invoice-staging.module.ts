import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenerateVndorInvoiceStagingRoutingModule } from './generate-vndor-invoice-staging-routing.module';
import { SharedModule } from 'shared/shared.module';
import { GenerateVndorInvoiceStagingPage } from './generate-vndor-invoice-staging/generate-vndor-invoice-staging.page';
import { GenerateVendorInvoiceStagingComponentModule } from 'components/function/generate-vendor-invoice-staging/generate-vendor-invoice-staging.module';

@NgModule({
  declarations: [GenerateVndorInvoiceStagingPage],
  imports: [CommonModule, SharedModule, GenerateVndorInvoiceStagingRoutingModule, GenerateVendorInvoiceStagingComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GenerateVndorInvoiceStagingModule {}
