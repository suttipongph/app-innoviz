import { Component, OnInit } from '@angular/core';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'generate-vndor-invoice-staging.page',
  templateUrl: './generate-vndor-invoice-staging.page.html',
  styleUrls: ['./generate-vndor-invoice-staging.page.scss']
})
export class GenerateVndorInvoiceStagingPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_FUNCTION_GEN.GENERATE_VENDOR_INVOICE_STAGING, servicePath: 'Accounting/Function/GenerateVendorInvoiceStaging' };
  }
}
