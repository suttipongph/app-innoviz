import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { GenerateVndorInvoiceStagingPage } from './generate-vndor-invoice-staging/generate-vndor-invoice-staging.page';

const routes: Routes = [
  {
    path: '',
    component: GenerateVndorInvoiceStagingPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GenerateVndorInvoiceStagingRoutingModule {}
