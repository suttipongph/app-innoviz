import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { GenInterestRealizeProcessTransPage } from './gen-interest-realize-process-trans/gen-interest-realize-process-trans.page';

const routes: Routes = [
  {
    path: '',
    component: GenInterestRealizeProcessTransPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GenInterestRealizeProcessTransFunctionRoutingModule {}
