import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN, ROUTE_MASTER_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-gen-interest-realize-process-trans',
  templateUrl: './gen-interest-realize-process-trans.page.html',
  styleUrls: ['./gen-interest-realize-process-trans.page.scss']
})
export class GenInterestRealizeProcessTransPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.INCOME_REALIZATION,
      servicePath: `Accounting/Function/IncomeRealization`
    };
  }
}
