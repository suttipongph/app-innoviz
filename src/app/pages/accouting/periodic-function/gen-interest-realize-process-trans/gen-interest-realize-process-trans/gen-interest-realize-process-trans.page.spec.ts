import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GenInterestRealizeProcessTransPage } from './gen-interest-realize-process-trans.page';

describe('PostPage', () => {
  let component: GenInterestRealizeProcessTransPage;
  let fixture: ComponentFixture<GenInterestRealizeProcessTransPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GenInterestRealizeProcessTransPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenInterestRealizeProcessTransPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
