import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenInterestRealizeProcessTransFunctionRoutingModule } from './gen-interest-realize-process-trans-function-routing.module';
import { SharedModule } from 'shared/shared.module';
import { GenInterestRealizeProcessTransPage } from './gen-interest-realize-process-trans/gen-interest-realize-process-trans.page';
import { GenInterestRealizeProcessTransComponentModule } from 'components/function/gen-interest-realize-process-trans/gen-interest-realize-process-trans.module';

@NgModule({
  declarations: [GenInterestRealizeProcessTransPage],
  imports: [CommonModule, SharedModule, GenInterestRealizeProcessTransFunctionRoutingModule, GenInterestRealizeProcessTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GenInterestRealizeProcessTransFunctionModule {}
