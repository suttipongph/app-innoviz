import { Component, OnInit } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';

@Component({
  selector: 'generate-accounting-staging.page',
  templateUrl: './generate-accounting-staging.page.html',
  styleUrls: ['./generate-accounting-staging.page.scss']
})
export class GenerateAccountingStagingPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_FUNCTION_GEN.GENERATE_ACCOUNTING_STAGING, servicePath: 'Accounting/Function/GenerateAccountingStaging' };
  }
}
