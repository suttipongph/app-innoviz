import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenerateAccountingStagingRoutingModule } from './generate-accounting-staging-routing.module';
import { SharedModule } from 'shared/shared.module';
import { GenerateAccountingStagingComponentModule } from 'components/function/generate-accounting-staging/generate-accounting-staging.module';
import { GenerateAccountingStagingPage } from './generate-accounting-staging/generate-accounting-staging.page';

@NgModule({
  declarations: [GenerateAccountingStagingPage],
  imports: [CommonModule, SharedModule, GenerateAccountingStagingRoutingModule, GenerateAccountingStagingComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GenerateAccountingStagingModule {}
