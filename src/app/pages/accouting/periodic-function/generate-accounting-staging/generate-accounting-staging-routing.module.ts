import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { GenerateAccountingStagingPage } from './generate-accounting-staging/generate-accounting-staging.page';

const routes: Routes = [
  {
    path: '',
    component: GenerateAccountingStagingPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GenerateAccountingStagingRoutingModule {}
