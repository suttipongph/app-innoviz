import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountingPeriodicFunctionRoutingModule } from './accounting-periodic-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, AccountingPeriodicFunctionRoutingModule]
})
export class AccountingPeriodicFunctionModule {}
