import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'function/incomerealization',
    loadChildren: () =>
      import('./gen-interest-realize-process-trans/gen-interest-realize-process-trans-function.module').then(
        (m) => m.GenInterestRealizeProcessTransFunctionModule
      )
  },
  {
    path: 'function/generateaccountingstaging',
    loadChildren: () => import('./generate-accounting-staging/generate-accounting-staging.module').then((m) => m.GenerateAccountingStagingModule)
  },
  {
    path: 'function/generatevendorinvoicestaging',
    loadChildren: () =>
      import('./generate-vndor-invoice-staging/generate-vndor-invoice-staging.module').then((m) => m.GenerateVndorInvoiceStagingModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountingPeriodicFunctionRoutingModule {}
