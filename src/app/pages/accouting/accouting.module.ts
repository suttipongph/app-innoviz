import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccoutingRoutingModule } from './accouting-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, AccoutingRoutingModule]
})
export class AccoutingModule {}
