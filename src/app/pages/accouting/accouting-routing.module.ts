import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'inquiry',
    loadChildren: () => import('./inquiry/inquiry.module').then((m) => m.InquiryModule)
  },
  {
    path: 'interface',
    loadChildren: () => import('./interface-function/accounting-interface-function.module').then((m) => m.AccountingInterfaceFunctionModule)
  },
  {
    path: 'periodic',
    loadChildren: () => import('./periodic-function/accounting-periodic-function.module').then((m) => m.AccountingPeriodicFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccoutingRoutingModule {}
