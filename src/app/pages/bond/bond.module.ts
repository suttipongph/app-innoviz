import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BondRoutingModule } from './bond-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, BondRoutingModule]
})
export class BondModule {}
