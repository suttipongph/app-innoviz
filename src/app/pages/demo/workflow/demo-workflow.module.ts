import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'primeng/api';
import { DemoWorkflowRoutingModule } from './demo-workflow-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, DemoWorkflowRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DemoWorkflowModule {}
