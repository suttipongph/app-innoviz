import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoActionWorkflowPage } from './demo-action-workflow.page';

describe('DemoWorkflowComponent', () => {
  let component: DemoActionWorkflowPage;
  let fixture: ComponentFixture<DemoActionWorkflowPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DemoActionWorkflowPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoActionWorkflowPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
