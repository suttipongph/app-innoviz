import { Component, OnInit } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-demo-workflow',
  templateUrl: './demo-action-workflow.page.html',
  styleUrls: ['./demo-action-workflow.page.scss']
})
export class DemoActionWorkflowPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: 'actiondemo',
      servicePath: 'Demo/Workflow/ActionWorkflow'
    };
  }
}
