import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { DemoActionWorkflowPage } from './demo-action-workflow/demo-action-workflow.page';
import { DemoActionWorkflowRoutingModule } from './demo-action-workflow-routing.module';
import { DemoComponentModule } from 'components/demo/demo.module';

@NgModule({
  declarations: [DemoActionWorkflowPage],
  imports: [CommonModule, SharedModule, DemoActionWorkflowRoutingModule, DemoComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DemoActionWorkflowModule {}
