import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { DemoActionWorkflowPage } from './demo-action-workflow/demo-action-workflow.page';

const routes: Routes = [
  {
    path: '',
    component: DemoActionWorkflowPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoActionWorkflowRoutingModule {}
