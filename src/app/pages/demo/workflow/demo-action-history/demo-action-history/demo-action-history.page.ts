import { Component, OnInit } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-demo-action-history',
  templateUrl: './demo-action-history.page.html',
  styleUrls: ['./demo-action-history.page.scss']
})
export class DemoActionHistoryPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: 'actionhistory',
      servicePath: 'Demo/Workflow/ActionHistory'
    };
  }
}
