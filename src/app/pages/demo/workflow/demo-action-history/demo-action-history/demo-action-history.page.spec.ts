import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoActionHistoryPage } from './demo-action-history.page';

describe('DemoActionHistoryPage', () => {
  let component: DemoActionHistoryPage;
  let fixture: ComponentFixture<DemoActionHistoryPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DemoActionHistoryPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoActionHistoryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
