import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { DemoComponentModule } from 'components/demo/demo.module';
import { DemoActionHistoryRoutingModule } from './demo-action-history-routing.module';
import { DemoActionHistoryPage } from './demo-action-history/demo-action-history.page';
import { ActionHistoryComponentModule } from 'components/action-history/action-history.module';

@NgModule({
  declarations: [DemoActionHistoryPage],
  imports: [CommonModule, SharedModule, DemoActionHistoryRoutingModule, ActionHistoryComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DemoActionHistoryModule {}
