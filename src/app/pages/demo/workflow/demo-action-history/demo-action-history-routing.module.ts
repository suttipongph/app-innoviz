import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { DemoActionHistoryPage } from './demo-action-history/demo-action-history.page';

const routes: Routes = [
  {
    path: '',
    component: DemoActionHistoryPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoActionHistoryRoutingModule {}
