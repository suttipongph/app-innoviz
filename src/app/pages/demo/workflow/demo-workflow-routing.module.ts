import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'actiondemo',
    loadChildren: () => import('./demo-action-workflow/demo-action-workflow.module').then((m) => m.DemoActionWorkflowModule)
  },
  {
    path: 'actionhistory',
    loadChildren: () => import('./demo-action-history/demo-action-history.module').then((m) => m.DemoActionHistoryModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoWorkflowRoutingModule {}
