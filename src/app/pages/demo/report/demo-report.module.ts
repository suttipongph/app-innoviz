import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'primeng/api';
import { DemoReportRoutingModule } from './demo-report-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, DemoReportRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DemoReportModule {}
