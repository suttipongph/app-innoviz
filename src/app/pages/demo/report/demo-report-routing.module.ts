import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'demoreportdemo',
    loadChildren: () => import('./demo-report-demo/demo-report-demo.module').then((m) => m.DemoReportDemoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoReportRoutingModule {}
