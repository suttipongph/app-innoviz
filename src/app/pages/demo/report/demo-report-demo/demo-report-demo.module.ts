import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { DemoComponentModule } from 'components/demo/demo.module';
import { DemoReportDemoPage } from './demo-report-demo/demo-report-demo.page';
import { DemoReportDemoRoutingModule } from './demo-report-demo-routing.module';

@NgModule({
  declarations: [DemoReportDemoPage],
  imports: [CommonModule, SharedModule, DemoReportDemoRoutingModule, DemoComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DemoReportDemoModule {}
