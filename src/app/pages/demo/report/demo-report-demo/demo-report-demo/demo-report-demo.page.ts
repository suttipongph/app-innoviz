import { Component, OnInit } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'demo-report-demo-page',
  templateUrl: './demo-report-demo.page.html',
  styleUrls: ['./demo-report-demo.page.scss']
})
export class DemoReportDemoPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: 'demoreportdemo',
      servicePath: 'Demo/Report/DemoReportDemo'
    };
  }
}
