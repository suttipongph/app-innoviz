import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { DemoReportDemoPage } from './demo-report-demo/demo-report-demo.page';

const routes: Routes = [
  {
    path: '',
    component: DemoReportDemoPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoReportDemoRoutingModule {}
