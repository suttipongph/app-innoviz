import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DemoRoutingModule } from './demo-routing.module';
import { DemoListPage } from './demo-list/demo-list.page';
import { DemoItemPage } from './demo-item/demo-item.page';
import { SharedModule } from 'shared/shared.module';
import { DemoComponentModule } from 'components/demo/demo.module';

@NgModule({
  declarations: [DemoListPage, DemoItemPage],
  imports: [CommonModule, SharedModule, DemoRoutingModule, DemoComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DemoModule {}
