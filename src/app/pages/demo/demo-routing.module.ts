import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { DemoItemPage } from './demo-item/demo-item.page';
import { DemoListPage } from './demo-list/demo-list.page';

const routes: Routes = [
  {
    path: '',
    component: DemoListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: DemoItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/workflow',
    loadChildren: () => import('./workflow/demo-workflow.module').then((m) => m.DemoWorkflowModule)
  },
  {
    path: ':id/report',
    loadChildren: () => import('./report/demo-report.module').then((m) => m.DemoReportModule)
  }
  // {
  //   path: ':id/relatedinfo',
  //   loadChildren: () =>
  //     import('../master-information/customer-table/relatedinfo/customer-relatedinfo.module').then((m) => m.CustomerRelatedinfoModule)
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoRoutingModule {}
