import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DemoItemPage } from './Demo-item.page';

describe('DemoItemPage', () => {
  let component: DemoItemPage;
  let fixture: ComponentFixture<DemoItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DemoItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
