import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UIControllerService } from 'core/services/uiController.service';
import { AppConst, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { MenuItem } from 'shared/models/primeModel';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'demo-item-page',
  templateUrl: './demo-item.page.html',
  styleUrls: ['./demo-item.page.scss']
})
export class DemoItemPage implements OnInit {
  pageInfo: PageInformationModel;
  relatedInfoItems: MenuItem[];
  constructor(private translate: TranslateService, private uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
    this.setRelatedInfoOptions();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.DEMO, servicePath: 'Demo' };
  }
  setRelatedInfoOptions(): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.CONTACT_PERSON',
        command: () => this.uiService.toRelatedInfo({ path: ROUTE_RELATED_GEN.CONTACT_PERSON }),
        visible: this.uiService.setAuthorizedBySite([AppConst.BACK]),
        tabindex: '2'
      },
      {
        label: 'LABEL.AUTHORIZED_PERSON',
        command: () => this.uiService.toRelatedInfo({ path: ROUTE_RELATED_GEN.AUTHORIZED_PERSON_TRANS }),
        tabindex: '1'
      },
      {
        label: 'LABEL.CONTACT_TRANSACTIONS',
        command: () => this.uiService.toRelatedInfo({ path: ROUTE_RELATED_GEN.CONTACT })
      }
    ];
  }
}
