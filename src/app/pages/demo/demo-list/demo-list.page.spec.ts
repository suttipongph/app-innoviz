import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DemoListPage } from './demo-list.page';

describe('DemoListPage', () => {
  let component: DemoListPage;
  let fixture: ComponentFixture<DemoListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DemoListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
