import { Component, OnInit } from '@angular/core';
import { ColumnType, ROUTE_MASTER_GEN, SortType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'demo-list-page',
  templateUrl: './demo-list.page.html',
  styleUrls: ['./demo-list.page.scss']
})
export class DemoListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'demoGUID';
    this.option.canCreate = false;
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.BLACKLIST_STATUS_ID',
        textKey: 'demoId',
        type: ColumnType.STRING,
        visibility: false,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: false,
        sorting: SortType.NONE,
        order: 1
      },
      {
        label: 'LABEL.BRANCH_ID',
        textKey: 'brance_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        order: 0
      },
      {
        label: 'LABEL.ACCOUNTING_DATE',
        textKey: 'accoutingDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.DEMO, servicePath: 'Demo' };
  }
}
