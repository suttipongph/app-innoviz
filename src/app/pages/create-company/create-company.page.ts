import { Component, OnInit } from '@angular/core';
import { OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-create-company',
  templateUrl: './create-company.page.html',
  styleUrls: ['./create-company.page.scss']
})
export class CreateCompanyPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }

  setPath(): void {
    this.pageInfo = {
      pagePath: 'initdata',
      servicePath: `Company`
    };
  }
}
