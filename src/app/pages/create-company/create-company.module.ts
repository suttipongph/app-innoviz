import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { CreateCompanyPage } from './create-company.page';
import { CreateCompanyRoutingModule } from './create-company-routing.module';
import { CreateCompanyComponentModule } from 'components/setup/company/create-company/create-company.module';

@NgModule({
  declarations: [CreateCompanyPage],
  imports: [CommonModule, SharedModule, CreateCompanyRoutingModule, CreateCompanyComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreateCompanyModule {}
