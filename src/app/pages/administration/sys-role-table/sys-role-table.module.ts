import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SysRoleTableRoutingModule } from './sys-role-table-routing.module';
import { SysRoleTableListPage } from './sys-role-table-list/sys-role-table-list.page';
import { SysRoleTableItemPage } from './sys-role-table-item/sys-role-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { SysRoleTableComponentModule } from 'components/sys-role-table/sys-role-table.module';

@NgModule({
  declarations: [SysRoleTableListPage, SysRoleTableItemPage],
  imports: [CommonModule, SharedModule, SysRoleTableRoutingModule, SysRoleTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SysRoleTableModule {}
