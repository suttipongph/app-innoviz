import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ImportAccessRightPage } from './import-access-right/import-access-right.page';

const routes: Routes = [
  {
    path: '',
    component: ImportAccessRightPage,
    canActivate: [AuthGuardService]
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SysRoleTableImportAccessRightFunctionRoutingModule {}
