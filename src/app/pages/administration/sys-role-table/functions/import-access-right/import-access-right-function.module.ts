import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { ImportAccessRightPage } from './import-access-right/import-access-right.page';
import { SysRoleTableImportAccessRightFunctionRoutingModule } from './import-access-right-function-routing.module';
import { ImportAccessRightComponentModule } from 'components/sys-role-table/import-access-right/import-access-right.module';

@NgModule({
  declarations: [ImportAccessRightPage],
  imports: [CommonModule, SharedModule, SysRoleTableImportAccessRightFunctionRoutingModule, ImportAccessRightComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SysRoleTableImportAccessRightFunctionModule {}
