import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'import-access-right-page',
  templateUrl: './import-access-right.page.html',
  styleUrls: ['./import-access-right.page.scss']
})
export class ImportAccessRightPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.IMPORT_ACCESS_RIGHT,
      servicePath: `SysAccessRight/Function/ImportAccessRight`
    };
  }
}
