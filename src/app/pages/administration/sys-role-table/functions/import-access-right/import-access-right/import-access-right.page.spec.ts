import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportAccessRightPage } from './import-access-right.page';

describe('ImportAccessRightComponent', () => {
  let component: ImportAccessRightPage;
  let fixture: ComponentFixture<ImportAccessRightPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImportAccessRightPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportAccessRightPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
