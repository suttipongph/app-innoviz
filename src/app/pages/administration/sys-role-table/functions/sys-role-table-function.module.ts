import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'primeng/api';
import { SysRoleTableFunctionRoutingModule } from './sys-role-table-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, SysRoleTableFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SysRoleTableFunctionModule {}
