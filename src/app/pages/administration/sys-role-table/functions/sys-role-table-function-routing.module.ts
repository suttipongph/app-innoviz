import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'importaccessright',
    loadChildren: () => import('./import-access-right/import-access-right-function.module').then((m) => m.SysRoleTableImportAccessRightFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SysRoleTableFunctionRoutingModule {}
