import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SysRoleTableListPage } from './sys-role-table-list/sys-role-table-list.page';
import { SysRoleTableItemPage } from './sys-role-table-item/sys-role-table-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuardService],
    component: SysRoleTableListPage
  },
  {
    path: ':id',
    canActivate: [AuthGuardService],
    component: SysRoleTableItemPage
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/sys-role-table-function.module').then((m) => m.SysRoleTableFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SysRoleTableRoutingModule {}
