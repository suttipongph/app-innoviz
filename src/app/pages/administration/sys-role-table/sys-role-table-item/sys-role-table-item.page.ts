import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'sys-role-table-item.page',
  templateUrl: './sys-role-table-item.page.html',
  styleUrls: ['./sys-role-table-item.page.scss']
})
export class SysRoleTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.SYSROLETABLE, servicePath: 'SysAccessRight' };
  }
}
