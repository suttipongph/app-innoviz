import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: 'sysusertable',
    loadChildren: () => import('pages/administration/sys-user-table/sys-user-table.module').then((m) => m.SysUserTableModule)
  },
  {
    path: 'sysroletable',
    loadChildren: () => import('pages/administration/sys-role-table/sys-role-table.module').then((m) => m.SysRoleTableModule)
  },
  {
    path: 'inquiry',
    loadChildren: () => import('pages/administration/inquiry/inquiry.module').then((m) => m.AdminInquiryModule)
  },
  {
    path: 'periodic',
    loadChildren: () => import('pages/administration/periodic-function/admin-periodic-function.module').then((m) => m.AdminPeriodicFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
