import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SysUserTableRoutingModule } from './sys-user-table-routing.module';
import { SysUserTableListPage } from './sys-user-table-list/sys-user-table-list.page';
import { SysUserTableItemPage } from './sys-user-table-item/sys-user-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { SysUserTableComponentModule } from 'components/sys-user-table/sys-user-table.module';

@NgModule({
  declarations: [SysUserTableListPage, SysUserTableItemPage],
  imports: [CommonModule, SharedModule, SysUserTableRoutingModule, SysUserTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SysUserTableModule {}
