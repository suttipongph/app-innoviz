import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'sys-user-table-item.page',
  templateUrl: './sys-user-table-item.page.html',
  styleUrls: ['./sys-user-table-item.page.scss']
})
export class SysUserTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.SYSUSERTABLE, servicePath: 'SysUser' };
  }
}
