import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ActiveUserPage } from './active-user/active-user.page';

const routes: Routes = [
  {
    path: '',
    component: ActiveUserPage,
    canActivate: [AuthGuardService]
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SysUserTableActiveUserFunctionRoutingModule {}
