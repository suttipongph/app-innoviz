import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveUserPage } from './active-user.page';

describe('ActiveUserComponent', () => {
  let component: ActiveUserPage;
  let fixture: ComponentFixture<ActiveUserPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActiveUserPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveUserPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
