import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { ActiveUserPage } from './active-user/active-user.page';
import { SysUserTableActiveUserFunctionRoutingModule } from './active-user-function-routing.module';
import { InactiveUserComponentModule } from 'components/sys-user-table/inactive-user/inactive-user.module';

@NgModule({
  declarations: [ActiveUserPage],
  imports: [CommonModule, SharedModule, SysUserTableActiveUserFunctionRoutingModule, InactiveUserComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SysUserTableActiveUserFunctionModule {}
