import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'primeng/api';
import { SysUserTableFunctionRoutingModule } from './sys-user-table-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, SysUserTableFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SysUserTableFunctionModule {}
