import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InactiveUserPage } from './inactive-user.page';

describe('InactiveUserComponent', () => {
  let component: InactiveUserPage;
  let fixture: ComponentFixture<InactiveUserPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InactiveUserPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InactiveUserPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
