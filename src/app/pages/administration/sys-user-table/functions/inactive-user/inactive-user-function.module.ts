import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { InactiveUserPage } from './inactive-user/inactive-user.page';
import { SysUserTableInactiveUserFunctionRoutingModule } from './inactive-user-function-routing.module';
import { InactiveUserComponentModule } from 'components/sys-user-table/inactive-user/inactive-user.module';

@NgModule({
  declarations: [InactiveUserPage],
  imports: [CommonModule, SharedModule, SysUserTableInactiveUserFunctionRoutingModule, InactiveUserComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SysUserTableInactiveUserFunctionModule {}
