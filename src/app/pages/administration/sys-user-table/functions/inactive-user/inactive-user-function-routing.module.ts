import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { InactiveUserPage } from './inactive-user/inactive-user.page';

const routes: Routes = [
  {
    path: '',
    component: InactiveUserPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SysUserTableInactiveUserFunctionRoutingModule {}
