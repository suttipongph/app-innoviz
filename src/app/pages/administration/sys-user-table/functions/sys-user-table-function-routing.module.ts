import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'inactiveuser',
    loadChildren: () => import('./inactive-user/inactive-user-function.module').then((m) => m.SysUserTableInactiveUserFunctionModule)
  },
  {
    path: 'activeuser',
    loadChildren: () => import('./active-user/active-user-function.module').then((m) => m.SysUserTableActiveUserFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SysUserTableFunctionRoutingModule {}
