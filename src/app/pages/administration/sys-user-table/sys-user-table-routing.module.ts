import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SysUserTableListPage } from './sys-user-table-list/sys-user-table-list.page';
import { SysUserTableItemPage } from './sys-user-table-item/sys-user-table-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuardService],
    component: SysUserTableListPage
  },
  {
    path: ':id',
    canActivate: [AuthGuardService],
    component: SysUserTableItemPage
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/sys-user-table-function.module').then((m) => m.SysUserTableFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SysUserTableRoutingModule {}
