import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'function/userlogcleanup',
    loadChildren: () =>
      import('./user-log-clean-up/user-log-clean-up.module').then(
        (m) => m.UserLogCleanUpModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminPeriodicFunctionRoutingModule {}
