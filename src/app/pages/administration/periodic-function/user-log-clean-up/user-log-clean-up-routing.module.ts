import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { UserLogCleanUpPage } from './user-log-clean-up/user-log-clean-up.page';

const routes: Routes = [
  {
    path: '',
    component: UserLogCleanUpPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserLogCleanUpRoutingModule {}
