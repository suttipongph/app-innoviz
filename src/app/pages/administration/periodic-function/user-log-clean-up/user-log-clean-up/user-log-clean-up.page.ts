import { Component, OnInit } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';

@Component({
  selector: 'user-log-clean-up.page',
  templateUrl: './user-log-clean-up.page.html',
  styleUrls: ['./user-log-clean-up.page.scss']
})
export class UserLogCleanUpPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_FUNCTION_GEN.USER_LOG_CLEANUP, servicePath: 'SysUserLog/Function/UserLogCleanUp' };
  }
}
