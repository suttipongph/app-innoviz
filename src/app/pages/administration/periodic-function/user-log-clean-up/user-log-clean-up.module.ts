import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserLogCleanUpRoutingModule } from './user-log-clean-up-routing.module';
import { SharedModule } from 'shared/shared.module';
import { UserLogCleanUpComponentModule } from 'components/function/user-log-clean-up/user-log-clean-up.module';
import { UserLogCleanUpPage } from './user-log-clean-up/user-log-clean-up.page';

@NgModule({
  declarations: [UserLogCleanUpPage],
  imports: [CommonModule, SharedModule, UserLogCleanUpRoutingModule, UserLogCleanUpComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UserLogCleanUpModule {}
