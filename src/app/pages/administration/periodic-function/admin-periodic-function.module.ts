import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminPeriodicFunctionRoutingModule } from './admin-periodic-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, AdminPeriodicFunctionRoutingModule]
})
export class AdminPeriodicFunctionModule {}
