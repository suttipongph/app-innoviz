import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { SysUserLogListPage } from './sys-user-log-list/sys-user-log-list.page';

const routes: Routes = [
  {
    path: '',
    component: SysUserLogListPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SysUserLogRoutingModule {}
