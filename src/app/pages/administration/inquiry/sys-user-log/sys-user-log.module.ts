import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { SysUserLogListPage } from './sys-user-log-list/sys-user-log-list.page';
import { SysUserLogRoutingModule } from './sys-user-log-routing.module';
import { SysUserLogComponentModule } from 'components/sys-user-log/sys-user-log.module';

@NgModule({
  declarations: [SysUserLogListPage],
  imports: [CommonModule, SharedModule, SysUserLogRoutingModule, SysUserLogComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SysUserLogModule {}
