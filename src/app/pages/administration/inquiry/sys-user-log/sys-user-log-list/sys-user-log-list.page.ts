import { Component, OnInit } from '@angular/core';
import { ROUTE_ADMIN, ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'sys-user-log-list-page',
  templateUrl: './sys-user-log-list.page.html',
  styleUrls: ['./sys-user-log-list.page.scss']
})
export class SysUserLogListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'sessionId';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_ADMIN.INQUIRY.SYSUSERLOG, servicePath: 'SysUserLog' };
  }
}
