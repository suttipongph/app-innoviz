import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminInquiryRoutingModule } from './inquiry-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, AdminInquiryRoutingModule]
})
export class AdminInquiryModule {}
