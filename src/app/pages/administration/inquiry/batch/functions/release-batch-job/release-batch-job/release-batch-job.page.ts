import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'release-batch-job-page',
  templateUrl: './release-batch-job.page.html',
  styleUrls: ['./release-batch-job.page.scss']
})
export class ReleaseBatchJobPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.RELEASE_BATCH_JOB,
      servicePath: `BatchObject/Function/ReleaseBatchJob`
    };
  }
}
