import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ReleaseBatchJobPage } from './release-batch-job/release-batch-job.page';

const routes: Routes = [
  {
    path: '',
    component: ReleaseBatchJobPage,
    canActivate:[AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReleaseBatchJobFunctionRoutingModule {}
