import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReleaseBatchJobPage } from './release-batch-job.page';

describe('ReleaseBatchJobPage', () => {
  let component: ReleaseBatchJobPage;
  let fixture: ComponentFixture<ReleaseBatchJobPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReleaseBatchJobPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReleaseBatchJobPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
