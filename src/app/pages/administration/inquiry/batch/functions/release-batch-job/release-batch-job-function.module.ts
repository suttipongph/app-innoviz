import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReleaseBatchJobFunctionRoutingModule } from './release-batch-job-function-routing.module';
import { ReleaseBatchJobPage } from './release-batch-job/release-batch-job.page';
import { SharedModule } from 'shared/shared.module';
import { ReleaseBatchJobComponentModule } from 'components/batch/release-batch-job/release-batch-job.module';

@NgModule({
  declarations: [ReleaseBatchJobPage],
  imports: [CommonModule, SharedModule, ReleaseBatchJobFunctionRoutingModule, ReleaseBatchJobComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReleaseBatchJobFunctionModule {}
