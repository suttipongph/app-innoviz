import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CancelBatchJobPage } from './cancel-batch-job/cancel-batch-job.page';
import { SharedModule } from 'shared/shared.module';
import { CancelBatchJobComponentModule } from 'components/batch/cancel-batch-job/cancel-batch-job.module';
import { CancelBatchJobFunctionRoutingModule } from './cancel-batch-job-function-routing.module';

@NgModule({
  declarations: [CancelBatchJobPage],
  imports: [CommonModule, SharedModule, CancelBatchJobFunctionRoutingModule, CancelBatchJobComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CancelBatchJobFunctionModule {}
