import { Component, OnInit } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { UIControllerService } from 'core/services/uiController.service';

@Component({
  selector: 'cancel-batch-job-page',
  templateUrl: './cancel-batch-job.page.html',
  styleUrls: ['./cancel-batch-job.page.scss']
})
export class CancelBatchJobPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.CANCEL_BATCH_JOB,
      servicePath: `BatchObject/Function/CancelBatchJob`
    };
  }
}
