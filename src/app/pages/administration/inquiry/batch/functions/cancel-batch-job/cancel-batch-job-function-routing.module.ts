import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CancelBatchJobPage } from './cancel-batch-job/cancel-batch-job.page';

const routes: Routes = [
  {
    path: '',
    component: CancelBatchJobPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CancelBatchJobFunctionRoutingModule {}
