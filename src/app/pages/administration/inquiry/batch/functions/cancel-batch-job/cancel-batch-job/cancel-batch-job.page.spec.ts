import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CancelBatchJobPage } from './cancel-batch-job.page';

describe('CancelBatchJobPage', () => {
  let component: CancelBatchJobPage;
  let fixture: ComponentFixture<CancelBatchJobPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CancelBatchJobPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelBatchJobPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
