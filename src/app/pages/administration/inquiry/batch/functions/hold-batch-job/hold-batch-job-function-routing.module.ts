import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { HoldBatchJobPage } from './hold-batch-job/hold-batch-job.page';

const routes: Routes = [
  {
    path: '',
    component: HoldBatchJobPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HoldBatchJobFunctionRoutingModule {}
