import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HoldBatchJobPage } from './hold-batch-job.page';

describe('HoldBatchJobPage', () => {
  let component: HoldBatchJobPage;
  let fixture: ComponentFixture<HoldBatchJobPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HoldBatchJobPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HoldBatchJobPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
