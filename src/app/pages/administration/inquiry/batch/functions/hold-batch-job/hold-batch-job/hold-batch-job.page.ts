import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'hold-batch-job-page',
  templateUrl: './hold-batch-job.page.html',
  styleUrls: ['./hold-batch-job.page.scss']
})
export class HoldBatchJobPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.HOLD_BATCH_JOB,
      servicePath: `BatchObject/Function/HoldBatchJob`
    };
  }
}
