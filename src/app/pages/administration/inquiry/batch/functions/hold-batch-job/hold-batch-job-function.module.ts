import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HoldBatchJobFunctionRoutingModule } from './hold-batch-job-function-routing.module';
import { HoldBatchJobPage } from './hold-batch-job/hold-batch-job.page';
import { SharedModule } from 'shared/shared.module';
import { HoldBatchJobComponentModule } from 'components/batch/hold-batch-job/hold-batch-job.module';

@NgModule({
  declarations: [HoldBatchJobPage],
  imports: [CommonModule, SharedModule, HoldBatchJobFunctionRoutingModule, HoldBatchJobComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HoldBatchJobFunctionModule {}
