import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CleanUpBatchHistoryFunctionRoutingModule } from './clean-up-batch-history-function-routing.module';
import { CleanUpBatchHistoryPage } from './clean-up-batch-history/clean-up-batch-history.page';
import { SharedModule } from 'shared/shared.module';
import { CleanUpBatchHistoryComponentModule } from 'components/batch/clean-up-batch-history/clean-up-batch-history.module';

@NgModule({
  declarations: [CleanUpBatchHistoryPage],
  imports: [CommonModule, SharedModule, CleanUpBatchHistoryFunctionRoutingModule, CleanUpBatchHistoryComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CleanUpBatchHistoryFunctionModule {}
