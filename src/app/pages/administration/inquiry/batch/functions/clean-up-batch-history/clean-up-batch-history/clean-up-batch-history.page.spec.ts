import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CleanUpBatchHistoryPage } from './clean-up-batch-history.page';

describe('CleanUpBatchHistoryPage', () => {
  let component: CleanUpBatchHistoryPage;
  let fixture: ComponentFixture<CleanUpBatchHistoryPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CleanUpBatchHistoryPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CleanUpBatchHistoryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
