import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'cancelbatchjob',
    loadChildren: () => import('./cancel-batch-job/cancel-batch-job-function.module').then((m) => m.CancelBatchJobFunctionModule)
  },
  {
    path: 'cleanupbatchhistory',
    loadChildren: () => import('./clean-up-batch-history/clean-up-batch-history-function.module').then((m) => m.CleanUpBatchHistoryFunctionModule)
  },
  {
    path: 'holdbatchjob',
    loadChildren: () => import('./hold-batch-job/hold-batch-job-function.module').then((m) => m.HoldBatchJobFunctionModule)
  },
  {
    path: 'releasebatchjob',
    loadChildren: () => import('./release-batch-job/release-batch-job-function.module').then((m) => m.ReleaseBatchJobFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BatchFunctionRoutingModule {}
