import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'primeng/api';
import { BatchFunctionRoutingModule } from './batch-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, BatchFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BatchFunctionModule {}
