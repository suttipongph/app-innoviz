import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'batch-log-item.page',
  templateUrl: './batch-log-item.page.html',
  styleUrls: ['./batch-log-item.page.scss']
})
export class BatchLogItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: 'batchinstancelog-child',
      servicePath: 'BatchObject',
      childPaths: [{ pagePath: 'batchinstancelog-child', servicePath: 'BatchObject' }]
    };
  }
}
