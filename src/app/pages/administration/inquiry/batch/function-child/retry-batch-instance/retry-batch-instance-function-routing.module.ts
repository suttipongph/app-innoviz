import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { RetryBatchInstancePage } from './retry-batch-instance/retry-batch-instance.page';

const routes: Routes = [
  {
    path: '',
    component: RetryBatchInstancePage,
    canActivate:[AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RetryBatchInstanceFunctionRoutingModule {}
