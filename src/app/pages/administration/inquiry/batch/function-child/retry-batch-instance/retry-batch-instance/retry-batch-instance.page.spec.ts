import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RetryBatchInstancePage } from './retry-batch-instance.page';

describe('RetryBatchInstancePage', () => {
  let component: RetryBatchInstancePage;
  let fixture: ComponentFixture<RetryBatchInstancePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RetryBatchInstancePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RetryBatchInstancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
