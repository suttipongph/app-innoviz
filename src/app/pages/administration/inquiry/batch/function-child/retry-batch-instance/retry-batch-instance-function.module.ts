import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RetryBatchInstanceFunctionRoutingModule } from './retry-batch-instance-function-routing.module';
import { RetryBatchInstancePage } from './retry-batch-instance/retry-batch-instance.page';
import { SharedModule } from 'shared/shared.module';
import { RetryBatchInstanceComponentModule } from 'components/batch/retry-batch-instance/retry-batch-instance.module';

@NgModule({
  declarations: [RetryBatchInstancePage],
  imports: [CommonModule, SharedModule, RetryBatchInstanceFunctionRoutingModule, RetryBatchInstanceComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RetryBatchInstanceFunctionModule {}
