import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'retry-batch-instance-page',
  templateUrl: './retry-batch-instance.page.html',
  styleUrls: ['./retry-batch-instance.page.scss']
})
export class RetryBatchInstancePage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.RETRY_BATCH_INSTANCE,
      servicePath: `BatchObject/BatchHistory-Child/Function/RetryBatchInstance`
    };
  }
}
