import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'primeng/api';
import { BatchHistoryFunctionRoutingModule } from './batch-history-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, BatchHistoryFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BatchHistoryFunctionModule {}
