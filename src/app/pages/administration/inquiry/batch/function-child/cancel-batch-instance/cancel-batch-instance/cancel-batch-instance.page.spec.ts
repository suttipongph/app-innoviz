import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CancelBatchInstancePage } from './cancel-batch-instance.page';

describe('CancelBatchInstancePage', () => {
  let component: CancelBatchInstancePage;
  let fixture: ComponentFixture<CancelBatchInstancePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CancelBatchInstancePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelBatchInstancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
