import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CancelBatchInstancePage } from './cancel-batch-instance/cancel-batch-instance.page';
import { SharedModule } from 'shared/shared.module';
import { CancelBatchInstanceFunctionRoutingModule } from './cancel-batch-instance-function-routing.module';
import { CancelBatchInstanceComponentModule } from 'components/batch/cancel-batch-instance/cancel-batch-instance.module';

@NgModule({
  declarations: [CancelBatchInstancePage],
  imports: [CommonModule, SharedModule, CancelBatchInstanceFunctionRoutingModule, CancelBatchInstanceComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CancelBatchInstanceFunctionModule {}
