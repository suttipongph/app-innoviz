import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CancelBatchInstancePage } from './cancel-batch-instance/cancel-batch-instance.page';

const routes: Routes = [
  {
    path: '',
    component: CancelBatchInstancePage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CancelBatchInstanceFunctionRoutingModule {}
