import { Component, OnInit } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { UIControllerService } from 'core/services/uiController.service';

@Component({
  selector: 'cancel-batch-instance-page',
  templateUrl: './cancel-batch-instance.page.html',
  styleUrls: ['./cancel-batch-instance.page.scss']
})
export class CancelBatchInstancePage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.CANCEL_BATCH_INSTANCE,
      servicePath: `BatchObject/BatchHistory-Child/Function/CancelBatchInstance`
    };
  }
}
