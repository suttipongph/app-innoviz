import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'cancelbatchinstance',
    loadChildren: () => import('./cancel-batch-instance/cancel-batch-instance-function.module').then((m) => m.CancelBatchInstanceFunctionModule)
  },
  {
    path: 'retrybatchinstance',
    loadChildren: () => import('./retry-batch-instance/retry-batch-instance-function.module').then((m) => m.RetryBatchInstanceFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BatchHistoryFunctionRoutingModule {}
