import { Component, OnInit } from '@angular/core';
import { ROUTE_ADMIN, ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'batch-view-item.page',
  templateUrl: './batch-view-item.page.html',
  styleUrls: ['./batch-view-item.page.scss']
})
export class BatchViewItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_ADMIN.INQUIRY.BATCH,
      servicePath: 'BatchObject',
      childPaths: [{ pagePath: 'batchhistory-child', servicePath: 'BatchObject' }]
    };
  }
}
