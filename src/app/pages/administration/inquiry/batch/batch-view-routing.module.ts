import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { BatchHistoryItemPage } from './batch-history-item/batch-history-item.page';
import { BatchLogItemPage } from './batch-log-item/batch-log-item.page';
import { BatchViewItemPage } from './batch-view-item/batch-view-item.page';
import { BatchViewListPage } from './batch-view-list/batch-view-list.page';

const routes: Routes = [
  {
    path: '',
    component: BatchViewListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BatchViewItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/batch-function.module').then((m) => m.BatchFunctionModule)
  },
  {
    path: ':id/batchhistory-child/:id',
    component: BatchHistoryItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/batchhistory-child/:id/function',
    loadChildren: () => import('./function-child/batch-history-function.module').then((m) => m.BatchHistoryFunctionModule)
  },
  {
    path: ':id/batchhistory-child/:id/batchinstancelog-child/:id',
    component: BatchLogItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BatchViewRoutingModule {}
