import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { BatchViewListPage } from './batch-view-list/batch-view-list.page';
import { BatchViewItemPage } from './batch-view-item/batch-view-item.page';
import { BatchHistoryItemPage } from './batch-history-item/batch-history-item.page';
import { BatchLogItemPage } from './batch-log-item/batch-log-item.page';
import { BatchViewRoutingModule } from './batch-view-routing.module';
import { BatchViewComponentModule } from 'components/batch/batch-view/batch-view.module';
import { BatchHistoryComponentModule } from 'components/batch/batch-history/batch-history.module';
import { BatchLogComponentModule } from 'components/batch/batch-log/batch-log.module';

@NgModule({
  declarations: [BatchViewListPage, BatchViewItemPage, BatchHistoryItemPage, BatchLogItemPage],
  imports: [CommonModule, SharedModule, BatchViewRoutingModule, BatchViewComponentModule, BatchHistoryComponentModule, BatchLogComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BatchViewModule {}
