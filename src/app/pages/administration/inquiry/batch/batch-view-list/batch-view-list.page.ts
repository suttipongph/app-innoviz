import { Component, OnInit } from '@angular/core';
import { ROUTE_ADMIN, ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'batch-view-list-page',
  templateUrl: './batch-view-list.page.html',
  styleUrls: ['./batch-view-list.page.scss']
})
export class BatchViewListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'jobId';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_ADMIN.INQUIRY.BATCH, servicePath: 'BatchObject' };
  }
}
