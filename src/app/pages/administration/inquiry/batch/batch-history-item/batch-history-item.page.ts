import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'batch-history-item.page',
  templateUrl: './batch-history-item.page.html',
  styleUrls: ['./batch-history-item.page.scss']
})
export class BatchHistoryItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: 'batchhistory-child',
      servicePath: 'BatchObject',
      childPaths: [{ pagePath: 'batchinstancelog-child', servicePath: 'BatchObject' }]
    };
  }
}
