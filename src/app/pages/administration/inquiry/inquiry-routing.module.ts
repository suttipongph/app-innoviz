import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [
  { path: 'batch', loadChildren: () => import('./batch/batch-view.module').then((m) => m.BatchViewModule) },
  { path: 'sysuserlog', loadChildren: () => import('./sys-user-log/sys-user-log.module').then((m) => m.SysUserLogModule) }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminInquiryRoutingModule {}
