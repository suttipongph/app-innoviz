import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HirePurchaseRoutingModule } from './hire-purchase-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, HirePurchaseRoutingModule]
})
export class HirePurchaseModule {}
