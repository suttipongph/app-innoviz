import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'document-condition-info-item.page',
  templateUrl: './document-condition-info-item.page.html',
  styleUrls: ['./document-condition-info-item.page.scss']
})
export class DocumentConditionInfoItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    const productTypeWording = getProductType(this.masterRoute, true);
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.BUYERTABLE_DOCUMENT_CONDITION_INFO,
      servicePath: `CreditAppTable/${productTypeWording}/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.BILLING_DOCUMENT_CONDITION_TRANS_BILLING,
          servicePath: `CreditAppTable/${productTypeWording}/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans`
        },
        {
          pagePath: ROUTE_RELATED_GEN.RECEIPT_DOCUMENT_CONDITION_TRANS_RECEIPT,
          servicePath: `CreditAppTable/${productTypeWording}/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans`
        }
      ]
    };
  }
}
