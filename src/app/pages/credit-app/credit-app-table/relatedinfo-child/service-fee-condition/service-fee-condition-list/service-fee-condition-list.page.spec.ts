import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ServiceFeeConditionListPage } from './service-fee-condition-list.page';

describe('ServiceFeeConditionListPage', () => {
  let component: ServiceFeeConditionListPage;
  let fixture: ComponentFixture<ServiceFeeConditionListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ServiceFeeConditionListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceFeeConditionListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
