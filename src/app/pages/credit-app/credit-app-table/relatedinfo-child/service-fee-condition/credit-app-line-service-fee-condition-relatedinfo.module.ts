import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppLineServiceFeeConditionRelatedinfoRoutingModule } from './credit-app-line-service-fee-condition-relatedinfo-routing.module';
import { ServiceFeeConditionListPage } from './service-fee-condition-list/service-fee-condition-list.page';
import { ServiceFeeConditionItemPage } from './service-fee-condition-item/service-fee-condition-item.page';
import { SharedModule } from 'shared/shared.module';
import { ServiceFeeConditionTransComponentModule } from 'components/service-fee-condition-trans/service-fee-condition-trans.module';

@NgModule({
  declarations: [ServiceFeeConditionListPage, ServiceFeeConditionItemPage],
  imports: [CommonModule, SharedModule, CreditAppLineServiceFeeConditionRelatedinfoRoutingModule, ServiceFeeConditionTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppLineServiceFeeConditionRelatedinfoModule {}
