import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyerInvoiceTableListPage } from './buyer-invoice-table-list.page';

describe('BuyerInvoiceTableListComponent', () => {
  let component: BuyerInvoiceTableListPage;
  let fixture: ComponentFixture<BuyerInvoiceTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BuyerInvoiceTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerInvoiceTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
