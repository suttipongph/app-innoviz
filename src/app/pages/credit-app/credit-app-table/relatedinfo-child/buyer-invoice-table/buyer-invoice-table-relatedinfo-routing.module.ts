import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { BuyerInvoiceTableItemPage } from './buyer-invoice-table-item/buyer-invoice-table-item.page';
import { BuyerInvoiceTableListPage } from './buyer-invoice-table-list/buyer-invoice-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: BuyerInvoiceTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BuyerInvoiceTableItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyerInvoiceTableRelatedinfoRoutingModule {}
