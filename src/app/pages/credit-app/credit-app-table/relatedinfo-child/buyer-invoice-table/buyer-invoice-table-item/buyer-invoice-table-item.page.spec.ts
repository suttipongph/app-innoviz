import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyerInvoiceTableItemPage } from './buyer-invoice-table-item.page';

describe('BuyerInvoiceTableItemComponent', () => {
  let component: BuyerInvoiceTableItemPage;
  let fixture: ComponentFixture<BuyerInvoiceTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BuyerInvoiceTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerInvoiceTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
