import { Component } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-buyer-invoice-table-item',
  templateUrl: './buyer-invoice-table-item.page.html',
  styleUrls: ['./buyer-invoice-table-item.page.scss']
})
export class BuyerInvoiceTableItemPage {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'buyerInvoiceTableGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.BUYER_INVOICE,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/CreditAppLine-Child/RelatedInfo/BuyerInvoice`
    };
  }
}
