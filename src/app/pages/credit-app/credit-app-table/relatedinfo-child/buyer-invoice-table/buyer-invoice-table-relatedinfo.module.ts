import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuyerInvoiceTableRelatedinfoRoutingModule } from './buyer-invoice-table-relatedinfo-routing.module';
import { SharedModule } from 'shared/shared.module';
import { BuyerInvoiceTableItemPage } from './buyer-invoice-table-item/buyer-invoice-table-item.page';
import { BuyerInvoiceTableListPage } from './buyer-invoice-table-list/buyer-invoice-table-list.page';
import { BuyerInvoiceTableComponentModule } from 'components/buyer-invoice/buyer-invoice-table/buyer-invoice-table.module';

@NgModule({
  declarations: [BuyerInvoiceTableItemPage, BuyerInvoiceTableListPage],
  imports: [CommonModule, SharedModule, BuyerInvoiceTableRelatedinfoRoutingModule, BuyerInvoiceTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BuyerTableBuyerInvoiceRelatedinfoModule {}
