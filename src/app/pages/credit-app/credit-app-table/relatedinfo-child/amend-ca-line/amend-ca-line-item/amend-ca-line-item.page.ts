import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'amend-ca-line-item.page',
  templateUrl: './amend-ca-line-item.page.html',
  styleUrls: ['./amend-ca-line-item.page.scss']
})
export class AmendCaLineItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    const productTypeWording = getProductType(this.masterRoute, true);
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.FACTORING_CREDIT_APPLICATION_TABLE,
      servicePath: `CreditAppTable/${productTypeWording}/CreditAppLine-Child/RelatedInfo/AmendCaLine`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.BILLING_DOCUMENT_CONDITION_TRANS_BILLING,
          servicePath: `CreditAppTable/${productTypeWording}/CreditAppLine-Child/RelatedInfo/AmendCaLine`
        },
        {
          pagePath: ROUTE_RELATED_GEN.RECEIPT_DOCUMENT_CONDITION_TRANS_RECEIPT,
          servicePath: `CreditAppTable/${productTypeWording}/CreditAppLine-Child/RelatedInfo/AmendCaLine`
        }
      ]
    };
  }
}
