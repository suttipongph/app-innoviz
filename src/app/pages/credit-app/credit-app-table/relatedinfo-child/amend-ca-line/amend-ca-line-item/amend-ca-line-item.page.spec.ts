import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AmendCaLineItemPage } from './amend-ca-line-item.page';

describe('AmendCaItemPage', () => {
  let component: AmendCaLineItemPage;
  let fixture: ComponentFixture<AmendCaLineItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AmendCaLineItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmendCaLineItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
