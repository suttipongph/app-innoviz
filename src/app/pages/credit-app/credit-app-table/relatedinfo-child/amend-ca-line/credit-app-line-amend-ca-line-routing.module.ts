import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AmendCaLineListPage } from './amend-ca-line-list/amend-ca-line-list.page';
import { AmendCaLineItemPage } from './amend-ca-line-item/amend-ca-line-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { BillingDocumentConditionTransItemPage } from 'pages/credit-app/credit-app-table/relatedinfo-child/amend-ca-line/billing-document-condition-trans-item/billing-document-condition-trans-item.page';
import { ReceiptDocumentConditionTransItemPage } from 'pages/credit-app/credit-app-table/relatedinfo-child/amend-ca-line/receipt-document-condition-trans-item/receipt-document-condition-trans-item.page';
const routes: Routes = [
  {
    path: '',
    component: AmendCaLineListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: AmendCaLineItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/billingdocumentconditiontrans-child/:id',
    component: BillingDocumentConditionTransItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/receiptdocumentconditiontrans-child/:id',
    component: ReceiptDocumentConditionTransItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/credit-app-line-amend-line-relatedinfo.module').then((m) => m.CreditAppLineAmendLineRelatedinfoModule)
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/credit-app-table-amend-ca-line-function.module').then((m) => m.CreditAppTableAmendCALineFunctionModule)
  },
  {
    path: ':id/workflow',
    loadChildren: () =>
      import('./workflow/amend-ca-line-action-workflow/amend-ca-line-action-workflow.module').then((m) => m.AmendCaLineActionWorkflowModule)
  },
  {
    path: ':id/workflow/actionhistory',
    loadChildren: () =>
      import('./workflow/amend-ca-line-action-history/amend-ca-line-action-history.module').then((m) => m.AmendCaLineActionHistoryModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppLineAmendCaLineRoutingModule {}
