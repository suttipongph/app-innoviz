import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AmendCaLineListPage } from './amend-ca-line-list.page';

describe('AmendCaListPage', () => {
  let component: AmendCaLineListPage;
  let fixture: ComponentFixture<AmendCaLineListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AmendCaLineListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmendCaLineListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
