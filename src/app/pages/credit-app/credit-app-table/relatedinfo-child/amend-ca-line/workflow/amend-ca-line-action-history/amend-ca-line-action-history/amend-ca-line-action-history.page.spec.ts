import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkFlowCreditAppRequestActionHistoryPage } from './amend-ca-line-action-history.page';

describe('WorkFlowCreditAppRequestActionHistoryPage', () => {
  let component: WorkFlowCreditAppRequestActionHistoryPage;
  let fixture: ComponentFixture<WorkFlowCreditAppRequestActionHistoryPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WorkFlowCreditAppRequestActionHistoryPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkFlowCreditAppRequestActionHistoryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
