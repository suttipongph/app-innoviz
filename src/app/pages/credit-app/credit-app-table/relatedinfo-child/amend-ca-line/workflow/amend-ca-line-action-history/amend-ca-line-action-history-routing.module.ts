import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { WorkFlowCreditAppRequestActionHistoryPage } from './amend-ca-line-action-history/amend-ca-line-action-history.page';

const routes: Routes = [
  {
    path: '',
    component: WorkFlowCreditAppRequestActionHistoryPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AmendCaLineActionHistoryRoutingModule {}
