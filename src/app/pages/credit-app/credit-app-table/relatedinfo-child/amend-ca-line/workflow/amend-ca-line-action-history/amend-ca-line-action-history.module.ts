import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { AmendCaLineActionHistoryRoutingModule } from './amend-ca-line-action-history-routing.module';
import { WorkFlowCreditAppRequestActionHistoryPage } from './amend-ca-line-action-history/amend-ca-line-action-history.page';
import { ActionHistoryComponentModule } from 'components/action-history/action-history.module';

@NgModule({
  declarations: [WorkFlowCreditAppRequestActionHistoryPage],
  imports: [CommonModule, SharedModule, AmendCaLineActionHistoryRoutingModule, ActionHistoryComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AmendCaLineActionHistoryModule {}
