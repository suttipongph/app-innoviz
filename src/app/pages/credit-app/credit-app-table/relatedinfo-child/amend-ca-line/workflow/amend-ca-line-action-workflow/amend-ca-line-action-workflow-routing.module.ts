import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ActionWorkflowCreditAppRequestPage } from './action-workflow-credit-app-request/action-workflow-credit-app-request-page';
import { StartWorkflowCreditAppRequestPage } from './start-workflow-credit-app-request/start-workflow-credit-app-request-page';

const routes: Routes = [
  {
    path: 'startworkflow',
    component: StartWorkflowCreditAppRequestPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'actionworkflow',
    component: ActionWorkflowCreditAppRequestPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AmendCaLineActionWorkflowRoutingModule {}
