import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-update-status-amend-buyer-info-page',
  templateUrl: './update-status-amend-buyer-info-page.component.html',
  styleUrls: ['./update-status-amend-buyer-info-page.component.scss']
})
export class UpdateStatusAmendBuyerInfoPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.UPDATE_STATUS_AMEND_BUYER_INFO,
      servicePath: `CreditAppTable/${getProductType(
        this.masterRoute,
        true
      )}/CreditAppLine-Child/RelatedInfo/AmendCaLine/Function/UpdateStatusAmendBuyerInfo`
    };
  }
}
