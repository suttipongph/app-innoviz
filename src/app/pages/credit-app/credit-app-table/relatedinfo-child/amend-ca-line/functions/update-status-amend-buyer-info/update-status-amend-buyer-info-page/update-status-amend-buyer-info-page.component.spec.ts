import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateStatusAmendBuyerInfoPage } from './update-status-amend-buyer-info-page.component';

describe('UpdateStatusAmendBuyerInfoPageComponent', () => {
  let component: UpdateStatusAmendBuyerInfoPage;
  let fixture: ComponentFixture<UpdateStatusAmendBuyerInfoPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UpdateStatusAmendBuyerInfoPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateStatusAmendBuyerInfoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
