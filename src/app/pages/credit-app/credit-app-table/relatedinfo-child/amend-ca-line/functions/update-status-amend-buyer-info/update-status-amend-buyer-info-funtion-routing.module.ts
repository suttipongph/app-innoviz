import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { UpdateStatusAmendBuyerInfoPage } from './update-status-amend-buyer-info-page/update-status-amend-buyer-info-page.component';

const routes: Routes = [
  {
    path: '',
    component: UpdateStatusAmendBuyerInfoPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpdateStatusAmendBuyerInfoFuntionRoutingModule {}
