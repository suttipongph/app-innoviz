import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UpdateStatusAmendBuyerInfoFuntionRoutingModule } from './update-status-amend-buyer-info-funtion-routing.module';
import { UpdateStatusAmendBuyerInfoComponentModule } from 'components/credit-app-request-line-amend/update-status-amend-buyer-info/update-status-amend-buyer-info.module';
import { UpdateStatusAmendBuyerInfoPage } from './update-status-amend-buyer-info-page/update-status-amend-buyer-info-page.component';

@NgModule({
  declarations: [UpdateStatusAmendBuyerInfoPage],
  imports: [CommonModule, UpdateStatusAmendBuyerInfoFuntionRoutingModule, UpdateStatusAmendBuyerInfoComponentModule]
})
export class UpdateStatusAmendBuyerInfoFuntionModule {}
