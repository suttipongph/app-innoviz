import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'printsetbookdoctrans',
    loadChildren: () => import('./print-book-doc/print-book-doc-function.module').then((m) => m.PrintBookDocFunctionModule)
  },
  {
    path: 'updatestatusamendbuyerinfo',
    loadChildren: () =>
      import('./update-status-amend-buyer-info/update-status-amend-buyer-info-funtion.module').then((m) => m.UpdateStatusAmendBuyerInfoFuntionModule)
  },
  {
    path: 'cancelcreditapprequest',
    loadChildren: () =>
      import('./cancel-credit-app-req/cancel-credit-app-req.page-function.module').then((m) => m.CancelCreditAppRequestFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableAmendCALineFunctionRoutingModule {}
