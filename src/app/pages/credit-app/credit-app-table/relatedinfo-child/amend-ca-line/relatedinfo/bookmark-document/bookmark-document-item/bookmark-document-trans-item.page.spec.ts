import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditAppRequestTableBookmarkDocumentTransItemPage } from './bookmark-document-trans-item.page';

describe('BookmarkDocumentTransItemComponent', () => {
  let component: CreditAppRequestTableBookmarkDocumentTransItemPage;
  let fixture: ComponentFixture<CreditAppRequestTableBookmarkDocumentTransItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreditAppRequestTableBookmarkDocumentTransItemPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppRequestTableBookmarkDocumentTransItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
