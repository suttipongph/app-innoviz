import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AssignmentAgreementOutstandingListPage } from './assignment-agreement-outstanding-list.page';

describe('AssignmentAgreementOutstandingListPage', () => {
  let component: AssignmentAgreementOutstandingListPage;
  let fixture: ComponentFixture<AssignmentAgreementOutstandingListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AssignmentAgreementOutstandingListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentAgreementOutstandingListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
