import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CreditAppRequestTableBookmarkDocumentTransItemPage } from './bookmark-document-item/bookmark-document-trans-item.page';
import { CreditAppRequestTableBookmarkDocumentTransListPage } from './bookmark-document-list/bookmark-document-trans-list.page';
/* import { BusinessCollateralItemPage } from './business-collateral-item/business-collateral-item.page';
import { CreditAppRequestTableBusinessCollateralInfoItemPage } from './credit-app-req-bus-collaterl-info-item/credit-app-req-bus-collaterl-info-item.page';
import { CreditAppRequestTableBusinessCollateralItemPage } from './credit-app-req-bus-collaterl-item/credit-app-req-bus-collaterl-item.page'; */

const routes: Routes = [
  {
    path: '',
    component: CreditAppRequestTableBookmarkDocumentTransListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CreditAppRequestTableBookmarkDocumentTransItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/bookmark-document-trans-function.module').then((m) => m.BookmarkDocumentFunctionModule)
  }
  /* {
    path: 'creditapprequesttablebookmarkdocumenttrans-child/:id',
    component: CreditAppRequestTableBookmarkDocumentTransItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'custbookmarkdocumenttrans-child/:id',
    component: CreditAppRequestTableBookmarkDocumentTransItemPage,
    canActivate: [AuthGuardService]
  } */
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AmendLineCreditAppRequestTableBookmarkDocumentRelatedinfoRoutingModule {}
