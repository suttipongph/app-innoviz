import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditAppRequestTableBookmarkDocumentTransListPage } from './bookmark-document-trans-list.page';

describe('BookmarkDocumentTransListComponent', () => {
  let component: CreditAppRequestTableBookmarkDocumentTransListPage;
  let fixture: ComponentFixture<CreditAppRequestTableBookmarkDocumentTransListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreditAppRequestTableBookmarkDocumentTransListPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppRequestTableBookmarkDocumentTransListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
