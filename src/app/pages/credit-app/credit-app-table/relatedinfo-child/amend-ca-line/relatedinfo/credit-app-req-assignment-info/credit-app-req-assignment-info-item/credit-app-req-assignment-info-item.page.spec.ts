import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditAppReqAssignmentInfoItemPage } from './credit-app-req-assignment-info-item.page';

describe('BusinessCollateralItemPage', () => {
  let component: CreditAppReqAssignmentInfoItemPage;
  let fixture: ComponentFixture<CreditAppReqAssignmentInfoItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditAppReqAssignmentInfoItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppReqAssignmentInfoItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
