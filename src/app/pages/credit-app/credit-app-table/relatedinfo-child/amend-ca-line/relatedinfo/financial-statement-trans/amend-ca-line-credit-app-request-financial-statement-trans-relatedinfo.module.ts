import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FinancialStatementTransComponentModule } from 'components/financial-statement-trans/financial-statement-trans/financial-statement-trans.module';
import { FinancialStatementTransListPage } from './financial-statement-trans-list/financial-statement-trans-list.page';
import { FinancialStatementTransItemPage } from './financial-statement-trans-item/financial-statement-trans-item.page';
import { AmendLineCreditAppRequestTableFinancialStatementTransRelatedinfoRoutingModule } from './amend-ca-line-credit-app-request-financial-statement-trans-relatedinfo-routing.module';

@NgModule({
  declarations: [FinancialStatementTransListPage, FinancialStatementTransItemPage],
  imports: [CommonModule, SharedModule, AmendLineCreditAppRequestTableFinancialStatementTransRelatedinfoRoutingModule, FinancialStatementTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AmendLineCreditAppRequestTableFinancialStatementTransRelatedinfoModule {}
