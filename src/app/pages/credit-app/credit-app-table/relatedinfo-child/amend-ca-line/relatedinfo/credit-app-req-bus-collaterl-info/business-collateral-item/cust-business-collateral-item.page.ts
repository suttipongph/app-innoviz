import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'cust-business-collateral-item.page',
  templateUrl: './cust-business-collateral-item.page.html',
  styleUrls: ['./cust-business-collateral-item.page.scss']
})
export class BusinessCollateralItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.CUST_BUSINESS_COLLATERAL,
      servicePath: `CreditAppTable/${getProductType(
        this.masterRoute,
        true
      )}/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral`
    };
  }
}
