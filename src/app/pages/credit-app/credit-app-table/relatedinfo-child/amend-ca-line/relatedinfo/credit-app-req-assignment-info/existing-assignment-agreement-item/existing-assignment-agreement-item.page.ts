import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';

@Component({
  selector: 'existing-assignment-agreement-item.page',
  templateUrl: './existing-assignment-agreement-item.page.html',
  styleUrls: ['./existing-assignment-agreement-item.page.scss']
})
export class ExistingAssignmentAgreementItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  activeName: string = this.uiService.getRelatedInfoActiveTableName();
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.EXIS_ASSIGNMENT,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment`
    };
  }
}
