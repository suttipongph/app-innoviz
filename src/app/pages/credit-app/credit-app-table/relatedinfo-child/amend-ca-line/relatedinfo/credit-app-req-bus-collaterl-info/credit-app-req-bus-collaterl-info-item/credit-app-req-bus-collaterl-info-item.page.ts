import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'credit-app-req-bus-collaterl-info-item.page',
  templateUrl: './credit-app-req-bus-collaterl-info-item.page.html',
  styleUrls: ['./credit-app-req-bus-collaterl-info-item.page.scss']
})
export class CreditAppRequestTableBusinessCollateralInfoItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  option: OptionModel[];
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    const productTypeWording = getProductType(this.masterRoute, true);
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.FACTORING_CREDIT_APPLICATION_TABLE,
      servicePath: `CreditAppTable/${productTypeWording}/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.CUST_BUSINESS_COLLATERAL,
          servicePath: `CreditAppTable/${productTypeWording}/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral`
        },
        {
          pagePath: ROUTE_RELATED_GEN.CREDIT_APP_REQUEST_TABLE_BUSINESS_COLLATERAL,
          servicePath: `CreditAppTable/${productTypeWording}/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral`
        }
      ]
    };
  }
}
