import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, EmptyGuid, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel, RowIdentity } from 'shared/models/systemModel';

@Component({
  selector: 'app-memo-list',
  templateUrl: './memo-list.page.html',
  styleUrls: ['./memo-list.page.scss']
})
export class MemoListPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  option: OptionModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  parentId = this.uiService.getRelatedInfoParentTableKey();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'memoTransGUID';
    const columns: ColumnModel[] = [];

    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.MEMO,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans`
    };
  }
}
