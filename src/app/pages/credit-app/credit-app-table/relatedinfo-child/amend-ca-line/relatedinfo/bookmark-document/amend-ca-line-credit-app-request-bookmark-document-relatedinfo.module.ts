import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AmendLineCreditAppRequestTableBookmarkDocumentRelatedinfoRoutingModule } from './amend-ca-line-credit-app-request-bookmark-document-relatedinfo-routing.module';
import { SharedModule } from 'shared/shared.module';
import { CreditAppRequestTableBookmarkDocumentTransItemPage } from './bookmark-document-item/bookmark-document-trans-item.page';
import { CreditAppRequestTableBookmarkDocumentTransListPage } from './bookmark-document-list/bookmark-document-trans-list.page';
import { BookmarkDocumentTransComponentModule } from 'components/bookmark-document-trans/bookmark-document-trans/bookmark-document-trans.module';

@NgModule({
  declarations: [CreditAppRequestTableBookmarkDocumentTransItemPage, CreditAppRequestTableBookmarkDocumentTransListPage],
  imports: [
    CommonModule,
    SharedModule,
    AmendLineCreditAppRequestTableBookmarkDocumentRelatedinfoRoutingModule,
/*     CreditAppReqBusCollaterlInfoComponentModule,
    CreditAppReqBusinessCollateralComponentModule, */
    BookmarkDocumentTransComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AmendLineCreditAppRequestTableBookmarkDocumentRelatedinfoModule {}
