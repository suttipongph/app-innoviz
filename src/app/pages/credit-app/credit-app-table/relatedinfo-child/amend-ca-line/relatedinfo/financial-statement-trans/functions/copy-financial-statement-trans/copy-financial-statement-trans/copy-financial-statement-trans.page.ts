import { Component, OnInit } from '@angular/core';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel, PathParamModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-copy-verification',
  templateUrl: './copy-financial-statement-trans.page.html',
  styleUrls: ['./copy-financial-statement-trans.page.scss']
})
export class CopyFinancialStatementTransPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {
    // super();
  }

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.COPY_FINANCIAL_STATEMENT_TRANS,
      servicePath: `CreditAppTable/${getProductType(
        this.masterRoute,
        true
      )}/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/Function/copyfinancialstatementtrans`
    };
  }
}
