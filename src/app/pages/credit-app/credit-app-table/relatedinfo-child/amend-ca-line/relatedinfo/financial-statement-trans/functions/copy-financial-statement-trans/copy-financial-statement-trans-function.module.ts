import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CopyVerificationFunctionRoutingModule } from './copy-financial-statement-trans-function-routing.module';
import { SharedModule } from 'shared/shared.module';
import { CopyFinancialStatementTransComponentModule } from 'components/financial-statement-trans/copy-financial-statement-trans/copy-financial-statement-trans.module';
import { CopyFinancialStatementTransPage } from './copy-financial-statement-trans/copy-financial-statement-trans.page';

@NgModule({
  declarations: [CopyFinancialStatementTransPage],
  imports: [CommonModule, SharedModule, CopyVerificationFunctionRoutingModule, CopyFinancialStatementTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CopyFinancialStatementTransFunctionModule {}
