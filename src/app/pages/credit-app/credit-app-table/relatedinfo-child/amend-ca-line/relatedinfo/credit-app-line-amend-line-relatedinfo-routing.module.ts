import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'businesscollateral',
    loadChildren: () =>
      import('./credit-app-req-bus-collaterl-info/amend-ca-line-credit-app-request-business-collateral-relatedinfo.module').then(
        (m) => m.AmendLineCreditAppRequestTableBusinessCollateralRelatedinfoModule
      )
  },
  {
    path: 'memotrans',
    loadChildren: () => import('./memo/amend-ca-line-memo-trans-relatedinfo.module').then((m) => m.AmendCaLineMemoTransRelatedinfoModule)
  },
  {
    path: 'bookmarkdocumenttrans',
    loadChildren: () =>
      import('./bookmark-document/amend-ca-line-credit-app-request-bookmark-document-relatedinfo.module').then(
        (m) => m.AmendLineCreditAppRequestTableBookmarkDocumentRelatedinfoModule
      )
  },
  {
    path: 'financialstatementtrans',
    loadChildren: () =>
      import('./financial-statement-trans/amend-ca-line-credit-app-request-financial-statement-trans-relatedinfo.module').then(
        (m) => m.AmendLineCreditAppRequestTableFinancialStatementTransRelatedinfoModule
      )
  },
  {
    path: 'attachment',
    loadChildren: () => import('./attachment/amend-ca-line-attachment.module').then((m) => m.AmendCALineAttachmentModule)
  },
  {
    path: 'servicefeeconditiontrans',
    loadChildren: () =>
      import('./service-fee-condition/amend-ca-line-service-fee-condition-relatedinfo.module').then(
        (m) => m.AmendCaLineServiceFeeConditionRelatedinfoModule
      )
  },
  {
    path: 'assignment',
    loadChildren: () =>
      import('./credit-app-req-assignment-info/amend-ca-line-assignment-relatedinfo.module').then((m) => m.AmendCaLineAssignmentRelatedinfoModule)
  },
  {
    path: 'assignmentagreementoutstanding',
    loadChildren: () =>
      import('./assignment-agreement-outstanding/credit-app-table-assignment-agreement-outstanding-relatedinfo.module').then(
        (m) => m.CreditAppTableAssignmentAgreementOutstandingRelatedinfoModule
      )
  },
  {
    path: 'creditoutstanding',
    loadChildren: () =>
      import('./credit-outstanding/credit-app-request-credit-outstanding-relatedinfo.module').then(
        (m) => m.CreditAppRequestTableCreditOutstandingRelatedinfoModule
      )
  },
  {
    path: 'cabuyercreditoutstanding',

    loadChildren: () =>
      import('./ca-buyer-credit-outstanding/amend-ca-ca-buyer-credit-outstanding-relatedinfo.module').then(
        (m) => m.AmendCCreditAppRequestTableCaBuyerCreditOutstandingRelatedinfoModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppLineAmendLineRelatedinfoRoutingModule {}
