import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'service-fee-condition-item.page',
  templateUrl: './service-fee-condition-item.page.html',
  styleUrls: ['./service-fee-condition-item.page.scss']
})
export class ServiceFeeConditionItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.SERVICE_FEE_CONDITION_TRANS,
      servicePath: `CreditAppTable/${getProductType(
        this.masterRoute,
        true
      )}/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans`
    };
  }
}
