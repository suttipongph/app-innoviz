import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreditAppLineAmendLineRelatedinfoRoutingModule } from './credit-app-line-amend-line-relatedinfo-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CreditAppLineAmendLineRelatedinfoRoutingModule
  ]
})
export class CreditAppLineAmendLineRelatedinfoModule { }
