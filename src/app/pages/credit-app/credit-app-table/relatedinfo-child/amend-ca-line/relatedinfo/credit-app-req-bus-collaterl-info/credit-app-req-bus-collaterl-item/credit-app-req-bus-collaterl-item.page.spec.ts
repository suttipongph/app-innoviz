import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditAppRequestTableBusinessCollateralItemPage } from './credit-app-req-bus-collaterl-item.page';

describe('BusinessCollateralItemPage', () => {
  let component: CreditAppRequestTableBusinessCollateralItemPage;
  let fixture: ComponentFixture<CreditAppRequestTableBusinessCollateralItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditAppRequestTableBusinessCollateralItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppRequestTableBusinessCollateralItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
