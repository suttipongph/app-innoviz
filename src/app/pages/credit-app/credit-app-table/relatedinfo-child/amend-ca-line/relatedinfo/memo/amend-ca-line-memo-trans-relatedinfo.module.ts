import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { MemoTransComponentModule } from 'components/memo-trans/memo-trans/memo-trans.module';
import { MemoListPage } from './memo-list/memo-list.page';
import { MemoItemPage } from './memo-item/memo-item.page';
import { AmendCaLineMemoTransRelatedinfoRoutingModule } from './amend-ca-line-memo-trans-relatedinfo-routing.module';

@NgModule({
  declarations: [MemoListPage, MemoItemPage],
  imports: [CommonModule, SharedModule, AmendCaLineMemoTransRelatedinfoRoutingModule, MemoTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AmendCaLineMemoTransRelatedinfoModule {}
