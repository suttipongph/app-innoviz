import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServiceFeeConditionListPage } from './service-fee-condition-list/service-fee-condition-list.page';
import { ServiceFeeConditionItemPage } from './service-fee-condition-item/service-fee-condition-item.page';
import { SharedModule } from 'shared/shared.module';
import { ServiceFeeConditionTransComponentModule } from 'components/service-fee-condition-trans/service-fee-condition-trans.module';
import { AmendCaLineServiceFeeConditionRelatedinfoRoutingModule } from './amend-ca-line-service-fee-condition-relatedinfo-routing.module';
@NgModule({
  declarations: [ServiceFeeConditionListPage, ServiceFeeConditionItemPage],
  imports: [CommonModule, SharedModule, AmendCaLineServiceFeeConditionRelatedinfoRoutingModule, ServiceFeeConditionTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AmendCaLineServiceFeeConditionRelatedinfoModule {}
