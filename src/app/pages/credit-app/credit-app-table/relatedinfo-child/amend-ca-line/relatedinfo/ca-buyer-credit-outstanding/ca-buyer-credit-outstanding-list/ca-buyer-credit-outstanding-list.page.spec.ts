import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CaBuyerCreditOutstandingListPage } from './ca-buyer-credit-outstanding-list.page';

describe('CaBuyerCreditOutstandingListPage', () => {
  let component: CaBuyerCreditOutstandingListPage;
  let fixture: ComponentFixture<CaBuyerCreditOutstandingListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CaBuyerCreditOutstandingListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CaBuyerCreditOutstandingListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
