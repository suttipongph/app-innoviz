import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AmendCaLineListPage } from './amend-ca-line-list/amend-ca-line-list.page';
import { AmendCaLineItemPage } from './amend-ca-line-item/amend-ca-line-item.page';
import { SharedModule } from 'shared/shared.module';
import { CreditAppLineAmendCaLineRoutingModule } from './credit-app-line-amend-ca-line-routing.module';
import { CreditAppRequestLineAmendComponentModule } from 'components/credit-app-request-line-amend/credit-app-request-line-amend/credit-app-request-line-amend.module';
import { DocumentConditionTransComponentModule } from 'components/document-condition-trans/document-condition-trans/document-condition-trans.module';
import { BillingDocumentConditionTransItemPage } from 'pages/credit-app/credit-app-table/relatedinfo-child/amend-ca-line/billing-document-condition-trans-item/billing-document-condition-trans-item.page';
import { ReceiptDocumentConditionTransItemPage } from 'pages/credit-app/credit-app-table/relatedinfo-child/amend-ca-line/receipt-document-condition-trans-item/receipt-document-condition-trans-item.page';

@NgModule({
  declarations: [AmendCaLineListPage, AmendCaLineItemPage, BillingDocumentConditionTransItemPage, ReceiptDocumentConditionTransItemPage],
  imports: [
    CommonModule,
    SharedModule,
    CreditAppLineAmendCaLineRoutingModule,
    CreditAppRequestLineAmendComponentModule,
    DocumentConditionTransComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppLineAmendCaLineModule {}
