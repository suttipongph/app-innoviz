import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'servicefeeconditiontrans',
    loadChildren: () =>
      import('./service-fee-condition/credit-app-line-service-fee-condition-relatedinfo.module').then(
        (m) => m.CreditAppLineServiceFeeConditionRelatedinfoModule
      )
  },
  {
    path: 'documentconditiontrans',
    loadChildren: () =>
      import('./document-condition-info/credit-app-request-line-document-condition-info.module').then(
        (m) => m.CreditAppRequestLineDocumentConditionInfoModule
      )
  },
  {
    path: 'buyeragreementtrans',
    loadChildren: () =>
      import('./buyer-agreement-trans/credit-app-line-buyer-agreement-trans-relatedinfo.module').then(
        (m) => m.CreditAppLineBuyerAgreementTransRelatedinfoModule
      )
  },
  {
    path: 'buyerinvoice',
    loadChildren: () => import('./buyer-invoice-table/buyer-invoice-table-relatedinfo.module').then((m) => m.BuyerTableBuyerInvoiceRelatedinfoModule)
  },
  {
    path: 'amendcaline',
    loadChildren: () => import('./amend-ca-line/credit-app-line-amend-ca-line.module').then((m) => m.CreditAppLineAmendCaLineModule)
  },
  {
    path: 'attachment',
    loadChildren: () => import('./attachment/credit-app-line-attachment.module').then((m) => m.CreditAppLineTableAttachmentModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppLineRelatedinfoRoutingModule {}
