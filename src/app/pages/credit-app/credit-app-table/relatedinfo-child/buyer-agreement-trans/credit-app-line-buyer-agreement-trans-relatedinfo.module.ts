import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppLineBuyerAgreementTransRelatedinfoRoutingModule } from './credit-app-line-buyer-agreement-trans-relatedinfo-routing.module';
import { SharedModule } from 'shared/shared.module';
import { BuyerAgreementTransComponentModule } from 'components/buyer-agreement-trans/buyer-agreement-trans/buyer-agreement-trans.module';
import { BuyerAgreementTransListPage } from './buyer-agreement-trans-list/buyer-agreement-trans-list.page';
import { BuyerAgreementTransItemPage } from './buyer-agreement-trans-item/buyer-agreement-trans-item.page';

@NgModule({
  declarations: [BuyerAgreementTransListPage, BuyerAgreementTransItemPage],
  imports: [CommonModule, SharedModule, CreditAppLineBuyerAgreementTransRelatedinfoRoutingModule, BuyerAgreementTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppLineBuyerAgreementTransRelatedinfoModule {}
