import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CreditAppLineItemPage } from './credit-app-line-item/credit-app-line-item.page';
import { CreditAppTableItemPage } from './credit-app-table-item/credit-app-table-item.page';
import { CreditAppTableListPage } from './credit-app-table-list/credit-app-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: CreditAppTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CreditAppTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/creditappline-child/:id',
    component: CreditAppLineItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/credit-app-table-relatedinfo.module').then((m) => m.CreditAppTableRelatedinfoModule)
  },
  {
    path: ':id/creditappline-child/:id/relatedinfo',
    loadChildren: () => import('./relatedinfo-child/credit-app-line-relatedinfo.module').then((m) => m.CreditAppLineRelatedinfoModule)
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/credit-app-table-function.module').then((m) => m.CreditAppTableFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableRoutingModule {}
