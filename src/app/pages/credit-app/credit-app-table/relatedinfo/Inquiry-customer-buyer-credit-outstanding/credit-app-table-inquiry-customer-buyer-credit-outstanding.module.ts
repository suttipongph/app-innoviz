import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { InquiryCustomerBuyerCreditOutstandingListPage } from './Inquiry-customer-buyer-credit-outstanding-list/Inquiry-customer-buyer-credit-outstanding-list.page';
import { InquiryCALineOutstandingByCAComponentModule } from 'components/inquiry/inquiry-ca-line-outstanding-by-ca/inquiry-ca-line-outstanding-by-ca.module';
import { CreditAppTableInquiryCustomerBuyerCreditOutstandingRoutingModule } from './credit-app-table-inquiry-customer-buyer-credit-outstanding-routing.module';

@NgModule({
  declarations: [InquiryCustomerBuyerCreditOutstandingListPage],
  imports: [CommonModule, SharedModule, CreditAppTableInquiryCustomerBuyerCreditOutstandingRoutingModule, InquiryCALineOutstandingByCAComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTableInquiryCustomerBuyerCreditOutstandingModule {}
