import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { InquiryCustomerBuyerCreditOutstandingListPage } from './Inquiry-customer-buyer-credit-outstanding-list/Inquiry-customer-buyer-credit-outstanding-list.page';

const routes: Routes = [
  {
    path: '',
    component: InquiryCustomerBuyerCreditOutstandingListPage
    ,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableInquiryCustomerBuyerCreditOutstandingRoutingModule {}
