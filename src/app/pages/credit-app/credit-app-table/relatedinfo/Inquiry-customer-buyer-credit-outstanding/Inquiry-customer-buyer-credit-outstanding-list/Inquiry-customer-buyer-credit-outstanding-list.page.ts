import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'Inquiry-customer-buyer-credit-outstanding-list-page',
  templateUrl: './Inquiry-customer-buyer-credit-outstanding-list.page.html',
  styleUrls: ['./Inquiry-customer-buyer-credit-outstanding-list.page.scss']
})
export class InquiryCustomerBuyerCreditOutstandingListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  parentId: string = null;
  constructor(public uiService: UIControllerService) {
    this.parentId = uiService.getRelatedInfoParentTableKey();
  }

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canDelete = false;
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'creditAppTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.CUSTOMER_BUYER_CREDIT_OUTSTANDING,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/${ROUTE_RELATED_GEN.CUSTOMER_BUYER_CREDIT_OUTSTANDING}`
    };
  }
}
