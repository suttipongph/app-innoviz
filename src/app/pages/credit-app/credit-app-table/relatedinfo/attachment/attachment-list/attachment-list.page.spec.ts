import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AttachmentListPage } from './attachment-list.page';

describe('AttachmentListPage', () => {
  let component: AttachmentListPage;
  let fixture: ComponentFixture<AttachmentListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AttachmentListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachmentListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
