import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RetentionTransactionListPage } from './retention-transaction-list.page';

describe('RetentionTransactionListPage', () => {
  let component: RetentionTransactionListPage;
  let fixture: ComponentFixture<RetentionTransactionListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RetentionTransactionListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RetentionTransactionListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
