import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreditAppTableRelatedinfoRoutingModule } from './credit-app-table-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, CreditAppTableRelatedinfoRoutingModule]
})
export class CreditAppTableRelatedinfoModule {}
