import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AmendCaListPage } from './amend-ca-list.page';

describe('AmendCaListPage', () => {
  let component: AmendCaListPage;
  let fixture: ComponentFixture<AmendCaListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AmendCaListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmendCaListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
