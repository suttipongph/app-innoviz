import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AmendCaListPage } from './amend-ca-list/amend-ca-list.page';
import { AmendCaItemPage } from './amend-ca-item/amend-ca-item.page';
import { SharedModule } from 'shared/shared.module';
import { CreditAppRequestTableAmendComponentModule } from 'components/credit-app-request-table-amend/credit-app-request-table-amend/credit-app-request-table-amend.module';
import { AuthorizedPersonTransComponentModule } from 'components/authorized-person-trans/authorized-person-trans.module';
import { AuthorizedPersonTransItemPage } from './authorized-person-trans-item/authorized-person-trans-item.page';
import { GuarantorTransItemPage } from './guarantor-trans-item/guarantor-trans-item.page';
import { GuarantorTransComponentModule } from 'components/guarantor-trans/guarantor-trans/guarantor-trans.module';
import { AmendCaRelatedinfoRoutingModule } from './amend-ca-relatedinfo-routing.module';

@NgModule({
  declarations: [AmendCaListPage, AmendCaItemPage, AuthorizedPersonTransItemPage, GuarantorTransItemPage],
  imports: [
    CommonModule,
    SharedModule,
    AmendCaRelatedinfoRoutingModule,
    CreditAppRequestTableAmendComponentModule,
    AuthorizedPersonTransComponentModule,
    GuarantorTransComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AmendCaRelatedinfoModule {}
