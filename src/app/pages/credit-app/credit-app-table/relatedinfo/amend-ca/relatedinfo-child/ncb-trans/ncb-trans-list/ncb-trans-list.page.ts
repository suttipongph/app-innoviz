import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-ncb-trans-list-page',
  templateUrl: './ncb-trans-list.page.html',
  styleUrls: ['./ncb-trans-list.page.scss']
})
export class NCBTransListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'ncbPersonTransGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.NCB_TRANS,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/AuthorizedPersonTrans-Child/RelatedInfo/NCBTrans`
    };
  }
}
