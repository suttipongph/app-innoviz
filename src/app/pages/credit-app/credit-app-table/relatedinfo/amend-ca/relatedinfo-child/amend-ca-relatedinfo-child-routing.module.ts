import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'ncbtrans',
    loadChildren: () =>
      import('./ncb-trans/amend-ca-autorized-person-trans-ncb-trans-relatedinfo.module').then(
        (m) => m.AmendCaAutorizedPersonTransNcbTransRelatedinfoModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AmendCaRelatedinfoChildRoutingModule {}
