import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'primeng/api';
import { AmendCaRelatedinfoChildRoutingModule } from './amend-ca-relatedinfo-child-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, AmendCaRelatedinfoChildRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AmendCaRelatedinfoChildModule {}
