import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'printsetbookdoctrans',
    loadChildren: () => import('./print-book-doc/print-book-doc-function.module').then((m) => m.PrintBookDocFunctionModule)
  },
  {
    path: 'cancelcreditapprequest',
    loadChildren: () =>
      import('./cancel-credit-app-req/cancel-credit-app-req.page-function.module').then((m) => m.CancelCreditAppRequestFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AmendCaFunctionRoutingModule {}
