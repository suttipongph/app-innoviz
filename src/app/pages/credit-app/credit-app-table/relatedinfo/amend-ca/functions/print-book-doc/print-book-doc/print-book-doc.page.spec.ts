import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PrintBookDocPage } from './print-book-doc.page';

describe('AddendumPage', () => {
  let component: PrintBookDocPage;
  let fixture: ComponentFixture<PrintBookDocPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PrintBookDocPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintBookDocPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
