import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CancelCreditAppRequestPage } from './cancel-credit-app-req/cancel-credit-app-req.page';

const routes: Routes = [
  {
    path: '',
    component: CancelCreditAppRequestPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CancelCreditAppRequestFunctionRoutingModule {}
