import { Component, OnInit } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { UIControllerService } from 'core/services/uiController.service';
import { getProductType } from 'shared/functions/value.function';

@Component({
  selector: 'cancel-credit-app-req',
  templateUrl: './cancel-credit-app-req.page.html',
  styleUrls: ['./cancel-credit-app-req.page.scss']
})
export class CancelCreditAppRequestPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.CANCEL_CREDIT_APPLICATION_REQUEST,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/relatedinfo/amendca/Function/CancelCreditAppRequest`
    };
  }
}
