import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'shared/shared.module';
import { AmendCaFunctionRoutingModule } from './amend-ca-function.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedModule,
    AmendCaFunctionRoutingModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AmendCaFunctionsModule {}
