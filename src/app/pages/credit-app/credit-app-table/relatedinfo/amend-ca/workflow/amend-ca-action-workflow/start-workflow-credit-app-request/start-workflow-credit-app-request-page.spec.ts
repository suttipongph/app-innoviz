import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StartWorkflowCreditAppRequestPage } from './start-workflow-credit-app-request-page';

describe('DemoWorkflowComponent', () => {
  let component: StartWorkflowCreditAppRequestPage;
  let fixture: ComponentFixture<StartWorkflowCreditAppRequestPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StartWorkflowCreditAppRequestPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StartWorkflowCreditAppRequestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
