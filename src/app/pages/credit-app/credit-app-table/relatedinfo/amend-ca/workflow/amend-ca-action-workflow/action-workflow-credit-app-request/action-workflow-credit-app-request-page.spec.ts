import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionWorkflowCreditAppRequestPage } from './action-workflow-credit-app-request-page';

describe('DemoWorkflowComponent', () => {
  let component: ActionWorkflowCreditAppRequestPage;
  let fixture: ComponentFixture<ActionWorkflowCreditAppRequestPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ActionWorkflowCreditAppRequestPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionWorkflowCreditAppRequestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
