import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { AmendCaActionHistoryRoutingModule } from './amend-ca-action-history-routing.module';
import { WorkFlowCreditAppRequestActionHistoryPage } from './amend-ca-action-history/amend-ca-action-history.page';
import { ActionHistoryComponentModule } from 'components/action-history/action-history.module';

@NgModule({
  declarations: [WorkFlowCreditAppRequestActionHistoryPage],
  imports: [CommonModule, SharedModule, AmendCaActionHistoryRoutingModule, ActionHistoryComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AmendCaActionHistoryModule {}
