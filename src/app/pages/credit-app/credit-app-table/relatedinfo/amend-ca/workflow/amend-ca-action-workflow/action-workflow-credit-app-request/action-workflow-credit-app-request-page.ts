import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';
const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';
@Component({
  selector: 'app-action-workflow-credit-app-request',
  templateUrl: './action-workflow-credit-app-request-page.html',
  styleUrls: ['./action-workflow-credit-app-request-page.scss']
})
export class ActionWorkflowCreditAppRequestPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  productType: string = getProductType(this.masterRoute);
  productTypeWording: string = getProductType(this.masterRoute, true);
  activeName: string = this.uiService.getRelatedInfoActiveTableName();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    switch (this.activeName) {
      case LOAN_REQUEST:
        this.setPathLoanRequest();
        break;
      case BUYER_MATCHING:
        this.setPathBuyermatching();
        break;
      default:
        this.setPathMainCreditLimit();
        break;
    }
  }
  setPathMainCreditLimit(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.FACTORING_CREDIT_APPLICATION_TABLE,
      servicePath: `CreditAppTable/${this.productTypeWording}/RelatedInfo/AmendCa/Workflow/ActionWorkflow`,
      childPaths: [
        {
          pagePath: ROUTE_MASTER_GEN.FACTORING_CREDIT_APPLICATION_TABLE,
          servicePath: `CreditAppTable/${this.productTypeWording}/RelatedInfo/AmendCa/Workflow/ActionWorkflow`
        }
      ]
    };
  }
  setPathLoanRequest(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.PROJECT_FINANCE_CREDIT_APPLICATION_TABLE,
      servicePath: `CreditAppTable/${this.productTypeWording}/RelatedInfo/Loanrequest`,
      childPaths: [
        {
          pagePath: ROUTE_MASTER_GEN.PROJECT_FINANCE_CREDIT_APPLICATION_TABLE,
          servicePath: `CreditAppTable/${this.productTypeWording}/RelatedInfo/Loanrequest`
        }
      ]
    };
  }
  setPathBuyermatching(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.FACTORING_CREDIT_APPLICATION_TABLE,
      servicePath: `CreditAppTable/${this.productTypeWording}/RelatedInfo/Buyermatching`,
      childPaths: [
        {
          pagePath: ROUTE_MASTER_GEN.FACTORING_CREDIT_APPLICATION_TABLE,
          servicePath: `CreditAppTable/${this.productTypeWording}/RelatedInfo/Buyermatching`
        }
      ]
    };
  }
}
