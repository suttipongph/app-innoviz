import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AmendCaListPage } from './amend-ca-list/amend-ca-list.page';
import { AmendCaItemPage } from './amend-ca-item/amend-ca-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { AuthorizedPersonTransItemPage } from './authorized-person-trans-item/authorized-person-trans-item.page';
import { GuarantorTransItemPage } from './guarantor-trans-item/guarantor-trans-item.page';
import { AmendCaRelatedinfoModule } from './amend-ca-relatedinfo.module';

const routes: Routes = [
  {
    path: '',
    component: AmendCaListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: AmendCaItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/authorizedpersontrans-child/:id',
    component: AuthorizedPersonTransItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/guarantortrans-child/:id',
    component: GuarantorTransItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/authorizedpersontrans-child/:id/relatedinfo',
    loadChildren: () => import('./relatedinfo-child/amend-ca-relatedinfo-child.module').then((m) => m.AmendCaRelatedinfoChildModule)
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/amend-ca-relatedinfo.module').then((m) => m.AmendCaRelatedinfoModule)
  },
  {
    path: ':id/workflow',
    loadChildren: () => import('./workflow/amend-ca-action-workflow/amend-ca-action-workflow.module').then((m) => m.AmendCaActionWorkflowModule)
  },
  {
    path: ':id/workflow/actionhistory',
    loadChildren: () => import('./workflow/amend-ca-action-history/amend-ca-action-history.module').then((m) => m.AmendCaActionHistoryModule)
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/amend-ca-function.module').then((m) => m.AmendCaFunctionRoutingModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AmendCaRelatedinfoRoutingModule {}
