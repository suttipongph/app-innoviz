import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'primeng/api';
import { TaxReportTransRelatedinfoFunctionRoutingModule } from './tax-report-trans-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, TaxReportTransRelatedinfoFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TaxReportTransRelatedinfoFunctionModule {}
