import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditAppReqAssignmentItemPage } from './credit-app-req-assignment-item.page';

describe('BusinessCollateralItemPage', () => {
  let component: CreditAppReqAssignmentItemPage;
  let fixture: ComponentFixture<CreditAppReqAssignmentItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditAppReqAssignmentItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppReqAssignmentItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
