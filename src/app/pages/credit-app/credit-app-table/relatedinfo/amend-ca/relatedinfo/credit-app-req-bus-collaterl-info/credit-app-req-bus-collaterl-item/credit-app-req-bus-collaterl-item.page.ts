import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';
import { AccessModeView } from 'shared/models/viewModel';

@Component({
  selector: 'credit-app-req-bus-collaterl-item.page',
  templateUrl: './credit-app-req-bus-collaterl-item.page.html',
  styleUrls: ['./credit-app-req-bus-collaterl-item.page.scss']
})
export class CreditAppRequestTableBusinessCollateralItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.CREDIT_APP_REQUEST_TABLE_BUSINESS_COLLATERAL,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral`
    };
  }
}
