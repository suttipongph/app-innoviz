import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FinancialCreditListPage } from './financial-credit-list.page';

describe('FinancialCreditListPage', () => {
  let component: FinancialCreditListPage;
  let fixture: ComponentFixture<FinancialCreditListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FinancialCreditListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialCreditListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
