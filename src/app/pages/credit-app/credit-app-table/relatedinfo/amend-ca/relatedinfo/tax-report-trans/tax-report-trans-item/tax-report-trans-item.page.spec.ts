import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TaxReportTransItemPage } from './tax-report-trans-item.page';

describe('TaxReportTransItemPage', () => {
  let component: TaxReportTransItemPage;
  let fixture: ComponentFixture<TaxReportTransItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaxReportTransItemPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxReportTransItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
