import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { TaxReportTransComponentModule } from 'components/tax-report-trans/tax-report-trans/tax-report-trans.module';
import { TaxReportTransItemPage } from './tax-report-trans-item/tax-report-trans-item.page';
import { TaxReportTransListPage } from './tax-report-trans-list/tax-report-trans-list.page';
import { AmendCaTaxReportTransRelatedinfoRoutingModule } from './amend-ca-tax-report-trans-relatedinfo-routing.module';

@NgModule({
  declarations: [TaxReportTransListPage, TaxReportTransItemPage],
  imports: [CommonModule, SharedModule, AmendCaTaxReportTransRelatedinfoRoutingModule, TaxReportTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AmendCaTaxReportTransRelatedinfoModule {}
