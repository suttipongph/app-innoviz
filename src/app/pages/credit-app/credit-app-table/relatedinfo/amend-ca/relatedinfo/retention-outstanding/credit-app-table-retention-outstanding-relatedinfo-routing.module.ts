import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RetentionOutstandingListPage } from './retention-outstanding-list/retention-outstanding-list.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: RetentionOutstandingListPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableRetentionOutstandingRelatedinfoRoutingModule {}
