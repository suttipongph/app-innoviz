import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'memotrans',
    loadChildren: () => import('./memo/amend-ca-memo-trans-relatedinfo.module').then((m) => m.AmendCaMemoTransRelatedinfoModule)
  },
  {
    path: 'businesscollateral',
    loadChildren: () =>
      import('./credit-app-req-bus-collaterl-info/credit-app-request-business-collateral-relatedinfo.module').then(
        (m) => m.CreditAppRequestTableBusinessCollateralRelatedinfoModule
      )
  },
  {
    path: 'bookmarkdocumenttrans',
    loadChildren: () =>
      import('./bookmark-document-trans/amend-ca-bookmark-document-trans-relatedinfo.module').then(
        (m) => m.AmendCaBookmarkDocumentTransRelatedinfoModule
      )
  },
  {
    path: 'financialcredittrans',
    loadChildren: () =>
      import('./financial/financial-credit-trans-relatedinfo.module').then((m) => m.CreditAppRequestTableFinancialCreditRelatedinfoModule)
  },
  {
    path: 'financialcredittrans',
    loadChildren: () =>
      import('./financial-credit/financial-credit-trans-relatedinfo.module').then((m) => m.CreditAppRequestTableFinancialCreditRelatedinfoModule)
  },
  {
    path: 'ncbtrans',
    loadChildren: () => import('./ncb-trans/credit-app-table-ncb-trans-relatedinfo.module').then((m) => m.CreditAppRequestTableNCBRelatedinfoModule)
  },
  {
    path: 'financialstatementtrans',
    loadChildren: () =>
      import('./financial-statement-trans/amend-ca-financial-statement-trans-relatedinfo.module').then(
        (m) => m.AmendCaFinancialStatementTransRelatedinfoModule
      )
  },
  {
    path: 'taxreporttrans',
    loadChildren: () =>
      import('./tax-report-trans/amend-ca-tax-report-trans-relatedinfo.module').then((m) => m.AmendCaTaxReportTransRelatedinfoModule)
  },
  { path: 'attachment', loadChildren: () => import('./attachment/amend-ca-attachment.module').then((m) => m.AmendCAAttachmentModule) },
  {
    path: 'retentionoutstanding',
    loadChildren: () =>
      import('./retention-outstanding/credit-app-table-retention-outstanding-relatedinfo.module').then(
        (m) => m.CreditAppTableRetentionOutstandingRelatedinfoModule
      )
  },
  {
    path: 'assignmentagreementoutstanding',
    loadChildren: () =>
      import('./assignment-agreement-outstanding/credit-app-table-assignment-agreement-outstanding-relatedinfo.module').then(
        (m) => m.CreditAppTableAssignmentAgreementOutstandingRelatedinfoModule
      )
  },
  {
    path: 'creditoutstanding',
    loadChildren: () =>
      import('./credit-outstanding/credit-app-request-credit-outstanding-relatedinfo.module').then(
        (m) => m.CreditAppRequestTableCreditOutstandingRelatedinfoModule
      )
  },
  {
    path: 'assignment',
    loadChildren: () =>
      import('./credit-app-req-assignment-info/amend-ca-assignment-relatedinfo.module').then((m) => m.AmendCaAssignmentRelatedinfoModule)
  },
  {
    path: 'cabuyercreditoutstanding',
    loadChildren: () =>
      import('./ca-buyer-credit-outstanding/amend-ca-ca-buyer-credit-outstanding-relatedinfo.module').then(
        (m) => m.AmendCCreditAppRequestTableCaBuyerCreditOutstandingRelatedinfoModule
      )
  },
  {
    path: 'servicefeeconditiontrans',
    loadChildren: () =>
      import('./service-fee-condition/amend-ca-service-fee-condition-relatedinfo.module').then((m) => m.AmendCaServiceFeeConditionRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AmendCaRelatedinfoRoutingModule {}
