import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'financial-credit-item.page',
  templateUrl: './financial-credit-item.page.html',
  styleUrls: ['./financial-credit-item.page.scss']
})
export class FinancialCreditItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.FINANCIAL_CREDIT,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans`
    };
  }
}
