import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AmendCaAssignmentRelatedinfoRoutingModule } from './amend-ca-assignment-relatedinfo-routing.module';
import { SharedModule } from 'shared/shared.module';
import { CreditAppReqAssignmentInfoItemPage } from './credit-app-req-assignment-info-item/credit-app-req-assignment-info-item.page';
import { ExistingAssignmentAgreementItemPage } from './existing-assignment-agreement-item/existing-assignment-agreement-item.page';
import { CreditAppReqAssignmentItemPage } from './credit-app-req-assignment-item/credit-app-req-assignment-item.page';
import { CreditAppReqAssignmentComponentModule } from 'components/credit-app-req-assignment/credit-app-req-assignment/credit-app-req-assignment.module';
import { CreditAppReqAssignmentInfoComponentModule } from 'components/credit-app-req-assignment/credit-app-req-assignment-info/credit-app-req-assignment-info.module';
import { ExistingAssignmentAgreementComponentModule } from 'components/credit-app-req-assignment/existing-assignment-agreement/existing-assignment-agreement.module';

@NgModule({
  declarations: [CreditAppReqAssignmentInfoItemPage, ExistingAssignmentAgreementItemPage, CreditAppReqAssignmentItemPage],
  imports: [
    CommonModule,
    SharedModule,
    AmendCaAssignmentRelatedinfoRoutingModule,
    CreditAppReqAssignmentComponentModule,
    CreditAppReqAssignmentInfoComponentModule,
    ExistingAssignmentAgreementComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AmendCaAssignmentRelatedinfoModule {}
