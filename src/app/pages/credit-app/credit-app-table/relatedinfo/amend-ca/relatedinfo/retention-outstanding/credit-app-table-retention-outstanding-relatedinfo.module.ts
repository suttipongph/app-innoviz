import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppTableRetentionOutstandingRelatedinfoRoutingModule } from './credit-app-table-retention-outstanding-relatedinfo-routing.module';
import { RetentionOutstandingListPage } from './retention-outstanding-list/retention-outstanding-list.page';
import { SharedModule } from 'shared/shared.module';
import { CAReqRetentionOutstandingComponentModule } from 'components/ca-req-retention-outstanding/ca-req-retention-outstanding/ca-req-retention-outstanding.module';

@NgModule({
  declarations: [RetentionOutstandingListPage],
  imports: [CommonModule, SharedModule, CreditAppTableRetentionOutstandingRelatedinfoRoutingModule, CAReqRetentionOutstandingComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTableRetentionOutstandingRelatedinfoModule {}
