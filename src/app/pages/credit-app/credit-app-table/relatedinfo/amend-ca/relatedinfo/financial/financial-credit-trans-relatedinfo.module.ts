import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreditAppRequestTableFinancialCreditRelatedinfoRoutingModule } from './financial-credit-trans-relatedinfo-routing.module';
import { SharedModule } from 'shared/shared.module';
import { FinancialCreditTransComponentModule } from 'components/financial-credit-trans/financial-credit-trans.module';
import { FinancialCreditItemPage } from './financial-credit-item/financial-credit-item.page';
import { FinancialCreditListPage } from './financial-credit-list/financial-credit-list.page';

@NgModule({
  declarations: [FinancialCreditItemPage, FinancialCreditListPage],
  imports: [CommonModule, SharedModule, CreditAppRequestTableFinancialCreditRelatedinfoRoutingModule, FinancialCreditTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppRequestTableFinancialCreditRelatedinfoModule {}
