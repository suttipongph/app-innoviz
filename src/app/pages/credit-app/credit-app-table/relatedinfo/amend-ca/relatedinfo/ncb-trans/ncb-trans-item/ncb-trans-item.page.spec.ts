import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NCBTransItemPage } from './ncb-trans-item.page';

describe('NCBTransItem', () => {
  let component: NCBTransItemPage;
  let fixture: ComponentFixture<NCBTransItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NCBTransItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NCBTransItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
