import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CustBusinessCollateralItemPage } from './cust-business-collateral-item/cust-business-collateral-item.page';
import { CreditAppRequestTableBusinessCollateralInfoItemPage } from './credit-app-req-bus-collaterl-info-item/credit-app-req-bus-collaterl-info-item.page';
import { CreditAppRequestTableBusinessCollateralItemPage } from './credit-app-req-bus-collaterl-item/credit-app-req-bus-collaterl-item.page';

const routes: Routes = [
  {
    path: '',
    component: CreditAppRequestTableBusinessCollateralInfoItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'creditapprequesttablebusinesscollateral-child/:id',
    component: CreditAppRequestTableBusinessCollateralItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'custbusinesscollateral-child/:id',
    component: CustBusinessCollateralItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppRequestTableBusinessCollateralRelatedinfoRoutingModule {}
