import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookmarkDocumentFunctionRoutingModule } from './bookmark-document-trans-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, BookmarkDocumentFunctionRoutingModule]
})
export class BookmarkDocumentFunctionModule {}
