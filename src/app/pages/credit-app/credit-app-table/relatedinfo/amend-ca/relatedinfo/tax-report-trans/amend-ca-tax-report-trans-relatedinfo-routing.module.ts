import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { TaxReportTransItemPage } from './tax-report-trans-item/tax-report-trans-item.page';
import { TaxReportTransListPage } from './tax-report-trans-list/tax-report-trans-list.page';

const routes: Routes = [
  {
    path: '',
    component: TaxReportTransListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: TaxReportTransItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'function',
    loadChildren: () => import('./functions/tax-report-trans-function.module').then((m) => m.TaxReportTransRelatedinfoFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AmendCaTaxReportTransRelatedinfoRoutingModule {}
