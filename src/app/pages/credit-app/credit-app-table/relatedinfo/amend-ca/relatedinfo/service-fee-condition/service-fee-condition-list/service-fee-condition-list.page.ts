import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'service-fee-condition-list-page',
  templateUrl: './service-fee-condition-list.page.html',
  styleUrls: ['./service-fee-condition-list.page.scss']
})
export class ServiceFeeConditionListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.SERVICE_FEE_CONDITION_TRANS,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans`
    };
  }
}
