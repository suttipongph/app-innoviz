import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrintBookDocTransFunctionRoutingModule } from './print-book-doc-trans-function-routing.module';
import { PrintBookDocTransPage } from './print-book-doc-trans/print-book-doc-trans.page';
import { SharedModule } from 'shared/shared.module';
import { PrintBookDocTransComponentModule } from 'components/bookmark-document-trans/print-book-doc-trans/print-book-doc-trans.module';

@NgModule({
  declarations: [PrintBookDocTransPage],
  imports: [CommonModule, SharedModule, PrintBookDocTransFunctionRoutingModule, PrintBookDocTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PrintBookDocTransFunctionModule {}
