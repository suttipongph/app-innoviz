import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CopyTaxReportTransPage } from './copy-tax-report-trans/copy-tax-report-trans.page';

const routes: Routes = [
  {
    path: '',
    component: CopyTaxReportTransPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppReqTableCopyTaxReportFunctionRoutingModule {}
