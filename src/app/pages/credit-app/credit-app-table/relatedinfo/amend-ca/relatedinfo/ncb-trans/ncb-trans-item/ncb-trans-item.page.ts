import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-ncb-trans-item-page',
  templateUrl: './ncb-trans-item.page.html',
  styleUrls: ['./ncb-trans-item.page.scss']
})
export class NCBTransItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.NCB_TRANS,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/AmendCa/RelatedInfo/NCBTrans`
    };
  }
}
