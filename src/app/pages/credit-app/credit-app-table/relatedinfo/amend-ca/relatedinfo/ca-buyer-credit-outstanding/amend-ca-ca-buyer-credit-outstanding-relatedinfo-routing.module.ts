import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CaBuyerCreditOutstandingListPage } from './ca-buyer-credit-outstanding-list/ca-buyer-credit-outstanding-list.page';

const routes: Routes = [
  {
    path: '',
    component: CaBuyerCreditOutstandingListPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AmendCaCreditAppRequestTableCaBuyerCreditOutstandingRelatedinfoRoutingModule {}
