import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { OptionModel, PageInformationModel } from 'shared/models/systemModel';

const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';

@Component({
  selector: 'credit-app-req-assignment-info-item.page',
  templateUrl: './credit-app-req-assignment-info-item.page.html',
  styleUrls: ['./credit-app-req-assignment-info-item.page.scss']
})
export class CreditAppReqAssignmentInfoItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  option: OptionModel[];
  constructor(public uiService: UIControllerService) {}
  activeName: string = this.uiService.getRelatedInfoActiveTableName();
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.CREDIT_APP_REQUEST_TABLE_ASSIGNMENT,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/AmendCa/RelatedInfo/Assignment`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.EXIS_ASSIGNMENT,
          servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/AmendCa/RelatedInfo/Assignment`
        },
        {
          pagePath: ROUTE_RELATED_GEN.CREDIT_APP_REQUEST_TABLE_ASSIGNMENT,
          servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/AmendCa/RelatedInfo/Assignment`
        }
      ]
    };
  }
}
