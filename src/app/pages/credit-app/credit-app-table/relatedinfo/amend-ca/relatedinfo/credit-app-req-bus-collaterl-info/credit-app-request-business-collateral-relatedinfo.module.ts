import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppRequestTableBusinessCollateralRelatedinfoRoutingModule } from './credit-app-request-business-collateral-relatedinfo-routing.module';
import { SharedModule } from 'shared/shared.module';
import { CreditAppRequestTableBusinessCollateralInfoItemPage } from './credit-app-req-bus-collaterl-info-item/credit-app-req-bus-collaterl-info-item.page';
import { CreditAppReqBusCollaterlInfoComponentModule } from 'components/credit-app-req-business-collateral/credit-app-req-bus-collaterl-info/credit-app-req-bus-collaterl-info.module';
import { CreditAppReqBusinessCollateralComponentModule } from 'components/credit-app-req-business-collateral/credit-app-req-business-collateral/credit-app-req-business-collateral.module';
import { CustBusinessCollateralComponentModule } from 'components/cust-business-collateral/cust-business-collateral.module';
import { CustBusinessCollateralItemPage } from './cust-business-collateral-item/cust-business-collateral-item.page';
import { CreditAppRequestTableBusinessCollateralItemPage } from './credit-app-req-bus-collaterl-item/credit-app-req-bus-collaterl-item.page';

@NgModule({
  declarations: [
    CreditAppRequestTableBusinessCollateralInfoItemPage,
    CustBusinessCollateralItemPage,
    CreditAppRequestTableBusinessCollateralItemPage
  ],
  imports: [
    CommonModule,
    SharedModule,
    CreditAppRequestTableBusinessCollateralRelatedinfoRoutingModule,
    CreditAppReqBusCollaterlInfoComponentModule,
    CreditAppReqBusinessCollateralComponentModule,
    CustBusinessCollateralComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppRequestTableBusinessCollateralRelatedinfoModule {}
