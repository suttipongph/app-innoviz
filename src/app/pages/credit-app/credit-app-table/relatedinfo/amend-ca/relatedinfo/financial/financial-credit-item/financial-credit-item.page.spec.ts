import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FinancialCreditItemPage } from './financial-credit-item.page';

describe('FinancialCreditItemPage', () => {
  let component: FinancialCreditItemPage;
  let fixture: ComponentFixture<FinancialCreditItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FinancialCreditItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialCreditItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
