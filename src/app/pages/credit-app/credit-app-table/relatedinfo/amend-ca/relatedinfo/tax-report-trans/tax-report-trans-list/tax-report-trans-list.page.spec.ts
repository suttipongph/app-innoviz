import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TaxReportTransListPage } from './tax-report-trans-list.page';

describe('TaxReportTransListPage', () => {
  let component: TaxReportTransListPage;
  let fixture: ComponentFixture<TaxReportTransListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaxReportTransListPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxReportTransListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
