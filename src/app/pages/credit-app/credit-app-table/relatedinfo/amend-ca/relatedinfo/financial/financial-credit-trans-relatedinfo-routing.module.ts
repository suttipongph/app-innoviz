import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { FinancialCreditItemPage } from './financial-credit-item/financial-credit-item.page';
import { FinancialCreditListPage } from './financial-credit-list/financial-credit-list.page';

const routes: Routes = [
  {
    path: '',
    component: FinancialCreditListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: FinancialCreditItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppRequestTableFinancialCreditRelatedinfoRoutingModule {}
