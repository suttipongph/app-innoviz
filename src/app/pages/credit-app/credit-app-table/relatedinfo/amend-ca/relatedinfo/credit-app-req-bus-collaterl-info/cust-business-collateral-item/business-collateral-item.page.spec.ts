import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CustBusinessCollateralItemPage } from './cust-business-collateral-item.page';

describe('BusinessCollateralItemPage', () => {
  let component: CustBusinessCollateralItemPage;
  let fixture: ComponentFixture<CustBusinessCollateralItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CustBusinessCollateralItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustBusinessCollateralItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
