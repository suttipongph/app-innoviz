import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditAppRequestTableBusinessCollateralInfoItemPage } from './credit-app-req-bus-collaterl-info-item.page';

describe('BusinessCollateralItemPage', () => {
  let component: CreditAppRequestTableBusinessCollateralInfoItemPage;
  let fixture: ComponentFixture<CreditAppRequestTableBusinessCollateralInfoItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditAppRequestTableBusinessCollateralInfoItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppRequestTableBusinessCollateralInfoItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
