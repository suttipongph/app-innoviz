import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { AmendCaRelatedinfoRoutingModule } from './amend-ca-relatedinfo-routing.module';
import { BookmarkDocumentTransItemComponent } from 'components/bookmark-document-trans/bookmark-document-trans/bookmark-document-trans-item/bookmark-document-trans-item.component';
import { BookmarkDocumentTransListComponent } from 'components/bookmark-document-trans/bookmark-document-trans/bookmark-document-trans-list/bookmark-document-trans-list.component';
//import { BookmarkDocumentTransItemComponent } from './bookmark-document-trans/bookmark-document-trans-item/bookmark-document-trans-item.component';
//import { BookmarkDocumentTransListComponent } from './bookmark-document-trans/bookmark-document-trans-list/bookmark-document-trans-list.component';

@NgModule({
  //declarations: [BookmarkDocumentTransItemComponent, BookmarkDocumentTransListComponent],
  imports: [CommonModule, SharedModule, AmendCaRelatedinfoRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AmendCaRelatedinfoModule {}
