import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AmendCaItemPage } from './amend-ca-item.page';

describe('AmendCaItemPage', () => {
  let component: AmendCaItemPage;
  let fixture: ComponentFixture<AmendCaItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AmendCaItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmendCaItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
