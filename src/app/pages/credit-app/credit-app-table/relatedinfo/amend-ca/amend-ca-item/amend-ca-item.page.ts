import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'amend-ca-item.page',
  templateUrl: './amend-ca-item.page.html',
  styleUrls: ['./amend-ca-item.page.scss']
})
export class AmendCaItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.FACTORING_CREDIT_APPLICATION_TABLE,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/AmendCa`,
      childPaths: [
        {
          pagePath: ROUTE_MASTER_GEN.AUTHORIZED_PERSON_TRANS_CHILD,
          servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/AmendCa`
        },
        {
          pagePath: ROUTE_MASTER_GEN.GUARANTOR_TRANS_CHILD,
          servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/AmendCa`
        }
      ]
    };
  }
}
