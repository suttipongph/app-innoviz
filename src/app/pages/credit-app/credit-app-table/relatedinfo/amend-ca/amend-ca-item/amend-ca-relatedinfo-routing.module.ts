import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AmendCaListPage } from '../amend-ca-list/amend-ca-list.page';
import { AmendCaItemPage } from './amend-ca-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { AuthorizedPersonTransItemPage } from '../authorized-person-trans-item/authorized-person-trans-item.page';
import { GuarantorTransItemPage } from '../guarantor-trans-item/guarantor-trans-item.page';

const routes: Routes = [
  {
    path: '',
    component: AmendCaListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: AmendCaItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/authorizedpersontrans-child/:id',
    component: AuthorizedPersonTransItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/guarantortrans-child/:id',
    component: GuarantorTransItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AmendCaRelatedinfoRoutingModule {}
