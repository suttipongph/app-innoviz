import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CloseCreditLimitListPage } from './close-credit-limit-list/close-credit-limit-list.page';
import { CloseCreditLimitItemPage } from './close-credit-limit-item/close-credit-limit-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: CloseCreditLimitListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CloseCreditLimitItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/close-credit-limit-relatedinfo-memo.module').then((m) => m.CloseCreditLimitRelatedinfoModule)
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/credit-app-table-function.module').then((m) => m.CloseCreditLimitFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CloseCreditLimitRelatedinfoRoutingModule {}
