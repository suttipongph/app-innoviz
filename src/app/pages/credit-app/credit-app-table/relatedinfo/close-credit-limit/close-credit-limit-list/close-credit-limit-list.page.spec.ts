import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CloseCreditLimitListPage } from './close-credit-limit-list.page';

describe('CloseCreditLimitListPage', () => {
  let component: CloseCreditLimitListPage;
  let fixture: ComponentFixture<CloseCreditLimitListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CloseCreditLimitListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CloseCreditLimitListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
