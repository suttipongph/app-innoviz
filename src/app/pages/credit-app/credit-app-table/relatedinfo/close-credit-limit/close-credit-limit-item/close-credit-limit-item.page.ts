import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'close-credit-limit-item.page',
  templateUrl: './close-credit-limit-item.page.html',
  styleUrls: ['./close-credit-limit-item.page.scss']
})
export class CloseCreditLimitItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.CLOSE_CREDIT_LIMIT,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/CloseCreditLimit`
    };
  }
}
