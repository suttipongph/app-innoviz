import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CloseCreditLimitItemPage } from './close-credit-limit-item.page';

describe('CloseCreditLimitItemPage', () => {
  let component: CloseCreditLimitItemPage;
  let fixture: ComponentFixture<CloseCreditLimitItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CloseCreditLimitItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CloseCreditLimitItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
