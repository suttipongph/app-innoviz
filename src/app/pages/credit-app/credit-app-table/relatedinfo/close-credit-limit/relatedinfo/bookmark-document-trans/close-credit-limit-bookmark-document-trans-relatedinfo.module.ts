import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { BookmarkDocumentTransListPage } from './bookmark-document-list/bookmark-document-trans-list.page';
import { BookmarkDocumentTransItemPage } from './bookmark-document-item/bookmark-document-trans-item.page';
import { CloseCreditLimitBookmarkDocumentTransRelatedinfoRoutingModule } from './close-credit-limit-bookmark-document-trans-relatedinfo-routing.module';
import { BookmarkDocumentTransComponentModule } from 'components/bookmark-document-trans/bookmark-document-trans/bookmark-document-trans.module';

@NgModule({
  declarations: [BookmarkDocumentTransListPage, BookmarkDocumentTransItemPage],
  imports: [CommonModule, SharedModule, CloseCreditLimitBookmarkDocumentTransRelatedinfoRoutingModule, BookmarkDocumentTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CloseCreditLimitBookmarkDocumentTransRelatedinfoModule {}
