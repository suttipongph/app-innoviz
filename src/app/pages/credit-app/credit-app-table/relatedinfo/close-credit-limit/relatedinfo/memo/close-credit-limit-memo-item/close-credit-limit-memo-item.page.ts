import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'close-credit-limit-memo-item.page',
  templateUrl: './close-credit-limit-memo-item.page.html',
  styleUrls: ['./close-credit-limit-memo-item.page.scss']
})
export class CloseCreditLimitMemoItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.MEMO,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans`
    };
  }
}
