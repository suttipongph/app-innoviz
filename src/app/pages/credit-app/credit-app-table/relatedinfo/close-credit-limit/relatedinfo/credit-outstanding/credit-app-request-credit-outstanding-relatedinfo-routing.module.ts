import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CreditOutstandingListPage } from './credit-outstanding-list/credit-outstanding-list.page';

const routes: Routes = [
  {
    path: '',
    component: CreditOutstandingListPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppRequestTableCreditOutstandingRelatedinfoRoutingModule {}
