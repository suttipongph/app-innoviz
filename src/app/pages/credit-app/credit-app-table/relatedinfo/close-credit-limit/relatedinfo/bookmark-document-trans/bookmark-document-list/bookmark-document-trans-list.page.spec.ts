import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookmarkDocumentTransListPage } from './bookmark-document-trans-list.page';

describe('BookmarkDocumentTransListComponent', () => {
  let component: BookmarkDocumentTransListPage;
  let fixture: ComponentFixture<BookmarkDocumentTransListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookmarkDocumentTransListPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkDocumentTransListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
