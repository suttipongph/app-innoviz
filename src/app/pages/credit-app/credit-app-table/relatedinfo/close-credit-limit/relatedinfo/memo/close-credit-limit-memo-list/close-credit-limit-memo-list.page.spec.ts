import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CloseCreditLimitMemoListPage } from './close-credit-limit-memo-list.page';

describe('MemoListPage', () => {
  let component: CloseCreditLimitMemoListPage;
  let fixture: ComponentFixture<CloseCreditLimitMemoListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CloseCreditLimitMemoListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CloseCreditLimitMemoListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
