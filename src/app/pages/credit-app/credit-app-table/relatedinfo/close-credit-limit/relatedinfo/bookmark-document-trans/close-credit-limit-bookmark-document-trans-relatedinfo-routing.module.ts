import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { BookmarkDocumentTransItemPage } from './bookmark-document-item/bookmark-document-trans-item.page';
import { BookmarkDocumentTransListPage } from './bookmark-document-list/bookmark-document-trans-list.page';

const routes: Routes = [
  {
    path: '',
    component: BookmarkDocumentTransListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BookmarkDocumentTransItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/bookmark-document-trans-function.module').then((m) => m.BookmarkDocumentFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CloseCreditLimitBookmarkDocumentTransRelatedinfoRoutingModule {}
