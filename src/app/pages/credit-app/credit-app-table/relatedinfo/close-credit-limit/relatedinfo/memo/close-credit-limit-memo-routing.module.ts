import { AuthGuardService } from 'core/services/auth-guard.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CloseCreditLimitMemoListPage } from './close-credit-limit-memo-list/close-credit-limit-memo-list.page';
import { CloseCreditLimitMemoItemPage } from './close-credit-limit-memo-item/close-credit-limit-memo-item.page';

const routes: Routes = [
  {
    path: '',
    component: CloseCreditLimitMemoListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CloseCreditLimitMemoItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CloseCreditLimitMemoRelatedinfoRoutingModule {}
