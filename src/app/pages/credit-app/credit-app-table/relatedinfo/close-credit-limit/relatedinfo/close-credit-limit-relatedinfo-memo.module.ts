import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CloseCreditLimitRelatedinfoRoutingModule } from './close-credit-limit-relatedinfo-memo-routing.module';
import { SharedModule } from 'shared/shared.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, CloseCreditLimitRelatedinfoRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CloseCreditLimitRelatedinfoModule {}
