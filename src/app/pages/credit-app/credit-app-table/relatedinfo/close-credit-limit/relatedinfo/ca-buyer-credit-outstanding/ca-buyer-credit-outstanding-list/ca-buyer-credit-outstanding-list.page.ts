import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';
const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';

@Component({
  selector: 'ca-buyer-credit-outstanding-list-page',
  templateUrl: './ca-buyer-credit-outstanding-list.page.html',
  styleUrls: ['./ca-buyer-credit-outstanding-list.page.scss']
})
export class CaBuyerCreditOutstandingListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  activeName: string = this.uiService.getRelatedInfoActiveTableName();

  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'CaBuyerCreditOutstandingGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.CA_BUYER_CREDIT_OUTSTANDING,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding`
    };
  }
}
