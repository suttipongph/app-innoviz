import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditOutstandingListPage } from './credit-outstanding-list.page';

describe('CreditOutstandingListPage', () => {
  let component: CreditOutstandingListPage;
  let fixture: ComponentFixture<CreditOutstandingListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditOutstandingListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditOutstandingListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
