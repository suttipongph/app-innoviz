import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [
  {
    path: 'memotrans',
    loadChildren: () => import('./memo/close-credit-limit-memo.module').then((m) => m.CloseCreditLimitMemoRelatedinfoModule)
  },
  {
    path: 'bookmarkdocumenttrans',
    loadChildren: () =>
      import('./bookmark-document-trans/close-credit-limit-bookmark-document-trans-relatedinfo.module').then(
        (m) => m.CloseCreditLimitBookmarkDocumentTransRelatedinfoModule
      )
  },
  {
    path: 'attachment',
    loadChildren: () => import('./attachment/close-credit-limit-attachment.module').then((m) => m.CloseCreditLimitAttachmentModule)
  },
  {
    path: 'assignmentagreementoutstanding',
    loadChildren: () =>
      import('./assignment-agreement-outstanding/credit-app-table-assignment-agreement-outstanding-relatedinfo.module').then(
        (m) => m.CreditAppTableAssignmentAgreementOutstandingRelatedinfoModule
      )
  },
  {
    path: 'creditoutstanding',
    loadChildren: () =>
      import('./credit-outstanding/credit-app-request-credit-outstanding-relatedinfo.module').then(
        (m) => m.CreditAppRequestTableCreditOutstandingRelatedinfoModule
      )
  },
  {
    path: 'cabuyercreditoutstanding',

    loadChildren: () =>
      import('./ca-buyer-credit-outstanding/amend-ca-ca-buyer-credit-outstanding-relatedinfo.module').then(
        (m) => m.AmendCCreditAppRequestTableCaBuyerCreditOutstandingRelatedinfoModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CloseCreditLimitRelatedinfoRoutingModule {}
