import { MemoTransComponentModule } from 'components/memo-trans/memo-trans/memo-trans.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CloseCreditLimitMemoRelatedinfoRoutingModule } from './close-credit-limit-memo-routing.module';
import { CloseCreditLimitMemoListPage } from './close-credit-limit-memo-list/close-credit-limit-memo-list.page';
import { CloseCreditLimitMemoItemPage } from './close-credit-limit-memo-item/close-credit-limit-memo-item.page';
import { SharedModule } from 'shared/shared.module';

@NgModule({
  declarations: [CloseCreditLimitMemoListPage, CloseCreditLimitMemoItemPage],
  imports: [CommonModule, SharedModule, CloseCreditLimitMemoRelatedinfoRoutingModule, MemoTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CloseCreditLimitMemoRelatedinfoModule {}
