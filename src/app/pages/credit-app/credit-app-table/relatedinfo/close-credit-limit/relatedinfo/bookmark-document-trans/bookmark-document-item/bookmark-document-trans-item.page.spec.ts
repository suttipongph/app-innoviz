import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookmarkDocumentTransItemPage } from './bookmark-document-trans-item.page';

describe('BookmarkDocumentTransItemComponent', () => {
  let component: BookmarkDocumentTransItemPage;
  let fixture: ComponentFixture<BookmarkDocumentTransItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookmarkDocumentTransItemPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkDocumentTransItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
