import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CloseCreditLimitMemoItemPage } from './close-credit-limit-memo-item.page';

describe('MemoItemPage', () => {
  let component: CloseCreditLimitMemoItemPage;
  let fixture: ComponentFixture<CloseCreditLimitMemoItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CloseCreditLimitMemoItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CloseCreditLimitMemoItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
