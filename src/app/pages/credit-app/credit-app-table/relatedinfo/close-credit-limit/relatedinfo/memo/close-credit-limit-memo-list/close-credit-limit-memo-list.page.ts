import { ROUTE_RELATED_GEN } from './../../../../../../../../shared/constants/constantGen';
import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';
import { getProductType } from 'shared/functions/value.function';
import { UIControllerService } from 'core/services/uiController.service';

@Component({
  selector: 'close-credit-limit-memo-list-page',
  templateUrl: './close-credit-limit-memo-list.page.html',
  styleUrls: ['./close-credit-limit-memo-list.page.scss']
})
export class CloseCreditLimitMemoListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'memoTransGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.MEMO,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans`
    };
  }
}
