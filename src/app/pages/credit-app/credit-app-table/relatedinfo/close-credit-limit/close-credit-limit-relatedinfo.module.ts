import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CloseCreditLimitRelatedinfoRoutingModule } from './close-credit-limit-relatedinfo-routing.module';
import { CloseCreditLimitListPage } from './close-credit-limit-list/close-credit-limit-list.page';
import { CloseCreditLimitItemPage } from './close-credit-limit-item/close-credit-limit-item.page';
import { SharedModule } from 'shared/shared.module';
import { ClosingCreditLimitRequestComponentModule } from 'components/closing-credit-limit-request/closing-credit-limit-request/closing-credit-limit-request.module';

@NgModule({
  declarations: [CloseCreditLimitListPage, CloseCreditLimitItemPage],
  imports: [CommonModule, SharedModule, CloseCreditLimitRelatedinfoRoutingModule, ClosingCreditLimitRequestComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CloseCreditLimitRelatedinfoModule {}
