import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { UpdateClosingResultPage } from './update-closing-result/update-closing-result.page';
import { UpdateClosingResultFunctionRoutingModule } from './update-closing-result-function-routing.module';
import { UpdateClosingResultComponentModule } from 'components/closing-credit-limit-request/update-closing-result/update-closing-result.module';

@NgModule({
  declarations: [UpdateClosingResultPage],
  imports: [CommonModule, SharedModule, UpdateClosingResultFunctionRoutingModule, UpdateClosingResultComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UpdateClosingResultFunctionModule {}
