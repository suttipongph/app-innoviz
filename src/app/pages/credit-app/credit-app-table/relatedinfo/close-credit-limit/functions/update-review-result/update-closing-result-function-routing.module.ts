import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { UpdateClosingResultPage } from './update-closing-result/update-closing-result.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateClosingResultPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpdateClosingResultFunctionRoutingModule {}
