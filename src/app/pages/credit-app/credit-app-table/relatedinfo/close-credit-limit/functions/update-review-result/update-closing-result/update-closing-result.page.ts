import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel, PathParamModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-update-closing-result-page',
  templateUrl: './update-closing-result.page.html',
  styleUrls: ['./update-closing-result.page.scss']
})
export class UpdateClosingResultPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }

  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.UPDATE_CLOSING_RESULT,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/CloseCreditLimit/Function/updateclosingresult`
    };
  }
}
