import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'primeng/api';
import { CloseCreditLimitFunctionRoutingModule } from './credit-app-table-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, CloseCreditLimitFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CloseCreditLimitFunctionModule {}
