import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CancelCreditAppRequestFunctionRoutingModule } from './cancel-credit-app-req.page-function-routing.module';
import { CancelCreditAppRequestPage } from './cancel-credit-app-req/cancel-credit-app-req.page';
import { SharedModule } from 'shared/shared.module';
import { CancelCreditAppRequestComponentModule } from 'components/credit-app-request/cancel-credit-app-request/cancel-credit-app-request.module';

@NgModule({
  declarations: [CancelCreditAppRequestPage],
  imports: [CommonModule, SharedModule, CancelCreditAppRequestFunctionRoutingModule, CancelCreditAppRequestComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CancelCreditAppRequestFunctionModule {}
