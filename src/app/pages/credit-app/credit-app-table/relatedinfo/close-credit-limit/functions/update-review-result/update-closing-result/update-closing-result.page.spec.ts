import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UpdateClosingResultPage } from './update-closing-result.page';

describe('AddendumPage', () => {
  let component: UpdateClosingResultPage;
  let fixture: ComponentFixture<UpdateClosingResultPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UpdateClosingResultPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateClosingResultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
