import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'credit-application-transaction-item.page',
  templateUrl: './credit-application-transaction-item.page.html',
  styleUrls: ['./credit-application-transaction-item.page.scss']
})
export class CreditApplicationTransactionItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.CREDIT_APPLICATION_TRANSACTION,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/CreditApplicationTransaction`
    };
  }
}
