import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditApplicationTransactionItemPage } from './credit-application-transaction-item.page';

describe('CreditApplicationTransactionItemPage', () => {
  let component: CreditApplicationTransactionItemPage;
  let fixture: ComponentFixture<CreditApplicationTransactionItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditApplicationTransactionItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditApplicationTransactionItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
