import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditApplicationTransactionRelatedinfoRoutingModule } from './credit-application-transaction-relatedinfo-routing.module';
import { CreditApplicationTransactionListPage } from './credit-application-transaction-list/credit-application-transaction-list.page';
import { CreditApplicationTransactionItemPage } from './credit-application-transaction-item/credit-application-transaction-item.page';
import { SharedModule } from 'shared/shared.module';
import { CreditAppTransComponentModule } from 'components/credit-app-trans/credit-app-trans/credit-app-trans.module';

@NgModule({
  declarations: [CreditApplicationTransactionListPage, CreditApplicationTransactionItemPage],
  imports: [CommonModule, SharedModule, CreditApplicationTransactionRelatedinfoRoutingModule, CreditAppTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditApplicationTransactionRelatedinfoModule {}
