import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreditApplicationTransactionListPage } from './credit-application-transaction-list/credit-application-transaction-list.page';
import { CreditApplicationTransactionItemPage } from './credit-application-transaction-item/credit-application-transaction-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: CreditApplicationTransactionListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CreditApplicationTransactionItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditApplicationTransactionRelatedinfoRoutingModule {}
