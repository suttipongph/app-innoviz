import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditApplicationTransactionListPage } from './credit-application-transaction-list.page';

describe('CreditApplicationTransactionListPage', () => {
  let component: CreditApplicationTransactionListPage;
  let fixture: ComponentFixture<CreditApplicationTransactionListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditApplicationTransactionListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditApplicationTransactionListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
