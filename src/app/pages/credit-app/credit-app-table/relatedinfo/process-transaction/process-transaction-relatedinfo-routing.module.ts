import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProcessTransListPage } from './process-transaction-list/process-transaction-list.page';
import { ProcessTransItemPage } from './process-transaction-item/process-transaction-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: ProcessTransListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ProcessTransItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableProcessTransactionRelatedinfoRoutingModule {}
