import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RetentionConditionListPage } from './retention-condition-list.page';

describe('RetentionConditionListPage', () => {
  let component: RetentionConditionListPage;
  let fixture: ComponentFixture<RetentionConditionListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RetentionConditionListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RetentionConditionListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
