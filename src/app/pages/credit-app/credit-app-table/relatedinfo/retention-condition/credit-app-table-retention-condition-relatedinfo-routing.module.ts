import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RetentionConditionListPage } from './retention-condition-list/retention-condition-list.page';
import { RetentionConditionItemPage } from './retention-condition-item/retention-condition-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: RetentionConditionListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: RetentionConditionItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableRetentionConditionRelatedinfoRoutingModule {}
