import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'withdrawal-table-list-page',
  templateUrl: './withdrawal-table-list.page.html',
  styleUrls: ['./withdrawal-table-list.page.scss']
})
export class WithdrawalTableListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  parentId = this.uiService.getRelatedInfoParentTableKey();

  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'withdrawalTableGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'creditAppTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_WITHDRAWAL,
      servicePath: `CreditAppTable/Projectfinance/RelatedInfo/${ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_WITHDRAWAL}`
    };
  }
}
