import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { WithdrawalLineItemPage } from './withdrawal-line-item/withdrawal-line-item.page';
import { WithdrawalTableItemPage } from './withdrawal-table-item/withdrawal-table-item.page';
import { WithdrawalTableListPage } from './withdrawal-table-list/withdrawal-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: WithdrawalTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: WithdrawalTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/withdrawallinewithdrawal-child/:id',
    component: WithdrawalLineItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/function',
    loadChildren: () =>
      import('./function/credit-app-table-withdrawal-table-function.module').then((m) => m.CreditAppTableWithdrawalTableFunctionModule)
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () =>
      import('./relatedinfo/credit-app-table-withdrawal-table-relatedinfo.module').then((m) => m.CreditAppTableWithdrawalTableRelatedinfoModule)
  },
  {
    path: ':id/withdrawallinewithdrawal-child/:id/relatedinfo',
    loadChildren: () =>
      import('./relatedinfo-child/credit-app-table-withdrawal-line-relatedinfo.module').then((m) => m.CreditAppTableWithdrawalLineRelatedinfoModule)
  },
  {
    path: ':id/report',
    loadChildren: () => import('./report/credit-app-table-withdrawal-table-report.module').then((m) => m.CreditAppTableWithdrawalTableReportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableWithdrawalTableRoutingModule {}
