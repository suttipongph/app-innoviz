import { ComponentFixture, TestBed } from '@angular/core/testing';
import { WithdrawalLineItemPage } from './withdrawal-line-item.page';

describe('WithdrawalLineItemPage', () => {
  let component: WithdrawalLineItemPage;
  let fixture: ComponentFixture<WithdrawalLineItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WithdrawalLineItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WithdrawalLineItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
