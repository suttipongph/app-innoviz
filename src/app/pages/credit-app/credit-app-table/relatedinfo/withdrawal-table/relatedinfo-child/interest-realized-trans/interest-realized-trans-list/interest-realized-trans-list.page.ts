import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'interest-realized-trans-list-page',
  templateUrl: './interest-realized-trans-list.page.html',
  styleUrls: ['./interest-realized-trans-list.page.scss']
})
export class InterestRealizedTransListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  parentRoute = this.uiService.getRelatedInfoParentTableKey();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'RefGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentRoute
      }
    ];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.INTEREST_REALIZED_TRANSACTION,

      servicePath: `CreditAppTable/ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/${ROUTE_RELATED_GEN.INTEREST_REALIZED_TRANSACTION}`
    };
  }
}
