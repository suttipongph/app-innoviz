import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'primeng/api';
import { CreditAppTableWithdrawalLineRelatedinfoRoutingModule } from './credit-app-table-withdrawal-line-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, CreditAppTableWithdrawalLineRelatedinfoRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTableWithdrawalLineRelatedinfoModule {}
