import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerTransactionListPage } from './customer-transaction-list/customer-transaction-list.page';
import { CustomerTransactionItemPage } from './customer-transaction-item/customer-transaction-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: CustomerTransactionListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CustomerTransactionItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerTransactionRelatedinfoRoutingModule {}
