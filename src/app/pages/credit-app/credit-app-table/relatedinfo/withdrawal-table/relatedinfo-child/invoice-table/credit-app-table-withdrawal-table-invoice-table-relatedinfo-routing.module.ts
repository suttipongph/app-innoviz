import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { InvoiceLineItemPage } from './invoice-line-item/invoice-line-item.page';
import { InvoiceTableItemPage } from './invoice-table-item/invoice-table-item.page';
import { InvoiceTableListPage } from './invoice-table-list/invoice-table.page';

const routes: Routes = [
  {
    path: '',
    component: InvoiceTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: InvoiceTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/invoiceline-child/:id',
    component: InvoiceLineItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/invoice-and-collection-relatedinfo-module').then((m) => m.InvoiceAndCollectionRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableWithdrawalTableInvoiceTableReledinfoRoutingModule {}
