import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'invoicetable',
    loadChildren: () =>
      import('./invoice-table/credit-app-table-withdrawal-table-invoice-table-relatedinfo.module').then(
        (m) => m.CreditAppTableWithdrawalTableInvoiceTableReledinfoModule
      )
  },
  {
    path: 'interestrealizedtrans',
    loadChildren: () =>
      import('./interest-realized-trans/withdrawal-line-interest-realized-trans-relatedinfo.module').then(
        (m) => m.WithdrawalLineInterestRealizedTransRelatedinfoModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableWithdrawalLineRelatedinfoRoutingModule {}
