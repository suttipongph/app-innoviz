import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { PrintWithdrawalComponentModule } from 'components/withdrawal/print-withdrawal/print-withdrawal.module';
import { PrintWithdrawalTablePage } from './print-withdrawal/print-withdrawal.page';
import { PrintWithdrawalTableReportRoutingModule } from './credit-app-table-print-withdrawal-report-routing.module';

@NgModule({
  declarations: [PrintWithdrawalTablePage],
  imports: [CommonModule, SharedModule, PrintWithdrawalTableReportRoutingModule, PrintWithdrawalComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PrintWithdrawalTableReportModule {}
