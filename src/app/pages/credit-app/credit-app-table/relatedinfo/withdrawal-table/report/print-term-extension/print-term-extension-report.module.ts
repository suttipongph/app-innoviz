import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { PrintWithdrawalComponentModule } from 'components/withdrawal/print-withdrawal/print-withdrawal.module';
import { PrintTermExtensionPage } from './print-term-extension/print-term-extension.page';
import { PrintTermExtensionReportRoutingModule } from './printterm-extension-report-routing.module';
import { PrintWithdrawalTermExtensionComponentModule } from 'components/withdrawal/print-withdrawal-term-extension/print-withdrawal-term-extension.module';

@NgModule({
  declarations: [PrintTermExtensionPage],
  imports: [
    CommonModule,
    SharedModule,
    PrintTermExtensionReportRoutingModule,
    PrintWithdrawalComponentModule,
    PrintWithdrawalTermExtensionComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PrintTermExtensionReportModule {}
