import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_REPORT_GEN } from 'shared/constants/constantGen';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'print-purchase-page',
  templateUrl: './print-term-extension.page.html',
  styleUrls: ['./print-term-extension.page.scss']
})
export class PrintTermExtensionPage implements OnInit {
  pageInfo: PageInformationModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_REPORT_GEN.PRINT_WITHDRAWAL_TERM_EXTENSION,
      servicePath: `CreditAppTable/ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/Report/PrintWithdrawalTermExtension`
    };
  }
}
