import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PrintWithdrawalTablePage } from './print-withdrawal/print-withdrawal.page';

const routes: Routes = [
  {
    path: '',
    component: PrintWithdrawalTablePage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrintWithdrawalTableReportRoutingModule {}
