import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_REPORT_GEN } from 'shared/constants/constantGen';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'print-purchase-page',
  templateUrl: './print-withdrawal.page.html',
  styleUrls: ['./print-withdrawal.page.scss']
})
export class PrintWithdrawalTablePage implements OnInit {
  pageInfo: PageInformationModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_REPORT_GEN.PRINT_WITHDRAWAL,
      servicePath: `CreditAppTable/ProjectFinance/RelatedInfo/${this.activeRoute}/Report/PrintWithdrawalTable`
    };
  }
}
