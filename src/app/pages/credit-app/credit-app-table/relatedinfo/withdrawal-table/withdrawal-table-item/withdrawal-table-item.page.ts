import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'withdrawal-table-item.page',
  templateUrl: './withdrawal-table-item.page.html',
  styleUrls: ['./withdrawal-table-item.page.scss']
})
export class WithdrawalTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_WITHDRAWAL,
      servicePath: `CreditAppTable/ProjectFinance/RelatedInfo/${ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_WITHDRAWAL}`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.WITHDRAWAL_LINE_WITHDRAWAL,
          servicePath: `CreditAppTable/ProjectFinance/RelatedInfo/${ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_WITHDRAWAL}`
        }
      ]
    };
  }
}
