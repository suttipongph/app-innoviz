import { Component, OnInit } from '@angular/core';
import { ROUTE_FUNCTION_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'post-withdrawal-page',
  templateUrl: './post-withdrawal.page.html',
  styleUrls: ['./post-withdrawal.page.scss']
})
export class PostWithdrawalPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.POST_WITHDRAWAL,
      servicePath: `CreditAppTable/ProjectFinance/RelatedInfo/${ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_WITHDRAWAL}/Function/PostWithdrawal`
    };
  }
}
