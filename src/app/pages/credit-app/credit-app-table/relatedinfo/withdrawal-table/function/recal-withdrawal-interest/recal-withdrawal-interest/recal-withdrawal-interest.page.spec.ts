import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecalWithdrawalInterestPage } from './recal-withdrawal-interest.page';

describe('RecalWithdrawalInterestPage', () => {
  let component: RecalWithdrawalInterestPage;
  let fixture: ComponentFixture<RecalWithdrawalInterestPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RecalWithdrawalInterestPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecalWithdrawalInterestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
