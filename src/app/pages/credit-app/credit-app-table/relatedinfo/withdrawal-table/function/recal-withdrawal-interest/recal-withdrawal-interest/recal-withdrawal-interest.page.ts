import { Component, OnInit } from '@angular/core';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'recal-withdrawal-interest-page',
  templateUrl: './recal-withdrawal-interest.page.html',
  styleUrls: ['./recal-withdrawal-interest.page.scss']
})
export class RecalWithdrawalInterestPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.RECALCULATE_INTEREST,
      servicePath: 'CreditAppTable/ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/Function/RecalculateInterest'
    };
  }
}
