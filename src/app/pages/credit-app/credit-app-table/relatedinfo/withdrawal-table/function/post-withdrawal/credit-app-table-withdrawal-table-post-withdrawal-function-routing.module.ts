import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PostWithdrawalPage } from './post-withdrawal/post-withdrawal.page';

const routes: Routes = [
  {
    path: '',
    component: PostWithdrawalPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableWithdrawalTablePostWithdrawalFunctionRoutingModule {}
