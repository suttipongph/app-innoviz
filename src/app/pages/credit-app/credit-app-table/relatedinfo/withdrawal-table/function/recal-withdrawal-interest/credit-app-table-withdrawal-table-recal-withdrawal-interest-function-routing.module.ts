import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { RecalWithdrawalInterestPage } from './recal-withdrawal-interest/recal-withdrawal-interest.page';

const routes: Routes = [
  {
    path: '',
    component: RecalWithdrawalInterestPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableWithdrawalTableRecalWithdrawalInterestFunctionRoutingModule {}
