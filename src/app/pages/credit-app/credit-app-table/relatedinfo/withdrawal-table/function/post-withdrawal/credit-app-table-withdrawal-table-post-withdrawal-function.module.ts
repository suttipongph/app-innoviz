import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { PostWithdrawalPage } from './post-withdrawal/post-withdrawal.page';
import { CreditAppTableWithdrawalTablePostWithdrawalFunctionRoutingModule } from './credit-app-table-withdrawal-table-post-withdrawal-function-routing.module';
import { PostWithdrawalComponentModule } from 'components/withdrawal/post-withdrawal/post-withdrawal.module';

@NgModule({
  declarations: [PostWithdrawalPage],
  imports: [CommonModule, SharedModule, CreditAppTableWithdrawalTablePostWithdrawalFunctionRoutingModule, PostWithdrawalComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTableWithdrawalTablePostWithdrawalFunctionModule {}
