import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'recalculateinterest',
    loadChildren: () =>
      import('./recal-withdrawal-interest/credit-app-table-withdrawal-table-recal-withdrawal-interest-function.module').then(
        (m) => m.CreditAppTableWithdrawalTableRecalWithdrawalInterestFunctionModule
      )
  },
  {
    path: 'postwithdrawal',
    loadChildren: () =>
      import('./post-withdrawal/credit-app-table-withdrawal-table-post-withdrawal-function.module').then(
        (m) => m.CreditAppTableWithdrawalTablePostWithdrawalFunctionModule
      )
  },
  {
    path: 'cancelwithdrawal',
    loadChildren: () =>
      import('./cancel-withdrawal/credit-app-table-withdrawal-table-cancel-withdrawal-function.module').then(
        (m) => m.CreditAppTableWithdrawalTableCancelWithdrawalFunctionModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableWithdrawalTableFunctionRoutingModule {}
