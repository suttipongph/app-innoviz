import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelWithdrawalPage } from './cancel-withdrawal.page';

describe('CancelWithdrawalPage', () => {
  let component: CancelWithdrawalPage;
  let fixture: ComponentFixture<CancelWithdrawalPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CancelWithdrawalPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelWithdrawalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
