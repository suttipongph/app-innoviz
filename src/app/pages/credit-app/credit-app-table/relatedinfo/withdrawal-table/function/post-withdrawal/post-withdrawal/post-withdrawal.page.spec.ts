import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostWithdrawalPage } from './post-withdrawal.page';

describe('PostWithdrawalPage', () => {
  let component: PostWithdrawalPage;
  let fixture: ComponentFixture<PostWithdrawalPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PostWithdrawalPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostWithdrawalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
