import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { RecalWithdrawalInterestPage } from './recal-withdrawal-interest/recal-withdrawal-interest.page';
import { CreditAppTableWithdrawalTableRecalWithdrawalInterestFunctionRoutingModule } from './credit-app-table-withdrawal-table-recal-withdrawal-interest-function-routing.module';
import { RecalWithdrawalInterestComponentModule } from 'components/withdrawal/recal-withdrawal-interest/recal-withdrawal-interest.module';

@NgModule({
  declarations: [RecalWithdrawalInterestPage],
  imports: [
    CommonModule,
    SharedModule,
    CreditAppTableWithdrawalTableRecalWithdrawalInterestFunctionRoutingModule,
    RecalWithdrawalInterestComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTableWithdrawalTableRecalWithdrawalInterestFunctionModule {}
