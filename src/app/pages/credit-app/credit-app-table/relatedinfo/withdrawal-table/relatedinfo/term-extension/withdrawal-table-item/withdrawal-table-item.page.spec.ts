import { ComponentFixture, TestBed } from '@angular/core/testing';
import { WithdrawalTableItemPage } from './withdrawal-table-item.page';

describe('WithdrawalItemPage', () => {
  let component: WithdrawalTableItemPage;
  let fixture: ComponentFixture<WithdrawalTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WithdrawalTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WithdrawalTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
