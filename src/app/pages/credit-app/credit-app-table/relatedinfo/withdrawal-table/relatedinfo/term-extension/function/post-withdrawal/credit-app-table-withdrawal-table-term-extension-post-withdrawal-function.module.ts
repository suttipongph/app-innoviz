import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { CreditAppTableWithdrawalTableTermExtensionPostWithdrawalFunctionRoutingModule } from './credit-app-table-withdrawal-table-term-extension-post-withdrawal-function-routing.module';
import { PostWithdrawalComponentModule } from 'components/withdrawal/post-withdrawal/post-withdrawal.module';
import { PostWithdrawalPage } from './post-withdrawal/post-withdrawal.page';

@NgModule({
  declarations: [PostWithdrawalPage],
  imports: [CommonModule, SharedModule, CreditAppTableWithdrawalTableTermExtensionPostWithdrawalFunctionRoutingModule, PostWithdrawalComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTableWithdrawalTableTermExtensionPostWithdrawalFunctionModule {}
