import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'withdrawal-table-list-page',
  templateUrl: './withdrawal-table-list.page.html',
  styleUrls: ['./withdrawal-table-list.page.scss']
})
export class WithdrawalTableListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  parentId = this.uiService.getRelatedInfoParentTableKey();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'withdrawalTableGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'originWithdrawalTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      },
      {
        label: null,
        textKey: 'termExtension',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: 'true'
      }
    ];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_TERM_EXTENSION,
      servicePath: `CreditAppTable/Projectfinance/RelatedInfo/${ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_WITHDRAWAL}/RelatedInfo/${ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_TERM_EXTENSION}`
    };
  }
}
