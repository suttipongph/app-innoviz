import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppTableWithdrawalTableVendorPaymentTransRelatedinfoRoutingModule } from './credit-app-table-withdrawal-table-vendor-payment-trans-relatedinfo-routing.module';
import { VendorPaymentTransListPage } from './vendor-payment-trans-list/vendor-payment-trans-list.page';
import { VendorPaymentTransItemPage } from './vendor-payment-trans-item/vendor-payment-trans-item.page';
import { SharedModule } from 'shared/shared.module';
import { VendorPaymentTransComponentModule } from 'components/vendor-payment-trans/vendor-payment-trans/vendor-payment-trans.module';

@NgModule({
  declarations: [VendorPaymentTransListPage, VendorPaymentTransItemPage],
  imports: [CommonModule, SharedModule, CreditAppTableWithdrawalTableVendorPaymentTransRelatedinfoRoutingModule, VendorPaymentTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTableWithdrawalTableVendorPaymentTransRelatedinfoModule {}
