import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ProductSettledTransItemPage } from './product-settled-trans-item/product-settled-trans-item-page';
import { ProductSettledTransListPage } from './product-settled-trans-list/product-settled-trans-list-page';

const routes: Routes = [
  {
    path: '',
    component: ProductSettledTransListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ProductSettledTransItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WithdrawalTableProductSettledTransRoutingModule {}
