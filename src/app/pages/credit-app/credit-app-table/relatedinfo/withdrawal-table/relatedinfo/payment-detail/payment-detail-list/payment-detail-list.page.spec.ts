import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PaymentDetailListPage } from './payment-detail-list.page';

describe('PaymentDetailListPage', () => {
  let component: PaymentDetailListPage;
  let fixture: ComponentFixture<PaymentDetailListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PaymentDetailListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentDetailListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
