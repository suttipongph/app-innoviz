import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'invoice-table-page',
  templateUrl: './invoice-table.page.html',
  styleUrls: ['./invoice-table.page.scss']
})
export class InvoiceTableListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'invoiceTableGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.INVOICE_TABLE,
      servicePath: `CreditAppTable/ProjectFinance/RelatedInfo/${this.activeRoute}/RelatedInfo/${ROUTE_RELATED_GEN.SERVICE_FEE_TRANS}/RelatedInfo/${ROUTE_RELATED_GEN.INVOICE_TABLE}`
    };
  }
}
