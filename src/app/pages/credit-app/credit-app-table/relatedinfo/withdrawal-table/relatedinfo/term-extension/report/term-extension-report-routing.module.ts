import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'printwithdrawaltermextension',
    loadChildren: () => import('./print-term-extension/print-term-extension-report.module').then((m) => m.PrintTermExtensionReportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TermExtensionReportRoutingModule {}
