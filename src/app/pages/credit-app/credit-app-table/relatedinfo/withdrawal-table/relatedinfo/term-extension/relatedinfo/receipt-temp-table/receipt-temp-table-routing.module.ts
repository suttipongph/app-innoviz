import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ReceiptTempPaymDetailItemPage } from './receipt-temp-paym-detail-item/receipt-temp-paym-detail-item.page';
import { ReceiptTempTableItemPage } from './receipt-temp-table-item/receipt-temp-table-item.page';
import { ReceiptTempTableListPage } from './receipt-temp-table-list/receipt-temp-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: ReceiptTempTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ReceiptTempTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/receipttemppaymdetail-child/:id',
    component: ReceiptTempPaymDetailItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/receipt-temp-table-relatedinfo.module').then((m) => m.ReceiptTempTableRelatedinfoModule)
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/receipt-temp-table-function.module').then((m) => m.ReceiptTempTableFunctionModule)
  },
  {
    path: ':id/report',
    loadChildren: () => import('./report/receipt-temp-report.module').then((m) => m.ReceiptTempReportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceiptTempTableRoutingModule {}
