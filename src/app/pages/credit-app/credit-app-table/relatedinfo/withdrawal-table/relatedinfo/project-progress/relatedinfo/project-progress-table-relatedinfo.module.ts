import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectProgressTableRelatedinfoRoutingModule } from './project-progress-table-relatedinfo-routing.module';
import { SharedModule } from 'shared/shared.module';
import { ProjectProgressTableComponentModule } from 'components/project-progress-table/project-progress-table.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, ProjectProgressTableRelatedinfoRoutingModule, ProjectProgressTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjectProgressTableRelatedinfoModule {}
