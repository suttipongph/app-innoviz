import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ProjectProgressItemPage } from './project-progress-item.page';

describe('ProjectProgressItemPage', () => {
  let component: ProjectProgressItemPage;
  let fixture: ComponentFixture<ProjectProgressItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProjectProgressItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectProgressItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
