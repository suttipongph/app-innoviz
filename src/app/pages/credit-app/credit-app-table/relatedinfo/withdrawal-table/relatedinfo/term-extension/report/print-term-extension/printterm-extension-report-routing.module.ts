import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PrintTermExtensionPage } from './print-term-extension/print-term-extension.page';

const routes: Routes = [
  {
    path: '',
    component: PrintTermExtensionPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrintTermExtensionReportRoutingModule {}
