import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PostReceiptTempPage } from './post/post-receipt-temp.page';

const routes: Routes = [
  {
    path: '',
    component: PostReceiptTempPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostReceiptTempFunctionRoutingModule {}
