import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReceiptTempTableReceiptTableRelatedinfoRoutingModule } from './receipt-temp-table-receipt-table-relatedinfo-routing.module';
import { ReceiptTableListPage } from './receipt-table-list/receipt-table-list.page';
import { ReceiptTableItemPage } from './receipt-table-item/receipt-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { ReceiptTableComponentModule } from 'components/receipt-table/receipt-table/receipt-table.module';
import { ReceiptLineComponentModule } from 'components/receipt-table/receipt-line/receipt-line.module';
import { ReceiptLineItemPage } from './receipt-line-item/receipt-line-item.page';

@NgModule({
  declarations: [ReceiptTableListPage, ReceiptTableItemPage, ReceiptLineItemPage],
  imports: [CommonModule, SharedModule, ReceiptTempTableReceiptTableRelatedinfoRoutingModule, ReceiptTableComponentModule, ReceiptLineComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReceiptTempTableReceiptTableRelatedinfoModule {}
