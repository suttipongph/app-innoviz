import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PaymentDetailItemPage } from './payment-detail-item.page';

describe('PaymentDetailItemPage', () => {
  let component: PaymentDetailItemPage;
  let fixture: ComponentFixture<PaymentDetailItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PaymentDetailItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentDetailItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
