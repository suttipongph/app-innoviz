import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtendTermPage } from './extend-term.page';

describe('ExtendTermPage', () => {
  let component: ExtendTermPage;
  let fixture: ComponentFixture<ExtendTermPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExtendTermPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtendTermPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
