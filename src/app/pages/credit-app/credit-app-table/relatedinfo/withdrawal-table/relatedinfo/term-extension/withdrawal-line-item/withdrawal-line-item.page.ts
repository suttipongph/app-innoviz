import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'withdrawal-line-item-page',
  templateUrl: './withdrawal-line-item.page.html',
  styleUrls: ['./withdrawal-line-item.page.scss']
})
export class WithdrawalLineItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.WITHDRAWAL_LINE_TERM_EXTENSION,
      servicePath: `CreditAppTable/Projectfinance/RelatedInfo/${ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_WITHDRAWAL}/RelatedInfo/${ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_TERM_EXTENSION}`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.WITHDRAWAL_LINE_TERM_EXTENSION,
          servicePath: `CreditAppTable/Projectfinance/RelatedInfo/${ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_WITHDRAWAL}/RelatedInfo/${ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_TERM_EXTENSION}`
        }
      ]
    };
  }
}
