import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { CancelWithdrawalPage } from './cancel-withdrawal/cancel-withdrawal.page';
import { CreditAppTableWithdrawalTableTermExtensionCancelWithdrawalFunctionRoutingModule } from './credit-app-table-withdrawal-table-cancel-withdrawal-function-routing.module';
import { CancelWithdrawalComponentModule } from 'components/withdrawal/cancel-withdrawal/cancel-withdrawal.module';

@NgModule({
  declarations: [CancelWithdrawalPage],
  imports: [
    CommonModule,
    SharedModule,
    CreditAppTableWithdrawalTableTermExtensionCancelWithdrawalFunctionRoutingModule,
    CancelWithdrawalComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTableWithdrawalTableTermExtensionCancelWithdrawalFunctionModule {}
