import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { EmptyGuid, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel, RowIdentity } from 'shared/models/systemModel';

@Component({
  selector: 'app-memo-list',
  templateUrl: './memo-list.page.html',
  styleUrls: ['./memo-list.page.scss']
})
export class MemoListPage implements OnInit {
  pageInfo: PageInformationModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.MEMO,
      servicePath: `CreditAppTable/ProjectFinance/RelatedInfo/${this.activeRoute}/RelatedInfo/${ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_TERM_EXTENSION}/RelatedInfo/ProjectProgress/RelatedInfo/MemoTrans`
    };
  }
}
