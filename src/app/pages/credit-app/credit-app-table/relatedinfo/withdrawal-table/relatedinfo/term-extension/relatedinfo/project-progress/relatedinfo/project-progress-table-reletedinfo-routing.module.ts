import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'attachment',
    loadChildren: () => import('./attachment/attachment.module').then((m) => m.AttachmentModule)
  },
  {
    path: 'memotrans',
    loadChildren: () =>
      import('./memo/project-progress-table-memo-trans-relatedinfo.module').then((m) => m.ProjectProgressTableTableMemoTransRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectProgressTableRelatedinfoRoutingModule {}
