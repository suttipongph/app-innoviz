import { Component, OnInit } from '@angular/core';
import { ColumnType, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-product-settled-trans-list-page',
  templateUrl: './product-settled-trans-list-page.html',
  styleUrls: ['./product-settled-trans-list-page.scss']
})
export class ProductSettledTransListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  refId;
  constructor() {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'productSettledTransGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }

  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.PRODUCT_SETTLEMENT,
      servicePath: `CreditAppTable/ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/${ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_TERM_EXTENSION}/RelatedInfo/ProductSettlement`
    };
  }
}
