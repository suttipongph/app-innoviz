import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppTableWithdrawalTableTermExtensionBuyerAgreementTransRelatedinfoRoutingModule } from './credit-app-table-withdrawal-table-term-extension-agreement-trans-relatedinfo-routing.module';
import { SharedModule } from 'shared/shared.module';
import { BuyerAgreementTransComponentModule } from 'components/buyer-agreement-trans/buyer-agreement-trans/buyer-agreement-trans.module';
import { BuyerAgreementTransListPage } from './buyer-agreement-trans-list/buyer-agreement-trans-list.page';
import { BuyerAgreementTransItemPage } from './buyer-agreement-trans-item/buyer-agreement-trans-item.page';

@NgModule({
  declarations: [BuyerAgreementTransListPage, BuyerAgreementTransItemPage],
  imports: [
    CommonModule,
    SharedModule,
    CreditAppTableWithdrawalTableTermExtensionBuyerAgreementTransRelatedinfoRoutingModule,
    BuyerAgreementTransComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTableWithdrawalTableTermExtensionBuyerAgreementTransRelatedinfoModule {}
