import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ProjectProgressListPage } from './project-progress-list.page';

describe('ProjectProgressListPage', () => {
  let component: ProjectProgressListPage;
  let fixture: ComponentFixture<ProjectProgressListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProjectProgressListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectProgressListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
