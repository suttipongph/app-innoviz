import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { TermExtensionReportRoutingModule } from './term-extension-report-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, TermExtensionReportRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TermExtensionReportModule {}
