import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CancelReceiptTempPage } from './cancel/cancel-receipt-temp.page';

const routes: Routes = [
  {
    path: '',
    component: CancelReceiptTempPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CancelReceiptTempFunctionRoutingModule {}
