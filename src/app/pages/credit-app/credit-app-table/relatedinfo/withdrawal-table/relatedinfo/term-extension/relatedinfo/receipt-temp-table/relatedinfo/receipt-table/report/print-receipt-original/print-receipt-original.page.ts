import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_REPORT_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'print-receipt-original-page',
  templateUrl: './print-receipt-original.page.html',
  styleUrls: ['./print-receipt-original.page.scss']
})
export class PrintReceiptOriginalPage implements OnInit {
  pageInfo: PageInformationModel;
  pageLabel: string;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageLabel = 'original';
    this.pageInfo = {
      pagePath: ROUTE_REPORT_GEN.PRINT_ORIGINAL,
      servicePath: `ReceiptTable/Report/PrintReceiptOriginal`
    };
  }
}
