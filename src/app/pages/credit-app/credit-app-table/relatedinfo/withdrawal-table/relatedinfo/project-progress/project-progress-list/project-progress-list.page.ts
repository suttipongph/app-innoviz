import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'project-progress-list-page',
  templateUrl: './project-progress-list.page.html',
  styleUrls: ['./project-progress-list.page.scss']
})
export class ProjectProgressListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'projectProgressGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.PROJECT_PROGRESS,
      servicePath: 'CreditAppTable/ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ProjectProgress'
    };
  }
}
