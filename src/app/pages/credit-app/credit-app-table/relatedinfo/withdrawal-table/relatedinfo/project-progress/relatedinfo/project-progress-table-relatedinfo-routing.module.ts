import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: 'memotrans',
    loadChildren: () =>
      import('./memo/project-progress-table-memo-trans-relatedinfo.module').then((m) => m.ProjectProgressTableTableMemoTransRelatedinfoModule)
  },
  {
    path: 'attachment',
    loadChildren: () => import('./attachment/attachment.module').then((m) => m.AttachmentModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectProgressTableRelatedinfoRoutingModule {}
