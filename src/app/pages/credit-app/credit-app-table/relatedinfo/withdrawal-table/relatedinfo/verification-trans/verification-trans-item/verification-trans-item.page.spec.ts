import { ComponentFixture, TestBed } from '@angular/core/testing';
import { VerificationTransItemPage } from './verification-trans-item.page';

describe('VendorPaymentItemPage', () => {
  let component: VerificationTransItemPage;
  let fixture: ComponentFixture<VerificationTransItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VerificationTransItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationTransItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
