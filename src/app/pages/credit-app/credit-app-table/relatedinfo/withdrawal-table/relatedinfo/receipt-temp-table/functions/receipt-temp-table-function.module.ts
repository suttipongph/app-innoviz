import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { ReceiptTempTableFunctionRoutingModule } from './receipt-temp-table-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, ReceiptTempTableFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReceiptTempTableFunctionModule {}
