import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PrintReceiptCopyPage } from './print-receipt-copy.page';

const routes: Routes = [
  {
    path: '',
    component: PrintReceiptCopyPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrintReceiptCopyRoutingModule {}
