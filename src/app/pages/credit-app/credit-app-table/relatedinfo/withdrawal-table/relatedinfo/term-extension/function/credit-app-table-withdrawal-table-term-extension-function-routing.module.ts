import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'extendterm',
    loadChildren: () =>
      import('./extend-term/credit-app-table-withdrawal-table-extend-term-function.module').then(
        (m) => m.CreditAppTableWithdrawalTableExtendTermFunctionModule
      )
  },
  {
    path: 'postwithdrawal',
    loadChildren: () =>
      import('./post-withdrawal/credit-app-table-withdrawal-table-term-extension-post-withdrawal-function.module').then(
        (m) => m.CreditAppTableWithdrawalTableTermExtensionPostWithdrawalFunctionModule
      )
  },
  {
    path: 'cancelwithdrawal',
    loadChildren: () =>
      import('./cancel-withdrawal/credit-app-table-withdrawal-table-cancel-withdrawal-function.module').then(
        (m) => m.CreditAppTableWithdrawalTableTermExtensionCancelWithdrawalFunctionModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableWithdrawalTableTermExtensionFunctionRoutingModule {}
