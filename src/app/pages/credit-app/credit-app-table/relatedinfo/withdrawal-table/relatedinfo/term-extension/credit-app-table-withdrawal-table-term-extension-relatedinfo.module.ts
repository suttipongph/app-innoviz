import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { WithdrawalTableComponentModule } from 'components/withdrawal/withdrawal-table/withdrawal-table.module';
import { WithdrawalLineComponentModule } from 'components/withdrawal/withdrawal-line/withdrawal-line.module';
import { WithdrawalTableListPage } from './withdrawal-table-list/withdrawal-table-list.page';
import { WithdrawalTableItemPage } from './withdrawal-table-item/withdrawal-table-item.page';
import { WithdrawalLineItemPage } from './withdrawal-line-item/withdrawal-line-item.page';
import { CreditAppTableWithdrawalTableTermExtensionRoutingModule } from './credit-app-table-withdrawal-table-term-extension-relatedinfo-routing.module';

@NgModule({
  declarations: [WithdrawalTableListPage, WithdrawalTableItemPage, WithdrawalLineItemPage],
  imports: [
    CommonModule,
    SharedModule,
    CreditAppTableWithdrawalTableTermExtensionRoutingModule,
    WithdrawalTableComponentModule,
    WithdrawalLineComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTableWithdrawalTableTermExtensionRelatedinfoModule {}
