import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: 'complete',
    loadChildren: () => import('./complete/complete-function.module').then((m) => m.CompleteFunctionModule)
  },
  {
    path: 'deposit',
    loadChildren: () => import('./deposit/deposit-function.module').then((m) => m.DepositFunctionModule)
  },
  {
    path: 'bounce',
    loadChildren: () => import('./bounce/bounce-function.module').then((m) => m.BounceFunctionModule)
  },
  {
    path: 'replace',
    loadChildren: () => import('./replace/replace-function.module').then((m) => m.ReplaceFunctionModule)
  },
  {
    path: 'returncheque',
    loadChildren: () => import('./return/return-function.module').then((m) => m.ReturnFunctionModule)
  },
  {
    path: 'cancelcheque',
    loadChildren: () => import('./cancel/cancel-function.module').then((m) => m.CancelFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChequeTableFunctionRoutingModule {}
