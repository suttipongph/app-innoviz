import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReceiptLineListPage } from './receipt-line-list/receipt-line-list.page';
import { ReceiptLineItemPage } from './receipt-line-item/receipt-line-item.page';

const routes: Routes = [
  {
    path: '',
    component: ReceiptLineListPage
  },
  {
    path: ':id',
    component: ReceiptLineItemPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceiptLineRoutingModule {}
