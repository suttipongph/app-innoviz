import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BounceFunctionRoutingModule } from './bounce-function-routing.module';
import { BouncePage } from './bounce/bounce.page';
import { SharedModule } from 'shared/shared.module';
import { BounceChequeComponentModule } from 'components/cheque-table/cheque-table/bounce-cheque/bounce-cheque.module';

@NgModule({
  declarations: [BouncePage],
  imports: [CommonModule, SharedModule, BounceFunctionRoutingModule, BounceChequeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BounceFunctionModule {}
