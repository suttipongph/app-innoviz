import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'service-fee-trans-item.page',
  templateUrl: './service-fee-trans-item.page.html',
  styleUrls: ['./service-fee-trans-item.page.scss']
})
export class ServiceFeeTransItemPage implements OnInit {
  pageInfo: PageInformationModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.SERVICE_FEE_TRANS,
      servicePath: `CreditAppTable/ProjectFinance/RelatedInfo/${this.activeRoute}/RelatedInfo/${ROUTE_RELATED_GEN.SERVICE_FEE_TRANS}`
    };
  }
}
