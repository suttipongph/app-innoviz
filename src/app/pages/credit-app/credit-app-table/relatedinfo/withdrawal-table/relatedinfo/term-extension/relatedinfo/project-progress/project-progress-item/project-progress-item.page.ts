import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'project-progress-item.page',
  templateUrl: './project-progress-item.page.html',
  styleUrls: ['./project-progress-item.page.scss']
})
export class ProjectProgressItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.PROJECT_PROGRESS,
      servicePath: `CreditAppTable/ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/${ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_TERM_EXTENSION}/RelatedInfo/ProjectProgress`
    };
  }
}
