import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppTableWithdrawalTablePaymentDetailRelatedinfoRoutingModule } from './credit-app-table-withdrawal-table-payment-detail-relatedinfo-routing.module';
import { PaymentDetailListPage } from './payment-detail-list/payment-detail-list.page';
import { PaymentDetailItemPage } from './payment-detail-item/payment-detail-item.page';
import { SharedModule } from 'shared/shared.module';
import { PaymentDetailComponentModule } from 'components/payment-detail/payment-detail.module';

@NgModule({
  declarations: [PaymentDetailListPage, PaymentDetailItemPage],
  imports: [CommonModule, SharedModule, CreditAppTableWithdrawalTablePaymentDetailRelatedinfoRoutingModule, PaymentDetailComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTableWithdrawalTablePaymentDetailRelatedinfoModule {}
