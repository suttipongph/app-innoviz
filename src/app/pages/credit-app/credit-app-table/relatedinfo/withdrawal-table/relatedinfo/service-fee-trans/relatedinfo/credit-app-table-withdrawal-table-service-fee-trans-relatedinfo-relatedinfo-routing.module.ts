import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'invoicetable',
    loadChildren: () =>
      import('./invoicetable/credit-app-table-withdrawal-table-service-fee-trans-invoicetable-relatedinfo.module').then(
        (m) => m.CreditAppTableWithdrawalTableServiceFeeTransInvoiceTableRelatedinfoModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableWithdrawalTableServiceFeeTransRelatedinfoRelatedinfoRoutingModule {}
