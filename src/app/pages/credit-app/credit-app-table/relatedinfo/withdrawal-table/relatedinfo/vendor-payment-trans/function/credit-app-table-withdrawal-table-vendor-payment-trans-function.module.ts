import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { CreditAppTableWithdrawalTableVendorPaymentTransFunctionRoutingModule } from './credit-app-table-withdrawal-table-vendor-payment-trans-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, CreditAppTableWithdrawalTableVendorPaymentTransFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTableWithdrawalTableVendorPaymentTransFunctionModule {}
