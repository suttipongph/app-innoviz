import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CancelWithdrawalPage } from './cancel-withdrawal/cancel-withdrawal.page';

const routes: Routes = [
  {
    path: '',
    component: CancelWithdrawalPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableWithdrawalTableTermExtensionCancelWithdrawalFunctionRoutingModule {}
