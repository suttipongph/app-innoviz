import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SuspenseSettlementListPage } from './suspense-settlement-list.page';

describe('SuspenseSettlementListPage', () => {
  let component: SuspenseSettlementListPage;
  let fixture: ComponentFixture<SuspenseSettlementListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SuspenseSettlementListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SuspenseSettlementListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
