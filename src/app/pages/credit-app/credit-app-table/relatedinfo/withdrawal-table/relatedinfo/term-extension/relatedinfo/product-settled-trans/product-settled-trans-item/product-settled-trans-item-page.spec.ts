import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductSettledTransItemPage } from './product-settled-trans-item-page';

describe('ProductSettledTransItemPageComponent', () => {
  let component: ProductSettledTransItemPage;
  let fixture: ComponentFixture<ProductSettledTransItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductSettledTransItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSettledTransItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
