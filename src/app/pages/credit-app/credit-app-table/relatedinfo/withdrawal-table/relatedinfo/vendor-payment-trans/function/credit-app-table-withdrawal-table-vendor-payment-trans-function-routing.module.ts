import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'sendvendorinvoicestaging',
    loadChildren: () =>
      import('./send-vendor-invoice/send-vendor-invoice-staging-function.module').then(
        (m) => m.CreditAppTableWithdrawalTableVendorPaymentTransSendVendorInvoiceStagingFunctionModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableWithdrawalTableVendorPaymentTransFunctionRoutingModule {}
