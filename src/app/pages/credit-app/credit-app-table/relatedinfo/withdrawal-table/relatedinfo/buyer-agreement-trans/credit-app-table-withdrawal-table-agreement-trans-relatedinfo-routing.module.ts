import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { BuyerAgreementTransItemPage } from './buyer-agreement-trans-item/buyer-agreement-trans-item.page';
import { BuyerAgreementTransListPage } from './buyer-agreement-trans-list/buyer-agreement-trans-list.page';

const routes: Routes = [
  {
    path: '',
    component: BuyerAgreementTransListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BuyerAgreementTransItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableWithdrawalTableBuyerAgreementTransRelatedinfoRoutingModule {}
