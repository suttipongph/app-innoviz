import { Component } from '@angular/core';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel, PathParamModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-complete',
  templateUrl: './complete.page.html',
  styleUrls: ['./complete.page.scss']
})
export class CompletePage extends BaseItemComponent<any> {
  pageInfo: PageInformationModel;

  constructor() {
    super();
    this.path = ROUTE_FUNCTION_GEN.COMPLETE;
  }
  onRelatedMenu(): void {
    const path: PathParamModel = {};
    this.toRelatedInfo(path);
  }
  toRelatedInfo(param: PathParamModel): void {
    super.toRelatedInfo(param);
  }
  ngOnInit(): void {
    this.setPath();
  }

  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.COMPLETE,
      servicePath: `ChequeTable/Function/Complete`
    };
  }
}
