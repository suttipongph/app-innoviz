import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectProgressRelatedinfoRoutingModule } from './project-progress-relatedinfo-routing.module';
import { ProjectProgressListPage } from './project-progress-list/project-progress-list.page';
import { ProjectProgressItemPage } from './project-progress-item/project-progress-item.page';
import { SharedModule } from 'shared/shared.module';
import { ProjectProgressTableComponentModule } from 'components/project-progress-table/project-progress-table.module';

@NgModule({
  declarations: [ProjectProgressListPage, ProjectProgressItemPage],
  imports: [CommonModule, SharedModule, ProjectProgressRelatedinfoRoutingModule, ProjectProgressTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjectProgressRelatedinfoModule {}
