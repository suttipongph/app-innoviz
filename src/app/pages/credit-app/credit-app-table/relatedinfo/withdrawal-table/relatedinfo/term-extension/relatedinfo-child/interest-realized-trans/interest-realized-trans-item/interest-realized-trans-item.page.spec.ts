import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InterestRealizedTransItemPage } from './interest-realized-trans-item.page';

describe('InterestRealizedTransItemPage', () => {
  let component: InterestRealizedTransItemPage;
  let fixture: ComponentFixture<InterestRealizedTransItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InterestRealizedTransItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestRealizedTransItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
