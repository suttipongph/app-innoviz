import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BuyerAgreementTransListPage } from './buyer-agreement-trans-list.page';

describe('BuyerAgreementTransListPage', () => {
  let component: BuyerAgreementTransListPage;
  let fixture: ComponentFixture<BuyerAgreementTransListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BuyerAgreementTransListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerAgreementTransListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
