import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppTableWithdrawalTableTermExtensionVerificationTransRelatedinfoRoutingModule } from './credit-app-table-withdrawal-table-term-extension-verification-trans-relatedinfo-routing.module';
import { VerificationTransListPage } from './verification-trans-list/verification-trans-list.page';
import { VerificationTransItemPage } from './verification-trans-item/verification-trans-item.page';
import { SharedModule } from 'shared/shared.module';
import { VerificationTransComponentModule } from 'components/verification-trans/verification-trans/verification-trans.module';

@NgModule({
  declarations: [VerificationTransListPage, VerificationTransItemPage],
  imports: [
    CommonModule,
    SharedModule,
    CreditAppTableWithdrawalTableTermExtensionVerificationTransRelatedinfoRoutingModule,
    VerificationTransComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTableWithdrawalTableTermExtensionVerificationTransRelatedinfoModule {}
