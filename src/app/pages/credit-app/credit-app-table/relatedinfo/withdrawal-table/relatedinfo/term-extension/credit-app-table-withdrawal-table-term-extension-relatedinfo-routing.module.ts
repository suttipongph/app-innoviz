import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { WithdrawalLineItemPage } from './withdrawal-line-item/withdrawal-line-item.page';
import { WithdrawalTableItemPage } from './withdrawal-table-item/withdrawal-table-item.page';
import { WithdrawalTableListPage } from './withdrawal-table-list/withdrawal-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: WithdrawalTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: WithdrawalTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/withdrawallinetermextension-child/:id',
    component: WithdrawalLineItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () =>
      import('./relatedinfo/credit-app-table-withdrawal-table-term-extension-relatedinfo-relatedinfo.module').then(
        (m) => m.CreditAppTableWithdrawalTableTermExtensionRelatedinfoRelatedInfoModule
      )
  },
  {
    path: ':id/withdrawallinetermextension-child/:id/relatedinfo',
    loadChildren: () =>
      import('./relatedinfo-child/credit-app-table-withdrawal-line-term-extension-relatedinfo.module').then(
        (m) => m.CreditAppTableWithdrawalLineTermExtensionRelatedinfoModule
      )
  },
  {
    path: ':id/function',
    loadChildren: () =>
      import('./function/credit-app-table-withdrawal-table-term-extension-function.module').then(
        (m) => m.CreditAppTableWithdrawalTableTermExtensionFunctionModule
      )
  },
  {
    path: ':id/report',
    loadChildren: () => import('./report/term-extension-report.module').then((m) => m.TermExtensionReportModule)
  },
  {
    path: 'function',
    loadChildren: () =>
      import('./function/credit-app-table-withdrawal-table-term-extension-function.module').then(
        (m) => m.CreditAppTableWithdrawalTableTermExtensionFunctionModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableWithdrawalTableTermExtensionRoutingModule {}
