import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ExtendTermPage } from './extend-term/extend-term.page';

const routes: Routes = [
  {
    path: '',
    component: ExtendTermPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableWithdrawalTableExtendTermFunctionRoutingModule {}
