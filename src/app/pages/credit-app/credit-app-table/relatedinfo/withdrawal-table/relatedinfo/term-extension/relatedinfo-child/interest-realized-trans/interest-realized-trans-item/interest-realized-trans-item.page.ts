import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'interest-realized-trans-item.page',
  templateUrl: './interest-realized-trans-item.page.html',
  styleUrls: ['./interest-realized-trans-item.page.scss']
})
export class InterestRealizedTransItemPage implements OnInit {
  pageInfo: PageInformationModel;
  parentRoute = this.uiService.getRelatedInfoParentTableName();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.INTEREST_REALIZED_TRANSACTION,
      servicePath: `CreditAppTable/ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalLineTermExtension-Child/RelatedInfo/${ROUTE_RELATED_GEN.INTEREST_REALIZED_TRANSACTION}`
    };
  }
}
