import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'verificationtrans',
    loadChildren: () =>
      import('./verification-trans/credit-app-table-withdrawal-table-term-extension-verification-trans-relatedinfo.module').then(
        (m) => m.CreditAppTableWithdrawalTableTermExtensionVerificationTransRelatedinfoModule
      )
  },
  {
    path: 'servicefeetrans',
    loadChildren: () =>
      import('./service-fee-trans/credit-app-table-withdrawal-table-term-extension-service-fee-trans-relatedinfo.module').then(
        (m) => m.CreditAppTableWithdrawalTableTermExtensionServiceFeeTransRelatedinfoModule
      )
  },
  {
    path: 'buyeragreementtrans',
    loadChildren: () =>
      import('./buyer-agreement-trans/credit-app-table-withdrawal-table-term-extension-agreement-trans-relatedinfo.module').then(
        (m) => m.CreditAppTableWithdrawalTableTermExtensionBuyerAgreementTransRelatedinfoModule
      )
  },
  {
    path: 'projectprogress',
    loadChildren: () => import('./project-progress/project-progress-relatedinfo.module').then((m) => m.ProjectProgressRelatedinfoModule)
  },
  {
    path: 'pdc',
    loadChildren: () => import('./pdc/pdc-relatedinfo.module').then((m) => m.WithdrawalTablePdcRelatedinfoModule)
  },
  {
    path: 'attachment',
    loadChildren: () => import('./attachment/withdrawal-table-attachment.module').then((m) => m.WithdrawalTableAttachmentModule)
  },
  {
    path: 'memotrans',
    loadChildren: () => import('./memo/withdrawal-table-memo-trans-relatedinfo.module').then((m) => m.WithdrawalTableTableMemoTransRelatedinfoModule)
  },
  {
    path: 'productsettlement',
    loadChildren: () =>
      import('./product-settled-trans/withdrawal-table-product-settled-trans.module').then((m) => m.WithdrawalTableProductSettledTransModule)
  },
  {
    path: 'suspensesettlement',
    loadChildren: () =>
      import('./suspense-settlement/term-extension-suspense-settlement-relatedinfo.module').then(
        (m) => m.TermExtensionSuspenseSettlementRelatedinfoModule
      )
  },
  {
    path: 'receipttemptable',
    loadChildren: () => import('./receipt-temp-table/receipt-temp-table.module').then((m) => m.ReceiptTempTableModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableWithdrawalTableTermExtensionRelatedinfoRelatedinfoRoutingModule {}
