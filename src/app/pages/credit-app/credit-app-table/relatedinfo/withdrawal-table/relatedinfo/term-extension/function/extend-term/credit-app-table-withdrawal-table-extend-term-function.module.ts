import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { CreditAppTableWithdrawalTableExtendTermFunctionRoutingModule } from './credit-app-table-withdrawal-table-extend-term-function-routing.module';
import { ExtendTermPage } from './extend-term/extend-term.page';
import { ExtendTermComponentModule } from 'components/withdrawal/extend-term/extend-term.module';

@NgModule({
  declarations: [ExtendTermPage],
  imports: [
    CommonModule,
    SharedModule,
    CreditAppTableWithdrawalTableExtendTermFunctionRoutingModule,
    ExtendTermComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTableWithdrawalTableExtendTermFunctionModule {}
