import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppTableWithdrawalTableServiceFeeTransRelatedinfoRelatedinfoRoutingModule } from './credit-app-table-withdrawal-table-service-fee-trans-relatedinfo-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, CreditAppTableWithdrawalTableServiceFeeTransRelatedinfoRelatedinfoRoutingModule]
})
export class CreditAppTableWithdrawalTableServiceFeeTransRelatedinfoRelatedinfoModule {}
