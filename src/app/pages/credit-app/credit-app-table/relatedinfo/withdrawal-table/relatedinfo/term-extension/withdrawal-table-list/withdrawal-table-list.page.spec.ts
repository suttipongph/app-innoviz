import { ComponentFixture, TestBed } from '@angular/core/testing';
import { WithdrawalTableListPage } from './withdrawal-table-list.page';

describe('WithdrawalListPage', () => {
  let component: WithdrawalTableListPage;
  let fixture: ComponentFixture<WithdrawalTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WithdrawalTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WithdrawalTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
