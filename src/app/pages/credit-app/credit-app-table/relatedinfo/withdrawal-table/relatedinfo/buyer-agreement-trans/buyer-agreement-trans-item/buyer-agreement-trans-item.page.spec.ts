import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BuyerAgreementTransItemPage } from './buyer-agreement-trans-item.page';

describe('BuyerAgreementTransItemPage', () => {
  let component: BuyerAgreementTransItemPage;
  let fixture: ComponentFixture<BuyerAgreementTransItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BuyerAgreementTransItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerAgreementTransItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
