import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'buyer-agreement-trans-item.page',
  templateUrl: './buyer-agreement-trans-item.page.html',
  styleUrls: ['./buyer-agreement-trans-item.page.scss']
})
export class BuyerAgreementTransItemPage implements OnInit {
  pageInfo: PageInformationModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.BUYER_AGREEMENT_TRANS,
      servicePath: `CreditAppTable/ProjectFinance/RelatedInfo/${ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_WITHDRAWAL}/RelatedInfo/${ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_TERM_EXTENSION}/RelatedInfo/${ROUTE_RELATED_GEN.BUYER_AGREEMENT_TRANS}`
    };
  }
}
