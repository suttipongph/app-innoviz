import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'paymentdetail',
    loadChildren: () =>
      import('./payment-detail/credit-app-table-withdrawal-table-payment-detail-relatedinfo.module').then(
        (m) => m.CreditAppTableWithdrawalTablePaymentDetailRelatedinfoModule
      )
  },
  {
    path: 'verificationtrans',
    loadChildren: () =>
      import('./verification-trans/credit-app-table-withdrawal-table-verification-trans-relatedinfo.module').then(
        (m) => m.CreditAppTableWithdrawalTableVerificationTransRelatedinfoModule
      )
  },
  {
    path: 'servicefeetrans',
    loadChildren: () =>
      import('./service-fee-trans/credit-app-table-withdrawal-table-service-fee-trans-relatedinfo.module').then(
        (m) => m.CreditAppTableWithdrawalTableServiceFeeTransRelatedinfoModule
      )
  },
  {
    path: 'memotrans',
    loadChildren: () => import('./memo/withdrawal-table-memo-trans-relatedinfo.module').then((m) => m.WithdrawalTableTableMemoTransRelatedinfoModule)
  },
  {
    path: 'withdrawaltabletermextension',
    loadChildren: () =>
      import('./term-extension/credit-app-table-withdrawal-table-term-extension-relatedinfo.module').then(
        (m) => m.CreditAppTableWithdrawalTableTermExtensionRelatedinfoModule
      )
  },
  {
    path: 'vendorpaymenttrans',
    loadChildren: () =>
      import('./vendor-payment-trans/credit-app-table-withdrawal-table-vendor-payment-trans-relatedinfo.module').then(
        (m) => m.CreditAppTableWithdrawalTableVendorPaymentTransRelatedinfoModule
      )
  },
  {
    path: 'projectprogress',
    loadChildren: () => import('./project-progress/project-progress-relatedinfo.module').then((m) => m.ProjectProgressRelatedinfoModule)
  },
  {
    path: 'pdc',
    loadChildren: () => import('./pdc/pdc-relatedinfo.module').then((m) => m.WithdrawalTablePdcRelatedinfoModule)
  },
  {
    path: 'invoicesettlement',
    loadChildren: () => import('./invoice-settlement/invoice-settlement-relatedinfo.module').then((m) => m.InvoiceSettlementRelatedinfoModule)
  },
  {
    path: 'suspensesettlement',
    loadChildren: () => import('./suspense-settlement/suspense-settlement-relatedinfo.module').then((m) => m.SuspenseSettlementRelatedinfoModule)
  },
  {
    path: 'attachment',
    loadChildren: () => import('./attachment/withdrawal-table-attachment.module').then((m) => m.WithdrawalTableAttachmentModule)
  },
  {
    path: 'productsettlement',
    loadChildren: () =>
      import('./product-settled-trans/withdrawal-table-product-settled-trans.module').then((m) => m.WithdrawalTableProductSettledTransModule)
  },
  {
    path: 'buyeragreementtrans',
    loadChildren: () =>
      import('./buyer-agreement-trans/credit-app-table-withdrawal-table-agreement-trans-relatedinfo.module').then(
        (m) => m.CreditAppTableWithdrawalTableBuyerAgreementTransRelatedinfoModule
      )
  },
  {
    path: 'receipttemptable',
    loadChildren: () => import('./receipt-temp-table/receipt-temp-table.module').then((m) => m.ReceiptTempTableModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableWithdrawalTableRelatedinfoRoutingModule {}
