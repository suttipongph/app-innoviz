import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChequeTableFunctionRoutingModule } from './cheque-table-function-routing.module';

import { SharedModule } from 'shared/shared.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, ChequeTableFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ChequeTableFunctionModule {}
