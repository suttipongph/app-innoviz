import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { ProjectProgressTableRelatedinfoRoutingModule } from './project-progress-table-reletedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, ProjectProgressTableRelatedinfoRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjectProgressTableRelatedinfoModule {}
