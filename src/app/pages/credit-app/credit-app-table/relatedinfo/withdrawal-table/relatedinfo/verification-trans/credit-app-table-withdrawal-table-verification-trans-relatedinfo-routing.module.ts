import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VerificationTransListPage } from './verification-trans-list/verification-trans-list.page';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { VerificationTransItemPage } from './verification-trans-item/verification-trans-item.page';

const routes: Routes = [
  {
    path: '',
    component: VerificationTransListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: VerificationTransItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableWithdrawalTableVerificationTransRelatedinfoRoutingModule {}
