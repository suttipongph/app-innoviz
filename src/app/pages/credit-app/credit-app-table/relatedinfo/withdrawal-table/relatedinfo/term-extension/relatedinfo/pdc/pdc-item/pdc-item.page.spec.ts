import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PdcItemPage } from './pdc-item.page';

describe('PdcItemPage', () => {
  let component: PdcItemPage;
  let fixture: ComponentFixture<PdcItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PdcItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PdcItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
