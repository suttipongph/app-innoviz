import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostReceiptTempFunctionRoutingModule } from './post-receipt-temp-function-routing.module';
import { SharedModule } from 'shared/shared.module';
import { PostReceiptTempPage } from './post/post-receipt-temp.page';
import { PostReceiptTempComponentModule } from 'components/receipt-temp-table/post-receipt-temp/post-receipt-temp.module';

@NgModule({
  declarations: [PostReceiptTempPage],
  imports: [CommonModule, SharedModule, PostReceiptTempFunctionRoutingModule, PostReceiptTempComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PostReceiptTempFunctionModule {}
