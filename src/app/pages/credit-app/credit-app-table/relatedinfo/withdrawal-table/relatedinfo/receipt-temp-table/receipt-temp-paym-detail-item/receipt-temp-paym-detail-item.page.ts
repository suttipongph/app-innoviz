import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'receipt-temp-paym-detail-item-page',
  templateUrl: './receipt-temp-paym-detail-item.page.html',
  styleUrls: ['./receipt-temp-paym-detail-item.page.scss']
})
export class ReceiptTempPaymDetailItemPage implements OnInit {
  pageInfo: PageInformationModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.RECEIPT_TEMP_PAYM_DETAIL, servicePath: `CreditAppTable/ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable`
    };
  }
}
