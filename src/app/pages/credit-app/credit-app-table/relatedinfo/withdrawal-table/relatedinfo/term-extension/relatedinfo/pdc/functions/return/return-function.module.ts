import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReturnFunctionRoutingModule } from './return-function-routing.module';
import { ReturnPage } from './return/return.page';
import { SharedModule } from 'shared/shared.module';
import { ReturnChequeComponentModule } from 'components/cheque-table/cheque-table/return-cheque/return-cheque.module';

@NgModule({
  declarations: [ReturnPage],
  imports: [CommonModule, SharedModule, ReturnFunctionRoutingModule, ReturnChequeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReturnFunctionModule {}
