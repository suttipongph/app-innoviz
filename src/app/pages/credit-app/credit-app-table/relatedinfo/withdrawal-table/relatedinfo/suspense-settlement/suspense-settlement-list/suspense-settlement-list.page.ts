import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, Operators, ROUTE_RELATED_GEN, SortType, SuspenseInvoiceType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'suspense-settlement-list-page',
  templateUrl: './suspense-settlement-list.page.html',
  styleUrls: ['./suspense-settlement-list.page.scss']
})
export class SuspenseSettlementListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'invoiceSettlementDetailGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'suspenseInvoiceType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: SuspenseInvoiceType.None.toString(),
        equalityOperator: Operators.NOT_EQUAL
      }
    ];
    this.option.columns = columns;
  }

  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.SUSPENSE_SETTLEMENT,
      servicePath: `CreditAppTable/ProjectFinance/RelatedInfo/${this.activeRoute}/RelatedInfo/SuspenseSettlement`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.SUSPENSE_SETTLEMENT,
          servicePath: `CreditAppTable/ProjectFinance/RelatedInfo/${this.activeRoute}/RelatedInfo/SuspenseSettlement`
        }
      ]
    };
  }
}
