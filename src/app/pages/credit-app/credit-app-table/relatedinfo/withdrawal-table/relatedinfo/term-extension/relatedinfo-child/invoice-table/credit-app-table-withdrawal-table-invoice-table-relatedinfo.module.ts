import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppTableWithdrawalTableInvoiceTableReledinfoRoutingModule } from './credit-app-table-withdrawal-table-invoice-table-relatedinfo-routing.module';
import { InvoiceTableItemPage } from './invoice-table-item/invoice-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { InvoiceTableComponentModule } from 'components/invoice-table/invoice-table/invoice-table.module';
import { InvoiceTableListPage } from './invoice-table-list/invoice-table.page';
import { InvoiceLineItemPage } from './invoice-line-item/invoice-line-item.page';
import { InvoiceLineComponentModule } from 'components/invoice-table/invoice-line/invoice-line.module';

@NgModule({
  declarations: [InvoiceTableListPage, InvoiceTableItemPage, InvoiceLineItemPage],
  imports: [CommonModule, SharedModule, CreditAppTableWithdrawalTableInvoiceTableReledinfoRoutingModule, InvoiceTableComponentModule, InvoiceLineComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTableWithdrawalTableInvoiceTableReledinfoModule {}
