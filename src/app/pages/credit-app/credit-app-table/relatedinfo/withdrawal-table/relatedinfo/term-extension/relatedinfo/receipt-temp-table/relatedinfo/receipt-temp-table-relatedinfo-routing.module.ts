import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'invoicesettlement',
    loadChildren: () =>
      import('./invoice-settlement/receipt-temp-table-invoice-settlement-relatedinfo.module').then(
        (m) => m.ReceiptTempTableInvoiceSettlementRelatedinfoModule
      )
  },
  {
    path: 'receipttable',
    loadChildren: () =>
      import('./receipt-table/receipt-temp-table-receipt-table-relatedinfo.module').then((m) => m.ReceiptTempTableReceiptTableRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceiptTempTableRelatedinfoRoutingModule {}
