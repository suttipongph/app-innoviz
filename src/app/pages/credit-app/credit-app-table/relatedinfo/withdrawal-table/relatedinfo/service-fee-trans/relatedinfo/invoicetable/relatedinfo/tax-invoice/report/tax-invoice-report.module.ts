import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { TaxInvoiceCopyReportRoutingModule } from './tax-invoice-report-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, TaxInvoiceCopyReportRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TaxInvoiceCopyReportModule {}
