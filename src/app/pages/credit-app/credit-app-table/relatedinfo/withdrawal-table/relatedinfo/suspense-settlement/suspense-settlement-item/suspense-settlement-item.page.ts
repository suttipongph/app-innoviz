import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'suspense-settlement-item.page',
  templateUrl: './suspense-settlement-item.page.html',
  styleUrls: ['./suspense-settlement-item.page.scss']
})
export class SuspenseSettlementItemPage implements OnInit {
  pageInfo: PageInformationModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.SUSPENSE_SETTLEMENT,
      servicePath: `CreditAppTable/ProjectFinance/RelatedInfo/${this.activeRoute}/RelatedInfo/SuspenseSettlement`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.SUSPENSE_SETTLEMENT,
          servicePath: `CreditAppTable/ProjectFinance/RelatedInfo/${this.activeRoute}/RelatedInfo/SuspenseSettlement`
        }
      ]
    };
  }
}
