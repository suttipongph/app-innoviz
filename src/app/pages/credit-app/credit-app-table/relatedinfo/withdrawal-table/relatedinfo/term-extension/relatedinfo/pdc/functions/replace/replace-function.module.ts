import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReplaceFunctionRoutingModule } from './replace-function-routing.module';
import { ReplacePage } from './replace/replace.page';
import { SharedModule } from 'shared/shared.module';
import { ReplaceChequeComponentModule } from 'components/cheque-table/cheque-table/replace-cheque/replace-cheque.module';

@NgModule({
  declarations: [ReplacePage],
  imports: [CommonModule, SharedModule, ReplaceFunctionRoutingModule, ReplaceChequeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReplaceFunctionModule {}
