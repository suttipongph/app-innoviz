import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectProgressListPage } from './project-progress-list/project-progress-list.page';
import { ProjectProgressItemPage } from './project-progress-item/project-progress-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: ProjectProgressListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ProjectProgressItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/project-progress-table-relatedinfo.module').then((m) => m.ProjectProgressTableRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectProgressRelatedinfoRoutingModule {}
