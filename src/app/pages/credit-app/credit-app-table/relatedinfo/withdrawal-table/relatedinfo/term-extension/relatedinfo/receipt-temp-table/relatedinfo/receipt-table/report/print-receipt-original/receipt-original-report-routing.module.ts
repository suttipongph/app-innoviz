import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PrintReceiptOriginalPage } from './print-receipt-original.page';

const routes: Routes = [
  {
    path: '',
    component: PrintReceiptOriginalPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrintReceiptOriginalRoutingModule {}
