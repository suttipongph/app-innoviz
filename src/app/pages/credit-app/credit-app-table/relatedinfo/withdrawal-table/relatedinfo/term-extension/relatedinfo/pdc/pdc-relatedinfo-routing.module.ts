import { AuthGuardService } from 'core/services/auth-guard.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PdcListPage } from './pdc-list/pdc-list.page';
import { PdcItemPage } from './pdc-item/pdc-item.page';

const routes: Routes = [
  {
    path: '',
    component: PdcListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: PdcItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/cheque-table-function.module').then((m) => m.ChequeTableFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WithdrawalTablePdcRelatedinfoRoutingModule {}
