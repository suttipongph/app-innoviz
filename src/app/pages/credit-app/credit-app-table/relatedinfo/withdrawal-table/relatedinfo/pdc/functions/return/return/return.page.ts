import { Component } from '@angular/core';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel, PathParamModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-return',
  templateUrl: './return.page.html',
  styleUrls: ['./return.page.scss']
})
export class ReturnPage extends BaseItemComponent<any> {
  pageInfo: PageInformationModel;
  constructor() {
    super();
    this.path = ROUTE_FUNCTION_GEN.CHEQUE_TABLE_RETURN;
  }
  onRelatedMenu(): void {
    const path: PathParamModel = {};
    this.toRelatedInfo(path);
  }
  toRelatedInfo(param: PathParamModel): void {
    super.toRelatedInfo(param);
  }
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.CHEQUE_TABLE_RETURN,
      servicePath: `ChequeTable/Function/ReturnCheque`
    };
  }
}
