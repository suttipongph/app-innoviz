import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PostReceiptTempPage } from './post-receipt-temp.page';

describe('PostReceiptTempPage', () => {
  let component: PostReceiptTempPage;
  let fixture: ComponentFixture<PostReceiptTempPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PostReceiptTempPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostReceiptTempPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
