import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'authorizedpersontrans',
    loadChildren: () =>
      import('./authorized-person-trans/credit-app-table-authorized-person-trans.module').then((m) => m.CreditAppTableAuthorizedPersonTransModule)
  },
  {
    path: 'ownertrans',
    loadChildren: () => import('./owner-trans/credit-app-table-owner-trans.module').then((m) => m.CreditAppTableOwnerTransModule)
  },
  {
    path: 'guarantortrans',
    loadChildren: () => import('./guarantor-trans/credit-app-table-guarantor-trans.module').then((m) => m.CreditAppTableGuarantorTransModule)
  },
  {
    path: 'processtransaction',
    loadChildren: () =>
      import('./process-transaction/process-transaction-relatedinfo.module').then((m) => m.CreditAppTableProcessTransactionRelatedinfoModule)
  },
  {
    path: 'retentionconditiontrans',
    loadChildren: () =>
      import('./retention-condition/credit-app-table-retention-condition-relatedinfo.module').then(
        (m) => m.CreditAppTableRetentionConditionRelatedinfoModule
      )
  },
  {
    path: 'servicefeeconditiontrans',
    loadChildren: () =>
      import('./service-fee-condition/credit-app-table-service-fee-condition-relatedinfo.module').then(
        (m) => m.CreditAppTableServiceFeeConditionRelatedinfoModule
      )
  },
  {
    path: 'amendca',
    loadChildren: () => import('./amend-ca/amend-ca-relatedinfo.module').then((m) => m.AmendCaRelatedinfoModule)
  },
  {
    path: 'purchasetablepurchase',
    loadChildren: () => import('./purchase-table/credit-app-table-purchase-table.module').then((m) => m.CreditAppTablePurchaseTableModule)
  },
  {
    path: 'purchasetablerollbill',
    loadChildren: () => import('./purchase-table/credit-app-table-purchase-table.module').then((m) => m.CreditAppTablePurchaseTableModule)
  },
  {
    path: 'creditapplicationtransaction',
    loadChildren: () =>
      import('./credit-application-transaction/credit-application-transaction-relatedinfo.module').then(
        (m) => m.CreditApplicationTransactionRelatedinfoModule
      )
  },
  {
    path: 'verificationtable',
    loadChildren: () =>
      import('./verification/credit-app-table-verification-relatedinfo.module').then((m) => m.CreditAppTableVerificationRelatedinfoModule)
  },
  {
    path: 'loanrequest',
    loadChildren: () =>
      import('pages/credit-app-request/credit-app-request-table/credit-app-request-table.module').then((m) => m.CreditAppRequestTableModule)
  },
  {
    path: 'buyermatching',
    loadChildren: () =>
      import('pages/credit-app-request/credit-app-request-table/credit-app-request-table.module').then((m) => m.CreditAppRequestTableModule)
  },
  {
    path: 'withdrawaltablewithdrawal',
    loadChildren: () => import('./withdrawal-table/credit-app-table-withdrawal-table.module').then((m) => m.CreditAppTableWithdrawalTableModule)
  },
  {
    path: 'closecreditlimit',
    loadChildren: () => import('./close-credit-limit/close-credit-limit-relatedinfo.module').then((m) => m.CloseCreditLimitRelatedinfoModule)
  },
  {
    path: 'attachment',
    loadChildren: () => import('./attachment/credit-app-table-attachment.module').then((m) => m.CreditAppTableAttachmentModule)
  },
  {
    path: 'customerbuyercreditoutstanding',
    loadChildren: () =>
      import('./Inquiry-customer-buyer-credit-outstanding/credit-app-table-inquiry-customer-buyer-credit-outstanding.module').then(
        (m) => m.CreditAppTableInquiryCustomerBuyerCreditOutstandingModule
      )
  },
  {
    path: 'retentiontransaction',
    loadChildren: () =>
      import('./retention-transaction/credit-app-table-retention-transaction-relatedinfo.module').then(
        (m) => m.CreditAppTableRetentionTransactionRelatedinfoModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableRelatedinfoRoutingModule {}
