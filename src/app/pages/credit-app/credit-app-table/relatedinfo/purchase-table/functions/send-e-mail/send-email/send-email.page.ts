import { Component } from '@angular/core';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel, PathParamModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-send-email-page',
  templateUrl: './send-email.page.html',
  styleUrls: ['./send-email.page.scss']
})
export class SendEmailPage extends BaseItemComponent<any> {
  pageInfo: PageInformationModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();

  constructor() {
    super();
    this.path = ROUTE_FUNCTION_GEN.PURCHASE_TABLE_SEND_EMAIL;
  }
  onRelatedMenu(): void {
    const path: PathParamModel = {};
    this.toRelatedInfo(path);
  }
  toRelatedInfo(param: PathParamModel): void {
    super.toRelatedInfo(param);
  }
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.PURCHASE_TABLE_SEND_EMAIL,
      servicePath: `CreditAppTable/Factoring/RelatedInfo/${this.activeRoute}/Function/PurchaseTableSendEmail`
    };
  }
}
