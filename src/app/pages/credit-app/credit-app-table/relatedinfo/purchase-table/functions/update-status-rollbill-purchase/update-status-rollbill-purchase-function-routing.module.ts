import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { UpdateStatusRollbillPurchasePage } from './update-status-rollbill-purchase/update-status-rollbill-purchase.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateStatusRollbillPurchasePage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpdateStatusRollbillPurchaseFunctionRoutingModule {}
