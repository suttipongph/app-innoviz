import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelPurchasePage } from './cancel-purchase.page';

describe('CancelPurchasePage', () => {
  let component: CancelPurchasePage;
  let fixture: ComponentFixture<CancelPurchasePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CancelPurchasePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelPurchasePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
