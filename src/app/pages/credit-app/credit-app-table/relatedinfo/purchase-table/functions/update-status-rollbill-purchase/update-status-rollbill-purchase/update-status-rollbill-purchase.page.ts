import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'update-status-rollbill-purchase-page',
  templateUrl: './update-status-rollbill-purchase.page.html',
  styleUrls: ['./update-status-rollbill-purchase.page.scss']
})
export class UpdateStatusRollbillPurchasePage implements OnInit {
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  pageInfo: PageInformationModel;
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.CANCEL_PURCHASE,
      servicePath: `CreditAppTable/Factoring/RelatedInfo/${this.activeRoute}/Function/UpdateStatusRollbillPurchase`
    };
  }
}
