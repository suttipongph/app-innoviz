import { Component } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-post',
  templateUrl: './post-purchase.page.html',
  styleUrls: ['./post-purchase.page.scss']
})
export class PostPurchasePage {
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  pageInfo: PageInformationModel;
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.POST_PURCHASE,
      servicePath: `CreditAppTable/Factoring/RelatedInfo/${this.activeRoute}/Function/PostPurchase`
    };
  }
}
