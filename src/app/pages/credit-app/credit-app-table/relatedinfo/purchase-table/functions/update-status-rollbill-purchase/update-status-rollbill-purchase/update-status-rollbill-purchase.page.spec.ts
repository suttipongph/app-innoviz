import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateStatusRollbillPurchasePage } from './update-status-rollbill-purchase.page';

describe('UpdateStatusRollbillPurchasePage', () => {
  let component: UpdateStatusRollbillPurchasePage;
  let fixture: ComponentFixture<UpdateStatusRollbillPurchasePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UpdateStatusRollbillPurchasePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateStatusRollbillPurchasePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
