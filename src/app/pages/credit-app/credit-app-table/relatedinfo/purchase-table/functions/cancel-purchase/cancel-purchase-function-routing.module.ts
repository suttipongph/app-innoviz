import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CancelPurchasePage } from './cancel-purchase/cancel-purchase.page';

const routes: Routes = [
  {
    path: '',
    component: CancelPurchasePage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CancelPurchaseFunctionRoutingModule {}
