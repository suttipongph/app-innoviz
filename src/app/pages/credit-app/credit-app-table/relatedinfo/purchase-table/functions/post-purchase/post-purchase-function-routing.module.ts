import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PostPurchasePage } from './post-purchase/post-purchase.page';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuardService],
    component: PostPurchasePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostPurchaseFunctionRoutingModule {}
