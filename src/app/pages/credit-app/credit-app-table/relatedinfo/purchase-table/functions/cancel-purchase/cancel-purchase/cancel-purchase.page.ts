import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'cancel-purchase-page',
  templateUrl: './cancel-purchase.page.html',
  styleUrls: ['./cancel-purchase.page.scss']
})
export class CancelPurchasePage implements OnInit {
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  pageInfo: PageInformationModel;
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.CANCEL_PURCHASE,
      servicePath: `CreditAppTable/Factoring/RelatedInfo/${this.activeRoute}/Function/CancelPurchase`
    };
  }
}
