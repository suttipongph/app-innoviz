import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SendEmailFunctionRoutingModule } from './send-email-function-routing.module';
import { SendEmailPage } from './send-email/send-email.page';
import { SharedModule } from 'shared/shared.module';
import { SendPurchaseEmailComponentModule } from 'components/purchase/send-purchase-email/send-purchase-email.module';

@NgModule({
  declarations: [SendEmailPage],
  imports: [CommonModule, SharedModule, SendEmailFunctionRoutingModule, SendPurchaseEmailComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SendEmailFunctionModule {}
