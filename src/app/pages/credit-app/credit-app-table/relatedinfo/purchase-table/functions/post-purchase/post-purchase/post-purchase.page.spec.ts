import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PostPurchasePage } from './post-purchase.page';

describe('PostPurchasePage', () => {
  let component: PostPurchasePage;
  let fixture: ComponentFixture<PostPurchasePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PostPurchasePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostPurchasePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
