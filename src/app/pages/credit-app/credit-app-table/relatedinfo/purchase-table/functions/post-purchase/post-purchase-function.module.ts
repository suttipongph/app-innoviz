import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostPurchaseFunctionRoutingModule } from './post-purchase-function-routing.module';
import { PostPurchasePage } from './post-purchase/post-purchase.page';
import { SharedModule } from 'shared/shared.module';
import { PostPurchaseComponentModule } from 'components/purchase/post-purchase/post-purchase.module';

@NgModule({
  declarations: [PostPurchasePage],
  imports: [CommonModule, SharedModule, PostPurchaseFunctionRoutingModule, PostPurchaseComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PostPurchaseFunctionModule {}
