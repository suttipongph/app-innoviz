import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { SendEmailPage } from './send-email/send-email.page';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuardService],
    component: SendEmailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SendEmailFunctionRoutingModule {}
