import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { UpdateStatusRollbillPurchaseFunctionRoutingModule } from './update-status-rollbill-purchase-function-routing.module';
import { UpdateStatusRollbillPurchasePage } from './update-status-rollbill-purchase/update-status-rollbill-purchase.page';
import { UpdateStatusRollbillPurchaseComponentModule } from 'components/purchase/update-status-rollbill-purchase/update-status-rollbill-purchase.module';

@NgModule({
  declarations: [UpdateStatusRollbillPurchasePage],
  imports: [CommonModule, SharedModule, UpdateStatusRollbillPurchaseFunctionRoutingModule, UpdateStatusRollbillPurchaseComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UpdateStatusRollbillPurchaseFunctionModule {}
