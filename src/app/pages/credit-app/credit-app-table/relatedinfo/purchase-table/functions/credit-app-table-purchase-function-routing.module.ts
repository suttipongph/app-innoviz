import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'purchasetablesendemail',
    loadChildren: () => import('./send-e-mail/send-email-function.module').then((m) => m.SendEmailFunctionModule)
  },
  {
    path: 'cancelpurchase',
    loadChildren: () => import('./cancel-purchase/cancel-purchase-function.module').then((m) => m.CancelPurchaseFunctionModule)
  },
  {
    path: 'postpurchase',
    loadChildren: () => import('./post-purchase/post-purchase-function.module').then((m) => m.PostPurchaseFunctionModule)
  },
  {
    path: 'updatestatusrollbillpurchase',
    loadChildren: () => import('./update-status-rollbill-purchase/update-status-rollbill-purchase-function.module').then((m) => m.UpdateStatusRollbillPurchaseFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTablePurchaseFunctionRoutingModule {}
