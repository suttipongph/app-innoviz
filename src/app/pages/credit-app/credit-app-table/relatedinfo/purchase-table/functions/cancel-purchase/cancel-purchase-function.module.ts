import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { CancelPurchasePage } from './cancel-purchase/cancel-purchase.page';
import { CancelPurchaseComponentModule } from 'components/purchase/cancel-purchase/cancel-purchase.module';
import { CancelPurchaseFunctionRoutingModule } from './cancel-purchase-function-routing.module';

@NgModule({
  declarations: [CancelPurchasePage],
  imports: [CommonModule, SharedModule, CancelPurchaseFunctionRoutingModule, CancelPurchaseComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CancelPurchaseFunctionModule {}
