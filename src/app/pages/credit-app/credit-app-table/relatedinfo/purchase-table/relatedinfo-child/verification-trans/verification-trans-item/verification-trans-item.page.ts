import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'verification-trans-item.page',
  templateUrl: './verification-trans-item.page.html',
  styleUrls: ['./verification-trans-item.page.scss']
})
export class VerificationTransItemPage implements OnInit {
  pageInfo: PageInformationModel;
  parentRoute = this.uiService.getRelatedInfoParentTableName();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.VERIFICATION_TRANS,
      servicePath: `CreditAppTable/Factoring/RelatedInfo/${this.parentRoute}/RelatedInfo/${ROUTE_RELATED_GEN.VERIFICATION_TRANS}`
    };
  }
}
