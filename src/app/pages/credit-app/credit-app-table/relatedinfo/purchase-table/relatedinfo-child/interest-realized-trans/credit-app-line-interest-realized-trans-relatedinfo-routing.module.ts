import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InterestRealizedTransListPage } from './interest-realized-trans-list/interest-realized-trans-list.page';
import { InterestRealizedTransItemPage } from './interest-realized-trans-item/interest-realized-trans-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: InterestRealizedTransListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: InterestRealizedTransItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppLineInterestRealizedTransRelatedinfoRoutingModule {}
