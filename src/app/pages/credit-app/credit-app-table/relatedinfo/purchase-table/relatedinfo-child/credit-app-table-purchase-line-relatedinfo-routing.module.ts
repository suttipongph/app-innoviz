import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'interestrealizedtrans',
    loadChildren: () =>
      import('./interest-realized-trans/credit-app-line-interest-realized-trans-relatedinfo.module').then(
        (m) => m.CreditAppLineInterestRealizedTransRelatedinfoModule
      )
  },
  {
    path: 'verificationtrans',
    loadChildren: () =>
      import('./verification-trans/credit-app-table-purchase-line-verification-trans-relatedinfo.module').then(
        (m) => m.CreditAppTablePurchaseLineVerificationTransRelatedinfoModule
      )
  },
  {
    path: 'invoicetable',
    loadChildren: () =>
      import('./invoicetable/credit-app-table-purchase-line-invoicetable-relatedinfo.module').then(
        (m) => m.CreditAppTablePurchaseLineInvoiceTableRelatedinfoModule
      )
  },
  {
    path: 'inquiryrollbillpurchaseline',
    loadChildren: () =>
      import('./inquiry-roll-bill-purchase-line/credit-app-line-inquiry-roll-bill-purchase-line-relatedinfo.module').then(
        (m) => m.CreditAppLineInquiryRollBillPurchaseLineRelatedinfoModule
      )
  },
  {
    path: 'productsettlement',
    loadChildren: () =>
      import('./product-settled-trans/purchase-table-line-product-settled-trans.module').then((m) => m.PurchaseTableLineProductSettledTransModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTablePurchaseLineRelatedinfoRoutingModule {}
