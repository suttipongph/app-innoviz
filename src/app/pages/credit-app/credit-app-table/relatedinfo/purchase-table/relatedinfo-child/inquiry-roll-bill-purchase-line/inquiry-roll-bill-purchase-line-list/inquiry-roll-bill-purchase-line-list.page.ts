import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'inquiry-roll-bill-purchase-line-list-page',
  templateUrl: './inquiry-roll-bill-purchase-line-list.page.html',
  styleUrls: ['./inquiry-roll-bill-purchase-line-list.page.scss']
})
export class InquiryRollBillPurchaseLineListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  parentRoute = this.uiService.getRelatedInfoParentTableName();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.INQUIRY_ROLL_BILL_PURCHASE_LINE,

      servicePath: `CreditAppTable/Factoring/RelatedInfo/${this.parentRoute}/RelatedInfo/${ROUTE_RELATED_GEN.INQUIRY_ROLL_BILL_PURCHASE_LINE}`
    };
  }
}
