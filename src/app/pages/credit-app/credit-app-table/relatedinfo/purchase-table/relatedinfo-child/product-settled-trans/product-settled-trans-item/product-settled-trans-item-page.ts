import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-product-settled-trans-item-page',
  templateUrl: './product-settled-trans-item-page.html',
  styleUrls: ['./product-settled-trans-item-page.scss']
})
export class ProductSettledTransItemPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  parentRoute = this.uiService.getRelatedInfoParentTableName();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.PRODUCT_SETTLEMENT,
      servicePath: `CreditAppTable/Factoring/RelatedInfo/${this.parentRoute}/RelatedInfo/productsettlement`
    };
  }
}
