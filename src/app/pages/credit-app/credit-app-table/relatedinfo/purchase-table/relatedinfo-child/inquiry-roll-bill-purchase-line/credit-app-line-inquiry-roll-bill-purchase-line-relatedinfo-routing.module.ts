import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InquiryRollBillPurchaseLineListPage } from './inquiry-roll-bill-purchase-line-list/inquiry-roll-bill-purchase-line-list.page';
import { InquiryRollBillPurchaseLineItemPage } from './inquiry-roll-bill-purchase-line-item/inquiry-roll-bill-purchase-line-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: InquiryRollBillPurchaseLineListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: InquiryRollBillPurchaseLineItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppLineInquiryRollBillPurchaseLineRelatedinfoRoutingModule {}
