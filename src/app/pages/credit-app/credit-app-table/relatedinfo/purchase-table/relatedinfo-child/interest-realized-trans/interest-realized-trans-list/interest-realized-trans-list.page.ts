import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'interest-realized-trans-list-page',
  templateUrl: './interest-realized-trans-list.page.html',
  styleUrls: ['./interest-realized-trans-list.page.scss']
})
export class InterestRealizedTransListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  parentRoute = this.uiService.getRelatedInfoParentTableName();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.INTEREST_REALIZED_TRANSACTION,

      servicePath: `CreditAppTable/Factoring/RelatedInfo/${this.parentRoute}/RelatedInfo/${ROUTE_RELATED_GEN.INTEREST_REALIZED_TRANSACTION}`
    };
  }
}
