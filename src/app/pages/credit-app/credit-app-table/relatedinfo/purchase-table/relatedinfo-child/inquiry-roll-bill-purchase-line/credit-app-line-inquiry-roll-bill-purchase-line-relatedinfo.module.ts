import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppLineInquiryRollBillPurchaseLineRelatedinfoRoutingModule } from './credit-app-line-inquiry-roll-bill-purchase-line-relatedinfo-routing.module';
import { InquiryRollBillPurchaseLineListPage } from './inquiry-roll-bill-purchase-line-list/inquiry-roll-bill-purchase-line-list.page';
import { InquiryRollBillPurchaseLineItemPage } from './inquiry-roll-bill-purchase-line-item/inquiry-roll-bill-purchase-line-item.page';
import { SharedModule } from 'shared/shared.module';
import { InquiryRollbillPurchaseLineComponentModule } from 'components/inquiry/inquiry-rollbill-purchase-line/inquiry-rollbill-purchase-line.module';

@NgModule({
  declarations: [InquiryRollBillPurchaseLineListPage, InquiryRollBillPurchaseLineItemPage],
  imports: [CommonModule, SharedModule, CreditAppLineInquiryRollBillPurchaseLineRelatedinfoRoutingModule, InquiryRollbillPurchaseLineComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppLineInquiryRollBillPurchaseLineRelatedinfoModule {}
