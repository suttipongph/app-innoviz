import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InquiryRollBillPurchaseLineItemPage } from './inquiry-roll-bill-purchase-line-item.page';

describe('InquiryRollBillPurchaseLineItemPage', () => {
  let component: InquiryRollBillPurchaseLineItemPage;
  let fixture: ComponentFixture<InquiryRollBillPurchaseLineItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InquiryRollBillPurchaseLineItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryRollBillPurchaseLineItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
