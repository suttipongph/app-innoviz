import { ComponentFixture, TestBed } from '@angular/core/testing';
import { VerificationTransListPage } from './verification-trans-list.page';

describe('VendorPaymentListPage', () => {
  let component: VerificationTransListPage;
  let fixture: ComponentFixture<VerificationTransListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VerificationTransListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationTransListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
