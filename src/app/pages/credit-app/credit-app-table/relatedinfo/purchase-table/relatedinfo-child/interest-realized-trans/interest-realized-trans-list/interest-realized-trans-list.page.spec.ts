import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InterestRealizedTransListPage } from './interest-realized-trans-list.page';

describe('InterestRealizedTransListPage', () => {
  let component: InterestRealizedTransListPage;
  let fixture: ComponentFixture<InterestRealizedTransListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InterestRealizedTransListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestRealizedTransListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
