import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InquiryRollBillPurchaseLineListPage } from './inquiry-roll-bill-purchase-line-list.page';

describe('InquiryRollBillPurchaseLineListPage', () => {
  let component: InquiryRollBillPurchaseLineListPage;
  let fixture: ComponentFixture<InquiryRollBillPurchaseLineListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InquiryRollBillPurchaseLineListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryRollBillPurchaseLineListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
