import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductSettledTransListPage } from './product-settled-trans-list-page';

describe('ProductSettledTransListPageComponent', () => {
  let component: ProductSettledTransListPage;
  let fixture: ComponentFixture<ProductSettledTransListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductSettledTransListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSettledTransListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
