import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppLineInterestRealizedTransRelatedinfoRoutingModule } from './credit-app-line-interest-realized-trans-relatedinfo-routing.module';
import { InterestRealizedTransListPage } from './interest-realized-trans-list/interest-realized-trans-list.page';
import { InterestRealizedTransItemPage } from './interest-realized-trans-item/interest-realized-trans-item.page';
import { SharedModule } from 'shared/shared.module';
import { InterestRealizedTransComponentModule } from 'components/interest-realized-trans/interest-realized-trans.module';

@NgModule({
  declarations: [InterestRealizedTransListPage, InterestRealizedTransItemPage],
  imports: [CommonModule, SharedModule, CreditAppLineInterestRealizedTransRelatedinfoRoutingModule, InterestRealizedTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppLineInterestRealizedTransRelatedinfoModule {}
