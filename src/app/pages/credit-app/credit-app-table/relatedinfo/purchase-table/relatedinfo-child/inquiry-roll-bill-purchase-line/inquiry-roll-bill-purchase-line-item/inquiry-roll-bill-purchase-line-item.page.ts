import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'inquiry-roll-bill-purchase-line-item.page',
  templateUrl: './inquiry-roll-bill-purchase-line-item.page.html',
  styleUrls: ['./inquiry-roll-bill-purchase-line-item.page.scss']
})
export class InquiryRollBillPurchaseLineItemPage implements OnInit {
  pageInfo: PageInformationModel;
  parentRoute = this.uiService.getRelatedInfoParentTableName();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.INQUIRY_ROLL_BILL_PURCHASE_LINE,
      servicePath: `CreditAppTable/Factoring/RelatedInfo/${this.parentRoute}/RelatedInfo/${ROUTE_RELATED_GEN.INQUIRY_ROLL_BILL_PURCHASE_LINE}`
    };
  }
}
