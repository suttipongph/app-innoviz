import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PurchaseLineItemPage } from './purchase-line-item.page';

describe('PurchaseLineItemPage', () => {
  let component: PurchaseLineItemPage;
  let fixture: ComponentFixture<PurchaseLineItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PurchaseLineItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseLineItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
