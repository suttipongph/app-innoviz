import { Component, OnInit } from '@angular/core';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'additional-purchase-page',
  templateUrl: './additional-purchase.page.html',
  styleUrls: ['./additional-purchase.page.scss']
})
export class AdditionalPurchasePage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.ADDITIONAL_PURCHASE,
      servicePath: 'CreditAppTable/Factoring/RelatedInfo/PurchaseLinePurchase-Child/Function/AdditionalPurchase'
    };
  }
}
