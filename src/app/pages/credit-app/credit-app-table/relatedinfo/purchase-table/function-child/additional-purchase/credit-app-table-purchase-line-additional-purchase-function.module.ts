import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { CreditAppTablePurchaseLineAdditionalPurchaseFunctionRoutingModule } from './credit-app-table-purchase-line-additional-purchase-function-routing.module';
import { AdditionalPurchaseComponentModule } from 'components/purchase/additional-purchase/additional-purchase.module';
import { AdditionalPurchasePage } from './additional-purchase/additional-purchase.page';

@NgModule({
  declarations: [AdditionalPurchasePage],
  imports: [CommonModule, SharedModule, CreditAppTablePurchaseLineAdditionalPurchaseFunctionRoutingModule, AdditionalPurchaseComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTablePurchaseLineAdditionalPurchaseFunctionModule {}
