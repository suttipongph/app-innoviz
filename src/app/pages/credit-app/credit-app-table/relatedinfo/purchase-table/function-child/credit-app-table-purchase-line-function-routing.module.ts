import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'additionalpurchase',
    loadChildren: () =>
      import('./additional-purchase/credit-app-table-purchase-line-additional-purchase-function.module').then(
        (m) => m.CreditAppTablePurchaseLineAdditionalPurchaseFunctionModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTablePurchaseLineFunctionRoutingModule {}
