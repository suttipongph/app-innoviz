import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdditionalPurchasePage } from './additional-purchase.page';

describe('AdditionalPurchasePage', () => {
  let component: AdditionalPurchasePage;
  let fixture: ComponentFixture<AdditionalPurchasePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdditionalPurchasePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdditionalPurchasePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
