import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { AdditionalPurchasePage } from './additional-purchase/additional-purchase.page';

const routes: Routes = [
  {
    path: '',
    component: AdditionalPurchasePage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTablePurchaseLineAdditionalPurchaseFunctionRoutingModule {}
