import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PurchaseLineItemPage } from './purchase-line-item/purchase-line-item.page';
import { PurchaseTableItemPage } from './purchase-table-item/purchase-table-item.page';
import { PurchaseTableListPage } from './purchase-table-list/purchase-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: PurchaseTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: PurchaseTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/purchaselinepurchase-child/:id',
    component: PurchaseLineItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/purchaselinerollbill-child/:id',
    component: PurchaseLineItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () =>
      import('./relatedinfo/credit-app-table-purchase-table-relatedinfo.module').then((m) => m.CreditAppTablePurchaseTableRelatedinfoModule)
  },
  {
    path: ':id/purchaselinepurchase-child/:id/relatedinfo',
    loadChildren: () =>
      import('./relatedinfo-child/credit-app-table-purchase-line-relatedinfo.module').then((m) => m.CreditAppTablePurchaseLineRelatedinfoModule)
  },
  {
    path: ':id/purchaselinepurchase-child/:id/function',
    loadChildren: () =>
      import('./function-child/credit-app-table-purchase-line-function.module').then((m) => m.CreditAppTablePurchaseLineFunctionModule)
  },
  {
    path: ':id/purchaselinerollbill-child/:id/relatedinfo',
    loadChildren: () =>
      import('./relatedinfo-child/credit-app-table-purchase-line-relatedinfo.module').then((m) => m.CreditAppTablePurchaseLineRelatedinfoModule)
  },
  {
    path: ':id/purchaselinerollbill-child/:id/function',
    loadChildren: () =>
      import('./function-child/credit-app-table-purchase-line-function.module').then((m) => m.CreditAppTablePurchaseLineFunctionModule)
  },
  {
    path: ':id/workflow',
    loadChildren: () => import('./workflow/purchase-action-workflow/purchase-action-workflow.module').then((m) => m.PurchaseActionWorkflowModule)
  },
  {
    path: ':id/workflow/actionhistory',
    loadChildren: () => import('./workflow/purchase-action-history/purchase-action-history.module').then((m) => m.PurchaseActionHistoryModule)
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/credit-app-table-purchase-function.module').then((m) => m.CreditAppTablePurchaseFunctionModule)
  },
  {
    path: ':id/report',
    loadChildren: () => import('./report/credit-app-table-purchase-table-report.module').then((m) => m.CreditAppTablePurchaseReportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTablePurchaseTableRoutingModule {}
