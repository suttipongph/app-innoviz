import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppTablePurchaseTableRoutingModule } from './credit-app-table-purchase-table-routing.module';
import { PurchaseTableListPage } from './purchase-table-list/purchase-table-list.page';
import { PurchaseTableItemPage } from './purchase-table-item/purchase-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { PurchaseLineItemPage } from './purchase-line-item/purchase-line-item.page';
import { PurchaseTableComponentModule } from 'components/purchase/purchase-table/purchase-table.module';
import { PurchaseLineComponentModule } from 'components/purchase/purchase-line/purchase-line.module';

@NgModule({
  declarations: [PurchaseTableListPage, PurchaseTableItemPage, PurchaseLineItemPage],
  imports: [CommonModule, SharedModule, CreditAppTablePurchaseTableRoutingModule, PurchaseTableComponentModule, PurchaseLineComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTablePurchaseTableModule {}
