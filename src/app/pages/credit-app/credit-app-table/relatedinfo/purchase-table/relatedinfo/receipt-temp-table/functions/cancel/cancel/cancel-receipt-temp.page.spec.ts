import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CancelReceiptTempPage } from './cancel-receipt-temp.page';

describe('CancelPage', () => {
  let component: CancelReceiptTempPage;
  let fixture: ComponentFixture<CancelReceiptTempPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CancelReceiptTempPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelReceiptTempPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
