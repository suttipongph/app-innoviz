import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, ReceiptTempRefType, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'receipt-temp-table-list-page',
  templateUrl: './receipt-temp-table-list.page.html',
  styleUrls: ['./receipt-temp-table-list.page.scss']
})
export class ReceiptTempTableListPage implements OnInit {
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor(public uiService: UIControllerService) { }

  ngOnInit(): void {
    this.setOption();
    this.setPath();
    
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'receiptTempTableGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }

  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.COLLECTION, servicePath: `CreditAppTable/Factoring/RelatedInfo/${this.activeRoute}/RelatedInfo/ReceiptTempTable` };
  }
}
