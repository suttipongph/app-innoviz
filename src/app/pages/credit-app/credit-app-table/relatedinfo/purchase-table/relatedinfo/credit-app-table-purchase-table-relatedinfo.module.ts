import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { CreditAppTablePurchaseTableRelatedinfoRoutingModule } from './credit-app-table-purchase-table-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, CreditAppTablePurchaseTableRelatedinfoRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTablePurchaseTableRelatedinfoModule {}
