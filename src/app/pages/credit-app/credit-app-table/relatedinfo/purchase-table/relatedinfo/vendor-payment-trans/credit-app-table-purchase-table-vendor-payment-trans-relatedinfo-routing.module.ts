import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VendorPaymentTransListPage } from './vendor-payment-trans-list/vendor-payment-trans-list.page';
import { VendorPaymentTransItemPage } from './vendor-payment-trans-item/vendor-payment-trans-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: VendorPaymentTransListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: VendorPaymentTransItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'function',
    loadChildren: () =>
      import('./function/purchase-table-vendor-payment-trans-function.module').then(
        (m) => m.CreditAppTablePurchaseTableVendorPaymentTransFunctionModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTablePurchaseTableVendorPaymentTransRelatedinfoRoutingModule {}
