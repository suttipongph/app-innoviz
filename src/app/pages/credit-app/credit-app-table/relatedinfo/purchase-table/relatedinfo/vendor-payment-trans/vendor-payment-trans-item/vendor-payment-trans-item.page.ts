import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'vendor-payment-trans-item.page',
  templateUrl: './vendor-payment-trans-item.page.html',
  styleUrls: ['./vendor-payment-trans-item.page.scss']
})
export class VendorPaymentTransItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.VENDOR_PAYMENT_TRANS,
      servicePath: `CreditAppTable/Factoring/RelatedInfo/${ROUTE_RELATED_GEN.PURCHASE_TABLE_PURCHASE}/RelatedInfo/${ROUTE_RELATED_GEN.VENDOR_PAYMENT_TRANS}`
    };
  }
}
