import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReceiptTempTableRelatedinfoRoutingModule } from './receipt-temp-table-relatedinfo-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReceiptTempTableRelatedinfoRoutingModule
  ]
})
export class ReceiptTempTableRelatedinfoModule { }
