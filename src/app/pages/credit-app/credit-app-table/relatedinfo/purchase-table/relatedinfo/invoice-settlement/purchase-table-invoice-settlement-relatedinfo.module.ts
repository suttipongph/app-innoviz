import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PurchaseTableInvoiceSettlementRelatedinfoRoutingModule } from './purchase-table-invoice-settlement-relatedinfo-routing.module';
import { InvoiceSettlementListPage } from './invoice-settlement-list/invoice-settlement-list.page';
import { InvoiceSettlementItemPage } from './invoice-settlement-item/invoice-settlement-item.page';
import { SharedModule } from 'shared/shared.module';
import { InvoiceSettlementDetailComponentModule } from 'components/invoice-settlement-detail/invoice-settlement-detail/invoice-settlement-detail.module';

@NgModule({
  declarations: [InvoiceSettlementListPage, InvoiceSettlementItemPage],
  imports: [CommonModule, SharedModule, PurchaseTableInvoiceSettlementRelatedinfoRoutingModule, InvoiceSettlementDetailComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PurchaseTableInvoiceSettlementRelatedinfoModule {}
