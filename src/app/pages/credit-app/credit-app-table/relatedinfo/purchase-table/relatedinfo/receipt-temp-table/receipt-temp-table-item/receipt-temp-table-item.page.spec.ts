import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReceiptTempTableItemPage } from './receipt-temp-table-item.page';

describe('ReceiptTempTableItemPage', () => {
  let component: ReceiptTempTableItemPage;
  let fixture: ComponentFixture<ReceiptTempTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReceiptTempTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptTempTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
