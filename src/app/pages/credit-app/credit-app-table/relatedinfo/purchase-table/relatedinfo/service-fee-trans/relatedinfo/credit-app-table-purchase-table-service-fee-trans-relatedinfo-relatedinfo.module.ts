import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppTablePurchaseTableServiceFeeTransRelatedinfoRelatedInfoRoutingModule } from './credit-app-table-purchase-table-service-fee-trans-relatedinfo-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, CreditAppTablePurchaseTableServiceFeeTransRelatedinfoRelatedInfoRoutingModule]
})
export class CreditAppTablePurchaseTableServiceFeeTransRelatedinfoRelatedInfoModule {}
