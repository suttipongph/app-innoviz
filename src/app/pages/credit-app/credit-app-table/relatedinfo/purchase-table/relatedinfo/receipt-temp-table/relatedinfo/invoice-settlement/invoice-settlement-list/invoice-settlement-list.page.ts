import { Component, OnInit } from '@angular/core';
import { ColumnType, RefType, ROUTE_RELATED_GEN, SortType, SuspenseInvoiceType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'invoice-settlement-list-page',
  templateUrl: './invoice-settlement-list.page.html',
  styleUrls: ['./invoice-settlement-list.page.scss']
})
export class InvoiceSettlementListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'invoiceSettlementDetailGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'suspenseInvoiceType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: SuspenseInvoiceType.None.toString()
      }
    ];
    this.option.columns = columns;
  }

  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.INVOICE_SETTLEMENT,
      servicePath: `ReceiptTempTable/RelatedInfo/InvoiceSettlement`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.INVOICE_SETTLEMENT,
          servicePath: `ReceiptTempTable/RelatedInfo/InvoiceSettlement`
        }
      ]
    };
  }
}
