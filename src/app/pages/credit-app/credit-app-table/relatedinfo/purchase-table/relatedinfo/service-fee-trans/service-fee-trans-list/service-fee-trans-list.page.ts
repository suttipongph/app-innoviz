import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'service-fee-trans-list-page',
  templateUrl: './service-fee-trans-list.page.html',
  styleUrls: ['./service-fee-trans-list.page.scss']
})
export class ServiceFeeTransListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();

  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'serviceFeeTransGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.SERVICE_FEE_TRANS,
      servicePath: `CreditAppTable/Factoring/RelatedInfo/${this.activeRoute}/RelatedInfo/${ROUTE_RELATED_GEN.SERVICE_FEE_TRANS}`
    };
  }
}
