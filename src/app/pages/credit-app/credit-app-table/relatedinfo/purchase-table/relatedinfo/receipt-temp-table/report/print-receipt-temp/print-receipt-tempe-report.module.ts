import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { PrintReceiptTempPage } from './print-receipt-temp/print-receipt-temp.page';
import { PrintReceiptTempReportRoutingModule } from './print-receipt-temp-report-routing.module';
import { PrintReceiptTempComponentModule } from 'components/receipt-temp-table/print-receipt-temp/print-receipt-temp.module';

@NgModule({
  declarations: [PrintReceiptTempPage],
  imports: [CommonModule, SharedModule, PrintReceiptTempReportRoutingModule, PrintReceiptTempComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PrintReceiptTempReportModule {}
