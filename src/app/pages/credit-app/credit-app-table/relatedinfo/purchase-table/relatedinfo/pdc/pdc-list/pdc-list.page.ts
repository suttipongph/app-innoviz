import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, RefType, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'pdc-list-page',
  templateUrl: './pdc-list.page.html',
  styleUrls: ['./pdc-list.page.scss']
})
export class PdcListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  parentId = this.uiService.getRelatedInfoParentTableKey();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'chequeTableGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      },
      {
        label: null,
        textKey: 'refType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: RefType.PurchaseTable.toString()
      }
    ];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.PDC,
      servicePath: `CreditAppTable/Factoring/RelatedInfo/${this.activeRoute}/RelatedInfo/${ROUTE_RELATED_GEN.PDC}`
    };
  }
}
