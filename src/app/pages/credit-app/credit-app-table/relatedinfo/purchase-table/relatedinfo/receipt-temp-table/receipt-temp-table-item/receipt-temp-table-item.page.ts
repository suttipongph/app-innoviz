import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { MenuItem } from 'shared/models/primeModel';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'receipt-temp-table-item-page',
  templateUrl: './receipt-temp-table-item.page.html',
  styleUrls: ['./receipt-temp-table-item.page.scss']
})
export class ReceiptTempTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  relatedInfoItems: MenuItem[];

  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
    this.setRelatedInfoOptionsByPurchaseTable();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.COLLECTION,
      servicePath: 'ReceiptTempTable',
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.RECEIPT_TEMP_PAYM_DETAIL,
          servicePath: `CreditAppTable/Factoring/RelatedInfo/${this.activeRoute}/RelatedInfo/ReceiptTempTable`
        }
      ]
    };
  }

  setRelatedInfoOptionsByPurchaseTable(): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        visible: false
      },
      {
        label: 'LABEL.MEMO',
        visible: false
      },
      {
        label: 'LABEL.SERVICE_FEE',
        visible: false
      },
      {
        label: 'LABEL.BUYER_RECEIPT',
        visible: false
      }
    ];
  }
}
