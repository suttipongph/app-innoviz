import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'cancelreceipttemp',
    loadChildren: () => import('./cancel/cancel-receipt-temp-function.module').then((m) => m.CancelReceiptTempFunctionModule)
  },
  {
    path: 'postreceipttemp',
    loadChildren: () => import('./post/post-receipt-temp-function.module').then((m) => m.PostReceiptTempFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceiptTempTableFunctionRoutingModule {}
