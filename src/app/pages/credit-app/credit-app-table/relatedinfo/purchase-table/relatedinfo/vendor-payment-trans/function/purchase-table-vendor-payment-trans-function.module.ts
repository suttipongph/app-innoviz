import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { CreditAppTablePurchaseTableVendorPaymentTransFunctionRoutingModule } from './purchase-table-vendor-payment-trans-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, CreditAppTablePurchaseTableVendorPaymentTransFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTablePurchaseTableVendorPaymentTransFunctionModule {}
