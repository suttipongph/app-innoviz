import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'attachment',
    loadChildren: () => import('./attachment/attachment.module').then((m) => m.AttachmentModule)
  },
  {
    path: 'servicefeetrans',
    loadChildren: () =>
      import('./service-fee-trans/credit-app-table-purchase-table-service-fee-trans-relatedinfo.module').then(
        (m) => m.CreditAppTablePurchaseTableServiceFeeTransRelatedinfoModule
      )
  },
  {
    path: 'vendorpaymenttrans',
    loadChildren: () =>
      import('./vendor-payment-trans/credit-app-table-purchase-table-vendor-payment-trans-relatedinfo.module').then(
        (m) => m.CreditAppTablePurchaseTableVendorPaymentTransRelatedinfoModule
      )
  },
  {
    path: 'paymentdetail',
    loadChildren: () =>
      import('./payment-detail/credit-app-table-purchase-table-payment-detail-relatedinfo.module').then(
        (m) => m.CreditAppTablePurchaseTablePaymentDetailRelatedinfoModule
      )
  },
  {
    path: 'pdc',
    loadChildren: () => import('./pdc/pdc-relatedinfo.module').then((m) => m.PdcRelatedinfoModule)
  },
  {
    path: 'memotrans',
    loadChildren: () => import('./memo/credit-app-table-purchase-table-memo-relatedinfo.module').then((m) => m.MemoRelatedinfoModule)
  },
  {
    path: 'invoicesettlement',
    loadChildren: () =>
      import('./invoice-settlement/purchase-table-invoice-settlement-relatedinfo.module').then(
        (m) => m.PurchaseTableInvoiceSettlementRelatedinfoModule
      )
  },
  {
    path: 'suspensesettlement',
    loadChildren: () => import('./suspense-settlement/suspense-settlement-relatedinfo.module').then((m) => m.SuspenseSettlementRelatedinfoModule)
  },
  {
    path: 'receipttemptable',
    loadChildren: () => import('./receipt-temp-table/receipt-temp-table.module').then((m) => m.ReceiptTempTableModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTablePurchaseTableRelatedinfoRoutingModule {}
