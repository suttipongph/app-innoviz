import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'receipt-line-list-page',
  templateUrl: './receipt-line-list.page.html',
  styleUrls: ['./receipt-line-list.page.scss']
})
export class ReceiptLineListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'receiptLineGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.RECEIPT_TABLE,
      servicePath: `ReceiptTempTable/RelatedInfo/receipttable/receiptline`,
      childPaths: [
        {
          pagePath: ROUTE_MASTER_GEN.RECEIPT_TABLE,
          servicePath: `ReceiptTempTable/RelatedInfo/receipttable/receiptline`
        }
      ]
    };
  }
}
