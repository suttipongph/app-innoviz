import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'printreceipttemp',
    loadChildren: () => import('./print-receipt-temp/print-receipt-tempe-report.module').then((m) => m.PrintReceiptTempReportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceiptTempReportRoutingModule {}
