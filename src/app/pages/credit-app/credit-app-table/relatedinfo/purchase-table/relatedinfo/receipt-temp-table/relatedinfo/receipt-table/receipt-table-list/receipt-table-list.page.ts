import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'receipt-table-list-page',
  templateUrl: './receipt-table-list.page.html',
  styleUrls: ['./receipt-table-list.page.scss']
})
export class ReceiptTableListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  parentId;
  constructor(public uiControllerService: UIControllerService
    ) {
      this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();

    }

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'receiptTableGUID';
    const columns: ColumnModel[] = [{
      label: null,
      textKey: 'refGUID',
      type: ColumnType.MASTER,
      visibility: true,
      sorting: SortType.NONE,
      parentKey: this.parentId
    }];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.RECEIPT_TABLE,
      servicePath: `ReceiptTempTable/RelatedInfo/receipttable`,
      childPaths: [
        {
          pagePath: ROUTE_MASTER_GEN.RECEIPT_TABLE,
          servicePath: `ReceiptTempTable/RelatedInfo/receipttable`
        }
      ]
    };
  }
}
