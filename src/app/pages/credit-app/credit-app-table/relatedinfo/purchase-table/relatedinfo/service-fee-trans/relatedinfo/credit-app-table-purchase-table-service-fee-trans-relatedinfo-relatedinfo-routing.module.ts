import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'invoicetable',
    loadChildren: () =>
      import('./invoicetable/credit-app-table-purchase-table-service-fee-trans-invoicetable-relatedinfo.module').then(
        (m) => m.CreditAppTablePurchaseTableServiceFeeTransInvoiceTableRelatedinfoModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTablePurchaseTableServiceFeeTransRelatedinfoRelatedInfoRoutingModule {}
