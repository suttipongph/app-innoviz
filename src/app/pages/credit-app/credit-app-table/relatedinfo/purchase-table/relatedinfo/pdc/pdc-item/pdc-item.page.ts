import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'pdc-item.page',
  templateUrl: './pdc-item.page.html',
  styleUrls: ['./pdc-item.page.scss']
})
export class PdcItemPage implements OnInit {
  pageInfo: PageInformationModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.PDC,
      servicePath: `CreditAppTable/Factoring/RelatedInfo/${this.activeRoute}/RelatedInfo/${ROUTE_RELATED_GEN.PDC}`
    };
  }
}
