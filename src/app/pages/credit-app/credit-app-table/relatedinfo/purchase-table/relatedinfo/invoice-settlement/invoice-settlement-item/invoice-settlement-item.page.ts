import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'invoice-settlement-item.page',
  templateUrl: './invoice-settlement-item.page.html',
  styleUrls: ['./invoice-settlement-item.page.scss']
})
export class InvoiceSettlementItemPage implements OnInit {
  pageInfo: PageInformationModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.INVOICE_SETTLEMENT,
      servicePath: `CreditAppTable/Factoring/RelatedInfo/${this.activeRoute}/RelatedInfo/InvoiceSettlement`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.INVOICE_SETTLEMENT,
          servicePath: `CreditAppTable/Factoring/RelatedInfo/${this.activeRoute}/RelatedInfo/InvoiceSettlement`
        }
      ]
    };
  }
}
