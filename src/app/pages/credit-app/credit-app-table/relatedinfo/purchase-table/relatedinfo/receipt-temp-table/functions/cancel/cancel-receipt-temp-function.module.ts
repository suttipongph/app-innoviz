import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CancelReceiptTempFunctionRoutingModule } from './cancel-receipt-temp-function-routing.module';
import { CancelReceiptTempPage } from './cancel/cancel-receipt-temp.page';
import { SharedModule } from 'shared/shared.module';
import { CancelReceiptTempComponentModule } from 'components/receipt-temp-table/cancel-receipt-temp/cancel-receipt-temp.module';

@NgModule({
  declarations: [CancelReceiptTempPage],
  imports: [CommonModule, SharedModule, CancelReceiptTempFunctionRoutingModule, CancelReceiptTempComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CancelReceiptTempFunctionModule {}
