import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaymentDetailListPage } from './payment-detail-list/payment-detail-list.page';
import { PaymentDetailItemPage } from './payment-detail-item/payment-detail-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: PaymentDetailListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: PaymentDetailItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTablePurchaseTablePaymentDetailRelatedinfoRoutingModule {}
