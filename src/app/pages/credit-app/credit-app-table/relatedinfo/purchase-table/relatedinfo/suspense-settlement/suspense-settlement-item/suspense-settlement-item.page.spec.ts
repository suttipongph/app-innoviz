import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SuspenseSettlementItemPage } from './suspense-settlement-item.page';

describe('SuspenseSettlementItemPage', () => {
  let component: SuspenseSettlementItemPage;
  let fixture: ComponentFixture<SuspenseSettlementItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SuspenseSettlementItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SuspenseSettlementItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
