import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompleteFunctionRoutingModule } from './complete-function-routing.module';
import { CompletePage } from './complete/complete.page';
import { SharedModule } from 'shared/shared.module';
import { CompleteChequeComponentModule } from 'components/cheque-table/cheque-table/complete-cheque/complete-cheque.module';
@NgModule({
  declarations: [CompletePage],
  imports: [CommonModule, SharedModule, CompleteFunctionRoutingModule, CompleteChequeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CompleteFunctionModule {}
