import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'send-vendor-invoice-staging-page',
  templateUrl: './send-vendor-invoice.page.html',
  styleUrls: ['./send-vendor-invoice.page.scss']
})
export class SendVendorInvoiceStagingPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.SEND_VENDOR_INVOICE_STAGING,
      servicePath: `CreditAppTable/Factoring/RelatedInfo/${ROUTE_RELATED_GEN.PURCHASE_TABLE_PURCHASE}/RelatedInfo/${ROUTE_RELATED_GEN.VENDOR_PAYMENT_TRANS}/Function/SendVendorInvoiceStaging`
    };
  }
}
