import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { PrintReceiptCopyRoutingModule } from './receipt-copy-report-routing.module';
import { PrintReceiptCopyPage } from './print-receipt-copy/print-receipt-copy.page';
import { PrintReceiptComponentModule } from 'components/receipt-table/print-receipt/print-receipt.module';

@NgModule({
  declarations: [PrintReceiptCopyPage],
  imports: [CommonModule, SharedModule, PrintReceiptCopyRoutingModule, PrintReceiptComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PrintReceiptCopyReportModule {}
