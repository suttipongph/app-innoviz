import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_REPORT_GEN } from 'shared/constants/constantGen';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'print-receipt-copy-page',
  templateUrl: './print-receipt-copy.page.html',
  styleUrls: ['./print-receipt-copy.page.scss']
})
export class PrintReceiptCopyPage implements OnInit {
  pageInfo: PageInformationModel;
  pageLabel: string;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageLabel = 'copy';
    this.pageInfo = {
      pagePath: ROUTE_REPORT_GEN.PRINT_COPY,
      servicePath: `ReceiptTable/Report/PrintReceiptCopy`
    };
  }
}
