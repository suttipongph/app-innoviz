import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PurchaseTableItemPage } from './purchase-table-item.page';

describe('PurchaseTableItemPage', () => {
  let component: PurchaseTableItemPage;
  let fixture: ComponentFixture<PurchaseTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PurchaseTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
