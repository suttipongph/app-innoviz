import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'purchase-table-item-page',
  templateUrl: './purchase-table-item.page.html',
  styleUrls: ['./purchase-table-item.page.scss']
})
export class PurchaseTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  childPagePath = null;
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPagePath();
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: this.activeRoute,
      servicePath: `CreditAppTable/Factoring/RelatedInfo/${this.activeRoute}`,
      childPaths: [
        {
          pagePath: this.childPagePath,
          servicePath: `CreditAppTable/Factoring/RelatedInfo/${this.activeRoute}`
        }
      ]
    };
  }

  setPagePath() {
    if (this.activeRoute == ROUTE_RELATED_GEN.PURCHASE_TABLE_PURCHASE) {
      this.childPagePath = ROUTE_RELATED_GEN.PURCHASE_LINE_PURCHASE;
    } else {
      this.childPagePath = ROUTE_RELATED_GEN.PURCHASE_LINE_ROLL_BILL;
    }
  }
}
