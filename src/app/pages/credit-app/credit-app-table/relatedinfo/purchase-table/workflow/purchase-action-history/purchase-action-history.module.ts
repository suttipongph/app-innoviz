import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { PurchaseActionHistoryRoutingModule } from './purchase-action-history-routing.module';
import { WorkFlowPurchaseActionHistoryPage } from './purchase-action-history/purchase-action-history.page';
import { ActionHistoryComponentModule } from 'components/action-history/action-history.module';

@NgModule({
  declarations: [WorkFlowPurchaseActionHistoryPage],
  imports: [CommonModule, SharedModule, PurchaseActionHistoryRoutingModule, ActionHistoryComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PurchaseActionHistoryModule {}
