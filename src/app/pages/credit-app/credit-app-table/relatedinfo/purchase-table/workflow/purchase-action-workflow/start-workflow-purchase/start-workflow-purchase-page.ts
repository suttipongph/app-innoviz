import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';
@Component({
  selector: 'app-start-workflow-purchase',
  templateUrl: './start-workflow-purchase-page.html',
  styleUrls: ['./start-workflow-purchase-page.scss']
})
export class StartWorkflowPurchasePage implements OnInit {
  pageInfo: PageInformationModel;
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.FACTORING_CREDIT_APPLICATION_TABLE,
      servicePath: this.setServicePath()
    };
  }

  setServicePath() {
    if (this.uiService.getRelatedInfoActiveTableName() == ROUTE_RELATED_GEN.PURCHASE_TABLE_PURCHASE) {
      return `CreditAppTable/Factoring/RelatedInfo/${ROUTE_RELATED_GEN.PURCHASE_TABLE_PURCHASE}/Workflow/StartWorkflow`;
    } else {
      return `CreditAppTable/Factoring/RelatedInfo/${ROUTE_RELATED_GEN.PURCHASE_TABLE_ROLL_BILL}/Workflow/StartWorkflow`;
    }
  }
}
