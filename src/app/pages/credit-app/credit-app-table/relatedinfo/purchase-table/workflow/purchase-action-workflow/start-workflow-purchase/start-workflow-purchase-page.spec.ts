import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StartWorkflowPurchasePage } from './start-workflow-purchase-page';

describe('DemoWorkflowComponent', () => {
  let component: StartWorkflowPurchasePage;
  let fixture: ComponentFixture<StartWorkflowPurchasePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StartWorkflowPurchasePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StartWorkflowPurchasePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
