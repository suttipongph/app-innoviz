import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { PurchaseActionWorkflowRoutingModule } from './purchase-action-workflow-routing.module';
import { StartWorkflowPurchasePage } from './start-workflow-purchase/start-workflow-purchase-page';
import { ActionWorkflowPurchasePage } from './action-workflow-purchase/action-workflow-purchase-page';
import { WorkFlowPurchaseComponentModule } from 'components/purchase/purchase-table/work-flow-purchase-table/work-flow-purchase.module';

@NgModule({
  declarations: [StartWorkflowPurchasePage, ActionWorkflowPurchasePage],
  imports: [CommonModule, SharedModule, PurchaseActionWorkflowRoutingModule, WorkFlowPurchaseComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PurchaseActionWorkflowModule {}
