import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkFlowPurchaseActionHistoryPage } from './purchase-action-history.page';

describe('WorkFlowPurchaseActionHistoryPage', () => {
  let component: WorkFlowPurchaseActionHistoryPage;
  let fixture: ComponentFixture<WorkFlowPurchaseActionHistoryPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WorkFlowPurchaseActionHistoryPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkFlowPurchaseActionHistoryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
