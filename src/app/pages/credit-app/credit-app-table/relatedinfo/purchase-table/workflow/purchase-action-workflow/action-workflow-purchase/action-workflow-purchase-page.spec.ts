import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionWorkflowPurchasePage } from './action-workflow-purchase-page';

describe('DemoWorkflowComponent', () => {
  let component: ActionWorkflowPurchasePage;
  let fixture: ComponentFixture<ActionWorkflowPurchasePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ActionWorkflowPurchasePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionWorkflowPurchasePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
