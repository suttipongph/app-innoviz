import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { WorkFlowPurchaseActionHistoryPage } from './purchase-action-history/purchase-action-history.page';

const routes: Routes = [
  {
    path: '',
    component: WorkFlowPurchaseActionHistoryPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseActionHistoryRoutingModule {}
