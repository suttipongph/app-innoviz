import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ActionWorkflowPurchasePage } from './action-workflow-purchase/action-workflow-purchase-page';
import { StartWorkflowPurchasePage } from './start-workflow-purchase/start-workflow-purchase-page';

const routes: Routes = [
  {
    path: 'startworkflow',
    component: StartWorkflowPurchasePage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'actionworkflow',
    component: ActionWorkflowPurchasePage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseActionWorkflowRoutingModule {}
