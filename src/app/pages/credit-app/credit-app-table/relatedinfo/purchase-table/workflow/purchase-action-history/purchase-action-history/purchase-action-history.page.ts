import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';
const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';
@Component({
  selector: 'app-purchase-action-history',
  templateUrl: './purchase-action-history.page.html',
  styleUrls: ['./purchase-action-history.page.scss']
})
export class WorkFlowPurchaseActionHistoryPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.FACTORING_CREDIT_APPLICATION_TABLE,
      servicePath: this.setServicePath()
    };
  }

  setServicePath() {
    if (this.uiService.getRelatedInfoActiveTableName() == ROUTE_RELATED_GEN.PURCHASE_TABLE_PURCHASE) {
      return `CreditAppTable/Factoring/RelatedInfo/${ROUTE_RELATED_GEN.PURCHASE_TABLE_PURCHASE}/Workflow/ActionHistory`;
    } else {
      return `CreditAppTable/Factoring/RelatedInfo/${ROUTE_RELATED_GEN.PURCHASE_TABLE_ROLL_BILL}/Workflow/ActionHistory`;
    }
  }
}
