import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PurchaseTableListPage } from './purchase-table-list.page';

describe('PurchaseTableListPage', () => {
  let component: PurchaseTableListPage;
  let fixture: ComponentFixture<PurchaseTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PurchaseTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
