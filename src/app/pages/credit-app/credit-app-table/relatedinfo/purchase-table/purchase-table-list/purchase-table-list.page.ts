import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'purchase-table-list-page',
  templateUrl: './purchase-table-list.page.html',
  styleUrls: ['./purchase-table-list.page.scss']
})
export class PurchaseTableListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  parentId = this.uiService.getRelatedInfoParentTableKey();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'purchaseTableGUID';
    const columns: ColumnModel[] = this.getColumns();
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: this.activeRoute,
      servicePath: `CreditAppTable/Factoring/RelatedInfo/${this.activeRoute}`
    };
  }

  getColumns() {
    if (this.activeRoute == ROUTE_RELATED_GEN.PURCHASE_TABLE_PURCHASE) {
      return [
        {
          label: null,
          textKey: 'rollBill',
          type: ColumnType.MASTER,
          visibility: true,
          sorting: SortType.NONE,
          parentKey: 'false'
        }
      ];
    } else {
      return [
        {
          label: null,
          textKey: 'rollBill',
          type: ColumnType.MASTER,
          visibility: true,
          sorting: SortType.NONE,
          parentKey: 'true'
        }
      ];
    }
  }
}
