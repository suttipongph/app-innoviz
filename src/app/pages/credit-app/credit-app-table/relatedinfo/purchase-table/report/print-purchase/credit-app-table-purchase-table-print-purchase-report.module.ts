import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { PrintPurchaseComponentModule } from 'components/purchase/print-purchase/print-purchase.module';
import { PrintPurchasePage } from './print-purchase/print-purchase.page';
import { PrintPurchaseReportRoutingModule } from './credit-app-table-purchase-table-print-purchase-report-routing.module';

@NgModule({
  declarations: [PrintPurchasePage],
  imports: [CommonModule, SharedModule, PrintPurchaseReportRoutingModule, PrintPurchaseComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PrintPurchaseReportModule {}
