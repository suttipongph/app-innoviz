import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PrintPurchaseRollbillPage } from './print-purchase-rollbill/print-purchase-rollbill.page';

const routes: Routes = [
  {
    path: '',
    component: PrintPurchaseRollbillPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrintPurchaseRollbillReportRoutingModule {}
