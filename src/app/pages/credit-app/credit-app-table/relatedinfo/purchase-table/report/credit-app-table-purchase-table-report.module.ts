import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { CreditAppTablePurchaseReportRoutingModule } from './credit-app-table-purchase-table-report-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, CreditAppTablePurchaseReportRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTablePurchaseReportModule {}
