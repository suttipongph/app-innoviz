import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PrintPurchasePage } from './print-purchase/print-purchase.page';

const routes: Routes = [
  {
    path: '',
    component: PrintPurchasePage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrintPurchaseReportRoutingModule {}
