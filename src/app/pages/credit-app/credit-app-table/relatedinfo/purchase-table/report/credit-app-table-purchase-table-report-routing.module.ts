import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'printpurchase',
    loadChildren: () =>
      import('./print-purchase/credit-app-table-purchase-table-print-purchase-report.module').then((m) => m.PrintPurchaseReportModule)
  },
  {
    path: 'printpurchaserollbill',
    loadChildren: () =>
      import('./print-purchase-rollbill/credit-app-table-purchase-table-print-purchase-rollbill-report.module').then(
        (m) => m.PrintPurchaseRollbillReportModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTablePurchaseReportRoutingModule {}
