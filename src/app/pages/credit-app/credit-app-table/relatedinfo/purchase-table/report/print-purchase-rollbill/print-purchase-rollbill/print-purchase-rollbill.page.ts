import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_REPORT_GEN } from 'shared/constants/constantGen';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'print-purchase-page',
  templateUrl: './print-purchase-rollbill.page.html',
  styleUrls: ['./print-purchase-rollbill.page.scss']
})
export class PrintPurchaseRollbillPage implements OnInit {
  pageInfo: PageInformationModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_REPORT_GEN.PRINT_PURCHASE_ROLLBILL,
      servicePath: `CreditAppTable/Factoring/RelatedInfo/${this.activeRoute}/Report/printpurchaserollbill`
    };
  }
}
