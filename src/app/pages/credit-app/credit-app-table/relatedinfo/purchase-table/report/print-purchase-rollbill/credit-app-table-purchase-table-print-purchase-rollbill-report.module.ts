import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { PrintPurchaseRollbillComponentModule } from 'components/purchase/print-purchase-rollbill/print-purchase-rollbill.module';
import { PrintPurchaseRollbillPage } from './print-purchase-rollbill/print-purchase-rollbill.page';
import { PrintPurchaseRollbillReportRoutingModule } from './credit-app-table-purchase-table-print-purchase-rollbill-report-routing.module';

@NgModule({
  declarations: [PrintPurchaseRollbillPage],
  imports: [CommonModule, SharedModule, PrintPurchaseRollbillReportRoutingModule, PrintPurchaseRollbillComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PrintPurchaseRollbillReportModule {}
