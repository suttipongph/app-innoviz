import { MemoTransComponentModule } from './../../../../../../components/memo-trans/memo-trans/memo-trans.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { VerificationRelatedinfoRoutingModule } from './verification-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, VerificationRelatedinfoRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class VerificationRelatedinfoModule {}
