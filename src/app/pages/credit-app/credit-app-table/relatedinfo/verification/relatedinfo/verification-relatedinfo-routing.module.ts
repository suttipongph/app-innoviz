import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'memotrans',
    loadChildren: () => import('./memo/memo-relatedinfo.module').then((m) => m.MemoRelatedinfoModule)
  },
  {
    path: 'attachment',
    loadChildren:() => import('./attachment/verification-attachment.module').then((m) => m.VerificationTableAttachmentModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VerificationRelatedinfoRoutingModule {}
