import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'copyverification',
    loadChildren: () => import('./copy-verification/copy-verification-function.module').then((m) => m.CopyVerificationFunctionModule)
  },
  {
    path: 'updateverificationstatus',
    loadChildren: () =>
      import('./update-verification-status/update-verification-status-function.module').then((m) => m.UpdateVerificationStatusFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableVerificationFunctionRoutingModule {}
