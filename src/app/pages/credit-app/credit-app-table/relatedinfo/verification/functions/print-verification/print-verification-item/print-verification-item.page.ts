import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_REPORT_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'print-verification-item-page',
  templateUrl: './print-verification-item.page.html',
  styleUrls: ['./print-verification-item.page.scss']
})
export class PrintVerificationItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_REPORT_GEN.PRINT_VERIFICATION,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/VerificationTable/Function/printverification`
    };
  }
}
