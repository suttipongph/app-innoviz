import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppTableVerificationFunctionRoutingModule } from './credit-app-table-verification-function-routing.module';
import { SharedModule } from 'shared/shared.module';
@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, CreditAppTableVerificationFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTableVerificationFunctionModule {}
