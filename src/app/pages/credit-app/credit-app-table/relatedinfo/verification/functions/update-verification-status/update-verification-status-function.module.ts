import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateVerificationStatusFunctionRoutingModule } from './update-verification-status-function-routing.module';
import { SharedModule } from 'shared/shared.module';
import { UpdateVerificationStatusComponentModule } from 'components/verification/update-verification-status/update-verification-status.module';
import { UpdateVerificationStatusPage } from './update-verification-status/update-verification-status.page';

@NgModule({
  declarations: [UpdateVerificationStatusPage],
  imports: [CommonModule, SharedModule, UpdateVerificationStatusFunctionRoutingModule, UpdateVerificationStatusComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UpdateVerificationStatusFunctionModule {}
