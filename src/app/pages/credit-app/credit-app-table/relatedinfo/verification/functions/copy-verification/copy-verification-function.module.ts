import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CopyVerificationFunctionRoutingModule } from './copy-verification-function-routing.module';
import { SharedModule } from 'shared/shared.module';
import { CopyVerificationComponentModule } from 'components/verification/copy-verification/copy-verification.module';
import { CopyVerificationPage } from './copy-verification/copy-verification.page';

@NgModule({
  declarations: [CopyVerificationPage],
  imports: [CommonModule, SharedModule, CopyVerificationFunctionRoutingModule, CopyVerificationComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CopyVerificationFunctionModule {}
