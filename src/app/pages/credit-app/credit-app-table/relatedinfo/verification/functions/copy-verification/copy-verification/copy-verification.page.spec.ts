import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CopyVerificationPage } from './copy-verification.page';

describe('CopyVerificationPage', () => {
  let component: CopyVerificationPage;
  let fixture: ComponentFixture<CopyVerificationPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CopyVerificationPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyVerificationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
