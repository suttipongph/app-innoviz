import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PrintVerificationItemPage } from './print-verification-item/print-verification-item.page';
import { PrintVerificationListPage } from './print-verification-list/print-verification-list.page';

const routes: Routes = [
  {
    path: '',
    component: PrintVerificationListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: PrintVerificationItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrintVerificationRoutingModule {}
