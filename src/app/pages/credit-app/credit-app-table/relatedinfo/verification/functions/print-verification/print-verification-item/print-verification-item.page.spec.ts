import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PrintVerificationItemPage } from './print-verification-item.page';

describe('PrintVerificationItemPage', () => {
  let component: PrintVerificationItemPage;
  let fixture: ComponentFixture<PrintVerificationItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PrintVerificationItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintVerificationItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
