import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UpdateVerificationStatusPage } from './update-verification-status.page';

describe('UpdateVerificationStatusPage', () => {
  let component: UpdateVerificationStatusPage;
  let fixture: ComponentFixture<UpdateVerificationStatusPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UpdateVerificationStatusPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateVerificationStatusPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
