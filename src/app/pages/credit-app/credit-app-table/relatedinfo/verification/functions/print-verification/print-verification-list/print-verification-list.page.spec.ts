import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PrintVerificationListPage } from './print-verification-list.page';

describe('PrintVerificationListPage', () => {
  let component: PrintVerificationListPage;
  let fixture: ComponentFixture<PrintVerificationListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PrintVerificationListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintVerificationListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
