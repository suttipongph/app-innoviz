import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { UpdateVerificationStatusPage } from './update-verification-status/update-verification-status.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateVerificationStatusPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpdateVerificationStatusFunctionRoutingModule {}
