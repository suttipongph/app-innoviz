import { Component, OnInit } from '@angular/core';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel, PathParamModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-copy-verification',
  templateUrl: './copy-verification.page.html',
  styleUrls: ['./copy-verification.page.scss']
})
export class CopyVerificationPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {
    // super();
  }

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.VERIFICATION_TABLE_COPY_VERIFICATION,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/VerificationTable/Function/copyverification`
    };
    console.log('this.pageinfo ', this.pageInfo);
  }
}
