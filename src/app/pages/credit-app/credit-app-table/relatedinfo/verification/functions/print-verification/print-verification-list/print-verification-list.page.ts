import { Component, OnInit } from '@angular/core';
import { ROUTE_REPORT_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'print-verification-list-page',
  templateUrl: './print-verification-list.page.html',
  styleUrls: ['./print-verification-list.page.scss']
})
export class PrintVerificationListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'printVerificationGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_REPORT_GEN.PRINT_VERIFICATION, servicePath: 'CreditAppTable' };
  }
}
