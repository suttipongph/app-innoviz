import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrintVerificationRoutingModule } from './print-verification-routing.module';
import { PrintVerificationListPage } from './print-verification-list/print-verification-list.page';
import { PrintVerificationItemPage } from './print-verification-item/print-verification-item.page';
import { SharedModule } from 'shared/shared.module';
import { PrintVerificationComponentModule } from 'components/verification/print-verification/print-verification.module';

@NgModule({
  declarations: [PrintVerificationListPage, PrintVerificationItemPage],
  imports: [CommonModule, SharedModule, PrintVerificationRoutingModule, PrintVerificationComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PrintVerificationModule {}
