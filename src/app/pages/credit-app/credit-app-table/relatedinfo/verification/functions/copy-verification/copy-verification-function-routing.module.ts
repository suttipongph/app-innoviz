import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CopyVerificationPage } from './copy-verification/copy-verification.page';

const routes: Routes = [
  {
    path: '',
    component: CopyVerificationPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CopyVerificationFunctionRoutingModule {}
