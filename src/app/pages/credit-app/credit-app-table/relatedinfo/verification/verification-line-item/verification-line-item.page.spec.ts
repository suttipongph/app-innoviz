import { ComponentFixture, TestBed } from '@angular/core/testing';
import { VerificationlineItemPage } from './verification-line-item.page';

describe('VerificationItemPage', () => {
  let component: VerificationlineItemPage;
  let fixture: ComponentFixture<VerificationlineItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VerificationlineItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationlineItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
