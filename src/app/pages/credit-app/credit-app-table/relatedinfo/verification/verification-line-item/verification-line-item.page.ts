import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'verification-line-item.page',
  templateUrl: './verification-line-item.page.html',
  styleUrls: ['./verification-line-item.page.scss']
})
export class VerificationlineItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.VERIFICATION_LINE,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/VerificationTable`
      //childPaths: [{ pagePath: ROUTE_RELATED_GEN.VERIFICATION_LINE, servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/VerificationTable` }]
    };
  }
}
