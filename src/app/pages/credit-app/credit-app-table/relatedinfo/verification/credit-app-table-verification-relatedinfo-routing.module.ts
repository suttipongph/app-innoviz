import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VerificationListPage } from './verification-list/verification-list.page';
import { VerificationItemPage } from './verification-item/verification-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { VerificationlineItemPage } from './verification-line-item/verification-line-item.page';

const routes: Routes = [
  {
    path: '',
    component: VerificationListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: VerificationItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/verificationline-child/:id',
    component: VerificationlineItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/credit-app-table-verification-function.module').then((m) => m.CreditAppTableVerificationFunctionModule)
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/verification-relatedinfo-routing.module').then((m) => m.VerificationRelatedinfoRoutingModule)
  },
  {
    path: ':id/report',
    loadChildren: () => import('./report/credit-app-table-verification-report.module').then((m) => m.CreditAppTableVerificationReportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableVerificationRelatedinfoRoutingModule {}
