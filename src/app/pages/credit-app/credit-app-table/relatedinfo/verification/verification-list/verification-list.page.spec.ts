import { ComponentFixture, TestBed } from '@angular/core/testing';
import { VerificationListPage } from './verification-list.page';

describe('VerificationListPage', () => {
  let component: VerificationListPage;
  let fixture: ComponentFixture<VerificationListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VerificationListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
