import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PrintVerificationPage } from './print-verification.page';

describe('PrintVerificationItemPage', () => {
  let component: PrintVerificationPage;
  let fixture: ComponentFixture<PrintVerificationPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PrintVerificationPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintVerificationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
