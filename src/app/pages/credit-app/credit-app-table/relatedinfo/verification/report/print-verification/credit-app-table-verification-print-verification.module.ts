import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrintVerificationRoutingModule } from './credit-app-table-verification-print-verification-routing.module';
import { PrintVerificationPage } from './print-verification/print-verification.page';
import { SharedModule } from 'shared/shared.module';
import { PrintVerificationComponentModule } from 'components/verification/print-verification/print-verification.module';

@NgModule({
  declarations: [PrintVerificationPage],
  imports: [CommonModule, SharedModule, PrintVerificationRoutingModule, PrintVerificationComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PrintVerificationModule {}
