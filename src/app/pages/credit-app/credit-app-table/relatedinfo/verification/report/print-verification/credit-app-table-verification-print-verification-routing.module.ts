import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PrintVerificationPage } from './print-verification/print-verification.page';

const routes: Routes = [
  {
    path: '',
    component: PrintVerificationPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrintVerificationRoutingModule {}
