import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'printverification',
    loadChildren: () => import('./print-verification/credit-app-table-verification-print-verification.module').then((m) => m.PrintVerificationModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableVerificationReportRoutingModule {}
