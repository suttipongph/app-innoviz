import { ComponentFixture, TestBed } from '@angular/core/testing';
import { VerificationItemPage } from './verification-item.page';

describe('VerificationItemPage', () => {
  let component: VerificationItemPage;
  let fixture: ComponentFixture<VerificationItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VerificationItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
