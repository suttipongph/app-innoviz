import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VerificationListPage } from './verification-list/verification-list.page';
import { VerificationItemPage } from './verification-item/verification-item.page';
import { CreditAppTableVerificationRelatedinfoRoutingModule } from './credit-app-table-verification-relatedinfo-routing.module';
import { SharedModule } from 'shared/shared.module';
import { VerificationTableComponentModule } from 'components/verification/verification-table/verification-table.module';
import { VerificationLineComponentModule } from 'components/verification/verification-line/verification-line.module';
import { VerificationlineItemPage } from './verification-line-item/verification-line-item.page';
@NgModule({
  declarations: [VerificationListPage, VerificationItemPage, VerificationlineItemPage],
  imports: [
    CommonModule,
    SharedModule,
    CreditAppTableVerificationRelatedinfoRoutingModule,
    VerificationTableComponentModule,
    VerificationLineComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTableVerificationRelatedinfoModule {}
