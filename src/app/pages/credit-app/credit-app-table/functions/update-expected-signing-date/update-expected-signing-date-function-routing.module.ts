import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { UpdateExpectedSigningDatePage } from './update-expected-signing-date/update-expected-signing-date.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateExpectedSigningDatePage,
    canActivate:[AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableUpdateExpectedSigningDateFunctionRoutingModule {}
