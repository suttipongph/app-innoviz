import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppTableUpdateExpectedSigningDateFunctionRoutingModule } from './update-expected-signing-date-function-routing.module';
import { UpdateExpectedSigningDatePage } from './update-expected-signing-date/update-expected-signing-date.page';
import { SharedModule } from 'shared/shared.module';
import { UpdateExpectedAgmSignDateComponentModule } from 'components/credit-app-table/update-expected-agm-sign-date/update-expected-agm-sign-date.module';

@NgModule({
  declarations: [UpdateExpectedSigningDatePage],
  imports: [CommonModule, SharedModule, CreditAppTableUpdateExpectedSigningDateFunctionRoutingModule,UpdateExpectedAgmSignDateComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTableUpdateExpectedSigningDateFunctionModule {}
