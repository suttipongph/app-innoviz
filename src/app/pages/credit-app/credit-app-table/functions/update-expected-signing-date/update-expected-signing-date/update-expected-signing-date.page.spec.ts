import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UpdateExpectedSigningDatePage } from './update-expected-signing-date.page';

describe('UpdateExpectedSigningDatePage', () => {
  let component: UpdateExpectedSigningDatePage;
  let fixture: ComponentFixture<UpdateExpectedSigningDatePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UpdateExpectedSigningDatePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateExpectedSigningDatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
