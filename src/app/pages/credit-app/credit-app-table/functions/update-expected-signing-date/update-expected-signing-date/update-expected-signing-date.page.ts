import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-update-expected-signing-date',
  templateUrl: './update-expected-signing-date.page.html',
  styleUrls: ['./update-expected-signing-date.page.scss']
})
export class UpdateExpectedSigningDatePage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.UPDATE_EXPECTED_SIGNING_DATE,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/Function/UpdateExpectedSigningDate`
    };
  }
}
