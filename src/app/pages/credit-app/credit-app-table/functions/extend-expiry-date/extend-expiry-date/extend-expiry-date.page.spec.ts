import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ExtendExpiryDatePage } from './extend-expiry-date.page';

describe('ExtendExpiryDatePage', () => {
  let component: ExtendExpiryDatePage;
  let fixture: ComponentFixture<ExtendExpiryDatePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExtendExpiryDatePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtendExpiryDatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
