import { Component, OnInit } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { UIControllerService } from 'core/services/uiController.service';
import { getProductType } from 'shared/functions/value.function';

@Component({
  selector: 'app-extend-expiry-date',
  templateUrl: './extend-expiry-date.page.html',
  styleUrls: ['./extend-expiry-date.page.scss']
})
export class ExtendExpiryDatePage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.EXTEND_EXPIRY_DATE,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/Function/ExtendExpiryDate`
    };
  }
}
