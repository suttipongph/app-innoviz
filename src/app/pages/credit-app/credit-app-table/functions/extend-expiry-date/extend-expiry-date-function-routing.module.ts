import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ExtendExpiryDatePage } from './extend-expiry-date/extend-expiry-date.page';

const routes: Routes = [
  {
    path: '',
    component: ExtendExpiryDatePage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableExtendExpiryDateFunctionRoutingModule {}
