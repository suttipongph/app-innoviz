import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppTableExtendExpiryDateFunctionRoutingModule } from './extend-expiry-date-function-routing.module';
import { ExtendExpiryDatePage } from './extend-expiry-date/extend-expiry-date.page';
import { SharedModule } from 'shared/shared.module';
import { ExtendExpiryDateComponentModule } from 'components/credit-app-table/extend-expiry-date/extend-expiry-date.module';

@NgModule({
  declarations: [ExtendExpiryDatePage],
  imports: [CommonModule, SharedModule, CreditAppTableExtendExpiryDateFunctionRoutingModule, ExtendExpiryDateComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTableExtendExpiryDateFunctionModule {}
