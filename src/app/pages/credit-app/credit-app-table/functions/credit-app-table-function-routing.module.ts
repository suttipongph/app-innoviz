import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'extendexpirydate',
    loadChildren: () => import('./extend-expiry-date/extend-expiry-date-function.module').then((m) => m.CreditAppTableExtendExpiryDateFunctionModule)
  },
  {
    path: 'updateexpectedsigningdate',
    loadChildren: () => import('./update-expected-signing-date/update-expected-signing-date-function.module').then((m) => m.CreditAppTableUpdateExpectedSigningDateFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableFunctionRoutingModule {}
