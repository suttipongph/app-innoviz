import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'credit-app-table-item-page',
  templateUrl: './credit-app-table-item.page.html',
  styleUrls: ['./credit-app-table-item.page.scss']
})
export class CreditAppTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    const producTyprWording = getProductType(this.masterRoute, true);
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.FACTORING_CREDIT_APPLICATION_TABLE,
      servicePath: `CreditAppTable/${producTyprWording}`,
      childPaths: [
        {
          pagePath: ROUTE_MASTER_GEN.CREDIT_APP_LINE,
          servicePath: `CreditAppTable/${producTyprWording}`
        }
      ]
    };
  }
}
