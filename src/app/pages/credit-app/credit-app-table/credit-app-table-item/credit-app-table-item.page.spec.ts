import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditAppTableItemPage } from './credit-app-table-item.page';

describe('CreditAppTableItemPage', () => {
  let component: CreditAppTableItemPage;
  let fixture: ComponentFixture<CreditAppTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditAppTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
