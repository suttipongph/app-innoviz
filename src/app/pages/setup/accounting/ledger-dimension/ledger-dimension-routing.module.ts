import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { LedgerDimensionItemPage } from './ledger-dimension-item/ledger-dimension-item.page';
import { LedgerDimensionListPage } from './ledger-dimension-list/ledger-dimension.page';

const routes: Routes = [
  {
    path: '',
    component: LedgerDimensionListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: LedgerDimensionItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LedgerDimensionRoutingModule {}
