import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LedgerDimensionRoutingModule } from './ledger-dimension-routing.module';
import { LedgerDimensionItemPage } from './ledger-dimension-item/ledger-dimension-item.page';
import { SharedModule } from 'shared/shared.module';
import { LedgerDimensionListPage } from './ledger-dimension-list/ledger-dimension.page';
import { LedgerDimensionComponentModule } from 'components/setup/ledger-dimension/ledger-dimension.module';

@NgModule({
  declarations: [LedgerDimensionListPage, LedgerDimensionItemPage],
  imports: [CommonModule, SharedModule, LedgerDimensionRoutingModule, LedgerDimensionComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LedgerDimensionModule {}
