import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'ledger-dimension-item.page',
  templateUrl: './ledger-dimension-item.page.html',
  styleUrls: ['./ledger-dimension-item.page.scss']
})
export class LedgerDimensionItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.LEDGERDIMENSION, servicePath: 'LedgerDimension' };
  }
}
