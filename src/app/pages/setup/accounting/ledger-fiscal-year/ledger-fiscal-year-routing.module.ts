import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { LedgerFiscalPeriodItemPage } from './ledger-fiscal-period-item/ledger-fiscal-period-item.page';
import { LedgerFiscalYearItemPage } from './ledger-fiscal-year-item/ledger-fiscal-year-item.page';
import { LedgerFiscalYearListPage } from './ledger-fiscal-year-list/ledger-fiscal-year.page';

const routes: Routes = [
  {
    path: '',
    component: LedgerFiscalYearListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: LedgerFiscalYearItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/ledgerfiscalperiod-child/:id',
    component: LedgerFiscalPeriodItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LedgerFiscalYearRoutingModule {}
