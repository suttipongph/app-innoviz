import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'ledger-fiscal-year-item.page',
  templateUrl: './ledger-fiscal-year-item.page.html',
  styleUrls: ['./ledger-fiscal-year-item.page.scss']
})
export class LedgerFiscalYearItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.LEDGERFISCALYEAR,
      servicePath: 'LedgerFiscalYear',
      childPaths: [{ pagePath: ROUTE_MASTER_GEN.LEDGERFISCALPERIOD, servicePath: 'LedgerFiscalYear' }]
    };
  }
}
