import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'ledger-fiscal-period-item.page',
  templateUrl: './ledger-fiscal-period-item.page.html',
  styleUrls: ['./ledger-fiscal-period-item.page.scss']
})
export class LedgerFiscalPeriodItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.LEDGERFISCALPERIOD, servicePath: 'LedgerFiscalYear' };
  }
}
