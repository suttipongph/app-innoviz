import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'ledger-fiscal-year-page',
  templateUrl: './ledger-fiscal-year.page.html',
  styleUrls: ['./ledger-fiscal-year.page.scss']
})
export class LedgerFiscalYearListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'ledgerFiscalYearGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.LEDGERFISCALYEAR,
      servicePath: 'LedgerFiscalYear'
    };
  }
}
