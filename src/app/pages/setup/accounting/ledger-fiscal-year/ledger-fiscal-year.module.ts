import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LedgerFiscalYearRoutingModule } from './ledger-fiscal-year-routing.module';
import { LedgerFiscalYearItemPage } from './ledger-fiscal-year-item/ledger-fiscal-year-item.page';
import { SharedModule } from 'shared/shared.module';
import { LedgerFiscalYearListPage } from './ledger-fiscal-year-list/ledger-fiscal-year.page';
import { LedgerFiscalYearComponentModule } from 'components/setup/ledger-fiscal-year/ledger-fiscal-year.module';
import { LedgerFiscalPeriodItemPage } from './ledger-fiscal-period-item/ledger-fiscal-period-item.page';
import { LedgerFiscalPeriodComponentModule } from 'components/setup/ledger-fiscal-period/ledger-fiscal-period.module';

@NgModule({
  declarations: [LedgerFiscalYearListPage, LedgerFiscalYearItemPage, LedgerFiscalPeriodItemPage],
  imports: [CommonModule, SharedModule, LedgerFiscalYearRoutingModule, LedgerFiscalYearComponentModule, LedgerFiscalPeriodComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LedgerFiscalYearModule {}
