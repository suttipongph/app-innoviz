import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [
  { path: 'ledgerdimension', loadChildren: () => import('./ledger-dimension/ledger-dimension.module').then((m) => m.LedgerDimensionModule) },
  { path: 'ledgerfiscalyear', loadChildren: () => import('./ledger-fiscal-year/ledger-fiscal-year.module').then((m) => m.LedgerFiscalYearModule) },
  { path: 'stagingtranstext', loadChildren: () => import('./staging-trans-text/staging-trans-text.module').then((m) => m.StagingTransTextModule) }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountingRoutingModule {}
