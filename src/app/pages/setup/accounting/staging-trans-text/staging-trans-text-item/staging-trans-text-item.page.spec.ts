import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StagingTransTextItemPage } from './staging-trans-text-item.page';

describe('StagingTransTextItemPage', () => {
  let component: StagingTransTextItemPage;
  let fixture: ComponentFixture<StagingTransTextItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StagingTransTextItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StagingTransTextItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
