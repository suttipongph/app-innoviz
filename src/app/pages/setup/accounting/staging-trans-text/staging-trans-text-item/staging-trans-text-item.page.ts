import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'staging-trans-text-item-page',
  templateUrl: './staging-trans-text-item.page.html',
  styleUrls: ['./staging-trans-text-item.page.scss']
})
export class StagingTransTextItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.STAGING_TRANS_TEXT, servicePath: 'StagingTransText' };
  }
}
