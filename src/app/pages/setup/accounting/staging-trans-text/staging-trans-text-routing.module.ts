import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { StagingTransTextItemPage } from './staging-trans-text-item/staging-trans-text-item.page';
import { StagingTransTextListPage } from './staging-trans-text-list/staging-trans-text-list.page';

const routes: Routes = [
  {
    path: '',
    component: StagingTransTextListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: StagingTransTextItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StagingTransTextRoutingModule {}
