import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StagingTransTextRoutingModule } from './staging-trans-text-routing.module';
import { StagingTransTextListPage } from './staging-trans-text-list/staging-trans-text-list.page';
import { StagingTransTextItemPage } from './staging-trans-text-item/staging-trans-text-item.page';
import { SharedModule } from 'shared/shared.module';
import { StagingTransTextComponentModule } from 'components/setup/staging-trans-text/staging-trans-text.module';

@NgModule({
  declarations: [StagingTransTextListPage, StagingTransTextItemPage],
  imports: [CommonModule, SharedModule, StagingTransTextRoutingModule, StagingTransTextComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StagingTransTextModule {}
