import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StagingTransTextListPage } from './staging-trans-text-list.page';

describe('StagingTransTextListPage', () => {
  let component: StagingTransTextListPage;
  let fixture: ComponentFixture<StagingTransTextListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StagingTransTextListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StagingTransTextListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
