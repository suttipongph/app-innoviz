import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditScoringRoutingModule } from './credit-scoring-routing.module';
import { CreditScoringListPage } from './credit-scoring-list/credit-scoring-list.page';
import { CreditScoringItemPage } from './credit-scoring-item/credit-scoring-item.page';
import { SharedModule } from 'shared/shared.module';
import { CreditScoringComponentModule } from 'components/setup/credit-scoring/credit-scoring.module';

@NgModule({
  declarations: [CreditScoringListPage, CreditScoringItemPage],
  imports: [CommonModule, SharedModule, CreditScoringRoutingModule, CreditScoringComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditScoringModule {}
