import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditScoringListPage } from './credit-scoring-list.page';

describe('CreditScoringListPage', () => {
  let component: CreditScoringListPage;
  let fixture: ComponentFixture<CreditScoringListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditScoringListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditScoringListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
