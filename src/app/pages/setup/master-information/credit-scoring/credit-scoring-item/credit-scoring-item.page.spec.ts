import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditScoringItemPage } from './credit-scoring-item.page';

describe('CreditScoringItemPage', () => {
  let component: CreditScoringItemPage;
  let fixture: ComponentFixture<CreditScoringItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditScoringItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditScoringItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
