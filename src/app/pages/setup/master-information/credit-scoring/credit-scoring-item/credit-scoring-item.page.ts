import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'credit-scoring-item-page',
  templateUrl: './credit-scoring-item.page.html',
  styleUrls: ['./credit-scoring-item.page.scss']
})
export class CreditScoringItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.CREDIT_SCORING, servicePath: 'CreditScoring' };
  }
}
