import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CreditScoringItemPage } from './credit-scoring-item/credit-scoring-item.page';
import { CreditScoringListPage } from './credit-scoring-list/credit-scoring-list.page';

const routes: Routes = [
  {
    path: '',
    component: CreditScoringListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CreditScoringItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditScoringRoutingModule {}
