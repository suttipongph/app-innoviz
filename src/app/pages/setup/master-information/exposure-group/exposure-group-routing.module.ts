import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ExposureGroupByProductItemPage } from './exposure-group-by-product-item/exposure-group-by-product-item.page';
import { ExposureGroupItemPage } from './exposure-group-item/exposure-group-item.page';
import { ExposureGroupListPage } from './exposure-group-list/exposure-group-list.page';

const routes: Routes = [
  {
    path: '',
    component: ExposureGroupListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ExposureGroupItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/exposuregroupbyproduct-child/:id',
    component: ExposureGroupByProductItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExposureGroupRoutingModule {}
