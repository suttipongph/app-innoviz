import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ExposureGroupListPage } from './exposure-group-list.page';

describe('ExposureGroupListPage', () => {
  let component: ExposureGroupListPage;
  let fixture: ComponentFixture<ExposureGroupListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExposureGroupListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExposureGroupListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
