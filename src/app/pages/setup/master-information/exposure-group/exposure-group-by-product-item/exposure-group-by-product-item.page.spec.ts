import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ExposureGroupByProductItemPage } from './exposure-group-by-product-item.page';

describe('ExposureGroupByProductItemPage', () => {
  let component: ExposureGroupByProductItemPage;
  let fixture: ComponentFixture<ExposureGroupByProductItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExposureGroupByProductItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExposureGroupByProductItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
