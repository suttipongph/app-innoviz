import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'exposure-group-by-product-item.page',
  templateUrl: './exposure-group-by-product-item.page.html',
  styleUrls: ['./exposure-group-by-product-item.page.scss']
})
export class ExposureGroupByProductItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.EXPOSURE_GROUP_BY_PRODUCT, servicePath: 'ExposureGroup' };
  }
}
