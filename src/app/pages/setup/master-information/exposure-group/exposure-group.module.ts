import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExposureGroupRoutingModule } from './exposure-group-routing.module';
import { ExposureGroupListPage } from './exposure-group-list/exposure-group-list.page';
import { ExposureGroupItemPage } from './exposure-group-item/exposure-group-item.page';
import { SharedModule } from 'shared/shared.module';
import { ExposureGroupComponentModule } from 'components/setup/exposure-group/exposure-group.module';
import { ExposureGroupByProductItemPage } from './exposure-group-by-product-item/exposure-group-by-product-item.page';
import { ExposureGroupByProductComponentModule } from 'components/setup/exposure-group-by-product/exposure-group-by-product.module';

@NgModule({
  declarations: [ExposureGroupListPage, ExposureGroupItemPage, ExposureGroupByProductItemPage],
  imports: [CommonModule, SharedModule, ExposureGroupRoutingModule, ExposureGroupComponentModule, ExposureGroupByProductComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExposureGroupModule {}
