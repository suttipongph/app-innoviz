import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ExposureGroupItemPage } from './exposure-group-item.page';

describe('ExposureGroupItemPage', () => {
  let component: ExposureGroupItemPage;
  let fixture: ComponentFixture<ExposureGroupItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExposureGroupItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExposureGroupItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
