import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'exposure-group-item-page',
  templateUrl: './exposure-group-item.page.html',
  styleUrls: ['./exposure-group-item.page.scss']
})
export class ExposureGroupItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.EXPOSURE_GROUP,
      servicePath: 'ExposureGroup',
      childPaths: [{ pagePath: ROUTE_MASTER_GEN.EXPOSURE_GROUP_BY_PRODUCT, servicePath: 'ExposureGroup' }]
    };
  }
}
