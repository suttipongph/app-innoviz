import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessSegmentRoutingModule } from './business-segment-routing.module';
import { BusinessSegmentListPage } from './business-segment-list/business-segment-list.page';
import { BusinessSegmentItemPage } from './business-segment-item/business-segment-item.page';
import { SharedModule } from 'shared/shared.module';
import { BusinessSegmentComponentModule } from 'components/setup/business-segment/business-segment.module';

@NgModule({
  declarations: [BusinessSegmentListPage, BusinessSegmentItemPage],
  imports: [CommonModule, SharedModule, BusinessSegmentRoutingModule, BusinessSegmentComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusinessSegmentModule {}
