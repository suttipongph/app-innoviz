import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessSegmentItemPage } from './business-segment-item.page';

describe('BusinessSegmentItemPage', () => {
  let component: BusinessSegmentItemPage;
  let fixture: ComponentFixture<BusinessSegmentItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BusinessSegmentItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessSegmentItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
