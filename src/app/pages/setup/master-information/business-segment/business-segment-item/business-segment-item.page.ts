import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'business-segment-item-page',
  templateUrl: './business-segment-item.page.html',
  styleUrls: ['./business-segment-item.page.scss']
})
export class BusinessSegmentItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.BUSINESS_SEGMENT, servicePath: 'BusinessSegment' };
  }
}
