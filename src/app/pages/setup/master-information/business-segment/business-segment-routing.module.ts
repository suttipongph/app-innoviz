import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { BusinessSegmentItemPage } from './business-segment-item/business-segment-item.page';
import { BusinessSegmentListPage } from './business-segment-list/business-segment-list.page';

const routes: Routes = [
  {
    path: '',
    component: BusinessSegmentListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BusinessSegmentItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessSegmentRoutingModule {}
