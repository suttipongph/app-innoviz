import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessSegmentListPage } from './business-segment-list.page';

describe('BusinessSegmentListPage', () => {
  let component: BusinessSegmentListPage;
  let fixture: ComponentFixture<BusinessSegmentListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BusinessSegmentListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessSegmentListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
