import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { KYCSetupItemPage } from './kyc-setup-item/kyc-setup-item.page';
import { KYCSetupListPage } from './kyc-setup-list/kyc-setup-list.page';

const routes: Routes = [
  {
    path: '',
    component: KYCSetupListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: KYCSetupItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KYCSetupRoutingModule {}
