import { ComponentFixture, TestBed } from '@angular/core/testing';
import { KYCSetupListPage } from './kyc-setup-list.page';

describe('KYCSetupListPage', () => {
  let component: KYCSetupListPage;
  let fixture: ComponentFixture<KYCSetupListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [KYCSetupListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KYCSetupListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
