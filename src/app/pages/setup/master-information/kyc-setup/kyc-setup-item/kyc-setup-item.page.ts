import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'kyc-setup-item-page',
  templateUrl: './kyc-setup-item.page.html',
  styleUrls: ['./kyc-setup-item.page.scss']
})
export class KYCSetupItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.KYC_SETUP, servicePath: 'KYCSetup' };
  }
}
