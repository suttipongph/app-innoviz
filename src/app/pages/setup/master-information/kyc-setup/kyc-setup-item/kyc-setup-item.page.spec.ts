import { ComponentFixture, TestBed } from '@angular/core/testing';
import { KYCSetupItemPage } from './kyc-setup-item.page';

describe('KYCSetupItemPage', () => {
  let component: KYCSetupItemPage;
  let fixture: ComponentFixture<KYCSetupItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [KYCSetupItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KYCSetupItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
