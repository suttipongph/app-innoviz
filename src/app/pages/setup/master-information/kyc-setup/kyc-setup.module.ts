import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KYCSetupRoutingModule } from './kyc-setup-routing.module';
import { KYCSetupListPage } from './kyc-setup-list/kyc-setup-list.page';
import { KYCSetupItemPage } from './kyc-setup-item/kyc-setup-item.page';
import { SharedModule } from 'shared/shared.module';
import { KYCSetupComponentModule } from 'components/setup/kyc-setup/kyc-setup.module';

@NgModule({
  declarations: [KYCSetupListPage, KYCSetupItemPage],
  imports: [CommonModule, SharedModule, KYCSetupRoutingModule, KYCSetupComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class KYCSetupModule {}
