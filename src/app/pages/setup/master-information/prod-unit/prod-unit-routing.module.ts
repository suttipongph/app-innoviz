import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ProdUnitItemPage } from './prod-unit-item/prod-unit-item.page';
import { ProdUnitListPage } from './prod-unit-list/prod-unit.page';

const routes: Routes = [
  {
    path: '',
    component: ProdUnitListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ProdUnitItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProdUnitRoutingModule {}
