import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'prod-unit-item.page',
  templateUrl: './prod-unit-item.page.html',
  styleUrls: ['./prod-unit-item.page.scss']
})
export class ProdUnitItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.PRODUNIT, servicePath: 'ProdUnit' };
  }
}
