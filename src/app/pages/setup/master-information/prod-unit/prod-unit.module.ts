import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProdUnitRoutingModule } from './prod-unit-routing.module';
import { ProdUnitItemPage } from './prod-unit-item/prod-unit-item.page';
import { SharedModule } from 'shared/shared.module';
import { ProdUnitComponentModule } from 'components/setup/prod-unit/prod-unit.module';
import { ProdUnitListPage } from './prod-unit-list/prod-unit.page';

@NgModule({
  declarations: [ProdUnitListPage, ProdUnitItemPage],
  imports: [CommonModule, SharedModule, ProdUnitRoutingModule, ProdUnitComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProdUnitModule {}
