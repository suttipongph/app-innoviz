import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BankTypeListPage } from './bank-type-list.page';

describe('BankTypeListPage', () => {
  let component: BankTypeListPage;
  let fixture: ComponentFixture<BankTypeListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BankTypeListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BankTypeListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
