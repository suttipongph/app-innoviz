import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BankTypeRoutingModule } from './bank-type-routing.module';
import { BankTypeListPage } from './bank-type-list/bank-type-list.page';
import { BankTypeItemPage } from './bank-type-item/bank-type-item.page';
import { SharedModule } from 'shared/shared.module';
import { BankTypeComponentModule } from 'components/setup/bank-type/bank-type.module';

@NgModule({
  declarations: [BankTypeListPage, BankTypeItemPage],
  imports: [CommonModule, SharedModule, BankTypeRoutingModule, BankTypeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BankTypeModule {}
