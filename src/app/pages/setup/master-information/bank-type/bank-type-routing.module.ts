import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { BankTypeItemPage } from './bank-type-item/bank-type-item.page';
import { BankTypeListPage } from './bank-type-list/bank-type-list.page';

const routes: Routes = [
  {
    path: '',
    component: BankTypeListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BankTypeItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BankTypeRoutingModule {}
