import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'bank-type-item-page',
  templateUrl: './bank-type-item.page.html',
  styleUrls: ['./bank-type-item.page.scss']
})
export class BankTypeItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.BANK_TYPE, servicePath: 'BankType' };
  }
}
