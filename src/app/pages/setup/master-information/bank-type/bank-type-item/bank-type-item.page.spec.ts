import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BankTypeItemPage } from './bank-type-item.page';

describe('BankTypeItemPage', () => {
  let component: BankTypeItemPage;
  let fixture: ComponentFixture<BankTypeItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BankTypeItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BankTypeItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
