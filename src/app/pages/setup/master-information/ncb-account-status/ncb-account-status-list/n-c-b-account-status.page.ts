import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'n-c-b-account-status-page',
  templateUrl: './n-c-b-account-status.page.html',
  styleUrls: ['./n-c-b-account-status.page.scss']
})
export class NCBAccountStatusListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.NCBACCOUNTSTATUS, servicePath: 'NCBAccountStatus' };
  }
}
