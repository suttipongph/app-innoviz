import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NCBAccountStatusRoutingModule } from './n-c-b-account-status-routing.module';
import { SharedModule } from 'shared/shared.module';
import { NCBAccountStatusItemPage } from './ncb-account-status-item/n-c-b-account-status-item.page';
import { NCBAccountStatusListPage } from './ncb-account-status-list/n-c-b-account-status.page';
import { NCBAccountStatusComponentModule } from 'components/setup/ncb-account-status/n-c-b-account-status.module';

@NgModule({
  declarations: [NCBAccountStatusListPage, NCBAccountStatusItemPage],
  imports: [CommonModule, SharedModule, NCBAccountStatusRoutingModule, NCBAccountStatusComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NCBAccountStatusModule {}
