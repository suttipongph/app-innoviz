import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'n-c-b-account-status-item.page',
  templateUrl: './n-c-b-account-status-item.page.html',
  styleUrls: ['./n-c-b-account-status-item.page.scss']
})
export class NCBAccountStatusItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.NCBACCOUNTSTATUS, servicePath: 'NCBAccountStatus' };
  }
}
