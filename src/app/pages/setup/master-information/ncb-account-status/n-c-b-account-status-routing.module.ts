import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { NCBAccountStatusItemPage } from './ncb-account-status-item/n-c-b-account-status-item.page';
import { NCBAccountStatusListPage } from './ncb-account-status-list/n-c-b-account-status.page';

const routes: Routes = [
  {
    path: '',
    component: NCBAccountStatusListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: NCBAccountStatusItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NCBAccountStatusRoutingModule {}
