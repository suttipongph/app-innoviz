import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenderRoutingModule } from './gender-routing.module';
import { GenderItemPage } from './gender-item/gender-item.page';
import { SharedModule } from 'shared/shared.module';
import { GenderListPage } from './gender-list/gender.page';
import { GenderComponentModule } from 'components/setup/gender/gender.module';

@NgModule({
  declarations: [GenderListPage, GenderItemPage],
  imports: [CommonModule, SharedModule, GenderRoutingModule, GenderComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GenderModule {}
