import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GenderItemPage } from './gender-item/gender-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { GenderListPage } from './gender-list/gender.page';

const routes: Routes = [
  {
    path: '',
    component: GenderListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: GenderItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GenderRoutingModule {}
