import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RaceListPage } from './race-list.page';

describe('RaceListPage', () => {
  let component: RaceListPage;
  let fixture: ComponentFixture<RaceListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RaceListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RaceListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
