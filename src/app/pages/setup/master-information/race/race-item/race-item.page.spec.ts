import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RaceItemPage } from './race-item.page';

describe('RaceItemPage', () => {
  let component: RaceItemPage;
  let fixture: ComponentFixture<RaceItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RaceItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RaceItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
