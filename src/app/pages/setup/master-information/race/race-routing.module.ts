import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { RaceItemPage } from './race-item/race-item.page';
import { RaceListPage } from './race-list/race-list.page';

const routes: Routes = [
  {
    path: '',
    component: RaceListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: RaceItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RaceRoutingModule {}
