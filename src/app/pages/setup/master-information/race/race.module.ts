import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RaceRoutingModule } from './race-routing.module';
import { RaceListPage } from './race-list/race-list.page';
import { RaceItemPage } from './race-item/race-item.page';
import { SharedModule } from 'shared/shared.module';
import { RaceComponentModule } from 'components/setup/race/race.module';

@NgModule({
  declarations: [RaceListPage, RaceItemPage],
  imports: [CommonModule, SharedModule, RaceRoutingModule, RaceComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RaceModule {}
