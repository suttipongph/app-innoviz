import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'authorizedpersontype',
    loadChildren: () => import('./authorized-person-type/authorized-person-type.module').then((m) => m.AuthorizedPersonTypeModule)
  },
  { path: 'banktype', loadChildren: () => import('./bank-type/bank-type.module').then((m) => m.BankTypeModule) },
  {
    path: 'banktype',
    loadChildren: () => import('./bank-type/bank-type.module').then((m) => m.BankTypeModule)
  },
  {
    path: 'businesscollateralstatus',
    loadChildren: () => import('./business-collateral-status/business-collateral-status.module').then((m) => m.BusinessCollateralStatusModule)
  },
  {
    path: 'businesscollateraltype',
    loadChildren: () => import('./business-collateral-type/business-collateral-type.module').then((m) => m.BusinessCollateralTypeModule)
  },
  {
    path: 'businesssegment',
    loadChildren: () => import('./business-segment/business-segment.module').then((m) => m.BusinessSegmentModule)
  },
  {
    path: 'businesssize',
    loadChildren: () => import('./business-size/business-size.module').then((m) => m.BusinessSizeModule)
  },
  {
    path: 'businesstype',
    loadChildren: () => import('./business-type/business-type.module').then((m) => m.BusinessTypeModule)
  },
  {
    path: 'creditscoring',
    loadChildren: () => import('./credit-scoring/credit-scoring.module').then((m) => m.CreditScoringModule)
  },
  {
    path: 'custgroup',
    loadChildren: () => import('./cust-group/cust-group.module').then((m) => m.CustGroupModule)
  },
  {
    path: 'kycsetup',
    loadChildren: () => import('./kyc-setup/kyc-setup.module').then((m) => m.KYCSetupModule)
  },
  {
    path: 'blackliststatus',
    loadChildren: () => import('./blacklist-status/blacklist-status.module').then((m) => m.BlacklistStatusModule)
  },
  {
    path: 'gradeclassification',
    loadChildren: () => import('./grade-classification/grade-classification.module').then((m) => m.GradeClassificationModule)
  },
  { path: 'guarantortype', loadChildren: () => import('./guarantor-type/guarantor-type.module').then((m) => m.GuarantorTypeModule) },
  {
    path: 'registrationtype',
    loadChildren: () => import('./registration-type/registration-type.module').then((m) => m.RegistrationTypeModule)
  },
  {
    path: 'race',
    loadChildren: () => import('./race/race.module').then((m) => m.RaceModule)
  },
  {
    path: 'nationality',
    loadChildren: () => import('./nationality/nationality.module').then((m) => m.NationalityModule)
  },
  {
    path: 'propertytype',
    loadChildren: () => import('./property-type/property-type.module').then((m) => m.PropertyTypeModule)
  },
  {
    path: 'vendgroup',
    loadChildren: () => import('./vend-group/vend-group.module').then((m) => m.VendGroupModule)
  },
  {
    path: 'maritalstatus',
    loadChildren: () => import('./marital-status/marital-status.module').then((m) => m.MaritalStatusModule)
  },
  {
    path: 'gender',
    loadChildren: () => import('./gender/gender.module').then((m) => m.GenderModule)
  },
  {
    path: 'occupation',
    loadChildren: () => import('./occupation/occupation.module').then((m) => m.OccupationModule)
  },
  {
    path: 'documenttype',
    loadChildren: () => import('./document-type/document-type.module').then((m) => m.DocumentTypeModule)
  },
  {
    path: 'lineofbusiness',
    loadChildren: () => import('./line-of-business/line-of-business.module').then((m) => m.LineOfBusinessModule)
  },
  {
    path: 'introducedby',
    loadChildren: () => import('./introduced-by/introduced-by.module').then((m) => m.IntroducedByModule)
  },
  {
    path: 'ncbaccountstatus',
    loadChildren: () => import('./ncb-account-status/n-c-b-account-status.module').then((m) => m.NCBAccountStatusModule)
  },
  {
    path: 'ownership',
    loadChildren: () => import('./ownership/ownership.module').then((m) => m.OwnershipModule)
  },
  {
    path: 'parentcompany',
    loadChildren: () => import('./parent-company/parent-company.module').then((m) => m.ParentCompanyModule)
  },
  {
    path: 'exposuregroup',
    loadChildren: () => import('./exposure-group/exposure-group.module').then((m) => m.ExposureGroupModule)
  },
  {
    path: 'productunit',
    loadChildren: () => import('./prod-unit/prod-unit.module').then((m) => m.ProdUnitModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MasterInformationRoutingModule {}
