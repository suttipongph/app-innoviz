import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OwnershipRoutingModule } from './ownership-routing.module';
import { OwnershipListPage } from './ownership-list/ownership-list.page';
import { OwnershipItemPage } from './ownership-item/ownership-item.page';
import { SharedModule } from 'shared/shared.module';
import { OwnershipComponentModule } from 'components/setup/ownership/ownership.module';

@NgModule({
  declarations: [OwnershipListPage, OwnershipItemPage],
  imports: [CommonModule, SharedModule, OwnershipRoutingModule, OwnershipComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OwnershipModule {}
