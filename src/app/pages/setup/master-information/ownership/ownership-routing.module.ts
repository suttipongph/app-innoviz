import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OwnershipListPage } from './ownership-list/ownership-list.page';
import { OwnershipItemPage } from './ownership-item/ownership-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: OwnershipListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: OwnershipItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OwnershipRoutingModule {}
