import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RegistrationTypeListPage } from './registration-type-list.page';

describe('RegistrationTypeListPage', () => {
  let component: RegistrationTypeListPage;
  let fixture: ComponentFixture<RegistrationTypeListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegistrationTypeListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationTypeListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
