import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { RegistrationTypeItemPage } from './registration-type-item/registration-type-item.page';
import { RegistrationTypeListPage } from './registration-type-list/registration-type-list.page';

const routes: Routes = [
  {
    path: '',
    component: RegistrationTypeListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: RegistrationTypeItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistrationTypeRoutingModule {}
