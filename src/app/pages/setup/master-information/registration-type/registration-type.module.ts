import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrationTypeRoutingModule } from './registration-type-routing.module';
import { RegistrationTypeListPage } from './registration-type-list/registration-type-list.page';
import { RegistrationTypeItemPage } from './registration-type-item/registration-type-item.page';
import { SharedModule } from 'shared/shared.module';
import { RegistrationTypeComponentModule } from 'components/setup/registration-type/registration-type.module';

@NgModule({
  declarations: [RegistrationTypeListPage, RegistrationTypeItemPage],
  imports: [CommonModule, SharedModule, RegistrationTypeRoutingModule, RegistrationTypeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RegistrationTypeModule {}
