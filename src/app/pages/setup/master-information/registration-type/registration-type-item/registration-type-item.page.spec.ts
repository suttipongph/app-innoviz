import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RegistrationTypeItemPage } from './registration-type-item.page';

describe('RegistrationTypeItemPage', () => {
  let component: RegistrationTypeItemPage;
  let fixture: ComponentFixture<RegistrationTypeItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegistrationTypeItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationTypeItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
