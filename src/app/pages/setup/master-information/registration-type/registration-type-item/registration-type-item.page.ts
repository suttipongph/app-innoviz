import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'registration-type-item-page',
  templateUrl: './registration-type-item.page.html',
  styleUrls: ['./registration-type-item.page.scss']
})
export class RegistrationTypeItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.REGISTRATION_TYPE, servicePath: 'RegistrationType' };
  }
}
