import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LineOfBusinessRoutingModule } from './line-of-business-routing.module';
import { LineOfBusinessItemPage } from './line-of-business-item/line-of-business-item.page';
import { SharedModule } from 'shared/shared.module';
import { LineOfBusinessListPage } from './line-of-business-list/line-of-business.page';
import { LineOfBusinessComponentModule } from 'components/setup/line-of-business/line-of-business.module';

@NgModule({
  declarations: [LineOfBusinessListPage, LineOfBusinessItemPage],
  imports: [CommonModule, SharedModule, LineOfBusinessRoutingModule, LineOfBusinessComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LineOfBusinessModule {}
