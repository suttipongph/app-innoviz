import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { LineOfBusinessItemPage } from './line-of-business-item/line-of-business-item.page';
import { LineOfBusinessListPage } from './line-of-business-list/line-of-business.page';

const routes: Routes = [
  {
    path: '',
    component: LineOfBusinessListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: LineOfBusinessItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LineOfBusinessRoutingModule {}
