import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { BusinessTypeItemPage } from './business-type-item/business-type-item.page';
import { BusinessTypeListPage } from './business-type-list/business-type-list.page';

const routes: Routes = [
  {
    path: '',
    component: BusinessTypeListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BusinessTypeItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessTypeRoutingModule {}
