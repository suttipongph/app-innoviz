import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'business-type-list-page',
  templateUrl: './business-type-list.page.html',
  styleUrls: ['./business-type-list.page.scss']
})
export class BusinessTypeListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'businessTypeGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.BUSINESS_TYPE, servicePath: 'BusinessType' };
  }
}
