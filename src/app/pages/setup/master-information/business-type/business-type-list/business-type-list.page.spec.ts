import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessTypeListPage } from './business-type-list.page';

describe('BusinessTypeListPage', () => {
  let component: BusinessTypeListPage;
  let fixture: ComponentFixture<BusinessTypeListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BusinessTypeListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessTypeListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
