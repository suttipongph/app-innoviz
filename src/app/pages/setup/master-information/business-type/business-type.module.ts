import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessTypeRoutingModule } from './business-type-routing.module';
import { BusinessTypeListPage } from './business-type-list/business-type-list.page';
import { BusinessTypeItemPage } from './business-type-item/business-type-item.page';
import { SharedModule } from 'shared/shared.module';
import { BusinessTypeComponentModule } from 'components/setup/business-type/business-type.module';

@NgModule({
  declarations: [BusinessTypeListPage, BusinessTypeItemPage],
  imports: [CommonModule, SharedModule, BusinessTypeRoutingModule, BusinessTypeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusinessTypeModule {}
