import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessTypeItemPage } from './business-type-item.page';

describe('BusinessTypeItemPage', () => {
  let component: BusinessTypeItemPage;
  let fixture: ComponentFixture<BusinessTypeItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BusinessTypeItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessTypeItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
