import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'business-type-item-page',
  templateUrl: './business-type-item.page.html',
  styleUrls: ['./business-type-item.page.scss']
})
export class BusinessTypeItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.BUSINESS_TYPE, servicePath: 'BusinessType' };
  }
}
