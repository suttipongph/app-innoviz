import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { NationalityItemPage } from './nationality-item/nationality-item.page';
import { NationalityListPage } from './nationality-list/nationality-list.page';

const routes: Routes = [
  {
    path: '',
    component: NationalityListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: NationalityItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NationalityRoutingModule {}
