import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NationalityRoutingModule } from './nationality-routing.module';
import { NationalityListPage } from './nationality-list/nationality-list.page';
import { NationalityItemPage } from './nationality-item/nationality-item.page';
import { SharedModule } from 'shared/shared.module';
import { NationalityComponentModule } from 'components/setup/nationality/nationality.module';

@NgModule({
  declarations: [NationalityListPage, NationalityItemPage],
  imports: [CommonModule, SharedModule, NationalityRoutingModule, NationalityComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NationalityModule {}
