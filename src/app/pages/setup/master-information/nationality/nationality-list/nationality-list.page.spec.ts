import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NationalityListPage } from './nationality-list.page';

describe('NationalityListPage', () => {
  let component: NationalityListPage;
  let fixture: ComponentFixture<NationalityListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NationalityListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NationalityListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
