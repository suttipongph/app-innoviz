import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NationalityItemPage } from './nationality-item.page';

describe('NationalityItemPage', () => {
  let component: NationalityItemPage;
  let fixture: ComponentFixture<NationalityItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NationalityItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NationalityItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
