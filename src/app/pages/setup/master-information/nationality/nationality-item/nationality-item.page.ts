import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'nationality-item-page',
  templateUrl: './nationality-item.page.html',
  styleUrls: ['./nationality-item.page.scss']
})
export class NationalityItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.NATIONALITY, servicePath: 'Nationality' };
  }
}
