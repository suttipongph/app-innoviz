import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessCollateralStatusItemPage } from './business-collateral-status-item.page';

describe('BusinessCollateralStatusItemPage', () => {
  let component: BusinessCollateralStatusItemPage;
  let fixture: ComponentFixture<BusinessCollateralStatusItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BusinessCollateralStatusItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCollateralStatusItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
