import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessCollateralStatusRoutingModule } from './business-collateral-status-routing.module';
import { BusinessCollateralStatusListPage } from './business-collateral-status-list/business-collateral-status-list.page';
import { BusinessCollateralStatusItemPage } from './business-collateral-status-item/business-collateral-status-item.page';
import { SharedModule } from 'shared/shared.module';
import { BusinessCollateralStatusComponentModule } from 'components/setup/business-collateral-status/business-collateral-status.module';

@NgModule({
  declarations: [BusinessCollateralStatusListPage, BusinessCollateralStatusItemPage],
  imports: [CommonModule, SharedModule, BusinessCollateralStatusRoutingModule, BusinessCollateralStatusComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusinessCollateralStatusModule {}
