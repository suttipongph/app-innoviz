import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { BusinessCollateralStatusItemPage } from './business-collateral-status-item/business-collateral-status-item.page';
import { BusinessCollateralStatusListPage } from './business-collateral-status-list/business-collateral-status-list.page';

const routes: Routes = [
  {
    path: '',
    component: BusinessCollateralStatusListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BusinessCollateralStatusItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessCollateralStatusRoutingModule {}
