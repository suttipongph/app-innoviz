import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'business-collateral-status-list-page',
  templateUrl: './business-collateral-status-list.page.html',
  styleUrls: ['./business-collateral-status-list.page.scss']
})
export class BusinessCollateralStatusListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'businessCollateralStatusGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.BUSINESS_COLLATERAL_STATUS, servicePath: 'BusinessCollateralStatus' };
  }
}
