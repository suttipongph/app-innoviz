import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessCollateralStatusListPage } from './business-collateral-status-list.page';

describe('BusinessCollateralStatusListPage', () => {
  let component: BusinessCollateralStatusListPage;
  let fixture: ComponentFixture<BusinessCollateralStatusListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BusinessCollateralStatusListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCollateralStatusListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
