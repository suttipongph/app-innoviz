import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessCollateralTypeItemPage } from './business-collateral-type-item.page';

describe('BusinessCollateralTypeItemPage', () => {
  let component: BusinessCollateralTypeItemPage;
  let fixture: ComponentFixture<BusinessCollateralTypeItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BusinessCollateralTypeItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCollateralTypeItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
