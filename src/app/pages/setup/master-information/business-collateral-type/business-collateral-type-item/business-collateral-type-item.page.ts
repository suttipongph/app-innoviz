import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'business-collateral-type-item-page',
  templateUrl: './business-collateral-type-item.page.html',
  styleUrls: ['./business-collateral-type-item.page.scss']
})
export class BusinessCollateralTypeItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.BUSINESS_COLLATERAL_TYPE,
      servicePath: 'BusinessCollateralType',
      childPaths: [
        {
          pagePath: ROUTE_MASTER_GEN.BUSINESS_COLLATERAL_SUB_TYPE,
          servicePath: 'BusinessCollateralType'
        }
      ]
    };
  }
}
