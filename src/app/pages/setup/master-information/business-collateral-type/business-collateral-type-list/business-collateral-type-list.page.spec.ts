import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessCollateralTypeListPage } from './business-collateral-type-list.page';

describe('BusinessCollateralTypeListPage', () => {
  let component: BusinessCollateralTypeListPage;
  let fixture: ComponentFixture<BusinessCollateralTypeListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BusinessCollateralTypeListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCollateralTypeListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
