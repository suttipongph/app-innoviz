import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { BusinessCollateralSubTypeItemPage } from './business-collateral-sub-type-item/business-collateral-sub-type-item.page';
import { BusinessCollateralTypeItemPage } from './business-collateral-type-item/business-collateral-type-item.page';
import { BusinessCollateralTypeListPage } from './business-collateral-type-list/business-collateral-type-list.page';

const routes: Routes = [
  {
    path: '',
    component: BusinessCollateralTypeListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BusinessCollateralTypeItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/businesscollateralsubtype-child/:id',
    component: BusinessCollateralSubTypeItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessCollateralTypeRoutingModule {}
