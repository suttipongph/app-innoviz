import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessCollateralTypeRoutingModule } from './business-collateral-type-routing.module';
import { BusinessCollateralTypeListPage } from './business-collateral-type-list/business-collateral-type-list.page';
import { BusinessCollateralTypeItemPage } from './business-collateral-type-item/business-collateral-type-item.page';
import { SharedModule } from 'shared/shared.module';
import { BusinessCollateralTypeComponentModule } from 'components/setup/business-collateral-type/business-collateral-type.module';
import { BusinessCollateralSubTypeItemPage } from './business-collateral-sub-type-item/business-collateral-sub-type-item.page';
import { BusinessCollateralSubTypeComponentModule } from 'components/setup/business-collateral-sub-type/business-collateral-sub-type.module';

@NgModule({
  declarations: [BusinessCollateralTypeListPage, BusinessCollateralTypeItemPage, BusinessCollateralSubTypeItemPage],
  imports: [
    CommonModule,
    SharedModule,
    BusinessCollateralTypeRoutingModule,
    BusinessCollateralTypeComponentModule,
    BusinessCollateralSubTypeComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusinessCollateralTypeModule {}
