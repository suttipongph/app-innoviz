import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessCollateralTypeItemPage } from '../business-collateral-type-item/business-collateral-type-item.page';
import { BusinessCollateralSubTypeItemPage } from './business-collateral-sub-type-item.page';

describe('BusinessCollateralTypeItemPage', () => {
  let component: BusinessCollateralSubTypeItemPage;
  let fixture: ComponentFixture<BusinessCollateralSubTypeItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BusinessCollateralSubTypeItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCollateralTypeItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
