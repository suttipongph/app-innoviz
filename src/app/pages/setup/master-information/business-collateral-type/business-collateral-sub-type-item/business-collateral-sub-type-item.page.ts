import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'business-collateral-sub-type-item-page',
  templateUrl: './business-collateral-sub-type-item.page.html',
  styleUrls: ['./business-collateral-sub-type-item.page.scss']
})
export class BusinessCollateralSubTypeItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.BUSINESS_COLLATERAL_SUB_TYPE, servicePath: 'BusinessCollateralType' };
  }
}
