import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BlacklistStatusListPage } from './blacklist-status-list.page';

describe('BlacklistStatusListPage', () => {
  let component: BlacklistStatusListPage;
  let fixture: ComponentFixture<BlacklistStatusListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BlacklistStatusListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BlacklistStatusListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
