import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlacklistStatusRoutingModule } from './blacklist-status-routing.module';
import { BlacklistStatusListPage } from './blacklist-status-list/blacklist-status-list.page';
import { BlacklistStatusItemPage } from './blacklist-status-item/blacklist-status-item.page';
import { SharedModule } from 'shared/shared.module';
import { BlacklistStatusComponentModule } from 'components/setup/blacklist-status/blacklist-status.module';

@NgModule({
  declarations: [BlacklistStatusListPage, BlacklistStatusItemPage],
  imports: [CommonModule, SharedModule, BlacklistStatusRoutingModule, BlacklistStatusComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BlacklistStatusModule {}
