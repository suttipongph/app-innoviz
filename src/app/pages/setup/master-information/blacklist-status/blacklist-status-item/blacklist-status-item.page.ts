import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'blacklist-status-item-page',
  templateUrl: './blacklist-status-item.page.html',
  styleUrls: ['./blacklist-status-item.page.scss']
})
export class BlacklistStatusItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.BLACKLIST_STATUS, servicePath: 'BlacklistStatus' };
  }
}
