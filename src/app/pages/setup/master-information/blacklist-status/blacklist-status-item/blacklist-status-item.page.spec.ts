import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BlacklistStatusItemPage } from './blacklist-status-item.page';

describe('BlacklistStatusItemPage', () => {
  let component: BlacklistStatusItemPage;
  let fixture: ComponentFixture<BlacklistStatusItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BlacklistStatusItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BlacklistStatusItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
