import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { BlacklistStatusItemPage } from './blacklist-status-item/blacklist-status-item.page';
import { BlacklistStatusListPage } from './blacklist-status-list/blacklist-status-list.page';

const routes: Routes = [
  {
    path: '',
    component: BlacklistStatusListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BlacklistStatusItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlacklistStatusRoutingModule {}
