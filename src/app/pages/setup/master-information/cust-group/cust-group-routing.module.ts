import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CustGroupItemPage } from './cust-group-item/cust-group-item.page';
import { CustGroupPage } from './cust-group-list/cust-group.page';

const routes: Routes = [
  {
    path: '',
    component: CustGroupPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CustGroupItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustGroupRoutingModule {}
