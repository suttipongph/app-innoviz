import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'cust-group-item.page',
  templateUrl: './cust-group-item.page.html',
  styleUrls: ['./cust-group-item.page.scss']
})
export class CustGroupItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
    console.log(this.setPath);
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.CUSTGROUP, servicePath: 'CustGroup' };
  }
}
