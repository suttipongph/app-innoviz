import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustGroupRoutingModule } from './cust-group-routing.module';
import { CustGroupItemPage } from './cust-group-item/cust-group-item.page';
import { SharedModule } from 'shared/shared.module';

import { CustGroupPage } from './cust-group-list/cust-group.page';
import { CustGroupComponentModule } from 'components/setup/cust-group/cust-group.module';

@NgModule({
  declarations: [CustGroupPage, CustGroupItemPage],
  imports: [CommonModule, SharedModule, CustGroupRoutingModule, CustGroupComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustGroupModule {}
