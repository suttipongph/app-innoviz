import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { IntroducedByItemPage } from './introduced-by-item/introduced-by-item.page';
import { IntroducedByListPage } from './introduced-by-list/introduced-by.page';

const routes: Routes = [
  {
    path: '',
    component: IntroducedByListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: IntroducedByItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntroducedByRoutingModule {}
