import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'introduced-by-item.page',
  templateUrl: './introduced-by-item.page.html',
  styleUrls: ['./introduced-by-item.page.scss']
})
export class IntroducedByItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.INTRODUCEDBY, servicePath: 'IntroducedBy' };
  }
}
