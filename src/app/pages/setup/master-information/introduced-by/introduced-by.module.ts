import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IntroducedByRoutingModule } from './introduced-by-routing.module';
import { IntroducedByItemPage } from './introduced-by-item/introduced-by-item.page';
import { SharedModule } from 'shared/shared.module';
import { IntroducedByListPage } from './introduced-by-list/introduced-by.page';
import { IntroducedByComponentModule } from 'components/setup/introduced-by/introduced-by.module';

@NgModule({
  declarations: [IntroducedByListPage, IntroducedByItemPage],
  imports: [CommonModule, SharedModule, IntroducedByRoutingModule, IntroducedByComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IntroducedByModule {}
