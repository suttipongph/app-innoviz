import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GradeClassificationRoutingModule } from './grade-classification-routing.module';
import { GradeClassificationListPage } from './grade-classification-list/grade-classification-list.page';
import { GradeClassificationItemPage } from './grade-classification-item/grade-classification-item.page';
import { SharedModule } from 'shared/shared.module';
import { GradeClassificationComponentModule } from 'components/setup/grade-classification/grade-classification.module';

@NgModule({
  declarations: [GradeClassificationListPage, GradeClassificationItemPage],
  imports: [CommonModule, SharedModule, GradeClassificationRoutingModule, GradeClassificationComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GradeClassificationModule {}
