import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GradeClassificationItemPage } from './grade-classification-item.page';

describe('GradeClassificationItemPage', () => {
  let component: GradeClassificationItemPage;
  let fixture: ComponentFixture<GradeClassificationItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GradeClassificationItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GradeClassificationItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
