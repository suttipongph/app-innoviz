import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'grade-classification-item-page',
  templateUrl: './grade-classification-item.page.html',
  styleUrls: ['./grade-classification-item.page.scss']
})
export class GradeClassificationItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.GRADE_CLASSIFICATION, servicePath: 'GradeClassification' };
  }
}
