import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { GradeClassificationItemPage } from './grade-classification-item/grade-classification-item.page';
import { GradeClassificationListPage } from './grade-classification-list/grade-classification-list.page';

const routes: Routes = [
  {
    path: '',
    component: GradeClassificationListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: GradeClassificationItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GradeClassificationRoutingModule {}
