import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GradeClassificationListPage } from './grade-classification-list.page';

describe('GradeClassificationListPage', () => {
  let component: GradeClassificationListPage;
  let fixture: ComponentFixture<GradeClassificationListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GradeClassificationListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GradeClassificationListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
