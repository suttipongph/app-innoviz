import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VendGroupListPage } from './vend-group-list/vend-group-list.page';
import { VendGroupItemPage } from './vend-group-item/vend-group-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: VendGroupListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: VendGroupItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendGroupRoutingModule {}
