import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'vend-group-item.page',
  templateUrl: './vend-group-item.page.html',
  styleUrls: ['./vend-group-item.page.scss']
})
export class VendGroupItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.VEND_GROUP, servicePath: 'VendGroup' };
  }
}
