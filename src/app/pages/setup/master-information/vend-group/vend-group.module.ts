import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendGroupRoutingModule } from './vend-group-routing.module';
import { VendGroupListPage } from './vend-group-list/vend-group-list.page';
import { VendGroupItemPage } from './vend-group-item/vend-group-item.page';
import { SharedModule } from 'shared/shared.module';
import { VendGroupComponentModule } from 'components/setup/vend-group/vend-group.module';

@NgModule({
  declarations: [VendGroupListPage, VendGroupItemPage],
  imports: [CommonModule, SharedModule, VendGroupRoutingModule, VendGroupComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class VendGroupModule {}
