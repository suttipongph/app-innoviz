import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { OccupationItemPage } from './occupation-item/occupation-item.page';
import { OccupationListPage } from './occupation-list/occupation.page';

const routes: Routes = [
  {
    path: '',
    component: OccupationListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: OccupationItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OccupationRoutingModule {}
