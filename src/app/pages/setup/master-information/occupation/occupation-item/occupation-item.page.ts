import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'occupation-item.page',
  templateUrl: './occupation-item.page.html',
  styleUrls: ['./occupation-item.page.scss']
})
export class OccupationItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.OCCUPATION, servicePath: 'Occupation' };
  }
}
