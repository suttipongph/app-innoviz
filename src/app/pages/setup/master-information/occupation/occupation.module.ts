import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OccupationRoutingModule } from './occupation-routing.module';
import { OccupationItemPage } from './occupation-item/occupation-item.page';
import { SharedModule } from 'shared/shared.module';
import { OccupationListPage } from './occupation-list/occupation.page';
import { OccupationComponentModule } from 'components/setup/occupation/occupation.module';

@NgModule({
  declarations: [OccupationListPage, OccupationItemPage],
  imports: [CommonModule, SharedModule, OccupationRoutingModule, OccupationComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OccupationModule {}
