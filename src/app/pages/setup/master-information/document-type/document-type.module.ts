import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentTypeRoutingModule } from './document-type-routing.module';
import { DocumentTypeListPage } from './document-type-list/document-type-list.page';
import { DocumentTypeItemPage } from './document-type-item/document-type-item.page';
import { SharedModule } from 'shared/shared.module';
import { DocumentTypeComponentModule } from 'components/setup/document-type/document-type.module';

@NgModule({
  declarations: [DocumentTypeListPage, DocumentTypeItemPage],
  imports: [CommonModule, SharedModule, DocumentTypeRoutingModule, DocumentTypeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DocumentTypeModule {}
