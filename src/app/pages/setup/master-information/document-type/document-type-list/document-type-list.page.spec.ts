import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DocumentTypeListPage } from './document-type-list.page';

describe('DocumentTypeListPage', () => {
  let component: DocumentTypeListPage;
  let fixture: ComponentFixture<DocumentTypeListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DocumentTypeListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentTypeListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
