import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DocumentTypeItemPage } from './document-type-item.page';

describe('DocumentTypeItemPage', () => {
  let component: DocumentTypeItemPage;
  let fixture: ComponentFixture<DocumentTypeItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DocumentTypeItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentTypeItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
