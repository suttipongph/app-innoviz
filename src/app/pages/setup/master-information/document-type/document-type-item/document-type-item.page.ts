import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'document-type-item-page',
  templateUrl: './document-type-item.page.html',
  styleUrls: ['./document-type-item.page.scss']
})
export class DocumentTypeItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.DOCUMENT_TYPE, servicePath: 'DocumentType' };
  }
}
