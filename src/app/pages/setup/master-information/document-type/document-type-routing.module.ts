import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { DocumentTypeItemPage } from './document-type-item/document-type-item.page';
import { DocumentTypeListPage } from './document-type-list/document-type-list.page';

const routes: Routes = [
  {
    path: '',
    component: DocumentTypeListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: DocumentTypeItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentTypeRoutingModule {}
