import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'marital-status-item.page',
  templateUrl: './marital-status-item.page.html',
  styleUrls: ['./marital-status-item.page.scss']
})
export class MaritalStatusItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.MARITALSTATUS, servicePath: 'MaritalStatus' };
  }
}
