import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaritalStatusRoutingModule } from './marital-status-routing.module';
import { MaritalStatusItemPage } from './marital-status-item/marital-status-item.page';
import { SharedModule } from 'shared/shared.module';
import { MaritalStatusListPage } from './marital-status-list/marital-status.page';
import { MaritalStatusComponentModule } from 'components/setup/marital-status/marital-status.module';

@NgModule({
  declarations: [MaritalStatusListPage, MaritalStatusItemPage],
  imports: [CommonModule, SharedModule, MaritalStatusRoutingModule, MaritalStatusComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MaritalStatusModule {}
