import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { MaritalStatusItemPage } from './marital-status-item/marital-status-item.page';
import { MaritalStatusListPage } from './marital-status-list/marital-status.page';

const routes: Routes = [
  {
    path: '',
    component: MaritalStatusListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: MaritalStatusItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaritalStatusRoutingModule {}
