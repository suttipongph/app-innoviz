import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ParentCompanyListPage } from './parent-company-list.page';

describe('ParentCompanyListPage', () => {
  let component: ParentCompanyListPage;
  let fixture: ComponentFixture<ParentCompanyListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ParentCompanyListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentCompanyListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
