import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ParentCompanyItemPage } from './parent-company-item.page';

describe('ParentCompanyItemPage', () => {
  let component: ParentCompanyItemPage;
  let fixture: ComponentFixture<ParentCompanyItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ParentCompanyItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentCompanyItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
