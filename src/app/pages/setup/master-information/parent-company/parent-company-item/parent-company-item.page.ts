import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'parent-company-item-page',
  templateUrl: './parent-company-item.page.html',
  styleUrls: ['./parent-company-item.page.scss']
})
export class ParentCompanyItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.PARENT_COMPANY, servicePath: 'ParentCompany' };
  }
}
