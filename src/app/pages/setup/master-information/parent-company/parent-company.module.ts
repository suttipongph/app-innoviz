import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParentCompanyRoutingModule } from './parent-company-routing.module';
import { ParentCompanyListPage } from './parent-company-list/parent-company-list.page';
import { ParentCompanyItemPage } from './parent-company-item/parent-company-item.page';
import { SharedModule } from 'shared/shared.module';
import { ParentCompanyComponentModule } from 'components/setup/parent-company/parent-company.module';

@NgModule({
  declarations: [ParentCompanyListPage, ParentCompanyItemPage],
  imports: [CommonModule, SharedModule, ParentCompanyRoutingModule, ParentCompanyComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ParentCompanyModule {}
