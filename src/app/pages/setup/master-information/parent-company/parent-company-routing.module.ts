import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ParentCompanyItemPage } from './parent-company-item/parent-company-item.page';
import { ParentCompanyListPage } from './parent-company-list/parent-company-list.page';

const routes: Routes = [
  {
    path: '',
    component: ParentCompanyListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ParentCompanyItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParentCompanyRoutingModule {}
