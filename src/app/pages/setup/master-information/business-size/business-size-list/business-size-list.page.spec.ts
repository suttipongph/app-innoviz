import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessSizeListPage } from './business-size-list.page';

describe('BusinessSizeListPage', () => {
  let component: BusinessSizeListPage;
  let fixture: ComponentFixture<BusinessSizeListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BusinessSizeListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessSizeListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
