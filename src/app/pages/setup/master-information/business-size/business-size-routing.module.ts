import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { BusinessSizeItemPage } from './business-size-item/business-size-item.page';
import { BusinessSizeListPage } from './business-size-list/business-size-list.page';

const routes: Routes = [
  {
    path: '',
    component: BusinessSizeListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BusinessSizeItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessSizeRoutingModule {}
