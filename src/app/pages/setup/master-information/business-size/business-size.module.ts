import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessSizeRoutingModule } from './business-size-routing.module';
import { BusinessSizeListPage } from './business-size-list/business-size-list.page';
import { BusinessSizeItemPage } from './business-size-item/business-size-item.page';
import { SharedModule } from 'shared/shared.module';
import { BusinessSizeComponentModule } from 'components/setup/business-size/business-size.module';

@NgModule({
  declarations: [BusinessSizeListPage, BusinessSizeItemPage],
  imports: [CommonModule, SharedModule, BusinessSizeRoutingModule, BusinessSizeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusinessSizeModule {}
