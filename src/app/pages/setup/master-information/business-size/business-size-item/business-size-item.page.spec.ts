import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessSizeItemPage } from './business-size-item.page';

describe('BusinessSizeItemPage', () => {
  let component: BusinessSizeItemPage;
  let fixture: ComponentFixture<BusinessSizeItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BusinessSizeItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessSizeItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
