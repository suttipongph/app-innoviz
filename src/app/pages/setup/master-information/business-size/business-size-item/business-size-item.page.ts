import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-business-size-item-page',
  templateUrl: './business-size-item.page.html',
  styleUrls: ['./business-size-item.page.scss']
})
export class BusinessSizeItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.BUSINESS_SIZE, servicePath: 'BusinessSize' };
  }
}
