import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { AuthorizedPersonTypeItemPage } from './authorized-person-type-item/authorized-person-type-item.page';
import { AuthorizedPersonTypeListPage } from './authorized-person-type-list/authorized-person-type-list.page';

const routes: Routes = [
  {
    path: '',
    component: AuthorizedPersonTypeListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: AuthorizedPersonTypeItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthorizedPersonTypeRoutingModule {}
