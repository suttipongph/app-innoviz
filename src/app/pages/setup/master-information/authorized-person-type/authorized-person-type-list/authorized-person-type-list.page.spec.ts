import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AuthorizedPersonTypeListPage } from './authorized-person-type-list.page';

describe('AuthorizedPersonTypeListPage', () => {
  let component: AuthorizedPersonTypeListPage;
  let fixture: ComponentFixture<AuthorizedPersonTypeListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AuthorizedPersonTypeListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorizedPersonTypeListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
