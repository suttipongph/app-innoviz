import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'authorized-person-type-item-page',
  templateUrl: './authorized-person-type-item.page.html',
  styleUrls: ['./authorized-person-type-item.page.scss']
})
export class AuthorizedPersonTypeItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.AUTHORIZED_PERSON_TYPE, servicePath: 'AuthorizedPersonType' };
  }
}
