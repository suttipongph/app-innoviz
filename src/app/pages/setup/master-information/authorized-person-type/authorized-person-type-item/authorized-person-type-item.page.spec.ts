import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AuthorizedPersonTypeItemPage } from './authorized-person-type-item.page';

describe('AuthorizedPersonTypeItemPage', () => {
  let component: AuthorizedPersonTypeItemPage;
  let fixture: ComponentFixture<AuthorizedPersonTypeItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AuthorizedPersonTypeItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorizedPersonTypeItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
