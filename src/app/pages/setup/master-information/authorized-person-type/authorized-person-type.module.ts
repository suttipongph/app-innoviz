import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthorizedPersonTypeRoutingModule } from './authorized-person-type-routing.module';
import { AuthorizedPersonTypeListPage } from './authorized-person-type-list/authorized-person-type-list.page';
import { AuthorizedPersonTypeItemPage } from './authorized-person-type-item/authorized-person-type-item.page';
import { SharedModule } from 'shared/shared.module';
import { AuthorizedPersonTypeComponentModule } from 'components/setup/authorized-person-type/authorized-person-type.module';

@NgModule({
  declarations: [AuthorizedPersonTypeListPage, AuthorizedPersonTypeItemPage],
  imports: [CommonModule, SharedModule, AuthorizedPersonTypeRoutingModule, AuthorizedPersonTypeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AuthorizedPersonTypeModule {}
