import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { GuarantorTypeItemPage } from './guarantor-type-item/guarantor-type-item.page';
import { GuarantorTypeListPage } from './guarantor-type-list/guarantor-type-list.page';

const routes: Routes = [
  {
    path: '',
    component: GuarantorTypeListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: GuarantorTypeItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuarantorTypeRoutingModule {}
