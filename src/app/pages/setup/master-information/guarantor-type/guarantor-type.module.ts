import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GuarantorTypeRoutingModule } from './guarantor-type-routing.module';
import { GuarantorTypeListPage } from './guarantor-type-list/guarantor-type-list.page';
import { GuarantorTypeItemPage } from './guarantor-type-item/guarantor-type-item.page';
import { SharedModule } from 'shared/shared.module';
import { GuarantorTypeComponentModule } from 'components/setup/guarantor-type/guarantor-type.module';

@NgModule({
  declarations: [GuarantorTypeListPage, GuarantorTypeItemPage],
  imports: [CommonModule, SharedModule, GuarantorTypeRoutingModule, GuarantorTypeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GuarantorTypeModule {}
