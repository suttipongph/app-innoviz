import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GuarantorTypeListPage } from './guarantor-type-list.page';

describe('GuarantorTypeListPage', () => {
  let component: GuarantorTypeListPage;
  let fixture: ComponentFixture<GuarantorTypeListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GuarantorTypeListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuarantorTypeListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
