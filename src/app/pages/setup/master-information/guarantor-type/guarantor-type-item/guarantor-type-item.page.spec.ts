import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GuarantorTypeItemPage } from './guarantor-type-item.page';

describe('GuarantorTypeItemPage', () => {
  let component: GuarantorTypeItemPage;
  let fixture: ComponentFixture<GuarantorTypeItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GuarantorTypeItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuarantorTypeItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
