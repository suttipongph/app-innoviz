import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PropertyTypeRoutingModule } from './property-type-routing.module';
import { PropertyTypeListPage } from './property-type-list/property-type-list.page';
import { PropertyTypeItemPage } from './property-type-item/property-type-item.page';
import { SharedModule } from 'shared/shared.module';
import { PropertyTypeComponentModule } from 'components/setup/property-type/property-type.module';

@NgModule({
  declarations: [PropertyTypeListPage, PropertyTypeItemPage],
  imports: [CommonModule, SharedModule, PropertyTypeRoutingModule, PropertyTypeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PropertyTypeModule {}
