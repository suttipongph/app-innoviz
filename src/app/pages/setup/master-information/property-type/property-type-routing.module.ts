import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PropertyTypeListPage } from './property-type-list/property-type-list.page';
import { PropertyTypeItemPage } from './property-type-item/property-type-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: PropertyTypeListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: PropertyTypeItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PropertyTypeRoutingModule {}
