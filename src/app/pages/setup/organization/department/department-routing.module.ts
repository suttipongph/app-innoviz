import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { DepartmentItemPage } from './department-item/department-item.page';
import { DepartmentListPage } from './department-list/department-list.page';

const routes: Routes = [
  {
    path: '',
    component: DepartmentListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: DepartmentItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DepartmentRoutingModule {}
