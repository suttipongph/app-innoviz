import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepartmentRoutingModule } from './department-routing.module';
import { DepartmentListPage } from './department-list/department-list.page';
import { DepartmentItemPage } from './department-item/department-item.page';
import { SharedModule } from 'shared/shared.module';
import { DepartmentComponentModule } from 'components/setup/department/department.module';

@NgModule({
  declarations: [DepartmentListPage, DepartmentItemPage],
  imports: [CommonModule, SharedModule, DepartmentRoutingModule, DepartmentComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DepartmentModule {}
