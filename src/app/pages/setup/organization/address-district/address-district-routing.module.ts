import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddressDistrictItemPage } from './address-district-item/address-district-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { AddressDistrictListPage } from './address-district-list/address-district.page';

const routes: Routes = [
  {
    path: '',
    component: AddressDistrictListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: AddressDistrictItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddressDistrictRoutingModule {}
