import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'address-district-item.page',
  templateUrl: './address-district-item.page.html',
  styleUrls: ['./address-district-item.page.scss']
})
export class AddressDistrictItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.ADDRESSDISTRICT, servicePath: 'AddressDistrict' };
  }
}
