import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddressDistrictRoutingModule } from './address-district-routing.module';
import { AddressDistrictItemPage } from './address-district-item/address-district-item.page';
import { SharedModule } from 'shared/shared.module';
import { AddressDistrictListPage } from './address-district-list/address-district.page';
import { AddressDistrictComponentModule } from 'components/setup/address-district/address-district.module';

@NgModule({
  declarations: [AddressDistrictListPage, AddressDistrictItemPage],
  imports: [CommonModule, SharedModule, AddressDistrictRoutingModule, AddressDistrictComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AddressDistrictModule {}
