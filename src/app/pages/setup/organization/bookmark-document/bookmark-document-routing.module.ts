import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { BookmarkDocumentItemPage } from './bookmark-document-item/bookmark-document-item.page';
import { BookmarkDocumentListPage } from './bookmark-document-list/bookmark-document-list.page';

const routes: Routes = [
  {
    path: '',
    component: BookmarkDocumentListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BookmarkDocumentItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookmarkDocumentRoutingModule {}
