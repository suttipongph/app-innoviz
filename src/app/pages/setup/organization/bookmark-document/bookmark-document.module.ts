import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookmarkDocumentRoutingModule } from './bookmark-document-routing.module';
import { BookmarkDocumentListPage } from './bookmark-document-list/bookmark-document-list.page';
import { BookmarkDocumentItemPage } from './bookmark-document-item/bookmark-document-item.page';
import { SharedModule } from 'shared/shared.module';
import { BookmarkDocumentComponentModule } from 'components/setup/bookmark-document/bookmark-document.module';

@NgModule({
  declarations: [BookmarkDocumentListPage, BookmarkDocumentItemPage],
  imports: [CommonModule, SharedModule, BookmarkDocumentRoutingModule, BookmarkDocumentComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BookmarkDocumentModule {}
