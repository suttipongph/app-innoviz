import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'bookmark-document-item-page',
  templateUrl: './bookmark-document-item.page.html',
  styleUrls: ['./bookmark-document-item.page.scss']
})
export class BookmarkDocumentItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.BOOKMARK_DOCUMENT, servicePath: 'BookmarkDocument' };
  }
}
