import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { AddressCountryItemPage } from './address-country-item/address-country-item.page';
import { AddressCountryListPage } from './address-country-list/address-country.page';

const routes: Routes = [
  {
    path: '',
    component: AddressCountryListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: AddressCountryItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddressCountryRoutingModule {}
