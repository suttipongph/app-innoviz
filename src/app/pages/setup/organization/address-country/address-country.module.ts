import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddressCountryRoutingModule } from './address-country-routing.module';
import { AddressCountryItemPage } from './address-country-item/address-country-item.page';
import { SharedModule } from 'shared/shared.module';
import { AddressCountryComponentModule } from 'components/setup/address-country/address-country.module';
import { AddressCountryListPage } from './address-country-list/address-country.page';

@NgModule({
  declarations: [AddressCountryListPage, AddressCountryItemPage],
  imports: [CommonModule, SharedModule, AddressCountryRoutingModule, AddressCountryComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AddressCountryModule {}
