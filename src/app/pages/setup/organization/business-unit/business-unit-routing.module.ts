import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { BusinessUnitItemPage } from './business-unit-item/business-unit-item.page';
import { BusinessUnitListPage } from './business-unit-list/business-unit-list.page';

const routes: Routes = [
  {
    path: '',
    component: BusinessUnitListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BusinessUnitItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessUnitRoutingModule {}
