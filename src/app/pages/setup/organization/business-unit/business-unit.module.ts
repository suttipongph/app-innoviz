import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessUnitRoutingModule } from './business-unit-routing.module';
import { BusinessUnitListPage } from './business-unit-list/business-unit-list.page';
import { BusinessUnitItemPage } from './business-unit-item/business-unit-item.page';
import { SharedModule } from 'shared/shared.module';
import { BusinessUnitComponentModule } from 'components/setup/business-unit/business-unit.module';

@NgModule({
  declarations: [BusinessUnitListPage, BusinessUnitItemPage],
  imports: [CommonModule, SharedModule, BusinessUnitRoutingModule, BusinessUnitComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusinessUnitModule {}
