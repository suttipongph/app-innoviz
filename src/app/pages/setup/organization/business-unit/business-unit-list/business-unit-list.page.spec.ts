import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessUnitListPage } from './business-unit-list.page';

describe('BusinessUnitListPage', () => {
  let component: BusinessUnitListPage;
  let fixture: ComponentFixture<BusinessUnitListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BusinessUnitListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessUnitListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
