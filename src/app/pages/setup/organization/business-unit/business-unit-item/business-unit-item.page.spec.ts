import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessUnitItemPage } from './business-unit-item.page';

describe('BusinessUnitItemPage', () => {
  let component: BusinessUnitItemPage;
  let fixture: ComponentFixture<BusinessUnitItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BusinessUnitItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessUnitItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
