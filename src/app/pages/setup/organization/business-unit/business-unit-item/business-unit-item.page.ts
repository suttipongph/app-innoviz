import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'business-unit-item-page',
  templateUrl: './business-unit-item.page.html',
  styleUrls: ['./business-unit-item.page.scss']
})
export class BusinessUnitItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.BUSINESS_UNIT, servicePath: 'BusinessUnit' };
  }
}
