import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmplTeamRoutingModule } from './empl-team-routing.module';
import { EmplTeamListPage } from './empl-team-list/empl-team-list.page';
import { EmplTeamItemPage } from './empl-team-item/empl-team-item.page';
import { SharedModule } from 'shared/shared.module';
import { EmplTeamComponentModule } from 'components/setup/empl-team/empl-team.module';

@NgModule({
  declarations: [EmplTeamListPage, EmplTeamItemPage],
  imports: [CommonModule, SharedModule, EmplTeamRoutingModule, EmplTeamComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EmplTeamModule {}
