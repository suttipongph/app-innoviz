import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmplTeamListPage } from './empl-team-list/empl-team-list.page';
import { EmplTeamItemPage } from './empl-team-item/empl-team-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: EmplTeamListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: EmplTeamItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmplTeamRoutingModule {}
