import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'empl-team-item.page',
  templateUrl: './empl-team-item.page.html',
  styleUrls: ['./empl-team-item.page.scss']
})
export class EmplTeamItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.EMPL_TEAM, servicePath: 'EmplTeam' };
  }
}
