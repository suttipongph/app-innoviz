import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarGroupRoutingModule } from './calendar-group-routing.module';
import { CalendarGroupListPage } from './calendar-group-list/calendar-group-list.page';
import { CalendarGroupItemPage } from './calendar-group-item/calendar-group-item.page';
import { SharedModule } from 'shared/shared.module';
import { CalendarGroupComponentModule } from 'components/setup/calendar-group/calendar-group.module';

@NgModule({
  declarations: [CalendarGroupListPage, CalendarGroupItemPage],
  imports: [CommonModule, SharedModule, CalendarGroupRoutingModule, CalendarGroupComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CalendarGroupModule {}
