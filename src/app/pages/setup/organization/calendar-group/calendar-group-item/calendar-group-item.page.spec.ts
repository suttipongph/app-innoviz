import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CalendarGroupItemPage } from './calendar-group-item.page';

describe('CalendarGroupItemPage', () => {
  let component: CalendarGroupItemPage;
  let fixture: ComponentFixture<CalendarGroupItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CalendarGroupItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarGroupItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
