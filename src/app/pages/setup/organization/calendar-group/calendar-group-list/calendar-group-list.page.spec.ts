import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CalendarGroupListPage } from './calendar-group-list.page';

describe('CalendarGroupListPage', () => {
  let component: CalendarGroupListPage;
  let fixture: ComponentFixture<CalendarGroupListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CalendarGroupListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarGroupListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
