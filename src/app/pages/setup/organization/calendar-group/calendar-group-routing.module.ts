import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CalendarGroupItemPage } from './calendar-group-item/calendar-group-item.page';
import { CalendarGroupListPage } from './calendar-group-list/calendar-group-list.page';

const routes: Routes = [
  {
    path: '',
    component: CalendarGroupListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CalendarGroupItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalendarGroupRoutingModule {}
