import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'withholding-tax-table-list-page',
  templateUrl: './withholding-tax-table-list.page.html',
  styleUrls: ['./withholding-tax-table-list.page.scss']
})
export class WithholdingTaxTableListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.WITHHOLDING_TAX_TABLE, servicePath: 'WithholdingTaxTable' };
  }
}
