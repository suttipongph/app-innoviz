import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WithholdingTaxTableRoutingModule } from './withholding-tax-table-routing.module';
import { WithholdingTaxTableListPage } from './withholding-tax-table-list/withholding-tax-table-list.page';
import { WithholdingTaxTableItemPage } from './withholding-tax-table-item/withholding-tax-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { WithholdingTaxTableComponentModule } from 'components/setup/withholding-tax-table/withholding-tax-table.module';
import { WithholdingTaxValueComponentModule } from 'components/setup/withholding-tax-value/withholding-tax-value.module';
import { WithholdingTaxValueItemPage } from './withholding-tax-value-item/withholding-tax-value-item.page';

@NgModule({
  declarations: [WithholdingTaxTableListPage, WithholdingTaxTableItemPage, WithholdingTaxValueItemPage],
  imports: [CommonModule, SharedModule, WithholdingTaxTableRoutingModule, WithholdingTaxTableComponentModule, WithholdingTaxValueComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WithholdingTaxTableModule {}
