import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'withholding-tax-value-item.page',
  templateUrl: './withholding-tax-value-item.page.html',
  styleUrls: ['./withholding-tax-value-item.page.scss']
})
export class WithholdingTaxValueItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.WITHHOLDING_TAX_VALUE, servicePath: 'WithholdingTaxTable' };
  }
}
