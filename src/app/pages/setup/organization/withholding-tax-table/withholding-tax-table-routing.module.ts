import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WithholdingTaxTableListPage } from './withholding-tax-table-list/withholding-tax-table-list.page';
import { WithholdingTaxTableItemPage } from './withholding-tax-table-item/withholding-tax-table-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { WithholdingTaxValueItemPage } from './withholding-tax-value-item/withholding-tax-value-item.page';

const routes: Routes = [
  {
    path: '',
    component: WithholdingTaxTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: WithholdingTaxTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/withholdingtaxvalue-child/:id',
    component: WithholdingTaxValueItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WithholdingTaxTableRoutingModule {}
