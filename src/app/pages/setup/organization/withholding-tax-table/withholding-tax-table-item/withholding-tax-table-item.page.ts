import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'withholding-tax-table-item.page',
  templateUrl: './withholding-tax-table-item.page.html',
  styleUrls: ['./withholding-tax-table-item.page.scss']
})
export class WithholdingTaxTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.WITHHOLDING_TAX_TABLE,
      servicePath: 'WithholdingTaxTable',
      childPaths: [{ pagePath: ROUTE_MASTER_GEN.WITHHOLDING_TAX_VALUE, servicePath: 'WithholdingTaxTable' }]
    };
  }
}
