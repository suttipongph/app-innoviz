import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarNonWorkingDateRoutingModule } from './calendar-non-working-date-routing.module';
import { CalendarNonWorkingDateListPage } from './calendar-non-working-date-list/calendar-non-working-date-list.page';
import { CalendarNonWorkingDateItemPage } from './calendar-non-working-date-item/calendar-non-working-date-item.page';
import { SharedModule } from 'shared/shared.module';
import { CalendarNonWorkingDateComponentModule } from 'components/setup/calendar-non-working-date/calendar-non-working-date.module';

@NgModule({
  declarations: [CalendarNonWorkingDateListPage, CalendarNonWorkingDateItemPage],
  imports: [CommonModule, SharedModule, CalendarNonWorkingDateRoutingModule, CalendarNonWorkingDateComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CalendarNonWorkingDateModule {}
