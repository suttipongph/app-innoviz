import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CalendarNonWorkingDateItemPage } from './calendar-non-working-date-item/calendar-non-working-date-item.page';
import { CalendarNonWorkingDateListPage } from './calendar-non-working-date-list/calendar-non-working-date-list.page';

const routes: Routes = [
  {
    path: '',
    component: CalendarNonWorkingDateListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CalendarNonWorkingDateItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalendarNonWorkingDateRoutingModule {}
