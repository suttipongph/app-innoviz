import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'calendar-non-working-date-list-page',
  templateUrl: './calendar-non-working-date-list.page.html',
  styleUrls: ['./calendar-non-working-date-list.page.scss']
})
export class CalendarNonWorkingDateListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'calendarNonWorkingDateGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.CALENDAR_NON_WORKING_DATE, servicePath: 'CalendarNonWorkingDate' };
  }
}
