import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CalendarNonWorkingDateListPage } from './calendar-non-working-date-list.page';

describe('CalendarNonWorkingDateListPage', () => {
  let component: CalendarNonWorkingDateListPage;
  let fixture: ComponentFixture<CalendarNonWorkingDateListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CalendarNonWorkingDateListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarNonWorkingDateListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
