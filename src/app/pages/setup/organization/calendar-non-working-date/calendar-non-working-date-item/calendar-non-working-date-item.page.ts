import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'calendar-non-working-date-item-page',
  templateUrl: './calendar-non-working-date-item.page.html',
  styleUrls: ['./calendar-non-working-date-item.page.scss']
})
export class CalendarNonWorkingDateItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.CALENDAR_NON_WORKING_DATE, servicePath: 'CalendarNonWorkingDate' };
  }
}
