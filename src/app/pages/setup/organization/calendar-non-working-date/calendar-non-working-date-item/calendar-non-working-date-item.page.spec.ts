import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CalendarNonWorkingDateItemPage } from './calendar-non-working-date-item.page';

describe('CalendarNonWorkingDateItemPage', () => {
  let component: CalendarNonWorkingDateItemPage;
  let fixture: ComponentFixture<CalendarNonWorkingDateItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CalendarNonWorkingDateItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarNonWorkingDateItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
