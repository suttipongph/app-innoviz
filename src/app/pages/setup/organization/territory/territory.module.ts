import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TerritoryRoutingModule } from './territory-routing.module';
import { TerritoryItemPage } from './territory-item/territory-item.page';
import { SharedModule } from 'shared/shared.module';
import { TerritoryListPage } from './territory-list/territory.page';
import { TerritoryComponentModule } from 'components/setup/territory/territory.module';

@NgModule({
  declarations: [TerritoryListPage, TerritoryItemPage],
  imports: [CommonModule, SharedModule, TerritoryRoutingModule, TerritoryComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TerritoryModule {}
