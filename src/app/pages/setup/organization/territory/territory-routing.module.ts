import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { TerritoryItemPage } from './territory-item/territory-item.page';
import { TerritoryListPage } from './territory-list/territory.page';

const routes: Routes = [
  {
    path: '',
    component: TerritoryListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: TerritoryItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TerritoryRoutingModule {}
