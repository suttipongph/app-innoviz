import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'territory-item.page',
  templateUrl: './territory-item.page.html',
  styleUrls: ['./territory-item.page.scss']
})
export class TerritoryItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.TERRITORY, servicePath: 'Territory' };
  }
}
