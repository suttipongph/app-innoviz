import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { DocumentTemplateTableItemPage } from './document-template-table-item/document-template-table-item.page';
import { DocumentTemplateTableListPage } from './document-template-table-list/document-template-table.page';

const routes: Routes = [
  {
    path: '',
    component: DocumentTemplateTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: DocumentTemplateTableItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentTemplateTableRoutingModule {}
