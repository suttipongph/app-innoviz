import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'document-template-table-item.page',
  templateUrl: './document-template-table-item.page.html',
  styleUrls: ['./document-template-table-item.page.scss']
})
export class DocumentTemplateTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.DOCUMENTTEMPLATETABLE, servicePath: 'DocumentTemplateTable' };
  }
}
