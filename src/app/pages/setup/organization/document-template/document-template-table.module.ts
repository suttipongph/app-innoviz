import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentTemplateTableRoutingModule } from './document-template-table-routing.module';
import { DocumentTemplateTableItemPage } from './document-template-table-item/document-template-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { DocumentTemplateTableComponentModule } from 'components/setup/document-template/document-template-table.module';
import { DocumentTemplateTableListPage } from './document-template-table-list/document-template-table.page';

@NgModule({
  declarations: [DocumentTemplateTableListPage, DocumentTemplateTableItemPage],
  imports: [CommonModule, SharedModule, DocumentTemplateTableRoutingModule, DocumentTemplateTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DocumentTemplateTableModule {}
