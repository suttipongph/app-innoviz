import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'verificationtype', loadChildren: () => import('./verification-type/verification-type.module').then((m) => m.VerificationTypeModule) },
  { path: 'department', loadChildren: () => import('./department/department.module').then((m) => m.DepartmentModule) },
  { path: 'intercompany', loadChildren: () => import('./intercompany/intercompany.module').then((m) => m.IntercompanyModule) },
  { path: 'documentprocess', loadChildren: () => import('./document-process/document-process.module').then((m) => m.DocumentProcessModule) },
  { path: 'addresscountry', loadChildren: () => import('./address-country/address-country.module').then((m) => m.AddressCountryModule) },
  {
    path: 'addresspostalcode',
    loadChildren: () => import('./address-postal-code/address-postal-code.module').then((m) => m.AddressPostalCodeModule)
  },
  { path: 'addressprovince', loadChildren: () => import('./address-province/address-province.module').then((m) => m.AddressProvinceModule) },
  { path: 'taxtable', loadChildren: () => import('./tax-table/tax-table.module').then((m) => m.TaxTableModule) },
  { path: 'addressdistrict', loadChildren: () => import('./address-district/address-district.module').then((m) => m.AddressDistrictModule) },
  {
    path: 'addresssubdistrict',
    loadChildren: () => import('./address-sub-district/address-sub-district.module').then((m) => m.AddressSubDistrictModule)
  },
  { path: 'currency', loadChildren: () => import('./currency/currency.module').then((m) => m.CurrencyModule) },
  {
    path: 'withholdingtaxgroup',
    loadChildren: () => import('./withholding-tax-group/withholding-tax-group.module').then((m) => m.WithholdingTaxGroupModule)
  },
  { path: 'territory', loadChildren: () => import('./territory/territory.module').then((m) => m.TerritoryModule) },
  { path: 'emplteam', loadChildren: () => import('./empl-team/empl-team.module').then((m) => m.EmplTeamModule) },
  { path: 'documentreason', loadChildren: () => import('./document-reason/document-reason.module').then((m) => m.DocumentReasonModule) },
  { path: 'bookmarkdocument', loadChildren: () => import('./bookmark-document/bookmark-document.module').then((m) => m.BookmarkDocumentModule) },

  {
    path: 'withholdingtaxtable',
    loadChildren: () => import('./withholding-tax-table/withholding-tax-table.module').then((m) => m.WithholdingTaxTableModule)
  },
  { path: 'numberseqtable', loadChildren: () => import('./number-seq-table/number-seq-table.module').then((m) => m.NumberSeqTableModule) },
  { path: 'businessunit', loadChildren: () => import('./business-unit/business-unit.module').then((m) => m.BusinessUnitModule) },
  {
    path: 'numberseqsetupbyproducttype',
    loadChildren: () =>
      import('./number-seq-setup-by-product-type/number-seq-setup-by-product-type.module').then((m) => m.NumberSeqSetupByProductTypeModule)
  },
  {
    path: 'numberseqparameter',
    loadChildren: () => import('./number-seq-parameter/number-seq-parameter.module').then((m) => m.NumberSeqParameterModule)
  },
  {
    path: 'calendargroup',
    loadChildren: () => import('./calendar-group/calendar-group.module').then((m) => m.CalendarGroupModule)
  },
  {
    path: 'calendarnonworkingdate',
    loadChildren: () => import('./calendar-non-working-date/calendar-non-working-date.module').then((m) => m.CalendarNonWorkingDateModule)
  },
  {
    path: 'companyparameter',
    loadChildren: () => import('./company-parameter/company-parameter.module').then((m) => m.CompanyParameterModule)
  },
  {
    path: 'documenttemplatetable',
    loadChildren: () => import('./document-template/document-template-table.module').then((m) => m.DocumentTemplateTableModule)
  },
  {
    path: 'bookmarkdocumenttemplatetable',
    loadChildren: () =>
      import('./bookmark-document-template-table/bookmark-document-template-table.module').then((m) => m.BookmarkDocumentTemplateTableModule)
  },
  {
    path: 'companysignature',
    loadChildren: () => import('./company-signature/company-signature.module').then((m) => m.CompanySignatureModule)
  },
  {
    path: 'invoicenumberseqsetup',
    loadChildren: () => import('./invoice-number-seq-setup/invoice-number-seq-setup.module').then((m) => m.InvoiceNumberSeqSetupModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SetupOrganizationRoutingModule {}
