import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { AddressPostalCodeItemPage } from './address-postal-code-item/address-postal-code-item.page';
import { AddressPostalCodeListPage } from './address-postal-code-list/address-postal-code.page';

const routes: Routes = [
  {
    path: '',
    component: AddressPostalCodeListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: AddressPostalCodeItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddressPostalCodeRoutingModule {}
