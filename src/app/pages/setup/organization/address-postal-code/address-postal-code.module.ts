import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddressPostalCodeRoutingModule } from './address-postal-code-routing.module';
import { AddressPostalCodeItemPage } from './address-postal-code-item/address-postal-code-item.page';
import { SharedModule } from 'shared/shared.module';
import { AddressPostalCodeListPage } from './address-postal-code-list/address-postal-code.page';
import { AddressPostalCodeComponentModule } from 'components/setup/address-postal-code/address-postal-code.module';

@NgModule({
  declarations: [AddressPostalCodeListPage, AddressPostalCodeItemPage],
  imports: [CommonModule, SharedModule, AddressPostalCodeRoutingModule, AddressPostalCodeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AddressPostalCodeModule {}
