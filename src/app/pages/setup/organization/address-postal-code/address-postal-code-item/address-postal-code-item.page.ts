import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'address-postal-code-item.page',
  templateUrl: './address-postal-code-item.page.html',
  styleUrls: ['./address-postal-code-item.page.scss']
})
export class AddressPostalCodeItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.ADDRESSPOSTALCODE, servicePath: 'AddressPostalCode' };
  }
}
