import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { BookmarkDocumentTemplateLineItemPage } from './bookmark-document-template-line-item/bookmark-document-template-line-item.page';
import { BookmarkDocumentTemplateTableItemPage } from './bookmark-document-template-table-item/bookmark-document-template-table-item.page';
import { BookmarkDocumentTemplateTableListPage } from './bookmark-document-template-table-list/bookmark-document-template-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: BookmarkDocumentTemplateTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BookmarkDocumentTemplateTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/bookmarkdocumenttemplateline-child/:id',
    component: BookmarkDocumentTemplateLineItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookmarkDocumentTemplateTableRoutingModule {}
