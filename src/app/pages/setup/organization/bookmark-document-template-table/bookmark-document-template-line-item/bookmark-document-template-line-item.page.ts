import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'bookmark-document-template-line-item-page',
  templateUrl: './bookmark-document-template-line-item.page.html',
  styleUrls: ['./bookmark-document-template-line-item.page.scss']
})
export class BookmarkDocumentTemplateLineItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.BOOKMARK_DOCUMENT_TEMPLATE_LINE, servicePath: 'BookmarkDocumentTemplateTable' };
  }
}
