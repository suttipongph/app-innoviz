import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BookmarkDocumentTemplateLineItemPage } from './bookmark-document-template-line-item.page';

describe('BookmarkDocumentTemplateTableItemPage', () => {
  let component: BookmarkDocumentTemplateLineItemPage;
  let fixture: ComponentFixture<BookmarkDocumentTemplateLineItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BookmarkDocumentTemplateLineItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkDocumentTemplateLineItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
