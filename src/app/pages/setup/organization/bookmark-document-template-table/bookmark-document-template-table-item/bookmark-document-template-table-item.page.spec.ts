import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BookmarkDocumentTemplateTableItemPage } from './bookmark-document-template-table-item.page';

describe('BookmarkDocumentTemplateTableItemPage', () => {
  let component: BookmarkDocumentTemplateTableItemPage;
  let fixture: ComponentFixture<BookmarkDocumentTemplateTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BookmarkDocumentTemplateTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkDocumentTemplateTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
