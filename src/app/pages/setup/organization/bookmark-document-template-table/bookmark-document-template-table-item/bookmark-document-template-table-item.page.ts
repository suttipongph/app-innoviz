import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'bookmark-document-template-table-item-page',
  templateUrl: './bookmark-document-template-table-item.page.html',
  styleUrls: ['./bookmark-document-template-table-item.page.scss']
})
export class BookmarkDocumentTemplateTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.BOOKMARK_DOCUMENT_TEMPLATE_TABLE,
      servicePath: 'BookmarkDocumentTemplateTable',
      childPaths: [
        {
          pagePath: ROUTE_MASTER_GEN.BOOKMARK_DOCUMENT_TEMPLATE_LINE,
          servicePath: 'BookmarkDocumentTemplateTable'
        }
      ]
    };
  }
}
