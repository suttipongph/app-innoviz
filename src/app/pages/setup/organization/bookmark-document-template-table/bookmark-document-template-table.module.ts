import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookmarkDocumentTemplateTableRoutingModule } from './bookmark-document-template-table-routing.module';
import { BookmarkDocumentTemplateTableListPage } from './bookmark-document-template-table-list/bookmark-document-template-table-list.page';
import { BookmarkDocumentTemplateTableItemPage } from './bookmark-document-template-table-item/bookmark-document-template-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { BookmarkDocumentTemplateTableComponentModule } from 'components/setup/bookmark-document-template-table/bookmark-document-template-table.module';
import { BookmarkDocumentTemplateLineItemPage } from './bookmark-document-template-line-item/bookmark-document-template-line-item.page';
import { BookmarkDocumentTemplateLineComponentModule } from 'components/setup/bookmark-document-template-line/bookmark-document-template-line.module';

@NgModule({
  declarations: [BookmarkDocumentTemplateTableListPage, BookmarkDocumentTemplateTableItemPage, BookmarkDocumentTemplateLineItemPage],
  imports: [
    CommonModule,
    SharedModule,
    BookmarkDocumentTemplateTableRoutingModule,
    BookmarkDocumentTemplateTableComponentModule,
    BookmarkDocumentTemplateLineComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BookmarkDocumentTemplateTableModule {}
