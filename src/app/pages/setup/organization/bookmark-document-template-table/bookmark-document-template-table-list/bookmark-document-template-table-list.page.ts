import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'bookmark-document-template-table-list-page',
  templateUrl: './bookmark-document-template-table-list.page.html',
  styleUrls: ['./bookmark-document-template-table-list.page.scss']
})
export class BookmarkDocumentTemplateTableListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'bookmarkDocumentTemplateTableGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.BOOKMARK_DOCUMENT_TEMPLATE_TABLE, servicePath: 'BookmarkDocumentTemplateTable' };
  }
}
