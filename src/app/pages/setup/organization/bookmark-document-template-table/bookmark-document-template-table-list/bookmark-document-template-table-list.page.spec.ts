import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BookmarkDocumentTemplateTableListPage } from './bookmark-document-template-table-list.page';

describe('BookmarkDocumentTemplateTableListPage', () => {
  let component: BookmarkDocumentTemplateTableListPage;
  let fixture: ComponentFixture<BookmarkDocumentTemplateTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BookmarkDocumentTemplateTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkDocumentTemplateTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
