import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'document-status-item.page',
  templateUrl: './document-status-item.page.html',
  styleUrls: ['./document-status-item.page.scss']
})
export class DocumentStatusItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {
    console.log('DocumentStatusItemPage');
  }

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.DOCUMENT_STATUS,
      servicePath: 'Document'
    };
  }
}
