import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentProcessRoutingModule } from './document-process-routing.module';
import { DocumentProcessListPage } from './document-process-list/document-process-list.page';
import { DocumentProcessItemPage } from './document-process-item/document-process-item.page';
import { SharedModule } from 'shared/shared.module';
import { DocumentProcessComponentModule } from 'components/setup/document-process/document-process.module';
import { DocumentStatusItemPage } from './document-status-item/document-status-item.page';
import { DocumentStatusComponentModule } from 'components/setup/document-status/document-status.module';

@NgModule({
  declarations: [DocumentProcessListPage, DocumentProcessItemPage, DocumentStatusItemPage],
  imports: [CommonModule, SharedModule, DocumentProcessRoutingModule, DocumentProcessComponentModule, DocumentStatusComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DocumentProcessModule {}
