import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'document-process-item.page',
  templateUrl: './document-process-item.page.html',
  styleUrls: ['./document-process-item.page.scss']
})
export class DocumentProcessItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.DOCUMENT_PROCESS,
      servicePath: 'Document',
      childPaths: [{ pagePath: 'documentstatus-child', servicePath: 'Document' }]
    };
  }
}
