import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DocumentProcessListPage } from './document-process-list/document-process-list.page';
import { DocumentProcessItemPage } from './document-process-item/document-process-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { DocumentStatusItemPage } from './document-status-item/document-status-item.page';

const routes: Routes = [
  {
    path: '',
    component: DocumentProcessListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: DocumentProcessItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/documentstatus-child/:id',
    component: DocumentStatusItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentProcessRoutingModule {}
