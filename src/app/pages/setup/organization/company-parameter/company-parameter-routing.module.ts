import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CompanyParameterItemPage } from './company-parameter-item/company-parameter-item.page';
import { CompanyParameterListPage } from './company-parameter-list/company-parameter.page';

const routes: Routes = [
  {
    path: '',
    component: CompanyParameterListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CompanyParameterItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyParameterRoutingModule {}
