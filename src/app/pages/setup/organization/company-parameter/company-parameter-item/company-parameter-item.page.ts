import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'company-parameter-item.page',
  templateUrl: './company-parameter-item.page.html',
  styleUrls: ['./company-parameter-item.page.scss']
})
export class CompanyParameterItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.COMPANYPARAMETER, servicePath: 'CompanyParameter' };
  }
}
