import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyParameterRoutingModule } from './company-parameter-routing.module';
import { CompanyParameterItemPage } from './company-parameter-item/company-parameter-item.page';
import { SharedModule } from 'shared/shared.module';
import { CompanyParameterComponentModule } from 'components/setup/company-parameter/company-parameter.module';
import { CompanyParameterListPage } from './company-parameter-list/company-parameter.page';

@NgModule({
  declarations: [CompanyParameterListPage, CompanyParameterItemPage],
  imports: [CommonModule, SharedModule, CompanyParameterRoutingModule, CompanyParameterComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CompanyParameterModule {}
