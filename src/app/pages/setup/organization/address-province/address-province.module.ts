import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddressProvinceRoutingModule } from './address-province-routing.module';
import { AddressProvinceItemPage } from './address-province-item/address-province-item.page';
import { SharedModule } from 'shared/shared.module';
import { AddressProvinceListPage } from './address-province-list/address-province.page';
import { AddressProvinceComponentModule } from 'components/setup/address-province/address-province.module';

@NgModule({
  declarations: [AddressProvinceListPage, AddressProvinceItemPage],
  imports: [CommonModule, SharedModule, AddressProvinceRoutingModule, AddressProvinceComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AddressProvinceModule {}
