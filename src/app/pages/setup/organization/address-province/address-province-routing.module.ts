import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';

import { AddressProvinceItemPage } from './address-province-item/address-province-item.page';
import { AddressProvinceListPage } from './address-province-list/address-province.page';

const routes: Routes = [
  {
    path: '',
    component: AddressProvinceListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: AddressProvinceItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddressProvinceRoutingModule {}
