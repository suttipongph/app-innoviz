import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NumberSeqParameterRoutingModule } from './number-seq-parameter-routing.module';
import { NumberSeqParameterItemPage } from './number-seq-parameter-item/number-seq-parameter-item.page';
import { SharedModule } from 'shared/shared.module';
import { NumberSeqParameterComponentModule } from 'components/setup/number-seq-parameter/number-seq-parameter.module';
import { NumberSeqParameterListPage } from './number-seq-parameter-list/number-seq-parameter.page';

@NgModule({
  declarations: [NumberSeqParameterListPage, NumberSeqParameterItemPage],
  imports: [CommonModule, SharedModule, NumberSeqParameterRoutingModule, NumberSeqParameterComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NumberSeqParameterModule {}
