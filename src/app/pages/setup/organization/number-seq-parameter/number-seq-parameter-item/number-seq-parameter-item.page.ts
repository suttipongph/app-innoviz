import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'number-seq-parameter-item.page',
  templateUrl: './number-seq-parameter-item.page.html',
  styleUrls: ['./number-seq-parameter-item.page.scss']
})
export class NumberSeqParameterItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.NUMBERSEQPARAMETER, servicePath: 'NumberSeqParameter' };
  }
}
