import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { NumberSeqParameterItemPage } from './number-seq-parameter-item/number-seq-parameter-item.page';
import { NumberSeqParameterListPage } from './number-seq-parameter-list/number-seq-parameter.page';

const routes: Routes = [
  {
    path: '',
    component: NumberSeqParameterListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: NumberSeqParameterItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NumberSeqParameterRoutingModule {}
