import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VerificationTypeRoutingModule } from './verification-type-routing.module';
import { VerificationTypeListPage } from './verification-type-list/verification-type-list.page';
import { VerificationTypeItemPage } from './verification-type-item/verification-type-item.page';
import { SharedModule } from 'shared/shared.module';
import { VerificationTypeComponentModule } from 'components/setup/verification-type/verification-type.module';

@NgModule({
  declarations: [VerificationTypeListPage, VerificationTypeItemPage],
  imports: [CommonModule, SharedModule, VerificationTypeRoutingModule, VerificationTypeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class VerificationTypeModule {}
