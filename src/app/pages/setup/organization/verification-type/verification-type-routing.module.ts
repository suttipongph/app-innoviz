import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { VerificationTypeItemPage } from './verification-type-item/verification-type-item.page';
import { VerificationTypeListPage } from './verification-type-list/verification-type-list.page';

const routes: Routes = [
  {
    path: '',
    component: VerificationTypeListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: VerificationTypeItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VerificationTypeRoutingModule {}
