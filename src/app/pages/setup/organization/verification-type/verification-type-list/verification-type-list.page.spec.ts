import { ComponentFixture, TestBed } from '@angular/core/testing';
import { VerificationTypeListPage } from './verification-type-list.page';

describe('VerificationTypeListPage', () => {
  let component: VerificationTypeListPage;
  let fixture: ComponentFixture<VerificationTypeListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VerificationTypeListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationTypeListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
