import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'verification-type-item-page',
  templateUrl: './verification-type-item.page.html',
  styleUrls: ['./verification-type-item.page.scss']
})
export class VerificationTypeItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.VERIFICATION_TYPE, servicePath: 'VerificationType' };
  }
}
