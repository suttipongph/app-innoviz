import { ComponentFixture, TestBed } from '@angular/core/testing';
import { VerificationTypeItemPage } from './verification-type-item.page';

describe('VerificationTypeItemPage', () => {
  let component: VerificationTypeItemPage;
  let fixture: ComponentFixture<VerificationTypeItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VerificationTypeItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationTypeItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
