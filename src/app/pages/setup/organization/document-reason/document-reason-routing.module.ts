import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { DocumentReasonItemPage } from './document-reason-item/document-reason-item.page';
import { DocumentReasonListPage } from './document-reason-list/document-reason.page';

const routes: Routes = [
  {
    path: '',
    component: DocumentReasonListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: DocumentReasonItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentReasonRoutingModule {}
