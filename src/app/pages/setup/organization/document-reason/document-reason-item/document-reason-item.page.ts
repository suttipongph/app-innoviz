import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'document-reason-item.page',
  templateUrl: './document-reason-item.page.html',
  styleUrls: ['./document-reason-item.page.scss']
})
export class DocumentReasonItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.DOCUMENTREASON, servicePath: 'DocumentReason' };
  }
}
