import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentReasonRoutingModule } from './document-reason-routing.module';
import { DocumentReasonItemPage } from './document-reason-item/document-reason-item.page';
import { SharedModule } from 'shared/shared.module';
import { DocumentReasonListPage } from './document-reason-list/document-reason.page';
import { DocumentReasonComponentModule } from 'components/setup/document-reason/document-reason.module';

@NgModule({
  declarations: [DocumentReasonListPage, DocumentReasonItemPage],
  imports: [CommonModule, SharedModule, DocumentReasonRoutingModule, DocumentReasonComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DocumentReasonModule {}
