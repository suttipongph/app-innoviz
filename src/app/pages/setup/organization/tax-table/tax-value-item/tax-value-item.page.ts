import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'tax-value-item.page',
  templateUrl: './tax-value-item.page.html',
  styleUrls: ['./tax-value-item.page.scss']
})
export class TaxValueItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.TAX_VALUE, servicePath: 'TaxTable' };
  }
}
