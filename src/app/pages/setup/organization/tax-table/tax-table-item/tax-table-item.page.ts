import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'tax-table-item.page',
  templateUrl: './tax-table-item.page.html',
  styleUrls: ['./tax-table-item.page.scss']
})
export class TaxTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.TAX_TABLE,
      servicePath: 'TaxTable',
      childPaths: [{ pagePath: ROUTE_MASTER_GEN.TAX_VALUE, servicePath: 'TaxTable' }]
    };
  }
}
