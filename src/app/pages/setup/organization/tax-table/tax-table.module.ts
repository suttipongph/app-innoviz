import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaxTableRoutingModule } from './tax-table-routing.module';
import { TaxTableListPage } from './tax-table-list/tax-table-list.page';
import { TaxTableItemPage } from './tax-table-item/tax-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { TaxTableComponentModule } from 'components/setup/tax-table/tax-table.module';
import { TaxValueItemPage } from './tax-value-item/tax-value-item.page';
import { TaxValueComponentModule } from 'components/setup/tax-value/tax-value.module';

@NgModule({
  declarations: [TaxTableListPage, TaxTableItemPage, TaxValueItemPage],
  imports: [CommonModule, SharedModule, TaxTableRoutingModule, TaxTableComponentModule, TaxValueComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TaxTableModule {}
