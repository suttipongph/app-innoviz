import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TaxTableListPage } from './tax-table-list/tax-table-list.page';
import { TaxTableItemPage } from './tax-table-item/tax-table-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { TaxValueItemPage } from './tax-value-item/tax-value-item.page';

const routes: Routes = [
  {
    path: '',
    component: TaxTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: TaxTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/taxvalue-child/:id',
    component: TaxValueItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaxTableRoutingModule {}
