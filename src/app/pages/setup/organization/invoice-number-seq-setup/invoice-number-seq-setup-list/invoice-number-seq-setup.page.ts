import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'invoice-number-seq-setup-page',
  templateUrl: './invoice-number-seq-setup.page.html',
  styleUrls: ['./invoice-number-seq-setup.page.scss']
})
export class InvoiceNumberSeqSetupListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'invoiceNumberSeqSetupGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.INVOICENUMBERSEQSETUP, servicePath: 'InvoiceNumberSeqSetup' };
  }
}
