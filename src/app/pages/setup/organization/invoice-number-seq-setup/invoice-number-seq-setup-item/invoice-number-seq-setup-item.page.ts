import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'invoice-number-seq-setup-item.page',
  templateUrl: './invoice-number-seq-setup-item.page.html',
  styleUrls: ['./invoice-number-seq-setup-item.page.scss']
})
export class InvoiceNumberSeqSetupItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.INVOICENUMBERSEQSETUP, servicePath: 'InvoiceNumberSeqSetup' };
  }
}
