import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { InvoiceNumberSeqSetupItemPage } from './invoice-number-seq-setup-item/invoice-number-seq-setup-item.page';
import { InvoiceNumberSeqSetupListPage } from './invoice-number-seq-setup-list/invoice-number-seq-setup.page';

const routes: Routes = [
  {
    path: '',
    component: InvoiceNumberSeqSetupListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: InvoiceNumberSeqSetupItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoiceNumberSeqSetupRoutingModule {}
