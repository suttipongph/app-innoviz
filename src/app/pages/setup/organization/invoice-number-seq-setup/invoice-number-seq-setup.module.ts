import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoiceNumberSeqSetupRoutingModule } from './invoice-number-seq-setup-routing.module';
import { InvoiceNumberSeqSetupItemPage } from './invoice-number-seq-setup-item/invoice-number-seq-setup-item.page';
import { SharedModule } from 'shared/shared.module';
import { InvoiceNumberSeqSetupListPage } from './invoice-number-seq-setup-list/invoice-number-seq-setup.page';
import { InvoiceNumberSeqSetupComponentModule } from 'components/setup/invoice-number-seq-setup/invoice-number-seq-setup.module';

@NgModule({
  declarations: [InvoiceNumberSeqSetupListPage, InvoiceNumberSeqSetupItemPage],
  imports: [CommonModule, SharedModule, InvoiceNumberSeqSetupRoutingModule, InvoiceNumberSeqSetupComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InvoiceNumberSeqSetupModule {}
