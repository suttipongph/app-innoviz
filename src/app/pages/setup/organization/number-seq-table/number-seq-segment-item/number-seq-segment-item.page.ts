import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'number-seq-segment-item.page',
  templateUrl: './number-seq-segment-item.page.html',
  styleUrls: ['./number-seq-segment-item.page.scss']
})
export class NumberSeqSegmentItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.NUMBERSEQSEGMENT, servicePath: 'NumberSeqTable' };
  }
}
