import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NumberSeqTableRoutingModule } from './number-seq-table-routing.module';
import { NumberSeqTableItemPage } from './number-seq-table-item/number-seq-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { NumberSeqTableComponentModule } from 'components/setup/number-seq-table/number-seq-table.module';
import { NumberSeqTableListPage } from './number-seq-table-list/number-seq-table.page';
import { NumberSeqSegmentItemPage } from './number-seq-segment-item/number-seq-segment-item.page';
import { NumberSeqSegmentComponentModule } from 'components/setup/number-seq-segment/number-seq-segment.module';

@NgModule({
  declarations: [NumberSeqTableListPage, NumberSeqTableItemPage, NumberSeqSegmentItemPage],
  imports: [CommonModule, SharedModule, NumberSeqTableRoutingModule, NumberSeqTableComponentModule, NumberSeqSegmentComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NumberSeqTableModule {}
