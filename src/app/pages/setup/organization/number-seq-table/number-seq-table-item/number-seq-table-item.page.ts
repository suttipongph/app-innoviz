import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'number-seq-table-item.page',
  templateUrl: './number-seq-table-item.page.html',
  styleUrls: ['./number-seq-table-item.page.scss']
})
export class NumberSeqTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.NUMBERSEQTABLE,
      servicePath: 'NumberSeqTable',
      childPaths: [{ pagePath: ROUTE_MASTER_GEN.NUMBERSEQSEGMENT, servicePath: 'NumberSeqTable' }]
    };
  }
}
