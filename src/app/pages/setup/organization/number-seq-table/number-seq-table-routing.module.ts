import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { NumberSeqSegmentItemPage } from './number-seq-segment-item/number-seq-segment-item.page';
import { NumberSeqTableItemPage } from './number-seq-table-item/number-seq-table-item.page';
import { NumberSeqTableListPage } from './number-seq-table-list/number-seq-table.page';

const routes: Routes = [
  {
    path: '',
    component: NumberSeqTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: NumberSeqTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/numberseqsegment-child/:id',
    component: NumberSeqSegmentItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NumberSeqTableRoutingModule {}
