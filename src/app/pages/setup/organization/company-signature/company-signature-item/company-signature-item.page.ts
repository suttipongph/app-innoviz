import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'company-signature-item.page',
  templateUrl: './company-signature-item.page.html',
  styleUrls: ['./company-signature-item.page.scss']
})
export class CompanySignatureItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.COMPANYSIGNATURE, servicePath: 'CompanySignature' };
  }
}
