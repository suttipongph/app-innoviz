import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanySignatureRoutingModule } from './company-signature-routing.module';
import { CompanySignatureItemPage } from './company-signature-item/company-signature-item.page';
import { SharedModule } from 'shared/shared.module';
import { CompanySignatureListPage } from './company-signature-list/company-signature.page';
import { CompanySignatureComponentModule } from 'components/setup/company-signature/company-signature.module';

@NgModule({
  declarations: [CompanySignatureListPage, CompanySignatureItemPage],
  imports: [CommonModule, SharedModule, CompanySignatureRoutingModule, CompanySignatureComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CompanySignatureModule {}
