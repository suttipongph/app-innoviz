import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CompanySignatureItemPage } from './company-signature-item/company-signature-item.page';
import { CompanySignatureListPage } from './company-signature-list/company-signature.page';

const routes: Routes = [
  {
    path: '',
    component: CompanySignatureListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CompanySignatureItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanySignatureRoutingModule {}
