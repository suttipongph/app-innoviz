import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { NumberSeqSetupByProductTypeItemPage } from './number-seq-setup-by-product-type-item/number-seq-setup-by-product-type-item.page';
import { NumberSeqSetupByProductTypeListPage } from './number-seq-setup-by-product-type-list/number-seq-setup-by-product-type-list.page';

const routes: Routes = [
  {
    path: '',
    component: NumberSeqSetupByProductTypeListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: NumberSeqSetupByProductTypeItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NumberSeqSetupByProductTypeRoutingModule {}
