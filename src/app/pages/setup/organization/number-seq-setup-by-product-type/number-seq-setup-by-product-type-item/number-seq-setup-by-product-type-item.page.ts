import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'number-seq-setup-by-product-type-item-page',
  templateUrl: './number-seq-setup-by-product-type-item.page.html',
  styleUrls: ['./number-seq-setup-by-product-type-item.page.scss']
})
export class NumberSeqSetupByProductTypeItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.NUMBER_SEQ_SETUP_BY_PRODUCT_TYPE, servicePath: 'NumberSeqSetupByProductType' };
  }
}
