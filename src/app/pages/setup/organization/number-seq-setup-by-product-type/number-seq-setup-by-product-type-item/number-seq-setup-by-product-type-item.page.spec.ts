import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NumberSeqSetupByProductTypeItemPage } from './number-seq-setup-by-product-type-item.page';

describe('NumberSeqSetupByProductTypeItemPage', () => {
  let component: NumberSeqSetupByProductTypeItemPage;
  let fixture: ComponentFixture<NumberSeqSetupByProductTypeItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NumberSeqSetupByProductTypeItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NumberSeqSetupByProductTypeItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
