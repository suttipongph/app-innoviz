import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'number-seq-setup-by-product-type-list-page',
  templateUrl: './number-seq-setup-by-product-type-list.page.html',
  styleUrls: ['./number-seq-setup-by-product-type-list.page.scss']
})
export class NumberSeqSetupByProductTypeListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'numberSeqSetupByProductTypeGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.NUMBER_SEQ_SETUP_BY_PRODUCT_TYPE, servicePath: 'NumberSeqSetupByProductType' };
  }
}
