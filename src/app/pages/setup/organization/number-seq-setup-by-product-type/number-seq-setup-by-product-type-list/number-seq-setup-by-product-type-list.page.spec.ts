import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NumberSeqSetupByProductTypeListPage } from './number-seq-setup-by-product-type-list.page';

describe('NumberSeqSetupByProductTypeListPage', () => {
  let component: NumberSeqSetupByProductTypeListPage;
  let fixture: ComponentFixture<NumberSeqSetupByProductTypeListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NumberSeqSetupByProductTypeListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NumberSeqSetupByProductTypeListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
