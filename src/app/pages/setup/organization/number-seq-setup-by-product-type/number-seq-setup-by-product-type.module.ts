import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NumberSeqSetupByProductTypeRoutingModule } from './number-seq-setup-by-product-type-routing.module';
import { NumberSeqSetupByProductTypeListPage } from './number-seq-setup-by-product-type-list/number-seq-setup-by-product-type-list.page';
import { NumberSeqSetupByProductTypeItemPage } from './number-seq-setup-by-product-type-item/number-seq-setup-by-product-type-item.page';
import { SharedModule } from 'shared/shared.module';
import { NumberSeqSetupByProductTypeComponentModule } from 'components/setup/number-seq-setup-by-product-type/number-seq-setup-by-product-type.module';

@NgModule({
  declarations: [NumberSeqSetupByProductTypeListPage, NumberSeqSetupByProductTypeItemPage],
  imports: [CommonModule, SharedModule, NumberSeqSetupByProductTypeRoutingModule, NumberSeqSetupByProductTypeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NumberSeqSetupByProductTypeModule {}
