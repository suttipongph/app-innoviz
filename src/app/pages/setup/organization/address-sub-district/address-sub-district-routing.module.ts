import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { AddressSubDistrictItemPage } from './address-sub-district-item/address-sub-district-item.page';
import { AddressSubDistrictListPage } from './address-sub-district-list/address-sub-district.page';

const routes: Routes = [
  {
    path: '',
    component: AddressSubDistrictListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: AddressSubDistrictItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddressSubDistrictRoutingModule {}
