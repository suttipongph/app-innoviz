import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddressSubDistrictRoutingModule } from './address-sub-district-routing.module';
import { AddressSubDistrictItemPage } from './address-sub-district-item/address-sub-district-item.page';
import { SharedModule } from 'shared/shared.module';
import { AddressSubDistrictListPage } from './address-sub-district-list/address-sub-district.page';
import { AddressSubDistrictComponentModule } from 'components/setup/address-sub-district/address-sub-district.module';

@NgModule({
  declarations: [AddressSubDistrictListPage, AddressSubDistrictItemPage],
  imports: [CommonModule, SharedModule, AddressSubDistrictRoutingModule, AddressSubDistrictComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AddressSubDistrictModule {}
