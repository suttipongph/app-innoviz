import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'withholding-tax-group-page',
  templateUrl: './withholding-tax-group.page.html',
  styleUrls: ['./withholding-tax-group.page.scss']
})
export class WithholdingTaxGroupListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'withholdingTaxGroupGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.WITHHOLDINGTAXGROUP, servicePath: 'WithholdingTaxGroup' };
  }
}
