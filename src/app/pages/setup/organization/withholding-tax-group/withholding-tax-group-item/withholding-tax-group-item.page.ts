import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'withholding-tax-group-item.page',
  templateUrl: './withholding-tax-group-item.page.html',
  styleUrls: ['./withholding-tax-group-item.page.scss']
})
export class WithholdingTaxGroupItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.WITHHOLDINGTAXGROUP, servicePath: 'WithholdingTaxGroup' };
  }
}
