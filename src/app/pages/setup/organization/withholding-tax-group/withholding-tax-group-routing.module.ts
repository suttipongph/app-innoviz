import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { WithholdingTaxGroupItemPage } from './withholding-tax-group-item/withholding-tax-group-item.page';
import { WithholdingTaxGroupListPage } from './withholding-tax-group-list/withholding-tax-group.page';

const routes: Routes = [
  {
    path: '',
    component: WithholdingTaxGroupListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: WithholdingTaxGroupItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WithholdingTaxGroupRoutingModule {}
