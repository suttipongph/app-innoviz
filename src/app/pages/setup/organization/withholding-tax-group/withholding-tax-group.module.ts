import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WithholdingTaxGroupRoutingModule } from './withholding-tax-group-routing.module';
import { WithholdingTaxGroupItemPage } from './withholding-tax-group-item/withholding-tax-group-item.page';
import { SharedModule } from 'shared/shared.module';
import { WithholdingTaxGroupListPage } from './withholding-tax-group-list/withholding-tax-group.page';
import { WithholdingTaxGroupComponentModule } from 'components/setup/withholding-tax-group/withholding-tax-group.module';

@NgModule({
  declarations: [WithholdingTaxGroupListPage, WithholdingTaxGroupItemPage],
  imports: [CommonModule, SharedModule, WithholdingTaxGroupRoutingModule, WithholdingTaxGroupComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WithholdingTaxGroupModule {}
