import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CurrencyItemPage } from './currency-item/currency-item.page';
import { CurrencyListPage } from './currency-list/currency.page';
import { ExchangeRateItemPage } from './exchange-rate-item/exchange-rate-item.page';

const routes: Routes = [
  {
    path: '',
    component: CurrencyListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CurrencyItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/exchangerate-child/:id',
    component: ExchangeRateItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CurrencyRoutingModule {}
