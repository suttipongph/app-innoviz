import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'exchange-rate-item.page',
  templateUrl: './exchange-rate-item.page.html',
  styleUrls: ['./exchange-rate-item.page.scss']
})
export class ExchangeRateItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.EXCHANGERATE, servicePath: 'Currency' };
  }
}
