import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CurrencyRoutingModule } from './currency-routing.module';
import { CurrencyItemPage } from './currency-item/currency-item.page';
import { SharedModule } from 'shared/shared.module';
import { CurrencyListPage } from './currency-list/currency.page';
import { CurrencyComponentModule } from 'components/setup/currency/currency.module';
import { ExchangeRateItemPage } from './exchange-rate-item/exchange-rate-item.page';
import { ExchangeRateComponentModule } from 'components/setup/exchange-rate/exchange-rate.module';

@NgModule({
  declarations: [CurrencyListPage, CurrencyItemPage, ExchangeRateItemPage],
  imports: [CommonModule, SharedModule, CurrencyRoutingModule, CurrencyComponentModule, ExchangeRateComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CurrencyModule {}
