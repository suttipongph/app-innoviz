import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IntercompanyRoutingModule } from './intercompany-routing.module';
import { IntercompanyListPage } from './intercompany-list/intercompany-list.page';
import { IntercompanyItemPage } from './intercompany-item/intercompany-item.page';
import { SharedModule } from 'shared/shared.module';
import { IntercompanyComponentModule } from 'components/setup/intercompany/intercompany.module';

@NgModule({
  declarations: [IntercompanyListPage, IntercompanyItemPage],
  imports: [CommonModule, SharedModule, IntercompanyRoutingModule, IntercompanyComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IntercompanyModule {}
