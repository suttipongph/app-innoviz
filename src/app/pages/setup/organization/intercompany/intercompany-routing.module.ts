import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { IntercompanyItemPage } from './intercompany-item/intercompany-item.page';
import { IntercompanyListPage } from './intercompany-list/intercompany-list.page';

const routes: Routes = [
  {
    path: '',
    component: IntercompanyListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: IntercompanyItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntercompanyRoutingModule {}
