import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'intercompany-item-page',
  templateUrl: './intercompany-item.page.html',
  styleUrls: ['./intercompany-item.page.scss']
})
export class IntercompanyItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.INTERCOMPANY, servicePath: 'Intercompany' };
  }
}
