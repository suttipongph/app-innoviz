import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'masterinformation',
    loadChildren: () => import('./master-information/master-information.module').then((m) => m.MasterInformationModule)
  },
  {
    path: 'organization',
    loadChildren: () => import('./organization/setup-organization.module').then((m) => m.SetupOrganizationModule)
  },
  {
    path: 'creditapplication',
    loadChildren: () => import('./credit-application/credit-application.module').then((m) => m.CreditApplicationModule)
  },
  {
    path: 'invoiceandcollection',
    loadChildren: () => import('./invoice-and-collection/invoice-and-collection.module').then((m) => m.InvoiceAndCollectionModule)
  },
  {
    path: 'accounting',
    loadChildren: () => import('./accounting/accounting.module').then((m) => m.AccountingModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SetupRoutingModule {}
