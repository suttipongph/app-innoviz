import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentConditionTemplateTableRoutingModule } from './document-condition-template-table-routing.module';
import { DocumentConditionTemplateTableListPage } from './document-condition-template-table-list/document-condition-template-table-list.page';
import { DocumentConditionTemplateTableItemPage } from './document-condition-template-table-item/document-condition-template-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { DocumentConditionTemplateTableComponentModule } from 'components/setup/document-condition-template-table/document-condition-template-table.module';
import { DocumentConditionTemplateLineItemPage } from './document-condition-template-line-item/document-condition-template-line-item.page';
import { DocumentConditionTemplateLineComponentModule } from 'components/setup/document-condition-template-line/document-condition-template-line.module';

@NgModule({
  declarations: [DocumentConditionTemplateTableListPage, DocumentConditionTemplateTableItemPage, DocumentConditionTemplateLineItemPage],
  imports: [
    CommonModule,
    SharedModule,
    DocumentConditionTemplateTableRoutingModule,
    DocumentConditionTemplateTableComponentModule,
    DocumentConditionTemplateLineComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DocumentConditionTemplateTableModule {}
