import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'document-condition-template-line-item-page',
  templateUrl: './document-condition-template-line-item.page.html',
  styleUrls: ['./document-condition-template-line-item.page.scss']
})
export class DocumentConditionTemplateLineItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.DOCUMENT_CONDITION_TEMPLATE_LINE, servicePath: 'DocumentConditionTemplateTable' };
  }
}
