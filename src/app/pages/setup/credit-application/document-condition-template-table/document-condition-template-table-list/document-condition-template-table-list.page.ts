import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'document-condition-template-table-list-page',
  templateUrl: './document-condition-template-table-list.page.html',
  styleUrls: ['./document-condition-template-table-list.page.scss']
})
export class DocumentConditionTemplateTableListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'documentConditionTemplateTableGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.DOCUMENT_CONDITION_TEMPLATE_TABLE, servicePath: 'DocumentConditionTemplateTable' };
  }
}
