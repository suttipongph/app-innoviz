import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DocumentConditionTemplateTableListPage } from './document-condition-template-table-list.page';

describe('DocumentConditionTemplateTableListPage', () => {
  let component: DocumentConditionTemplateTableListPage;
  let fixture: ComponentFixture<DocumentConditionTemplateTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DocumentConditionTemplateTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentConditionTemplateTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
