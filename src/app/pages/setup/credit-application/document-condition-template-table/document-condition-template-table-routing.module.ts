import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { DocumentConditionTemplateLineItemPage } from './document-condition-template-line-item/document-condition-template-line-item.page';
import { DocumentConditionTemplateTableItemPage } from './document-condition-template-table-item/document-condition-template-table-item.page';
import { DocumentConditionTemplateTableListPage } from './document-condition-template-table-list/document-condition-template-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: DocumentConditionTemplateTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: DocumentConditionTemplateTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/documentconditiontemplateline-child/:id',
    component: DocumentConditionTemplateLineItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentConditionTemplateTableRoutingModule {}
