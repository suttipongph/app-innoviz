import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DocumentConditionTemplateTableItemPage } from './document-condition-template-table-item.page';

describe('DocumentConditionTemplateTableItemPage', () => {
  let component: DocumentConditionTemplateTableItemPage;
  let fixture: ComponentFixture<DocumentConditionTemplateTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DocumentConditionTemplateTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentConditionTemplateTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
