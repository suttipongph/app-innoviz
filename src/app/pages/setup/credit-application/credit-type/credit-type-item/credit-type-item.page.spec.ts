import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditTypeItemPage } from './credit-type-item.page';

describe('CreditTypeItemPage', () => {
  let component: CreditTypeItemPage;
  let fixture: ComponentFixture<CreditTypeItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditTypeItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditTypeItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
