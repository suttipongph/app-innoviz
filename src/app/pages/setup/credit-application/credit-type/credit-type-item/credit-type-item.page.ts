import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'credit-type-item-page',
  templateUrl: './credit-type-item.page.html',
  styleUrls: ['./credit-type-item.page.scss']
})
export class CreditTypeItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.CREDIT_TYPE, servicePath: 'CreditType' };
  }
}
