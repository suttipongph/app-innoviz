import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditTypeRoutingModule } from './credit-type-routing.module';
import { CreditTypeListPage } from './credit-type-list/credit-type-list.page';
import { CreditTypeItemPage } from './credit-type-item/credit-type-item.page';
import { SharedModule } from 'shared/shared.module';
import { CreditTypeComponentModule } from 'components/setup/credit-type/credit-type.module';

@NgModule({
  declarations: [CreditTypeListPage, CreditTypeItemPage],
  imports: [CommonModule, SharedModule, CreditTypeRoutingModule, CreditTypeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditTypeModule {}
