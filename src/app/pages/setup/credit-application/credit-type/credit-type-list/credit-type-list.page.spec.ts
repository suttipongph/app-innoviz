import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditTypeListPage } from './credit-type-list.page';

describe('CreditTypeListPage', () => {
  let component: CreditTypeListPage;
  let fixture: ComponentFixture<CreditTypeListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditTypeListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditTypeListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
