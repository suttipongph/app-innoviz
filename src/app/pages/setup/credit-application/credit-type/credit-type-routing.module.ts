import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CreditTypeItemPage } from './credit-type-item/credit-type-item.page';
import { CreditTypeListPage } from './credit-type-list/credit-type-list.page';

const routes: Routes = [
  {
    path: '',
    component: CreditTypeListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CreditTypeItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditTypeRoutingModule {}
