import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductSubTypeRoutingModule } from './product-sub-type-routing.module';
import { ProductSubTypeListPage } from './product-sub-type-list/product-sub-type-list.page';
import { ProductSubTypeItemPage } from './product-sub-type-item/product-sub-type-item.page';
import { SharedModule } from 'shared/shared.module';
import { ProductSubTypeComponentModule } from 'components/setup/product-sub-type/product-sub-type.module';

@NgModule({
  declarations: [ProductSubTypeListPage, ProductSubTypeItemPage],
  imports: [CommonModule, SharedModule, ProductSubTypeRoutingModule, ProductSubTypeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProductSubTypeModule {}
