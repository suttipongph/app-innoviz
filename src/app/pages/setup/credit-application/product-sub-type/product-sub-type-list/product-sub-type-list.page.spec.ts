import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ProductSubTypeListPage } from './product-sub-type-list.page';

describe('ProductSubTypeListPage', () => {
  let component: ProductSubTypeListPage;
  let fixture: ComponentFixture<ProductSubTypeListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductSubTypeListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSubTypeListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
