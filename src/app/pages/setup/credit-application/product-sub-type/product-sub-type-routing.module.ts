import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ProductSubTypeItemPage } from './product-sub-type-item/product-sub-type-item.page';
import { ProductSubTypeListPage } from './product-sub-type-list/product-sub-type-list.page';

const routes: Routes = [
  {
    path: '',
    component: ProductSubTypeListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ProductSubTypeItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductSubTypeRoutingModule {}
