import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ProductSubTypeItemPage } from './product-sub-type-item.page';

describe('ProductSubTypeItemPage', () => {
  let component: ProductSubTypeItemPage;
  let fixture: ComponentFixture<ProductSubTypeItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductSubTypeItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSubTypeItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
