import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'billing-responsible-by-item-page',
  templateUrl: './billing-responsible-by-item.page.html',
  styleUrls: ['./billing-responsible-by-item.page.scss']
})
export class BillingResponsibleByItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.BILLING_RESPONSIBLE_BY, servicePath: 'BillingResponsibleBy' };
  }
}
