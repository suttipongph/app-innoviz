import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BillingResponsibleByItemPage } from './billing-responsible-by-item.page';

describe('BillingResponsibleByItemPage', () => {
  let component: BillingResponsibleByItemPage;
  let fixture: ComponentFixture<BillingResponsibleByItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BillingResponsibleByItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingResponsibleByItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
