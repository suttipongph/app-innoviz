import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BillingResponsibleByRoutingModule } from './billing-responsible-by-routing.module';
import { BillingResponsibleByListPage } from './billing-responsible-by-list/billing-responsible-by-list.page';
import { BillingResponsibleByItemPage } from './billing-responsible-by-item/billing-responsible-by-item.page';
import { SharedModule } from 'shared/shared.module';
import { BillingResponsibleByComponentModule } from 'components/setup/billing-responsible-by/billing-responsible-by.module';

@NgModule({
  declarations: [BillingResponsibleByListPage, BillingResponsibleByItemPage],
  imports: [CommonModule, SharedModule, BillingResponsibleByRoutingModule, BillingResponsibleByComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BillingResponsibleByModule {}
