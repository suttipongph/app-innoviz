import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { BillingResponsibleByItemPage } from './billing-responsible-by-item/billing-responsible-by-item.page';
import { BillingResponsibleByListPage } from './billing-responsible-by-list/billing-responsible-by-list.page';

const routes: Routes = [
  {
    path: '',
    component: BillingResponsibleByListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BillingResponsibleByItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillingResponsibleByRoutingModule {}
