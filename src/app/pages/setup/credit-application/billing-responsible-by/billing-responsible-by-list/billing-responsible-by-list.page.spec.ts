import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BillingResponsibleByListPage } from './billing-responsible-by-list.page';

describe('BillingResponsibleByListPage', () => {
  let component: BillingResponsibleByListPage;
  let fixture: ComponentFixture<BillingResponsibleByListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BillingResponsibleByListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingResponsibleByListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
