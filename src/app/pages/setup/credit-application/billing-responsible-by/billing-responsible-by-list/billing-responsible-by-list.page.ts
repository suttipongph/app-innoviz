import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'billing-responsible-by-list-page',
  templateUrl: './billing-responsible-by-list.page.html',
  styleUrls: ['./billing-responsible-by-list.page.scss']
})
export class BillingResponsibleByListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'billingResponsibleByGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.BILLING_RESPONSIBLE_BY, servicePath: 'BillingResponsibleBy' };
  }
}
