import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CreditTermItemPage } from './credit-term-item/credit-term-item.page';
import { CreditTermListPage } from './credit-term-list/credit-term-list.page';

const routes: Routes = [
  {
    path: '',
    component: CreditTermListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CreditTermItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditTermRoutingModule {}
