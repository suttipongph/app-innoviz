import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditTermListPage } from './credit-term-list.page';

describe('CreditTermListPage', () => {
  let component: CreditTermListPage;
  let fixture: ComponentFixture<CreditTermListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditTermListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditTermListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
