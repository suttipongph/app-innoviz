import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditTermItemPage } from './credit-term-item.page';

describe('CreditTermItemPage', () => {
  let component: CreditTermItemPage;
  let fixture: ComponentFixture<CreditTermItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditTermItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditTermItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
