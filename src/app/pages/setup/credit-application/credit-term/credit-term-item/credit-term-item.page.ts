import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'credit-term-item-page',
  templateUrl: './credit-term-item.page.html',
  styleUrls: ['./credit-term-item.page.scss']
})
export class CreditTermItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.CREDIT_TERM, servicePath: 'CreditTerm' };
  }
}
