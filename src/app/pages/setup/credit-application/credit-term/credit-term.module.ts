import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditTermRoutingModule } from './credit-term-routing.module';
import { CreditTermListPage } from './credit-term-list/credit-term-list.page';
import { CreditTermItemPage } from './credit-term-item/credit-term-item.page';
import { SharedModule } from 'shared/shared.module';
import { CreditTermComponentModule } from 'components/setup/credit-term/credit-term.module';

@NgModule({
  declarations: [CreditTermListPage, CreditTermItemPage],
  imports: [CommonModule, SharedModule, CreditTermRoutingModule, CreditTermComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditTermModule {}
