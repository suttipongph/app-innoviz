import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'method-of-payment-item.page',
  templateUrl: './method-of-payment-item.page.html',
  styleUrls: ['./method-of-payment-item.page.scss']
})
export class MethodOfPaymentItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.METHODOFPAYMENT, servicePath: 'MethodOfPayment' };
  }
}
