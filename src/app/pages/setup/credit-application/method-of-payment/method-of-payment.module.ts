import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MethodOfPaymentRoutingModule } from './method-of-payment-routing.module';
import { MethodOfPaymentItemPage } from './method-of-payment-item/method-of-payment-item.page';
import { SharedModule } from 'shared/shared.module';
import { MethodOfPaymentListPage } from './method-of-payment-list/method-of-payment.page';
import { MethodOfPaymentComponentModule } from 'components/setup/method-of-payment/method-of-payment.module';

@NgModule({
  declarations: [MethodOfPaymentListPage, MethodOfPaymentItemPage],
  imports: [CommonModule, SharedModule, MethodOfPaymentRoutingModule, MethodOfPaymentComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MethodOfPaymentModule {}
