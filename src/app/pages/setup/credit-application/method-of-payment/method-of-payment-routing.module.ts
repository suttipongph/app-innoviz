import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { MethodOfPaymentItemPage } from './method-of-payment-item/method-of-payment-item.page';
import { MethodOfPaymentListPage } from './method-of-payment-list/method-of-payment.page';

const routes: Routes = [
  {
    path: '',
    component: MethodOfPaymentListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: MethodOfPaymentItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MethodOfPaymentRoutingModule {}
