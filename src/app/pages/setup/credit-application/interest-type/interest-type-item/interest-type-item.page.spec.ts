import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InterestTypeItemPage } from './interest-type-item.page';

describe('InterestTypeItemPage', () => {
  let component: InterestTypeItemPage;
  let fixture: ComponentFixture<InterestTypeItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InterestTypeItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestTypeItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
