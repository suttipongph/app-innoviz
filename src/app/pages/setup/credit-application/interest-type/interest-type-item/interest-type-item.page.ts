import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'interest-type-item-page',
  templateUrl: './interest-type-item.page.html',
  styleUrls: ['./interest-type-item.page.scss']
})
export class InterestTypeItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.INTEREST_TYPE,
      servicePath: 'InterestType',
      childPaths: [{ pagePath: ROUTE_MASTER_GEN.INTEREST_TYPE_VALUE, servicePath: 'InterestType' }]
    };
  }
}
