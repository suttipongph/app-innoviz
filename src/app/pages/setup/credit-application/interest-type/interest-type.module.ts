import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InterestTypeRoutingModule } from './interest-type-routing.module';
import { InterestTypeListPage } from './interest-type-list/interest-type-list.page';
import { InterestTypeItemPage } from './interest-type-item/interest-type-item.page';
import { SharedModule } from 'shared/shared.module';
import { InterestTypeComponentModule } from 'components/setup/interest-type/interest-type.module';
import { InterestTypeValueItemPage } from './interest-type-value-item/interest-type-value-item.page';
import { InterestTypeValueComponentModule } from 'components/setup/interest-type-value/interest-type-value.module';

@NgModule({
  declarations: [InterestTypeListPage, InterestTypeItemPage, InterestTypeValueItemPage],
  imports: [CommonModule, SharedModule, InterestTypeRoutingModule, InterestTypeComponentModule, InterestTypeValueComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InterestTypeModule {}
