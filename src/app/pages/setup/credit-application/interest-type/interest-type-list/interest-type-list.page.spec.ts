import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InterestTypeListPage } from './interest-type-list.page';

describe('InterestTypeListPage', () => {
  let component: InterestTypeListPage;
  let fixture: ComponentFixture<InterestTypeListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InterestTypeListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestTypeListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
