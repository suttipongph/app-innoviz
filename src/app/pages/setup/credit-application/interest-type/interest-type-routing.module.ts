import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { InterestTypeItemPage } from './interest-type-item/interest-type-item.page';
import { InterestTypeListPage } from './interest-type-list/interest-type-list.page';
import { InterestTypeValueItemPage } from './interest-type-value-item/interest-type-value-item.page';

const routes: Routes = [
  {
    path: '',
    component: InterestTypeListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: InterestTypeItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/interesttypevalue-child/:id',
    component: InterestTypeValueItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InterestTypeRoutingModule {}
