import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'interest-type-value-item-page',
  templateUrl: './interest-type-value-item.page.html',
  styleUrls: ['./interest-type-value-item.page.scss']
})
export class InterestTypeValueItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.INTEREST_TYPE_VALUE,
      servicePath: 'InterestType'
    };
  }
}
