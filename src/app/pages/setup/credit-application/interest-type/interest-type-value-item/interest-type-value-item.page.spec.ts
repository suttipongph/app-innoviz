import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InterestTypeValueItemPage } from './interest-type-value-item.page';

describe('InterestTypeItemPage', () => {
  let component: InterestTypeValueItemPage;
  let fixture: ComponentFixture<InterestTypeValueItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InterestTypeValueItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestTypeValueItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
