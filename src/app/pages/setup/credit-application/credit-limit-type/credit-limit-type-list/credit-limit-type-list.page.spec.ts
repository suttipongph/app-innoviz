import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditLimitTypeListPage } from './credit-limit-type-list.page';

describe('CreditLimitTypeListPage', () => {
  let component: CreditLimitTypeListPage;
  let fixture: ComponentFixture<CreditLimitTypeListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditLimitTypeListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditLimitTypeListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
