import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditLimitTypeRoutingModule } from './credit-limit-type-routing.module';
import { CreditLimitTypeListPage } from './credit-limit-type-list/credit-limit-type-list.page';
import { CreditLimitTypeItemPage } from './credit-limit-type-item/credit-limit-type-item.page';
import { SharedModule } from 'shared/shared.module';
import { CreditLimitTypeComponentModule } from 'components/setup/credit-limit-type/credit-limit-type.module';

@NgModule({
  declarations: [CreditLimitTypeListPage, CreditLimitTypeItemPage],
  imports: [CommonModule, SharedModule, CreditLimitTypeRoutingModule, CreditLimitTypeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditLimitTypeModule {}
