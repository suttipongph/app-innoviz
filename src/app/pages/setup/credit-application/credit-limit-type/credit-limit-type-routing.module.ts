import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CreditLimitTypeItemPage } from './credit-limit-type-item/credit-limit-type-item.page';
import { CreditLimitTypeListPage } from './credit-limit-type-list/credit-limit-type-list.page';

const routes: Routes = [
  {
    path: '',
    component: CreditLimitTypeListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CreditLimitTypeItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditLimitTypeRoutingModule {}
