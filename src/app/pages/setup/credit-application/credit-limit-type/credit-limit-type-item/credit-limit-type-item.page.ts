import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'credit-limit-type-item-page',
  templateUrl: './credit-limit-type-item.page.html',
  styleUrls: ['./credit-limit-type-item.page.scss']
})
export class CreditLimitTypeItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.CREDIT_LIMIT_TYPE, servicePath: 'CreditLimitType' };
  }
}
