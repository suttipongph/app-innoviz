import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditLimitTypeItemPage } from './credit-limit-type-item.page';

describe('CreditLimitTypeItemPage', () => {
  let component: CreditLimitTypeItemPage;
  let fixture: ComponentFixture<CreditLimitTypeItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditLimitTypeItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditLimitTypeItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
