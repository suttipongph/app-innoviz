import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServiceFeeCondTemplateTableRoutingModule } from './service-fee-cond-template-table-routing.module';
import { ServiceFeeCondTemplateTableListPage } from './service-fee-cond-template-table-list/service-fee-cond-template-table-list.page';
import { ServiceFeeCondTemplateTableItemPage } from './service-fee-cond-template-table-item/service-fee-cond-template-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { ServiceFeeCondTemplateTableComponentModule } from 'components/setup/service-fee-cond-template-table/service-fee-cond-template-table.module';
import { ServiceFeeCondTemplateLineComponentModule } from 'components/setup/service-fee-cond-template-line/service-fee-cond-template-line.module';
import { ServiceFeeCondTemplateLineItemPage } from './service-fee-cond-template-line-item/service-fee-cond-template-line-item.page';

@NgModule({
  declarations: [ServiceFeeCondTemplateTableListPage, ServiceFeeCondTemplateTableItemPage, ServiceFeeCondTemplateLineItemPage],
  imports: [
    CommonModule,
    SharedModule,
    ServiceFeeCondTemplateTableRoutingModule,
    ServiceFeeCondTemplateTableComponentModule,
    ServiceFeeCondTemplateLineComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ServiceFeeCondTemplateTableModule {}
