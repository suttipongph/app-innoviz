import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceFeeCondTemplateLineItemPage } from './service-fee-cond-template-line-item.page';

describe('ServiceFeeCondTemplateLineItemComponent', () => {
  let component: ServiceFeeCondTemplateLineItemPage;
  let fixture: ComponentFixture<ServiceFeeCondTemplateLineItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ServiceFeeCondTemplateLineItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceFeeCondTemplateLineItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
