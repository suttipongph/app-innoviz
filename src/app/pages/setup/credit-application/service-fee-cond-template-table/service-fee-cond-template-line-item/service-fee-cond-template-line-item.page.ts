import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-service-fee-cond-template-line-item',
  templateUrl: './service-fee-cond-template-line-item.page.html',
  styleUrls: ['./service-fee-cond-template-line-item.page.scss']
})
export class ServiceFeeCondTemplateLineItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.SERVICE_FEE_COND_TEMPLATE_LINE, servicePath: 'ServiceFeeCondTemplateTable' };
  }
}
