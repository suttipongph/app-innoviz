import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'service-fee-cond-template-table-item-page',
  templateUrl: './service-fee-cond-template-table-item.page.html',
  styleUrls: ['./service-fee-cond-template-table-item.page.scss']
})
export class ServiceFeeCondTemplateTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.SERVICE_FEE_COND_TEMPLATE_TABLE,
      servicePath: 'ServiceFeeCondTemplateTable',
      childPaths: [
        {
          pagePath: ROUTE_MASTER_GEN.SERVICE_FEE_COND_TEMPLATE_LINE,
          servicePath: 'ServiceFeeCondTemplateTable'
        }
      ]
    };
  }
}
