import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ServiceFeeCondTemplateTableItemPage } from './service-fee-cond-template-table-item.page';

describe('ServiceFeeCondTemplateTableItemPage', () => {
  let component: ServiceFeeCondTemplateTableItemPage;
  let fixture: ComponentFixture<ServiceFeeCondTemplateTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ServiceFeeCondTemplateTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceFeeCondTemplateTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
