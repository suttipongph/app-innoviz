import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ServiceFeeCondTemplateLineItemPage } from './service-fee-cond-template-line-item/service-fee-cond-template-line-item.page';
import { ServiceFeeCondTemplateTableItemPage } from './service-fee-cond-template-table-item/service-fee-cond-template-table-item.page';
import { ServiceFeeCondTemplateTableListPage } from './service-fee-cond-template-table-list/service-fee-cond-template-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: ServiceFeeCondTemplateTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ServiceFeeCondTemplateTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/servicefeecondtemplateline-child/:id',
    component: ServiceFeeCondTemplateLineItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceFeeCondTemplateTableRoutingModule {}
