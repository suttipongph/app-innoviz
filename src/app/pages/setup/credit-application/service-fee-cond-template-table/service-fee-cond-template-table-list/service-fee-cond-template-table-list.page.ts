import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'service-fee-cond-template-table-list-page',
  templateUrl: './service-fee-cond-template-table-list.page.html',
  styleUrls: ['./service-fee-cond-template-table-list.page.scss']
})
export class ServiceFeeCondTemplateTableListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'serviceFeeCondTemplateTableGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.SERVICE_FEE_COND_TEMPLATE_TABLE, servicePath: 'ServiceFeeCondTemplateTable' };
  }
}
