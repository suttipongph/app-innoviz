import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ServiceFeeCondTemplateTableListPage } from './service-fee-cond-template-table-list.page';

describe('ServiceFeeCondTemplateTableListPage', () => {
  let component: ServiceFeeCondTemplateTableListPage;
  let fixture: ComponentFixture<ServiceFeeCondTemplateTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ServiceFeeCondTemplateTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceFeeCondTemplateTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
