import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditApplicationRoutingModule } from './credit-application-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, CreditApplicationRoutingModule]
})
export class CreditApplicationModule {}
