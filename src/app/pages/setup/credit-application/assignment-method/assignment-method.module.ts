import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssignmentMethodRoutingModule } from './assignment-method-routing.module';
import { AssignmentMethodListPage } from './assignment-method-list/assignment-method-list.page';
import { AssignmentMethodItemPage } from './assignment-method-item/assignment-method-item.page';
import { SharedModule } from 'shared/shared.module';
import { AssignmentMethodComponentModule } from 'components/setup/assignment-method/assignment-method.module';

@NgModule({
  declarations: [AssignmentMethodListPage, AssignmentMethodItemPage],
  imports: [CommonModule, SharedModule, AssignmentMethodRoutingModule, AssignmentMethodComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AssignmentMethodModule {}
