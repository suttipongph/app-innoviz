import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'assignment-method-item-page',
  templateUrl: './assignment-method-item.page.html',
  styleUrls: ['./assignment-method-item.page.scss']
})
export class AssignmentMethodItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.ASSIGNMENT_METHOD, servicePath: 'AssignmentMethod' };
  }
}
