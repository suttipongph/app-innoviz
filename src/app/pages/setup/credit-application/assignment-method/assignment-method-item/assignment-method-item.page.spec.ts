import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AssignmentMethodItemPage } from './assignment-method-item.page';

describe('AssignmentMethodItemPage', () => {
  let component: AssignmentMethodItemPage;
  let fixture: ComponentFixture<AssignmentMethodItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AssignmentMethodItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentMethodItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
