import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AssignmentMethodListPage } from './assignment-method-list.page';

describe('AssignmentMethodListPage', () => {
  let component: AssignmentMethodListPage;
  let fixture: ComponentFixture<AssignmentMethodListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AssignmentMethodListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentMethodListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
