import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { AssignmentMethodItemPage } from './assignment-method-item/assignment-method-item.page';
import { AssignmentMethodListPage } from './assignment-method-list/assignment-method-list.page';

const routes: Routes = [
  {
    path: '',
    component: AssignmentMethodListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: AssignmentMethodItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssignmentMethodRoutingModule {}
