import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'assignmentmethod', loadChildren: () => import('./assignment-method/assignment-method.module').then((m) => m.AssignmentMethodModule) },
  {
    path: 'billingresponsibleby',
    loadChildren: () => import('./billing-responsible-by/billing-responsible-by.module').then((m) => m.BillingResponsibleByModule)
  },
  { path: 'creditlimittype', loadChildren: () => import('./credit-limit-type/credit-limit-type.module').then((m) => m.CreditLimitTypeModule) },
  { path: 'creditterm', loadChildren: () => import('./credit-term/credit-term.module').then((m) => m.CreditTermModule) },
  { path: 'interesttype', loadChildren: () => import('./interest-type/interest-type.module').then((m) => m.InterestTypeModule) },
  { path: 'productsubtype', loadChildren: () => import('./product-sub-type/product-sub-type.module').then((m) => m.ProductSubTypeModule) },
  { path: 'credittype', loadChildren: () => import('./credit-type/credit-type.module').then((m) => m.CreditTypeModule) },
  {
    path: 'retentionconditionsetup',
    loadChildren: () => import('./retention-condition-setup/retention-condition-setup.module').then((m) => m.RetentionConditionSetupModule)
  },
  { path: 'methodofpayment', loadChildren: () => import('./method-of-payment/method-of-payment.module').then((m) => m.MethodOfPaymentModule) },
  {
    path: 'servicefeecondtemplatetable',
    loadChildren: () =>
      import('./service-fee-cond-template-table/service-fee-cond-template-table.module').then((m) => m.ServiceFeeCondTemplateTableModule)
  },
  {
    path: 'documentconditiontemplatetable',
    loadChildren: () =>
      import('./document-condition-template-table/document-condition-template-table.module').then((m) => m.DocumentConditionTemplateTableModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditApplicationRoutingModule {}
