import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'retention-condition-setup-list-page',
  templateUrl: './retention-condition-setup-list.page.html',
  styleUrls: ['./retention-condition-setup-list.page.scss']
})
export class RetentionConditionSetupListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'retentionConditionSetupGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.RETENTION_CONDITION_SETUP, servicePath: 'RetentionConditionSetup' };
  }
}
