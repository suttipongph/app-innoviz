import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RetentionConditionSetupListPage } from './retention-condition-setup-list.page';

describe('RetentionConditionSetupListPage', () => {
  let component: RetentionConditionSetupListPage;
  let fixture: ComponentFixture<RetentionConditionSetupListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RetentionConditionSetupListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RetentionConditionSetupListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
