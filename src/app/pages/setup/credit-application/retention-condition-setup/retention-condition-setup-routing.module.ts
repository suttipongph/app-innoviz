import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { RetentionConditionSetupItemPage } from './retention-condition-setup-item/retention-condition-setup-item.page';
import { RetentionConditionSetupListPage } from './retention-condition-setup-list/retention-condition-setup-list.page';

const routes: Routes = [
  {
    path: '',
    component: RetentionConditionSetupListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: RetentionConditionSetupItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RetentionConditionSetupRoutingModule {}
