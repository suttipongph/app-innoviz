import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RetentionConditionSetupRoutingModule } from './retention-condition-setup-routing.module';
import { RetentionConditionSetupListPage } from './retention-condition-setup-list/retention-condition-setup-list.page';
import { RetentionConditionSetupItemPage } from './retention-condition-setup-item/retention-condition-setup-item.page';
import { SharedModule } from 'shared/shared.module';
import { RetentionConditionSetupComponentModule } from 'components/setup/retention-condition-setup/retention-condition-setup.module';

@NgModule({
  declarations: [RetentionConditionSetupListPage, RetentionConditionSetupItemPage],
  imports: [CommonModule, SharedModule, RetentionConditionSetupRoutingModule, RetentionConditionSetupComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RetentionConditionSetupModule {}
