import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RetentionConditionSetupItemPage } from './retention-condition-setup-item.page';

describe('RetentionConditionSetupItemPage', () => {
  let component: RetentionConditionSetupItemPage;
  let fixture: ComponentFixture<RetentionConditionSetupItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RetentionConditionSetupItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RetentionConditionSetupItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
