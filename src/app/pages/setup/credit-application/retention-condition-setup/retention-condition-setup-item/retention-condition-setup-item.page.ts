import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'retention-condition-setup-item-page',
  templateUrl: './retention-condition-setup-item.page.html',
  styleUrls: ['./retention-condition-setup-item.page.scss']
})
export class RetentionConditionSetupItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.RETENTION_CONDITION_SETUP, servicePath: 'RetentionConditionSetup' };
  }
}
