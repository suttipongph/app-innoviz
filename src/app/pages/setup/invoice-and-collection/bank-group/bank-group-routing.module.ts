import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BankGroupPage } from './bank-group-list/bank-group.page';
import { BankGroupItemPage } from './bank-group-item/bank-group-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuardService],
    component: BankGroupPage
  },
  {
    path: ':id',
    canActivate: [AuthGuardService],
    component: BankGroupItemPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BankGroupRoutingModule {}
