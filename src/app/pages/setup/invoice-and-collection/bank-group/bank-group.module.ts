import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BankGroupRoutingModule } from './bank-group-routing.module';
import { BankGroupPage } from './bank-group-list/bank-group.page';
import { BankGroupItemPage } from './bank-group-item/bank-group-item.page';
import { SharedModule } from 'shared/shared.module';
import { BankGroupComponentModule } from 'components/setup/bank-group/bank-group.module';

@NgModule({
  declarations: [BankGroupPage, BankGroupItemPage],
  imports: [CommonModule, SharedModule, BankGroupRoutingModule, BankGroupComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BankGroupModule {}
