import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'financialinstitution',
    loadChildren: () => import('./bank-group/bank-group.module').then((m) => m.BankGroupModule)
  },
  {
    path: 'invoicerevenuetype',
    loadChildren: () => import('./invoice-revenue-type/invoice-revenue-type.module').then((m) => m.InvoiceRevenueTypeModule)
  },
  {
    path: 'invoicetype',
    loadChildren: () => import('./invoice-type/invoice-type.module').then((m) => m.InvoiceTypeModule)
  },
  {
    path: 'jobtype',
    loadChildren: () => import('./job-type/job-type.module').then((m) => m.JobTypeModule)
  },
  {
    path: 'documentreturnmethod',
    loadChildren: () => import('./document-return-method/document-return-method.module').then((m) => m.DocumentReturnMethodModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoiceAndCollectionRoutingModule {}
