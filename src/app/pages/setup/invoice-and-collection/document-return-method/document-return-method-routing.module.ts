import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { DocumentReturnMethodItemPage } from './document-return-method-item/document-return-method-item.page';
import { DocumentReturnMethodListPage } from './document-return-method-list/document-return-method-list.page';

const routes: Routes = [
  {
    path: '',
    component: DocumentReturnMethodListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: DocumentReturnMethodItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentReturnMethodRoutingModule {}
