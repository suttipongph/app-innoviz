import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentReturnMethodRoutingModule } from './document-return-method-routing.module';
import { DocumentReturnMethodListPage } from './document-return-method-list/document-return-method-list.page';
import { DocumentReturnMethodItemPage } from './document-return-method-item/document-return-method-item.page';
import { SharedModule } from 'shared/shared.module';
import { DocumentReturnMethodComponentModule } from 'components/setup/document-return-method/document-return-method.module';

@NgModule({
  declarations: [DocumentReturnMethodListPage, DocumentReturnMethodItemPage],
  imports: [CommonModule, SharedModule, DocumentReturnMethodRoutingModule, DocumentReturnMethodComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DocumentReturnMethodModule {}
