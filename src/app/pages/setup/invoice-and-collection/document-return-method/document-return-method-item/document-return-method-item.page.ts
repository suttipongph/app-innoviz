import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'document-return-method-item-page',
  templateUrl: './document-return-method-item.page.html',
  styleUrls: ['./document-return-method-item.page.scss']
})
export class DocumentReturnMethodItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.DOCUMENT_RETURN_METHOD, servicePath: 'DocumentReturnMethod' };
  }
}
