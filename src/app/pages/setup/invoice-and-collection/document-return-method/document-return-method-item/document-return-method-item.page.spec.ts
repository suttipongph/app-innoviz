import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DocumentReturnMethodItemPage } from './document-return-method-item.page';

describe('DocumentReturnMethodItemPage', () => {
  let component: DocumentReturnMethodItemPage;
  let fixture: ComponentFixture<DocumentReturnMethodItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DocumentReturnMethodItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentReturnMethodItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
