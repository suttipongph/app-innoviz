import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DocumentReturnMethodListPage } from './document-return-method-list.page';

describe('DocumentReturnMethodListPage', () => {
  let component: DocumentReturnMethodListPage;
  let fixture: ComponentFixture<DocumentReturnMethodListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DocumentReturnMethodListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentReturnMethodListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
