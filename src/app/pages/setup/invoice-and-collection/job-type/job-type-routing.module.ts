import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { JobTypeItemPage } from './job-type-item/job-type-item.page';
import { JobTypeListPage } from './job-type-list/job-type-list.page';

const routes: Routes = [
  {
    path: '',
    component: JobTypeListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: JobTypeItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobTypeRoutingModule {}
