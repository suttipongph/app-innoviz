import { ComponentFixture, TestBed } from '@angular/core/testing';
import { JobTypeListPage } from './job-type-list.page';

describe('JobTypeListPage', () => {
  let component: JobTypeListPage;
  let fixture: ComponentFixture<JobTypeListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [JobTypeListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobTypeListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
