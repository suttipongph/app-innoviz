import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobTypeRoutingModule } from './job-type-routing.module';
import { JobTypeListPage } from './job-type-list/job-type-list.page';
import { JobTypeItemPage } from './job-type-item/job-type-item.page';
import { SharedModule } from 'shared/shared.module';
import { JobTypeComponentModule } from 'components/setup/job-type/job-type.module';

@NgModule({
  declarations: [JobTypeListPage, JobTypeItemPage],
  imports: [CommonModule, SharedModule, JobTypeRoutingModule, JobTypeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JobTypeModule {}
