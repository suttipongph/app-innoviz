import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'job-type-item-page',
  templateUrl: './job-type-item.page.html',
  styleUrls: ['./job-type-item.page.scss']
})
export class JobTypeItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.JOB_TYPE, servicePath: 'JobType' };
  }
}
