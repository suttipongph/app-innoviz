import { ComponentFixture, TestBed } from '@angular/core/testing';
import { JobTypeItemPage } from './job-type-item.page';

describe('JobTypeItemPage', () => {
  let component: JobTypeItemPage;
  let fixture: ComponentFixture<JobTypeItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [JobTypeItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobTypeItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
