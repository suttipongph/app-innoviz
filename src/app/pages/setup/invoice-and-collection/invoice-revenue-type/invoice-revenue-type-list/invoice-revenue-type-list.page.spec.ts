import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InvoiceRevenueTypeListPage } from './invoice-revenue-type-list.page';

describe('InvoiceRevenueTypeListPage', () => {
  let component: InvoiceRevenueTypeListPage;
  let fixture: ComponentFixture<InvoiceRevenueTypeListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InvoiceRevenueTypeListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceRevenueTypeListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
