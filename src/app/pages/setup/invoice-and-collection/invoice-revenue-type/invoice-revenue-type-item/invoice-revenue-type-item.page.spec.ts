import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InvoiceRevenueTypeItemPage } from './invoice-revenue-type-item.page';

describe('InvoiceRevenueTypeItemPage', () => {
  let component: InvoiceRevenueTypeItemPage;
  let fixture: ComponentFixture<InvoiceRevenueTypeItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InvoiceRevenueTypeItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceRevenueTypeItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
