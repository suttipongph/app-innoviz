import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'invoice-revenue-type-item-page',
  templateUrl: './invoice-revenue-type-item.page.html',
  styleUrls: ['./invoice-revenue-type-item.page.scss']
})
export class InvoiceRevenueTypeItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.INVOICE_REVENUE_TYPE, servicePath: 'InvoiceRevenueType' };
  }
}
