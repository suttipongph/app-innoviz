import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoiceRevenueTypeRoutingModule } from './invoice-revenue-type-routing.module';
import { InvoiceRevenueTypeListPage } from './invoice-revenue-type-list/invoice-revenue-type-list.page';
import { InvoiceRevenueTypeItemPage } from './invoice-revenue-type-item/invoice-revenue-type-item.page';
import { SharedModule } from 'shared/shared.module';
import { InvoiceRevenueTypeComponentModule } from 'components/setup/invoice-revenue-type/invoice-revenue-type.module';

@NgModule({
  declarations: [InvoiceRevenueTypeListPage, InvoiceRevenueTypeItemPage],
  imports: [CommonModule, SharedModule, InvoiceRevenueTypeRoutingModule, InvoiceRevenueTypeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InvoiceRevenueTypeModule {}
