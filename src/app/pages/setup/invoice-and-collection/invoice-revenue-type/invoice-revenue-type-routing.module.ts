import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { InvoiceRevenueTypeItemPage } from './invoice-revenue-type-item/invoice-revenue-type-item.page';
import { InvoiceRevenueTypeListPage } from './invoice-revenue-type-list/invoice-revenue-type-list.page';

const routes: Routes = [
  {
    path: '',
    component: InvoiceRevenueTypeListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: InvoiceRevenueTypeItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoiceRevenueTypeRoutingModule {}
