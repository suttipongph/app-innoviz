import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoiceTypeRoutingModule } from './invoice-type-routing.module';
import { InvoiceTypeItemPage } from './invoice-type-item/invoice-type-item.page';
import { SharedModule } from 'shared/shared.module';
import { InvoiceTypeComponentModule } from 'components/invoice-type/invoice-type.module';
import { InvoiceTypeListPage } from './invoice-type-list/invoice-type.page';

@NgModule({
  declarations: [InvoiceTypeListPage, InvoiceTypeItemPage],
  imports: [CommonModule, SharedModule, InvoiceTypeRoutingModule, InvoiceTypeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InvoiceTypeModule {}
