import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InvoiceTypeItemPage } from './invoice-type-item/invoice-type-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { InvoiceTypeListPage } from './invoice-type-list/invoice-type.page';

const routes: Routes = [
  {
    path: '',
    component: InvoiceTypeListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: InvoiceTypeItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoiceTypeRoutingModule {}
