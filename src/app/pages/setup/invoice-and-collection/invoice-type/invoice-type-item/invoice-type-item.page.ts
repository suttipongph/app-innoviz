import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'invoice-type-item.page',
  templateUrl: './invoice-type-item.page.html',
  styleUrls: ['./invoice-type-item.page.scss']
})
export class InvoiceTypeItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.INVOICETYPE, servicePath: 'InvoiceType' };
  }
}
