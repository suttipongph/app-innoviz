import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LoanRequestAgreementPage } from './loan-request-agreement.page';

describe('LoanRequestAgreementPage', () => {
  let component: LoanRequestAgreementPage;
  let fixture: ComponentFixture<LoanRequestAgreementPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoanRequestAgreementPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanRequestAgreementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
