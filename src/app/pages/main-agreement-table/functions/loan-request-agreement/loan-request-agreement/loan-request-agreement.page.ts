import { Component, OnInit } from '@angular/core';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel, PathParamModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-loan-request-agreement',
  templateUrl: './loan-request-agreement.page.html',
  styleUrls: ['./loan-request-agreement.page.scss']
})
export class LoanRequestAgreementPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.LOAN_REQUEST_AGREEMENT,
      servicePath: `MainAgreementTable/${getProductType(this.masterRoute, true)}/${this.currentRoute}/Function/loanrequestagreement`
    };
  }
}
