import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { LoanRequestAgreementFunctionRoutingModule } from './loan-request-agreement-function-routing.module';
import { SharedModule } from 'shared/shared.module';
import { LoanRequestAgreementPage } from './loan-request-agreement/loan-request-agreement.page';
import { CommonModule } from '@angular/common';
import { GenMainAgmLoanRequestComponentModule } from 'components/main-agreement-table/gen-main-agm-loan-request/gen-main-agm-loan-request.module';

@NgModule({
  declarations: [LoanRequestAgreementPage],
  imports: [CommonModule, SharedModule, LoanRequestAgreementFunctionRoutingModule, GenMainAgmLoanRequestComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LoanRequestAgreementFunctionModule {}
