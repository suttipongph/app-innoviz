import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { LoanRequestAgreementPage } from './loan-request-agreement/loan-request-agreement.page';

const routes: Routes = [
  {
    path: '',
    component: LoanRequestAgreementPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoanRequestAgreementFunctionRoutingModule {}
