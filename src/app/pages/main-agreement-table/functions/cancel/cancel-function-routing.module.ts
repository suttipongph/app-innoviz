import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CancelAgreementPage } from './cancel/cancel.page';

const routes: Routes = [
  {
    path: '',
    component: CancelAgreementPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainAgreementCancelFunctionRoutingModule {}
