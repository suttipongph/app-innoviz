import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddendumFunctionRoutingModule } from './addendum-function-routing.module';
import { AddendumPage } from './addendum/addendum.page';
import { SharedModule } from 'shared/shared.module';
import { GenMainAgmAddendumComponentModule } from 'components/main-agreement-table/main-agm-addendum/main-agm-addendum.module';

@NgModule({
  declarations: [AddendumPage],
  imports: [CommonModule, SharedModule, AddendumFunctionRoutingModule, GenMainAgmAddendumComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AddendumFunctionModule {}
