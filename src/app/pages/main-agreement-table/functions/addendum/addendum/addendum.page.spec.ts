import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AddendumPage } from './addendum.page';

describe('AddendumPage', () => {
  let component: AddendumPage;
  let fixture: ComponentFixture<AddendumPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AddendumPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddendumPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
