import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainAgreementTableFunctionRoutingModule } from './main-agreement-table-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, MainAgreementTableFunctionRoutingModule]
})
export class MainAgreementTableFunctionModule {}
