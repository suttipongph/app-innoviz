import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MainAgreementTableListPage } from './main-agreement-table-list.page';

describe('MainAgreementTableListPage', () => {
  let component: MainAgreementTableListPage;
  let fixture: ComponentFixture<MainAgreementTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MainAgreementTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainAgreementTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
