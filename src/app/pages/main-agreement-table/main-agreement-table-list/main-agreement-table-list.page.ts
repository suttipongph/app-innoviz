import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { AgreementDocType, ColumnType, ROUTE_MASTER_GEN, SortType } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'main-agreement-table-list-page',
  templateUrl: './main-agreement-table-list.page.html',
  styleUrls: ['./main-agreement-table-list.page.scss']
})
export class MainAgreementTableListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'mainAgreementTableGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'productType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: getProductType(this.masterRoute)
      }
    ];

    this.setColumnOptionByNewMainAgreement(columns);
    this.setColumnOptionByAllMainAgreement(columns);

    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: `/${getProductType(this.masterRoute, true)}/${this.currentRoute}/${ROUTE_MASTER_GEN.MAIN_AGREEMENT_TABLE}`,
      servicePath: `${ROUTE_MASTER_GEN.MAIN_AGREEMENT_TABLE}/${getProductType(this.masterRoute, true)}/${this.currentRoute}`
    };
  }

  setColumnOptionByNewMainAgreement(columns: ColumnModel[]) {
    if (this.currentRoute == 'newmainagreementtable') {
      columns.push({
        label: null,
        textKey: 'agreementDocType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: AgreementDocType.New.toString()
      });
    }
  }
  setColumnOptionByAllMainAgreement(columns: ColumnModel[]) {
    if (this.currentRoute == 'allmainagreementtable') {
      this.option.canCreate = false;
    }
  }
}
