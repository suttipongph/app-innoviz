import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MainAgreementTableItemPage } from './main-agreement-table-item.page';

describe('MainAgreementTableItemPage', () => {
  let component: MainAgreementTableItemPage;
  let fixture: ComponentFixture<MainAgreementTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MainAgreementTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainAgreementTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
