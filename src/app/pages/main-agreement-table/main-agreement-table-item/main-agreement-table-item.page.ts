import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'main-agreement-table-item-page',
  templateUrl: './main-agreement-table-item.page.html',
  styleUrls: ['./main-agreement-table-item.page.scss']
})
export class MainAgreementTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    const productTypeWording = getProductType(this.masterRoute, true);
    this.pageInfo = {
      pagePath: `/${getProductType(this.masterRoute, true)}/${this.currentRoute}/${ROUTE_MASTER_GEN.MAIN_AGREEMENT_TABLE}`,
      servicePath: `${ROUTE_MASTER_GEN.MAIN_AGREEMENT_TABLE}/${productTypeWording}/${this.currentRoute}`
    };
  }
}
