import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainAgreementTableRoutingModule } from './main-agreement-table-routing.module';
import { MainAgreementTableListPage } from './main-agreement-table-list/main-agreement-table-list.page';
import { MainAgreementTableItemPage } from './main-agreement-table-item/main-agreement-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { MainAgreementTableComponentModule } from 'components/main-agreement-table/main-agreement-table.module';

@NgModule({
  declarations: [MainAgreementTableListPage, MainAgreementTableItemPage],
  imports: [CommonModule, SharedModule, MainAgreementTableRoutingModule, MainAgreementTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MainAgreementTableModule {}
