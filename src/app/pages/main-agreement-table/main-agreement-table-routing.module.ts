import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { MainAgreementTableItemPage } from './main-agreement-table-item/main-agreement-table-item.page';
import { MainAgreementTableListPage } from './main-agreement-table-list/main-agreement-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: MainAgreementTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: MainAgreementTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/main-agreement-table-relatedinfo.module').then((m) => m.MainAgreementTableRelatedinfoModule)
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/main-agreement-table-function.module').then((m) => m.MainAgreementTableFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainAgreementTableRoutingModule {}
