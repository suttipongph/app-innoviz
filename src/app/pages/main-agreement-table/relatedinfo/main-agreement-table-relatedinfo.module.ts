import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainAgreementTableRelatedinfoRoutingModule } from './main-agreement-table-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, MainAgreementTableRelatedinfoRoutingModule]
})
export class MainAgreementTableRelatedinfoModule {}
