import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-guarantor-agreement-line-item',
  templateUrl: './guarantor-agreement-line-item.page.html',
  styleUrls: ['./guarantor-agreement-line-item.page.scss']
})
export class GuarantorAgreementLineItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.GUARANTOR_AGREEMENT_LINE,
      servicePath: `MainAgreementTable/${getProductType(this.masterRoute, true)}/${this.currentRoute}/RelatedInfo/GuarantorAgreementTable`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.GUARANTOR_AGREEMENT_LINE_AFFLIATE,
          servicePath: `MainAgreementTable/${getProductType(this.masterRoute, true)}/${this.currentRoute}/RelatedInfo/GuarantorAgreementTable`
        }
      ]
    };
  }
}
