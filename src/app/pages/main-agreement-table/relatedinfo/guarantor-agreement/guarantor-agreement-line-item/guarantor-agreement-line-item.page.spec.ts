import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuarantorAgreementLineItemPage } from './guarantor-agreement-line-item.page';

describe('GuarantorAgreementLineItemComponent', () => {
  let component: GuarantorAgreementLineItemPage;
  let fixture: ComponentFixture<GuarantorAgreementLineItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GuarantorAgreementLineItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuarantorAgreementLineItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
