import { GuarantorAgreementLineAffliateItemPage } from './guarantor-agreement-line-affliate-item/guarantor-agreement-line-affliate-item.page';
import { GuarantorAgreementTableListComponent } from '../../../../components/guarantor-agreement/guarantor-agreement-table/guarantor-agreement-table-list/guarantor-agreement-table-list.component';
import { GuarantorAgreementLineListCustomComponent } from '../../../../components/guarantor-agreement/guarantor-agreement-line/guarantor-agreement-line-list/guarantor-agreement-line-list-custom.component';
import { AuthGuardService } from '../../../../core/services/auth-guard.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GuarantorAgreementListPage } from './guarantor-agreement-list/guarantor-agreement-list.page';
import { GuarantorAgreementItemPage } from './guarantor-agreement-item/guarantor-agreement-item.page';
import { GuarantorAgreementLineItemPage } from './guarantor-agreement-line-item/guarantor-agreement-line-item.page';

const routes: Routes = [
  {
    path: '',
    component: GuarantorAgreementListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: GuarantorAgreementItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/guarantoragreementline-child/:id',
    component: GuarantorAgreementLineItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id',
    component: GuarantorAgreementLineAffliateItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/guarantor-agreement-function.module').then((m) => m.MainAgreementTableGuarantorAgreementFunctionModule)
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/guarantor-agreement-relatedinfo.module').then((m) => m.GuarantorAgreementMemoTransRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainAgreementTableGuarantorAgreementRelatedinfoRoutingModule {}
