import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GuarantorAgreementListPage } from './guarantor-agreement-list.page';

describe('GuarantorAgreementListPage', () => {
  let component: GuarantorAgreementListPage;
  let fixture: ComponentFixture<GuarantorAgreementListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GuarantorAgreementListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuarantorAgreementListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
