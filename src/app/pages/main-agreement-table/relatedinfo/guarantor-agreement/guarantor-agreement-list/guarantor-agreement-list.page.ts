import { getProductType } from 'shared/functions/value.function';
import { ROUTE_RELATED_GEN } from './../../../../../shared/constants/constantGen';
import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';
import { UIControllerService } from 'core/services/uiController.service';

@Component({
  selector: 'guarantor-agreement-list-page',
  templateUrl: './guarantor-agreement-list.page.html',
  styleUrls: ['./guarantor-agreement-list.page.scss']
})
export class GuarantorAgreementListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'guarantorAgreementTableGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.GUARANTOR_AGREEMENT_TABLE,
      servicePath: `MainAgreementTable/${getProductType(this.masterRoute, true)}/${this.currentRoute}/RelatedInfo/GuarantorAgreementTable`
    };
  }
}
