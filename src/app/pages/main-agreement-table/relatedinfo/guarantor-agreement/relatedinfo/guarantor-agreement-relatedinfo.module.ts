import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GuarantorAgreementRelatedinfoRoutingModule } from './guarantor-agreement-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, GuarantorAgreementRelatedinfoRoutingModule]
})
export class GuarantorAgreementMemoTransRelatedinfoModule {}
