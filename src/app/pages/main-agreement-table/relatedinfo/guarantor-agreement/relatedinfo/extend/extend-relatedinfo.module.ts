import { GuarantorAgreementLineAffliateItemPage } from './../../guarantor-agreement-line-affliate-item/guarantor-agreement-line-affliate-item.page';
import { GuarantorAgreementTableComponentModule } from 'components/guarantor-agreement/guarantor-agreement-table/guarantor-agreement-table.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExtendRelatedinfoRoutingModule } from './extend-relatedinfo-routing.module';
import { ExtendListPage } from './extend-list/extend-list.page';
import { ExtendItemPage } from './extend-item/extend-item.page';
import { SharedModule } from 'shared/shared.module';
import { ExtendGuarantorAgreementComponentModule } from 'components/guarantor-agreement/extend-guarantor-agreement/extend-guarantor-agreement.module';
import { GuarantorAgreementLineComponentModule } from 'components/guarantor-agreement/guarantor-agreement-line/guarantor-agreement-line.module';
import { ExtendLineItemPage } from './extend-line-item/extend-line-item.page';
import { ExtendLineAffliateItemPage } from './extend-line-affliate-item/extend-line-affliate-item.page';
import { GuarantorAgreementLineAffiliateComponentModule } from 'components/guarantor-agreement/guarantor-agreement-line-affiliate/guarantor-agreement-line-affiliate.module';

@NgModule({
  declarations: [ExtendListPage, ExtendItemPage, ExtendLineItemPage, ExtendLineAffliateItemPage],
  imports: [
    CommonModule,
    SharedModule,
    ExtendRelatedinfoRoutingModule,
    GuarantorAgreementTableComponentModule,
    GuarantorAgreementLineComponentModule,
    GuarantorAgreementLineAffiliateComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExtendRelatedinfoModule {}
