import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-guarantor-agreement-line-item',
  templateUrl: './extend-line-item.page.html',
  styleUrls: ['./extend-line-item.page.scss']
})
export class ExtendLineItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.EXTEND_LINE,
      servicePath: `GuarantorAgreementTable/${getProductType(this.masterRoute, true)}/${
        this.currentRoute
      }/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.EXTEND_LINE,
          servicePath: `GuarantorAgreementTable/${getProductType(this.masterRoute, true)}/${
            this.currentRoute
          }/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend`
        }
      ]
    };
  }
}
