import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CloseAgreementPage } from './close.page';

describe('ClosePage', () => {
  let component: CloseAgreementPage;
  let fixture: ComponentFixture<CloseAgreementPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CloseAgreementPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CloseAgreementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
