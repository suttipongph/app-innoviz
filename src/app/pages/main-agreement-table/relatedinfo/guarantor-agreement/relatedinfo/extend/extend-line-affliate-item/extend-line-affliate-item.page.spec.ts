import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtendLineAffliateItemPage } from './extend-line-affliate-item.page';

describe('GuarantorAgreementLineAffliateItemPage', () => {
  let component: ExtendLineAffliateItemPage;
  let fixture: ComponentFixture<ExtendLineAffliateItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExtendLineAffliateItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtendLineAffliateItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
