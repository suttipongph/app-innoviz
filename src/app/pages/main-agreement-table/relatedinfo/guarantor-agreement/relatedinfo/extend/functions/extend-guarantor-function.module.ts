import { ExtendGuarantorAgreementComponentModule } from 'components/guarantor-agreement/extend-guarantor-agreement/extend-guarantor-agreement.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenerateExtendGuarantorFunctionRoutingModule } from './extend-guarantor-function-routing.module';
import { SharedModule } from 'shared/shared.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, GenerateExtendGuarantorFunctionRoutingModule],

  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GenerateExtendGuarantorFunctionModule {}
