import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ExtendGuarantorPage } from './extend-guarantor.page';

describe('GenerateExtendGuarantorPage', () => {
  let component: ExtendGuarantorPage;
  let fixture: ComponentFixture<ExtendGuarantorPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExtendGuarantorPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtendGuarantorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
