import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ExtendGuarantorPage } from './extend-guarantor/extend-guarantor.page';

const routes: Routes = [
  {
    path: '',
    component: ExtendGuarantorPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GenerateExtendGuarantorFunctionRoutingModule {}
