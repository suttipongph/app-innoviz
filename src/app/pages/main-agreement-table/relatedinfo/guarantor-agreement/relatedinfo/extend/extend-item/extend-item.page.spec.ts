import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ExtendItemPage } from './extend-item.page';

describe('ExtendItemPage', () => {
  let component: ExtendItemPage;
  let fixture: ComponentFixture<ExtendItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExtendItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtendItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
