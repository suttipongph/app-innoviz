import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CopyCustVisitingPage } from './copy-bookmark-document-from-template.page';

describe('CopyCustVisitingPage', () => {
  let component: CopyCustVisitingPage;
  let fixture: ComponentFixture<CopyCustVisitingPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CopyCustVisitingPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyCustVisitingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
