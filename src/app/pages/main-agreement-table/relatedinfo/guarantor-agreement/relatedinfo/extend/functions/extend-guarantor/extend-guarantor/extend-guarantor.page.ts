import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PathParamModel } from 'shared/models/systemModel';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';

@Component({
  selector: 'extend-guarantor',
  templateUrl: './extend-guarantor.page.html',
  styleUrls: ['./extend-guarantor.page.scss']
})
export class ExtendGuarantorPage implements OnInit {
  pageInfo: PageInformationModel;
  currentRoute = this.uiService.getMasterRoute(1);

  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.GENERATE_EXTEND_GUARANTOR,
      servicePath: `${ROUTE_MASTER_GEN.MAIN_AGREEMENT_TABLE}/${getProductType(this.masterRoute, true)}/${
        this.currentRoute
      }/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/${ROUTE_FUNCTION_GEN.GENERATE_EXTEND_GUARANTOR}`
    };
  }
}
