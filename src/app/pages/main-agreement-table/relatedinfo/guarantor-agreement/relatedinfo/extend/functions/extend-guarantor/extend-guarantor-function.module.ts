import { ExtendGuarantorAgreementComponentModule } from 'components/guarantor-agreement/extend-guarantor-agreement/extend-guarantor-agreement.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenerateExtendGuarantorFunctionRoutingModule } from './extend-guarantor-function-routing.module';
import { SharedModule } from 'shared/shared.module';
import { ExtendGuarantorPage } from './extend-guarantor/extend-guarantor.page';

@NgModule({
  declarations: [ExtendGuarantorPage],
  imports: [CommonModule, SharedModule, GenerateExtendGuarantorFunctionRoutingModule, ExtendGuarantorAgreementComponentModule],

  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GenerateExtendGuarantorFunctionModule {}
