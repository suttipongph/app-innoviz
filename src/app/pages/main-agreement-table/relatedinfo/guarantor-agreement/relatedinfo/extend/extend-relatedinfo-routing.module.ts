import { AuthGuardService } from './../../../../../../core/services/auth-guard.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';
import { ExtendListPage } from './extend-list/extend-list.page';
import { ExtendItemPage } from './extend-item/extend-item.page';
import { ExtendLineItemPage } from './extend-line-item/extend-line-item.page';
import { ExtendLineAffliateItemPage } from './extend-line-affliate-item/extend-line-affliate-item.page';

const routes: Routes = [
  {
    path: '',
    component: ExtendListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ExtendItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/guarantoragreementline-child/:id',
    component: ExtendLineItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id',
    component: ExtendLineAffliateItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/extend-guarantor-function.module').then((m) => m.GenerateExtendGuarantorFunctionModule)
  },
  {
    path: 'function',
    loadChildren: () => import('./functions/extend-guarantor-function.module').then((m) => m.GenerateExtendGuarantorFunctionModule)
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/guarantor-agreement-relatedinfo.module').then((m) => m.GuarantorAgreementExtendRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExtendRelatedinfoRoutingModule {}
