import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'memotrans',
    loadChildren: () => import('./memo/guarantor-agreement-memo-relatedinfo.module').then((m) => m.GuarantorAgreementMemoTransRelatedinfoModule)
  },
  {
    path: 'bookmarkdocumenttrans',
    loadChildren: () =>
      import('./bookmark-document-trans/main-agreement-table-bookmark-document-trans-relatedinfo.module').then(
        (m) => m.MainAgreementTableBookmarkDocumentRelatedinfoModule
      )
  },
  {
    path: 'attachment',
    loadChildren: () => import('./attachment/guarantor-agm-table-attachment.module').then((m) => m.GuarantorAgmTableAttachmentModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuarantorAgreementExtendRelatedinfoRoutingModule {}
