import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtendLineItemPage } from './extend-line-item.page';

describe('GuarantorAgreementLineItemComponent', () => {
  let component: ExtendLineItemPage;
  let fixture: ComponentFixture<ExtendLineItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExtendLineItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtendLineItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
