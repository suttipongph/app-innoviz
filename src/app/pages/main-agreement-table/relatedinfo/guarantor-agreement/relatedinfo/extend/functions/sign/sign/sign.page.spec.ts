import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SignAgreementPage } from './sign.page';

describe('SignPage', () => {
  let component: SignAgreementPage;
  let fixture: ComponentFixture<SignAgreementPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SignAgreementPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignAgreementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
