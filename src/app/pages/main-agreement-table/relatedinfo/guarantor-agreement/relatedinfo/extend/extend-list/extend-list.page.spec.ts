import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ExtendListPage } from './extend-list.page';

describe('ExtendListPage', () => {
  let component: ExtendListPage;
  let fixture: ComponentFixture<ExtendListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExtendListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtendListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
