import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-extend-line-affliate-item',
  templateUrl: './extend-line-affliate-item.page.html',
  styleUrls: ['./extend-line-affliate-item.page.scss']
})
export class ExtendLineAffliateItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.EXTEND_LINE_AFFILIATE,
      servicePath: `GuarantorAgreementTable/${getProductType(this.masterRoute, true)}/${
        this.currentRoute
      }/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.EXTEND_LINE_AFFILIATE,
          servicePath: `GuarantorAgreementTable/${getProductType(this.masterRoute, true)}/${
            this.currentRoute
          }/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend`
        }
      ]
    };
  }
}
