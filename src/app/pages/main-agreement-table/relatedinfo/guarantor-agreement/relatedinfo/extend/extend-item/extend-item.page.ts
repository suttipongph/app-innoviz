import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'extend-item.page',
  templateUrl: './extend-item.page.html',
  styleUrls: ['./extend-item.page.scss']
})
export class ExtendItemPage implements OnInit {
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  pageInfo: PageInformationModel;
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.EXTEND,
      servicePath: `GuarantorAgreementTable/${getProductType(this.masterRoute, true)}/${
        this.currentRoute
      }/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.GUARANTOR_AGREEMENT_LINE,
          servicePath: `GuarantorAgreementTable/${getProductType(this.masterRoute, true)}/${
            this.currentRoute
          }/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend`
        }
      ]
    };
  }
}
