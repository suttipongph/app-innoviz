import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GuarantorAgreementPostFunctionRoutingModule } from './post-function-routing.module';
import { PostAgreementPage } from './post/post.page';
import { SharedModule } from 'shared/shared.module';
import { ManageAgreementComponentModule } from 'components/function/manage-agreement/manage-agreement.module';

@NgModule({
  declarations: [PostAgreementPage],
  imports: [CommonModule, SharedModule, GuarantorAgreementPostFunctionRoutingModule, ManageAgreementComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GuarantorAgreementPostFunctionModule {}
