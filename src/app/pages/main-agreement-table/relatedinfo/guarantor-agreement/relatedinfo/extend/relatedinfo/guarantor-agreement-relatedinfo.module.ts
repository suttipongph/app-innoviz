import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GuarantorAgreementExtendRelatedinfoRoutingModule } from './guarantor-agreement-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, GuarantorAgreementExtendRelatedinfoRoutingModule]
})
export class GuarantorAgreementExtendRelatedinfoModule {}
