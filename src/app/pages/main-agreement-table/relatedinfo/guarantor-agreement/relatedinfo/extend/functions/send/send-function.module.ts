import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GuarantorAgreementSendFunctionRoutingModule } from './send-function-routing.module';
import { SendAgreementPage } from './send/send.page';
import { SharedModule } from 'shared/shared.module';
import { ManageAgreementComponentModule } from 'components/function/manage-agreement/manage-agreement.module';

@NgModule({
  declarations: [SendAgreementPage],
  imports: [CommonModule, SharedModule, GuarantorAgreementSendFunctionRoutingModule, ManageAgreementComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GuarantorAgreementSendFunctionModule {}
