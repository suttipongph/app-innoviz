import { Component } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { PathParamModel } from 'shared/models/systemModel';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';

@Component({
  selector: 'app-copy-guarantor-from-ca',
  templateUrl: './copy-guarantor-from-ca.page.html',
  styleUrls: ['./copy-guarantor-from-ca.page.scss']
})
export class CopyGuarantorFromCaPage extends BaseItemComponent<any> {
  pageInfo: PageInformationModel;
  currentRoute = this.uiService.getMasterRoute(1);

  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {
    super();
    // this.path = ROUTE_FUNCTION_GEN.GUARANTOR_AGREEMENT_TABLE_COPY_GUARANTOR_FROM_CA;
  }
  onRelatedMenu(): void {
    const path: PathParamModel = {};
    this.toRelatedInfo(path);
  }
  toRelatedInfo(param: PathParamModel): void {
    super.toRelatedInfo(param);
  }
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.GUARANTOR_AGREEMENT_TABLE_COPY_GUARANTOR_FROM_CA,
      servicePath: `${ROUTE_MASTER_GEN.MAIN_AGREEMENT_TABLE}/${getProductType(this.masterRoute, true)}/${
        this.currentRoute
      }/RelatedInfo/GuarantorAgreementTable/Function/${ROUTE_FUNCTION_GEN.GUARANTOR_AGREEMENT_TABLE_COPY_GUARANTOR_FROM_CA}`
    };
  }
}
