import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookmarkDocumentListPage } from './bookmark-document-list/bookmark-document-list.page';
import { BookmarkDocumentItemPage } from './bookmark-document-item/bookmark-document-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: BookmarkDocumentListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BookmarkDocumentItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/bookmark-document-trans-function.module').then((m) => m.BookmarkDocumentFunctionModule)
  },
  {
    path: 'function',
    loadChildren: () => import('./functions/bookmark-document-trans-function.module').then((m) => m.BookmarkDocumentFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainAgreementTableBookmarkDocumentRelatedinfoRoutingModule {}
