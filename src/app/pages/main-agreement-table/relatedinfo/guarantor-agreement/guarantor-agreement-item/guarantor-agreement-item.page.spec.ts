import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GuarantorAgreementItemPage } from './guarantor-agreement-item.page';

describe('GuarantorAgreementItemPage', () => {
  let component: GuarantorAgreementItemPage;
  let fixture: ComponentFixture<GuarantorAgreementItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GuarantorAgreementItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuarantorAgreementItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
