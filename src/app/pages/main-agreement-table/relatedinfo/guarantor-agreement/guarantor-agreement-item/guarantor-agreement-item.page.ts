import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from './../../../../../shared/constants/constantGen';
import { Component, OnInit } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { UIControllerService } from 'core/services/uiController.service';
import { getProductType } from 'shared/functions/value.function';

@Component({
  selector: 'guarantor-agreement-item.page',
  templateUrl: './guarantor-agreement-item.page.html',
  styleUrls: ['./guarantor-agreement-item.page.scss']
})
export class GuarantorAgreementItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.GUARANTOR_AGREEMENT_TABLE,
      servicePath: `MainAgreementTable/${getProductType(this.masterRoute, true)}/${this.currentRoute}/RelatedInfo/GuarantorAgreementTable`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.GUARANTOR_AGREEMENT_LINE,
          servicePath: `MainAgreementTable/${getProductType(this.masterRoute, true)}/${this.currentRoute}/RelatedInfo/GuarantorAgreementTable`
        }
      ]
    };
  }
}
