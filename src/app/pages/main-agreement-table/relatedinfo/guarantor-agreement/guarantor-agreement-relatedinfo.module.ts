import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainAgreementTableGuarantorAgreementRelatedinfoRoutingModule } from './guarantor-agreement-routing.module';
import { GuarantorAgreementListPage } from './guarantor-agreement-list/guarantor-agreement-list.page';
import { GuarantorAgreementItemPage } from './guarantor-agreement-item/guarantor-agreement-item.page';
import { SharedModule } from 'shared/shared.module';
import { GuarantorAgreementTableComponentModule } from 'components/guarantor-agreement/guarantor-agreement-table/guarantor-agreement-table.module';
import { GuarantorAgreementLineItemPage } from './guarantor-agreement-line-item/guarantor-agreement-line-item.page';
import { GuarantorAgreementLineItemComponent } from 'components/guarantor-agreement/guarantor-agreement-line/guarantor-agreement-line-item/guarantor-agreement-line-item.component';
import { GuarantorAgreementLineComponentModule } from 'components/guarantor-agreement/guarantor-agreement-line/guarantor-agreement-line.module';
import { GuarantorAgreementLineAffliateItemPage } from './guarantor-agreement-line-affliate-item/guarantor-agreement-line-affliate-item.page';
import { GuarantorAgreementLineAffiliateComponentModule } from 'components/guarantor-agreement/guarantor-agreement-line-affiliate/guarantor-agreement-line-affiliate.module';

@NgModule({
  declarations: [GuarantorAgreementListPage, GuarantorAgreementItemPage, GuarantorAgreementLineItemPage, GuarantorAgreementLineAffliateItemPage],
  imports: [
    GuarantorAgreementTableComponentModule,
    CommonModule,
    SharedModule,
    MainAgreementTableGuarantorAgreementRelatedinfoRoutingModule,
    GuarantorAgreementLineComponentModule,
    GuarantorAgreementLineAffiliateComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MainAgreementTableGuarantorAgreementRelatedinfoModule {}
