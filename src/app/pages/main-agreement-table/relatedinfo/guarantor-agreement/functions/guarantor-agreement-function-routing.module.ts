import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'copyguarantorfromca',
    loadChildren: () => import('./copy-guarantor-from-ca/copy-guarantor-from-ca-function.module').then((m) => m.CopyGuarantorFromCaFunctionModule)
  },
  {
    path: 'cancelagreement',
    loadChildren: () => import('./cancel/cancel-function.module').then((m) => m.GuarantorAgreementCancelFunctionModule)
  },
  {
    path: 'closeagreement',
    loadChildren: () => import('./close/close-function.module').then((m) => m.GuarantorAgreementCloseFunctionModule)
  },
  {
    path: 'postagreement',
    loadChildren: () => import('./post/post-function.module').then((m) => m.GuarantorAgreementPostFunctionModule)
  },
  {
    path: 'sendagreement',
    loadChildren: () => import('./send/send-function.module').then((m) => m.GuarantorAgreementSendFunctionModule)
  },
  {
    path: 'signagreement',
    loadChildren: () => import('./sign/sign-function.module').then((m) => m.GuarantorAgreementSignFunctionModule)
  },
  {
    path: 'printsetbookdoctrans',
    loadChildren: () => import('./print-book-doc/print-book-doc-function.module').then((m) => m.PrintBookDocFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainAgreementTableGuarantorAgreementFunctionRoutingModule {}
