import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainAgreementTableGuarantorAgreementFunctionRoutingModule } from './guarantor-agreement-function-routing.module';
import { SharedModule } from 'shared/shared.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, MainAgreementTableGuarantorAgreementFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MainAgreementTableGuarantorAgreementFunctionModule {}
