import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CopyGuarantorFromCaFunctionRoutingModule } from './copy-guarantor-from-ca-function-routing.module';
import { CopyGuarantorFromCaPage } from './copy-guarantor-from-ca/copy-guarantor-from-ca.page';
import { SharedModule } from 'shared/shared.module';
import { CopyGuarantorFromCAComponentModule } from 'components/guarantor-agreement/copy-guarantor-from-ca/copy-guarantor-from-ca.module';

@NgModule({
  declarations: [CopyGuarantorFromCaPage],
  imports: [CommonModule, SharedModule, CopyGuarantorFromCaFunctionRoutingModule, CopyGuarantorFromCAComponentModule],

  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CopyGuarantorFromCaFunctionModule {}
