import { AuthGuardService } from './../../../../../../core/services/auth-guard.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CopyGuarantorFromCaPage } from './copy-guarantor-from-ca/copy-guarantor-from-ca.page';

const routes: Routes = [
  {
    path: '',
    component: CopyGuarantorFromCaPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CopyGuarantorFromCaFunctionRoutingModule {}
