import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CopyGuarantorFromCaPage } from './copy-guarantor-from-ca.page';

describe('CopyGuarantorFromCaPage', () => {
  let component: CopyGuarantorFromCaPage;
  let fixture: ComponentFixture<CopyGuarantorFromCaPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CopyGuarantorFromCaPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyGuarantorFromCaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
