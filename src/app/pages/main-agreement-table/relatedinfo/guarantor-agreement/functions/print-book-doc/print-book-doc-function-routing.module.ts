import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PrintBookDocPage } from './print-book-doc/print-book-doc.page';

const routes: Routes = [
  {
    path: '',
    component: PrintBookDocPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrintBookDocFunctionRoutingModule {}
