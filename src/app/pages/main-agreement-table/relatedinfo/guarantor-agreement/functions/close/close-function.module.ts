import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GuarantorAgreementCloseFunctionRoutingModule } from './close-function-routing.module';
import { CloseAgreementPage } from './close/close.page';
import { SharedModule } from 'shared/shared.module';
import { ManageAgreementComponentModule } from 'components/function/manage-agreement/manage-agreement.module';

@NgModule({
  declarations: [CloseAgreementPage],
  imports: [CommonModule, SharedModule, GuarantorAgreementCloseFunctionRoutingModule, ManageAgreementComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GuarantorAgreementCloseFunctionModule {}
