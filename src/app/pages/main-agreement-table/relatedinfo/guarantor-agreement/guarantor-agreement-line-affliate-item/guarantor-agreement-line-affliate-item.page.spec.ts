import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuarantorAgreementLineAffliateItemPage } from './guarantor-agreement-line-affliate-item.page';

describe('GuarantorAgreementLineAffliateItemPage', () => {
  let component: GuarantorAgreementLineAffliateItemPage;
  let fixture: ComponentFixture<GuarantorAgreementLineAffliateItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GuarantorAgreementLineAffliateItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuarantorAgreementLineAffliateItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
