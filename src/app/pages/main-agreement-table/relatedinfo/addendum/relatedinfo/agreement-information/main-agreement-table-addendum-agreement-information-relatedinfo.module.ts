import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainAgreementTableAddendumAgreementInformationRelatedinfoRoutingModule } from './main-agreement-table-addendum-agreement-information-relatedinfo-routing.module';
import { AgreementInformationListPage } from './agreement-information-list/agreement-information-list.page';
import { AgreementInformationItemPage } from './agreement-information-item/agreement-information-item.page';
import { SharedModule } from 'shared/shared.module';
import { AgreementTableInfoComponentModule } from 'components/agreement-table-info/agreement-table-info.module';

@NgModule({
  declarations: [AgreementInformationListPage, AgreementInformationItemPage],
  imports: [CommonModule, SharedModule, MainAgreementTableAddendumAgreementInformationRelatedinfoRoutingModule, AgreementTableInfoComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MainAgreementTableAddendumAgreementInformationRelatedinfoModule {}
