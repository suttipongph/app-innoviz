import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainAgreementTableAddendumRelatedinfoRoutingModule } from './main-agreement-table-addendum-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, MainAgreementTableAddendumRelatedinfoRoutingModule]
})
export class MainAgreementTableAddendumRelatedinfoModule {}
