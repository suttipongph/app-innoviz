import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainAgreementTableServiceFeeTransRelatedinfoFunctionRoutingModule } from './main-agreement-table-service-fee-trans-relatedinfo-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, MainAgreementTableServiceFeeTransRelatedinfoFunctionRoutingModule]
})
export class MainAgreementTableServiceFeeTransRelatedinfoFunctionModule {}
