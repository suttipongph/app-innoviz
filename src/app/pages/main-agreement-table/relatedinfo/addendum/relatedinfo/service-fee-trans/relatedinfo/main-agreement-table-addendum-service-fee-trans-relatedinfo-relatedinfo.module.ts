import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainAgreementTableAddendumServiceFeeTransRelatedinfoRelatedinfoRoutingModule } from './main-agreement-table-addendum-service-fee-trans-relatedinfo-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, MainAgreementTableAddendumServiceFeeTransRelatedinfoRelatedinfoRoutingModule]
})
export class MainAgreementTableAddendumServiceFeeTransRelatedinfoRelatedinfoModule {}
