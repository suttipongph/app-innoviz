import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServiceFeeConditionTransRoutingModule } from './copy-service-fee-condition-trans-from-ca-routing.module';
import { SharedModule } from 'shared/shared.module';
import { CopyServiceFeeConditionTransFromCAPage } from './copy-service-fee-condition-trans-from-ca/copy-service-fee-condition-trans-from-ca.page';
import { CopyServiceFeeCondCARequestComponentModule } from 'components/service-fee-trans/copy-service-fee-cond-ca-request/copy-service-fee-cond-ca-request.module';

@NgModule({
  declarations: [CopyServiceFeeConditionTransFromCAPage],
  imports: [CommonModule, SharedModule, ServiceFeeConditionTransRoutingModule, CopyServiceFeeCondCARequestComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ServiceFeeConditionTransModule {}
