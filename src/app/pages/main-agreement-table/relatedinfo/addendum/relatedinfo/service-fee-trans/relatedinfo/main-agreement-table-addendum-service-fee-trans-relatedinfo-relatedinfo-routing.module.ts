import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'invoicetable',
    loadChildren: () =>
      import('./invoicetable/main-agreement-table-addendum-service-fee-trans-invoicetable-relatedinfo.module').then(
        (m) => m.MainAgreementTableAddendumServiceFeeTransInvoiceTableRelatedinfoModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainAgreementTableAddendumServiceFeeTransRelatedinfoRelatedinfoRoutingModule {}
