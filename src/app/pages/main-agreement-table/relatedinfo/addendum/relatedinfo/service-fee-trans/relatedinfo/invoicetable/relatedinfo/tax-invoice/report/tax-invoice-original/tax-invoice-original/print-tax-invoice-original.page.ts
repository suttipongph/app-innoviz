import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_REPORT_GEN } from 'shared/constants/constantGen';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'print-tax-invoice-original-page',
  templateUrl: './print-tax-invoice-original.page.html',
  styleUrls: ['./print-tax-invoice-original.page.scss']
})
export class PrintTaxInvoiceOriginalPage implements OnInit {
  pageInfo: PageInformationModel;
  activeRoute = this.uiService.getRelatedInfoActiveTableName();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_REPORT_GEN.PRINT_TAX_INVOICE_ORIGINAL,
      servicePath: 'InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal'
    };
  }
}
