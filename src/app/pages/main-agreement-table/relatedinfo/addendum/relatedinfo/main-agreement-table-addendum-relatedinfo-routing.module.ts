import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'memotrans',
    loadChildren: () =>
      import('./memo/main-agreement-table-addendum-memo-trans-relatedinfo.module').then((m) => m.MainAgreementTableAddendumMemoTransRelatedinfoModule)
  },
  {
    path: 'jointventuretrans',
    loadChildren: () =>
      import('./joint-venture-trans/main-agreement-table-addendum-joint-venture-trans.module').then(
        (m) => m.MainAgreementTableAddendumJointVentureTransModule
      )
  },
  {
    path: 'bookmarkdocumenttrans',
    loadChildren: () =>
      import('./bookmark-document-trans/main-agreement-table-addendum-bookmark-document-trans-relatedinfo.module').then(
        (m) => m.MainAgreementTableAddendumBookmarkDocumentRelatedinfoModule
      )
  },
  {
    path: 'agreementtableinfo',
    loadChildren: () =>
      import('./agreement-information/main-agreement-table-addendum-agreement-information-relatedinfo.module').then(
        (m) => m.MainAgreementTableAddendumAgreementInformationRelatedinfoModule
      )
  },
  {
    path: 'consortiumtrans',
    loadChildren: () =>
      import('./consortium/main-agreement-table-addendum-consortium-relatedinfo.module').then(
        (m) => m.MainAgreementTableAddendumConsortiumRelatedinfoModule
      )
  },
  {
    path: 'servicefeetrans',
    loadChildren: () =>
      import('./service-fee-trans/main-agreement-table-addendum-service-fee-trans-relatedinfo.module').then(
        (m) => m.MainAgreementTableAddendumServiceFeeTransRelatedinfoModule
      )
  },
  {
    path: 'attachment',
    loadChildren: () => import('./attachment/main-agm-table-addendum-attachment.module').then((m) => m.MainAgreementTableAddendumAttachmentModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainAgreementTableAddendumRelatedinfoRoutingModule {}
