import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'joint-venture-trans-list-page',
  templateUrl: './joint-venture-trans-list.page.html',
  styleUrls: ['./joint-venture-trans-list.page.scss']
})
export class JointVentureTransListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);

  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.JOINT_VENTURE_TRANS,
      servicePath: `${ROUTE_MASTER_GEN.MAIN_AGREEMENT_TABLE}/${getProductType(this.masterRoute, true)}/${this.currentRoute}/RelatedInfo/${
        ROUTE_RELATED_GEN.ADDENDUM_MAINAGREEMENT_TABLE
      }/RelatedInfo/${ROUTE_RELATED_GEN.JOINT_VENTURE_TRANS}`
    };
  }
}
