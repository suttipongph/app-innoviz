import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ConsortiumTransItemPage } from './consortium-trans-item.page';

describe('ConsortiumItemPage', () => {
  let component: ConsortiumTransItemPage;
  let fixture: ComponentFixture<ConsortiumTransItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConsortiumTransItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsortiumTransItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
