import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AddendumItemPage } from './addendum-item.page';

describe('AddendumItemPage', () => {
  let component: AddendumItemPage;
  let fixture: ComponentFixture<AddendumItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AddendumItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddendumItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
