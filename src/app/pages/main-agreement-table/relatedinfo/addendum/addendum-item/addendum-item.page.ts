import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { MenuItem } from 'primeng/api';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'addendum-item.page',
  templateUrl: './addendum-item.page.html',
  styleUrls: ['./addendum-item.page.scss']
})
export class AddendumItemPage implements OnInit {
  pageInfo: PageInformationModel;
  relatedInfoItems: MenuItem[];
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
    this.setRelatedInfoOptionsByAddendum();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: `/${getProductType(this.masterRoute, true)}/${this.currentRoute}/${ROUTE_MASTER_GEN.MAIN_AGREEMENT_TABLE}/RelatedInfo/${
        ROUTE_RELATED_GEN.ADDENDUM_MAINAGREEMENT_TABLE
      }`,
      servicePath: `${ROUTE_MASTER_GEN.MAIN_AGREEMENT_TABLE}/${getProductType(this.masterRoute, true)}/${this.currentRoute}/RelatedInfo/${
        ROUTE_RELATED_GEN.ADDENDUM_MAINAGREEMENT_TABLE
      }`
    };
  }
  setRelatedInfoOptionsByAddendum(): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.GUARANTOR_AGREEMENT',
        visible: false
      },
      {
        label: 'LABEL.ADDENDUM',
        visible: false
      },
      {
        label: 'LABEL.NOTICE_OF_CANCELLATION',
        visible: false
      }
    ];
  }
}
