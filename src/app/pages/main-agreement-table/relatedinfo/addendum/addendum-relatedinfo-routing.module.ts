import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddendumListPage } from './addendum-list/addendum-list.page';
import { AddendumItemPage } from './addendum-item/addendum-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: AddendumListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: AddendumItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'function',
    loadChildren: () => import('./functions/addendum-releatedinfo-function.module').then((m) => m.AddendumRelatedinfoFunctionModule)
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () =>
      import('./relatedinfo/main-agreement-table-addendum-relatedinfo.module').then((m) => m.MainAgreementTableAddendumRelatedinfoModule)
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/addendum-releatedinfo-function.module').then((m) => m.AddendumRelatedinfoFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddendumRelatedinfoRoutingModule {}
