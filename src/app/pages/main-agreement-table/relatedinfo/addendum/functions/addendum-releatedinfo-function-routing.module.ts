import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'addendum',
    loadChildren: () => import('./addendum/addendum-function.module').then((m) => m.AddendumFunctionModule)
  },
  {
    path: 'printsetbookdoctrans',
    loadChildren: () => import('./print-book-doc/print-book-doc-function.module').then((m) => m.PrintBookDocFunctionModule)
  },
  {
    path: 'cancelagreement',
    loadChildren: () => import('./cancel/cancel-function.module').then((m) => m.MainAgreementCancelFunctionModule)
  },
  {
    path: 'closeagreement',
    loadChildren: () => import('./close/close-function.module').then((m) => m.MainAgreementCloseFunctionModule)
  },
  {
    path: 'postagreement',
    loadChildren: () => import('./post/post-function.module').then((m) => m.MainAgreementPostFunctionModule)
  },
  {
    path: 'sendagreement',
    loadChildren: () => import('./send/send-function.module').then((m) => m.MainAgreementSendFunctionModule)
  },
  {
    path: 'signagreement',
    loadChildren: () => import('./sign/sign-function.module').then((m) => m.MainAgreementSignFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddendumRelatedinfoFunctionRoutingModule {}
