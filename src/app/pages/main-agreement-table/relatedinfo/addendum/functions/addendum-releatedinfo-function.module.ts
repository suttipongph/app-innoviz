import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddendumRelatedinfoFunctionRoutingModule } from './addendum-releatedinfo-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, AddendumRelatedinfoFunctionRoutingModule]
})
export class AddendumRelatedinfoFunctionModule {}
