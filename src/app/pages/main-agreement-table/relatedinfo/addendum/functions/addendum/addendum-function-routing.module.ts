import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { AddendumPage } from './addendum/addendum.page';

const routes: Routes = [
  {
    path: '',
    component: AddendumPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddendumFunctionRoutingModule {}
