import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddendumRelatedinfoRoutingModule } from './addendum-relatedinfo-routing.module';
import { AddendumListPage } from './addendum-list/addendum-list.page';
import { AddendumItemPage } from './addendum-item/addendum-item.page';
import { SharedModule } from 'shared/shared.module';
import { MainAgreementTableComponentModule } from 'components/main-agreement-table/main-agreement-table.module';

@NgModule({
  declarations: [AddendumListPage, AddendumItemPage],
  imports: [CommonModule, SharedModule, AddendumRelatedinfoRoutingModule, MainAgreementTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AddendumRelatedinfoModule {}
