import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AddendumListPage } from './addendum-list.page';

describe('AddendumListPage', () => {
  let component: AddendumListPage;
  let fixture: ComponentFixture<AddendumListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AddendumListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddendumListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
