import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainAgreementTableServiceFeeTransRelatedinfoRelatedInfoRoutingModule } from './main-agreement-table-service-fee-trans-relatedinfo-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, MainAgreementTableServiceFeeTransRelatedinfoRelatedInfoRoutingModule]
})
export class MainAgreementTableServiceFeeTransRelatedinfoRelatedInfoModule {}
