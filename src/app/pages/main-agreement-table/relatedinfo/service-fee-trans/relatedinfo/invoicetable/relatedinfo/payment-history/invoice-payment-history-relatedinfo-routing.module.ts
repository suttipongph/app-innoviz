import { AuthGuardService } from 'core/services/auth-guard.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';
import { PaymentHistoryListPage } from './payment-history-list/payment-history-list.page';
import { PaymentHistoryItemPage } from './payment-history-item/payment-history-item.page';

const routes: Routes = [
  {
    path: '',
    component: PaymentHistoryListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: PaymentHistoryItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoicePaymentHistoryRelatedinfoRoutingModule {}
