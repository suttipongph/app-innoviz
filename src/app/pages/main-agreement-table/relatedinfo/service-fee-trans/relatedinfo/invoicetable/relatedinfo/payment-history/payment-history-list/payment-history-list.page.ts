import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'payment-history-list-page',
  templateUrl: './payment-history-list.page.html',
  styleUrls: ['./payment-history-list.page.scss']
})
export class PaymentHistoryListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute(1);
  product = this.uiService.getMasterRoute();
  pathName = this.uiService.getRelatedInfoOriginTableName();

  parentId = this.uiService.getRelatedInfoParentTableKey();

  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'paymentHistoryGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'invoiceTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.PAYMENT_HISTORY,
      servicePath: `${ROUTE_MASTER_GEN.MAIN_AGREEMENT_TABLE}/${getProductType(this.product, true)}/${this.currentRoute}/RelatedInfo/${
        ROUTE_RELATED_GEN.SERVICE_FEE_TRANS
      }/RelatedInfo/${ROUTE_RELATED_GEN.INVOICE_TABLE}/RelatedInfo/PaymentHistory`
    };
  }
}
