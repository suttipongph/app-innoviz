import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'tax-invoice-item.page',
  templateUrl: './tax-invoice-item.page.html',
  styleUrls: ['./tax-invoice-item.page.scss']
})
export class TaxInvoiceItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    // this.pageInfo = {
    //   pagePath: ROUTE_RELATED_GEN.TAX_INVOICE,
    //   servicePath: 'InvoiceTable/RelatedInfo/TaxInvoice',
    //   childPaths: [
    //     {
    //       pagePath: ROUTE_RELATED_GEN.TAX_INVOICE_LINE,
    //       servicePath: 'InvoiceTable/RelatedInfo/TaxInvoice'
    //     }
    //   ]
    // };
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.TAX_INVOICE,
      servicePath: `${ROUTE_MASTER_GEN.MAIN_AGREEMENT_TABLE}/${getProductType(this.masterRoute, true)}/${this.currentRoute}/RelatedInfo/${
        ROUTE_RELATED_GEN.SERVICE_FEE_TRANS
      }/RelatedInfo/${ROUTE_RELATED_GEN.INVOICE_TABLE}/RelatedInfo/TaxInvoice`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.TAX_INVOICE_LINE,
          servicePath: `${ROUTE_MASTER_GEN.MAIN_AGREEMENT_TABLE}/${getProductType(this.masterRoute, true)}/${this.currentRoute}/RelatedInfo/${
            ROUTE_RELATED_GEN.SERVICE_FEE_TRANS
          }/RelatedInfo/${ROUTE_RELATED_GEN.INVOICE_TABLE}/RelatedInfo/TaxInvoice`
        }
      ]
    };
  }
}
