import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ServiceFeeTransItemPage } from './service-fee-trans-item.page';

describe('ServiceFeeItemPage', () => {
  let component: ServiceFeeTransItemPage;
  let fixture: ComponentFixture<ServiceFeeTransItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ServiceFeeTransItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceFeeTransItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
