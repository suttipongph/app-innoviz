import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CopyServiceFeeConditionTransFromCAPage } from './copy-service-fee-condition-trans-from-ca/copy-service-fee-condition-trans-from-ca.page';

const routes: Routes = [
  {
    path: '',
    component: CopyServiceFeeConditionTransFromCAPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceFeeConditionTransRoutingModule {}
