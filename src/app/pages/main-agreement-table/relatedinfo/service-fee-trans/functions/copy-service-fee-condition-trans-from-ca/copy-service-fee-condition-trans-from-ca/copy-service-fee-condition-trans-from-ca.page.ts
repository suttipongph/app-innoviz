import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'copy-service-fee-condition-trans-from-ca-page',
  templateUrl: './copy-service-fee-condition-trans-from-ca.page.html',
  styleUrls: ['./copy-service-fee-condition-trans-from-ca.page.scss']
})
export class CopyServiceFeeConditionTransFromCAPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);

  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA,
      servicePath: `${ROUTE_MASTER_GEN.MAIN_AGREEMENT_TABLE}/${getProductType(this.masterRoute, true)}/${this.currentRoute}/RelatedInfo/${
        ROUTE_RELATED_GEN.SERVICE_FEE_TRANS
      }/Function/${ROUTE_FUNCTION_GEN.COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA}`
    };
  }
}
