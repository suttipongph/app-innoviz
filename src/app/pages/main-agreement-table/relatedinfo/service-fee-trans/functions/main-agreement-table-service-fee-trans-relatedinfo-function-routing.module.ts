import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'copyservicefeeconditiontransfromca',
    loadChildren: () =>
      import('./copy-service-fee-condition-trans-from-ca/copy-service-fee-condition-trans-from-ca.module').then(
        (m) => m.ServiceFeeConditionTransModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainAgreementTableServiceFeeTransRelatedinfoFunctionRoutingModule {}
