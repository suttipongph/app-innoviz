import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServiceFeeTransListPage } from './service-fee-trans-list/service-fee-trans-list.page';
import { ServiceFeeTransItemPage } from './service-fee-trans-item/service-fee-trans-item.page';
import { SharedModule } from 'shared/shared.module';
import { ServiceFeeTransComponentModule } from 'components/service-fee-trans/service-fee-trans.module';
import { MainAgreementTableServiceFeeTransRelatedinfoRoutingModule } from './main-agreement-table-service-fee-trans-relatedinfo-routing.module';

@NgModule({
  declarations: [ServiceFeeTransListPage, ServiceFeeTransItemPage],
  imports: [CommonModule, SharedModule, MainAgreementTableServiceFeeTransRelatedinfoRoutingModule, ServiceFeeTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MainAgreementTableServiceFeeTransRelatedinfoModule {}
