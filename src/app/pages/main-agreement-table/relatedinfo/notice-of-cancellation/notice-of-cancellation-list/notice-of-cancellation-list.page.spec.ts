import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoticeOfCancellationListPage } from './notice-of-cancellation-list.page';

describe('NoticeOfCancellationListPage', () => {
  let component: NoticeOfCancellationListPage;
  let fixture: ComponentFixture<NoticeOfCancellationListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NoticeOfCancellationListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoticeOfCancellationListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
