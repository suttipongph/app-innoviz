import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NoticeOfCancellationListPage } from './notice-of-cancellation-list/notice-of-cancellation-list.page';
import { NoticeOfCancellationItemPage } from './notice-of-cancellation-item/notice-of-cancellation-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: NoticeOfCancellationListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: NoticeOfCancellationItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'function',
    loadChildren: () =>
      import('./functions/notice-of-cancellation-relatedinfo-function.module').then((m) => m.NoticeOfCancellationRelatedinfoFunctionModule)
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () =>
      import('./relatedinfo/main-agreement-table-notice-of-cancellation-relatedinfo.module').then(
        (m) => m.MainAgreementTableNoticeOfCancellationRelatedinfoModule
      )
  },
  {
    path: ':id/function',
    loadChildren: () =>
      import('./functions/notice-of-cancellation-relatedinfo-function.module').then((m) => m.NoticeOfCancellationRelatedinfoFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NoticeOfCancellationRelatedinfoRoutingModule {}
