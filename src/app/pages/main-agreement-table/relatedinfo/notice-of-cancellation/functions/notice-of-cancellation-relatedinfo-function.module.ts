import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoticeOfCancellationRelatedinfoFunctionRoutingModule } from './notice-of-cancellation-relatedinfo-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, NoticeOfCancellationRelatedinfoFunctionRoutingModule]
})
export class NoticeOfCancellationRelatedinfoFunctionModule {}
