import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SendAgreementPage } from './send.page';

describe('SendPage', () => {
  let component: SendAgreementPage;
  let fixture: ComponentFixture<SendAgreementPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SendAgreementPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SendAgreementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
