import { Component } from '@angular/core';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel, PathParamModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-notice-of-cancellation',
  templateUrl: './notice-of-cancellation.page.html',
  styleUrls: ['./notice-of-cancellation.page.scss']
})
export class NoticeOfCancellationPage extends BaseItemComponent<any> {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {
    super();
  }

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.NOTICE_OF_CANCELLATION,
      servicePath: `MainAgreementTable/${getProductType(this.masterRoute, true)}/${this.currentRoute}/RelatedInfo/${ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION_MAINAGREEMENT_TABLE}/Function/${
        ROUTE_FUNCTION_GEN.NOTICE_OF_CANCELLATION
      }`
    };
  }
}
