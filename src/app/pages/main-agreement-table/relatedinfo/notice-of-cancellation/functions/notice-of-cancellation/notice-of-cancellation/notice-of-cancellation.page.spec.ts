import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoticeOfCancellationPage } from './notice-of-cancellation.page';

describe('NoticeOfCancellationPage', () => {
  let component: NoticeOfCancellationPage;
  let fixture: ComponentFixture<NoticeOfCancellationPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NoticeOfCancellationPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoticeOfCancellationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
