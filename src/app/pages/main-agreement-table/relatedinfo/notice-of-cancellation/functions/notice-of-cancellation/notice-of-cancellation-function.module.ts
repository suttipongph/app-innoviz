import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoticeOfCancellationFunctionRoutingModule } from './notice-of-cancellation-function-routing.module';
import { NoticeOfCancellationPage } from './notice-of-cancellation/notice-of-cancellation.page';
import { GenMainAgmNoticeOfCancelComponentModule } from 'components/main-agreement-table/gen-main-agm-notice-of-cancel/gen-main-agm-notice-of-cancel.module';
import { SharedModule } from 'shared/shared.module';

@NgModule({
  declarations: [NoticeOfCancellationPage],
  imports: [CommonModule, SharedModule, NoticeOfCancellationFunctionRoutingModule, GenMainAgmNoticeOfCancelComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NoticeOfCancellationFunctionModule {}
