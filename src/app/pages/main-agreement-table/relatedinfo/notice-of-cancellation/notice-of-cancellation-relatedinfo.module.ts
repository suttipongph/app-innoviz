import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoticeOfCancellationRelatedinfoRoutingModule } from './notice-of-cancellation-relatedinfo-routing.module';
import { NoticeOfCancellationListPage } from './notice-of-cancellation-list/notice-of-cancellation-list.page';
import { NoticeOfCancellationItemPage } from './notice-of-cancellation-item/notice-of-cancellation-item.page';
import { SharedModule } from 'shared/shared.module';
import { MainAgreementTableComponentModule } from 'components/main-agreement-table/main-agreement-table.module';

@NgModule({
  declarations: [NoticeOfCancellationListPage, NoticeOfCancellationItemPage],
  imports: [CommonModule, SharedModule, NoticeOfCancellationRelatedinfoRoutingModule, MainAgreementTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NoticeOfCancellationRelatedinfoModule {}
