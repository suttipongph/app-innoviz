import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'memotrans',
    loadChildren: () =>
      import('./memo/main-agreement-table-notice-of-cancellation-memo-trans-relatedinfo.module').then(
        (m) => m.MainAgreementTableNoticeOfCancellationMemoTransRelatedinfoModule
      )
  },
  {
    path: 'jointventuretrans',
    loadChildren: () =>
      import('./joint-venture-trans/main-agreement-table-notice-of-cancellation-joint-venture-trans.module').then(
        (m) => m.MainAgreementTableNoticeOfCancellationJointVentureTransModule
      )
  },
  {
    path: 'bookmarkdocumenttrans',
    loadChildren: () =>
      import('./bookmark-document-trans/main-agreement-table-notice-of-cancellation-bookmark-document-trans-relatedinfo.module').then(
        (m) => m.MainAgreementTableNoticeOfCancellationBookmarkDocumentRelatedinfoModule
      )
  },
  {
    path: 'agreementtableinfo',
    loadChildren: () =>
      import('./agreement-information/main-agreement-table-notice-of-cancellation-agreement-information-relatedinfo.module').then(
        (m) => m.MainAgreementTableNoticeOfCancellationAgreementInformationRelatedinfoModule
      )
  },
  {
    path: 'consortiumtrans',
    loadChildren: () =>
      import('./consortium/main-agreement-table-notice-of-cancellation-consortium-relatedinfo.module').then(
        (m) => m.MainAgreementTableNoticeOfCancellationConsortiumRelatedinfoModule
      )
  },
  {
    path: 'servicefeetrans',
    loadChildren: () =>
      import('./service-fee-trans/main-agreement-table-notice-of-cancellation-service-fee-trans-relatedinfo.module').then(
        (m) => m.MainAgreementTableNoticeOfCancellationServiceFeeTransRelatedinfoModule
      )
  },
  {
    path: 'attachment',
    loadChildren: () =>
      import('./attachment/main-agm-table-notice-of-cancellation-attachment.module').then(
        (m) => m.MainAgreementTableNoticeOfCancellationAttachmentModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainAgreementTableNoticeOfCancellationRelatedinfoRoutingModule {}
