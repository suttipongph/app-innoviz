import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MemoItemPage } from './memo-item.page';

describe('MemoItemPage', () => {
  let component: MemoItemPage;
  let fixture: ComponentFixture<MemoItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MemoItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MemoItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
