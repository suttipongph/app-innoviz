import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainAgreementTableNoticeOfCancellationRelatedinfoRoutingModule } from './main-agreement-table-notice-of-cancellation-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, MainAgreementTableNoticeOfCancellationRelatedinfoRoutingModule]
})
export class MainAgreementTableNoticeOfCancellationRelatedinfoModule {}
