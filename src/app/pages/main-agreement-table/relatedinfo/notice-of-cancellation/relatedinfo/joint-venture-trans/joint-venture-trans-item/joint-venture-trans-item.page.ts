import { Component, OnInit } from '@angular/core';
import { JointVentureTransService } from 'components/joint-venture-trans/joint-venture-trans.service';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'joint-venture-trans-item.page',
  templateUrl: './joint-venture-trans-item.page.html',
  styleUrls: ['./joint-venture-trans-item.page.scss']
})
export class JointVentureTransItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);

  constructor(public uiService: UIControllerService, public service: JointVentureTransService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.JOINT_VENTURE_TRANS,
      servicePath: `${ROUTE_MASTER_GEN.MAIN_AGREEMENT_TABLE}/${getProductType(this.masterRoute, true)}/${this.currentRoute}/RelatedInfo/${
        ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION_MAINAGREEMENT_TABLE
      }/RelatedInfo/${ROUTE_RELATED_GEN.JOINT_VENTURE_TRANS}`
    };
  }
}
