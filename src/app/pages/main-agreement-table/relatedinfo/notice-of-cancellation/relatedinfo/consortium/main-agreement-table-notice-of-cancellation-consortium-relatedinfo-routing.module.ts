import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ConsortiumTransItemPage } from './consortium-trans-item/consortium-trans-item.page';
import { ConsortiumTransListPage } from './consortium-trans-list/consortium-trans-list.page';

const routes: Routes = [
  {
    path: '',
    component: ConsortiumTransListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ConsortiumTransItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainAgreementTableNoticeOfCancellationConsortiumRelatedinfoRoutingModule {}
