import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainAgreementTableNoticeOfCancellationConsortiumRelatedinfoRoutingModule } from './main-agreement-table-notice-of-cancellation-consortium-relatedinfo-routing.module';
import { SharedModule } from 'shared/shared.module';
import { ConsortiumTransComponentModule } from 'components/consortium-trans/consortium-trans.module';
import { ConsortiumTransListPage } from './consortium-trans-list/consortium-trans-list.page';
import { ConsortiumTransItemPage } from './consortium-trans-item/consortium-trans-item.page';

@NgModule({
  declarations: [ConsortiumTransListPage, ConsortiumTransItemPage],
  imports: [CommonModule, SharedModule, MainAgreementTableNoticeOfCancellationConsortiumRelatedinfoRoutingModule, ConsortiumTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MainAgreementTableNoticeOfCancellationConsortiumRelatedinfoModule {}
