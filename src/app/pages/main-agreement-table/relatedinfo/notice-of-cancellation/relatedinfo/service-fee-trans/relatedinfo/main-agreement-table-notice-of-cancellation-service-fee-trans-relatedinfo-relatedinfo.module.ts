import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainAgreementTableNoticeOfCancellationServiceFeeTransRelatedinfoRelatedinfoRoutingModule } from './main-agreement-table-notice-of-cancellation-service-fee-trans-relatedinfo-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, MainAgreementTableNoticeOfCancellationServiceFeeTransRelatedinfoRelatedinfoRoutingModule]
})
export class MainAgreementTableNoticeOfCancellationServiceFeeTransRelatedinfoRelatedinfoModule {}
