import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TaxInvoiceListPage } from './tax-invoice-list/tax-invoice-list.page';
import { TaxInvoiceItemPage } from './tax-invoice-item/tax-invoice-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { TaxInvoiceLineItemPage } from './tax-invoice-line-item/tax-invoice-line-item.page';

const routes: Routes = [
  {
    path: '',
    component: TaxInvoiceListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: TaxInvoiceItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/taxinvoiceline-child/:id',
    component: TaxInvoiceLineItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/report',
    loadChildren: () => import('./report/tax-invoice-report.module').then((m) => m.TaxInvoiceCopyReportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaxInvoiceRelatedinfoRoutingModule {}
