import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { MainAgreementTableNoticeOfCancellationServiceFeeTransInvoiceTableRelatedinfoRoutingModule } from './main-agreement-table-notice-of-cancellation-service-fee-trans-invoicetable-relatedinfo-routing.module';
import { InvoiceTableComponentModule } from 'components/invoice-table/invoice-table/invoice-table.module';
import { InvoiceTableListPage } from './invoice-table-list/invoice-table.page';
import { InvoiceTableItemPage } from './invoice-table-item/invoice-table-item.page';
import { InvoiceLineItemPage } from './invoice-line-item/invoice-line-item.page';
import { InvoiceLineComponentModule } from 'components/invoice-table/invoice-line/invoice-line.module';

@NgModule({
  declarations: [InvoiceTableListPage, InvoiceTableItemPage, InvoiceLineItemPage],
  imports: [
    CommonModule,
    SharedModule,
    MainAgreementTableNoticeOfCancellationServiceFeeTransInvoiceTableRelatedinfoRoutingModule,
    InvoiceTableComponentModule,
    InvoiceLineComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MainAgreementTableNoticeOfCancellationServiceFeeTransInvoiceTableRelatedinfoModule {}
