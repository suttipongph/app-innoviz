import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'tax-invoice-line-item-page',
  templateUrl: './tax-invoice-line-item.page.html',
  styleUrls: ['./tax-invoice-line-item.page.scss']
})
export class TaxInvoiceLineItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.TAX_INVOICE,
      servicePath: `${ROUTE_MASTER_GEN.MAIN_AGREEMENT_TABLE}/${getProductType(this.masterRoute, true)}/${this.currentRoute}/RelatedInfo/${
        ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION_MAINAGREEMENT_TABLE
      }/RelatedInfo/${ROUTE_RELATED_GEN.SERVICE_FEE_TRANS}/RelatedInfo/${ROUTE_RELATED_GEN.INVOICE_TABLE}/RelatedInfo/TaxInvoice`
    };
  }
}
