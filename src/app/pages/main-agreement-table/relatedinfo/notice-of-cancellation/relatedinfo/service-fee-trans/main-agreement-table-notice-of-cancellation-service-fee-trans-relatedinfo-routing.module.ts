import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ServiceFeeTransListPage } from './service-fee-trans-list/service-fee-trans-list.page';
import { ServiceFeeTransItemPage } from './service-fee-trans-item/service-fee-trans-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: ServiceFeeTransListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ServiceFeeTransItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () =>
      import('./relatedinfo/main-agreement-table-notice-of-cancellation-service-fee-trans-relatedinfo-relatedinfo.module').then(
        (m) => m.MainAgreementTableNoticeOfCancellationServiceFeeTransRelatedinfoRelatedinfoModule
      )
  },
  {
    path: 'function',
    loadChildren: () =>
      import('./functions/main-agreement-table-service-fee-trans-relatedinfo-function.module').then(
        (m) => m.MainAgreementTableServiceFeeTransRelatedinfoFunctionModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainAgreementTableNoticeOfCancellationServiceFeeTransRelatedinfoRoutingModule {}
