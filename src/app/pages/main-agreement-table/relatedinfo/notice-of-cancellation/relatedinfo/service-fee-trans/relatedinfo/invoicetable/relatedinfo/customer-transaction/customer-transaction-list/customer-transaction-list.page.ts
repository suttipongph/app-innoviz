import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'customer-transaction-list-page',
  templateUrl: './customer-transaction-list.page.html',
  styleUrls: ['./customer-transaction-list.page.scss']
})
export class CustomerTransactionListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute(1);
  product = this.uiService.getMasterRoute();
  pathName = this.uiService.getRelatedInfoOriginTableName();

  parentId = this.uiService.getRelatedInfoParentTableKey();

  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'custTransGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.CUSTOMER_TRANSACTION,
      servicePath: `${ROUTE_MASTER_GEN.MAIN_AGREEMENT_TABLE}/${getProductType(this.product, true)}/${this.currentRoute}/RelatedInfo/${
        ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION_MAINAGREEMENT_TABLE
      }/RelatedInfo/${ROUTE_RELATED_GEN.SERVICE_FEE_TRANS}/RelatedInfo/${ROUTE_RELATED_GEN.INVOICE_TABLE}/RelatedInfo/CustTransaction`
    };
  }
}
