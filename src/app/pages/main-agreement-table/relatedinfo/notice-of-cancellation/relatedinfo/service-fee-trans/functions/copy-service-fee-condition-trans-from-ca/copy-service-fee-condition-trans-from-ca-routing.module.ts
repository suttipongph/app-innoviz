import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CopyServiceFeeConditionTransFromCAPage } from './copy-service-fee/copy-service-fee.page';

const routes: Routes = [
  {
    path: '',
    component: CopyServiceFeeConditionTransFromCAPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceFeeConditionTransRoutingModule {}
