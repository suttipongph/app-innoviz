import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'invoicetable',
    loadChildren: () =>
      import('./invoicetable/main-agreement-table-notice-of-cancellation-service-fee-trans-invoicetable-relatedinfo.module').then(
        (m) => m.MainAgreementTableNoticeOfCancellationServiceFeeTransInvoiceTableRelatedinfoModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainAgreementTableNoticeOfCancellationServiceFeeTransRelatedinfoRelatedinfoRoutingModule {}
