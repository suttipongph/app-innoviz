import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CopyServiceFeeConditionTransFromCAPage } from './copy-service-fee.page';

describe('ServiceFeeConditionTransItemPage', () => {
  let component: CopyServiceFeeConditionTransFromCAPage;
  let fixture: ComponentFixture<CopyServiceFeeConditionTransFromCAPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CopyServiceFeeConditionTransFromCAPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyServiceFeeConditionTransFromCAPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
