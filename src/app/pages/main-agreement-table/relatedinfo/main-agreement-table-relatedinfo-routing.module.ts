import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'memotrans',
    loadChildren: () =>
      import('./memo/main-agreement-table-memo-trans-relatedinfo.module').then((m) => m.MainAgreementTableMemoTransRelatedinfoModule)
  },
  {
    path: 'jointventuretrans',
    loadChildren: () =>
      import('./joint-venture-trans/main-agreement-table-joint-venture-trans.module').then((m) => m.MainAgreementTableJointVentureTransModule)
  },
  {
    path: 'bookmarkdocumenttrans',
    loadChildren: () =>
      import('./bookmark-document-trans/main-agreement-table-bookmark-document-trans-relatedinfo.module').then(
        (m) => m.MainAgreementTableBookmarkDocumentRelatedinfoModule
      )
  },
  {
    path: 'agreementtableinfo',
    loadChildren: () =>
      import('./agreement-information/agreement-information-relatedinfo.module').then((m) => m.AgreementInformationRelatedinfoModule)
  },
  {
    path: 'consortiumtrans',
    loadChildren: () =>
      import('./consortium/main-agreement-table-consortium-relatedinfo.module').then((m) => m.MainAgreementTableConsortiumRelatedinfoModule)
  },
  {
    path: 'guarantoragreementtable',
    loadChildren: () =>
      import('./guarantor-agreement/guarantor-agreement-relatedinfo.module').then((m) => m.MainAgreementTableGuarantorAgreementRelatedinfoModule)
  },
  {
    path: 'noticeofcancellationmainagreementtable',
    loadChildren: () =>
      import('./notice-of-cancellation/notice-of-cancellation-relatedinfo.module').then((m) => m.NoticeOfCancellationRelatedinfoModule)
  },
  {
    path: 'addendummainagreementtable',
    loadChildren: () => import('./addendum/addendum-relatedinfo.module').then((m) => m.AddendumRelatedinfoModule)
  },
  {
    path: 'servicefeetrans',
    loadChildren: () =>
      import('./service-fee-trans/main-agreement-table-service-fee-trans-relatedinfo.module').then(
        (m) => m.MainAgreementTableServiceFeeTransRelatedinfoModule
      )
  },
  {
    path: 'attachment',
    loadChildren: () => import('./attachment/main-agm-table-attachment.module').then((m) => m.MainAgreementTableAttachmentModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainAgreementTableRelatedinfoRoutingModule {}
