import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AgreementInformationListPage } from './agreement-information-list.page';

describe('AgreementInformationListPage', () => {
  let component: AgreementInformationListPage;
  let fixture: ComponentFixture<AgreementInformationListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AgreementInformationListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgreementInformationListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
