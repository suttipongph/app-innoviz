import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreditAppTableListPage } from './credit-app-table-list/credit-app-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: CreditAppTableListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppTableRoutingModule {}
