import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppTableRoutingModule } from './credit-app-table-routing.module';
import { CreditAppTableListPage } from './credit-app-table-list/credit-app-table-list.page';
import { SharedModule } from 'shared/shared.module';
import { CreditAppTableComponentModule } from 'components/demo-credit-app-table/credit-app-table/credit-app-table.module';

@NgModule({
  declarations: [CreditAppTableListPage],
  imports: [CommonModule, SharedModule, CreditAppTableRoutingModule, CreditAppTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DemoCreditAppTableModule {}
