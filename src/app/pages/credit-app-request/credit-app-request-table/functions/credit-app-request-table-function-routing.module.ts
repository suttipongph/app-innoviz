import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'cancelcreditapprequest',
    loadChildren: () =>
      import('./cancel-credit-app-req/cancel-credit-app-req.page-function.module').then((m) => m.CancelCreditAppRequestFunctionModule)
  },
  {
    path: 'printsetbookdoctrans',
    loadChildren: () => import('./print-book-doc/print-book-doc-function.module').then((m) => m.PrintBookDocFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppRequestFunctionRoutingModule {}
