import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CancelCreditAppRequestPage } from './cancel-credit-app-req.page';

describe('CancelCreditAppRequestPage', () => {
  let component: CancelCreditAppRequestPage;
  let fixture: ComponentFixture<CancelCreditAppRequestPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CancelCreditAppRequestPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelCreditAppRequestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
