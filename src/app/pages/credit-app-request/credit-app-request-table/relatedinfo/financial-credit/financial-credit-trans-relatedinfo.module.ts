import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FinancialCreditListPage } from './financial-credit-list/financial-credit-list.page';
import { FinancialCreditItemPage } from './financial-credit-item/financial-credit-item.page';
import { CreditAppRequestTableFinancialCreditRelatedinfoRoutingModule } from './financial-credit-trans-relatedinfo-routing.module';
import { SharedModule } from 'shared/shared.module';
import { FinancialCreditTransComponentModule } from 'components/financial-credit-trans/financial-credit-trans.module';

@NgModule({
  declarations: [FinancialCreditListPage, FinancialCreditItemPage],
  imports: [CommonModule, SharedModule, CreditAppRequestTableFinancialCreditRelatedinfoRoutingModule, FinancialCreditTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppRequestTableFinancialCreditRelatedinfoModule {}
