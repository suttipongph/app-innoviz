import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditAppTableListPage } from './credit-app-table-list.page';

describe('CreditAppTableListPage', () => {
  let component: CreditAppTableListPage;
  let fixture: ComponentFixture<CreditAppTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditAppTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
