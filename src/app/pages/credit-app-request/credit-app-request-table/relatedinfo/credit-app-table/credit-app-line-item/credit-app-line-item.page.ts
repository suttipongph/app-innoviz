import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'credit-app-line-item-page',
  templateUrl: './credit-app-line-item.page.html',
  styleUrls: ['./credit-app-line-item.page.scss']
})
export class CreditAppLineItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.CREDIT_APP_LINE,
      servicePath: `CreditAppRequestTable/${getProductType(this.masterRoute, true)}/RelatedInfo/CreditAppTable`
    };
  }
}
