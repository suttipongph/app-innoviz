import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditAppLineItemPage } from './credit-app-line-item.page';

describe('CreditAppLineItemPage', () => {
  let component: CreditAppLineItemPage;
  let fixture: ComponentFixture<CreditAppLineItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditAppLineItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppLineItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
