import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppTableRoutingModule } from './credit-app-table-routing.module';
import { CreditAppTableListPage } from './credit-app-table-list/credit-app-table-list.page';
import { CreditAppTableItemPage } from './credit-app-table-item/credit-app-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { CreditAppTableComponentModule } from 'components/credit-app-table/credit-app-table/credit-app-table.module';
import { CreditAppLineComponentModule } from 'components/credit-app-table/credit-app-line/credit-app-line.module';
import { CreditAppLineItemPage } from './credit-app-line-item/credit-app-line-item.page';

@NgModule({
  declarations: [CreditAppTableListPage, CreditAppTableItemPage, CreditAppLineItemPage],
  imports: [CommonModule, SharedModule, CreditAppTableRoutingModule, CreditAppTableComponentModule, CreditAppLineComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppTableModule {}
