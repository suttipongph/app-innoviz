import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'printbookdoctrans',
    loadChildren: () => import('./print-book-doc-trans/print-book-doc-trans-function.module').then((m) => m.PrintBookDocTransFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookmarkDocumentFunctionRoutingModule {}
