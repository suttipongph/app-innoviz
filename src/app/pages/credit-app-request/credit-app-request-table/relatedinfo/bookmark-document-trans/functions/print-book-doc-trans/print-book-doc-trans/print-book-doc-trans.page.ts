import { Component, OnInit } from '@angular/core';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel, PathParamModel } from 'shared/models/systemModel';

const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';

@Component({
  selector: 'app-print-book-doc-trans',
  templateUrl: './print-book-doc-trans.page.html',
  styleUrls: ['./print-book-doc-trans.page.scss']
})
export class PrintBookDocTransPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  activeName: string = this.uiService.getRelatedInfoActiveTableName();

  ngOnInit(): void {
    this.setPath();
  }

  setPath(): void {
    switch (this.activeName) {
      case LOAN_REQUEST:
        this.setPathLoanRequest();
        break;
      case BUYER_MATCHING:
        this.setPathBuyermatching();
        break;
      default:
        this.setPathMainCreditAppRequestTable();
        break;
    }
  }
  setPathLoanRequest(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.PRINT_BOOKMARK_DOCUMENT_TRANSACTION,
      servicePath: `CreditAppTable/${getProductType(
        this.masterRoute,
        true
      )}/RelatedInfo/LoanRequest/RelatedInfo/BookmarkDocumentTrans/Function/printbookdoctrans`
    };
  }
  setPathMainCreditAppRequestTable(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.PRINT_BOOKMARK_DOCUMENT_TRANSACTION,
      servicePath: `CreditAppRequestTable/${getProductType(this.masterRoute, true)}/RelatedInfo/BookmarkDocumentTrans/Function/printbookdoctrans`
    };
  }
  setPathBuyermatching(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.PRINT_BOOKMARK_DOCUMENT_TRANSACTION,
      servicePath: `CreditAppTable/${getProductType(
        this.masterRoute,
        true
      )}/RelatedInfo/BuyerMatching/RelatedInfo/BookmarkDocumentTrans/Function/printbookdoctrans`
    };
  }
}
