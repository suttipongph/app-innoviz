import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { OptionModel, PageInformationModel } from 'shared/models/systemModel';

const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';

@Component({
  selector: 'credit-app-req-bus-collaterl-info-item.page',
  templateUrl: './credit-app-req-bus-collaterl-info-item.page.html',
  styleUrls: ['./credit-app-req-bus-collaterl-info-item.page.scss']
})
export class CreditAppRequestTableBusinessCollateralInfoItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  option: OptionModel[];
  constructor(public uiService: UIControllerService) {}
  activeName: string = this.uiService.getRelatedInfoActiveTableName();
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    switch (this.activeName) {
      case LOAN_REQUEST:
        this.setPathLoanRequest();
        break;
      case BUYER_MATCHING:
        this.setPathBuyermatching();
        break;
      default:
        this.setPathMainCreditAppRequestTable();
        break;
    }
  }
  setPathMainCreditAppRequestTable(): void {
    const productTypeWording = getProductType(this.masterRoute, true);
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.CREDIT_APP_REQUEST_TABLE,
      servicePath: `CreditAppRequestTable/${productTypeWording}/RelatedInfo/BusinessCollateral`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.CUST_BUSINESS_COLLATERAL,
          servicePath: `CreditAppRequestTable/${productTypeWording}/RelatedInfo/BusinessCollateral`
        },
        {
          pagePath: ROUTE_RELATED_GEN.CREDIT_APP_REQUEST_TABLE_BUSINESS_COLLATERAL,
          servicePath: `CreditAppRequestTable/${productTypeWording}/RelatedInfo/BusinessCollateral`
        }
      ]
    };
  }
  setPathLoanRequest(): void {
    const productTypeWording = getProductType(this.masterRoute, true);
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.CREDIT_APP_REQUEST_TABLE,
      servicePath: `CreditAppTable/${productTypeWording}/RelatedInfo/LoanRequest/RelatedInfo/BusinessCollateral`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.CUST_BUSINESS_COLLATERAL,
          servicePath: `CreditAppTable/${productTypeWording}/RelatedInfo/LoanRequest/RelatedInfo/BusinessCollateral`
        },
        {
          pagePath: ROUTE_RELATED_GEN.CREDIT_APP_REQUEST_TABLE_BUSINESS_COLLATERAL,
          servicePath: `CreditAppTable/${productTypeWording}/RelatedInfo/LoanRequest/RelatedInfo/BusinessCollateral`
        }
      ]
    };
  }
  setPathBuyermatching(): void {
    const productTypeWording = getProductType(this.masterRoute, true);
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.CREDIT_APP_REQUEST_TABLE,
      servicePath: `CreditAppTable/${productTypeWording}/RelatedInfo/BuyerMatching/RelatedInfo/BusinessCollateral`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.CUST_BUSINESS_COLLATERAL,
          servicePath: `CreditAppTable/${productTypeWording}/RelatedInfo/BuyerMatching/RelatedInfo/BusinessCollateral`
        },
        {
          pagePath: ROUTE_RELATED_GEN.CREDIT_APP_REQUEST_TABLE_BUSINESS_COLLATERAL,
          servicePath: `CreditAppTable/${productTypeWording}/RelatedInfo/BuyerMatching/RelatedInfo/BusinessCollateral`
        }
      ]
    };
  }
}
