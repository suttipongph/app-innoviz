import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RetentionTransactionListPage } from './retention-transaction-list/retention-transaction-list.page';
import { RetentionTransactionItemPage } from './retention-transaction-item/retention-transaction-item.page';
import { SharedModule } from 'shared/shared.module';
import { RetentionTransComponentModule } from 'components/retention-trans/retention-trans.module';
import { from } from 'rxjs';
import { CreditAppRequestTableRetentionTransactionRelatedinfoRoutingModule } from './credit-app-request-table-retention-transaction-relatedinfo-routing.module';

@NgModule({
  declarations: [RetentionTransactionListPage, RetentionTransactionItemPage],
  imports: [CommonModule, SharedModule, CreditAppRequestTableRetentionTransactionRelatedinfoRoutingModule, RetentionTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppRequestTableRetentionTransactionRelatedinfoModule {}
