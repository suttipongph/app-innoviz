import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, RefType, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';

@Component({
  selector: 'retention-transaction-list-page',
  templateUrl: './retention-transaction-list.page.html',
  styleUrls: ['./retention-transaction-list.page.scss']
})
export class RetentionTransactionListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();

  constructor(public uiService: UIControllerService) {}
  activeName: string = this.uiService.getRelatedInfoActiveTableName();
  activeKey: string = this.uiService.getRelatedInfoParentTableKey();

  ngOnInit(): void {
    this.setPath();
    this.setOption();
  }
  setPath(): void {
    switch (this.activeName) {
      case LOAN_REQUEST:
        this.setPathLoanRequest();
        break;
      case BUYER_MATCHING:
        this.setPathBuyermatching();
        break;
      default:
        this.setPathMainCreditAppRequestTable();
        break;
    }
  }
  setOption(): void {
    this.option = new OptionModel();
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'RefGUID',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.activeKey
      },
      {
        label: null,
        textKey: 'RefType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: RefType.CreditAppRequestTable.toString()
      }
    ];
    this.option.columns = columns;
  }
  setPathMainCreditAppRequestTable(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.RETENTION_TRANSACTION,
      servicePath: `CreditAppRequestTable/${getProductType(this.masterRoute, true)}/RelatedInfo/RetentionTransaction`
    };
  }
  setPathLoanRequest(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.RETENTION_TRANSACTION,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/LoanRequest/RelatedInfo/RetentionTransaction`
    };
  }
  setPathBuyermatching(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.RETENTION_TRANSACTION,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/BuyerMatching/RelatedInfo/RetentionTransaction`
    };
  }
}
