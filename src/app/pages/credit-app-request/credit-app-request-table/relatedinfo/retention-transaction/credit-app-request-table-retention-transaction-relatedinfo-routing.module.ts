import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RetentionTransactionListPage } from './retention-transaction-list/retention-transaction-list.page';
import { RetentionTransactionItemPage } from './retention-transaction-item/retention-transaction-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: RetentionTransactionListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: RetentionTransactionItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppRequestTableRetentionTransactionRelatedinfoRoutingModule {}
