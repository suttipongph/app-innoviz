import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppRequestTableCaBuyerCreditOutstandingRelatedinfoRoutingModule } from './ca-buyer-credit-outstanding-relatedinfo-routing.module';
import { CaBuyerCreditOutstandingListPage } from './ca-buyer-credit-outstanding-list/ca-buyer-credit-outstanding-list.page';
import { SharedModule } from 'shared/shared.module';
import { CAReqBuyerCreditOutstandingComponentModule } from 'components/ca-req-buyer-credit-outstanding/ca-req-buyer-credit-outstanding/ca-req-buyer-credit-outstanding.module';

@NgModule({
  declarations: [CaBuyerCreditOutstandingListPage],
  imports: [
    CommonModule,
    SharedModule,
    CreditAppRequestTableCaBuyerCreditOutstandingRelatedinfoRoutingModule,
    CAReqBuyerCreditOutstandingComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppRequestTableCaBuyerCreditOutstandingRelatedinfoModule {}
