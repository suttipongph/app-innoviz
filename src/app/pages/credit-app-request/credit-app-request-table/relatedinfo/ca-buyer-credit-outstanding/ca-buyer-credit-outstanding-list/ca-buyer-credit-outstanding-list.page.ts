import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';
const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';

@Component({
  selector: 'ca-buyer-credit-outstanding-list-page',
  templateUrl: './ca-buyer-credit-outstanding-list.page.html',
  styleUrls: ['./ca-buyer-credit-outstanding-list.page.scss']
})
export class CaBuyerCreditOutstandingListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  parentKey: string = null;
  constructor(public uiService: UIControllerService) {
    this.parentKey = uiService.getRelatedInfoParentTableKey();
  }
  activeName: string = this.uiService.getRelatedInfoActiveTableName();

  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'CaBuyerCreditOutstandingGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'creditAppRequestTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentKey
      }
    ];
    this.option.columns = columns;
  }
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setPath(): void {
    switch (this.activeName) {
      case LOAN_REQUEST:
        this.setPathLoanRequest();
        break;
      case BUYER_MATCHING:
        this.setPathBuyermatching();
        break;
      default:
        this.setPathMainCreditAppRequestTable();
        break;
    }
  }
  setPathLoanRequest(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.CA_BUYER_CREDIT_OUTSTANDING,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/LoanRequest/RelatedInfo/CaBuyerCreditOutstanding`
    };
  }
  setPathMainCreditAppRequestTable(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.CA_BUYER_CREDIT_OUTSTANDING,
      servicePath: `CreditAppRequestTable/${getProductType(this.masterRoute, true)}/RelatedInfo/CaBuyerCreditOutstanding`
    };
  }
  setPathBuyermatching(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.CA_BUYER_CREDIT_OUTSTANDING,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/BuyerMatching/RelatedInfo/CaBuyerCreditOutstanding`
    };
  }
}
