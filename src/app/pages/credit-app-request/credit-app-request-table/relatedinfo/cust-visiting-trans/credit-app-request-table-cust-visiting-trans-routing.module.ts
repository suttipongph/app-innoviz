import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustVisitingTransListPage } from './cust-visiting-trans-list/cust-visiting-trans-list.page';
import { CustVisitingTransItemPage } from './cust-visiting-trans-item/cust-visiting-trans-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: CustVisitingTransListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CustVisitingTransItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'function',
    loadChildren: () =>
      import('./functions/credit-app-request-table-function.module').then((m) => m.CreditAppReqTableCustVisitingRelatedinfoFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppRequestTableCustVisitingTransRoutingModule {}
