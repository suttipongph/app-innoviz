import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'copycustvisiting',
    loadChildren: () =>
      import('./copy-cust-visiting/copy-cust-visiting-function.module').then(
        (m) => m.CreditAppReqTableCustVisitingRelCopyCustVisitingFunctionFunctionModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppReqTableCustVisitingRelatedinfoFunctionRoutingModule {}
