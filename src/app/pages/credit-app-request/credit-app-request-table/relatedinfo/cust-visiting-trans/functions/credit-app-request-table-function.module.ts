import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'primeng/api';
import { CreditAppReqTableCustVisitingRelatedinfoFunctionRoutingModule } from './credit-app-request-table-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, CreditAppReqTableCustVisitingRelatedinfoFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppReqTableCustVisitingRelatedinfoFunctionModule {}
