import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppReqTableCustVisitingRelCopyCustVisitingFunctionRoutingModule } from './copy-cust-visiting-function-routing.module';
import { CopyCustVisitingTransPage } from './copy-cust-visiting/copy-cust-visiting.page';
import { SharedModule } from 'shared/shared.module';
import { CopyCustVisitingComponentModule } from 'components/cust-visiting-trans/copy-cust-visiting/copy-cust-visiting.module';

@NgModule({
  declarations: [CopyCustVisitingTransPage],
  imports: [CommonModule, SharedModule, CreditAppReqTableCustVisitingRelCopyCustVisitingFunctionRoutingModule, CopyCustVisitingComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppReqTableCustVisitingRelCopyCustVisitingFunctionFunctionModule {}
