import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CopyCustVisitingTransPage } from './copy-cust-visiting/copy-cust-visiting.page';

const routes: Routes = [
  {
    path: '',
    component: CopyCustVisitingTransPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppReqTableCustVisitingRelCopyCustVisitingFunctionRoutingModule {}
