import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NCBTransListPage } from './ncb-trans-list.page';

describe('NCBTransListPage', () => {
  let component: NCBTransListPage;
  let fixture: ComponentFixture<NCBTransListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NCBTransListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NCBTransListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
