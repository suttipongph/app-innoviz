import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NCBTransListPage } from './ncb-trans-list/ncb-trans-list.page';
import { NCBTransItemPage } from './ncb-trans-item/ncb-trans-item.page';
import { SharedModule } from 'shared/shared.module';
import { NCBTransComponentModule } from 'components/ncb-trans/ncb-trans.module';
import { CreditAppRequestTableNCBTransRelatedinfoRoutingModule } from './credit-app-request-table-ncb-trans-relatedinfo-routing.module';

@NgModule({
  declarations: [NCBTransListPage, NCBTransItemPage],
  imports: [CommonModule, SharedModule, CreditAppRequestTableNCBTransRelatedinfoRoutingModule, NCBTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppRequestTableNCBRelatedinfoModule {}
