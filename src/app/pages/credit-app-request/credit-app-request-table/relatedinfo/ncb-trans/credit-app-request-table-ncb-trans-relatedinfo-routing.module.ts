import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { NCBTransItemPage } from './ncb-trans-item/ncb-trans-item.page';
import { NCBTransListPage } from './ncb-trans-list/ncb-trans-list.page';

const routes: Routes = [
  {
    path: '',
    component: NCBTransListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: NCBTransItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppRequestTableNCBTransRelatedinfoRoutingModule {}
