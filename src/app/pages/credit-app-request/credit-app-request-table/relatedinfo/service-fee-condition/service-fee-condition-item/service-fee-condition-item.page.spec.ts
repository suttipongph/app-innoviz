import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ServiceFeeConditionItemPage } from './service-fee-condition-item.page';

describe('ServiceFeeConditionItemPage', () => {
  let component: ServiceFeeConditionItemPage;
  let fixture: ComponentFixture<ServiceFeeConditionItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ServiceFeeConditionItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceFeeConditionItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
