import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TaxReportTransListPage } from './tax-report-trans-list/tax-report-trans-list.page';
import { TaxReportTransItemPage } from './tax-report-trans-item/tax-report-trans-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: TaxReportTransListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: TaxReportTransItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'function',
    loadChildren: () =>
      import('./functions/credit-app-request-table-function.module').then((m) => m.CreditAppReqTableFinancialRelatedinfoFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppRequestTableTaxReportTransRoutingModule {}
