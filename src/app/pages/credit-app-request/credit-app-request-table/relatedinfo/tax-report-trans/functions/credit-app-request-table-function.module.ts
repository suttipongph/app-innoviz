import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'primeng/api';
import { CreditAppReqTableFinancialRelatedinfoFunctionRoutingModule } from './credit-app-request-table-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, CreditAppReqTableFinancialRelatedinfoFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppReqTableFinancialRelatedinfoFunctionModule {}
