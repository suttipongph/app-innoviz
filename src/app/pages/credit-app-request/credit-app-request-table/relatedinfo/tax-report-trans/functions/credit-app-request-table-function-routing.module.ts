import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'copytaxreporttrans',
    loadChildren: () =>
      import('./copy-tax-report-trans/copy-tax-report-trans-function.module').then(
        (m) => m.CreditAppReqTableFinancialRelatedinfoCopyTaxReportFunctionModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppReqTableFinancialRelatedinfoFunctionRoutingModule {}
