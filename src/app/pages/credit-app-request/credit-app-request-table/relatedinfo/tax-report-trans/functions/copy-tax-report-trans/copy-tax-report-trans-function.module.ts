import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppReqTableFinancialRelatedinfoCopyTaxReportFunctionRoutingModule } from './copy-tax-report-trans-function-routing.module';
import { CopyTaxReportTransPage } from './copy-tax-report-trans/copy-tax-report-trans.page';
import { SharedModule } from 'shared/shared.module';
import { CopyTaxReportTransComponentModule } from 'components/tax-report-trans/copy-tax-report-trans/copy-tax-report-trans.module';

@NgModule({
  declarations: [CopyTaxReportTransPage],
  imports: [CommonModule, SharedModule, CreditAppReqTableFinancialRelatedinfoCopyTaxReportFunctionRoutingModule, CopyTaxReportTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppReqTableFinancialRelatedinfoCopyTaxReportFunctionModule {}
