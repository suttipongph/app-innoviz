import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CopyTaxReportTransPage } from './copy-tax-report-trans.page';

describe('CopyTaxReportTransPage', () => {
  let component: CopyTaxReportTransPage;
  let fixture: ComponentFixture<CopyTaxReportTransPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CopyTaxReportTransPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyTaxReportTransPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
