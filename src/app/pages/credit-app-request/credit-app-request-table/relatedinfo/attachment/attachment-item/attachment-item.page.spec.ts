import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AttachmentItemPage } from './attachment-item.page';

describe('AttachmentItemPage', () => {
  let component: AttachmentItemPage;
  let fixture: ComponentFixture<AttachmentItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AttachmentItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachmentItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
