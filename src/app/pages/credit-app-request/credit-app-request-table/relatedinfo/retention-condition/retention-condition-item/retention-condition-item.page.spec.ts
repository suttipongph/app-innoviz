import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RetentionConditionItemPage } from './retention-condition-item.page';

describe('RetentionConditionItemPage', () => {
  let component: RetentionConditionItemPage;
  let fixture: ComponentFixture<RetentionConditionItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RetentionConditionItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RetentionConditionItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
