import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'retention-condition-item.page',
  templateUrl: './retention-condition-item.page.html',
  styleUrls: ['./retention-condition-item.page.scss']
})
export class RetentionConditionItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.RETENTION_CONDITION_TRANS,
      servicePath: `CreditAppRequestTable/${getProductType(this.masterRoute, true)}/RelatedInfo/RetentionConditionTrans`
    };
  }
}
