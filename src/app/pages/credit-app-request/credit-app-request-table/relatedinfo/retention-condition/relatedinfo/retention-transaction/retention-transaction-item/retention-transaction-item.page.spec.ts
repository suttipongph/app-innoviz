import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RetentionTransactionItemPage } from './retention-transaction-item.page';

describe('RetentionTransactionItemPage', () => {
  let component: RetentionTransactionItemPage;
  let fixture: ComponentFixture<RetentionTransactionItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RetentionTransactionItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RetentionTransactionItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
