import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'retention-transaction-item.page',
  templateUrl: './retention-transaction-item.page.html',
  styleUrls: ['./retention-transaction-item.page.scss']
})
export class RetentionTransactionItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();

  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.RETENTION_TRANSACTION,
      servicePath: `CreditApprequestTable/${getProductType(
        this.masterRoute,
        true
      )}/RelatedInfo/RetentionConditionTrans/RelatedInfo/RetentionTransaction`
    };
  }
}
