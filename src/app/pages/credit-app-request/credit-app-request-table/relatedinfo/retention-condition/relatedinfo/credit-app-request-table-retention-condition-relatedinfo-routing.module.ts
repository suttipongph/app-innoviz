import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'retentiontransaction',
    loadChildren: () =>
      import('./retention-transaction/retention-transaction-relatedinfo.module').then((m) => m.RetentionTransactionRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppRequestTableRetentionConditionRelatedinfoRoutingModule {}
