import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, RefType, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'retention-transaction-list-page',
  templateUrl: './retention-transaction-list.page.html',
  styleUrls: ['./retention-transaction-list.page.scss']
})
export class RetentionTransactionListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  parentId = this.uiService.getRelatedInfoActiveTableKey();
  masterId = this.uiService.getRelatedInfoOriginTableKey();
  constructor(public uiService: UIControllerService) {}
  masterRoute = this.uiService.getMasterRoute();
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'retentionTransGUID';
    const columns: ColumnModel[] = [];
    this.setColumnOptionByPath(columns);
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.RETENTION_TRANSACTION,
      servicePath: `CreditApprequestTable/${getProductType(
        this.masterRoute,
        true
      )}/RelatedInfo/RetentionConditionTrans/RelatedInfo/RetentionTransaction`
    };
  }
  setColumnOptionByPath(columns: ColumnModel[]): any {
    columns.push({
      label: null,
      textKey: 'refGUID',
      type: ColumnType.MASTER,
      visibility: true,
      sorting: SortType.NONE,
      parentKey: this.masterId
    });
    columns.push({
      label: null,
      textKey: 'refType',
      type: ColumnType.MASTER,
      visibility: true,
      sorting: SortType.NONE,
      parentKey: RefType.CreditAppRequestTable.toString()
    });
  }
}
