import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RetentionConditionListPage } from './retention-condition-list/retention-condition-list.page';
import { RetentionConditionItemPage } from './retention-condition-item/retention-condition-item.page';
import { CreditAppRequestTableRetentionConditionRelatedinfoRoutingModule } from './credit-app-request-table-retention-condition-relatedinfo-routing.module';
import { SharedModule } from 'shared/shared.module';
import { RetentionConditionTransComponentModule } from 'components/retention-condition-trans/retention-condition-trans.module';

@NgModule({
  declarations: [RetentionConditionListPage, RetentionConditionItemPage],
  imports: [CommonModule, SharedModule, CreditAppRequestTableRetentionConditionRelatedinfoRoutingModule, RetentionConditionTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppRequestTableRetentionConditionRelatedinfoModule {}
