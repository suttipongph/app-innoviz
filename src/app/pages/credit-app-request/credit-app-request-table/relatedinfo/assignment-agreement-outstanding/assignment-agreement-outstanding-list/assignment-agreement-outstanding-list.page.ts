import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';

@Component({
  selector: 'assignment-agreement-outstanding-list-page',
  templateUrl: './assignment-agreement-outstanding-list.page.html',
  styleUrls: ['./assignment-agreement-outstanding-list.page.scss']
})
export class AssignmentAgreementOutstandingListPage implements OnInit {
  activeName: string = this.uiService.getRelatedInfoActiveTableName();
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'assignmentAgreementOutstandingGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    switch (this.activeName) {
      case LOAN_REQUEST:
        this.setPathLoanRequest();
        break;
      case BUYER_MATCHING:
        this.setPathBuyermatching();
        break;
      default:
        this.setPathMainCreditAppRequestTable();
        break;
    }
  }

  setPathMainCreditAppRequestTable(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.ASSIGNMENT_AGREEMENT_OUTSTANDING,
      servicePath: `CreditAppRequestTable/${getProductType(this.masterRoute, true)}/RelatedInfo/AssignmentAgreementOutstanding`
    };
  }
  setPathBuyermatching(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.ASSIGNMENT_AGREEMENT_OUTSTANDING,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/BuyerMatching/RelatedInfo/AssignmentAgreementOutstanding`
    };
  }
  setPathLoanRequest(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.ASSIGNMENT_AGREEMENT_OUTSTANDING,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/LoanRequest/RelatedInfo/AssignmentAgreementOutstanding`
    };
  }
}
