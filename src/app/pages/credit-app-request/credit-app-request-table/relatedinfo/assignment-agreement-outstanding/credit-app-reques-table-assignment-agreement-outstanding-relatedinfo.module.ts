import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssignmentAgreementOutstandingListPage } from './assignment-agreement-outstanding-list/assignment-agreement-outstanding-list.page';
import { CreditAppRequestTableAssignmentAgreementOutstandingRelatedinfoRoutingModule } from './credit-app-reques-table-assignment-agreement-outstanding-relatedinfo-routing.module';
import { SharedModule } from 'shared/shared.module';
import { CAReqAssignmentOutstandingComponentModule } from 'components/ca-req-assignment-outstanding/ca-req-assignment-outstanding/ca-req-assignment-outstanding.module';

@NgModule({
  declarations: [AssignmentAgreementOutstandingListPage],
  imports: [
    CommonModule,
    SharedModule,
    CreditAppRequestTableAssignmentAgreementOutstandingRelatedinfoRoutingModule,
    CAReqAssignmentOutstandingComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppRequestTableAssignmentAgreementOutstandingRelatedinfoModule {}
