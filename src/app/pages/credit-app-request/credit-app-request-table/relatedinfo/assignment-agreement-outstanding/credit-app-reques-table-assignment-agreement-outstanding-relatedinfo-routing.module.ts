import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { AssignmentAgreementOutstandingListPage } from './assignment-agreement-outstanding-list/assignment-agreement-outstanding-list.page';

const routes: Routes = [
  {
    path: '',
    component: AssignmentAgreementOutstandingListPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppRequestTableAssignmentAgreementOutstandingRelatedinfoRoutingModule {}
