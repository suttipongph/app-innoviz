import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OwnerTransListPage } from './owner-trans-list/owner-trans-list.page';
import { OwnerTransItemPage } from './owner-trans-item/owner-trans-item.page';
import { SharedModule } from 'shared/shared.module';
import { OwnerTransComponentModule } from 'components/owner-trans/owner-trans.module';
import { CreditAppRequestTableOwnerTransRoutingModule } from './credit-app-request-table-owner-trans-routing.module';

@NgModule({
  declarations: [OwnerTransListPage, OwnerTransItemPage],
  imports: [CommonModule, SharedModule, CreditAppRequestTableOwnerTransRoutingModule, OwnerTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppRequestTableOwnerTransModule {}
