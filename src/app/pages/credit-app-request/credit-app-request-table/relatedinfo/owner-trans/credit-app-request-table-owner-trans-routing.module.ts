import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OwnerTransListPage } from './owner-trans-list/owner-trans-list.page';
import { OwnerTransItemPage } from './owner-trans-item/owner-trans-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: OwnerTransListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: OwnerTransItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppRequestTableOwnerTransRoutingModule {}
