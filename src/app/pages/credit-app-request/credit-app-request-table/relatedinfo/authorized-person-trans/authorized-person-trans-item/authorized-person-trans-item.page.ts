import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { MenuItem } from 'shared/models/primeModel';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';
import { getProductType } from 'shared/functions/value.function';

@Component({
  selector: 'authorized-person-trans-item.page',
  templateUrl: './authorized-person-trans-item.page.html',
  styleUrls: ['./authorized-person-trans-item.page.scss']
})
export class AuthorizedPersonTransItemPage implements OnInit {
  pageInfo: PageInformationModel;
  relatedInfoItems: MenuItem[];
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
    this.setRelatedInfoOptions();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.AUTHORIZED_PERSON_TRANS,
      servicePath: `CreditAppRequestTable/${getProductType(this.masterRoute, true)}/RelatedInfo/AuthorizedPersonTrans`
    };
  }
  setRelatedInfoOptions(): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.MANAGE',
        items: [
          {
            label: 'LABEL.NCB',
            command: () => this.uiService.toRelatedInfo({ path: ROUTE_RELATED_GEN.NCB_TRANS })
          }
        ]
      }
    ];
  }
}
