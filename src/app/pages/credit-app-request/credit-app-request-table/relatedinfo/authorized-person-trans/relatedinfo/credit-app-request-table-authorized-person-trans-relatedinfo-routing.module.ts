import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'ncbtrans',
    loadChildren: () =>
      import('./ncb/credit-app-request-table-authorized-person-trans-ncb-relatedinfo.module').then(
        (m) => m.CreditAppRequestTableAuthorizedPersonTransNCBRelatedinfoModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppRequestTableAuthorizedPersonTransRelatedinfoRoutingModule {}
