import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NCBListPage } from './ncb-list/ncb-list.page';
import { NCBItemPage } from './ncb-item/ncb-item.page';
import { SharedModule } from 'shared/shared.module';
import { NCBTransComponentModule } from 'components/ncb-trans/ncb-trans.module';
import { CreditAppRequestTableAuthorizedPersonTransNCBRelatedinfoRoutingModule } from './credit-app-request-table-authorized-person-trans-ncb-relatedinfo-routing.module';

@NgModule({
  declarations: [NCBListPage, NCBItemPage],
  imports: [CommonModule, SharedModule, CreditAppRequestTableAuthorizedPersonTransNCBRelatedinfoRoutingModule, NCBTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppRequestTableAuthorizedPersonTransNCBRelatedinfoModule {}
