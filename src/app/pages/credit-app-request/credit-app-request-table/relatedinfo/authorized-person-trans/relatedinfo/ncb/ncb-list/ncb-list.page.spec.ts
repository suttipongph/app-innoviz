import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NCBListPage } from './ncb-list.page';

describe('NCBListPage', () => {
  let component: NCBListPage;
  let fixture: ComponentFixture<NCBListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NCBListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NCBListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
