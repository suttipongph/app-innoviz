import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NCBListPage } from './ncb-list/ncb-list.page';
import { NCBItemPage } from './ncb-item/ncb-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: NCBListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: NCBItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppRequestTableAuthorizedPersonTransNCBRelatedinfoRoutingModule {}
