import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NCBItemPage } from './ncb-item.page';

describe('NCBItem', () => {
  let component: NCBItemPage;
  let fixture: ComponentFixture<NCBItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NCBItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NCBItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
