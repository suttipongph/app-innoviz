import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { CreditAppRequestTableAuthorizedPersonTransRelatedinfoRoutingModule } from './credit-app-request-table-authorized-person-trans-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, CreditAppRequestTableAuthorizedPersonTransRelatedinfoRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppRequestTableAuthorizedPersonTransRelatedinfoModule {}
