import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-ncb-item-page',
  templateUrl: './ncb-item.page.html',
  styleUrls: ['./ncb-item.page.scss']
})
export class NCBItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.NCB_TRANS,
      servicePath: `CreditApprequestTable/${getProductType(this.masterRoute, true)}/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans`
    };
  }
}
