import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { ExistingAssignmentAgreementItemPage } from './existing-assignment-agreement-item/existing-assignment-agreement-item.page';
import { CreditAppReqAssignmentInfoItemPage } from './credit-app-req-assignment-info-item/credit-app-req-assignment-info-item.page';
import { CreditAppReqAssignmentItemPage } from './credit-app-req-assignment-item/credit-app-req-assignment-item.page';

const routes: Routes = [
  {
    path: '',
    component: CreditAppReqAssignmentInfoItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'creditapprequesttableassignment-child/:id',
    component: CreditAppReqAssignmentItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'existingassignment-child/:id',
    component: ExistingAssignmentAgreementItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppRequestTableAssignmentRelatedinfoRoutingModule {}
