import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ExistingAssignmentAgreementItemPage } from './existing-assignment-agreement-item.page';

describe('BusinessCollateralItemPage', () => {
  let component: ExistingAssignmentAgreementItemPage;
  let fixture: ComponentFixture<ExistingAssignmentAgreementItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExistingAssignmentAgreementItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExistingAssignmentAgreementItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
