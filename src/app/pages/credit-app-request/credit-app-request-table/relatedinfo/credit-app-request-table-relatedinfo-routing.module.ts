import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'authorizedpersontrans',
    loadChildren: () =>
      import('./authorized-person-trans/credit-app-request-table-authorized-person-trans.module').then(
        (m) => m.CreditAppRequestTableAuthorizedPersonTransModule
      )
  },
  {
    path: 'financialstatementtrans',
    loadChildren: () =>
      import('./financial-statement-trans/credit-app-request-table-financial-statement-trans.module').then(
        (m) => m.CreditAppRequestTableFinancialStatementTransModule
      )
  },
  {
    path: 'taxreporttrans',
    loadChildren: () =>
      import('./tax-report-trans/credit-app-request-table-tax-report-trans.module').then((m) => m.CreditAppRequestTableTaxReportTransModule)
  },
  {
    path: 'servicefeeconditiontrans',
    loadChildren: () =>
      import('./service-fee-condition/credit-app-request-table-service-fee-condition-relatedinfo.module').then(
        (m) => m.CreditAppRequestTableServiceFeeConditionRelatedinfoModule
      )
  },
  {
    path: 'retentionconditiontrans',
    loadChildren: () =>
      import('./retention-condition/credit-app-request-table-retention-condition-relatedinfo.module').then(
        (m) => m.CreditAppRequestTableRetentionConditionRelatedinfoModule
      )
  },
  {
    path: 'custvisitingtrans',
    loadChildren: () =>
      import('./cust-visiting-trans/credit-app-request-table-cust-visiting-trans.module').then((m) => m.CreditAppRequestTableCustVisitingTransModule)
  },
  {
    path: 'guarantortrans',
    loadChildren: () =>
      import('./guarantor-trans/credit-app-request-table-guarantor-trans.module').then((m) => m.CreditAppRequestTableGuarantorTransModule)
  },
  {
    path: 'ncbtrans',
    loadChildren: () =>
      import('./ncb-trans/credit-app-request-table-ncb-trans-relatedinfo.module').then((m) => m.CreditAppRequestTableNCBRelatedinfoModule)
  },
  {
    path: 'ownertrans',
    loadChildren: () => import('./owner-trans/credit-app-request-table-owner-trans.module').then((m) => m.CreditAppRequestTableOwnerTransModule)
  },
  {
    path: 'memotrans',
    loadChildren: () =>
      import('./memo/credit-app-reques-table-memo-trans-relatedinfo.module').then((m) => m.CreditAppRequestTableMemoTransRelatedinfoModule)
  },
  {
    path: 'financialcredittrans',
    loadChildren: () =>
      import('./financial-credit/financial-credit-trans-relatedinfo.module').then((m) => m.CreditAppRequestTableFinancialCreditRelatedinfoModule)
  },
  {
    path: 'businesscollateral',
    loadChildren: () =>
      import('./credit-app-req-bus-collaterl-info/credit-app-request-business-collateral-relatedinfo.module').then(
        (m) => m.CreditAppRequestTableBusinessCollateralRelatedinfoModule
      )
  },
  {
    path: 'bookmarkdocumenttrans',
    loadChildren: () =>
      import('./bookmark-document-trans/credit-app-request-table-bookmark-document-trans-relatedinfo.module').then(
        (m) => m.CreditAppRequestTableBookmarkDocumentRelatedinfoModule
      )
  },
  {
    path: 'assignmentagreementoutstanding',
    loadChildren: () =>
      import('./assignment-agreement-outstanding/credit-app-reques-table-assignment-agreement-outstanding-relatedinfo.module').then(
        (m) => m.CreditAppRequestTableAssignmentAgreementOutstandingRelatedinfoModule
      )
  },
  {
    path: 'attachment',
    loadChildren: () =>
      import('./attachment/credit-app-reques-table-attachment-relatedinfo.module').then((m) => m.CreditAppRequestTableAttachmentRelatedinfoModule)
  },
  {
    path: 'retentiontransaction',
    loadChildren: () =>
      import('./retention-transaction/credit-app-request-table-retention-transaction-relatedinfo.module').then(
        (m) => m.CreditAppRequestTableRetentionTransactionRelatedinfoModule
      )
  },
  {
    path: 'creditapptable',
    loadChildren: () => import('./credit-app-table/credit-app-table.module').then((m) => m.CreditAppTableModule)
  },
  {
    path: 'retentionoutstanding',
    loadChildren: () =>
      import('./retention-outstanding/credit-app-request-table-retention-outstanding-relatedinfo.module').then(
        (m) => m.CreditAppRequestTableRetentionOutstandingRelatedinfoModule
      )
  },
  {
    path: 'assignmentagreementoutstanding',
    loadChildren: () =>
      import('./assignment-agreement-outstanding/credit-app-reques-table-assignment-agreement-outstanding-relatedinfo.module').then(
        (m) => m.CreditAppRequestTableAssignmentAgreementOutstandingRelatedinfoModule
      )
  },
  {
    path: 'creditoutstanding',
    loadChildren: () =>
      import('./credit-outstanding/credit-app-request-credit-outstanding-relatedinfo.module').then(
        (m) => m.CreditAppRequestTableCreditOutstandingRelatedinfoModule
      )
  },
  {
    path: 'assignment',
    loadChildren: () =>
      import('./credit-app-req-assignment-info/credit-app-request-assignment-relatedinfo.module').then(
        (m) => m.CreditAppRequestTableAssignmentRelatedinfoModule
      )
  },
  {
    path: 'cabuyercreditoutstanding',
    loadChildren: () =>
      import('./ca-buyer-credit-outstanding/ca-buyer-credit-outstanding-relatedinfo.module').then(
        (m) => m.CreditAppRequestTableCaBuyerCreditOutstandingRelatedinfoModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppRequestTableRelatedinfoRoutingModule {}
