import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GuarantorTransListPage } from './guarantor-trans-list/guarantor-trans-list.page';
import { GuarantorTransItemPage } from './guarantor-trans-item/guarantor-trans-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: GuarantorTransListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: GuarantorTransItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppRequestTableGuarantorTransRoutingModule {}
