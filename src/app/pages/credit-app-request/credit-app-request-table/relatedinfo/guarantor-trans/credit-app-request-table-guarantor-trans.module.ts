import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GuarantorTransListPage } from './guarantor-trans-list/guarantor-trans-list.page';
import { GuarantorTransItemPage } from './guarantor-trans-item/guarantor-trans-item.page';
import { SharedModule } from 'shared/shared.module';
import { GuarantorTransComponentModule } from 'components/guarantor-trans/guarantor-trans/guarantor-trans.module';
import { CreditAppRequestTableGuarantorTransRoutingModule } from './credit-app-request-table-guarantor-trans-routing.module';

@NgModule({
  declarations: [GuarantorTransListPage, GuarantorTransItemPage],
  imports: [CommonModule, SharedModule, CreditAppRequestTableGuarantorTransRoutingModule, GuarantorTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppRequestTableGuarantorTransModule {}
