import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RetentionOutstandingListPage } from './retention-outstanding-list.page';

describe('RetentionOutstandingListPage', () => {
  let component: RetentionOutstandingListPage;
  let fixture: ComponentFixture<RetentionOutstandingListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RetentionOutstandingListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RetentionOutstandingListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
