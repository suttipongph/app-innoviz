import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CopyFinancialStatementTransPage } from './copy-financial-statement-trans/copy-financial-statement-trans.page';

const routes: Routes = [
  {
    path: '',
    component: CopyFinancialStatementTransPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppReqTableFinancialRelatedinfoCopyFinancialFunctionRoutingModule {}
