import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditAppRequestTableItemPage } from './credit-app-request-table-item.page';

describe('CreditAppRequestTableItemPage', () => {
  let component: CreditAppRequestTableItemPage;
  let fixture: ComponentFixture<CreditAppRequestTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditAppRequestTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppRequestTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
