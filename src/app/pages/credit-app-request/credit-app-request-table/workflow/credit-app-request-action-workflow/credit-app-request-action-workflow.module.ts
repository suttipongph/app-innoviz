import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { CreditAppRequestActionWorkflowRoutingModule } from './credit-app-request-action-workflow-routing.module';
import { StartWorkflowCreditAppRequestPage } from './start-workflow-credit-app-request/start-workflow-credit-app-request-page';
import { WorkFlowCreditAppRequestComponentModule } from 'components/credit-app-request/work-flow-credit-app-request/work-flow-credit-app-request.module';
import { ActionWorkflowCreditAppRequestPage } from './action-workflow-credit-app-request/action-workflow-credit-app-request-page';

@NgModule({
  declarations: [StartWorkflowCreditAppRequestPage, ActionWorkflowCreditAppRequestPage],
  imports: [CommonModule, SharedModule, CreditAppRequestActionWorkflowRoutingModule, WorkFlowCreditAppRequestComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppRequestActionWorkflowModule {}
