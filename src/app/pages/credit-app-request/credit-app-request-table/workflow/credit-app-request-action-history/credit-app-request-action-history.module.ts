import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { CreditAppRequestActionHistoryRoutingModule } from './credit-app-request-action-history-routing.module';
import { WorkFlowCreditAppRequestActionHistoryPage } from './credit-app-request-action-history/credit-app-request-action-history.page';
import { ActionHistoryComponentModule } from 'components/action-history/action-history.module';

@NgModule({
  declarations: [WorkFlowCreditAppRequestActionHistoryPage],
  imports: [CommonModule, SharedModule, CreditAppRequestActionHistoryRoutingModule, ActionHistoryComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppRequestActionHistoryModule {}
