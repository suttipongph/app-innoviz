import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { WorkFlowCreditAppRequestActionHistoryPage } from './credit-app-request-action-history/credit-app-request-action-history.page';

const routes: Routes = [
  {
    path: '',
    component: WorkFlowCreditAppRequestActionHistoryPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppRequestActionHistoryRoutingModule {}
