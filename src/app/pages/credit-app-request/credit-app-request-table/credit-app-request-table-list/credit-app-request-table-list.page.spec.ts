import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditAppRequestTableListPage } from './credit-app-request-table-list.page';

describe('CreditAppRequestTableListPage', () => {
  let component: CreditAppRequestTableListPage;
  let fixture: ComponentFixture<CreditAppRequestTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditAppRequestTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppRequestTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
