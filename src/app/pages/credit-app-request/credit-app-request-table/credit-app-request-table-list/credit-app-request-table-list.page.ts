import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ColumnType, CreditAppRequestType, ROUTE_MASTER_GEN, SortType } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';
const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';

@Component({
  selector: 'credit-app-request-table-list-page',
  templateUrl: './credit-app-request-table-list.page.html',
  styleUrls: ['./credit-app-request-table-list.page.scss']
})
export class CreditAppRequestTableListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  productType: string = getProductType(this.masterRoute);
  productTypeWording: string = getProductType(this.masterRoute, true);
  activeName: string = this.uiService.getRelatedInfoActiveTableName();
  parentId: string = null;
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setOption();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'creditAppRequestTableGUID';
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
    this.setPath();
  }
  setPath(): void {
    switch (this.activeName) {
      case LOAN_REQUEST:
        this.setOptionLoanRequest();
        this.setPathLoanRequest();
        break;
      case BUYER_MATCHING:
        this.setOptionBuyermatching();
        this.setPathBuyermatching();
        break;
      default:
        this.setOptionMainCreditLimit();
        this.setPathMainCreditLimit();
        break;
    }
  }
  setOptionMainCreditLimit(): void {
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'productType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.productType
      },
      {
        label: null,
        textKey: 'creditAppRequestType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: CreditAppRequestType.MainCreditLimit.toString()
      }
    ];
    this.option.columns = columns;
  }
  setPathMainCreditLimit(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.CREDIT_APP_REQUEST_TABLE,
      servicePath: `CreditAppRequestTable/${this.productTypeWording}`
    };
  }
  setOptionLoanRequest(): void {
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'productType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: getProductType(this.masterRoute)
      },
      {
        label: null,
        textKey: 'creditAppRequestType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: CreditAppRequestType.LoanRequest.toString()
      },
      {
        label: null,
        textKey: 'refCreditAppTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
  }
  setPathLoanRequest(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.PROJECT_FINANCE_CREDIT_APPLICATION_TABLE,
      servicePath: `CreditAppTable/${this.productTypeWording}/RelatedInfo/Loanrequest`
    };
  }
  setOptionBuyermatching(): void {
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'productType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: getProductType(this.masterRoute)
      },
      {
        label: null,
        textKey: 'creditAppRequestType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: CreditAppRequestType.BuyerMatching.toString()
      },
      {
        label: null,
        textKey: 'refCreditAppTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
  }
  setPathBuyermatching(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.FACTORING_CREDIT_APPLICATION_TABLE,
      servicePath: `CreditAppTable/${this.productTypeWording}/RelatedInfo/Buyermatching`
    };
  }
}
