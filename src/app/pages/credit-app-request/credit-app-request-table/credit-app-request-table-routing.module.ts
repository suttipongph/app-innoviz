import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CreditAppRequestLineItemPage } from './credit-app-request-line-item/credit-app-request-line-item.page';
import { CreditAppRequestTableItemPage } from './credit-app-request-table-item/credit-app-request-table-item.page';
import { CreditAppRequestTableListPage } from './credit-app-request-table-list/credit-app-request-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: CreditAppRequestTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CreditAppRequestTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/creditapprequestline-child/:id',
    component: CreditAppRequestLineItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/credit-app-request-table-relatedinfo.module').then((m) => m.CreditAppRequestTableRelatedinfoModule)
  },
  {
    path: ':id/creditapprequestline-child/:id/relatedinfo',
    loadChildren: () => import('./relatedinfo-child/credit-app-request-line-relatedinfo.module').then((m) => m.CreditAppRequestLineRelatedinfoModule)
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/credit-app-request-table-function.module').then((m) => m.CreditAppRequestTableFunctionModule)
  },
  {
    path: ':id/workflow',
    loadChildren: () =>
      import('./workflow/credit-app-request-action-workflow/credit-app-request-action-workflow.module').then(
        (m) => m.CreditAppRequestActionWorkflowModule
      )
  },
  {
    path: ':id/workflow/actionhistory',
    loadChildren: () =>
      import('./workflow/credit-app-request-action-history/credit-app-request-action-history.module').then(
        (m) => m.CreditAppRequestActionHistoryModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppRequestTableRoutingModule {}
