import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppRequestTableRoutingModule } from './credit-app-request-table-routing.module';
import { CreditAppRequestTableListPage } from './credit-app-request-table-list/credit-app-request-table-list.page';
import { CreditAppRequestTableItemPage } from './credit-app-request-table-item/credit-app-request-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { CreditAppRequestTableComponentModule } from 'components/credit-app-request/credit-app-request-table/credit-app-request-table.module';
import { CreditAppRequestLineComponentModule } from 'components/credit-app-request/credit-app-request-line/credit-app-request-line.module';
import { CreditAppRequestLineItemPage } from './credit-app-request-line-item/credit-app-request-line-item.page';

@NgModule({
  declarations: [CreditAppRequestTableListPage, CreditAppRequestTableItemPage, CreditAppRequestLineItemPage],
  imports: [
    CommonModule,
    SharedModule,
    CreditAppRequestTableRoutingModule,
    CreditAppRequestTableComponentModule,
    CreditAppRequestLineComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppRequestTableModule {}
