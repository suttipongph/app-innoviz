import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CopyBuyerAgreementTransPage } from './copy-buyer-agreement-trans/copy-buyer-agreement-trans.page';

const routes: Routes = [
  {
    path: '',
    component: CopyBuyerAgreementTransPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppReqTablebuyerAGMRelatedinfoCopyBuyerAGMFunctionRoutingModule {}
