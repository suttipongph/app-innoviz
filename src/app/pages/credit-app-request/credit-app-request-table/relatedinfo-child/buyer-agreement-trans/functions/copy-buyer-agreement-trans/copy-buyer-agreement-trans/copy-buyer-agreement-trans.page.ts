import { Component, OnInit } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { UIControllerService } from 'core/services/uiController.service';
import { getProductType } from 'shared/functions/value.function';
const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';

@Component({
  selector: 'copy-buyer-agreement-trans-page',
  templateUrl: './copy-buyer-agreement-trans.page.html',
  styleUrls: ['./copy-buyer-agreement-trans.page.scss']
})
export class CopyBuyerAgreementTransPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  activeName: string = this.uiService.getRelatedInfoActiveTableName();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    switch (this.activeName) {
      case LOAN_REQUEST:
        this.setPathLoanRequest();
        break;
      case BUYER_MATCHING:
        this.setPathBuyermatching();
        break;
      default:
        this.setPathMainCreditAppRequestTable();
        break;
    }
  }

  setPathMainCreditAppRequestTable(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.COPY_BUYER_AGREEMENT_TRANS,
      servicePath: `CreditAppRequestTable/${getProductType(
        this.masterRoute,
        true
      )}/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans`
    };
  }
  setPathBuyermatching(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.COPY_BUYER_AGREEMENT_TRANS,
      servicePath: `CreditAppTable/${getProductType(
        this.masterRoute,
        true
      )}/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans`
    };
  }

  setPathLoanRequest(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.COPY_BUYER_AGREEMENT_TRANS,
      servicePath: `CreditAppTable/${getProductType(
        this.masterRoute,
        true
      )}/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans`
    };
  }
}
