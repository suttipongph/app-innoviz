import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'copybuyeragreementtrans',
    loadChildren: () =>
      import('./copy-buyer-agreement-trans/copy-buyer-agreement-trans.module').then(
        (m) => m.CreditAppReqTablebuyerAGMRelatedinfoCopyBuyerAGMFunctionModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppReqTableBuyerAGMRelatedinfoFunctionRoutingModule {}
