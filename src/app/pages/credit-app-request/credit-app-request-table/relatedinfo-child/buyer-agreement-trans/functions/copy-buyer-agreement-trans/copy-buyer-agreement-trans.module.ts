import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditAppReqTablebuyerAGMRelatedinfoCopyBuyerAGMFunctionRoutingModule } from './copy-buyer-agreement-trans-routing.module';
import { CopyBuyerAgreementTransPage } from './copy-buyer-agreement-trans/copy-buyer-agreement-trans.page';
import { SharedModule } from 'shared/shared.module';
import { CopyBuyerAgreementTransComponentModule } from 'components/buyer-agreement-trans/copy-buyer-agreement-trans/copy-buyer-agreement-trans.module';

@NgModule({
  declarations: [CopyBuyerAgreementTransPage],
  imports: [
    CommonModule,
    SharedModule,
    CreditAppReqTablebuyerAGMRelatedinfoCopyBuyerAGMFunctionRoutingModule,
    CopyBuyerAgreementTransComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditAppReqTablebuyerAGMRelatedinfoCopyBuyerAGMFunctionModule {}
