import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CopyBuyerAgreementTransPage } from './copy-buyer-agreement-trans.page';

describe('CopyBuyerAgreementTransPage', () => {
  let component: CopyBuyerAgreementTransPage;
  let fixture: ComponentFixture<CopyBuyerAgreementTransPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CopyBuyerAgreementTransPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyBuyerAgreementTransPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
