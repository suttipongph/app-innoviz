import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MemoListPage } from './memo-list.page';

describe('MemoListPage', () => {
  let component: MemoListPage;
  let fixture: ComponentFixture<MemoListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MemoListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MemoListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
