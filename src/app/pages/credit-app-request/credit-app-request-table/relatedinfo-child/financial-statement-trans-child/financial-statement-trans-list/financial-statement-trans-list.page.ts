import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';

@Component({
  selector: 'financial-statement-trans-list-page',
  templateUrl: './financial-statement-trans-list.page.html',
  styleUrls: ['./financial-statement-trans-list.page.scss']
})
export class FinancialStatementTransListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  activeName: string = this.uiService.getRelatedInfoActiveTableName();

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    switch (this.activeName) {
      case LOAN_REQUEST:
        this.setPathLoanRequest();
        break;
      case BUYER_MATCHING:
        this.setPathBuyermatching();
        break;
      default:
        this.setPathMainCreditAppRequestTable();
        break;
    }
  }
  setPathMainCreditAppRequestTable(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.FINANCIAL_STATEMENT_TRANS,
      servicePath: `CreditAppRequestTable/${getProductType(this.masterRoute, true)}/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans`
    };
  }
  setPathBuyermatching(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.FINANCIAL_STATEMENT_TRANS,
      servicePath: `CreditAppTable/${getProductType(
        this.masterRoute,
        true
      )}/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans`
    };
  }
  setPathLoanRequest(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.FINANCIAL_STATEMENT_TRANS,
      servicePath: `CreditAppTable/${getProductType(
        this.masterRoute,
        true
      )}/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans`
    };
  }
}
