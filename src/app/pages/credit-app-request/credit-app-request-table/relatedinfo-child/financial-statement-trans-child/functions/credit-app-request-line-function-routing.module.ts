import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'copyfinancialstatementtrans',
    loadChildren: () =>
      import('./copy-financial-statement-trans/copy-financial-statement-trans-function.module').then(
        (m) => m.CreditAppReqLineFinancialRelatedinfoCopyFinancialFunctionModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppReqLineFinancialRelatedinfoFunctionRoutingModule {}
