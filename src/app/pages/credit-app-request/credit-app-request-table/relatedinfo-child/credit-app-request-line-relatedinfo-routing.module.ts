import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'financialstatementtrans',
    loadChildren: () =>
      import('./financial-statement-trans-child/credit-app-request-line-financial-statement-trans.module').then(
        (m) => m.CreditAppRequestLineFinancialStatementTransModule
      )
  },
  {
    path: 'memotrans',
    loadChildren: () =>
      import('./memo/credit-app-reques-line-memo-trans-relatedinfo.module').then((m) => m.CreditAppRequestLineMemoTransRelatedinfoModule)
  },
  {
    path: 'servicefeeconditiontrans',
    loadChildren: () =>
      import('./service-fee-condition/credit-app-request-line-service-fee-condition-relatedinfo.module').then(
        (m) => m.CreditAppRequestLineServiceFeeConditionRelatedinfoModule
      )
  },
  {
    path: 'documentconditiontrans',
    loadChildren: () =>
      import('./document-condition-info/credit-app-request-line-document-condition-info.module').then(
        (m) => m.CreditAppRequestLineDocumentConditionInfoModule
      )
  },
  {
    path: 'buyeragreementtrans',
    loadChildren: () =>
      import('./buyer-agreement-trans/credit-app-request-line-buyer-agreement-trans-relatedinfo.module').then(
        (m) => m.CreditAppRequestLineBuyerAgreementTransRelatedinfoModule
      )
  },
  {
    path: 'attachment',
    loadChildren: () =>
      import('./attachment/credit-app-request-line-attachment-relatedinfo.module').then((m) => m.CreditAppRequestLineAttachmentRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditAppRequestLineRelatedinfoRoutingModule {}
