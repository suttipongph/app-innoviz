import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';

@Component({
  selector: 'document-condition-info-item.page',
  templateUrl: './document-condition-info-item.page.html',
  styleUrls: ['./document-condition-info-item.page.scss']
})
export class DocumentConditionInfoItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  activeName: string = this.uiService.getRelatedInfoActiveTableName();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    switch (this.activeName) {
      case LOAN_REQUEST:
        this.setPathLoanRequest();
        break;
      case BUYER_MATCHING:
        this.setPathBuyermatching();
        break;
      default:
        this.setPathMainCreditAppRequestTable();
        break;
    }
  }

  setPathMainCreditAppRequestTable(): void {
    const productTypeWording = getProductType(this.masterRoute, true);
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.BUYERTABLE_DOCUMENT_CONDITION_INFO,
      servicePath: `CreditAppRequestTable/${productTypeWording}/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.BILLING_DOCUMENT_CONDITION_TRANS_BILLING,
          servicePath: `CreditAppRequestTable/${productTypeWording}/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans`
        },
        {
          pagePath: ROUTE_RELATED_GEN.RECEIPT_DOCUMENT_CONDITION_TRANS_RECEIPT,
          servicePath: `CreditAppRequestTable/${productTypeWording}/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans`
        }
      ]
    };
  }
  setPathBuyermatching(): void {
    const productTypeWording = getProductType(this.masterRoute, true);
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.BUYERTABLE_DOCUMENT_CONDITION_INFO,
      servicePath: `CreditAppTable/${productTypeWording}/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.BILLING_DOCUMENT_CONDITION_TRANS_BILLING,
          servicePath: `CreditAppTable/${productTypeWording}/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans`
        },
        {
          pagePath: ROUTE_RELATED_GEN.RECEIPT_DOCUMENT_CONDITION_TRANS_RECEIPT,
          servicePath: `CreditAppTable/${productTypeWording}/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans`
        }
      ]
    };
  }
  setPathLoanRequest(): void {
    const productTypeWording = getProductType(this.masterRoute, true);
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.BUYERTABLE_DOCUMENT_CONDITION_INFO,
      servicePath: `CreditAppTable/${productTypeWording}/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.BILLING_DOCUMENT_CONDITION_TRANS_BILLING,
          servicePath: `CreditAppTable/${productTypeWording}/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans`
        },
        {
          pagePath: ROUTE_RELATED_GEN.RECEIPT_DOCUMENT_CONDITION_TRANS_RECEIPT,
          servicePath: `CreditAppTable/${productTypeWording}/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans`
        }
      ]
    };
  }
}
