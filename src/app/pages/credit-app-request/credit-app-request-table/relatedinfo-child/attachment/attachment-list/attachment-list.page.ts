import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';

@Component({
  selector: 'attachment-list-page',
  templateUrl: './attachment-list.page.html',
  styleUrls: ['./attachment-list.page.scss']
})
export class AttachmentListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;

  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  activeName: string = this.uiService.getRelatedInfoActiveTableName();
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'attachmentGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    switch (this.activeName) {
      case LOAN_REQUEST:
        this.setPathLoanRequest();
        break;
      case BUYER_MATCHING:
        this.setPathBuyermatching();
        break;
      default:
        this.setPathMainCreditAppRequestTable();
        break;
    }
  }
  setPathMainCreditAppRequestTable(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.ATTACHMENT,
      servicePath: `CreditAppRequestTable/${getProductType(this.masterRoute, true)}/CreditAppRequestLine-Child/RelatedInfo/Attachment`
    };
  }
  setPathLoanRequest(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.ATTACHMENT,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/Attachment`
    };
  }
  setPathBuyermatching(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.ATTACHMENT,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/Attachment`
    };
  }
}
