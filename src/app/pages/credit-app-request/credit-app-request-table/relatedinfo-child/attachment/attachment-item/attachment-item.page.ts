import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';

@Component({
  selector: 'attachment-item.page',
  templateUrl: './attachment-item.page.html',
  styleUrls: ['./attachment-item.page.scss']
})
export class AttachmentItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}
  activeName: string = this.uiService.getRelatedInfoActiveTableName();
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    switch (this.activeName) {
      case LOAN_REQUEST:
        this.setPathLoanRequest();
        break;
      case BUYER_MATCHING:
        this.setPathBuyermatching();
        break;
      default:
        this.setPathMainCreditAppRequestTable();
        break;
    }
  }
  setPathMainCreditAppRequestTable(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.ATTACHMENT,
      servicePath: `CreditAppRequestTable/${getProductType(this.masterRoute, true)}/CreditAppRequestLine-Child/RelatedInfo/Attachment`
    };
  }
  setPathLoanRequest(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.ATTACHMENT,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/Attachment`
    };
  }
  setPathBuyermatching(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.CREDIT_APP_REQUEST_TABLE_ATTACHMENT,
      servicePath: `CreditAppTable/${getProductType(this.masterRoute, true)}/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/Attachment`
    };
  }
}
