import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditAppRequestLineItemPage } from './credit-app-request-line-item.page';

describe('CreditAppRequestTableItemPage', () => {
  let component: CreditAppRequestLineItemPage;
  let fixture: ComponentFixture<CreditAppRequestLineItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreditAppRequestLineItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppRequestLineItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
