import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { BusinessCollateralAgmLineItemPage } from './business-collateral-agm-line-item/business-collateral-agm-line-item.page';
import { BusinessCollateralAgmTableItemPage } from './business-collateral-agm-table-item/business-collateral-agm-table-item.page';
import { BusinessCollateralAgmTableListPage } from './business-collateral-agm-table-list/business-collateral-agm-table-list.page';

const routes: Routes = [
  {
    path: '',
    component: BusinessCollateralAgmTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BusinessCollateralAgmTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/businesscollateralagmline-child/:id',
    component: BusinessCollateralAgmLineItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () =>
      import('./relatedinfo/business-collateral-agm-table-relatedinfo.module').then((m) => m.BusinessCollateralAgmTableRelatedinfoModule)
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/business-collateral-agm-table-function.module').then((m) => m.BusinessCollateralAgmTableFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessCollateralAgmTableBusinessCollateralAgmTableRoutingModule {}
