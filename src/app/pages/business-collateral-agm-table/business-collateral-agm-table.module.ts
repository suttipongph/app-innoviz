import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessCollateralAgmTableBusinessCollateralAgmTableRoutingModule } from './business-collateral-agm-table-routing.module';
import { BusinessCollateralAgmTableListPage } from './business-collateral-agm-table-list/business-collateral-agm-table-list.page';
import { BusinessCollateralAgmTableItemPage } from './business-collateral-agm-table-item/business-collateral-agm-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { BusinessCollateralAgmTableComponentModule } from 'components/business-collateral-agm/business-collateral-agm-table/business-collateral-agm-table.module';
import { BusinessCollateralAgmLineItemPage } from './business-collateral-agm-line-item/business-collateral-agm-line-item.page';
import { BusinessCollateralAgmLineComponentModule } from 'components/business-collateral-agm/business-collateral-agm-line/business-collateral-agm-line.module';

@NgModule({
  declarations: [BusinessCollateralAgmTableListPage, BusinessCollateralAgmTableItemPage, BusinessCollateralAgmLineItemPage],
  imports: [
    CommonModule,
    SharedModule,
    BusinessCollateralAgmTableBusinessCollateralAgmTableRoutingModule,
    BusinessCollateralAgmTableComponentModule,
    BusinessCollateralAgmLineComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusinessCollateralAgmTableModule {}
