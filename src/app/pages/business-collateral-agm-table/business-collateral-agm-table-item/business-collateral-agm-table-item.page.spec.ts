import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessCollateralAgmTableItemPage } from './business-collateral-agm-table-item.page';

describe('BusinessCollateralAgmTableItemPage', () => {
  let component: BusinessCollateralAgmTableItemPage;
  let fixture: ComponentFixture<BusinessCollateralAgmTableItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BusinessCollateralAgmTableItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCollateralAgmTableItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
