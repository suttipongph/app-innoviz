import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'business-collateral-agm-line-item-page',
  templateUrl: './business-collateral-agm-line-item.page.html',
  styleUrls: ['./business-collateral-agm-line-item.page.scss']
})
export class BusinessCollateralAgmLineItemPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }

  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.BUSINESS_COLLATERAL_AGM_LINE,
      servicePath: `${ROUTE_MASTER_GEN.BUSINESS_COLLATERAL_AGM_TABLE}/${getProductType(this.masterRoute, true)}/${this.currentRoute}`
    };
  }
}
