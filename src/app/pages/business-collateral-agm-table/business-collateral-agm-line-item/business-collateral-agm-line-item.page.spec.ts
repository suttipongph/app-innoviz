import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessCollateralAgmLineItemPage } from './business-collateral-agm-line-item.page';

describe('BusinessCollateralAgmLineItemPage', () => {
  let component: BusinessCollateralAgmLineItemPage;
  let fixture: ComponentFixture<BusinessCollateralAgmLineItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BusinessCollateralAgmLineItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCollateralAgmLineItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
