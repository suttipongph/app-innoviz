import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GenBusCollateralAgmAddendumPage } from './gen-bus-collateral-agm-addendum.page';

describe('GenBusCollateralAgmAddendum', () => {
  let component: GenBusCollateralAgmAddendumPage;
  let fixture: ComponentFixture<GenBusCollateralAgmAddendumPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GenBusCollateralAgmAddendumPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenBusCollateralAgmAddendumPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
