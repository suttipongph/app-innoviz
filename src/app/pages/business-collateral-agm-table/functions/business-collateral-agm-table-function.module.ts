import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessCollateralAgmTableFunctionRoutingModule } from './business-collateral-agm-table-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, BusinessCollateralAgmTableFunctionRoutingModule]
})
export class BusinessCollateralAgmTableFunctionModule {}
