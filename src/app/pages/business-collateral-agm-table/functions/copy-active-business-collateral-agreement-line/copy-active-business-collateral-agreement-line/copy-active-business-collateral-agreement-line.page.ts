import { Component } from '@angular/core';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { ROUTE_FUNCTION_GEN, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'copy-active-business-collateral-agreement-line',
  templateUrl: './copy-active-business-collateral-agreement-line.page.html',
  styleUrls: ['./copy-active-business-collateral-agreement-line.page.scss']
})
export class CopyActiveBusinessCollateralAgreementLinePage extends BaseItemComponent<any> {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor() {
    super();
    this.path = ROUTE_FUNCTION_GEN.COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE;
  }

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.UPDATE_EXPECTED_SIGNING_DATE,
      servicePath: `${ROUTE_MASTER_GEN.BUSINESS_COLLATERAL_AGM_TABLE}/${getProductType(this.masterRoute, true)}/${this.currentRoute}/RelatedInfo/${
        ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE
      }/Function/${ROUTE_FUNCTION_GEN.COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE}`
    };
  }
}
