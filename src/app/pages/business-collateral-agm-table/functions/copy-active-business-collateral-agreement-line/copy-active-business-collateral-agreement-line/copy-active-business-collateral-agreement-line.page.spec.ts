import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CopyActiveBusinessCollateralAgreementLinePage } from './copy-active-business-collateral-agreement-line.page';

describe('CopyBusinessCollateralFromCaRequestPage', () => {
  let component: CopyActiveBusinessCollateralAgreementLinePage;
  let fixture: ComponentFixture<CopyActiveBusinessCollateralAgreementLinePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CopyActiveBusinessCollateralAgreementLinePage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyActiveBusinessCollateralAgreementLinePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
