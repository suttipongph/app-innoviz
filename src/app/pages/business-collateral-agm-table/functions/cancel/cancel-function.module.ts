import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessCollateralAgmCancelFunctionRoutingModule } from './cancel-function-routing.module';
import { CancelAgreementPage } from './cancel/cancel.page';
import { SharedModule } from 'shared/shared.module';
import { ManageAgreementComponentModule } from 'components/function/manage-agreement/manage-agreement.module';

@NgModule({
  declarations: [CancelAgreementPage],
  imports: [CommonModule, SharedModule, BusinessCollateralAgmCancelFunctionRoutingModule, ManageAgreementComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusinessCollateralAgmCancelFunctionModule {}
