import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ConsortuiumItemPage } from './consortuium-item.page';

describe('ConsortuiumItemPage', () => {
  let component: ConsortuiumItemPage;
  let fixture: ComponentFixture<ConsortuiumItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConsortuiumItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsortuiumItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
