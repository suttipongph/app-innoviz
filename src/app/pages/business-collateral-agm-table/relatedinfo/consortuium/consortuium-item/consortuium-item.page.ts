import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'consortuium-item.page',
  templateUrl: './consortuium-item.page.html',
  styleUrls: ['./consortuium-item.page.scss']
})
export class ConsortuiumItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.BUSINESS_COLLATERAL_AGM_TABLE, servicePath: 'BusinessCollateralAgmTable/RelatedInfo/Consortuium' };
  }
}
