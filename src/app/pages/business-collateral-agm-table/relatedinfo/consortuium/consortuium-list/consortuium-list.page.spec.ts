import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ConsortuiumListPage } from './consortuium-list.page';

describe('ConsortuiumListPage', () => {
  let component: ConsortuiumListPage;
  let fixture: ComponentFixture<ConsortuiumListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConsortuiumListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsortuiumListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
