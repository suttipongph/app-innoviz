import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsortuiumRelatedinfoRoutingModule } from './consortuium-relatedinfo-routing.module';
import { ConsortuiumListPage } from './consortuium-list/consortuium-list.page';
import { ConsortuiumItemPage } from './consortuium-item/consortuium-item.page';
import { SharedModule } from 'shared/shared.module';
import { ConsortiumTransComponentModule } from 'components/consortium-trans/consortium-trans.module';

@NgModule({
  declarations: [ConsortuiumListPage, ConsortuiumItemPage],
  imports: [CommonModule, SharedModule, ConsortuiumRelatedinfoRoutingModule, ConsortiumTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ConsortuiumRelatedinfoModule {}
