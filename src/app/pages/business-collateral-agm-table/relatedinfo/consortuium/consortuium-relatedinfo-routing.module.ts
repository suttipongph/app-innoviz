import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConsortuiumListPage } from './consortuium-list/consortuium-list.page';
import { ConsortuiumItemPage } from './consortuium-item/consortuium-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: ConsortuiumListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ConsortuiumItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsortuiumRelatedinfoRoutingModule {}
