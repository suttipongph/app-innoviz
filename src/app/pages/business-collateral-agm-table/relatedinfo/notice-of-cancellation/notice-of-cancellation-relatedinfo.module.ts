import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessCollateralAgmNoticeOfCancellationRelatedinfoRoutingModule } from './notice-of-cancellation-relatedinfo-routing.module';
import { NoticeOfCancellationListPage } from './notice-of-cancellation-list/notice-of-cancellation-list.page';
import { NoticeOfCancellationItemPage } from './notice-of-cancellation-item/notice-of-cancellation-item.page';
import { SharedModule } from 'shared/shared.module';
import { BusinessCollateralAgmTableComponentModule } from 'components/business-collateral-agm/business-collateral-agm-table/business-collateral-agm-table.module';
import { NoticeOfCancellationLineItemPage } from './notice-of-cancellation-line-item/notice-of-cancellation-line-item.page';
import { BusinessCollateralAgmLineComponentModule } from 'components/business-collateral-agm/business-collateral-agm-line/business-collateral-agm-line.module';

@NgModule({
  declarations: [NoticeOfCancellationListPage, NoticeOfCancellationItemPage, NoticeOfCancellationLineItemPage],
  imports: [
    CommonModule,
    SharedModule,
    BusinessCollateralAgmNoticeOfCancellationRelatedinfoRoutingModule,
    BusinessCollateralAgmTableComponentModule,
    BusinessCollateralAgmLineComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusinessCollateralAgmNoticeOfCancellationRelatedinfoModule {}
