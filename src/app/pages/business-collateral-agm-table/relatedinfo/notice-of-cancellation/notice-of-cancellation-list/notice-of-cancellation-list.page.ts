import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { AgreementDocType, ColumnType, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'notice-of-cancellation-list-page',
  templateUrl: './notice-of-cancellation-list.page.html',
  styleUrls: ['./notice-of-cancellation-list.page.scss']
})
export class NoticeOfCancellationListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  parentId = this.uiService.getRelatedInfoParentTableKey();
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'businessCollateralAgmTableGUID';
    this.option.canCreate = false;
    this.option.canDelete = false;
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'agreementDocType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: AgreementDocType.NoticeOfCancellation.toString()
      },
      {
        label: null,
        textKey: 'refBusinessCollateralAgmTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: `/${getProductType(this.masterRoute, true)}/${this.currentRoute}/${ROUTE_MASTER_GEN.BUSINESS_COLLATERAL_AGM_TABLE}/RelatedInfo/${
        ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE
      }`,
      servicePath: `${ROUTE_MASTER_GEN.BUSINESS_COLLATERAL_AGM_TABLE}/${getProductType(this.masterRoute, true)}/${this.currentRoute}/RelatedInfo/${
        ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE
      }`
    };
  }
}
