import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { MenuItem } from 'primeng/api';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'notice-of-cancellation-item.page',
  templateUrl: './notice-of-cancellation-item.page.html',
  styleUrls: ['./notice-of-cancellation-item.page.scss']
})
export class NoticeOfCancellationItemPage implements OnInit {
  pageInfo: PageInformationModel;
  relatedInfoItems: MenuItem[];
  pageLabel = 'notice';
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: `/${getProductType(this.masterRoute, true)}/${this.currentRoute}/${ROUTE_MASTER_GEN.BUSINESS_COLLATERAL_AGM_TABLE}/RelatedInfo/${
        ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE
      }`,
      servicePath: `${ROUTE_MASTER_GEN.BUSINESS_COLLATERAL_AGM_TABLE}/${getProductType(this.masterRoute, true)}/${this.currentRoute}/RelatedInfo/${
        ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE
      }`,
      childPaths: [
        {
          pagePath: ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_LINE,
          servicePath: `${ROUTE_MASTER_GEN.BUSINESS_COLLATERAL_AGM_TABLE}/${getProductType(this.masterRoute, true)}/${
            this.currentRoute
          }/RelatedInfo/${ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE}`
        }
      ]
    };
  }
}
