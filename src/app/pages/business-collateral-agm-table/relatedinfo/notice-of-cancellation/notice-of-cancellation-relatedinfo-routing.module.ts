import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NoticeOfCancellationListPage } from './notice-of-cancellation-list/notice-of-cancellation-list.page';
import { NoticeOfCancellationItemPage } from './notice-of-cancellation-item/notice-of-cancellation-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { NoticeOfCancellationLineItemPage } from './notice-of-cancellation-line-item/notice-of-cancellation-line-item.page';

const routes: Routes = [
  {
    path: '',
    component: NoticeOfCancellationListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: NoticeOfCancellationItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/noticeofcancellationbusinesscollateralagmline-child/:id',
    component: NoticeOfCancellationLineItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'function',
    loadChildren: () => import('./functions/notice-of-cancellation-function.module').then((m) => m.NoticeOfCancellationFunctionModule)
  },
  {
    path: ':id/function',
    loadChildren: () => import('./functions/notice-of-cancellation-function.module').then((m) => m.NoticeOfCancellationFunctionModule)
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () =>
      import('./relatedinfo/business-collateral-agm-table-notice-of-cancellation-relatedinfo.module').then(
        (m) => m.BusinessCollateralAgmTableNoticeOfCancellationRelatedinfoModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessCollateralAgmNoticeOfCancellationRelatedinfoRoutingModule {}
