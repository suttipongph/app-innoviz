import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { NotiOfCancelBusCollateralAgmPage } from './noti-of-cancel-bus-collateral-agm/noti-of-cancel-bus-collateral-agm.page';

const routes: Routes = [
  {
    path: '',
    component: NotiOfCancelBusCollateralAgmPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotiOfCancelBusCollateralAgmRoutingModule {}
