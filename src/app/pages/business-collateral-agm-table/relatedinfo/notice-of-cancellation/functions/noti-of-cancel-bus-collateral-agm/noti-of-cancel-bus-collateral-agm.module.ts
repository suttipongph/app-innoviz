import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotiOfCancelBusCollateralAgmPage } from './noti-of-cancel-bus-collateral-agm/noti-of-cancel-bus-collateral-agm.page';
import { NotiOfCancelBusCollateralAgmRoutingModule } from './noti-of-cancel-bus-collateral-agm-routing.module';
import { SharedModule } from 'shared/shared.module';
import { CancelBusinessCollateralAgmComponentModule } from 'components/business-collateral-agm/cancel-business-collateral-agm/cancel-business-collateral-agm.module';

@NgModule({
  declarations: [NotiOfCancelBusCollateralAgmPage],
  imports: [CommonModule, SharedModule, NotiOfCancelBusCollateralAgmRoutingModule, CancelBusinessCollateralAgmComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NotiOfCancelBusCollateralAgmModule {}
