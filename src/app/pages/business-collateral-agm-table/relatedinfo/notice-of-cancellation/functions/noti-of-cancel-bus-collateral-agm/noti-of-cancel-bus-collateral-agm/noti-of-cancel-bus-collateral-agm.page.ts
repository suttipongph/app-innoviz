import { Component } from '@angular/core';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { ROUTE_FUNCTION_GEN, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'noti-of-cancel-bus-collateral-agm',
  templateUrl: './noti-of-cancel-bus-collateral-agm.page.html',
  styleUrls: ['./noti-of-cancel-bus-collateral-agm.page.scss']
})
export class NotiOfCancelBusCollateralAgmPage extends BaseItemComponent<any> {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor() {
    super();
    this.path = ROUTE_FUNCTION_GEN.GENERATE_NOTICE_OF_CANCELLATION;
  }

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.UPDATE_EXPECTED_SIGNING_DATE,
      servicePath: `${ROUTE_MASTER_GEN.BUSINESS_COLLATERAL_AGM_TABLE}/${getProductType(this.masterRoute, true)}/${this.currentRoute}/RelatedInfo/${
        ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE
      }/Function/${ROUTE_FUNCTION_GEN.NOTICE_OF_CANCELLATION}`
    };
  }
}
