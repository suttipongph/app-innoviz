import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NotiOfCancelBusCollateralAgmPage } from './noti-of-cancel-bus-collateral-agm.page';

describe('CopyBusinessCollateralFromCaRequestPage', () => {
  let component: NotiOfCancelBusCollateralAgmPage;
  let fixture: ComponentFixture<NotiOfCancelBusCollateralAgmPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NotiOfCancelBusCollateralAgmPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotiOfCancelBusCollateralAgmPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
