import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'primeng/api';
import { NoticeOfCancellationFunctionRoutingModule } from './notice-of-cancellation-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, NoticeOfCancellationFunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NoticeOfCancellationFunctionModule {}
