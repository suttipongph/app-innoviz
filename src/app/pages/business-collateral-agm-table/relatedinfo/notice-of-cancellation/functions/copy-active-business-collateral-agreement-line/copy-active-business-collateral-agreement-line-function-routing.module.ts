import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CopyActiveBusinessCollateralAgreementLinePage } from './copy-active-business-collateral-agreement-line/copy-active-business-collateral-agreement-line.page';

const routes: Routes = [
  {
    path: '',
    component: CopyActiveBusinessCollateralAgreementLinePage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CopyActiveBusinessCollateralAgreementLineFunctionRoutingModule {}
