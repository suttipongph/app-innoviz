import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CopyActiveBusinessCollateralAgreementLinePage } from './copy-active-business-collateral-agreement-line/copy-active-business-collateral-agreement-line.page';
import { CopyActiveBusinessCollateralAgreementLineFunctionRoutingModule } from './copy-active-business-collateral-agreement-line-function-routing.module';
import { SharedModule } from 'shared/shared.module';
import { CopyBusinessCollateralAgmLineComponentModule } from 'components/business-collateral-agm/copy-business-collateral-agm-line/copy-business-collateral-agm-line.module';

@NgModule({
  declarations: [CopyActiveBusinessCollateralAgreementLinePage],
  imports: [CommonModule, SharedModule, CopyActiveBusinessCollateralAgreementLineFunctionRoutingModule, CopyBusinessCollateralAgmLineComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CopyActiveBusinessCollateralAgreementLineFunctionModule {}
