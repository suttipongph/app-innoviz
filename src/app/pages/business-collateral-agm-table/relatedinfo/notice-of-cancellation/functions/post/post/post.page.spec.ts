import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PostAgreementPage } from './post.page';

describe('PostPage', () => {
  let component: PostAgreementPage;
  let fixture: ComponentFixture<PostAgreementPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PostAgreementPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostAgreementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
