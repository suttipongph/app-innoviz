import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CancelAgreementPage } from './cancel.page';

describe('CancelPage', () => {
  let component: CancelAgreementPage;
  let fixture: ComponentFixture<CancelAgreementPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CancelAgreementPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelAgreementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
