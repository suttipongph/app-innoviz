import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'invoicetable',
    loadChildren: () =>
      import('./inv/service-fee-trans-invoicetable-relatedinfo.module').then(
        (m) => m.BusinessCollateralAgmTableNoticeOfCancellationServiceFeeTransInvoiceTableRelatedinfoModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessCollateralAgmTableNoticeOfCancellationServiceFeeTransRelatedinfoRelatedInfoRoutingModule {}
