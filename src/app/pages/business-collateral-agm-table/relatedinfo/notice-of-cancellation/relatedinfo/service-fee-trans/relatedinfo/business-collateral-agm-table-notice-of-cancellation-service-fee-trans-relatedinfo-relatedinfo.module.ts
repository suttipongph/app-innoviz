import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessCollateralAgmTableNoticeOfCancellationServiceFeeTransRelatedinfoRelatedInfoRoutingModule } from './business-collateral-agm-table-notice-of-cancellation-service-fee-trans-relatedinfo-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, BusinessCollateralAgmTableNoticeOfCancellationServiceFeeTransRelatedinfoRelatedInfoRoutingModule]
})
export class BusinessCollateralAgmTableNoticeOfCancellationServiceFeeTransRelatedinfoRelatedInfoModule {}
