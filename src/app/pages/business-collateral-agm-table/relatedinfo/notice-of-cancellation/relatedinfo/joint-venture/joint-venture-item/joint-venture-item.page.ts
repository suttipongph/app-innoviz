import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'joint-venture-item.page',
  templateUrl: './joint-venture-item.page.html',
  styleUrls: ['./joint-venture-item.page.scss']
})
export class JointVentureItemPage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.JOINT_VENTURE_TRANS,
      servicePath: `BusinessCollateralAgmTable/${getProductType(this.masterRoute, true)}/${this.currentRoute}/RelatedInfo/${
        ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE
      }/RelatedInfo/${ROUTE_RELATED_GEN.JOINT_VENTURE_TRANS}`
    };
  }
}
