import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BookmarkDocumentItemPage } from './bookmark-document-item.page';

describe('BookmarkDocumentItemPage', () => {
  let component: BookmarkDocumentItemPage;
  let fixture: ComponentFixture<BookmarkDocumentItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BookmarkDocumentItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkDocumentItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
