import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BusinessCollateralAgmTableNoticeOfCancellationRelatedinfoRoutingModule } from './business-collateral-agm-table-notice-of-cancellation-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, BusinessCollateralAgmTableNoticeOfCancellationRelatedinfoRoutingModule]
})
export class BusinessCollateralAgmTableNoticeOfCancellationRelatedinfoModule {}
