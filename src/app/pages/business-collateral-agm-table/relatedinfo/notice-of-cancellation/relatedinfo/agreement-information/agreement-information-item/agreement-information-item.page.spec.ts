import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AgreementInformationItemPage } from './agreement-information-item.page';

describe('AgreementInformationItemPage', () => {
  let component: AgreementInformationItemPage;
  let fixture: ComponentFixture<AgreementInformationItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AgreementInformationItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgreementInformationItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
