import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'agreementtableinfo',
    loadChildren: () =>
      import('./agreement-information/business-collateral-agm-table-notice-of-cancellation-agreement-information-relatedinfo.module').then(
        (m) => m.BusinessCollateralAgmTableNoticeOfCancellationAgreementInformationRelatedinfoModule
      )
  },
  {
    path: 'servicefeetrans',
    loadChildren: () =>
      import('./service-fee-trans/business-collateral-agm-table-notice-of-cancellation-service-fee-trans-relatedinfo.module').then(
        (m) => m.BusinessCollateralAgmTableNoticeOfCancellationServiceFeeTransRelatedinfoModule
      )
  },
  {
    path: 'memotrans',
    loadChildren: () =>
      import('./memo/business-collateral-agm-table-notice-of-cancellation-memo-relatedinfo.module').then(
        (m) => m.BusinessCollateralAgmTableNoticeOfCancellationMemoRelatedinfoModule
      )
  },
  {
    path: 'jointventuretrans',
    loadChildren: () =>
      import('./joint-venture/business-collateral-agm-table-notice-of-cancellation-joint-venture-relatedinfo.module').then(
        (m) => m.BusinessCollateralAgmTableNoticeOfCancellationJointVentureRelatedinfoModule
      )
  },
  {
    path: 'consortiumtrans',
    loadChildren: () =>
      import('./consortium/business-collateral-agm-table-notice-of-cancellation-consortium-trans-relatedinfo.module').then(
        (m) => m.BusinessCollateralAgmTableNoticeOfCancellationConsortiumTransRelatedinfoModule
      )
  },
  {
    path: 'bookmarkdocumenttrans',
    loadChildren: () =>
      import('./bookmark-document/business-collateral-agm-table-notice-of-cancellation-bookmark-document-relatedinfo.module').then(
        (m) => m.BusinessCollateralAgmNoticeOfCancellationBookmarkDocumentRelatedinfoModule
      )
  },
  {
    path: 'attachment',
    loadChildren: () =>
      import('./attachment/business-collateral-agm-table-notice-of-cancellation-attachment.module').then(
        (m) => m.BusinessCollateralAgmTableNoticeOfCancellationAttachmentModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessCollateralAgmTableNoticeOfCancellationRelatedinfoRoutingModule {}
