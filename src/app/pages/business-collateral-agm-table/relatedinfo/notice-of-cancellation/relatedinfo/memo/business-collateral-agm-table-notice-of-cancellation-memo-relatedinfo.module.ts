import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessCollateralAgmTableNoticeOfCancellationMemoRelatedinfoRoutingModule } from './business-collateral-agm-table-notice-of-cancellation-memo-relatedinfo-routing.module';
import { MemoListPage } from './memo-list/memo-list.page';
import { MemoItemPage } from './memo-item/memo-item.page';
import { SharedModule } from 'shared/shared.module';
import { MemoTransComponentModule } from 'components/memo-trans/memo-trans/memo-trans.module';

@NgModule({
  declarations: [MemoListPage, MemoItemPage],
  imports: [CommonModule, SharedModule, BusinessCollateralAgmTableNoticeOfCancellationMemoRelatedinfoRoutingModule, MemoTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusinessCollateralAgmTableNoticeOfCancellationMemoRelatedinfoModule {}
