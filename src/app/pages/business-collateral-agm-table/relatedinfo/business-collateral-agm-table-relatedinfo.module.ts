import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BusinessCollateralAgmTableRelatedinfoRoutingModule } from './business-collateral-agm-table-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, BusinessCollateralAgmTableRelatedinfoRoutingModule]
})
export class BusinessCollateralAgmTableRelatedinfoModule {}
