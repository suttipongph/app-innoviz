import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessCollateralAgmTableServiceFeeTransRelatedinfoRelatedInfoRoutingModule } from './business-collateral-agm-table-service-fee-trans-relatedinfo-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, BusinessCollateralAgmTableServiceFeeTransRelatedinfoRelatedInfoRoutingModule]
})
export class BusinessCollateralAgmTableServiceFeeTransRelatedinfoRelatedInfoModule {}
