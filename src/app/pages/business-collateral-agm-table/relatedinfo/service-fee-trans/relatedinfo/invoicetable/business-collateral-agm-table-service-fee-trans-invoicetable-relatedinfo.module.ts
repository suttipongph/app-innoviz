import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { InvoiceTableComponentModule } from 'components/invoice-table/invoice-table/invoice-table.module';
import { InvoiceTableListPage } from './invoice-table-list/invoice-table.page';
import { InvoiceTableItemPage } from './invoice-table-item/invoice-table-item.page';
import { InvoiceLineComponentModule } from 'components/invoice-table/invoice-line/invoice-line.module';
import { InvoiceLineItemPage } from './invoice-line-item/invoice-line-item.page';
import { BusinessCollateralAgmTableServiceFeeTransInvoiceTableRelatedinfoRoutingModule } from './business-collateral-agm-table-service-fee-trans-invoicetable-relatedinfo-routing.module';

@NgModule({
  declarations: [InvoiceTableListPage, InvoiceTableItemPage, InvoiceLineItemPage],
  imports: [
    CommonModule,
    SharedModule,
    BusinessCollateralAgmTableServiceFeeTransInvoiceTableRelatedinfoRoutingModule,
    InvoiceTableComponentModule,
    InvoiceLineComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusinessCollateralAgmTableServiceFeeTransInvoiceTableRelatedinfoModule {}
