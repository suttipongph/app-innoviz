import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PaymentHistoryListPage } from './payment-history-list.page';

describe('PaymentHistoryListPage', () => {
  let component: PaymentHistoryListPage;
  let fixture: ComponentFixture<PaymentHistoryListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PaymentHistoryListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentHistoryListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
