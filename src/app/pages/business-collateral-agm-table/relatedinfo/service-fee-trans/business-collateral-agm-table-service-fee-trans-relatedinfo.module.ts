import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServiceFeeTransListPage } from './service-fee-trans-list/service-fee-trans-list.page';
import { ServiceFeeTransItemPage } from './service-fee-trans-item/service-fee-trans-item.page';
import { SharedModule } from 'shared/shared.module';
import { ServiceFeeTransComponentModule } from 'components/service-fee-trans/service-fee-trans.module';
import { BusinessCollateralAgmTableServiceFeeTransRelatedinfoRoutingModule } from './business-collateral-agm-table-service-fee-trans-relatedinfo-routing.module';

@NgModule({
  declarations: [ServiceFeeTransListPage, ServiceFeeTransItemPage],
  imports: [CommonModule, SharedModule, BusinessCollateralAgmTableServiceFeeTransRelatedinfoRoutingModule, ServiceFeeTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusinessCollateralAgmTableServiceFeeTransRelatedinfoModule {}
