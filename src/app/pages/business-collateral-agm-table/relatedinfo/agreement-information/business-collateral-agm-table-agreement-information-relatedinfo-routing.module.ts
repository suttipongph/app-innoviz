import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgreementInformationListPage } from './agreement-information-list/agreement-information-list.page';
import { AgreementInformationItemPage } from './agreement-information-item/agreement-information-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: AgreementInformationListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: AgreementInformationItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessCollateralAgmTableAgreementInformationRelatedinfoRoutingModule {}
