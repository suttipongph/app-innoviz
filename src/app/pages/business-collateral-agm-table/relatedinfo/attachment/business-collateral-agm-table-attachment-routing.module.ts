import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AttachmentListPage } from './attachment-list/attachment-list.page';
import { AttachmentItemPage } from './attachment-item/attachment-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: AttachmentListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: AttachmentItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessCollateralAgmTableAttachmentRoutingModule {}
