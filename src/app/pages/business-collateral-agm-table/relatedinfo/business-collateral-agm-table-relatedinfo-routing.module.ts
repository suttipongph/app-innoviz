import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'agreementtableinfo',
    loadChildren: () =>
      import('./agreement-information/business-collateral-agm-table-agreement-information-relatedinfo.module').then(
        (m) => m.BusinessCollateralAgmTableAgreementInformationRelatedinfoModule
      )
  },
  {
    path: 'noticeofcancellationbusinesscollateralagmtable',
    loadChildren: () =>
      import('./notice-of-cancellation/notice-of-cancellation-relatedinfo.module').then(
        (m) => m.BusinessCollateralAgmNoticeOfCancellationRelatedinfoModule
      )
  },
  {
    path: 'addendumbusinesscollateralagmtable',
    loadChildren: () =>
      import('./addendum/business-collateral-agm-table-addendum-relatedinfo.module').then((m) => m.BusinessCollateralAgmAddendumRelatedinfoModule)
  },
  {
    path: 'servicefeetrans',
    loadChildren: () =>
      import('./service-fee-trans/business-collateral-agm-table-service-fee-trans-relatedinfo.module').then(
        (m) => m.BusinessCollateralAgmTableServiceFeeTransRelatedinfoModule
      )
  },
  {
    path: 'memotrans',
    loadChildren: () => import('./memo/memo-relatedinfo.module').then((m) => m.MemoRelatedinfoModule)
  },
  {
    path: 'jointventuretrans',
    loadChildren: () => import('./joint-venture/joint-venture-relatedinfo.module').then((m) => m.JointVentureRelatedinfoModule)
  },
  {
    path: 'consortiumtrans',
    loadChildren: () =>
      import('./consortium/business-collateral-agm-table-consortium-trans-relatedinfo.module').then(
        (m) => m.BusinessCollateralAgmTableConsortiumTransRelatedinfoModule
      )
  },
  {
    path: 'bookmarkdocumenttrans',
    loadChildren: () =>
      import('./bookmark-document/business-collateral-agm-table-bookmark-document-relatedinfo.module').then(
        (m) => m.BusinessCollateralAgmBookmarkDocumentRelatedinfoModule
      )
  },
  {
    path: 'attachment',
    loadChildren: () =>
      import('./attachment/business-collateral-agm-table-attachment.module').then((m) => m.BusinessCollateralAgmTableAttachmentModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessCollateralAgmTableRelatedinfoRoutingModule {}
