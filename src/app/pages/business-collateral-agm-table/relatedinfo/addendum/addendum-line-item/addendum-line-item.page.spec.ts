import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AddendumLineItemPage } from './addendum-line-item.page';

describe('AddendumLineItemPage', () => {
  let component: AddendumLineItemPage;
  let fixture: ComponentFixture<AddendumLineItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AddendumLineItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddendumLineItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
