import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { PostAgreementPage } from './post/post.page';

const routes: Routes = [
  {
    path: '',
    component: PostAgreementPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessCollateralAgmPostFunctionRoutingModule {}
