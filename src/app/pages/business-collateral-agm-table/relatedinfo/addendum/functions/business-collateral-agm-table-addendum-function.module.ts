import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessCollateralAgmTableAddendumFunctionRoutingModule } from './business-collateral-agm-table-addendum-function-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, BusinessCollateralAgmTableAddendumFunctionRoutingModule]
})
export class BusinessCollateralAgmTableAddendumFunctionModule {}
