import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { GenBusCollateralAgmAddendumPage } from './gen-bus-collateral-agm-addendum/gen-bus-collateral-agm-addendum.page';

const routes: Routes = [
  {
    path: '',
    component: GenBusCollateralAgmAddendumPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GenBusCollateralAgmAddendumRoutingModule {}
