import { Component } from '@angular/core';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { UIControllerService } from 'core/services/uiController.service';
import { ROUTE_FUNCTION_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { PageInformationModel, PathParamModel } from 'shared/models/systemModel';

@Component({
  selector: 'gen-bus-collateral-agm-addendum',
  templateUrl: './gen-bus-collateral-agm-addendum.page.html',
  styleUrls: ['./gen-bus-collateral-agm-addendum.page.scss']
})
export class GenBusCollateralAgmAddendumPage {
  pageInfo: PageInformationModel;
  currentRoute = this.uiService.getMasterRoute(1);
  masterRoute = this.uiService.getMasterRoute();
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    console.log('masterRoute: ', this.masterRoute);
    console.log('currentRoute: ', this.currentRoute);

    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.GEN_BUS_ADDENDUM,
      servicePath: `BusinessCollateralagmTable/${getProductType(this.masterRoute, true)}/${this.currentRoute}/RelatedInfo/${
        ROUTE_RELATED_GEN.ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE
      }/Function/${ROUTE_FUNCTION_GEN.GEN_BUS_ADDENDUM}`
    };
  }
}
