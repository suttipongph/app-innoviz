import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenBusCollateralAgmAddendumRoutingModule } from './gen-bus-collateral-agm-addendum-routing.module';
import { GenBusCollateralAgmAddendumPage } from './gen-bus-collateral-agm-addendum/gen-bus-collateral-agm-addendum.page';
import { SharedModule } from 'shared/shared.module';
import { GenBusCollateralAgmAddendumComponentModule } from 'components/business-collateral-agm/gen-bus-collateral-agm-addendum/gen-bus-collateral-agm-addendum.module';

@NgModule({
  declarations: [GenBusCollateralAgmAddendumPage],
  imports: [CommonModule, SharedModule, GenBusCollateralAgmAddendumRoutingModule, GenBusCollateralAgmAddendumComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GenBusCollateralAgmAddendumFunctionModule {}
