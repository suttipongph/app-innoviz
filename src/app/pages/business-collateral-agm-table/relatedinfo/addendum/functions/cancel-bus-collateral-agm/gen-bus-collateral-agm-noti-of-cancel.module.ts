import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoticeOfCancellationFunctionRoutingModule } from './gen-bus-collateral-agm-noti-of-cancel-routing.module';
import { NoticeOfCancellationPage } from './gen-bus-collateral-agm-noti-of-cancel/gen-bus-collateral-agm-noti-of-cancel.page';
import { SharedModule } from 'shared/shared.module';
import { CancelBusinessCollateralAgmComponentModule } from 'components/business-collateral-agm/cancel-business-collateral-agm/cancel-business-collateral-agm.module';

@NgModule({
  declarations: [NoticeOfCancellationPage],
  imports: [CommonModule, SharedModule, NoticeOfCancellationFunctionRoutingModule, CancelBusinessCollateralAgmComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NoticeOfCancellationFunctionModule {}
