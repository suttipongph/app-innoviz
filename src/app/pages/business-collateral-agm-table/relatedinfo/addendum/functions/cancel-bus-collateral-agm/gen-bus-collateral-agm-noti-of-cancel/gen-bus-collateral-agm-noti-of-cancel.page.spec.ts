import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoticeOfCancellationPage } from './gen-bus-collateral-agm-noti-of-cancel.page';

describe('NoticeOfCancellationPage', () => {
  let component: NoticeOfCancellationPage;
  let fixture: ComponentFixture<NoticeOfCancellationPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NoticeOfCancellationPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoticeOfCancellationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
