import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'generateaddendum',
    loadChildren: () =>
      import('./gen-bus-collateral-agm-addendum/gen-bus-collateral-agm-addendum.module').then((m) => m.GenBusCollateralAgmAddendumFunctionModule)
  },
  {
    path: 'noticeofcancellation',
    loadChildren: () =>
      import('./cancel-bus-collateral-agm/gen-bus-collateral-agm-noti-of-cancel.module').then((m) => m.NoticeOfCancellationFunctionModule)
  },
  {
    path: 'cancelagreement',
    loadChildren: () => import('./cancel/cancel-function.module').then((m) => m.BusinessCollateralAgmCancelFunctionModule)
  },
  {
    path: 'closeagreement',
    loadChildren: () => import('./close/close-function.module').then((m) => m.BusinessCollateralAgmCloseFunctionModule)
  },
  {
    path: 'postagreement',
    loadChildren: () => import('./post/post-function.module').then((m) => m.BusinessCollateralAgmPostFunctionModule)
  },
  {
    path: 'sendagreement',
    loadChildren: () => import('./send/send-function.module').then((m) => m.BusinessCollateralAgmSendFunctionModule)
  },
  {
    path: 'signagreement',
    loadChildren: () => import('./sign/sign-function.module').then((m) => m.BusinessCollateralAgmSignFunctionModule)
  },
  {
    path: 'printsetbookdoctrans',
    loadChildren: () => import('./print-book-doc/print-book-doc-function.module').then((m) => m.PrintBookDocFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessCollateralAgmTableAddendumFunctionRoutingModule {}
