import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JointVentureListPage } from './joint-venture-list/joint-venture-list.page';
import { JointVentureItemPage } from './joint-venture-item/joint-venture-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: JointVentureListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: JointVentureItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessCollateralAgmTableAddendumJointVentureRelatedinfoRoutingModule {}
