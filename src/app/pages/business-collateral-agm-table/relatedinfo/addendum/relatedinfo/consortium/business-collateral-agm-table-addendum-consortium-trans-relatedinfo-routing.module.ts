import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConsortiumListPage } from './consortium-list/consortium-list.page';
import { ConsortiumItemPage } from './consortium-item/consortium-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: ConsortiumListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ConsortiumItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessCollateralAgmTableAddendumConsortiumRelatedinfoRoutingModule {}
