import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BusinessCollateralAgmTableAddendumRelatedinfoRoutingModule } from './business-collateral-agm-table-addendum-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, BusinessCollateralAgmTableAddendumRelatedinfoRoutingModule]
})
export class BusinessCollateralAgmAddendumTableRelatedinfoModule {}
