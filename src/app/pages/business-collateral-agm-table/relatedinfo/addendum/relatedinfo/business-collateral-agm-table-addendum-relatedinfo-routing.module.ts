import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'agreementtableinfo',
    loadChildren: () =>
      import('./agreement-information/business-collateral-agm-table-addendum-agreement-information-relatedinfo.module').then(
        (m) => m.BusinessCollateralAgmTableAddendumAgreementInformationRelatedinfoModule
      )
  },
  {
    path: 'servicefeetrans',
    loadChildren: () =>
      import('./service-fee-trans/business-collateral-agm-table-addendum-service-fee-trans-relatedinfo.module').then(
        (m) => m.BusinessCollateralAgmTableAddendumServiceFeeTransRelatedinfoModule
      )
  },
  {
    path: 'memotrans',
    loadChildren: () =>
      import('./memo/business-collateral-agm-table-addendum-memo-relatedinfo.module').then(
        (m) => m.BusinessCollateralAgmTableAddendumMemoRelatedinfoModule
      )
  },
  {
    path: 'jointventuretrans',
    loadChildren: () =>
      import('./joint-venture/business-collateral-agm-table-addendum-joint-venture-relatedinfo.module').then(
        (m) => m.BusinessCollateralAgmTableAddendumJointVentureRelatedinfoModule
      )
  },
  {
    path: 'consortiumtrans',
    loadChildren: () =>
      import('./consortium/business-collateral-agm-table-addendum-consortium-trans-relatedinfo.module').then(
        (m) => m.BusinessCollateralAgmTableAddendumConsortiumTransRelatedinfoModule
      )
  },
  {
    path: 'bookmarkdocumenttrans',
    loadChildren: () =>
      import('./bookmark-document/business-collateral-agm-table-addendum-bookmark-document-relatedinfo.module').then(
        (m) => m.BusinessCollateralAgmAddendumBookmarkDocumentRelatedinfoModule
      )
  },
  {
    path: 'attachment',
    loadChildren: () =>
      import('./attachment/business-collateral-agm-table-addendum-attachment.module').then(
        (m) => m.BusinessCollateralAgmTableAddendumAttachmentModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessCollateralAgmTableAddendumRelatedinfoRoutingModule {}
