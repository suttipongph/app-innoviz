import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessCollateralAgmTableAddendumServiceFeeTransRelatedinfoRelatedInfoRoutingModule } from './business-collateral-agm-table-addendum-service-fee-trans-relatedinfo-relatedinfo-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, BusinessCollateralAgmTableAddendumServiceFeeTransRelatedinfoRelatedInfoRoutingModule]
})
export class BusinessCollateralAgmTableAddendumServiceFeeTransRelatedinfoRelatedInfoModule {}
