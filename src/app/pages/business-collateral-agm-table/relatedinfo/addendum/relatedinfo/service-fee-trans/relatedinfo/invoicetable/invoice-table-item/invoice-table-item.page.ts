import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'invoice-table-item.page',
  templateUrl: './invoice-table-item.page.html',
  styleUrls: ['./invoice-table-item.page.scss']
})
export class InvoiceTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_RELATED_GEN.INVOICE_TABLE,
      servicePath: 'InvoiceTable',
      childPaths: [{ pagePath: ROUTE_MASTER_GEN.INVOICELINE, servicePath: 'InvoiceTable' }]
    };
  }
}
