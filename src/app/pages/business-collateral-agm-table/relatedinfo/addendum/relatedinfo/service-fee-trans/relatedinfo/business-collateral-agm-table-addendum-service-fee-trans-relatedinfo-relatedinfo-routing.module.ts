import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'invoicetable',
    loadChildren: () =>
      import('./invoicetable/business-collateral-agm-table-service-fee-trans-invoicetable-relatedinfo.module').then(
        (m) => m.BusinessCollateralAgmTableServiceFeeTransInvoiceTableRelatedinfoModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessCollateralAgmTableAddendumServiceFeeTransRelatedinfoRelatedInfoRoutingModule {}
