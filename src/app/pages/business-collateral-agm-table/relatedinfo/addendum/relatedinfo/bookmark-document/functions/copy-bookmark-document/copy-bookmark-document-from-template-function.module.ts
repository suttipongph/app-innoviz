import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CopyBookmarkDocumentFromTemplateFunctionRoutingModule } from './copy-bookmark-document-from-template-function-routing.module';
import { CopyBookmarkDocumentFromTemplatePage } from './copy-bookmark-document-from-template/copy-bookmark-document-from-template.page';
import { SharedModule } from 'shared/shared.module';
import { CopyBookmarkDocumentTemplateComponentModule } from 'components/bookmark-document-trans/copy-bookmark-document-template/copy-bookmark-document-template.module';

@NgModule({
  declarations: [CopyBookmarkDocumentFromTemplatePage],
  imports: [CommonModule, SharedModule, CopyBookmarkDocumentFromTemplateFunctionRoutingModule, CopyBookmarkDocumentTemplateComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CopyBookmarkDocumentFromTemplateFunctionModule {}
