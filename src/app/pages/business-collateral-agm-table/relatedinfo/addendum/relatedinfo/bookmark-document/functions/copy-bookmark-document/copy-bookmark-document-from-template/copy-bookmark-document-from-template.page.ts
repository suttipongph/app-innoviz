import { Component, OnInit } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { ROUTE_FUNCTION_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { UIControllerService } from 'core/services/uiController.service';
import { getProductType } from 'shared/functions/value.function';

@Component({
  selector: 'copy-bookmark-document-from-template-page',
  templateUrl: './copy-bookmark-document-from-template.page.html',
  styleUrls: ['./copy-bookmark-document-from-template.page.scss']
})
export class CopyBookmarkDocumentFromTemplatePage implements OnInit {
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_FUNCTION_GEN.COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE,
      servicePath: `BusinessCollateralAgmTable/${getProductType(this.masterRoute, true)}/${this.currentRoute}/RelatedInfo/${
        ROUTE_RELATED_GEN.ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE
      }/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage`
    };
  }
}
