import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessCollateralAgmAddendumRelatedinfoRoutingModule } from './business-collateral-agm-table-addendum-relatedinfo-routing.module';
import { AddendumListPage } from './addendum-list/addendum-list.page';
import { AddendumItemPage } from './addendum-item/addendum-item.page';
import { SharedModule } from 'shared/shared.module';
import { BusinessCollateralAgmTableComponentModule } from 'components/business-collateral-agm/business-collateral-agm-table/business-collateral-agm-table.module';
import { AddendumLineItemPage } from './addendum-line-item/addendum-line-item.page';
import { BusinessCollateralAgmLineComponentModule } from 'components/business-collateral-agm/business-collateral-agm-line/business-collateral-agm-line.module';

@NgModule({
  declarations: [AddendumListPage, AddendumItemPage, AddendumLineItemPage],
  imports: [
    CommonModule,
    SharedModule,
    BusinessCollateralAgmAddendumRelatedinfoRoutingModule,
    BusinessCollateralAgmTableComponentModule,
    BusinessCollateralAgmLineComponentModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusinessCollateralAgmAddendumRelatedinfoModule {}
