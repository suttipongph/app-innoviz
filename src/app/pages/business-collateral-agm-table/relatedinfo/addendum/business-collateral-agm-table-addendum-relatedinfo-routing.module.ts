import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddendumListPage } from './addendum-list/addendum-list.page';
import { AddendumItemPage } from './addendum-item/addendum-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { AddendumLineItemPage } from './addendum-line-item/addendum-line-item.page';

const routes: Routes = [
  {
    path: '',
    component: AddendumListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: AddendumItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/addendumbusinesscollateralagmline-child/:id',
    component: AddendumLineItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'function',
    loadChildren: () =>
      import('./functions/business-collateral-agm-table-addendum-function.module').then((m) => m.BusinessCollateralAgmTableAddendumFunctionModule)
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () =>
      import('./relatedinfo/business-collateral-agm-table-addendum-relatedinfo.module').then(
        (m) => m.BusinessCollateralAgmAddendumTableRelatedinfoModule
      )
  },
  {
    path: ':id/function',
    loadChildren: () =>
      import('./functions/business-collateral-agm-table-addendum-function.module').then((m) => m.BusinessCollateralAgmTableAddendumFunctionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessCollateralAgmAddendumRelatedinfoRoutingModule {}
