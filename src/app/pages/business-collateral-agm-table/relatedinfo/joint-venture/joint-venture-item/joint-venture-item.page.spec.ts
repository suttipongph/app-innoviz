import { ComponentFixture, TestBed } from '@angular/core/testing';
import { JointVentureItemPage } from './joint-venture-item.page';

describe('JointVentureItemPage', () => {
  let component: JointVentureItemPage;
  let fixture: ComponentFixture<JointVentureItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [JointVentureItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JointVentureItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
