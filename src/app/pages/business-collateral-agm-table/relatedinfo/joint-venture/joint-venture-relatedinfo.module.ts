import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JointVentureRelatedinfoRoutingModule } from './joint-venture-relatedinfo-routing.module';
import { JointVentureListPage } from './joint-venture-list/joint-venture-list.page';
import { JointVentureItemPage } from './joint-venture-item/joint-venture-item.page';
import { SharedModule } from 'shared/shared.module';
import { JointVentureTransComponentModule } from 'components/joint-venture-trans/joint-venture-trans.module';

@NgModule({
  declarations: [JointVentureListPage, JointVentureItemPage],
  imports: [CommonModule, SharedModule, JointVentureRelatedinfoRoutingModule, JointVentureTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JointVentureRelatedinfoModule {}
