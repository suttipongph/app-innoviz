import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessCollateralAgmTableConsortiumRelatedinfoRoutingModule } from './business-collateral-agm-table-consortium-trans-relatedinfo-routing.module';
import { ConsortiumListPage } from './consortium-list/consortium-list.page';
import { ConsortiumItemPage } from './consortium-item/consortium-item.page';
import { SharedModule } from 'shared/shared.module';
import { ConsortiumTransComponentModule } from 'components/consortium-trans/consortium-trans.module';

@NgModule({
  declarations: [ConsortiumListPage, ConsortiumItemPage],
  imports: [CommonModule, SharedModule, BusinessCollateralAgmTableConsortiumRelatedinfoRoutingModule, ConsortiumTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusinessCollateralAgmTableConsortiumTransRelatedinfoModule {}
