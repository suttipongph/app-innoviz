import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ConsortiumListPage } from './consortium-list.page';

describe('ConsortiumListPage', () => {
  let component: ConsortiumListPage;
  let fixture: ComponentFixture<ConsortiumListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConsortiumListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsortiumListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
