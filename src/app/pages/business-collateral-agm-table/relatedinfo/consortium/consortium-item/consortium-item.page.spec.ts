import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ConsortiumItemPage } from './consortium-item.page';

describe('ConsortiumItemPage', () => {
  let component: ConsortiumItemPage;
  let fixture: ComponentFixture<ConsortiumItemPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConsortiumItemPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsortiumItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
