import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookmarkDocumentListPage } from './bookmark-document-list/bookmark-document-list.page';
import { BookmarkDocumentItemPage } from './bookmark-document-item/bookmark-document-item.page';
import { SharedModule } from 'shared/shared.module';
import { BusinessCollateralAgmBookmarkDocumentRoutingModule } from './business-collateral-agm-table-bookmark-document-relatedinfo-routing.module';
import { BookmarkDocumentTransComponentModule } from 'components/bookmark-document-trans/bookmark-document-trans/bookmark-document-trans.module';

@NgModule({
  declarations: [BookmarkDocumentListPage, BookmarkDocumentItemPage],
  imports: [CommonModule, SharedModule, BusinessCollateralAgmBookmarkDocumentRoutingModule, BookmarkDocumentTransComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusinessCollateralAgmBookmarkDocumentRelatedinfoModule {}
