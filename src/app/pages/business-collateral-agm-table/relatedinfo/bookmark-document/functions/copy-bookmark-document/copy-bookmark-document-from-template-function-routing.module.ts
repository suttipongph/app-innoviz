import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { CopyBookmarkDocumentFromTemplatePage } from './copy-bookmark-document-from-template/copy-bookmark-document-from-template.page';

const routes: Routes = [
  {
    path: '',
    component: CopyBookmarkDocumentFromTemplatePage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CopyBookmarkDocumentFromTemplateFunctionRoutingModule {}
