import { Component, OnInit } from '@angular/core';
import { UIControllerService } from 'core/services/uiController.service';
import { AgreementDocType, ColumnType, ROUTE_MASTER_GEN, SortType } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'business-collateral-agm-table-list-page',
  templateUrl: './business-collateral-agm-table-list.page.html',
  styleUrls: ['./business-collateral-agm-table-list.page.scss']
})
export class BusinessCollateralAgmTableListPage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  masterRoute = this.uiService.getMasterRoute();
  currentRoute = this.uiService.getMasterRoute(1);
  constructor(public uiService: UIControllerService) {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'businessCollateralAgmTableGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'productType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: getProductType(this.masterRoute)
      }
    ];

    this.setColumnOptionByNewBusinessCollateralAgm(columns);
    this.setColumnOptionByAllBusinessCollateralAgm(columns);
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: `/${getProductType(this.masterRoute, true)}/${this.currentRoute}/${ROUTE_MASTER_GEN.BUSINESS_COLLATERAL_AGM_TABLE}`,
      servicePath: `${ROUTE_MASTER_GEN.BUSINESS_COLLATERAL_AGM_TABLE}/${getProductType(this.masterRoute, true)}/${this.currentRoute}`
    };
  }
  setColumnOptionByNewBusinessCollateralAgm(columns: ColumnModel[]) {
    if (this.currentRoute === 'newbusinesscollateralagmtable') {
      columns.push({
        label: null,
        textKey: 'agreementDocType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: AgreementDocType.New.toString()
      });
    }
  }
  setColumnOptionByAllBusinessCollateralAgm(columns: ColumnModel[]) {
    if (this.currentRoute === 'allbusinesscollateralagmtable') {
      this.option.canCreate = false;
    }
  }
}
