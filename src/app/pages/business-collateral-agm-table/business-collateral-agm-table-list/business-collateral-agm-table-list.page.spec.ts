import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessCollateralAgmTableListPage } from './business-collateral-agm-table-list.page';

describe('BusinessCollateralAgmTableListPage', () => {
  let component: BusinessCollateralAgmTableListPage;
  let fixture: ComponentFixture<BusinessCollateralAgmTableListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BusinessCollateralAgmTableListPage]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCollateralAgmTableListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
