import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LeasingRoutingModule } from './leasing-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, LeasingRoutingModule]
})
export class LeasingModule {}
