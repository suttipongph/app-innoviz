import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss']
})
export class HomePage implements OnInit {
  option: OptionModel;
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setOption();
    this.setPath();
  }
  setOption(): void {
    this.option = new OptionModel();
    this.option.key = 'formURL';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
  }
  setPath(): void {
    this.pageInfo = {
      pagePath: ROUTE_MASTER_GEN.HOME,
      servicePath: 'Home',
      childPaths: [
        {
          pagePath: ROUTE_MASTER_GEN.HOME,
          servicePath: 'Home',
          pageInputId: 'ACTIVE_TASK_LIST'
        },
        {
          pagePath: ROUTE_MASTER_GEN.HOME,
          servicePath: 'Home',
          pageInputId: 'OPENED_TASK_LIST'
        }
      ]
    };
  }
}
