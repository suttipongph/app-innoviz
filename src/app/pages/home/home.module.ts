import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { HomePage } from './home.page';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponentModule } from 'components/home/home.module';

@NgModule({
  declarations: [HomePage],
  imports: [CommonModule, SharedModule, HomeRoutingModule, HomeComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HomeModule {}
