import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'contacttrans',
    loadChildren: () => import('./contact/employee-table-contact-trans-relatedinfo.module').then((m) => m.EmployeeTableContactTransRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeTableRelatedinfoRoutingModule {}
