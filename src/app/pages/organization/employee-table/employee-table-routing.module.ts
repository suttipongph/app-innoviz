import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeTableListPage } from './employee-table-list/employee-table-list.page';
import { EmployeeTableItemPage } from './employee-table-item/employee-table-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: EmployeeTableListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: EmployeeTableItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/employee-table-relatedinfo.module').then((m) => m.EmployeeTableRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeTableRoutingModule {}
