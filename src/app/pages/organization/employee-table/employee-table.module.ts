import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeTableRoutingModule } from './employee-table-routing.module';
import { EmployeeTableListPage } from './employee-table-list/employee-table-list.page';
import { EmployeeTableItemPage } from './employee-table-item/employee-table-item.page';
import { SharedModule } from 'shared/shared.module';
import { EmployeeTableComponentModule } from 'components/setup/employee-table/employee-table.module';

@NgModule({
  declarations: [EmployeeTableListPage, EmployeeTableItemPage],
  imports: [CommonModule, SharedModule, EmployeeTableRoutingModule, EmployeeTableComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EmployeeTableModule {}
