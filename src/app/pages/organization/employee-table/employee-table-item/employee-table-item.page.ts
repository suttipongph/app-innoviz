import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'employee-table-item.page',
  templateUrl: './employee-table-item.page.html',
  styleUrls: ['./employee-table-item.page.scss']
})
export class EmployeeTableItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_MASTER_GEN.EMPLOYEE_TABLE, servicePath: 'EmployeeTable' };
  }
}
