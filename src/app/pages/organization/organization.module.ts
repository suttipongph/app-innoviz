import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetupOrganizationRoutingModule } from './organization-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SetupOrganizationRoutingModule]
})
export class OrganizationModule {}
