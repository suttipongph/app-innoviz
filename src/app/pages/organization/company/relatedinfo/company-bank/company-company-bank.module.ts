import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyBankListPage } from './company-bank-list/company-bank-list.page';
import { CompanyBankItemPage } from './company-bank-item/company-bank-item.page';
import { SharedModule } from 'shared/shared.module';
import { CompanyBankComponentModule } from 'components/company-bank/company-bank.module';
import { CompanyCompanyBankRoutingModule } from './company-company-bank-routing.module';

@NgModule({
  declarations: [CompanyBankListPage, CompanyBankItemPage],
  imports: [CommonModule, SharedModule, CompanyCompanyBankRoutingModule, CompanyBankComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CompanyCompanyBankModule {}
