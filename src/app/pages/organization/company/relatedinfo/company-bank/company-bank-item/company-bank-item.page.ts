import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'company-bank-item.page',
  templateUrl: './company-bank-item.page.html',
  styleUrls: ['./company-bank-item.page.scss']
})
export class CompanyBankItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.COMPANY_BANK, servicePath: 'Company/RelatedInfo/CompanyBank' };
  }
}
