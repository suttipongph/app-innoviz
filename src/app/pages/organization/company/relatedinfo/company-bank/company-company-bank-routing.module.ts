import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompanyBankListPage } from './company-bank-list/company-bank-list.page';
import { CompanyBankItemPage } from './company-bank-item/company-bank-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: CompanyBankListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CompanyBankItemPage,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyCompanyBankRoutingModule {}
