import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'companybank',
    loadChildren: () => import('./company-bank/company-company-bank.module').then((m) => m.CompanyCompanyBankModule)
  },
  {
    path: 'branch',
    loadChildren: () => import('./branch/branch.module').then((m) => m.CompanyCompanyBranchModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyRelatedinfoRoutingModule {}
