import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyCompanyBranchRoutingModule } from './branch-routing.module';
import { BranchItemPage } from './branch-item/branch-item.page';
import { SharedModule } from 'shared/shared.module';
import { BranchListPage } from './branch-list/branch.page';
import { BranchComponentModule } from 'components/branch/branch/branch.module';

@NgModule({
  declarations: [BranchListPage, BranchItemPage],
  imports: [CommonModule, SharedModule, CompanyCompanyBranchRoutingModule, BranchComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CompanyCompanyBranchModule {}
