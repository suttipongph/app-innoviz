import { Component, OnInit } from '@angular/core';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'branch-item.page',
  templateUrl: './branch-item.page.html',
  styleUrls: ['./branch-item.page.scss']
})
export class BranchItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}

  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.COMPANY_BRANCH, servicePath: 'Company/RelatedInfo/branch' };
  }
}
