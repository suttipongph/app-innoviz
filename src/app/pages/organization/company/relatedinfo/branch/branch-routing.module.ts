import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'core/services/auth-guard.service';
import { BranchItemPage } from './branch-item/branch-item.page';
import { BranchListPage } from './branch-list/branch.page';

const routes: Routes = [
  {
    path: '',
    component: BranchListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: BranchItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/company-branch-relatedinfo.module').then((m) => m.CompanyBranchRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyCompanyBranchRoutingModule {}
