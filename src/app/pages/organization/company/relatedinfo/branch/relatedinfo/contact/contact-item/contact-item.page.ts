import { Component, OnInit } from '@angular/core';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'app-contact-item',
  templateUrl: './contact-item.page.html',
  styleUrls: ['./contact-item.page.scss']
})
export class ContactItemPage implements OnInit {
  pageInfo: PageInformationModel;
  constructor() {}
  ngOnInit(): void {
    this.setPath();
  }
  setPath(): void {
    this.pageInfo = { pagePath: ROUTE_RELATED_GEN.CONTACT, servicePath: 'Company/RelatedInfo/branch/relatedinfo/ContactTrans' };
  }
}
