import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'addresstrans',
    loadChildren: () => import('./address-trans/branch-relatedinfo-address-trans.module').then((m) => m.CompanyBranchRelatedinfoAddressTransModule)
  },
  {
    path: 'contacttrans',
    loadChildren: () => import('./contact/branch-relatedinfo-contact-trans-.module').then((m) => m.CompanyBranchRelatedinfoContactTransModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyBranchRelatedinfoRoutingModule {}
