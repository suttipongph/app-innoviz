import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompanyListPage } from './company-list/company-list.page';
import { CompanyItemPage } from './company-item/company-item.page';
import { AuthGuardService } from 'core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: CompanyListPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: CompanyItemPage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id/relatedinfo',
    loadChildren: () => import('./relatedinfo/company-relatedinfo.module').then((m) => m.CompanyRelatedinfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyRoutingModule {}
