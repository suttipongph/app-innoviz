import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyRoutingModule } from './company-routing.module';
import { CompanyListPage } from './company-list/company-list.page';
import { CompanyItemPage } from './company-item/company-item.page';
import { SharedModule } from 'shared/shared.module';
import { CompanyComponentModule } from 'components/setup/company/company/company.module';

@NgModule({
  declarations: [CompanyListPage, CompanyItemPage],
  imports: [CommonModule, SharedModule, CompanyRoutingModule, CompanyComponentModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CompanyModule {}
