import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'company', loadChildren: () => import('./company/company.module').then((m) => m.CompanyModule) },
  { path: 'employeetable', loadChildren: () => import('./employee-table/employee-table.module').then((m) => m.EmployeeTableModule) }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SetupOrganizationRoutingModule {}
