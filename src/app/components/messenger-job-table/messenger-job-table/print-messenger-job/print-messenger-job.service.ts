import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PrintMessengerJobReportView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class PrintMessengerJobService {
  serviceKey = 'printMessengerJobGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getPrintMessengerJobById(id: string): Observable<PrintMessengerJobReportView> {
    const url = `${this.servicePath}/GetPrintMessengerJobById/id=${id}`;
    return this.dataGateway.get(url);
  }
}
