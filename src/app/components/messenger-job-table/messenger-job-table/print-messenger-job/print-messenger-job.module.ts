import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PrintMessengerJobReportComponent } from './print-messenger-job/print-messenger-job-report.component';
import { PrintMessengerJobService } from './print-messenger-job.service';
import { PrintMessengerJobReportCustomComponent } from './print-messenger-job/print-messenger-job-report-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PrintMessengerJobReportComponent],
  providers: [PrintMessengerJobService, PrintMessengerJobReportCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PrintMessengerJobReportComponent]
})
export class PrintMessengerJobComponentModule {}
