import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintMessengerJobReportComponent } from './print-messenger-job-report.component';

describe('PrintMessengerJobReportViewComponent', () => {
  let component: PrintMessengerJobReportComponent;
  let fixture: ComponentFixture<PrintMessengerJobReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrintMessengerJobReportComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintMessengerJobReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
