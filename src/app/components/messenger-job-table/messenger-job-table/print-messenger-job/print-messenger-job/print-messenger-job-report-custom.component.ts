import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { PrintMessengerJobReportView } from 'shared/models/viewModel';

const firstGroup = [
  'MESSENGER_JOB_TABLE_GUID',
  'DOCUMENT_STATUS_GUID',
  'JOB_DATE',
  'JOB_TIME',
  'JOB_TYPE_GUID',
  'MESSENGER_TABLE_GUID',
  'JOB_DETAIL',
  'CONTACT_TO',
  'CONTACT_PERSON_NAME',
  'JOB_START_TIME',
  'JOB_END_TIME'
];

export class PrintMessengerJobReportCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: PrintMessengerJobReportView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<PrintMessengerJobReportView> {
    let model = new PrintMessengerJobReportView();
    model.printMessengerJobGUID = EmptyGuid;
    return of(model);
  }
}
