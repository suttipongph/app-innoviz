import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CancelMessengerJobService } from './cancel-messenger-job.service';
import { CancelMessengerJobComponent } from './cancel-messenger-job/cancel-messenger-job.component';
import { CancelMessengerJobCustomComponent } from './cancel-messenger-job/cancel-messenger-job-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CancelMessengerJobComponent],
  providers: [CancelMessengerJobService, CancelMessengerJobCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CancelMessengerJobComponent]
})
export class CancelMessengerJobComponentModule {}
