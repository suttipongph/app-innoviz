import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CancelMessengerJobView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CancelMessengerJobService {
  serviceKey = 'cancelMessengerJobGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  deleteCancelMessengerJob(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCancelMessengerJob`;
    return this.dataGateway.delete(url, row);
  }
  getCancelMessengerJobById(id: string): Observable<CancelMessengerJobView> {
    const url = `${this.servicePath}/GetCancelMessengerJobById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCancelMessengerJob(vmModel: CancelMessengerJobView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCancelMessengerJob`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCancelMessengerJob(vmModel: CancelMessengerJobView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCancelMessengerJob`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
