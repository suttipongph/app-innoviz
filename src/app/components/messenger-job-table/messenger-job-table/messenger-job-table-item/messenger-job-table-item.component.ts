import { Message } from 'primeng/api';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { map } from 'rxjs/operators';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, RefType, ROUTE_REPORT_GEN, MessengerJobStatus } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { MessengerJobTableItemView, RefIdParm } from 'shared/models/viewModel';
import { MessengerJobTableService } from '../messenger-job-table.service';
import { MessengerJobTableItemCustomComponent } from './messenger-job-table-item-custom.component';
@Component({
  selector: 'messenger-job-table-item',
  templateUrl: './messenger-job-table-item.component.html',
  styleUrls: ['./messenger-job-table-item.component.scss']
})
export class MessengerJobTableItemComponent extends BaseItemComponent<MessengerJobTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  addressTransOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  contactPersonTransOptions: SelectItems[] = [];
  contactToOptions: SelectItems[] = [];
  creditAppLineOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  employeeTableOptions: SelectItems[] = [];
  jobTypeOptions: SelectItems[] = [];
  messengerJobTableOptions: SelectItems[] = [];
  messengerTableOptions: SelectItems[] = [];
  priorityOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  resultOptions: SelectItems[] = [];
  assignmentAgreementTableOptions: SelectItems[] = [];
  constructor(
    private service: MessengerJobTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: MessengerJobTableItemCustomComponent,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.custom.baseService.service = service;
    this.parentId = this.custom.getRefGUID(this.uiControllerService);
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getContactToEnumDropDown(this.contactToOptions);
    this.baseDropdown.getPriorityEnumDropDown(this.priorityOptions);
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.baseDropdown.getResultEnumDropDown(this.resultOptions);
  }
  getById(): Observable<MessengerJobTableItemView> {
    return this.service.getMessengerJobTableById(this.id);
  }
  getInitialData(): Observable<MessengerJobTableItemView> {
    return this.custom.getInitialData(this.uiControllerService, this.userDataService);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions(result);
        this.setFunctionOptions(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.model.contactTo = null;
      this.onAsyncRunner(this.model);
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(
      this.custom.initContactPersonOption(model),
      this.custom.initAddressTransOption(model),
      this.baseDropdown.getBuyerTableDropDown(),
      this.baseDropdown.getCustomerTableDropDown(),
      this.baseDropdown.getDocumentStatusDropDown(),
      this.baseDropdown.getEmployeeTableDropDown(),
      this.baseDropdown.getJobTypeDropDown(),
      this.custom.getMessengerTableDropDown(
        model,
        this.baseDropdown.getMessengerJobTableDropDown(),
        this.baseDropdown.getMessengerJobTableByCustomerDropDown(model.customerTableGUID)
      ),
      this.baseDropdown.getMessengerTableDropDown(),
      this.custom.getAssignmentAgreementTableDropDown(model)
    ).subscribe(
      ([
        contactPersonTransOptions,
        addressTransOptions,
        buyerTable,
        customerTable,
        documentStatus,
        employeeTable,
        jobType,
        messengerJobTable,
        messengerTable,
        assignmentAgreementTable
      ]) => {
        this.contactPersonTransOptions = contactPersonTransOptions;
        this.addressTransOptions = addressTransOptions;
        this.buyerTableOptions = buyerTable;
        this.customerTableOptions = customerTable;
        this.documentStatusOptions = documentStatus;
        this.employeeTableOptions = employeeTable;
        this.jobTypeOptions = jobType;
        this.messengerJobTableOptions = messengerJobTable;
        this.messengerTableOptions = messengerTable;
        this.assignmentAgreementTableOptions = assignmentAgreementTable;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model, this.uiControllerService).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  setRelatedInfoOptions(model: MessengerJobTableItemView): void {
    const isContactTo = this.custom.isContactTo(model);
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        command: () => {
          const refIdParm: RefIdParm = { refType: RefType.MessengerJob, refGUID: model.messengerJobTableGUID };
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: refIdParm
          });
        }
      },
      { label: 'LABEL.CHEQUE', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.CHEQUE }) },
      {
        label: 'LABEL.SERVICE_FEE_TRANSACTIONS',
        disabled: isContactTo,
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.SERVICE_FEE_TRANS,
            parameters: {
              refId: model.jobId,
              taxDate: model.jobDate,
              productType: model.productType,
              invoiceTable_CustomerTableGUID: model.customerTableGUID
            }
          })
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(model: MessengerJobTableItemView): void {
    this.functionItems = [
      {
        label: 'LABEL.ASSIGN',
        command: () =>
          this.toFunction({
            path: ROUTE_FUNCTION_GEN.MESSENGER_JOB_TABLE_ASSIGN
          }),
        disabled: !(model.documentStatus_StatusId == MessengerJobStatus.Draft)
      },
      {
        label: 'LABEL.POST',
        command: () =>
          this.toFunction({
            path: ROUTE_FUNCTION_GEN.MESSENGER_JOB_TABLE_POST
          }),
        disabled: !(model.documentStatus_StatusId == MessengerJobStatus.Assigned)
      },
      {
        label: 'LABEL.CANCEL',
        command: () =>
          this.toFunction({
            path: ROUTE_FUNCTION_GEN.MESSENGER_JOB_TABLE_CANCEL
          }),
        disabled: !(model.documentStatus_StatusId == MessengerJobStatus.Draft || model.documentStatus_StatusId == MessengerJobStatus.Assigned)
      },
      {
        label: 'LABEL.PRINT',
        items: [
          {
            label: 'LABEL.MESSENGER_JOB',
            command: () =>
              this.toReport({
                path: ROUTE_REPORT_GEN.PRINT_MESSENGER_JOB
              }),
            disabled: model.documentStatus_StatusId == MessengerJobStatus.Cancelled,
            visible: true
          }
        ]
      }
    ];
    super.setFunctionOptionsMapping();
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateMessengerJobTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createMessengerJobTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  contactToChange(e: any): any {
    this.custom.getDropdownByContactTo(this.model).subscribe(
      ([contactPersonTransOptions, addressTransOptions, assignmentAgreementTable]) => {
        this.contactPersonTransOptions = contactPersonTransOptions;
        this.addressTransOptions = addressTransOptions;
        this.assignmentAgreementTableOptions = assignmentAgreementTable;
        this.custom.contactChange(e, this.model, this.customerTableOptions, this.buyerTableOptions);
      },
      (error) => {
        this.baseService.notificationService.showErrorMessageFromResponse(error);
      }
    );
    this.custom.getFieldAccessing(this.model, this.uiControllerService).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  contactPersontransChange(e: any): any {
    this.custom.contactPersontransChange(e, this.model, this.contactPersonTransOptions);
  }
  addressTransChange(e: any): any {
    this.custom.addressTransChange(e, this.model, this.addressTransOptions);
  }
  messagerOnChange(e: any): any {
    this.custom.messagerOnChange(e, this.model, this.messengerTableOptions);
  }
  customerChange(e: any): any {
    this.getDropdownByCustomer(this.model);
    this.custom.clearFieldAfterGetDropdownByCustomer(this.model);
    this.custom.customerChange(e, this.model, this.customerTableOptions, this.messengerJobTableOptions);
    this.custom.getFieldAccessing(this.model, this.uiControllerService).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  buyerChange(e: any): any {
    this.getDropdownByBuyer(this.model);
    this.custom.clearFieldAfterGetDropdownByBuyer(this.model);
    this.custom.buyerChange(e, this.model, this.buyerTableOptions);
    this.custom.getFieldAccessing(this.model, this.uiControllerService).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  getDropdownByCustomer(model: MessengerJobTableItemView): void {
    forkJoin(
      this.custom.initContactPersonOption(model),
      this.custom.initAddressTransOption(model),
      this.custom.getAssignmentAgreementTableDropDown(model),
      this.custom.getMessengerTableDropDown(
        model,
        this.baseDropdown.getMessengerJobTableDropDown(),
        this.baseDropdown.getMessengerJobTableByCustomerDropDown(model.customerTableGUID)
      )
    ).subscribe(
      ([contactPersonTransOptions, addressTransOptions, assignmentAgreementTableOptions, messengerJobTableOptions]) => {
        this.contactPersonTransOptions = contactPersonTransOptions;
        this.addressTransOptions = addressTransOptions;
        this.assignmentAgreementTableOptions = assignmentAgreementTableOptions;
        this.messengerJobTableOptions = messengerJobTableOptions;
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  getDropdownByBuyer(model: MessengerJobTableItemView): void {
    forkJoin(
      this.custom.getCreditAppLineOptions(model, this.uiControllerService),
      this.custom.initContactPersonOption(model),
      this.custom.initAddressTransOption(model),
      this.custom.getAssignmentAgreementTableDropDown(model)
    ).subscribe(
      ([creditAppLineOptions, contactPersonTransOptions, addressTransOptions, assignmentAgreementTable]) => {
        this.creditAppLineOptions = creditAppLineOptions;
        this.contactPersonTransOptions = contactPersonTransOptions;
        this.addressTransOptions = addressTransOptions;
        this.assignmentAgreementTableOptions = assignmentAgreementTable;
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  jobTypeOnChange(e: any): any {
    this.custom.jobTypeOnChange(e, this.model, this.jobTypeOptions);
  }
}
