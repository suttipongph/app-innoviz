import { BuyerTableItemView } from './../../../../shared/models/viewModel/buyerTableItemView';
import { ContactTo } from './../../../../shared/constants/enumsBusiness';
import { Message } from 'primeng/api';
import { ROUTE_RELATED_GEN } from './../../../../shared/constants/constantGen';
import { AssignMessengerJobView } from './../../../../shared/models/viewModel/assignMessengerJobView';
import { PostMessengerJobView } from './../../../../shared/models/viewModel/postMessengerJobView';
import { CancelMessengerJobView } from './../../../../shared/models/viewModel/cancelMessengerJobView';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { forkJoin, Observable, of } from 'rxjs';
import { AppConst, EmptyGuid, MessengerJobStatus, RefType, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import {
  MessengerJobTableItemView,
  ContactPersonTransItemView,
  AddressTransItemView,
  MessengerTableItemView,
  CustomerTableItemView,
  JobTypeItemView
} from 'shared/models/viewModel';
import { map } from 'rxjs/operators';
import { UIControllerService } from 'core/services/uiController.service';
import { UserDataService } from 'core/services/user-data.service';
import { BaseDropdownComponent } from 'core/components/base/base.dropdown.component';

const firstGroup = [
  'VENDOR_ID',
  'DOCUMENT_STATUS_GUID',
  'REF_TYPE',
  'REF_ID',
  'REF_GUID',
  'MESSENGER_JOB_TABLE_GUID',
  'PRODUCT_TYPE',
  'REF_CREDIT_APP_LINE_GUID'
];
const refGroup = ['REF_TYPE', 'REF_ID', 'REF_GUID'];
const secondGroup = ['PRODUCT_TYPE'];
const isManualOncreate = ['JOB_ID'];
const messengerRequsetOnCreate = ['PRODUCT_TYPE', 'CUSTOMER_TABLE_GUID', 'BUYER_TABLE_GUID'];
const messengerRequsetOnView = ['CUSTOMER_TABLE_GUID', 'BUYER_TABLE_GUID'];
const condition2 = ['CUSTOMER_TABLE_GUID', 'BUYER_TABLE_GUID', 'CONTACT_PERSON_TRANS_GUID', 'ADDRESS_TRANS_GUID'];
const condition3 = ['CUSTOMER_TABLE_GUID', 'BUYER_TABLE_GUID'];
const condition4 = ['RESULT', 'RESULT_REMARK', 'FEE_AMOUNT', 'EXPENSE_AMOUNT'];
export class MessengerJobTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  isManual;
  buyerRequire = false;
  customerRequire = false;
  contactPersonNameRequire = false;
  contactAddress1Require = false;
  contactAddress2Require = false;
  assignmentAgreementIdRequire = false;
  contactPerson = false;
  contactAddress = false;
  getFieldAccessing(model: MessengerJobTableItemView, uiControllerService: UIControllerService): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    const parentName = uiControllerService.getRelatedInfoActiveTableName();
    fieldAccessing.push({ filedIds: refGroup, readonly: true });
    if (isNullOrUndefOrEmptyGUID(model.messengerJobTableGUID)) {
      if (parentName === ROUTE_RELATED_GEN.MESSENGER_REQUEST) {
        fieldAccessing.push({ filedIds: messengerRequsetOnCreate, readonly: true });
      }
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      if (model.contactTo === ContactTo.Specific) {
        fieldAccessing.push({ filedIds: condition2, readonly: true });
      } else {
        fieldAccessing.push({ filedIds: condition2, readonly: false });
      }

      return forkJoin([this.validateIsManualNumberSeq(model.companyGUID)]).pipe(
        map(([result]) => {
          this.isManual = result;
          fieldAccessing.push({ filedIds: isManualOncreate, readonly: !result });
          return fieldAccessing;
        })
      );
    } else {
      fieldAccessing.push({ filedIds: isManualOncreate, readonly: true });
      if (parentName === ROUTE_RELATED_GEN.MESSENGER_REQUEST) {
        fieldAccessing.push({ filedIds: messengerRequsetOnView, readonly: true });
      }
      if (model.documentStatus_StatusId === MessengerJobStatus.Assigned) {
        fieldAccessing.push({ filedIds: [AppConst.ALL_ID], readonly: true });
        fieldAccessing.push({ filedIds: condition4, readonly: false });
      } else if (model.documentStatus_StatusId === MessengerJobStatus.Draft) {
        fieldAccessing.push({ filedIds: firstGroup, readonly: true });
        fieldAccessing.push({ filedIds: secondGroup, readonly: true });
        if (model.contactTo === ContactTo.Specific) {
          fieldAccessing.push({ filedIds: condition2, readonly: true });
        } else {
          fieldAccessing.push({ filedIds: condition2, readonly: false });
        }

        if (model.refType === RefType.WithdrawalLine || model.refType === RefType.PurchaseLine) {
          fieldAccessing.push({ filedIds: condition3, readonly: true });
        }
      } else {
        fieldAccessing.push({ filedIds: [AppConst.ALL_ID], readonly: true });
      }
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: MessengerJobTableItemView): Observable<boolean> {
    const canEdit = model.documentStatus_StatusId === MessengerJobStatus.Posted || model.documentStatus_StatusId === MessengerJobStatus.Cancelled;
    return of(canEdit);
  }
  getInitialData(uiControllerService: UIControllerService, userDataService: UserDataService): Observable<MessengerJobTableItemView> {
    const panrentName = uiControllerService.getRelatedInfoOriginTableName();
    switch (panrentName) {
      case ROUTE_RELATED_GEN.MESSENGER_REQUEST:
        return this.baseService.service.GetMessengerJobRequestInitialData(
          uiControllerService.getKey('inquirywithdrawallineoutstand'),
          userDataService.getUserIdFromToken(),
          userDataService.getCurrentCompanyGUID()
        );
      case 'inquirywithdrawallineoutstand':
        return this.baseService.service.GetMessengerJobRequestInitialDataByInquiryWithdrawalLineOutstand(
          uiControllerService.getKey('inquirywithdrawallineoutstand')
        );
      case 'inquirypurchaselineoutstanding':
        return this.baseService.service.GetMessengerJobRequestInitialData(
          uiControllerService.getKey('inquirypurchaselineoutstanding'),
          userDataService.getUserIdFromToken(),
          userDataService.getCurrentCompanyGUID()
        );
      default:
        return this.baseService.service.getInitialData(userDataService.getCurrentCompanyGUID());
    }
  }

  getCreditAppLineOptions(model: MessengerJobTableItemView, uiControllerService: UIControllerService): Observable<SelectItems[]> {
    const panrentName = uiControllerService.getRelatedInfoActiveTableName();
    switch (panrentName) {
      case ROUTE_RELATED_GEN.MESSENGER_REQUEST:
        if (model.productType && !isNullOrUndefOrEmptyGUID(model.buyerTableGUID)) {
          return this.baseService.baseDropdown.getCreditAppLineDropDown([model.buyerTableGUID]);
        } else {
          return of([]);
        }
      default:
        if (
          model.refType === RefType.MessengerJob &&
          model.contactTo === ContactTo.Buyer &&
          model.productType &&
          !isNullOrUndefOrEmptyGUID(model.buyerTableGUID)
        ) {
          return this.baseService.baseDropdown.getCreditAppLineDropDown([model.productType.toString(), model.buyerTableGUID]);
        } else {
          return of([]);
        }
    }
  }
  validateIsManualNumberSeq(companyId: string): Observable<boolean> {
    return this.baseService.service.getNumberSeqParameter(companyId);
  }
  initContactPersonOption(model: MessengerJobTableItemView): Observable<SelectItems[]> {
    if (model.contactTo === ContactTo.Buyer) {
      this.buyerRequire = true;
      this.customerRequire = true;
      if (!isNullOrUndefOrEmptyGUID(model.buyerTableGUID)) {
        return this.baseService.baseDropdown.getContactPersonDropDown([model.buyerTableGUID.toString(), RefType.Buyer.toString()]);
      } else {
        return of([]);
      }
    } else if (model.contactTo === ContactTo.Customer) {
      this.buyerRequire = false;
      this.customerRequire = true;
      if (!isNullOrUndefOrEmptyGUID(model.customerTableGUID)) {
        return this.baseService.baseDropdown.getContactPersonDropDown([model.customerTableGUID.toString(), RefType.Customer.toString()]);
      } else {
        return of([]);
      }
    } else {
      this.buyerRequire = false;
      this.customerRequire = false;
      return of([]);
    }
  }
  initAddressTransOption(model: MessengerJobTableItemView): Observable<SelectItems[]> {
    if (model.contactTo === ContactTo.Buyer) {
      this.buyerRequire = true;
      this.customerRequire = true;
      if (!isNullOrUndefOrEmptyGUID(model.buyerTableGUID)) {
        return this.baseService.baseDropdown.getAddressTransByDropDown([model.buyerTableGUID.toString(), RefType.Buyer.toString()]);
      } else {
        return of([]);
      }
    } else if (model.contactTo === ContactTo.Customer) {
      this.buyerRequire = false;
      this.customerRequire = true;
      if (!isNullOrUndefOrEmptyGUID(model.customerTableGUID)) {
        return this.baseService.baseDropdown.getAddressTransByDropDown([model.customerTableGUID.toString(), RefType.Customer.toString()]);
      } else {
        return of([]);
      }
    } else {
      this.buyerRequire = false;
      this.customerRequire = false;
      return of([]);
    }
  }
  contactChange(contactTo: any, model: MessengerJobTableItemView, customerTableOptions: SelectItems[], buyerTableOptions: SelectItems[]): any {
    const fieldAccessing: FieldAccessing[] = [];
    this.clearData(model);
    if (model.refType === RefType.MessengerJob) {
      model.customerTableGUID = null;
      model.buyerTableGUID = null;
    }
    if (contactTo === ContactTo.Customer && !isNullOrUndefOrEmptyGUID(model.customerTableGUID)) {
      model.contactName = this.setContactNameByCustomer(model, model.customerTableGUID, customerTableOptions);
    }
    if (contactTo === ContactTo.Customer && isNullOrUndefOrEmptyGUID(model.customerTableGUID)) {
      model.contactName = '';
    }

    if (contactTo === ContactTo.Buyer && !isNullOrUndefOrEmptyGUID(model.buyerTableGUID)) {
      model.contactName = this.setContactNameByBuyer(model, model.buyerTableGUID, buyerTableOptions);
    }
    if (contactTo === ContactTo.Buyer && isNullOrUndefOrEmptyGUID(model.buyerTableGUID)) {
      model.contactName = '';
    }
    this.setMandatoryByContactTo(contactTo);
    return of(fieldAccessing);
  }
  setContactNameByBuyer(model: MessengerJobTableItemView, buyerTableGUID: string, buyerTableOptions: SelectItems[]): string {
    const rowData: BuyerTableItemView = isNullOrUndefOrEmptyGUID(buyerTableGUID)
      ? new BuyerTableItemView()
      : (buyerTableOptions.find((fn) => fn.value === buyerTableGUID).rowData as BuyerTableItemView);

    return rowData.buyerName;
  }
  setMandatoryByContactTo(contactTo: any): any {
    if (contactTo === ContactTo.Customer) {
      this.customerRequire = true;
      this.buyerRequire = false;
      this.contactPersonNameRequire = true;
      this.contactAddress1Require = true;
      this.contactAddress2Require = true;
    } else if (contactTo === ContactTo.Buyer) {
      this.customerRequire = true;
      this.buyerRequire = true;
      this.contactPersonNameRequire = true;
      this.contactAddress1Require = true;
      this.contactAddress2Require = true;
    } else {
      this.customerRequire = false;
      this.buyerRequire = false;
      this.contactPersonNameRequire = false;
      this.contactAddress1Require = false;
      this.contactAddress2Require = false;
    }
  }
  setContactNameByCustomer(model: MessengerJobTableItemView, customerTableGUID: string, customerTableOptions: SelectItems[]): string {
    const rowData: CustomerTableItemView = isNullOrUndefOrEmptyGUID(customerTableGUID)
      ? new CustomerTableItemView()
      : (customerTableOptions.find((fn) => fn.value === customerTableGUID).rowData as CustomerTableItemView);
    return rowData.name;
  }

  messagerOnChange(e: any, model: MessengerJobTableItemView, messengerTableOptions: SelectItems[]): any {
    const messagerTable: MessengerTableItemView = messengerTableOptions.find((t) => t.value === model.messengerTableGUID)
      .rowData as MessengerTableItemView;
    model.vendorId = messagerTable.vendorTable_VendorId;
  }
  clearData(model: MessengerJobTableItemView): void {
    model.refMessengerJobTableGUID = null;
    model.contactName = null;
    model.assignmentAgreementTableGUID = null;
    model.contactPersonTransGUID = null;
    model.contactPersonName = null;
    model.contactPersonPhone = null;
    model.contactPersonExtension = null;
    model.contactPersonMobile = null;
    model.contactPersonPosition = null;
    model.contactPersonDepartment = null;
    model.addressTransGUID = null;
    model.address1 = null;
    model.address2 = null;
  }
  buyerSetContact(model: MessengerJobTableItemView): void {
    model.contactName = model.buyerTable_Value;
  }
  customerSetContact(model: MessengerJobTableItemView): void {
    model.contactName = model.customerTable_Value;
  }
  setContactPersonTrans(model: MessengerJobTableItemView, contactPersonTransOptions: SelectItems[]): void {
    if (!isNullOrUndefined(model.contactPersonTransGUID)) {
      const contactPersonTrans: ContactPersonTransItemView = contactPersonTransOptions.find((t) => t.value === model.contactPersonTransGUID)
        .rowData as ContactPersonTransItemView;
      model.contactPersonName = contactPersonTrans.relatedPersonTable_Name;
      model.contactPersonPhone = contactPersonTrans.relatedPersonTable_Phone;
      model.contactPersonExtension = contactPersonTrans.relatedPersonTable_Extension;
      model.contactPersonMobile = contactPersonTrans.relatedPersonTable_Mobile;
      model.contactPersonPosition = contactPersonTrans.relatedPersonTable_Position;
    }
  }
  setAddressTransTrans(model: MessengerJobTableItemView, addressTransOptions: SelectItems[]): void {
    if (!isNullOrUndefined(model.addressTransGUID)) {
      const addressTrans: AddressTransItemView = addressTransOptions.find((t) => t.value === model.addressTransGUID).rowData as AddressTransItemView;
      model.address1 = addressTrans.address1;
      model.address2 = addressTrans.address2;
    }
  }
  contactPersontransChange(e: any, model: MessengerJobTableItemView, contactPersonTransOptions: SelectItems[]): any {
    const row: ContactPersonTransItemView = isNullOrUndefOrEmptyGUID(e)
      ? new ContactPersonTransItemView()
      : (contactPersonTransOptions.find((t) => t.value === model.contactPersonTransGUID).rowData as ContactPersonTransItemView);
    model.contactPersonName = null;
    model.contactPersonPhone = null;
    model.contactPersonExtension = null;
    model.contactPersonMobile = null;
    model.contactPersonPosition = null;
    model.contactPersonDepartment = null;

    model.contactPersonName = row.relatedPersonTable_Name;
    model.contactPersonPhone = row.relatedPersonTable_Phone;
    model.contactPersonExtension = row.relatedPersonTable_Extension;
    model.contactPersonMobile = row.relatedPersonTable_Mobile;
    model.contactPersonPosition = row.relatedPersonTable_Position;
  }
  addressTransChange(e: any, model: MessengerJobTableItemView, addressTransOptions: SelectItems[]): any {
    const row: AddressTransItemView = isNullOrUndefOrEmptyGUID(e)
      ? new AddressTransItemView()
      : (addressTransOptions.find((t) => t.value === model.addressTransGUID).rowData as AddressTransItemView);
    model.address1 = null;
    model.address2 = null;
    model.address1 = row.address1;
    model.address2 = row.address2;
  }
  customerChange(e: any, model: MessengerJobTableItemView, customerOption: SelectItems[], messengerJobTableOptions: SelectItems[]): any {
    const fieldAccessing: FieldAccessing[] = [];
    const rowCustomer: CustomerTableItemView = customerOption.find((t) => t.value === model.customerTableGUID).rowData as CustomerTableItemView;
    model.refMessengerJobTableGUID = null;
    if (model.contactTo === ContactTo.Customer) {
      model.contactPersonTransGUID = null;
      model.contactPersonName = null;
      model.contactPersonPhone = null;
      model.contactPersonExtension = null;
      model.contactPersonMobile = null;
      model.contactPersonPosition = null;
      model.addressTransGUID = null;
      model.address1 = null;
      model.address2 = null;
    }
    if (model.contactTo === ContactTo.Customer && !isNullOrUndefOrEmptyGUID(model.customerTableGUID)) {
      model.contactName = rowCustomer.name;
    }
    if (model.contactTo === ContactTo.Customer && isNullOrUndefOrEmptyGUID(model.customerTableGUID)) {
      model.contactName = null;
    }
    return of(fieldAccessing);
  }
  buyerChange(e: any, model: MessengerJobTableItemView, buyerOptonn: SelectItems[]): any {
    const rowBuyer: BuyerTableItemView = isNullOrUndefOrEmptyGUID(e)
      ? new BuyerTableItemView()
      : (buyerOptonn.find((t) => t.value === e).rowData as BuyerTableItemView);
    const fieldAccessing: FieldAccessing[] = [];
    if (model.contactTo === ContactTo.Buyer) {
      model.contactPersonTransGUID = null;
      model.contactPersonName = null;
      model.contactPersonPhone = null;
      model.contactPersonExtension = null;
      model.contactPersonMobile = null;
      model.contactPersonPosition = null;
      model.addressTransGUID = null;
      model.address1 = null;
      model.address2 = null;
      model.contactName = rowBuyer.buyerName;
    }
    if (model.contactTo === ContactTo.Buyer && !isNullOrUndefOrEmptyGUID(model.buyerTableGUID)) {
      model.contactName = rowBuyer.buyerName;
    }
    if (model.contactTo === ContactTo.Buyer && isNullOrUndefOrEmptyGUID(model.buyerTableGUID)) {
      model.contactName = null;
    }
    return of(fieldAccessing);
  }

  getRefGUID(uiControllerService: UIControllerService): any {
    const panrentName = uiControllerService.getRelatedInfoActiveTableName();
    switch (panrentName) {
      case ROUTE_RELATED_GEN.MESSENGER_REQUEST:
        return uiControllerService.getKey('inquirywithdrawallineoutstand');
      default:
        return null;
    }
  }
  isContactTo(model: MessengerJobTableItemView): boolean {
    if (model.contactTo === ContactTo.Specific) {
      return true;
    } else {
      return false;
    }
  }
  getMessengerTableDropDown(model: MessengerJobTableItemView, isNullCustomerTableGUID, isNotNull): Observable<SelectItems[]> {
    if (isNullOrUndefOrEmptyGUID(model.customerTableGUID)) {
      return isNullCustomerTableGUID;
    } else {
      return isNotNull;
    }
  }
  getAssignmentAgreementTableDropDown(model: MessengerJobTableItemView): Observable<SelectItems[]> {
    if (model.contactTo === ContactTo.Customer) {
      if (!isNullOrUndefOrEmptyGUID(model.customerTableGUID)) {
        return this.baseService.baseDropdown.getAssignmentAgreementTableByCustomerTableDropDown(model.customerTableGUID);
      } else {
        return of([]);
      }
    } else if (model.contactTo === ContactTo.Buyer) {
      if (!isNullOrUndefOrEmptyGUID(model.buyerTableGUID)) {
        return this.baseService.baseDropdown.getAssignmentAgreementTableByBuyerDropDown(model.buyerTableGUID);
      } else {
        return of([]);
      }
    } else if (model.contactTo === ContactTo.Specific) {
      return this.baseService.baseDropdown.getAssignmentAgreementTableDropDown();
    } else {
      return this.baseService.baseDropdown.getAssignmentAgreementTableDropDown();
    }
  }
  getDropdownByContactTo(model: MessengerJobTableItemView): Observable<any> {
    return forkJoin(this.initContactPersonOption(model), this.initAddressTransOption(model), this.getAssignmentAgreementTableDropDown(model));
  }
  clearFieldAfterGetDropdownByCustomer(model: MessengerJobTableItemView): void {
    if (model.contactTo === ContactTo.Customer) {
      model.contactPersonTransGUID = null;
      model.addressTransGUID = null;
      model.assignmentAgreementTableGUID = null;
    }
  }
  clearFieldAfterGetDropdownByBuyer(model: MessengerJobTableItemView): void {
    if (model.contactTo === ContactTo.Buyer) {
      model.contactPersonTransGUID = null;
      model.addressTransGUID = null;
      model.assignmentAgreementTableGUID = null;
      model.refCreditAppLineGUID = null;
    }
  }
  jobTypeOnChange(e: any, model: MessengerJobTableItemView, jobTypeOptions: SelectItems[]): any {
    const jobType: JobTypeItemView = isNullOrUndefOrEmptyGUID(model.jobTypeGUID)
      ? new JobTypeItemView()
      : (jobTypeOptions.find((t) => t.value === model.jobTypeGUID).rowData as JobTypeItemView);
    console.log(jobType);
    this.assignmentAgreementIdRequire = jobType?.assignment;
  }
}
