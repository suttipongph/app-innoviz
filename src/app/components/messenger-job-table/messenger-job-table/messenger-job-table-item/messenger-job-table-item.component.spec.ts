import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessengerJobTableItemComponent } from './messenger-job-table-item.component';

describe('MessengerJobTableItemViewComponent', () => {
  let component: MessengerJobTableItemComponent;
  let fixture: ComponentFixture<MessengerJobTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MessengerJobTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessengerJobTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
