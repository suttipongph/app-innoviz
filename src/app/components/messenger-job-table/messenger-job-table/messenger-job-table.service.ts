import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { MessengerJobTableItemView, MessengerJobTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class MessengerJobTableService {
  serviceKey = 'messengerJobTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getMessengerJobTableToList(search: SearchParameter): Observable<SearchResult<MessengerJobTableListView>> {
    const url = `${this.servicePath}/GetMessengerJobTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteMessengerJobTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteMessengerJobTable`;
    return this.dataGateway.delete(url, row);
  }
  getMessengerJobTableById(id: string): Observable<MessengerJobTableItemView> {
    const url = `${this.servicePath}/GetMessengerJobTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createMessengerJobTable(vmModel: MessengerJobTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateMessengerJobTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateMessengerJobTable(vmModel: MessengerJobTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateMessengerJobTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getNumberSeqParameter(companyId: string): Observable<ResponseModel> {
    const url = `${this.servicePath}/getNumberSeqParameter/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
  getInitialData(companyGUID: string): Observable<any> {
    const url = `${this.servicePath}/GetMessengerJobInitialData/companyGUID=${companyGUID}`;
    return this.dataGateway.get(url);
  }
  GetMessengerJobRequestInitialDataByInquiryWithdrawalLineOutstand(refGUID: string, userId: string, companyId: string): Observable<any> {
    const url = `${this.servicePath}/GetMessengerJobRequestInitialData/refGUID=${refGUID}`;
    return this.dataGateway.get(url);
  }
  GetMessengerJobRequestInitialData(refGUID: string, userId: string, companyId: string): Observable<any> {
    const url = `${this.servicePath}/GetMessengerJobRequestInitialData/refGUID=${refGUID}/userId=${userId}/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
}
