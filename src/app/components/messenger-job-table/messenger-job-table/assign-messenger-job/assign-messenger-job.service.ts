import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AssignMessengerJobView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class AssignMessengerJobService {
  serviceKey = 'assignMessengerJobGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  deleteAssignMessengerJob(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteAssignMessengerJob`;
    return this.dataGateway.delete(url, row);
  }
  getAssignMessengerJobById(id: string): Observable<AssignMessengerJobView> {
    const url = `${this.servicePath}/GetAssignMessengerJobById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createAssignMessengerJob(vmModel: AssignMessengerJobView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateAssignMessengerJob`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateAssignMessengerJob(vmModel: AssignMessengerJobView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateAssignMessengerJob`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
