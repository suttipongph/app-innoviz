import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignMessengerJobComponent } from './assign-messenger-job.component';

describe('AssignMessengerJobViewComponent', () => {
  let component: AssignMessengerJobComponent;
  let fixture: ComponentFixture<AssignMessengerJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AssignMessengerJobComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignMessengerJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
