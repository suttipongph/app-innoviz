import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AssignMessengerJobService } from './assign-messenger-job.service';
import { AssignMessengerJobComponent } from './assign-messenger-job/assign-messenger-job.component';
import { AssignMessengerJobCustomComponent } from './assign-messenger-job/assign-messenger-job-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [AssignMessengerJobComponent],
  providers: [AssignMessengerJobService, AssignMessengerJobCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [AssignMessengerJobComponent]
})
export class AssignMessengerJobComponentModule {}
