import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { MessengerJobTableListView } from 'shared/models/viewModel';
import { MessengerJobTableService } from '../messenger-job-table.service';
import { MessengerJobTableListCustomComponent } from './messenger-job-table-list-custom.component';

@Component({
  selector: 'messenger-job-table-list',
  templateUrl: './messenger-job-table-list.component.html',
  styleUrls: ['./messenger-job-table-list.component.scss']
})
export class MessengerJobTableListComponent extends BaseListComponent<MessengerJobTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  addressTransOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  contactPersonTransOptions: SelectItems[] = [];
  contactToOptions: SelectItems[] = [];
  creditAppLineOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  employeeTableOptions: SelectItems[] = [];
  jobTypeOptions: SelectItems[] = [];
  messengerJobTableOptions: SelectItems[] = [];
  messengerTableOptions: SelectItems[] = [];
  priorityOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  resultOptions: SelectItems[] = [];

  constructor(public custom: MessengerJobTableListCustomComponent, private service: MessengerJobTableService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getPriorityEnumDropDown(this.priorityOptions);
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.baseDropdown.getResultEnumDropDown(this.resultOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
    this.baseDropdown.getEmployeeTableDropDown(this.employeeTableOptions);
    this.baseDropdown.getJobTypeDropDown(this.jobTypeOptions);
    this.baseDropdown.getMessengerTableDropDown(this.messengerTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'messengerJobTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.JOB_ID',
        textKey: 'jobId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.JOB_DATE',
        textKey: 'jobDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.JOB_START_TIME',
        textKey: 'jobStartTime',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.JOB_END_TIME',
        textKey: 'jobEndTime',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.JOB_TYPE_ID',
        textKey: 'jobType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.jobTypeOptions,
        searchingKey: 'jobTypeGUID',
        sortingKey: 'jobType_JobTypeId'
      },
      {
        label: 'LABEL.MESSENGER_ID',
        textKey: 'messengerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.messengerTableOptions,
        searchingKey: 'messengerTableGUID',
        sortingKey: 'messengerTable_MessengerId'
      },
      {
        label: 'LABEL.PRIORITY',
        textKey: 'priority',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.priorityOptions
      },
      {
        label: 'LABEL.RESULT',
        textKey: 'result',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.resultOptions
      },
      {
        label: 'LABEL.REQUESTOR_ID',
        textKey: 'employeeTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.employeeTableOptions,
        searchingKey: 'requestorGUID',
        sortingKey: 'employeeTable_EmployeeId'
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId'
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerTableOptions,
        sortingKey: 'buyerTable_BuyerId',
        searchingKey: 'buyerTableGUID'
      },
      {
        label: 'LABEL.CONTACT_NAME',
        textKey: 'contactName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentStatusOptions,
        sortingKey: 'documentStatus_StatusId',
        searchingKey: 'documentStatusGUID'
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<MessengerJobTableListView>> {
    return this.service.getMessengerJobTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteMessengerJobTable(row));
  }
}
