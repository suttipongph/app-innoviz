import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MessengerJobTableListComponent } from './messenger-job-table-list.component';

describe('MessengerJobTableListViewComponent', () => {
  let component: MessengerJobTableListComponent;
  let fixture: ComponentFixture<MessengerJobTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MessengerJobTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessengerJobTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
