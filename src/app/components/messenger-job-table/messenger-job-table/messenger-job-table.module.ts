import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MessengerJobTableService } from './messenger-job-table.service';
import { MessengerJobTableListComponent } from './messenger-job-table-list/messenger-job-table-list.component';
import { MessengerJobTableItemComponent } from './messenger-job-table-item/messenger-job-table-item.component';
import { MessengerJobTableItemCustomComponent } from './messenger-job-table-item/messenger-job-table-item-custom.component';
import { MessengerJobTableListCustomComponent } from './messenger-job-table-list/messenger-job-table-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [MessengerJobTableListComponent, MessengerJobTableItemComponent],
  providers: [MessengerJobTableService, MessengerJobTableItemCustomComponent, MessengerJobTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [MessengerJobTableListComponent, MessengerJobTableItemComponent]
})
export class MessengerJobTableComponentModule {}
