import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PostMessengerJobView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class PostMessengerJobService {
  serviceKey = 'postMessengerJobGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  deletePostMessengerJob(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeletePostMessengerJob`;
    return this.dataGateway.delete(url, row);
  }
  getPostMessengerJobById(id: string): Observable<PostMessengerJobView> {
    const url = `${this.servicePath}/GetPostMessengerJobById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createPostMessengerJob(vmModel: PostMessengerJobView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreatePostMessengerJob`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updatePostMessengerJob(vmModel: PostMessengerJobView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdatePostMessengerJob`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
