import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostMessengerJobComponent } from './post-messenger-job.component';

describe('PostMessengerJobViewComponent', () => {
  let component: PostMessengerJobComponent;
  let fixture: ComponentFixture<PostMessengerJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PostMessengerJobComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostMessengerJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
