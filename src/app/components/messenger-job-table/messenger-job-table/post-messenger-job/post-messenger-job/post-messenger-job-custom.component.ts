import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { PostMessengerJobView } from 'shared/models/viewModel';

const firstGroup = [
  'MESSENGER_JOB_TABLE_GUID',
  'DOCUMENT_STATUS_GUID',
  'JOB_DATE',
  'JOB_TIME',
  'JOB_TYPE_GUID',
  'MESSENGER_TABLE_GUID',
  'JOB_DETAIL',
  'CONTACT_TO',
  'CONTACT_PERSON_NAME',
  'RESULT',
  'RESULT_REMARK',
  'FEE_AMOUNT',
  'SERVICE_FEE_AMOUNT',
  'JOB_START_TIME',
  'JOB_END_TIME'
];

export class PostMessengerJobCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: PostMessengerJobView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: PostMessengerJobView): Observable<boolean> {
    return of(false);
  }
  getInitialData(): Observable<PostMessengerJobView> {
    const model = new PostMessengerJobView();
    model.postMessengerJobGUID = EmptyGuid;
    return of(model);
  }
}
