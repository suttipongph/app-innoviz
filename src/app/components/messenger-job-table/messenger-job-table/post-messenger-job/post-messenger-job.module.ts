import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PostMessengerJobService } from './post-messenger-job.service';
import { PostMessengerJobComponent } from './post-messenger-job/post-messenger-job.component';
import { PostMessengerJobCustomComponent } from './post-messenger-job/post-messenger-job-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PostMessengerJobComponent],
  providers: [PostMessengerJobService, PostMessengerJobCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PostMessengerJobComponent]
})
export class PostMessengerJobComponentModule {}
