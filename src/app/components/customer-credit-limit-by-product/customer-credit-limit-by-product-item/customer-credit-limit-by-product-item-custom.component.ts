import { Observable, of } from 'rxjs';
import { EmptyGuid, RefType } from 'shared/constants';
import { BaseServiceModel, FieldAccessing, RefTypeModel } from 'shared/models/systemModel';
import { CustomerCreditLimitByProductItemView } from 'shared/models/viewModel';

const firstGroup = ['CREDIT_LIMIT_BALANCE', 'CUSTOMER_TABLE_GUID'];

export class CustomerCreditLimitByProductItemCustomComponent {
  refGUID: string;
  refType: RefType;
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CustomerCreditLimitByProductItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CustomerCreditLimitByProductItemView): Observable<boolean> {
    return of(!canCreate);
  }
  setRefTypeRefGUID(passingObj: RefTypeModel): void {
    this.refType = passingObj.refType;
    this.refGUID = passingObj.refGUID;
  }
}
