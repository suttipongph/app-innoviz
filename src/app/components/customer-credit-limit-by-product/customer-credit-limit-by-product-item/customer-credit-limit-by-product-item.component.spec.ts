import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerCreditLimitByProductItemComponent } from './customer-credit-limit-by-product-item.component';

describe('CustomerCreditLimitByProductItemViewComponent', () => {
  let component: CustomerCreditLimitByProductItemComponent;
  let fixture: ComponentFixture<CustomerCreditLimitByProductItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustomerCreditLimitByProductItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerCreditLimitByProductItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
