import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CustomerCreditLimitByProductItemView, CustomerCreditLimitByProductListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CustomerCreditLimitByProductService {
  serviceKey = 'customerCreditLimitByProductGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCustomerCreditLimitByProductToList(search: SearchParameter): Observable<SearchResult<CustomerCreditLimitByProductListView>> {
    const url = `${this.servicePath}/GetCustomerCreditLimitByProductList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCustomerCreditLimitByProduct(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCustomerCreditLimitByProduct`;
    return this.dataGateway.delete(url, row);
  }
  getCustomerCreditLimitByProductById(id: string): Observable<CustomerCreditLimitByProductItemView> {
    const url = `${this.servicePath}/GetCustomerCreditLimitByProductById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCustomerCreditLimitByProduct(vmModel: CustomerCreditLimitByProductItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCustomerCreditLimitByProduct`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCustomerCreditLimitByProduct(vmModel: CustomerCreditLimitByProductItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCustomerCreditLimitByProduct`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<CustomerCreditLimitByProductItemView> {
    const url = `${this.servicePath}/GetCustomerCreditLimitByProductInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
}
