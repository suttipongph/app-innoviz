import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomerCreditLimitByProductService } from './customer-credit-limit-by-product.service';
import { CustomerCreditLimitByProductListComponent } from './customer-credit-limit-by-product-list/customer-credit-limit-by-product-list.component';
import { CustomerCreditLimitByProductItemComponent } from './customer-credit-limit-by-product-item/customer-credit-limit-by-product-item.component';
import { CustomerCreditLimitByProductItemCustomComponent } from './customer-credit-limit-by-product-item/customer-credit-limit-by-product-item-custom.component';
import { CustomerCreditLimitByProductListCustomComponent } from './customer-credit-limit-by-product-list/customer-credit-limit-by-product-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CustomerCreditLimitByProductListComponent, CustomerCreditLimitByProductItemComponent],
  providers: [CustomerCreditLimitByProductService, CustomerCreditLimitByProductItemCustomComponent, CustomerCreditLimitByProductListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CustomerCreditLimitByProductListComponent, CustomerCreditLimitByProductItemComponent]
})
export class CustomerCreditLimitByProductComponentModule {}
