import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CustomerCreditLimitByProductListComponent } from './customer-credit-limit-by-product-list.component';

describe('CustomerCreditLimitByProductListViewComponent', () => {
  let component: CustomerCreditLimitByProductListComponent;
  let fixture: ComponentFixture<CustomerCreditLimitByProductListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustomerCreditLimitByProductListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerCreditLimitByProductListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
