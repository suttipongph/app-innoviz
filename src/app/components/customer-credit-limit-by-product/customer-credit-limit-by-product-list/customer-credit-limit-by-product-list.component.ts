import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CustomerCreditLimitByProductListView } from 'shared/models/viewModel';
import { CustomerCreditLimitByProductService } from '../customer-credit-limit-by-product.service';
import { CustomerCreditLimitByProductListCustomComponent } from './customer-credit-limit-by-product-list-custom.component';

@Component({
  selector: 'customer-credit-limit-by-product-list',
  templateUrl: './customer-credit-limit-by-product-list.component.html',
  styleUrls: ['./customer-credit-limit-by-product-list.component.scss']
})
export class CustomerCreditLimitByProductListComponent extends BaseListComponent<CustomerCreditLimitByProductListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  productTypeOptions: SelectItems[] = [];

  constructor(
    public custom: CustomerCreditLimitByProductListCustomComponent,
    private service: CustomerCreditLimitByProductService,
    private currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getKey('customertable');
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setRefTypeRefGUID(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'customerCreditLimitByProductGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: 'LABEL.CREDIT_LIMIT',
        textKey: 'creditLimit',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CREDIT_LIMIT_BALANCE',
        textKey: 'creditLimitBalance',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        disabledFilter: true
      },
      {
        label: null,
        textKey: 'customerTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: isNullOrUndefined(this.custom.refGUID) ? this.parentId : this.custom.refGUID
      }
      // ,
      // {
      //   label: null,
      //   textKey: 'customerTableGUID',
      //   type: ColumnType.MASTER,
      //   visibility: true,
      //   sorting: SortType.NONE,
      //   parentKey: this.parentId
      // }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CustomerCreditLimitByProductListView>> {
    return this.service.getCustomerCreditLimitByProductToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCustomerCreditLimitByProduct(row));
  }
}
