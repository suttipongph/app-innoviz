import { Observable } from 'rxjs';
import { RefType } from 'shared/constants';
import { BaseServiceModel, RefTypeModel } from 'shared/models/systemModel';
export class CustomerCreditLimitByProductListCustomComponent {
  refGUID: string;
  refType: RefType;
  baseService: BaseServiceModel<any>;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }

  setRefTypeRefGUID(passingObj: RefTypeModel): void {
    this.refType = passingObj.refType;
    this.refGUID = passingObj.refGUID;
  }
}
