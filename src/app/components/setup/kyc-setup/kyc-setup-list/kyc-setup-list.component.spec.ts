import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { KYCListComponent } from './kyc-setup-list.component';

describe('KYCListViewComponent', () => {
  let component: KYCListComponent;
  let fixture: ComponentFixture<KYCListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KYCListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KYCListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
