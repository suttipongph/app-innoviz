import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { KYCSetupItemView } from 'shared/models/viewModel';

const firstGroup = ['KYC_ID'];

export class KYCSetupItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: KYCSetupItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.kycSetupGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: KYCSetupItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<KYCSetupItemView> {
    let model: KYCSetupItemView = new KYCSetupItemView();
    model.kycSetupGUID = EmptyGuid;
    return of(model);
  }
}
