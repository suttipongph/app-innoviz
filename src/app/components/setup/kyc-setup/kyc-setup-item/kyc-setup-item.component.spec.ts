import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KYCSetupItemComponent } from './kyc-setup-item.component';

describe('KYCSetupItemViewComponent', () => {
  let component: KYCSetupItemComponent;
  let fixture: ComponentFixture<KYCSetupItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KYCSetupItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KYCSetupItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
