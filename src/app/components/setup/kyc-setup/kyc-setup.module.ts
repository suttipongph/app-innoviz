import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { KYCSetupService } from './kyc-setup.service';
import { KYCSetupListComponent } from './kyc-setup-list/kyc-setup-list.component';
import { KYCSetupItemComponent } from './kyc-setup-item/kyc-setup-item.component';
import { KYCSetupItemCustomComponent } from './kyc-setup-item/kyc-setup-item-custom.component';
import { KYCSetupListCustomComponent } from './kyc-setup-list/kyc-setup-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [KYCSetupListComponent, KYCSetupItemComponent],
  providers: [KYCSetupService, KYCSetupItemCustomComponent, KYCSetupListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [KYCSetupListComponent, KYCSetupItemComponent]
})
export class KYCSetupComponentModule {}
