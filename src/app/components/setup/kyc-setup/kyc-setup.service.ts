import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { KYCSetupItemView, KYCSetupListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class KYCSetupService {
  serviceKey = 'kycSetupGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getKYCSetupToList(search: SearchParameter): Observable<SearchResult<KYCSetupListView>> {
    const url = `${this.servicePath}/GetKYCSetupList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteKYCSetup(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteKYCSetup`;
    return this.dataGateway.delete(url, row);
  }
  getKYCSetupById(id: string): Observable<KYCSetupItemView> {
    const url = `${this.servicePath}/GetKYCSetupById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createKYCSetup(vmModel: KYCSetupItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateKYCSetup`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateKYCSetup(vmModel: KYCSetupItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateKYCSetup`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
