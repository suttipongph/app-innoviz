import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { LedgerFiscalPeriodItemView, LedgerFiscalPeriodListView } from 'shared/models/viewModel';

@Injectable()
export class LedgerFiscalPeriodService {
  serviceKey = 'ledgerFiscalPeriodGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getLedgerFiscalPeriodToList(search: SearchParameter): Observable<SearchResult<LedgerFiscalPeriodListView>> {
    const url = `${this.servicePath}/GetLedgerFiscalPeriodList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteLedgerFiscalPeriod(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteLedgerFiscalPeriod`;
    return this.dataGateway.delete(url, row);
  }
  getLedgerFiscalPeriodById(id: string): Observable<LedgerFiscalPeriodItemView> {
    const url = `${this.servicePath}/GetLedgerFiscalPeriodById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createLedgerFiscalPeriod(vmModel: LedgerFiscalPeriodItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateLedgerFiscalPeriod`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateLedgerFiscalPeriod(vmModel: LedgerFiscalPeriodItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateLedgerFiscalPeriod`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<LedgerFiscalPeriodItemView> {
    const url = `${this.servicePath}/GetInitialLedgerFiscalYearData/id=${id}`;
    return this.dataGateway.get(url);
  }
}
