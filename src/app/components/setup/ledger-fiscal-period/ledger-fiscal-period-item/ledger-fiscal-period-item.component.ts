import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { LedgerFiscalPeriodItemView } from 'shared/models/viewModel';
import { LedgerFiscalPeriodService } from '../ledger-fiscal-period.service';
import { LedgerFiscalPeriodItemCustomComponent } from './ledger-fiscal-period-item-custom.component';
@Component({
  selector: 'ledger-fiscal-period-item',
  templateUrl: './ledger-fiscal-period-item.component.html',
  styleUrls: ['./ledger-fiscal-period-item.component.scss']
})
export class LedgerFiscalPeriodItemComponent extends BaseItemComponent<LedgerFiscalPeriodItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  ledgerFiscalPeriodStatusOptions: SelectItems[] = [];
  ledgerFiscalYearOptions: SelectItems[] = [];
  constructor(
    private service: LedgerFiscalPeriodService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: LedgerFiscalPeriodItemCustomComponent
  ) {
    super();
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiService.getKey('ledgerfiscalyear');
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getLedgerFiscalPeriodStatusDropDown(this.ledgerFiscalPeriodStatusOptions);
  }
  getById(): Observable<LedgerFiscalPeriodItemView> {
    return this.service.getLedgerFiscalPeriodById(this.id);
  }
  getInitialData(): Observable<LedgerFiscalPeriodItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }
  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getLedgerFiscalYearDropDown()).subscribe(
      ([ledgerFiscalYear]) => {
        this.ledgerFiscalYearOptions = ledgerFiscalYear;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {}
  setFunctionOptions(): void {}
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateLedgerFiscalPeriod(this.model), isColsing);
    } else {
      super.onCreate(this.service.createLedgerFiscalPeriod(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
