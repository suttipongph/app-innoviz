import { Observable, of } from 'rxjs';
import { isUpdateMode } from 'shared/functions/value.function';
import { FieldAccessing } from 'shared/models/systemModel';
import { LedgerFiscalPeriodItemView } from 'shared/models/viewModel';

const firstGroup = ['FISCAL_YEAR_ID', 'START_DATE', 'END_DATE'];

export class LedgerFiscalPeriodItemCustomComponent {
  getFieldAccessing(model: LedgerFiscalPeriodItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.ledgerFiscalPeriodGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: LedgerFiscalPeriodItemView): Observable<boolean> {
    return of(!canCreate);
  }
}
