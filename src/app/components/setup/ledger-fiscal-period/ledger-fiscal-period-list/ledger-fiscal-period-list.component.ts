import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { LedgerFiscalPeriodListView } from 'shared/models/viewModel';
import { LedgerFiscalPeriodService } from '../ledger-fiscal-period.service';
import { LedgerFiscalPeriodListCustomComponent } from './ledger-fiscal-period-list-custom.component';

@Component({
  selector: 'ledger-fiscal-period-list',
  templateUrl: './ledger-fiscal-period-list.component.html',
  styleUrls: ['./ledger-fiscal-period-list.component.scss']
})
export class LedgerFiscalPeriodListComponent extends BaseListComponent<LedgerFiscalPeriodListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  ledgerFiscalPeriodStatusOptions: SelectItems[] = [];
  constructor(private service: LedgerFiscalPeriodService, public custom: LedgerFiscalPeriodListCustomComponent) {
    super();
    this.parentId = this.uiService.getKey('ledgerfiscalyear');
  }
  ngOnInit(): void {
    this.accessRightChildPage = 'LedgerFiscalPeriodListPage';
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    // super.checkAccessMode(this.accessService.getAccessRight());
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onEnumLoader(): void {
    this.setDataGridOption();
    this.baseDropdown.getLedgerFiscalPeriodStatusDropDown(this.ledgerFiscalPeriodStatusOptions);
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'ledgerFiscalPeriodGUID';
    // this.option.canCreate = false;
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.START_DATE',
        textKey: 'startDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.END_DATE',
        textKey: 'endDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PERIOD_STATUS',
        textKey: 'periodStatus',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.ledgerFiscalPeriodStatusOptions
      },
      {
        label: null,
        textKey: 'ledgerFiscalYearGUID',
        type: ColumnType.MASTER,
        visibility: false,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<LedgerFiscalPeriodListView>> {
    return this.service.getLedgerFiscalPeriodToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteLedgerFiscalPeriod(row));
  }
}
