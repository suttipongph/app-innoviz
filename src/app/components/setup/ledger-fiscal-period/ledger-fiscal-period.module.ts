import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LedgerFiscalPeriodService } from './ledger-fiscal-period.service';
import { LedgerFiscalPeriodListComponent } from './ledger-fiscal-period-list/ledger-fiscal-period-list.component';
import { LedgerFiscalPeriodItemComponent } from './ledger-fiscal-period-item/ledger-fiscal-period-item.component';
import { LedgerFiscalPeriodItemCustomComponent } from './ledger-fiscal-period-item/ledger-fiscal-period-item-custom.component';
import { LedgerFiscalPeriodListCustomComponent } from './ledger-fiscal-period-list/ledger-fiscal-period-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [LedgerFiscalPeriodListComponent, LedgerFiscalPeriodItemComponent],
  providers: [LedgerFiscalPeriodService, LedgerFiscalPeriodItemCustomComponent, LedgerFiscalPeriodListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [LedgerFiscalPeriodListComponent, LedgerFiscalPeriodItemComponent]
})
export class LedgerFiscalPeriodComponentModule {}
