import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { EmployeeTableItemView, EmployeeTableListView } from 'shared/models/viewModel';

@Injectable()
export class EmployeeTableService {
  serviceKey = 'employeeTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getEmployeeTableToList(search: SearchParameter): Observable<SearchResult<EmployeeTableListView>> {
    const url = `${this.servicePath}/GetEmployeeTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteEmployeeTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteEmployeeTable`;
    return this.dataGateway.delete(url, row);
  }
  getEmployeeTableById(id: string): Observable<EmployeeTableItemView> {
    const url = `${this.servicePath}/GetEmployeeTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createEmployeeTable(vmModel: EmployeeTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateEmployeeTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateEmployeeTable(vmModel: EmployeeTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateEmployeeTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  validateIsManualNumberSeq(companyId: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeq/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
}
