import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EmployeeTableService } from './employee-table.service';
import { EmployeeTableListComponent } from './employee-table-list/employee-table-list.component';
import { EmployeeTableItemComponent } from './employee-table-item/employee-table-item.component';
import { EmployeeTableItemCustomComponent } from './employee-table-item/employee-table-item-custom.component';
import { EmployeeTableListCustomComponent } from './employee-table-list/employee-table-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [EmployeeTableListComponent, EmployeeTableItemComponent],
  providers: [EmployeeTableService, EmployeeTableItemCustomComponent, EmployeeTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [EmployeeTableListComponent, EmployeeTableItemComponent]
})
export class EmployeeTableComponentModule {}
