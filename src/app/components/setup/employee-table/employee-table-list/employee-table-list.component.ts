import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, EMPLOYEE_TABLE, EMPL_TEAM, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { EmployeeTableListView } from 'shared/models/viewModel';
import { EmployeeTableService } from '../employee-table.service';
import { EmployeeTableListCustomComponent } from './employee-table-list-custom.component';

@Component({
  selector: 'employee-table-list',
  templateUrl: './employee-table-list.component.html',
  styleUrls: ['./employee-table-list.component.scss']
})
export class EmployeeTableListComponent extends BaseListComponent<EmployeeTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  departmentOptions: SelectItems[] = [];
  defaultBitOptions: SelectItems[] = [];
  businessUnitOptions: SelectItems[] = [];
  constructor(private service: EmployeeTableService, public custom: EmployeeTableListCustomComponent) {
    super();
  }
  ngOnInit(): void { }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void { }
  checkPageMode(): void { }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getDepartmentToDropDown(this.departmentOptions);
    this.baseDropdown.getDefaultBitDropDown(this.defaultBitOptions);
    this.baseDropdown.getBusinessUnitToDropDown(this.businessUnitOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'employeeTableGUID';
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    const columns: ColumnModel[] = [
      { label: 'LABEL.EMPLOYEE_ID', textKey: EMPLOYEE_TABLE.PK, type: ColumnType.STRING, visibility: true, sorting: SortType.NONE },
      { label: 'LABEL.NAME', textKey: 'name', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE },
      {
        label: 'LABEL.TEAM_ID',
        textKey: EMPL_TEAM.FK,
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DEPARTMENT_ID',
        textKey: 'department_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.departmentOptions,
        searchingKey: 'departmentGUID',
        sortingKey: 'department_DepartmentId'
      },
      {
        label: 'LABEL.BUSINESS_UNIT_ID',
        textKey: 'businessUnit_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.businessUnitOptions,
        searchingKey: 'businessUnitGUID',
        sortingKey: 'businessUnit_BusinessUnitId'
      },
      {
        label: 'LABEL.ASSIST_MD',
        textKey: 'assistMD',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defaultBitOptions
      },
      {
        label: 'LABEL.IN_ACTIVE',
        textKey: 'inActive',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defaultBitOptions
      },
      { label: 'LABEL.USER_NAME', textKey: 'sysuserTable_UserName', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<EmployeeTableListView>> {
    return this.service.getEmployeeTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteEmployeeTable(row));
  }
}
