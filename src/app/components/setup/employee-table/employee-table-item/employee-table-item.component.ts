import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { IVZFileUploadComponent } from 'shared/components/ivz-file-upload/ivz-file-upload.component';
import { Dimension, EmptyGuid, ROUTE_RELATED_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmpty, isUndefinedOrZeroLength, isUpdateMode } from 'shared/functions/value.function';
import { FileInformation, FileUpload, FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { EmployeeTableItemView, LedgerDimensionItemView } from 'shared/models/viewModel';
import { EmployeeTableService } from '../employee-table.service';
import { EmployeeTableItemCustomComponent } from './employee-table-item-custom.component';
@Component({
  selector: 'employee-table-item',
  templateUrl: './employee-table-item.component.html',
  styleUrls: ['./employee-table-item.component.scss']
})
export class EmployeeTableItemComponent extends BaseItemComponent<EmployeeTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  departmentOptions: SelectItems[] = [];
  emplTeamOptions: SelectItems[] = [];
  businessUnitOptions: SelectItems[] = [];
  reportToEmployeeOptions: SelectItems[] = [];
  dimension1Options: SelectItems[] = [];
  dimension2Options: SelectItems[] = [];
  dimension3Options: SelectItems[] = [];
  dimension4Options: SelectItems[] = [];
  dimension5Options: SelectItems[] = [];

  // file
  @ViewChild('fileUploadCmpnt') fileComponent: IVZFileUploadComponent;
  fileUpload: FileUpload = { fileInfos: [] };
  allowBrowse: boolean = true;
  requireId: boolean = true;
  constructor(private service: EmployeeTableService, private currentActivatedRoute: ActivatedRoute, public custom: EmployeeTableItemCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {
    this.fileDirty = false;
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<EmployeeTableItemView> {
    return this.service.getEmployeeTableById(this.id);
  }
  getInitialData(): Observable<EmployeeTableItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }
  onAsyncRunner(model?: any): void {
    forkJoin(
      this.baseDropdown.getDepartmentToDropDown(),
      this.baseDropdown.getEmplTeamToDropDown(),
      this.baseDropdown.getLedgerDimensionToDropDown(),
      this.baseDropdown.getBusinessUnitToDropDown(),
      this.custom.getEmployeeTableToDropDown(model)
    ).subscribe(
      ([department, emplTeam, ledgerDimension, businessUnit, reportToEmployee]) => {
        this.departmentOptions = department;
        this.emplTeamOptions = emplTeam;
        this.businessUnitOptions = businessUnit;
        this.reportToEmployeeOptions = reportToEmployee;
        this.dimension1Options = ledgerDimension.filter((t) => (t.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension1);
        this.dimension2Options = ledgerDimension.filter((t) => (t.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension2);
        this.dimension3Options = ledgerDimension.filter((t) => (t.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension3);
        this.dimension4Options = ledgerDimension.filter((t) => (t.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension4);
        this.dimension5Options = ledgerDimension.filter((t) => (t.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension5);
        if (!isNullOrUndefined(model)) {
          this.model = model;
          this.custom.validateAssistChanged(this.model.assistMD);
          this.getFileUpload();
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model, this.service).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.MANAGE',
        items: [
          {
            label: 'LABEL.CONTACT',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.CONTACT })
          }
        ]
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(): void {}
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    this.setFileUpload();
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateEmployeeTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createEmployeeTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onFileChange(e: FileUpload) {
    this.fileDirty = true;
  }
  getFileUpload() {
    if (!isNullOrUndefOrEmpty(this.model.signature)) {
      let signature = new FileInformation();
      this.fileUpload.fileInfos = [];

      signature.base64 = this.model.signature;
      signature.isRemovable = false;
      signature.isPreviewable = true;
      signature.contentType = 'data:image/jpeg;base64,';
      signature.fileName = 'signature.jpg';
      signature.fileDisplayName = 'signature.jpg';

      this.fileUpload.fileInfos.push(signature);
      this.fileComponent.setFileFromExternal(signature);
    }
  }
  setFileUpload() {
    if (isUndefinedOrZeroLength(this.fileUpload.fileInfos) || isUndefinedOrZeroLength(this.fileUpload.fileInfos[0])) {
      return;
    }

    let file = this.fileUpload.fileInfos[0].base64.split(',');
    this.model.signature = file[file.length - 1];
  }
  onAssistChange(isAssist: boolean) {
    this.custom.validateAssistChanged(isAssist);
  }
}
