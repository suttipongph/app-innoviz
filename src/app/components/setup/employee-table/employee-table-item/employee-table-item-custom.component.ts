import { AppInjector } from 'app-injector';
import { NotificationService } from 'core/services/notification.service';
import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isNullOrUndefined, isNullOrUndefOrEmpty, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { EmployeeTableItemView } from 'shared/models/viewModel';
import { EmployeeTableService } from '../employee-table.service';

const firstGroup = ['EMPLOYEE_ID'];
const secoundGroup = ['USER_NAME'];
export class EmployeeTableItemCustomComponent {
  baseService: BaseServiceModel<EmployeeTableService>;
  isManunal: boolean = false;
  isAssist = false;
  notificationService = AppInjector.get(NotificationService);
  getFieldAccessing(model: EmployeeTableItemView, employeeTableService: EmployeeTableService): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    return new Observable((observer) => {
      fieldAccessing.push({ filedIds: secoundGroup, readonly: true });
      if (!isUpdateMode(model.employeeTableGUID)) {
        this.validateIsManualNumberSeq(employeeTableService, model.companyGUID).subscribe(
          (result) => {
            this.isManunal = result;
            fieldAccessing.push({ filedIds: firstGroup, readonly: !this.isManunal });
            observer.next(fieldAccessing);
          },
          (error) => {
            this.notificationService.showErrorMessageFromResponse(error);
          }
        );
      } else {
        fieldAccessing.push({ filedIds: firstGroup, readonly: true });
        observer.next(fieldAccessing);
      }
    });
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: EmployeeTableItemView): Observable<boolean> {
    return of(!canCreate);
  }
  validateIsManualNumberSeq(employeeTableService: EmployeeTableService, companyId: string): Observable<boolean> {
    return employeeTableService.validateIsManualNumberSeq(companyId);
  }
  getInitialData(): Observable<EmployeeTableItemView> {
    let model: EmployeeTableItemView = new EmployeeTableItemView();
    model.employeeTableGUID = EmptyGuid;
    return of(model);
  }
  getEmployeeTableToDropDown(model: EmployeeTableItemView): Observable<SelectItems[]> {
    if (isNullOrUndefined(model)) {
      return this.baseService.baseDropdown.getEmployeeTableDropDown();
    } else {
      return this.baseService.baseDropdown.getEmployeeTableByNotCurrentEmpDropDown(model.employeeTableGUID);
    }
  }
  validateAssistChanged(isAssist: boolean) {
    this.isAssist = isAssist;
  }
}
