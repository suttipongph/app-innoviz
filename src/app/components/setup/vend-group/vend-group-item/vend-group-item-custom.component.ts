import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { VendGroupItemView } from 'shared/models/viewModel';

const firstGroup = ['VEND_GROUP_ID'];
export class VendGroupItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: VendGroupItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.vendGroupGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: VendGroupItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<VendGroupItemView> {
    let model: VendGroupItemView = new VendGroupItemView();
    model.vendGroupGUID = EmptyGuid;
    return of(model);
  }
}
