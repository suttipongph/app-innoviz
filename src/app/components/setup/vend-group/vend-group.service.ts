import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { VendGroupItemView, VendGroupListView } from 'shared/models/viewModel';

@Injectable()
export class VendGroupService {
  serviceKey = 'vendGroupGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getVendGroupToList(search: SearchParameter): Observable<SearchResult<VendGroupListView>> {
    const url = `${this.servicePath}/GetVendGroupList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteVendGroup(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteVendGroup`;
    return this.dataGateway.delete(url, row);
  }
  getVendGroupById(id: string): Observable<VendGroupItemView> {
    const url = `${this.servicePath}/GetVendGroupById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createVendGroup(vmModel: VendGroupItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateVendGroup`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateVendGroup(vmModel: VendGroupItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateVendGroup`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
