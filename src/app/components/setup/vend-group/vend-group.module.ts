import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VendGroupService } from './vend-group.service';
import { VendGroupListComponent } from './vend-group-list/vend-group-list.component';
import { VendGroupItemComponent } from './vend-group-item/vend-group-item.component';
import { VendGroupItemCustomComponent } from './vend-group-item/vend-group-item-custom.component';
import { VendGroupListCustomComponent } from './vend-group-list/vend-group-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [VendGroupListComponent, VendGroupItemComponent],
  providers: [VendGroupService, VendGroupItemCustomComponent, VendGroupListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [VendGroupListComponent, VendGroupItemComponent]
})
export class VendGroupComponentModule {}
