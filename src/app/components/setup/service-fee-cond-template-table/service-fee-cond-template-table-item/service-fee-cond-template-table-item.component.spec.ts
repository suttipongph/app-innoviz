import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceFeeCondTemplateTableItemComponent } from './service-fee-cond-template-table-item.component';

describe('ServiceFeeCondTemplateTableItemViewComponent', () => {
  let component: ServiceFeeCondTemplateTableItemComponent;
  let fixture: ComponentFixture<ServiceFeeCondTemplateTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ServiceFeeCondTemplateTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceFeeCondTemplateTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
