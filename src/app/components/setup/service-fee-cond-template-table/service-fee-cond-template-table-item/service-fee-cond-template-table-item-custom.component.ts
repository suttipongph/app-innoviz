import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { ServiceFeeCondTemplateTableItemView } from 'shared/models/viewModel';

const firstGroup = ['SERVICE_FEE_COND_TEMPLATE_ID'];
const condition1 = ['PRODUCT_TYPE'];

export class ServiceFeeCondTemplateTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: ServiceFeeCondTemplateTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.serviceFeeCondTemplateTableGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return this.validateIsServiceFeeCondTemplateLine(model.serviceFeeCondTemplateTableGUID).pipe(
      map((result) => {
        fieldAccessing.push({ filedIds: condition1, readonly: result });
        return fieldAccessing;
      })
    );
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ServiceFeeCondTemplateTableItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<ServiceFeeCondTemplateTableItemView> {
    let model: ServiceFeeCondTemplateTableItemView = new ServiceFeeCondTemplateTableItemView();
    model.serviceFeeCondTemplateTableGUID = EmptyGuid;
    return of(model);
  }
  validateIsServiceFeeCondTemplateLine(serviceFeeCondTemplateTableGUID: string): Observable<boolean> {
    return this.baseService.service.validateIsServiceFeeCondTemplateLine(serviceFeeCondTemplateTableGUID).pipe(
      map((res) => res),
      catchError((e) => {
        this.baseService.notificationService.showErrorMessageFromResponse(e);
        return e;
      })
    );
  }
}
