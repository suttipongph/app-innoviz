import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServiceFeeCondTemplateTableService } from './service-fee-cond-template-table.service';
import { ServiceFeeCondTemplateTableListComponent } from './service-fee-cond-template-table-list/service-fee-cond-template-table-list.component';
import { ServiceFeeCondTemplateTableItemComponent } from './service-fee-cond-template-table-item/service-fee-cond-template-table-item.component';
import { ServiceFeeCondTemplateTableItemCustomComponent } from './service-fee-cond-template-table-item/service-fee-cond-template-table-item-custom.component';
import { ServiceFeeCondTemplateTableListCustomComponent } from './service-fee-cond-template-table-list/service-fee-cond-template-table-list-custom.component';
import { ServiceFeeCondTemplateLineComponentModule } from '../service-fee-cond-template-line/service-fee-cond-template-line.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, ServiceFeeCondTemplateLineComponentModule],
  declarations: [ServiceFeeCondTemplateTableListComponent, ServiceFeeCondTemplateTableItemComponent],
  providers: [ServiceFeeCondTemplateTableService, ServiceFeeCondTemplateTableItemCustomComponent, ServiceFeeCondTemplateTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ServiceFeeCondTemplateTableListComponent, ServiceFeeCondTemplateTableItemComponent]
})
export class ServiceFeeCondTemplateTableComponentModule {}
