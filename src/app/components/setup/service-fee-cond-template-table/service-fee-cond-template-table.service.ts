import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { ServiceFeeCondTemplateTableItemView, ServiceFeeCondTemplateTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ServiceFeeCondTemplateTableService {
  serviceKey = 'serviceFeeCondTemplateTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getServiceFeeCondTemplateTableToList(search: SearchParameter): Observable<SearchResult<ServiceFeeCondTemplateTableListView>> {
    const url = `${this.servicePath}/GetServiceFeeCondTemplateTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteServiceFeeCondTemplateTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteServiceFeeCondTemplateTable`;
    return this.dataGateway.delete(url, row);
  }
  getServiceFeeCondTemplateTableById(id: string): Observable<ServiceFeeCondTemplateTableItemView> {
    const url = `${this.servicePath}/GetServiceFeeCondTemplateTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createServiceFeeCondTemplateTable(vmModel: ServiceFeeCondTemplateTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateServiceFeeCondTemplateTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateServiceFeeCondTemplateTable(vmModel: ServiceFeeCondTemplateTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateServiceFeeCondTemplateTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  validateIsServiceFeeCondTemplateLine(serviceFeeCondTemplateTableGUID: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsServiceFeeCondTemplateLine/serviceFeeCondTemplateTableGUID=${serviceFeeCondTemplateTableGUID}`;
    return this.dataGateway.get(url);
  }
}
