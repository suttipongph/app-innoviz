import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ServiceFeeCondTemplateTableListComponent } from './service-fee-cond-template-table-list.component';

describe('ServiceFeeCondTemplateTableListViewComponent', () => {
  let component: ServiceFeeCondTemplateTableListComponent;
  let fixture: ComponentFixture<ServiceFeeCondTemplateTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ServiceFeeCondTemplateTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceFeeCondTemplateTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
