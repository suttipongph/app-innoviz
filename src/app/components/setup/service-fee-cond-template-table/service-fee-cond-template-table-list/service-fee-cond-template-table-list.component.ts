import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { ServiceFeeCondTemplateTableListView } from 'shared/models/viewModel';
import { ServiceFeeCondTemplateTableService } from '../service-fee-cond-template-table.service';
import { ServiceFeeCondTemplateTableListCustomComponent } from './service-fee-cond-template-table-list-custom.component';

@Component({
  selector: 'service-fee-cond-template-table-list',
  templateUrl: './service-fee-cond-template-table-list.component.html',
  styleUrls: ['./service-fee-cond-template-table-list.component.scss']
})
export class ServiceFeeCondTemplateTableListComponent extends BaseListComponent<ServiceFeeCondTemplateTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  productTypeOptions: SelectItems[] = [];

  constructor(public custom: ServiceFeeCondTemplateTableListCustomComponent, private service: ServiceFeeCondTemplateTableService) {
    super();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'serviceFeeCondTemplateTableGUID';
    const columns: ColumnModel[] = [
      {
        label: this.translate.instant('LABEL.PRODUCT_TYPE'),
        textKey: 'productType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: this.translate.instant('LABEL.SERVICE_FEE_CONDITION_TEMPLATE_ID'),
        textKey: 'serviceFeeCondTemplateId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: this.translate.instant('LABEL.DESCRIPTION'),
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<ServiceFeeCondTemplateTableListView>> {
    return this.service.getServiceFeeCondTemplateTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteServiceFeeCondTemplateTable(row));
  }
}
