import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { LedgerDimensionItemView, LedgerDimensionListView } from 'shared/models/viewModel';

@Injectable()
export class LedgerDimensionService {
  serviceKey = 'ledgerDimensionGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getLedgerDimensionToList(search: SearchParameter): Observable<SearchResult<LedgerDimensionListView>> {
    const url = `${this.servicePath}/GetLedgerDimensionList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteLedgerDimension(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteLedgerDimension`;
    return this.dataGateway.delete(url, row);
  }
  getLedgerDimensionById(id: string): Observable<LedgerDimensionItemView> {
    const url = `${this.servicePath}/GetLedgerDimensionById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createLedgerDimension(vmModel: LedgerDimensionItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateLedgerDimension`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateLedgerDimension(vmModel: LedgerDimensionItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateLedgerDimension`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
