import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { FieldAccessing } from 'shared/models/systemModel';
import { LedgerDimensionItemView } from 'shared/models/viewModel';

const firstGroup = ['DIMENSION', 'DIMENSION_CODE'];
export class LedgerDimensionItemCustomComponent {
  getFieldAccessing(model: LedgerDimensionItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.ledgerDimensionGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: LedgerDimensionItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<LedgerDimensionItemView> {
    let model: LedgerDimensionItemView = new LedgerDimensionItemView();
    model.ledgerDimensionGUID = EmptyGuid;
    return of(model);
  }
}
