import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LedgerDimensionService } from './ledger-dimension.service';
import { LedgerDimensionListComponent } from './ledger-dimension-list/ledger-dimension-list.component';
import { LedgerDimensionItemComponent } from './ledger-dimension-item/ledger-dimension-item.component';
import { LedgerDimensionItemCustomComponent } from './ledger-dimension-item/ledger-dimension-item-custom.component';
import { LedgerDimensionListCustomComponent } from './ledger-dimension-list/ledger-dimension-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [LedgerDimensionListComponent, LedgerDimensionItemComponent],
  providers: [LedgerDimensionService, LedgerDimensionItemCustomComponent, LedgerDimensionListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [LedgerDimensionListComponent, LedgerDimensionItemComponent]
})
export class LedgerDimensionComponentModule {}
