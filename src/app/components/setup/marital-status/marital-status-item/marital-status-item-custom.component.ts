import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { MaritalStatusItemView } from 'shared/models/viewModel';

const firstGroup = ['MARITAL_STATUS_ID'];
export class MaritalStatusItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: MaritalStatusItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.maritalStatusGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: MaritalStatusItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<MaritalStatusItemView> {
    let model: MaritalStatusItemView = new MaritalStatusItemView();
    model.maritalStatusGUID = EmptyGuid;
    return of(model);
  }
}
