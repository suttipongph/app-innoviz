import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaritalStatusService } from './marital-status.service';
import { MaritalStatusListComponent } from './marital-status-list/marital-status-list.component';
import { MaritalStatusItemComponent } from './marital-status-item/marital-status-item.component';
import { MaritalStatusItemCustomComponent } from './marital-status-item/marital-status-item-custom.component';
import { MaritalStatusListCustomComponent } from './marital-status-list/marital-status-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [MaritalStatusListComponent, MaritalStatusItemComponent],
  providers: [MaritalStatusService, MaritalStatusItemCustomComponent, MaritalStatusListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [MaritalStatusListComponent, MaritalStatusItemComponent]
})
export class MaritalStatusComponentModule {}
