import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { MaritalStatusItemView, MaritalStatusListView } from 'shared/models/viewModel';

@Injectable()
export class MaritalStatusService {
  serviceKey = 'maritalStatusGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getMaritalStatusToList(search: SearchParameter): Observable<SearchResult<MaritalStatusListView>> {
    const url = `${this.servicePath}/GetMaritalStatusList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteMaritalStatus(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteMaritalStatus`;
    return this.dataGateway.delete(url, row);
  }
  getMaritalStatusById(id: string): Observable<MaritalStatusItemView> {
    const url = `${this.servicePath}/GetMaritalStatusById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createMaritalStatus(vmModel: MaritalStatusItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateMaritalStatus`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateMaritalStatus(vmModel: MaritalStatusItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateMaritalStatus`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
