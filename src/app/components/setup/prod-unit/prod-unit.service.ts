import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { ProdUnitItemView, ProdUnitListView } from 'shared/models/viewModel';

@Injectable()
export class ProdUnitService {
  serviceKey = 'prodUnitGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getProdUnitToList(search: SearchParameter): Observable<SearchResult<ProdUnitListView>> {
    const url = `${this.servicePath}/GetProdUnitList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteProdUnit(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteProdUnit`;
    return this.dataGateway.delete(url, row);
  }
  getProdUnitById(id: string): Observable<ProdUnitItemView> {
    const url = `${this.servicePath}/GetProdUnitById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createProdUnit(vmModel: ProdUnitItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateProdUnit`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateProdUnit(vmModel: ProdUnitItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateProdUnit`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
