import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProdUnitService } from './prod-unit.service';
import { ProdUnitListComponent } from './prod-unit-list/prod-unit-list.component';
import { ProdUnitItemComponent } from './prod-unit-item/prod-unit-item.component';
import { ProdUnitItemCustomComponent } from './prod-unit-item/prod-unit-item-custom.component';
import { ProdUnitListCustomComponent } from './prod-unit-list/prod-unit-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ProdUnitListComponent, ProdUnitItemComponent],
  providers: [ProdUnitService, ProdUnitItemCustomComponent, ProdUnitListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ProdUnitListComponent, ProdUnitItemComponent]
})
export class ProdUnitComponentModule {}
