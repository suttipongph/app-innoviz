import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { FieldAccessing } from 'shared/models/systemModel';
import { ProdUnitItemView } from 'shared/models/viewModel';

const firstGroup = ['UNIT_ID'];
export class ProdUnitItemCustomComponent {
  getFieldAccessing(model: ProdUnitItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.prodUnitGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ProdUnitItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<ProdUnitItemView> {
    let model: ProdUnitItemView = new ProdUnitItemView();
    model.prodUnitGUID = EmptyGuid;
    return of(model);
  }
}
