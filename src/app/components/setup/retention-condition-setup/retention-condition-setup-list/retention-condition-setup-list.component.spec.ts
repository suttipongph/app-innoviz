import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RetentionConditionSetupListComponent } from './retention-condition-setup-list.component';

describe('RetentionConditionSetupListViewComponent', () => {
  let component: RetentionConditionSetupListComponent;
  let fixture: ComponentFixture<RetentionConditionSetupListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RetentionConditionSetupListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetentionConditionSetupListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
