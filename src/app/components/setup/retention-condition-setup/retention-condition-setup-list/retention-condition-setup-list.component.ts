import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { RetentionConditionSetupListView } from 'shared/models/viewModel';
import { RetentionConditionSetupService } from '../retention-condition-setup.service';
import { RetentionConditionSetupListCustomComponent } from './retention-condition-setup-list-custom.component';

@Component({
  selector: 'retention-condition-setup-list',
  templateUrl: './retention-condition-setup-list.component.html',
  styleUrls: ['./retention-condition-setup-list.component.scss']
})
export class RetentionConditionSetupListComponent extends BaseListComponent<RetentionConditionSetupListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  productTypeOptions: SelectItems[] = [];
  retentionCalculateBaseOptions: SelectItems[] = [];
  retentionDeductionMethodOptions: SelectItems[] = [];

  constructor(public custom: RetentionConditionSetupListCustomComponent, private service: RetentionConditionSetupService) {
    super();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRetentionCalculateBaseEnumDropDown(this.retentionCalculateBaseOptions);
    this.baseDropdown.getRetentionDeductionMethodEnumDropDown(this.retentionDeductionMethodOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = false;
    this.option.canDelete = false;
    this.option.key = 'retentionConditionSetupGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: 'LABEL.RETENTION_DEDUCTION_METHOD',
        textKey: 'retentionDeductionMethod',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.retentionDeductionMethodOptions
      },
      {
        label: 'LABEL.RETENTION_CALCULATE_BASE',
        textKey: 'retentionCalculateBase',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.retentionCalculateBaseOptions
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<RetentionConditionSetupListView>> {
    return this.service.getRetentionConditionSetupToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteRetentionConditionSetup(row));
  }
}
