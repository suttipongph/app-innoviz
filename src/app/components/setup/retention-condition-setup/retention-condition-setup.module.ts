import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RetentionConditionSetupService } from './retention-condition-setup.service';
import { RetentionConditionSetupListComponent } from './retention-condition-setup-list/retention-condition-setup-list.component';
import { RetentionConditionSetupItemComponent } from './retention-condition-setup-item/retention-condition-setup-item.component';
import { RetentionConditionSetupItemCustomComponent } from './retention-condition-setup-item/retention-condition-setup-item-custom.component';
import { RetentionConditionSetupListCustomComponent } from './retention-condition-setup-list/retention-condition-setup-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [RetentionConditionSetupListComponent, RetentionConditionSetupItemComponent],
  providers: [RetentionConditionSetupService, RetentionConditionSetupItemCustomComponent, RetentionConditionSetupListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [RetentionConditionSetupListComponent, RetentionConditionSetupItemComponent]
})
export class RetentionConditionSetupComponentModule {}
