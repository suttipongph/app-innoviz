import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { RetentionConditionSetupItemView, RetentionConditionSetupListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class RetentionConditionSetupService {
  serviceKey = 'retentionConditionSetupGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getRetentionConditionSetupToList(search: SearchParameter): Observable<SearchResult<RetentionConditionSetupListView>> {
    const url = `${this.servicePath}/GetRetentionConditionSetupList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteRetentionConditionSetup(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteRetentionConditionSetup`;
    return this.dataGateway.delete(url, row);
  }
  getRetentionConditionSetupById(id: string): Observable<RetentionConditionSetupItemView> {
    const url = `${this.servicePath}/GetRetentionConditionSetupById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createRetentionConditionSetup(vmModel: RetentionConditionSetupItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateRetentionConditionSetup`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateRetentionConditionSetup(vmModel: RetentionConditionSetupItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateRetentionConditionSetup`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
