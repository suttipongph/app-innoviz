import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { FieldAccessing } from 'shared/models/systemModel';
import { RetentionConditionSetupItemView } from 'shared/models/viewModel';

const firstGroup = ['PRODUCT_TYPE', 'RETENTION_DEDUCTION_METHOD', 'RETENTION_CALCULATE_BASE'];

export class RetentionConditionSetupItemCustomComponent {
  getFieldAccessing(model: RetentionConditionSetupItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: RetentionConditionSetupItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<RetentionConditionSetupItemView> {
    let model: RetentionConditionSetupItemView = new RetentionConditionSetupItemView();
    model.retentionConditionSetupGUID = EmptyGuid;
    return of(model);
  }
}
