import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetentionConditionSetupItemComponent } from './retention-condition-setup-item.component';

describe('RetentionConditionSetupItemViewComponent', () => {
  let component: RetentionConditionSetupItemComponent;
  let fixture: ComponentFixture<RetentionConditionSetupItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RetentionConditionSetupItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetentionConditionSetupItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
