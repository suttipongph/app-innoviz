import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { RetentionConditionSetupItemView } from 'shared/models/viewModel';
import { RetentionConditionSetupService } from '../retention-condition-setup.service';
import { RetentionConditionSetupItemCustomComponent } from './retention-condition-setup-item-custom.component';
@Component({
  selector: 'retention-condition-setup-item',
  templateUrl: './retention-condition-setup-item.component.html',
  styleUrls: ['./retention-condition-setup-item.component.scss']
})
export class RetentionConditionSetupItemComponent extends BaseItemComponent<RetentionConditionSetupItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  productTypeOptions: SelectItems[] = [];
  retentionCalculateBaseOptions: SelectItems[] = [];
  retentionDeductionMethodOptions: SelectItems[] = [];

  constructor(
    private service: RetentionConditionSetupService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: RetentionConditionSetupItemCustomComponent
  ) {
    super();
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRetentionCalculateBaseEnumDropDown(this.retentionCalculateBaseOptions);
    this.baseDropdown.getRetentionDeductionMethodEnumDropDown(this.retentionDeductionMethodOptions);
  }
  getById(): Observable<RetentionConditionSetupItemView> {
    return this.service.getRetentionConditionSetupById(this.id);
  }
  getInitialData(): Observable<RetentionConditionSetupItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }
  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateRetentionConditionSetup(this.model), isColsing);
    } else {
      super.onCreate(this.service.createRetentionConditionSetup(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
