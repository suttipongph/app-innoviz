import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BusinessTypeService } from './business-type.service';
import { BusinessTypeListComponent } from './business-type-list/business-type-list.component';
import { BusinessTypeItemComponent } from './business-type-item/business-type-item.component';
import { BusinessTypeItemCustomComponent } from './business-type-item/business-type-item-custom.component';
import { BusinessTypeListCustomComponent } from './business-type-list/business-type-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BusinessTypeListComponent, BusinessTypeItemComponent],
  providers: [BusinessTypeService, BusinessTypeItemCustomComponent, BusinessTypeListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BusinessTypeListComponent, BusinessTypeItemComponent]
})
export class BusinessTypeComponentModule {}
