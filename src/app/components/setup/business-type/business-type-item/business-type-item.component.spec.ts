import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessTypeItemComponent } from './business-type-item.component';

describe('BusinessTypeItemViewComponent', () => {
  let component: BusinessTypeItemComponent;
  let fixture: ComponentFixture<BusinessTypeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BusinessTypeItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessTypeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
