import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { BusinessTypeItemView } from 'shared/models/viewModel';

const firstGroup = ['BUSINESS_TYPE_ID'];

export class BusinessTypeItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: BusinessTypeItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.businessTypeGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BusinessTypeItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<BusinessTypeItemView> {
    let model: BusinessTypeItemView = new BusinessTypeItemView();
    model.businessTypeGUID = EmptyGuid;
    return of(model);
  }
}
