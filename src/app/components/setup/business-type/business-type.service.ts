import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BusinessTypeItemView, BusinessTypeListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class BusinessTypeService {
  serviceKey = 'businessTypeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBusinessTypeToList(search: SearchParameter): Observable<SearchResult<BusinessTypeListView>> {
    const url = `${this.servicePath}/GetBusinessTypeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBusinessType(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBusinessType`;
    return this.dataGateway.delete(url, row);
  }
  getBusinessTypeById(id: string): Observable<BusinessTypeItemView> {
    const url = `${this.servicePath}/GetBusinessTypeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBusinessType(vmModel: BusinessTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBusinessType`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBusinessType(vmModel: BusinessTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBusinessType`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
