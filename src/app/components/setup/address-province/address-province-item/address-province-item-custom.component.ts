import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { AddressProvinceItemView } from 'shared/models/viewModel';

const firstGroup = ['PROVINCE_ID'];
export class AddressProvinceItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: AddressProvinceItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.addressProvinceGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: AddressProvinceItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<AddressProvinceItemView> {
    let model: AddressProvinceItemView = new AddressProvinceItemView();
    model.addressProvinceGUID = EmptyGuid;
    return of(model);
  }
}
