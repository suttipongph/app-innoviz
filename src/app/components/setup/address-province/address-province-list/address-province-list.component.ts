import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { AddressProvinceListView } from 'shared/models/viewModel';
import { AddressProvinceService } from '../address-province.service';
import { AddressProvinceListCustomComponent } from './address-province-list-custom.component';

@Component({
  selector: 'address-province-list',
  templateUrl: './address-province-list.component.html',
  styleUrls: ['./address-province-list.component.scss']
})
export class AddressProvinceListComponent extends BaseListComponent<AddressProvinceListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  addressCountryOptions: SelectItems[] = [];
  constructor(private service: AddressProvinceService, public custom: AddressProvinceListCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'addressProvinceGUID';
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    const columns: ColumnModel[] = [
      { label: 'LABEL.COUNTRY_ID', textKey: 'countryId', type: ColumnType.STRING, visibility: false, sorting: SortType.NONE },
      { label: 'LABEL.PROVINCE_ID', textKey: 'provinceId', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE },
      { label: 'LABEL.NAME', textKey: 'name', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<AddressProvinceListView>> {
    return this.service.getAddressProvinceToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteAddressProvince(row));
  }
}
