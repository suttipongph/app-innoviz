import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { AddressProvinceItemView, AddressProvinceListView } from 'shared/models/viewModel';

@Injectable()
export class AddressProvinceService {
  serviceKey = 'addressProvinceGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getAddressProvinceToList(search: SearchParameter): Observable<SearchResult<AddressProvinceListView>> {
    const url = `${this.servicePath}/GetAddressProvinceList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteAddressProvince(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteAddressProvince`;
    return this.dataGateway.delete(url, row);
  }
  getAddressProvinceById(id: string): Observable<AddressProvinceItemView> {
    const url = `${this.servicePath}/GetAddressProvinceById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createAddressProvince(vmModel: AddressProvinceItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateAddressProvince`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateAddressProvince(vmModel: AddressProvinceItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateAddressProvince`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
