import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddressProvinceService } from './address-province.service';
import { AddressProvinceListComponent } from './address-province-list/address-province-list.component';
import { AddressProvinceItemComponent } from './address-province-item/address-province-item.component';
import { AddressProvinceItemCustomComponent } from './address-province-item/address-province-item-custom.component';
import { AddressProvinceListCustomComponent } from './address-province-list/address-province-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [AddressProvinceListComponent, AddressProvinceItemComponent],
  providers: [AddressProvinceService, AddressProvinceItemCustomComponent, AddressProvinceListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [AddressProvinceListComponent, AddressProvinceItemComponent]
})
export class AddressProvinceComponentModule {}
