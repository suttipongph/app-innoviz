import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { TerritoryItemView } from 'shared/models/viewModel';

const firstGroup = ['TERRITORY_ID'];
export class TerritoryItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: TerritoryItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.territoryGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: TerritoryItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<TerritoryItemView> {
    let model: TerritoryItemView = new TerritoryItemView();
    model.territoryGUID = EmptyGuid;
    return of(model);
  }
}
