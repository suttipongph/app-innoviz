import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TerritoryService } from './territory.service';
import { TerritoryListComponent } from './territory-list/territory-list.component';
import { TerritoryItemComponent } from './territory-item/territory-item.component';
import { TerritoryItemCustomComponent } from './territory-item/territory-item-custom.component';
import { TerritoryListCustomComponent } from './territory-list/territory-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [TerritoryListComponent, TerritoryItemComponent],
  providers: [TerritoryService, TerritoryItemCustomComponent, TerritoryListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [TerritoryListComponent, TerritoryItemComponent]
})
export class TerritoryComponentModule {}
