import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { TerritoryItemView, TerritoryListView } from 'shared/models/viewModel';

@Injectable()
export class TerritoryService {
  serviceKey = 'territoryGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getTerritoryToList(search: SearchParameter): Observable<SearchResult<TerritoryListView>> {
    const url = `${this.servicePath}/GetTerritoryList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteTerritory(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteTerritory`;
    return this.dataGateway.delete(url, row);
  }
  getTerritoryById(id: string): Observable<TerritoryItemView> {
    const url = `${this.servicePath}/GetTerritoryById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createTerritory(vmModel: TerritoryItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateTerritory`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateTerritory(vmModel: TerritoryItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateTerritory`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
