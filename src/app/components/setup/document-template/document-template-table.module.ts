import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DocumentTemplateTableService } from './document-template-table.service';
import { DocumentTemplateTableListComponent } from './document-template-table-list/document-template-table-list.component';
import { DocumentTemplateTableItemComponent } from './document-template-table-item/document-template-table-item.component';
import { DocumentTemplateTableItemCustomComponent } from './document-template-table-item/document-template-table-item-custom.component';
import { DocumentTemplateTableListCustomComponent } from './document-template-table-list/document-template-table-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [DocumentTemplateTableListComponent, DocumentTemplateTableItemComponent],
  providers: [DocumentTemplateTableService, DocumentTemplateTableItemCustomComponent, DocumentTemplateTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [DocumentTemplateTableListComponent, DocumentTemplateTableItemComponent]
})
export class DocumentTemplateTableComponentModule {}
