import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { FieldAccessing, FileUpload, FileInformation, BaseServiceModel } from 'shared/models/systemModel';
import { DocumentTemplateTableItemView } from 'shared/models/viewModel';

const firstGroup = ['TEMPLATE_ID'];

export class DocumentTemplateTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: DocumentTemplateTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.documentTemplateTableGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: DocumentTemplateTableItemView): Observable<boolean> {
    return of(!canCreate);
  }

  getInitialData(): Observable<DocumentTemplateTableItemView> {
    let model: DocumentTemplateTableItemView = new DocumentTemplateTableItemView();
    model.documentTemplateTableGUID = EmptyGuid;
    return of(model);
  }
  getFileInformationFromModel(model: DocumentTemplateTableItemView): FileInformation {
    const document = new FileInformation();
    const contentType = this.baseService.fileService.getDataMimeHeaderFromFileName(model.fileName);
    document.base64 = contentType + model.base64Data;
    document.isRemovable = false;
    document.isPreviewable = true;
    document.contentType = contentType;
    document.fileName = model.fileName;
    document.fileDisplayName = model.fileName;
    return document;
  }
}
