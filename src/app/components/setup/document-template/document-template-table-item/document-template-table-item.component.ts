import { ViewChild } from '@angular/core';
import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { IVZFileUploadComponent } from 'shared/components/ivz-file-upload/ivz-file-upload.component';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmpty, isUndefinedOrZeroLength, isUpdateMode } from 'shared/functions/value.function';
import { FileUpload, FormValidationModel, PageInformationModel, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { DocumentTemplateTableItemView } from 'shared/models/viewModel';
import { DocumentTemplateTableService } from '../document-template-table.service';
import { DocumentTemplateTableItemCustomComponent } from './document-template-table-item-custom.component';
@Component({
  selector: 'document-template-table-item',
  templateUrl: './document-template-table-item.component.html',
  styleUrls: ['./document-template-table-item.component.scss']
})
export class DocumentTemplateTableItemComponent extends BaseItemComponent<DocumentTemplateTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  fileUpload: FileUpload = { fileInfos: [] };
  documentTemplateTypeOptions: SelectItems[] = [];
  @ViewChild('fileUploadCmpnt') fileComponent: IVZFileUploadComponent;
  docxType: string = '.docx';
  constructor(
    private service: DocumentTemplateTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: DocumentTemplateTableItemCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  ngOnDestroy(): void {}

  getInitialData(): Observable<DocumentTemplateTableItemView> {
    return this.custom.getInitialData();
  }
  checkPageMode(): void {
    this.fileDirty = false;
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getDocumentTemplateTypeEnumDropDown(this.documentTemplateTypeOptions);
  }
  getById(): Observable<DocumentTemplateTableItemView> {
    return this.service.getDocumentTemplateTableById(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }
  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
          this.getFileUpload();
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {}
  setFunctionOptions(): void {}
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isClosing: boolean): void {
    if (isUndefinedOrZeroLength(this.fileUpload.fileInfos) || isUndefinedOrZeroLength(this.fileUpload.fileInfos[0])) {
      // popup validation fail >> no file selected
      const topic: TranslateModel = { code: 'ERROR.00159' };
      const contents: TranslateModel[] = [{ code: 'ERROR.00160' }];
      this.notificationService.showErrorMessageFromManual(topic, contents);
      return;
    }
    this.setFileUpload();
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateDocumentTemplateTable(this.model), isClosing);
    } else {
      super.onCreate(this.service.createDocumentTemplateTable(this.model), isClosing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }

  onFileChange(e: FileUpload): void {
    this.fileDirty = true;
  }

  getFileUpload(): void {
    if (!isNullOrUndefOrEmpty(this.model.base64Data)) {
      this.fileUpload.fileInfos = [];
      const document = this.custom.getFileInformationFromModel(this.model);
      this.fileUpload.fileInfos.push(document);

      this.fileComponent.setFileFromExternal(document);
    }
  }
  setFileUpload(): void {
    if (isUndefinedOrZeroLength(this.fileUpload.fileInfos) || isUndefinedOrZeroLength(this.fileUpload.fileInfos[0])) {
      return;
    }
    if (!isUpdateMode(this.id) || (isUpdateMode(this.id) && this.fileDirty)) {
      let file = this.fileUpload.fileInfos[0].base64.split(',');
      this.model.fileInfo = this.fileUpload.fileInfos[0];
      this.model.fileName = this.model.fileInfo.fileName;
      this.model.base64Data = file[file.length - 1];
      this.fileDirty = false;
    } else {
      this.model.fileInfo = null;
    }
  }
}
