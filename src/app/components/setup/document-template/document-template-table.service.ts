import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { DocumentTemplateTableItemView, DocumentTemplateTableListView } from 'shared/models/viewModel';

@Injectable()
export class DocumentTemplateTableService {
  serviceKey = 'documentTemplateTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getDocumentTemplateTableToList(search: SearchParameter): Observable<SearchResult<DocumentTemplateTableListView>> {
    const url = `${this.servicePath}/GetDocumentTemplateTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteDocumentTemplateTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteDocumentTemplateTable`;
    return this.dataGateway.delete(url, row);
  }
  getDocumentTemplateTableById(id: string): Observable<DocumentTemplateTableItemView> {
    const url = `${this.servicePath}/GetDocumentTemplateTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createDocumentTemplateTable(vmModel: DocumentTemplateTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateDocumentTemplateTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateDocumentTemplateTable(vmModel: DocumentTemplateTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateDocumentTemplateTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
