import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BusinessCollateralStatusService } from './business-collateral-status.service';
import { BusinessCollateralStatusListComponent } from './business-collateral-status-list/business-collateral-status-list.component';
import { BusinessCollateralStatusItemComponent } from './business-collateral-status-item/business-collateral-status-item.component';
import { BusinessCollateralStatusItemCustomComponent } from './business-collateral-status-item/business-collateral-status-item-custom.component';
import { BusinessCollateralStatusListCustomComponent } from './business-collateral-status-list/business-collateral-status-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BusinessCollateralStatusListComponent, BusinessCollateralStatusItemComponent],
  providers: [BusinessCollateralStatusService, BusinessCollateralStatusItemCustomComponent, BusinessCollateralStatusListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BusinessCollateralStatusListComponent, BusinessCollateralStatusItemComponent]
})
export class BusinessCollateralStatusComponentModule {}
