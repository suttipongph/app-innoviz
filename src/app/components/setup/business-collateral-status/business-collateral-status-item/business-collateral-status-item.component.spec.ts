import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessCollateralStatusItemComponent } from './business-collateral-status-item.component';

describe('BusinessCollateralStatusItemViewComponent', () => {
  let component: BusinessCollateralStatusItemComponent;
  let fixture: ComponentFixture<BusinessCollateralStatusItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BusinessCollateralStatusItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCollateralStatusItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
