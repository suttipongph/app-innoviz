import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { BusinessCollateralStatusItemView } from 'shared/models/viewModel/businessCollateralStatusItemView';

const firstGroup = ['BUSINESS_COLLATERAL_STATUS_ID'];

export class BusinessCollateralStatusItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: BusinessCollateralStatusItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.businessCollateralStatusGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BusinessCollateralStatusItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<BusinessCollateralStatusItemView> {
    let model: BusinessCollateralStatusItemView = new BusinessCollateralStatusItemView();
    model.businessCollateralStatusGUID = EmptyGuid;
    return of(model);
  }
}
