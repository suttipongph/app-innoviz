import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessCollateralStatusListComponent } from './business-collateral-status-list.component';

describe('BusinessCollateralStatusListViewComponent', () => {
  let component: BusinessCollateralStatusListComponent;
  let fixture: ComponentFixture<BusinessCollateralStatusListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BusinessCollateralStatusListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCollateralStatusListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
