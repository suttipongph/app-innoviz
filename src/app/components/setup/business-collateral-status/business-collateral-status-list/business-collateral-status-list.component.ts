import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { BusinessCollateralStatusListView } from 'shared/models/viewModel/businessCollateralStatusListView';
import { BusinessCollateralStatusService } from '../business-collateral-status.service';
import { BusinessCollateralStatusListCustomComponent } from './business-collateral-status-list-custom.component';

@Component({
  selector: 'business-collateral-status-list',
  templateUrl: './business-collateral-status-list.component.html',
  styleUrls: ['./business-collateral-status-list.component.scss']
})
export class BusinessCollateralStatusListComponent extends BaseListComponent<BusinessCollateralStatusListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  constructor(public custom: BusinessCollateralStatusListCustomComponent, private service: BusinessCollateralStatusService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }

  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'businessCollateralStatusGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.BUSINESS_COLLATERAL_STATUS_ID',
        textKey: 'businessCollateralStatusId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<BusinessCollateralStatusListView>> {
    return this.service.getBusinessCollateralStatusToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteBusinessCollateralStatus(row));
  }
}
