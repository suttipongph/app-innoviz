import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { BusinessCollateralStatusItemView } from 'shared/models/viewModel/businessCollateralStatusItemView';
import { BusinessCollateralStatusListView } from 'shared/models/viewModel/businessCollateralStatusListView';
@Injectable()
export class BusinessCollateralStatusService {
  serviceKey = 'businessCollateralStatusGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBusinessCollateralStatusToList(search: SearchParameter): Observable<SearchResult<BusinessCollateralStatusListView>> {
    const url = `${this.servicePath}/GetBusinessCollateralStatusList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBusinessCollateralStatus(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBusinessCollateralStatus`;
    return this.dataGateway.delete(url, row);
  }
  getBusinessCollateralStatusById(id: string): Observable<BusinessCollateralStatusItemView> {
    const url = `${this.servicePath}/GetBusinessCollateralStatusById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBusinessCollateralStatus(vmModel: BusinessCollateralStatusItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBusinessCollateralStatus`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBusinessCollateralStatus(vmModel: BusinessCollateralStatusItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBusinessCollateralStatus`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
