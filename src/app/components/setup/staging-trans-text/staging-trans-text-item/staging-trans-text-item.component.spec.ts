import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StagingTransTextItemComponent } from './staging-trans-text-item.component';

describe('StagingTransTextItemViewComponent', () => {
  let component: StagingTransTextItemComponent;
  let fixture: ComponentFixture<StagingTransTextItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StagingTransTextItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StagingTransTextItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
