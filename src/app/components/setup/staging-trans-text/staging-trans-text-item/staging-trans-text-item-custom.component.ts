import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { StagingTransTextItemView } from 'shared/models/viewModel/stagingTransTextItemView';

const firstGroup = ['PROCESS_TRANS_TYPE'];

export class StagingTransTextItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: StagingTransTextItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.stagingTransTextGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: StagingTransTextItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<StagingTransTextItemView> {
    let model = new StagingTransTextItemView();
    model.stagingTransTextGUID = EmptyGuid;
    return of(model);
  }
}
