import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StagingTransTextService } from './staging-trans-text.service';
import { StagingTransTextListComponent } from './staging-trans-text-list/staging-trans-text-list.component';
import { StagingTransTextItemComponent } from './staging-trans-text-item/staging-trans-text-item.component';
import { StagingTransTextItemCustomComponent } from './staging-trans-text-item/staging-trans-text-item-custom.component';
import { StagingTransTextListCustomComponent } from './staging-trans-text-list/staging-trans-text-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [StagingTransTextListComponent, StagingTransTextItemComponent],
  providers: [StagingTransTextService, StagingTransTextItemCustomComponent, StagingTransTextListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [StagingTransTextListComponent, StagingTransTextItemComponent]
})
export class StagingTransTextComponentModule {}
