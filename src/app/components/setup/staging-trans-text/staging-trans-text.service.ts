import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { StagingTransTextListView } from 'shared/models/viewModel/stagingTransTextListView';
import { StagingTransTextItemView } from 'shared/models/viewModel/stagingTransTextItemView';
@Injectable()
export class StagingTransTextService {
  serviceKey = 'stagingTransTextGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getStagingTransTextToList(search: SearchParameter): Observable<SearchResult<StagingTransTextListView>> {
    const url = `${this.servicePath}/GetStagingTransTextList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteStagingTransText(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteStagingTransText`;
    return this.dataGateway.delete(url, row);
  }
  getStagingTransTextById(id: string): Observable<StagingTransTextItemView> {
    const url = `${this.servicePath}/GetStagingTransTextById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createStagingTransText(vmModel: StagingTransTextItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateStagingTransText`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateStagingTransText(vmModel: StagingTransTextItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateStagingTransText`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
