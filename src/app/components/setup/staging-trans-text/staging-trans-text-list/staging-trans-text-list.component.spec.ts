import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StagingTransTextListComponent } from './staging-trans-text-list.component';

describe('StagingTransTextListViewComponent', () => {
  let component: StagingTransTextListComponent;
  let fixture: ComponentFixture<StagingTransTextListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StagingTransTextListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StagingTransTextListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
