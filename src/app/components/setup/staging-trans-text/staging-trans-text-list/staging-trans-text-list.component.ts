import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { StagingTransTextListView } from 'shared/models/viewModel/stagingTransTextListView';
import { StagingTransTextService } from '../staging-trans-text.service';
import { StagingTransTextListCustomComponent } from './staging-trans-text-list-custom.component';

@Component({
  selector: 'staging-trans-text-list',
  templateUrl: './staging-trans-text-list.component.html',
  styleUrls: ['./staging-trans-text-list.component.scss']
})
export class StagingTransTextListComponent extends BaseListComponent<StagingTransTextListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  processTransTypeOptions: SelectItems[] = [];

  constructor(public custom: StagingTransTextListCustomComponent, private service: StagingTransTextService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getProcessTransTypeEnumDropDown(this.processTransTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'stagingTransTextGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.PROCESS_TRANSACTION_TYPE',
        textKey: 'processTransType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.processTransTypeOptions
      },
      {
        label: 'LABEL.TRANSACTION_TEXT',
        textKey: 'transText',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<StagingTransTextListView>> {
    return this.service.getStagingTransTextToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteStagingTransText(row));
  }
}
