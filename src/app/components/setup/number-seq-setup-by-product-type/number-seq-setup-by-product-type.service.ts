import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { NumberSeqSetupByProductTypeItemView, NumberSeqSetupByProductTypeListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class NumberSeqSetupByProductTypeService {
  serviceKey = 'numberSeqSetupByProductTypeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getNumberSeqSetupByProductTypeToList(search: SearchParameter): Observable<SearchResult<NumberSeqSetupByProductTypeListView>> {
    const url = `${this.servicePath}/GetNumberSeqSetupByProductTypeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteNumberSeqSetupByProductType(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteNumberSeqSetupByProductType`;
    return this.dataGateway.delete(url, row);
  }
  getNumberSeqSetupByProductTypeById(id: string): Observable<NumberSeqSetupByProductTypeItemView> {
    const url = `${this.servicePath}/GetNumberSeqSetupByProductTypeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createNumberSeqSetupByProductType(vmModel: NumberSeqSetupByProductTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateNumberSeqSetupByProductType`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateNumberSeqSetupByProductType(vmModel: NumberSeqSetupByProductTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateNumberSeqSetupByProductType`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
