import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NumberSeqSetupByProductTypeService } from './number-seq-setup-by-product-type.service';
import { NumberSeqSetupByProductTypeListComponent } from './number-seq-setup-by-product-type-list/number-seq-setup-by-product-type-list.component';
import { NumberSeqSetupByProductTypeItemComponent } from './number-seq-setup-by-product-type-item/number-seq-setup-by-product-type-item.component';
import { NumberSeqSetupByProductTypeItemCustomComponent } from './number-seq-setup-by-product-type-item/number-seq-setup-by-product-type-item-custom.component';
import { NumberSeqSetupByProductTypeListCustomComponent } from './number-seq-setup-by-product-type-list/number-seq-setup-by-product-type-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [NumberSeqSetupByProductTypeListComponent, NumberSeqSetupByProductTypeItemComponent],
  providers: [NumberSeqSetupByProductTypeService, NumberSeqSetupByProductTypeItemCustomComponent, NumberSeqSetupByProductTypeListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [NumberSeqSetupByProductTypeListComponent, NumberSeqSetupByProductTypeItemComponent]
})
export class NumberSeqSetupByProductTypeComponentModule {}
