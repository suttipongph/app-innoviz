import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { NumberSeqSetupByProductTypeListView } from 'shared/models/viewModel';
import { NumberSeqSetupByProductTypeService } from '../number-seq-setup-by-product-type.service';
import { NumberSeqSetupByProductTypeListCustomComponent } from './number-seq-setup-by-product-type-list-custom.component';

@Component({
  selector: 'number-seq-setup-by-product-type-list',
  templateUrl: './number-seq-setup-by-product-type-list.component.html',
  styleUrls: ['./number-seq-setup-by-product-type-list.component.scss']
})
export class NumberSeqSetupByProductTypeListComponent extends BaseListComponent<NumberSeqSetupByProductTypeListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  numberSeqTableOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];

  constructor(public custom: NumberSeqSetupByProductTypeListCustomComponent, private service: NumberSeqSetupByProductTypeService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getNumberSeqTableDropDown(this.numberSeqTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'numberSeqSetupByProductTypeGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: 'LABEL.CREDIT_APPLICATION_REQUEST_NUMBER_SEQUENCE_CODE',
        textKey: 'creditAppRequestNumber_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.numberSeqTableOptions,
        sortingKey: 'numberSeqCode',
        searchingKey: 'creditAppRequestNumberSeqGUID'
      },
      {
        label: 'LABEL.CREDIT_APPLICATION_NUMBER_SEQUENCE_CODE',
        textKey: 'creditAppNumberSeq_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.numberSeqTableOptions,
        sortingKey: 'numberSeqCode',
        searchingKey: 'creditAppNumberSeqGUID'
      },
      {
        label: 'LABEL.INTERNAL_MAIN_AGREEMENT_NUMBER_SEQUENCE_CODE',
        textKey: 'internalMainAgreementNumber_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.numberSeqTableOptions,
        sortingKey: 'numberSeqCode',
        searchingKey: 'internalMainAgreementNumberSeqGUID'
      },
      {
        label: 'LABEL.MAIN_AGREEMENT_NUMBER_SEQUENCE_CODE',
        textKey: 'mainAgreementNumber_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.numberSeqTableOptions,
        sortingKey: 'numberSeqCode',
        searchingKey: 'mainAgreementNumberSeqGUID'
      },
      {
        label: 'LABEL.INTERNAL_GUARANTOR_AGREEMENT_NUMBER_SEQUENCE_CODE',
        textKey: 'internalGuarantorAgreementNumber_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.numberSeqTableOptions,
        sortingKey: 'numberSeqCode',
        searchingKey: 'internalGuarantorAgreementNumberSeqGUID'
      },
      {
        label: 'LABEL.GUARANTOR_AGREEMENT_NUMBER_SEQUENCE_CODE',
        textKey: 'guarantorAgreementNumber_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.numberSeqTableOptions,
        sortingKey: 'numberSeqCode',
        searchingKey: 'guarantorAgreementNumberSeqGUID'
      },
      {
        label: 'LABEL.TAX_INVOICE_NUMBER_SEQUENCE_CODE',
        textKey: 'taxInvoiceNumberSeq_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.numberSeqTableOptions,
        sortingKey: 'numberSeqCode',
        searchingKey: 'taxInvoiceNumberSeqGUID'
      },
      {
        label: 'LABEL.TAX_CREDIT_NOTE_NUMBER_SEQUENCE_CODE',
        textKey: 'taxCreditNoteNumberSeq_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.numberSeqTableOptions,
        sortingKey: 'numberSeqCode',
        searchingKey: 'taxCreditNoteNumberSeqGUID'
      },
      {
        label: 'LABEL.RECEIPT_NUMBER_SEQUENCE_CODE',
        textKey: 'receiptNumberSeq_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.numberSeqTableOptions,
        sortingKey: 'numberSeqCode',
        searchingKey: 'receiptNumberSeqGUID'
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<NumberSeqSetupByProductTypeListView>> {
    return this.service.getNumberSeqSetupByProductTypeToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteNumberSeqSetupByProductType(row));
  }
}
