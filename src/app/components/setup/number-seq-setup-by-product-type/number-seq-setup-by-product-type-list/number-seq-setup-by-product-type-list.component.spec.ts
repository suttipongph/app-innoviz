import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NumberSeqSetupByProductTypeListComponent } from './number-seq-setup-by-product-type-list.component';

describe('NumberSeqSetupByProductTypeListViewComponent', () => {
  let component: NumberSeqSetupByProductTypeListComponent;
  let fixture: ComponentFixture<NumberSeqSetupByProductTypeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NumberSeqSetupByProductTypeListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumberSeqSetupByProductTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
