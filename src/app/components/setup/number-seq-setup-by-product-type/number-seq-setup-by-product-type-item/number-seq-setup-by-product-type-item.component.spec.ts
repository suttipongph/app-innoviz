import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumberSeqSetupByProductTypeItemComponent } from './number-seq-setup-by-product-type-item.component';

describe('NumberSeqSetupByProductTypeItemViewComponent', () => {
  let component: NumberSeqSetupByProductTypeItemComponent;
  let fixture: ComponentFixture<NumberSeqSetupByProductTypeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NumberSeqSetupByProductTypeItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumberSeqSetupByProductTypeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
