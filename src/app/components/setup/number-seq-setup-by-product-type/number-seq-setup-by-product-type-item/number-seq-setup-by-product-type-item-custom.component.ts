import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { NumberSeqSetupByProductTypeItemView } from 'shared/models/viewModel';

const firstGroup = [];

export class NumberSeqSetupByProductTypeItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: NumberSeqSetupByProductTypeItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: NumberSeqSetupByProductTypeItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<NumberSeqSetupByProductTypeItemView> {
    let model = new NumberSeqSetupByProductTypeItemView();
    model.numberSeqSetupByProductTypeGUID = EmptyGuid;
    return of(model);
  }
}
