import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { InvoiceRevenueTypeItemView, InvoiceRevenueTypeListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class InvoiceRevenueTypeService {
  serviceKey = 'invoiceRevenueTypeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.servicePath = param.servicePath;
  }
  getInvoiceRevenueTypeToList(search: SearchParameter): Observable<SearchResult<InvoiceRevenueTypeListView>> {
    const url = `${this.servicePath}/GetInvoiceRevenueTypeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteInvoiceRevenueType(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteInvoiceRevenueType`;
    return this.dataGateway.delete(url, row);
  }
  getInvoiceRevenueTypeById(id: string): Observable<InvoiceRevenueTypeItemView> {
    const url = `${this.servicePath}/GetInvoiceRevenueTypeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createInvoiceRevenueType(vmModel: InvoiceRevenueTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateInvoiceRevenueType`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateInvoiceRevenueType(vmModel: InvoiceRevenueTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateInvoiceRevenueType`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getTaxValueByFeeTax(feeTaxId: string): Observable<number> {
    const url = `${this.servicePath}/GetTaxValueByFeeTax/feeTaxId=${feeTaxId}`;
    return this.dataGateway.get(url);
  }
}
