import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InvoiceRevenueTypeService } from './invoice-revenue-type.service';
import { InvoiceRevenueTypeListComponent } from './invoice-revenue-type-list/invoice-revenue-type-list.component';
import { InvoiceRevenueTypeItemComponent } from './invoice-revenue-type-item/invoice-revenue-type-item.component';
import { InvoiceRevenueTypeItemCustomComponent } from './invoice-revenue-type-item/invoice-revenue-type-item-custom.component';
import { InvoiceRevenueTypeListCustomComponent } from './invoice-revenue-type-list/invoice-revenue-type-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [InvoiceRevenueTypeListComponent, InvoiceRevenueTypeItemComponent],
  providers: [InvoiceRevenueTypeService, InvoiceRevenueTypeItemCustomComponent, InvoiceRevenueTypeListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [InvoiceRevenueTypeListComponent, InvoiceRevenueTypeItemComponent]
})
export class InvoiceRevenueTypeComponentModule {}
