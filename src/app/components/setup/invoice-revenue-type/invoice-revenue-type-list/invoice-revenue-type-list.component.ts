import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { InvoiceRevenueTypeListView } from 'shared/models/viewModel';
import { InvoiceRevenueTypeService } from '../invoice-revenue-type.service';
import { InvoiceRevenueTypeListCustomComponent } from './invoice-revenue-type-list-custom.component';

@Component({
  selector: 'invoice-revenue-type-list',
  templateUrl: './invoice-revenue-type-list.component.html',
  styleUrls: ['./invoice-revenue-type-list.component.scss']
})
export class InvoiceRevenueTypeListComponent extends BaseListComponent<InvoiceRevenueTypeListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  assetFeeTypeOptions: SelectItems[] = [];
  intercompanyTableOptions: SelectItems[] = [];
  invoiceRevenueTypeOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  serviceFeeCategoryOptions: SelectItems[] = [];
  taxTableOptions: SelectItems[] = [];
  withholdingTaxTableOptions: SelectItems[] = [];

  constructor(public custom: InvoiceRevenueTypeListCustomComponent, private service: InvoiceRevenueTypeService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getAssetFeeTypeEnumDropDown(this.assetFeeTypeOptions);
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getServiceFeeCategoryEnumDropDown(this.serviceFeeCategoryOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getInvoiceRevenueTypeDropDown(this.invoiceRevenueTypeOptions);
    this.baseDropdown.getTaxTableDropDown(this.taxTableOptions);
    this.baseDropdown.getWithholdingTaxTableDropDown(this.withholdingTaxTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'invoiceRevenueTypeGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.SERVICE_FEE_TYPE_ID',
        textKey: 'revenueTypeId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.AMOUNT_BEFORE_TAX',
        textKey: 'feeAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: 'LABEL.TAX_CODE',
        textKey: 'taxTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.taxTableOptions,
        sortingKey: 'taxTable_TaxCode',
        searchingKey: 'feeTaxGUID'
      },
      {
        label: 'LABEL.TAX_AMOUNT',
        textKey: 'feeTaxAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: 'LABEL.WITHHOLDING_TAX_CODE',
        textKey: 'withholdingTaxTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.withholdingTaxTableOptions,
        sortingKey: 'withholdingTaxTable_WHTCode',
        searchingKey: 'feeWHTGUID'
      },
      {
        label: 'LABEL.ADDITIONAL_SERVICE_FEE_TYPE_ID',
        textKey: 'invoiceRevenueType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.invoiceRevenueTypeOptions,
        sortingKey: 'invoiceRevenueType_revenueTypeId',
        searchingKey: 'serviceFeeRevenueTypeGUID'
      },
      {
        label: 'LABEL.ASSET_FEE_TYPE',
        textKey: 'assetFeeType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.assetFeeTypeOptions
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<InvoiceRevenueTypeListView>> {
    return this.service.getInvoiceRevenueTypeToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteInvoiceRevenueType(row));
  }
}
