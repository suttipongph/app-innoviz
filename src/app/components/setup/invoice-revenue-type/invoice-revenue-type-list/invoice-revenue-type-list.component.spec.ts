import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InvoiceRevenueTypeListComponent } from './invoice-revenue-type-list.component';

describe('InvoiceRevenueTypeListViewComponent', () => {
  let component: InvoiceRevenueTypeListComponent;
  let fixture: ComponentFixture<InvoiceRevenueTypeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InvoiceRevenueTypeListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceRevenueTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
