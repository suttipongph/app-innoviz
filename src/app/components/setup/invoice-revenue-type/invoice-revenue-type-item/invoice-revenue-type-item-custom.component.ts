import { TranslateService } from '@ngx-translate/core';
import { AppInjector } from 'app-injector';
import { NotificationService } from 'core/services/notification.service';
import { Observable, of } from 'rxjs';
import { AssetFeeType, EmptyGuid, ServiceFeeCategory } from 'shared/constants';
import { isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { InvoiceRevenueTypeItemView } from 'shared/models/viewModel';
import { InvoiceRevenueTypeService } from '../invoice-revenue-type.service';

const firstGroup = ['REVENUE_TYPE_ID'];

export class InvoiceRevenueTypeItemCustomComponent {
  baseService: BaseServiceModel<any>;
  notificationService = AppInjector.get(NotificationService);
  translateService = AppInjector.get(TranslateService);
  getFieldAccessing(model: InvoiceRevenueTypeItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.invoiceRevenueTypeGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: InvoiceRevenueTypeItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<InvoiceRevenueTypeItemView> {
    let model = new InvoiceRevenueTypeItemView();
    model.invoiceRevenueTypeGUID = EmptyGuid;
    model.assetFeeType = AssetFeeType.None;
    model.serviceFeeCategory = ServiceFeeCategory.None;
    return of(model);
  }

  setFeeTaxAmount(model: InvoiceRevenueTypeItemView) {
    if (!isNullOrUndefOrEmptyGUID(model.feeTaxGUID)) {
      this.baseService.service.getTaxValueByFeeTax(model.feeTaxGUID).subscribe(
        (result) => {
          if (result != 0) {
            model.feeTaxAmount = (result / 100) * model.feeAmount;
          } else {
            model.feeTaxAmount = 0;
          }
        },
        (error) => {
          model.feeTaxAmount = 0;
          //this.notificationService.showErrorMessageFromResponse(error); //issue 0021784 แจ้ง error 2 ครั้ง
        }
      );
    } else {
      model.feeTaxAmount = 0;
    }
  }
  feeTaxAmountValidation(model: InvoiceRevenueTypeItemView) {
    if (model.feeTaxAmount < 0) {
      return {
        code: 'ERROR.GREATER_THAN_EQUAL_OR_ZERO',
        parameters: [this.translateService.instant('LABEL.TAX_AMOUNT')]
      };
    }
    return null;
  }
}
