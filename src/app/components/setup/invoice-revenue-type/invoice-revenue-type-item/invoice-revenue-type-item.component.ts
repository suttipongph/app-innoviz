import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { InvoiceRevenueTypeItemView } from 'shared/models/viewModel';
import { InvoiceRevenueTypeService } from '../invoice-revenue-type.service';
import { InvoiceRevenueTypeItemCustomComponent } from './invoice-revenue-type-item-custom.component';
@Component({
  selector: 'invoice-revenue-type-item',
  templateUrl: './invoice-revenue-type-item.component.html',
  styleUrls: ['./invoice-revenue-type-item.component.scss']
})
export class InvoiceRevenueTypeItemComponent extends BaseItemComponent<InvoiceRevenueTypeItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  assetFeeTypeOptions: SelectItems[] = [];
  intercompanyTableOptions: SelectItems[] = [];
  invoiceRevenueTypeOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  serviceFeeCategoryOptions: SelectItems[] = [];
  taxTableOptions: SelectItems[] = [];
  withholdingTaxTableOptions: SelectItems[] = [];

  constructor(
    public service: InvoiceRevenueTypeService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: InvoiceRevenueTypeItemCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkPageMode();
    this.checkAccessMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getAssetFeeTypeEnumDropDown(this.assetFeeTypeOptions);
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getServiceFeeCategoryEnumDropDown(this.serviceFeeCategoryOptions);
  }
  getById(): Observable<InvoiceRevenueTypeItemView> {
    return this.service.getInvoiceRevenueTypeById(this.id);
  }
  getInitialData(): Observable<InvoiceRevenueTypeItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(
      this.baseDropdown.getIntercompanyTableDropDown(),
      this.baseDropdown.getInvoiceRevenueTypeDropDown(),
      this.baseDropdown.getTaxTableDropDown(),
      this.baseDropdown.getWithholdingTaxTableDropDown()
    ).subscribe(
      ([intercompanyTable, invoiceRevenueType, taxTable, withholdingTaxTable]) => {
        this.intercompanyTableOptions = intercompanyTable;
        this.invoiceRevenueTypeOptions = invoiceRevenueType;
        this.taxTableOptions = taxTable;
        this.withholdingTaxTableOptions = withholdingTaxTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateInvoiceRevenueType(this.model), isColsing);
    } else {
      super.onCreate(this.service.createInvoiceRevenueType(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onFeeAmountChange() {
    this.custom.setFeeTaxAmount(this.model);
  }
  onTaxCodeChange() {
    this.custom.setFeeTaxAmount(this.model);
  }
}
