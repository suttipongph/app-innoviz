import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceRevenueTypeItemComponent } from './invoice-revenue-type-item.component';

describe('InvoiceRevenueTypeItemViewComponent', () => {
  let component: InvoiceRevenueTypeItemComponent;
  let fixture: ComponentFixture<InvoiceRevenueTypeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InvoiceRevenueTypeItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceRevenueTypeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
