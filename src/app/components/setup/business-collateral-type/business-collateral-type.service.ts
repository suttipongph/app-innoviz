import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { BusinessCollateralTypeItemView, BusinessCollateralTypeListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class BusinessCollateralTypeService {
  serviceKey = 'businessCollateralTypeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBusinessCollateralTypeToList(search: SearchParameter): Observable<SearchResult<BusinessCollateralTypeListView>> {
    const url = `${this.servicePath}/GetBusinessCollateralTypeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBusinessCollateralType(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBusinessCollateralType`;
    return this.dataGateway.delete(url, row);
  }
  getBusinessCollateralTypeById(id: string): Observable<BusinessCollateralTypeItemView> {
    const url = `${this.servicePath}/GetBusinessCollateralTypeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBusinessCollateralType(vmModel: BusinessCollateralTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBusinessCollateralType`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBusinessCollateralType(vmModel: BusinessCollateralTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBusinessCollateralType`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
