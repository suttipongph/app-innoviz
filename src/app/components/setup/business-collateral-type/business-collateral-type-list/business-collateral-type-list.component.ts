import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { BusinessCollateralTypeListView } from 'shared/models/viewModel';
import { BusinessCollateralTypeService } from '../business-collateral-type.service';
import { BusinessCollateralTypeListCustomComponent } from './business-collateral-type-list-custom.component';

@Component({
  selector: 'business-collateral-type-list',
  templateUrl: './business-collateral-type-list.component.html',
  styleUrls: ['./business-collateral-type-list.component.scss']
})
export class BusinessCollateralTypeListComponent extends BaseListComponent<BusinessCollateralTypeListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  defaultBitOptions: SelectItems[] = [];

  constructor(public custom: BusinessCollateralTypeListCustomComponent, private service: BusinessCollateralTypeService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getDefaultBitDropDown(this.defaultBitOptions);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'businessCollateralTypeGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.BUSINESS_COLLATERAL_TYPE_ID',
        textKey: 'businessCollateralTypeId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.AGREEMENT_ORDERING',
        textKey: 'agreementOrdering',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<BusinessCollateralTypeListView>> {
    return this.service.getBusinessCollateralTypeToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteBusinessCollateralType(row));
  }
}
