import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessCollateralTypeListComponent } from './business-collateral-type-list.component';

describe('BusinessCollateralTypeListViewComponent', () => {
  let component: BusinessCollateralTypeListComponent;
  let fixture: ComponentFixture<BusinessCollateralTypeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BusinessCollateralTypeListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCollateralTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
