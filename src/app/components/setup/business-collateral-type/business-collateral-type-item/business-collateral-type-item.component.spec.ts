import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessCollateralTypeItemComponent } from './business-collateral-type-item.component';

describe('BusinessCollateralTypeItemViewComponent', () => {
  let component: BusinessCollateralTypeItemComponent;
  let fixture: ComponentFixture<BusinessCollateralTypeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BusinessCollateralTypeItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCollateralTypeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
