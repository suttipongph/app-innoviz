import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { BusinessCollateralTypeItemView } from 'shared/models/viewModel';

const firstGroup = ['BUSINESS_COLLATERAL_TYPE_ID'];

export class BusinessCollateralTypeItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: BusinessCollateralTypeItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.businessCollateralTypeGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BusinessCollateralTypeItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<BusinessCollateralTypeItemView> {
    let model: BusinessCollateralTypeItemView = new BusinessCollateralTypeItemView();
    model.businessCollateralTypeGUID = EmptyGuid;
    return of(model);
  }
}
