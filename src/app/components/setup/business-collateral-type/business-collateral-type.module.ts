import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BusinessCollateralTypeService } from './business-collateral-type.service';
import { BusinessCollateralTypeListComponent } from './business-collateral-type-list/business-collateral-type-list.component';
import { BusinessCollateralTypeItemComponent } from './business-collateral-type-item/business-collateral-type-item.component';
import { BusinessCollateralTypeItemCustomComponent } from './business-collateral-type-item/business-collateral-type-item-custom.component';
import { BusinessCollateralTypeListCustomComponent } from './business-collateral-type-list/business-collateral-type-list-custom.component';
import { BusinessCollateralSubTypeItemPage } from 'pages/setup/master-information/business-collateral-type/business-collateral-sub-type-item/business-collateral-sub-type-item.page';
import { BusinessCollateralSubTypeComponentModule } from '../business-collateral-sub-type/business-collateral-sub-type.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, BusinessCollateralSubTypeComponentModule],
  declarations: [BusinessCollateralTypeListComponent, BusinessCollateralTypeItemComponent],
  providers: [BusinessCollateralTypeService, BusinessCollateralTypeItemCustomComponent, BusinessCollateralTypeListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BusinessCollateralTypeListComponent, BusinessCollateralTypeItemComponent]
})
export class BusinessCollateralTypeComponentModule {}
