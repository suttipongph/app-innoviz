import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { CurrencyItemView, CurrencyListView, ExchangeRateItemView } from 'shared/models/viewModel';

@Injectable()
export class CurrencyService {
  serviceKey = 'currencyGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCurrencyToList(search: SearchParameter): Observable<SearchResult<CurrencyListView>> {
    const url = `${this.servicePath}/GetCurrencyList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCurrency(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCurrency`;
    return this.dataGateway.delete(url, row);
  }
  getCurrencyById(id: string): Observable<CurrencyItemView> {
    const url = `${this.servicePath}/GetCurrencyById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCurrency(vmModel: CurrencyItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCurrency`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCurrency(vmModel: CurrencyItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCurrency`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
