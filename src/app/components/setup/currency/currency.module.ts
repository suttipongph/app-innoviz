import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CurrencyService } from './currency.service';
import { CurrencyListComponent } from './currency-list/currency-list.component';
import { CurrencyItemComponent } from './currency-item/currency-item.component';
import { CurrencyItemCustomComponent } from './currency-item/currency-item-custom.component';
import { CurrencyListCustomComponent } from './currency-list/currency-list-custom.component';
import { ExchangeRateComponentModule } from '../exchange-rate/exchange-rate.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, ExchangeRateComponentModule],
  declarations: [CurrencyListComponent, CurrencyItemComponent],
  providers: [CurrencyService, CurrencyItemCustomComponent, CurrencyListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CurrencyListComponent, CurrencyItemComponent]
})
export class CurrencyComponentModule {}
