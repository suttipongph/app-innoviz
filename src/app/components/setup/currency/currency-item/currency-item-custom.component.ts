import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CurrencyItemView } from 'shared/models/viewModel';

const firstGroup = ['CURRENCY_ID'];
export class CurrencyItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CurrencyItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.currencyGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CurrencyItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<CurrencyItemView> {
    let model: CurrencyItemView = new CurrencyItemView();
    model.currencyGUID = EmptyGuid;
    return of(model);
  }
}
