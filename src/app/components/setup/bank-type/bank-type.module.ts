import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BankTypeService } from './bank-type.service';
import { BankTypeListComponent } from './bank-type-list/bank-type-list.component';
import { BankTypeItemComponent } from './bank-type-item/bank-type-item.component';
import { BankTypeItemCustomComponent } from './bank-type-item/bank-type-item-custom.component';
import { BankTypeListCustomComponent } from './bank-type-list/bank-type-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BankTypeListComponent, BankTypeItemComponent],
  providers: [BankTypeService, BankTypeItemCustomComponent, BankTypeListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BankTypeListComponent, BankTypeItemComponent]
})
export class BankTypeComponentModule {}
