import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { BankTypeItemView } from 'shared/models/viewModel/bankTypeItemView';

const firstGroup = ['BANK_TYPE_ID'];

export class BankTypeItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: BankTypeItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.bankTypeGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BankTypeItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<BankTypeItemView> {
    let model: BankTypeItemView = new BankTypeItemView();
    model.bankTypeGUID = EmptyGuid;
    return of(model);
  }
}
