import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankTypeItemComponent } from './bank-type-item.component';

describe('BankTypeItemViewComponent', () => {
  let component: BankTypeItemComponent;
  let fixture: ComponentFixture<BankTypeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BankTypeItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankTypeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
