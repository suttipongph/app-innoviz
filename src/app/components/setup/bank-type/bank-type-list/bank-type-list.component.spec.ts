import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BankTypeListComponent } from './bank-type-list.component';

describe('BankTypeListViewComponent', () => {
  let component: BankTypeListComponent;
  let fixture: ComponentFixture<BankTypeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BankTypeListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
