import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { BankTypeItemView } from 'shared/models/viewModel/bankTypeItemView';
import { BankTypeListView } from 'shared/models/viewModel/bankTypeListView';
@Injectable()
export class BankTypeService {
  serviceKey = 'bankTypeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBankTypeToList(search: SearchParameter): Observable<SearchResult<BankTypeListView>> {
    const url = `${this.servicePath}/GetBankTypeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBankType(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBankType`;
    return this.dataGateway.delete(url, row);
  }
  getBankTypeById(id: string): Observable<BankTypeItemView> {
    const url = `${this.servicePath}/GetBankTypeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBankType(vmModel: BankTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBankType`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBankType(vmModel: BankTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBankType`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
