import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { LineOfBusinessItemView, LineOfBusinessListView } from 'shared/models/viewModel';

@Injectable()
export class LineOfBusinessService {
  serviceKey = 'lineOfBusinessGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getLineOfBusinessToList(search: SearchParameter): Observable<SearchResult<LineOfBusinessListView>> {
    const url = `${this.servicePath}/GetLineOfBusinessList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteLineOfBusiness(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteLineOfBusiness`;
    return this.dataGateway.delete(url, row);
  }
  getLineOfBusinessById(id: string): Observable<LineOfBusinessItemView> {
    const url = `${this.servicePath}/GetLineOfBusinessById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createLineOfBusiness(vmModel: LineOfBusinessItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateLineOfBusiness`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateLineOfBusiness(vmModel: LineOfBusinessItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateLineOfBusiness`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
