import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { LineOfBusinessItemView } from 'shared/models/viewModel';

const firstGroup = ['LINE_OF_BUSINESS_ID'];
export class LineOfBusinessItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: LineOfBusinessItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.lineOfBusinessGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: LineOfBusinessItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<LineOfBusinessItemView> {
    let model: LineOfBusinessItemView = new LineOfBusinessItemView();
    model.lineOfBusinessGUID = EmptyGuid;
    return of(model);
  }
}
