import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LineOfBusinessService } from './line-of-business.service';
import { LineOfBusinessListComponent } from './line-of-business-list/line-of-business-list.component';
import { LineOfBusinessItemComponent } from './line-of-business-item/line-of-business-item.component';
import { LineOfBusinessItemCustomComponent } from './line-of-business-item/line-of-business-item-custom.component';
import { LineOfBusinessListCustomComponent } from './line-of-business-list/line-of-business-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [LineOfBusinessListComponent, LineOfBusinessItemComponent],
  providers: [LineOfBusinessService, LineOfBusinessItemCustomComponent, LineOfBusinessListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [LineOfBusinessListComponent, LineOfBusinessItemComponent]
})
export class LineOfBusinessComponentModule {}
