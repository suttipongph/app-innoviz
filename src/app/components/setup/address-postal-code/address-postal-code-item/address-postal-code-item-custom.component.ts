import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { AddressPostalCodeItemView } from 'shared/models/viewModel';

const firstGroup = ['POSTAL_CODE'];
export class AddressPostalCodeItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: AddressPostalCodeItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.addressPostalCodeGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: AddressPostalCodeItemView): Observable<boolean> {
    return of(isUpdateMode(model.addressPostalCodeGUID) ? true : !canCreate);
  }
  getInitialData(): Observable<AddressPostalCodeItemView> {
    let model: AddressPostalCodeItemView = new AddressPostalCodeItemView();
    model.addressPostalCodeGUID = EmptyGuid;
    return of(model);
  }
}
