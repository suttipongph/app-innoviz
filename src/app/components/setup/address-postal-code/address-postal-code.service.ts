import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { AddressPostalCodeItemView, AddressPostalCodeListView } from 'shared/models/viewModel';

@Injectable()
export class AddressPostalCodeService {
  serviceKey = 'addressPostalCodeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getAddressPostalCodeToList(search: SearchParameter): Observable<SearchResult<AddressPostalCodeListView>> {
    const url = `${this.servicePath}/GetAddressPostalCodeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteAddressPostalCode(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteAddressPostalCode`;
    return this.dataGateway.delete(url, row);
  }
  getAddressPostalCodeById(id: string): Observable<AddressPostalCodeItemView> {
    const url = `${this.servicePath}/GetAddressPostalCodeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createAddressPostalCode(vmModel: AddressPostalCodeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateAddressPostalCode`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateAddressPostalCode(vmModel: AddressPostalCodeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateAddressPostalCode`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
