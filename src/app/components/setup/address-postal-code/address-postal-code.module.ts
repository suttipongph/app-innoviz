import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddressPostalCodeService } from './address-postal-code.service';
import { AddressPostalCodeListComponent } from './address-postal-code-list/address-postal-code-list.component';
import { AddressPostalCodeItemComponent } from './address-postal-code-item/address-postal-code-item.component';
import { AddressPostalCodeItemCustomComponent } from './address-postal-code-item/address-postal-code-item-custom.component';
import { AddressPostalCodeListCustomComponent } from './address-postal-code-list/address-postal-code-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [AddressPostalCodeListComponent, AddressPostalCodeItemComponent],
  providers: [AddressPostalCodeService, AddressPostalCodeItemCustomComponent, AddressPostalCodeListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [AddressPostalCodeListComponent, AddressPostalCodeItemComponent]
})
export class AddressPostalCodeComponentModule {}
