import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, EMPL_TEAM, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { EmplTeamListView } from 'shared/models/viewModel';
import { EmplTeamService } from '../empl-team.service';
import { EmplTeamListCustomComponent } from './empl-team-list-custom.component';

@Component({
  selector: 'empl-team-list',
  templateUrl: './empl-team-list.component.html',
  styleUrls: ['./empl-team-list.component.scss']
})
export class EmplTeamListComponent extends BaseListComponent<EmplTeamListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  constructor(private service: EmplTeamService, public custom: EmplTeamListCustomComponent) {
    super();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'emplTeamGUID';
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    const columns: ColumnModel[] = [
      { label: 'LABEL.TEAM_ID', textKey: EMPL_TEAM.PK, type: ColumnType.STRING, visibility: true, sorting: SortType.NONE },
      { label: 'LABEL.NAME', textKey: 'name', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<EmplTeamListView>> {
    return this.service.getEmplTeamToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteEmplTeam(row));
  }
}
