import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { FieldAccessing } from 'shared/models/systemModel';
import { EmplTeamItemView } from 'shared/models/viewModel';

const firstGroup = ['TEAM_ID'];
export class EmplTeamItemCustomComponent {
  getFieldAccessing(model: EmplTeamItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.emplTeamGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: EmplTeamItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<EmplTeamItemView> {
    let model: EmplTeamItemView = new EmplTeamItemView();
    model.emplTeamGUID = EmptyGuid;
    return of(model);
  }
}
