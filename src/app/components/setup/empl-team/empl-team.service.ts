import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { EmplTeamItemView, EmplTeamListView } from 'shared/models/viewModel';

@Injectable()
export class EmplTeamService {
  serviceKey = 'emplTeamGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getEmplTeamToList(search: SearchParameter): Observable<SearchResult<EmplTeamListView>> {
    const url = `${this.servicePath}/GetEmplTeamList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteEmplTeam(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteEmplTeam`;
    return this.dataGateway.delete(url, row);
  }
  getEmplTeamById(id: string): Observable<EmplTeamItemView> {
    const url = `${this.servicePath}/GetEmplTeamById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createEmplTeam(vmModel: EmplTeamItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateEmplTeam`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateEmplTeam(vmModel: EmplTeamItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateEmplTeam`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
