import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EmplTeamService } from './empl-team.service';
import { EmplTeamListComponent } from './empl-team-list/empl-team-list.component';
import { EmplTeamItemComponent } from './empl-team-item/empl-team-item.component';
import { EmplTeamItemCustomComponent } from './empl-team-item/empl-team-item-custom.component';
import { EmplTeamListCustomComponent } from './empl-team-list/empl-team-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [EmplTeamListComponent, EmplTeamItemComponent],
  providers: [EmplTeamService, EmplTeamItemCustomComponent, EmplTeamListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [EmplTeamListComponent, EmplTeamItemComponent]
})
export class EmplTeamComponentModule {}
