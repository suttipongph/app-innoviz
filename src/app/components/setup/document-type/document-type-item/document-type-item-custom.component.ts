import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { DocumentTypeItemView } from 'shared/models/viewModel';

const firstGroup = ['DOCUMENT_TYPE_ID'];

export class DocumentTypeItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: DocumentTypeItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.documentTypeGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: DocumentTypeItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<DocumentTypeItemView> {
    let model: DocumentTypeItemView = new DocumentTypeItemView();
    model.documentTypeGUID = EmptyGuid;
    return of(model);
  }
}
