import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentTypeItemComponent } from './document-type-item.component';

describe('DocumentTypeItemViewComponent', () => {
  let component: DocumentTypeItemComponent;
  let fixture: ComponentFixture<DocumentTypeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DocumentTypeItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentTypeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
