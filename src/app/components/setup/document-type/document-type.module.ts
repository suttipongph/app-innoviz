import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DocumentTypeService } from './document-type.service';
import { DocumentTypeListComponent } from './document-type-list/document-type-list.component';
import { DocumentTypeItemComponent } from './document-type-item/document-type-item.component';
import { DocumentTypeItemCustomComponent } from './document-type-item/document-type-item-custom.component';
import { DocumentTypeListCustomComponent } from './document-type-list/document-type-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [DocumentTypeListComponent, DocumentTypeItemComponent],
  providers: [DocumentTypeService, DocumentTypeItemCustomComponent, DocumentTypeListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [DocumentTypeListComponent, DocumentTypeItemComponent]
})
export class DocumentTypeComponentModule {}
