import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { DocumentTypeItemView, DocumentTypeListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class DocumentTypeService {
  serviceKey = 'documentTypeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getDocumentTypeToList(search: SearchParameter): Observable<SearchResult<DocumentTypeListView>> {
    const url = `${this.servicePath}/GetDocumentTypeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteDocumentType(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteDocumentType`;
    return this.dataGateway.delete(url, row);
  }
  getDocumentTypeById(id: string): Observable<DocumentTypeItemView> {
    const url = `${this.servicePath}/GetDocumentTypeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createDocumentType(vmModel: DocumentTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateDocumentType`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateDocumentType(vmModel: DocumentTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateDocumentType`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
