import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessCollateralSubTypeListComponent } from './business-collateral-sub-type-list.component';

describe('BusinessCollateralSubTypeListViewComponent', () => {
  let component: BusinessCollateralSubTypeListComponent;
  let fixture: ComponentFixture<BusinessCollateralSubTypeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BusinessCollateralSubTypeListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCollateralSubTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
