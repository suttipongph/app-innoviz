import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, ROUTE_MASTER_GEN, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { BusinessCollateralSubTypeListView } from 'shared/models/viewModel';
import { BusinessCollateralSubTypeService } from '../business-collateral-sub-type.service';
import { BusinessCollateralSubTypeListCustomComponent } from './business-collateral-sub-type-list-custom.component';

@Component({
  selector: 'business-collateral-sub-type-list',
  templateUrl: './business-collateral-sub-type-list.component.html',
  styleUrls: ['./business-collateral-sub-type-list.component.scss']
})
export class BusinessCollateralSubTypeListComponent extends BaseListComponent<BusinessCollateralSubTypeListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  defaultBitOptions: SelectItems[] = [];
  businessCollateralTypeOptions: SelectItems[] = [];
  constructor(public custom: BusinessCollateralSubTypeListCustomComponent, private service: BusinessCollateralSubTypeService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getKey('businesscollateraltype');
  }
  ngOnInit(): void {
    this.accessRightChildPage = 'BusinessCollateralSubTypeListPage';
    this.redirectPath = ROUTE_MASTER_GEN.BUSINESS_COLLATERAL_SUB_TYPE;
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getDefaultBitDropDown(this.defaultBitOptions);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBusinessCollateralTypeDropDown(this.businessCollateralTypeOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'businessCollateralSubTypeGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.BUSINESS_COLLATERAL_TYPE_ID',
        textKey: 'businessCollateralType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'businessCollateralTypeGUID',
        sortingKey: 'businessCollateralTypeId',
        masterList: this.businessCollateralTypeOptions,
        disabledFilter: true
      },
      {
        label: 'LABEL.BUSINESS_COLLATERAL_SUBTYPE_ID',
        textKey: 'businessCollateralSubTypeId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DEBTOR_CLAIMS',
        textKey: 'debtorClaims',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defaultBitOptions
      },
      {
        label: 'LABEL.AGREEMENT_ORDERING',
        textKey: 'agreementOrdering',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'businessCollateralTypeGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<BusinessCollateralSubTypeListView>> {
    return this.service.getBusinessCollateralSubTypeToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteBusinessCollateralSubType(row));
  }
}
