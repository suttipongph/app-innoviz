import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BusinessCollateralSubTypeService } from './business-collateral-sub-type.service';
import { BusinessCollateralSubTypeListComponent } from './business-collateral-sub-type-list/business-collateral-sub-type-list.component';
import { BusinessCollateralSubTypeItemComponent } from './business-collateral-sub-type-item/business-collateral-sub-type-item.component';
import { BusinessCollateralSubTypeItemCustomComponent } from './business-collateral-sub-type-item/business-collateral-sub-type-item-custom.component';
import { BusinessCollateralSubTypeListCustomComponent } from './business-collateral-sub-type-list/business-collateral-sub-type-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BusinessCollateralSubTypeListComponent, BusinessCollateralSubTypeItemComponent],
  providers: [BusinessCollateralSubTypeService, BusinessCollateralSubTypeItemCustomComponent, BusinessCollateralSubTypeListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BusinessCollateralSubTypeListComponent, BusinessCollateralSubTypeItemComponent]
})
export class BusinessCollateralSubTypeComponentModule {}
