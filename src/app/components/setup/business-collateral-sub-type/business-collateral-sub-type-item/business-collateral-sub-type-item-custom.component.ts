import { AppInjector } from 'app-injector';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { BusinessCollateralSubTypeItemView } from 'shared/models/viewModel';
import { BusinessCollateralSubTypeService } from '../business-collateral-sub-type.service';

const firstGroup = ['BUSINESS_COLLATERAL_TYPE_GUID', 'BUSINESS_COLLATERAL_SUB_TYPE_ID'];
const secondGroup = ['BUSINESS_COLLATERAL_TYPE_GUID'];
export class BusinessCollateralSubTypeItemCustomComponent {
  baseService: BaseServiceModel<any>;
  public uiControllerService = AppInjector.get(UIControllerService);
  getFieldAccessing(model: BusinessCollateralSubTypeItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.businessCollateralSubTypeGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    } else {
      fieldAccessing.push({ filedIds: secondGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BusinessCollateralSubTypeItemView): Observable<boolean> {
    return of(!canCreate);
  }
  setDefualtValue(businessCollateralSubTypeService: BusinessCollateralSubTypeService, model: BusinessCollateralSubTypeItemView): void {
    model.businessCollateralTypeGUID = this.uiControllerService.getKey('businesscollateraltype');
  }
  getInitialData(): Observable<BusinessCollateralSubTypeItemView> {
    let model: BusinessCollateralSubTypeItemView = new BusinessCollateralSubTypeItemView();
    model.businessCollateralSubTypeGUID = EmptyGuid;
    return of(model);
  }
}
