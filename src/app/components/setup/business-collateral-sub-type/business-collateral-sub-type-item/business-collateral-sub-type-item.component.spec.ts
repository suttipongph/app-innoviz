import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessCollateralSubTypeItemComponent } from './business-collateral-sub-type-item.component';

describe('BusinessCollateralSubTypeItemViewComponent', () => {
  let component: BusinessCollateralSubTypeItemComponent;
  let fixture: ComponentFixture<BusinessCollateralSubTypeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BusinessCollateralSubTypeItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCollateralSubTypeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
