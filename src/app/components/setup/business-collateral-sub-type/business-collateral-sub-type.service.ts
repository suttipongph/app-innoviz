import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { BusinessCollateralSubTypeItemView, BusinessCollateralSubTypeListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class BusinessCollateralSubTypeService {
  serviceKey = 'businessCollateralSubTypeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBusinessCollateralSubTypeToList(search: SearchParameter): Observable<SearchResult<BusinessCollateralSubTypeListView>> {
    const url = `${this.servicePath}/GetBusinessCollateralSubTypeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBusinessCollateralSubType(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBusinessCollateralSubType`;
    return this.dataGateway.delete(url, row);
  }
  getBusinessCollateralSubTypeById(id: string): Observable<BusinessCollateralSubTypeItemView> {
    const url = `${this.servicePath}/GetBusinessCollateralSubTypeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBusinessCollateralSubType(vmModel: BusinessCollateralSubTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBusinessCollateralSubType`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBusinessCollateralSubType(vmModel: BusinessCollateralSubTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBusinessCollateralSubType`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  Getinitialdata(businessCollateralTypeGUID: string): Observable<BusinessCollateralSubTypeItemView> {
    const url = `${this.servicePath}/GetBusinessCollateralSubTypeInitialdata/businessCollateralTypeGUID=${businessCollateralTypeGUID}`;
    return this.dataGateway.get(url);
  }
}
