import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditTypeListComponent } from './credit-type-list.component';

describe('CreditTypeListViewComponent', () => {
  let component: CreditTypeListComponent;
  let fixture: ComponentFixture<CreditTypeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditTypeListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
