import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreditTypeService } from './credit-type.service';
import { CreditTypeListComponent } from './credit-type-list/credit-type-list.component';
import { CreditTypeItemComponent } from './credit-type-item/credit-type-item.component';
import { CreditTypeItemCustomComponent } from './credit-type-item/credit-type-item-custom.component';
import { CreditTypeListCustomComponent } from './credit-type-list/credit-type-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CreditTypeListComponent, CreditTypeItemComponent],
  providers: [CreditTypeService, CreditTypeItemCustomComponent, CreditTypeListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CreditTypeListComponent, CreditTypeItemComponent]
})
export class CreditTypeComponentModule {}
