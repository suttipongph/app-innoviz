import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { CreditTypeItemView } from 'shared/models/viewModel/creditTypeItemView';
import { CreditTypeListView } from 'shared/models/viewModel/creditTypeListView';
@Injectable()
export class CreditTypeService {
  serviceKey = 'creditTypeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCreditTypeToList(search: SearchParameter): Observable<SearchResult<CreditTypeListView>> {
    const url = `${this.servicePath}/GetCreditTypeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCreditType(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCreditType`;
    return this.dataGateway.delete(url, row);
  }
  getCreditTypeById(id: string): Observable<CreditTypeItemView> {
    const url = `${this.servicePath}/GetCreditTypeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCreditType(vmModel: CreditTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCreditType`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCreditType(vmModel: CreditTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCreditType`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
