import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CreditTypeItemView } from 'shared/models/viewModel/creditTypeItemView';

const firstGroup = ['CREDIT_TYPE_ID'];

export class CreditTypeItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CreditTypeItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.creditTypeGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CreditTypeItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<CreditTypeItemView> {
    let model: CreditTypeItemView = new CreditTypeItemView();
    model.creditTypeGUID = EmptyGuid;
    return of(model);
  }
}
