import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditTypeItemComponent } from './credit-type-item.component';

describe('CreditTypeItemViewComponent', () => {
  let component: CreditTypeItemComponent;
  let fixture: ComponentFixture<CreditTypeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditTypeItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditTypeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
