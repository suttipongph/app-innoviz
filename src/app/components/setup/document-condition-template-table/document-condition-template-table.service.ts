import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { DocumentConditionTemplateTableItemView, DocumentConditionTemplateTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class DocumentConditionTemplateTableService {
  serviceKey = 'documentConditionTemplateTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getDocumentConditionTemplateTableToList(search: SearchParameter): Observable<SearchResult<DocumentConditionTemplateTableListView>> {
    const url = `${this.servicePath}/GetDocumentConditionTemplateTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteDocumentConditionTemplateTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteDocumentConditionTemplateTable`;
    return this.dataGateway.delete(url, row);
  }
  getDocumentConditionTemplateTableById(id: string): Observable<DocumentConditionTemplateTableItemView> {
    const url = `${this.servicePath}/GetDocumentConditionTemplateTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createDocumentConditionTemplateTable(vmModel: DocumentConditionTemplateTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateDocumentConditionTemplateTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateDocumentConditionTemplateTable(vmModel: DocumentConditionTemplateTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateDocumentConditionTemplateTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  validateIsManualNumberSeq(): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeq`;
    return this.dataGateway.get(url);
  }
}
