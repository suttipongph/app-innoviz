import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DocumentConditionTemplateTableService } from './document-condition-template-table.service';
import { DocumentConditionTemplateTableListComponent } from './document-condition-template-table-list/document-condition-template-table-list.component';
import { DocumentConditionTemplateTableItemComponent } from './document-condition-template-table-item/document-condition-template-table-item.component';
import { DocumentConditionTemplateTableItemCustomComponent } from './document-condition-template-table-item/document-condition-template-table-item-custom.component';
import { DocumentConditionTemplateTableListCustomComponent } from './document-condition-template-table-list/document-condition-template-table-list-custom.component';
import { DocumentConditionTemplateLineComponentModule } from '../document-condition-template-line/document-condition-template-line.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, DocumentConditionTemplateLineComponentModule],
  declarations: [DocumentConditionTemplateTableListComponent, DocumentConditionTemplateTableItemComponent],
  providers: [
    DocumentConditionTemplateTableService,
    DocumentConditionTemplateTableItemCustomComponent,
    DocumentConditionTemplateTableListCustomComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [DocumentConditionTemplateTableListComponent, DocumentConditionTemplateTableItemComponent]
})
export class DocumentConditionTemplateTableComponentModule {}
