import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DocumentConditionTemplateTableListComponent } from './document-condition-template-table-list.component';

describe('DocumentConditionTemplateTableListViewComponent', () => {
  let component: DocumentConditionTemplateTableListComponent;
  let fixture: ComponentFixture<DocumentConditionTemplateTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DocumentConditionTemplateTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentConditionTemplateTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
