import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { DocumentConditionTemplateTableListView } from 'shared/models/viewModel';
import { DocumentConditionTemplateTableService } from '../document-condition-template-table.service';
import { DocumentConditionTemplateTableListCustomComponent } from './document-condition-template-table-list-custom.component';

@Component({
  selector: 'document-condition-template-table-list',
  templateUrl: './document-condition-template-table-list.component.html',
  styleUrls: ['./document-condition-template-table-list.component.scss']
})
export class DocumentConditionTemplateTableListComponent
  extends BaseListComponent<DocumentConditionTemplateTableListView>
  implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  constructor(public custom: DocumentConditionTemplateTableListCustomComponent, private service: DocumentConditionTemplateTableService) {
    super();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'documentConditionTemplateTableGUID';
    const columns: ColumnModel[] = [
      {
        label: this.translate.instant('LABEL.DOCUMENT_CONDITION_TEMPLATE_ID'),
        textKey: 'documentConditionTemplateTableId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: this.translate.instant('LABEL.DESCRIPTION'),
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<DocumentConditionTemplateTableListView>> {
    return this.service.getDocumentConditionTemplateTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteDocumentConditionTemplateTable(row));
  }
}
