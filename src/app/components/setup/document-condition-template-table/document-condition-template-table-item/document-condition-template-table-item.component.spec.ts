import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentConditionTemplateTableItemComponent } from './document-condition-template-table-item.component';

describe('DocumentConditionTemplateTableItemViewComponent', () => {
  let component: DocumentConditionTemplateTableItemComponent;
  let fixture: ComponentFixture<DocumentConditionTemplateTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DocumentConditionTemplateTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentConditionTemplateTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
