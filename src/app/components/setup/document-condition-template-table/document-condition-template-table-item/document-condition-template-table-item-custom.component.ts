import { AppInjector } from 'app-injector';
import { NotificationService } from 'core/services/notification.service';
import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { FieldAccessing } from 'shared/models/systemModel';
import { BuyerAgreementTableItemView, DocumentConditionTemplateTableItemView } from 'shared/models/viewModel';
import { DocumentConditionTemplateTableService } from '../document-condition-template-table.service';

const firstGroup = ['DOCUMENT_CONDITION_TEMPLATE_TABLE_ID'];

export class DocumentConditionTemplateTableItemCustomComponent {
  notificationService = AppInjector.get(NotificationService);
  isManunal: boolean = false;
  // getFieldAccessing(model: DocumentConditionTemplateTableItemView): Observable<FieldAccessing[]> {
  //   const fieldAccessing: FieldAccessing[] = [];
  //   if (isUpdateMode(model.documentConditionTemplateTableGUID)) {
  //     fieldAccessing.push({ filedIds: firstGroup, readonly: true });
  //     return of(fieldAccessing);
  //   }
  // }
  getFieldAccessing(
    model: DocumentConditionTemplateTableItemView,
    documentConditionTemplateTableService: DocumentConditionTemplateTableService
  ): Observable<any> {
    let fieldAccessing: FieldAccessing[] = [];
    return new Observable((observer) => {
      // set firstGroup
      if (!isUpdateMode(model.documentConditionTemplateTableGUID)) {
        this.validateIsManualNumberSeq(documentConditionTemplateTableService).subscribe(
          (result) => {
            this.isManunal = result;
            fieldAccessing.push({ filedIds: firstGroup, readonly: !this.isManunal });
            observer.next(fieldAccessing);
          },
          (error) => {
            this.notificationService.showErrorMessageFromResponse(error);
          }
        );
      } else {
        fieldAccessing.push({ filedIds: firstGroup, readonly: true });
        observer.next(fieldAccessing);
      }
    });
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: DocumentConditionTemplateTableItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<DocumentConditionTemplateTableItemView> {
    const model: DocumentConditionTemplateTableItemView = new DocumentConditionTemplateTableItemView();
    model.documentConditionTemplateTableGUID = EmptyGuid;
    return of(model);
  }
  validateIsManualNumberSeq(documentConditionTemplateTableService: DocumentConditionTemplateTableService): Observable<boolean> {
    return documentConditionTemplateTableService.validateIsManualNumberSeq();
  }
}
