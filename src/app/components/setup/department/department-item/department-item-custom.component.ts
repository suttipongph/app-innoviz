import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { DepartmentItemView } from 'shared/models/viewModel';

const firstGroup = ['DEPARTMENT_ID'];

export class DepartmentItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: DepartmentItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.departmentGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: DepartmentItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<DepartmentItemView> {
    let model: DepartmentItemView = new DepartmentItemView();
    model.departmentGUID = EmptyGuid;
    return of(model);
  }
}
