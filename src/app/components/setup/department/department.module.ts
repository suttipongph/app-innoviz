import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DepartmentService } from './department.service';
import { DepartmentListComponent } from './department-list/department-list.component';
import { DepartmentItemComponent } from './department-item/department-item.component';
import { DepartmentItemCustomComponent } from './department-item/department-item-custom.component';
import { DepartmentListCustomComponent } from './department-list/department-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [DepartmentListComponent, DepartmentItemComponent],
  providers: [DepartmentService, DepartmentItemCustomComponent, DepartmentListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [DepartmentListComponent, DepartmentItemComponent]
})
export class DepartmentComponentModule {}
