import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { DepartmentItemView, DepartmentListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class DepartmentService {
  serviceKey = 'departmentGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getDepartmentToList(search: SearchParameter): Observable<SearchResult<DepartmentListView>> {
    const url = `${this.servicePath}/GetDepartmentList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteDepartment(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteDepartment`;
    return this.dataGateway.delete(url, row);
  }
  getDepartmentById(id: string): Observable<DepartmentItemView> {
    const url = `${this.servicePath}/GetDepartmentById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createDepartment(vmModel: DepartmentItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateDepartment`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateDepartment(vmModel: DepartmentItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateDepartment`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
