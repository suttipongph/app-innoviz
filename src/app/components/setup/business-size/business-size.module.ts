import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BusinessSizeService } from './business-size.service';
import { BusinessSizeListComponent } from './business-size-list/business-size-list.component';
import { BusinessSizeItemComponent } from './business-size-item/business-size-item.component';
import { BusinessSizeItemCustomComponent } from './business-size-item/business-size-item-custom.component';
import { BusinessSizeListCustomComponent } from './business-size-list/business-size-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BusinessSizeListComponent, BusinessSizeItemComponent],
  providers: [BusinessSizeService, BusinessSizeItemCustomComponent, BusinessSizeListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BusinessSizeListComponent, BusinessSizeItemComponent]
})
export class BusinessSizeComponentModule {}
