import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessSizeItemComponent } from './business-size-item.component';

describe('BusinessSizeItemViewComponent', () => {
  let component: BusinessSizeItemComponent;
  let fixture: ComponentFixture<BusinessSizeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BusinessSizeItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessSizeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
