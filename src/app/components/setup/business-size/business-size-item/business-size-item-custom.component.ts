import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { BusinessSizeItemView } from 'shared/models/viewModel';

const firstGroup = ['BUSINESS_SIZE_ID'];

export class BusinessSizeItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: BusinessSizeItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.businessSizeGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BusinessSizeItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<BusinessSizeItemView> {
    let model: BusinessSizeItemView = new BusinessSizeItemView();
    model.businessSizeGUID = EmptyGuid;
    return of(model);
  }
}
