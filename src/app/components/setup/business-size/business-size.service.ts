import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BusinessSizeItemView, BusinessSizeListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class BusinessSizeService {
  serviceKey = 'businessSizeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBusinessSizeToList(search: SearchParameter): Observable<SearchResult<BusinessSizeListView>> {
    const url = `${this.servicePath}/GetBusinessSizeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBusinessSize(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBusinessSize`;
    return this.dataGateway.delete(url, row);
  }
  getBusinessSizeById(id: string): Observable<BusinessSizeItemView> {
    const url = `${this.servicePath}/GetBusinessSizeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBusinessSize(vmModel: BusinessSizeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBusinessSize`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBusinessSize(vmModel: BusinessSizeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBusinessSize`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
