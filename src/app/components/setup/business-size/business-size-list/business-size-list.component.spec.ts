import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessSizeListComponent } from './business-size-list.component';

describe('BusinessSizeListViewComponent', () => {
  let component: BusinessSizeListComponent;
  let fixture: ComponentFixture<BusinessSizeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BusinessSizeListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessSizeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
