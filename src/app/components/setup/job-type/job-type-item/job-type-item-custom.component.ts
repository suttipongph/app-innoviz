import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { JobTypeItemView } from 'shared/models/viewModel';

const firstGroup = ['JOB_TYPE_ID'];

export class JobTypeItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: JobTypeItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.jobTypeGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }

    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: JobTypeItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<JobTypeItemView> {
    let model = new JobTypeItemView();
    model.jobTypeGUID = EmptyGuid;
    return of(model);
  }
}
