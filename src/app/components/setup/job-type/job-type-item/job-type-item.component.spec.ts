import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobTypeItemComponent } from './job-type-item.component';

describe('JobTypeItemViewComponent', () => {
  let component: JobTypeItemComponent;
  let fixture: ComponentFixture<JobTypeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [JobTypeItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobTypeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
