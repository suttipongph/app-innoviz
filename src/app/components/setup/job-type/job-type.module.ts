import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JobTypeService } from './job-type.service';
import { JobTypeListComponent } from './job-type-list/job-type-list.component';
import { JobTypeItemComponent } from './job-type-item/job-type-item.component';
import { JobTypeItemCustomComponent } from './job-type-item/job-type-item-custom.component';
import { JobTypeListCustomComponent } from './job-type-list/job-type-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [JobTypeListComponent, JobTypeItemComponent],
  providers: [JobTypeService, JobTypeItemCustomComponent, JobTypeListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [JobTypeListComponent, JobTypeItemComponent]
})
export class JobTypeComponentModule {}
