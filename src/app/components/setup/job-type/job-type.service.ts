import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { JobTypeItemView, JobTypeListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class JobTypeService {
  serviceKey = 'jobTypeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getJobTypeToList(search: SearchParameter): Observable<SearchResult<JobTypeListView>> {
    const url = `${this.servicePath}/GetJobTypeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteJobType(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteJobType`;
    return this.dataGateway.delete(url, row);
  }
  getJobTypeById(id: string): Observable<JobTypeItemView> {
    const url = `${this.servicePath}/GetJobTypeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createJobType(vmModel: JobTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateJobType`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateJobType(vmModel: JobTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateJobType`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
