import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { JobTypeListView } from 'shared/models/viewModel';
import { JobTypeService } from '../job-type.service';
import { JobTypeListCustomComponent } from './job-type-list-custom.component';

@Component({
  selector: 'job-type-list',
  templateUrl: './job-type-list.component.html',
  styleUrls: ['./job-type-list.component.scss']
})
export class JobTypeListComponent extends BaseListComponent<JobTypeListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  showDocConVerifyTypeOptions: SelectItems[] = [];
  assignmentOptions: SelectItems[] = [];

  constructor(public custom: JobTypeListCustomComponent, private service: JobTypeService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getShowDocConVerifyTypeEnumDropDown(this.showDocConVerifyTypeOptions);
    this.baseDropdown.getDefaultBitDropDown(this.assignmentOptions);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'jobTypeGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.JOB_TYPE_ID',
        textKey: 'jobTypeId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.SHOW_DOC_CON_VERIFY_TYPE',
        textKey: 'showDocConVerifyType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.showDocConVerifyTypeOptions
      },
      {
        label: 'LABEL.ASSIGNMENT',
        textKey: 'assignment',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.assignmentOptions
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<JobTypeListView>> {
    return this.service.getJobTypeToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteJobType(row));
  }
}
