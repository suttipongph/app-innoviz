import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { JobTypeListComponent } from './job-type-list.component';

describe('JobTypeListViewComponent', () => {
  let component: JobTypeListComponent;
  let fixture: ComponentFixture<JobTypeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [JobTypeListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
