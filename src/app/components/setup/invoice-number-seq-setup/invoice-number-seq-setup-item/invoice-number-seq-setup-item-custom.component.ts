import { Observable, of } from 'rxjs';
import { isNullOrUndefOrEmpty, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { InvoiceNumberSeqSetupItemView, InvoiceTypeItemView } from 'shared/models/viewModel';

const firstGroup = ['BRANCH_ID', 'PRODUCT_TYPE', 'INVOICE_TYPE_ID'];
export class InvoiceNumberSeqSetupItemCustomComponent {
  requireAutoGen: boolean = false;
  getFieldAccessing(model: InvoiceNumberSeqSetupItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.invoiceNumberSeqSetupGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: InvoiceNumberSeqSetupItemView): Observable<boolean> {
    return of(!canCreate);
  }

  // field change ##############################################################
  onInvoiceChanged(model: InvoiceNumberSeqSetupItemView, invoiceTypeOptions: SelectItems[]) {
    this.setinvRevenueType(model, invoiceTypeOptions);
  }

  setinvRevenueType(invoiceTypeGUID: any, invoiceTypeOptions: SelectItems[]) {
    if (!isNullOrUndefOrEmpty(invoiceTypeGUID)) {
      const InvoiceType = invoiceTypeOptions.find((o) => o.value === invoiceTypeGUID).rowData as InvoiceTypeItemView;
      this.requireAutoGen = InvoiceType.autoGen;
    } else {
      this.requireAutoGen = false;
    }

    // this.requireAutoGen = rowData.autoGen;

    // if (!isNullOrUndefOrEmpty(model.invoiceTypeGUID)) {
    // } else {
    // }
  }

  // // validate field ############################################################
  // onInvoiceChanged(e) {
  //   if (!isNullOrUndefOrEmpty(e.value)) {
  //     const InvoiceType = this.invoiceTypeOption.find((o) => o.value === e.value).rowData as InvoiceTypeView;
  //     this.requireAutoGen = InvoiceType.autoGen;
  //   } else {
  //     this.requireAutoGen = false;
  //   }
  // }

  // onValidateAutoGen(model: InvoiceNumberSeqSetupItemView) {
  //   if (this.requireAutoGen && isNullOrUndefOrEmpty(model.autoGenInvRevenueTypeGUID)) {
  //     return {
  //       code: 'ERROR.MANDATORY_FIELD',
  //       parameters: []
  //     };
  //   } else {
  //     return null;
  //   }
  // } R02 issue 0020141
}
