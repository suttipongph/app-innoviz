import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmpty, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { InvoiceNumberSeqSetupItemView } from 'shared/models/viewModel';
import { InvoiceTypeItemView } from 'shared/models/viewModel';
import { InvoiceNumberSeqSetupService } from '../invoice-number-seq-setup.service';
import { InvoiceNumberSeqSetupItemCustomComponent } from './invoice-number-seq-setup-item-custom.component';

@Component({
  selector: 'invoice-number-seq-setup-item',
  templateUrl: './invoice-number-seq-setup-item.component.html',
  styleUrls: ['./invoice-number-seq-setup-item.component.scss']
})
export class InvoiceNumberSeqSetupItemComponent extends BaseItemComponent<InvoiceNumberSeqSetupItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  numberSeqTableOptions: SelectItems[] = [];
  branchOptions: SelectItems[] = [];
  invoiceTypeOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];

  constructor(
    private service: InvoiceNumberSeqSetupService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: InvoiceNumberSeqSetupItemCustomComponent
  ) {
    super();
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
  }
  getById(): Observable<InvoiceNumberSeqSetupItemView> {
    return this.service.getInvoiceNumberSeqSetupById(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.onAsyncRunner();
    this.model.invoiceNumberSeqSetupGUID = EmptyGuid;
    super.setDefaultValueSystemFields();
  }
  onAsyncRunner(model?: any): void {
    forkJoin(
      this.baseDropdown.getNumberSeqTableDropDown(),
      this.baseDropdown.getBranchToDropDown(),
      this.baseDropdown.getInvoiceTypeDropDown()
    ).subscribe(
      ([numberSeqTable, branch, invoiceType]) => {
        this.numberSeqTableOptions = numberSeqTable;
        this.branchOptions = branch;
        this.invoiceTypeOptions = invoiceType;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {}
  setFunctionOptions(): void {}
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateInvoiceNumberSeqSetup(this.model), isColsing);
    } else {
      super.onCreate(this.service.createInvoiceNumberSeqSetup(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }

  onInvoiceChanged(e): void {
    this.custom.onInvoiceChanged(e, this.invoiceTypeOptions);
  }
}
