import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { InvoiceNumberSeqSetupItemView, InvoiceNumberSeqSetupListView } from 'shared/models/viewModel';

@Injectable()
export class InvoiceNumberSeqSetupService {
  serviceKey = 'invoiceNumberSeqSetupGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getInvoiceNumberSeqSetupToList(search: SearchParameter): Observable<SearchResult<InvoiceNumberSeqSetupListView>> {
    const url = `${this.servicePath}/GetInvoiceNumberSeqSetupList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteInvoiceNumberSeqSetup(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteInvoiceNumberSeqSetup`;
    return this.dataGateway.delete(url, row);
  }
  getInvoiceNumberSeqSetupById(id: string): Observable<InvoiceNumberSeqSetupItemView> {
    const url = `${this.servicePath}/GetInvoiceNumberSeqSetupById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createInvoiceNumberSeqSetup(vmModel: InvoiceNumberSeqSetupItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateInvoiceNumberSeqSetup`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateInvoiceNumberSeqSetup(vmModel: InvoiceNumberSeqSetupItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateInvoiceNumberSeqSetup`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
