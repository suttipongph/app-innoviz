import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InvoiceNumberSeqSetupService } from './invoice-number-seq-setup.service';
import { InvoiceNumberSeqSetupListComponent } from './invoice-number-seq-setup-list/invoice-number-seq-setup-list.component';
import { InvoiceNumberSeqSetupItemComponent } from './invoice-number-seq-setup-item/invoice-number-seq-setup-item.component';
import { InvoiceNumberSeqSetupItemCustomComponent } from './invoice-number-seq-setup-item/invoice-number-seq-setup-item-custom.component';
import { InvoiceNumberSeqSetupListCustomComponent } from './invoice-number-seq-setup-list/invoice-number-seq-setup-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [InvoiceNumberSeqSetupListComponent, InvoiceNumberSeqSetupItemComponent],
  providers: [InvoiceNumberSeqSetupService, InvoiceNumberSeqSetupItemCustomComponent, InvoiceNumberSeqSetupListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [InvoiceNumberSeqSetupListComponent, InvoiceNumberSeqSetupItemComponent]
})
export class InvoiceNumberSeqSetupComponentModule {}
