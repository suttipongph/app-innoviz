import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { InvoiceNumberSeqSetupListView } from 'shared/models/viewModel';
import { InvoiceNumberSeqSetupService } from '../invoice-number-seq-setup.service';
import { InvoiceNumberSeqSetupListCustomComponent } from './invoice-number-seq-setup-list-custom.component';

@Component({
  selector: 'invoice-number-seq-setup-list',
  templateUrl: './invoice-number-seq-setup-list.component.html',
  styleUrls: ['./invoice-number-seq-setup-list.component.scss']
})
export class InvoiceNumberSeqSetupListComponent extends BaseListComponent<InvoiceNumberSeqSetupListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  numberSeqTableOptions: SelectItems[] = [];
  branchOptions: SelectItems[] = [];
  invoiceTypeOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  constructor(private service: InvoiceNumberSeqSetupService, public custom: InvoiceNumberSeqSetupListCustomComponent) {
    super();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.setDataGridOption();
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getNumberSeqTableDropDown(this.numberSeqTableOptions);
    this.baseDropdown.getBranchToDropDown(this.branchOptions);
    this.baseDropdown.getInvoiceTypeDropDown(this.invoiceTypeOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'invoiceNumberSeqSetupGUID';
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.BRANCH_ID',
        textKey: 'branch_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.branchOptions,
        searchingKey: 'branchGUID',
        sortingKey: 'branch_BranchId'
      },
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: 'LABEL.INVOICE_TYPE_ID',
        textKey: 'invoiceType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.invoiceTypeOptions,
        searchingKey: 'invoiceTypeGUID',
        sortingKey: 'invoiceType_InvoiceTypeId'
      },
      {
        label: 'LABEL.NUMBER_SEQ_CODE',
        textKey: 'numberSeqTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.numberSeqTableOptions,
        searchingKey: 'numberSeqTableGUID',
        sortingKey: 'numberSeqTable_NumberSeqCode'
      }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<InvoiceNumberSeqSetupListView>> {
    return this.service.getInvoiceNumberSeqSetupToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteInvoiceNumberSeqSetup(row));
  }
}
