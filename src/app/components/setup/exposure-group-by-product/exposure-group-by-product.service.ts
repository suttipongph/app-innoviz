import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ExposureGroupByProductItemView, ExposureGroupByProductListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ExposureGroupByProductService {
  serviceKey = 'exposureGroupByProductGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.servicePath = param.servicePath;
  }
  getExposureGroupByProductToList(search: SearchParameter): Observable<SearchResult<ExposureGroupByProductListView>> {
    const url = `${this.servicePath}/GetExposureGroupByProductList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteExposureGroupByProduct(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteExposureGroupByProduct`;
    return this.dataGateway.delete(url, row);
  }
  getExposureGroupByProductById(id: string): Observable<ExposureGroupByProductItemView> {
    const url = `${this.servicePath}/GetExposureGroupByProductById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createExposureGroupByProduct(vmModel: ExposureGroupByProductItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateExposureGroupByProduct`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateExposureGroupByProduct(vmModel: ExposureGroupByProductItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateExposureGroupByProduct`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<ExposureGroupByProductItemView> {
    const url = `${this.servicePath}/GetExposureGroupByProductInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
}
