import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExposureGroupByProductService } from './exposure-group-by-product.service';
import { ExposureGroupByProductListComponent } from './exposure-group-by-product-list/exposure-group-by-product-list.component';
import { ExposureGroupByProductItemComponent } from './exposure-group-by-product-item/exposure-group-by-product-item.component';
import { ExposureGroupByProductItemCustomComponent } from './exposure-group-by-product-item/exposure-group-by-product-item-custom.component';
import { ExposureGroupByProductListCustomComponent } from './exposure-group-by-product-list/exposure-group-by-product-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ExposureGroupByProductListComponent, ExposureGroupByProductItemComponent],
  providers: [ExposureGroupByProductService, ExposureGroupByProductItemCustomComponent, ExposureGroupByProductListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ExposureGroupByProductListComponent, ExposureGroupByProductItemComponent]
})
export class ExposureGroupByProductComponentModule {}
