import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExposureGroupByProductItemComponent } from './exposure-group-by-product-item.component';

describe('ExposureGroupByProductItemViewComponent', () => {
  let component: ExposureGroupByProductItemComponent;
  let fixture: ComponentFixture<ExposureGroupByProductItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExposureGroupByProductItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExposureGroupByProductItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
