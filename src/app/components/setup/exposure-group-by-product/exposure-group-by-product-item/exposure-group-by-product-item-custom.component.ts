import { Observable, of } from 'rxjs';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { ExposureGroupByProductItemView } from 'shared/models/viewModel';

const firstGroup = ['EXPOSURE_GROUP_GUID'];

export class ExposureGroupByProductItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: ExposureGroupByProductItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ExposureGroupByProductItemView): Observable<boolean> {
    return of(!canCreate);
  }
}
