import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ExposureGroupByProductListComponent } from './exposure-group-by-product-list.component';

describe('ExposureGroupByProductListViewComponent', () => {
  let component: ExposureGroupByProductListComponent;
  let fixture: ComponentFixture<ExposureGroupByProductListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExposureGroupByProductListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExposureGroupByProductListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
