import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, ROUTE_MASTER_GEN, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { ExposureGroupByProductListView } from 'shared/models/viewModel';
import { ExposureGroupByProductService } from '../exposure-group-by-product.service';
import { ExposureGroupByProductListCustomComponent } from './exposure-group-by-product-list-custom.component';

@Component({
  selector: 'exposure-group-by-product-list',
  templateUrl: './exposure-group-by-product-list.component.html',
  styleUrls: ['./exposure-group-by-product-list.component.scss']
})
export class ExposureGroupByProductListComponent extends BaseListComponent<ExposureGroupByProductListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  productTypeOptions: SelectItems[] = [];

  constructor(
    public custom: ExposureGroupByProductListCustomComponent,
    private service: ExposureGroupByProductService,
    private uiControllerService: UIControllerService
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiControllerService.getKey('exposuregroup');
  }
  ngOnInit(): void {
    this.accessRightChildPage = 'ExposureGroupByProductListPage';
    this.redirectPath = ROUTE_MASTER_GEN.EXPOSURE_GROUP_BY_PRODUCT;
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'exposureGroupByProductGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.EXPOSURE_GROUP_ID',
        textKey: 'exposureGroup_ExposureGroupId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: 'LABEL.EXPOSURE_AMOUNT',
        textKey: 'exposureAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: null,
        textKey: 'exposureGroupGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<ExposureGroupByProductListView>> {
    return this.service.getExposureGroupByProductToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteExposureGroupByProduct(row));
  }
}
