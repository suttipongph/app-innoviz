import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { FieldAccessing } from 'shared/models/systemModel';
import { BusinessUnitItemView } from 'shared/models/viewModel';
import { isUpdateMode } from 'shared/functions/value.function';

const firstGroup = ['BUSINESS_UNIT_ID_INPUT'];

export class BusinessUnitItemCustomComponent {
  getFieldAccessing(model: BusinessUnitItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.businessUnitGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BusinessUnitItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<BusinessUnitItemView> {
    let model = new BusinessUnitItemView();
    model.businessUnitGUID = EmptyGuid;
    return of(model);
  }
}
