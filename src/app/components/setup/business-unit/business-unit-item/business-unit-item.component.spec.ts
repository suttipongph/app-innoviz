import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessUnitItemComponent } from './business-unit-item.component';

describe('BusinessUnitItemViewComponent', () => {
  let component: BusinessUnitItemComponent;
  let fixture: ComponentFixture<BusinessUnitItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BusinessUnitItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessUnitItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
