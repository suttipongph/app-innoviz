import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BusinessUnitItemView, BusinessUnitListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class BusinessUnitService {
  serviceKey = 'businessUnitGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBusinessUnitToList(search: SearchParameter): Observable<SearchResult<BusinessUnitListView>> {
    const url = `${this.servicePath}/GetBusinessUnitList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBusinessUnit(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBusinessUnit`;
    return this.dataGateway.delete(url, row);
  }
  getBusinessUnitById(id: string): Observable<BusinessUnitItemView> {
    const url = `${this.servicePath}/GetBusinessUnitById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBusinessUnit(vmModel: BusinessUnitItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBusinessUnit`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBusinessUnit(vmModel: BusinessUnitItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBusinessUnit`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
