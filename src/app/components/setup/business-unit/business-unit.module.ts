import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BusinessUnitService } from './business-unit.service';
import { BusinessUnitListComponent } from './business-unit-list/business-unit-list.component';
import { BusinessUnitItemComponent } from './business-unit-item/business-unit-item.component';
import { BusinessUnitItemCustomComponent } from './business-unit-item/business-unit-item-custom.component';
import { BusinessUnitListCustomComponent } from './business-unit-list/business-unit-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BusinessUnitListComponent, BusinessUnitItemComponent],
  providers: [BusinessUnitService, BusinessUnitItemCustomComponent, BusinessUnitListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BusinessUnitListComponent, BusinessUnitItemComponent]
})
export class BusinessUnitComponentModule {}
