import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { BusinessUnitListView } from 'shared/models/viewModel';
import { BusinessUnitService } from '../business-unit.service';
import { BusinessUnitListCustomComponent } from './business-unit-list-custom.component';

@Component({
  selector: 'business-unit-list',
  templateUrl: './business-unit-list.component.html',
  styleUrls: ['./business-unit-list.component.scss']
})
export class BusinessUnitListComponent extends BaseListComponent<BusinessUnitListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  parentBusinessUnitGUIDOptions: SelectItems[] = [];
  constructor(public custom: BusinessUnitListCustomComponent, private service: BusinessUnitService) {
    super();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBusinessUnitToDropDown(this.parentBusinessUnitGUIDOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'businessUnitGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.BUSINESS_UNIT_ID',
        textKey: 'businessUnitId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PARENT_BUSINESS_UNIT_ID',
        textKey: 'parentBusinessUnit_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.parentBusinessUnitGUIDOptions,
        sortingKey: 'parentBusinessUnit_BusinessUnitId',
        searchingKey: 'parentBusinessUnitGUID'
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<BusinessUnitListView>> {
    return this.service.getBusinessUnitToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteBusinessUnit(row));
  }
}
