import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BankGroupService } from './bank-group.service';
import { BankGroupListComponent } from './bank-group-list/bank-group-list.component';
import { BankGroupItemComponent } from './bank-group-item/bank-group-item.component';
import { BankGroupItemCustomComponent } from './bank-group-item/bank-group-item-custom.component';
import { BankGroupListCustomComponent } from './bank-group-list/bank-group-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BankGroupListComponent, BankGroupItemComponent],
  providers: [BankGroupService, BankGroupItemCustomComponent, BankGroupListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BankGroupListComponent, BankGroupItemComponent]
})
export class BankGroupComponentModule {}
