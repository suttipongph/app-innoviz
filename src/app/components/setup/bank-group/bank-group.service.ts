import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { BankGroupItemView, BankGroupListView } from 'shared/models/viewModel';

@Injectable()
export class BankGroupService {
  serviceKey = 'bankGroupGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBankGroupToList(search: SearchParameter): Observable<SearchResult<BankGroupListView>> {
    const url = `${this.servicePath}/GetBankGroupList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBankGroup(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBankGroup`;
    return this.dataGateway.delete(url, row);
  }
  getBankGroupById(id: string): Observable<BankGroupItemView> {
    const url = `${this.servicePath}/GetBankGroupById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBankGroup(vmModel: BankGroupItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBankGroup`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBankGroup(vmModel: BankGroupItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBankGroup`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
