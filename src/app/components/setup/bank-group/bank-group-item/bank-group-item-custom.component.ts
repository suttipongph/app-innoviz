import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { BankGroupItemView } from 'shared/models/viewModel';

const firstGroup = ['BANK_GROUP_ID'];
export class BankGroupItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: BankGroupItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.bankGroupGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BankGroupItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<BankGroupItemView> {
    let model: BankGroupItemView = new BankGroupItemView();
    model.bankGroupGUID = EmptyGuid;
    return of(model);
  }
}
