import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InterestTypeValueService } from './interest-type-value.service';
import { InterestTypeValueListComponent } from './interest-type-value-list/interest-type-value-list.component';
import { InterestTypeValueItemComponent } from './interest-type-value-item/interest-type-value-item.component';
import { InterestTypeValueItemCustomComponent } from './interest-type-value-item/interest-type-value-item-custom.component';
import { InterestTypeValueListCustomComponent } from './interest-type-value-list/interest-type-value-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [InterestTypeValueListComponent, InterestTypeValueItemComponent],
  providers: [InterestTypeValueService, InterestTypeValueItemCustomComponent, InterestTypeValueListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [InterestTypeValueListComponent, InterestTypeValueItemComponent]
})
export class InterestTypeValueComponentModule {}
