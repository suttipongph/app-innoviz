import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InterestTypeValueListComponent } from './interest-type-value-list.component';

describe('InterestTypeValueListViewComponent', () => {
  let component: InterestTypeValueListComponent;
  let fixture: ComponentFixture<InterestTypeValueListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InterestTypeValueListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestTypeValueListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
