import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, ROUTE_MASTER_GEN, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { InterestTypeValueListView } from 'shared/models/viewModel';
import { InterestTypeValueService } from '../interest-type-value.service';
import { InterestTypeValueListCustomComponent } from './interest-type-value-list-custom.component';

@Component({
  selector: 'interest-type-value-list',
  templateUrl: './interest-type-value-list.component.html',
  styleUrls: ['./interest-type-value-list.component.scss']
})
export class InterestTypeValueListComponent extends BaseListComponent<InterestTypeValueListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  interestTypeOptions: SelectItems[] = [];

  constructor(public custom: InterestTypeValueListCustomComponent, private service: InterestTypeValueService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getKey('interesttype');
  }
  ngOnInit(): void {
    this.accessRightChildPage = 'InterestTypeValueListPage';
    this.redirectPath = ROUTE_MASTER_GEN.INTEREST_TYPE_VALUE;
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'interestTypeValueGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.EFFECTIVE_FROM',
        textKey: 'effectiveFrom',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.EFFECTIVE_TO',
        textKey: 'effectiveTo',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.VALUE',
        textKey: 'value',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'interestTypeGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<InterestTypeValueListView>> {
    return this.service.getInterestTypeValueToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteInterestTypeValue(row));
  }
}
