import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { InterestTypeValueItemView } from 'shared/models/viewModel';

const firstGroup = ['INTEREST_TYPE_GUID'];

export class InterestTypeValueItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: InterestTypeValueItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.interestTypeGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: InterestTypeValueItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(model: InterestTypeValueItemView, parentId: string): Observable<InterestTypeValueItemView> {
    model.interestTypeGUID = parentId;
    model.interestTypeValueGUID = EmptyGuid;
    return of(model);
  }
}
