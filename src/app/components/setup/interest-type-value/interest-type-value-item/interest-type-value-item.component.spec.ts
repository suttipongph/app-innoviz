import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterestTypeValueItemComponent } from './interest-type-value-item.component';

describe('InterestTypeValueItemViewComponent', () => {
  let component: InterestTypeValueItemComponent;
  let fixture: ComponentFixture<InterestTypeValueItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InterestTypeValueItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestTypeValueItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
