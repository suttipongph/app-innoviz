import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { InterestTypeValueItemView, InterestTypeValueListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class InterestTypeValueService {
  serviceKey = 'interestTypeValueGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getInterestTypeValueToList(search: SearchParameter): Observable<SearchResult<InterestTypeValueListView>> {
    const url = `${this.servicePath}/GetInterestTypeValueList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteInterestTypeValue(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteInterestTypeValue`;
    return this.dataGateway.delete(url, row);
  }
  getInterestTypeValueById(id: string): Observable<InterestTypeValueItemView> {
    const url = `${this.servicePath}/GetInterestTypeValueById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createInterestTypeValue(vmModel: InterestTypeValueItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateInterestTypeValue`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateInterestTypeValue(vmModel: InterestTypeValueItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateInterestTypeValue`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<InterestTypeValueItemView> {
    const url = `${this.servicePath}/GetInterestTypeValueInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
}
