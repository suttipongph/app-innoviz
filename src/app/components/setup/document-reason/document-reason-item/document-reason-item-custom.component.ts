import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { DocumentReasonItemView } from 'shared/models/viewModel';

const firstGroup = ['REASON_ID', 'REF_TYPE'];
export class DocumentReasonItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: DocumentReasonItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.documentReasonGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: DocumentReasonItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<DocumentReasonItemView> {
    let model: DocumentReasonItemView = new DocumentReasonItemView();
    model.documentReasonGUID = EmptyGuid;
    return of(model);
  }
}
