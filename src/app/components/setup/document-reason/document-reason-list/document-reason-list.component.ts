import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems,
  OptionModel
} from 'shared/models/systemModel';
import { DocumentReasonListView } from 'shared/models/viewModel';
import { DocumentReasonService } from '../document-reason.service';
import { DocumentReasonListCustomComponent } from './document-reason-list-custom.component';

@Component({
  selector: 'document-reason-list',
  templateUrl: './document-reason-list.component.html',
  styleUrls: ['./document-reason-list.component.scss']
})
export class DocumentReasonListComponent extends BaseListComponent<DocumentReasonListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  refTypeOptions: SelectItems[] = [];
  constructor(private service: DocumentReasonService, public custom: DocumentReasonListCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'documentReasonGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.REF_TYPE',
        textKey: 'refType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.refTypeOptions
      },
      { label: 'LABEL.REASON_ID', textKey: 'reasonId', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE },
      { label: 'LABEL.DESCRIPTION', textKey: 'description', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<DocumentReasonListView>> {
    return this.service.getDocumentReasonToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteDocumentReason(row));
  }
}
