import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { DocumentReasonItemView, DocumentReasonListView } from 'shared/models/viewModel';

@Injectable()
export class DocumentReasonService {
  serviceKey = 'documentReasonGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getDocumentReasonToList(search: SearchParameter): Observable<SearchResult<DocumentReasonListView>> {
    const url = `${this.servicePath}/GetDocumentReasonList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteDocumentReason(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteDocumentReason`;
    return this.dataGateway.delete(url, row);
  }
  getDocumentReasonById(id: string): Observable<DocumentReasonItemView> {
    const url = `${this.servicePath}/GetDocumentReasonById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createDocumentReason(vmModel: DocumentReasonItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateDocumentReason`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateDocumentReason(vmModel: DocumentReasonItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateDocumentReason`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
