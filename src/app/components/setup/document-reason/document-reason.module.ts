import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DocumentReasonService } from './document-reason.service';
import { DocumentReasonListComponent } from './document-reason-list/document-reason-list.component';
import { DocumentReasonItemComponent } from './document-reason-item/document-reason-item.component';
import { DocumentReasonItemCustomComponent } from './document-reason-item/document-reason-item-custom.component';
import { DocumentReasonListCustomComponent } from './document-reason-list/document-reason-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [DocumentReasonListComponent, DocumentReasonItemComponent],
  providers: [DocumentReasonService, DocumentReasonItemCustomComponent, DocumentReasonListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [DocumentReasonListComponent, DocumentReasonItemComponent]
})
export class DocumentReasonComponentModule {}
