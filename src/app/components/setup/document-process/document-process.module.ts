import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DocumentProcessService } from './document-process.service';
import { DocumentProcessListComponent } from './document-process-list/document-process-list.component';
import { DocumentProcessItemComponent } from './document-process-item/document-process-item.component';
import { DocumentProcessItemCustomComponent } from './document-process-item/document-process-item-custom.component';
import { DocumentProcessListCustomComponent } from './document-process-list/document-process-list-custom.component';
import { DocumentStatusComponentModule } from '../document-status/document-status.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, DocumentStatusComponentModule],
  declarations: [DocumentProcessListComponent, DocumentProcessItemComponent],
  providers: [DocumentProcessService, DocumentProcessItemCustomComponent, DocumentProcessListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [DocumentProcessListComponent, DocumentProcessItemComponent]
})
export class DocumentProcessComponentModule {}
