import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { DocumentProcessItemView, DocumentProcessListView } from 'shared/models/viewModel';

@Injectable()
export class DocumentProcessService {
  serviceKey = 'documentProcessGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getDocumentProcessToList(search: SearchParameter): Observable<SearchResult<DocumentProcessListView>> {
    const url = `${this.servicePath}/GetDocumentProcessList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteDocumentProcess(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteDocumentProcess`;
    return this.dataGateway.delete(url, row);
  }
  getDocumentProcessById(id: string): Observable<DocumentProcessItemView> {
    const url = `${this.servicePath}/GetDocumentProcessById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createDocumentProcess(vmModel: DocumentProcessItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateDocumentProcess`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateDocumentProcess(vmModel: DocumentProcessItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateDocumentProcess`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
