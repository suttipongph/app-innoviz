import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { DocumentProcessItemView } from 'shared/models/viewModel';

const firstGroup = ['PROCESS_ID'];
export class DocumentProcessItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: DocumentProcessItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.documentProcessGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: DocumentProcessItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<DocumentProcessItemView> {
    let model: DocumentProcessItemView = new DocumentProcessItemView();
    model.documentProcessGUID = EmptyGuid;
    return of(model);
  }
}
