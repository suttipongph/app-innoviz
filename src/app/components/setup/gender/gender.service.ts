import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { GenderItemView, GenderListView } from 'shared/models/viewModel';

@Injectable()
export class GenderService {
  serviceKey = 'genderGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getGenderToList(search: SearchParameter): Observable<SearchResult<GenderListView>> {
    const url = `${this.servicePath}/GetGenderList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteGender(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteGender`;
    return this.dataGateway.delete(url, row);
  }
  getGenderById(id: string): Observable<GenderItemView> {
    const url = `${this.servicePath}/GetGenderById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createGender(vmModel: GenderItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateGender`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateGender(vmModel: GenderItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateGender`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
