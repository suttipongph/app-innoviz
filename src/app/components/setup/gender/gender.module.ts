import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GenderService } from './gender.service';
import { GenderListComponent } from './gender-list/gender-list.component';
import { GenderItemComponent } from './gender-item/gender-item.component';
import { GenderItemCustomComponent } from './gender-item/gender-item-custom.component';
import { GenderListCustomComponent } from './gender-list/gender-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [GenderListComponent, GenderItemComponent],
  providers: [GenderService, GenderItemCustomComponent, GenderListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [GenderListComponent, GenderItemComponent]
})
export class GenderComponentModule {}
