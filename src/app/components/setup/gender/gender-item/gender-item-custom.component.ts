import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { GenderItemView } from 'shared/models/viewModel';

const firstGroup = ['VEND_GROUP_ID'];
export class GenderItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: GenderItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.genderGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: GenderItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<GenderItemView> {
    let model: GenderItemView = new GenderItemView();
    model.genderGUID = EmptyGuid;
    return of(model);
  }
}
