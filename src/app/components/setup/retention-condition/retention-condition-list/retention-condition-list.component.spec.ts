import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RetentionConditionListComponent } from './retention-condition-list.component';

describe('RetentionConditionListViewComponent', () => {
  let component: RetentionConditionListComponent;
  let fixture: ComponentFixture<RetentionConditionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RetentionConditionListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetentionConditionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
