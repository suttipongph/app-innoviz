import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetentionConditionItemComponent } from './retention-condition-item.component';

describe('RetentionConditionItemViewComponent', () => {
  let component: RetentionConditionItemComponent;
  let fixture: ComponentFixture<RetentionConditionItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RetentionConditionItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetentionConditionItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
