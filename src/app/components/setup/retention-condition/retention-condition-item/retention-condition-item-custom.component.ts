import { Observable, of } from 'rxjs';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { RetentionConditionItemView } from 'shared/models/viewModel';

const firstGroup = [];

export class RetentionConditionItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: RetentionConditionItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.retentionConditionGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: RetentionConditionItemView): Observable<boolean> {
    return of(!canCreate);
  }
}
