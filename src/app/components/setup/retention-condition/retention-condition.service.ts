import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { RetentionConditionItemView, RetentionConditionListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class RetentionConditionService {
  serviceKey = 'retentionConditionGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getRetentionConditionToList(search: SearchParameter): Observable<SearchResult<RetentionConditionListView>> {
    const url = `${this.servicePath}/GetRetentionConditionList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteRetentionCondition(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteRetentionCondition`;
    return this.dataGateway.delete(url, row);
  }
  getRetentionConditionById(id: string): Observable<RetentionConditionItemView> {
    const url = `${this.servicePath}/GetRetentionConditionById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createRetentionCondition(vmModel: RetentionConditionItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateRetentionCondition`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateRetentionCondition(vmModel: RetentionConditionItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateRetentionCondition`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
