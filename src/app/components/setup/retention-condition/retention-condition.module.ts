import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RetentionConditionService } from './retention-condition.service';
import { RetentionConditionListComponent } from './retention-condition-list/retention-condition-list.component';
import { RetentionConditionItemComponent } from './retention-condition-item/retention-condition-item.component';
import { RetentionConditionItemCustomComponent } from './retention-condition-item/retention-condition-item-custom.component';
import { RetentionConditionListCustomComponent } from './retention-condition-list/retention-condition-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [RetentionConditionListComponent, RetentionConditionItemComponent],
  providers: [RetentionConditionService, RetentionConditionItemCustomComponent, RetentionConditionListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [RetentionConditionListComponent, RetentionConditionItemComponent]
})
export class RetentionConditionComponentModule {}
