import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IntercompanyListComponent } from './intercompany-list.component';

describe('IntercompanyListViewComponent', () => {
  let component: IntercompanyListComponent;
  let fixture: ComponentFixture<IntercompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IntercompanyListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntercompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
