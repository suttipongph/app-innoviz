import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IntercompanyService } from './intercompany.service';
import { IntercompanyListComponent } from './intercompany-list/intercompany-list.component';
import { IntercompanyItemComponent } from './intercompany-item/intercompany-item.component';
import { IntercompanyItemCustomComponent } from './intercompany-item/intercompany-item-custom.component';
import { IntercompanyListCustomComponent } from './intercompany-list/intercompany-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [IntercompanyListComponent, IntercompanyItemComponent],
  providers: [IntercompanyService, IntercompanyItemCustomComponent, IntercompanyListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [IntercompanyListComponent, IntercompanyItemComponent]
})
export class IntercompanyComponentModule {}
