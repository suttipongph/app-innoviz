import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { IntercompanyItemView, IntercompanyListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class IntercompanyService {
  serviceKey = 'intercompanyGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getIntercompanyToList(search: SearchParameter): Observable<SearchResult<IntercompanyListView>> {
    const url = `${this.servicePath}/GetIntercompanyList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteIntercompany(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteIntercompany`;
    return this.dataGateway.delete(url, row);
  }
  getIntercompanyById(id: string): Observable<IntercompanyItemView> {
    const url = `${this.servicePath}/GetIntercompanyById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createIntercompany(vmModel: IntercompanyItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateIntercompany`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateIntercompany(vmModel: IntercompanyItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateIntercompany`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
