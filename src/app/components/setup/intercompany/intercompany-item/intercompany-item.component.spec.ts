import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntercompanyItemComponent } from './intercompany-item.component';

describe('IntercompanyItemViewComponent', () => {
  let component: IntercompanyItemComponent;
  let fixture: ComponentFixture<IntercompanyItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IntercompanyItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntercompanyItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
