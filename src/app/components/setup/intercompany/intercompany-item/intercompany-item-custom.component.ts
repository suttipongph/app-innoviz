import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { IntercompanyItemView } from 'shared/models/viewModel';

const firstGroup = ['INTERCOMPANY_ID'];

export class IntercompanyItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: IntercompanyItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.intercompanyGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: IntercompanyItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<IntercompanyItemView> {
    let model: IntercompanyItemView = new IntercompanyItemView();
    model.intercompanyGUID = EmptyGuid;
    return of(model);
  }
}
