import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { OccupationItemView } from 'shared/models/viewModel';

const firstGroup = ['OCCUPATION_ID'];
export class OccupationItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: OccupationItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.occupationGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: OccupationItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<OccupationItemView> {
    let model: OccupationItemView = new OccupationItemView();
    model.occupationGUID = EmptyGuid;
    return of(model);
  }
}
