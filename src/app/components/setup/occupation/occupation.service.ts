import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { OccupationItemView, OccupationListView } from 'shared/models/viewModel';

@Injectable()
export class OccupationService {
  serviceKey = 'occupationGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getOccupationToList(search: SearchParameter): Observable<SearchResult<OccupationListView>> {
    const url = `${this.servicePath}/GetOccupationList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteOccupation(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteOccupation`;
    return this.dataGateway.delete(url, row);
  }
  getOccupationById(id: string): Observable<OccupationItemView> {
    const url = `${this.servicePath}/GetOccupationById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createOccupation(vmModel: OccupationItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateOccupation`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateOccupation(vmModel: OccupationItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateOccupation`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
