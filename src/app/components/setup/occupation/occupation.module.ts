import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OccupationService } from './occupation.service';
import { OccupationListComponent } from './occupation-list/occupation-list.component';
import { OccupationItemComponent } from './occupation-item/occupation-item.component';
import { OccupationItemCustomComponent } from './occupation-item/occupation-item-custom.component';
import { OccupationListCustomComponent } from './occupation-list/occupation-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [OccupationListComponent, OccupationItemComponent],
  providers: [OccupationService, OccupationItemCustomComponent, OccupationListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [OccupationListComponent, OccupationItemComponent]
})
export class OccupationComponentModule {}
