import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CalendarGroupItemView, CalendarGroupListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CalendarGroupService {
  serviceKey = 'calendarGroupGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCalendarGroupToList(search: SearchParameter): Observable<SearchResult<CalendarGroupListView>> {
    const url = `${this.servicePath}/GetCalendarGroupList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCalendarGroup(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCalendarGroup`;
    return this.dataGateway.delete(url, row);
  }
  getCalendarGroupById(id: string): Observable<CalendarGroupItemView> {
    const url = `${this.servicePath}/GetCalendarGroupById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCalendarGroup(vmModel: CalendarGroupItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCalendarGroup`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCalendarGroup(vmModel: CalendarGroupItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCalendarGroup`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
