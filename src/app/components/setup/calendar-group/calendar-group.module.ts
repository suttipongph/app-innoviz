import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CalendarGroupService } from './calendar-group.service';
import { CalendarGroupListComponent } from './calendar-group-list/calendar-group-list.component';
import { CalendarGroupItemComponent } from './calendar-group-item/calendar-group-item.component';
import { CalendarGroupItemCustomComponent } from './calendar-group-item/calendar-group-item-custom.component';
import { CalendarGroupListCustomComponent } from './calendar-group-list/calendar-group-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CalendarGroupListComponent, CalendarGroupItemComponent],
  providers: [CalendarGroupService, CalendarGroupItemCustomComponent, CalendarGroupListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CalendarGroupListComponent, CalendarGroupItemComponent]
})
export class CalendarGroupComponentModule {}
