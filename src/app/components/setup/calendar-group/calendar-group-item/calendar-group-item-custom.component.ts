import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CalendarGroupItemView } from 'shared/models/viewModel';

const firstGroup = ['CALENDAR_GROUP_ID'];

export class CalendarGroupItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CalendarGroupItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.calendarGroupGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CalendarGroupItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<CalendarGroupItemView> {
    let model = new CalendarGroupItemView();
    model.calendarGroupGUID = EmptyGuid;
    return of(model);
  }
}
