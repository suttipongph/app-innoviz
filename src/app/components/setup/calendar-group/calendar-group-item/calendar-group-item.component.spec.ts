import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarGroupItemComponent } from './calendar-group-item.component';

describe('CalendarGroupItemViewComponent', () => {
  let component: CalendarGroupItemComponent;
  let fixture: ComponentFixture<CalendarGroupItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CalendarGroupItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarGroupItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
