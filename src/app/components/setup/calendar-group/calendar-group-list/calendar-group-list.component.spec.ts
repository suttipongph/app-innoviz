import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CalendarGroupListComponent } from './calendar-group-list.component';

describe('CalendarGroupListViewComponent', () => {
  let component: CalendarGroupListComponent;
  let fixture: ComponentFixture<CalendarGroupListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CalendarGroupListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarGroupListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
