import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DocumentConditionTemplateLineService } from './document-condition-template-line.service';
import { DocumentConditionTemplateLineListComponent } from './document-condition-template-line-list/document-condition-template-line-list.component';
import { DocumentConditionTemplateLineItemComponent } from './document-condition-template-line-item/document-condition-template-line-item.component';
import { DocumentConditionTemplateLineItemCustomComponent } from './document-condition-template-line-item/document-condition-template-line-item-custom.component';
import { DocumentConditionTemplateLineListCustomComponent } from './document-condition-template-line-list/document-condition-template-line-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [DocumentConditionTemplateLineListComponent, DocumentConditionTemplateLineItemComponent],
  providers: [
    DocumentConditionTemplateLineService,
    DocumentConditionTemplateLineItemCustomComponent,
    DocumentConditionTemplateLineListCustomComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [DocumentConditionTemplateLineListComponent, DocumentConditionTemplateLineItemComponent]
})
export class DocumentConditionTemplateLineComponentModule {}
