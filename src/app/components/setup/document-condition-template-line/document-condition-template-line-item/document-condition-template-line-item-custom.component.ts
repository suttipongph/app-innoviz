import { Observable, of } from 'rxjs';
import { FieldAccessing } from 'shared/models/systemModel';
import { DocumentConditionTemplateLineItemView } from 'shared/models/viewModel';

const firstGroup = ['DOCUMENT_CONDITION_TEMPLATE_TABLE_GUID'];

export class DocumentConditionTemplateLineItemCustomComponent {
  getFieldAccessing(model: DocumentConditionTemplateLineItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: DocumentConditionTemplateLineItemView): Observable<boolean> {
    return of(!canCreate);
  }
}
