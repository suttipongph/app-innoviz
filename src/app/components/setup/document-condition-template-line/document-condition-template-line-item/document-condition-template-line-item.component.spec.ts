import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentConditionTemplateLineItemComponent } from './document-condition-template-line-item.component';

describe('DocumentConditionTemplateLineItemViewComponent', () => {
  let component: DocumentConditionTemplateLineItemComponent;
  let fixture: ComponentFixture<DocumentConditionTemplateLineItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DocumentConditionTemplateLineItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentConditionTemplateLineItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
