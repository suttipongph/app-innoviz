import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { DocumentConditionTemplateLineItemView, DocumentConditionTemplateLineListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class DocumentConditionTemplateLineService {
  serviceKey = 'documentConditionTemplateLineGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getDocumentConditionTemplateLineToList(search: SearchParameter): Observable<SearchResult<DocumentConditionTemplateLineListView>> {
    const url = `${this.servicePath}/GetDocumentConditionTemplateLineList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteDocumentConditionTemplateLine(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteDocumentConditionTemplateLine`;
    return this.dataGateway.delete(url, row);
  }
  getDocumentConditionTemplateLineById(id: string): Observable<DocumentConditionTemplateLineItemView> {
    const url = `${this.servicePath}/GetDocumentConditionTemplateLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createDocumentConditionTemplateLine(vmModel: DocumentConditionTemplateLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateDocumentConditionTemplateLine`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateDocumentConditionTemplateLine(vmModel: DocumentConditionTemplateLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateDocumentConditionTemplateLine`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(documentConditionTemplateTableGUID: string): Observable<any> {
    const url = `${this.servicePath}/GetDocumentConditionTemplateInitialData/documentConditionTemplateTableGUID=${documentConditionTemplateTableGUID}`;
    return this.dataGateway.get(url);
  }
}
