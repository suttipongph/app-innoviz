import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DocumentConditionTemplateLineListComponent } from './document-condition-template-line-list.component';

describe('DocumentConditionTemplateLineListViewComponent', () => {
  let component: DocumentConditionTemplateLineListComponent;
  let fixture: ComponentFixture<DocumentConditionTemplateLineListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DocumentConditionTemplateLineListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentConditionTemplateLineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
