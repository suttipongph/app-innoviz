import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { DocumentConditionTemplateLineListView } from 'shared/models/viewModel';
import { DocumentConditionTemplateLineService } from '../document-condition-template-line.service';
import { DocumentConditionTemplateLineListCustomComponent } from './document-condition-template-line-list-custom.component';

@Component({
  selector: 'document-condition-template-line-list',
  templateUrl: './document-condition-template-line-list.component.html',
  styleUrls: ['./document-condition-template-line-list.component.scss']
})
export class DocumentConditionTemplateLineListComponent
  extends BaseListComponent<DocumentConditionTemplateLineListView>
  implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  documentConditionTemplateTableOptions: SelectItems[] = [];
  documentTypeOptions: SelectItems[] = [];
  defualtBitOption: SelectItems[] = [];

  constructor(public custom: DocumentConditionTemplateLineListCustomComponent, private service: DocumentConditionTemplateLineService) {
    super();
    this.parentId = this.uiService.getKey('documentconditiontemplatetable');
  }
  ngOnInit(): void {
    this.accessRightChildPage = 'DocumentConditionTemplateLineListComponent';
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getDefaultBitDropDown(this.defualtBitOption);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    // super.checkAccessMode(this.accessService.getAccessRight());
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getDocumentTypeDropDown(this.documentTypeOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'documentConditionTemplateLineGUID';
    const columns: ColumnModel[] = [
      {
        label: this.translate.instant('LABEL.DOCUMENT_TYPE_ID'),
        textKey: 'documentType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentTypeOptions,
        searchingKey: 'documentTypeGUID',
        sortingKey: 'documentTypeId'
      },
      {
        label: this.translate.instant('LABEL.MANDATORY'),
        textKey: 'mandatory',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defualtBitOption
      },
      {
        label: null,
        textKey: 'documentConditionTemplateTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<DocumentConditionTemplateLineListView>> {
    return this.service.getDocumentConditionTemplateLineToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteDocumentConditionTemplateLine(row));
  }
}
