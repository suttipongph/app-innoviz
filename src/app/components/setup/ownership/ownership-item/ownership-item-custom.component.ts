import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { FieldAccessing } from 'shared/models/systemModel';
import { OwnershipItemView } from 'shared/models/viewModel';

const firstGroup = ['OWNERSHIP_ID'];
export class OwnershipItemCustomComponent {
  getFieldAccessing(model: OwnershipItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.ownershipGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: OwnershipItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<OwnershipItemView> {
    let model: OwnershipItemView = new OwnershipItemView();
    model.ownershipGUID = EmptyGuid;
    return of(model);
  }
}
