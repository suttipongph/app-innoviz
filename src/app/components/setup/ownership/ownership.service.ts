import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { OwnershipItemView, OwnershipListView } from 'shared/models/viewModel';

@Injectable()
export class OwnershipService {
  serviceKey = 'ownershipGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getOwnershipToList(search: SearchParameter): Observable<SearchResult<OwnershipListView>> {
    const url = `${this.servicePath}/GetOwnershipList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteOwnership(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteOwnership`;
    return this.dataGateway.delete(url, row);
  }
  getOwnershipById(id: string): Observable<OwnershipItemView> {
    const url = `${this.servicePath}/GetOwnershipById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createOwnership(vmModel: OwnershipItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateOwnership`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateOwnership(vmModel: OwnershipItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateOwnership`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
