import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OwnershipService } from './ownership.service';
import { OwnershipListComponent } from './ownership-list/ownership-list.component';
import { OwnershipItemComponent } from './ownership-item/ownership-item.component';
import { OwnershipItemCustomComponent } from './ownership-item/ownership-item-custom.component';
import { OwnershipListCustomComponent } from './ownership-list/ownership-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [OwnershipListComponent, OwnershipItemComponent],
  providers: [OwnershipService, OwnershipItemCustomComponent, OwnershipListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [OwnershipListComponent, OwnershipItemComponent]
})
export class OwnershipComponentModule {}
