import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MethodOfPaymentService } from './method-of-payment.service';
import { MethodOfPaymentListComponent } from './method-of-payment-list/method-of-payment-list.component';
import { MethodOfPaymentItemComponent } from './method-of-payment-item/method-of-payment-item.component';
import { MethodOfPaymentItemCustomComponent } from './method-of-payment-item/method-of-payment-item-custom.component';
import { MethodOfPaymentListCustomComponent } from './method-of-payment-list/method-of-payment-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [MethodOfPaymentListComponent, MethodOfPaymentItemComponent],
  providers: [MethodOfPaymentService, MethodOfPaymentItemCustomComponent, MethodOfPaymentListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [MethodOfPaymentListComponent, MethodOfPaymentItemComponent]
})
export class MethodOfPaymentComponentModule {}
