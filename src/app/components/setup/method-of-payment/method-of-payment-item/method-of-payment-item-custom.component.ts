import { Observable, of } from 'rxjs';
import { EmptyGuid, PaymentType } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { MethodOfPaymentItemView } from 'shared/models/viewModel';
import { MethodOfPaymentService } from '../method-of-payment.service';

const firstGroup = ['METHOD_OF_PAYMENT_ID'];

export class MethodOfPaymentItemCustomComponent {
  baseService: BaseServiceModel<MethodOfPaymentService>;
  accountNumRequire = true;
  getFieldAccessing(model: MethodOfPaymentItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];

    if (isUpdateMode(model.methodOfPaymentGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      if (model.paymentType === PaymentType.SuspenseAccount) {
        this.accountNumRequire = false;
      } else {
        this.accountNumRequire = true;
      }
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: MethodOfPaymentItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<MethodOfPaymentItemView> {
    let model: MethodOfPaymentItemView = new MethodOfPaymentItemView();
    model.methodOfPaymentGUID = EmptyGuid;
    model.paymentType = PaymentType.Ledger;
    return of(model);
  }
  paymentTypeOnChang(e: any, model: MethodOfPaymentItemView) {
    if (model.paymentType === PaymentType.SuspenseAccount) {
      this.accountNumRequire = false;
    } else {
      this.accountNumRequire = true;
    }
  }
}
