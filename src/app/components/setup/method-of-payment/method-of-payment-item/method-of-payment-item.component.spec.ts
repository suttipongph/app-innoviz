import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MethodOfPaymentItemComponent } from './method-of-payment-item.component';

describe('MethodOfPaymentItemViewComponent', () => {
  let component: MethodOfPaymentItemComponent;
  let fixture: ComponentFixture<MethodOfPaymentItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MethodOfPaymentItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MethodOfPaymentItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
