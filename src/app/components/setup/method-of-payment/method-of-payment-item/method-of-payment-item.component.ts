import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { MethodOfPaymentItemView } from 'shared/models/viewModel';
import { MethodOfPaymentService } from '../method-of-payment.service';
import { MethodOfPaymentItemCustomComponent } from './method-of-payment-item-custom.component';
@Component({
  selector: 'method-of-payment-item',
  templateUrl: './method-of-payment-item.component.html',
  styleUrls: ['./method-of-payment-item.component.scss']
})
export class MethodOfPaymentItemComponent extends BaseItemComponent<MethodOfPaymentItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  companyBankOptions: SelectItems[] = [];
  paymentTypeOptions: SelectItems[] = [];
  accountTypeOption: SelectItems[] = [];

  constructor(
    private service: MethodOfPaymentService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: MethodOfPaymentItemCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getPaymentTypeEnumDropDown(this.paymentTypeOptions);
    this.baseDropdown.getAccounTypeEnumDropDown(this.accountTypeOption);
  }
  getById(): Observable<MethodOfPaymentItemView> {
    return this.service.getMethodOfPaymentById(this.id);
  }
  getInitialData(): Observable<MethodOfPaymentItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner(result);
      super.setDefaultValueSystemFields();
    });
  }
  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getCompanyBankDropDown()).subscribe(
      ([companyBank]) => {
        this.companyBankOptions = companyBank;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateMethodOfPayment(this.model), isColsing);
    } else {
      super.onCreate(this.service.createMethodOfPayment(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  paymentTypeOnChang(e: any): any {
    this.custom.paymentTypeOnChang(e, this.model);
  }
}
