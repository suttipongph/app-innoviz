import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MethodOfPaymentListComponent } from './method-of-payment-list.component';

describe('MethodOfPaymentListViewComponent', () => {
  let component: MethodOfPaymentListComponent;
  let fixture: ComponentFixture<MethodOfPaymentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MethodOfPaymentListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MethodOfPaymentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
