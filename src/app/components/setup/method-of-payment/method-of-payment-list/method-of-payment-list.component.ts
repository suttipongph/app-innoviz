import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { MethodOfPaymentListView } from 'shared/models/viewModel';
import { MethodOfPaymentService } from '../method-of-payment.service';
import { MethodOfPaymentListCustomComponent } from './method-of-payment-list-custom.component';

@Component({
  selector: 'method-of-payment-list',
  templateUrl: './method-of-payment-list.component.html',
  styleUrls: ['./method-of-payment-list.component.scss']
})
export class MethodOfPaymentListComponent extends BaseListComponent<MethodOfPaymentListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  companyBankOptions: SelectItems[] = [];
  paymentTypeOptions: SelectItems[] = [];
  accountTypeOption: SelectItems[] = [];

  constructor(public custom: MethodOfPaymentListCustomComponent, private service: MethodOfPaymentService) {
    super();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getPaymentTypeEnumDropDown(this.paymentTypeOptions);
    this.baseDropdown.getAccounTypeEnumDropDown(this.accountTypeOption);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCompanyBankDropDown(this.companyBankOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'methodOfPaymentGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.METHOD_OF_PAYMENT_ID',
        textKey: 'methodOfPaymentId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.COMPANY_BANK_ID',
        textKey: 'companyBank_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.companyBankOptions,
        searchingKey: 'companyBankGUID',
        sortingKey: 'companyBank_AccountNumber'
      },
      {
        label: 'LABEL.PAYMENT_TYPE',
        textKey: 'paymentType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.paymentTypeOptions
      },
      {
        label: 'LABEL.ACCOUNT_TYPE',
        textKey: 'accountType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.accountTypeOption
      },
      {
        label: 'LABEL.ACCOUNT_NUMBER',
        textKey: 'accountNum',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PAYMENT_REMARK',
        textKey: 'paymentRemark',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<MethodOfPaymentListView>> {
    return this.service.getMethodOfPaymentToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteMethodOfPayment(row));
  }
}
