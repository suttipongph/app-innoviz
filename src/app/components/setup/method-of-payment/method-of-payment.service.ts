import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { MethodOfPaymentItemView, MethodOfPaymentListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class MethodOfPaymentService {
  serviceKey = 'methodOfPaymentGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getMethodOfPaymentToList(search: SearchParameter): Observable<SearchResult<MethodOfPaymentListView>> {
    const url = `${this.servicePath}/GetMethodOfPaymentList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteMethodOfPayment(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteMethodOfPayment`;
    return this.dataGateway.delete(url, row);
  }
  getMethodOfPaymentById(id: string): Observable<MethodOfPaymentItemView> {
    const url = `${this.servicePath}/GetMethodOfPaymentById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createMethodOfPayment(vmModel: MethodOfPaymentItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateMethodOfPayment`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateMethodOfPayment(vmModel: MethodOfPaymentItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateMethodOfPayment`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
