import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExposureGroupService } from './exposure-group.service';
import { ExposureGroupListComponent } from './exposure-group-list/exposure-group-list.component';
import { ExposureGroupItemComponent } from './exposure-group-item/exposure-group-item.component';
import { ExposureGroupItemCustomComponent } from './exposure-group-item/exposure-group-item-custom.component';
import { ExposureGroupListCustomComponent } from './exposure-group-list/exposure-group-list-custom.component';
import { ExposureGroupByProductComponentModule } from '../exposure-group-by-product/exposure-group-by-product.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, ExposureGroupByProductComponentModule],
  declarations: [ExposureGroupListComponent, ExposureGroupItemComponent],
  providers: [ExposureGroupService, ExposureGroupItemCustomComponent, ExposureGroupListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ExposureGroupListComponent, ExposureGroupItemComponent]
})
export class ExposureGroupComponentModule {}
