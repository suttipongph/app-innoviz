import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ExposureGroupItemView, ExposureGroupListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ExposureGroupService {
  serviceKey = 'exposureGroupGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.servicePath = param.servicePath;
  }
  getExposureGroupToList(search: SearchParameter): Observable<SearchResult<ExposureGroupListView>> {
    const url = `${this.servicePath}/GetExposureGroupList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteExposureGroup(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteExposureGroup`;
    return this.dataGateway.delete(url, row);
  }
  getExposureGroupById(id: string): Observable<ExposureGroupItemView> {
    const url = `${this.servicePath}/GetExposureGroupById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createExposureGroup(vmModel: ExposureGroupItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateExposureGroup`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateExposureGroup(vmModel: ExposureGroupItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateExposureGroup`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
