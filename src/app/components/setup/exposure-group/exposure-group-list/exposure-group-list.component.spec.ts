import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ExposureGroupListComponent } from './exposure-group-list.component';

describe('ExposureGroupListViewComponent', () => {
  let component: ExposureGroupListComponent;
  let fixture: ComponentFixture<ExposureGroupListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExposureGroupListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExposureGroupListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
