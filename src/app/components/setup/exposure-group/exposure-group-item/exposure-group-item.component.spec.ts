import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExposureGroupItemComponent } from './exposure-group-item.component';

describe('ExposureGroupItemViewComponent', () => {
  let component: ExposureGroupItemComponent;
  let fixture: ComponentFixture<ExposureGroupItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExposureGroupItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExposureGroupItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
