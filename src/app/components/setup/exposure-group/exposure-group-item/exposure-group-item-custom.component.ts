import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { ExposureGroupItemView } from 'shared/models/viewModel';

const firstGroup = ['EXPOSURE_GROUP_ID'];

export class ExposureGroupItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: ExposureGroupItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.exposureGroupGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ExposureGroupItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<ExposureGroupItemView> {
    let model = new ExposureGroupItemView();
    model.exposureGroupGUID = EmptyGuid;
    return of(model);
  }
}
