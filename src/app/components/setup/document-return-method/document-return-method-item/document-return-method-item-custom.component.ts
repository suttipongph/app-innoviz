import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { DocumentReturnMethodItemView } from 'shared/models/viewModel';

const firstGroup = ['DOCUMENT_RETURN_METHOD_ID'];

export class DocumentReturnMethodItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: DocumentReturnMethodItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.documentReturnMethodGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: DocumentReturnMethodItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<DocumentReturnMethodItemView> {
    let model = new DocumentReturnMethodItemView();
    model.documentReturnMethodGUID = EmptyGuid;
    return of(model);
  }
}
