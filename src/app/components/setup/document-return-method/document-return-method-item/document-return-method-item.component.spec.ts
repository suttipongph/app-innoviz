import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentReturnMethodItemComponent } from './document-return-method-item.component';

describe('DocumentReturnMethodItemViewComponent', () => {
  let component: DocumentReturnMethodItemComponent;
  let fixture: ComponentFixture<DocumentReturnMethodItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DocumentReturnMethodItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentReturnMethodItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
