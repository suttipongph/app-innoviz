import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DocumentReturnMethodListComponent } from './document-return-method-list.component';

describe('DocumentReturnMethodListViewComponent', () => {
  let component: DocumentReturnMethodListComponent;
  let fixture: ComponentFixture<DocumentReturnMethodListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DocumentReturnMethodListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentReturnMethodListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
