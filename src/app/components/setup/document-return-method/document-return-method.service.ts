import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { DocumentReturnMethodItemView, DocumentReturnMethodListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class DocumentReturnMethodService {
  serviceKey = 'documentReturnMethodGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getDocumentReturnMethodToList(search: SearchParameter): Observable<SearchResult<DocumentReturnMethodListView>> {
    const url = `${this.servicePath}/GetDocumentReturnMethodList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteDocumentReturnMethod(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteDocumentReturnMethod`;
    return this.dataGateway.delete(url, row);
  }
  getDocumentReturnMethodById(id: string): Observable<DocumentReturnMethodItemView> {
    const url = `${this.servicePath}/GetDocumentReturnMethodById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createDocumentReturnMethod(vmModel: DocumentReturnMethodItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateDocumentReturnMethod`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateDocumentReturnMethod(vmModel: DocumentReturnMethodItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateDocumentReturnMethod`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
