import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DocumentReturnMethodService } from './document-return-method.service';
import { DocumentReturnMethodListComponent } from './document-return-method-list/document-return-method-list.component';
import { DocumentReturnMethodItemComponent } from './document-return-method-item/document-return-method-item.component';
import { DocumentReturnMethodItemCustomComponent } from './document-return-method-item/document-return-method-item-custom.component';
import { DocumentReturnMethodListCustomComponent } from './document-return-method-list/document-return-method-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [DocumentReturnMethodListComponent, DocumentReturnMethodItemComponent],
  providers: [DocumentReturnMethodService, DocumentReturnMethodItemCustomComponent, DocumentReturnMethodListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [DocumentReturnMethodListComponent, DocumentReturnMethodItemComponent]
})
export class DocumentReturnMethodComponentModule {}
