import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CalendarNonWorkingDateItemView, CalendarNonWorkingDateListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CalendarNonWorkingDateService {
  serviceKey = 'calendarNonWorkingDateGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCalendarNonWorkingDateToList(search: SearchParameter): Observable<SearchResult<CalendarNonWorkingDateListView>> {
    const url = `${this.servicePath}/GetCalendarNonWorkingDateList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCalendarNonWorkingDate(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCalendarNonWorkingDate`;
    return this.dataGateway.delete(url, row);
  }
  getCalendarNonWorkingDateById(id: string): Observable<CalendarNonWorkingDateItemView> {
    const url = `${this.servicePath}/GetCalendarNonWorkingDateById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCalendarNonWorkingDate(vmModel: CalendarNonWorkingDateItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCalendarNonWorkingDate`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCalendarNonWorkingDate(vmModel: CalendarNonWorkingDateItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCalendarNonWorkingDate`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
