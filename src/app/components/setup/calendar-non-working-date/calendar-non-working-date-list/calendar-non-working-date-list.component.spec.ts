import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CalendarNonWorkingDateListComponent } from './calendar-non-working-date-list.component';

describe('CalendarNonWorkingDateListViewComponent', () => {
  let component: CalendarNonWorkingDateListComponent;
  let fixture: ComponentFixture<CalendarNonWorkingDateListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CalendarNonWorkingDateListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarNonWorkingDateListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
