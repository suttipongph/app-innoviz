import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CalendarNonWorkingDateListView } from 'shared/models/viewModel';
import { CalendarNonWorkingDateService } from '../calendar-non-working-date.service';
import { CalendarNonWorkingDateListCustomComponent } from './calendar-non-working-date-list-custom.component';

@Component({
  selector: 'calendar-non-working-date-list',
  templateUrl: './calendar-non-working-date-list.component.html',
  styleUrls: ['./calendar-non-working-date-list.component.scss']
})
export class CalendarNonWorkingDateListComponent extends BaseListComponent<CalendarNonWorkingDateListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  calendarGroupOptions: SelectItems[] = [];
  holidayTypeOptions: SelectItems[] = [];

  constructor(public custom: CalendarNonWorkingDateListCustomComponent, private service: CalendarNonWorkingDateService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getHolidayTypeEnumDropDown(this.holidayTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCalendarGroupDropDown(this.calendarGroupOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'calendarNonWorkingDateGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.CALENDAR_GROUP_ID',
        textKey: 'calendarGroup_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.calendarGroupOptions,
        searchingKey: 'calendarGroupGUID',
        sortingKey: 'calendarGroup_calendarGroupId'
      },
      {
        label: 'LABEL.CALENDAR_DATE',
        textKey: 'calendarDate',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.HOLIDAY_TYPE',
        textKey: 'holidayType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.holidayTypeOptions
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CalendarNonWorkingDateListView>> {
    return this.service.getCalendarNonWorkingDateToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCalendarNonWorkingDate(row));
  }
}
