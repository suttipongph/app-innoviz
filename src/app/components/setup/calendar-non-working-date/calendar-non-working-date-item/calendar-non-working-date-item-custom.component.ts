import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CalendarNonWorkingDateItemView } from 'shared/models/viewModel';

const firstGroup = ['CALENDAR_GROUP_GUID', 'CALENDAR_DATE'];

export class CalendarNonWorkingDateItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CalendarNonWorkingDateItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.calendarNonWorkingDateGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CalendarNonWorkingDateItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<CalendarNonWorkingDateItemView> {
    let model = new CalendarNonWorkingDateItemView();
    model.calendarNonWorkingDateGUID = EmptyGuid;
    return of(model);
  }
}
