import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarNonWorkingDateItemComponent } from './calendar-non-working-date-item.component';

describe('CalendarNonWorkingDateItemViewComponent', () => {
  let component: CalendarNonWorkingDateItemComponent;
  let fixture: ComponentFixture<CalendarNonWorkingDateItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CalendarNonWorkingDateItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarNonWorkingDateItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
