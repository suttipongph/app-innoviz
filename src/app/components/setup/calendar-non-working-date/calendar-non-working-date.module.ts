import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CalendarNonWorkingDateService } from './calendar-non-working-date.service';
import { CalendarNonWorkingDateListComponent } from './calendar-non-working-date-list/calendar-non-working-date-list.component';
import { CalendarNonWorkingDateItemComponent } from './calendar-non-working-date-item/calendar-non-working-date-item.component';
import { CalendarNonWorkingDateItemCustomComponent } from './calendar-non-working-date-item/calendar-non-working-date-item-custom.component';
import { CalendarNonWorkingDateListCustomComponent } from './calendar-non-working-date-list/calendar-non-working-date-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CalendarNonWorkingDateListComponent, CalendarNonWorkingDateItemComponent],
  providers: [CalendarNonWorkingDateService, CalendarNonWorkingDateItemCustomComponent, CalendarNonWorkingDateListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CalendarNonWorkingDateListComponent, CalendarNonWorkingDateItemComponent]
})
export class CalendarNonWorkingDateComponentModule {}
