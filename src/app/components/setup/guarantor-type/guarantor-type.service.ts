import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { GuarantorTypeItemView, GuarantorTypeListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class GuarantorTypeService {
  serviceKey = 'guarantorTypeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getGuarantorTypeToList(search: SearchParameter): Observable<SearchResult<GuarantorTypeListView>> {
    const url = `${this.servicePath}/GetGuarantorTypeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteGuarantorType(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteGuarantorType`;
    return this.dataGateway.delete(url, row);
  }
  getGuarantorTypeById(id: string): Observable<GuarantorTypeItemView> {
    const url = `${this.servicePath}/GetGuarantorTypeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createGuarantorType(vmModel: GuarantorTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateGuarantorType`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateGuarantorType(vmModel: GuarantorTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateGuarantorType`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
