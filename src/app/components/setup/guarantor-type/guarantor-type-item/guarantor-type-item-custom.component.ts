import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { GuarantorTypeItemView } from 'shared/models/viewModel';

const firstGroup = ['GUARANTOR_TYPE_ID'];

export class GuarantorTypeItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: GuarantorTypeItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.guarantorTypeGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: GuarantorTypeItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<GuarantorTypeItemView> {
    let model: GuarantorTypeItemView = new GuarantorTypeItemView();
    model.guarantorTypeGUID = EmptyGuid;
    return of(model);
  }
}
