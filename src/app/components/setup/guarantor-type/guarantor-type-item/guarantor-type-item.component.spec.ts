import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuarantorTypeItemComponent } from './guarantor-type-item.component';

describe('GuarantorTypeItemViewComponent', () => {
  let component: GuarantorTypeItemComponent;
  let fixture: ComponentFixture<GuarantorTypeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GuarantorTypeItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuarantorTypeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
