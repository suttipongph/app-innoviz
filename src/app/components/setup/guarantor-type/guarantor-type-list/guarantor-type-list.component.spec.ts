import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GuarantorTypeListComponent } from './guarantor-type-list.component';

describe('GuarantorTypeListViewComponent', () => {
  let component: GuarantorTypeListComponent;
  let fixture: ComponentFixture<GuarantorTypeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GuarantorTypeListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuarantorTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
