import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GuarantorTypeService } from './guarantor-type.service';
import { GuarantorTypeListComponent } from './guarantor-type-list/guarantor-type-list.component';
import { GuarantorTypeItemComponent } from './guarantor-type-item/guarantor-type-item.component';
import { GuarantorTypeItemCustomComponent } from './guarantor-type-item/guarantor-type-item-custom.component';
import { GuarantorTypeListCustomComponent } from './guarantor-type-list/guarantor-type-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [GuarantorTypeListComponent, GuarantorTypeItemComponent],
  providers: [GuarantorTypeService, GuarantorTypeItemCustomComponent, GuarantorTypeListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [GuarantorTypeListComponent, GuarantorTypeItemComponent]
})
export class GuarantorTypeComponentModule {}
