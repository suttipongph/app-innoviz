import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreditTermService } from './credit-term.service';
import { CreditTermListComponent } from './credit-term-list/credit-term-list.component';
import { CreditTermItemComponent } from './credit-term-item/credit-term-item.component';
import { CreditTermItemCustomComponent } from './credit-term-item/credit-term-item-custom.component';
import { CreditTermListCustomComponent } from './credit-term-list/credit-term-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CreditTermListComponent, CreditTermItemComponent],
  providers: [CreditTermService, CreditTermItemCustomComponent, CreditTermListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CreditTermListComponent, CreditTermItemComponent]
})
export class CreditTermComponentModule {}
