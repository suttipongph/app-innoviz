import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditTermItemComponent } from './credit-term-item.component';

describe('CreditTermItemViewComponent', () => {
  let component: CreditTermItemComponent;
  let fixture: ComponentFixture<CreditTermItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditTermItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditTermItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
