import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CreditTermItemView } from 'shared/models/viewModel';

const firstGroup = ['CREDIT_TERM_ID'];

export class CreditTermItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CreditTermItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.creditTermGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CreditTermItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<CreditTermItemView> {
    let model: CreditTermItemView = new CreditTermItemView();
    model.creditTermGUID = EmptyGuid;
    return of(model);
  }
}
