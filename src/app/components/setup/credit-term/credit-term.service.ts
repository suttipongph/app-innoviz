import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CreditTermItemView, CreditTermListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CreditTermService {
  serviceKey = 'creditTermGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCreditTermToList(search: SearchParameter): Observable<SearchResult<CreditTermListView>> {
    const url = `${this.servicePath}/GetCreditTermList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCreditTerm(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCreditTerm`;
    return this.dataGateway.delete(url, row);
  }
  getCreditTermById(id: string): Observable<CreditTermItemView> {
    const url = `${this.servicePath}/GetCreditTermById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCreditTerm(vmModel: CreditTermItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCreditTerm`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCreditTerm(vmModel: CreditTermItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCreditTerm`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
