import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditTermListComponent } from './credit-term-list.component';

describe('CreditTermListViewComponent', () => {
  let component: CreditTermListComponent;
  let fixture: ComponentFixture<CreditTermListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditTermListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditTermListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
