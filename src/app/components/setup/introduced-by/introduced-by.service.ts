import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { IntroducedByItemView, IntroducedByListView } from 'shared/models/viewModel';

@Injectable()
export class IntroducedByService {
  serviceKey = 'introducedByGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getIntroducedByToList(search: SearchParameter): Observable<SearchResult<IntroducedByListView>> {
    const url = `${this.servicePath}/GetIntroducedByList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteIntroducedBy(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteIntroducedBy`;
    return this.dataGateway.delete(url, row);
  }
  getIntroducedByById(id: string): Observable<IntroducedByItemView> {
    const url = `${this.servicePath}/GetIntroducedByById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createIntroducedBy(vmModel: IntroducedByItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateIntroducedBy`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateIntroducedBy(vmModel: IntroducedByItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateIntroducedBy`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
