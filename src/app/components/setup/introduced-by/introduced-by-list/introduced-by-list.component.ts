import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { IntroducedByListView } from 'shared/models/viewModel';
import { IntroducedByService } from '../introduced-by.service';
import { IntroducedByListCustomComponent } from './introduced-by-list-custom.component';

@Component({
  selector: 'introduced-by-list',
  templateUrl: './introduced-by-list.component.html',
  styleUrls: ['./introduced-by-list.component.scss']
})
export class IntroducedByListComponent extends BaseListComponent<IntroducedByListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  vendorTableOptions: SelectItems[] = [];
  constructor(private service: IntroducedByService, public custom: IntroducedByListCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getVendorTableDropDown(this.vendorTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'introducedByGUID';
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    const columns: ColumnModel[] = [
      { label: 'LABEL.INTRODUCED_BY_ID', textKey: 'introducedById', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE },
      { label: 'LABEL.DESCRIPTION', textKey: 'description', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE },
      {
        label: 'LABEL.VENDOR_ID',
        textKey: 'vendorTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.vendorTableOptions,
        searchingKey: 'vendorTableGUID',
        sortingKey: 'vendorTable_VendorId'
      }
    ];

    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<IntroducedByListView>> {
    return this.service.getIntroducedByToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteIntroducedBy(row));
  }
}
