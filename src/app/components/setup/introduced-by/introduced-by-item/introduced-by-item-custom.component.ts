import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { IntroducedByItemView } from 'shared/models/viewModel';

const firstGroup = ['INTRODUCED_BY_ID'];
export class IntroducedByItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: IntroducedByItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.introducedByGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: IntroducedByItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<IntroducedByItemView> {
    let model: IntroducedByItemView = new IntroducedByItemView();
    model.introducedByGUID = EmptyGuid;
    return of(model);
  }
}
