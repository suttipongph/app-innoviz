import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IntroducedByService } from './introduced-by.service';
import { IntroducedByListComponent } from './introduced-by-list/introduced-by-list.component';
import { IntroducedByItemComponent } from './introduced-by-item/introduced-by-item.component';
import { IntroducedByItemCustomComponent } from './introduced-by-item/introduced-by-item-custom.component';
import { IntroducedByListCustomComponent } from './introduced-by-list/introduced-by-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [IntroducedByListComponent, IntroducedByItemComponent],
  providers: [IntroducedByService, IntroducedByItemCustomComponent, IntroducedByListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [IntroducedByListComponent, IntroducedByItemComponent]
})
export class IntroducedByComponentModule {}
