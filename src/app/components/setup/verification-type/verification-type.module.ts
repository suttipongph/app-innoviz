import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VerificationTypeService } from './verification-type.service';
import { VerificationTypeListComponent } from './verification-type-list/verification-type-list.component';
import { VerificationTypeItemComponent } from './verification-type-item/verification-type-item.component';
import { VerificationTypeItemCustomComponent } from './verification-type-item/verification-type-item-custom.component';
import { VerificationTypeListCustomComponent } from './verification-type-list/verification-type-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [VerificationTypeListComponent, VerificationTypeItemComponent],
  providers: [VerificationTypeService, VerificationTypeItemCustomComponent, VerificationTypeListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [VerificationTypeListComponent, VerificationTypeItemComponent]
})
export class VerificationTypeComponentModule {}
