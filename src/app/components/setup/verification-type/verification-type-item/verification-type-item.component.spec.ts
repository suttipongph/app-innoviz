import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerificationTypeItemComponent } from './verification-type-item.component';

describe('VerificationTypeItemViewComponent', () => {
  let component: VerificationTypeItemComponent;
  let fixture: ComponentFixture<VerificationTypeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VerificationTypeItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationTypeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
