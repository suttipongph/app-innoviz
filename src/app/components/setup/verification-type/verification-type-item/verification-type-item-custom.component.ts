import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { VerificationTypeItemView } from 'shared/models/viewModel';

const firstGroup = ['VERIFICATION_TYPE_ID'];

export class VerificationTypeItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: VerificationTypeItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.verificationTypeGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: VerificationTypeItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<VerificationTypeItemView> {
    let model: VerificationTypeItemView = new VerificationTypeItemView();
    model.verificationTypeGUID = EmptyGuid;
    return of(model);
  }
}
