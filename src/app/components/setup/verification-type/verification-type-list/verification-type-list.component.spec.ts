import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { VerificationTypeListComponent } from './verification-type-list.component';

describe('VerificationTypeListViewComponent', () => {
  let component: VerificationTypeListComponent;
  let fixture: ComponentFixture<VerificationTypeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VerificationTypeListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
