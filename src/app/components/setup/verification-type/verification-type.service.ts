import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { VerificationTypeItemView, VerificationTypeListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class VerificationTypeService {
  serviceKey = 'verificationTypeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getVerificationTypeToList(search: SearchParameter): Observable<SearchResult<VerificationTypeListView>> {
    const url = `${this.servicePath}/GetVerificationTypeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteVerificationType(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteVerificationType`;
    return this.dataGateway.delete(url, row);
  }
  getVerificationTypeById(id: string): Observable<VerificationTypeItemView> {
    const url = `${this.servicePath}/GetVerificationTypeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createVerificationType(vmModel: VerificationTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateVerificationType`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateVerificationType(vmModel: VerificationTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateVerificationType`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
