import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { CompanyParameterItemView, CompanyParameterListView } from 'shared/models/viewModel';

@Injectable()
export class CompanyParameterService {
  serviceKey = 'companyParameterGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCompanyParameterToList(search: SearchParameter): Observable<SearchResult<CompanyParameterListView>> {
    const url = `${this.servicePath}/GetCompanyParameterList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCompanyParameter(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCompanyParameter`;
    return this.dataGateway.delete(url, row);
  }
  getCompanyParameterById(id: string): Observable<CompanyParameterItemView> {
    const url = `${this.servicePath}/GetCompanyParameterById/id=${id}`;
    return this.dataGateway.get(url);
  }
  getCompanyParameterByCompany(id: string): Observable<CompanyParameterItemView> {
    const url = `${this.servicePath}/GetCompanyParameterByCompanyId/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCompanyParameter(vmModel: CompanyParameterItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCompanyParameter`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCompanyParameter(vmModel: CompanyParameterItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCompanyParameter`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getDataCompaneParameterValidation(vmModel: CompanyParameterItemView): Observable<boolean> {
    const url = `${this.servicePath}/GetDataCompaneParameterValidation`;
    return this.dataGateway.post(url, vmModel);
  }
}
