import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CompanyParameterService } from './company-parameter.service';
import { CompanyParameterListComponent } from './company-parameter-list/company-parameter-list.component';
import { CompanyParameterItemComponent } from './company-parameter-item/company-parameter-item.component';
import { CompanyParameterItemCustomComponent } from './company-parameter-item/company-parameter-item-custom.component';
import { CompanyParameterListCustomComponent } from './company-parameter-list/company-parameter-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CompanyParameterListComponent, CompanyParameterItemComponent],
  providers: [CompanyParameterService, CompanyParameterItemCustomComponent, CompanyParameterListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CompanyParameterListComponent, CompanyParameterItemComponent]
})
export class CompanyParameterComponentModule {}
