import { flatten } from '@angular/compiler';
import { TranslateService } from '@ngx-translate/core';
import { AppInjector } from 'app-injector';
import { MethodOfPaymentItemComponent } from 'components/setup/method-of-payment/method-of-payment-item/method-of-payment-item.component';
import { Observable, of } from 'rxjs';
import { PaymentType } from 'shared/constants';
import { isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { SelectItem } from 'shared/models/primeModel';

import { BaseServiceModel, FieldAccessing, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { CompanyParameterItemView, InvoiceTypeItemView, MethodOfPaymentItemView } from 'shared/models/viewModel';

const firstGroup = [
  'NUMBER_SEQ_VARIABLE',
  'CALC_SHEET_INC_ADDITIONAL_COST',
  'MAX_COPY_FINANCIAL_STATEMENT_NUM_OF_YEAR',
  'MAX_COPY_TAX_REPORT_NUM_OF_MONTH'
];

export class CompanyParameterItemCustomComponent {
  baseService: BaseServiceModel<any>;

  getFieldAccessing(model: CompanyParameterItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(model: CompanyParameterItemView): Observable<boolean> {
    return this.baseService.service.getDataCompaneParameterValidation(model);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CompanyParameterItemView): Observable<boolean> {
    return of(!canCreate);
  }
}
