import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, IvzDropdownOnFocusModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { CompanyParameterItemView } from 'shared/models/viewModel';
import { CompanyParameterService } from '../company-parameter.service';
import { CompanyParameterItemCustomComponent } from './company-parameter-item-custom.component';
@Component({
  selector: 'company-parameter-item',
  templateUrl: './company-parameter-item.component.html',
  styleUrls: ['./company-parameter-item.component.scss']
})
export class CompanyParameterItemComponent extends BaseItemComponent<CompanyParameterItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  //#region general
  homeCurrencyOptions: SelectItems[] = [];
  companyCalendarGroupOptions: SelectItems[] = [];
  botCalendarGroupOptions: SelectItems[] = [];
  settlementMethodOfPaymentOptions: SelectItems[] = [];
  suspenseMethodOfPaymentOptions: SelectItems[] = [];
  serviceFeeInvTypeOptions: SelectItems[] = [];
  creditRequestServiceFeeTypeOptions: SelectItems[] = [];
  //#endregion general

  //#region factoring
  ftInterestInvTypeOptions: SelectItems[] = [];
  ftInterestRefundInvTypeOptions: SelectItems[] = [];
  ftFeeInvTypeOptions: SelectItems[] = [];
  ftInvTypeOptions: SelectItems[] = [];
  ftReserveToBeRefundedInvTypeOptions: SelectItems[] = [];
  ftRetentionInvTypeOptions: SelectItems[] = [];
  ftUnearnedInterestInvTypeOptions: SelectItems[] = [];
  ftRollbillUnearnedInterestInvTypeOptions: SelectItems[] = [];
  ftBillingFeeInvRevenueTypeOptions: SelectItems[] = [];
  ftReserveInvRevenueTypeOptions: SelectItems[] = [];
  ftReceiptFeeInvRevenueTypeOptions: SelectItems[] = [];
  //#endregion factoring

  //#region project finance
  pfInterestCutDayOptions: SelectItems[] = [];
  pfInterestInvTypeOptions: SelectItems[] = [];
  pfInterestRefundInvTypeOptions: SelectItems[] = [];
  pfRetentionInvTypeOptions: SelectItems[] = [];
  pfUnearnedInterestInvTypeOptions: SelectItems[] = [];
  pfInvTypeOptions: SelectItems[] = [];
  pfTermExtensionInvRevenueTypeOptions: SelectItems[] = [];
  pfExtendCreditTermOptions: SelectItems[] = [];
  //#endregion project finance

  //#region  bond
  bondRetentionInvTypeOptions: SelectItems[] = [];
  //#endregion bond

  //#region lcdlc
  lcRetentionInvTypeOptions: SelectItems[] = [];
  //#endregion lcdlc

  //#region invoice details
  invoiceIssuingOptions: SelectItems[] = [];
  installInvTypeOptions: SelectItems[] = [];
  installAdvInvTypeOptions: SelectItems[] = [];
  initInvAdvInvTypeOptions: SelectItems[] = [];
  initInvDepInvTypeOptions: SelectItems[] = [];
  initInvDownInvTypeOptions: SelectItems[] = [];
  depositReturnInvTypeOptions: SelectItems[] = [];
  collectionInvTypeOptions: SelectItems[] = [];
  vehicleTaxInvTypeOptions: SelectItems[] = [];
  vehicleTaxServiceFeeTaxOptions: SelectItems[] = [];
  insuranceInvTypeOptions: SelectItems[] = [];
  insuranceTaxOptions: SelectItems[] = [];
  compulsoryInvTypeOptions: SelectItems[] = [];
  compulsoryTaxOptions: SelectItems[] = [];
  maintenanceTaxOptions: SelectItems[] = [];

  //#endregion invoice details

  //#region auto settlement
  earlyToleranceMethodOfPaymentOptions: SelectItems[] = [];
  earlyPayOffToleranceInvTypeOptions: SelectItems[] = [];
  paymStructureRefNumOptions: SelectItems[] = [];
  receiptGenByOptions: SelectItems[] = [];
  penaltyCalculationMethodOptions: SelectItems[] = [];
  waiveMethodOfPaymentOptions: SelectItems[] = [];
  discountMethodOfPaymentOptions: SelectItems[] = [];
  //#endregion auto settlement

  //#region  penalty
  penaltyBaseOptions: SelectItems[] = [];
  penaltyInvTypeOptions: SelectItems[] = [];
  //#endregion penalty

  //#region  collection
  collectionFeeMethodOptions: SelectItems[] = [];
  overdueTypeOptions: SelectItems[] = [];
  //#endregion collection

  //#region  classification
  nPLMethodOptions: SelectItems[] = [];
  //#endregion classification

  //#region  provision
  provisionMethodOptions: SelectItems[] = [];
  provisionRateByOptions: SelectItems[] = [];
  //#endregion
  employeeActiveOption: SelectItems[] = [];

  constructor(
    private service: CompanyParameterService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CompanyParameterItemCustomComponent
  ) {
    super();
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getPaymStructureRefNumDropDown(this.paymStructureRefNumOptions),
      this.baseDropdown.getReceiptGenByDropDown(this.receiptGenByOptions),
      this.baseDropdown.getPenaltyBaseDropDown(this.penaltyBaseOptions),
      this.baseDropdown.getCollectionFeeMethodDropDown(this.collectionFeeMethodOptions),
      this.baseDropdown.getNPLMethodDropDown(this.nPLMethodOptions),
      this.baseDropdown.getProvisionMethodDropDown(this.provisionMethodOptions),
      this.baseDropdown.getProvisionRateByDropDown(this.provisionRateByOptions),
      this.baseDropdown.getPenaltyCalculationMethodDropDown(this.penaltyCalculationMethodOptions),
      this.baseDropdown.getOverdueTypeDropDown(this.overdueTypeOptions),
      this.baseDropdown.getInvoiceIssuingToDropDown(this.invoiceIssuingOptions);
    this.baseDropdown.getDayOfMonthEnumDropDown(this.pfInterestCutDayOptions);
  }
  getById(): Observable<CompanyParameterItemView> {
    return this.service.getCompanyParameterByCompany(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.onAsyncRunner();
    this.model.companyParameterGUID = EmptyGuid;
    super.setDefaultValueSystemFields();
  }
  onAsyncRunner(model?: any): void {
    forkJoin([
      this.baseDropdown.getCurrencyDropDown(),
      this.baseDropdown.getMethodOfPaymentDropDown(),
      this.baseDropdown.getTaxTableDropDown(),
      this.baseDropdown.getInvoiceTypeDropDown(),
      this.baseDropdown.getCalendarGroupDropDown(),
      this.baseDropdown.getInvoiceRevenueTypeDropDown(),
      this.baseDropdown.getCreditTermDropDown(),
      this.baseDropdown.getEmployeeFilterActiveStatusDropdown(model?.operReportSignatureGUID)
    ]).subscribe(
      ([currency, methodOfPayment, taxTable, invoiceType, calendargroup, invoiceRevenueType, creditTerm, employeeActive]) => {
        //#region general
        this.homeCurrencyOptions = currency;
        this.companyCalendarGroupOptions = calendargroup;
        this.botCalendarGroupOptions = calendargroup;
        this.settlementMethodOfPaymentOptions = methodOfPayment;
        this.suspenseMethodOfPaymentOptions = methodOfPayment;
        this.serviceFeeInvTypeOptions = invoiceType;
        this.creditRequestServiceFeeTypeOptions = invoiceRevenueType;
        //#endregion general

        //#region factoring
        this.ftInterestInvTypeOptions = invoiceType;
        this.ftInterestRefundInvTypeOptions = invoiceType;
        this.ftFeeInvTypeOptions = invoiceType;
        this.ftInvTypeOptions = invoiceType;
        this.ftReserveToBeRefundedInvTypeOptions = invoiceType;
        this.ftRetentionInvTypeOptions = invoiceType;
        this.ftUnearnedInterestInvTypeOptions = invoiceType;
        this.ftRollbillUnearnedInterestInvTypeOptions = invoiceType;
        this.ftBillingFeeInvRevenueTypeOptions = invoiceRevenueType;
        this.ftReserveInvRevenueTypeOptions = invoiceRevenueType;
        this.ftReceiptFeeInvRevenueTypeOptions = invoiceRevenueType;
        //#endregion factoring

        //#region project finance
        this.pfInterestInvTypeOptions = invoiceType;
        this.pfRetentionInvTypeOptions = invoiceType;
        this.pfInterestRefundInvTypeOptions = invoiceType;
        this.pfUnearnedInterestInvTypeOptions = invoiceType;
        this.pfInvTypeOptions = invoiceType;
        this.pfTermExtensionInvRevenueTypeOptions = invoiceRevenueType;
        this.pfExtendCreditTermOptions = creditTerm;
        //#endregion project finance

        //#region  bond
        this.bondRetentionInvTypeOptions = invoiceType;
        //#endregion bond

        //#region lcdlc
        this.lcRetentionInvTypeOptions = invoiceType;
        //#endregion lcdlc

        //#region invoice details
        this.installInvTypeOptions = invoiceType;
        this.installAdvInvTypeOptions = invoiceType;
        this.initInvAdvInvTypeOptions = invoiceType;
        this.initInvDepInvTypeOptions = invoiceType;
        this.initInvDownInvTypeOptions = invoiceType;
        this.depositReturnInvTypeOptions = invoiceType;
        this.collectionInvTypeOptions = invoiceType;
        this.vehicleTaxInvTypeOptions = invoiceType;
        this.vehicleTaxServiceFeeTaxOptions = taxTable;
        this.insuranceInvTypeOptions = invoiceType;
        this.insuranceTaxOptions = taxTable;
        this.compulsoryInvTypeOptions = invoiceType;
        this.compulsoryTaxOptions = taxTable;
        this.maintenanceTaxOptions = taxTable;
        //#endregion invoice details

        //#region auto settlement
        this.earlyToleranceMethodOfPaymentOptions = methodOfPayment;
        this.earlyPayOffToleranceInvTypeOptions = invoiceType;
        this.waiveMethodOfPaymentOptions = methodOfPayment;
        this.discountMethodOfPaymentOptions = methodOfPayment;
        //#endregion auto settlement

        //#region  penalty
        this.penaltyInvTypeOptions = invoiceType;
        //#endregion penalty

        this.employeeActiveOption = employeeActive;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {}
  setFunctionOptions(): void {}
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateCompanyParameter(this.model), isColsing);
    } else {
      super.onCreate(this.service.createCompanyParameter(this.model), isColsing);
    }
  }
  toList() {
    this.uiService.getNavigator('/');
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation(this.model);
  }

  onOver() {
    if (!(this.model.earlyToleranceOverAmount >= 0)) {
      return {
        code: 'ERROR.GREATER_THAN_EQUAL_OR_ZERO',
        parameters: [this.translate.get('LABEL.AMOUNT')['value']]
      };
    } else {
      return null;
    }
  }

  onUnder() {
    if (!(this.model.earlyToleranceUnderAmount <= 0)) {
      return {
        code: 'ERROR.LESS_THAN_ZERO',
        parameters: [this.translate.get('LABEL.AMOUNT')['value']]
      };
    } else {
      return null;
    }
  }
  onEmployeeTableDropdownFocus(e: IvzDropdownOnFocusModel): void {
    super.setDropdownOptionOnFocus(e, this.model, this.baseDropdown.getEmployeeFilterActiveStatusDropdown()).subscribe((result) => {
      this.employeeActiveOption = result;
    });
  }
}
