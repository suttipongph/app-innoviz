import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddressCountryService } from './address-country.service';
import { AddressCountryListComponent } from './address-country-list/address-country-list.component';
import { AddressCountryItemComponent } from './address-country-item/address-country-item.component';
import { AddressCountryItemCustomComponent } from './address-country-item/address-country-item-custom.component';
import { AddressCountryListCustomComponent } from './address-country-list/address-country-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [AddressCountryListComponent, AddressCountryItemComponent],
  providers: [AddressCountryService, AddressCountryItemCustomComponent, AddressCountryListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [AddressCountryListComponent, AddressCountryItemComponent]
})
export class AddressCountryComponentModule {}
