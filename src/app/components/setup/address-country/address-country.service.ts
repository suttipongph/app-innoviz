import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { AddressCountryItemView, AddressCountryListView } from 'shared/models/viewModel';

@Injectable()
export class AddressCountryService {
  serviceKey = 'addressCountryGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getAddressCountryToList(search: SearchParameter): Observable<SearchResult<AddressCountryListView>> {
    const url = `${this.servicePath}/GetAddressCountryList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteAddressCountry(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteAddressCountry`;
    return this.dataGateway.delete(url, row);
  }
  getAddressCountryById(id: string): Observable<AddressCountryItemView> {
    const url = `${this.servicePath}/GetAddressCountryById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createAddressCountry(vmModel: AddressCountryItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateAddressCountry`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateAddressCountry(vmModel: AddressCountryItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateAddressCountry`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
