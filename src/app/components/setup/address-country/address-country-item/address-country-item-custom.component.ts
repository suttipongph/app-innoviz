import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { AddressCountryItemView } from 'shared/models/viewModel';

const firstGroup = ['COUNTRY_ID'];
export class AddressCountryItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: AddressCountryItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.addressCountryGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: AddressCountryItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<AddressCountryItemView> {
    let model: AddressCountryItemView = new AddressCountryItemView();
    model.addressCountryGUID = EmptyGuid;
    return of(model);
  }
}
