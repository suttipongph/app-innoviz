import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthorizedPersonTypeService } from './authorized-person-type.service';
import { AuthorizedPersonTypeListComponent } from './authorized-person-type-list/authorized-person-type-list.component';
import { AuthorizedPersonTypeItemComponent } from './authorized-person-type-item/authorized-person-type-item.component';
import { AuthorizedPersonTypeItemCustomComponent } from './authorized-person-type-item/authorized-person-type-item-custom.component';
import { AuthorizedPersonTypeListCustomComponent } from './authorized-person-type-list/authorized-person-type-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [AuthorizedPersonTypeListComponent, AuthorizedPersonTypeItemComponent],
  providers: [AuthorizedPersonTypeService, AuthorizedPersonTypeItemCustomComponent, AuthorizedPersonTypeListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [AuthorizedPersonTypeListComponent, AuthorizedPersonTypeItemComponent]
})
export class AuthorizedPersonTypeComponentModule {}
