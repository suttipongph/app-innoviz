import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AuthorizedPersonTypeListComponent } from './authorized-person-type-list.component';

describe('AuthorizedPersonTypeListViewComponent', () => {
  let component: AuthorizedPersonTypeListComponent;
  let fixture: ComponentFixture<AuthorizedPersonTypeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AuthorizedPersonTypeListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorizedPersonTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
