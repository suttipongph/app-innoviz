import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorizedPersonTypeItemComponent } from './authorized-person-type-item.component';

describe('AuthorizedPersonTypeItemViewComponent', () => {
  let component: AuthorizedPersonTypeItemComponent;
  let fixture: ComponentFixture<AuthorizedPersonTypeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AuthorizedPersonTypeItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorizedPersonTypeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
