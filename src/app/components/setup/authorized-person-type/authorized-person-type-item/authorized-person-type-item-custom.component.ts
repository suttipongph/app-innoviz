import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { AuthorizedPersonTypeItemView } from 'shared/models/viewModel';

const firstGroup = ['AUTHORIZED_PERSON_TYPE_ID'];

export class AuthorizedPersonTypeItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: AuthorizedPersonTypeItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.authorizedPersonTypeGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: AuthorizedPersonTypeItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<AuthorizedPersonTypeItemView> {
    let model: AuthorizedPersonTypeItemView = new AuthorizedPersonTypeItemView();
    model.authorizedPersonTypeGUID = EmptyGuid;
    return of(model);
  }
}
