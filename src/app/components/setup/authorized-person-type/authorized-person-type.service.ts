import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AuthorizedPersonTypeItemView, AuthorizedPersonTypeListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class AuthorizedPersonTypeService {
  serviceKey = 'authorizedPersonTypeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getAuthorizedPersonTypeToList(search: SearchParameter): Observable<SearchResult<AuthorizedPersonTypeListView>> {
    const url = `${this.servicePath}/GetAuthorizedPersonTypeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteAuthorizedPersonType(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteAuthorizedPersonType`;
    return this.dataGateway.delete(url, row);
  }
  getAuthorizedPersonTypeById(id: string): Observable<AuthorizedPersonTypeItemView> {
    const url = `${this.servicePath}/GetAuthorizedPersonTypeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createAuthorizedPersonType(vmModel: AuthorizedPersonTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateAuthorizedPersonType`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateAuthorizedPersonType(vmModel: AuthorizedPersonTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateAuthorizedPersonType`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
