import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GradeClassificationListComponent } from './grade-classification-list.component';

describe('GradeClassificationListViewComponent', () => {
  let component: GradeClassificationListComponent;
  let fixture: ComponentFixture<GradeClassificationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GradeClassificationListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GradeClassificationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
