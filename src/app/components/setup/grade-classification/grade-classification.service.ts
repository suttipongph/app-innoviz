import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { GradeClassificationItemView, GradeClassificationListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class GradeClassificationService {
  serviceKey = 'gradeClassificationGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getGradeClassificationToList(search: SearchParameter): Observable<SearchResult<GradeClassificationListView>> {
    const url = `${this.servicePath}/GetGradeClassificationList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteGradeClassification(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteGradeClassification`;
    return this.dataGateway.delete(url, row);
  }
  getGradeClassificationById(id: string): Observable<GradeClassificationItemView> {
    const url = `${this.servicePath}/GetGradeClassificationById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createGradeClassification(vmModel: GradeClassificationItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateGradeClassification`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateGradeClassification(vmModel: GradeClassificationItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateGradeClassification`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
