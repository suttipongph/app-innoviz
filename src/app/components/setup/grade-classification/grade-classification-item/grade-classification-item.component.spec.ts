import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GradeClassificationItemComponent } from './grade-classification-item.component';

describe('GradeClassificationItemViewComponent', () => {
  let component: GradeClassificationItemComponent;
  let fixture: ComponentFixture<GradeClassificationItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GradeClassificationItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GradeClassificationItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
