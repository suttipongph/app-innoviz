import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { GradeClassificationItemView } from 'shared/models/viewModel';

const firstGroup = ['GRADE_CLASSIFICATION_ID'];

export class GradeClassificationItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: GradeClassificationItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.gradeClassificationGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: GradeClassificationItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<GradeClassificationItemView> {
    let model: GradeClassificationItemView = new GradeClassificationItemView();
    model.gradeClassificationGUID = EmptyGuid;
    return of(model);
  }
}
