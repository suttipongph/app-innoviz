import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GradeClassificationService } from './grade-classification.service';
import { GradeClassificationListComponent } from './grade-classification-list/grade-classification-list.component';
import { GradeClassificationItemComponent } from './grade-classification-item/grade-classification-item.component';
import { GradeClassificationItemCustomComponent } from './grade-classification-item/grade-classification-item-custom.component';
import { GradeClassificationListCustomComponent } from './grade-classification-list/grade-classification-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [GradeClassificationListComponent, GradeClassificationItemComponent],
  providers: [GradeClassificationService, GradeClassificationItemCustomComponent, GradeClassificationListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [GradeClassificationListComponent, GradeClassificationItemComponent]
})
export class GradeClassificationComponentModule {}
