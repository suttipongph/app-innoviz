import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RaceService } from './race.service';
import { RaceListComponent } from './race-list/race-list.component';
import { RaceItemComponent } from './race-item/race-item.component';
import { RaceItemCustomComponent } from './race-item/race-item-custom.component';
import { RaceListCustomComponent } from './race-list/race-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [RaceListComponent, RaceItemComponent],
  providers: [RaceService, RaceItemCustomComponent, RaceListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [RaceListComponent, RaceItemComponent]
})
export class RaceComponentModule {}
