import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { RaceItemView, RaceListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class RaceService {
  serviceKey = 'raceGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getRaceToList(search: SearchParameter): Observable<SearchResult<RaceListView>> {
    const url = `${this.servicePath}/GetRaceList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteRace(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteRace`;
    return this.dataGateway.delete(url, row);
  }
  getRaceById(id: string): Observable<RaceItemView> {
    const url = `${this.servicePath}/GetRaceById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createRace(vmModel: RaceItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateRace`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateRace(vmModel: RaceItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateRace`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
