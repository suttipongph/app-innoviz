import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { RaceItemView } from 'shared/models/viewModel';

const firstGroup = ['RACE_ID'];

export class RaceItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: RaceItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.raceGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: RaceItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<RaceItemView> {
    let model: RaceItemView = new RaceItemView();
    model.raceGUID = EmptyGuid;
    return of(model);
  }
}
