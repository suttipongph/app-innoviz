import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlacklistStatusItemComponent } from './blacklist-status-item.component';

describe('BlacklistStatusItemViewComponent', () => {
  let component: BlacklistStatusItemComponent;
  let fixture: ComponentFixture<BlacklistStatusItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BlacklistStatusItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlacklistStatusItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
