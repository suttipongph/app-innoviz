import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { BlacklistStatusItemView } from 'shared/models/viewModel';

const firstGroup = ['BLACKLIST_STATUS_ID'];

export class BlacklistStatusItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: BlacklistStatusItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.blacklistStatusGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BlacklistStatusItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<BlacklistStatusItemView> {
    let model: BlacklistStatusItemView = new BlacklistStatusItemView();
    model.blacklistStatusGUID = EmptyGuid;
    return of(model);
  }
}
