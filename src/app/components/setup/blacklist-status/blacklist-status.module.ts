import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BlacklistStatusService } from './blacklist-status.service';
import { BlacklistStatusListComponent } from './blacklist-status-list/blacklist-status-list.component';
import { BlacklistStatusItemComponent } from './blacklist-status-item/blacklist-status-item.component';
import { BlacklistStatusItemCustomComponent } from './blacklist-status-item/blacklist-status-item-custom.component';
import { BlacklistStatusListCustomComponent } from './blacklist-status-list/blacklist-status-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BlacklistStatusListComponent, BlacklistStatusItemComponent],
  providers: [BlacklistStatusService, BlacklistStatusItemCustomComponent, BlacklistStatusListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BlacklistStatusListComponent, BlacklistStatusItemComponent]
})
export class BlacklistStatusComponentModule {}
