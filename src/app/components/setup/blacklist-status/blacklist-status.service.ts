import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BlacklistStatusItemView, BlacklistStatusListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class BlacklistStatusService {
  serviceKey = 'blacklistStatusGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBlacklistStatusToList(search: SearchParameter): Observable<SearchResult<BlacklistStatusListView>> {
    const url = `${this.servicePath}/GetBlacklistStatusList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBlacklistStatus(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBlacklistStatus`;
    return this.dataGateway.delete(url, row);
  }
  getBlacklistStatusById(id: string): Observable<BlacklistStatusItemView> {
    const url = `${this.servicePath}/GetBlacklistStatusById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBlacklistStatus(vmModel: BlacklistStatusItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBlacklistStatus`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBlacklistStatus(vmModel: BlacklistStatusItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBlacklistStatus`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
