import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BlacklistStatusListComponent } from './blacklist-status-list.component';

describe('BlacklistStatusListViewComponent', () => {
  let component: BlacklistStatusListComponent;
  let fixture: ComponentFixture<BlacklistStatusListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BlacklistStatusListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlacklistStatusListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
