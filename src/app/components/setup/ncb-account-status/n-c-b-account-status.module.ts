import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NCBAccountStatusService } from './n-c-b-account-status.service';
import { NCBAccountStatusListComponent } from './ncb-account-status-list/n-c-b-account-status-list.component';
import { NCBAccountStatusItemComponent } from './ncb-account-status-item/n-c-b-account-status-item.component';
import { NCBAccountStatusItemCustomComponent } from './ncb-account-status-item/n-c-b-account-status-item-custom.component';
import { NCBAccountStatusListCustomComponent } from './ncb-account-status-list/n-c-b-account-status-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [NCBAccountStatusListComponent, NCBAccountStatusItemComponent],
  providers: [NCBAccountStatusService, NCBAccountStatusItemCustomComponent, NCBAccountStatusListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [NCBAccountStatusListComponent, NCBAccountStatusItemComponent]
})
export class NCBAccountStatusComponentModule {}
