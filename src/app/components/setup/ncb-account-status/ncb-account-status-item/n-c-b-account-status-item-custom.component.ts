import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { NCBAccountStatusItemView } from 'shared/models/viewModel';

const firstGroup = ['NCB_ACC_STATUS_ID'];
export class NCBAccountStatusItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: NCBAccountStatusItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.ncbAccountStatusGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: NCBAccountStatusItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<NCBAccountStatusItemView> {
    let model: NCBAccountStatusItemView = new NCBAccountStatusItemView();
    model.ncbAccountStatusGUID = EmptyGuid;
    return of(model);
  }
}
