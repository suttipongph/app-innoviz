import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { NCBAccountStatusItemView, NCBAccountStatusListView } from 'shared/models/viewModel';

@Injectable()
export class NCBAccountStatusService {
  serviceKey = 'ncbAccountStatusGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getNCBAccountStatusToList(search: SearchParameter): Observable<SearchResult<NCBAccountStatusListView>> {
    const url = `${this.servicePath}/GetNCBAccountStatusList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteNCBAccountStatus(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteNCBAccountStatus`;
    return this.dataGateway.delete(url, row);
  }
  getNCBAccountStatusById(id: string): Observable<NCBAccountStatusItemView> {
    const url = `${this.servicePath}/GetNCBAccountStatusById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createNCBAccountStatus(vmModel: NCBAccountStatusItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateNCBAccountStatus`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateNCBAccountStatus(vmModel: NCBAccountStatusItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateNCBAccountStatus`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
