import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { NumberSeqTableItemView } from 'shared/models/viewModel';
import { NumberSeqTableService } from '../number-seq-table.service';
import { NumberSeqTableItemCustomComponent } from './number-seq-table-item-custom.component';
@Component({
  selector: 'number-seq-table-item',
  templateUrl: './number-seq-table-item.component.html',
  styleUrls: ['./number-seq-table-item.component.scss']
})
export class NumberSeqTableItemComponent extends BaseItemComponent<NumberSeqTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  constructor(
    private service: NumberSeqTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: NumberSeqTableItemCustomComponent
  ) {
    super();
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {}
  getById(): Observable<NumberSeqTableItemView> {
    return this.service.getNumberSeqTableById(this.id);
  }
  getInitialData(): Observable<NumberSeqTableItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }
  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {}
  setFunctionOptions(): void {}

  // get dropdown item ##########################################################

  // validate field ############################################################
  validateSmallest() {
    if (this.model.smallest > this.model.largest) {
      return this.showErrorTag(['SMALLEST', 'GREATER_THAN', 'LARGEST']);
    } else if (this.model.smallest > this.model.next) {
      return this.showErrorTag(['SMALLEST', 'GREATER_THAN', 'NEXT']);
    } else {
      return null;
    }
  }

  validateNext() {
    if (this.model.next < this.model.smallest) {
      return this.showErrorTag(['NEXT', 'LESS_THAN', 'SMALLEST']);
    } else if (this.model.next > this.model.largest) {
      return this.showErrorTag(['NEXT', 'GREATER_THAN', 'LARGEST']);
    } else {
      return null;
    }
  }

  validateLargest() {
    if (this.model.largest < this.model.smallest) {
      return this.showErrorTag(['LARGEST', 'LESS_THAN', 'SMALLEST']);
    } else if (this.model.largest < this.model.next) {
      return this.showErrorTag(['LARGEST', 'LESS_THAN', 'NEXT']);
    } else {
      return null;
    }
  }
  showErrorTag(pam: Array<string>) {
    return {
      code: 'ERROR.00314',
      parameters: pam.map((p) => this.translate.instant(`LABEL.${p}`))
    };
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateNumberSeqTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createNumberSeqTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
