import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { FieldAccessing } from 'shared/models/systemModel';
import { NumberSeqTableItemView } from 'shared/models/viewModel';

const firstGroup = ['NUMBER_SEQ_CODE'];
export class NumberSeqTableItemCustomComponent {
  getFieldAccessing(model: NumberSeqTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.numberSeqTableGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: NumberSeqTableItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<NumberSeqTableItemView> {
    let model: NumberSeqTableItemView = new NumberSeqTableItemView();
    model.numberSeqTableGUID = EmptyGuid;
    model.smallest = 1;
    model.largest = 99999;
    model.next = 1;
    model.manual = false;
    return of(model);
  }
}
