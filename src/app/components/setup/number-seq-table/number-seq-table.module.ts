import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NumberSeqTableService } from './number-seq-table.service';
import { NumberSeqTableListComponent } from './number-seq-table-list/number-seq-table-list.component';
import { NumberSeqTableItemComponent } from './number-seq-table-item/number-seq-table-item.component';
import { NumberSeqTableItemCustomComponent } from './number-seq-table-item/number-seq-table-item-custom.component';
import { NumberSeqTableListCustomComponent } from './number-seq-table-list/number-seq-table-list-custom.component';
import { NumberSeqSegmentComponentModule } from '../number-seq-segment/number-seq-segment.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, NumberSeqSegmentComponentModule],
  declarations: [NumberSeqTableListComponent, NumberSeqTableItemComponent],
  providers: [NumberSeqTableService, NumberSeqTableItemCustomComponent, NumberSeqTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [NumberSeqTableListComponent, NumberSeqTableItemComponent]
})
export class NumberSeqTableComponentModule {}
