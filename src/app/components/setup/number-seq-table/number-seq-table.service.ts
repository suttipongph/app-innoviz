import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { NumberSeqTableItemView, NumberSeqTableListView } from 'shared/models/viewModel';

@Injectable()
export class NumberSeqTableService {
  serviceKey = 'numberSeqTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getNumberSeqTableToList(search: SearchParameter): Observable<SearchResult<NumberSeqTableListView>> {
    const url = `${this.servicePath}/GetNumberSeqTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteNumberSeqTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteNumberSeqTable`;
    return this.dataGateway.delete(url, row);
  }
  getNumberSeqTableById(id: string): Observable<NumberSeqTableItemView> {
    const url = `${this.servicePath}/GetNumberSeqTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createNumberSeqTable(vmModel: NumberSeqTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateNumberSeqTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateNumberSeqTable(vmModel: NumberSeqTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateNumberSeqTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
