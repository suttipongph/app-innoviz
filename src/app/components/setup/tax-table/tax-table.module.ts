import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TaxTableService } from './tax-table.service';
import { TaxTableListComponent } from './tax-table-list/tax-table-list.component';
import { TaxTableItemComponent } from './tax-table-item/tax-table-item.component';
import { TaxTableItemCustomComponent } from './tax-table-item/tax-table-item-custom.component';
import { TaxTableListCustomComponent } from './tax-table-list/tax-table-list-custom.component';
import { TaxValueComponentModule } from '../tax-value/tax-value.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, TaxValueComponentModule],
  declarations: [TaxTableListComponent, TaxTableItemComponent],
  providers: [TaxTableService, TaxTableItemCustomComponent, TaxTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [TaxTableListComponent, TaxTableItemComponent]
})
export class TaxTableComponentModule {}
