import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { TaxTableItemView, TaxTableListView } from 'shared/models/viewModel';

@Injectable()
export class TaxTableService {
  serviceKey = 'taxTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getTaxTableToList(search: SearchParameter): Observable<SearchResult<TaxTableListView>> {
    const url = `${this.servicePath}/GetTaxTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteTaxTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteTaxTable`;
    return this.dataGateway.delete(url, row);
  }
  getTaxTableById(id: string): Observable<TaxTableItemView> {
    const url = `${this.servicePath}/GetTaxTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createTaxTable(vmModel: TaxTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateTaxTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateTaxTable(vmModel: TaxTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateTaxTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
