import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { TaxTableItemView } from 'shared/models/viewModel';

const firstGroup = ['TAX_CODE'];
export class TaxTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: TaxTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.taxTableGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: TaxTableItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<TaxTableItemView> {
    let model: TaxTableItemView = new TaxTableItemView();
    model.taxTableGUID = EmptyGuid;
    return of(model);
  }
}
