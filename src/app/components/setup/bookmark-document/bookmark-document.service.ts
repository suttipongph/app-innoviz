import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { BookmarkDocumentItemView, BookmarkDocumentListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class BookmarkDocumentService {
  serviceKey = 'bookmarkDocumentGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBookmarkDocumentToList(search: SearchParameter): Observable<SearchResult<BookmarkDocumentListView>> {
    const url = `${this.servicePath}/GetBookmarkDocumentList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBookmarkDocument(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBookmarkDocument`;
    return this.dataGateway.delete(url, row);
  }
  getBookmarkDocumentById(id: string): Observable<BookmarkDocumentItemView> {
    const url = `${this.servicePath}/GetBookmarkDocumentById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBookmarkDocument(vmModel: BookmarkDocumentItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBookmarkDocument`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBookmarkDocument(vmModel: BookmarkDocumentItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBookmarkDocument`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
