import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookmarkDocumentItemComponent } from './bookmark-document-item.component';

describe('BookmarkDocumentItemViewComponent', () => {
  let component: BookmarkDocumentItemComponent;
  let fixture: ComponentFixture<BookmarkDocumentItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BookmarkDocumentItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkDocumentItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
