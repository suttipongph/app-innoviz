import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { FieldAccessing } from 'shared/models/systemModel';
import { BookmarkDocumentItemView } from 'shared/models/viewModel';

const firstGroup = ['BOOKMARK_DOCUMENT_ID'];

export class BookmarkDocumentItemCustomComponent {
  getFieldAccessing(model: BookmarkDocumentItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.bookmarkDocumentGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BookmarkDocumentItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<BookmarkDocumentItemView> {
    let model: BookmarkDocumentItemView = new BookmarkDocumentItemView();
    model.bookmarkDocumentGUID = EmptyGuid;
    return of(model);
  }
}
