import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { BookmarkDocumentListView } from 'shared/models/viewModel';
import { BookmarkDocumentService } from '../bookmark-document.service';
import { BookmarkDocumentListCustomComponent } from './bookmark-document-list-custom.component';

@Component({
  selector: 'bookmark-document-list',
  templateUrl: './bookmark-document-list.component.html',
  styleUrls: ['./bookmark-document-list.component.scss']
})
export class BookmarkDocumentListComponent extends BaseListComponent<BookmarkDocumentListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  bookmarkDocumentRefTypeOptions: SelectItems[] = [];
  documentTemplateTypeOptions: SelectItems[] = [];

  constructor(public custom: BookmarkDocumentListCustomComponent, private service: BookmarkDocumentService) {
    super();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getBookmarkDocumentRefTypeEnumDropDown(this.bookmarkDocumentRefTypeOptions);
    this.baseDropdown.getDocumentTemplateTypeEnumDropDown(this.documentTemplateTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'bookmarkDocumentGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.BOOKMARK_DOCUMENT_ID',
        textKey: 'bookmarkDocumentId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DOCUMENT_TEMPLATE_TYPE',
        textKey: 'documentTemplateType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentTemplateTypeOptions
      },
      {
        label: 'LABEL.BOOKMARK_DOCUMENT_REFERENCE_TYPE',
        textKey: 'bookmarkDocumentRefType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.bookmarkDocumentRefTypeOptions
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<BookmarkDocumentListView>> {
    return this.service.getBookmarkDocumentToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteBookmarkDocument(row));
  }
}
