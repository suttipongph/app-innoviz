import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BookmarkDocumentListComponent } from './bookmark-document-list.component';

describe('BookmarkDocumentListViewComponent', () => {
  let component: BookmarkDocumentListComponent;
  let fixture: ComponentFixture<BookmarkDocumentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BookmarkDocumentListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkDocumentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
