import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BookmarkDocumentService } from './bookmark-document.service';
import { BookmarkDocumentListComponent } from './bookmark-document-list/bookmark-document-list.component';
import { BookmarkDocumentItemComponent } from './bookmark-document-item/bookmark-document-item.component';
import { BookmarkDocumentItemCustomComponent } from './bookmark-document-item/bookmark-document-item-custom.component';
import { BookmarkDocumentListCustomComponent } from './bookmark-document-list/bookmark-document-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BookmarkDocumentListComponent, BookmarkDocumentItemComponent],
  providers: [BookmarkDocumentService, BookmarkDocumentItemCustomComponent, BookmarkDocumentListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BookmarkDocumentListComponent, BookmarkDocumentItemComponent]
})
export class BookmarkDocumentComponentModule {}
