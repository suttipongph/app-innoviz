import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WithholdingTaxValueService } from './withholding-tax-value.service';
import { WithholdingTaxValueListComponent } from './withholding-tax-value-list/withholding-tax-value-list.component';
import { WithholdingTaxValueItemComponent } from './withholding-tax-value-item/withholding-tax-value-item.component';
import { WithholdingTaxValueItemCustomComponent } from './withholding-tax-value-item/withholding-tax-value-item-custom.component';
import { WithholdingTaxValueListCustomComponent } from './withholding-tax-value-list/withholding-tax-value-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [WithholdingTaxValueListComponent, WithholdingTaxValueItemComponent],
  providers: [WithholdingTaxValueService, WithholdingTaxValueItemCustomComponent, WithholdingTaxValueListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [WithholdingTaxValueListComponent, WithholdingTaxValueItemComponent]
})
export class WithholdingTaxValueComponentModule {}
