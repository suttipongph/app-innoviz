import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { WithholdingTaxValueItemView, WithholdingTaxValueListView } from 'shared/models/viewModel';

@Injectable()
export class WithholdingTaxValueService {
  serviceKey = 'withholdingTaxValueGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getWithholdingTaxValueToList(search: SearchParameter): Observable<SearchResult<WithholdingTaxValueListView>> {
    const url = `${this.servicePath}/GetWithholdingTaxValueList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteWithholdingTaxValue(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteWithholdingTaxValue`;
    return this.dataGateway.delete(url, row);
  }
  getWithholdingTaxValueById(id: string): Observable<WithholdingTaxValueItemView> {
    const url = `${this.servicePath}/GetWithholdingTaxValueById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createWithholdingTaxValue(vmModel: WithholdingTaxValueItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateWithholdingTaxValue`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateWithholdingTaxValue(vmModel: WithholdingTaxValueItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateWithholdingTaxValue`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<WithholdingTaxValueItemView> {
    const url = `${this.servicePath}/GetWithholdingTaxValueInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
}
