import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { WithholdingTaxValueItemView } from 'shared/models/viewModel';
import { WithholdingTaxValueService } from '../withholding-tax-value.service';
import { WithholdingTaxValueItemCustomComponent } from './withholding-tax-value-item-custom.component';
@Component({
  selector: 'withholding-tax-value-item',
  templateUrl: './withholding-tax-value-item.component.html',
  styleUrls: ['./withholding-tax-value-item.component.scss']
})
export class WithholdingTaxValueItemComponent extends BaseItemComponent<WithholdingTaxValueItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  withholdingTaxTableOptions: SelectItems[] = [];
  constructor(
    private service: WithholdingTaxValueService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: WithholdingTaxValueItemCustomComponent,
    private uiControllerService: UIControllerService
  ) {
    super();
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiControllerService.getKey('withholdingtaxtable');
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<WithholdingTaxValueItemView> {
    return this.service.getWithholdingTaxValueById(this.id);
  }
  getInitialData(): Observable<WithholdingTaxValueItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }
  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getWithholdingTaxTableDropDown()).subscribe(
      ([withholdingTaxTable]) => {
        this.withholdingTaxTableOptions = withholdingTaxTable;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {}
  setFunctionOptions(): void {}
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateWithholdingTaxValue(this.model), isColsing);
    } else {
      super.onCreate(this.service.createWithholdingTaxValue(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
