import { Observable, of } from 'rxjs';
import { isNullOrUndefOrEmpty } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { WithholdingTaxValueItemView } from 'shared/models/viewModel';

const firstGroup = ['WHT_CODE'];
export class WithholdingTaxValueItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: WithholdingTaxValueItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: WithholdingTaxValueItemView): Observable<boolean> {
    return of(!canCreate);
  }
  validateEffectiveFrom(fromDate: string): any {
    if (isNullOrUndefOrEmpty(fromDate)) {
      return {
        code: 'ERROR.MANDATORY_FIELD',
        parameters: []
      };
    }
    return null;
  }
}
