import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { AddressDistrictItemView, AddressDistrictListView } from 'shared/models/viewModel';

@Injectable()
export class AddressDistrictService {
  serviceKey = 'addressDistrictGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getAddressDistrictToList(search: SearchParameter): Observable<SearchResult<AddressDistrictListView>> {
    const url = `${this.servicePath}/GetAddressDistrictList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteAddressDistrict(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteAddressDistrict`;
    return this.dataGateway.delete(url, row);
  }
  getAddressDistrictById(id: string): Observable<AddressDistrictItemView> {
    const url = `${this.servicePath}/GetAddressDistrictById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createAddressDistrict(vmModel: AddressDistrictItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateAddressDistrict`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateAddressDistrict(vmModel: AddressDistrictItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateAddressDistrict`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
