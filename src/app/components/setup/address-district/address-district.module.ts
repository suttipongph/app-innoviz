import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddressDistrictService } from './address-district.service';
import { AddressDistrictListComponent } from './address-district-list/address-district-list.component';
import { AddressDistrictItemComponent } from './address-district-item/address-district-item.component';
import { AddressDistrictItemCustomComponent } from './address-district-item/address-district-item-custom.component';
import { AddressDistrictListCustomComponent } from './address-district-list/address-district-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [AddressDistrictListComponent, AddressDistrictItemComponent],
  providers: [AddressDistrictService, AddressDistrictItemCustomComponent, AddressDistrictListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [AddressDistrictListComponent, AddressDistrictItemComponent]
})
export class AddressDistrictComponentModule {}
