import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { AddressDistrictItemView } from 'shared/models/viewModel';

const firstGroup = ['DISTRICT_ID'];
export class AddressDistrictItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: AddressDistrictItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.addressDistrictGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: AddressDistrictItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<AddressDistrictItemView> {
    let model: AddressDistrictItemView = new AddressDistrictItemView();
    model.addressDistrictGUID = EmptyGuid;
    return of(model);
  }
}
