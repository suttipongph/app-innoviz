import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { AddressDistrictListView } from 'shared/models/viewModel';
import { AddressDistrictService } from '../address-district.service';
import { AddressDistrictListCustomComponent } from './address-district-list-custom.component';

@Component({
  selector: 'address-district-list',
  templateUrl: './address-district-list.component.html',
  styleUrls: ['./address-district-list.component.scss']
})
export class AddressDistrictListComponent extends BaseListComponent<AddressDistrictListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  addressProvinceOptions: SelectItems[] = [];
  constructor(private service: AddressDistrictService, public custom: AddressDistrictListCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getAddressProvinceDropDown(this.addressProvinceOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'addressDistrictGUID';
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.PROVINCE_ID',
        textKey: 'addressProvince_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.addressProvinceOptions,
        sortingKey: 'provinceId',
        searchingKey: 'addressProvinceGUID'
      },
      { label: 'LABEL.DISTRICT_ID', textKey: 'districtId', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE },
      { label: 'LABEL.NAME', textKey: 'name', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<AddressDistrictListView>> {
    return this.service.getAddressDistrictToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteAddressDistrict(row));
  }
}
