import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { NumberSeqParameterListView } from 'shared/models/viewModel';
import { NumberSeqParameterService } from '../number-seq-parameter.service';
import { NumberSeqParameterListCustomComponent } from './number-seq-parameter-list-custom.component';

@Component({
  selector: 'number-seq-parameter-list',
  templateUrl: './number-seq-parameter-list.component.html',
  styleUrls: ['./number-seq-parameter-list.component.scss']
})
export class NumberSeqParameterListComponent extends BaseListComponent<NumberSeqParameterListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  numberSeqTableOptions: SelectItems[] = [];
  constructor(private service: NumberSeqParameterService, public custom: NumberSeqParameterListCustomComponent) {
    super();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getNumberSeqTableDropDown(this.numberSeqTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'numberSeqParameterGUID';
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    const columns: ColumnModel[] = [
      { label: 'LABEL.REFERENCE_ID', textKey: 'referenceId', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE },
      {
        label: 'LABEL.NUMBER_SEQ_CODE',
        textKey: 'numberSeqTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.numberSeqTableOptions,
        sortingKey: 'numberSeqTable_NumberSeqCode',
        searchingKey: 'numberSeqTableGUID'
      }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<NumberSeqParameterListView>> {
    return this.service.getNumberSeqParameterToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteNumberSeqParameter(row));
  }
}
