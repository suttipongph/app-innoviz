import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { FieldAccessing } from 'shared/models/systemModel';
import { NumberSeqParameterItemView } from 'shared/models/viewModel';

const firstGroup = ['REFERENCE_ID'];
export class NumberSeqParameterItemCustomComponent {
  getFieldAccessing(model: NumberSeqParameterItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.numberSeqParameterGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: NumberSeqParameterItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<NumberSeqParameterItemView> {
    let model: NumberSeqParameterItemView = new NumberSeqParameterItemView();
    model.numberSeqParameterGUID = EmptyGuid;
    return of(model);
  }
  // field change ##############################################################

  // validate field ############################################################
}
