import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { NumberSeqParameterItemView, NumberSeqParameterListView } from 'shared/models/viewModel';

@Injectable()
export class NumberSeqParameterService {
  serviceKey = 'numberSeqParameterGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getNumberSeqParameterToList(search: SearchParameter): Observable<SearchResult<NumberSeqParameterListView>> {
    const url = `${this.servicePath}/GetNumberSeqParameterList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteNumberSeqParameter(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteNumberSeqParameter`;
    return this.dataGateway.delete(url, row);
  }
  getNumberSeqParameterById(id: string): Observable<NumberSeqParameterItemView> {
    const url = `${this.servicePath}/GetNumberSeqParameterById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createNumberSeqParameter(vmModel: NumberSeqParameterItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateNumberSeqParameter`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateNumberSeqParameter(vmModel: NumberSeqParameterItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateNumberSeqParameter`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
