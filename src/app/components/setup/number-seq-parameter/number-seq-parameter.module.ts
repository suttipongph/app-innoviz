import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NumberSeqParameterService } from './number-seq-parameter.service';
import { NumberSeqParameterListComponent } from './number-seq-parameter-list/number-seq-parameter-list.component';
import { NumberSeqParameterItemComponent } from './number-seq-parameter-item/number-seq-parameter-item.component';
import { NumberSeqParameterItemCustomComponent } from './number-seq-parameter-item/number-seq-parameter-item-custom.component';
import { NumberSeqParameterListCustomComponent } from './number-seq-parameter-list/number-seq-parameter-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [NumberSeqParameterListComponent, NumberSeqParameterItemComponent],
  providers: [NumberSeqParameterService, NumberSeqParameterItemCustomComponent, NumberSeqParameterListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [NumberSeqParameterListComponent, NumberSeqParameterItemComponent]
})
export class NumberSeqParameterComponentModule {}
