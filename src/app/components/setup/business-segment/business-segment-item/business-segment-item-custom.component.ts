import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { BusinessSegmentItemView } from 'shared/models/viewModel';

const firstGroup = ['BUSINESS_SEGMENT_ID'];

export class BusinessSegmentItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: BusinessSegmentItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.businessSegmentGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BusinessSegmentItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<BusinessSegmentItemView> {
    let model: BusinessSegmentItemView = new BusinessSegmentItemView();
    model.businessSegmentGUID = EmptyGuid;
    return of(model);
  }
}
