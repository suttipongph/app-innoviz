import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessSegmentItemComponent } from './business-segment-item.component';

describe('BusinessSegmentItemViewComponent', () => {
  let component: BusinessSegmentItemComponent;
  let fixture: ComponentFixture<BusinessSegmentItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BusinessSegmentItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessSegmentItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
