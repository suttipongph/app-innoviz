import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BusinessSegmentItemView, BusinessSegmentListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class BusinessSegmentService {
  serviceKey = 'businessSegmentGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBusinessSegmentToList(search: SearchParameter): Observable<SearchResult<BusinessSegmentListView>> {
    const url = `${this.servicePath}/GetBusinessSegmentList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBusinessSegment(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBusinessSegment`;
    return this.dataGateway.delete(url, row);
  }
  getBusinessSegmentById(id: string): Observable<BusinessSegmentItemView> {
    const url = `${this.servicePath}/GetBusinessSegmentById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBusinessSegment(vmModel: BusinessSegmentItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBusinessSegment`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBusinessSegment(vmModel: BusinessSegmentItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBusinessSegment`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
