import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BusinessSegmentService } from './business-segment.service';
import { BusinessSegmentListComponent } from './business-segment-list/business-segment-list.component';
import { BusinessSegmentItemComponent } from './business-segment-item/business-segment-item.component';
import { BusinessSegmentItemCustomComponent } from './business-segment-item/business-segment-item-custom.component';
import { BusinessSegmentListCustomComponent } from './business-segment-list/business-segment-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BusinessSegmentListComponent, BusinessSegmentItemComponent],
  providers: [BusinessSegmentService, BusinessSegmentItemCustomComponent, BusinessSegmentListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BusinessSegmentListComponent, BusinessSegmentItemComponent]
})
export class BusinessSegmentComponentModule {}
