import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { BusinessSegmentListView } from 'shared/models/viewModel';
import { BusinessSegmentService } from '../business-segment.service';
import { BusinessSegmentListCustomComponent } from './business-segment-list-custom.component';

@Component({
  selector: 'business-segment-list',
  templateUrl: './business-segment-list.component.html',
  styleUrls: ['./business-segment-list.component.scss']
})
export class BusinessSegmentListComponent extends BaseListComponent<BusinessSegmentListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  businessSegmentTypeOptions: SelectItems[] = [];

  constructor(public custom: BusinessSegmentListCustomComponent, private service: BusinessSegmentService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getBusinessSegmentTypeEnumDropDown(this.businessSegmentTypeOptions);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'businessSegmentGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.BUSINESS_SEGMENT_ID',
        textKey: 'businessSegmentId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },

      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BUSINESS_SEGMENT_TYPE',
        textKey: 'businessSegmentType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.businessSegmentTypeOptions
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<BusinessSegmentListView>> {
    return this.service.getBusinessSegmentToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteBusinessSegment(row));
  }
}
