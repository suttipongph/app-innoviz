import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessSegmentListComponent } from './business-segment-list.component';

describe('BusinessSegmentListViewComponent', () => {
  let component: BusinessSegmentListComponent;
  let fixture: ComponentFixture<BusinessSegmentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BusinessSegmentListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessSegmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
