import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { NationalityItemView } from 'shared/models/viewModel';

const firstGroup = ['NATIONALITY_ID'];

export class NationalityItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: NationalityItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.nationalityGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: NationalityItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<NationalityItemView> {
    let model: NationalityItemView = new NationalityItemView();
    model.nationalityGUID = EmptyGuid;
    return of(model);
  }
}
