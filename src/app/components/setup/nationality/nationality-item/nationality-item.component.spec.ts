import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NationalityItemComponent } from './nationality-item.component';

describe('NationalityItemViewComponent', () => {
  let component: NationalityItemComponent;
  let fixture: ComponentFixture<NationalityItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NationalityItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NationalityItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
