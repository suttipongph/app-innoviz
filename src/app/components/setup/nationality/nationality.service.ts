import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { NationalityItemView, NationalityListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class NationalityService {
  serviceKey = 'nationalityGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getNationalityToList(search: SearchParameter): Observable<SearchResult<NationalityListView>> {
    const url = `${this.servicePath}/GetNationalityList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteNationality(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteNationality`;
    return this.dataGateway.delete(url, row);
  }
  getNationalityById(id: string): Observable<NationalityItemView> {
    const url = `${this.servicePath}/GetNationalityById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createNationality(vmModel: NationalityItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateNationality`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateNationality(vmModel: NationalityItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateNationality`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
