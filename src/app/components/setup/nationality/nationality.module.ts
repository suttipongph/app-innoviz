import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NationalityService } from './nationality.service';
import { NationalityListComponent } from './nationality-list/nationality-list.component';
import { NationalityItemComponent } from './nationality-item/nationality-item.component';
import { NationalityItemCustomComponent } from './nationality-item/nationality-item-custom.component';
import { NationalityListCustomComponent } from './nationality-list/nationality-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [NationalityListComponent, NationalityItemComponent],
  providers: [NationalityService, NationalityItemCustomComponent, NationalityListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [NationalityListComponent, NationalityItemComponent]
})
export class NationalityComponentModule {}
