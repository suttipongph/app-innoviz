import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { CompanySignatureListView } from 'shared/models/viewModel';
import { CompanySignatureService } from '../company-signature.service';
import { CompanySignatureListCustomComponent } from './company-signature-list-custom.component';

@Component({
  selector: 'company-signature-list',
  templateUrl: './company-signature-list.component.html',
  styleUrls: ['./company-signature-list.component.scss']
})
export class CompanySignatureListComponent extends BaseListComponent<CompanySignatureListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  companySignatureRefTypeOptions: SelectItems[] = [];
  branchOptions: SelectItems[] = [];
  constructor(private service: CompanySignatureService, public custom: CompanySignatureListCustomComponent) {
    super();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getCompanySignatureRefTypeEnumDropDown(this.companySignatureRefTypeOptions);
    this.baseDropdown.getBranchToDropDown(this.branchOptions);
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'companySignatureGUID';
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.BRANCH_ID',
        textKey: 'branch_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.ASC,
        masterList: this.branchOptions,
        sortingOrder: 0,
        searchingKey: 'BranchGUID',
        sortingKey: 'branch_BranchId'
      },
      {
        label: 'LABEL.REF_TYPE',
        textKey: 'refType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.ASC,
        masterList: this.companySignatureRefTypeOptions,
        sortingOrder: 1
      },
      { label: 'LABEL.ORDERING', textKey: 'ordering', type: ColumnType.INT, visibility: true, sorting: SortType.NONE },
      { label: 'LABEL.EMPLOYEE_ID', textKey: 'employeeTable_EmployeeId', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE },
      { label: 'LABEL.NAME', textKey: 'employeeTable_Name', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CompanySignatureListView>> {
    return this.service.getCompanySignatureToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCompanySignature(row));
  }
}
