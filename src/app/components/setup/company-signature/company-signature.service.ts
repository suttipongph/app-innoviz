import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { CompanySignatureItemView, CompanySignatureListView } from 'shared/models/viewModel';

@Injectable()
export class CompanySignatureService {
  serviceKey = 'companySignatureGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCompanySignatureToList(search: SearchParameter): Observable<SearchResult<CompanySignatureListView>> {
    const url = `${this.servicePath}/GetCompanySignatureList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCompanySignature(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCompanySignature`;
    return this.dataGateway.delete(url, row);
  }
  getCompanySignatureById(id: string): Observable<CompanySignatureItemView> {
    const url = `${this.servicePath}/GetCompanySignatureById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCompanySignature(vmModel: CompanySignatureItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCompanySignature`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCompanySignature(vmModel: CompanySignatureItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCompanySignature`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getMaxOrderingByRefTypeAndBranch(refType: number, branch: string): Observable<number> {
    const url = `${this.servicePath}/GetMaxOrderingByRefTypeAndBranch/${refType}/${branch}`;
    return this.dataGateway.get(url);
  }
  getInitialData() {
    const url = `${this.servicePath}/GetInitialDataBranch`;
    return this.dataGateway.get(url);
  }
}
