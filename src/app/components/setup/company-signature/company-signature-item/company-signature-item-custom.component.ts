import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { FieldAccessing } from 'shared/models/systemModel';
import { CompanySignatureItemView } from 'shared/models/viewModel';

const firstGroup = ['BRANCH_ID', 'REF_TYPE'];
export class CompanySignatureItemCustomComponent {
  getFieldAccessing(model: CompanySignatureItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CompanySignatureItemView): Observable<boolean> {
    return of(!canCreate);
  }

  getInitialData(): Observable<CompanySignatureItemView> {
    let model: CompanySignatureItemView = new CompanySignatureItemView();
    model.companyGUID = EmptyGuid;
    return of(model);
  }
}
