import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmpty, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, IvzDropdownOnFocusModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { CompanySignatureItemView } from 'shared/models/viewModel';
import { CompanySignatureService } from '../company-signature.service';
import { CompanySignatureItemCustomComponent } from './company-signature-item-custom.component';
@Component({
  selector: 'company-signature-item',
  templateUrl: './company-signature-item.component.html',
  styleUrls: ['./company-signature-item.component.scss']
})
export class CompanySignatureItemComponent extends BaseItemComponent<CompanySignatureItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  companySignatureRefTypeOptions: SelectItems[] = [];
  branchOptions: SelectItems[] = [];
  employeeTableOptions: SelectItems[] = [];
  constructor(
    private service: CompanySignatureService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CompanySignatureItemCustomComponent
  ) {
    super();
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  getInitialData(): Observable<CompanySignatureItemView> {
    return this.service.getInitialData();
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getCompanySignatureRefTypeEnumDropDown(this.companySignatureRefTypeOptions);
  }
  getById(): Observable<CompanySignatureItemView> {
    return this.service.getCompanySignatureById(this.id);
  }
  getMaxOrdering() {
    this.service.getMaxOrderingByRefTypeAndBranch(this.model.refType, this.model.branchGUID).subscribe(
      (result) => {
        this.model.ordering = result + 1;
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
      this.getMaxOrdering();
    });
  }
  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getBranchToDropDown(), this.baseDropdown.getEmployeeFilterActiveStatusDropdown(model?.employeeTableGUID)).subscribe(
      ([branch, employeeTableActive]) => {
        this.branchOptions = branch;
        this.employeeTableOptions = employeeTableActive;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {}
  setFunctionOptions(): void {}
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateCompanySignature(this.model), isColsing);
    } else {
      super.onCreate(this.service.createCompanySignature(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  getOrdering() {
    if (!isNullOrUndefOrEmpty(this.model.refType) && !isNullOrUndefOrEmpty(this.model.branchGUID)) {
      this.getMaxOrderingByRefType();
    } else {
      this.model.ordering = null;
    }
  }

  getMaxOrderingByRefType() {
    this.service.getMaxOrderingByRefTypeAndBranch(this.model.refType, this.model.branchGUID).subscribe(
      (result) => {
        this.model.ordering = result + 1;
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  onEmployeeTableDropdownFocus(e: IvzDropdownOnFocusModel): void {
    super.setDropdownOptionOnFocus(e, this.model, this.baseDropdown.getEmployeeFilterActiveStatusDropdown()).subscribe((result) => {
      this.employeeTableOptions = result;
    });
  }
}
