import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CompanySignatureService } from './company-signature.service';
import { CompanySignatureListComponent } from './company-signature-list/company-signature-list.component';
import { CompanySignatureItemComponent } from './company-signature-item/company-signature-item.component';
import { CompanySignatureItemCustomComponent } from './company-signature-item/company-signature-item-custom.component';
import { CompanySignatureListCustomComponent } from './company-signature-list/company-signature-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CompanySignatureListComponent, CompanySignatureItemComponent],
  providers: [CompanySignatureService, CompanySignatureItemCustomComponent, CompanySignatureListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CompanySignatureListComponent, CompanySignatureItemComponent]
})
export class CompanySignatureComponentModule {}
