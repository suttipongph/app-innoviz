import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreditScoringService } from './credit-scoring.service';
import { CreditScoringListComponent } from './credit-scoring-list/credit-scoring-list.component';
import { CreditScoringItemComponent } from './credit-scoring-item/credit-scoring-item.component';
import { CreditScoringItemCustomComponent } from './credit-scoring-item/credit-scoring-item-custom.component';
import { CreditScoringListCustomComponent } from './credit-scoring-list/credit-scoring-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CreditScoringListComponent, CreditScoringItemComponent],
  providers: [CreditScoringService, CreditScoringItemCustomComponent, CreditScoringListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CreditScoringListComponent, CreditScoringItemComponent]
})
export class CreditScoringComponentModule {}
