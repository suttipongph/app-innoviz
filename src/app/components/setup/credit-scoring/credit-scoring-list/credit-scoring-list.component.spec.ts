import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditScoringListComponent } from './credit-scoring-list.component';

describe('CreditScoringListViewComponent', () => {
  let component: CreditScoringListComponent;
  let fixture: ComponentFixture<CreditScoringListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditScoringListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditScoringListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
