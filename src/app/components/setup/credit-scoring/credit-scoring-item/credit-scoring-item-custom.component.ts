import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CreditScoringItemView } from 'shared/models/viewModel';

const firstGroup = ['CREDIT_SCORING_ID'];

export class CreditScoringItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CreditScoringItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.creditScoringGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CreditScoringItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<CreditScoringItemView> {
    let model: CreditScoringItemView = new CreditScoringItemView();
    model.creditScoringGUID = EmptyGuid;
    return of(model);
  }
}
