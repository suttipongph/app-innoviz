import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditScoringItemComponent } from './credit-scoring-item.component';

describe('CreditScoringItemViewComponent', () => {
  let component: CreditScoringItemComponent;
  let fixture: ComponentFixture<CreditScoringItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditScoringItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditScoringItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
