import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CreditScoringItemView, CreditScoringListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CreditScoringService {
  serviceKey = 'creditScoringGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCreditScoringToList(search: SearchParameter): Observable<SearchResult<CreditScoringListView>> {
    const url = `${this.servicePath}/GetCreditScoringList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCreditScoring(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCreditScoring`;
    return this.dataGateway.delete(url, row);
  }
  getCreditScoringById(id: string): Observable<CreditScoringItemView> {
    const url = `${this.servicePath}/GetCreditScoringById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCreditScoring(vmModel: CreditScoringItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCreditScoring`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCreditScoring(vmModel: CreditScoringItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCreditScoring`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
