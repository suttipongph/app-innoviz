import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BookmarkDocumentTemplateLineItemView, BookmarkDocumentTemplateLineListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class BookmarkDocumentTemplateLineService {
  serviceKey = 'bookmarkDocumentTemplateLineGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBookmarkDocumentTemplateLineToList(search: SearchParameter): Observable<SearchResult<BookmarkDocumentTemplateLineListView>> {
    const url = `${this.servicePath}/GetBookmarkDocumentTemplateLineList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBookmarkDocumentTemplateLine(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBookmarkDocumentTemplateLine`;
    return this.dataGateway.delete(url, row);
  }
  getBookmarkDocumentTemplateLineById(id: string): Observable<BookmarkDocumentTemplateLineItemView> {
    const url = `${this.servicePath}/GetBookmarkDocumentTemplateLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBookmarkDocumentTemplateLine(vmModel: BookmarkDocumentTemplateLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBookmarkDocumentTemplateLine`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBookmarkDocumentTemplateLine(vmModel: BookmarkDocumentTemplateLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBookmarkDocumentTemplateLine`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(bookmarkDocumentTemplateTableGUID: string): Observable<BookmarkDocumentTemplateLineItemView> {
    const url = `${this.servicePath}/GetInitialData/id=${bookmarkDocumentTemplateTableGUID}`;
    return this.dataGateway.get(url);
  }
}
