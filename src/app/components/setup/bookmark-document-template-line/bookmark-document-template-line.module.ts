import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BookmarkDocumentTemplateLineService } from './bookmark-document-template-line.service';
import { BookmarkDocumentTemplateLineListComponent } from './bookmark-document-template-line-list/bookmark-document-template-line-list.component';
import { BookmarkDocumentTemplateLineItemComponent } from './bookmark-document-template-line-item/bookmark-document-template-line-item.component';
import { BookmarkDocumentTemplateLineItemCustomComponent } from './bookmark-document-template-line-item/bookmark-document-template-line-item-custom.component';
import { BookmarkDocumentTemplateLineListCustomComponent } from './bookmark-document-template-line-list/bookmark-document-template-line-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BookmarkDocumentTemplateLineListComponent, BookmarkDocumentTemplateLineItemComponent],
  providers: [BookmarkDocumentTemplateLineService, BookmarkDocumentTemplateLineItemCustomComponent, BookmarkDocumentTemplateLineListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BookmarkDocumentTemplateLineListComponent, BookmarkDocumentTemplateLineItemComponent]
})
export class BookmarkDocumentTemplateLineComponentModule {}
