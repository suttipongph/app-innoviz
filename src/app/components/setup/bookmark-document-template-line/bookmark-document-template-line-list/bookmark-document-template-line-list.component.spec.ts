import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BookmarkDocumentTemplateLineListComponent } from './bookmark-document-template-line-list.component';

describe('BookmarkDocumentTemplateLineListViewComponent', () => {
  let component: BookmarkDocumentTemplateLineListComponent;
  let fixture: ComponentFixture<BookmarkDocumentTemplateLineListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BookmarkDocumentTemplateLineListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkDocumentTemplateLineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
