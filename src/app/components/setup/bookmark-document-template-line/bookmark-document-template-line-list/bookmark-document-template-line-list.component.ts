import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { BookmarkDocumentTemplateLineListView } from 'shared/models/viewModel';
import { BookmarkDocumentTemplateLineService } from '../bookmark-document-template-line.service';
import { BookmarkDocumentTemplateLineListCustomComponent } from './bookmark-document-template-line-list-custom.component';

@Component({
  selector: 'bookmark-document-template-line-list',
  templateUrl: './bookmark-document-template-line-list.component.html',
  styleUrls: ['./bookmark-document-template-line-list.component.scss']
})
export class BookmarkDocumentTemplateLineListComponent extends BaseListComponent<BookmarkDocumentTemplateLineListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  bookmarkDocumentOptions: SelectItems[] = [];
  bookmarkDocumentTemplateTableOptions: SelectItems[] = [];
  documentTemplateTableOptions: SelectItems[] = [];
  documentTemplateTypeOptions: SelectItems[] = [];

  constructor(public custom: BookmarkDocumentTemplateLineListCustomComponent, private service: BookmarkDocumentTemplateLineService) {
    super();
    this.custom.baseService = this.baseService;
    this.accessRightChildPage = 'BookmarkDocumentTemplateLineListPage';
    this.parentId = this.uiService.getKey('bookmarkdocumenttemplatetable');
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getDocumentTemplateTypeEnumDropDown(this.documentTemplateTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBookmarkDocumentDropDown(this.bookmarkDocumentOptions);
    this.baseDropdown.getDocumentTemplateTableDropDown(this.documentTemplateTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'bookmarkDocumentTemplateLineGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.BOOKMARK_DOCUMENT_ID',
        textKey: 'bookmarkDocument_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.ASC,
        masterList: this.bookmarkDocumentOptions,
        searchingKey: 'bookmarkDocumentGUID',
        sortingKey: 'bookmarkDocumentId'
      },
      {
        label: 'LABEL.DOCUMENT_TEMPLATE_TYPE',
        textKey: 'documentTemplateType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentTemplateTypeOptions
      },
      {
        label: 'LABEL.DOCUMENT_TEMPLATE_ID',
        textKey: 'documentTemplateTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentTemplateTableOptions,
        searchingKey: 'documentTemplateTableGUID',
        sortingKey: 'templateId'
      },
      {
        label: null,
        textKey: 'bookmarkDocumentTemplateTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<BookmarkDocumentTemplateLineListView>> {
    return this.service.getBookmarkDocumentTemplateLineToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteBookmarkDocumentTemplateLine(row));
  }
}
