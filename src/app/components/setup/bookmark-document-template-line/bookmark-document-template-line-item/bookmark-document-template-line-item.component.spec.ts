import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookmarkDocumentTemplateLineItemComponent } from './bookmark-document-template-line-item.component';

describe('BookmarkDocumentTemplateLineItemViewComponent', () => {
  let component: BookmarkDocumentTemplateLineItemComponent;
  let fixture: ComponentFixture<BookmarkDocumentTemplateLineItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BookmarkDocumentTemplateLineItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkDocumentTemplateLineItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
