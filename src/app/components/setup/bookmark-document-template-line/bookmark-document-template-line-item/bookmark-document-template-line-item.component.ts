import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmpty, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { BookmarkDocumentTemplateLineItemView } from 'shared/models/viewModel';
import { BookmarkDocumentTemplateLineService } from '../bookmark-document-template-line.service';
import { BookmarkDocumentTemplateLineItemCustomComponent } from './bookmark-document-template-line-item-custom.component';
@Component({
  selector: 'bookmark-document-template-line-item',
  templateUrl: './bookmark-document-template-line-item.component.html',
  styleUrls: ['./bookmark-document-template-line-item.component.scss']
})
export class BookmarkDocumentTemplateLineItemComponent extends BaseItemComponent<BookmarkDocumentTemplateLineItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  bookmarkDocumentOptions: SelectItems[] = [];
  bookmarkDocumentTemplateTableOptions: SelectItems[] = [];
  documentTemplateTableOptions: SelectItems[] = [];
  documentTemplateTypeOptions: SelectItems[] = [];

  constructor(
    private service: BookmarkDocumentTemplateLineService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: BookmarkDocumentTemplateLineItemCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiService.getKey('bookmarkdocumenttemplatetable');
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getDocumentTemplateTypeEnumDropDown(this.documentTemplateTypeOptions);
  }
  getById(): Observable<BookmarkDocumentTemplateLineItemView> {
    return this.service.getBookmarkDocumentTemplateLineById(this.id);
  }
  getInitialData(): Observable<BookmarkDocumentTemplateLineItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.model.documentTemplateType = null;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: BookmarkDocumentTemplateLineItemView): void {
    const documentType = isNullOrUndefOrEmpty(model) ? -1 : model.documentTemplateType;
    forkJoin(
      this.baseDropdown.getBookmarkDocumentDropDown(),
      this.baseDropdown.getBookmarkDocumentTemplateTableDropDown(),
      this.baseDropdown.getDocumentTemplateTableDropDownByDocumenttemplatetype(documentType.toString())
    ).subscribe(
      ([bookmarkDocument, bookmarkDocumentTemplateTable, documentTemplateTable]) => {
        this.bookmarkDocumentOptions = bookmarkDocument;
        this.bookmarkDocumentTemplateTableOptions = bookmarkDocumentTemplateTable;
        this.documentTemplateTableOptions = documentTemplateTable;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateBookmarkDocumentTemplateLine(this.model), isColsing);
    } else {
      super.onCreate(this.service.createBookmarkDocumentTemplateLine(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onBookmarkDocumentIDChange(): void {
    this.custom.setDocumentTemplateType(this.model, this.bookmarkDocumentOptions);
    this.custom.getDocumentTemplateTableOption(this.model).subscribe((result) => {
      this.documentTemplateTableOptions = result;
    });
  }
}
