import { AppInjector } from 'app-injector';
import { NotificationService } from 'core/services/notification.service';
import { Observable, of } from 'rxjs';
import { DocumentTemplateType, EmptyGuid } from 'shared/constants';
import { isNullOrUndefOrEmpty, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { BookmarkDocumentItemView, BookmarkDocumentTemplateLineItemView } from 'shared/models/viewModel';
import { BookmarkDocumentTemplateLineService } from '../bookmark-document-template-line.service';

const firstGroup = ['BOOKMARK_DOCUMENT_TEMPLATE_TABLE_GUID', 'BOOKMARK_DOCUMENT_TEMPLATE_LINE_GUID', 'DOCUMENT_TEMPLATE_TYPE'];

export class BookmarkDocumentTemplateLineItemCustomComponent {
  baseService: BaseServiceModel<any>;
  documentTemplateType: DocumentTemplateType = 0;
  getFieldAccessing(model: BookmarkDocumentTemplateLineItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.bookmarkDocumentTemplateLineGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }

  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BookmarkDocumentTemplateLineItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<BookmarkDocumentTemplateLineItemView> {
    let model = new BookmarkDocumentTemplateLineItemView();
    model.bookmarkDocumentTemplateLineGUID = EmptyGuid;
    return of(model);
  }
  getDocumentTemplateTableOption(model: BookmarkDocumentTemplateLineItemView): Observable<SelectItems[]> {
    if (!isNullOrUndefOrEmpty(model.documentTemplateType)) {
      return this.baseService.baseDropdown.getDocumentTemplateTableDropDownByDocumenttemplatetype(model.documentTemplateType.toString());
    } else {
      return of([]);
    }
  }
  setDocumentTemplateType(model: BookmarkDocumentTemplateLineItemView, bookmarkDocumentOptions: SelectItems[]) {
    model.documentTemplateTableGUID = null;
    if (model.bookmarkDocumentGUID == null) {
      model.documentTemplateType = null;
    } else {
      const row = bookmarkDocumentOptions.find((o) => o.value === model.bookmarkDocumentGUID).rowData as BookmarkDocumentItemView;
      model.documentTemplateType = row.documentTemplateType;
    }
  }
}
