import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditLimitTypeItemComponent } from './credit-limit-type-item.component';

describe('CreditLimitTypeItemViewComponent', () => {
  let component: CreditLimitTypeItemComponent;
  let fixture: ComponentFixture<CreditLimitTypeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditLimitTypeItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditLimitTypeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
