import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, TranslateModel } from 'shared/models/systemModel';
import { CreditLimitTypeItemView } from 'shared/models/viewModel';
import { CreditLimitExpiration } from 'shared/constants';

const firstGroup = [
  'CREDIT_LIMIT_TYPE_ID',
  'DESCRIPTION',
  'PRODUCT_TYPE',
  'CREDIT_LIMIT_CONDITION_TYPE',
  'PARENT_CREDIT_LIMIT_TYPE_GUID',
  'CREDIT_LIMIT_EXPIRATION',
  'VALIDATE_CREDIT_LIMIT_TYPE',
  'REVOLVING',
  'BUYER_MATCHING_AND_LOAN_REQUEST',
  'ALLOW_ADD_LINE',
  'VALIDATE_BUYER_AGREEMENT',
  'BOOKMARK_ORDERING'
];
const setreadonlybyCreditlimitexpiration = ['INACTIVE_DAY', 'CLOSE_DAY'];

export class CreditLimitTypeItemCustomComponent {
  baseService: BaseServiceModel<any>;
  isInactiveAndCloseDayReadOnly = false;
  getFieldAccessing(model: CreditLimitTypeItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.creditLimitTypeGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      if (model.creditLimitExpiration != CreditLimitExpiration.ByTransaction) {
        fieldAccessing.push({ filedIds: setreadonlybyCreditlimitexpiration, readonly: true });
        this.isInactiveAndCloseDayReadOnly = true;
      } else {
        fieldAccessing.push({ filedIds: setreadonlybyCreditlimitexpiration, readonly: false });
        this.isInactiveAndCloseDayReadOnly = false;
      }
    } else {
      this.isInactiveAndCloseDayReadOnly = false;
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
      fieldAccessing.push({ filedIds: setreadonlybyCreditlimitexpiration, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CreditLimitTypeItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<CreditLimitTypeItemView> {
    let model: CreditLimitTypeItemView = new CreditLimitTypeItemView();
    model.creditLimitTypeGUID = EmptyGuid;
    return of(model);
  }

  setFieldAccessingByCreditlimitexpiration(creditLimitExpiration: number): FieldAccessing[] {
    if (creditLimitExpiration != CreditLimitExpiration.ByTransaction) {
      return [{ filedIds: setreadonlybyCreditlimitexpiration, readonly: false }];
    } else {
      return [{ filedIds: setreadonlybyCreditlimitexpiration, readonly: true }];
    }
  }
  onCreditLimitTypeInActive(model: CreditLimitTypeItemView): TranslateModel {
    if (model.inactiveDay <= 0 && this.isInactiveAndCloseDayReadOnly == false) {
      return {
        code: 'ERROR.GREATER_THAN_ZERO',
        parameters: [this.baseService.translate.instant('LABEL.INACTIVE_DAY')]
      };
    } else {
      return null;
    }
  }
  onCreditLimitTypeCloseDay(model: CreditLimitTypeItemView): TranslateModel {
    if (model.closeDay <= 0 && this.isInactiveAndCloseDayReadOnly == false) {
      return {
        code: 'ERROR.GREATER_THAN_ZERO',
        parameters: [this.baseService.translate.instant('LABEL.CLOSE_DAY')]
      };
    } else {
      return null;
    }
  }
}
