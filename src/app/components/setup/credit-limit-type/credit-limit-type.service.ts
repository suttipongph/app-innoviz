import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CreditLimitTypeItemView, CreditLimitTypeListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CreditLimitTypeService {
  serviceKey = 'creditLimitTypeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCreditLimitTypeToList(search: SearchParameter): Observable<SearchResult<CreditLimitTypeListView>> {
    const url = `${this.servicePath}/GetCreditLimitTypeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCreditLimitType(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCreditLimitType`;
    return this.dataGateway.delete(url, row);
  }
  getCreditLimitTypeById(id: string): Observable<CreditLimitTypeItemView> {
    const url = `${this.servicePath}/GetCreditLimitTypeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCreditLimitType(vmModel: CreditLimitTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCreditLimitType`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCreditLimitType(vmModel: CreditLimitTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCreditLimitType`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
