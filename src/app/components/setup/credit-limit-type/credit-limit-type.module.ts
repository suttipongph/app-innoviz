import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreditLimitTypeService } from './credit-limit-type.service';
import { CreditLimitTypeListComponent } from './credit-limit-type-list/credit-limit-type-list.component';
import { CreditLimitTypeItemComponent } from './credit-limit-type-item/credit-limit-type-item.component';
import { CreditLimitTypeItemCustomComponent } from './credit-limit-type-item/credit-limit-type-item-custom.component';
import { CreditLimitTypeListCustomComponent } from './credit-limit-type-list/credit-limit-type-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CreditLimitTypeListComponent, CreditLimitTypeItemComponent],
  providers: [CreditLimitTypeService, CreditLimitTypeItemCustomComponent, CreditLimitTypeListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CreditLimitTypeListComponent, CreditLimitTypeItemComponent]
})
export class CreditLimitTypeComponentModule {}
