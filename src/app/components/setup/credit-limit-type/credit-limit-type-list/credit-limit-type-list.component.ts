import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CreditLimitTypeListView } from 'shared/models/viewModel';
import { CreditLimitTypeService } from '../credit-limit-type.service';
import { CreditLimitTypeListCustomComponent } from './credit-limit-type-list-custom.component';

@Component({
  selector: 'credit-limit-type-list',
  templateUrl: './credit-limit-type-list.component.html',
  styleUrls: ['./credit-limit-type-list.component.scss']
})
export class CreditLimitTypeListComponent extends BaseListComponent<CreditLimitTypeListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  creditLimitConditionTypeOptions: SelectItems[] = [];
  creditLimitExpirationOptions: SelectItems[] = [];
  creditLimitTypeOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  defaultBitOptions: SelectItems[] = [];

  constructor(public custom: CreditLimitTypeListCustomComponent, private service: CreditLimitTypeService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getCreditLimitConditionTypeEnumDropDown(this.creditLimitConditionTypeOptions);
    this.baseDropdown.getCreditLimitExpirationEnumDropDown(this.creditLimitExpirationOptions);
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getDefaultBitDropDown(this.defaultBitOptions);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'creditLimitTypeGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.CREDIT_LIMIT_TYPE_ID',
        textKey: 'creditLimitTypeId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: 'LABEL.REVOLVING',
        textKey: 'revolving',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defaultBitOptions
      },
      {
        label: 'LABEL.CREDIT_LIMIT_EXPIRATION',
        textKey: 'creditLimitExpiration',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditLimitExpirationOptions
      },
      {
        label: 'LABEL.CREDIT_LIMIT_EXPIRY_DAY',
        textKey: 'creditLimitExpiryDay',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.INACTIVE_DAY',
        textKey: 'inactiveDay',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CLOSE_DAY',
        textKey: 'closeDay',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BOOKMARK_ORDERING',
        textKey: 'bookmarkOrdering',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CreditLimitTypeListView>> {
    return this.service.getCreditLimitTypeToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCreditLimitType(row));
  }
}
