import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditLimitTypeListComponent } from './credit-limit-type-list.component';

describe('CreditLimitTypeListViewComponent', () => {
  let component: CreditLimitTypeListComponent;
  let fixture: ComponentFixture<CreditLimitTypeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditLimitTypeListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditLimitTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
