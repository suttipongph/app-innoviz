import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { WithholdingTaxGroupItemView, WithholdingTaxGroupListView } from 'shared/models/viewModel';

@Injectable()
export class WithholdingTaxGroupService {
  serviceKey = 'withholdingTaxGroupGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getWithholdingTaxGroupToList(search: SearchParameter): Observable<SearchResult<WithholdingTaxGroupListView>> {
    const url = `${this.servicePath}/GetWithholdingTaxGroupList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteWithholdingTaxGroup(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteWithholdingTaxGroup`;
    return this.dataGateway.delete(url, row);
  }
  getWithholdingTaxGroupById(id: string): Observable<WithholdingTaxGroupItemView> {
    const url = `${this.servicePath}/GetWithholdingTaxGroupById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createWithholdingTaxGroup(vmModel: WithholdingTaxGroupItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateWithholdingTaxGroup`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateWithholdingTaxGroup(vmModel: WithholdingTaxGroupItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateWithholdingTaxGroup`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
