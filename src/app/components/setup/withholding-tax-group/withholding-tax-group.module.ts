import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WithholdingTaxGroupService } from './withholding-tax-group.service';
import { WithholdingTaxGroupListComponent } from './withholding-tax-group-list/withholding-tax-group-list.component';
import { WithholdingTaxGroupItemComponent } from './withholding-tax-group-item/withholding-tax-group-item.component';
import { WithholdingTaxGroupItemCustomComponent } from './withholding-tax-group-item/withholding-tax-group-item-custom.component';
import { WithholdingTaxGroupListCustomComponent } from './withholding-tax-group-list/withholding-tax-group-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [WithholdingTaxGroupListComponent, WithholdingTaxGroupItemComponent],
  providers: [WithholdingTaxGroupService, WithholdingTaxGroupItemCustomComponent, WithholdingTaxGroupListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [WithholdingTaxGroupListComponent, WithholdingTaxGroupItemComponent]
})
export class WithholdingTaxGroupComponentModule {}
