import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { WithholdingTaxGroupItemView } from 'shared/models/viewModel';

const firstGroup = ['WHT_GROUP_ID'];
export class WithholdingTaxGroupItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: WithholdingTaxGroupItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.withholdingTaxGroupGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: WithholdingTaxGroupItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<WithholdingTaxGroupItemView> {
    let model: WithholdingTaxGroupItemView = new WithholdingTaxGroupItemView();
    model.withholdingTaxGroupGUID = EmptyGuid;
    return of(model);
  }
}
