import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RegistrationTypeListComponent } from './registration-type-list.component';

describe('RegistrationTypeListViewComponent', () => {
  let component: RegistrationTypeListComponent;
  let fixture: ComponentFixture<RegistrationTypeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RegistrationTypeListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
