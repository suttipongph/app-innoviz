import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { RegistrationTypeItemView } from 'shared/models/viewModel';

const firstGroup = ['REGISTRATION_TYPE_ID'];

export class RegistrationTypeItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: RegistrationTypeItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.registrationTypeGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: RegistrationTypeItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<RegistrationTypeItemView> {
    let model: RegistrationTypeItemView = new RegistrationTypeItemView();
    model.registrationTypeGUID = EmptyGuid;
    return of(model);
  }
}
