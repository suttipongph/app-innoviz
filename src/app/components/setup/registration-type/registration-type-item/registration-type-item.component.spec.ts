import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationTypeItemComponent } from './registration-type-item.component';

describe('RegistrationTypeItemViewComponent', () => {
  let component: RegistrationTypeItemComponent;
  let fixture: ComponentFixture<RegistrationTypeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RegistrationTypeItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationTypeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
