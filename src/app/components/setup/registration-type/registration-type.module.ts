import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegistrationTypeService } from './registration-type.service';
import { RegistrationTypeListComponent } from './registration-type-list/registration-type-list.component';
import { RegistrationTypeItemComponent } from './registration-type-item/registration-type-item.component';
import { RegistrationTypeItemCustomComponent } from './registration-type-item/registration-type-item-custom.component';
import { RegistrationTypeListCustomComponent } from './registration-type-list/registration-type-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [RegistrationTypeListComponent, RegistrationTypeItemComponent],
  providers: [RegistrationTypeService, RegistrationTypeItemCustomComponent, RegistrationTypeListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [RegistrationTypeListComponent, RegistrationTypeItemComponent]
})
export class RegistrationTypeComponentModule {}
