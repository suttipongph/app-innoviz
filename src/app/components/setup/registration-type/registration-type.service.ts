import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { RegistrationTypeItemView, RegistrationTypeListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class RegistrationTypeService {
  serviceKey = 'registrationTypeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getRegistrationTypeToList(search: SearchParameter): Observable<SearchResult<RegistrationTypeListView>> {
    const url = `${this.servicePath}/GetRegistrationTypeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteRegistrationType(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteRegistrationType`;
    return this.dataGateway.delete(url, row);
  }
  getRegistrationTypeById(id: string): Observable<RegistrationTypeItemView> {
    const url = `${this.servicePath}/GetRegistrationTypeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createRegistrationType(vmModel: RegistrationTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateRegistrationType`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateRegistrationType(vmModel: RegistrationTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateRegistrationType`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
