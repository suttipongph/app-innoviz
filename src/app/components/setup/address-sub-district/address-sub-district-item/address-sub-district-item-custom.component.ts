import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { AddressSubDistrictItemView } from 'shared/models/viewModel';

const firstGroup = ['SUB_DISTRICT_ID'];
export class AddressSubDistrictItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: AddressSubDistrictItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.addressSubDistrictGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: AddressSubDistrictItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<AddressSubDistrictItemView> {
    let model: AddressSubDistrictItemView = new AddressSubDistrictItemView();
    model.addressSubDistrictGUID = EmptyGuid;
    return of(model);
  }
}
