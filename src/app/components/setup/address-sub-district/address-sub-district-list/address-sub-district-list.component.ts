import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { AddressSubDistrictListView } from 'shared/models/viewModel';
import { AddressSubDistrictService } from '../address-sub-district.service';
import { AddressSubDistrictListCustomComponent } from './address-sub-district-list-custom.component';

@Component({
  selector: 'address-sub-district-list',
  templateUrl: './address-sub-district-list.component.html',
  styleUrls: ['./address-sub-district-list.component.scss']
})
export class AddressSubDistrictListComponent extends BaseListComponent<AddressSubDistrictListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  addressDistrictOptions: SelectItems[] = [];
  addressProvinceOptions: SelectItems[] = [];
  constructor(private service: AddressSubDistrictService, public custom: AddressSubDistrictListCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getAddressDistrictDropDown(this.addressDistrictOptions);
    this.baseDropdown.getAddressProvinceDropDown(this.addressProvinceOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'addressSubDistrictGUID';
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.PROVINCE_ID',
        textKey: 'addressProvince_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.addressProvinceOptions,
        sortingKey: 'addressProvince_ProvinceId',
        searchingKey: 'addressProvinceGUID'
      },
      {
        label: 'LABEL.DISTRICT_ID',
        textKey: 'addressDistrict_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.addressDistrictOptions,
        sortingKey: 'addressDistrict_DistrictId',
        searchingKey: 'addressDistrictGUID'
      },
      { label: 'LABEL.SUB_DISTRICT_ID', textKey: 'subDistrictId', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE },
      { label: 'LABEL.NAME', textKey: 'name', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<AddressSubDistrictListView>> {
    return this.service.getAddressSubDistrictToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteAddressSubDistrict(row));
  }
}
