import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { AddressSubDistrictItemView, AddressSubDistrictListView } from 'shared/models/viewModel';

@Injectable()
export class AddressSubDistrictService {
  serviceKey = 'addressSubDistrictGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getAddressSubDistrictToList(search: SearchParameter): Observable<SearchResult<AddressSubDistrictListView>> {
    const url = `${this.servicePath}/GetAddressSubDistrictList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteAddressSubDistrict(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteAddressSubDistrict`;
    return this.dataGateway.delete(url, row);
  }
  getAddressSubDistrictById(id: string): Observable<AddressSubDistrictItemView> {
    const url = `${this.servicePath}/GetAddressSubDistrictById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createAddressSubDistrict(vmModel: AddressSubDistrictItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateAddressSubDistrict`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateAddressSubDistrict(vmModel: AddressSubDistrictItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateAddressSubDistrict`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
