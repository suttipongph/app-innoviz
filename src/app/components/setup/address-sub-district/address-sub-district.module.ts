import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddressSubDistrictService } from './address-sub-district.service';
import { AddressSubDistrictListComponent } from './address-sub-district-list/address-sub-district-list.component';
import { AddressSubDistrictItemComponent } from './address-sub-district-item/address-sub-district-item.component';
import { AddressSubDistrictItemCustomComponent } from './address-sub-district-item/address-sub-district-item-custom.component';
import { AddressSubDistrictListCustomComponent } from './address-sub-district-list/address-sub-district-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [AddressSubDistrictListComponent, AddressSubDistrictItemComponent],
  providers: [AddressSubDistrictService, AddressSubDistrictItemCustomComponent, AddressSubDistrictListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [AddressSubDistrictListComponent, AddressSubDistrictItemComponent]
})
export class AddressSubDistrictComponentModule {}
