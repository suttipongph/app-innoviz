import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExchangeRateService } from './exchange-rate.service';
import { ExchangeRateListComponent } from './exchange-rate-list/exchange-rate-list.component';
import { ExchangeRateItemComponent } from './exchange-rate-item/exchange-rate-item.component';
import { ExchangeRateItemCustomComponent } from './exchange-rate-item/exchange-rate-item-custom.component';
import { ExchangeRateListCustomComponent } from './exchange-rate-list/exchange-rate-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ExchangeRateListComponent, ExchangeRateItemComponent],
  providers: [ExchangeRateService, ExchangeRateItemCustomComponent, ExchangeRateListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ExchangeRateListComponent, ExchangeRateItemComponent]
})
export class ExchangeRateComponentModule {}
