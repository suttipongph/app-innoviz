import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { ExchangeRateItemView, ExchangeRateListView } from 'shared/models/viewModel';

@Injectable()
export class ExchangeRateService {
  serviceKey = 'exchangeRateGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getExchangeRateToList(search: SearchParameter): Observable<SearchResult<ExchangeRateListView>> {
    const url = `${this.servicePath}/GetExchangeRateList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteExchangeRate(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteExchangeRate`;
    return this.dataGateway.delete(url, row);
  }
  getExchangeRateById(id: string): Observable<ExchangeRateItemView> {
    const url = `${this.servicePath}/GetExchangeRateById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createExchangeRate(vmModel: ExchangeRateItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateExchangeRate`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateExchangeRate(vmModel: ExchangeRateItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateExchangeRate`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  GetExchangeRateInitialData(currencyGUID: string): Observable<ExchangeRateItemView> {
    const url = `${this.servicePath}/GetExchangeRateInitialData/id=${currencyGUID}`;
    return this.dataGateway.get(url);
  }
}
