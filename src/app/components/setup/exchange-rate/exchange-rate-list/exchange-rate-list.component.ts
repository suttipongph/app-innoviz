import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, ROUTE_MASTER_GEN, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { ExchangeRateListView } from 'shared/models/viewModel';
import { ExchangeRateService } from '../exchange-rate.service';
import { ExchangeRateListCustomComponent } from './exchange-rate-list-custom.component';

@Component({
  selector: 'exchange-rate-list',
  templateUrl: './exchange-rate-list.component.html',
  styleUrls: ['./exchange-rate-list.component.scss']
})
export class ExchangeRateListComponent extends BaseListComponent<ExchangeRateListView> implements BaseListInterface {
  parentKey: string;
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  constructor(private service: ExchangeRateService, public custom: ExchangeRateListCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getKey('currency');
  }
  ngOnInit(): void {
    this.accessRightChildPage = 'ExchangeRateListPage';
    this.redirectPath = ROUTE_MASTER_GEN.EXCHANGERATE;
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onEnumLoader(): void {
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'exchangeRateGUID';
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.CURRENCY_ID',
        textKey: 'currency_CurrencyId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.HOME_CURRENCY_ID',
        textKey: 'homeCurrency_CurrencyId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      { label: 'LABEL.EFFECTIVE_FROM', textKey: 'effectiveFrom', type: ColumnType.DATERANGE, visibility: true, sorting: SortType.NONE },
      { label: 'LABEL.EFFECTIVE_TO', textKey: 'effectiveTo', type: ColumnType.DATERANGE, visibility: true, sorting: SortType.NONE },
      { label: 'LABEL.RATE', textKey: 'rate', type: ColumnType.DECIMAL, visibility: true, sorting: SortType.NONE, format: this.CURRENCY_16_5_POS },
      {
        label: null,
        textKey: 'currencyGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<ExchangeRateListView>> {
    return this.service.getExchangeRateToList(search);
  }

  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteExchangeRate(row));
  }
}
