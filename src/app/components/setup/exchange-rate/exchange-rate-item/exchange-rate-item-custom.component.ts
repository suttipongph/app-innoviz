import { AppInjector } from 'app-injector';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, of } from 'rxjs';
import { isCreateUrl, isNullOrUndefOrEmpty, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { ExchangeRateItemView } from 'shared/models/viewModel';
import { ExchangeRateService } from '../exchange-rate.service';

const firstGroup = ['CURRENCY_ID', 'HOME_CURRENCY_ID'];
export class ExchangeRateItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: ExchangeRateItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ExchangeRateItemView): Observable<boolean> {
    return of(true);
  }
}
