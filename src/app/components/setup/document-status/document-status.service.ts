import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { DocumentProcessItemView, DocumentStatusItemView, DocumentStatusListView } from 'shared/models/viewModel';

@Injectable()
export class DocumentStatusService {
  serviceKey = 'documentStatusGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getDocumentStatusToList(search: SearchParameter): Observable<SearchResult<DocumentStatusListView>> {
    const url = `${this.servicePath}/GetDocumentStatusList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteDocumentStatus(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteDocumentStatus`;
    return this.dataGateway.delete(url, row);
  }
  getDocumentStatusById(id: string): Observable<DocumentStatusItemView> {
    const url = `${this.servicePath}/GetDocumentStatusById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createDocumentStatus(vmModel: DocumentStatusItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateDocumentStatus`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateDocumentStatus(vmModel: DocumentStatusItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateDocumentStatus`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  GetInitialDataByProcess(id: string): Observable<DocumentStatusItemView> {
    const url = `${this.servicePath}/GetInitialDataByProcess/id=${id}`;
    return this.dataGateway.get(url);
  }
}
