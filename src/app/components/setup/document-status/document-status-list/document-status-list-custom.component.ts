import { AppInjector } from 'app-injector';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { BaseServiceModel } from 'shared/models/systemModel';
export class DocumentStatusListCustomComponent {
  baseService: BaseServiceModel<any>;
  parentKey: string;
  uiControllerService = AppInjector.get(UIControllerService);
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  getKey(): void {
    this.parentKey = this.uiControllerService.getKey('documentprocess');
  }
}
