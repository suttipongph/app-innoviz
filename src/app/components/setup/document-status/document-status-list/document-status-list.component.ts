import { Component, Input } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, DOCUMENT_STATUS, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { DocumentStatusListView } from 'shared/models/viewModel';
import { DocumentStatusService } from '../document-status.service';
import { DocumentStatusListCustomComponent } from './document-status-list-custom.component';

@Component({
  selector: 'document-status-list',
  templateUrl: './document-status-list.component.html',
  styleUrls: ['./document-status-list.component.scss']
})
export class DocumentStatusListComponent extends BaseListComponent<DocumentStatusListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  documentProcessOptions: SelectItems[] = [];
  constructor(private service: DocumentStatusService, public custom: DocumentStatusListCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.accessRightChildPage = 'DocumentStatusListPage';
  }
  ngOnInit(): void {
    this.custom.getKey();
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getDocumentProcessDropDown(this.documentProcessOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'documentStatusGUID';
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.STATUS_ID',
        textKey: DOCUMENT_STATUS.PK,
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'documentProcessGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.custom.parentKey
      }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<DocumentStatusListView>> {
    return this.service.getDocumentStatusToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteDocumentStatus(row));
  }
}
