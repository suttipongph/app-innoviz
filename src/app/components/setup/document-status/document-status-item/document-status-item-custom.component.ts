import { Observable, of } from 'rxjs';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { DocumentStatusItemView } from 'shared/models/viewModel';
const firstGroup = ['PROCESS_ID', 'STATUS_CODE', 'STATUS_ID'];
export class DocumentStatusItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: DocumentStatusItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: DocumentStatusItemView): Observable<boolean> {
    return of(!canCreate);
  }
}
