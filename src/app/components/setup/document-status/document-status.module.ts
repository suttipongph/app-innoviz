import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DocumentStatusService } from './document-status.service';
import { DocumentStatusListComponent } from './document-status-list/document-status-list.component';
import { DocumentStatusItemComponent } from './document-status-item/document-status-item.component';
import { DocumentStatusItemCustomComponent } from './document-status-item/document-status-item-custom.component';
import { DocumentStatusListCustomComponent } from './document-status-list/document-status-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [DocumentStatusListComponent, DocumentStatusItemComponent],
  providers: [DocumentStatusService, DocumentStatusItemCustomComponent, DocumentStatusListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [DocumentStatusListComponent, DocumentStatusItemComponent]
})
export class DocumentStatusComponentModule {}
