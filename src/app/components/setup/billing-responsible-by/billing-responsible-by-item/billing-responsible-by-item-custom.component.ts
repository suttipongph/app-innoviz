import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { BillingResponsibleByItemView } from 'shared/models/viewModel';

const firstGroup = ['BILLING_RESPONSIBLE_BY_ID'];

export class BillingResponsibleByItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: BillingResponsibleByItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.billingResponsibleByGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BillingResponsibleByItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<BillingResponsibleByItemView> {
    let model: BillingResponsibleByItemView = new BillingResponsibleByItemView();
    model.billingResponsibleByGUID = EmptyGuid;
    return of(model);
  }
}
