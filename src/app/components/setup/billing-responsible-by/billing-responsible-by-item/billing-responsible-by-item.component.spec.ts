import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingResponsibleByItemComponent } from './billing-responsible-by-item.component';

describe('BillingResponsibleByItemViewComponent', () => {
  let component: BillingResponsibleByItemComponent;
  let fixture: ComponentFixture<BillingResponsibleByItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BillingResponsibleByItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingResponsibleByItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
