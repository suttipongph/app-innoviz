import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BillingResponsibleByService } from './billing-responsible-by.service';
import { BillingResponsibleByListComponent } from './billing-responsible-by-list/billing-responsible-by-list.component';
import { BillingResponsibleByItemComponent } from './billing-responsible-by-item/billing-responsible-by-item.component';
import { BillingResponsibleByItemCustomComponent } from './billing-responsible-by-item/billing-responsible-by-item-custom.component';
import { BillingResponsibleByListCustomComponent } from './billing-responsible-by-list/billing-responsible-by-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BillingResponsibleByListComponent, BillingResponsibleByItemComponent],
  providers: [BillingResponsibleByService, BillingResponsibleByItemCustomComponent, BillingResponsibleByListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BillingResponsibleByListComponent, BillingResponsibleByItemComponent]
})
export class BillingResponsibleByComponentModule {}
