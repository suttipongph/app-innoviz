import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { BillingResponsibleByListView } from 'shared/models/viewModel';
import { BillingResponsibleByService } from '../billing-responsible-by.service';
import { BillingResponsibleByListCustomComponent } from './billing-responsible-by-list-custom.component';

@Component({
  selector: 'billing-responsible-by-list',
  templateUrl: './billing-responsible-by-list.component.html',
  styleUrls: ['./billing-responsible-by-list.component.scss']
})
export class BillingResponsibleByListComponent extends BaseListComponent<BillingResponsibleByListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  billingByOptions: SelectItems[] = [];

  constructor(public custom: BillingResponsibleByListCustomComponent, private service: BillingResponsibleByService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getBillingByEnumDropDown(this.billingByOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBillingByEnumDropDown(this.billingByOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'billingResponsibleByGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.BILLING_RESPONSIBLE_BY_ID',
        textKey: 'billingResponsibleById',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BILLING_BY',
        textKey: 'billingBy',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.billingByOptions
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<BillingResponsibleByListView>> {
    return this.service.getBillingResponsibleByToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteBillingResponsibleBy(row));
  }
}
