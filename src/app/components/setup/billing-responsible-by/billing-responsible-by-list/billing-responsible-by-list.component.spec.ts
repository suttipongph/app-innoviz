import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BillingResponsibleByListComponent } from './billing-responsible-by-list.component';

describe('BillingResponsibleByListViewComponent', () => {
  let component: BillingResponsibleByListComponent;
  let fixture: ComponentFixture<BillingResponsibleByListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BillingResponsibleByListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingResponsibleByListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
