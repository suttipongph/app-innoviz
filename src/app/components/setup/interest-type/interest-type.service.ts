import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { InterestTypeItemView, InterestTypeListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class InterestTypeService {
  serviceKey = 'interestTypeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getInterestTypeToList(search: SearchParameter): Observable<SearchResult<InterestTypeListView>> {
    const url = `${this.servicePath}/GetInterestTypeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteInterestType(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteInterestType`;
    return this.dataGateway.delete(url, row);
  }
  getInterestTypeById(id: string): Observable<InterestTypeItemView> {
    const url = `${this.servicePath}/GetInterestTypeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createInterestType(vmModel: InterestTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateInterestType`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateInterestType(vmModel: InterestTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateInterestType`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
