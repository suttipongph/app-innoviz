import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterestTypeItemComponent } from './interest-type-item.component';

describe('InterestTypeItemViewComponent', () => {
  let component: InterestTypeItemComponent;
  let fixture: ComponentFixture<InterestTypeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InterestTypeItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestTypeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
