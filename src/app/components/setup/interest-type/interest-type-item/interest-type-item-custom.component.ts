import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { InterestTypeItemView } from 'shared/models/viewModel';

const firstGroup = ['INTEREST_TYPE_ID'];

export class InterestTypeItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: InterestTypeItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.interestTypeGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: InterestTypeItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<InterestTypeItemView> {
    let model: InterestTypeItemView = new InterestTypeItemView();
    model.interestTypeGUID = EmptyGuid;
    return of(model);
  }
}
