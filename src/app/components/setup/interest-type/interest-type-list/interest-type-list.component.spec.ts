import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InterestTypeListComponent } from './interest-type-list.component';

describe('InterestTypeListViewComponent', () => {
  let component: InterestTypeListComponent;
  let fixture: ComponentFixture<InterestTypeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InterestTypeListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
