import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InterestTypeService } from './interest-type.service';
import { InterestTypeListComponent } from './interest-type-list/interest-type-list.component';
import { InterestTypeItemComponent } from './interest-type-item/interest-type-item.component';
import { InterestTypeItemCustomComponent } from './interest-type-item/interest-type-item-custom.component';
import { InterestTypeListCustomComponent } from './interest-type-list/interest-type-list-custom.component';
import { InterestTypeValueComponentModule } from '../interest-type-value/interest-type-value.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, InterestTypeValueComponentModule],
  declarations: [InterestTypeListComponent, InterestTypeItemComponent],
  providers: [InterestTypeService, InterestTypeItemCustomComponent, InterestTypeListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [InterestTypeListComponent, InterestTypeItemComponent]
})
export class InterestTypeComponentModule {}
