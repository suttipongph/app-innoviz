import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ParentCompanyItemView, ParentCompanyListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ParentCompanyService {
  serviceKey = 'parentCompanyGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getParentCompanyToList(search: SearchParameter): Observable<SearchResult<ParentCompanyListView>> {
    const url = `${this.servicePath}/GetParentCompanyList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteParentCompany(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteParentCompany`;
    return this.dataGateway.delete(url, row);
  }
  getParentCompanyById(id: string): Observable<ParentCompanyItemView> {
    const url = `${this.servicePath}/GetParentCompanyById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createParentCompany(vmModel: ParentCompanyItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateParentCompany`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateParentCompany(vmModel: ParentCompanyItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateParentCompany`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
