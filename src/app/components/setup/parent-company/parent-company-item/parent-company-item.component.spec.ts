import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParentCompanyItemComponent } from './parent-company-item.component';

describe('ParentCompanyItemViewComponent', () => {
  let component: ParentCompanyItemComponent;
  let fixture: ComponentFixture<ParentCompanyItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ParentCompanyItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentCompanyItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
