import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { ParentCompanyItemView } from 'shared/models/viewModel';

const firstGroup = ['PARENT_COMPANY_ID'];

export class ParentCompanyItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: ParentCompanyItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.parentCompanyGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ParentCompanyItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<ParentCompanyItemView> {
    let model: ParentCompanyItemView = new ParentCompanyItemView();
    model.parentCompanyGUID = EmptyGuid;
    return of(model);
  }
}
