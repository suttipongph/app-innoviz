import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ParentCompanyListComponent } from './parent-company-list.component';

describe('ParentCompanyListViewComponent', () => {
  let component: ParentCompanyListComponent;
  let fixture: ComponentFixture<ParentCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ParentCompanyListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
