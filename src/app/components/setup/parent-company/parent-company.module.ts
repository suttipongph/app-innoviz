import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ParentCompanyService } from './parent-company.service';
import { ParentCompanyListComponent } from './parent-company-list/parent-company-list.component';
import { ParentCompanyItemComponent } from './parent-company-item/parent-company-item.component';
import { ParentCompanyItemCustomComponent } from './parent-company-item/parent-company-item-custom.component';
import { ParentCompanyListCustomComponent } from './parent-company-list/parent-company-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ParentCompanyListComponent, ParentCompanyItemComponent],
  providers: [ParentCompanyService, ParentCompanyItemCustomComponent, ParentCompanyListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ParentCompanyListComponent, ParentCompanyItemComponent]
})
export class ParentCompanyComponentModule {}
