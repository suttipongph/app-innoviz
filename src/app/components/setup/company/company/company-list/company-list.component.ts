import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { CompanyListView } from 'shared/models/viewModel';
import { CompanyService } from '../company.service';
import { CompanyListCustomComponent } from './company-list-custom.component';

@Component({
  selector: 'company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.scss']
})
export class CompanyListComponent extends BaseListComponent<CompanyListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  branchOptions: SelectItems[] = [];

  constructor(private service: CompanyService, public custom: CompanyListCustomComponent) {
    super();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBranchToDropDown(this.branchOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'companyGUID';
    const columns: ColumnModel[] = [
      { label: 'LABEL.COMPANY_ID', textKey: 'companyId', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE },
      { label: 'LABEL.NAME', textKey: 'name', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE },
      { label: 'LABEL.SECOND_NAME', textKey: 'secondName', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE },
      { label: 'LABEL.ALIAS_NAME', textKey: 'altName', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE },
      { label: 'LABEL.TAX_ID', textKey: 'taxId', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE },
      {
        label: 'LABEL.DEFAULT_BRANCH',
        textKey: 'branch_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.branchOptions,
        searchingKey: 'defaultBranchGUID',
        sortingKey: 'branch_BranchId'
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CompanyListView>> {
    return this.service.getCompanyToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCompany(row));
  }
}
