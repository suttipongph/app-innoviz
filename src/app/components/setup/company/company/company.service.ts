import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { CompanyItemView, CompanyListView } from 'shared/models/viewModel';

@Injectable()
export class CompanyService {
  serviceKey = 'companyGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCompanyToList(search: SearchParameter): Observable<SearchResult<CompanyListView>> {
    const url = `${this.servicePath}/GetCompanyList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCompany(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCompany`;
    return this.dataGateway.delete(url, row);
  }
  getCompanyById(id: string): Observable<CompanyItemView> {
    const url = `${this.servicePath}/GetCompanyById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCompany(vmModel: CompanyItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCompany`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCompany(vmModel: CompanyItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCompany`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
