import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CompanyService } from './company.service';
import { CompanyListComponent } from './company-list/company-list.component';
import { CompanyItemComponent } from './company-item/company-item.component';
import { CompanyItemCustomComponent } from './company-item/company-item-custom.component';
import { CompanyListCustomComponent } from './company-list/company-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CompanyListComponent, CompanyItemComponent],
  providers: [CompanyService, CompanyItemCustomComponent, CompanyListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CompanyListComponent, CompanyItemComponent]
})
export class CompanyComponentModule {}
