import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { FieldAccessing } from 'shared/models/systemModel';
import { CompanyItemView } from 'shared/models/viewModel';

const firstGroup = ['DEFAULT_BRANCH'];
export class CompanyItemCustomComponent {
  getFieldAccessing(model: CompanyItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CompanyItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<CompanyItemView> {
    let model: CompanyItemView = new CompanyItemView();
    model.companyGUID = EmptyGuid;
    return of(model);
  }
}
