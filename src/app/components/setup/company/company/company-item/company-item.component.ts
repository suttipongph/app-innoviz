import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { IVZFileUploadComponent } from 'shared/components/ivz-file-upload/ivz-file-upload.component';
import { EmptyGuid, ROUTE_RELATED_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmpty, isUndefinedOrZeroLength, isUpdateMode } from 'shared/functions/value.function';
import { FileInformation, FileUpload, FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { CompanyItemView } from 'shared/models/viewModel';
import { CompanyService } from '../company.service';
import { CompanyItemCustomComponent } from './company-item-custom.component';
@Component({
  selector: 'company-item',
  templateUrl: './company-item.component.html',
  styleUrls: ['./company-item.component.scss']
})
export class CompanyItemComponent extends BaseItemComponent<CompanyItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  branchOptions: SelectItems[] = [];
  fileUpload: FileUpload = { fileInfos: [] };
  @ViewChild('fileUploadCmpnt') fileComponent: IVZFileUploadComponent;
  docxType: string = '.docx';
  constructor(private service: CompanyService, private currentActivatedRoute: ActivatedRoute, public custom: CompanyItemCustomComponent) {
    super();
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {
    this.fileDirty = false;
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<CompanyItemView> {
    return this.service.getCompanyById(this.id);
  }
  getInitialData(): Observable<CompanyItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }
  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getBranchToDropDown()).subscribe(
      ([branch]) => {
        this.branchOptions = branch;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
          this.getFileUpload();
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.BANK',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.COMPANY_BANK })
      },
      {
        label: 'LABEL.BRANCH',
        command: () => this.toRelatedInfo({ path: `${ROUTE_RELATED_GEN.COMPANY_BRANCH}/${this.model.defaultBranchGUID}` })
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(): void {}
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    this.setFileUpload();
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateCompany(this.model), isColsing);
    } else {
      super.onCreate(this.service.createCompany(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onFileChange(e: FileUpload) {
    this.fileDirty = true;
  }
  getFileUpload() {
    if (!isNullOrUndefOrEmpty(this.model.companyLogo)) {
      let companyLogo = new FileInformation();
      this.fileUpload.fileInfos = [];

      companyLogo.base64 = this.model.companyLogo;
      companyLogo.isRemovable = false;
      companyLogo.isPreviewable = true;
      companyLogo.contentType = 'data:image/jpeg;base64,';
      companyLogo.fileName = 'companyLogo.jpg';
      companyLogo.fileDisplayName = 'companyLogo.jpg';
      this.fileUpload.fileInfos.push(companyLogo);
      this.fileComponent.setFileFromExternal(companyLogo);
    }
  }
  setFileUpload() {
    if (isUndefinedOrZeroLength(this.fileUpload.fileInfos) || isUndefinedOrZeroLength(this.fileUpload.fileInfos[0])) {
      return;
    }

    let file = this.fileUpload.fileInfos[0].base64.split(',');
    this.model.companyLogo = file[file.length - 1];
  }
}
