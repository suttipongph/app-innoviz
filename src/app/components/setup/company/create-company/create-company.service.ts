import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CreateCompanyParamView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CreateCompanyService {
  serviceKey = 'createCompanyGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  createNewCompany(vmModel: CreateCompanyParamView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCompany`;
    return this.dataGateway.post(url, vmModel);
  }
}
