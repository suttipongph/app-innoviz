import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateCompanyComponent } from './create-company/create-company.component';
import { CreateCompanyCustomComponent } from './create-company/create-company-custom.component';
import { CreateCompanyService } from './create-company.service';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CreateCompanyComponent],
  providers: [CreateCompanyService, CreateCompanyCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CreateCompanyComponent]
})
export class CreateCompanyComponentModule {}
