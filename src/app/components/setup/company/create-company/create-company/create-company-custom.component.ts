import { Observable, of } from 'rxjs';
import { AppConst, EmptyGuid } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CreateCompanyParamView } from 'shared/models/viewModel';

const firstGroup = [];
const stepOne = ['INPUT_COMPANY_ID', 'COMPANY_NAME', 'SECOND_NAME'];
const stepTwo = ['INPUT_BRANCH_ID', 'BRANCH_NAME'];
const stepThree = ['USERNAME', 'SYSUSERTABLE_NAME', 'EMPLOYEE_TABLE_EMPLOYEE_ID', 'BUSINESS_UNIT_BUSINESS_UNIT_ID', 'ROLE_DISPLAY'];
const stepFour = [];
export class CreateCompanyCustomComponent {
  baseService: BaseServiceModel<any>;
  roleDisplay: string;
  getFieldAccessing(model: CreateCompanyParamView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(model?: CreateCompanyParamView): Observable<CreateCompanyParamView> {
    model = isNullOrUndefined(model) ? new CreateCompanyParamView() : model;
    const userFullName = this.baseService.userDataService.getUserFullName();
    const username = this.baseService.userDataService.getUsernameFromToken();
    const userId = this.baseService.userDataService.getUserIdFromToken();

    model.companyGUID = EmptyGuid;
    model.branchGUID = EmptyGuid;
    model.userId = userId;
    model.userName = username;
    model.sysUserTable_Name = userFullName;
    model.employeeTable_EmployeeId = username;

    this.roleDisplay = AppConst.ADMINROLE;

    return of(model);
  }
  getFieldAccessingStep1(): FieldAccessing[] {
    return [
      { filedIds: stepOne, readonly: false },
      { filedIds: stepThree, readonly: true }
    ];
  }
  getFieldAccessingStep2(): FieldAccessing[] {
    return [
      { filedIds: stepTwo, readonly: false },
      { filedIds: stepThree, readonly: true }
    ];
  }
  getFieldAccessingStep3(): FieldAccessing[] {
    return [{ filedIds: stepThree, readonly: true }];
  }
  getFieldAccessingStep4(): FieldAccessing[] {
    return [
      { filedIds: stepOne, readonly: true },
      { filedIds: stepTwo, readonly: true },
      { filedIds: stepThree, readonly: true }
    ];
  }
}
