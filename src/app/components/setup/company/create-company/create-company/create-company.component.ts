import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUndefinedOrZeroLength, isUpdateMode, transformLabel } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, TranslateModel } from 'shared/models/systemModel';
import { CreateCompanyParamView } from 'shared/models/viewModel';
import { CreateCompanyService } from '../create-company.service';
import { CreateCompanyCustomComponent } from './create-company-custom.component';
import { Steps } from 'primeng/steps';
import { ValidationService } from 'shared/services/validation.service';
import { AppConst } from 'shared/constants';
@Component({
  selector: 'create-company',
  templateUrl: './create-company.component.html',
  styleUrls: ['./create-company.component.scss']
})
export class CreateCompanyComponent extends BaseItemComponent<CreateCompanyParamView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  roleDisplay: string;
  stepItems = [];
  activeIndex: number = 0;
  currentIndex: number = 0;
  selectedIndex: number = 0;
  @ViewChild('stepper') stepper: Steps;
  validateStepOneFlag: boolean = false;
  validateStepTwoFlag: boolean = false;
  constructor(
    private service: CreateCompanyService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CreateCompanyCustomComponent,
    private validationService: ValidationService
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];

    this.stepItems = transformLabel([
      { label: 'LABEL.COMPANY_INFO' },
      { label: 'LABEL.BRANCH_INFO' },
      { label: 'LABEL.USER_AND_ROLE' },
      { label: 'LABEL.CREATE_INITIAL_SETUP_DATA' }
    ]);
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<CreateCompanyParamView> {
    return of(new CreateCompanyParamView());
  }
  getInitialData(): Observable<CreateCompanyParamView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(true).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        const header: TranslateModel = { code: 'CONFIRM.CONFIRM' };
        const message: TranslateModel = { code: 'CONFIRM.00066' };
        this.notificationService.showManualConfirmDialog(header, message);
        this.notificationService.isAccept.subscribe((isConfirm) => {
          if (isConfirm) {
            this.onSubmit(true);
          }
          this.notificationService.isAccept.observers = [];
        });
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    this.service.createNewCompany(this.model).subscribe((res) => {
      this.onClose();
    });
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onClose(): void {
    console.log(this.currentActivatedRoute);
    const urlPath = this.currentActivatedRoute.parent.snapshot.url[0].path;

    if (urlPath === 'initdata') {
      this.router.navigate([`/siteselect`], { skipLocationChange: true });
    }
  }

  // === stepper ==========================
  onStepsChange(index: number): void {
    this.selectedIndex = index;
  }
  onStepperClick(event): void {
    // clicked to different step
    if (this.selectedIndex !== this.currentIndex) {
      if (this.currentIndex === 0) {
        // force stepper to stay at step0 until validated
        let selected = this.selectedIndex;
        this.forceClickToIndex(event, 0);

        // validate step 0
        let isCompanyForm = this.validateFirstStep();
        this.validateStepOneFlag = true;
        if (isCompanyForm) {
          if (selected > 1) {
            // validate step 1
            let isBranchFormValid = this.validateSecondStep();

            if (isBranchFormValid) {
              // force to selected index
              // init step selected index
              this.forceClickToIndex(event, selected);
              this.initCurrentIndex();
            } else {
              // force to step 1
              this.forceClickToIndex(event, 1);
              this.validateStepTwoFlag = true;
            }
          } else if (selected === 1) {
            this.forceClickToIndex(event, 1);
            this.initCurrentIndex();
          }
          //   // step backwards
        } else {
          // company form not valid
          this.validateStepOneFlag = true;
          this.forceClickToIndex(event, 0);
        }
      } else if (this.currentIndex === 1) {
        this.validateStepTwoFlag = true;
        if (this.selectedIndex < this.currentIndex) {
          // step backwards
          this.currentIndex = this.selectedIndex;
        } else {
          let isBranchFormValid = this.validateSecondStep();
          if (isBranchFormValid) {
            this.forceClickToIndex(event, this.selectedIndex);
            this.initCurrentIndex();
          } else {
            this.forceClickToIndex(event, 1);
          }
        }
      } else if (this.currentIndex > 1) {
        this.currentIndex = this.selectedIndex;
        this.initCurrentIndex();
      }
    } else {
      // selectedIndex === currentIndex
      this.currentIndex = this.selectedIndex;
    }
  }
  next(): void {
    if (this.currentIndex === 0) {
      let result = this.validateFirstStep();
      if (!result) {
        this.validateStepOneFlag = true;
        return;
      } else {
        this.activeIndex++;
        this.currentIndex = this.currentIndex + 1;
        this.initCurrentIndex();
      }
    } else if (this.currentIndex !== 0 && this.currentIndex < 2) {
      if (!this.validateCurrentIndex()) {
        this.validateStepTwoFlag = true;
        return;
      }

      this.activeIndex++;
      this.currentIndex = this.currentIndex < 3 ? this.currentIndex + 1 : 0;
      this.initCurrentIndex();
    } else {
      this.activeIndex++;
      this.currentIndex = this.currentIndex < 3 ? this.currentIndex + 1 : 0;
      this.initCurrentIndex();
    }
  }
  prev(): void {
    this.activeIndex--;
    this.currentIndex = this.currentIndex > 0 ? this.currentIndex - 1 : 3;
    this.initCurrentIndex();
  }

  forceClickToIndex(event, index: number): void {
    this.currentIndex = index;
    this.stepper.itemClick(event, this.stepItems[index], index);
    this.initCurrentIndex();
  }

  initCurrentIndex(): void {
    if (this.currentIndex === 0) {
      super.setBaseFieldAccessing(this.custom.getFieldAccessingStep1());
      this.validationService.getFormValidation();
    } else if (this.currentIndex === 1) {
      super.setBaseFieldAccessing(this.custom.getFieldAccessingStep2());
      this.validationService.getFormValidation();
    } else if (this.currentIndex === 2) {
      this.custom.getInitialData(this.model).subscribe((result) => {
        this.model = result;
        this.model.businessUnit_BusinessUnitId = this.model.companyId;
        this.roleDisplay = AppConst.ADMINROLE;
        super.setBaseFieldAccessing(this.custom.getFieldAccessingStep3());
      });
    } else if (this.currentIndex === 3) {
      this.custom.getInitialData(this.model).subscribe((result) => {
        this.model = result;
        this.roleDisplay = AppConst.ADMINROLE;
        super.setBaseFieldAccessing(this.custom.getFieldAccessingStep4());
      });
    }
  }

  validateCurrentIndex(): boolean {
    switch (this.currentIndex) {
      case 0:
        return this.validateFirstStep();
      case 1:
        return this.validateSecondStep();

      default:
        return false;
    }
  }
  validateFirstStep(): boolean {
    if (!isUndefinedOrZeroLength(this.model.companyId) && !isUndefinedOrZeroLength(this.model.company_Name)) {
      return true;
    }

    return false;
  }

  validateSecondStep(): boolean {
    if (!isUndefinedOrZeroLength(this.model.branchId) && !isUndefinedOrZeroLength(this.model.branch_Name)) {
      return true;
    }

    return false;
  }
}
