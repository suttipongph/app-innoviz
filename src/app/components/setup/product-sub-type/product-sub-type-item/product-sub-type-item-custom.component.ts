import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { ProductSubTypeItemView } from 'shared/models/viewModel';

const firstGroup = ['PRODUCT_SUB_TYPE_ID'];

export class ProductSubTypeItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: ProductSubTypeItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.productSubTypeGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ProductSubTypeItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<ProductSubTypeItemView> {
    let model: ProductSubTypeItemView = new ProductSubTypeItemView();
    model.productSubTypeGUID = EmptyGuid;
    return of(model);
  }
}
