import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductSubTypeItemComponent } from './product-sub-type-item.component';

describe('ProductSubTypeItemViewComponent', () => {
  let component: ProductSubTypeItemComponent;
  let fixture: ComponentFixture<ProductSubTypeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductSubTypeItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSubTypeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
