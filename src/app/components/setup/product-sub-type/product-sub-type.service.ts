import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ProductSubTypeItemView, ProductSubTypeListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ProductSubTypeService {
  serviceKey = 'productSubTypeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getProductSubTypeToList(search: SearchParameter): Observable<SearchResult<ProductSubTypeListView>> {
    const url = `${this.servicePath}/GetProductSubTypeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteProductSubType(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteProductSubType`;
    return this.dataGateway.delete(url, row);
  }
  getProductSubTypeById(id: string): Observable<ProductSubTypeItemView> {
    const url = `${this.servicePath}/GetProductSubTypeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createProductSubType(vmModel: ProductSubTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateProductSubType`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateProductSubType(vmModel: ProductSubTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateProductSubType`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
