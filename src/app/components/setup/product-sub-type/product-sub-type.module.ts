import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductSubTypeService } from './product-sub-type.service';
import { ProductSubTypeListComponent } from './product-sub-type-list/product-sub-type-list.component';
import { ProductSubTypeItemComponent } from './product-sub-type-item/product-sub-type-item.component';
import { ProductSubTypeItemCustomComponent } from './product-sub-type-item/product-sub-type-item-custom.component';
import { ProductSubTypeListCustomComponent } from './product-sub-type-list/product-sub-type-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ProductSubTypeListComponent, ProductSubTypeItemComponent],
  providers: [ProductSubTypeService, ProductSubTypeItemCustomComponent, ProductSubTypeListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ProductSubTypeListComponent, ProductSubTypeItemComponent]
})
export class ProductSubTypeComponentModule {}
