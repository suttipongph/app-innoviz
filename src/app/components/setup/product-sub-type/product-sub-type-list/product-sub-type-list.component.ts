import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { ProductSubTypeListView } from 'shared/models/viewModel';
import { ProductSubTypeService } from '../product-sub-type.service';
import { ProductSubTypeListCustomComponent } from './product-sub-type-list-custom.component';

@Component({
  selector: 'product-sub-type-list',
  templateUrl: './product-sub-type-list.component.html',
  styleUrls: ['./product-sub-type-list.component.scss']
})
export class ProductSubTypeListComponent extends BaseListComponent<ProductSubTypeListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  productTypeOptions: SelectItems[] = [];
  calcInterestMethodOptions: SelectItems[] = [];
  calcInterestDayMethodOptions: SelectItems[] = [];

  constructor(public custom: ProductSubTypeListCustomComponent, private service: ProductSubTypeService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getCalcInterestMethodEnumDropDown(this.calcInterestMethodOptions);
    this.baseDropdown.getCalcInterestDayMethodEnumDropDown(this.calcInterestDayMethodOptions);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'productSubTypeGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.PRODUCT_SUB_TYPE_ID',
        textKey: 'productSubTypeId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: 'LABEL.MAX_INTEREST_FEE_PCT',
        textKey: 'maxInterestFeePct',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.PERCENT_5_2
      },
      {
        label: 'LABEL.GUARANTOR_AGREEMENT_YEAR',
        textKey: 'guarantorAgreementYear',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CALCULATE_INTEREST_METHOD',
        textKey: 'calcInterestMethod',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.calcInterestMethodOptions
      },
      {
        label: 'LABEL.CALCULATE_INTEREST_DAY_METHOD',
        textKey: 'calcInterestDayMethod',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.calcInterestDayMethodOptions
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<ProductSubTypeListView>> {
    return this.service.getProductSubTypeToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteProductSubType(row));
  }
}
