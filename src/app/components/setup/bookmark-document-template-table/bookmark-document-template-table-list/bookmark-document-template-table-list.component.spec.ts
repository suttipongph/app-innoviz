import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BookmarkDocumentTemplateTableListComponent } from './bookmark-document-template-table-list.component';

describe('BookmarkDocumentTemplateTableListViewComponent', () => {
  let component: BookmarkDocumentTemplateTableListComponent;
  let fixture: ComponentFixture<BookmarkDocumentTemplateTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BookmarkDocumentTemplateTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkDocumentTemplateTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
