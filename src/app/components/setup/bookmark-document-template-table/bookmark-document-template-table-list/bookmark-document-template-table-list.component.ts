import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { BookmarkDocumentTemplateTableListView } from 'shared/models/viewModel';
import { BookmarkDocumentTemplateTableService } from '../bookmark-document-template-table.service';
import { BookmarkDocumentTemplateTableListCustomComponent } from './bookmark-document-template-table-list-custom.component';

@Component({
  selector: 'bookmark-document-template-table-list',
  templateUrl: './bookmark-document-template-table-list.component.html',
  styleUrls: ['./bookmark-document-template-table-list.component.scss']
})
export class BookmarkDocumentTemplateTableListComponent
  extends BaseListComponent<BookmarkDocumentTemplateTableListView>
  implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  bookmarkDocumentRefTypeOptions: SelectItems[] = [];

  constructor(public custom: BookmarkDocumentTemplateTableListCustomComponent, private service: BookmarkDocumentTemplateTableService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getBookmarkDocumentRefTypeEnumDropDown(this.bookmarkDocumentRefTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'bookmarkDocumentTemplateTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.BOOKMARK_DOCUMENT_TEMPLATE_ID',
        textKey: 'bookmarkDocumentTemplateId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BOOKMARK_DOCUMENT_REFERENCE_TYPE',
        textKey: 'bookmarkDocumentRefType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.bookmarkDocumentRefTypeOptions
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<BookmarkDocumentTemplateTableListView>> {
    return this.service.getBookmarkDocumentTemplateTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteBookmarkDocumentTemplateTable(row));
  }
}
