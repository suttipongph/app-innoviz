import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BookmarkDocumentTemplateTableService } from './bookmark-document-template-table.service';
import { BookmarkDocumentTemplateTableListComponent } from './bookmark-document-template-table-list/bookmark-document-template-table-list.component';
import { BookmarkDocumentTemplateTableItemComponent } from './bookmark-document-template-table-item/bookmark-document-template-table-item.component';
import { BookmarkDocumentTemplateTableItemCustomComponent } from './bookmark-document-template-table-item/bookmark-document-template-table-item-custom.component';
import { BookmarkDocumentTemplateTableListCustomComponent } from './bookmark-document-template-table-list/bookmark-document-template-table-list-custom.component';
import { BookmarkDocumentTemplateLineComponentModule } from '../bookmark-document-template-line/bookmark-document-template-line.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, BookmarkDocumentTemplateLineComponentModule],
  declarations: [BookmarkDocumentTemplateTableListComponent, BookmarkDocumentTemplateTableItemComponent],
  providers: [
    BookmarkDocumentTemplateTableService,
    BookmarkDocumentTemplateTableItemCustomComponent,
    BookmarkDocumentTemplateTableListCustomComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BookmarkDocumentTemplateTableListComponent, BookmarkDocumentTemplateTableItemComponent]
})
export class BookmarkDocumentTemplateTableComponentModule {}
