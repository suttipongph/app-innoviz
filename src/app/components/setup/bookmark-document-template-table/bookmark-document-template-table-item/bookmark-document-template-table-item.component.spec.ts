import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookmarkDocumentTemplateTableItemComponent } from './bookmark-document-template-table-item.component';

describe('BookmarkDocumentTemplateTableItemViewComponent', () => {
  let component: BookmarkDocumentTemplateTableItemComponent;
  let fixture: ComponentFixture<BookmarkDocumentTemplateTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BookmarkDocumentTemplateTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkDocumentTemplateTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
