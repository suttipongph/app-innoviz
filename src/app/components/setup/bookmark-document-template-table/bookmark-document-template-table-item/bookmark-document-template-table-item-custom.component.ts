import { AppInjector } from 'app-injector';
import { BookmarkDocumentTemplateLineService } from 'components/setup/bookmark-document-template-line/bookmark-document-template-line.service';
import { NotificationService } from 'core/services/notification.service';
import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { BookmarkDocumentTemplateTableItemView } from 'shared/models/viewModel';
import { BookmarkDocumentTemplateTableService } from '../bookmark-document-template-table.service';

const firstGroup = ['BOOKMARK_DOCUMENT_TEMPLATE_ID', 'BOOKMARK_DOCUMENT_TEMPLATE_TABLE_GUID'];

export class BookmarkDocumentTemplateTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  notificationService = AppInjector.get(NotificationService);
  isManunal: boolean = false;
  // getFieldAccessing(model: BookmarkDocumentTemplateTableItemView): Observable<FieldAccessing[]> {
  //   const fieldAccessing: FieldAccessing[] = [];
  //   if (isUpdateMode(model.bookmarkDocumentTemplateTableGUID)) {
  //     fieldAccessing.push({ filedIds: firstGroup, readonly: true });
  //   } else {
  //     fieldAccessing.push({ filedIds: firstGroup, readonly: false });
  //   }
  //   return of(fieldAccessing);
  // }
  getFieldAccessing(
    model: BookmarkDocumentTemplateTableItemView,
    bookmarkDocumentTemplateTableService: BookmarkDocumentTemplateTableService
  ): Observable<any> {
    let fieldAccessing: FieldAccessing[] = [];
    return new Observable((observer) => {
      // set firstGroup
      if (!isUpdateMode(model.bookmarkDocumentTemplateTableGUID)) {
        this.validateIsManualNumberSeq(bookmarkDocumentTemplateTableService).subscribe(
          (result) => {
            this.isManunal = result;
            fieldAccessing.push({ filedIds: firstGroup, readonly: !this.isManunal });
            observer.next(fieldAccessing);
          },
          (error) => {
            this.notificationService.showErrorMessageFromResponse(error);
          }
        );
      } else {
        fieldAccessing.push({ filedIds: firstGroup, readonly: true });
        observer.next(fieldAccessing);
      }
    });
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BookmarkDocumentTemplateTableItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<BookmarkDocumentTemplateTableItemView> {
    let model = new BookmarkDocumentTemplateTableItemView();
    model.bookmarkDocumentTemplateTableGUID = EmptyGuid;
    return of(model);
  }
  validateIsManualNumberSeq(bookmarkDocumentTemplateTableService: BookmarkDocumentTemplateTableService): Observable<boolean> {
    return bookmarkDocumentTemplateTableService.validateIsManualNumberSeq();
  }
}
