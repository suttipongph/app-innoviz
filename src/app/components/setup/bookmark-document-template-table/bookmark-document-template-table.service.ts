import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BookmarkDocumentTemplateTableItemView, BookmarkDocumentTemplateTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class BookmarkDocumentTemplateTableService {
  serviceKey = 'bookmarkDocumentTemplateTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBookmarkDocumentTemplateTableToList(search: SearchParameter): Observable<SearchResult<BookmarkDocumentTemplateTableListView>> {
    const url = `${this.servicePath}/GetBookmarkDocumentTemplateTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBookmarkDocumentTemplateTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBookmarkDocumentTemplateTable`;
    return this.dataGateway.delete(url, row);
  }
  getBookmarkDocumentTemplateTableById(id: string): Observable<BookmarkDocumentTemplateTableItemView> {
    const url = `${this.servicePath}/GetBookmarkDocumentTemplateTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBookmarkDocumentTemplateTable(vmModel: BookmarkDocumentTemplateTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBookmarkDocumentTemplateTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBookmarkDocumentTemplateTable(vmModel: BookmarkDocumentTemplateTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBookmarkDocumentTemplateTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  validateIsManualNumberSeq(): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeq`;
    return this.dataGateway.get(url);
  }
}
