import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { LedgerFiscalYearListView } from 'shared/models/viewModel';
import { LedgerFiscalYearService } from '../ledger-fiscal-year.service';
import { LedgerFiscalYearListCustomComponent } from './ledger-fiscal-year-list-custom.component';

@Component({
  selector: 'ledger-fiscal-year-list',
  templateUrl: './ledger-fiscal-year-list.component.html',
  styleUrls: ['./ledger-fiscal-year-list.component.scss']
})
export class LedgerFiscalYearListComponent extends BaseListComponent<LedgerFiscalYearListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  ledgerFiscalPeriodUnitOptions: SelectItems[] = [];
  constructor(private service: LedgerFiscalYearService, public custom: LedgerFiscalYearListCustomComponent) {
    super();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.setDataGridOption();
    this.baseDropdown.getLedgerFiscalPeriodUnitDropDown(this.ledgerFiscalPeriodUnitOptions);
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'ledgerFiscalYearGUID';
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.FISCAL_YEAR_ID',
        textKey: 'fiscalYearId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.LENGTH_OF_PERIOD',
        textKey: 'lengthOfPeriod',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PERIOD_UNIT',
        textKey: 'periodUnit',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.ledgerFiscalPeriodUnitOptions
      },
      {
        label: 'LABEL.START_DATE',
        textKey: 'startDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.END_DATE',
        textKey: 'endDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<LedgerFiscalYearListView>> {
    return this.service.getLedgerFiscalYearToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteLedgerFiscalYear(row));
  }
}
