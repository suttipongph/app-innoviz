import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LedgerFiscalYearService } from './ledger-fiscal-year.service';
import { LedgerFiscalYearListComponent } from './ledger-fiscal-year-list/ledger-fiscal-year-list.component';
import { LedgerFiscalYearItemComponent } from './ledger-fiscal-year-item/ledger-fiscal-year-item.component';
import { LedgerFiscalYearItemCustomComponent } from './ledger-fiscal-year-item/ledger-fiscal-year-item-custom.component';
import { LedgerFiscalYearListCustomComponent } from './ledger-fiscal-year-list/ledger-fiscal-year-list-custom.component';
import { LedgerFiscalPeriodComponentModule } from '../ledger-fiscal-period/ledger-fiscal-period.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, LedgerFiscalPeriodComponentModule],
  declarations: [LedgerFiscalYearListComponent, LedgerFiscalYearItemComponent],
  providers: [LedgerFiscalYearService, LedgerFiscalYearItemCustomComponent, LedgerFiscalYearListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [LedgerFiscalYearListComponent, LedgerFiscalYearItemComponent]
})
export class LedgerFiscalYearComponentModule {}
