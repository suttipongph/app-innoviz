import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { FieldAccessing, TranslateModel } from 'shared/models/systemModel';
import { LedgerFiscalYearItemView } from 'shared/models/viewModel';

const firstGroup = ['FISCAL_YEAR_ID', 'LENGTH_OF_PERIOD', 'PERIOD_UNIT', 'START_DATE', 'END_DATE'];
export class LedgerFiscalYearItemCustomComponent {
  getFieldAccessing(model: LedgerFiscalYearItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.ledgerFiscalYearGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: LedgerFiscalYearItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<LedgerFiscalYearItemView> {
    let model: LedgerFiscalYearItemView = new LedgerFiscalYearItemView();
    model.ledgerFiscalYearGUID = EmptyGuid;
    return of(model);
  }
}
