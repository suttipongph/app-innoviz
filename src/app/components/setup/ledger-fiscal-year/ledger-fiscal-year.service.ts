import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { LedgerFiscalYearItemView, LedgerFiscalYearListView } from 'shared/models/viewModel';

@Injectable()
export class LedgerFiscalYearService {
  serviceKey = 'ledgerFiscalYearGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getLedgerFiscalYearToList(search: SearchParameter): Observable<SearchResult<LedgerFiscalYearListView>> {
    const url = `${this.servicePath}/GetLedgerFiscalYearList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteLedgerFiscalYear(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteLedgerFiscalYear`;
    return this.dataGateway.delete(url, row);
  }
  getLedgerFiscalYearById(id: string): Observable<LedgerFiscalYearItemView> {
    const url = `${this.servicePath}/GetLedgerFiscalYearById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createLedgerFiscalYear(vmModel: LedgerFiscalYearItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateLedgerFiscalYear`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateLedgerFiscalYear(vmModel: LedgerFiscalYearItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateLedgerFiscalYear`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
