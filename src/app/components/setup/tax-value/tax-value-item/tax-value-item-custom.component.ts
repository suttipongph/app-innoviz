import { NotificationService } from 'core/services/notification.service';
import { Observable, of } from 'rxjs';
import { datetoString, isDateFromGreaterThanDateTo } from 'shared/functions/date.function';
import { isNullOrUndefOrEmpty } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { TaxValueItemView } from 'shared/models/viewModel';

const firstGroup = ['TAX_CODE'];
export class TaxValueItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: TaxValueItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: TaxValueItemView): Observable<boolean> {
    return of(!canCreate);
  }
  validateEffectiveFrom(fromDate: string): any {
    if (isNullOrUndefOrEmpty(fromDate)) {
      return {
        code: 'ERROR.MANDATORY_FIELD',
        parameters: []
      };
    }
    return null;
  }
}
