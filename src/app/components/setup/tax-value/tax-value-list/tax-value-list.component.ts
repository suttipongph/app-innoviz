import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, ROUTE_MASTER_GEN, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { TaxValueListView } from 'shared/models/viewModel';
import { TaxValueService } from '../tax-value.service';
import { TaxValueListCustomComponent } from './tax-value-list-custom.component';
import { UIControllerService } from 'core/services/uiController.service';

@Component({
  selector: 'tax-value-list',
  templateUrl: './tax-value-list.component.html',
  styleUrls: ['./tax-value-list.component.scss']
})
export class TaxValueListComponent extends BaseListComponent<TaxValueListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  constructor(private service: TaxValueService, public custom: TaxValueListCustomComponent, private uiControllerService: UIControllerService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiControllerService.getKey('taxtable');
  }
  ngOnInit(): void {
    this.accessRightChildPage = 'TaxValueListPage';
    this.redirectPath = ROUTE_MASTER_GEN.TAX_VALUE;
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onEnumLoader(): void {
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'taxValueGUID';
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.EFFECTIVE_FROM',
        textKey: 'effectiveFrom',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.EFFECTIVE_TO',
        textKey: 'effectiveTo',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.VALUE',
        textKey: 'value',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.PERCENT_5_2_POS
      },
      {
        label: null,
        textKey: 'taxTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<TaxValueListView>> {
    return this.service.getTaxValueToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteTaxValue(row));
  }
}
