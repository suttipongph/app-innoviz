import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TaxValueService } from './tax-value.service';
import { TaxValueListComponent } from './tax-value-list/tax-value-list.component';
import { TaxValueItemComponent } from './tax-value-item/tax-value-item.component';
import { TaxValueItemCustomComponent } from './tax-value-item/tax-value-item-custom.component';
import { TaxValueListCustomComponent } from './tax-value-list/tax-value-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [TaxValueListComponent, TaxValueItemComponent],
  providers: [TaxValueService, TaxValueItemCustomComponent, TaxValueListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [TaxValueListComponent, TaxValueItemComponent]
})
export class TaxValueComponentModule {}
