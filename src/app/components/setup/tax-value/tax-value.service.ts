import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { TaxValueItemView, TaxValueListView } from 'shared/models/viewModel';

@Injectable()
export class TaxValueService {
  serviceKey = 'taxValueGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getTaxValueToList(search: SearchParameter): Observable<SearchResult<TaxValueListView>> {
    const url = `${this.servicePath}/GetTaxValueList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteTaxValue(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteTaxValue`;
    return this.dataGateway.delete(url, row);
  }
  getTaxValueById(id: string): Observable<TaxValueItemView> {
    const url = `${this.servicePath}/GetTaxValueById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createTaxValue(vmModel: TaxValueItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateTaxValue`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateTaxValue(vmModel: TaxValueItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateTaxValue`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<TaxValueItemView> {
    const url = `${this.servicePath}/GetTaxValueInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
}
