import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';

import { CustGroupService } from '../cust-group.service';
import { CustGroupListCustomComponent } from './cust-group-list-custom.component';
import { CustGroupListView } from 'shared/models/viewModel';

@Component({
  selector: 'cust-group-list',
  templateUrl: './cust-group-list.component.html',
  styleUrls: ['./cust-group-list.component.scss']
})
export class CustGroupListComponent extends BaseListComponent<CustGroupListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  numberSeqTableOptions: SelectItems[] = [];
  constructor(private service: CustGroupService, public custom: CustGroupListCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getNumberSeqTableDropDown(this.numberSeqTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'custGroupGUID';
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    const columns: ColumnModel[] = [
      { label: 'LABEL.CUST_GROUP_ID', textKey: 'custGroupId', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE },
      { label: 'LABEL.DESCRIPTION', textKey: 'description', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE },
      {
        label: 'LABEL.NUMBER_SEQ_CODE',
        textKey: 'numberSeqTable_Value',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.numberSeqTableOptions,
        searchingKey: 'numberSeqTableGUID',
        sortingKey: 'numberSeqCode'
      }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CustGroupListView>> {
    return this.service.getCustGroupToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCustGroup(row));
  }
}
