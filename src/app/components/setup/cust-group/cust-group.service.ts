import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { CustGroupItemView, CustGroupListView } from 'shared/models/viewModel';

@Injectable()
export class CustGroupService {
  serviceKey = 'custGroupGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCustGroupToList(search: SearchParameter): Observable<SearchResult<CustGroupListView>> {
    const url = `${this.servicePath}/GetCustGroupList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCustGroup(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCustGroup`;
    return this.dataGateway.delete(url, row);
  }
  getCustGroupById(id: string): Observable<CustGroupItemView> {
    const url = `${this.servicePath}/GetCustGroupById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCustGroup(vmModel: CustGroupItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCustGroup`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCustGroup(vmModel: CustGroupItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCustGroup`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
