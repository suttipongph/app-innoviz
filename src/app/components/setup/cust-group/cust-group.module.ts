import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustGroupService } from './cust-group.service';
import { CustGroupListComponent } from './cust-group-list/cust-group-list.component';
import { CustGroupItemComponent } from './cust-group-item/cust-group-item.component';
import { CustGroupItemCustomComponent } from './cust-group-item/cust-group-item-custom.component';
import { CustGroupListCustomComponent } from './cust-group-list/cust-group-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CustGroupListComponent, CustGroupItemComponent],
  providers: [CustGroupService, CustGroupItemCustomComponent, CustGroupListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CustGroupListComponent, CustGroupItemComponent]
})
export class CustGroupComponentModule {}
