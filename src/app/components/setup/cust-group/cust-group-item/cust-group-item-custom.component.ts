import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CustGroupItemView } from 'shared/models/viewModel';

const firstGroup = ['CUST_GROUP_ID'];
export class CustGroupItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CustGroupItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.custGroupGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CustGroupItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<CustGroupItemView> {
    let model: CustGroupItemView = new CustGroupItemView();
    model.custGroupGUID = EmptyGuid;
    return of(model);
  }
}
