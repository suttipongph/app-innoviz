import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { NumberSeqSegmentItemView, NumberSeqSegmentListView } from 'shared/models/viewModel';

@Injectable()
export class NumberSeqSegmentService {
  serviceKey = 'numberSeqSegmentGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getNumberSeqSegmentToList(search: SearchParameter): Observable<SearchResult<NumberSeqSegmentListView>> {
    const url = `${this.servicePath}/GetNumberSeqSegmentList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteNumberSeqSegment(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteNumberSeqSegment`;
    return this.dataGateway.delete(url, row);
  }
  getNumberSeqSegmentById(id: string): Observable<NumberSeqSegmentItemView> {
    const url = `${this.servicePath}/GetNumberSeqSegmentById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createNumberSeqSegment(vmModel: NumberSeqSegmentItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateNumberSeqSegment`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateNumberSeqSegment(vmModel: NumberSeqSegmentItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateNumberSeqSegment`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<NumberSeqSegmentItemView> {
    const url = `${this.servicePath}/GetNumberSeqSegmentInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
}
