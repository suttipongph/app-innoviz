import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, NumberSeqSegmentType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmpty, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { NumberSeqSegmentItemView } from 'shared/models/viewModel';
import { NumberSeqSegmentService } from '../number-seq-segment.service';
import { NumberSeqSegmentItemCustomComponent } from './number-seq-segment-item-custom.component';
@Component({
  selector: 'number-seq-segment-item',
  templateUrl: './number-seq-segment-item.component.html',
  styleUrls: ['./number-seq-segment-item.component.scss']
})
export class NumberSeqSegmentItemComponent extends BaseItemComponent<NumberSeqSegmentItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  ordering: Number = 0;
  numberSeqSegmentTypeOptions: SelectItems[] = [];
  numberSeqTableOptions: SelectItems[] = [];
  constructor(
    private service: NumberSeqSegmentService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: NumberSeqSegmentItemCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiService.getKey('numberseqtable');
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getNumberSeqSegmentTypeDropDown(this.numberSeqSegmentTypeOptions);
  }
  getById(): Observable<NumberSeqSegmentItemView> {
    return this.service.getNumberSeqSegmentById(this.id);
  }
  getInitialData(): Observable<NumberSeqSegmentItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }
  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getNumberSeqTableDropDown()).subscribe(
      ([numberSeqTable]) => {
        this.numberSeqTableOptions = numberSeqTable;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  onSeqmentTypeChange(e: any): void {
    this.custom.setModelBySeqmentTypeChange(this.model);
    super.setBaseFieldAccessing(this.custom.setFieldAccessingBySegmentType(e));
  }
  setRelatedInfoOptions(): void {}
  setFunctionOptions(): void {}
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateNumberSeqSegment(this.model), isColsing);
    } else {
      super.onCreate(this.service.createNumberSeqSegment(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
