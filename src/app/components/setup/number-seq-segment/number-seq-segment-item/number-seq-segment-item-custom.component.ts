import { Observable, of } from 'rxjs';
import { NumberSeqSegmentType } from 'shared/constants';
import { isNullOrUndefOrEmpty, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, TranslateModel } from 'shared/models/systemModel';
import { NumberSeqSegmentItemView } from 'shared/models/viewModel';

const firstGroup = ['NUMBER_SEQ_CODE', 'ORDERING'];
const secondGroup = ['SEGMENT_VALUE'];

export class NumberSeqSegmentItemCustomComponent {
  ifSegmentTypeisConstant: boolean = false;
  baseService: BaseServiceModel<any>;
  numberSeqSegmentType = NumberSeqSegmentType;
  getFieldAccessing(model: NumberSeqSegmentItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    fieldAccessing.push(...this.setFieldAccessingBySegmentType(model.segmentType));
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: NumberSeqSegmentItemView): Observable<boolean> {
    return of(!canCreate);
  }
  // field change ##############################################################
  setFieldAccessingBySegmentType(value: number): FieldAccessing[] {
    this.conditionBySegmentType(value);
    return [{ filedIds: secondGroup, readonly: !this.ifSegmentTypeisConstant }];
  }
  conditionBySegmentType(value: number): void {
    this.ifSegmentTypeisConstant = value === NumberSeqSegmentType.Constant ? true : false;
  }
  // validate field ############################################################
  validateSagmentValue(model: NumberSeqSegmentItemView): TranslateModel {
    if (this.ifSegmentTypeisConstant && isNullOrUndefOrEmpty(model.segmentValue)) {
      return {
        code: 'ERROR.MANDATORY_FIELD',
        parameters: []
      };
    } else {
      return null;
    }
  }
  setModelBySeqmentTypeChange(model: NumberSeqSegmentItemView): void {
    model.segmentValue = null;
  }
}
