import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { NumberSeqSegmentListView } from 'shared/models/viewModel';
import { NumberSeqSegmentService } from '../number-seq-segment.service';
import { NumberSeqSegmentListCustomComponent } from './number-seq-segment-list-custom.component';

@Component({
  selector: 'number-seq-segment-list',
  templateUrl: './number-seq-segment-list.component.html',
  styleUrls: ['./number-seq-segment-list.component.scss']
})
export class NumberSeqSegmentListComponent extends BaseListComponent<NumberSeqSegmentListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  numberSeqSegmentTypeOptions: SelectItems[] = [];
  constructor(private service: NumberSeqSegmentService, public custom: NumberSeqSegmentListCustomComponent) {
    super();
    this.parentId = this.uiService.getKey('numberseqtable');
  }
  ngOnInit(): void {
    this.accessRightChildPage = 'NumberSeqSegmentListPage';
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onEnumLoader(): void {
    this.baseDropdown.getNumberSeqSegmentTypeDropDown(this.numberSeqSegmentTypeOptions);
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'numberSeqSegmentGUID';
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.ORDERING',
        textKey: 'ordering',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.SEGMENT_TYPE',
        textKey: 'segmentType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.numberSeqSegmentTypeOptions
      },
      {
        label: 'LABEL.SEGMENT_VALUE',
        textKey: 'segmentValue',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'numberSeqTableGUID',
        type: ColumnType.MASTER,
        visibility: false,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<NumberSeqSegmentListView>> {
    return this.service.getNumberSeqSegmentToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteNumberSeqSegment(row));
  }
}
