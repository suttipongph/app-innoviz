import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NumberSeqSegmentService } from './number-seq-segment.service';
import { NumberSeqSegmentListComponent } from './number-seq-segment-list/number-seq-segment-list.component';
import { NumberSeqSegmentItemComponent } from './number-seq-segment-item/number-seq-segment-item.component';
import { NumberSeqSegmentItemCustomComponent } from './number-seq-segment-item/number-seq-segment-item-custom.component';
import { NumberSeqSegmentListCustomComponent } from './number-seq-segment-list/number-seq-segment-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [NumberSeqSegmentListComponent, NumberSeqSegmentItemComponent],
  providers: [NumberSeqSegmentService, NumberSeqSegmentItemCustomComponent, NumberSeqSegmentListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [NumberSeqSegmentListComponent, NumberSeqSegmentItemComponent]
})
export class NumberSeqSegmentComponentModule {}
