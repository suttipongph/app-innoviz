import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PropertyTypeService } from './property-type.service';
import { PropertyTypeListComponent } from './property-type-list/property-type-list.component';
import { PropertyTypeItemComponent } from './property-type-item/property-type-item.component';
import { PropertyTypeItemCustomComponent } from './property-type-item/property-type-item-custom.component';
import { PropertyTypeListCustomComponent } from './property-type-list/property-type-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PropertyTypeListComponent, PropertyTypeItemComponent],
  providers: [PropertyTypeService, PropertyTypeItemCustomComponent, PropertyTypeListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PropertyTypeListComponent, PropertyTypeItemComponent]
})
export class PropertyTypeComponentModule {}
