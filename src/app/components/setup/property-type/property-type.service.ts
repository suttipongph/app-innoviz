import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { PropertyTypeItemView, PropertyTypeListView } from 'shared/models/viewModel';

@Injectable()
export class PropertyTypeService {
  serviceKey = 'propertyTypeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getPropertyTypeToList(search: SearchParameter): Observable<SearchResult<PropertyTypeListView>> {
    const url = `${this.servicePath}/GetPropertyTypeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deletePropertyType(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeletePropertyType`;
    return this.dataGateway.delete(url, row);
  }
  getPropertyTypeById(id: string): Observable<PropertyTypeItemView> {
    const url = `${this.servicePath}/GetPropertyTypeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createPropertyType(vmModel: PropertyTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreatePropertyType`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updatePropertyType(vmModel: PropertyTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdatePropertyType`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
