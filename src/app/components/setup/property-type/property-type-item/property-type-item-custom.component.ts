import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { PropertyTypeItemView } from 'shared/models/viewModel';

const firstGroup = ['PROPERTY_TYPE_ID'];
export class PropertyTypeItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: PropertyTypeItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.propertyTypeGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: PropertyTypeItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<PropertyTypeItemView> {
    let model: PropertyTypeItemView = new PropertyTypeItemView();
    model.propertyTypeGUID = EmptyGuid;
    return of(model);
  }
}
