import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { WithholdingTaxTableItemView } from 'shared/models/viewModel';
import { WithholdingTaxTableService } from '../withholding-tax-table.service';
import { WithholdingTaxTableItemCustomComponent } from './withholding-tax-table-item-custom.component';
@Component({
  selector: 'withholding-tax-table-item',
  templateUrl: './withholding-tax-table-item.component.html',
  styleUrls: ['./withholding-tax-table-item.component.scss']
})
export class WithholdingTaxTableItemComponent extends BaseItemComponent<WithholdingTaxTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  constructor(
    private service: WithholdingTaxTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: WithholdingTaxTableItemCustomComponent
  ) {
    super();
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {}
  getById(): Observable<WithholdingTaxTableItemView> {
    return this.service.getWithholdingTaxTableById(this.id);
  }
  getInitialData(): Observable<WithholdingTaxTableItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }
  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {}
  setFunctionOptions(): void {}

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateWithholdingTaxTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createWithholdingTaxTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
