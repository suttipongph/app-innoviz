import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { FieldAccessing } from 'shared/models/systemModel';
import { WithholdingTaxTableItemView } from 'shared/models/viewModel';

const firstGroup = ['WHT_CODE'];
export class WithholdingTaxTableItemCustomComponent {
  getFieldAccessing(model: WithholdingTaxTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.withholdingTaxTableGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: WithholdingTaxTableItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<WithholdingTaxTableItemView> {
    let model: WithholdingTaxTableItemView = new WithholdingTaxTableItemView();
    model.withholdingTaxTableGUID = EmptyGuid;
    return of(model);
  }
}
