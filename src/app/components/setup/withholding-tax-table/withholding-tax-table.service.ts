import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { WithholdingTaxTableItemView, WithholdingTaxTableListView } from 'shared/models/viewModel';

@Injectable()
export class WithholdingTaxTableService {
  serviceKey = 'withholdingTaxTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getWithholdingTaxTableToList(search: SearchParameter): Observable<SearchResult<WithholdingTaxTableListView>> {
    const url = `${this.servicePath}/GetWithholdingTaxTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteWithholdingTaxTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteWithholdingTaxTable`;
    return this.dataGateway.delete(url, row);
  }
  getWithholdingTaxTableById(id: string): Observable<WithholdingTaxTableItemView> {
    const url = `${this.servicePath}/GetWithholdingTaxTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createWithholdingTaxTable(vmModel: WithholdingTaxTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateWithholdingTaxTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateWithholdingTaxTable(vmModel: WithholdingTaxTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateWithholdingTaxTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
