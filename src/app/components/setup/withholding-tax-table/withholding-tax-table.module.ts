import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WithholdingTaxTableService } from './withholding-tax-table.service';
import { WithholdingTaxTableListComponent } from './withholding-tax-table-list/withholding-tax-table-list.component';
import { WithholdingTaxTableItemComponent } from './withholding-tax-table-item/withholding-tax-table-item.component';
import { WithholdingTaxTableItemCustomComponent } from './withholding-tax-table-item/withholding-tax-table-item-custom.component';
import { WithholdingTaxTableListCustomComponent } from './withholding-tax-table-list/withholding-tax-table-list-custom.component';
import { WithholdingTaxValueComponentModule } from '../withholding-tax-value/withholding-tax-value.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, WithholdingTaxValueComponentModule],
  declarations: [WithholdingTaxTableListComponent, WithholdingTaxTableItemComponent],
  providers: [WithholdingTaxTableService, WithholdingTaxTableItemCustomComponent, WithholdingTaxTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [WithholdingTaxTableListComponent, WithholdingTaxTableItemComponent]
})
export class WithholdingTaxTableComponentModule {}
