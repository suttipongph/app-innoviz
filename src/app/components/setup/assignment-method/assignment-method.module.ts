import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AssignmentMethodService } from './assignment-method.service';
import { AssignmentMethodListComponent } from './assignment-method-list/assignment-method-list.component';
import { AssignmentMethodItemComponent } from './assignment-method-item/assignment-method-item.component';
import { AssignmentMethodItemCustomComponent } from './assignment-method-item/assignment-method-item-custom.component';
import { AssignmentMethodListCustomComponent } from './assignment-method-list/assignment-method-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [AssignmentMethodListComponent, AssignmentMethodItemComponent],
  providers: [AssignmentMethodService, AssignmentMethodItemCustomComponent, AssignmentMethodListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [AssignmentMethodListComponent, AssignmentMethodItemComponent]
})
export class AssignmentMethodComponentModule {}
