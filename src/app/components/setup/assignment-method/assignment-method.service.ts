import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AssignmentMethodItemView, AssignmentMethodListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class AssignmentMethodService {
  serviceKey = 'assignmentMethodGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getAssignmentMethodToList(search: SearchParameter): Observable<SearchResult<AssignmentMethodListView>> {
    const url = `${this.servicePath}/GetAssignmentMethodList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteAssignmentMethod(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteAssignmentMethod`;
    return this.dataGateway.delete(url, row);
  }
  getAssignmentMethodById(id: string): Observable<AssignmentMethodItemView> {
    const url = `${this.servicePath}/GetAssignmentMethodById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createAssignmentMethod(vmModel: AssignmentMethodItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateAssignmentMethod`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateAssignmentMethod(vmModel: AssignmentMethodItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateAssignmentMethod`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
