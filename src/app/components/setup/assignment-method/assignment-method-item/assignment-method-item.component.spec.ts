import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmentMethodItemComponent } from './assignment-method-item.component';

describe('AssignmentMethodItemViewComponent', () => {
  let component: AssignmentMethodItemComponent;
  let fixture: ComponentFixture<AssignmentMethodItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AssignmentMethodItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentMethodItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
