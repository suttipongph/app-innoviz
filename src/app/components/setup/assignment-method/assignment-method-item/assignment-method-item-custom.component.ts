import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { AssignmentMethodItemView } from 'shared/models/viewModel';

const firstGroup = ['ASSIGNMENT_METHOD_ID'];

export class AssignmentMethodItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: AssignmentMethodItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.assignmentMethodGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: AssignmentMethodItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<AssignmentMethodItemView> {
    let model: AssignmentMethodItemView = new AssignmentMethodItemView();
    model.assignmentMethodGUID = EmptyGuid;
    return of(model);
  }
}
