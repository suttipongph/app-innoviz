import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AssignmentMethodListComponent } from './assignment-method-list.component';

describe('AssignmentMethodListViewComponent', () => {
  let component: AssignmentMethodListComponent;
  let fixture: ComponentFixture<AssignmentMethodListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AssignmentMethodListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentMethodListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
