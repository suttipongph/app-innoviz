import { Observable, of } from 'rxjs';
import { isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { InvoiceRevenueTypeItemView, ServiceFeeCondTemplateLineItemView } from 'shared/models/viewModel';

const firstGroup = ['SERVICE_FEE_COND_TEMPLATE_TABLE_GUID'];

export class ServiceFeeCondTemplateLineItemCustomComponent {
  baseService: BaseServiceModel<any>;

  getFieldAccessing(model: ServiceFeeCondTemplateLineItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ServiceFeeCondTemplateLineItemView): Observable<boolean> {
    return of(!canCreate);
  }

  onServicefeetypeChange(model: ServiceFeeCondTemplateLineItemView, option: SelectItems[]): void {
    if (!isNullOrUndefOrEmptyGUID(model.invoiceRevenueTypeGUID)) {
      this.setValueByServicefeetype(model, option);
    } else {
      model.description = '';
      model.amountBeforeTax = null;
    }
  }
  setValueByServicefeetype(model: ServiceFeeCondTemplateLineItemView, option: SelectItems[]): void {
    const row = option.find((o) => o.value === model.invoiceRevenueTypeGUID).rowData as InvoiceRevenueTypeItemView;
    model.description = row.description;
    model.amountBeforeTax = row.feeAmount;
  }
  validateAmountBeforeTax(model: ServiceFeeCondTemplateLineItemView): TranslateModel {
    if (model.amountBeforeTax < 0) {
      return {
        code: 'ERROR.GREATER_THAN_EQUAL_OR_ZERO',
        parameters: [this.baseService.translate.instant('LABEL.AMOUNT_BEFORE_TAX')]
      };
    } else {
      return null;
    }
  }
}
