import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceFeeCondTemplateLineItemComponent } from './service-fee-cond-template-line-item.component';

describe('ServiceFeeCondTemplateLineItemViewComponent', () => {
  let component: ServiceFeeCondTemplateLineItemComponent;
  let fixture: ComponentFixture<ServiceFeeCondTemplateLineItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ServiceFeeCondTemplateLineItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceFeeCondTemplateLineItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
