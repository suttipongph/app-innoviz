import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServiceFeeCondTemplateLineService } from './service-fee-cond-template-line.service';
import { ServiceFeeCondTemplateLineListComponent } from './service-fee-cond-template-line-list/service-fee-cond-template-line-list.component';
import { ServiceFeeCondTemplateLineItemComponent } from './service-fee-cond-template-line-item/service-fee-cond-template-line-item.component';
import { ServiceFeeCondTemplateLineItemCustomComponent } from './service-fee-cond-template-line-item/service-fee-cond-template-line-item-custom.component';
import { ServiceFeeCondTemplateLineListCustomComponent } from './service-fee-cond-template-line-list/service-fee-cond-template-line-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ServiceFeeCondTemplateLineListComponent, ServiceFeeCondTemplateLineItemComponent],
  providers: [ServiceFeeCondTemplateLineService, ServiceFeeCondTemplateLineItemCustomComponent, ServiceFeeCondTemplateLineListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ServiceFeeCondTemplateLineListComponent, ServiceFeeCondTemplateLineItemComponent]
})
export class ServiceFeeCondTemplateLineComponentModule {}
