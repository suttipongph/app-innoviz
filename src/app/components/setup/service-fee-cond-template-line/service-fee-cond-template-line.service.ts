import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { ServiceFeeCondTemplateLineItemView, ServiceFeeCondTemplateLineListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ServiceFeeCondTemplateLineService {
  serviceKey = 'serviceFeeCondTemplateLineGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getServiceFeeCondTemplateLineToList(search: SearchParameter): Observable<SearchResult<ServiceFeeCondTemplateLineListView>> {
    const url = `${this.servicePath}/GetServiceFeeCondTemplateLineList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteServiceFeeCondTemplateLine(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteServiceFeeCondTemplateLine`;
    return this.dataGateway.delete(url, row);
  }
  getServiceFeeCondTemplateLineById(id: string): Observable<ServiceFeeCondTemplateLineItemView> {
    const url = `${this.servicePath}/GetServiceFeeCondTemplateLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createServiceFeeCondTemplateLine(vmModel: ServiceFeeCondTemplateLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateServiceFeeCondTemplateLine`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateServiceFeeCondTemplateLine(vmModel: ServiceFeeCondTemplateLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateServiceFeeCondTemplateLine`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<ServiceFeeCondTemplateLineItemView> {
    const url = `${this.servicePath}/GetServiceFeeCondTemplateLineInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getInvoiceRevenueTypeDefaultData(id: string): Observable<ServiceFeeCondTemplateLineItemView> {
    const url = `${this.servicePath}/GetInvoiceRevenueTypeInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
}
