import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ServiceFeeCondTemplateLineListComponent } from './service-fee-cond-template-line-list.component';

describe('ServiceFeeCondTemplateLineListViewComponent', () => {
  let component: ServiceFeeCondTemplateLineListComponent;
  let fixture: ComponentFixture<ServiceFeeCondTemplateLineListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ServiceFeeCondTemplateLineListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceFeeCondTemplateLineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
