import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ColumnType, ROUTE_MASTER_GEN, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { ServiceFeeCondTemplateLineListView } from 'shared/models/viewModel';
import { ServiceFeeCondTemplateLineService } from '../service-fee-cond-template-line.service';
import { ServiceFeeCondTemplateLineListCustomComponent } from './service-fee-cond-template-line-list-custom.component';

@Component({
  selector: 'service-fee-cond-template-line-list',
  templateUrl: './service-fee-cond-template-line-list.component.html',
  styleUrls: ['./service-fee-cond-template-line-list.component.scss']
})
export class ServiceFeeCondTemplateLineListComponent extends BaseListComponent<ServiceFeeCondTemplateLineListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  @Output() deleteEmit = new EventEmitter<boolean>();

  invoiceRevenueTypeOptions: SelectItems[] = [];
  serviceFeeCondTemplateTableOptions: SelectItems[] = [];

  constructor(public custom: ServiceFeeCondTemplateLineListCustomComponent, private service: ServiceFeeCondTemplateLineService) {
    super();
    this.parentId = this.uiService.getKey('servicefeecondtemplatetable');
  }
  ngOnInit(): void {
    this.accessRightChildPage = 'ServiceFeeCondTemplateLineListPage';
    this.redirectPath = ROUTE_MASTER_GEN.SERVICE_FEE_COND_TEMPLATE_LINE;
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getInvoiceRevenueTypeDropDown(this.invoiceRevenueTypeOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'serviceFeeCondTemplateLineGUID';
    const columns: ColumnModel[] = [
      {
        label: this.translate.instant('LABEL.ORDERING'),
        textKey: 'ordering',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: this.translate.instant('LABEL.SERVICE_FEE_TYPE_ID'),
        textKey: 'invoiceRevenueType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.invoiceRevenueTypeOptions
      },
      {
        label: this.translate.instant('LABEL.DESCRIPTION'),
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: this.translate.instant('LABEL.AMOUNT_BEFORE_TAX'),
        textKey: 'amountBeforeTax',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: null,
        textKey: 'serviceFeeCondTemplateTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<ServiceFeeCondTemplateLineListView>> {
    return this.service.getServiceFeeCondTemplateLineToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteServiceFeeCondTemplateLine(row).pipe(tap(() => this.deleteEmit.emit(true))));
  }
}
