import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ListPurchaseLineOutstandingItemView, ListPurchaseLineOutstandingListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ListPurchaseLineOutstandingService {
  serviceKey = 'listPurchaseLineOutstandingGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getListPurchaseLineOutstandingToList(search: SearchParameter): Observable<SearchResult<ListPurchaseLineOutstandingListView>> {
    const url = `${this.servicePath}/GetListPurchaseLineOutstandingList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteListPurchaseLineOutstanding(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteListPurchaseLineOutstanding`;
    return this.dataGateway.delete(url, row);
  }
  getListPurchaseLineOutstandingById(id: string): Observable<ListPurchaseLineOutstandingItemView> {
    const url = `${this.servicePath}/GetListPurchaseLineOutstandingById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createListPurchaseLineOutstanding(vmModel: ListPurchaseLineOutstandingItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateListPurchaseLineOutstanding`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateListPurchaseLineOutstanding(vmModel: ListPurchaseLineOutstandingItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateListPurchaseLineOutstanding`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
