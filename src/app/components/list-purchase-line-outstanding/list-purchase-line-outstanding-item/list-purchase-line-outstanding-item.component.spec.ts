import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPurchaseLineOutstandingItemComponent } from './list-purchase-line-outstanding-item.component';

describe('ListPurchaseLineOutstandingItemViewComponent', () => {
  let component: ListPurchaseLineOutstandingItemComponent;
  let fixture: ComponentFixture<ListPurchaseLineOutstandingItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListPurchaseLineOutstandingItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPurchaseLineOutstandingItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
