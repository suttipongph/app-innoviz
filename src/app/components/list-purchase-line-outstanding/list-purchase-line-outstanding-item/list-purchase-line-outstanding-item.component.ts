import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { ListPurchaseLineOutstandingItemView } from 'shared/models/viewModel';
import { ListPurchaseLineOutstandingService } from '../list-purchase-line-outstanding.service';
import { ListPurchaseLineOutstandingItemCustomComponent } from './list-purchase-line-outstanding-item-custom.component';
@Component({
  selector: 'list-purchase-line-outstanding-item',
  templateUrl: './list-purchase-line-outstanding-item.component.html',
  styleUrls: ['./list-purchase-line-outstanding-item.component.scss']
})
export class ListPurchaseLineOutstandingItemComponent extends BaseItemComponent<ListPurchaseLineOutstandingItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  buyerInvoiceTableOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  custBankOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  methodOfPaymentOptions: SelectItems[] = [];
  purchaseTableOptions: SelectItems[] = [];

  constructor(
    private service: ListPurchaseLineOutstandingService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: ListPurchaseLineOutstandingItemCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<ListPurchaseLineOutstandingItemView> {
    return this.service.getListPurchaseLineOutstandingById(this.id);
  }
  getInitialData(): Observable<ListPurchaseLineOutstandingItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(
      this.baseDropdown.getBuyerInvoiceTableDropDown(),
      this.baseDropdown.getBuyerTableDropDown(),
      this.baseDropdown.getCustBankDropDown(),
      this.baseDropdown.getCustomerTableDropDown(),
      this.baseDropdown.getMethodOfPaymentDropDown(),
      this.baseDropdown.getPurchaseTableDropDown()
    ).subscribe(
      ([buyerInvoiceTable, buyerTable, custBank, customerTable, methodOfPayment, purchaseTable]) => {
        this.buyerInvoiceTableOptions = buyerInvoiceTable;
        this.buyerTableOptions = buyerTable;
        this.custBankOptions = custBank;
        this.customerTableOptions = customerTable;
        this.methodOfPaymentOptions = methodOfPayment;
        this.purchaseTableOptions = purchaseTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateListPurchaseLineOutstanding(this.model), isColsing);
    } else {
      super.onCreate(this.service.createListPurchaseLineOutstanding(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
