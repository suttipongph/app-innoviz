import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListPurchaseLineOutstandingService } from './list-purchase-line-outstanding.service';
import { ListPurchaseLineOutstandingListComponent } from './list-purchase-line-outstanding-list/list-purchase-line-outstanding-list.component';
import { ListPurchaseLineOutstandingItemComponent } from './list-purchase-line-outstanding-item/list-purchase-line-outstanding-item.component';
import { ListPurchaseLineOutstandingItemCustomComponent } from './list-purchase-line-outstanding-item/list-purchase-line-outstanding-item-custom.component';
import { ListPurchaseLineOutstandingListCustomComponent } from './list-purchase-line-outstanding-list/list-purchase-line-outstanding-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ListPurchaseLineOutstandingListComponent, ListPurchaseLineOutstandingItemComponent],
  providers: [ListPurchaseLineOutstandingService, ListPurchaseLineOutstandingItemCustomComponent, ListPurchaseLineOutstandingListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ListPurchaseLineOutstandingListComponent, ListPurchaseLineOutstandingItemComponent]
})
export class ListPurchaseLineOutstandingComponentModule {}
