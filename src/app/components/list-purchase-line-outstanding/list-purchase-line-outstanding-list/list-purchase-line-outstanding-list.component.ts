import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { ListPurchaseLineOutstandingListView, PurchaseTableItemView } from 'shared/models/viewModel';
import { ListPurchaseLineOutstandingService } from '../list-purchase-line-outstanding.service';
import { ListPurchaseLineOutstandingListCustomComponent } from './list-purchase-line-outstanding-list-custom.component';

@Component({
  selector: 'list-purchase-line-outstanding-list',
  templateUrl: './list-purchase-line-outstanding-list.component.html',
  styleUrls: ['./list-purchase-line-outstanding-list.component.scss']
})
export class ListPurchaseLineOutstandingListComponent extends BaseListComponent<ListPurchaseLineOutstandingListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  @Input() inputPurchaseTableGUID: string = null;
  @Output() rowEmit = new EventEmitter<RowIdentity>();

  buyerInvoiceTableOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  custBankOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  methodOfPaymentOptions: SelectItems[] = [];
  purchaseTableOptions: SelectItems[] = [];

  constructor(public custom: ListPurchaseLineOutstandingListCustomComponent, private service: ListPurchaseLineOutstandingService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBuyerTableDropDown();
    this.baseDropdown.getCustomerTableDropDown();
    this.baseDropdown.getMethodOfPaymentDropDown();
    this.baseDropdown.getPurchaseTableDropDown();
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = true;
    this.option.canDelete = false;
    this.option.key = 'listPurchaseLineOutstandingGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.PURCHASE_ID',
        textKey: 'purchaseTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'purchaseTableGUID',
        sortingKey: 'purchaseId',
        masterList: this.purchaseTableOptions
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerId',
        masterList: this.customerTableOptions
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'buyerTableGUID',
        sortingKey: 'buyerId',
        masterList: this.buyerTableOptions
      },
      {
        label: 'LABEL.DUE_DATE',
        textKey: 'dueDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BILLING_DATE',
        textKey: 'billingDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.COLLECTION_DATE',
        textKey: 'collectionDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.INVOICE_AMOUNT',
        textKey: 'invoiceAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PURCHASE_AMOUNT',
        textKey: 'purchaseAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.OUTSTANDING',
        textKey: 'outstanding',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.METHOD_OF_PAYMENT_ID',
        textKey: 'methodOfPayment_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'methodOfPaymentGUID',
        sortingKey: 'methodOfPaymentId',
        masterList: this.methodOfPaymentOptions
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<ListPurchaseLineOutstandingListView>> {
    search.refTable = this.inputPurchaseTableGUID;
    return this.service.getListPurchaseLineOutstandingToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    this.rowEmit.emit(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteListPurchaseLineOutstanding(row));
  }
}
