import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ListPurchaseLineOutstandingListComponent } from './list-purchase-line-outstanding-list.component';

describe('ListPurchaseLineOutstandingListViewComponent', () => {
  let component: ListPurchaseLineOutstandingListComponent;
  let fixture: ComponentFixture<ListPurchaseLineOutstandingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListPurchaseLineOutstandingListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPurchaseLineOutstandingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
