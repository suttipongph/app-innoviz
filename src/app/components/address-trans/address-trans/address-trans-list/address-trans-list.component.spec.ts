import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AddressTransListComponent } from './address-trans-list.component';

describe('AddressTransListViewComponent', () => {
  let component: AddressTransListComponent;
  let fixture: ComponentFixture<AddressTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddressTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
