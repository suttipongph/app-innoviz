import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { AddressTransListView } from 'shared/models/viewModel';
import { AddressTransService } from '../address-trans.service';
import { AddressTransListCustomComponent } from './address-trans-list-custom.component';

@Component({
  selector: 'address-trans-list',
  templateUrl: './address-trans-list.component.html',
  styleUrls: ['./address-trans-list.component.scss']
})
export class AddressTransListComponent extends BaseListComponent<AddressTransListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
  }

  addressCountryOptions: SelectItems[] = [];
  addressDistrictOptions: SelectItems[] = [];
  addressPostalCodeOptions: SelectItems[] = [];
  addressProvinceOptions: SelectItems[] = [];
  addressSubDistrictOptions: SelectItems[] = [];
  ownershipOptions: SelectItems[] = [];
  propertyTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  defualtBitOption: SelectItems[] = [];

  constructor(public custom: AddressTransListCustomComponent, private service: AddressTransService, public uiControllerService: UIControllerService) {
    super();
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.baseDropdown.getDefaultBitDropDown(this.defualtBitOption);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'addressTransGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.NAME',
        textKey: 'name',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.PRIMARY',
        textKey: 'primary',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defualtBitOption
      },
      {
        label: 'LABEL.CURRENT_ADDRESS',
        textKey: 'currentAddress',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defualtBitOption
      },
      {
        label: 'LABEL.ADDRESS_1',
        textKey: 'address1',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<AddressTransListView>> {
    return this.service.getAddressTransToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteAddressTrans(row));
  }
}
