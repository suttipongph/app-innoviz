import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddressTransItemComponent } from './address-trans-item.component';

describe('AddressTransItemViewComponent', () => {
  let component: AddressTransItemComponent;
  let fixture: ComponentFixture<AddressTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddressTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
