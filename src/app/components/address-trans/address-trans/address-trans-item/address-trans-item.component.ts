import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { AddressTransItemView } from 'shared/models/viewModel';
import { AddressTransService } from '../address-trans.service';
import { AddressTransItemCustomComponent } from './address-trans-item-custom.component';
@Component({
  selector: 'address-trans-item',
  templateUrl: './address-trans-item.component.html',
  styleUrls: ['./address-trans-item.component.scss']
})
export class AddressTransItemComponent extends BaseItemComponent<AddressTransItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  addressCountryOptions: SelectItems[] = [];
  addressDistrictOptions: SelectItems[] = [];
  addressPostalCodeOptions: SelectItems[] = [];
  addressProvinceOptions: SelectItems[] = [];
  addressSubDistrictOptions: SelectItems[] = [];
  ownershipOptions: SelectItems[] = [];
  propertyTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];

  constructor(
    private service: AddressTransService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: AddressTransItemCustomComponent,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<AddressTransItemView> {
    return this.service.getAddressTransById(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner(result);
      super.setDefaultValueSystemFields();
    });
  }
  onAsyncRunner(model?: any): void {
    forkJoin(
      this.baseDropdown.getRelatedInfoAddressTransAddressCountryDropDown(),
      this.custom.getAddressPostalCodeOption(model),
      this.baseDropdown.getRelatedInfoAddressTransOwnershipDropDown(),
      this.baseDropdown.getRelatedInfoAddressTransPropertyTypeDropDown()
    ).subscribe(
      ([addressCountry, addressPostalCode, ownership, propertyType]) => {
        this.addressCountryOptions = addressCountry;
        this.addressPostalCodeOptions = addressPostalCode;
        this.ownershipOptions = ownership;
        this.propertyTypeOptions = propertyType;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateAddressTrans(this.model), isColsing);
    } else {
      super.onCreate(this.service.createAddressTrans(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  getInitialData(): Observable<AddressTransItemView> {
    return this.service.getInitialData(this.parentId);
  }
  onAddressCountryChange(isFirstTime = false): void {
    if (!isFirstTime) {
      this.addressDistrictOptions.length = 0;
      this.addressSubDistrictOptions.length = 0;
      this.addressPostalCodeOptions.length = 0;
      this.model.addressProvinceGUID = null;
      this.model.addressDistrictGUID = null;
      this.model.addressSubDistrictGUID = null;
      this.model.addressPostalCodeGUID = null;
    }
    this.custom.getAddressProvinceOption(this.model).subscribe((result) => {
      this.addressProvinceOptions = result;
    });
  }
  onAddressProvinceChange(isFirstTime = false): void {
    if (!isFirstTime) {
      this.addressSubDistrictOptions.length = 0;
      this.addressPostalCodeOptions.length = 0;
      this.model.addressDistrictGUID = null;
      this.model.addressSubDistrictGUID = null;
      this.model.addressPostalCodeGUID = null;
    }
    this.custom.getAddressDistrictOption(this.model).subscribe((result) => {
      this.addressDistrictOptions = result;
    });
  }
  onAddressDistrictChange(isFirstTime = false): void {
    if (!isFirstTime) {
      this.addressPostalCodeOptions.length = 0;
      this.model.addressSubDistrictGUID = null;
      this.model.addressPostalCodeGUID = null;
    }
    this.custom.getAddressSubDistrictOption(this.model).subscribe((result) => {
      this.addressSubDistrictOptions = result;
    });
  }
  onIsTaxChange(): void {
    this.custom.isTax = this.model.isTax;
    console.log(this.model.isTax);
  }
  onAddressSubDistrictChange(isFirstTime = false): void {
    if (!isFirstTime) {
      this.model.addressPostalCodeGUID = null;
    }
    this.custom.getAddressPostalCodeOption(this.model).subscribe((result) => {
      this.addressPostalCodeOptions = result;
    });
  }
}
