import { RefType } from 'shared/constants';
import { Observable, of } from 'rxjs';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { AddressTransItemView } from 'shared/models/viewModel';

const firstGroup = ['REF_TYPE', 'REF_ID', 'ADDRESS2'];

const COMPANY = 'company';

export class AddressTransItemCustomComponent {
  baseService: BaseServiceModel<any>;
  originName: string = null;
  visibleAltAddress: boolean = false;
  isTax = false;
  getFieldAccessing(model: AddressTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    this.setVisibleAltAddress();
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: AddressTransItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getAddressProvinceOption(model: AddressTransItemView): Observable<SelectItems[]> {
    if (!isNullOrUndefined(model.addressCountryGUID)) {
      return this.baseService.baseDropdown.getAddressProvinceByAddressCountryDropDown(model.addressCountryGUID);
    } else {
      return of([]);
    }
  }
  getAddressDistrictOption(model: AddressTransItemView): Observable<SelectItems[]> {
    if (!isNullOrUndefined(model.addressProvinceGUID)) {
      return this.baseService.baseDropdown.getAddressDistrictByAddressProvinceDropDown(model.addressProvinceGUID);
    } else {
      return of([]);
    }
  }
  getAddressSubDistrictOption(model: AddressTransItemView): Observable<SelectItems[]> {
    if (!isNullOrUndefined(model.addressDistrictGUID)) {
      return this.baseService.baseDropdown.getAddressSubDistrictByAddressDistrictDropDown(model.addressDistrictGUID);
    } else {
      return of([]);
    }
  }
  setVisibleAltAddress(): void {
    this.originName = this.baseService.uiService.getRelatedInfoOriginTableName();
    if (this.originName === COMPANY) {
      this.visibleAltAddress = true;
    }
  }
  getAddressPostalCodeOption(model: AddressTransItemView): Observable<SelectItems[]> {
    if (!isNullOrUndefined(model.addressSubDistrictGUID)) {
      return this.baseService.baseDropdown.getAddressPostalCodeDropDown(model.addressSubDistrictGUID);
    } else {
      return of([]);
    }
  }
}
