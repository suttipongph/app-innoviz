import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddressTransService } from './address-trans.service';
import { AddressTransListComponent } from './address-trans-list/address-trans-list.component';
import { AddressTransItemComponent } from './address-trans-item/address-trans-item.component';
import { AddressTransItemCustomComponent } from './address-trans-item/address-trans-item-custom.component';
import { AddressTransListCustomComponent } from './address-trans-list/address-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [AddressTransListComponent, AddressTransItemComponent],
  providers: [AddressTransService, AddressTransItemCustomComponent, AddressTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [AddressTransListComponent, AddressTransItemComponent]
})
export class AddressTransComponentModule {}
