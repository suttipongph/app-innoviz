import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AddressTransItemView, AddressTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class AddressTransService {
  serviceKey = 'addressTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getAddressTransToList(search: SearchParameter): Observable<SearchResult<AddressTransListView>> {
    const url = `${this.servicePath}/GetAddressTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteAddressTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteAddressTrans`;
    return this.dataGateway.delete(url, row);
  }
  getAddressTransById(id: string): Observable<AddressTransItemView> {
    const url = `${this.servicePath}/GetAddressTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createAddressTrans(vmModel: AddressTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateAddressTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateAddressTrans(vmModel: AddressTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateAddressTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<AddressTransItemView> {
    const url = `${this.servicePath}/GetAddressTransInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
}
