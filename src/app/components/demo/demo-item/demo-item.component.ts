import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { IVZFileUploadComponent } from 'shared/components/ivz-file-upload/ivz-file-upload.component';
import { AppConst, EmptyGuid, ROUTE_RELATED_GEN, WORKFLOW } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmpty, isUndefinedOrZeroLength, isUpdateMode } from 'shared/functions/value.function';
import { FileInformation, FileUpload, FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { DemoItemView } from 'shared/models/viewModel';
import { DemoService } from '../demo.service';
import { DemoItemCustomComponent } from './demo-item-custom.component';
@Component({
  selector: 'demo-item',
  templateUrl: './demo-item.component.html',
  styleUrls: ['./demo-item.component.scss']
})
export class DemoItemComponent extends BaseItemComponent<DemoItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  documentProcessOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  employeeTableOptions: SelectItems[] = [];
  fileUpload: FileUpload = { fileInfos: [] };
  @ViewChild('fileUploadCmpnt') fileComponent: IVZFileUploadComponent;
  docxType: string = '.docx';
  isUpdateMode: boolean;
  index = 1;
  constructor(private service: DemoService, private currentActivatedRoute: ActivatedRoute, public custom: DemoItemCustomComponent) {
    super();
    this.baseService.service = this.service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.fileDirty = false;
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<DemoItemView> {
    return this.service.getDemoById(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        result[WORKFLOW.PROCESSINSTANCEID] = 602;
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
        this.setWorkflowOption(result);
        this.getFileUpload();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.model.demoGUID = EmptyGuid;
    this.onAsyncRunner();
    super.setDefaultValueSystemFields();
  }
  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getDocumentProcessDropdown()).subscribe(
      ([documentProcess]) => {
        this.documentProcessOptions = documentProcess;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          this.model.demoAmount = 0;
          this.model.description = null;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.CONTACT_PERSON',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.CONTACT_PERSON }),
        visible: this.uiService.setAuthorizedBySite([AppConst.BACK]),
        tabindex: '2'
      },
      {
        label: 'LABEL.AUTHORIZED_PERSON',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.AUTHORIZED_PERSON_TRANS }),
        tabindex: '1'
      },
      {
        label: 'LABEL.CONTACT_TRANSACTIONS',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.CONTACT })
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(): void {
    this.functionItems = [
      {
        label: 'Print bookmark doc (single)',
        command: () => this.custom.printBookmarkSingle()
      },
      {
        label: 'Print bookmark doc (multiple)',
        command: () => this.custom.printBookmarkMultiple()
      },
      {
        label: 'Test report',
        command: () => this.toReport({ path: 'demoreportdemo' })
      }
    ];
    super.setFunctionOptionsMapping();
  }
  setWorkflowOption(model: any): void {
    const DEMO_WORKFLOW = 'actiondemo';
    const disableAction = false; // logic condition
    super.setK2WorkflowOption(
      model,
      { path: DEMO_WORKFLOW, parameters: { [WORKFLOW.ACTIONHISTORY_REFGUID]: '55F91BD0-82FD-445D-A481-C5D432A763FF' } },
      disableAction
    );
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    this.setFileUpload();
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateDemo(this.model), isColsing);
    } else {
      super.onCreate(this.service.createDemo(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onDocumentProcessChange(id: string): void {
    if (!isNullOrUndefined(id)) {
      this.baseDropdown.getDocumentStatusByDocumentProcessDropdown(id).subscribe((result) => {
        this.documentStatusOptions = result;
      });
    } else {
      this.documentStatusOptions = [];
    }
  }
  onDocumentStatusChange(id: string): void {}
  onEmployeeTableChange(id: string): void {}
  onFileChange(e: FileUpload) {
    this.fileDirty = true;
  }
  getFileUpload() {
    if (!isNullOrUndefOrEmpty(this.model.demoDocument)) {
      let demoDocument = new FileInformation();
      this.fileUpload.fileInfos = [];

      demoDocument.base64 = this.model.demoDocument;
      demoDocument.isRemovable = false;
      demoDocument.isPreviewable = true;

      this.fileUpload.fileInfos.push(demoDocument);
      this.fileComponent.displayFile = demoDocument;
    }
  }
  setFileUpload() {
    if (isUndefinedOrZeroLength(this.fileUpload.fileInfos) || isUndefinedOrZeroLength(this.fileUpload.fileInfos[0])) {
      return;
    }

    let file = this.fileUpload.fileInfos[0].base64.split(',');
    this.model.demoDocument = file[file.length - 1];
  }
  accountingAmountValidation() {
    if (this.model.demoAmount < 1) {
      return {
        code: 'ERROR.GREATER_THAN_EQUAL_OR_ZERO',
        parameters: [this.translate.instant('LABEL.ACCOUNTING_AMOUNT')]
      };
    }
    return null;
  }
}
