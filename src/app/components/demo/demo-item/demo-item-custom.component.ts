import { Observable, of } from 'rxjs';
import { isUndefinedOrZeroLength, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { DemoItemView } from 'shared/models/viewModel';

const firstGroup = ['BLACKLIST_STATUS_ID', 'PAID_UP_CAPITAL'];
const secondGroup = ['PAID_UP_CAPITAL', 'DATE_OF_ISSUE'];

export class DemoItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: DemoItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.demoGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }
    // fieldAccessing.push({ filedIds: secondGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: DemoItemView): Observable<boolean> {
    return of(!canCreate);
  }
  printBookmarkSingle(): void {
    this.baseService.service.printBookmarkDocSingle().subscribe(
      (result) => {
        if (!isUndefinedOrZeroLength(result)) {
          this.baseService.fileService.saveAsFile(result.base64, result.fileName);
        }
      },
      (error) => {
        this.baseService.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  printBookmarkMultiple(): void {
    this.baseService.service.printBookmarkDocMultiple().subscribe(
      (result) => {
        if (!isUndefinedOrZeroLength(result)) {
          result.forEach((file) => this.baseService.fileService.saveAsFile(file.base64, file.fileName));
        }
      },
      (error) => {
        this.baseService.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
}
