import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DemoService } from './demo.service';
import { DemoListComponent } from './demo-list/demo-list.component';
import { DemoItemComponent } from './demo-item/demo-item.component';
import { DemoItemCustomComponent } from './demo-item/demo-item-custom.component';
import { DemoListCustomComponent } from './demo-list/demo-list-custom.component';
import { DemoWorkflowComponent } from './demo-workflow/demo-workflow.component';
import { DemoWorkflowCustomComponent } from './demo-workflow/demo-workflow-custom.component';
import { DemoReportComponent } from './demo-report/demo-report.component';
import { DemoReportCustomComponent } from './demo-report/demo-report-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [DemoListComponent, DemoItemComponent, DemoWorkflowComponent, DemoReportComponent],
  providers: [DemoService, DemoItemCustomComponent, DemoListCustomComponent, DemoWorkflowCustomComponent, DemoReportCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [DemoListComponent, DemoItemComponent, DemoWorkflowComponent, DemoReportComponent]
})
export class DemoComponentModule {}
