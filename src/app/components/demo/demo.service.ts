import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { DemoItemView, DemoListView } from 'shared/models/viewModel';
import { FileInformation, PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { DemoWorkflowParamView } from 'shared/models/viewModel/demoWorkflowParamView';
@Injectable()
export class DemoService {
  serviceKey = 'demoGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getDemoToList(search: SearchParameter): Observable<SearchResult<DemoListView>> {
    const url = `${this.servicePath}/GetDemoList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteDemo(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteDemo`;
    return this.dataGateway.delete(url, row);
  }
  getDemoById(id: string): Observable<DemoItemView> {
    const url = `${this.servicePath}/GetDemoById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createDemo(vmModel: DemoItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateDemo`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateDemo(vmModel: DemoItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateDemo`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getStartWorkflowInitialData(id: string): Observable<DemoWorkflowParamView> {
    const url = `${this.servicePath}/GetStartWorkflowInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getActionWorkflowInitialData(id: string): Observable<DemoWorkflowParamView> {
    const url = `${this.servicePath}/GetActionWorkflowInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  printBookmarkDocSingle(): Observable<FileInformation> {
    const url = `${this.servicePath}/GetBookmarkDocFileSingle`;
    return this.dataGateway.get(url);
  }
  printBookmarkDocMultiple(): Observable<FileInformation[]> {
    const url = `${this.servicePath}/GetBookmarkDocFileMultiple`;
    return this.dataGateway.get(url);
  }
}
