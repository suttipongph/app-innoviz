import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable, of } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import { Guid } from 'shared/functions/value.function';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { DemoListView, DemoSearchParam } from 'shared/models/viewModel';
import { DemoService } from '../demo.service';
import { DemoListCustomComponent } from './demo-list-custom.component';

@Component({
  selector: 'demo-list',
  templateUrl: './demo-list.component.html',
  styleUrls: ['./demo-list.component.scss']
})
export class DemoListComponent extends BaseListComponent<DemoListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  advModel:DemoSearchParam = new DemoSearchParam();
  documentProcessOptions: SelectItems[] = [];
  constructor(public custom: DemoListCustomComponent, private service: DemoService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'demoGUIDOld';
    this.option.isAdvance = true;
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.BLACKLIST_STATUS_ID',
        textKey: 'demoId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BRANCH_ID',
        textKey: 'brance_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.ACCOUNTING_DATE',
        textKey: 'accoutingDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    const addvances: ColumnModel[] = [
      {
        label: null,
        textKey: 'demoId',
        type: ColumnType.VARIABLES,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'description',
        type: ColumnType.VARIABLES,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'documentProcessGUID',
        type: ColumnType.VARIABLE,
        visibility: true,
        sorting: SortType.NONE
      }
    ];

    this.option.columns = columns;
    this.option.advanceColumns = addvances;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<DemoListView>> {
    console.log('getList',search);
    
    const result: SearchResult<DemoListView> = new SearchResult<DemoListView>();
    const list: DemoListView[] = [];
    for (let i = 0; i < 10; i++) {
      let item: DemoListView = new DemoListView();
      item.demoGUID = Guid.newGuid.toString();
      item.demoId = `demo00${i}`;
      item.description = `desc00${i}`;
      item.accoutingDate = `${i}/03/2020`;
      item.branch_Values = `branch${i}`;
      list.push(item);
    }
    result.results = list;
    result.paginator = { page: 0, first: 0, rows: 10, pageCount: 0 };
    return of(result);
    //return this.service.getDemoToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteDemo(row));
  }
}
