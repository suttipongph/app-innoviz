import { Observable, of } from 'rxjs';
import { BaseServiceModel, FieldAccessing, TranslateModel } from 'shared/models/systemModel';

const firstGroup = [];
export class DemoReportCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getValidateStatus(id: string, oriId: string): TranslateModel {
    const error: TranslateModel = { code: 'ERROR.90021', parameters: [] };
    return id === oriId ? error : null;
  }
}
