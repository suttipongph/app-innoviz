import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseWorkflowComponent } from 'core/components/base-workflow/base-workflow.component';
import { forkJoin, Observable, of } from 'rxjs';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { DemoWorkflowParamView } from 'shared/models/viewModel/demoWorkflowParamView';
import { DemoService } from '../demo.service';
import { DemoWorkflowCustomComponent } from './demo-workflow-custom.component';

@Component({
  selector: 'demo-workflow',
  templateUrl: './demo-workflow.component.html',
  styleUrls: ['./demo-workflow.component.scss']
})
export class DemoWorkflowComponent extends BaseWorkflowComponent<DemoWorkflowParamView> {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  // customerTableOptions: SelectItems[] = [];

  constructor(private service: DemoService, private currentActivatedRoute: ActivatedRoute, public custom: DemoWorkflowCustomComponent) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.workflowName = 'workflowname';
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    super.workflowGetTaskBySerialNumber().subscribe(
      (result) => {
        this.checkAccessMode();
        this.checkPageMode();
      },
      (error) => {
        console.error(error);
        throw error;
      }
    );
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<DemoWorkflowParamView> {
    //return this.service.getUpdateCustomerStatusById(this.id);
    return of(new DemoWorkflowParamView());
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {}

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing().subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  onSave(validation: FormValidationModel): void {}
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        // this.custom.getModelParam(this.model).subscribe((model) => {
        this.onSubmit(true, this.model);
        // });
      }
    });
  }
  onSubmit(isColsing: boolean, model: DemoWorkflowParamView): void {
    // case call function
    // super.onExecuteFunction(this.service.updateCustomerStatus(model));
    // case start workflow
    // super.startWorkflow(model);
    // case action workflow
    super.actionWorkflow(model);
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
