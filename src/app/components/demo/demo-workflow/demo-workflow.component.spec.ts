import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoWorkflowComponent } from './demo-workflow.component';

describe('DemoWorkflowComponent', () => {
  let component: DemoWorkflowComponent;
  let fixture: ComponentFixture<DemoWorkflowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemoWorkflowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoWorkflowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
