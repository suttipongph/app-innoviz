import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServiceFeeTransService } from './service-fee-trans.service';
import { ServiceFeeTransListComponent } from './service-fee-trans-list/service-fee-trans-list.component';
import { ServiceFeeTransItemComponent } from './service-fee-trans-item/service-fee-trans-item.component';
import { ServiceFeeTransItemCustomComponent } from './service-fee-trans-item/service-fee-trans-item-custom.component';
import { ServiceFeeTransListCustomComponent } from './service-fee-trans-list/service-fee-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ServiceFeeTransListComponent, ServiceFeeTransItemComponent],
  providers: [ServiceFeeTransService, ServiceFeeTransItemCustomComponent, ServiceFeeTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ServiceFeeTransListComponent, ServiceFeeTransItemComponent]
})
export class ServiceFeeTransComponentModule {}
