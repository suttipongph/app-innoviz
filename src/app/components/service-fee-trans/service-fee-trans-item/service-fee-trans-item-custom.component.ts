import { TranslateService } from '@ngx-translate/core';
import { AppInjector } from 'app-injector';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { EmptyGuid, ProductType, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode, resetModelToDefualt } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { AccessModeView, InvoiceRevenueTypeItemView, InvoiceTableItemView, ServiceFeeTransItemView } from 'shared/models/viewModel';
import { GetTaxValueAndWHTValueParm } from 'shared/models/viewModel/getTaxValueAndWHTValueParm';
import { ServiceFeeTransService } from '../service-fee-trans.service';
const MAIN_AGREEMENT_TABLE = 'mainagreementtable';
const ADDENDUM_MAIN_AGREEMENT_TABLE = 'addendummainagreementtable';
const ASSIGNMENT_AGREEMENT_TABLE = 'assignmentagreementtable';
const BUSINESS_COLLATERAL_AGM_TABLE = 'businesscollateralagmtable';
const PURCHASE_TABLE = 'purchasetablepurchase';
const WITHDRAWAL_TABLE_WITHDRAWAL = 'withdrawaltablewithdrawal';
const WITHDRAWAL_TABLE_TERM_EXTENSION = 'withdrawaltabletermextension';
const MESSENGER_JOB_TABLE = 'messengerjobtable';
const MESSENGER_REQUEST = 'messengerrequest';
const RESERVE_REFUND = 'reserverefund';
const RETENTION_REFUND = 'retentionrefund';
const SUSPENSE_REFUND = 'suspenserefund';
const RECEIPT_TEMP_TABLE = 'receipttemptable';
const NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE = 'noticeofcancellationmainagreementtable';
const ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE = 'addendumbusinesscollateralagmtable';
const NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE = 'noticeofcancellation';
const NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE = 'noticeofcancellationbusinesscollateralagmtable';

const firstGroup = ['TAX_AMOUNT', 'AMOUNT_BEFORE_TAX', 'AMOUNT_INCLUDE_TAX', 'WHT_AMOUNT', 'REF_TYPE', 'REF_ID', 'SETTLE_INVOICE_AMOUNT'];
const condition1 = ['SETTLE_AMOUNT', 'SETTLE_WHT_AMOUNT'];
const condition2 = ['CN_REASON_GUID', 'ORIG_INVOICE', 'ORIG_INVOICE_ID', 'ORIG_INVOICE_AMOUNT', 'ORIG_TAX_INVOICE_ID', 'ORIG_TAX_INVOICE_AMOUNT'];

export class ServiceFeeTransItemCustomComponent {
  baseService: BaseServiceModel<ServiceFeeTransService>;
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  refId: string = null;
  productType: ProductType = 0;
  taxDate: string = null;
  customerTableGUID: string = null;
  isRequiredCon1: boolean = false;
  errorGetTaxValue: boolean = false;
  errorGetWHTValue: boolean = false;
  resultGetTaxValue: number = 0;
  resultGetWHTValue: number = 0;
  tempTaxTableGUID: string = null;
  tempWithholdingTaxTableGUID: string = null;
  getFieldAccessing(model: ServiceFeeTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });

    switch (this.parentName) {
      case MAIN_AGREEMENT_TABLE:
      case ADDENDUM_MAIN_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE:
      case ASSIGNMENT_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE:
      case BUSINESS_COLLATERAL_AGM_TABLE:
      case ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
      case NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
        fieldAccessing.push({ filedIds: condition1, readonly: true });
        break;
      default:
        if (model.feeAmount <= 0 || model.invoiceRevenueType_InterompanyTableGUID != null) {
          fieldAccessing.push({ filedIds: condition1, readonly: true });
        } else {
          fieldAccessing.push({ filedIds: condition1, readonly: false });
        }
    }
    if (model.feeAmount < 0) {
      fieldAccessing.push({ filedIds: condition2, readonly: false });
      this.isRequiredCon1 = true;
    } else {
      fieldAccessing.push({ filedIds: condition2, readonly: true });
      this.isRequiredCon1 = false;
    }

    return of(fieldAccessing);
  }
  getDataValidation(): Observable<any> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ServiceFeeTransItemView, isWorkflowMode: boolean): Observable<boolean> {
    return this.setAccessModeByParent(model, isWorkflowMode).pipe(
      map((result) => (isUpdateMode(model.serviceFeeTransGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  setPassParameter(passingObj: ServiceFeeTransItemView) {
    if (!isNullOrUndefined(passingObj)) {
      this.refId = passingObj.refId;
      this.taxDate = passingObj.taxDate;
      this.productType = passingObj.productType;
      this.customerTableGUID = passingObj.invoiceTable_CustomerTableGUID;
      if (!isNullOrUndefined(passingObj.accessModeView)) {
        this.accessModeView = passingObj.accessModeView;
      }
    }
  }
  setAccessModeByParent(model: ServiceFeeTransItemView, isWorkflowMode: boolean): Observable<AccessModeView> {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (this.parentName) {
      case MAIN_AGREEMENT_TABLE:
      case ADDENDUM_MAIN_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE:
        return this.getAccessModeByMainAgreementTable(model.refGUID);
      case ASSIGNMENT_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE:
        return this.getAccessModeByAssignmentAgreementTable(model.refGUID);
      case BUSINESS_COLLATERAL_AGM_TABLE:
      case ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
      case NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
        return this.getAccessModeByBusinessCollateralAgmTable(model.refGUID);
      case PURCHASE_TABLE:
        return this.getPurchaseTableAccessMode(model.refGUID, isWorkflowMode);
      case WITHDRAWAL_TABLE_WITHDRAWAL:
      case WITHDRAWAL_TABLE_TERM_EXTENSION:
        return this.getWithdrawalTableAcessMode(model.refGUID);
      case MESSENGER_JOB_TABLE:
      case MESSENGER_REQUEST:
        return this.getMessengerJobTableAcessMode(model.refGUID);
      case SUSPENSE_REFUND:
      case RESERVE_REFUND:
      case RETENTION_REFUND:
        return this.getCustomerRefundTableAccessMode(model.refGUID);
      case RECEIPT_TEMP_TABLE:
        return this.getReceiptTempTableAccessMode(model.refGUID);
      default:
        return of(this.accessModeView);
    }
  }
  getAccessModeByAssignmentAgreementTable(parentId: string): Observable<AccessModeView> {
    return this.baseService.service.getAccessModeByAssignmentAgreementTable(parentId).pipe(
      tap((result) => {
        this.accessModeView = result;
      })
    );
  }
  getPurchaseTableAccessMode(purchaseId: string, isWorkflowMode: boolean): Observable<AccessModeView> {
    return this.baseService.service.getPurchaseTableAccessMode(purchaseId, isWorkflowMode).pipe(
      tap((result) => {
        this.accessModeView = result;
      })
    );
  }
  getAccessModeByBusinessCollateralAgmTable(parentId: string): Observable<AccessModeView> {
    return this.baseService.service.getAccessModeByBusinessCollateralAgmTable(parentId).pipe(
      tap((result) => {
        this.accessModeView = result;
        this.accessModeView.canView = true;
      })
    );
  }
  getAccessModeByMainAgreementTable(parentId: string): Observable<AccessModeView> {
    return this.baseService.service.getAccessModeByMainAgreementTable(parentId).pipe(
      tap((result) => {
        this.accessModeView = result;
      })
    );
  }
  getWithdrawalTableAcessMode(parentId: string): Observable<AccessModeView> {
    return this.baseService.service.getWithdrawalTableAcessMode(parentId).pipe(
      tap((result) => {
        this.accessModeView = result;
      })
    );
  }
  getMessengerJobTableAcessMode(parentId: string): Observable<AccessModeView> {
    return this.baseService.service.getMessengerJobTableAcessMode(parentId).pipe(
      tap((result) => {
        this.accessModeView = result;
      })
    );
  }
  getCustomerRefundTableAccessMode(parentId: string): Observable<AccessModeView> {
    return this.baseService.service.getCustomerRefundTableAccessMode(parentId).pipe(
      tap((result) => {
        this.accessModeView = result;
      })
    );
  }
  getReceiptTempTableAccessMode(parentId: string): Observable<AccessModeView> {
    return this.baseService.service.getReceiptTempTableAccessMode(parentId).pipe(
      tap((result) => {
        this.accessModeView = result;
      })
    );
  }
  setModelByInvoiceRevenueType(model: ServiceFeeTransItemView, invoiceRevenueTypeOptions: SelectItems[]) {
    this.setFieldByInvoiceRevenueType(model, invoiceRevenueTypeOptions);
  }
  setFieldByInvoiceRevenueType(model: ServiceFeeTransItemView, invoiceRevenueTypeOptions: SelectItems[]): void {
    if (isNullOrUndefOrEmptyGUID(model.invoiceRevenueTypeGUID)) {
      model.description = '';
      model.feeAmount = 0;
      model.taxAmount = 0;
      model.invoiceRevenueType_InterompanyTableGUID = null;
      model.taxTableGUID = null;
      model.withholdingTaxTableGUID = null;
      this.tempTaxTableGUID = null;
      this.tempWithholdingTaxTableGUID = null;
    } else {
      const row = invoiceRevenueTypeOptions.find((o) => o.value === model.invoiceRevenueTypeGUID).rowData as InvoiceRevenueTypeItemView;
      model.description = row.description;
      model.feeAmount = row.feeAmount;
      model.taxAmount = row.feeTaxAmount;
      model.invoiceRevenueType_InterompanyTableGUID = row.intercompanyTableGUID;
      model.taxTableGUID = null;
      model.withholdingTaxTableGUID = null;
      this.tempTaxTableGUID = row.feeTaxGUID;
      this.tempWithholdingTaxTableGUID = row.feeWHTGUID;
      if (row.intercompanyTableGUID != null) {
        model.settleAmount = 0;
        model.settleWHTAmount = 0;
        model.settleInvoiceAmount = 0;
      }
    }
  }
  setCalculateField(model: ServiceFeeTransItemView): void {
    var getTaxValueAndWHTValueParm = new GetTaxValueAndWHTValueParm();
    getTaxValueAndWHTValueParm.withholdingTaxTableGUID =
      this.tempWithholdingTaxTableGUID != null ? this.tempWithholdingTaxTableGUID : model.withholdingTaxTableGUID;
    getTaxValueAndWHTValueParm.taxTableGUID = this.tempTaxTableGUID != null ? this.tempTaxTableGUID : model.taxTableGUID;
    getTaxValueAndWHTValueParm.taxDate = model.taxDate;

    forkJoin(this.getTaxValue(getTaxValueAndWHTValueParm), this.getWHTValue(getTaxValueAndWHTValueParm)).subscribe(() => {
      if (this.errorGetTaxValue) {
        model.taxTableGUID = null;
        model.taxValue = 0;
      } else {
        model.taxValue = this.resultGetTaxValue;
        model.taxTableGUID = getTaxValueAndWHTValueParm.taxTableGUID;
      }
      if (this.errorGetWHTValue) {
        model.withholdingTaxTableGUID = null;
        model.whTaxValue = 0;
      } else {
        model.whTaxValue = this.resultGetWHTValue;
        model.withholdingTaxTableGUID = getTaxValueAndWHTValueParm.withholdingTaxTableGUID;
      }
      this.tempTaxTableGUID = null;
      this.tempWithholdingTaxTableGUID = null;
      this.getCalculateField(model);
    });
  }
  getTaxValue(getTaxValueAndWHTValueParm: GetTaxValueAndWHTValueParm): Observable<any> {
    return this.baseService.service.getTaxValue(getTaxValueAndWHTValueParm).pipe(
      tap((result) => {
        this.errorGetTaxValue = false;
        this.resultGetTaxValue = result;
      }),
      catchError((e) => {
        this.errorGetTaxValue = true;
        this.resultGetTaxValue = 0;
        return of(e);
      })
    );
  }
  getWHTValue(getTaxValueAndWHTValueParm: GetTaxValueAndWHTValueParm): Observable<any> {
    return this.baseService.service.getWHTValue(getTaxValueAndWHTValueParm).pipe(
      tap((result) => {
        this.errorGetWHTValue = false;
        this.resultGetWHTValue = result;
      }),
      catchError((e) => {
        this.errorGetWHTValue = true;
        this.resultGetWHTValue = 0;
        return of(e);
      })
    );
  }
  getCalculateField(model: ServiceFeeTransItemView): void {
    this.baseService.service.getCalculateField(model).subscribe((result) => {
      model.taxAmount = result.taxAmount;
      model.amountBeforeTax = result.amountBeforeTax;
      model.amountIncludeTax = result.amountIncludeTax;
      model.whtAmount = result.whtAmount;
      if (model.invoiceRevenueType_InterompanyTableGUID == null) {
        model.settleWHTAmount = result.settleWHTAmount;
        model.settleInvoiceAmount = result.settleInvoiceAmount;
        this.getCalculateSettleInvoiceAmount(model);
      }
    });
  }
  getCalculateSettleInvoiceAmount(model: ServiceFeeTransItemView): void {
    model.settleInvoiceAmount = model.settleAmount + model.settleWHTAmount;
  }
  setModelByParameter(model: ServiceFeeTransItemView): void {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    if (!(this.parentName === 'messengerjobtable')) {
      model.refId = this.refId;
      model.taxDate = this.taxDate;
      model.productType = this.productType;
    }
  }
  onSettleAmountValidate(model: ServiceFeeTransItemView) {
    if (model.feeAmount > 0 && model.settleAmount > model.amountIncludeTax) {
      return {
        code: 'ERROR.90031',
        parameters: [this.baseService.translate.instant('LABEL.SETTLE_AMOUNT'), (model.amountIncludeTax - model.whtAmount).toString()]
      };
    }
    return null;
  }
  onSettleWHTAmountValidate(model: ServiceFeeTransItemView) {
    if (model.feeAmount > 0 && model.settleWHTAmount > model.whtAmount) {
      return {
        code: 'ERROR.90031',
        parameters: [
          this.baseService.translate.instant('LABEL.SETTLE_WITHHOLDING_TAX_AMOUNT'),
          this.baseService.translate.instant('LABEL.WHT_AMOUNT')
        ]
      };
    }
    return null;
  }
  setModelByFeeAmount(model: ServiceFeeTransItemView): void {
    if (model.feeAmount <= 0 || model.invoiceRevenueType_InterompanyTableGUID != null) {
      model.settleAmount = 0;
      model.settleWHTAmount = 0;
      model.settleInvoiceAmount = 0;
    }
    if (model.feeAmount >= 0) {
      model.cnReasonGUID = null;
      model.origInvoice = null;
      model.origInvoiceId = null;
      model.origInvoiceAmount = 0;
      model.origTaxInvoiceId = null;
      model.origTaxInvoiceAmount = 0;
    }
  }
  setmodelByOriginalInvoice(model: ServiceFeeTransItemView, invoiceTableOptions: SelectItems[]): void {
    if (isNullOrUndefined(model.origInvoice)) {
      model.origInvoiceId = null;
      model.origInvoiceAmount = 0;
      model.origTaxInvoiceId = null;
      model.origTaxInvoiceAmount = 0;
    } else {
      var invoiceTable = invoiceTableOptions.find((f) => f.value === model.origInvoice).rowData as InvoiceTableItemView;
      model.origInvoiceId = invoiceTable.invoiceId;
      model.origInvoiceAmount = invoiceTable.invoiceAmount;
      model.origTaxInvoiceId = invoiceTable.invoiceId;
      model.origTaxInvoiceAmount = invoiceTable.invoiceAmount;
    }
  }
  valiedateServiceFeeTrans(model: ServiceFeeTransItemView): Observable<boolean> {
    var getTaxValueAndWHTValueParm = new GetTaxValueAndWHTValueParm();
    getTaxValueAndWHTValueParm.withholdingTaxTableGUID = model.withholdingTaxTableGUID;
    getTaxValueAndWHTValueParm.taxTableGUID = model.taxTableGUID;
    getTaxValueAndWHTValueParm.taxDate = model.taxDate;
    return this.baseService.service.validateServiceFeeTrans(getTaxValueAndWHTValueParm);
  }
}
