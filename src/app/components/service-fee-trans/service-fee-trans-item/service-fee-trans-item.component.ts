import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, Dimension, RefType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmpty, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { LedgerDimensionItemView, ServiceFeeTransItemView } from 'shared/models/viewModel';
import { ServiceFeeTransService } from '../service-fee-trans.service';
import { ServiceFeeTransItemCustomComponent } from './service-fee-trans-item-custom.component';
@Component({
  selector: 'service-fee-trans-item',
  templateUrl: './service-fee-trans-item.component.html',
  styleUrls: ['./service-fee-trans-item.component.scss']
})
export class ServiceFeeTransItemComponent extends BaseItemComponent<ServiceFeeTransItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  documentReasonOptions: SelectItems[] = [];
  invoiceRevenueTypeOptions: SelectItems[] = [];
  invoiceTableOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  taxTableOptions: SelectItems[] = [];
  withholdingTaxTableOptions: SelectItems[] = [];
  dimension1Options: SelectItems[] = [];
  dimension2Options: SelectItems[] = [];
  dimension3Options: SelectItems[] = [];
  dimension4Options: SelectItems[] = [];
  dimension5Options: SelectItems[] = [];

  constructor(
    private service: ServiceFeeTransService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: ServiceFeeTransItemCustomComponent,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
  }
  ngOnInit(): void {
    let passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setPassParameter(passingObj);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<ServiceFeeTransItemView> {
    return this.service.getServiceFeeTransById(this.id);
  }
  getInitialData(): Observable<ServiceFeeTransItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },

      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.custom.setModelByParameter(this.model);
      this.onAsyncRunner(result);
      super.setDefaultValueSystemFields();
    });
  }

  setRelatedInfoOptions(): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.INQUIRY',
        items: [
          {
            label: 'LABEL.INVOICE',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.INVOICE_TABLE })
          }
        ]
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }

  onAsyncRunner(model?: any): void {
    let productType = isNullOrUndefOrEmpty(model) ? this.model.productType : this.custom.productType;
    forkJoin(
      this.baseDropdown.getDocumentReasonDropDown(RefType.Invoice.toString()),
      this.baseDropdown.getInvoiceRevenueTypeByProductTypeDropDown(productType.toString()),
      this.baseDropdown.getOriginalInvoiceDropDown(this.custom.customerTableGUID),
      this.baseDropdown.getLedgerDimensionDropDown(),
      this.baseDropdown.getTaxTableDropDown(),
      this.baseDropdown.getWithholdingTaxTableDropDown()
    ).subscribe(
      ([documentReason, invoiceRevenueType, originalInvoice, ledgerDimension, taxTable, withholdingTaxTable]) => {
        this.documentReasonOptions = documentReason;
        this.invoiceRevenueTypeOptions = invoiceRevenueType;
        this.invoiceTableOptions = originalInvoice;
        this.dimension1Options = ledgerDimension.filter((t) => (t.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension1);
        this.dimension2Options = ledgerDimension.filter((t) => (t.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension2);
        this.dimension3Options = ledgerDimension.filter((t) => (t.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension3);
        this.dimension4Options = ledgerDimension.filter((t) => (t.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension4);
        this.dimension5Options = ledgerDimension.filter((t) => (t.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension5);
        this.taxTableOptions = taxTable;
        this.withholdingTaxTableOptions = withholdingTaxTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          this.custom.setModelByParameter(this.model);
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model, this.isWorkFlowMode()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.custom.valiedateServiceFeeTrans(this.model).subscribe((res) => {
          if (res) {
            this.onSubmit(false);
          }
        });
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.custom.valiedateServiceFeeTrans(this.model).subscribe((res) => {
          if (res) {
            this.onSubmit(true);
          }
        });
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateServiceFeeTrans(this.model), isColsing);
    } else {
      super.onCreate(this.service.createServiceFeeTrans(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onInvoiceRevenueTypeChange() {
    this.custom.setModelByInvoiceRevenueType(this.model, this.invoiceRevenueTypeOptions);
    this.onFeeAmountChange();
  }
  onIncludeTaxChange() {
    this.custom.setCalculateField(this.model);
  }
  onFeeAmountChange() {
    this.custom.setCalculateField(this.model);
    this.custom.setModelByFeeAmount(this.model);
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  onTaxTableChange() {
    this.custom.setCalculateField(this.model);
  }
  onTaxAmountChange() {
    this.custom.setCalculateField(this.model);
  }
  onWithholdingTaxTableChange() {
    this.custom.setCalculateField(this.model);
  }
  onSettleAmountChange() {
    this.custom.setCalculateField(this.model);
  }
  onSettleWHTAmountChange() {
    this.custom.getCalculateSettleInvoiceAmount(this.model);
  }
  onOriginalInvoiceChange() {
    this.custom.setmodelByOriginalInvoice(this.model, this.invoiceTableOptions);
  }
}
