import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceFeeTransItemComponent } from './service-fee-trans-item.component';

describe('ServiceFeeTransItemViewComponent', () => {
  let component: ServiceFeeTransItemComponent;
  let fixture: ComponentFixture<ServiceFeeTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ServiceFeeTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceFeeTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
