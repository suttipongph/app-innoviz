import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { CopyServiceFeeCondCARequestView } from 'shared/models/viewModel';
@Injectable()
export class CopyServiceFeeCondCARequestService {
  serviceKey = 'copyServiceFeeCondCARequestGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  deleteCopyServiceFeeCondCARequest(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCopyServiceFeeCondCARequest`;
    return this.dataGateway.delete(url, row);
  }
  getCopyServiceFeeCondCARequestById(id: string): Observable<CopyServiceFeeCondCARequestView> {
    const url = `${this.servicePath}/GetCopyServiceFeeCondCARequestById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCopyServiceFeeCondCARequest(vmModel: CopyServiceFeeCondCARequestView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCopyServiceFeeCondCARequest`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCopyServiceFeeCondCARequest(vmModel: CopyServiceFeeCondCARequestView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCopyServiceFeeCondCARequest`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getCopyServiceFeeCondCARequestInitialData(id: string): Observable<CopyServiceFeeCondCARequestView> {
    const url = `${this.servicePath}/GetCopyServiceFeeCondCARequestInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
}
