import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { CopyServiceFeeCondCARequestView } from 'shared/models/viewModel';
import { CopyServiceFeeCondCARequestService } from '../copy-service-fee-cond-ca-request.service';
import { CopyServiceFeeCondCARequestCustomComponent } from './copy-service-fee-cond-ca-request-custom.component';
@Component({
  selector: 'copy-service-fee-cond-ca-request',
  templateUrl: './copy-service-fee-cond-ca-request.component.html',
  styleUrls: ['./copy-service-fee-cond-ca-request.component.scss']
})
export class CopyServiceFeeCondCARequestComponent extends BaseItemComponent<CopyServiceFeeCondCARequestView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  creditAppRequestTableOptions: SelectItems[] = [];

  constructor(
    private service: CopyServiceFeeCondCARequestService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CopyServiceFeeCondCARequestCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.isUpdateMode = false;
    this.setInitialCreatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<CopyServiceFeeCondCARequestView> {
    return this.service.getCopyServiceFeeCondCARequestById(this.id);
  }
  getInitialData(): Observable<CopyServiceFeeCondCARequestView> {
    return this.service.getCopyServiceFeeCondCARequestInitialData(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner(result);
      this.setRelatedInfoOptions();
      this.setFunctionOptions();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: CopyServiceFeeCondCARequestView): void {
    forkJoin(this.baseDropdown.getCreditAppRequestTableByCreditAppTableDropDown(model.creditAppTableGUID)).subscribe(
      ([creditAppRequestTable]) => {
        this.creditAppRequestTableOptions = creditAppRequestTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  onClose(): void {
    super.onBack();
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    super.onExecuteFunction(this.service.createCopyServiceFeeCondCARequest(this.model));
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
