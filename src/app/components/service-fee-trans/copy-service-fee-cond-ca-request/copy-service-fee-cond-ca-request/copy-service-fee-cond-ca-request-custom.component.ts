import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CopyServiceFeeCondCARequestView } from 'shared/models/viewModel';

const firstGroup = ['SERVICE_FEE_CATEGORY'];

export class CopyServiceFeeCondCARequestCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CopyServiceFeeCondCARequestView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CopyServiceFeeCondCARequestView): Observable<boolean> {
    return of(false);
  }
  getInitialData(): Observable<CopyServiceFeeCondCARequestView> {
    let model = new CopyServiceFeeCondCARequestView();
    model.copyServiceFeeCondCARequestGUID = EmptyGuid;
    return of(model);
  }
}
