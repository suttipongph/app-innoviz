import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopyServiceFeeCondCARequestComponent } from './copy-service-fee-cond-ca-request.component';

describe('CopyServiceFeeCondCARequestViewComponent', () => {
  let component: CopyServiceFeeCondCARequestComponent;
  let fixture: ComponentFixture<CopyServiceFeeCondCARequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CopyServiceFeeCondCARequestComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyServiceFeeCondCARequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
