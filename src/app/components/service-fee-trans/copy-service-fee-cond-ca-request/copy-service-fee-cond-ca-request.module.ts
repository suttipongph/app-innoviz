import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CopyServiceFeeCondCARequestService } from './copy-service-fee-cond-ca-request.service';
import { CopyServiceFeeCondCARequestComponent } from './copy-service-fee-cond-ca-request/copy-service-fee-cond-ca-request.component';
import { CopyServiceFeeCondCARequestCustomComponent } from './copy-service-fee-cond-ca-request/copy-service-fee-cond-ca-request-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CopyServiceFeeCondCARequestComponent],
  providers: [CopyServiceFeeCondCARequestService, CopyServiceFeeCondCARequestComponent, CopyServiceFeeCondCARequestCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CopyServiceFeeCondCARequestComponent]
})
export class CopyServiceFeeCondCARequestComponentModule {}
