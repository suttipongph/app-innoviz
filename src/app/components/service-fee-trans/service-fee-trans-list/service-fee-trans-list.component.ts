import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, ROUTE_FUNCTION_GEN, SortType } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { ServiceFeeTransListView } from 'shared/models/viewModel';
import { ServiceFeeTransService } from '../service-fee-trans.service';
import { ServiceFeeTransListCustomComponent } from './service-fee-trans-list-custom.component';

@Component({
  selector: 'service-fee-trans-list',
  templateUrl: './service-fee-trans-list.component.html',
  styleUrls: ['./service-fee-trans-list.component.scss']
})
export class ServiceFeeTransListComponent extends BaseListComponent<ServiceFeeTransListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  invoiceRevenueTypeOptions: SelectItems[] = [];
  ledgerDimensionOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  taxTableOptions: SelectItems[] = [];
  withholdingTaxTableOptions: SelectItems[] = [];
  defualtBitOption: SelectItems[] = [];

  constructor(
    public custom: ServiceFeeTransListCustomComponent,
    private service: ServiceFeeTransService,
    public uiControllerService: UIControllerService,
    private currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
    this.custom.service = this.service;
  }
  ngOnInit(): void {
    let passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setPassParameter(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();

    this.custom.setAccessModeByParentStatus(this.parentId, this.isWorkFlowMode()).subscribe((result) => {
      this.setFunctionOptions();
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.baseDropdown.getDefaultBitDropDown(this.defualtBitOption);
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getInvoiceRevenueTypeByProductTypeDropDown(this.custom.productType.toString(), this.invoiceRevenueTypeOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'serviceFeeTransGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.ORDERING',
        textKey: 'ordering',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.SERVICE_FEE_TYPE_ID',
        textKey: 'invoiceRevenueType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.invoiceRevenueTypeOptions,
        searchingKey: 'invoiceRevenueTypeGUID',
        sortingKey: 'invoiceRevenueType_RevenueTypeId'
      },
      {
        label: 'LABEL.INCLUDE_TAX',
        textKey: 'includeTax',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defualtBitOption
      },
      {
        label: 'LABEL.FEE_AMOUNT',
        textKey: 'feeAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.AMOUNT_BEFORE_TAX',
        textKey: 'amountBeforeTax',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<ServiceFeeTransListView>> {
    return this.service.getServiceFeeTransToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteServiceFeeTrans(row));
  }
  setFunctionOptions(): void {
    this.functionItems = [
      {
        label: 'LABEL.COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA',
        command: () => {
          this.toFunction({
            path: ROUTE_FUNCTION_GEN.COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA
          });
        },
        visible: this.custom.setVisableFunctionCopyServiceFeeConTransFromCA(),
        disabled: !this.custom.accessModeView.canCreate
      }
    ];
    super.setFunctionOptionsMapping();
  }
}
