import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ServiceFeeTransListComponent } from './service-fee-trans-list.component';

describe('ServiceFeeTransListViewComponent', () => {
  let component: ServiceFeeTransListComponent;
  let fixture: ComponentFixture<ServiceFeeTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ServiceFeeTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceFeeTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
