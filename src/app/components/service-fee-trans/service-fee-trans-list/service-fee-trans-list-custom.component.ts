import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { tap } from 'rxjs/operators';
import { ProductType, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView } from 'shared/models/viewModel';
import { ServiceFeeTransService } from '../service-fee-trans.service';
const MAIN_AGREEMENT_TABLE = 'mainagreementtable';
const ADDENDUM_MAIN_AGREEMENT_TABLE = 'addendummainagreementtable';
const ASSIGNMENT_AGREEMENT_TABLE = 'assignmentagreementtable';
const BUSINESS_COLLATERAL_AGM_TABLE = 'businesscollateralagmtable';
const PURCHASE_TABLE = 'purchasetablepurchase';
const WITHDRAWAL_TABLE_WITHDRAWAL = 'withdrawaltablewithdrawal';
const WITHDRAWAL_TABLE_TERM_EXTENSION = 'withdrawaltabletermextension';
const MESSENGER_JOB_TABLE = 'messengerjobtable';
const MESSENGER_REQUEST = 'messengerrequest';
const RESERVE_REFUND = 'reserverefund';
const RETENTION_REFUND = 'retentionrefund';
const SUSPENSE_REFUND = 'suspenserefund';
const RECEIPT_TEMP_TABLE = 'receipttemptable';
const NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE = 'noticeofcancellationmainagreementtable';
const ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE = 'addendumbusinesscollateralagmtable';
const NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE = 'noticeofcancellation';
const NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE = 'noticeofcancellationbusinesscollateralagmtable';

export class ServiceFeeTransListCustomComponent {
  baseService: BaseServiceModel<ServiceFeeTransService>;
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  service: ServiceFeeTransService;
  productType: ProductType = 0;

  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setAccessModeByParentStatus(parentId: string, isWorkflowMode: boolean): Observable<AccessModeView> {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (this.parentName) {
      case MAIN_AGREEMENT_TABLE:
      case ADDENDUM_MAIN_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE:
        return this.getAccessModeByMainAgreementTable(parentId);
      case ASSIGNMENT_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE:
        return this.getAccessModeByAssignmentAgreementTable(parentId);
      case BUSINESS_COLLATERAL_AGM_TABLE:
      case ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
      case NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
        return this.getAccessModeByBusinessCollateralAgmTable(parentId);
      case PURCHASE_TABLE:
        return this.getPurchaseTableAccessMode(parentId, isWorkflowMode);
      case WITHDRAWAL_TABLE_WITHDRAWAL:
      case WITHDRAWAL_TABLE_TERM_EXTENSION:
        return this.getWithdrawalTableAcessMode(parentId);
      case MESSENGER_JOB_TABLE:
      case MESSENGER_REQUEST:
        return this.getMessengerJobTableAccessMode(parentId);
      case SUSPENSE_REFUND:
      case RESERVE_REFUND:
      case RETENTION_REFUND:
        return this.getCustomerRefundTableAccessMode(parentId);
      case RECEIPT_TEMP_TABLE:
        return this.getReceiptTempTableAccessMode(parentId);
      default:
        return of(this.accessModeView);
    }
  }

  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
  getAccessModeByAssignmentAgreementTable(parentId: string): Observable<AccessModeView> {
    return this.baseService.service.getAccessModeByAssignmentAgreementTable(parentId).pipe(
      tap((result) => {
        this.accessModeView = result;
        this.accessModeView.canView = true;
      })
    );
  }
  getPurchaseTableAccessMode(purchaseId: string, isWorkflowMode: boolean): Observable<AccessModeView> {
    return this.baseService.service.getPurchaseTableAccessMode(purchaseId, isWorkflowMode).pipe(
      tap((result) => {
        this.accessModeView = result;
        this.accessModeView.canView = true;
      })
    );
  }
  getAccessModeByBusinessCollateralAgmTable(parentId: string): Observable<AccessModeView> {
    return this.baseService.service.getAccessModeByBusinessCollateralAgmTable(parentId).pipe(
      tap((result) => {
        this.accessModeView = result;
        this.accessModeView.canView = true;
      })
    );
  }
  getAccessModeByMainAgreementTable(parentId: string): Observable<AccessModeView> {
    return this.baseService.service.getAccessModeByMainAgreementTable(parentId).pipe(
      tap((result) => {
        this.accessModeView = result;
        this.accessModeView.canView = true;
      })
    );
  }
  getWithdrawalTableAcessMode(parentId: string): Observable<AccessModeView> {
    return this.baseService.service.getWithdrawalTableAcessMode(parentId).pipe(
      tap((result) => {
        this.accessModeView = result;
        this.accessModeView.canView = true;
      })
    );
  }
  getCustomerRefundTableAccessMode(parentId: string): Observable<AccessModeView> {
    return this.baseService.service.getCustomerRefundTableAccessMode(parentId).pipe(
      tap((result) => {
        this.accessModeView = result;
        this.accessModeView.canView = true;
      })
    );
  }
  getMessengerJobTableAccessMode(parentId: string): Observable<AccessModeView> {
    return this.baseService.service.getMessengerJobTableAcessMode(parentId).pipe(
      tap((result) => {
        this.accessModeView = result;
        this.accessModeView.canView = true;
      })
    );
  }
  getReceiptTempTableAccessMode(parentId: string): Observable<AccessModeView> {
    return this.baseService.service.getReceiptTempTableAccessMode(parentId).pipe(
      tap((result) => {
        this.accessModeView = result;
        this.accessModeView.canView = true;
      })
    );
  }
  setVisableFunctionCopyServiceFeeConTransFromCA(): boolean {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (this.parentName) {
      case MAIN_AGREEMENT_TABLE:
      case ADDENDUM_MAIN_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE:
        return true;
      default:
        return false;
    }
  }
  setPassParameter(passingObj: any) {
    if (!isNullOrUndefined(passingObj)) {
      this.productType = passingObj.productType;
    }
  }
}
