import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, ServiceFeeTransItemView, ServiceFeeTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { GetTaxValueAndWHTValueParm } from 'shared/models/viewModel/getTaxValueAndWHTValueParm';
import { ServiceFeeTransCalculateFieldView } from 'shared/models/viewModel/serviceFeeTransCalculateFieldView';
@Injectable()
export class ServiceFeeTransService {
  serviceKey = 'serviceFeeTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getServiceFeeTransToList(search: SearchParameter): Observable<SearchResult<ServiceFeeTransListView>> {
    const url = `${this.servicePath}/GetServiceFeeTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteServiceFeeTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteServiceFeeTrans`;
    return this.dataGateway.delete(url, row);
  }
  getServiceFeeTransById(id: string): Observable<ServiceFeeTransItemView> {
    const url = `${this.servicePath}/GetServiceFeeTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createServiceFeeTrans(vmModel: ServiceFeeTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateServiceFeeTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateServiceFeeTrans(vmModel: ServiceFeeTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateServiceFeeTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<ServiceFeeTransItemView> {
    const url = `${this.servicePath}/GetServiceFeeTransInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getCalculateField(vmModel: ServiceFeeTransItemView): Observable<ServiceFeeTransCalculateFieldView> {
    const url = `${this.servicePath}/GetCalculateField`;
    return this.dataGateway.post(url, vmModel);
  }
  getPurchaseTableAccessMode(purchaseId: string, isWorkflowMode: boolean): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetPurchaseTableAccessMode/purchaseId=${purchaseId}/isWorkflowMode=${isWorkflowMode}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByAssignmentAgreementTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByAssignmentAgreementTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByBusinessCollateralAgmTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByBusinessCollateralAgmTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByMainAgreementTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByMainAgreementTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getWithdrawalTableAcessMode(withdrawalId: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetWithdrawalTableAcessMode/withdrawalId=${withdrawalId}`;
    return this.dataGateway.get(url);
  }
  getMessengerJobTableAccessMode(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetMessengerJobTableAccessMode/id=${id}`;
    return this.dataGateway.get(url);
  }
  getMessengerJobTableAcessMode(messengerjobtableId: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetMessengerJobTableAcessMode/messengerjobtableId=${messengerjobtableId}`;
    return this.dataGateway.get(url);
  }
  getCustomerRefundTableAccessMode(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetCustomerRefundTableAccessMode/id=${id}`;
    return this.dataGateway.get(url);
  }
  getReceiptTempTableAccessMode(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetReceiptTempTableAccessMode/id=${id}`;
    return this.dataGateway.get(url);
  }
  validateServiceFeeTrans(vwModel: GetTaxValueAndWHTValueParm): Observable<boolean> {
    const url = `${this.servicePath}/ValidateServiceFeeTrans`;
    return this.dataGateway.post(url, vwModel);
  }
  getTaxValue(vwModel: GetTaxValueAndWHTValueParm): Observable<number> {
    const url = `${this.servicePath}/GetTaxValue`;
    return this.dataGateway.post(url, vwModel);
  }
  getWHTValue(vwModel: GetTaxValueAndWHTValueParm): Observable<number> {
    const url = `${this.servicePath}/GetWHTValue`;
    return this.dataGateway.post(url, vwModel);
  }
}
