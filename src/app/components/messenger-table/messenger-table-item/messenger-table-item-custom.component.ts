import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { MessengerTableItemView } from 'shared/models/viewModel';

const firstGroup = [];

export class MessengerTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: MessengerTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: MessengerTableItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<MessengerTableItemView> {
    let model = new MessengerTableItemView();
    model.messengerTableGUID = EmptyGuid;
    return of(model);
  }
}
