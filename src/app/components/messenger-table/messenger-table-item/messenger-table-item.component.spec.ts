import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessengerTableItemComponent } from './messenger-table-item.component';

describe('MessengerTableItemViewComponent', () => {
  let component: MessengerTableItemComponent;
  let fixture: ComponentFixture<MessengerTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MessengerTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessengerTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
