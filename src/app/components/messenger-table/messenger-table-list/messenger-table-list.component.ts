import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { MessengerTableListView } from 'shared/models/viewModel';
import { MessengerTableService } from '../messenger-table.service';
import { MessengerTableListCustomComponent } from './messenger-table-list-custom.component';

@Component({
  selector: 'messenger-table-list',
  templateUrl: './messenger-table-list.component.html',
  styleUrls: ['./messenger-table-list.component.scss']
})
export class MessengerTableListComponent extends BaseListComponent<MessengerTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  vendorTableOptions: SelectItems[] = [];

  constructor(public custom: MessengerTableListCustomComponent, private service: MessengerTableService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getVendorTableDropDown(this.vendorTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'messengerTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.NAME',
        textKey: 'name',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.TAX_ID',
        textKey: 'taxId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.VENDOR_ID',
        textKey: 'vendorTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.vendorTableOptions,
        searchingKey: 'vendorTableGUID',
        sortingKey: 'vendorTable_VendorId'
      },
      {
        label: 'LABEL.PHONE',
        textKey: 'phone',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PLATE_NUMBER',
        textKey: 'plateNumber',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DRIVER_LICENSE_ID',
        textKey: 'driverLicenseId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<MessengerTableListView>> {
    return this.service.getMessengerTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteMessengerTable(row));
  }
}
