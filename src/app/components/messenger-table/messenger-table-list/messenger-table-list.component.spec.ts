import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MessengerTableListComponent } from './messenger-table-list.component';

describe('MessengerTableListViewComponent', () => {
  let component: MessengerTableListComponent;
  let fixture: ComponentFixture<MessengerTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MessengerTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessengerTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
