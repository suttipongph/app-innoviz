import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MessengerTableService } from './messenger-table.service';
import { MessengerTableListComponent } from './messenger-table-list/messenger-table-list.component';
import { MessengerTableItemComponent } from './messenger-table-item/messenger-table-item.component';
import { MessengerTableItemCustomComponent } from './messenger-table-item/messenger-table-item-custom.component';
import { MessengerTableListCustomComponent } from './messenger-table-list/messenger-table-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [MessengerTableListComponent, MessengerTableItemComponent],
  providers: [MessengerTableService, MessengerTableItemCustomComponent, MessengerTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [MessengerTableListComponent, MessengerTableItemComponent]
})
export class MessengerTableComponentModule {}
