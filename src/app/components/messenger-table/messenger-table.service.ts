import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { MessengerTableItemView, MessengerTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class MessengerTableService {
  serviceKey = 'messengerTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getMessengerTableToList(search: SearchParameter): Observable<SearchResult<MessengerTableListView>> {
    const url = `${this.servicePath}/GetMessengerTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteMessengerTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteMessengerTable`;
    return this.dataGateway.delete(url, row);
  }
  getMessengerTableById(id: string): Observable<MessengerTableItemView> {
    const url = `${this.servicePath}/GetMessengerTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createMessengerTable(vmModel: MessengerTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateMessengerTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateMessengerTable(vmModel: MessengerTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateMessengerTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
