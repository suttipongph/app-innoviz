import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyerInvoiceTableItemComponent } from './buyer-invoice-table-item.component';

describe('BuyerInvoiceTableItemViewComponent', () => {
  let component: BuyerInvoiceTableItemComponent;
  let fixture: ComponentFixture<BuyerInvoiceTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BuyerInvoiceTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerInvoiceTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
