import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmpty, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { BuyerInvoiceTableItemView } from 'shared/models/viewModel';
import { BuyerInvoiceTableService } from '../buyer-invoice-table.service';
import { BuyerInvoiceTableItemCustomComponent } from './buyer-invoice-table-item-custom.component';
@Component({
  selector: 'buyer-invoice-table-item',
  templateUrl: './buyer-invoice-table-item.component.html',
  styleUrls: ['./buyer-invoice-table-item.component.scss']
})
export class BuyerInvoiceTableItemComponent extends BaseItemComponent<BuyerInvoiceTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  buyerAgreementLineOptions: SelectItems[] = [];
  buyerAgreementTableOptions: SelectItems[] = [];

  constructor(
    private service: BuyerInvoiceTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: BuyerInvoiceTableItemCustomComponent,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<BuyerInvoiceTableItemView> {
    return this.service.getBuyerInvoiceTableById(this.id);
  }
  getInitialData(): Observable<BuyerInvoiceTableItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    let buyerAgreementTableGUID = isNullOrUndefOrEmpty(model) ? this.model.buyerAgreementTableGUID : model.buyerAgreementTableGUID;
    let customerTableGUID = isNullOrUndefOrEmpty(model) ? this.model.creditAppTable_CustomerTableGUID : model.creditAppTable_CustomerTableGUID;
    let buyerTableGUID = isNullOrUndefOrEmpty(model) ? this.model.creditAppLine_BuyerTableGUID : model.creditAppLine_BuyerTableGUID;
    forkJoin(
      this.custom.getBuyerAgreementLineDropDown(buyerAgreementTableGUID),
      this.baseDropdown.getBuyerAgeementTableByCustomerAndBuyerDropDown([customerTableGUID, buyerTableGUID])
    ).subscribe(
      ([buyerAgreementLine, buyerAgreementTable]) => {
        this.buyerAgreementLineOptions = buyerAgreementLine;
        this.buyerAgreementTableOptions = buyerAgreementTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateBuyerInvoiceTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createBuyerInvoiceTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onBuyerAgreementTableChange() {
    this.custom.getBuyerAgreementLineDropDown(this.model.buyerAgreementTableGUID).subscribe((result) => {
      this.model.buyerAgreementLineGUID = null;
      this.buyerAgreementLineOptions = result;
    });
  }
}
