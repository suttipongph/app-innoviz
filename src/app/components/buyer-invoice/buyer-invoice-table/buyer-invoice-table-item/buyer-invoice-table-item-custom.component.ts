import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { EmptyGuid, PurchaseStatus } from 'shared/constants';
import { isNullOrUndefOrEmpty, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, BuyerInvoiceTableItemView } from 'shared/models/viewModel';

const firstGroup = ['CREDIT_APP_ID', 'LINE_NUM', 'BUYER_ID'];

const CREDIT_APP_LINE_CHILD = 'creditappline-child';
export class BuyerInvoiceTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  accessModeView: AccessModeView = new AccessModeView();
  parentName = null;
  getFieldAccessing(model: BuyerInvoiceTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BuyerInvoiceTableItemView): Observable<boolean> {
    return this.setAccessModeByParentStatus(model).pipe(
      map((result) => (isUpdateMode(model.buyerInvoiceTableGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  getInitialData(): Observable<BuyerInvoiceTableItemView> {
    let model = new BuyerInvoiceTableItemView();
    model.buyerInvoiceTableGUID = EmptyGuid;
    return of(model);
  }
  setAccessModeByParentStatus(model: BuyerInvoiceTableItemView): Observable<AccessModeView> {
    let parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
    switch (this.parentName) {
      case CREDIT_APP_LINE_CHILD:
        return this.getAccessModeByCreditAppLine(parentId, model);
      default:
        return of(this.accessModeView);
    }
  }

  getAccessModeByCreditAppLine(parentId: string, model: BuyerInvoiceTableItemView): Observable<AccessModeView> {
    return this.baseService.service.getAccessModeByCreditAppLine(parentId).pipe(
      tap((result: AccessModeView) => {
        if (isNullOrUndefOrEmpty(model.documentStatus_StatusId)) {
          this.accessModeView.canCreate = true;
          this.accessModeView.canDelete = true;
          this.accessModeView.canView = true;
          return of(this.accessModeView);
        } else {
          let isStatusValid = model.documentStatus_StatusId === PurchaseStatus.Cancelled;
          result.canCreate = result.canCreate && isStatusValid;
          result.canDelete = result.canDelete && isStatusValid;
          result.canView = result.canView && isStatusValid;
          this.accessModeView = result;
          return result;
        }
      })
    );
  }
  getBuyerAgreementLineDropDown(buyerAgreementTableGUID: string): Observable<SelectItems[]> {
    if (!isNullOrUndefOrEmptyGUID(buyerAgreementTableGUID)) {
      return this.baseService.baseDropdown.getBuyerAgreementLineByBuyerAgreementTransDropDown(buyerAgreementTableGUID);
    } else {
      return of([]);
    }
  }
}
