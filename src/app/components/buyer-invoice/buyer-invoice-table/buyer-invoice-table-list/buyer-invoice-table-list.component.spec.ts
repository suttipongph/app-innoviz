import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BuyerInvoiceTableListComponent } from './buyer-invoice-table-list.component';

describe('BuyerInvoiceTableListViewComponent', () => {
  let component: BuyerInvoiceTableListComponent;
  let fixture: ComponentFixture<BuyerInvoiceTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BuyerInvoiceTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerInvoiceTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
