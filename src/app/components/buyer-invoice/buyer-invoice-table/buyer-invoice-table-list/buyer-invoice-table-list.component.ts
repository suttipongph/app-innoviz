import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { AccessMode, ColumnType, PurchaseStatus, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { BuyerInvoiceTableListView } from 'shared/models/viewModel';
import { BuyerInvoiceTableService } from '../buyer-invoice-table.service';
import { BuyerInvoiceTableListCustomComponent } from './buyer-invoice-table-list-custom.component';
const CREDIT_APP_LINE_CHILD = 'creditappline-child';
@Component({
  selector: 'buyer-invoice-table-list',
  templateUrl: './buyer-invoice-table-list.component.html',
  styleUrls: ['./buyer-invoice-table-list.component.scss']
})
export class BuyerInvoiceTableListComponent extends BaseListComponent<BuyerInvoiceTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  buyerAgreementLineOptions: SelectItems[] = [];
  buyerAgreementTableOptions: SelectItems[] = [];

  constructor(
    public custom: BuyerInvoiceTableListCustomComponent,
    private service: BuyerInvoiceTableService,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.setAccessModeByParentStatus().subscribe((result) => {
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBuyerAgreementLineDropDown(this.buyerAgreementLineOptions);
    this.baseDropdown.getBuyerAgreementTableDropDown(this.buyerAgreementTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'buyerInvoiceTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.BUYER_INVOICE_ID',
        textKey: 'buyerInvoiceId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.INVOICE_DATE',
        textKey: 'invoiceDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DUE_DATE',
        textKey: 'dueDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.AMOUNT',
        textKey: 'amount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BUYER_AGREEMENT_ID',
        textKey: 'buyerAgreementTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerAgreementTableOptions,
        searchingKey: 'buyerAgreementTableGUID',
        sortingKey: 'buyerAgreementTable_BuyerAgreementId'
      },
      {
        label: 'LABEL.PEROID',
        textKey: 'buyerAgreementLine_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerAgreementLineOptions,
        searchingKey: 'buyerAgreementLineGUID',
        sortingKey: 'buyerAgreementLine_Period'
      },
      {
        label: null,
        textKey: 'creditAppLineGUID',
        type: ColumnType.MASTER,
        visibility: false,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<BuyerInvoiceTableListView>> {
    return this.service.getBuyerInvoiceTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteBuyerInvoiceTable(row));
  }
  getRowAuthorize(row: BuyerInvoiceTableListView[]): void {
    if (this.custom.parentName == CREDIT_APP_LINE_CHILD) {
      row.forEach((item) => {
        let accessMode: AccessMode = item.accessModeCanDelete ? AccessMode.full : AccessMode.creator;
        item.rowAuthorize = this.getSingleRowAuthorize(accessMode, item);
      });
    } else {
      super.getRowAuthorize(row);
    }
  }
}
