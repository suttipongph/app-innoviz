import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView, BuyerInvoiceTableListView } from 'shared/models/viewModel';
import { BuyerInvoiceTableService } from '../buyer-invoice-table.service';
const CREDIT_APP_LINE_CHILD = 'creditappline-child';
export class BuyerInvoiceTableListCustomComponent {
  baseService: BaseServiceModel<BuyerInvoiceTableService>;
  accessModeView: AccessModeView = new AccessModeView();
  parentName = null;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }

  setAccessModeByParentStatus(): Observable<AccessModeView> {
    let parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
    switch (this.parentName) {
      case CREDIT_APP_LINE_CHILD:
        return this.baseService.service.getAccessModeByCreditAppLine(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            this.accessModeView.canView = true;
          })
        );
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
}
