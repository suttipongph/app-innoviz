import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BuyerInvoiceTableService } from './buyer-invoice-table.service';
import { BuyerInvoiceTableListComponent } from './buyer-invoice-table-list/buyer-invoice-table-list.component';
import { BuyerInvoiceTableItemComponent } from './buyer-invoice-table-item/buyer-invoice-table-item.component';
import { BuyerInvoiceTableItemCustomComponent } from './buyer-invoice-table-item/buyer-invoice-table-item-custom.component';
import { BuyerInvoiceTableListCustomComponent } from './buyer-invoice-table-list/buyer-invoice-table-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BuyerInvoiceTableListComponent, BuyerInvoiceTableItemComponent],
  providers: [BuyerInvoiceTableService, BuyerInvoiceTableItemCustomComponent, BuyerInvoiceTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BuyerInvoiceTableListComponent, BuyerInvoiceTableItemComponent]
})
export class BuyerInvoiceTableComponentModule {}
