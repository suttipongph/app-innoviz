import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, BuyerInvoiceTableItemView, BuyerInvoiceTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class BuyerInvoiceTableService {
  serviceKey = 'buyerInvoiceTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBuyerInvoiceTableToList(search: SearchParameter): Observable<SearchResult<BuyerInvoiceTableListView>> {
    const url = `${this.servicePath}/GetBuyerInvoiceTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBuyerInvoiceTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBuyerInvoiceTable`;
    return this.dataGateway.delete(url, row);
  }
  getBuyerInvoiceTableById(id: string): Observable<BuyerInvoiceTableItemView> {
    const url = `${this.servicePath}/GetBuyerInvoiceTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBuyerInvoiceTable(vmModel: BuyerInvoiceTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBuyerInvoiceTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBuyerInvoiceTable(vmModel: BuyerInvoiceTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBuyerInvoiceTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<BuyerInvoiceTableItemView> {
    const url = `${this.servicePath}/GetBuyerInvoiceInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }

  getAccessModeByCreditAppLine(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppLine/id=${id}`;
    return this.dataGateway.get(url);
  }
}
