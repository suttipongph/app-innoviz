import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { EmptyGuid, ProductType } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { AccessModeView, ProductSettledTransItemView } from 'shared/models/viewModel';
import { ProductSettledTransService } from '../product-settled-trans.service';

const firstGroup = [
  'INVOICE_SETTLEMENT_DETAIL_GUID',
  'PRODUCT_TYPE',
  'INTEREST_DATE',
  'SETTLED_DATE',
  'INTEREST_DAY',
  'SETTLED_INVOICE_AMOUNT',
  'ORIGINAL_INTEREST_CALC_AMOUNT',
  'SETTLED_PURCHASE_AMOUNT',
  'SETTLED_RESERVE_AMOUNT',
  'RESERVE_TO_BE_REFUND',
  'LAST_RECEIVED_DATE',
  'PDC_INTEREST_OUTSTANDING',
  'POSTED_INTEREST_AMOUNT',
  'REF_TYPE',
  'REF_ID',
  'REF_GUID',
  'DOCUMENT_ID',
  'ORIGINAL_REF_GUID',
  'ORIGINAL_REF_ID',
  'ORIGINAL_DOCUMENT_ID',
  'PRODUCT_SETTLED_TRANS_GUID'
];
const RECEIPT_TEMP_TABLE = 'receipttemptable';

export class ProductSettledTransItemCustomComponent {
  baseService: BaseServiceModel<any>;
  isFactoring: boolean = false;
  isProjectFinance: boolean = false;
  originName: string = null;
  originKey: string = null;
  accessModeView: AccessModeView = new AccessModeView();
  getFieldAccessing(model: ProductSettledTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    this.setVisibleGroup(model);
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  setVisibleGroup(model: ProductSettledTransItemView): void {
    this.isFactoring = model.productType === ProductType.Factoring;
    this.isProjectFinance = model.productType === ProductType.ProjectFinance;
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }

  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ProductSettledTransItemView): Observable<boolean> {
    return this.setAccessModeByParent().pipe(
      map((result) => (isUpdateMode(model.productSettledTransGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  setAccessModeByParent(): Observable<AccessModeView> {
    this.originName = this.baseService.uiService.getRelatedInfoOriginTableName();
    this.originKey = this.baseService.uiService.getRelatedInfoOriginTableKey();
    switch (this.originName) {
      case RECEIPT_TEMP_TABLE:
        return this.getReceiptTempTableAccessMode(this.originKey);
      default:
        this.accessModeView.canCreate = false;
        this.accessModeView.canView = false;
        this.accessModeView.canDelete = false;
        return of(this.accessModeView);
    }
  }
  getInitialData(): Observable<ProductSettledTransItemView> {
    let model = new ProductSettledTransItemView();
    model.productSettledTransGUID = EmptyGuid;
    return of(model);
  }
  getReceiptTempTableAccessMode(parentId: string): Observable<AccessModeView> {
    return (this.baseService.service as ProductSettledTransService).getReceiptTempTableAccessMode(parentId).pipe(
      tap((result) => {
        this.accessModeView = result;
      })
    );
  }
}
