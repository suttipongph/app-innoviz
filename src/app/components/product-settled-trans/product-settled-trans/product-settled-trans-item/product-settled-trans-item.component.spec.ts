import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductSettledTransItemComponent } from './product-settled-trans-item.component';

describe('ProductSettledTransItemViewComponent', () => {
  let component: ProductSettledTransItemComponent;
  let fixture: ComponentFixture<ProductSettledTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductSettledTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSettledTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
