import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProductSettledTransListComponent } from './product-settled-trans-list.component';

describe('ProductSettledTransListViewComponent', () => {
  let component: ProductSettledTransListComponent;
  let fixture: ComponentFixture<ProductSettledTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductSettledTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSettledTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
