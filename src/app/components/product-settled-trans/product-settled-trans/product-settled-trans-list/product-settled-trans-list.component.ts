import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { ProductSettledTransListView } from 'shared/models/viewModel';
import { ProductSettledTransService } from '../product-settled-trans.service';
import { ProductSettledTransListCustomComponent } from './product-settled-trans-list-custom.component';

@Component({
  selector: 'product-settled-trans-list',
  templateUrl: './product-settled-trans-list.component.html',
  styleUrls: ['./product-settled-trans-list.component.scss']
})
export class ProductSettledTransListComponent extends BaseListComponent<ProductSettledTransListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  productTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  invoiceSettlementDetaileOptions: SelectItems[] = [];
  constructor(
    public custom: ProductSettledTransListCustomComponent,
    private service: ProductSettledTransService,
    public currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
  }
  ngOnInit(): void {
    this.model = this.getPassingObject(this.currentActivatedRoute);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.setAccessModeByParent(this.model).subscribe((result) => {
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getInvoiceSettlementDetaileDropDown(this.invoiceSettlementDetaileOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'productSettledTransGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.INVOICE_ID',
        textKey: 'invoiceSettlementDetail_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'invoiceSettlementDetailGUID',
        sortingKey: 'invoiceTable_InvoiceId',
        masterList: this.invoiceSettlementDetaileOptions
      },
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: 'LABEL.INTEREST_DATE',
        textKey: 'interestDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.SETTLED_DATE',
        textKey: 'settledDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.INTEREST_DAY',
        textKey: 'interestDay',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE,
        format: this.INTEGER
      },
      {
        label: 'LABEL.SETTLED_PURCHASE_AMOUNT',
        textKey: 'settledPurchaseAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: 'LABEL.SETTLED_RESERVE_AMOUNT',
        textKey: 'settledReserveAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: 'LABEL.SETTLED_INVOICE_AMOUNT',
        textKey: 'settledInvoiceAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: 'LABEL.CALCULATED_INTEREST_AMOUNT',
        textKey: 'interestCalcAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: 'LABEL.DOCUMENT_ID',
        textKey: 'documentId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'InvoiceSettlementDetailGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.model.invoiceSettlementDetailGUID
      }
    ];
    this.custom.setOption(columns, this.model);
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<ProductSettledTransListView>> {
    return this.service.getProductSettledTransToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteProductSettledTrans(row));
  }
}
