import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ColumnType, RefType, SortType } from 'shared/constants';
import { BaseServiceModel, ColumnModel } from 'shared/models/systemModel';
import { AccessModeView, ProductSettledTransListView } from 'shared/models/viewModel';
import { ProductSettledTransService } from '../product-settled-trans.service';
const RECEIPT_TEMP_TABLE = 'receipttemptable';
const CREDITAPP_TABLE = 'creditapptable';
const WITHDRAWAL_TABLE = 'withdrawaltablewithdrawal';
const PURCHASE_TABLE_PURCHASE = 'purchaselinepurchase-child';
const WITHDRAWAL_TABLE_TERMEXTENTION = 'withdrawaltabletermextension';

export class ProductSettledTransListCustomComponent {
  baseService: BaseServiceModel<any>;
  originName: string = null;
  originKey: string = null;
  relatedName: string = null;
  relatedKey: string = null;
  accessModeView: AccessModeView = new AccessModeView();
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setAccessModeByParent(model: ProductSettledTransListView): Observable<AccessModeView> {
    this.originName = this.baseService.uiService.getRelatedInfoOriginTableName();
    this.originKey = this.baseService.uiService.getRelatedInfoOriginTableKey();
    this.relatedName = this.baseService.uiService.getRelatedInfoParentTableName();
    this.relatedKey = this.baseService.uiService.getRelatedInfoParentTableKey();
    switch (this.originName) {
      case RECEIPT_TEMP_TABLE:
        return this.getReceiptTempTableAccessMode(this.originKey);
      case CREDITAPP_TABLE:
        switch (this.relatedName) {
          case WITHDRAWAL_TABLE:
            return this.getWithdawalTableAccessMode(this.relatedKey);
            break;
          case WITHDRAWAL_TABLE_TERMEXTENTION:
            return this.getWithdawalTableAccessMode(this.relatedKey);

          case PURCHASE_TABLE_PURCHASE:
            return this.getPurchaseTablePurchaseAccessMode(this.relatedKey);
        }
      default:
        this.accessModeView.canCreate = false;
        this.accessModeView.canView = false;
        this.accessModeView.canDelete = false;
        return of(this.accessModeView);
    }
  }
  getReceiptTempTableAccessMode(parentId: string): Observable<AccessModeView> {
    return (this.baseService.service as ProductSettledTransService).getReceiptTempTableAccessMode(parentId).pipe(
      tap((result) => {
        this.accessModeView = result;
        this.accessModeView.canView = true;
      })
    );
  }
  getWithdawalTableAccessMode(parentId: string): Observable<AccessModeView> {
    return (this.baseService.service as ProductSettledTransService).getWithdawalTableAccessMode(parentId).pipe(
      tap((result) => {
        this.accessModeView = result;
        this.accessModeView.canView = true;
      })
    );
  }
  getPurchaseTablePurchaseAccessMode(parentId: string): Observable<AccessModeView> {
    return (this.baseService.service as ProductSettledTransService).getPurchaseTablePurchaseAccessMode(parentId).pipe(
      tap((result) => {
        this.accessModeView = result;
        this.accessModeView.canView = true;
      })
    );
  }
  setOption(option: ColumnModel[], model: any): any {
    this.originName = this.baseService.uiService.getRelatedInfoOriginTableName();
    this.relatedName = this.baseService.uiService.getRelatedInfoParentTableName();
    if (this.originName == CREDITAPP_TABLE && (this.relatedName == WITHDRAWAL_TABLE || this.relatedName == WITHDRAWAL_TABLE_TERMEXTENTION)) {
      if (model.originalRefGUID) {
        option.push({
          label: null,
          textKey: 'withdrawalTable_WithdrawalTableGUID',
          type: ColumnType.MASTER,
          visibility: true,
          sorting: SortType.NONE,
          parentKey: model.originalRefGUID
        });
        option.push({
          label: null,
          textKey: 'refType',
          type: ColumnType.MASTER,
          visibility: true,
          sorting: SortType.NONE,
          parentKey: RefType.WithdrawalLine.toString()
        });
      }
    } else {
      if (model.originalRefGUID) {
        option.push({
          label: null,
          textKey: 'originalRefGUID',
          type: ColumnType.MASTER,
          visibility: true,
          sorting: SortType.NONE,
          parentKey: model.originalRefGUID
        });
        option.push({
          label: null,
          textKey: 'refType',
          type: ColumnType.MASTER,
          visibility: true,
          sorting: SortType.NONE,
          parentKey: model.refType
        });
      }
    }
  }
}
