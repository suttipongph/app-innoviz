import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductSettledTransService } from './product-settled-trans.service';
import { ProductSettledTransListComponent } from './product-settled-trans-list/product-settled-trans-list.component';
import { ProductSettledTransItemComponent } from './product-settled-trans-item/product-settled-trans-item.component';
import { ProductSettledTransItemCustomComponent } from './product-settled-trans-item/product-settled-trans-item-custom.component';
import { ProductSettledTransListCustomComponent } from './product-settled-trans-list/product-settled-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ProductSettledTransListComponent, ProductSettledTransItemComponent],
  providers: [ProductSettledTransService, ProductSettledTransItemCustomComponent, ProductSettledTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ProductSettledTransListComponent, ProductSettledTransItemComponent]
})
export class ProductSettledTransComponentModule {}
