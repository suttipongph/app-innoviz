import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel} from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { RecalProductSettledResultView, RecalProductSettledTransView } from 'shared/models/viewModel/RecalProductSettledTransView';
@Injectable()
export class RecalProductSettledTransService {
  serviceKey = 'invoiceSettlementDetailGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getRecalProductSettledTransById(id: string): Observable<RecalProductSettledTransView> {
    const url = `${this.servicePath}/GetRecalProductSettledTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  recalculateProductSettledTrans(vmModel: RecalProductSettledTransView): Observable<RecalProductSettledResultView> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vmModel);
  }
}
