import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { RecalProductSettledTransView } from 'shared/models/viewModel/RecalProductSettledTransView';

const firstGroup = ['INVOICE_ID', 'DOCUMENT_ID'];
export class RecalProductSettledTransCustomComponent {
  invoiceId: string;
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: RecalProductSettledTransView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: RecalProductSettledTransView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<RecalProductSettledTransView> {
    let model = new RecalProductSettledTransView();
    model.productSettledTransGUID = EmptyGuid;
    return of(model);
  }
}
