import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecalProductSettledTransComponent } from './recal-product-settled-trans.component';

describe('RecalProductSettledTransComponent', () => {
  let component: RecalProductSettledTransComponent;
  let fixture: ComponentFixture<RecalProductSettledTransComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecalProductSettledTransComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecalProductSettledTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
