import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RecalProductSettledTransComponent } from './recal-product-settled-trans/recal-product-settled-trans.component';
import { RecalProductSettledTransCustomComponent } from './recal-product-settled-trans/recal-product-settled-trans-custom.component';
import { RecalProductSettledTransService } from './recal-product-settled-trans.service';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [RecalProductSettledTransComponent],
  providers: [RecalProductSettledTransService, RecalProductSettledTransCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [RecalProductSettledTransComponent]
})
export class RecalProductSettledTransComponentModule {}
