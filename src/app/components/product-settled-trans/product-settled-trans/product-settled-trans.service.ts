import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, ProductSettledTransItemView, ProductSettledTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ProductSettledTransService {
  serviceKey = 'productSettledTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getProductSettledTransToList(search: SearchParameter): Observable<SearchResult<ProductSettledTransListView>> {
    const url = `${this.servicePath}/GetProductSettledTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteProductSettledTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteProductSettledTrans`;
    return this.dataGateway.delete(url, row);
  }
  getProductSettledTransById(id: string): Observable<ProductSettledTransItemView> {
    const url = `${this.servicePath}/GetProductSettledTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createProductSettledTrans(vmModel: ProductSettledTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateProductSettledTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateProductSettledTrans(vmModel: ProductSettledTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateProductSettledTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getReceiptTempTableAccessMode(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetReceiptTempTableAccessMode/id=${id}`;
    return this.dataGateway.get(url);
  }
  getWithdawalTableAccessMode(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetWithdawalTableAccessMode/id=${id}`;
    return this.dataGateway.get(url);
  }
  getPurchaseTablePurchaseAccessMode(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetPurchaseTablePurchaseAccessMode/id=${id}`;
    return this.dataGateway.get(url);
  }
}
