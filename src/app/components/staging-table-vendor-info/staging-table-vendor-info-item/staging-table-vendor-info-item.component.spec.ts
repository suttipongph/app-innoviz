import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StagingTableVendorInfoItemComponent } from './staging-table-vendor-info-item.component';

describe('StagingTableVendorInfoItemViewComponent', () => {
  let component: StagingTableVendorInfoItemComponent;
  let fixture: ComponentFixture<StagingTableVendorInfoItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StagingTableVendorInfoItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StagingTableVendorInfoItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
