import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { StagingTableVendorInfoItemView } from 'shared/models/viewModel/stagingTableVendorInfoItemView';

const firstGroup = [
  'VENDOR_ID',
  'NAME',
  'ALT_NAME',
  'CURRENCY_ID',
  'GROUPD_ID',
  'VENDOR_TAX_ID',
  'BANK_ACCOUNT',
  'BANK_ACCOUNT_NAME',
  'BANK_GROUP_ID',
  'BANK_BRANCH',
  'ADDRESS_NAME',
  'ADDRESS',
  'COUNTRY_ID',
  'PROVINCE_ID',
  'DISTRICT_ID',
  'SUB_DISTRICT_ID',
  'POSTAL_CODE',
  'TAX_BRANCH_ID',
  'PROCESS_TRANS_GUID',
  'VENDOR_PAYMENT_TRANS_GUID',
  'STAGING_TABLE_VENDOR_INFO_GUID',
  'STAGING_TABLE_COMPANY_ID',
  'RECORD_TYPE'
];

export class StagingTableVendorInfoItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: StagingTableVendorInfoItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: StagingTableVendorInfoItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(): Observable<StagingTableVendorInfoItemView> {
    let model = new StagingTableVendorInfoItemView();
    model.stagingTableVendorInfoGUID = EmptyGuid;
    return of(model);
  }
}
