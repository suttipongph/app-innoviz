import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StagingTableVendorInfoListComponent } from './staging-table-vendor-info-list.component';

describe('StagingTableVendorInfoListViewComponent', () => {
  let component: StagingTableVendorInfoListComponent;
  let fixture: ComponentFixture<StagingTableVendorInfoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StagingTableVendorInfoListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StagingTableVendorInfoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
