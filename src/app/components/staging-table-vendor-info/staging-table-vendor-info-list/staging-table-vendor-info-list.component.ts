import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { StagingTableVendorInfoListView } from 'shared/models/viewModel/stagingTableVendorInfoListView';
import { StagingTableVendorInfoService } from '../staging-table-vendor-info.service';
import { StagingTableVendorInfoListCustomComponent } from './staging-table-vendor-info-list-custom.component';

@Component({
  selector: 'staging-table-vendor-info-list',
  templateUrl: './staging-table-vendor-info-list.component.html',
  styleUrls: ['./staging-table-vendor-info-list.component.scss']
})
export class StagingTableVendorInfoListComponent extends BaseListComponent<StagingTableVendorInfoListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  recordTypeOptions: SelectItems[] = [];
  constructor(
    public custom: StagingTableVendorInfoListCustomComponent,
    private service: StagingTableVendorInfoService,
    private currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {
    let passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setPassParameter(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getRecordTypeEnumDropDown(this.recordTypeOptions);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'stagingTableVendorInfoGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.RECORD_TYPE',
        textKey: 'recordType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.recordTypeOptions
      },
      {
        label: 'LABEL.VENDOR_ID',
        textKey: 'vendorId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.NAME',
        textKey: 'name',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CURRENCY_ID',
        textKey: 'currencyId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.VEND_GROUP_ID',
        textKey: 'vendGroupId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BANK_ACCOUNT_NAME',
        textKey: 'bankAccountName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BANK_ACCOUNT',
        textKey: 'bankAccount',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.FINANCIAL_INSTITUTION_ID',
        textKey: 'bankGroupId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'processTransGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.custom.processTransGUID
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<StagingTableVendorInfoListView>> {
    return this.service.getStagingTableVendorInfoToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteStagingTableVendorInfo(row));
  }
}
