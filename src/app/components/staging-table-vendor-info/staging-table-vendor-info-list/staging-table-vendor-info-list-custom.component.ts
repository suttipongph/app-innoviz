import { Observable } from 'rxjs';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel } from 'shared/models/systemModel';
import { StagingTableVendorInfoListView } from 'shared/models/viewModel/stagingTableVendorInfoListView';

export class StagingTableVendorInfoListCustomComponent {
  baseService: BaseServiceModel<any>;
  processTransGUID: string = null;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setPassParameter(passingObj: StagingTableVendorInfoListView) {
    if (!isNullOrUndefined(passingObj)) {
      this.processTransGUID = passingObj.processTransGUID;
    }
  }
}
