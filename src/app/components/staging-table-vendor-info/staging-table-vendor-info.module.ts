import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StagingTableVendorInfoService } from './staging-table-vendor-info.service';
import { StagingTableVendorInfoListComponent } from './staging-table-vendor-info-list/staging-table-vendor-info-list.component';
import { StagingTableVendorInfoItemComponent } from './staging-table-vendor-info-item/staging-table-vendor-info-item.component';
import { StagingTableVendorInfoItemCustomComponent } from './staging-table-vendor-info-item/staging-table-vendor-info-item-custom.component';
import { StagingTableVendorInfoListCustomComponent } from './staging-table-vendor-info-list/staging-table-vendor-info-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [StagingTableVendorInfoListComponent, StagingTableVendorInfoItemComponent],
  providers: [StagingTableVendorInfoService, StagingTableVendorInfoItemCustomComponent, StagingTableVendorInfoListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [StagingTableVendorInfoListComponent, StagingTableVendorInfoItemComponent]
})
export class StagingTableVendorInfoComponentModule {}
