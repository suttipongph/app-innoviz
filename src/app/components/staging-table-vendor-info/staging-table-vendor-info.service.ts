import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { StagingTableVendorInfoListView } from 'shared/models/viewModel/stagingTableVendorInfoListView';
import { StagingTableVendorInfoItemView } from 'shared/models/viewModel/stagingTableVendorInfoItemView';
@Injectable()
export class StagingTableVendorInfoService {
  serviceKey = 'stagingTableVendorInfoGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getStagingTableVendorInfoToList(search: SearchParameter): Observable<SearchResult<StagingTableVendorInfoListView>> {
    const url = `${this.servicePath}/GetStagingTableVendorInfoList/${search.branchFilterMode}`;
    console.log(url);
    return this.dataGateway.getList(url, search);
  }
  deleteStagingTableVendorInfo(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteStagingTableVendorInfo`;
    return this.dataGateway.delete(url, row);
  }
  getStagingTableVendorInfoById(id: string): Observable<StagingTableVendorInfoItemView> {
    const url = `${this.servicePath}/GetStagingTableVendorInfoById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createStagingTableVendorInfo(vmModel: StagingTableVendorInfoItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateStagingTableVendorInfo`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateStagingTableVendorInfo(vmModel: StagingTableVendorInfoItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateStagingTableVendorInfo`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
