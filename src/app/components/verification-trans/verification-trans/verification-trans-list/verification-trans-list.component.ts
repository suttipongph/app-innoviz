import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { VerificationTransListView } from 'shared/models/viewModel/verificationTransListView';
import { VerificationTransService } from '../verification-trans.service';
import { VerificationTransListCustomComponent } from './verification-trans-list-custom.component';

@Component({
  selector: 'verification-trans-list',
  templateUrl: './verification-trans-list.component.html',
  styleUrls: ['./verification-trans-list.component.scss']
})
export class VerificationTransListComponent extends BaseListComponent<VerificationTransListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  refTypeOptions: SelectItems[] = [];
  verificationTableOptions: SelectItems[] = [];

  constructor(
    public custom: VerificationTransListCustomComponent,
    private service: VerificationTransService,
    public uiControllerService: UIControllerService,
    private currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
    this.custom.activeId = this.uiControllerService.getRelatedInfoActiveTableKey();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.setAccessModeByParentStatus(this.parentId, this.isWorkFlowMode()).subscribe((result) => {
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getVerificationTableByVerificationTransListViewDropDown(this.verificationTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.getCanView();
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'verificationTransGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.VERIFICATION_ID',
        textKey: 'verificationTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.verificationTableOptions,
        searchingKey: 'verificationTableGUID',
        sortingKey: 'verificationTable_VerificationId'
      },
      {
        label: 'LABEL.VERIFICATION_DATE',
        textKey: 'verificationTable_VerificationDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<VerificationTransListView>> {
    return this.service.getVerificationTransToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteVerificationTrans(row));
  }
}
