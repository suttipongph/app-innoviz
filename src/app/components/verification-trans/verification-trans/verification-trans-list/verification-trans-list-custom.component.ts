import { VerificationTableService } from 'components/verification/verification-table/verification-table.service';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView } from 'shared/models/viewModel';
import { VerificationTransService } from '../verification-trans.service';

export class VerificationTransListCustomComponent {
  baseService: BaseServiceModel<VerificationTransService>;
  parentName = null;
  activeId: string;
  accessModeView: AccessModeView = new AccessModeView();

  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setAccessModeByParentStatus(parentId: string, isWorkflowMode: boolean) {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (this.parentName) {
      case ROUTE_RELATED_GEN.PURCHASE_LINE_PURCHASE:
      case ROUTE_RELATED_GEN.PURCHASE_LINE_ROLL_BILL:
        return this.baseService.service
          .getPurchaseTableAccessMode(this.activeId, isWorkflowMode)
          .pipe(tap((result) => (this.accessModeView = result)));
      case ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_WITHDRAWAL:
      case ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_TERM_EXTENSION:
        return this.baseService.service.getWithdrawalTableAccessMode(parentId).pipe(tap((result) => (this.accessModeView = result)));
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
}
