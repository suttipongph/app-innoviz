import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { VerificationTransListComponent } from './verification-trans-list.component';

describe('VerificationTransListViewComponent', () => {
  let component: VerificationTransListComponent;
  let fixture: ComponentFixture<VerificationTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VerificationTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
