import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VerificationTransService } from './verification-trans.service';
import { VerificationTransListComponent } from './verification-trans-list/verification-trans-list.component';
import { VerificationTransItemComponent } from './verification-trans-item/verification-trans-item.component';
import { VerificationTransItemCustomComponent } from './verification-trans-item/verification-trans-item-custom.component';
import { VerificationTransListCustomComponent } from './verification-trans-list/verification-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [VerificationTransListComponent, VerificationTransItemComponent],
  providers: [VerificationTransService, VerificationTransItemCustomComponent, VerificationTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [VerificationTransListComponent, VerificationTransItemComponent]
})
export class VerificationTransComponentModule {}
