import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerificationTransItemComponent } from './verification-trans-item.component';

describe('VerificationTransItemViewComponent', () => {
  let component: VerificationTransItemComponent;
  let fixture: ComponentFixture<VerificationTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VerificationTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
