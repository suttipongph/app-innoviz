import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { VerificationTransItemView } from 'shared/models/viewModel/verificationTransItemView';
import { VerificationTransService } from '../verification-trans.service';
import { VerificationTransItemCustomComponent } from './verification-trans-item-custom.component';
@Component({
  selector: 'verification-trans-item',
  templateUrl: './verification-trans-item.component.html',
  styleUrls: ['./verification-trans-item.component.scss']
})
export class VerificationTransItemComponent extends BaseItemComponent<VerificationTransItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  refTypeOptions: SelectItems[] = [];
  verificationTableOptions: SelectItems[] = [];

  constructor(
    private service: VerificationTransService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: VerificationTransItemCustomComponent,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
    this.custom.activeId = this.uiControllerService.getRelatedInfoActiveTableKey();
  }
  ngOnInit(): void {
    let passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setPassParameter(passingObj);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<VerificationTransItemView> {
    return this.service.getVerificationTransById(this.id);
  }
  getInitialData(): Observable<VerificationTransItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.custom.setModelByParameter(this.model);
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(
      this.custom.getVerificationTableByVerificationTransDropDown(isNullOrUndefined(model) ? this.model.documentId : model?.documentId)
    ).subscribe(
      ([verificationTable]) => {
        this.verificationTableOptions = verificationTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          this.custom.setModelByParameter(this.model);
          this.custom.setModelByVerificationTable(this.model, this.verificationTableOptions);
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model, this.isWorkFlowMode()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateVerificationTrans(this.model), isColsing);
    } else {
      super.onCreate(this.service.createVerificationTrans(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onVerificationTableChange() {
    this.custom.setModelByVerificationTable(this.model, this.verificationTableOptions);
  }
}
