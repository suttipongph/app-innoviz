import { Observable, of } from 'rxjs';
import { EmptyGuid, ROUTE_RELATED_GEN } from 'shared/constants';
import { map } from 'rxjs/operators';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, VerificationTableItemView } from 'shared/models/viewModel';
import { VerificationTransItemView } from 'shared/models/viewModel/verificationTransItemView';
import { VerificationTransService } from '../verification-trans.service';

const firstGroup = ['VERIFICATION_DATE', 'BUYER_ID', 'REMARK', 'REF_TYPE', 'REF_ID', 'DOCUMENT_ID', 'VERIFICATION_TRANS_GUID'];

export class VerificationTransItemCustomComponent {
  baseService: BaseServiceModel<VerificationTransService>;
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  refId: string = null;
  buyerTableGUID: string = null;
  creditAppTableGUID: string = null;
  activeId: string = null;
  getFieldAccessing(model: VerificationTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: VerificationTransItemView, isWorkflowMode: boolean): Observable<boolean> {
    const accessright = isNullOrUndefOrEmptyGUID(model.verificationTransGUID) ? canCreate : canUpdate;
    switch (this.parentName) {
      case ROUTE_RELATED_GEN.PURCHASE_LINE_PURCHASE:
      case ROUTE_RELATED_GEN.PURCHASE_LINE_ROLL_BILL:
        return this.baseService.service.getPurchaseTableAccessMode(this.activeId, isWorkflowMode).pipe(
          map((result) => {
            return !(result.canView && accessright);
          })
        );
      case ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_WITHDRAWAL:
      case ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_TERM_EXTENSION:
        return this.baseService.service.getWithdrawalTableAccessMode(model.refGUID).pipe(
          map((result) => {
            return !(result.canView && accessright);
          })
        );
      default:
        return of(
          !isUpdateMode(model.verificationTransGUID) ? !(this.accessModeView.canCreate && canCreate) : !(this.accessModeView.canView && canUpdate)
        );
    }
  }
  setPassParameter(passingObj: VerificationTransItemView) {
    if (!isNullOrUndefined(passingObj)) {
      this.refId = passingObj.refId;
      this.buyerTableGUID = passingObj.buyerTableGUID;
      this.creditAppTableGUID = passingObj.creditAppTableGUID;
    }
  }
  setModelByParameter(model: VerificationTransItemView): void {
    model.refId = this.refId;
  }
  setModelByVerificationTable(model: VerificationTransItemView, verificationTableOptions: SelectItems[]) {
    this.setValueByVerificationTable(model, verificationTableOptions);
  }
  setValueByVerificationTable(model: VerificationTransItemView, verificationTableOptions: SelectItems[]): void {
    const row = isNullOrUndefOrEmptyGUID(model.verificationTableGUID)
      ? new VerificationTableItemView()
      : (verificationTableOptions.find((o) => o.value === model.verificationTableGUID).rowData as VerificationTableItemView);
    model.verificationDate = row.verificationDate;
    model.buyerTable_Values = row.buyerTable_Values;
    model.remark = row.remark;
  }
  getVerificationTableByVerificationTransDropDown(documentId: string): Observable<SelectItems[]> {
    return this.baseService.baseDropdown.getVerificationTableByVerificationTransItemViewDropDown([
      this.buyerTableGUID,
      this.creditAppTableGUID,
      documentId
    ]);
  }
}
