import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult, TranslateModel } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { VerificationTransListView } from 'shared/models/viewModel/verificationTransListView';
import { VerificationTransItemView } from 'shared/models/viewModel/verificationTransItemView';
import { AccessModeView } from 'shared/models/viewModel';
@Injectable()
export class VerificationTransService {
  serviceKey = 'verificationTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getVerificationTransToList(search: SearchParameter): Observable<SearchResult<VerificationTransListView>> {
    const url = `${this.servicePath}/GetVerificationTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteVerificationTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteVerificationTrans`;
    return this.dataGateway.delete(url, row);
  }
  getVerificationTransById(id: string): Observable<VerificationTransItemView> {
    const url = `${this.servicePath}/GetVerificationTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createVerificationTrans(vmModel: VerificationTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateVerificationTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateVerificationTrans(vmModel: VerificationTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateVerificationTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<VerificationTransItemView> {
    const url = `${this.servicePath}/GetVerificationTransInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  validateVerificationTrans(vmModel: VerificationTransItemView): Observable<boolean> {
    const url = `${this.servicePath}/ValidateVerificationTrans`;
    return this.dataGateway.post(url, vmModel);
  }

  getPurchaseTableAccessMode(purchaseId: string, isWorkflowMode: boolean): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetPurchaseTableAccessMode/purchaseId=${purchaseId}/isWorkflowMode=${isWorkflowMode}`;
    return this.dataGateway.get(url);
  }
  getWithdrawalTableAccessMode(withdrawalId: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetWithdrawalTableAccessMode/withdrawalId=${withdrawalId}`;
    return this.dataGateway.get(url);
  }
}
