import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CancelBuyerReceiptTableService } from './cancel-buyer-receipt-table.service';
import { CancelBuyerReceiptTableComponent } from './cancel-buyer-receipt-table/cancel-buyer-receipt-table.component';
import { CancelBuyerReceiptTableCustomComponent } from './cancel-buyer-receipt-table/cancel-buyer-receipt-table-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CancelBuyerReceiptTableComponent],
  providers: [CancelBuyerReceiptTableService, CancelBuyerReceiptTableCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CancelBuyerReceiptTableComponent]
})
export class CancelBuyerReceiptTableComponentModule {}
