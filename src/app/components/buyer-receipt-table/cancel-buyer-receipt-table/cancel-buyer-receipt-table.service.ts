import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { CancelBuyerReceiptTableResultView, CancelBuyerReceiptTableView } from 'shared/models/viewModel/cancelBuyerReceiptTableView';
@Injectable()
export class CancelBuyerReceiptTableService {
  serviceKey = 'buyerReceiptTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCancelBuyerReceiptTableById(id: string): Observable<CancelBuyerReceiptTableView> {
    const url = `${this.servicePath}/GetCancelBuyerReceiptTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  cancelBuyerReceiptTable(vmModel: CancelBuyerReceiptTableView): Observable<CancelBuyerReceiptTableResultView> {
    const url = `${this.servicePath}/CancelBuyerReceiptTable`;
    return this.dataGateway.post(url, vmModel);
  }
}
