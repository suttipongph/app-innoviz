import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CancelBuyerReceiptTableView } from 'shared/models/viewModel/cancelBuyerReceiptTableView';

const firstGroup = ['BUYER_RECEIPT_ID', 'BUYER_RECEIPT_DATE', 'BUYER_ID', 'CUSTOMER_ID', 'BUYER_RECEIPT_AMOUNT', 'CANCEL_RECEIPT'];

export class CancelBuyerReceiptTableCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CancelBuyerReceiptTableView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CancelBuyerReceiptTableView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<CancelBuyerReceiptTableView> {
    let model = new CancelBuyerReceiptTableView();
    model.buyerReceiptTableGUID = EmptyGuid;
    return of(model);
  }
}
