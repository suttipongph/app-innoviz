import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelBuyerReceiptTableComponent } from './cancel-buyer-receipt-table.component';

describe('CancelBuyerReceiptTableViewComponent', () => {
  let component: CancelBuyerReceiptTableComponent;
  let fixture: ComponentFixture<CancelBuyerReceiptTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CancelBuyerReceiptTableComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelBuyerReceiptTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
