import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrintBuyerReceiptService } from './print-buyer-receipt.service';
import { PrintBuyerReceiptReportCustomComponent } from './print-buyer-receipt/print-buyer-receipt-report-custom.component';
import { PrintBuyerReceiptReportComponent } from './print-buyer-receipt/print-buyer-receipt-report.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PrintBuyerReceiptReportComponent],
  providers: [PrintBuyerReceiptService, PrintBuyerReceiptReportCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PrintBuyerReceiptReportComponent]
})
export class PrintBuyerReceiptComponentModule {}
