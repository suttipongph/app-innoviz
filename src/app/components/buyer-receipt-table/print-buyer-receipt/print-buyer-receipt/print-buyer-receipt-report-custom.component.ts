import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { PrintBuyerReceiptReportView } from 'shared/models/viewModel';

const firstGroup = ['BUYER_RECEIPT_ID', 'BUYER_RECEIPT_DATE', 'BUYER_ID', 'CUSTOMER_ID', 'BUYER_RECEIPT_AMOUNT', 'CANCEL'];

export class PrintBuyerReceiptReportCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: PrintBuyerReceiptReportView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<PrintBuyerReceiptReportView> {
    let model = new PrintBuyerReceiptReportView();
    model.printBuyerReceiptGUID = EmptyGuid;
    return of(model);
  }
}
