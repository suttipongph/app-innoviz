import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintBuyerReceiptReportComponent } from './print-buyer-receipt-report.component';

describe('PrintBuyerReceiptReportViewComponent', () => {
  let component: PrintBuyerReceiptReportComponent;
  let fixture: ComponentFixture<PrintBuyerReceiptReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrintBuyerReceiptReportComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintBuyerReceiptReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
