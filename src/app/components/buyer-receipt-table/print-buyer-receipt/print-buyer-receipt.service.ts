import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PrintBuyerReceiptReportView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class PrintBuyerReceiptService {
  serviceKey = 'printBuyerReceiptGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getPrintBuyerReceiptToList(search: SearchParameter): Observable<SearchResult<PrintBuyerReceiptReportView>> {
    const url = `${this.servicePath}/GetPrintBuyerReceiptList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deletePrintBuyerReceipt(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeletePrintBuyerReceipt`;
    return this.dataGateway.delete(url, row);
  }
  getPrintBuyerReceiptById(id: string): Observable<PrintBuyerReceiptReportView> {
    const url = `${this.servicePath}/GetPrintBuyerReceiptById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createPrintBuyerReceipt(vmModel: PrintBuyerReceiptReportView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreatePrintBuyerReceipt`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updatePrintBuyerReceipt(vmModel: PrintBuyerReceiptReportView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdatePrintBuyerReceipt`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
