import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BuyerReceiptTableService } from './buyer-receipt-table.service';
import { BuyerReceiptTableListComponent } from './buyer-receipt-table-list/buyer-receipt-table-list.component';
import { BuyerReceiptTableItemComponent } from './buyer-receipt-table-item/buyer-receipt-table-item.component';
import { BuyerReceiptTableItemCustomComponent } from './buyer-receipt-table-item/buyer-receipt-table-item-custom.component';
import { BuyerReceiptTableListCustomComponent } from './buyer-receipt-table-list/buyer-receipt-table-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BuyerReceiptTableListComponent, BuyerReceiptTableItemComponent],
  providers: [BuyerReceiptTableService, BuyerReceiptTableItemCustomComponent, BuyerReceiptTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BuyerReceiptTableListComponent, BuyerReceiptTableItemComponent]
})
export class BuyerReceiptTableComponentModule {}
