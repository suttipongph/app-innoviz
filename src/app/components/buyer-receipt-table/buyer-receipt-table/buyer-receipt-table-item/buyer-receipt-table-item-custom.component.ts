import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConst, EmptyGuid } from 'shared/constants';
import { isNullOrUndefined, isNullOrUndefOrEmpty, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { AccessModeView, BuyerReceiptTableItemView } from 'shared/models/viewModel';
import { BuyerReceiptTableService } from '../buyer-receipt-table.service';

const firstGroup = [
  'BUYER_RECEIPT_ID',
  'BUYER_TABLE_GUID',
  'CUSTOMER_TABLE_GUID',
  'CANCEL',
  'ASSIGNMENT_AGREEMENT_TABLE_GUID',
  'BUYER_AGREEMENT_TABLE_GUID',
  'COMPANY_BANK_ID',
  'REF_TYPE',
  'REF_ID',
  'BUYER_RECEIPT_TABLE_GUID',
  'REF_GUID'
];
const secondGroup = [
  'BUYER_TABLE_GUID',
  'CUSTOMER_TABLE_GUID',
  'CANCEL',
  'ASSIGNMENT_AGREEMENT_TABLE_GUID',
  'BUYER_AGREEMENT_TABLE_GUID',
  'COMPANY_BANK_ID',
  'REF_TYPE',
  'REF_ID',
  'BUYER_RECEIPT_TABLE_GUID',
  'REF_GUID'
];
const RECEIPT_TABLE = 'receipttemptable';
const INQUIRY_PURCHASELINE_OUTSTANDING = 'inquirypurchaselineoutstanding';
const INQUIRY_WITHDRAWALLINE_OUTSTANDING = 'inquirywithdrawallineoutstand';
export class BuyerReceiptTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  isManual: boolean = false;
  isbuyerReceiptId: boolean = true;
  originTableName: string = null;
  originTableId: string = null;
  accessModeView: AccessModeView = new AccessModeView();
  getFieldAccessing(model: BuyerReceiptTableItemView, service: BuyerReceiptTableService): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    return new Observable((observer) => {
      this.validateIsManualNumberSeq(service, model.companyGUID).subscribe((result) => {
        this.isManual = result;
        if (this.isManual) {
          if (model.cancel) {
            fieldAccessing.push({ filedIds: [AppConst.ALL_ID], readonly: true });
          }
          fieldAccessing.push({ filedIds: secondGroup, readonly: true });
          observer.next(fieldAccessing);
        } else {
          if (model.cancel) {
            fieldAccessing.push({ filedIds: [AppConst.ALL_ID], readonly: true });
          }
          this.isbuyerReceiptId = false;
          fieldAccessing.push({ filedIds: firstGroup, readonly: true });
          observer.next(fieldAccessing);
        }
      });
    });
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }

  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BuyerReceiptTableItemView): Observable<boolean> {
    return this.setAccessModeByParent(model).pipe(
      map((result) => (isUpdateMode(model.buyerReceiptTableGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  setPassingObject(model: BuyerReceiptTableItemView): void {
    this.originTableName = this.baseService.uiService.getRelatedInfoOriginTableName();
    this.originTableId = this.baseService.uiService.getRelatedInfoParentTableKey();
    if (!isNullOrUndefined(model) && !isNullOrUndefOrEmpty(model.accessModeView)) {
      this.accessModeView = model.accessModeView;
    }
  }
  setAccessModeByParent(model: BuyerReceiptTableItemView): Observable<AccessModeView> {
    switch (this.originTableName) {
      case RECEIPT_TABLE:
        this.accessModeView.canCreate = false;
        break;
      default:
        this.accessModeView.canCreate = this.accessModeView.canCreate && !model.cancel;
        this.accessModeView.canView = this.accessModeView.canView && !model.cancel;
        break;
    }
    return of(this.accessModeView);
  }
  getInitialData(): Observable<BuyerReceiptTableItemView> {
    switch (this.originTableName) {
      case INQUIRY_PURCHASELINE_OUTSTANDING:
        return this.baseService.service.getInitialPurchaseLineData(this.originTableId);
      case INQUIRY_WITHDRAWALLINE_OUTSTANDING:
        return this.baseService.service.getInitialWithdrawalLineData(this.originTableId);
      default:
        return of();
    }
  }
  validateIsManualNumberSeq(service: BuyerReceiptTableService, id: string): Observable<boolean> {
    return service.validateIsManualNumberSeq(id);
  }
}
