import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyerReceiptTableItemComponent } from './buyer-receipt-table-item.component';

describe('BuyerReceiptTableItemViewComponent', () => {
  let component: BuyerReceiptTableItemComponent;
  let fixture: ComponentFixture<BuyerReceiptTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BuyerReceiptTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerReceiptTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
