import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, ROUTE_REPORT_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { BuyerReceiptTableItemView } from 'shared/models/viewModel';
import { BuyerReceiptTableService } from '../buyer-receipt-table.service';
import { BuyerReceiptTableItemCustomComponent } from './buyer-receipt-table-item-custom.component';
@Component({
  selector: 'buyer-receipt-table-item',
  templateUrl: './buyer-receipt-table-item.component.html',
  styleUrls: ['./buyer-receipt-table-item.component.scss']
})
export class BuyerReceiptTableItemComponent extends BaseItemComponent<BuyerReceiptTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  methodOfPaymentOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  constructor(
    private service: BuyerReceiptTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: BuyerReceiptTableItemCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setPassingObject(passingObj);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<BuyerReceiptTableItemView> {
    return this.service.getBuyerReceiptTableById(this.id);
  }
  getInitialData(): Observable<BuyerReceiptTableItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getMethodOfPaymentDropDown()).subscribe(
      ([methodOfPayment]) => {
        this.methodOfPaymentOptions = methodOfPayment;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model, this.service).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  setFunctionOptions(model: BuyerReceiptTableItemView): void {
    this.functionItems = [
      {
        label: 'LABEL.CANCEL',
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.CANCEL_BUYER_RECEIPT_TABLE }),
        disabled: model.cancel
      },
      {
        label: 'LABEL.PRINT',
        items: [
          {
            label: 'LABEL.BUYER_RECEIPT',
            command: () =>
              this.toReport({
                path: ROUTE_REPORT_GEN.PRINT_BUYER_RECEIPT
              }),
            disabled: model.cancel
          }
        ]
      }
    ];
    super.setFunctionOptionsMapping();
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateBuyerReceiptTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createBuyerReceiptTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
