import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BuyerReceiptTableItemView, BuyerReceiptTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class BuyerReceiptTableService {
  serviceKey = 'buyerReceiptTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBuyerReceiptTableToList(search: SearchParameter): Observable<SearchResult<BuyerReceiptTableListView>> {
    const url = `${this.servicePath}/GetBuyerReceiptTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBuyerReceiptTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBuyerReceiptTable`;
    return this.dataGateway.delete(url, row);
  }
  getBuyerReceiptTableById(id: string): Observable<BuyerReceiptTableItemView> {
    const url = `${this.servicePath}/GetBuyerReceiptTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBuyerReceiptTable(vmModel: BuyerReceiptTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBuyerReceiptTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBuyerReceiptTable(vmModel: BuyerReceiptTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBuyerReceiptTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  validateIsManualNumberSeq(companyId: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeq/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
  getInitialPurchaseLineData(id: string): Observable<any> {
    const url = `${this.servicePath}/GetBuyerReceiptInitialPurchaseLineData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getInitialWithdrawalLineData(id: string): Observable<any> {
    const url = `${this.servicePath}/GetBuyerReceiptInitialWithdrawalLineData/id=${id}`;
    return this.dataGateway.get(url);
  }
}
