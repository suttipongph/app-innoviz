import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { interval, Observable } from 'rxjs';
import { ColumnType, RefType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { BuyerReceiptTableListView } from 'shared/models/viewModel';
import { BuyerReceiptTableService } from '../buyer-receipt-table.service';
import { BuyerReceiptTableListCustomComponent } from './buyer-receipt-table-list-custom.component';
const RECEIPT_TABLE = 'receipttemptable';
@Component({
  selector: 'buyer-receipt-table-list',
  templateUrl: './buyer-receipt-table-list.component.html',
  styleUrls: ['./buyer-receipt-table-list.component.scss']
})
export class BuyerReceiptTableListComponent extends BaseListComponent<BuyerReceiptTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  methodOfPaymentOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  defualtBitOption: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];

  constructor(public custom: BuyerReceiptTableListCustomComponent, private service: BuyerReceiptTableService, public canActivated: ActivatedRoute) {
    super();
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.custom.setInitialListData(this.getPassingObject(this.canActivated)).subscribe((result) => {
      this.setDataGridOption();
    });
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.baseDropdown.getMethodOfPaymentDropDown(this.methodOfPaymentOptions);
    this.baseDropdown.getDefaultBitDropDown(this.defualtBitOption);
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
    this.baseDropdown.getCustomerTableByDropDown(this.customerTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'buyerReceiptTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.BUYER_RECEIPT_ID',
        textKey: 'buyerReceiptId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.BUYER_RECEIPT_DATE',
        textKey: 'buyerReceiptDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerTableOptions,
        searchingKey: 'buyerTableGUID',
        sortingKey: 'buyerTable_BuyerId'
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId'
      },
      {
        label: 'LABEL.BUYER_RECEIPT_AMOUNT',
        textKey: 'buyerReceiptAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CANCEL',
        textKey: 'cancel',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defualtBitOption
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.custom.setFilterCodition(columns);
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<BuyerReceiptTableListView>> {
    return this.service.getBuyerReceiptTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteBuyerReceiptTable(row));
  }
}
