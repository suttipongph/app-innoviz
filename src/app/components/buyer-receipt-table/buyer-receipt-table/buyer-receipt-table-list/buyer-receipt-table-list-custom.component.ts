import { Observable, of } from 'rxjs';
import { ColumnType, RefType, SortType } from 'shared/constants';
import { isNullOrUndefined, isNullOrUndefOrEmpty } from 'shared/functions/value.function';
import { BaseServiceModel, ColumnModel } from 'shared/models/systemModel';
import { AccessModeView, BuyerReceiptTableListView } from 'shared/models/viewModel';
const RECEIPT_TEMP_TABLE = 'receipttemptable';
const COLLECTION_FOLLOW_UP = 'collectionfollowup';
export class BuyerReceiptTableListCustomComponent {
  baseService: BaseServiceModel<any>;
  originTableName: string = null;
  accessModeView: AccessModeView = new AccessModeView();
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setInitialListData(model: BuyerReceiptTableListView): Observable<AccessModeView> {
    if (!isNullOrUndefOrEmpty(model) && !isNullOrUndefOrEmpty(model.accessModeView)) {
      this.accessModeView = model.accessModeView;
    }
    this.originTableName = this.baseService.uiService.getRelatedInfoOriginTableName();
    switch (this.originTableName) {
      case RECEIPT_TEMP_TABLE:
        this.accessModeView.canCreate = false;
        break;
      default:
        break;
    }
    return of(this.accessModeView);
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  setFilterCodition(columns: ColumnModel[]): any {
    let parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (parentName) {
      case COLLECTION_FOLLOW_UP:
        const filterbyrefType = {
          label: null,
          textKey: 'refType',
          type: ColumnType.INT,
          visibility: true,
          sorting: SortType.NONE,
          parentKey: RefType.CollectionFollowUp.toString()
        };
        return columns.push(filterbyrefType);
      default:
        return columns;
    }
  }
}
