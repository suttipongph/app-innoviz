import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BuyerReceiptTableListComponent } from './buyer-receipt-table-list.component';

describe('BuyerReceiptTableListViewComponent', () => {
  let component: BuyerReceiptTableListComponent;
  let fixture: ComponentFixture<BuyerReceiptTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BuyerReceiptTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerReceiptTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
