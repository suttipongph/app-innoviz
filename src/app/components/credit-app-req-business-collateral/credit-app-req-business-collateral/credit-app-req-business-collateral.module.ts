import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreditAppReqBusinessCollateralService } from './credit-app-req-business-collateral.service';
import { CreditAppReqBusinessCollateralListComponent } from './credit-app-req-business-collateral-list/credit-app-req-business-collateral-list.component';
import { CreditAppReqBusinessCollateralItemComponent } from './credit-app-req-business-collateral-item/credit-app-req-business-collateral-item.component';
import { CreditAppReqBusinessCollateralItemCustomComponent } from './credit-app-req-business-collateral-item/credit-app-req-business-collateral-item-custom.component';
import { CreditAppReqBusinessCollateralListCustomComponent } from './credit-app-req-business-collateral-list/credit-app-req-business-collateral-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CreditAppReqBusinessCollateralListComponent, CreditAppReqBusinessCollateralItemComponent],
  providers: [
    CreditAppReqBusinessCollateralService,
    CreditAppReqBusinessCollateralItemCustomComponent,
    CreditAppReqBusinessCollateralListCustomComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CreditAppReqBusinessCollateralListComponent, CreditAppReqBusinessCollateralItemComponent]
})
export class CreditAppReqBusinessCollateralComponentModule {}
