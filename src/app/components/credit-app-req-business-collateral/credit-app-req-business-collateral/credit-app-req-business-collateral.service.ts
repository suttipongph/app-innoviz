import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, CreditAppReqBusinessCollateralItemView, CreditAppReqBusinessCollateralListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CreditAppReqBusinessCollateralService {
  serviceKey = 'creditAppReqBusinessCollateralGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCreditAppReqBusinessCollateralToList(search: SearchParameter): Observable<SearchResult<CreditAppReqBusinessCollateralListView>> {
    const url = `${this.servicePath}/GetCreditAppReqBusinessCollateralList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCreditAppReqBusinessCollateral(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCreditAppReqBusinessCollateral`;
    return this.dataGateway.delete(url, row);
  }
  getCreditAppReqBusinessCollateralById(id: string): Observable<CreditAppReqBusinessCollateralItemView> {
    const url = `${this.servicePath}/GetCreditAppReqBusinessCollateralById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCreditAppReqBusinessCollateral(vmModel: CreditAppReqBusinessCollateralItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCreditAppReqBusinessCollateral`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCreditAppReqBusinessCollateral(vmModel: CreditAppReqBusinessCollateralItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCreditAppReqBusinessCollateral`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getCreditAppReqBusColateralInitialData(id: string): Observable<CreditAppReqBusinessCollateralItemView> {
    const url = `${this.servicePath}/GetCreditAppReqBusColateralInitialData/creditAppRequestTableGUID=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByCreditAppRequestTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppRequestTable/id=${id}`;
    return this.dataGateway.get(url);
  }
}
