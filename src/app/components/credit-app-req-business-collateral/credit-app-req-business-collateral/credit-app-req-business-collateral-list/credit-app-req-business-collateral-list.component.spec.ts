import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditAppReqBusinessCollateralListComponent } from './credit-app-req-business-collateral-list.component';

describe('CreditAppReqBusinessCollateralListViewComponent', () => {
  let component: CreditAppReqBusinessCollateralListComponent;
  let fixture: ComponentFixture<CreditAppReqBusinessCollateralListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditAppReqBusinessCollateralListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppReqBusinessCollateralListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
