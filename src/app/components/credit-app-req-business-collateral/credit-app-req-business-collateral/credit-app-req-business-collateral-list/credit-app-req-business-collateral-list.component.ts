import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CreditAppReqBusinessCollateralListView } from 'shared/models/viewModel';
import { CreditAppReqBusinessCollateralService } from '../credit-app-req-business-collateral.service';
import { CreditAppReqBusinessCollateralListCustomComponent } from './credit-app-req-business-collateral-list-custom.component';

@Component({
  selector: 'credit-app-req-business-collateral-list',
  templateUrl: './credit-app-req-business-collateral-list.component.html',
  styleUrls: ['./credit-app-req-business-collateral-list.component.scss']
})
export class CreditAppReqBusinessCollateralListComponent
  extends BaseListComponent<CreditAppReqBusinessCollateralListView>
  implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  bankGroupOptions: SelectItems[] = [];
  bankTypeOptions: SelectItems[] = [];
  businessCollateralSubTypeOptions: SelectItems[] = [];
  businessCollateralTypeOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  custBusinessCollateralOptions: SelectItems[] = [];
  customerTableOption: SelectItems[] = [];
  creditAppRequestTableOption: SelectItems[] = [];
  defaultBitOption: SelectItems[] = [];
  constructor(
    public custom: CreditAppReqBusinessCollateralListCustomComponent,
    private service: CreditAppReqBusinessCollateralService,
    public currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.parentId = this.custom.getRelatedInfoParentTableKey(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.setAccessModeByParentStatus(this.isWorkFlowMode()).subscribe((result) => {
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getDefaultBitDropDown(this.defaultBitOption);
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBusinessCollateralSubTypeDropDown(this.businessCollateralSubTypeOptions);
    this.baseDropdown.getBusinessCollateralTypeDropDown(this.businessCollateralTypeOptions);
    this.baseDropdown.getCustBusinessCollateralDropDown(this.custBusinessCollateralOptions);
    this.baseDropdown.getCustomerTableByDropDown(this.customerTableOption);
    this.baseDropdown.getCreditAppRequestTableDropDown(this.creditAppRequestTableOption);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'creditAppReqBusinessCollateralGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.IS_NEW',
        textKey: 'isNew',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defaultBitOption
      },
      {
        label: 'LABEL.CUSTOMER_BUSINESS_COLLATERAL_ID',
        textKey: 'custBusinessCollateral_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.custBusinessCollateralOptions,
        searchingKey: 'custBusinessCollateralGUID',
        sortingKey: 'custBusinessCollateral_CustBusinessCollateralId'
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOption,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId'
      },
      {
        label: 'LABEL.CREDIT_APPLICATION_REQUEST_ID',
        textKey: 'creditAppRequestTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditAppRequestTableOption,
        searchingKey: 'creditAppRequestTableGUID',
        sortingKey: 'creditAppRequestTable_CreditAppRequestId'
      },
      {
        label: 'LABEL.BUSINESS_COLLATERAL_TYPE_ID',
        textKey: 'businessCollateralType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.businessCollateralTypeOptions,
        searchingKey: 'businessCollateralTypeGUID',
        sortingKey: 'businessCollateralType_BusinessCollateralTypeId'
      },
      {
        label: 'LABEL.BUSINESS_COLLATERAL_SUBTYPE_ID',
        textKey: 'businessCollateralSubType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.businessCollateralSubTypeOptions,
        searchingKey: 'businessCollateralSubTypeGUID',
        sortingKey: 'businessCollateralSubType_BusinessCollateralSubTypeId'
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.REFERENCE_AGREEMENT_ID',
        textKey: 'refAgreementId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.REFERENCE_AGREEMENT_DATE',
        textKey: 'refAgreementDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BUSINESS_COLLATERAL_VALUE',
        textKey: 'businessCollateralValue',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: 'LABEL.CAPITAL_VALUATION',
        textKey: 'capitalValuation',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: null,
        textKey: 'CreatedDateTime',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: null,
        textKey: 'creditAppRequestTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CreditAppReqBusinessCollateralListView>> {
    return this.service.getCreditAppReqBusinessCollateralToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCreditAppReqBusinessCollateral(row));
  }
}
