import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CreditAppRequestStatus } from 'shared/constants';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView, CreditAppReqBusinessCollateralListView } from 'shared/models/viewModel';
import { CreditAppReqBusinessCollateralService } from '../credit-app-req-business-collateral.service';
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const AMEND_CA = 'amendca';
const AMEND_CA_LINE = 'amendcaline';
const BUYER_MATCHING = 'buyermatching';
const LOAN_REQUEST = 'loanrequest';

export class CreditAppReqBusinessCollateralListCustomComponent {
  baseService: BaseServiceModel<any>;
  accessModeView: AccessModeView = new AccessModeView();
  parentName: string;
  parentId: string;
  isBuyerMatching: boolean = false;
  isLoanRequest: boolean = false;

  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setAccessModeByParentStatus(isWorkFlowMode: boolean): Observable<AccessModeView> {
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        return this.getAccessModeByCreditAppRequestTable(this.parentId, isWorkFlowMode);
      case AMEND_CA:
        return this.getAccessModeByCreditAppRequestTable(this.parentId, isWorkFlowMode);
      case AMEND_CA_LINE:
        return this.getAccessModeByCreditAppRequestTable(this.parentId, isWorkFlowMode);
      case BUYER_MATCHING:
      case LOAN_REQUEST:
        this.checkIsBuyerMacthingOrLoanRequest();
        return this.getAccessModeByCreditAppRequestTable(this.parentId, isWorkFlowMode);
      default:
        return of(this.accessModeView);
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
  getRelatedInfoParentTableKey(passingObj: CreditAppReqBusinessCollateralListView): string {
    let parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        this.parentId = parentId;
        break;
      case AMEND_CA:
        this.parentId = passingObj.creditAppRequestTableGUID;
        break;
      case AMEND_CA_LINE:
        this.parentId = passingObj.creditAppRequestTableGUID;
        break;
      case BUYER_MATCHING:
      case LOAN_REQUEST:
        this.parentId = passingObj.creditAppRequestTableGUID;
        break;
      default:
        this.parentId = null;
        break;
    }
    return this.parentId;
  }
  getAccessModeByCreditAppRequestTable(parentId: string, isWorkFlowMode: boolean): Observable<AccessModeView> {
    return (this.baseService.service as CreditAppReqBusinessCollateralService).getAccessModeByCreditAppRequestTable(parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
        const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
        this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
        this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
        this.accessModeView.canView = true;
      })
    );
  }
  checkIsBuyerMacthingOrLoanRequest(): void {
    this.isBuyerMatching = window.location.href.match(BUYER_MATCHING) ? true : false;
    this.isLoanRequest = window.location.href.match(LOAN_REQUEST) ? true : false;
    if (this.isBuyerMatching) {
      this.parentId = this.baseService.uiService.getKey(BUYER_MATCHING);
    } else if (this.isLoanRequest) {
      this.parentId = this.baseService.uiService.getKey(LOAN_REQUEST);
    }
  }
}
