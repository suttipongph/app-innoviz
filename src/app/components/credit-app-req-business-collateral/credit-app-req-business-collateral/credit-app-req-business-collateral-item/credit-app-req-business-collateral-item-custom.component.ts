import { Confirmation } from 'primeng/api';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConst, CreditAppRequestStatus, EmptyGuid, IdentificationType } from 'shared/constants';
import { isNullOrUndefOrEmptyGUID, isUpdateMode, resetModelToDefualt } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, BuyerTableItemView, CreditAppReqBusinessCollateralItemView, CustBusinessCollateralItemView } from 'shared/models/viewModel';

const firstGroup = ['CUSTOMER_TABLE_GUID', 'CREDIT_APP_REQUEST_TABLE_GUID'];
const secondGroup = ['CUST_BUSINESS_COLLATERAL_GUID'];
const thirdGroup = ['IS_NEW'];
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const AMEND_CA = 'amendca';
const AMEND_CA_LINE = 'amendcaline';
const BUYER_MATCHING = 'buyermatching';
const LOAN_REQUEST = 'loanrequest';
const CREDITAPP_TABLE = 'creditapptable';
export class CreditAppReqBusinessCollateralItemCustomComponent {
  baseService: BaseServiceModel<any>;
  isNewConfirmChange: boolean = true;
  parentName: string;
  parentId: string;
  isBuyerMatching: boolean = false;
  isLoanRequest: boolean = false;

  accessModeView: AccessModeView = new AccessModeView();
  getFieldAccessing(model: CreditAppReqBusinessCollateralItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (model.isNew) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      fieldAccessing.push({ filedIds: secondGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: [AppConst.ALL_ID], readonly: true });
      fieldAccessing.push({ filedIds: secondGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(
    canCreate: boolean,
    canUpdate: boolean,
    model: CreditAppReqBusinessCollateralItemView,
    isWorkFlowMode: boolean
  ): Observable<boolean> {
    return this.setAccessModeByParentStatus(isWorkFlowMode).pipe(
      map((result) => {
        return isUpdateMode(model.creditAppReqBusinessCollateralGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate);
      })
    );
  }
  setAccessModeByParentStatus(isWorkFlowMode: boolean): Observable<AccessModeView> {
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        return this.getAccessModeByCreditAppRequestTable(this.parentId, isWorkFlowMode);
      case AMEND_CA:
        return this.getAccessModeByCreditAppRequestTable(this.parentId, isWorkFlowMode);
      case AMEND_CA_LINE:
        return this.getAccessModeByCreditAppRequestTable(this.parentId, isWorkFlowMode);
      case BUYER_MATCHING:
      case LOAN_REQUEST:
        this.checkIsBuyerMacthingOrLoanRequest();
        return this.getAccessModeByCreditAppRequestTable(this.parentId, isWorkFlowMode);
      default:
        return of(this.accessModeView);
    }
  }
  getAccessModeByCreditAppRequestTable(parentId: string, isWorkFlowMode: boolean): Observable<AccessModeView> {
    return this.baseService.service.getAccessModeByCreditAppRequestTable(parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
        const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
        this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
        this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
        this.accessModeView.canView = isDraftStatus ? true : this.accessModeView.canView && isWorkFlowMode;
      })
    );
  }
  getRelatedInfoParentTableKey(passingObj: CreditAppReqBusinessCollateralItemView): string {
    let parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        this.parentId = parentId;
        break;
      case BUYER_MATCHING:
      case LOAN_REQUEST:
        this.checkIsBuyerMacthingOrLoanRequest();
        break;
      case AMEND_CA:
        this.parentId = passingObj.creditAppRequestTableGUID;
        break;
      case AMEND_CA_LINE:
        this.parentId = passingObj.creditAppRequestTableGUID;
        break;
      default:
        this.parentId = null;
        break;
    }

    return this.parentId;
  }
  onIsNewChange(model: CreditAppReqBusinessCollateralItemView): Observable<any> {
    const confirmation: Confirmation = {
      header: this.baseService.translate.instant('CONFIRM.CONFIRM'),
      message: this.baseService.translate.instant('CONFIRM.90007', [this.baseService.translate.instant('LABEL.CUSTOMER_BUSINESS_COLLATERAL')])
    };
    return new Observable((observer) => {
      this.baseService.notificationService.showConfirmDialog(confirmation);
      this.baseService.notificationService.isAccept.subscribe((isConfirm) => {
        this.isNewConfirmChange = isConfirm;
        if (isConfirm) {
          resetModelToDefualt(model, this.baseService.dataGateway.types, [
            'customerTable_Values',
            'customerTableGUID',
            'creditAppRequestTable_Values',
            'creditAppRequestTableGUID'
          ]);
        } else {
          model.isNew = !model.isNew;
        }
        this.baseService.notificationService.isAccept.observers = [];
        observer.next();
      });
    });
  }
  onCustBusinessCollateralChange(model: CreditAppReqBusinessCollateralItemView, custBusinessCollateralOptions: SelectItems[]): void {
    const rowData = isNullOrUndefOrEmptyGUID(model.custBusinessCollateralGUID)
      ? new CustBusinessCollateralItemView()
      : (custBusinessCollateralOptions.find((f) => f.value === model.custBusinessCollateralGUID).rowData as CustBusinessCollateralItemView);
    this.setValueByCustBusCollateral(model, rowData);
  }
  onBusinessCollateralTypeChange(model: CreditAppReqBusinessCollateralItemView): void {
    this.setValueByBusinessCollateralType(model);
  }
  onBuyerTableChange(model: CreditAppReqBusinessCollateralItemView, buyerTableOptions: SelectItems[]): void {
    const rowData = isNullOrUndefOrEmptyGUID(model.buyerTableGUID)
      ? new BuyerTableItemView()
      : (buyerTableOptions.find((f) => f.value === model.buyerTableGUID).rowData as BuyerTableItemView);

    this.setValueByBuyerTable(model, rowData);
  }
  setValueByBuyerTable(model: CreditAppReqBusinessCollateralItemView, buyer: BuyerTableItemView): void {
    model.buyerName = buyer.buyerName;
    model.buyerTaxIdentificationId = buyer.identificationType === IdentificationType.TaxId ? buyer.taxId : buyer.passportId;
  }
  setValueByBusinessCollateralType(model: CreditAppReqBusinessCollateralItemView): void {
    model.businessCollateralSubTypeGUID = null;
  }
  setValueByCustBusCollateral(model: CreditAppReqBusinessCollateralItemView, custBusinessCollateral: CustBusinessCollateralItemView): void {
    model.businessCollateralTypeGUID = custBusinessCollateral.businessCollateralTypeGUID;
    model.businessCollateralSubTypeGUID = custBusinessCollateral.businessCollateralSubTypeGUID;
    model.description = custBusinessCollateral.description;
    model.buyerTableGUID = custBusinessCollateral.buyerTableGUID;
    model.buyerName = custBusinessCollateral.buyerName;
    model.bankGroupGUID = custBusinessCollateral.bankGroupGUID;
    model.bankTypeGUID = custBusinessCollateral.bankTypeGUID;
    model.accountNumber = custBusinessCollateral.accountNumber;
    model.refAgreementId = custBusinessCollateral.refAgreementId;
    model.refAgreementDate = custBusinessCollateral.refAgreementDate;
    model.preferentialCreditorNumber = custBusinessCollateral.preferentialCreditorNumber;
    model.lessee = custBusinessCollateral.lessee;
    model.lessor = custBusinessCollateral.lessor;
    model.registrationPlateNumber = custBusinessCollateral.registrationPlateNumber;
    model.machineNumber = custBusinessCollateral.machineNumber;
    model.machineRegisteredStatus = custBusinessCollateral.machineRegisteredStatus;
    model.chassisNumber = custBusinessCollateral.chassisNumber;
    model.registeredPlace = custBusinessCollateral.registeredPlace;
    model.guaranteeAmount = custBusinessCollateral.guaranteeAmount;
    model.businessCollateralValue = custBusinessCollateral.businessCollateralValue;
    model.quantity = custBusinessCollateral.quantity;
    model.unit = custBusinessCollateral.unit;
    model.projectName = custBusinessCollateral.projectName;
    model.titleDeedNumber = custBusinessCollateral.titleDeedNumber;
    model.titleDeedSubDistrict = custBusinessCollateral.titleDeedSubDistrict;
    model.titleDeedDistrict = custBusinessCollateral.titleDeedDistrict;
    model.titleDeedProvince = custBusinessCollateral.titleDeedProvince;
    model.dbdRegistrationId = custBusinessCollateral.dbdRegistrationId;
    model.dbdRegistrationDate = custBusinessCollateral.dbdRegistrationDate;
    model.dbdRegistrationAmount = custBusinessCollateral.dbdRegistrationAmount;
    model.dbdRegistrationDescription = custBusinessCollateral.dbdRegistrationDescription;
    model.capitalValuation = custBusinessCollateral.capitalValuation;
    model.valuationCommittee = custBusinessCollateral.valuationCommittee;
    model.dateOfValuation = custBusinessCollateral.dateOfValuation;
    model.ownership = custBusinessCollateral.ownership;
    model.buyerTaxIdentificationId = custBusinessCollateral.buyerTaxIdentificationId;
  }
  setAccessingByIsNew(model: CreditAppReqBusinessCollateralItemView): FieldAccessing[] {
    const fieldAccessing: FieldAccessing[] = [];
    if (!this.isNewConfirmChange) {
      return;
    }
    model.isNew
      ? fieldAccessing.push(
          ...[
            { filedIds: [AppConst.ALL_ID], readonly: false },
            { filedIds: firstGroup, readonly: true },
            { filedIds: secondGroup, readonly: true }
          ]
        )
      : fieldAccessing.push(
          ...[
            { filedIds: [AppConst.ALL_ID], readonly: true },
            { filedIds: secondGroup, readonly: false }
          ]
        );
    fieldAccessing.push({ filedIds: thirdGroup, readonly: false });
    return fieldAccessing;
  }

  checkIsBuyerMacthingOrLoanRequest(): void {
    this.isBuyerMatching = window.location.href.match(BUYER_MATCHING) ? true : false;
    this.isLoanRequest = window.location.href.match(LOAN_REQUEST) ? true : false;
    if (this.isBuyerMatching) {
      this.parentId = this.baseService.uiService.getKey(BUYER_MATCHING);
    } else if (this.isLoanRequest) {
      this.parentId = this.baseService.uiService.getKey(LOAN_REQUEST);
    }
  }
}
