import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditAppReqBusinessCollateralItemComponent } from './credit-app-req-business-collateral-item.component';

describe('CreditAppReqBusinessCollateralItemViewComponent', () => {
  let component: CreditAppReqBusinessCollateralItemComponent;
  let fixture: ComponentFixture<CreditAppReqBusinessCollateralItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditAppReqBusinessCollateralItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppReqBusinessCollateralItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
