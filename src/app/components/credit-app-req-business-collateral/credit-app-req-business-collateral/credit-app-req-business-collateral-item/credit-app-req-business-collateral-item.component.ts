import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { CreditAppReqBusinessCollateralItemView } from 'shared/models/viewModel/creditAppReqBusinessCollateralItemView';
import { CreditAppReqBusinessCollateralService } from '../credit-app-req-business-collateral.service';
import { CreditAppReqBusinessCollateralItemCustomComponent } from './credit-app-req-business-collateral-item-custom.component';
@Component({
  selector: 'credit-app-req-business-collateral-item',
  templateUrl: './credit-app-req-business-collateral-item.component.html',
  styleUrls: ['./credit-app-req-business-collateral-item.component.scss']
})
export class CreditAppReqBusinessCollateralItemComponent
  extends BaseItemComponent<CreditAppReqBusinessCollateralItemView>
  implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  bankGroupOptions: SelectItems[] = [];
  bankTypeOptions: SelectItems[] = [];
  businessCollateralSubTypeOptions: SelectItems[] = [];
  businessCollateralTypeOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  custBusinessCollateralOptions: SelectItems[] = [];

  constructor(
    private service: CreditAppReqBusinessCollateralService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CreditAppReqBusinessCollateralItemCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.parentId = this.custom.getRelatedInfoParentTableKey(passingObj);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<CreditAppReqBusinessCollateralItemView> {
    return this.service.getCreditAppReqBusinessCollateralById(this.id);
  }
  getInitialData(): Observable<CreditAppReqBusinessCollateralItemView> {
    return this.service.getCreditAppReqBusColateralInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: CreditAppReqBusinessCollateralItemView): void {
    const customerTableGUID = isNullOrUndefined(model) ? this.model.customerTableGUID : model.customerTableGUID;
    forkJoin(
      this.baseDropdown.getBankGroupDropDown(),
      this.baseDropdown.getBankTypeDropDown(),
      this.baseDropdown.getBusinessCollateralTypeDropDown(),
      this.baseDropdown.getBuyerTableDropDown(),
      this.baseDropdown.getNotCancelCustBusinessCollateralByCustomerDropDown(customerTableGUID)
    ).subscribe(
      ([bankGroup, bankType, businessCollateralType, buyerTable, custBusinessCollateral]) => {
        this.bankGroupOptions = bankGroup;
        this.bankTypeOptions = bankType;
        this.businessCollateralTypeOptions = businessCollateralType;
        this.buyerTableOptions = buyerTable;
        this.custBusinessCollateralOptions = custBusinessCollateral;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model, this.isWorkFlowMode()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateCreditAppReqBusinessCollateral(this.model), isColsing);
    } else {
      super.onCreate(this.service.createCreditAppReqBusinessCollateral(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onBusinessCollateralTypeChange(): void {
    this.custom.onBusinessCollateralTypeChange(this.model);
    this.onBusinessCollateralTypeBinding();
  }
  onBusinessCollateralTypeBinding(): void {
    this.getBusinessCollateralSubTypeByBusinessCollateralSubTypDropDown();
  }
  onIsNewChange(): void {
    this.custom.onIsNewChange(this.model).subscribe(() => {
      this.onIsNewBinding();
    });
  }
  onIsNewBinding(): void {
    super.setBaseFieldAccessing(this.custom.setAccessingByIsNew(this.model));
  }
  onCustBusinessCollateralChange(): void {
    this.custom.onCustBusinessCollateralChange(this.model, this.custBusinessCollateralOptions);
    this.onBusinessCollateralTypeBinding();
  }
  onBuyerTableChange(): void {
    this.custom.onBuyerTableChange(this.model, this.buyerTableOptions);
  }
  getBusinessCollateralSubTypeByBusinessCollateralSubTypDropDown(): void {
    if (!isNullOrUndefOrEmptyGUID(this.model.businessCollateralTypeGUID)) {
      this.baseService.baseDropdown
        .getBusinessCollateralSubTypeByBusinessCollateralTypDropDown(this.model.businessCollateralTypeGUID)
        .subscribe((result) => {
          this.businessCollateralSubTypeOptions = result;
        });
    } else {
      this.businessCollateralSubTypeOptions.length = 0;
    }
  }
}
