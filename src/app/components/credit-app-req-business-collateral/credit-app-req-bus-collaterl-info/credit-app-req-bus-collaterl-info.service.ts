import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CreditAppReqBusCollaterlInfoItemView, CreditAppReqBusCollaterlInfoListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CreditAppReqBusCollaterlInfoService {
  serviceKey = 'creditAppReqBusCollaterlInfoGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCreditAppReqBusCollaterlInfoToList(search: SearchParameter): Observable<SearchResult<CreditAppReqBusCollaterlInfoListView>> {
    const url = `${this.servicePath}/GetCreditAppReqBusCollaterlInfoList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCreditAppReqBusCollaterlInfo(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCreditAppReqBusCollaterlInfo`;
    return this.dataGateway.delete(url, row);
  }
  getCreditAppReqBusCollaterlInfoById(id: string): Observable<CreditAppReqBusCollaterlInfoItemView> {
    const url = `${this.servicePath}/GetCreditAppReqBusCollaterlInfoById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCreditAppReqBusCollaterlInfo(vmModel: CreditAppReqBusCollaterlInfoItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCreditAppReqBusCollaterlInfo`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCreditAppReqBusCollaterlInfo(vmModel: CreditAppReqBusCollaterlInfoItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCreditAppReqBusCollaterlInfo`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
