import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreditAppReqBusCollaterlInfoService } from './credit-app-req-bus-collaterl-info.service';
import { CreditAppReqBusCollaterlInfoItemComponent } from './credit-app-req-bus-collaterl-info-item/credit-app-req-bus-collaterl-info-item.component';
import { CreditAppReqBusCollaterlInfoItemCustomComponent } from './credit-app-req-bus-collaterl-info-item/credit-app-req-bus-collaterl-info-item-custom.component';
import { CustBusinessCollateralComponentModule } from 'components/cust-business-collateral/cust-business-collateral.module';
import { CreditAppReqBusinessCollateralComponentModule } from '../credit-app-req-business-collateral/credit-app-req-business-collateral.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    CustBusinessCollateralComponentModule,
    CreditAppReqBusinessCollateralComponentModule
  ],
  declarations: [CreditAppReqBusCollaterlInfoItemComponent],
  providers: [CreditAppReqBusCollaterlInfoService, CreditAppReqBusCollaterlInfoItemCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CreditAppReqBusCollaterlInfoItemComponent]
})
export class CreditAppReqBusCollaterlInfoComponentModule {}
