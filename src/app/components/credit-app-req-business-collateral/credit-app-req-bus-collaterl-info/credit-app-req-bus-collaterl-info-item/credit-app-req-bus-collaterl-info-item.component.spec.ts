import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditAppReqBusCollaterlInfoItemComponent } from './credit-app-req-bus-collaterl-info-item.component';

describe('CreditAppReqBusCollaterlInfoItemViewComponent', () => {
  let component: CreditAppReqBusCollaterlInfoItemComponent;
  let fixture: ComponentFixture<CreditAppReqBusCollaterlInfoItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditAppReqBusCollaterlInfoItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppReqBusCollaterlInfoItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
