import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CreditAppReqBusCollaterlInfoItemView } from 'shared/models/viewModel';

const firstGroup = ['CREDIT_APPLICATION_REQUEST_ID'];

export class CreditAppReqBusCollaterlInfoItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CreditAppReqBusCollaterlInfoItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CreditAppReqBusCollaterlInfoItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(passingObj: CreditAppReqBusCollaterlInfoItemView): Observable<CreditAppReqBusCollaterlInfoItemView> {
    const model = new CreditAppReqBusCollaterlInfoItemView();
    model.creditAppReqBusCollaterlInfoGUID = EmptyGuid;
    model.creditAppRequestTable_Values = passingObj.creditAppRequestTable_Values;
    model.customerTable_Values = passingObj.customerTable_Values;
    return of(model);
  }
}
