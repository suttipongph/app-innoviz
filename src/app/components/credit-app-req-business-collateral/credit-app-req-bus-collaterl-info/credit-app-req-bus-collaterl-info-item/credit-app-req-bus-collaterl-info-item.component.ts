import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel } from 'shared/models/systemModel';
import { CreditAppReqBusCollaterlInfoItemView } from 'shared/models/viewModel';
import { CreditAppReqBusCollaterlInfoService } from '../credit-app-req-bus-collaterl-info.service';
import { CreditAppReqBusCollaterlInfoItemCustomComponent } from './credit-app-req-bus-collaterl-info-item-custom.component';
@Component({
  selector: 'credit-app-req-bus-collaterl-info-item',
  templateUrl: './credit-app-req-bus-collaterl-info-item.component.html',
  styleUrls: ['./credit-app-req-bus-collaterl-info-item.component.scss']
})
export class CreditAppReqBusCollaterlInfoItemComponent extends BaseItemComponent<CreditAppReqBusCollaterlInfoItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  passingObj: any;

  constructor(
    private service: CreditAppReqBusCollaterlInfoService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CreditAppReqBusCollaterlInfoItemCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {
    this.passingObj = this.getPassingObject(this.currentActivatedRoute);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.isUpdateMode = true;
    this.setInitialCreatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {}
  getById(): Observable<CreditAppReqBusCollaterlInfoItemView> {
    return this.service.getCreditAppReqBusCollaterlInfoById(this.id);
  }
  getInitialData(): Observable<CreditAppReqBusCollaterlInfoItemView> {
    return this.custom.getInitialData(this.passingObj);
  }
  setInitialUpdatingData(): void {
    this.onAsyncRunner();
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe(
      (result) => {
        this.model = result;
        this.onAsyncRunner();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(false, false, this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateCreditAppReqBusCollaterlInfo(this.model), isColsing);
    } else {
      super.onCreate(this.service.createCreditAppReqBusCollaterlInfo(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
