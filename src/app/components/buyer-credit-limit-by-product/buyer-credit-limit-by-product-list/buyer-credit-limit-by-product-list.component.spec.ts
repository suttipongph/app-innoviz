import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BuyerCreditLimitByProductListComponent } from './buyer-credit-limit-by-product-list.component';

describe('BuyerCreditLimitByProductListViewComponent', () => {
  let component: BuyerCreditLimitByProductListComponent;
  let fixture: ComponentFixture<BuyerCreditLimitByProductListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BuyerCreditLimitByProductListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerCreditLimitByProductListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
