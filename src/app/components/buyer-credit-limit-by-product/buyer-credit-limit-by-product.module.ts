import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BuyerCreditLimitByProductService } from './buyer-credit-limit-by-product.service';
import { BuyerCreditLimitByProductListComponent } from './buyer-credit-limit-by-product-list/buyer-credit-limit-by-product-list.component';
import { BuyerCreditLimitByProductItemComponent } from './buyer-credit-limit-by-product-item/buyer-credit-limit-by-product-item.component';
import { BuyerCreditLimitByProductItemCustomComponent } from './buyer-credit-limit-by-product-item/buyer-credit-limit-by-product-item-custom.component';
import { BuyerCreditLimitByProductListCustomComponent } from './buyer-credit-limit-by-product-list/buyer-credit-limit-by-product-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BuyerCreditLimitByProductListComponent, BuyerCreditLimitByProductItemComponent],
  providers: [BuyerCreditLimitByProductService, BuyerCreditLimitByProductItemCustomComponent, BuyerCreditLimitByProductListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BuyerCreditLimitByProductListComponent, BuyerCreditLimitByProductItemComponent]
})
export class BuyerCreditLimitByProductComponentModule {}
