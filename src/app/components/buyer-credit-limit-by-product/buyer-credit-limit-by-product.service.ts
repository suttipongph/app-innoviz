import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { BuyerCreditLimitByProductItemView, BuyerCreditLimitByProductListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class BuyerCreditLimitByProductService {
  serviceKey = 'buyerCreditLimitByProductGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBuyerCreditLimitByProductToList(search: SearchParameter): Observable<SearchResult<BuyerCreditLimitByProductListView>> {
    const url = `${this.servicePath}/GetBuyerCreditLimitByProductList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBuyerCreditLimitByProduct(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBuyerCreditLimitByProduct`;
    return this.dataGateway.delete(url, row);
  }
  getBuyerCreditLimitByProductById(id: string): Observable<BuyerCreditLimitByProductItemView> {
    const url = `${this.servicePath}/GetBuyerCreditLimitByProductById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBuyerCreditLimitByProduct(vmModel: BuyerCreditLimitByProductItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBuyerCreditLimitByProduct`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBuyerCreditLimitByProduct(vmModel: BuyerCreditLimitByProductItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBuyerCreditLimitByProduct`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
