import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { BuyerCreditLimitByProductItemView } from 'shared/models/viewModel';

const firstGroup = ['BUYER_TABLE_GUID', 'CREDIT_LIMIT_BALANCE'];
const secondGroup = ['PRODUCT_TYPE'];

export class BuyerCreditLimitByProductItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: BuyerCreditLimitByProductItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    if (isUpdateMode(model.buyerCreditLimitByProductGUID)) {
      fieldAccessing.push({ filedIds: secondGroup, readonly: true });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BuyerCreditLimitByProductItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(parentId): Observable<BuyerCreditLimitByProductItemView> {
    let model: BuyerCreditLimitByProductItemView = new BuyerCreditLimitByProductItemView();
    model.buyerCreditLimitByProductGUID = EmptyGuid;
    model.buyerTableGUID = parentId;
    return of(model);
  }
}
