import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyerCreditLimitByProductItemComponent } from './buyer-credit-limit-by-product-item.component';

describe('BuyerCreditLimitByProductItemViewComponent', () => {
  let component: BuyerCreditLimitByProductItemComponent;
  let fixture: ComponentFixture<BuyerCreditLimitByProductItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BuyerCreditLimitByProductItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerCreditLimitByProductItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
