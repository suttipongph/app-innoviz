import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { ExtendTermParamView, ExtendTermResultView } from 'shared/models/viewModel';
import { ExtendTermService } from '../extend-term.service';
import { ExtendTermCustomComponent } from './extend-term-custom.component';
@Component({
  selector: 'extend-term',
  templateUrl: './extend-term.component.html',
  styleUrls: ['./extend-term.component.scss']
})
export class ExtendTermComponent extends BaseItemComponent<ExtendTermParamView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  creditTermOptions: SelectItems[] = [];

  constructor(private service: ExtendTermService, private currentActivatedRoute: ActivatedRoute, public custom: ExtendTermCustomComponent) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialUpdatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<ExtendTermParamView> {
    return this.service.getExtendTermById(this.id);
  }  
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
  }

  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getCreditTermDropDown()).subscribe(
      ([creditTerm]) => {
        this.creditTermOptions = creditTerm;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing().subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {}
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.service.getExtendTermValidation(this.model).subscribe((isValid) => {
          if (isValid) {
            const header: TranslateModel = { code: 'CONFIRM.CONFIRM' };
            const message: TranslateModel = { code: 'CONFIRM.90073', parameters: ['LABEL.EXTEND_TERM'] };
            this.notificationService.showManualConfirmDialog(header, message);
            this.notificationService.isAccept.subscribe((isConfirm) => {
              if (isConfirm) {
                this.onSubmit(true, this.model);
              }
              this.notificationService.isAccept.observers = [];
            });
          }
        });
      }
    });
  }
  onSubmit(isColsing: boolean, model: ExtendTermParamView): void {
    super.onExecuteFunction(this.service.extendTerm(model));
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  
}
