import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { ExtendTermService } from '../extend-term.service';

const firstGroup = [
  'WITHDRAWAL_ID',
  'DOCUMENT_STATUS',
  'CUSTOMER_ID',
  'BUYER_ID',
  'TERM_EXTENSION',
  'NUMBER_OF_EXTENSION',
  'WITHDRAWAL_DATE',
  'DUE_DATE',
  'CREDIT_TERM_ID',
  'TOTAL_INTEREST_PCT',
  'WITHDRAWAL_AMOUNT',
  'OUTSTANDING'
];

export class ExtendTermCustomComponent {
  baseService: BaseServiceModel<ExtendTermService>;
  getFieldAccessing(): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  } 
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
}
