import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExtendTermService } from './extend-term.service';
import { ExtendTermComponent } from './extend-term/extend-term.component';
import { ExtendTermCustomComponent } from './extend-term/extend-term-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ExtendTermComponent],
  providers: [ExtendTermService, ExtendTermCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ExtendTermComponent]
})
export class ExtendTermComponentModule {}
