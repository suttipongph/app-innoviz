import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ExtendTermParamView, ExtendTermResultView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ExtendTermService {
  serviceKey = 'extendTermGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getExtendTermById(id: string): Observable<ExtendTermParamView> {
    const url = `${this.servicePath}/GetExtendTermById/id=${id}`;
    return this.dataGateway.get(url);
  }
  getExtendTermValidation(vmModel: ExtendTermParamView): Observable<boolean> {
    const url = `${this.servicePath}/ExtendTermValidation`;
    return this.dataGateway.post(url,vmModel);
  }
  extendTerm(vmModel: ExtendTermParamView): Observable<ExtendTermResultView> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vmModel);
  }
}
