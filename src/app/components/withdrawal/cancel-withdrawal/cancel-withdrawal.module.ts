import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CancelWithdrawalService } from './cancel-withdrawal.service';
import { CancelWithdrawalComponent } from './cancel-withdrawal/cancel-withdrawal.component';
import { CancelWithdrawalCustomComponent } from './cancel-withdrawal/cancel-withdrawal-custom.component';
@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CancelWithdrawalComponent],
  providers: [CancelWithdrawalService, CancelWithdrawalCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CancelWithdrawalComponent]
})
export class CancelWithdrawalComponentModule {}
