import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { CancelWithdrawalResultView, CancelWithdrawalView } from 'shared/models/viewModel/cancelWithdrawalView';
@Injectable()
export class CancelWithdrawalService {
  serviceKey = 'cancelWithdrawalGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCancelWithdrawalById(id: string): Observable<CancelWithdrawalView> {
    const url = `${this.servicePath}/GetCancelWithdrawalById/id=${id}`;
    return this.dataGateway.get(url);
  }
  cancelWithdrawal(vmModel: CancelWithdrawalView): Observable<CancelWithdrawalResultView> {
    const url = `${this.servicePath}/CancelWithdrawal`;
    return this.dataGateway.post(url, vmModel);
  }
}
