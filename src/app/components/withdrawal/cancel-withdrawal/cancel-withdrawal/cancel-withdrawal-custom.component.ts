import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CancelWithdrawalView } from 'shared/models/viewModel/cancelWithdrawalView';

const firstGroup = [
  'WITHDRAWAL_ID',
  'DOCUMENT_STATUS',
  'CUSTOMER_ID',
  'BUYER_ID',
  'WITHDRAWAL_DATE',
  'CREDIT_TERM_ID',
  'DUE_DATE',
  'TOTAL_INTEREST_PCT',
  'WITHDRAWAL_AMOUNT'
];

export class CancelWithdrawalCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CancelWithdrawalView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CancelWithdrawalView): Observable<boolean> {
    return of(false);
  }
  getInitialData(): Observable<CancelWithdrawalView> {
    let model = new CancelWithdrawalView();
    model.withdrawalTableGUID = EmptyGuid;
    return of(model);
  }
}
