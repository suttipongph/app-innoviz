import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelWithdrawalComponent } from './cancel-withdrawal.component';

describe('CancelWithdrawalViewComponent', () => {
  let component: CancelWithdrawalComponent;
  let fixture: ComponentFixture<CancelWithdrawalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CancelWithdrawalComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelWithdrawalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
