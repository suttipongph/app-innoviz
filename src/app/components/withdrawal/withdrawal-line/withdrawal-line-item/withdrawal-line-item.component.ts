import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { Dimension, RefType, ROUTE_RELATED_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { LedgerDimensionItemView, WithdrawalLineItemView } from 'shared/models/viewModel';
import { WithdrawalLineService } from '../withdrawal-line.service';
import { WithdrawalLineItemCustomComponent } from './withdrawal-line-item-custom.component';
@Component({
  selector: 'withdrawal-line-item',
  templateUrl: './withdrawal-line-item.component.html',
  styleUrls: ['./withdrawal-line-item.component.scss']
})
export class WithdrawalLineItemComponent extends BaseItemComponent<WithdrawalLineItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  chequeTableCustomerOptions: SelectItems[] = [];
  chequeTableBuyerOptions: SelectItems[] = [];
  ledgerDimensionOptions1: SelectItems[] = [];
  ledgerDimensionOptions2: SelectItems[] = [];
  ledgerDimensionOptions3: SelectItems[] = [];
  ledgerDimensionOptions4: SelectItems[] = [];
  ledgerDimensionOptions5: SelectItems[] = [];
  withdrawalLineTypeOptions: SelectItems[] = [];

  constructor(
    private service: WithdrawalLineService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: WithdrawalLineItemCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getWithdrawalLineTypeEnumDropDown(this.withdrawalLineTypeOptions);
  }
  getById(): Observable<WithdrawalLineItemView> {
    return this.service.getWithdrawalLineById(this.id);
  }
  getInitialData(): Observable<WithdrawalLineItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    let tempModel: WithdrawalLineItemView = this.isUpdateMode ? model : this.model;
    forkJoin([
      this.custom.getPDCCustomerDropdown(tempModel),
      this.custom.getPDCBuyerDropdown(tempModel),
      this.baseDropdown.getLedgerDimensionDropDown()
    ]).subscribe(
      ([chequeTableCustomer, chequeTableBuyer, ledgerDimension]) => {
        this.chequeTableCustomerOptions = chequeTableCustomer;
        this.chequeTableBuyerOptions = chequeTableBuyer;
        this.setLedgerDimension(ledgerDimension);
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  setRelatedInfoOptions(): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.INQUIRY',
        items: [
          {
            label: 'LABEL.INVOICE',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.INVOICE_TABLE,
                parameters: { refGUID: this.model.withdrawalLineGUID, refType: RefType.WithdrawalLine }
              })
          },
          //   { label: 'LABEL.INTEREST_REALIZATION', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.INTEREST_REALIZATION }) }
          //   { label: 'LABEL.INVOICE', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.INVOICE }) },
          { label: 'LABEL.INTEREST_REALIZATION', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.INTEREST_REALIZED_TRANSACTION }) }
        ]
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateWithdrawalLine(this.model), isColsing);
    } else {
      super.onCreate(this.service.createWithdrawalLine(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onCustomerPDCTChange() {
    this.custom.setModelByCustomerPDCT(this.model, this.chequeTableCustomerOptions);
  }
  onBillingDateValidation() {
    return this.custom.getCustomerPDCTChangeValidation(this.model);
  }
  onCollectionDateValidation() {
    return this.custom.getCollectionDateValidation(this.model);
  }
  setLedgerDimension(ledgerDimension: SelectItems[]): void {
    this.ledgerDimensionOptions1 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension1);
    this.ledgerDimensionOptions2 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension2);
    this.ledgerDimensionOptions3 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension3);
    this.ledgerDimensionOptions4 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension4);
    this.ledgerDimensionOptions5 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension5);
  }
}
