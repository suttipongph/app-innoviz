import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WithdrawalLineItemComponent } from './withdrawal-line-item.component';

describe('WithdrawalLineItemViewComponent', () => {
  let component: WithdrawalLineItemComponent;
  let fixture: ComponentFixture<WithdrawalLineItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WithdrawalLineItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WithdrawalLineItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
