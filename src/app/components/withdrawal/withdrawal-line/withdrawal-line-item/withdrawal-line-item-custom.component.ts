import { TranslateService } from '@ngx-translate/core';
import { AppInjector } from 'app-injector';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { EmptyGuid, WithdrawalStatus } from 'shared/constants';
import { isDateFromGreaterThanDateTo } from 'shared/functions/date.function';
import { isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { ChequeTableItemView, WithdrawalLineItemView } from 'shared/models/viewModel';
import { WithdrawalLineService } from '../withdrawal-line.service';

const firstGroup = [
  'WITHDRAWAL_TABLE_GUID',
  'WITHDRAWAL_LINE_TYPE',
  'START_DATE',
  'DUE_DATE',
  'INTEREST_DATE',
  'INTEREST_DAY',
  'INTEREST_AMOUNT',
  'WITHDRAWAL_LINE_INVOICE_TABLE_GUID',
  'CLOSED_FOR_TERM_EXTENSION',
  'CUSTOMER_PDC_DATE',
  'CUSTOMER_PDC_AMOUNT',
  'LINE_NUM'
];

export class WithdrawalLineItemCustomComponent {
  baseService: BaseServiceModel<any>;
  translateService = AppInjector.get(TranslateService);

  getFieldAccessing(model: WithdrawalLineItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: WithdrawalLineItemView): Observable<boolean> {
    const accessright = isNullOrUndefOrEmptyGUID(model.withdrawalLineGUID) ? canCreate : canUpdate;
    let service: WithdrawalLineService = this.baseService.service;
    return service.getWithdrawalTableAccessMode(model.withdrawalTableGUID).pipe(
      map((result) => {
        let accessLogic = isNullOrUndefOrEmptyGUID(model.withdrawalLineGUID) ? result.canCreate : result.canView;
        return !(accessLogic && accessright);
      })
    );
  }
  getInitialData(): Observable<WithdrawalLineItemView> {
    let model = new WithdrawalLineItemView();
    model.withdrawalLineGUID = EmptyGuid;
    return of(model);
  }
  setModelByCustomerPDCT(model: WithdrawalLineItemView, customerPDCTOption: SelectItems[]) {
    if (!isNullOrUndefOrEmptyGUID(model.customerPDCTableGUID)) {
      let customerPDCT: ChequeTableItemView = customerPDCTOption.find((t) => t.value == model.customerPDCTableGUID).rowData as ChequeTableItemView;
      model.customerPDCDate = customerPDCT.chequeDate;
      model.customerPDCAmount = customerPDCT.amount;
    } else {
      model.customerPDCDate = null;
    }
  }

  getCustomerPDCTChangeValidation(model: WithdrawalLineItemView): any {
    let condition = isDateFromGreaterThanDateTo(model.withdrawalTable_WithdrawalDate, model.billingDate);
    if (condition) {
      return {
        code: 'ERROR.DATE_CANNOT_LESS_THAN',
        parameters: [this.translateService.instant('LABEL.BILLING_DATE'), this.translateService.instant('LABEL.WITHDRAWAL_DATE')]
      };
    }
    return null;
  }
  getCollectionDateValidation(model: WithdrawalLineItemView): any {
    let condition = isDateFromGreaterThanDateTo(model.withdrawalTable_WithdrawalDate, model.collectionDate);

    if (condition) {
      return {
        code: 'ERROR.DATE_CANNOT_LESS_THAN',
        parameters: [this.translateService.instant('LABEL.COLLECTION_DATE'), this.translateService.instant('LABEL.WITHDRAWAL_DATE')]
      };
    }
    return null;
  }

  getPDCCustomerDropdown(model: WithdrawalLineItemView) {
    if (model.withdrawalTable_StatusId !== WithdrawalStatus.Draft) {
      return this.baseService.baseDropdown.getChequeTableDropDown();
    } else {
      return this.baseService.baseDropdown.getChequeTableByWithdrawalLineCustomerDropDown([
        model.withdrawalTableGUID,
        model.withdrawalTable_CustomerTableGUID,
        model.withdrawalTable_BuyerTableGUID
      ]);
    }
  }
  getPDCBuyerDropdown(model: WithdrawalLineItemView) {
    if (model.withdrawalTable_StatusId !== WithdrawalStatus.Draft) {
      return this.baseService.baseDropdown.getChequeTableDropDown();
    } else {
      return this.baseService.baseDropdown.getChequeTableByWithdrawalLineBuyerDropDown([
        model.withdrawalTableGUID,
        model.withdrawalTable_CustomerTableGUID,
        model.withdrawalTable_BuyerTableGUID
      ]);
    }
  }
}
