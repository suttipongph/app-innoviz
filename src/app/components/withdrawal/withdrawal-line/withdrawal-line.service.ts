import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, WithdrawalLineItemView, WithdrawalLineListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class WithdrawalLineService {
  serviceKey = 'withdrawalLineGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getWithdrawalLineToList(search: SearchParameter): Observable<SearchResult<WithdrawalLineListView>> {
    const url = `${this.servicePath}/GetWithdrawalLineList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteWithdrawalLine(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteWithdrawalLine`;
    return this.dataGateway.delete(url, row);
  }
  getWithdrawalLineById(id: string): Observable<WithdrawalLineItemView> {
    const url = `${this.servicePath}/GetWithdrawalLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createWithdrawalLine(vmModel: WithdrawalLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateWithdrawalLine`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateWithdrawalLine(vmModel: WithdrawalLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateWithdrawalLine`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getWithdrawalTableAccessMode(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetWithdrawalTableAccessMode/id=${id}`;
    return this.dataGateway.get(url);
  }
}
