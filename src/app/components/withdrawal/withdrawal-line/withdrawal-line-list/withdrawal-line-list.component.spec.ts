import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WithdrawalLineListComponent } from './withdrawal-line-list.component';

describe('WithdrawalLineListViewComponent', () => {
  let component: WithdrawalLineListComponent;
  let fixture: ComponentFixture<WithdrawalLineListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WithdrawalLineListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WithdrawalLineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
