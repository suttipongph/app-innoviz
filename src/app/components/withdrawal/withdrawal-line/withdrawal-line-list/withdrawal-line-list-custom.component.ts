import { Observable } from 'rxjs';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { BaseServiceModel } from 'shared/models/systemModel';

export class WithdrawalLineListCustomComponent {
  baseService: BaseServiceModel<any>;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  getParentKey() {
    let parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (parentName) {
      case ROUTE_RELATED_GEN.CREDIT_APP_TABLE:
        return this.baseService.uiService.getKey(ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_WITHDRAWAL);
      case ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_WITHDRAWAL:
        return this.baseService.uiService.getKey(ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_TERM_EXTENSION);
    }
  }
}
