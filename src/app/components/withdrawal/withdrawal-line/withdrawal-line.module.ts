import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WithdrawalLineService } from './withdrawal-line.service';
import { WithdrawalLineListComponent } from './withdrawal-line-list/withdrawal-line-list.component';
import { WithdrawalLineItemComponent } from './withdrawal-line-item/withdrawal-line-item.component';
import { WithdrawalLineItemCustomComponent } from './withdrawal-line-item/withdrawal-line-item-custom.component';
import { WithdrawalLineListCustomComponent } from './withdrawal-line-list/withdrawal-line-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [WithdrawalLineListComponent, WithdrawalLineItemComponent],
  providers: [WithdrawalLineService, WithdrawalLineItemCustomComponent, WithdrawalLineListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [WithdrawalLineListComponent, WithdrawalLineItemComponent]
})
export class WithdrawalLineComponentModule {}
