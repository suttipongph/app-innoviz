import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { RecalWithdrawalInterestParamView, RecalWithdrawalInterestResultView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class RecalWithdrawalInterestService {
  serviceKey = 'recalWithdrawalInterestGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getRecalWithdrawalInterestById(id: string): Observable<RecalWithdrawalInterestParamView> {
    const url = `${this.servicePath}/GetRecalWithdrawalInterestById/id=${id}`;
    return this.dataGateway.get(url);
  }
  recalWithdrawalInterest(vmModel: RecalWithdrawalInterestParamView): Observable<RecalWithdrawalInterestResultView> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vmModel);
  }
}
