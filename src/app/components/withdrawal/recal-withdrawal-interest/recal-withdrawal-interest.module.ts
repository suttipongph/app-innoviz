import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RecalWithdrawalInterestService } from './recal-withdrawal-interest.service';
import { RecalWithdrawalInterestComponent } from './recal-withdrawal-interest/recal-withdrawal-interest.component';
import { RecalWithdrawalInterestCustomComponent } from './recal-withdrawal-interest/recal-withdrawal-interest-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [RecalWithdrawalInterestComponent],
  providers: [RecalWithdrawalInterestService, RecalWithdrawalInterestCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [RecalWithdrawalInterestComponent]
})
export class RecalWithdrawalInterestComponentModule {}
