import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecalWithdrawalInterestComponent } from './recal-withdrawal-interest.component';

describe('RecalWithdrawalInterestViewComponent', () => {
  let component: RecalWithdrawalInterestComponent;
  let fixture: ComponentFixture<RecalWithdrawalInterestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RecalWithdrawalInterestComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecalWithdrawalInterestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
