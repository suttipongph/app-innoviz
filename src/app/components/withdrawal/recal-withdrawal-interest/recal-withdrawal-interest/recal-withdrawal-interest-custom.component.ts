import { Observable, of } from 'rxjs';
import { BaseServiceModel, FieldAccessing, TranslateModel } from 'shared/models/systemModel';
import { RecalWithdrawalInterestParamView } from 'shared/models/viewModel';

const firstGroup = ['WITHDRAWAL_ID', 'DUE_DATE', 'TOTAL_INTEREST_PCT'];

export class RecalWithdrawalInterestCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: RecalWithdrawalInterestParamView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<RecalWithdrawalInterestParamView> {
    let model = new RecalWithdrawalInterestParamView();
    return of(model);
  }
  onValidateWithdrawalAmount(model: RecalWithdrawalInterestParamView): TranslateModel {
    if (model.withdrawalAmount <= 0) {
      return {
        code: 'ERROR.GREATER_THAN_ZERO',
        parameters: [this.baseService.translate.instant('LABEL.WITHDRAWAL_AMOUNT')]
      };
    } else {
      return null;
    }
  }
}
