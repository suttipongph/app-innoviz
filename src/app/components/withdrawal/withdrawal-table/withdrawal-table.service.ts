import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { WithdrawalTableItemView, WithdrawalTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class WithdrawalTableService {
  serviceKey = 'withdrawalTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getWithdrawalTableToList(search: SearchParameter): Observable<SearchResult<WithdrawalTableListView>> {
    const url = `${this.servicePath}/GetWithdrawalTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteWithdrawalTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteWithdrawalTable`;
    return this.dataGateway.delete(url, row);
  }
  getWithdrawalTableById(id: string): Observable<WithdrawalTableItemView> {
    const url = `${this.servicePath}/GetWithdrawalTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createWithdrawalTable(vmModel: WithdrawalTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateWithdrawalTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateWithdrawalTable(vmModel: WithdrawalTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateWithdrawalTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<WithdrawalTableItemView> {
    const url = `${this.servicePath}/GetWithdrawalTableInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  validateIsManualNumberSeq(companyId: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeqWithdrawalTable/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
  isWithdrawalLineByWithdrawalTableEmpty(id: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsWithdrawalLineByWithdrawalEmpty/id=${id}`;
    return this.dataGateway.get(url);
  }
  isBuyerAgreementTransByWithdrawalTableExist(id: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsBuyerAgreementTransByWithdrawalTableExist/id=${id}`;
    return this.dataGateway.get(url);
  }
  getWithdrawalTableByWithdrawalDate(vmModel: WithdrawalTableItemView): Observable<WithdrawalTableItemView> {
    const url = `${this.servicePath}/GetWithdrawalTableByWithdrawalDate`;
    return this.dataGateway.post(url, vmModel);
  }
  getWithdrawalTableByTermExtensionInvoiceRevenueType(vmModel: WithdrawalTableItemView): Observable<WithdrawalTableItemView> {
    const url = `${this.servicePath}/GetWithdrawalTableByTermExtensionInvoiceRevenueType`;
    return this.dataGateway.post(url, vmModel);
  }
  getWithdrawalTermExtensionDisable(id: string): Observable<boolean> {
    const url = `${this.servicePath}/GetWithdrawalTermExtensionDisable/id=${id}`;
    return this.dataGateway.get(url);
  }
}
