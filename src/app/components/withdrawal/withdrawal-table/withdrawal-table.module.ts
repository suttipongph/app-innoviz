import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WithdrawalTableService } from './withdrawal-table.service';
import { WithdrawalTableListComponent } from './withdrawal-table-list/withdrawal-table-list.component';
import { WithdrawalTableItemComponent } from './withdrawal-table-item/withdrawal-table-item.component';
import { WithdrawalTableItemCustomComponent } from './withdrawal-table-item/withdrawal-table-item-custom.component';
import { WithdrawalTableListCustomComponent } from './withdrawal-table-list/withdrawal-table-list-custom.component';
import { WithdrawalLineComponentModule } from '../withdrawal-line/withdrawal-line.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, WithdrawalLineComponentModule],
  declarations: [WithdrawalTableListComponent, WithdrawalTableItemComponent],
  providers: [WithdrawalTableService, WithdrawalTableItemCustomComponent, WithdrawalTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [WithdrawalTableListComponent, WithdrawalTableItemComponent]
})
export class WithdrawalTableComponentModule {}
