import { forkJoin, Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ROUTE_RELATED_GEN, WithdrawalStatus } from 'shared/constants';
import { BaseServiceModel } from 'shared/models/systemModel';
import { WithdrawalTableListView } from 'shared/models/viewModel';

export class WithdrawalTableListCustomComponent {
  baseService: BaseServiceModel<any>;
  title = 'LABEL.WITHDRAWAL';
  termextensiondisable: boolean = true;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setTitle() {
    let parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (parentName) {
      case ROUTE_RELATED_GEN.CREDIT_APP_TABLE:
        this.title = 'LABEL.WITHDRAWAL';
        break;
      case ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_WITHDRAWAL:
        this.title = 'LABEL.TERM_EXTENSION';
        break;
      default:
        this.title = 'LABEL.WITHDRAWAL';
        break;
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    let parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    return accessRight && parentName != ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_WITHDRAWAL;
  }
  getWithdrawalTermExtensionVisible(): boolean {
    let parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (parentName) {
      case ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_WITHDRAWAL:
        return true;
      default:
        return false;
    }
  }
  getValidateTermExt(): Observable<any> {
    let parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    let parentKey = this.baseService.uiService.getRelatedInfoParentTableKey();
    if (parentName == ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_WITHDRAWAL) {
      return this.baseService.service.getWithdrawalTermExtensionDisable(parentKey).pipe(
        tap((result) => {
          this.termextensiondisable = result as boolean;
          return result;
        })
      );
    } else {
      return of(true);
    }
  }
  getValidateFunction(): Observable<any> {
    return this.getValidateTermExt();
  }
}
