import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WithdrawalTableListComponent } from './withdrawal-table-list.component';

describe('WithdrawalTableListViewComponent', () => {
  let component: WithdrawalTableListComponent;
  let fixture: ComponentFixture<WithdrawalTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WithdrawalTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WithdrawalTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
