import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, ROUTE_FUNCTION_GEN, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { WithdrawalTableListView } from 'shared/models/viewModel';
import { WithdrawalTableService } from '../withdrawal-table.service';
import { WithdrawalTableListCustomComponent } from './withdrawal-table-list-custom.component';

@Component({
  selector: 'withdrawal-table-list',
  templateUrl: './withdrawal-table-list.component.html',
  styleUrls: ['./withdrawal-table-list.component.scss']
})
export class WithdrawalTableListComponent extends BaseListComponent<WithdrawalTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  creditTermOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  defualtBitOption: SelectItems[] = [];

  constructor(public custom: WithdrawalTableListCustomComponent, private service: WithdrawalTableService) {
    super();
    this.baseService.service = this.service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {
    this.custom.setTitle();
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.getValidateFunction().subscribe((result) => {
      this.setFunctionOptions();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getDefaultBitDropDown(this.defualtBitOption);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCreditTermDropDown(this.creditTermOptions);
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'withdrawalTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.WITHDRAWAL_ID',
        textKey: 'withdrawalId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC,
        sortingKey: 'withdrawalId',
        searchingKey: 'withdrawalId'
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId'
      },
      {
        label: 'LABEL.WITHDRAWAL_DATE',
        textKey: 'withdrawalDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DUE_DATE',
        textKey: 'dueDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CREDIT_TERM_ID',
        textKey: 'creditTerm_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditTermOptions,
        searchingKey: 'creditTermGUID',
        sortingKey: 'creditTerm_CreditTermId'
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentStatusOptions,
        searchingKey: 'documentStatusGUID',
        sortingKey: 'documentStatus_StatusId'
      },
      {
        label: 'LABEL.TERM_EXTENSION',
        textKey: 'termExtension',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defualtBitOption
      },
      {
        label: 'LABEL.NUMBER_OF_EXTENSION',
        textKey: 'numberOfExtension',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<WithdrawalTableListView>> {
    return this.service.getWithdrawalTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteWithdrawalTable(row));
  }
  setFunctionOptions(): void {
    const termextensionvisible = this.custom.getWithdrawalTermExtensionVisible();
    this.functionItems = [
      {
        label: 'LABEL.EXTEND_TERM',
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.EXTEND_TERM }),
        visible: termextensionvisible,
        disabled: this.custom.termextensiondisable
      }
    ];
    super.setFunctionOptionsMapping();
  }
}
