import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WithdrawalTableItemComponent } from './withdrawal-table-item.component';

describe('WithdrawalTableItemViewComponent', () => {
  let component: WithdrawalTableItemComponent;
  let fixture: ComponentFixture<WithdrawalTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WithdrawalTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WithdrawalTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
