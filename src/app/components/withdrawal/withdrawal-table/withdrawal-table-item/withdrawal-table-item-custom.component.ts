import { forkJoin, Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { CreditLimitConditionType, EmptyGuid, ROUTE_RELATED_GEN, WithdrawalStatus } from 'shared/constants';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, CreditAppLineItemView, InvoiceRevenueTypeItemView, WithdrawalTableItemView } from 'shared/models/viewModel';

const firstGroup = [
  'WITHDRAWAL_ID',
  'PRODUCT_TYPE',
  'CREDIT_APP_TABLE_GUID',
  'DOCUMENT_STATUS_GUID',
  'CUSTOMER_TABLE_GUID',
  'BUYER_TABLE_GUID',
  'DUE_DATE',
  'TOTAL_INTEREST_PCT',
  'INTEREST_CUT_DAY',
  'METHOD_OF_PAYMENT_GUID',
  'NET_PAID',
  'RETENTION_CALCULATE_BASE',
  'RETENTION_PCT',
  'TERM_EXTENSION',
  'NUMBER_OF_EXTENSION',
  'EXTEND_WITHDRAWAL_TABLE_GUID',
  'ORIGINAL_WITHDRAWAL_TABLE_GUID',
  'TERM_EXTENSION_INVOICE_REVENUE_TYPE_GUID',
  'TERM_EXTENSION_FEE_AMOUNT',
  'SETTLE_TERM_EXTENSION_FEE_AMOUNT',
  'EXTEND_INTEREST_DATE',
  'DOCUMENT_REASON_GUID'
];

// numberSeq
const secoundGroup = ['WITHDRAWAL_ID'];

// term extension
const thirdGroup = ['ASSIGNMENT_AGREEMENT_TABLE_GUID', 'ASSIGNMENT_AMOUNT'];
const forthGroup = ['TERM_EXTENSION_INVOICE_REVENUE_TYPE_GUID', 'TERM_EXTENSION_FEE_AMOUNT'];
const settleTermExtensionGroup = ['SETTLE_TERM_EXTENSION_FEE_AMOUNT'];
// is withdrawal line exist?
const fifthGroup = ['WITHDRAWAL_DATE', 'CREDIT_TERM_GUID'];
const sixthGroup = ['WITHDRAWAL_AMOUNT'];
const seventhGroup = ['CREDIT_APP_LINE_GUID', 'BUYER_AGREEMENT_TABLE_GUID'];

const operationGroup = ['OPERATION_COMMENT'];
const creditGroup = ['CREDIT_COMMENT'];
const marketingGroup = ['MARKETING_COMMENT'];

export class WithdrawalTableItemCustomComponent {
  isManual = true;
  isCreditAppLineMandatory = false;
  isBuyerAgreementTableMandatory = false;
  baseService: BaseServiceModel<any>;
  title = 'LABEL.WITHDRAWAL';
  getFieldAccessing(model: WithdrawalTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });

    // set mandatory
    if (model.creditLimitType_CreditLimitConditionType == CreditLimitConditionType.ByCreditApplication) {
      this.isCreditAppLineMandatory = true;
    } else {
      this.isCreditAppLineMandatory = false;
    }
    if (model.creditLimitType_ValidateBuyerAgreement) {
      this.isBuyerAgreementTableMandatory = true;
    } else {
      this.isBuyerAgreementTableMandatory = false;
    }

    if (!isUpdateMode(model.withdrawalTableGUID)) {
      fieldAccessing.push({ filedIds: sixthGroup, readonly: false });
      return this.baseService.service.validateIsManualNumberSeq(model.companyGUID).pipe(
        map((result: boolean) => {
          this.isManual = result;
          fieldAccessing.push({ filedIds: secoundGroup, readonly: !this.isManual });
          return fieldAccessing;
        }),
        catchError((err) => {
          this.baseService.notificationService.showErrorMessageFromResponse(err);
          return fieldAccessing;
        })
      );
    } else {
      fieldAccessing.push({ filedIds: secoundGroup, readonly: true });
      if (model.termExtension) {
        fieldAccessing.push({ filedIds: thirdGroup, readonly: true });
        fieldAccessing.push({ filedIds: seventhGroup, readonly: true });
        fieldAccessing.push({ filedIds: forthGroup, readonly: false });
      } else {
        fieldAccessing.push({ filedIds: thirdGroup, readonly: false });
        fieldAccessing.push({ filedIds: forthGroup, readonly: true });
      }

      if (
        model.termExtension &&
        !isNullOrUndefOrEmptyGUID(model.termExtensionInvoiceRevenueTypeGUID) &&
        isNullOrUndefOrEmptyGUID(model.invoiceRevenueType_IntercompanyTableGUID)
      ) {
        fieldAccessing.push({ filedIds: settleTermExtensionGroup, readonly: false });
      } else {
        fieldAccessing.push({ filedIds: settleTermExtensionGroup, readonly: true });
      }

      if (model.operationMarkComment) {
        fieldAccessing.push({ filedIds: operationGroup, readonly: false });
      } else {
        fieldAccessing.push({ filedIds: operationGroup, readonly: true });
      }
      if (model.creditMarkComment) {
        fieldAccessing.push({ filedIds: creditGroup, readonly: false });
      } else {
        fieldAccessing.push({ filedIds: creditGroup, readonly: true });
      }
      if (model.marketingMarkComment) {
        fieldAccessing.push({ filedIds: marketingGroup, readonly: false });
      } else {
        fieldAccessing.push({ filedIds: marketingGroup, readonly: true });
      }

      return forkJoin([
        this.baseService.service.isBuyerAgreementTransByWithdrawalTableExist(model.withdrawalTableGUID),
        this.baseService.service.isWithdrawalLineByWithdrawalTableEmpty(model.withdrawalTableGUID)
      ]).pipe(
        map(([r1, r2]) => {
          if (!model.termExtension) {
            fieldAccessing.push({ filedIds: seventhGroup, readonly: r1 as boolean });
          }
          fieldAccessing.push({ filedIds: fifthGroup, readonly: !r2 });
          fieldAccessing.push({ filedIds: sixthGroup, readonly: !r2 });
          return fieldAccessing;
        })
      );
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: WithdrawalTableItemView): Observable<boolean> {
    return this.getAccessModeByParent(model).pipe(
      map((result) => {
        let accessright = isNullOrUndefOrEmptyGUID(model.withdrawalTableGUID) ? canCreate : canUpdate;
        let accessLogic = isNullOrUndefOrEmptyGUID(model.withdrawalTableGUID) ? result.canCreate : result.canView;
        return !(accessLogic && accessright);
      })
    );
  }
  getAccessModeByParent(model: WithdrawalTableItemView): Observable<AccessModeView> {
    let accessMode = new AccessModeView();
    accessMode.canView = model.documentStatus_StatusId == WithdrawalStatus.Draft;
    accessMode.canCreate = model.documentStatus_StatusId == WithdrawalStatus.Draft;
    return of(accessMode);
  }
  getInitialData(): Observable<WithdrawalTableItemView> {
    let model = new WithdrawalTableItemView();
    model.withdrawalTableGUID = EmptyGuid;
    return of(model);
  }
  setTitle() {
    let parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (parentName) {
      case ROUTE_RELATED_GEN.CREDIT_APP_TABLE:
        this.title = 'LABEL.WITHDRAWAL';
        break;
      case ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_WITHDRAWAL:
        this.title = 'LABEL.TERM_EXTENSION';
        break;
      default:
        this.title = 'LABEL.WITHDRAWAL';
        break;
    }
  }

  setModelByCreditAppLine(model: WithdrawalTableItemView, creditAppLineOption: SelectItems[]) {
    if (!isNullOrUndefOrEmptyGUID(model.creditAppLineGUID)) {
      let creditAppLine: CreditAppLineItemView = creditAppLineOption.find((t) => t.value == model.creditAppLineGUID).rowData as CreditAppLineItemView;
      model.buyerTableGUID = creditAppLine.buyerTableGUID;
      model.buyerTable_Values = creditAppLine.buyerTable_Values;
      model.methodOfPaymentGUID = creditAppLine.methodOfPaymentGUID;
      model.methodOfPayment_Values = creditAppLine.methodOfPayment_Values;
    } else {
      model.buyerTableGUID = null;
      model.buyerTable_Values = '';
      model.methodOfPaymentGUID = null;
      model.methodOfPayment_Values = '';
    }
  }
  setModelByWithdrawalAmount(model: WithdrawalTableItemView) {
    model.assignmentAmount = model.withdrawalAmount;
  }

  setModelByWithdrawalDate(model: WithdrawalTableItemView) {
    if (isNullOrUndefined(model.withdrawalDate)) {
      model.creditAppLineGUID = null;
      model.buyerTableGUID = null;
      model.buyerTable_Values = '';
      model.methodOfPaymentGUID = null;
      model.methodOfPayment_Values = '';
      model.assignmentAgreementTableGUID = null;
    } else if (!isNullOrUndefined(model.withdrawalDate) && !isNullOrUndefOrEmptyGUID(model.creditTermGUID)) {
      this.baseService.service.getWithdrawalTableByWithdrawalDate(model).subscribe(
        (result) => {
          model.dueDate = result.dueDate;
        },
        (error) => {
          this.baseService.notificationService.showErrorMessageFromResponse(error);
          model.dueDate = null;
        }
      );
    } else {
      model.dueDate = null;
    }
  }
  setModelByTermExtensionInvoiceRevenueType(model: WithdrawalTableItemView) {
    if (!isNullOrUndefOrEmptyGUID(model.termExtensionInvoiceRevenueTypeGUID) && !isNullOrUndefOrEmptyGUID(model.creditTermGUID)) {
      this.baseService.service.getWithdrawalTableByTermExtensionInvoiceRevenueType(model).subscribe(
        (result) => {
          model.termExtensionFeeAmount = result.termExtensionFeeAmount;
          model.settleTermExtensionFeeAmount = result.termExtensionFeeAmount;
        },
        (error) => {
          this.baseService.notificationService.showErrorMessageFromResponse(error);
          model.termExtensionFeeAmount = 0;
          model.settleTermExtensionFeeAmount = 0;
        }
      );
    } else {
      model.termExtensionFeeAmount = 0;
      model.settleTermExtensionFeeAmount = 0;
    }
  }
  setModelByTermExtensionFeeAmount(model: WithdrawalTableItemView) {
    model.settleTermExtensionFeeAmount = model.termExtensionFeeAmount;
  }
  setAccessModeByOperation(model: WithdrawalTableItemView): FieldAccessing[] {
    model.operationComment = '';
    if (model.operationMarkComment) {
      return [{ filedIds: operationGroup, readonly: false }];
    } else {
      return [{ filedIds: operationGroup, readonly: true }];
    }
  }
  setAccessModeByCredit(model: WithdrawalTableItemView): FieldAccessing[] {
    model.creditComment = '';
    if (model.creditMarkComment) {
      return [{ filedIds: creditGroup, readonly: false }];
    } else {
      return [{ filedIds: creditGroup, readonly: true }];
    }
  }
  getAssignmentAgreementTableByWithdrawalDropDown(model: WithdrawalTableItemView) {
    if (!isNullOrUndefOrEmptyGUID(model.customerTableGUID) && !isNullOrUndefOrEmptyGUID(model.buyerTableGUID)) {
      return this.baseService.baseDropdown.getAssignmentAgreementTableByWithdrawalDropDown([model.customerTableGUID, model.buyerTableGUID]);
    } else {
      return of([]);
    }
  }

  getBuyerAgreementTableByCreditAppLineDropDown(creditAppLineGUID: string) {
    if (!isNullOrUndefOrEmptyGUID(creditAppLineGUID)) {
      return this.baseService.baseDropdown.getBuyerAgeementTableByWithdrawalTableDropDown(creditAppLineGUID);
    } else {
      return of([]);
    }
  }
  getWithdrawalVisible(model: WithdrawalTableItemView): boolean {
    if (model.termExtension === false) {
      return true;
    } else {
      return false;
    }
  }
  getWithdrawalDisable(model: WithdrawalTableItemView): boolean {
    if (model.termExtension === false && model.documentStatus_StatusId !== WithdrawalStatus.Cancelled) {
      return false;
    } else {
      return true;
    }
  }
  getWithdrawalTermExtensionVisible(model: WithdrawalTableItemView): boolean {
    if (model.termExtension === true) {
      return true;
    } else {
      return false;
    }
  }
  getWithdrawalTermExtensionDisable(model: WithdrawalTableItemView): boolean {
    if (model.termExtension === true && model.documentStatus_StatusId !== WithdrawalStatus.Cancelled) {
      return false;
    } else {
      return true;
    }
  }
  setAccessModeByInvoiceRevenueType(model: WithdrawalTableItemView, invoiceRevenueTypeOptions: SelectItems[]): FieldAccessing[] {
    if (model.termExtension && !isNullOrUndefOrEmptyGUID(model.termExtensionInvoiceRevenueTypeGUID)) {
      let invoiceRevenueType: InvoiceRevenueTypeItemView = invoiceRevenueTypeOptions.find((t) => t.value == model.termExtensionInvoiceRevenueTypeGUID)
        .rowData as InvoiceRevenueTypeItemView;
      if (isNullOrUndefOrEmptyGUID(invoiceRevenueType.intercompanyTableGUID)) {
        return [{ filedIds: settleTermExtensionGroup, readonly: false }];
      } else {
        return [{ filedIds: settleTermExtensionGroup, readonly: true }];
      }
    } else {
      return [{ filedIds: settleTermExtensionGroup, readonly: true }];
    }
  }
  setAccessModeByMarketing(model: WithdrawalTableItemView): FieldAccessing[] {
    model.marketingComment = '';
    if (model.marketingMarkComment) {
      return [{ filedIds: marketingGroup, readonly: false }];
    } else {
      return [{ filedIds: marketingGroup, readonly: true }];
    }
  }
}
