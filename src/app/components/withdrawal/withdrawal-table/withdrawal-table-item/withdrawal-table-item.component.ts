import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import {
  EmptyGuid,
  ROUTE_RELATED_GEN,
  ROUTE_FUNCTION_GEN,
  ProductType,
  Dimension,
  WithdrawalStatus,
  RefType,
  SuspenseInvoiceType,
  ReceivedFrom,
  ROUTE_REPORT_GEN
} from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, IvzDropdownOnFocusModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { LedgerDimensionItemView, RefIdParm, WithdrawalTableItemView } from 'shared/models/viewModel';
import { WithdrawalTableService } from '../withdrawal-table.service';
import { WithdrawalTableItemCustomComponent } from './withdrawal-table-item-custom.component';
@Component({
  selector: 'withdrawal-table-item',
  templateUrl: './withdrawal-table-item.component.html',
  styleUrls: ['./withdrawal-table-item.component.scss']
})
export class WithdrawalTableItemComponent extends BaseItemComponent<WithdrawalTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  assignmentAgreementTableOptions: SelectItems[] = [];
  buyerAgreementTableOptions: SelectItems[] = [];
  creditAppLineOptions: SelectItems[] = [];
  creditTermOptions: SelectItems[] = [];
  interestCutDayOptions: SelectItems[] = [];
  invoiceRevenueTypeOptions: SelectItems[] = [];
  ledgerDimensionOptions1: SelectItems[] = [];
  ledgerDimensionOptions2: SelectItems[] = [];
  ledgerDimensionOptions3: SelectItems[] = [];
  ledgerDimensionOptions4: SelectItems[] = [];
  ledgerDimensionOptions5: SelectItems[] = [];
  dayOfMonthOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  receiptTempTableOptions: SelectItems[] = [];
  retentionCalculateBaseOptions: SelectItems[] = [];
  documentReasonOptions: SelectItems[] = [];
  employeeActiveOption: SelectItems[] = [];

  constructor(
    private service: WithdrawalTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: WithdrawalTableItemCustomComponent
  ) {
    super();
    this.baseService.service = this.service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {
    this.custom.setTitle();
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {
    this.baseDropdown.getDayOfMonthEnumDropDown(this.dayOfMonthOptions);
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRetentionCalculateBaseEnumDropDown(this.retentionCalculateBaseOptions);
  }
  getById(): Observable<WithdrawalTableItemView> {
    return this.service.getWithdrawalTableById(this.id);
  }
  getInitialData(): Observable<WithdrawalTableItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions(result);
        this.setFunctionOptions(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    let tempModel: WithdrawalTableItemView = this.isUpdateMode ? model : this.model;
    forkJoin([
      this.custom.getAssignmentAgreementTableByWithdrawalDropDown(tempModel),
      this.custom.getBuyerAgreementTableByCreditAppLineDropDown(tempModel.creditAppLineGUID),
      this.baseDropdown.getCreditAppLineByWithdrawalDropDown([tempModel.creditAppTableGUID, tempModel.withdrawalDate]),
      this.baseDropdown.getCreditTermDropDown(),
      this.baseDropdown.getInvoiceRevenueTypeDropDown(),
      this.baseDropdown.getLedgerDimensionDropDown(),
      this.baseDropdown.getReceiptTempTableDropDown(),
      this.baseDropdown.getDocumentReasonDropDown(RefType.WithdrawalTable.toString()),
      this.baseDropdown.getEmployeeFilterActiveStatusDropdown(model?.operReportSignatureGUID)
    ]).subscribe(
      ([
        assignmentAgreementTable,
        buyerAgreementTable,
        creditAppLine,
        creditTerm,
        invoiceRevenueType,
        ledgerDimension,
        receiptTempTable,
        documentReason,
        employeeActiveOption
      ]) => {
        this.assignmentAgreementTableOptions = assignmentAgreementTable;
        this.buyerAgreementTableOptions = buyerAgreementTable;
        this.creditAppLineOptions = creditAppLine;
        this.creditTermOptions = creditTerm;
        this.invoiceRevenueTypeOptions = invoiceRevenueType;
        this.receiptTempTableOptions = receiptTempTable;
        this.documentReasonOptions = documentReason;
        this.setLedgerDimension(ledgerDimension);
        this.employeeActiveOption = employeeActiveOption;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  setRelatedInfoOptions(model: WithdrawalTableItemView): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        command: () => {
          const refIdParm: RefIdParm = { refType: RefType.WithdrawalTable, refGUID: this.model.withdrawalTableGUID };
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: refIdParm
          });
        }
      },
      { label: 'LABEL.MEMO', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.MEMO }) },
      {
        label: 'LABEL.VERIFICATION',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.VERIFICATION_TRANS,
            parameters: {
              refId: model.withdrawalId,
              buyerTableGUID: model.buyerTableGUID,
              creditAppTableGUID: model.creditAppTableGUID
            }
          })
      },
      { label: 'LABEL.PDC', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.PDC }) },
      {
        label: 'LABEL.SERVICE_FEE_TRANSACTIONS',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.SERVICE_FEE_TRANS,
            parameters: {
              refId: model.withdrawalId,
              taxDate: model.withdrawalDate,
              productType: ProductType.ProjectFinance,
              invoiceTable_CustomerTableGUID: model.customerTableGUID
            }
          })
      },
      {
        label: 'LABEL.INVOICE_SETTLEMENT',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.INVOICE_SETTLEMENT,
            parameters: {
              refGUID: model.withdrawalTableGUID,
              refType: RefType.WithdrawalTable,
              refId: model.withdrawalId,
              invoiceTable_CustomerTableGUID: model.customerTableGUID,
              suspenseInvoiceType: SuspenseInvoiceType.None,
              productInvoice: false,
              productType: null,
              receivedFrom: ReceivedFrom.Customer
            }
          }),
        disabled: model.termExtension,
        visible: !model.termExtension
      },
      {
        label: 'LABEL.SUSPENSE_SETTLEMENT',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.SUSPENSE_SETTLEMENT,
            parameters: {
              refGUID: model.withdrawalTableGUID,
              refType: RefType.WithdrawalTable,
              refId: model.withdrawalId,
              invoiceTable_CustomerTableGUID: model.customerTableGUID,
              suspenseInvoiceType: SuspenseInvoiceType.SuspenseAccount,
              productInvoice: false,
              productType: null,
              receivedFrom: ReceivedFrom.Customer
            }
          })
      },
      {
        label: 'LABEL.PAYMENT_DETAIL',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.PAYMENT_DETAIL,
            parameters: {
              refId: model.withdrawalId,
              totalPaymentAmount: model.netPaid,
              customerTableGUID: model.customerTableGUID
            }
          }),
        disabled: model.termExtension,
        visible: !model.termExtension
      },

      { label: 'LABEL.PROJECT_PROGRESS', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.PROJECT_PROGRESS }) },
      {
        label: 'LABEL.BUYER_AGREEMENT_TRANSACTION',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.BUYER_AGREEMENT_TRANS })
      },
      {
        label: 'LABEL.TERM_EXTENSION',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_TERM_EXTENSION }),
        disabled: model.termExtension,
        visible: !model.termExtension
      },
      {
        label: 'LABEL.INQUIRY',
        items: [
          {
            label: 'LABEL.VENDOR_PAYMENT',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.VENDOR_PAYMENT_TRANS,
                parameters: {
                  refId: model.withdrawalId,
                  refGUID: model.withdrawalTableGUID,
                  refType: RefType.WithdrawalTable,
                  tableLabel: this.custom.title
                }
              }),
            disabled: model.termExtension,
            visible: !model.termExtension
          },
          {
            label: 'LABEL.WITHDRAWAL_SETTLEMENT',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.PRODUCT_SETTLEMENT,
                parameters: {
                  refGUID: model.withdrawalTableGUID,
                  refType: RefType.WithdrawalTable,
                  refId: model.withdrawalId,
                  invoiceTable_CustomerTableGUID: model.customerTableGUID,
                  suspenseInvoiceType: SuspenseInvoiceType.SuspenseAccount,
                  productInvoice: false,
                  productType: null,
                  receivedFrom: ReceivedFrom.Customer,
                  originalRefGUID: this.getRefGUID(model)
                }
              })
          },
          {
            label: 'LABEL.COLLECTION',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.RECEIPT_TEMP_TABLE,
                parameters: {
                  refGUID: model.receiptTempTableGUID,
                  refType: RefType.WithdrawalTable
                }
              })
          }
        ]
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  getRefGUID(model: WithdrawalTableItemView): any {
    if (model.termExtension) {
      return model.originalWithdrawalTableGUID;
    } else {
      return model.withdrawalTableGUID;
    }
  }
  setFunctionOptions(model: WithdrawalTableItemView): void {
    const getwithdrawaldisable = this.custom.getWithdrawalDisable(model);
    const getwithdrawalvisible = this.custom.getWithdrawalVisible(model);
    const getwithdrawaltermextensionvisible = this.custom.getWithdrawalTermExtensionVisible(model);
    const getwithdrawaltermextensiondisible = this.custom.getWithdrawalTermExtensionDisable(model);
    this.functionItems = [
      {
        label: 'LABEL.RECALCULATE_INTEREST',
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.RECALCULATE_INTEREST }),
        visible: !model.termExtension && model.documentStatus_StatusId == WithdrawalStatus.Draft
      },
      {
        label: 'LABEL.POST',
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.POST_WITHDRAWAL }),
        disabled: !(model.documentStatus_StatusId == WithdrawalStatus.Draft)
      },
      {
        label: 'LABEL.CANCEL',
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.CANCEL_WITHDRAWAL }),
        disabled: !(model.documentStatus_StatusId == WithdrawalStatus.Draft)
      },
      {
        label: 'LABEL.PRINT',
        items: [
          {
            label: 'LABEL.WITHDRAWAL',
            command: () =>
              this.toReport({
                path: ROUTE_REPORT_GEN.PRINT_WITHDRAWAL
              }),
            disabled: getwithdrawaldisable,
            visible: getwithdrawalvisible
          },
          {
            label: 'LABEL.TERM_EXTENSION_WITHDRAWAL',
            visible: getwithdrawaltermextensionvisible,
            disabled: getwithdrawaltermextensiondisible,
            command: () =>
              this.toReport({
                path: ROUTE_REPORT_GEN.PRINT_WITHDRAWAL_TERM_EXTENSION
              })
          }
        ]
      }
    ];
    super.setFunctionOptionsMapping();
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateWithdrawalTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createWithdrawalTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }

  onCreditAppLineChange() {
    this.custom.setModelByCreditAppLine(this.model, this.creditAppLineOptions);
    this.setBuyerAgreementTableOptionByCreditAppLine();
    this.setAssignmentAgreementTableOption();
  }
  onWithdrawalAmountChange() {
    this.custom.setModelByWithdrawalAmount(this.model);
  }
  onWithdrawalDateChange() {
    this.assignmentAgreementTableOptions.length = 0;
    this.custom.setModelByWithdrawalDate(this.model);
    this.setCreditAppLineOptionByWithdrawalDate();
  }
  onTermExtensionInvoiceRevenueTypeChange() {
    this.custom.setModelByTermExtensionInvoiceRevenueType(this.model);
    super.setBaseFieldAccessing(this.custom.setAccessModeByInvoiceRevenueType(this.model, this.invoiceRevenueTypeOptions));
  }
  onTermExtensionFeeAmountChange() {
    this.custom.setModelByTermExtensionFeeAmount(this.model);
  }
  onOperationChange() {
    super.setBaseFieldAccessing(this.custom.setAccessModeByOperation(this.model));
  }
  onCreditChange() {
    super.setBaseFieldAccessing(this.custom.setAccessModeByCredit(this.model));
  }
  setLedgerDimension(ledgerDimension: SelectItems[]): void {
    this.ledgerDimensionOptions1 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension1);
    this.ledgerDimensionOptions2 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension2);
    this.ledgerDimensionOptions3 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension3);
    this.ledgerDimensionOptions4 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension4);
    this.ledgerDimensionOptions5 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension5);
  }
  onEmployeeTableDropdownFocus(e: IvzDropdownOnFocusModel): void {
    super.setDropdownOptionOnFocus(e, this.model, this.baseDropdown.getEmployeeFilterActiveStatusDropdown()).subscribe((result) => {
      this.employeeActiveOption = result;
    });
  }
  onMarketingChange() {
    super.setBaseFieldAccessing(this.custom.setAccessModeByMarketing(this.model));
  }

  //#region update pattern dropdown
  setCreditAppLineOptionByWithdrawalDate() {
    if (!isNullOrUndefined(this.model.withdrawalDate)) {
      this.baseService.baseDropdown
        .getCreditAppLineByWithdrawalDropDown([this.model.creditAppTableGUID, this.model.withdrawalDate])
        .subscribe((result) => {
          this.creditAppLineOptions = result;
        });
    } else {
      this.creditAppLineOptions.length = 0;
    }
  }
  setBuyerAgreementTableOptionByCreditAppLine() {
    this.model.buyerAgreementTableGUID = null;
    if (!isNullOrUndefOrEmptyGUID(this.model.creditAppLineGUID)) {
      this.baseService.baseDropdown.getBuyerAgeementTableByWithdrawalTableDropDown(this.model.creditAppLineGUID).subscribe((result) => {
        this.buyerAgreementTableOptions = result;
      });
    } else {
      this.buyerAgreementTableOptions.length = 0;
    }
  }
  setAssignmentAgreementTableOption() {
    this.model.assignmentAgreementTableGUID = null;
    if (!isNullOrUndefOrEmptyGUID(this.model.customerTableGUID) && !isNullOrUndefOrEmptyGUID(this.model.buyerTableGUID)) {
      this.baseService.baseDropdown
        .getAssignmentAgreementTableByWithdrawalDropDown([this.model.customerTableGUID, this.model.buyerTableGUID])
        .subscribe((result) => {
          this.assignmentAgreementTableOptions = result;
        });
    } else {
      this.assignmentAgreementTableOptions.length = 0;
    }
  }
  //#endregion
}
