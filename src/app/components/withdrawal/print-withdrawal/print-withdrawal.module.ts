import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrintWithdrawalService } from './print-withdrawal.service';
import { PrintWithdrawalReportCustomComponent } from './print-withdrawal/print-withdrawal-report-custom.component';
import { PrintWithdrawalReportComponent } from './print-withdrawal/print-withdrawal-report.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PrintWithdrawalReportComponent],
  providers: [PrintWithdrawalService, PrintWithdrawalReportCustomComponent, PrintWithdrawalReportCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PrintWithdrawalReportComponent]
})
export class PrintWithdrawalComponentModule {}
