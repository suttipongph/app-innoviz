import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { PrintWithdrawalReportView } from 'shared/models/viewModel';

const firstGroup = [
  'WITHDRAWAL_ID',
  'DOCUMENT_STATUS',
  'CUSTOMER_ID',
  'BUYER_ID',
  'TERM_EXTENSION',
  'NUMBER_OF_EXTENSION',
  'WITHDRAWAL_DATE',
  'DUE_DATE',
  'CREDIT_TERM_ID',
  'TOTAL_INTEREST_PCT',
  'WITHDRAWAL_AMOUNT'
];

export class PrintWithdrawalReportCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: PrintWithdrawalReportView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<PrintWithdrawalReportView> {
    let model = new PrintWithdrawalReportView();
    model.printWithdrawalGUID = EmptyGuid;
    return of(model);
  }
}
