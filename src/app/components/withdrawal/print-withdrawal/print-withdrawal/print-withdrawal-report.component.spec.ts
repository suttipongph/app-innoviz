import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintWithdrawalReportComponent } from './print-withdrawal-report.component';

describe('PrintWithdrawalReportViewComponent', () => {
  let component: PrintWithdrawalReportComponent;
  let fixture: ComponentFixture<PrintWithdrawalReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrintWithdrawalReportComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintWithdrawalReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
