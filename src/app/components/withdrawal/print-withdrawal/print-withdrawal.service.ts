import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PrintWithdrawalReportView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class PrintWithdrawalService {
  serviceKey = 'printWithdrawalGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getPrintWithdrawalToList(search: SearchParameter): Observable<SearchResult<PrintWithdrawalReportView>> {
    const url = `${this.servicePath}/GetPrintWithdrawalList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deletePrintWithdrawal(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeletePrintWithdrawal`;
    return this.dataGateway.delete(url, row);
  }
  getPrintWithdrawalById(id: string): Observable<PrintWithdrawalReportView> {
    const url = `${this.servicePath}/GetPrintWithdrawalById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createPrintWithdrawal(vmModel: PrintWithdrawalReportView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreatePrintWithdrawal`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updatePrintWithdrawal(vmModel: PrintWithdrawalReportView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdatePrintWithdrawal`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
