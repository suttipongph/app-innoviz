import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrintWithdrawalTermExtensionService } from './print-withdrawal-term-extension.service';
import { PrintWithdrawalTermExtensionReportCustomComponent } from './print-withdrawal-term-extension/print-withdrawal-term-extension-report-custom.component';
import { PrintWithdrawalTermExtensionReportComponent } from './print-withdrawal-term-extension/print-withdrawal-term-extension-report.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PrintWithdrawalTermExtensionReportComponent],
  providers: [PrintWithdrawalTermExtensionService, PrintWithdrawalTermExtensionReportCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PrintWithdrawalTermExtensionReportComponent]
})
export class PrintWithdrawalTermExtensionComponentModule {}
