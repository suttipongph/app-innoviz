import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintWithdrawalTermExtensionReportComponent } from './print-withdrawal-term-extension-report.component';

describe('PrintWithdrawalTermExtensionReportViewComponent', () => {
  let component: PrintWithdrawalTermExtensionReportComponent;
  let fixture: ComponentFixture<PrintWithdrawalTermExtensionReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrintWithdrawalTermExtensionReportComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintWithdrawalTermExtensionReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
