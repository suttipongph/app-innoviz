import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PrintWithdrawalTermExtensionReportView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class PrintWithdrawalTermExtensionService {
  serviceKey = 'printWithdrawalTermExtensionGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getPrintWithdrawalTermExtensionToList(search: SearchParameter): Observable<SearchResult<PrintWithdrawalTermExtensionReportView>> {
    const url = `${this.servicePath}/GetPrintWithdrawalTermExtensionList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deletePrintWithdrawalTermExtension(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeletePrintWithdrawalTermExtension`;
    return this.dataGateway.delete(url, row);
  }
  getPrintWithdrawalTermExtensionById(id: string): Observable<PrintWithdrawalTermExtensionReportView> {
    const url = `${this.servicePath}/GetPrintWithdrawalTermExtensionById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createPrintWithdrawalTermExtension(vmModel: PrintWithdrawalTermExtensionReportView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreatePrintWithdrawalTermExtension`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updatePrintWithdrawalTermExtension(vmModel: PrintWithdrawalTermExtensionReportView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdatePrintWithdrawalTermExtension`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
