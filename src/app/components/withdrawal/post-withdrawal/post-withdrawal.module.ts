import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PostWithdrawalService } from './post-withdrawal.service';
import { PostWithdrawalCustomComponent } from './post-withdrawal/post-withdrawal-custom.component';
import { PostWithdrawalComponent } from './post-withdrawal/post-withdrawal.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PostWithdrawalComponent],
  providers: [PostWithdrawalService, PostWithdrawalCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PostWithdrawalComponent]
})
export class PostWithdrawalComponentModule {}
