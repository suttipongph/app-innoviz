import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { PostWithdrawalResultView, PostWithdrawalView } from 'shared/models/viewModel/postWithdrawalView';
@Injectable()
export class PostWithdrawalService {
  serviceKey = 'withdrawalTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getPostWithdrawalById(id: string): Observable<PostWithdrawalView> {
    const url = `${this.servicePath}/GetPostWithdrawalById/id=${id}`;
    return this.dataGateway.get(url);
  }
  postWithdrawal(vmModel: PostWithdrawalView): Observable<PostWithdrawalResultView> {
    const url = `${this.servicePath}/PostWithdrawal`;
    return this.dataGateway.post(url, vmModel);
  }
}
