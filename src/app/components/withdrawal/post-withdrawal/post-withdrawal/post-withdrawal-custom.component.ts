import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { PostWithdrawalView } from 'shared/models/viewModel/postWithdrawalView';

const firstGroup = [
  'WITHDRAWAL_ID',
  'DOCUMENT_STATUS',
  'CUSTOMER_ID',
  'BUYER_ID',
  'TERM_EXTENSION',
  'NUMBER_OF_EXTENSION',
  'WITHDRAWAL_DATE',
  'DUE_DATE',
  'CREDIT_TERM_ID',
  'TOTAL_INTEREST_PCT',
  'WITHDRAWAL_AMOUNT'
];

export class PostWithdrawalCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: PostWithdrawalView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: PostWithdrawalView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<PostWithdrawalView> {
    let model = new PostWithdrawalView();
    model.withdrawalTableGUID = EmptyGuid;
    return of(model);
  }
}
