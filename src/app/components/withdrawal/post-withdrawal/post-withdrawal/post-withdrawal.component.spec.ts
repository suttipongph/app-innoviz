import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostWithdrawalComponent } from './post-withdrawal.component';

describe('PostWithdrawalViewComponent', () => {
  let component: PostWithdrawalComponent;
  let fixture: ComponentFixture<PostWithdrawalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PostWithdrawalComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostWithdrawalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
