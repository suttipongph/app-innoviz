import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  AccessModeView,
  CalcSettleInvoiceAmountResultView,
  CalcSettlePurchaseAmountResultView,
  InvoiceSettlementDetailItemView,
  InvoiceSettlementDetailListView
} from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { CustomerRefundTableItemView } from 'shared/models/viewModel/customerRefundTableItemView';
@Injectable()
export class InvoiceSettlementDetailService {
  serviceKey = 'invoiceSettlementDetailGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getInvoiceSettlementDetailToList(search: SearchParameter): Observable<SearchResult<InvoiceSettlementDetailListView>> {
    const url = `${this.servicePath}/GetInvoiceSettlementDetailList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteInvoiceSettlementDetail(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteInvoiceSettlementDetail`;
    return this.dataGateway.delete(url, row);
  }
  getInvoiceSettlementDetailById(id: string): Observable<InvoiceSettlementDetailItemView> {
    const url = `${this.servicePath}/GetInvoiceSettlementDetailById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createInvoiceSettlementDetail(vmModel: InvoiceSettlementDetailItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateInvoiceSettlementDetail`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateInvoiceSettlementDetail(vmModel: InvoiceSettlementDetailItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateInvoiceSettlementDetail`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(vmModel: InvoiceSettlementDetailItemView): Observable<InvoiceSettlementDetailItemView> {
    const url = `${this.servicePath}/GetInvoiceSettlementDetailInitialData`;
    return this.dataGateway.post(url, vmModel);
  }
  getCalcSettlePurchaseAmount(vmModel: InvoiceSettlementDetailItemView): Observable<CalcSettlePurchaseAmountResultView> {
    const url = `${this.servicePath}/GetCalcSettlePurchaseAmount`;
    return this.dataGateway.post(url, vmModel);
  }
  getCalcSettleInvoiceAmount(vmModel: InvoiceSettlementDetailItemView, editWHT: boolean): Observable<CalcSettleInvoiceAmountResultView> {
    const url = `${this.servicePath}/GetCalcSettleInvoiceAmount/EditWHT=${editWHT}`;
    return this.dataGateway.post(url, vmModel);
  }
  getPurchaseTableAccessMode(purchaseId: string, isWorkflowMode: boolean): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetPurchaseTableAccessMode/purchaseId=${purchaseId}/isWorkflowMode=${isWorkflowMode}`;
    return this.dataGateway.get(url);
  }
  getWithdrawalTableAcessMode(withdrawalId: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetWithdrawalTableAcessMode/withdrawalId=${withdrawalId}`;
    return this.dataGateway.get(url);
  }
  getReceiptTempTableAccessMode(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetReceiptTempTableAccessMode/id=${id}`;
    return this.dataGateway.get(url);
  }
  getCustomerRefundTableAccessMode(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetCustomerRefundTableAccessMode/id=${id}`;
    return this.dataGateway.get(url);
  }
  getCustomerRefundTableById(id: string): Observable<CustomerRefundTableItemView> {
    const url = `${this.servicePath}/GetCustomerRefundTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
}
