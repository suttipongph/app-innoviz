import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InvoiceSettlementDetailListComponent } from './invoice-settlement-detail-list.component';

describe('InvoiceSettlementDetailListViewComponent', () => {
  let component: InvoiceSettlementDetailListComponent;
  let fixture: ComponentFixture<InvoiceSettlementDetailListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InvoiceSettlementDetailListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceSettlementDetailListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
