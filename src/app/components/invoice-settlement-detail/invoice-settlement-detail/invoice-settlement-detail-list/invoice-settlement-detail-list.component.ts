import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { BracketType, ColumnType, Operators, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { InvoiceSettlementDetailListView } from 'shared/models/viewModel';
import { InvoiceSettlementDetailService } from '../invoice-settlement-detail.service';
import { InvoiceSettlementDetailListCustomComponent } from './invoice-settlement-detail-list-custom.component';

@Component({
  selector: 'invoice-settlement-detail-list',
  templateUrl: './invoice-settlement-detail-list.component.html',
  styleUrls: ['./invoice-settlement-detail-list.component.scss']
})
export class InvoiceSettlementDetailListComponent extends BaseListComponent<InvoiceSettlementDetailListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  @Output() deleteEmit = new EventEmitter<boolean>();
  productTypeOptions: SelectItems[] = [];
  invoiceTableOption: SelectItems[] = [];
  invoiceTypeOption: SelectItems[] = [];
  defaultBitOption: SelectItems[] = [];
  suspenseInvoiceTypeOptions: SelectItems[] = [];
  model: InvoiceSettlementDetailListView;
  constructor(
    public custom: InvoiceSettlementDetailListCustomComponent,
    private service: InvoiceSettlementDetailService,
    public currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
  }
  ngOnInit(): void {
    this.accessRightChildPage = 'SuspenseSettlementListPage';
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    const passObject = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setAccessModeByParent(passObject, this.isWorkFlowMode()).subscribe((result) => {
      this.model = result;
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getDefaultBitDropDown(this.defaultBitOption);
    this.baseDropdown.getSuspenseInvoiceTypeEnumDropDown(this.suspenseInvoiceTypeOptions);
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.custom.checkAccessMode(this.accessRightChildPage, this.isRelatedMode));
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getInvoiceTableDropDown(this.invoiceTableOption);
    this.baseDropdown.getInvoiceTypeDropDown(this.invoiceTypeOption);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'invoiceSettlementDetailGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: 'LABEL.DOCUMENT_ID',
        textKey: 'documentId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.INVOICE_ID',
        textKey: 'invoiceTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'invoiceTableGUID',
        sortingKey: 'invoiceTable_InvoiceId',
        masterList: this.invoiceTableOption
      },
      {
        label: 'LABEL.INVOICE_DUE_DATE',
        textKey: 'invoiceDueDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.INVOICE_TYPE_ID',
        textKey: 'invoiceType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'InvoiceTable_InvoiceTypeGUID',
        sortingKey: 'invoiceType_InvoiceTypeId',
        masterList: this.invoiceTypeOption
      },
      {
        label: this.custom.isSuspensesettlement ? 'LABEL.SUSPENSE_INVOICE_TYPE' : null,
        textKey: 'suspenseInvoiceType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.suspenseInvoiceTypeOptions,
        duplicateKey: 'suspenseInvoiceTypeList'
      },
      {
        label: 'LABEL.INVOICE_AMOUNT',
        textKey: 'invoiceAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BALANCE_AMOUNT',
        textKey: 'balanceAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.SETTLE_AMOUNT',
        textKey: 'settleAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: this.custom.isSuspensesettlement ? null : 'LABEL.WHT_AMOUNT',
        textKey: 'whtAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: this.custom.isSuspensesettlement ? null : 'LABEL.WHT_SLIP_RECEIVED_FROM_CUSTOMER',
        textKey: 'whtSlipReceivedByCustomer',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defaultBitOption
      },
      {
        label: this.custom.isSuspensesettlement ? null : 'LABEL.SETTLE_INVOICE_AMOUNT',
        textKey: 'settleInvoiceAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.model.refGUID
      },
      {
        label: null,
        textKey: 'refType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.model.refType.toString()
      },
      {
        label: null,
        textKey: 'suspenseInvoiceType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        bracket: this.custom.suspenseInvoiceTypeCondition2 != null ? BracketType.SingleStart : BracketType.None,
        parentKey: this.custom.suspenseInvoiceType != null ? this.custom.suspenseInvoiceType.toString() : null
      },
      {
        label: null,
        textKey: 'suspenseInvoiceType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        operator: Operators.OR,
        bracket: this.custom.suspenseInvoiceTypeCondition2 != null ? BracketType.SingleEnd : BracketType.None,
        parentKey: this.custom.suspenseInvoiceTypeCondition2 != null ? this.custom.suspenseInvoiceTypeCondition2.toString() : null,
        duplicateKey: 'suspenseInvoiceTypeCondition2'
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<InvoiceSettlementDetailListView>> {
    return this.service.getInvoiceSettlementDetailToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteInvoiceSettlementDetail(row).pipe(tap(() => this.deleteEmit.emit(true))));
  }
}
