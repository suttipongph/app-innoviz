import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { RefType, SuspenseInvoiceType } from 'shared/constants';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessLevelModel, AccessModeView, InvoiceSettlementDetailListView } from 'shared/models/viewModel';
import { InvoiceSettlementDetailService } from '../invoice-settlement-detail.service';
const PURCHASE_TABLE_PURCHASE = 'purchasetablepurchase';
const PURCHASE_TABLE_ROLL_BILL = 'purchasetablerollbill';
const WITHDRAWAL_TABLE = 'withdrawaltablewithdrawal';
const RECEIPT_TEMP_TABLE = 'receipttemptable';
const SUSPENSE_REFUND = 'suspenserefund';
const RETENTION_REFUND = 'retentionrefund';
const RESERVE_REFUND = 'reserverefund';
const allRefund = [SUSPENSE_REFUND, RETENTION_REFUND, RESERVE_REFUND];
const RECEIPT_TEMP_PAYM_DETAIL = 'receipttemppaymdetail-child';
const WITHDRAWAL_TABLE_TERM_EXTENSION = 'withdrawaltabletermextension';

export class InvoiceSettlementDetailListCustomComponent {
  baseService: BaseServiceModel<any>;
  accessModeView: AccessModeView = new AccessModeView();
  parentName: string = null;
  title: string = 'LABEL.INVOICE_SETTLEMENT_DETAIL';
  isSuspensesettlement: boolean = false;
  suspenseInvoiceType: SuspenseInvoiceType = null;
  suspenseInvoiceTypeCondition2: SuspenseInvoiceType = null;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setTitle(): void {
    if (
      window.location.href.match('suspensesettlement') ||
      (allRefund.some((s) => window.location.href.match(s)) && !window.location.href.match('invoicesettlement'))
    ) {
      this.isSuspensesettlement = true;
      this.title = 'LABEL.SUSPENSE_SETTLEMENT_DETAIL';
    }
  }
  checkAccessMode(accessRightChildPage: string, isRelatedMode: boolean): Observable<AccessLevelModel> {
    return !isRelatedMode
      ? this.baseService.accessService.getNestedComponentAccessRight(true, accessRightChildPage)
      : this.baseService.accessService.getAccessRight();
  }
  setAccessModeByParent(model: InvoiceSettlementDetailListView, isWorkflowMode: boolean): Observable<InvoiceSettlementDetailListView> {
    this.setTitle();
    const refund = this.baseService.uiService.getKey('refund');
    const parentKey = this.baseService.uiService.getKey(refund);
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    this.getSuspenseAccountBymasterRoute(model);
    switch (this.parentName) {
      case PURCHASE_TABLE_PURCHASE:
      case PURCHASE_TABLE_ROLL_BILL:
        return this.getPurchaseTableAccessMode(model, isWorkflowMode);
      case WITHDRAWAL_TABLE:
        return this.getWithdrawalTableAcessMode(model);
      case WITHDRAWAL_TABLE_TERM_EXTENSION:
        return this.getWithdrawalTableAcessMode(model);
      case RECEIPT_TEMP_TABLE:
        return this.getReceiptTempTableAccessMode(model);
      case SUSPENSE_REFUND:
        return this.getCustomerRefundTableAccessMode(model);
      case RESERVE_REFUND:
        return this.getCustomerRefundTableAccessMode(model);
      case RECEIPT_TEMP_PAYM_DETAIL:
        return this.getReceiptTempTableAccessMode(model);
      default:
        const newModel: InvoiceSettlementDetailListView = new InvoiceSettlementDetailListView();
        newModel.refGUID = parentKey;
        newModel.refType = RefType.CustomerRefund;
        if (allRefund.some((s) => window.location.href.match(s))) {
          return this.getCustomerRefundTableAccessMode(newModel);
        }
        this.accessModeView.canCreate = false;
        this.accessModeView.canView = false;
        this.accessModeView.canDelete = false;
        return of(model);
    }
  }
  getPurchaseTableAccessMode(model: InvoiceSettlementDetailListView, isWorkflowMode: boolean): Observable<InvoiceSettlementDetailListView> {
    return new Observable((observable) => {
      (this.baseService.service as InvoiceSettlementDetailService).getPurchaseTableAccessMode(model.refGUID, isWorkflowMode).subscribe((result) => {
        this.accessModeView = result;
        this.accessModeView.canView = true;
        observable.next(model);
      });
    });
  }
  getWithdrawalTableAcessMode(model: InvoiceSettlementDetailListView): Observable<InvoiceSettlementDetailListView> {
    return new Observable((observable) => {
      (this.baseService.service as InvoiceSettlementDetailService).getWithdrawalTableAcessMode(model.refGUID).subscribe((result) => {
        this.accessModeView = result;
        this.accessModeView.canView = true;
        observable.next(model);
      });
    });
  }
  getReceiptTempTableAccessMode(model: InvoiceSettlementDetailListView): Observable<InvoiceSettlementDetailListView> {
    return new Observable((observable) => {
      (this.baseService.service as InvoiceSettlementDetailService).getReceiptTempTableAccessMode(model.refGUID).subscribe((result) => {
        this.accessModeView = result;
        this.accessModeView.canView = true;
        observable.next(model);
      });
    });
  }
  getCustomerRefundTableAccessMode(model: InvoiceSettlementDetailListView): Observable<InvoiceSettlementDetailListView> {
    return new Observable((observable) => {
      (this.baseService.service as InvoiceSettlementDetailService).getCustomerRefundTableAccessMode(model.refGUID).subscribe((result) => {
        this.accessModeView = result;
        this.accessModeView.canView = true;
        observable.next(model);
      });
    });
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
  getSuspenseAccountBymasterRoute(model: InvoiceSettlementDetailListView) {
    if (this.isSuspensesettlement) {
      const masterRoute = this.baseService.uiService.getMasterRoute(2);
      switch (masterRoute) {
        case SUSPENSE_REFUND:
          this.suspenseInvoiceType = SuspenseInvoiceType.SuspenseAccount;
          break;
        case RESERVE_REFUND:
          this.suspenseInvoiceType = SuspenseInvoiceType.SuspenseAccount;
          this.suspenseInvoiceTypeCondition2 = SuspenseInvoiceType.ToBeRefunded;
          break;
        case RETENTION_REFUND:
          this.suspenseInvoiceType = SuspenseInvoiceType.SuspenseAccount;
          this.suspenseInvoiceTypeCondition2 = SuspenseInvoiceType.RetentionReturn;
          break;
      }
    } else {
      this.suspenseInvoiceType = model.suspenseInvoiceType;
    }
  }
}
