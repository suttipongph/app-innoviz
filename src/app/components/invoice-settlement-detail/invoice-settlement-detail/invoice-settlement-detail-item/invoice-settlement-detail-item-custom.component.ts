import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ProductType, ReceivedFrom, RefType, SuspenseInvoiceType } from 'shared/constants';
import { isNullOrUndefined, isNullOrUndefOrEmpty, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import {
  AccessModeView,
  CalcSettleInvoiceAmountResultView,
  CalcSettlePurchaseAmountResultView,
  InvoiceSettlementDetailItemView,
  ListInvoiceOutstandingListView
} from 'shared/models/viewModel';
import { InvoiceSettlementDetailService } from '../invoice-settlement-detail.service';

const firstGroup = [
  'PRODUCT_TYPE',
  'DOCUMENT_ID',
  'CREDIT_APP_TABLE_GUID',
  'INVOICE_TABLE_GUID',
  'INVOICE_DUE_DATE',
  'INVOICE_TYPE_GUID',
  'SUSPENSE_INVOICE_TYPE',
  'CURRENCY_GUID',
  'INVOICE_AMOUNT',
  'BALANCE_AMOUNT',
  'SETTLE_INVOICE_AMOUNT',
  'SETTLE_TAX_AMOUNT',
  'BUYER_AGREEMENT_TABLE_GUID',
  'ASSIGNMENT_AGREEMENT_TABLE_GUID',
  'PURCHASE_AMOUNT',
  'PURCHASE_AMOUNT_BALANCE',
  'SETTLE_PURCHASE_AMOUNT',
  'SETTLE_RESERVE_AMOUNT',
  'RETENTION_AMOUNT_ACCUM',
  'REF_TYPE',
  'REF_ID',
  'REF_TEMP_RECEIPT_PAYM_DETAIL_GUID',
  'REF_GUID',
  'INVOICE_SETTLEMENT_DETAIL_GUID'
];
const condition1 = ['WHT_SLIP_RECEIVED_FROM_BUYER'];
const condition2 = ['WHT_AMOUNT', 'WHT_SLIP_RECEIVED_FROM_CUSTOMER'];
//#region  Suspense
// tslint:disable-next-line:variable-name
const condition1_Suspense = ['SETTLE_AMOUNT'];
//#endregion Suspense
const PURCHASE_TABLE_PURCHASE = 'purchasetablepurchase';
const WITHDRAWAL_TABLE = 'withdrawaltablewithdrawal';
const RECEIPT_TEMP_TABLE = 'receipttemptable';
const SUSPENSE_REFUND = 'suspenserefund';
const RETENTION_REFUND = 'retentionrefund';
const RESERVE_REFUND = 'reserverefund';
const allRefund = [SUSPENSE_REFUND, RETENTION_REFUND, RESERVE_REFUND];
const RECEIPT_TEMP_PAYM_DETAIL = 'receipttemppaymdetail-child';
const WITHDRAWAL_TABLE_TERM_EXTENSION = 'withdrawaltabletermextension';
export class InvoiceSettlementDetailItemCustomComponent {
  baseService: BaseServiceModel<any>;
  visibleOutStanding: boolean = true;
  visibleFactoring: boolean = false;
  visibleAssignmentAgreement: boolean = false;
  visibleRetension: boolean = false;
  visibleWhenSuspense: boolean = false;
  invoiceOutstanding: ListInvoiceOutstandingListView;
  model: InvoiceSettlementDetailItemView = new InvoiceSettlementDetailItemView();
  accessModeView: AccessModeView = new AccessModeView();
  parentName: string = null;
  title: string = 'LABEL.INVOICE_SETTLEMENT_DETAIL';
  isReceiptTemp: boolean = false;
  isSuspense: boolean = false;
  getFieldAccessing(model: InvoiceSettlementDetailItemView): Observable<FieldAccessing[]> {
    this.setVisibleGroup(model);
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    if (model.receivedFrom !== ReceivedFrom.Buyer) {
      fieldAccessing.push({ filedIds: condition1, readonly: true });
    }
    if (model.suspenseInvoiceType === SuspenseInvoiceType.ToBeRefunded) {
      fieldAccessing.push({ filedIds: condition1_Suspense, readonly: true });
    }
    fieldAccessing.push({ filedIds: condition2, readonly: model.whtAmount === 0 });

    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: InvoiceSettlementDetailItemView, isWorkFlowMode: boolean): Observable<boolean> {
    return this.setAccessModeByParent(model, isWorkFlowMode).pipe(
      map((result) => (isUpdateMode(model.invoiceSettlementDetailGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  setAccessModeByParent(model: InvoiceSettlementDetailItemView, isWorkflowMode: boolean): Observable<AccessModeView> {
    switch (this.parentName) {
      case PURCHASE_TABLE_PURCHASE:
        return this.getPurchaseTableAccessMode(model.refGUID, isWorkflowMode);
      case WITHDRAWAL_TABLE:
        return this.getWithdrawalTableAcessMode(model.refGUID);
      case WITHDRAWAL_TABLE_TERM_EXTENSION:
        return this.getWithdrawalTableAcessMode(model.refGUID);
      case RECEIPT_TEMP_TABLE:
        this.isReceiptTemp = true;
        return this.getReceiptTempTableAccessMode(model.refGUID);
      case SUSPENSE_REFUND:
        return this.getCustomerRefundTableAccessMode(model.refGUID);
      case RESERVE_REFUND:
        return this.getCustomerRefundTableAccessMode(model.refGUID);
      case RECEIPT_TEMP_PAYM_DETAIL:
        return this.getReceiptTempTableAccessMode(model.refGUID);
      default:
        if (allRefund.some((s) => window.location.href.match(s))) {
          return this.getCustomerRefundTableAccessMode(this.model.refGUID);
        }
        this.accessModeView.canCreate = false;
        this.accessModeView.canView = false;
        this.accessModeView.canDelete = false;
        return of(this.accessModeView);
    }
  }
  getPurchaseTableAccessMode(purchaseId: string, isWorkflowMode: boolean): Observable<AccessModeView> {
    return (this.baseService.service as InvoiceSettlementDetailService).getPurchaseTableAccessMode(purchaseId, isWorkflowMode).pipe(
      tap((result) => {
        this.accessModeView = result;
      })
    );
  }
  getWithdrawalTableAcessMode(parentId: string): Observable<AccessModeView> {
    return (this.baseService.service as InvoiceSettlementDetailService).getWithdrawalTableAcessMode(parentId).pipe(
      tap((result) => {
        this.accessModeView = result;
      })
    );
  }
  getReceiptTempTableAccessMode(parentId: string): Observable<AccessModeView> {
    return (this.baseService.service as InvoiceSettlementDetailService).getReceiptTempTableAccessMode(parentId).pipe(
      tap((result) => {
        this.accessModeView = result;
      })
    );
  }
  getCustomerRefundTableAccessMode(parentId: string): Observable<AccessModeView> {
    return (this.baseService.service as InvoiceSettlementDetailService).getCustomerRefundTableAccessMode(parentId).pipe(
      tap((result) => {
        this.accessModeView = result;
      })
    );
  }

  setVisibleOutStanding(invoiceSettlementDetailGUID: string): void {
    if (isUpdateMode(invoiceSettlementDetailGUID)) {
      this.visibleOutStanding = false;
    } else {
      this.visibleOutStanding = true;
    }
  }
  setVisibleGroup(model: InvoiceSettlementDetailItemView): void {
    this.visibleFactoring = model.invoiceTable_ProductType === ProductType.Factoring && model.invoiceTable_ProductInvoice;
    this.visibleAssignmentAgreement = model.receivedFrom === ReceivedFrom.Buyer;
    this.visibleRetension = model.suspenseInvoiceType === SuspenseInvoiceType.RetentionReturn;
    this.visibleWhenSuspense = window.location.href.match('suspensesettlement') ? true : false;
  }
  setInvoiceOutstanding(model: InvoiceSettlementDetailItemView, invoiceId: string): void {
    model.invoiceTableGUID = invoiceId;
  }
  setValueByPassParameter(model: InvoiceSettlementDetailItemView, invoiceSettlementDetailGUID: string): void {
    this.setTitle();
    this.setVisibleOutStanding(invoiceSettlementDetailGUID);
    if (allRefund.some((s) => window.location.href.match(s)) && isNullOrUndefined(model)) {
      this.getCustomerRefundTableById();
    } else {
      this.model = model;
    }
  }
  setTitle(): void {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    if (window.location.href.match('suspensesettlement')) {
      this.title = 'LABEL.SUSPENSE_SETTLEMENT_DETAIL';
      this.isSuspense = true;
    }
  }
  getInitialData(model: InvoiceSettlementDetailItemView): Observable<InvoiceSettlementDetailItemView> {
    model.refType = this.model.refType;
    model.refGUID = this.model.refGUID;
    model.refId = this.model.refId;
    model.suspenseInvoiceType = this.model.suspenseInvoiceType;
    model.receivedFrom = this.model.receivedFrom;
    model.refReceiptTempPaymDetailGUID = this.model.refReceiptTempPaymDetailGUID;
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    this.visibleWhenSuspense = window.location.href.match('suspensesettlement') ? true : false;
    return (this.baseService.service as InvoiceSettlementDetailService).getInitialData(model);
  }
  getCustomerRefundTableById(): void {
    const refund = this.baseService.uiService.getKey('refund');
    const parentKey = this.baseService.uiService.getKey(refund);
    (this.baseService.service as InvoiceSettlementDetailService).getCustomerRefundTableById(parentKey).subscribe((result) => {
      this.model.refGUID = result.customerRefundTableGUID;
      this.model.refType = RefType.CustomerRefund;
      this.model.refGUID = result.customerRefundTableGUID;
      this.model.refId = result.customerRefundId;
      this.model.suspenseInvoiceType = result.suspenseInvoiceType;
      this.model.receivedFrom = ReceivedFrom.Customer;
    });
  }
  onSettleAmountChange(model: InvoiceSettlementDetailItemView): void {
    if (this.visibleFactoring && !this.isSuspense) {
      // Call Shared method Method071
      (this.baseService.service as InvoiceSettlementDetailService).getCalcSettlePurchaseAmount(model).subscribe((result) => {
        this.setValueFromCalcSettlePurchaseAmount(model, result);
      });
    } else {
      // Call Shared method Method041
      (this.baseService.service as InvoiceSettlementDetailService).getCalcSettleInvoiceAmount(model, false).subscribe((result) => {
        this.setValueFromCalcSettleInvoiceAmount(model, result);
      });
    }
  }
  onWHTAmountChange(model: InvoiceSettlementDetailItemView): void {
    // Call Shared method Method041
    (this.baseService.service as InvoiceSettlementDetailService).getCalcSettleInvoiceAmount(model, true).subscribe((result) => {
      this.setValueFromCalcSettleInvoiceAmount(model, result);
    });
  }
  setValueFromCalcSettlePurchaseAmount(model: InvoiceSettlementDetailItemView, result: CalcSettlePurchaseAmountResultView): void {
    model.balanceAmount = result.balanceAmount;
    model.purchaseAmount = result.purchaseAmount;
    model.purchaseAmountBalance = result.purchaseAmountBalance;
    model.settleReserveAmount = result.settleReserveAmount;
    model.settlePurchaseAmount = result.settlePurchaseAmount;
    model.settleInvoiceAmount = result.settleInvoiceAmount;
    model.settleTaxAmount = result.settleTaxAmount;
    model.whtAmount = result.whtAmount;
  }
  setValueFromCalcSettleInvoiceAmount(model: InvoiceSettlementDetailItemView, result: CalcSettleInvoiceAmountResultView): void {
    model.balanceAmount = result.balanceAmount;
    model.settleInvoiceAmount = result.settleInvoiceAmount;
    model.settleTaxAmount = result.settleTaxAmount;
    if (!this.visibleWhenSuspense) {
      model.whtAmount = result.whtAmount;
      model.settleAmount = result.settleAmount;
    }
  }
  onSettleAmountValidation(model: InvoiceSettlementDetailItemView): any {
    if (model.balanceAmount > 0 && model.settleAmount > model.balanceAmount) {
      return {
        code: 'ERROR.90031',
        parameters: [this.baseService.translate.instant('LABEL.SETTLE_AMOUNT'), model.balanceAmount]
      };
    } else if (model.balanceAmount < 0 && model.settleAmount < model.balanceAmount) {
      return {
        code: 'ERROR.90031',
        parameters: [this.baseService.translate.instant('LABEL.SETTLE_AMOUNT'), model.balanceAmount]
      };
    } else if ((model.balanceAmount > 0 && model.settleAmount < 0) || (model.balanceAmount < 0 && model.settleAmount > 0)) {
      return {
        code: 'ERROR.90080',
        parameters: [this.baseService.translate.instant('LABEL.BALANCE_AMOUNT'), this.baseService.translate.instant('LABEL.SETTLE_AMOUNT')]
      };
    } else {
      return null;
    }
  }
  onWHTAmountValidation(model: InvoiceSettlementDetailItemView): any {
    if (model.whtAmount > model.invoiceTable_WHTBalance) {
      return {
        code: 'ERROR.90031',
        parameters: [this.baseService.translate.instant('LABEL.WHT_AMOUNT'), model.invoiceTable_WHTBalance]
      };
    } else if ((model.balanceAmount > 0 && model.settleAmount < 0) || (model.balanceAmount < 0 && model.settleAmount > 0)) {
      return {
        code: 'ERROR.90080',
        parameters: [this.baseService.translate.instant('LABEL.BALANCE_AMOUNT'), this.baseService.translate.instant('LABEL.WHT_AMOUNT')]
      };
    } else {
      return null;
    }
  }
  onSettleInvoiceAmountValidation(model: InvoiceSettlementDetailItemView): any {
    if (model.balanceAmount > 0 && model.settleInvoiceAmount > model.balanceAmount) {
      return {
        code: 'ERROR.90031',
        parameters: [this.baseService.translate.instant('LABEL.SETTLE_INVOICE_AMOUNT'), model.balanceAmount]
      };
    } else if (model.balanceAmount < 0 && model.settleInvoiceAmount < model.balanceAmount) {
      return {
        code: 'ERROR.90131',
        parameters: [this.baseService.translate.instant('LABEL.SETTLE_INVOICE_AMOUNT'), model.balanceAmount]
      };
    } else {
      return null;
    }
  }
}
