import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, RowIdentity, SelectItems } from 'shared/models/systemModel';
import { InvoiceSettlementDetailItemView } from 'shared/models/viewModel';
import { InvoiceSettlementDetailService } from '../invoice-settlement-detail.service';
import { InvoiceSettlementDetailItemCustomComponent } from './invoice-settlement-detail-item-custom.component';
@Component({
  selector: 'invoice-settlement-detail-item',
  templateUrl: './invoice-settlement-detail-item.component.html',
  styleUrls: ['./invoice-settlement-detail-item.component.scss']
})
export class InvoiceSettlementDetailItemComponent extends BaseItemComponent<InvoiceSettlementDetailItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
    this.outStandingPageInfo = this.childPaths[0];
  }
  interestCalculationMethodOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  suspenseInvoiceTypeOptions: SelectItems[] = [];
  outStandingPageInfo: PageInformationModel = null;
  constructor(
    private service: InvoiceSettlementDetailService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: InvoiceSettlementDetailItemCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setValueByPassParameter(passingObj, this.id);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getInterestCalculationMethodEnumDropDown(this.interestCalculationMethodOptions);
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.baseDropdown.getSuspenseInvoiceTypeEnumDropDown(this.suspenseInvoiceTypeOptions);
  }
  getById(): Observable<InvoiceSettlementDetailItemView> {
    return this.service.getInvoiceSettlementDetailById(this.id);
  }
  getInitialData(): Observable<InvoiceSettlementDetailItemView> {
    return this.custom.getInitialData(this.model);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions(result);
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
      this.setMenuVisibilty();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model, this.isWorkFlowMode()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(model: InvoiceSettlementDetailItemView): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.PRODUCT_SETTLEMENT',
        visible: this.custom.isReceiptTemp && !this.custom.isSuspense && model.invoiceTable_ProductInvoice,
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.PRODUCT_SETTLEMENT,
            parameters: { invoiceSettlementDetailGUID: this.model.invoiceSettlementDetailGUID }
          })
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateInvoiceSettlementDetail(this.model), isColsing);
    } else {
      super.onCreate(this.service.createInvoiceSettlementDetail(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onSubmitList(row: RowIdentity): void {
    this.custom.setInvoiceOutstanding(this.model, row.guid);
    this.setInitialCreatingData();
    this.custom.visibleOutStanding = false;
  }
  onSettleAmountChange(): void {
    this.custom.onSettleAmountChange(this.model);
  }
  onWHTAmountChange(): void {
    this.custom.onWHTAmountChange(this.model);
  }
  onSettleAmountValidation(): void {
    return this.custom.onSettleAmountValidation(this.model);
  }
  onWHTAmountValidation(): void {
    return this.custom.onWHTAmountValidation(this.model);
  }
  onSettleInvoiceAmountValidation(): void {
    return this.custom.onSettleInvoiceAmountValidation(this.model);
  }
}
