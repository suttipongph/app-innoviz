import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceSettlementDetailItemComponent } from './invoice-settlement-detail-item.component';

describe('InvoiceSettlementDetailItemViewComponent', () => {
  let component: InvoiceSettlementDetailItemComponent;
  let fixture: ComponentFixture<InvoiceSettlementDetailItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InvoiceSettlementDetailItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceSettlementDetailItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
