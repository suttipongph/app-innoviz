import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InvoiceSettlementDetailService } from './invoice-settlement-detail.service';
import { InvoiceSettlementDetailListComponent } from './invoice-settlement-detail-list/invoice-settlement-detail-list.component';
import { InvoiceSettlementDetailItemComponent } from './invoice-settlement-detail-item/invoice-settlement-detail-item.component';
import { InvoiceSettlementDetailItemCustomComponent } from './invoice-settlement-detail-item/invoice-settlement-detail-item-custom.component';
import { InvoiceSettlementDetailListCustomComponent } from './invoice-settlement-detail-list/invoice-settlement-detail-list-custom.component';
import { ListInvoiceOutstandingComponentModule } from 'components/list-invoice-outstanding/list-invoice-outstanding/list-invoice-outstanding.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, ListInvoiceOutstandingComponentModule],
  declarations: [InvoiceSettlementDetailListComponent, InvoiceSettlementDetailItemComponent],
  providers: [InvoiceSettlementDetailService, InvoiceSettlementDetailItemCustomComponent, InvoiceSettlementDetailListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [InvoiceSettlementDetailListComponent, InvoiceSettlementDetailItemComponent]
})
export class InvoiceSettlementDetailComponentModule {}
