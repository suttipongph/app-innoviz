import { Observable, of } from 'rxjs';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { SendVendorInvoiceStagingView } from 'shared/models/viewModel';

const firstGroup = ['DOCUMENT_ID', 'NUMBER'];

export class SendVendorInvoiceStagingCustomComponent {
  baseService: BaseServiceModel<any>;
  refId: string = '';
  refType: number = 0;
  refGUID: string = '';
  tableLabel: string = '';
  getFieldAccessing(model: SendVendorInvoiceStagingView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<SendVendorInvoiceStagingView> {
    let model = new SendVendorInvoiceStagingView();
    model.refGUID = this.refGUID;
    model.refType = this.refType;
    model.documentId = this.refId;
    return this.baseService.service.getSendVendorInvoiceStagingInitialData(model);
  }
  setPassParameter(passingObj: SendVendorInvoiceStagingView): void {
    if (!isNullOrUndefined(passingObj.refGUID)) {
      this.refGUID = passingObj.refGUID;
    }
    if (!isNullOrUndefined(passingObj.refType)) {
      this.refType = passingObj.refType;
    }
    if (!isNullOrUndefined(passingObj.documentId)) {
      this.refId = passingObj.documentId;
    }
    if (!isNullOrUndefined(passingObj.callerTableLabel)) {
      this.tableLabel = passingObj.callerTableLabel;
    }
  }
}
