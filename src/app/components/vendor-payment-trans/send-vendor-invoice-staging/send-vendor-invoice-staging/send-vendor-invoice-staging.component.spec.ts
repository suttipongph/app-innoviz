import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendVendorInvoiceStagingComponent } from './send-vendor-invoice-staging.component';

describe('SendVendorInvoiceStagingViewComponent', () => {
  let component: SendVendorInvoiceStagingComponent;
  let fixture: ComponentFixture<SendVendorInvoiceStagingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SendVendorInvoiceStagingComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendVendorInvoiceStagingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
