import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SendVendorInvoiceStagingService } from './send-vendor-invoice-staging.service';
import { SendVendorInvoiceStagingComponent } from './send-vendor-invoice-staging/send-vendor-invoice-staging.component';
import { SendVendorInvoiceStagingCustomComponent } from './send-vendor-invoice-staging/send-vendor-invoice-staging-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [SendVendorInvoiceStagingComponent],
  providers: [SendVendorInvoiceStagingService, SendVendorInvoiceStagingCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [SendVendorInvoiceStagingComponent]
})
export class SendVendorInvoiceStagingComponentModule {}
