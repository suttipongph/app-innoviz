import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { SendVendorInvoiceStagingView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class SendVendorInvoiceStagingService {
  serviceKey = 'sendVendorInvoiceStagingGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getSendVendorInvoiceStagingToList(search: SearchParameter): Observable<SearchResult<SendVendorInvoiceStagingView>> {
    const url = `${this.servicePath}/GetSendVendorInvoiceStagingList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteSendVendorInvoiceStaging(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteSendVendorInvoiceStaging`;
    return this.dataGateway.delete(url, row);
  }
  getSendVendorInvoiceStagingById(id: string): Observable<SendVendorInvoiceStagingView> {
    const url = `${this.servicePath}/GetSendVendorInvoiceStagingById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createSendVendorInvoiceStaging(vmModel: SendVendorInvoiceStagingView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateSendVendorInvoiceStaging`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateSendVendorInvoiceStaging(vmModel: SendVendorInvoiceStagingView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateSendVendorInvoiceStaging`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  sendVendorInvoiceStaging(vwModel: SendVendorInvoiceStagingView): Observable<boolean> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vwModel);
  }
  getSendVendorInvoiceStagingInitialData(vwModel: SendVendorInvoiceStagingView): Observable<SendVendorInvoiceStagingView> {
    const url = `${this.servicePath}/GetSendVendorInvoiceStagingInitialData`;
    return this.dataGateway.post(url, vwModel);
  }
}
