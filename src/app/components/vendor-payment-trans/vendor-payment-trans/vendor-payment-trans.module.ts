import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VendorPaymentTransService } from './vendor-payment-trans.service';
import { VendorPaymentTransListComponent } from './vendor-payment-trans-list/vendor-payment-trans-list.component';
import { VendorPaymentTransItemComponent } from './vendor-payment-trans-item/vendor-payment-trans-item.component';
import { VendorPaymentTransItemCustomComponent } from './vendor-payment-trans-item/vendor-payment-trans-item-custom.component';
import { VendorPaymentTransListCustomComponent } from './vendor-payment-trans-list/vendor-payment-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [VendorPaymentTransListComponent, VendorPaymentTransItemComponent],
  providers: [VendorPaymentTransService, VendorPaymentTransItemCustomComponent, VendorPaymentTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [VendorPaymentTransListComponent, VendorPaymentTransItemComponent]
})
export class VendorPaymentTransComponentModule {}
