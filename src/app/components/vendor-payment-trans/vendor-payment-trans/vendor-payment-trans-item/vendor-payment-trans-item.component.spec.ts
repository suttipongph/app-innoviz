import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorPaymentTransItemComponent } from './vendor-payment-trans-item.component';

describe('VendorPaymentTransItemViewComponent', () => {
  let component: VendorPaymentTransItemComponent;
  let fixture: ComponentFixture<VendorPaymentTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VendorPaymentTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorPaymentTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
