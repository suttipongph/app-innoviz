import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { VendorPaymentTransItemView } from 'shared/models/viewModel';

const firstGroup = [
  'CREDIT_APP_TABLE_GUID',
  'VENDOR_TABLE_GUID',
  'PAYMENT_DATE',
  'OFFSET_ACCOUNT',
  'TAX_TABLE_GUID',
  'AMOUNT_BEFORE_TAX',
  'TAX_AMOUNT',
  'TOTAL_AMOUNT',
  'DOCUMENT_STATUS_GUID',
  'DIMENSION1',
  'DIMENSION2',
  'DIMENSION3',
  'DIMENSION4',
  'DIMENSION5',
  'REF_TYPE',
  'REF_ID',
  'REF_GUID',
  'VENDOR_PAYMENT_TRANS_GUID',
  'VENDOR_TAX_INVOICE_ID'
];

export class VendorPaymentTransItemCustomComponent {
  baseService: BaseServiceModel<any>;
  refId: string = null;
  getFieldAccessing(model: VendorPaymentTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: VendorPaymentTransItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(): Observable<VendorPaymentTransItemView> {
    let model = new VendorPaymentTransItemView();
    model.vendorPaymentTransGUID = EmptyGuid;
    return of(model);
  }
  setPassParameter(passingObj: VendorPaymentTransItemView) {
    if (!isNullOrUndefined(passingObj.refId)) {
      this.refId = passingObj.refId;
    }
  }
  setModelByParameter(model: VendorPaymentTransItemView): void {
    if (isNullOrUndefined(model.vendorPaymentTransGUID)) {
      model.refId = this.refId;
    }
  }
}
