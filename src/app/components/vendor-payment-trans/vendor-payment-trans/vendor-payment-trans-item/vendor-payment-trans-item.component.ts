import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, Dimension } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { LedgerDimensionItemView, VendorPaymentTransItemView } from 'shared/models/viewModel';
import { VendorPaymentTransService } from '../vendor-payment-trans.service';
import { VendorPaymentTransItemCustomComponent } from './vendor-payment-trans-item-custom.component';
@Component({
  selector: 'vendor-payment-trans-item',
  templateUrl: './vendor-payment-trans-item.component.html',
  styleUrls: ['./vendor-payment-trans-item.component.scss']
})
export class VendorPaymentTransItemComponent extends BaseItemComponent<VendorPaymentTransItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  creditAppTableOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  taxTableOptions: SelectItems[] = [];
  vendorTableOptions: SelectItems[] = [];
  dimension1Options: SelectItems[] = [];
  dimension2Options: SelectItems[] = [];
  dimension3Options: SelectItems[] = [];
  dimension4Options: SelectItems[] = [];
  dimension5Options: SelectItems[] = [];

  constructor(
    private service: VendorPaymentTransService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: VendorPaymentTransItemCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {
    let passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setPassParameter(passingObj);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<VendorPaymentTransItemView> {
    return this.service.getVendorPaymentTransById(this.id);
  }
  getInitialData(): Observable<VendorPaymentTransItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.custom.setModelByParameter(this.model);
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(
      this.baseDropdown.getCreditAppTableDropDown(),
      this.baseDropdown.getDocumentStatusDropDown(),
      this.baseDropdown.getLedgerDimensionDropDown(),
      this.baseDropdown.getTaxTableDropDown(),
      this.baseDropdown.getVendorTableDropDown()
    ).subscribe(
      ([creditAppTable, documentStatus, ledgerDimension, taxTable, vendorTable]) => {
        this.creditAppTableOptions = creditAppTable;
        this.documentStatusOptions = documentStatus;
        this.dimension1Options = ledgerDimension.filter((t) => (t.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension1);
        this.dimension2Options = ledgerDimension.filter((t) => (t.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension2);
        this.dimension3Options = ledgerDimension.filter((t) => (t.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension3);
        this.dimension4Options = ledgerDimension.filter((t) => (t.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension4);
        this.dimension5Options = ledgerDimension.filter((t) => (t.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension5);
        this.taxTableOptions = taxTable;
        this.vendorTableOptions = vendorTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          this.custom.setModelByParameter(this.model);
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateVendorPaymentTrans(this.model), isColsing);
    } else {
      super.onCreate(this.service.createVendorPaymentTrans(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
