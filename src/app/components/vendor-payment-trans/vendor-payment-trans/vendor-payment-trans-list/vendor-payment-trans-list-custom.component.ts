import { Observable } from 'rxjs';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel } from 'shared/models/systemModel';
const RESERVE_REFUND = 'reserverefund';
const RETENTION_REFUND = 'retentionrefund';
const SUSPENSE_REFUND = 'suspenserefund';

export class VendorPaymentTransListCustomComponent {
  baseService: BaseServiceModel<any>;
  refGUID: string = '';
  refType: number = 0;
  refId: string = '';
  tableLabel: string = '';
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  getSendVendorVisible(): boolean {
    const parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (parentName) {
      case ROUTE_RELATED_GEN.PURCHASE_TABLE_PURCHASE:
      case ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_WITHDRAWAL:
      case RESERVE_REFUND:
      case RETENTION_REFUND:
      case SUSPENSE_REFUND:
        return true;
      default:
        return false;
    }
  }
  setPassParameter(passingObj: any): void {
    if (!isNullOrUndefined(passingObj.refGUID)) {
      this.refGUID = passingObj.refGUID;
    }
    if (!isNullOrUndefined(passingObj.refType)) {
      this.refType = passingObj.refType;
    }
    if (!isNullOrUndefined(passingObj.refId)) {
      this.refId = passingObj.refId;
    }
    if (!isNullOrUndefined(passingObj.tableLabel)) {
      this.tableLabel = passingObj.tableLabel;
    }
  }
}
