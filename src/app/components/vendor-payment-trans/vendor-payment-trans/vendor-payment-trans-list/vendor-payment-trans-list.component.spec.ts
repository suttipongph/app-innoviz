import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { VendorPaymentTransListComponent } from './vendor-payment-trans-list.component';

describe('VendorPaymentTransListViewComponent', () => {
  let component: VendorPaymentTransListComponent;
  let fixture: ComponentFixture<VendorPaymentTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VendorPaymentTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorPaymentTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
