import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, ROUTE_FUNCTION_GEN, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { SendVendorInvoiceStagingView, VendorPaymentTransListView } from 'shared/models/viewModel';
import { VendorPaymentTransService } from '../vendor-payment-trans.service';
import { VendorPaymentTransListCustomComponent } from './vendor-payment-trans-list-custom.component';

@Component({
  selector: 'vendor-payment-trans-list',
  templateUrl: './vendor-payment-trans-list.component.html',
  styleUrls: ['./vendor-payment-trans-list.component.scss']
})
export class VendorPaymentTransListComponent extends BaseListComponent<VendorPaymentTransListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  creditAppTableOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  ledgerDimensionOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  taxTableOptions: SelectItems[] = [];
  vendorTableOptions: SelectItems[] = [];

  constructor(
    public custom: VendorPaymentTransListCustomComponent,
    private service: VendorPaymentTransService,
    public uiControllerService: UIControllerService,
    private currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setPassParameter(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.setFunctionOptions();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCreditAppTableDropDown(this.creditAppTableOptions);
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
    this.baseDropdown.getVendorTableDropDown(this.vendorTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'vendorPaymentTransGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.CREDIT_APPLICATION_ID',
        textKey: 'creditAppTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditAppTableOptions,
        searchingKey: 'creditAppTableGUID',
        sortingKey: 'creditAppTable_CreditAppId'
      },
      {
        label: 'LABEL.VENDOR_ID',
        textKey: 'vendorTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.vendorTableOptions,
        searchingKey: 'vendorTableGUID',
        sortingKey: 'vendorTable_VendorId'
      },
      {
        label: 'LABEL.TRANSACTION_DATE',
        textKey: 'paymentDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.OFFSET_ACCOUNT',
        textKey: 'offsetAccount',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.TOTAL_AMOUNT',
        textKey: 'totalAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentStatusOptions,
        sortingKey: 'documentStatus_StatusId',
        searchingKey: 'documentStatusGUID'
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<VendorPaymentTransListView>> {
    return this.service.getVendorPaymentTransToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteVendorPaymentTrans(row));
  }
  setFunctionOptions(): void {
    const sendVendorVisible = this.custom.getSendVendorVisible();
    this.functionItems = [
      {
        label: 'LABEL.SEND_VENDOR_INVOICE_STAGING',
        command: () => {
          const parm: SendVendorInvoiceStagingView = new SendVendorInvoiceStagingView();
          parm.refGUID = this.custom.refGUID;
          parm.refType = this.custom.refType;
          parm.documentId = this.custom.refId;
          parm.callerTableLabel = this.custom.tableLabel;
          this.service.validateSendVendorInvoiceStagingMenuBtn(parm).subscribe((result) => {
            if (result) {
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.SEND_VENDOR_INVOICE_STAGING,
                parameters: parm
              });
            }
          });
        },
        visible: sendVendorVisible
      }
    ];
    super.setFunctionOptionsMapping();
  }
}
