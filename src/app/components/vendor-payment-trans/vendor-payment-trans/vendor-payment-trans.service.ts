import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { SendVendorInvoiceStagingView, VendorPaymentTransItemView, VendorPaymentTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class VendorPaymentTransService {
  serviceKey = 'vendorPaymentTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getVendorPaymentTransToList(search: SearchParameter): Observable<SearchResult<VendorPaymentTransListView>> {
    const url = `${this.servicePath}/GetVendorPaymentTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteVendorPaymentTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteVendorPaymentTrans`;
    return this.dataGateway.delete(url, row);
  }
  getVendorPaymentTransById(id: string): Observable<VendorPaymentTransItemView> {
    const url = `${this.servicePath}/GetVendorPaymentTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createVendorPaymentTrans(vmModel: VendorPaymentTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateVendorPaymentTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateVendorPaymentTrans(vmModel: VendorPaymentTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateVendorPaymentTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  validateSendVendorInvoiceStagingMenuBtn(vmModel: SendVendorInvoiceStagingView): Observable<boolean> {
    const url = `${this.servicePath}/ValidateSendVendorInvoiceStagingMenuBtn`;
    return this.dataGateway.post(url, vmModel);
  }
}
