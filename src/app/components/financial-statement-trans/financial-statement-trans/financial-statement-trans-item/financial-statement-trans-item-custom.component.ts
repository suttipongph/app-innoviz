import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { CreditAppRequestStatus, RefType } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, RefTypeAndCreditAppRequestTableModel, RefTypeModel } from 'shared/models/systemModel';
import { AccessModeView, FinancialStatementTransItemView } from 'shared/models/viewModel';

const firstGroup = ['REF_TYPE', 'REF_ID'];
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const CUSTOMER_TABLE = 'customertable';
const BUYER_TABLE = 'buyertable';
const CREDITAPP_TABLE = 'creditapptable';
const AMEND_CA = 'amendca';
const AMEND_CA_LINE = 'amendcaline';
const REVIEW_ACTIVE_CREDIT_APP_LINE = 'reviewactivecreditappline';
const REVIEW_CREDIT_APP_REQUEST = 'reviewcreditapprequest';
const BUYER_MATCHING = 'buyermatching';
const LOAN_REQUEST = 'loanrequest';

export class FinancialStatementTransItemCustomComponent {
  refGUID: string;
  refType: RefType;
  baseService: BaseServiceModel<any>;
  parentName = null;
  parentId = null;
  parentAmendCa = null;
  accessModeView: AccessModeView = new AccessModeView();
  isBuyerMatching: boolean = false;
  isLoanRequest: boolean = false;
  creditAppRequestTableGUID: string = null;
  getFieldAccessing(model: FinancialStatementTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: FinancialStatementTransItemView, isWorkFlowMode: boolean): Observable<boolean> {
    return this.setAccessModeByParentStatus(model, isWorkFlowMode).pipe(
      map((result) => (isUpdateMode(model.financialStatementTransGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  setAccessModeByParentStatus(model: FinancialStatementTransItemView, isWorkFlowMode: boolean): Observable<AccessModeView> {
    this.parentId = this.baseService.uiService.getRelatedInfoOriginTableKey();
    this.parentName = this.baseService.uiService.getRelatedInfoOriginTableName();
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        return this.getAccessModeByCreditAppRequestTable(this.parentId, isWorkFlowMode);
      case CREDITAPP_TABLE:
        if (this.parentAmendCa === AMEND_CA) {
          return this.getAccessModeByCreditAppRequestTable(this.refGUID, isWorkFlowMode);
        } else if (this.parentAmendCa === AMEND_CA_LINE) {
          return this.getAccessModeByCreditAppRequestTable(this.creditAppRequestTableGUID, isWorkFlowMode);
        } else {
          this.checkIsBuyerMacthingOrLoanRequest();
          return this.getAccessModeByCreditAppRequestTable(this.parentId, isWorkFlowMode);
        }
      case REVIEW_ACTIVE_CREDIT_APP_LINE:
        if (this.parentAmendCa === REVIEW_CREDIT_APP_REQUEST || this.parentAmendCa === AMEND_CA_LINE) {
          return this.getReviewCreditAppAccessModeByCreditAppRequestTable(this.creditAppRequestTableGUID);
        } else {
          return this.getReviewCreditAppAccessModeByCreditAppRequestTable(this.parentId);
        }
      case CUSTOMER_TABLE:
        this.accessModeView.canCreate = true;
        this.accessModeView.canDelete = true;
        this.accessModeView.canView = true;
        return of(this.accessModeView);
      case BUYER_TABLE:
        this.accessModeView.canCreate = true;
        this.accessModeView.canDelete = true;
        this.accessModeView.canView = true;
        return of(this.accessModeView);
      default:
        return of(this.accessModeView);
    }
  }
  getInnitialDataCustom(id: string): Observable<FinancialStatementTransItemView> {
    switch (this.parentName) {
      case CUSTOMER_TABLE:
        return this.baseService.service.getInitialData(id);
      case CREDITAPP_TABLE:
        if (this.parentAmendCa === AMEND_CA || this.parentAmendCa === AMEND_CA_LINE) {
          return this.baseService.service.getInitialData(this.refGUID);
        } else {
          return this.baseService.service.getInitialData(id);
        }
      case REVIEW_ACTIVE_CREDIT_APP_LINE:
        if (this.parentAmendCa === REVIEW_CREDIT_APP_REQUEST) {
          return this.baseService.service.getInitialData(this.refGUID);
        } else {
          return this.baseService.service.getInitialData(id);
        }
      default:
        return this.baseService.service.getInitialData(id);
    }
  }

  setRefTypeRefGUID(passingObj: RefTypeAndCreditAppRequestTableModel): void {
    this.refType = passingObj.refType;
    this.refGUID = passingObj.refGUID;
    this.creditAppRequestTableGUID = passingObj.creditAppRequestTableGUID;
  }
  checkIsBuyerMacthingOrLoanRequest(): void {
    this.isBuyerMatching = window.location.href.match(BUYER_MATCHING) ? true : false;
    this.isLoanRequest = window.location.href.match(LOAN_REQUEST) ? true : false;
    if (this.isBuyerMatching) {
      this.parentId = this.baseService.uiService.getKey(BUYER_MATCHING);
    } else if (this.isLoanRequest) {
      this.parentId = this.baseService.uiService.getKey(LOAN_REQUEST);
    }
  }

  getReviewCreditAppAccessModeByCreditAppRequestTable(parentId: string) {
    return this.baseService.service.getReviewCreditAppAccessModeByCreditAppRequestTable(parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
        this.accessModeView.canCreate = this.accessModeView.canCreate;
        this.accessModeView.canDelete = this.accessModeView.canDelete;
        this.accessModeView.canView = this.accessModeView.canView;
      })
    );
  }

  getAccessModeByCreditAppRequestTable(parentId: string, isWorkFlowMode: boolean): Observable<AccessModeView> {
    return this.baseService.service.getAccessModeByCreditAppRequestTable(parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
        const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
        this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
        this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
        this.accessModeView.canView = isDraftStatus ? true : this.accessModeView.canView && isWorkFlowMode;
      })
    );
  }
}
