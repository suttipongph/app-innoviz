import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinancialStatementTransItemComponent } from './financial-statement-trans-item.component';

describe('FinancialStatementTransItemViewComponent', () => {
  let component: FinancialStatementTransItemComponent;
  let fixture: ComponentFixture<FinancialStatementTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FinancialStatementTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialStatementTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
