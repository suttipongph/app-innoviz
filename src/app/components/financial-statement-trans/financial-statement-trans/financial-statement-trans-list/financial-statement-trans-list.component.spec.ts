import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FinancialStatementTransListComponent } from './financial-statement-trans-list.component';

describe('FinancialStatementTransListViewComponent', () => {
  let component: FinancialStatementTransListComponent;
  let fixture: ComponentFixture<FinancialStatementTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FinancialStatementTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialStatementTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
