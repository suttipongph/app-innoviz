import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, RefType, ROUTE_FUNCTION_GEN, SortType } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { FinancialStatementTransListView } from 'shared/models/viewModel';
import { FinancialStatementTransService } from '../financial-statement-trans.service';
import { FinancialStatementTransListCustomComponent } from './financial-statement-trans-list-custom.component';

const AMEND_CA = 'amendca';
@Component({
  selector: 'financial-statement-trans-list',
  templateUrl: './financial-statement-trans-list.component.html',
  styleUrls: ['./financial-statement-trans-list.component.scss']
})
export class FinancialStatementTransListComponent extends BaseListComponent<FinancialStatementTransListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  refTypeOptions: SelectItems[] = [];

  constructor(
    public custom: FinancialStatementTransListCustomComponent,
    public service: FinancialStatementTransService,
    private currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
    this.custom.originalPathName = this.uiService.getRelatedInfoOriginTableName();
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
  }
  ngOnInit(): void {
    this.functionItems = [];
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setRefTypeRefGUID(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.setAccessModeByParentStatus(this.isWorkFlowMode()).subscribe((result) => {
      this.setDataGridOption();
      this.setFunctionOptions();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'financialStatementTransGUID';
    const columns: ColumnModel[] = [
      {
        label: this.translate.instant('LABEL.YEAR'),
        textKey: 'year',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.DESC,
        format: this.YEAR,
        sortingKey: 'year'
      },
      {
        label: this.translate.instant('LABEL.ORDERING'),
        textKey: 'ordering',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.ASC,
        sortingKey: 'ordering'
      },
      {
        label: this.translate.instant('LABEL.DESCRIPTION'),
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: this.translate.instant('LABEL.AMOUNT'),
        textKey: 'amount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: isNullOrUndefined(this.custom.refGUID) ? this.parentId : this.custom.refGUID
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<FinancialStatementTransListView>> {
    return this.service.getFinancialStatementTransToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteFinancialStatementTrans(row));
  }
  setFunctionOptions(): void {
    this.functionItems = [
      {
        label: 'LABEL.COPY_FINANCIAL_STATEMENT_TRANS',
        command: () =>
          this.toFunction({
            path: ROUTE_FUNCTION_GEN.COPY_FINANCIAL_STATEMENT_TRANS,
            parameters: { refGUID: this.custom.refGUID, refType: this.custom.refType }
          }),
        disabled:
          (this.custom.accessModeView.canCreate == false && this.custom.enableCopyFinancialStatement == true) ||
          this.custom.enableCopyFinancialStatement == false,
        visible: this.custom.canCopyFinancial
      },
      {
        label: 'LABEL.IMPORT_FINANCIAL_STATEMENT_TRANS',
        command: () =>
          this.toFunction({
            path: ROUTE_FUNCTION_GEN.IMPORT_FINANCIAL_STATEMENT_TRANS,
            parameters: {
              refType: this.custom.refType,
              refGUID: this.custom.refGUID
            }
          }),
        disabled: !this.custom.accessModeView.canCreate,
        visible:this.custom.canImportFinancial
      }
    ];

    super.setFunctionOptionsMapping();
  }
}
