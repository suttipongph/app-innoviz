import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CreditAppRequestStatus, RefType } from 'shared/constants';
import { BaseServiceModel, RefTypeAndCreditAppRequestTableModel, RefTypeModel } from 'shared/models/systemModel';
import { AccessModeView } from 'shared/models/viewModel';

const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const CREDIT_APP_REQUEST_LINE = 'creditapprequestline-child'
const CUSTOMER_TABLE = 'customertable';
const BUYER_TABLE = 'buyertable';
const CREDITAPP_TABLE = 'creditapptable';
const AMEND_CA = 'amendca';
const AMEND_CA_LINE = 'amendcaline';
const REVIEW_ACTIVE_CREDIT_APP_LINE = 'reviewactivecreditappline';
const REVIEW_CREDIT_APP_REQUEST = 'reviewcreditapprequest';
const BUYER_MATCHING = 'buyermatching';
const LOAN_REQUEST = 'loanrequest';
export class FinancialStatementTransListCustomComponent {
  refGUID: string;
  refType: RefType;
  baseService: BaseServiceModel<any>;
  originalPathName = null;
  parentName = null;
  parentId = null;
  parentReviewCreditAppRequest = null;
  accessModeView: AccessModeView = new AccessModeView();

  enableCopyFinancialStatement = true;
  canCopyFinancial = true;
  isBuyerMatching: boolean = false;
  isLoanRequest: boolean = false;
  creditAppRequestTableGUID: string = null;
  canImportFinancial = true;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setAccessModeByParentStatus(isWorkFlowMode: boolean): Observable<AccessModeView> {
    this.parentId = this.baseService.uiService.getRelatedInfoOriginTableKey();
    this.originalPathName = this.baseService.uiService.getRelatedInfoOriginTableName();
    switch (this.originalPathName) {
      case CREDIT_APP_REQUEST_TABLE:
        if (this.parentName === CREDIT_APP_REQUEST_TABLE || this.parentName === CREDIT_APP_REQUEST_LINE) {
          this.canImportFinancial = false;
        }        
        return this.getAccessModeByCreditAppRequestTable(this.parentId, isWorkFlowMode);
      case CREDITAPP_TABLE:
        if (this.parentName === AMEND_CA) {
          this.canImportFinancial = false;
          return this.getAccessModeByCreditAppRequestTable(this.refGUID, isWorkFlowMode);
        } else if (this.parentName === AMEND_CA_LINE) {
          return this.getAccessModeByCreditAppRequestTable(this.creditAppRequestTableGUID, isWorkFlowMode);
        } else {
          this.checkIsBuyerMacthingOrLoanRequest();
          return this.getAccessModeByCreditAppRequestTable(this.parentId, isWorkFlowMode);
        }
      case REVIEW_ACTIVE_CREDIT_APP_LINE:
        if (this.parentName === REVIEW_CREDIT_APP_REQUEST) {
          return this.getReviewCreditAppAccessModeByCreditAppRequestTable(this.creditAppRequestTableGUID);
        } else {
          return this.getReviewCreditAppAccessModeByCreditAppRequestTable(this.parentId);
        }
      case CUSTOMER_TABLE:
        this.enableCopyFinancialStatement = false;
        this.accessModeView.canCreate = true;
        this.accessModeView.canDelete = true;
        this.accessModeView.canView = true;
        this.canCopyFinancial = false;
        return of(this.accessModeView);
      case BUYER_TABLE:
        this.enableCopyFinancialStatement = false;
        this.accessModeView.canCreate = true;
        this.accessModeView.canDelete = true;
        this.accessModeView.canView = true;
        return of(this.accessModeView);
      default:
        return of(this.accessModeView);
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
  setRefTypeRefGUID(passingObj: RefTypeAndCreditAppRequestTableModel): void {
    this.refType = passingObj.refType;
    this.refGUID = passingObj.refGUID;
    this.creditAppRequestTableGUID = passingObj.creditAppRequestTableGUID;
  }

  checkIsBuyerMacthingOrLoanRequest(): void {
    this.isBuyerMatching = window.location.href.match(BUYER_MATCHING) ? true : false;
    this.isLoanRequest = window.location.href.match(LOAN_REQUEST) ? true : false;
    if (this.isBuyerMatching) {
      this.parentId = this.baseService.uiService.getKey(BUYER_MATCHING);
    } else if (this.isLoanRequest) {
      this.parentId = this.baseService.uiService.getKey(LOAN_REQUEST);
    }
  }

  getReviewCreditAppAccessModeByCreditAppRequestTable(parentId: string) {
    return this.baseService.service.getReviewCreditAppAccessModeByCreditAppRequestTable(parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
        this.accessModeView.canCreate = this.accessModeView.canCreate;
        this.accessModeView.canDelete = this.accessModeView.canDelete;
        this.accessModeView.canView = true;
      })
    );
  }

  getAccessModeByCreditAppRequestTable(parentId: string, isWorkFlowMode: boolean): Observable<AccessModeView> {
    return this.baseService.service.getAccessModeByCreditAppRequestTable(parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
        const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
        this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
        this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
        this.accessModeView.canView = true;
      })
    );
  }
}
