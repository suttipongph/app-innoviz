import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { AccessModeView, FinancialStatementTransItemView, FinancialStatementTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class FinancialStatementTransService {
  serviceKey = 'financialStatementTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getFinancialStatementTransToList(search: SearchParameter): Observable<SearchResult<FinancialStatementTransListView>> {
    const url = `${this.servicePath}/GetFinancialStatementTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteFinancialStatementTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteFinancialStatementTrans`;
    return this.dataGateway.delete(url, row);
  }
  getFinancialStatementTransById(id: string): Observable<FinancialStatementTransItemView> {
    const url = `${this.servicePath}/GetFinancialStatementTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createFinancialStatementTrans(vmModel: FinancialStatementTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateFinancialStatementTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateFinancialStatementTrans(vmModel: FinancialStatementTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateFinancialStatementTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<FinancialStatementTransItemView> {
    const url = `${this.servicePath}/GetFinancialStatementTransInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByCreditAppRequestTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppRequestTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getReviewCreditAppAccessModeByCreditAppRequestTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetReviewCreditAppAccessModeByCreditAppRequestTable/id=${id}`;
    return this.dataGateway.get(url);
  }
}
