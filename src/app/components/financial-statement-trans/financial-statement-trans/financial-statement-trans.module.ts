import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FinancialStatementTransService } from './financial-statement-trans.service';
import { FinancialStatementTransListComponent } from './financial-statement-trans-list/financial-statement-trans-list.component';
import { FinancialStatementTransItemComponent } from './financial-statement-trans-item/financial-statement-trans-item.component';
import { FinancialStatementTransItemCustomComponent } from './financial-statement-trans-item/financial-statement-trans-item-custom.component';
import { FinancialStatementTransListCustomComponent } from './financial-statement-trans-list/financial-statement-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [FinancialStatementTransListComponent, FinancialStatementTransItemComponent],
  providers: [FinancialStatementTransService, FinancialStatementTransItemCustomComponent, FinancialStatementTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [FinancialStatementTransListComponent, FinancialStatementTransItemComponent]
})
export class FinancialStatementTransComponentModule {}
