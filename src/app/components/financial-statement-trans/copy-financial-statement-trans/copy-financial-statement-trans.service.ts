import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CopyFinancialStatementTransView } from 'shared/models/viewModel';
import { PageInformationModel, RefTypeModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CopyFinancialStatementTransService {
  serviceKey = 'copyFinancialStatementTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCopyFinancialStatementTransByRefTypeValidation(data: RefTypeModel): Observable<boolean> {
    const url = `${this.servicePath}/GetCopyFinancialStatementTransByRefTypeValidation`;
    return this.dataGateway.post(url, data);
  }
  checkFinancialHasCreditAppLine(data: RefTypeModel): Observable<boolean> {
    const url = `${this.servicePath}/CheckFinancialHasCreditAppLine`;
    return this.dataGateway.post(url, data);
  }
  copyFinancialStatementTrans(vmModel: CopyFinancialStatementTransView): Observable<CopyFinancialStatementTransView> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vmModel);
  }
}
