import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopyFinancialStatementTransComponent } from './copy-financial-statement-trans.component';

describe('CopyFinancialStatementTransViewComponent', () => {
  let component: CopyFinancialStatementTransComponent;
  let fixture: ComponentFixture<CopyFinancialStatementTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CopyFinancialStatementTransComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyFinancialStatementTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
