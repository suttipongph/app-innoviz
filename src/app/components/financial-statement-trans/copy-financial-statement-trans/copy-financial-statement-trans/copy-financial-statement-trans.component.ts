import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { CopyFinancialStatementTransView } from 'shared/models/viewModel';
import { CopyFinancialStatementTransService } from '../copy-financial-statement-trans.service';
import { CopyFinancialStatementTransCustomComponent } from './copy-financial-statement-trans-custom.component';
@Component({
  selector: 'copy-financial-statement-trans',
  templateUrl: './copy-financial-statement-trans.component.html',
  styleUrls: ['./copy-financial-statement-trans.component.scss']
})
export class CopyFinancialStatementTransComponent extends BaseItemComponent<CopyFinancialStatementTransView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  AMENDCA = 'amendcaline';

  constructor(
    private service: CopyFinancialStatementTransService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CopyFinancialStatementTransCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setRefTypeRefGUID(passingObj);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialCreatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<CopyFinancialStatementTransView> {
    return null;
  }
  getInitialData(): Observable<CopyFinancialStatementTransView> {
    return this.custom.getInitialData(this.model);
  }
  setInitialUpdatingData(): void {}
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing().subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {}
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.custom.getModelParam(this.model).subscribe((model) => {
          this.onSubmit(true, model);
        });
      }
    });
  }
  onSubmit(isColsing: boolean, model: CopyFinancialStatementTransView): void {
    super.onExecuteFunction(this.service.copyFinancialStatementTrans(model));
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation(this.model);
  }
}
