import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { EmptyGuid, RefType, ROUTE_FUNCTION_GEN, ROUTE_MASTER_GEN } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, RefTypeModel, TranslateModel } from 'shared/models/systemModel';
import { CopyFinancialStatementTransView } from 'shared/models/viewModel';
import { CopyFinancialStatementTransService } from '../copy-financial-statement-trans.service';

const firstGroup = [];

export class CopyFinancialStatementTransCustomComponent {
  refGUID: string;
  refType: RefType;
  baseService: BaseServiceModel<CopyFinancialStatementTransService>;
  getFieldAccessing(): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(model: CopyFinancialStatementTransView): Observable<CopyFinancialStatementTransView> {
    const parentRoute = this.baseService.uiService.getRelatedInfoParentTableName();
    const parentKey = this.baseService.uiService.getRelatedInfoParentTableKey();
    if(this.refType === RefType.CreditAppRequestTable){
      model.refGUID = this.refGUID;
      model.refType = this.refType;
    }else if(this.refType === RefType.CreditAppRequestLine){
      model.refGUID = this.refGUID;
      model.refType = this.refType;
    }     
    return of(model);
  }
  getDataValidation(model: CopyFinancialStatementTransView): Observable<boolean> {
    return this.baseService.service.getCopyFinancialStatementTransByRefTypeValidation(model).pipe(
      switchMap((result) => {
        if (result) {
          return of(result);
        } else {
          const translate: TranslateModel = {
            code: 'CONFIRM.90002',
            parameters: ['LABEL.COPY_FINANCIAL_STATEMENT_TRANS']
          };
          this.baseService.notificationService.showDeletionDialogManual(translate);
          return this.baseService.notificationService.isAccept.asObservable();
        }
      })
    );
  }
  getModelParam(model: CopyFinancialStatementTransView): Observable<CopyFinancialStatementTransView> {
    model.resultLabel = 'LABEL.FINANCIAL_STATEMENT_TRANSACTION';
    return of(model);
  }
  setRefTypeRefGUID(passingObj: RefTypeModel): void {
    if(!isNullOrUndefined(passingObj)){
      this.refType = passingObj.refType;
      this.refGUID = passingObj.refGUID;
    }    
  }
}
