import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CopyFinancialStatementTransService } from './copy-financial-statement-trans.service';
import { CopyFinancialStatementTransComponent } from './copy-financial-statement-trans/copy-financial-statement-trans.component';
import { CopyFinancialStatementTransCustomComponent } from './copy-financial-statement-trans/copy-financial-statement-trans-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CopyFinancialStatementTransComponent],
  providers: [CopyFinancialStatementTransService, CopyFinancialStatementTransCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CopyFinancialStatementTransComponent]
})
export class CopyFinancialStatementTransComponentModule {}
