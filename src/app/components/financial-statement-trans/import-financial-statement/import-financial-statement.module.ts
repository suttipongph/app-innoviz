import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ImportFinancialStatementService } from './import-financial-statement.service';
import { ImportFinancialStatementComponent } from './import-financial-statement/import-financial-statement.component';
import { ImportFinancialStatementCustomComponent } from './import-financial-statement/import-financial-statement-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ImportFinancialStatementComponent],
  providers: [ImportFinancialStatementService, ImportFinancialStatementCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ImportFinancialStatementComponent]
})
export class ImportFinancialStatementComponentModule {}
