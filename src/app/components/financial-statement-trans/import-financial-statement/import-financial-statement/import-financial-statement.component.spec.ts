import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportFinancialStatementComponent } from './import-financial-statement.component';

describe('ImportFinancialStatementViewComponent', () => {
  let component: ImportFinancialStatementComponent;
  let fixture: ComponentFixture<ImportFinancialStatementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ImportFinancialStatementComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportFinancialStatementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
