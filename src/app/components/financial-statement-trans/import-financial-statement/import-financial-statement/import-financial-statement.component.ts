import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { IVZFileUploadComponent } from 'shared/components/ivz-file-upload/ivz-file-upload.component';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUndefinedOrZeroLength, isUpdateMode } from 'shared/functions/value.function';
import { FileUpload, FormValidationModel, PageInformationModel, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { ImportFinancialStatementView } from 'shared/models/viewModel/importFinancialStatementView';
import { ImportFinancialStatementService } from '../import-financial-statement.service';
import { ImportFinancialStatementCustomComponent } from './import-financial-statement-custom.component';
@Component({
  selector: 'import-financial-statement',
  templateUrl: './import-financial-statement.component.html',
  styleUrls: ['./import-financial-statement.component.scss']
})
export class ImportFinancialStatementComponent extends BaseItemComponent<ImportFinancialStatementView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  refTypeOptions: SelectItems[] = [];
  fileUpload: FileUpload = new FileUpload();
  @ViewChild('fileUploadCmpnt') fileComponent: IVZFileUploadComponent;
  docxType: string = '.xlsx';
  constructor(
    private service: ImportFinancialStatementService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: ImportFinancialStatementCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  onSave(validation: FormValidationModel): void {
    throw new Error('Method not implemented.');
  }
  getById(): Observable<any> {
    throw new Error('Method not implemented.');
  }
  setInitialUpdatingData(): void {
    throw new Error('Method not implemented.');
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setRefTypeRefGUID(passingObj);

  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialCreatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getInitialData(): Observable<ImportFinancialStatementView> {
    return this.custom.getInitialData();
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.model.refGUID = this.custom.refGUID;
      this.model.refType = Number(this.custom.refType);
      this.model.importFinancialStatementGUID = EmptyGuid;    
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }
  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing().subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isClosing: boolean): void {
    if (isUndefinedOrZeroLength(this.fileUpload.fileInfos) || isUndefinedOrZeroLength(this.fileUpload.fileInfos[0])) {
      // popup validation fail >> no file selected
      const topic: TranslateModel = { code: 'ERROR.00159' };
      const contents: TranslateModel[] = [{ code: 'ERROR.00160' }];
      this.notificationService.showErrorMessageFromManual(topic, contents);
      return;
    }
    this.setFileUpload();
    super.onExecuteFunction(this.service.ImportFinancialStatement(this.model));
  }

  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onFileChange(e: FileUpload): void {
    this.fileUpload = e;
    this.fileDirty = true;
  }
  setFileUpload(): void {
    if (isUndefinedOrZeroLength(this.fileUpload.fileInfos) || isUndefinedOrZeroLength(this.fileUpload.fileInfos[0])) {
      return;
    }
    if (!isUpdateMode(this.id) || (isUpdateMode(this.id) && this.fileDirty)) {
      let file = this.fileUpload.fileInfos[0].base64.split(',');
      this.model.fileInfo = this.fileUpload.fileInfos[0];
    } else {
      this.model.fileInfo = null;
    }
  }
}
