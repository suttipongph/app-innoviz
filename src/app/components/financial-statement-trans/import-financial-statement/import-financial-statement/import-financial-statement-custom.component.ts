import { Observable, of } from 'rxjs';
import { EmptyGuid, RefType } from 'shared/constants';
import { getRefTypeFromRoute, isUndefinedOrZeroLength } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { RefIdParm } from 'shared/models/viewModel';
import { ImportFinancialStatementView } from 'shared/models/viewModel/importFinancialStatementView';
import { ImportFinancialStatementService } from '../import-financial-statement.service';

const firstGroup = ['REF_TYPE', 'REF_ID', 'NAME', 'FILE'];
export class ImportFinancialStatementCustomComponent {
  baseService: BaseServiceModel<ImportFinancialStatementService>;
  refType: string = '';
  refGUID: string = '';
  parentName: string = '';
  getFieldAccessing(): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getInitialData(): Observable<ImportFinancialStatementView> {
    return this.baseService.service.GetFinancialStatementTransRefId(this.refGUID,Number(this.refType));
  }
 
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  setRefTypeRefGUID(passingObj: RefIdParm): void {
    this.refType = passingObj.refType.toString();
    this.refGUID = passingObj.refGUID;
  }
}

