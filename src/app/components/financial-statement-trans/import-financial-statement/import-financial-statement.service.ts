import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { ImportFinancialStatementView } from 'shared/models/viewModel/importFinancialStatementView';
@Injectable()
export class ImportFinancialStatementService {
  serviceKey = 'importFinancialStatementGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  ImportFinancialStatement(vmModel: ImportFinancialStatementView): Observable<ResponseModel> {
    const url = `${this.servicePath}/ImportFinancialStatement`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  GetFinancialStatementTransRefId(refGUID: string, refType: number): Observable<ImportFinancialStatementView> {
    const url = `${this.servicePath}/GetFinancialStatementTransRefId/refGUID=${refGUID}/refType=${refType}`;
    return this.dataGateway.get(url);
  }
  
}
