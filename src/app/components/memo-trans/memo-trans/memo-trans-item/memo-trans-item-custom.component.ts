import { AppInjector } from 'app-injector';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { AccessModeView, MemoTransItemView } from 'shared/models/viewModel';

const firstGroup = ['REF_TYPE', 'REF_ID'];
const AMEND_CA = 'amendca';
const VERIFICATION = 'verificationtable';
const ASSIGNMENT_AGREEMENT = 'assignmentagreementtable';
const GUARANTOR_AGREEMENT = 'guarantoragreementtable';
const PURCHASE_TABLE_PURCHASE = 'purchasetablepurchase';
const PURCHASE_TABLE_ROLLBILL = 'purchasetablerollbill';
const WITHDRAWAL_TABLE = 'withdrawaltablewithdrawal';
const PROJECT_PROGRESS = 'projectprogress';
const MAIN_AGREEMENT_TABLE = 'mainagreementtable';
const ADDENDUM_MAIN_AGREEMENT_TABLE = 'addendummainagreementtable';
const NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE = 'noticeofcancellationmainagreementtable';
const BUSINESS_COLLATERAL_AGM = 'businesscollateralagmtable';
const ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE = 'addendumbusinesscollateralagmtable';
const NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT = 'noticeofcancellation';
const NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE = 'noticeofcancellationbusinesscollateralagmtable';

export class MemoTransItemCustomComponent {
  baseService: BaseServiceModel<any>;
  parentId: string = null;
  parentName: string = null;
  currentName: string = null;
  accessModeView: AccessModeView = new AccessModeView();
  public uiControllerService = AppInjector.get(UIControllerService);
  getFieldAccessing(model: MemoTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: MemoTransItemView): Observable<boolean> {
    return this.setAccessModeByParentStatus(model).pipe(
      map((result) => (isUpdateMode(model.memoTransGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  getRelatedInfoTableKey(passingObj: MemoTransItemView): string {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    this.currentName = this.baseService.uiService.getRelatedInfoActiveTableName();
    switch (this.currentName) {
      case ROUTE_RELATED_GEN.REVIEW_CREDIT_APP_REQUEST:
        this.parentId = passingObj.refGUID;
        break;
      case AMEND_CA:
        this.parentId = passingObj.refGUID;
        break;
      case VERIFICATION:
        this.parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
        break;
      case PURCHASE_TABLE_PURCHASE:
      case PURCHASE_TABLE_ROLLBILL:
        this.parentId = passingObj.refGUID;
        break;
      default:
        this.parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
        break;
    }
    return this.parentId;
  }

  setAccessModeByParentStatus(model: MemoTransItemView): Observable<AccessModeView> {
    switch (this.parentName) {
      case MAIN_AGREEMENT_TABLE:
      case ADDENDUM_MAIN_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE:
        return this.baseService.service.getAccessModeByMainAgreementTable(this.baseService.uiService.getRelatedInfoParentTableKey()).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
          })
        );
      case ASSIGNMENT_AGREEMENT:
      case NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT:
        return this.baseService.service.getAccessModeByAssignmentTable(this.baseService.uiService.getRelatedInfoParentTableKey()).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
          })
        );
      case PURCHASE_TABLE_PURCHASE:
      case PURCHASE_TABLE_ROLLBILL:
        this.accessModeView.canCreate = true;
        this.accessModeView.canDelete = true;
        this.accessModeView.canView = true;
        break;
      case BUSINESS_COLLATERAL_AGM:
      case ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
      case NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
        return this.baseService.service.getAccessModeByBusinessCollatealAgm(this.baseService.uiService.getRelatedInfoParentTableKey()).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
          })
        );
      default:
        this.accessModeView.canCreate = true;
        this.accessModeView.canDelete = true;
        this.accessModeView.canView = true;
        return of(this.accessModeView);
    }
  }
  getInitialData(parentId: string): Observable<MemoTransItemView> {
    this.currentName = this.baseService.uiService.getRelatedInfoActiveTableName();
    switch (this.currentName) {
      case VERIFICATION:
        return this.baseService.service.getInitialDataByVerification(parentId);
      case GUARANTOR_AGREEMENT:
        return this.baseService.service.getIntitailDataByGuarantorAgreement(parentId);
      case WITHDRAWAL_TABLE:
        switch (this.parentName) {
          case PROJECT_PROGRESS:
            return this.baseService.service.getIntitailDataByProjectProgress(parentId);
          default:
            return this.baseService.service.getIntitailDataByWithdrawal(parentId);
        }
      default:
        return this.baseService.service.getInitialData(parentId);
    }
  }
}
