import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemoTransItemComponent } from './memo-trans-item.component';

describe('MemoTransItemViewComponent', () => {
  let component: MemoTransItemComponent;
  let fixture: ComponentFixture<MemoTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MemoTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemoTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
