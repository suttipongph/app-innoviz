import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { AccessModeView, MemoTransItemView, MemoTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class MemoTransService {
  serviceKey = 'memoTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getMemoTransToList(search: SearchParameter): Observable<SearchResult<MemoTransListView>> {
    const url = `${this.servicePath}/GetMemoTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteMemoTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteMemoTrans`;
    return this.dataGateway.delete(url, row);
  }
  getMemoTransById(id: string): Observable<MemoTransItemView> {
    const url = `${this.servicePath}/GetMemoTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createMemoTrans(vmModel: MemoTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateMemoTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateMemoTrans(vmModel: MemoTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateMemoTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<MemoTransItemView> {
    const url = `${this.servicePath}/GetMemoTransInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getInitialDataByVerification(id: string): Observable<MemoTransItemView> {
    const url = `${this.servicePath}/GetMemoTransInitialDataByVerification/id=${id}`;
    return this.dataGateway.get(url);
  }
  getIntitailDataByGuarantorAgreement(id: string): Observable<MemoTransItemView> {
    const url = `${this.servicePath}/GetMemoTransInitialDataByGuarantorAgreement/id=${id}`;
    return this.dataGateway.get(url);
  }
  getIntitailDataByWithdrawal(id: string): Observable<MemoTransItemView> {
    const url = `${this.servicePath}/GetMemoTransInitialDataByWithdrawal/id=${id}`;
    return this.dataGateway.get(url);
  }
  getIntitailDataByProjectProgress(id: string): Observable<MemoTransItemView> {
    const url = `${this.servicePath}/GetMemoTransInitialDataByProjectPorgress/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByMainAgreementTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByMainAgreementTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByAssignmentTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByAssignmentTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByBusinessCollatealAgm(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByBusinessCollateralAgmTable/id=${id}`;
    return this.dataGateway.get(url);
  }
}
