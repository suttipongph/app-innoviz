import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MemoTransService } from './memo-trans.service';
import { MemoTransListComponent } from './memo-trans-list/memo-trans-list.component';
import { MemoTransItemComponent } from './memo-trans-item/memo-trans-item.component';
import { MemoTransItemCustomComponent } from './memo-trans-item/memo-trans-item-custom.component';
import { MemoTransListCustomComponent } from './memo-trans-list/memo-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [MemoTransListComponent, MemoTransItemComponent],
  providers: [MemoTransService, MemoTransItemCustomComponent, MemoTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [MemoTransListComponent, MemoTransItemComponent]
})
export class MemoTransComponentModule {}
