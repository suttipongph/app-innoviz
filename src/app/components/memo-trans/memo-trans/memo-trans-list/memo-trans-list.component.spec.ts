import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MemoTransListComponent } from './memo-trans-list.component';

describe('MemoTransListViewComponent', () => {
  let component: MemoTransListComponent;
  let fixture: ComponentFixture<MemoTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MemoTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemoTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
