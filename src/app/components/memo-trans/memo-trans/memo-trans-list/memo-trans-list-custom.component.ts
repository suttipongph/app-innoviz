import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView, MemoTransListView } from 'shared/models/viewModel';

const AMEND_CA = 'amendca';
const ASSIGNMENT_AGREEMENT = 'assignmentagreementtable';
const VERIFICATION = 'verification';
const PURCHASE_TABLE_PURCHASE = 'purchasetablepurchase';
const PURCHASE_TABLE_ROLLBILL = 'purchasetablerollbill';
const MAIN_AGREEMENT_TABLE = 'mainagreementtable';
const ADDENDUM_MAIN_AGREEMENT_TABLE = 'addendummainagreementtable';
const NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE = 'noticeofcancellationmainagreementtable';
const BUSINESS_COLLATERAL_AGM = 'businesscollateralagmtable';
const ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE = 'addendumbusinesscollateralagmtable';
const NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT = 'noticeofcancellation';
const NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE = 'noticeofcancellationbusinesscollateralagmtable';
export class MemoTransListCustomComponent {
  baseService: BaseServiceModel<any>;
  parentId: string = null;
  parentName: string = null;
  currentName: string = null;
  accessModeView: AccessModeView = new AccessModeView();
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  getRelatedInfoTableKey(passingObj: MemoTransListView): string {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    this.currentName = this.baseService.uiService.getRelatedInfoActiveTableName();
    switch (this.currentName) {
      case VERIFICATION:
        this.parentId = passingObj.refGUID;
        break;
      case AMEND_CA:
        this.parentId = passingObj.refGUID;
        break;
      case PURCHASE_TABLE_PURCHASE:
      case PURCHASE_TABLE_ROLLBILL:
        this.parentId = passingObj.refGUID;
        break;
      case ROUTE_RELATED_GEN.REVIEW_CREDIT_APP_REQUEST:
        this.parentId = passingObj.refGUID;
        break;
      default:
        this.parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
        break;
    }
    return this.parentId;
  }
  setAccessModeByParentStatus(): Observable<AccessModeView> {
    switch (this.parentName) {
      case MAIN_AGREEMENT_TABLE:
      case ADDENDUM_MAIN_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE:
        return this.baseService.service.getAccessModeByMainAgreementTable(this.baseService.uiService.getRelatedInfoParentTableKey()).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            this.accessModeView.canView = true;
          })
        );
      case ASSIGNMENT_AGREEMENT:
      case NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT:
        return this.baseService.service.getAccessModeByAssignmentTable(this.baseService.uiService.getRelatedInfoParentTableKey()).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            this.accessModeView.canView = true;
          })
        );
      case BUSINESS_COLLATERAL_AGM:
      case ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
      case NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
        return this.baseService.service.getAccessModeByBusinessCollatealAgm(this.baseService.uiService.getRelatedInfoParentTableKey()).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            this.accessModeView.canView = true;
          })
        );
      default:
        this.accessModeView.canCreate = true;
        this.accessModeView.canView = true;
        this.accessModeView.canDelete = true;
        return of(this.accessModeView);
    }
  }

  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
}
