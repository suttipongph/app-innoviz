import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { VendorTableItemView } from 'shared/models/viewModel';
import { VendorTableService } from '../vendor-table.service';
import { VendorTableItemCustomComponent } from './vendor-table-item-custom.component';
@Component({
  selector: 'vendor-table-item',
  templateUrl: './vendor-table-item.component.html',
  styleUrls: ['./vendor-table-item.component.scss']
})
export class VendorTableItemComponent extends BaseItemComponent<VendorTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  vendGroupOptions: SelectItems[] = [];
  recordTypeOptions: SelectItems[] = [];
  currencyOptions: SelectItems[] = [];
  constructor(private service: VendorTableService, private currentActivatedRoute: ActivatedRoute, public custom: VendorTableItemCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  getById(): Observable<VendorTableItemView> {
    return this.service.getVendorTableById(this.id);
  }
  getInitialData(): Observable<VendorTableItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }
  onEnumLoader(): void {
    this.baseDropdown.getRecordTypeEnumDropDown(this.recordTypeOptions);
  }
  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getVendGroupDropDown(), this.baseDropdown.getCurrencyDropDown()).subscribe(
      ([vendGroup, currency]) => {
        this.vendGroupOptions = vendGroup;
        this.currencyOptions = currency;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model, this.service).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {
    this.relatedInfoItems = [
      { label: 'LABEL.ADDRESS_TRANS', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.ADDRESSTRANS }) },
      {
        label: 'LABEL.BANK',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.VEND_BANK })
      },
      {
        label: 'LABEL.MANAGE',
        items: [
          {
            label: 'LABEL.CONTACT',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.CONTACT })
          }
        ]
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(): void {}
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateVendorTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createVendorTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
