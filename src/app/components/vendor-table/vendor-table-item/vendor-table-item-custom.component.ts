import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { VendorTableItemView } from 'shared/models/viewModel';
import { VendorTableService } from '../vendor-table.service';

const firstGroup = ['VENDOR_ID'];
const readonlyForever = ['SENT_TO_OTHER_SYSTEM'];
export class VendorTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  isManunal: boolean = false;
  getFieldAccessing(model: VendorTableItemView, vendorTableService: VendorTableService): Observable<any> {
    let fieldAccessing: FieldAccessing[] = [{ filedIds: readonlyForever, readonly: true }];
    return new Observable((observer) => {
      // set firstGroup
      if (!isUpdateMode(model.vendorTableGUID)) {
        this.validateIsManualNumberSeq(vendorTableService, model.companyGUID).subscribe(
          (result) => {
            this.isManunal = result;
            fieldAccessing.push({ filedIds: firstGroup, readonly: !this.isManunal });
            observer.next(fieldAccessing);
          },
          (error) => {
            this.isManunal = true;
            fieldAccessing.push({ filedIds: firstGroup, readonly: false });
            observer.next(fieldAccessing);
          }
        );
      } else {
        fieldAccessing.push({ filedIds: firstGroup, readonly: true });
        observer.next(fieldAccessing);
      }
    });
  }

  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: VendorTableItemView): Observable<boolean> {
    return of(!canCreate);
  }
  validateIsManualNumberSeq(vendorTableService: VendorTableService, companyId: string): Observable<boolean> {
    return vendorTableService.validateIsManualNumberSeq(companyId);
  }
  getInitialData(): Observable<VendorTableItemView> {
    let model: VendorTableItemView = new VendorTableItemView();
    model.vendorTableGUID = EmptyGuid;
    return of(model);
  }
}
