import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, CURRENCY, LANGUAGE, SortType, VENDOR_TABLE, VEND_GROUP } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { VendorTableListView } from 'shared/models/viewModel';
import { VendorTableService } from '../vendor-table.service';
import { VendorTableListCustomComponent } from './vendor-table-list-custom.component';

@Component({
  selector: 'vendor-table-list',
  templateUrl: './vendor-table-list.component.html',
  styleUrls: ['./vendor-table-list.component.scss']
})
export class VendorTableListComponent extends BaseListComponent<VendorTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  recordTypeOptions: SelectItems[] = [];
  vendGroupOptions: SelectItems[] = [];
  currencyOptions: SelectItems[] = [];

  constructor(private service: VendorTableService, public custom: VendorTableListCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getRecordTypeEnumDropDown(this.recordTypeOptions);
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getVendGroupDropDown(this.vendGroupOptions);
    this.baseDropdown.getCurrencyDropDown(this.currencyOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'vendorTableGUID';
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.VENDOR_ID',
        textKey: VENDOR_TABLE.PK,
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.NAME',
        textKey: 'name',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CURRENCY_ID',
        textKey: 'currency_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.currencyOptions,
        searchingKey: 'currencyGUID',
        sortingKey: 'currency_CurrencyId'
      },
      {
        label: 'LABEL.VEND_GROUP_ID',
        textKey: 'vendGroup_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.vendGroupOptions,
        searchingKey: 'vendGroupGUID',
        sortingKey: 'vendGroup_VendGroupId'
      },
      {
        label: 'LABEL.ALT_NAME',
        textKey: 'altName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.RECORD_TYPE',
        textKey: 'recordType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.recordTypeOptions
      },
      {
        label: 'LABEL.TAX_ID',
        textKey: 'taxId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.EXTERNAL_CODE',
        textKey: 'externalCode',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<VendorTableListView>> {
    return this.service.getVendorTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteVendorTable(row));
  }
}
