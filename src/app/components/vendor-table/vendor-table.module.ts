import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VendorTableService } from './vendor-table.service';
import { VendorTableListComponent } from './vendor-table-list/vendor-table-list.component';
import { VendorTableItemComponent } from './vendor-table-item/vendor-table-item.component';
import { VendorTableItemCustomComponent } from './vendor-table-item/vendor-table-item-custom.component';
import { VendorTableListCustomComponent } from './vendor-table-list/vendor-table-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [VendorTableListComponent, VendorTableItemComponent],
  providers: [VendorTableService, VendorTableItemCustomComponent, VendorTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [VendorTableListComponent, VendorTableItemComponent]
})
export class VendorTableComponentModule {}
