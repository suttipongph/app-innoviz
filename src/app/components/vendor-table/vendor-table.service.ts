import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { VendorTableItemView, VendorTableListView } from 'shared/models/viewModel';

@Injectable()
export class VendorTableService {
  serviceKey = 'vendorTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getVendorTableToList(search: SearchParameter): Observable<SearchResult<VendorTableListView>> {
    const url = `${this.servicePath}/GetVendorTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteVendorTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteVendorTable`;
    return this.dataGateway.delete(url, row);
  }
  getVendorTableById(id: string): Observable<VendorTableItemView> {
    const url = `${this.servicePath}/GetVendorTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createVendorTable(vmModel: VendorTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateVendorTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateVendorTable(vmModel: VendorTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateVendorTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  validateIsManualNumberSeq(companyId: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeq/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
}
