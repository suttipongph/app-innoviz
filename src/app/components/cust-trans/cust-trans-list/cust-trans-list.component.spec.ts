import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CustTransListComponent } from './cust-trans-list.component';

describe('CustTransListViewComponent', () => {
  let component: CustTransListComponent;
  let fixture: ComponentFixture<CustTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
