import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { RefType } from 'shared/constants';
import { BaseServiceModel, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { AccessModeView, CustomerTableItemView, CustTransListView } from 'shared/models/viewModel';
import { CustTransService } from '../cust-trans.service';
import { CustTransListComponent } from './cust-trans-list.component';
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const CUSTOMER_TABLE = 'customertable';
const BUYER_TABLE = 'buyertable';
const INVOICE_TABLE = 'invoicetable';
export class CustTransListCustomComponent {
  baseService: BaseServiceModel<any>;
  secondService: BaseServiceModel<CustTransService>;
  parentName = null;
  title: string = null;
  headtitle: string = null;
  customerId: string = '';
  customerName: string = '';
  accessModeView: AccessModeView = new AccessModeView();
  model: CustomerTableItemView;
  parentId: string;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setInitialListData(service: CustTransService): Observable<CustomerTableItemView> {
    this.model = new CustomerTableItemView();
    this.parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
    switch (this.parentName) {
      case CUSTOMER_TABLE:
        return service.getCustomerTablebyCusttrans(this.parentId);
      case BUYER_TABLE:
        return service.getBuyerTablebyCusttrans(this.parentId);
      case INVOICE_TABLE:
        return service.getInvoiceTablebyCusttrans(this.parentId);
    }
  }
  getListCustom(Search: SearchParameter, service: CustTransService): Observable<SearchResult<CustTransListView>> {
    switch (this.parentName) {
      case CUSTOMER_TABLE:
        return service.getCustTransToListByCustomer(Search);
      case BUYER_TABLE:
        return service.getCustTransToListByBuyer(Search);
      case INVOICE_TABLE:
        return service.getCustTransTolistByInvoice(Search);
    }
  }
  setAccessModeByParentStatus(parentId: string): Observable<AccessModeView> {
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        return this.baseService.service.getAccessModeByCreditAppRequestTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
          })
        );
      case CUSTOMER_TABLE:
        this.accessModeView.canCreate = true;
        this.accessModeView.canDelete = false;
        this.accessModeView.canView = true;
        return of(this.accessModeView);
      case INVOICE_TABLE:
        this.accessModeView.canCreate = false;
        this.accessModeView.canDelete = false;
        this.accessModeView.canView = true;
        return of(this.accessModeView);
      default:
        return of(this.accessModeView);
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
  setTitle(passingObj: any): any {
    switch (this.parentName) {
      case CUSTOMER_TABLE:
        let costomerid = passingObj.customerId;
        let customername = passingObj.name;
        this.headtitle = 'LABEL.CUSTOMER_TRANSACTIONS';
        this.title = ' : ' + costomerid + '   ' + customername;
        break;
      case BUYER_TABLE:
        let buyerId = passingObj.buyerId;
        let buyername = passingObj.buyerName;
        this.headtitle = 'LABEL.BUYER_TRANSACTIONS';
        this.title = ' : ' + buyerId + '   ' + buyername;
        break;
      case INVOICE_TABLE:
        this.headtitle = 'LABEL.CUSTOMER_TRANSACTIONS';
        break;
      default:
        this.headtitle = 'LABEL.CUSTOMER_TRANSACTIONS';
        this.title = '';
        break;
    }
  }
  setTextKey(): string {
    let textKey;
    switch (this.parentName) {
      case CUSTOMER_TABLE:
        textKey = 'customerTableGUID';
        break;
      case INVOICE_TABLE:
        textKey = 'invoiceTableGUID';
        break;
      case BUYER_TABLE:
        textKey = 'buyerTableGUID';
        break;
    }
    return textKey;
  }
}
