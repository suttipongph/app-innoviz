import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CustomerTableItemView, CustTransListView } from 'shared/models/viewModel';
import { CustTransItemCustomComponent } from '../cust-trans-item/cust-trans-item-custom.component';
import { CustTransService } from '../cust-trans.service';
import { CustTransListCustomComponent } from './cust-trans-list-custom.component';

@Component({
  selector: 'cust-trans-list',
  templateUrl: './cust-trans-list.component.html',
  styleUrls: ['./cust-trans-list.component.scss']
})
export class CustTransListComponent extends BaseListComponent<CustTransListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  productTypeOptions: SelectItems[] = [];
  custTransStatusOption: SelectItems[] = [];
  invoiceTableOption: SelectItems[] = [];
  customerTablesOption: SelectItems[] = [];
  buyerTableOption: SelectItems[] = [];
  invoiceTypeOption: SelectItems[] = [];
  currencyOption: SelectItems[] = [];
  customerTableModel: CustomerTableItemView;
  constructor(public custom: CustTransListCustomComponent, private service: CustTransService) {
    super();
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getCustTransStatusEnumDropDown(this.custTransStatusOption);
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.setInitialListData(this.service).subscribe((result) => {
      this.customerTableModel = result;
      this.custom.setTitle(this.customerTableModel);
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getInvoiceTableDropDown(this.invoiceTableOption);
    this.baseDropdown.getCustomerTableDropDown(this.customerTablesOption);
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOption);
    this.baseDropdown.getInvoiceTypeDropDown(this.invoiceTypeOption);
    this.baseDropdown.getCurrencyDropDown(this.currencyOption);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'custTransGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.INVOICE_ID',
        textKey: 'invoiceTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.invoiceTableOption,
        sortingKey: 'invoiceTable_InvoiceId',
        searchingKey: 'invoiceTableGUID'
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTablesOption,
        sortingKey: 'customerTable_CustomerId',
        searchingKey: 'customerTableGUID'
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerTableOption,
        sortingKey: 'buyerTable_BuyerId',
        searchingKey: 'buyerTableGUID'
      },
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions,
        sortingKey: 'productType',
        searchingKey: 'productType'
      },
      {
        label: 'LABEL.INVOICE_TYPE_ID',
        textKey: 'invoiceType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.invoiceTypeOption,
        sortingKey: 'invoiceType_InvoiceTypeId',
        searchingKey: 'invoiceTypeGUID'
      },

      {
        label: 'LABEL.DUE_DATE',
        textKey: 'dueDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.ASC,
        sortingKey: 'dueDate',
        searchingKey: 'dueDate'
      },
      {
        label: 'LABEL.CURRENCY_ID',
        textKey: 'currency_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.currencyOption,
        sortingKey: 'currency_CurrencyId',
        searchingKey: 'currencyGUID'
      },
      {
        label: 'LABEL.AMOUNT',
        textKey: 'transAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        sortingKey: 'transAmount',
        searchingKey: 'transAmount'
      },
      {
        label: 'LABEL.SETTLED_AMOUNT',
        textKey: 'settleAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        sortingKey: 'settleAmount',
        searchingKey: 'settleAmount'
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'custTransStatus',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.custTransStatusOption,
        sortingKey: 'custTransStatus',
        searchingKey: 'custTransStatus'
      },
      {
        label: null,
        textKey: this.custom.setTextKey(),
        type: ColumnType.MASTER,
        visibility: false,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CustTransListView>> {
    return this.custom.getListCustom(search, this.service);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCustTrans(row));
  }
}
