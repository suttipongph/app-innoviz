import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CustTransItemView } from 'shared/models/viewModel';

const firstGroup = [
  'INVOICE_TABLE_GUID',
  'CUSTOMER_TABLE_GUID',
  'BUYER_TABLE_GUID',
  'PRODUCT_TYPE',
  'INVOICE_TYPE_GUID',
  'TRANS_DATE',
  'DESCRIPTION',
  'DUE_DATE',
  'CURRENCY_GUID',
  'EXCHANGE_RATE',
  'TRANS_AMOUNT',
  'TRANS_AMOUNT_MST',
  'CUST_TRANS_STATUS',
  'CREDIT_APP_TABLE_GUID',
  'LAST_SETTLE_DATE',
  'SETTLE_AMOUNT',
  'SETTLE_AMOUNT_MST',
  'CUST_TRANS_GUID'
];
const CUSTOMER_TABLE = 'customertable';
const BUYER_TABLE = 'buyertable';

export class CustTransItemCustomComponent {
  parentName = null;
  headtitle: string = null;
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CustTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CustTransItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(): Observable<CustTransItemView> {
    let model = new CustTransItemView();
    model.custTransGUID = model.custTransGUID;
    return of(model);
  }
  setTitle(): string {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (this.parentName) {
      case CUSTOMER_TABLE:
        this.headtitle = 'LABEL.CUSTOMER_TRANSACTIONS';
        return this.headtitle;
      case BUYER_TABLE:
        this.headtitle = 'LABEL.BUYER_TRANSACTIONS';
        return this.headtitle;
      default:
        this.headtitle = 'LABEL.CUSTOMER_TRANSACTIONS';
        return this.headtitle;
    }
  }
}
