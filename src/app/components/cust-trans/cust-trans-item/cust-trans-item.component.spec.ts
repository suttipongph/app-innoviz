import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustTransItemComponent } from './cust-trans-item.component';

describe('CustTransItemViewComponent', () => {
  let component: CustTransItemComponent;
  let fixture: ComponentFixture<CustTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
