import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustTransService } from './cust-trans.service';
import { CustTransListComponent } from './cust-trans-list/cust-trans-list.component';
import { CustTransItemComponent } from './cust-trans-item/cust-trans-item.component';
import { CustTransItemCustomComponent } from './cust-trans-item/cust-trans-item-custom.component';
import { CustTransListCustomComponent } from './cust-trans-list/cust-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CustTransListComponent, CustTransItemComponent],
  providers: [CustTransService, CustTransItemCustomComponent, CustTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CustTransListComponent, CustTransItemComponent]
})
export class CustTransComponentModule {}
