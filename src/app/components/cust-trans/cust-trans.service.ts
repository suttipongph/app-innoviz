import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CustomerTableItemView, CustTransItemView, CustTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CustTransService {
  serviceKey = 'custTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCustTransToList(search: SearchParameter): Observable<SearchResult<CustTransListView>> {
    const url = `${this.servicePath}/GetCustTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCustTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCustTrans`;
    return this.dataGateway.delete(url, row);
  }
  getCustTransById(id: string): Observable<CustTransItemView> {
    const url = `${this.servicePath}/GetCustTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCustTrans(vmModel: CustTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCustTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCustTrans(vmModel: CustTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCustTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getCustomerTablebyCusttrans(id: string): Observable<CustomerTableItemView> {
    const url = `${this.servicePath}/GetCustomerTablebyCusttrans/id=${id}`;
    return this.dataGateway.get(url);
  }
  getBuyerTablebyCusttrans(id: string): Observable<CustomerTableItemView> {
    const url = `${this.servicePath}/GetBuyerTablebyCusttrans/id=${id}`;
    return this.dataGateway.get(url);
  }
  getCustTransToListByCustomer(search: SearchParameter): Observable<SearchResult<CustTransListView>> {
    const url = `${this.servicePath}/GetCustTransListByCustomer/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  getCustTransToListByBuyer(search: SearchParameter): Observable<SearchResult<CustTransListView>> {
    const url = `${this.servicePath}/GetCustTransListByBuyer/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  getCustTransTolistByInvoice(search: SearchParameter): Observable<SearchResult<CustTransListView>> {
    const url = `${this.servicePath}/GetCustTransListByInvoice/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  getInvoiceTablebyCusttrans(id: string): Observable<CustomerTableItemView> {
    const url = `${this.servicePath}/GetInvoiceTablebyCusttrans/id=${id}`;
    return this.dataGateway.get(url);
  }
}
