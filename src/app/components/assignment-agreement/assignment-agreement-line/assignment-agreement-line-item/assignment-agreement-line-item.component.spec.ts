import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmentAgreementLineItemComponent } from './assignment-agreement-line-item.component';

describe('AssignmentAgreementLineItemViewComponent', () => {
  let component: AssignmentAgreementLineItemComponent;
  let fixture: ComponentFixture<AssignmentAgreementLineItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AssignmentAgreementLineItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentAgreementLineItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
