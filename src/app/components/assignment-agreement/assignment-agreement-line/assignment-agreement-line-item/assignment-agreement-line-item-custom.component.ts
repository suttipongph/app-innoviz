import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AssignmentAgreementStatus, EmptyGuid, ROUTE_RELATED_GEN } from 'shared/constants';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, AssignmentAgreementLineItemView, BuyerAgreementTableItemView } from 'shared/models/viewModel';
import { AssignmentAgreementLineService } from '../assignment-agreement-line.service';

const firstGroup = [
  'INTERNAL_ASSIGNMENT_AGREEMENT_GUID',
  'ASSIGNMENT_AGREEMENT_ID',
  'LINE_NUM',
  'REFERENCE_AGREEMENT_ID',
  'START_DATE',
  'END_DATE',
  'BUYER_AGREEMENT_AMOUNT'
];
export class AssignmentAgreementLineItemCustomComponent {
  baseService: BaseServiceModel<AssignmentAgreementLineService>;
  title: string = null;
  getFieldAccessing(model: AssignmentAgreementLineItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: AssignmentAgreementLineItemView): Observable<boolean> {
    if (isUpdateMode(model.assignmentAgreementLineGUID)) {
      return of(model.assignmentAgreementTable_StatusId >= AssignmentAgreementStatus.Signed || !canUpdate);
    } else {
      return of(model.assignmentAgreementTable_StatusId >= AssignmentAgreementStatus.Signed || !canCreate);
    }
  }
  getInitialData(): Observable<AssignmentAgreementLineItemView> {
    let model = new AssignmentAgreementLineItemView();
    model.assignmentAgreementLineGUID = EmptyGuid;
    return of(model);
  }
  getBuyerAgreementData(id: string): Observable<BuyerAgreementTableItemView> {
    return this.baseService.service.getBuyerAgreementTableSumLineById(id);
  }
  clearModelByBuyerAgreementData(model: AssignmentAgreementLineItemView) {
    model.referenceAgreementID = '';
    model.startDate = null;
    model.endDate = null;
    model.buyerAgreementAmount = 0;
  }
  setTitle() {
    let activeParentName = this.baseService.uiService.getRelatedInfoActiveTableName();
    switch (activeParentName) {
      case ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION:
        this.title = 'LABEL.NOTICE_OF_CANCELLATION_LINE';

        break;
      case ROUTE_RELATED_GEN.ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
        this.title = 'LABEL.ADDENDUM_LINE';
        break;
      default:
        this.title = 'LABEL.ASSIGNMENT_AGREEMENT_LINE';
        break;
    }
  }
  getParentKey() {
    let activeParentName = this.baseService.uiService.getRelatedInfoActiveTableName();
    switch (activeParentName) {
      case ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION:
        return this.baseService.uiService.getKey('noticeofcancellation');
      case ROUTE_RELATED_GEN.ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
        return this.baseService.uiService.getKey('addendumbusinesscollateralagmtable');
      default:
        return this.baseService.uiService.getKey('assignmentagreementtable');
    }
  }
}
