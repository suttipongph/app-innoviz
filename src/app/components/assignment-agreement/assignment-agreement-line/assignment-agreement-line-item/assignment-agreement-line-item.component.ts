import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { AssignmentAgreementLineItemView, BuyerAgreementTableItemView } from 'shared/models/viewModel';
import { AssignmentAgreementLineService } from '../assignment-agreement-line.service';
import { AssignmentAgreementLineItemCustomComponent } from './assignment-agreement-line-item-custom.component';
@Component({
  selector: 'assignment-agreement-line-item',
  templateUrl: './assignment-agreement-line-item.component.html',
  styleUrls: ['./assignment-agreement-line-item.component.scss']
})
export class AssignmentAgreementLineItemComponent extends BaseItemComponent<AssignmentAgreementLineItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  buyerAgreementTableOptions: SelectItems[] = [];

  constructor(
    private service: AssignmentAgreementLineService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: AssignmentAgreementLineItemCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.custom.getParentKey();
  }
  ngOnInit(): void {
    this.custom.setTitle();
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<AssignmentAgreementLineItemView> {
    return this.service.getAssignmentAgreementLineById(this.id);
  }
  getInitialData(): Observable<AssignmentAgreementLineItemView> {
    return this.service.getAssignmentAgreementLineInitialDataByParentId(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    let tempModel = isNullOrUndefined(model) ? this.model : model;
    forkJoin(
      this.baseDropdown.getBuyerAgeementTableByCustomerAndBuyerDropDown([
        tempModel.assignmentAgreementTable_CustomerTableGUID,
        tempModel.assignmentAgreementTable_BuyerTableGUID
      ])
    ).subscribe(
      ([buyerAgreementTable]) => {
        this.buyerAgreementTableOptions = buyerAgreementTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateAssignmentAgreementLine(this.model), isColsing);
    } else {
      super.onCreate(this.service.createAssignmentAgreementLine(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onBuyerAgreementChange(): void {
    if (isNullOrUndefOrEmptyGUID(this.model.buyerAgreementTableGUID)) {
      this.custom.clearModelByBuyerAgreementData(this.model);
    } else {
      this.custom.getBuyerAgreementData(this.model.buyerAgreementTableGUID).subscribe((res) => {
        this.model.referenceAgreementID = res?.referenceAgreementID;
        this.model.startDate = res?.startDate;
        this.model.endDate = res?.endDate;
        this.model.buyerAgreementAmount = res?.buyerAgreementAmount;
      });
    }
  }
}
