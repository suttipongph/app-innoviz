import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AssignmentAgreementLineService } from './assignment-agreement-line.service';
import { AssignmentAgreementLineListComponent } from './assignment-agreement-line-list/assignment-agreement-line-list.component';
import { AssignmentAgreementLineItemComponent } from './assignment-agreement-line-item/assignment-agreement-line-item.component';
import { AssignmentAgreementLineItemCustomComponent } from './assignment-agreement-line-item/assignment-agreement-line-item-custom.component';
import { AssignmentAgreementLineListCustomComponent } from './assignment-agreement-line-list/assignment-agreement-line-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [AssignmentAgreementLineListComponent, AssignmentAgreementLineItemComponent],
  providers: [AssignmentAgreementLineService, AssignmentAgreementLineItemCustomComponent, AssignmentAgreementLineListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [AssignmentAgreementLineListComponent, AssignmentAgreementLineItemComponent]
})
export class AssignmentAgreementLineComponentModule {}
