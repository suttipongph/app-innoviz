import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  AccessModeView,
  AssignmentAgreementLineItemView,
  AssignmentAgreementLineListView,
  BuyerAgreementTableItemView
} from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class AssignmentAgreementLineService {
  serviceKey = 'assignmentAgreementLineGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getAssignmentAgreementLineToList(search: SearchParameter): Observable<SearchResult<AssignmentAgreementLineListView>> {
    const url = `${this.servicePath}/GetAssignmentAgreementLineList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteAssignmentAgreementLine(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteAssignmentAgreementLine`;
    return this.dataGateway.delete(url, row);
  }
  getAssignmentAgreementLineById(id: string): Observable<AssignmentAgreementLineItemView> {
    const url = `${this.servicePath}/GetAssignmentAgreementLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createAssignmentAgreementLine(vmModel: AssignmentAgreementLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateAssignmentAgreementLine`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateAssignmentAgreementLine(vmModel: AssignmentAgreementLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateAssignmentAgreementLine`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getBuyerAgreementTableSumLineById(id: string): Observable<BuyerAgreementTableItemView> {
    const url = `${this.servicePath}/GetBuyerAgreementTableSumLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAssignmentAgreementLineInitialDataByParentId(id: string): Observable<AssignmentAgreementLineItemView> {
    const url = `${this.servicePath}/GetAssignmentAgreementLineInitialDataByParentId/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByAssignmentTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByAssignmentTable/id=${id}`;
    return this.dataGateway.get(url);
  }
}
