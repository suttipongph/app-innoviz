import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { AssignmentAgreementLineListView } from 'shared/models/viewModel';
import { AssignmentAgreementLineService } from '../assignment-agreement-line.service';
import { AssignmentAgreementLineListCustomComponent } from './assignment-agreement-line-list-custom.component';

@Component({
  selector: 'assignment-agreement-line-list',
  templateUrl: './assignment-agreement-line-list.component.html',
  styleUrls: ['./assignment-agreement-line-list.component.scss']
})
export class AssignmentAgreementLineListComponent extends BaseListComponent<AssignmentAgreementLineListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  buyerAgreementTableOptions: SelectItems[] = [];

  constructor(public custom: AssignmentAgreementLineListCustomComponent, private service: AssignmentAgreementLineService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.custom.getParentKey();
  }
  ngOnInit(): void {
    this.custom.setTitle();
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    if (isUpdateMode(this.parentId)) {
      this.custom.setAccessModeByParentStatus(this.parentId).subscribe((result) => {
        this.setDataGridOption();
      });
    }
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, 'AssignmentAgreementLineListPage'));
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBuyerAgreementTableDropDown(this.buyerAgreementTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'assignmentAgreementLineGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.BUYER_AGREEMENT_ID',
        textKey: 'buyerAgreementTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        sortingKey: 'BuyerAgreementTable_BuyerAgreementId',
        masterList: this.buyerAgreementTableOptions,
        searchingKey: 'buyerAgreementTableGUID'
      },
      {
        label: 'LABEL.REFERENCE_AGREEMENT_ID',
        textKey: 'referenceAgreementID',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'assignMentAgreementTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<AssignmentAgreementLineListView>> {
    return this.service.getAssignmentAgreementLineToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteAssignmentAgreementLine(row));
  }
}
