import { AssignmentAgreementTableService } from 'components/assignment-agreement/assignment-agreement-table/assignment-agreement-table.service';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView } from 'shared/models/viewModel';
import { AssignmentAgreementLineService } from '../assignment-agreement-line.service';
const ASSIGNMENT_AGREEMENT_TABLE = 'assignmentagreementtable';
export class AssignmentAgreementLineListCustomComponent {
  accessModeView: AccessModeView = new AccessModeView();
  baseService: BaseServiceModel<AssignmentAgreementLineService>;
  title: string = null;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setAccessModeByParentStatus(parentId: string): Observable<AccessModeView> {
    return this.baseService.service.getAccessModeByAssignmentTable(parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
        this.accessModeView.canView = true;
      })
    );
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
  setTitle() {
    let activeParentName = this.baseService.uiService.getRelatedInfoActiveTableName();
    switch (activeParentName) {
      case ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION:
        this.title = 'LABEL.NOTICE_OF_CANCELLATION_LINE';
        break;
      case ROUTE_RELATED_GEN.ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
        this.title = 'LABEL.ADDENDUM_LINE';
        break;
      default:
        this.title = 'LABEL.ASSIGNMENT_AGREEMENT_LINE';
        break;
    }
  }
  getParentKey(): string {
    let activeParentName = this.baseService.uiService.getRelatedInfoActiveTableName();
    switch (activeParentName) {
      case ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION:
      case ROUTE_RELATED_GEN.ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
        return this.baseService.uiService.getKey(activeParentName);
      default:
        return this.baseService.uiService.getKey(ASSIGNMENT_AGREEMENT_TABLE);
    }
  }
}
