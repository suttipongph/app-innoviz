import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AssignmentAgreementLineListComponent } from './assignment-agreement-line-list.component';

describe('AssignmentAgreementLineListViewComponent', () => {
  let component: AssignmentAgreementLineListComponent;
  let fixture: ComponentFixture<AssignmentAgreementLineListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AssignmentAgreementLineListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentAgreementLineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
