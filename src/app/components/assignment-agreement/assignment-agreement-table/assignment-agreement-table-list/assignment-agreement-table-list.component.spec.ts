import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AssignmentAgreementTableListComponent } from './assignment-agreement-table-list.component';

describe('AssignmentAgreementTableListViewComponent', () => {
  let component: AssignmentAgreementTableListComponent;
  let fixture: ComponentFixture<AssignmentAgreementTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AssignmentAgreementTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentAgreementTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
