import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AgreementDocType, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AssignmentAgreementTableService } from '../assignment-agreement-table.service';
export class AssignmentAgreementTableListCustomComponent {
  baseService: BaseServiceModel<AssignmentAgreementTableService>;
  title: string = null;
  hasFunction = false;

  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  getNewAssigmentAgreementPageStatus(): boolean {
    const masterRoute = this.baseService.uiService.getMasterRoute(1);
    return masterRoute === 'new' ? true : false;
  }
  setTitle(passingObj: any) {
    if (!isNullOrUndefined(passingObj)) {
      let agreementDocType: AgreementDocType = passingObj.agreementDocType;
      switch (agreementDocType) {
        case AgreementDocType.NoticeOfCancellation:
          this.hasFunction = true;
          this.title = 'LABEL.NOTICE_OF_CANCELLATION';
          break;
        case AgreementDocType.Addendum:
          this.title = 'LABEL.ADDENDUM';
          break;
        default:
          if (this.baseService.uiService.getMasterRoute(1) === 'new') {
            this.title = 'LABEL.NEW_ASSIGNMENT_AGREEMENT';
          } else {
            this.title = 'LABEL.ALL_ASSIGNMENT_AGREEMENT';
          }
          break;
      }
    } else {
      if (this.baseService.uiService.getMasterRoute(1) === 'new') {
        this.title = 'LABEL.NEW_ASSIGNMENT_AGREEMENT';
      } else {
        this.title = 'LABEL.ALL_ASSIGNMENT_AGREEMENT';
      }
    }
  }

  setPreparingFunctionDataAndValidation(
    path: string,
    parentId: string,
  ): Observable<any> {
    switch (path) {
      case ROUTE_FUNCTION_GEN.GENERATE_NOTICE_OF_CANCELLATION:
        return this.baseService.service.getGenerateNoticeOfCancellationValidation(parentId).pipe(
          map((res) => {
            if (res) {
              return { assignmentAgreementItemView: res };
            } else {
              return null;
            }
          })
        );
      default:
        break;
    }
  }
}
