import { CREDIT_APP_REQUEST_TYPE } from './../../../../shared/constants/enumConstant';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, ROUTE_FUNCTION_GEN, SortType } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { AssignmentAgreementTableItemView, AssignmentAgreementTableListView } from 'shared/models/viewModel';
import { AssignmentAgreementTableService } from '../assignment-agreement-table.service';
import { AssignmentAgreementTableListCustomComponent } from './assignment-agreement-table-list-custom.component';

@Component({
  selector: 'assignment-agreement-table-list',
  templateUrl: './assignment-agreement-table-list.component.html',
  styleUrls: ['./assignment-agreement-table-list.component.scss']
})
export class AssignmentAgreementTableListComponent extends BaseListComponent<AssignmentAgreementTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  agreementDocTypeOptions: SelectItems[] = [];
  assignmentMethodOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  consortiumTableOptions: SelectItems[] = [];
  custBankOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  creditAppRequestTableOptions: SelectItems[] = [];
  creditAppReqAssignmentOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  isNewAssignment = true;

  constructor(
    public custom: AssignmentAgreementTableListCustomComponent,
    private service: AssignmentAgreementTableService,
    private currentActivatedRoute: ActivatedRoute,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {
    let passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setTitle(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
    this.onEnumLoader();
    this.setFunctionOptions();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getAgreementDocTypeEnumDropDown(this.agreementDocTypeOptions);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getAssignmentMethodDropDown(this.assignmentMethodOptions);
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
    this.baseDropdown.getCreditAppRequestTableDropDown(this.creditAppRequestTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'assignmentAgreementTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.INTERNAL_ASSIGNMENT_AGREEMENT_ID',
        textKey: 'internalAssignmentAgreementId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.ASSIGNMENT_AGREEMENT_ID',
        textKey: 'assignmentAgreementId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },

      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CREDIT_APP_REQUEST_ID',
        textKey: 'creditAppRequestTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditAppRequestTableOptions,
        searchingKey: 'refCreditAppRequestTableGUID',
        sortingKey: 'creditAppRequestTable_CreditAppRequestId'
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerTableOptions,
        searchingKey: 'buyerTableGUID',
        sortingKey: 'buyerTable_BuyerId'
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId'
      },
      {
        label: 'LABEL.ASSIGNMENT_METHOD_ID',
        textKey: 'assignmentMethod_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.assignmentMethodOptions,
        searchingKey: 'assignmentMethodGUID',
        sortingKey: 'assignmentMethod_AssignmentMethodId'
      },
      {
        label: 'LABEL.AGREEMENT_DATE',
        textKey: 'agreementDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.ASSIGNMENT_AGREEMENT_AMOUNT',
        textKey: 'assignmentAgreementAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.STATUS_ID',
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentStatusOptions,
        sortingKey: 'documentStatus_StatusId',
        searchingKey: 'documentStatusGUID'
      },
      {
        label: 'LABEL.AGREEMENT_DOCUMENT_TYPE',
        textKey: 'agreementDocType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.agreementDocTypeOptions
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<AssignmentAgreementTableListView>> {
    return this.service.getAssignmentAgreementTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteAssignmentAgreementTable(row));
  }

  setFunctionOptions(): void {
    this.functionItems = [
      {
        label: 'LABEL.GENERATE',
        items: [
          {
            label: 'LABEL.NOTICE_OF_CANCELLATION',
            command: () => this.setPreparingFunctionDataAndValidation(ROUTE_FUNCTION_GEN.GENERATE_NOTICE_OF_CANCELLATION, this.parentId)
          }
        ]
      }
    ];
    super.setFunctionOptionsMapping();
  }

  setPreparingFunctionDataAndValidation(path: string, parentId: string): void {
    this.custom.setPreparingFunctionDataAndValidation(path, parentId).subscribe((res) => {
      if (!isNullOrUndefined(res)) {
        this.toFunction({
          path: path,
          parameters: res
        });
      }
    });
  }
}
