import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AssignmentAgreementTableService } from './assignment-agreement-table.service';
import { AssignmentAgreementTableListComponent } from './assignment-agreement-table-list/assignment-agreement-table-list.component';
import { AssignmentAgreementTableItemComponent } from './assignment-agreement-table-item/assignment-agreement-table-item.component';
import { AssignmentAgreementTableItemCustomComponent } from './assignment-agreement-table-item/assignment-agreement-table-item-custom.component';
import { AssignmentAgreementTableListCustomComponent } from './assignment-agreement-table-list/assignment-agreement-table-list-custom.component';
import { AssignmentAgreementLineComponentModule } from '../assignment-agreement-line/assignment-agreement-line.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, AssignmentAgreementLineComponentModule],
  declarations: [AssignmentAgreementTableListComponent, AssignmentAgreementTableItemComponent],
  providers: [AssignmentAgreementTableService, AssignmentAgreementTableItemCustomComponent, AssignmentAgreementTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [AssignmentAgreementTableListComponent, AssignmentAgreementTableItemComponent]
})
export class AssignmentAgreementTableComponentModule {}
