import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  AgreementTableInfoItemView,
  AssignmentAgreementTableItemView,
  AssignmentAgreementTableListView,
  GenAssignmentAgmNoticeOfCancelView
} from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class AssignmentAgreementTableService {
  serviceKey = 'assignmentAgreementTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getAssignmentAgreementTableToList(search: SearchParameter): Observable<SearchResult<AssignmentAgreementTableListView>> {
    const url = `${this.servicePath}/GetAssignmentAgreementTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteAssignmentAgreementTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteAssignmentAgreementTable`;
    return this.dataGateway.delete(url, row);
  }
  getAssignmentAgreementTableById(id: string): Observable<AssignmentAgreementTableItemView> {
    const url = `${this.servicePath}/GetAssignmentAgreementTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createAssignmentAgreementTable(vmModel: AssignmentAgreementTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateAssignmentAgreementTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateAssignmentAgreementTable(vmModel: AssignmentAgreementTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateAssignmentAgreementTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(): Observable<AssignmentAgreementTableItemView> {
    const url = `${this.servicePath}/GetAssignmentAgreementTableInitialData`;
    return this.dataGateway.get(url);
  }
  getGenerateNoticeOfCancellationValidation(id: string): Observable<AssignmentAgreementTableItemView> {
    const url = `${this.servicePath}/GetGenerateNoticeOfCancellationValidation/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAgreementTableInfoByAssignmentAgreement(id: string): Observable<AgreementTableInfoItemView> {
    const url = `${this.servicePath}/GetAgreementTableInfoByAssignmentAgreement/id=${id}`;
    return this.dataGateway.get(url);
  }
  getVisibleManageMenu(id: string): Observable<number[]> {
    const url = `${this.servicePath}/GetVisibleManageMenu/id=${id}`;
    return this.dataGateway.get(url);
  }
  getUpdateAssignmentAgreementValidation(vmModel: AssignmentAgreementTableItemView): Observable<boolean> {
    const url = `${this.servicePath}/GetUpdateAssignmentAgreementValidation`;
    return this.dataGateway.post(url, vmModel);
  }
}
