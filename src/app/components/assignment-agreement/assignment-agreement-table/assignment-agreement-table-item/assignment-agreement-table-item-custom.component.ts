import { forkJoin, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { AgreementDocType, AppConst, AssignmentAgreementStatus, EmptyGuid, RefType, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { toMapModel } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems, TranslateModel } from 'shared/models/systemModel';
import {
  AssignmentAgreementLineItemView,
  AssignmentAgreementTableItemView,
  CreditAppReqAssignmentItemView,
  CustomerTableItemView,
  ManageAgreementView,
  BuyerTableItemView
} from 'shared/models/viewModel';
import { PrintSetBookDocTransView } from 'shared/models/viewModel/printSetBookDocTransView';
import { AssignmentAgreementTableService } from '../assignment-agreement-table.service';

const creatingGroup = [
  'AGREEMENT_DOC_TYPE',
  'SIGNING_DATE',
  'DOCUMENT_STATUS_GUID',
  'DOCUMENT_REASON_GUID',
  'CUSTOMER_NAME',
  'BUYER_NAME',
  'REF_ASSIGNMENT_AGREEMENT_TABLE_GUID',
  'REF_ASSIGNMENT_AGREEMENT_ID',
  'ASSIGNMENT_AGREEMENT_TABLE_GUID'
];
const defaultGroup = [
  'INTERNAL_ASSIGNMENT_AGREEMENT_ID',
  'CUSTOMER_TABLE_GUID',
  'BUYER_TABLE_GUID',
  'REF_CREDIT_APP_REQUEST_TABLE_GUID',
  'CREDIT_APP_REQ_ASSIGNMENT_GUID'
];
export class AssignmentAgreementTableItemCustomComponent {
  baseService: BaseServiceModel<AssignmentAgreementTableService>;
  title: string = null;
  getFieldAccessing(model: AssignmentAgreementTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (!isUpdateMode(model.assignmentAgreementTableGUID)) {
      if (!model.isInternalAssignmentAgreementIdManual) {
        fieldAccessing.push({ filedIds: ['INTERNAL_ASSIGNMENT_AGREEMENT_ID'], readonly: true });
      }
      if (!model.isAssignmentAgreementIdManual) {
        fieldAccessing.push({ filedIds: ['ASSIGNMENT_AGREEMENT_ID'], readonly: true });
      }
    } else {
      fieldAccessing.push({ filedIds: defaultGroup, readonly: true });
    }
    fieldAccessing.push({ filedIds: creatingGroup, readonly: true });
    if (model.documentStatus_StatusId === AssignmentAgreementStatus.Draft && !model.isAssignmentAgreementIdManual) {
      fieldAccessing.push({ filedIds: ['ASSIGNMENT_AGREEMENT_ID'], readonly: true });
    } else if (
      model.documentStatus_StatusId === AssignmentAgreementStatus.Posted ||
      model.documentStatus_StatusId === AssignmentAgreementStatus.Sent
    ) {
      fieldAccessing.push({ filedIds: ['ASSIGNMENT_AGREEMENT_ID'], readonly: true });
    } else if (
      [AssignmentAgreementStatus.Signed, AssignmentAgreementStatus.Closed, AssignmentAgreementStatus.Cancelled].some(
        (s) => s === model.documentStatus_StatusId
      )
    ) {
      fieldAccessing.push({ filedIds: [AppConst.ALL_ID], readonly: true });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: AssignmentAgreementTableItemView): Observable<boolean> {
    const statuses = [AssignmentAgreementStatus.Draft, AssignmentAgreementStatus.Posted, AssignmentAgreementStatus.Sent];
    return of(canCreate && canUpdate && statuses.some((s) => s === model.documentStatus_StatusId) ? false : true);
  }
  getCustBankOption(model: AssignmentAgreementTableItemView): Observable<SelectItems[]> {
    if (!isNullOrUndefined(model.customerTableGUID)) {
      return this.baseService.baseDropdown.getCustBankByCustomerDropDown(model.customerTableGUID);
    } else {
      return of([]);
    }
  }
  getCustomerData(id: string, customerOptions: SelectItems[]): CustomerTableItemView {
    if (!isNullOrUndefined(id)) {
      const item = customerOptions.find((f) => f.value === id);
      if (isNullOrUndefined(item)) {
        return null;
      } else {
        return item.rowData as CustomerTableItemView;
      }
    } else null;
  }
  getBuyerName(id: string, buyerTableOptions: SelectItems[]): string {
    if (!isNullOrUndefined(id)) {
      const item = buyerTableOptions.find((f) => f.value === id);
      if (isNullOrUndefined(item)) {
        return null;
      } else {
        return (item.rowData as BuyerTableItemView).buyerName;
      }
    }
  }
  getCreditAppReqAssignmentData(id: string, creditAppReqAssignmentOptions: SelectItems[]): CreditAppReqAssignmentItemView {
    if (!isNullOrUndefined(id)) {
      const item = creditAppReqAssignmentOptions.find((f) => f.value === id);
      if (isNullOrUndefined(item)) {
        return null;
      } else {
        return item.rowData as CreditAppReqAssignmentItemView;
      }
    }
  }
  setTitle(passingObj: any) {
    if (!isNullOrUndefined(passingObj)) {
      let agreementDocType: AgreementDocType = passingObj.agreementDocType;
      switch (agreementDocType) {
        case AgreementDocType.NoticeOfCancellation:
          this.title = 'LABEL.NOTICE_OF_CANCELLATION';
          break;
        case AgreementDocType.Addendum:
          this.title = 'LABEL.ADDENDUM';
          break;
        default:
          if (this.baseService.uiService.getMasterRoute(1) === 'new') {
            this.title = 'LABEL.NEW_ASSIGNMENT_AGREEMENT';
          } else {
            this.title = 'LABEL.ALL_ASSIGNMENT_AGREEMENT';
          }
          break;
      }
    } else {
      if (this.baseService.uiService.getMasterRoute(1) === 'new') {
        this.title = 'LABEL.NEW_ASSIGNMENT_AGREEMENT';
      } else {
        this.title = 'LABEL.ALL_ASSIGNMENT_AGREEMENT';
      }
    }
  }
  getManageAgreementParamModel(
    model: AssignmentAgreementTableItemView,
    customerTableOptions: SelectItems[],
    buyerTableOptions: SelectItems[]
  ): ManageAgreementView {
    const paramModel = new ManageAgreementView();
    toMapModel(model, paramModel);
    paramModel.agreementId = model.assignmentAgreementId;
    paramModel.internalAgreementId = model.internalAssignmentAgreementId;
    paramModel.assignmentMethodId = model.assignmentMethodGUID;
    paramModel.amount = model.assignmentAgreementAmount;
    paramModel.assignmentMethodId = model.assignmentMethod_AssignmentMethodId;
    if (!isNullOrUndefined(model.customerTableGUID)) {
      paramModel.customerTable_Values = customerTableOptions.find((f) => f.value === model.customerTableGUID).label;
    }
    if (!isNullOrUndefined(model.buyerTableGUID)) {
      paramModel.buyerTable_Values = buyerTableOptions.find((f) => f.value === model.buyerTableGUID).label;
    }
    return paramModel;
  }
  setPrintSetBookDocTransData(model: AssignmentAgreementTableItemView): PrintSetBookDocTransView {
    const paramModel = new PrintSetBookDocTransView();
    toMapModel(model, paramModel);
    paramModel.refType = RefType.AssignmentAgreement;
    paramModel.refGUID = model.assignmentAgreementTableGUID;
    paramModel.internalAgreementId = model.internalAssignmentAgreementId;
    paramModel.agreementId = model.assignmentAgreementId;
    paramModel.creditAppRequestDescription = model.description;
    paramModel.agreementDescription = model.description;
    return paramModel;
  }
  setCreditAppRequestDropDownByCustomer(model: AssignmentAgreementTableItemView) {
    if (!isNullOrUndefined(model.customerTableGUID)) {
      return this.baseService.baseDropdown.getCreditAppRequestTableByAssignmentAgreementTableDropDown(model.customerTableGUID);
    } else {
      return of([]);
    }
  }
  setCreditAppReqAssignmentDropDownByCreditAppRequest(model: AssignmentAgreementTableItemView, option: SelectItems[]) {
    model.creditAppReqAssignmentGUID = null;
    this.clearModelByCreditAppRequestAssignment(model);
  }
  clearModelByCreditAppRequestAssignment(model: AssignmentAgreementTableItemView) {
    model.referenceAgreementId = '';
    model.description = '';
    model.assignmentMethodGUID = null;
    model.assignmentAgreementAmount = 0;
    model.buyerTableGUID = null;
  }
  onValidateAssignmentAgreementAmount(model: AssignmentAgreementTableItemView): TranslateModel {
    if (model.assignmentAgreementAmount <= 0) {
      return {
        code: 'ERROR.GREATER_THAN_ZERO',
        parameters: [this.baseService.translate.instant('LABEL.ASSIGNMENT_AGREEMENT_AMOUNT')]
      };
    } else {
      return null;
    }
  }
  getUpdateAssignmentAgreementValidation(
    service: AssignmentAgreementTableService,
    path: string,
    model: AssignmentAgreementTableItemView
  ): Observable<any> {
    return service.getUpdateAssignmentAgreementValidation(model).pipe(
      map((res) => {
        if (res) {
          return { ChequeTableItemView: res };
        } else {
          return null;
        }
      })
    );
  }
  getDropdownByCustomerTable(model: AssignmentAgreementTableItemView, custBankOptions: SelectItems[], creditAppRequestTableOptions: SelectItems[]) {
    forkJoin([this.getCustBankOption(model), this.setCreditAppRequestDropDownByCustomer(model)]).subscribe(
      ([custBank, creditAppRequest]) => {
        custBankOptions = custBank;
        creditAppRequestTableOptions = creditAppRequest;
      },
      (error) => {
        this.baseService.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
}
