import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import {
  EmptyGuid,
  ROUTE_RELATED_GEN,
  ROUTE_FUNCTION_GEN,
  AgreementDocType,
  RefType,
  ManageAgreementAction,
  ProductType,
  AssignmentAgreementStatus
} from 'shared/constants';
import { modelRegister, toMapModel } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, AssignmentAgreementTableItemView, ManageAgreementView, RefIdParm } from 'shared/models/viewModel';
import { PrintSetBookDocTransView } from 'shared/models/viewModel/printSetBookDocTransView';
import { AssignmentAgreementTableService } from '../assignment-agreement-table.service';
import { AssignmentAgreementTableItemCustomComponent } from './assignment-agreement-table-item-custom.component';
@Component({
  selector: 'assignment-agreement-table-item',
  templateUrl: './assignment-agreement-table-item.component.html',
  styleUrls: ['./assignment-agreement-table-item.component.scss']
})
export class AssignmentAgreementTableItemComponent extends BaseItemComponent<AssignmentAgreementTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  isManual: boolean = false;
  agreementDocTypeOptions: SelectItems[] = [];
  assignmentMethodOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  consortiumTableOptions: SelectItems[] = [];
  custBankOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  creditAppRequestTableOptions: SelectItems[] = [];
  creditAppReqAssignmentOptions: SelectItems[] = [];
  manageMenuVisibility: number[] = [];
  passingObj: any;
  constructor(
    private service: AssignmentAgreementTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: AssignmentAgreementTableItemCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {
    this.passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setTitle(this.passingObj);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {
    this.baseDropdown.getAgreementDocTypeEnumDropDown(this.agreementDocTypeOptions);
  }
  getById(): Observable<AssignmentAgreementTableItemView> {
    return this.service.getAssignmentAgreementTableById(this.id);
  }
  getInitialData(): Observable<AssignmentAgreementTableItemView> {
    return this.service.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.service.getVisibleManageMenu(this.id).subscribe((res) => {
          this.manageMenuVisibility = res;
          this.setRelatedInfoOptions(result);
          this.setFunctionOptions(result);
        });
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    let tempModel: AssignmentAgreementTableItemView = this.isUpdateMode ? model : this.model;
    forkJoin([
      this.baseDropdown.getAssignmentMethodDropDown(),
      this.baseDropdown.getBuyerTableDropDown(),
      this.baseDropdown.getConsortiumTableDropDown(),
      this.baseDropdown.getCustomerTableDropDown(),
      this.baseService.baseDropdown.getCreditAppReqAssignmentByCreditAppRequestTableDropDown(tempModel.refCreditAppRequestTableGUID)
    ]).subscribe(
      ([assignmentMethod, buyerTable, consortiumTable, customerTable, creditAppReqAssignment]) => {
        this.assignmentMethodOptions = assignmentMethod;
        this.buyerTableOptions = buyerTable;
        this.consortiumTableOptions = consortiumTable;
        this.customerTableOptions = customerTable;
        this.creditAppReqAssignmentOptions = creditAppReqAssignment;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  setRelatedInfoOptions(model: AssignmentAgreementTableItemView): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        command: () => {
          const refIdParm: RefIdParm = { refType: RefType.AssignmentAgreement, refGUID: this.model.assignmentAgreementTableGUID };
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: refIdParm
          });
        }
      },
      { label: 'LABEL.MEMO', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.MEMO }) },
      {
        label: 'LABEL.AGREEMENT_INFORMATION',
        command: () => {
          this.service.getAgreementTableInfoByAssignmentAgreement(this.id).subscribe((result) => {
            if (isNullOrUndefined(result)) {
              this.toRelatedInfo({ path: `${ROUTE_RELATED_GEN.AGREEMENT_TABLE_INFO}/${EmptyGuid}` });
            } else {
              this.toRelatedInfo({ path: `${ROUTE_RELATED_GEN.AGREEMENT_TABLE_INFO}/${result.agreementTableInfoGUID}` });
            }
          });
        }
      },
      {
        label: 'LABEL.ASSIGNMENT_AGREEMENT_SETTLEMENT',
        visible: model.agreementDocType !== AgreementDocType.NoticeOfCancellation && model.agreementDocType !== AgreementDocType.Addendum,
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ASSIGNMENT_AGREEMENT_SETTLEMENT,
            parameters: {
              assignmentAgreementTableGUID: this.model.assignmentAgreementTableGUID
            }
          })
      },
      {
        label: 'LABEL.SERVICE_FEE',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.SERVICE_FEE_TRANS,
            parameters: {
              refId: model.internalAssignmentAgreementId,
              taxDate: model.agreementDate,
              productType: ProductType.Factoring,
              invoiceTable_CustomerTableGUID: model.customerTableGUID
            }
          })
      },
      {
        label: 'LABEL.JOINT_VENTURE',
        command: () =>
          this.toRelatedInfo({ path: ROUTE_RELATED_GEN.JOINT_VENTURE_TRANS, parameters: { statusCode: this.model.documentStatus_StatusId } })
      },
      { label: 'LABEL.CONSORTIUM', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.CONSORTIUM_TRANS }) },
      { label: 'LABEL.BOOKMARK_DOCUMENT', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.BOOKMARK_DOCUMENT_TRANS }) },
      {
        label: 'LABEL.NOTICE_OF_CANCELLATION',
        visible: model.agreementDocType !== AgreementDocType.NoticeOfCancellation && model.agreementDocType !== AgreementDocType.Addendum,
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION,
            parameters: { agreementDocType: AgreementDocType.NoticeOfCancellation }
          })
      },

      {
        label: 'LABEL.INQUIRY',
        visible: model.agreementDocType !== AgreementDocType.NoticeOfCancellation && model.agreementDocType !== AgreementDocType.Addendum,
        items: [
          {
            label: 'LABEL.INQUIRY_ASSIGNMENT_AGREEMENT_OUTSTANDING',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.INQUIRY_ASSIGNMENT_AGREEMENT_OUTSTANDING
              })
          }
        ]
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(model: AssignmentAgreementTableItemView): void {
    this.functionItems = [
      {
        label: 'LABEL.MANAGE',
        items: [
          {
            label: 'LABEL.POST',
            disabled: !this.manageMenuVisibility.some((s) => s === ManageAgreementAction.Post),
            command: () => {
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.POST_AGREEMENT,
                parameters: {
                  refType: RefType.AssignmentAgreement,
                  refGuid: this.id,
                  action: ManageAgreementAction.Post
                }
              });
            }
          },
          {
            label: 'LABEL.SEND',
            disabled: !this.manageMenuVisibility.some((s) => s === ManageAgreementAction.Send),
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.SEND_AGREEMENT,
                parameters: {
                  refType: RefType.AssignmentAgreement,
                  refGuid: this.id,
                  action: ManageAgreementAction.Send
                }
              })
          },
          {
            label: 'LABEL.SIGN',
            disabled: !this.manageMenuVisibility.some((s) => s === ManageAgreementAction.Sign),
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.SIGN_AGREEMENT,
                parameters: {
                  refType: RefType.AssignmentAgreement,
                  refGuid: this.id,
                  action: ManageAgreementAction.Sign
                }
              })
          },
          {
            label: 'LABEL.CLOSE',
            disabled: !this.manageMenuVisibility.some((s) => s === ManageAgreementAction.Close),
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.CLOSE_AGREEMENT,
                parameters: {
                  refType: RefType.AssignmentAgreement,
                  refGuid: this.id,
                  action: ManageAgreementAction.Close
                }
              })
          },
          {
            label: 'LABEL.CANCEL',
            disabled: !this.manageMenuVisibility.some((s) => s === ManageAgreementAction.Cancel),
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.CANCEL_AGREEMENT,
                parameters: {
                  refType: RefType.AssignmentAgreement,
                  refGuid: this.id,
                  action: ManageAgreementAction.Cancel
                }
              })
          }
        ]
      },
      {
        label: 'LABEL.PRINT',
        items: [
          {
            label: 'LABEL.BOOKMARK_DOCUMENT',
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.PRINT_SET_BOOKMARK_DOCUMENT_TRANSACTION,
                parameters: {
                  model: this.setPrintSetBookDocTransData()
                }
              })
          }
        ]
      },
      {
        label: 'LABEL.UPDATE_ASSIGNMENT_AGREEMENT_AMOUNT',
        disabled: !(model.documentStatus_StatusId === AssignmentAgreementStatus.Signed && model.agreementDocType === AgreementDocType.New),
        command: () => this.getUpdateAssignmentAgreementValidation(ROUTE_FUNCTION_GEN.UPDATE_ASSIGNMENT_AGREEMENT_AMOUNT)
      }
    ];
    super.setFunctionOptionsMapping();
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateAssignmentAgreementTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createAssignmentAgreementTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onCustomerChange(isBinding?: boolean): void {
    if (!isBinding) {
      const item = this.custom.getCustomerData(this.model.customerTableGUID, this.customerTableOptions);
      this.custBankOptions.length = 0;
      this.model.customerName = item?.name;
      this.model.customerAltName = item?.altName;
      this.model.consortiumTableGUID = null;
      this.model.custBankGUID = null;
    }
    forkJoin([this.custom.getCustBankOption(this.model), this.custom.setCreditAppRequestDropDownByCustomer(this.model)]).subscribe(
      ([custBank, creditAppRequest]) => {
        this.custBankOptions = custBank;
        this.creditAppRequestTableOptions = creditAppRequest;
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  onBuyerChange(): void {
    this.model.buyerName = this.custom.getBuyerName(this.model.buyerTableGUID, this.buyerTableOptions);
  }
  getManageAgreementParamModel(): ManageAgreementView {
    return this.custom.getManageAgreementParamModel(this.model, this.customerTableOptions, this.buyerTableOptions);
  }
  setPrintSetBookDocTransData(): PrintSetBookDocTransView {
    return this.custom.setPrintSetBookDocTransData(this.model);
  }
  onCreditAppRequestChange(): void {
    this.custom.setCreditAppReqAssignmentDropDownByCreditAppRequest(this.model, this.creditAppReqAssignmentOptions);
    this.getCreditAppReqAssignmentByCreditAppRequestTableDropDown();
  }
  getCreditAppReqAssignmentByCreditAppRequestTableDropDown() {
    if (!isNullOrUndefOrEmptyGUID(this.model.refCreditAppRequestTableGUID)) {
      this.baseService.baseDropdown
        .getCreditAppReqAssignmentByCreditAppRequestTableDropDown(this.model.refCreditAppRequestTableGUID)
        .subscribe((result) => {
          this.creditAppReqAssignmentOptions = result;
        });
    } else {
      this.creditAppReqAssignmentOptions.length = 0;
    }
  }
  onCreditAppReqAssignmentChange(): void {
    const item = this.custom.getCreditAppReqAssignmentData(this.model.creditAppReqAssignmentGUID, this.creditAppReqAssignmentOptions);
    if (!isNullOrUndefined(item)) {
      this.model.referenceAgreementId = item.referenceAgreementId;
      this.model.description = item.description;
      this.model.assignmentMethodGUID = item.assignmentMethodGUID;
      this.model.assignmentAgreementAmount = item.assignmentAgreementAmount;
      this.model.buyerTableGUID = item.buyerTableGUID;
      if (!isNullOrUndefined(this.model.buyerTableGUID) && !isNullOrUndefined(this.buyerTableOptions)) {
        this.model.buyerName = this.custom.getBuyerName(this.model.buyerTableGUID, this.buyerTableOptions);
      }
    } else {
      this.custom.clearModelByCreditAppRequestAssignment(this.model);
    }
  }
  getUpdateAssignmentAgreementValidation(path: string): void {
    this.custom.getUpdateAssignmentAgreementValidation(this.service, path, this.model).subscribe((res) => {
      if (!isNullOrUndefined(res)) {
        this.toFunction({
          path: path,
          parameters: {
            refType: RefType.AssignmentAgreement,
            refGuid: this.id,
            model: this.getManageAgreementParamModel()
          }
        });
      }
    });
  }
}
