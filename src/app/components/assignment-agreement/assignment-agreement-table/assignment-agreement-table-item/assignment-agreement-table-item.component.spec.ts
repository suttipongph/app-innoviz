import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmentAgreementTableItemComponent } from './assignment-agreement-table-item.component';

describe('AssignmentAgreementTableItemViewComponent', () => {
  let component: AssignmentAgreementTableItemComponent;
  let fixture: ComponentFixture<AssignmentAgreementTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AssignmentAgreementTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentAgreementTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
