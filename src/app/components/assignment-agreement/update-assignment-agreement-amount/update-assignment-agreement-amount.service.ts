import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { UpdateAssignmentAgreementAmountView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class UpdateAssignmentAgreementAmountService {
  serviceKey = 'updateAssignmentAgreementAmountGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getUpdateAssignmentAgreementAmountToList(search: SearchParameter): Observable<SearchResult<UpdateAssignmentAgreementAmountView>> {
    const url = `${this.servicePath}/GetUpdateAssignmentAgreementAmountList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteUpdateAssignmentAgreementAmount(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteUpdateAssignmentAgreementAmount`;
    return this.dataGateway.delete(url, row);
  }
  getUpdateAssignmentAgreementAmountById(id: string): Observable<UpdateAssignmentAgreementAmountView> {
    const url = `${this.servicePath}/GetUpdateAssignmentAgreementAmountById/id=${id}`;
    return this.dataGateway.get(url);
  }
  updateUpdateAssignmentAgreementAmount(vmModel: UpdateAssignmentAgreementAmountView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateUpdateAssignmentAgreementAmount`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
