import { Observable, of } from 'rxjs';
import { letProto } from 'rxjs-compat/operator/let';
import { EmptyGuid } from 'shared/constants';
import { isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { DocumentReasonItemView, UpdateAssignmentAgreementAmountView } from 'shared/models/viewModel';

const firstGroup = [
  'INTERNAL_ASSIGNMENT_AGREEMENT_ID',
  'ASSIGNMENT_AGREEMENT_ID',
  'DESCRIPTION',
  'ASSIGNMENT_METHOD_ID',
  'AGREEMENT_DATE',
  'ASSIGNMENT_AGREEMENT_AMOUNT',
  'CUSTOMER_TABLE_GUID',
  'CUSTOMER_NAME',
  'BUYER_TABLE_GUID',
  'BUYER_NAME'
];

export class UpdateAssignmentAgreementAmountCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: UpdateAssignmentAgreementAmountView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: UpdateAssignmentAgreementAmountView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<UpdateAssignmentAgreementAmountView> {
    let model = new UpdateAssignmentAgreementAmountView();
    model.updateAssignmentAgreementAmountGUID = EmptyGuid;
    return of(model);
  }
  onReasonChange(e: any, model: UpdateAssignmentAgreementAmountView, documentReasonOptions: SelectItems[]) {
    const row: DocumentReasonItemView = isNullOrUndefOrEmptyGUID(e)
      ? new DocumentReasonItemView()
      : (documentReasonOptions.find((e) => e.value === model.documentReasonGUID).rowData as DocumentReasonItemView);
    model.reasonRemark = row.description;
  }
}
