import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAssignmentAgreementAmountComponent } from './update-assignment-agreement-amount.component';

describe('UpdateAssignmentAgreementAmountViewComponent', () => {
  let component: UpdateAssignmentAgreementAmountComponent;
  let fixture: ComponentFixture<UpdateAssignmentAgreementAmountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UpdateAssignmentAgreementAmountComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAssignmentAgreementAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
