import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateAssignmentAgreementAmountService } from './update-assignment-agreement-amount.service';
import { UpdateAssignmentAgreementAmountComponent } from './update-assignment-agreement-amount/update-assignment-agreement-amount.component';

import { UpdateAssignmentAgreementAmountCustomComponent } from './update-assignment-agreement-amount/update-assignment-agreement-amount-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [UpdateAssignmentAgreementAmountComponent],
  providers: [UpdateAssignmentAgreementAmountService, UpdateAssignmentAgreementAmountCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [UpdateAssignmentAgreementAmountComponent]
})
export class UpdateAssignmentAgreementAmountComponentModule {}
