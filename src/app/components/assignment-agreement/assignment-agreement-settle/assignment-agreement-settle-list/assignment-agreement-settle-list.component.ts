import { Component, Input, OnInit } from '@angular/core';
import { makeStateKey } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { AssignmentAgreementSettleListView } from 'shared/models/viewModel';
import { AssignmentAgreementSettleService } from '../assignment-agreement-settle.service';
import { AssignmentAgreementSettleListCustomComponent } from './assignment-agreement-settle-list-custom.component';

@Component({
  selector: 'assignment-agreement-settle-list',
  templateUrl: './assignment-agreement-settle-list.component.html',
  styleUrls: ['./assignment-agreement-settle-list.component.scss']
})
export class AssignmentAgreementSettleListComponent extends BaseListComponent<AssignmentAgreementSettleListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  assignmentAgreementSettleOptions: SelectItems[] = [];
  assignmentAgreementTableOptions: SelectItems[] = [];
  buyerAgreementTableOptions: SelectItems[] = [];
  documentReasonOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  constructor(
    public custom: AssignmentAgreementSettleListCustomComponent,
    private service: AssignmentAgreementSettleService,
    public uiControllerService: UIControllerService,
    public currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBuyerAgreementTableDropDown(this.buyerAgreementTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = true;
    this.option.canDelete = false;
    this.option.key = 'assignmentAgreementSettleGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.SETTLED_DATE',
        textKey: 'settledDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.DESC,
        sortingKey: 'settledDate'
      },
      {
        label: 'LABEL.SETTLED_AMOUNT',
        textKey: 'settledAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.DESC,
        sortingKey: 'settledAmount'
      },
      {
        label: 'LABEL.BUYER_AGREEMENT_ID',
        textKey: 'buyerAgreementTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerAgreementTableOptions
      },
      {
        label: null,
        textKey: 'assignmentAgreementTableGUID ',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<AssignmentAgreementSettleListView>> {
    return this.service.getAssignmentAgreementSettleList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteAssignmentAgreementSettle(row));
  }
}
