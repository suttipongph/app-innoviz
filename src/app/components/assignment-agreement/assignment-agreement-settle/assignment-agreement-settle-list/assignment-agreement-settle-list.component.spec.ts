import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AssignmentAgreementSettleListComponent } from './assignment-agreement-settle-list.component';

describe('AssignmentAgreementSettleListViewComponent', () => {
  let component: AssignmentAgreementSettleListComponent;
  let fixture: ComponentFixture<AssignmentAgreementSettleListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AssignmentAgreementSettleListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentAgreementSettleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
