import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AssignmentAgreementSettleItemView, AssignmentAgreementSettleListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class AssignmentAgreementSettleService {
  serviceKey = 'assignmentAgreementSettleGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getAssignmentAgreementSettleList(search: SearchParameter): Observable<SearchResult<AssignmentAgreementSettleListView>> {
    const url = `${this.servicePath}/GetAssignmentAgreementSettleList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteAssignmentAgreementSettle(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteAssignmentAgreementSettle`;
    return this.dataGateway.delete(url, row);
  }
  getAssignmentAgreementSettleById(id: string): Observable<AssignmentAgreementSettleItemView> {
    const url = `${this.servicePath}/GetAssignmentAgreementSettleById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createAssignmentAgreementSettle(vmModel: AssignmentAgreementSettleItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateAssignmentAgreementSettle`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateAssignmentAgreementSettle(vmModel: AssignmentAgreementSettleItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateAssignmentAgreementSettle`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
