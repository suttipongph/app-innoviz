import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AssignmentAgreementSettleService } from './assignment-agreement-settle.service';
import { AssignmentAgreementSettleListComponent } from './assignment-agreement-settle-list/assignment-agreement-settle-list.component';
import { AssignmentAgreementSettleItemComponent } from './assignment-agreement-settle-item/assignment-agreement-settle-item.component';
import { AssignmentAgreementSettleItemCustomComponent } from './assignment-agreement-settle-item/assignment-agreement-settle-item-custom.component';
import { AssignmentAgreementSettleListCustomComponent } from './assignment-agreement-settle-list/assignment-agreement-settle-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [AssignmentAgreementSettleListComponent, AssignmentAgreementSettleItemComponent],
  providers: [AssignmentAgreementSettleService, AssignmentAgreementSettleItemCustomComponent, AssignmentAgreementSettleListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [AssignmentAgreementSettleListComponent, AssignmentAgreementSettleItemComponent]
})
export class AssignmentAgreementSettleComponentModule {}
