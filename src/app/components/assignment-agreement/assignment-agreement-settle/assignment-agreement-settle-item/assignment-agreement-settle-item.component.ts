import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, RefType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { AssignmentAgreementSettleItemView } from 'shared/models/viewModel';
import { AssignmentAgreementSettleService } from '../assignment-agreement-settle.service';
import { AssignmentAgreementSettleItemCustomComponent } from './assignment-agreement-settle-item-custom.component';
@Component({
  selector: 'assignment-agreement-settle-item',
  templateUrl: './assignment-agreement-settle-item.component.html',
  styleUrls: ['./assignment-agreement-settle-item.component.scss']
})
export class AssignmentAgreementSettleItemComponent extends BaseItemComponent<AssignmentAgreementSettleItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  assignmentAgreementSettleOptions: SelectItems[] = [];
  assignmentAgreementTableOptions: SelectItems[] = [];
  buyerAgreementTableOptions: SelectItems[] = [];
  documentReasonOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];

  disabledFnUpdateAssignmentAgreementsettle: boolean = true;
  constructor(
    private service: AssignmentAgreementSettleService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: AssignmentAgreementSettleItemCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<AssignmentAgreementSettleItemView> {
    return this.service.getAssignmentAgreementSettleById(this.id);
  }
  getInitialData(): Observable<AssignmentAgreementSettleItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(
      this.baseDropdown.getAssignmentAgreementSettleDropDown(),
      this.baseDropdown.getAssignmentAgreementTableDropDown(),
      this.baseDropdown.getBuyerAgreementTableDropDown(),
      this.baseDropdown.getDocumentReasonDropDown(RefType.AssignmentAgreement.toString())
    ).subscribe(
      ([assignmentAgreementSettle, assignmentAgreementTable, buyerAgreementTable, documentReason]) => {
        this.assignmentAgreementSettleOptions = assignmentAgreementSettle;
        this.assignmentAgreementTableOptions = assignmentAgreementTable;
        this.buyerAgreementTableOptions = buyerAgreementTable;
        this.documentReasonOptions = documentReason;

        if (!isNullOrUndefined(model)) {
          this.model = model;

          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(false, false, this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  setFunctionOptions(model: AssignmentAgreementSettleItemView): void {
    console.log(model.refAssignmentAgreementSettleGUID);
    this.functionItems = [
      {
        label: 'LABEL.UPDATE_ASSIGNMENT_AGREEMENT_SETTLEMENT',
        disabled: !(model.refAssignmentAgreementSettleGUID == null),
        visible: true,
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.UPDATE_ASSIGNMENT_AGREEMENT_SETTLE })
      }
    ];
    super.setFunctionOptionsMapping();
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateAssignmentAgreementSettle(this.model), isColsing);
    } else {
      super.onCreate(this.service.createAssignmentAgreementSettle(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
