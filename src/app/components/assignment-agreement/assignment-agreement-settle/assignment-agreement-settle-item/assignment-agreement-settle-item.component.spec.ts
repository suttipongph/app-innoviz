import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmentAgreementSettleItemComponent } from './assignment-agreement-settle-item.component';

describe('AssignmentAgreementSettleItemViewComponent', () => {
  let component: AssignmentAgreementSettleItemComponent;
  let fixture: ComponentFixture<AssignmentAgreementSettleItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AssignmentAgreementSettleItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentAgreementSettleItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
