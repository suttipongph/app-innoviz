import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { AssignmentAgreementSettleItemView } from 'shared/models/viewModel';

const firstGroup = [
  'REF_TYPE',
  'REF_ID',
  'REF_ASSIGNMENT_AGREEMENT_SETTLE_GUID',
  'ASSIGNMENT_AGREEMENT_TABLE_GUID',
  'SETTLED_DATE',
  'SETTLED_AMOUNT',
  'BUYER_AGREEMENT_TABLE_GUID',
  'DOCUMENT_REASON_GUID',
  'REF_GUID',
  'ASSIGNMENT_AGREEMENT_SETTLE_GUID'
];

export class AssignmentAgreementSettleItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: AssignmentAgreementSettleItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: AssignmentAgreementSettleItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<AssignmentAgreementSettleItemView> {
    let model = new AssignmentAgreementSettleItemView();
    model.assignmentAgreementSettleGUID = EmptyGuid;
    return of(model);
  }
  getdisabledFnUpdateAssignmentAgreementsettle(refAssignmentAgreementTableGUID: string): boolean {
    if (refAssignmentAgreementTableGUID) {
      return true;
    } else {
      return false;
    }
  }
}
