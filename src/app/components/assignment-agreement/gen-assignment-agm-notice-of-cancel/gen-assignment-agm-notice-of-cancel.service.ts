import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AssignmentAgreementTableItemView, GenAssignmentAgmNoticeOfCancelView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class GenAssignmentAgmNoticeOfCancelService {
  serviceKey = 'genAssignmentAgmNoticeOfCancelGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getGenerateNoticeOfCancellationValidation(id: string): Observable<AssignmentAgreementTableItemView> {
    const url = `${this.servicePath}/GetGenerateNoticeOfCancellationValidation/id=${id}`;
    return this.dataGateway.get(url);
  }

  generateNoticeOfCancellation(vmModel: GenAssignmentAgmNoticeOfCancelView): Observable<GenAssignmentAgmNoticeOfCancelView> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vmModel);
  }
  isManualNumberSeqByAssignmentAgreement(): Observable<boolean> {
    const url = `${this.servicePath}/IsManualNumberSeqByAssignmentAgreement`;
    return this.dataGateway.get(url);
  }
}
