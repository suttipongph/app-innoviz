import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenAssignmentAgmNoticeOfCancelComponent } from './gen-assignment-agm-notice-of-cancel.component';

describe('GenAssignmentAgmNoticeOfCancelViewComponent', () => {
  let component: GenAssignmentAgmNoticeOfCancelComponent;
  let fixture: ComponentFixture<GenAssignmentAgmNoticeOfCancelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GenAssignmentAgmNoticeOfCancelComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenAssignmentAgmNoticeOfCancelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
