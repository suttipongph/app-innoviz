import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { RefType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { AssignmentAgreementTableItemView, DocumentReasonItemView, GenAssignmentAgmNoticeOfCancelView } from 'shared/models/viewModel';
import { GenAssignmentAgmNoticeOfCancelService } from '../gen-assignment-agm-notice-of-cancel.service';
import { GenAssignmentAgmNoticeOfCancelCustomComponent } from './gen-assignment-agm-notice-of-cancel-custom.component';
@Component({
  selector: 'gen-assignment-agm-notice-of-cancel',
  templateUrl: './gen-assignment-agm-notice-of-cancel.component.html',
  styleUrls: ['./gen-assignment-agm-notice-of-cancel.component.scss']
})
export class GenAssignmentAgmNoticeOfCancelComponent extends BaseItemComponent<GenAssignmentAgmNoticeOfCancelView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  agreementDocTypeOptions: SelectItems[] = [];
  documentReasonOptions: SelectItems[] = [];
  assignmentMethodOptions: SelectItems[] = [];
  parentModel: AssignmentAgreementTableItemView;
  customerTable_Values: string;
  buyerTable_Values: string;

  constructor(
    private service: GenAssignmentAgmNoticeOfCancelService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: GenAssignmentAgmNoticeOfCancelCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.parentModel = passingObj['assignmentAgreementItemView'];
    this.customerTable_Values = this.parentModel.customerTable_Values;
    this.buyerTable_Values = this.parentModel.buyerTable_Values;
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialCreatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getAgreementDocTypeEnumDropDown(this.agreementDocTypeOptions);
  }
  getById(): Observable<GenAssignmentAgmNoticeOfCancelView> {
    return this.service.generateNoticeOfCancellation(this.model);
  }
  getInitialData(): Observable<GenAssignmentAgmNoticeOfCancelView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {}
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(
      this.baseDropdown.getDocumentReasonDropDown(RefType.AssignmentAgreement.toString()),
      this.baseDropdown.getAssignmentMethodDropDown()
    ).subscribe(
      ([documentReason, assignmentMethod]) => {
        this.documentReasonOptions = documentReason;
        this.assignmentMethodOptions = assignmentMethod;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing().subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {}
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.custom.getModelParam(this.model, this.parentModel).subscribe((model) => {
          this.onSubmit(true, model);
        });
      }
    });
  }
  onSubmit(isColsing: boolean, model: GenAssignmentAgmNoticeOfCancelView): void {
    super.onExecuteFunction(this.service.generateNoticeOfCancellation(model));
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onDocumentReasonChange(id: string): void {
    if (!isNullOrUndefined(id)) {
      this.custom.getRemakReason(id, this.documentReasonOptions).subscribe((res) => {
        this.model.reasonRemark = (res.rowData as DocumentReasonItemView).description;
      });
    }
  }
}
