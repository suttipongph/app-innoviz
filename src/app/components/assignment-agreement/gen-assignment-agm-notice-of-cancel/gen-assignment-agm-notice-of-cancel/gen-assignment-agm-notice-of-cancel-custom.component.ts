import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { AssignmentAgreementTableItemView, GenAssignmentAgmNoticeOfCancelView } from 'shared/models/viewModel';
import { GenAssignmentAgmNoticeOfCancelService } from '../gen-assignment-agm-notice-of-cancel.service';

const firstGroup = [
  'INTERNAL_ASSIGNMENT_AGREEMENT_ID',
  'ASSIGNMENT_AGREEMENT_ID',
  'ASSIGNMENT_AGREEMENT_DESCRIPTION',
  'ASSIGNMENT_METHOD_GUID',
  'AGREEMENT_DATE',
  'AGREEMENT_DOC_TYPE',
  'ASSIGNMENT_AGREEMENT_AMOUNT',
  'CUSTOMER_ID',
  'CUSTOMER_NAME',
  'BUYER_TABLE_ID',
  'BUYER_NAME'
];

export class GenAssignmentAgmNoticeOfCancelCustomComponent {
  baseService: BaseServiceModel<GenAssignmentAgmNoticeOfCancelService>;
  isManual = true;
  getFieldAccessing(): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return this.baseService.service.isManualNumberSeqByAssignmentAgreement().pipe(
      map((result) => {
        this.isManual = result;
        if (!result) {
          fieldAccessing.push({ filedIds: ['NOTICE_OF_CANCELLATION_INTERNAL_ASSIGNMENT_AGREEMENT_ID'], readonly: true });
          return fieldAccessing;
        }
      })
    );
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<GenAssignmentAgmNoticeOfCancelView> {
    let model = new GenAssignmentAgmNoticeOfCancelView();
    // model.genAssignmentAgmNoticeOfCancelGUID = EmptyGuid;
    return of(model);
  }
  getModelParam(model: GenAssignmentAgmNoticeOfCancelView, parent: AssignmentAgreementTableItemView): Observable<GenAssignmentAgmNoticeOfCancelView> {
    model.assignmentAgreementTableItemView = parent;
    return of(model);
  }
  getRemakReason(id: string, options: SelectItems[]): Observable<SelectItems> {
    const item = options.find((f) => f.value === id);
    return of(item);
  }
}
