import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GenAssignmentAgmNoticeOfCancelService } from './gen-assignment-agm-notice-of-cancel.service';
import { GenAssignmentAgmNoticeOfCancelComponent } from './gen-assignment-agm-notice-of-cancel/gen-assignment-agm-notice-of-cancel.component';
import { GenAssignmentAgmNoticeOfCancelCustomComponent } from './gen-assignment-agm-notice-of-cancel/gen-assignment-agm-notice-of-cancel-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [GenAssignmentAgmNoticeOfCancelComponent],
  providers: [GenAssignmentAgmNoticeOfCancelService, GenAssignmentAgmNoticeOfCancelCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [GenAssignmentAgmNoticeOfCancelComponent]
})
export class GenAssignmentAgmNoticeOfCancelComponentModule {}
