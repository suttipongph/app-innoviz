import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateAssignmentAgreementSettleService } from './update-assignment-agreement-settle.service';
import { UpdateAssignmentAgreementSettleComponent } from './update-assignment-agreement-settle/update-assignment-agreement-settle.component';
import { UpdateAssignmentAgreementSettleCustomComponent } from './update-assignment-agreement-settle/update-assignment-agreement-settle-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [UpdateAssignmentAgreementSettleComponent],
  providers: [UpdateAssignmentAgreementSettleService, UpdateAssignmentAgreementSettleCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [UpdateAssignmentAgreementSettleComponent]
})
export class UpdateAssignmentAgreementSettleComponentModule {}
