import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AssignmentAgreementSettleItemView, UpdateAssignmentAgreementSettleView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class UpdateAssignmentAgreementSettleService {
  serviceKey = 'updateAssignmentAgreementSettleGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getUpdateAssignmentAgreementSettleToList(search: SearchParameter): Observable<SearchResult<UpdateAssignmentAgreementSettleView>> {
    const url = `${this.servicePath}/GetUpdateAssignmentAgreementSettleList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteUpdateAssignmentAgreementSettle(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteUpdateAssignmentAgreementSettle`;
    return this.dataGateway.delete(url, row);
  }
  getUpdateAssignmentAgreementSettleById(id: string): Observable<AssignmentAgreementSettleItemView> {
    const url = `${this.servicePath}/GetUpdateAssignmentAgreementSettleById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createUpdateAssignmentAgreementSettle(vmModel: UpdateAssignmentAgreementSettleView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateUpdateAssignmentAgreementSettle`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateUpdateAssignmentAgreementSettle(vmModel: UpdateAssignmentAgreementSettleView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateUpdateAssignmentAgreementSettle`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
