import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, RefType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { AssignmentAgreementSettleItemView, UpdateAssignmentAgreementSettleView } from 'shared/models/viewModel';
import { UpdateAssignmentAgreementSettleService } from '../update-assignment-agreement-settle.service';
import { UpdateAssignmentAgreementSettleCustomComponent } from './update-assignment-agreement-settle-custom.component';
@Component({
  selector: 'update-assignment-agreement-settle',
  templateUrl: './update-assignment-agreement-settle.component.html',
  styleUrls: ['./update-assignment-agreement-settle.component.scss']
})
export class UpdateAssignmentAgreementSettleComponent extends BaseItemComponent<UpdateAssignmentAgreementSettleView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  documentReasonOptions: SelectItems[] = [];

  constructor(
    private service: UpdateAssignmentAgreementSettleService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: UpdateAssignmentAgreementSettleCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<AssignmentAgreementSettleItemView> {
    return this.service.getUpdateAssignmentAgreementSettleById(this.id);
  }
  getInitialData(): Observable<UpdateAssignmentAgreementSettleView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: AssignmentAgreementSettleItemView): void {
    forkJoin(this.baseDropdown.getDocumentReasonDropDown(RefType.AssignmentAgreementSettle.toString())).subscribe(
      ([documentReason]) => {
        this.documentReasonOptions = documentReason;

        if (!isNullOrUndefined(model)) {
          this.model.internalAssignmentAgreementId = model.assignmentAgreementTable_Value;
          this.model.assignmentAgreementId = model.assignmentAgreementTable_AssignmentAgreementId;
          this.model.buyerAgreementId = model.buyerTable_Value;
          this.model.assignmentAgreementSettleGUID = this.id;
          this.model.settledAmount = model.settledAmount;
          this.model.settledDate = model.settledDate;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    super.onExecuteFunction(this.service.updateUpdateAssignmentAgreementSettle(this.model));
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onClose(): void {
    super.onBack();
  }
}
