import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { UpdateAssignmentAgreementSettleView } from 'shared/models/viewModel';

const firstGroup = ['INTERNAL_ASSIGNMENT_AGREEMENT_ID', 'ASSIGNMENT_AGREEMENT_ID', 'BUYER_AGREEMENT_ID', 'SETTLED_DATE', 'SETTLED_AMOUNT'];

export class UpdateAssignmentAgreementSettleCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: UpdateAssignmentAgreementSettleView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<UpdateAssignmentAgreementSettleView> {
    let model = new UpdateAssignmentAgreementSettleView();
    model.updateAssignmentAgreementSettleGUID = EmptyGuid;
    return of(model);
  }
}
