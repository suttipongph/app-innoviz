import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAssignmentAgreementSettleComponent } from './update-assignment-agreement-settle.component';

describe('UpdateAssignmentAgreementSettleViewComponent', () => {
  let component: UpdateAssignmentAgreementSettleComponent;
  let fixture: ComponentFixture<UpdateAssignmentAgreementSettleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UpdateAssignmentAgreementSettleComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAssignmentAgreementSettleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
