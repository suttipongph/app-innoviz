import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServiceFeeConditionTransService } from './service-fee-condition-trans.service';
import { ServiceFeeConditionTransListComponent } from './service-fee-condition-trans-list/service-fee-condition-trans-list.component';
import { ServiceFeeConditionTransItemComponent } from './service-fee-condition-trans-item/service-fee-condition-trans-item.component';
import { ServiceFeeConditionTransItemCustomComponent } from './service-fee-condition-trans-item/service-fee-condition-trans-item-custom.component';
import { ServiceFeeConditionTransListCustomComponent } from './service-fee-condition-trans-list/service-fee-condition-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ServiceFeeConditionTransListComponent, ServiceFeeConditionTransItemComponent],
  providers: [ServiceFeeConditionTransService, ServiceFeeConditionTransItemCustomComponent, ServiceFeeConditionTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ServiceFeeConditionTransListComponent, ServiceFeeConditionTransItemComponent]
})
export class ServiceFeeConditionTransComponentModule {}
