import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ServiceFeeConditionTransListComponent } from './service-fee-condition-trans-list.component';

describe('ServiceFeeConditionTransListViewComponent', () => {
  let component: ServiceFeeConditionTransListComponent;
  let fixture: ComponentFixture<ServiceFeeConditionTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ServiceFeeConditionTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceFeeConditionTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
