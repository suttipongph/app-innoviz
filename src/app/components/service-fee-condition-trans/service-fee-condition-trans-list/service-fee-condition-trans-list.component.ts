import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, ROUTE_FUNCTION_GEN, SortType } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { ServiceFeeConditionTransListView } from 'shared/models/viewModel';
import { ServiceFeeConditionTransService } from '../service-fee-condition-trans.service';
import { ServiceFeeConditionTransListCustomComponent } from './service-fee-condition-trans-list-custom.component';

@Component({
  selector: 'service-fee-condition-trans-list',
  templateUrl: './service-fee-condition-trans-list.component.html',
  styleUrls: ['./service-fee-condition-trans-list.component.scss']
})
export class ServiceFeeConditionTransListComponent extends BaseListComponent<ServiceFeeConditionTransListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  invoiceRevenueTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  serviceFeeCategoryOptions: SelectItems[] = [];
  defualtBitOption: SelectItems[] = [];

  constructor(
    public custom: ServiceFeeConditionTransListCustomComponent,
    public service: ServiceFeeConditionTransService,
    private currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {
    let passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.parentId = this.custom.setParentKey(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.setAccessModeByParentStatus(this.isWorkFlowMode()).subscribe((result) => {
      this.setFunctionOptions();
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.baseDropdown.getServiceFeeCategoryEnumDropDown(this.serviceFeeCategoryOptions);
    this.baseDropdown.getDefaultBitDropDown(this.defualtBitOption);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getInvoiceRevenueTypeDropDown(this.invoiceRevenueTypeOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'serviceFeeConditionTransGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.ORDERING',
        textKey: 'ordering',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.SERVICE_FEE_TYPE_ID',
        textKey: 'invoiceRevenueType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.invoiceRevenueTypeOptions,
        sortingKey: 'invoiceRevenueType_revenueTypeId',
        searchingKey: 'invoiceRevenueTypeGUID'
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.SERVICE_FEE_CATEGORY',
        textKey: 'invoiceRevenueType_ServiceFeeCategory',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.serviceFeeCategoryOptions
      },
      {
        label: 'LABEL.AMOUNT_BEFORE_TAX',
        textKey: 'amountBeforeTax',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: 'LABEL.INACTIVE',
        textKey: 'inactive',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defualtBitOption
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<ServiceFeeConditionTransListView>> {
    return this.service.getServiceFeeConditionTransToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteServiceFeeConditionTrans(row));
  }
  setFunctionOptions(): void {
    this.functionItems = [
      {
        label: 'LABEL.COPY_SERVICE_FEE_CONDITION_TEMPLATE',
        // command: () => this.setPreparingFunctionDataAndValidation(ROUTE_FUNCTION_GEN.COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA),
        visible: false
      }
    ];
    super.setFunctionOptionsMapping();
  }
}
