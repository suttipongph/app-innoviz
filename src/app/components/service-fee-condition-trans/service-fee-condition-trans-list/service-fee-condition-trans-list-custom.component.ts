import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { CreditAppRequestStatus, ProductType, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView, ServiceFeeConditionTransListView } from 'shared/models/viewModel';
import { ServiceFeeConditionTransService } from '../service-fee-condition-trans.service';

const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const CREDIT_APP_REQUEST_LINE = 'creditapprequestline-child';
const CREDIT_APP_TABLE = 'creditapptable';
const CREDIT_APP_LINE = 'creditappline-child';
const BUYER_MATCHING = 'buyermatching';
const LOAN_REQUEST = 'loanrequest';
const AMEND_CA = 'amendca';
const AMEND_CA_LINE = 'amendcaline';
export class ServiceFeeConditionTransListCustomComponent {
  baseService: BaseServiceModel<any>;
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  service: ServiceFeeConditionTransService;
  originId = null;
  isBuyerMatching: boolean = false;
  isLoanRequest: boolean = false;
  parentId = null;

  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setAccessModeByParentStatus(isWorkFlowMode: boolean): Observable<AccessModeView> {
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        return this.getAccessModeByCreditAppRequestTable(this.originId, isWorkFlowMode);
      case CREDIT_APP_REQUEST_LINE:
        this.checkIsBuyerMacthingOrLoanRequest();
        return this.getAccessModeByCreditAppRequestTable(this.originId, isWorkFlowMode);
      case CREDIT_APP_TABLE:
        this.accessModeView.canCreate = false;
        this.accessModeView.canDelete = false;
        this.accessModeView.canView = true;
        return of(this.accessModeView);
      case CREDIT_APP_LINE:
        this.accessModeView.canCreate = false;
        this.accessModeView.canDelete = false;
        this.accessModeView.canView = true;
        return of(this.accessModeView);
      case AMEND_CA:
        return this.getAccessModeByCreditAppRequestTable(this.parentId, isWorkFlowMode);
      case AMEND_CA_LINE:
        return this.getAccessModeByCreditAppRequestTable(this.parentId, isWorkFlowMode);
      case BUYER_MATCHING:
        return this.getAccessModeByCreditAppRequestTable(this.parentId, isWorkFlowMode);
      case LOAN_REQUEST:
        return this.getAccessModeByCreditAppRequestTable(this.parentId, isWorkFlowMode);
      default:
        return of(this.accessModeView);
    }
  }
  setParentKey(model: ServiceFeeConditionTransListView) {
    this.originId = this.baseService.uiService.getRelatedInfoOriginTableKey();
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    this.parentId = this.baseService.uiService.getRelatedInfoParentTableKey();

    switch (this.parentName) {
      case AMEND_CA:
        return model.refGUID;
      case AMEND_CA_LINE:
        this.parentId = model.creditAppRequestTable_CreditAppRequestTableGUID;
        return model.refGUID;
      default:
        return this.parentId;
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
  getAccessModeByCreditAppRequestTable(parentId: string, isWorkFlowMode: boolean): Observable<AccessModeView> {
    return this.baseService.service.getAccessModeByCreditAppRequestTable(parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
        const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
        this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
        this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
        this.accessModeView.canView = true;
      })
    );
  }
  checkIsBuyerMacthingOrLoanRequest(): void {
    this.isBuyerMatching = window.location.href.match(BUYER_MATCHING) ? true : false;
    this.isLoanRequest = window.location.href.match(LOAN_REQUEST) ? true : false;
    if (this.isBuyerMatching) {
      this.originId = this.baseService.uiService.getKey(BUYER_MATCHING);
    } else if (this.isLoanRequest) {
      this.originId = this.baseService.uiService.getKey(LOAN_REQUEST);
    }
  }
}
