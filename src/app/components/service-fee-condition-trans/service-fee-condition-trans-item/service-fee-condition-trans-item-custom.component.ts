import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { CreditAppRequestStatus, EmptyGuid, ProductType } from 'shared/constants';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, InvoiceRevenueTypeItemView, ServiceFeeConditionTransItemView } from 'shared/models/viewModel';

const firstGroup = ['SERVICE_FEE_CATEGORY', 'INACTIVE', 'REF_TYPE', 'REF_ID', 'CREDIT_APP_REQUEST_TABLE_GUID'];
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const CREDIT_APP_REQUEST_LINE = 'creditapprequestline-child';
const CREDIT_APP_TABLE = 'creditapptable';
const CREDIT_APP_LINE = 'creditappline-child';
const BUYER_MATCHING = 'buyermatching';
const LOAN_REQUEST = 'loanrequest';
const AMEND_CA = 'amendca';
const AMEND_CA_LINE = 'amendcaline';
export class ServiceFeeConditionTransItemCustomComponent {
  baseService: BaseServiceModel<any>;
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  parentProductType: ProductType = null;
  originId = null;
  parentId = null;
  isBuyerMatching: boolean = false;
  isLoanRequest: boolean = false;
  getFieldAccessing(model: ServiceFeeConditionTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ServiceFeeConditionTransItemView, isWorkFlowMode: boolean): Observable<boolean> {
    return this.setAccessModeByParentStatus(model, isWorkFlowMode).pipe(
      map((result) => (isUpdateMode(model.refServiceFeeConditionTransGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  setAccessModeByParentStatus(model: ServiceFeeConditionTransItemView, isWorkFlowMode: boolean): Observable<AccessModeView> {
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        return this.getAccessModeByCreditAppRequestTable(this.originId, isWorkFlowMode);
      case CREDIT_APP_REQUEST_LINE:
        this.checkIsBuyerMacthingOrLoanRequest();
        return this.getAccessModeByCreditAppRequestTable(this.originId, isWorkFlowMode);
      case CREDIT_APP_TABLE:
        this.accessModeView.canCreate = false;
        this.accessModeView.canDelete = false;
        this.accessModeView.canView = false;
        return of(this.accessModeView);
      case CREDIT_APP_LINE:
        this.accessModeView.canCreate = false;
        this.accessModeView.canDelete = false;
        this.accessModeView.canView = false;
        return of(this.accessModeView);
      case AMEND_CA:
        return this.getAccessModeByCreditAppRequestTable(this.parentId, isWorkFlowMode);
      case AMEND_CA_LINE:
        return this.getAccessModeByCreditAppRequestTable(this.parentId, isWorkFlowMode);
      case BUYER_MATCHING:
        return this.getAccessModeByCreditAppRequestTable(this.parentId, isWorkFlowMode);
      case LOAN_REQUEST:
        return this.getAccessModeByCreditAppRequestTable(this.parentId, isWorkFlowMode);
      default:
        return of(this.accessModeView);
    }
  }
  setParentKey(model: ServiceFeeConditionTransItemView) {
    this.originId = this.baseService.uiService.getRelatedInfoOriginTableKey();
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    this.parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
    switch (this.parentName) {
      case AMEND_CA:
        return model.refGUID;
      case AMEND_CA_LINE:
        this.parentId = model.creditAppRequestTable_CreditAppRequestTableGUID;
        return model.refGUID;
      default:
        return this.parentId;
    }
  }
  getInitialData(): Observable<ServiceFeeConditionTransItemView> {
    let model = new ServiceFeeConditionTransItemView();
    model.serviceFeeConditionTransGUID = EmptyGuid;
    return of(model);
  }
  setModelByInvoiceRevenueType(model: ServiceFeeConditionTransItemView, option: SelectItems[]): void {
    if (!isNullOrUndefOrEmptyGUID(model.invoiceRevenueTypeGUID)) {
      let item: InvoiceRevenueTypeItemView = option.find(
        (t) => (t.rowData as InvoiceRevenueTypeItemView).invoiceRevenueTypeGUID == model.invoiceRevenueTypeGUID
      ).rowData as InvoiceRevenueTypeItemView;
      model.invoiceRevenueType_ServiceFeeCategory = item.serviceFeeCategory;
      model.description = item.description;
      model.amountBeforeTax = item.feeAmount;
    } else {
      model.invoiceRevenueType_ServiceFeeCategory = null;
      model.description = '';
      model.amountBeforeTax = 0;
    }
  }
  getAccessModeByCreditAppRequestTable(parentId: string, isWorkFlowMode: boolean): Observable<AccessModeView> {
    return this.baseService.service.getAccessModeByCreditAppRequestTable(parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
        const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
        this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
        this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
        this.accessModeView.canView = isDraftStatus ? true : this.accessModeView.canView && isWorkFlowMode;
      })
    );
  }
  checkIsBuyerMacthingOrLoanRequest(): void {
    this.isBuyerMatching = window.location.href.match(BUYER_MATCHING) ? true : false;
    this.isLoanRequest = window.location.href.match(LOAN_REQUEST) ? true : false;
    if (this.isBuyerMatching) {
      this.originId = this.baseService.uiService.getKey(BUYER_MATCHING);
    } else if (this.isLoanRequest) {
      this.originId = this.baseService.uiService.getKey(LOAN_REQUEST);
    }
  }
}
