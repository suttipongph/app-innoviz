import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { ServiceFeeConditionTransItemView } from 'shared/models/viewModel';
import { ServiceFeeConditionTransService } from '../service-fee-condition-trans.service';
import { ServiceFeeConditionTransItemCustomComponent } from './service-fee-condition-trans-item-custom.component';
@Component({
  selector: 'service-fee-condition-trans-item',
  templateUrl: './service-fee-condition-trans-item.component.html',
  styleUrls: ['./service-fee-condition-trans-item.component.scss']
})
export class ServiceFeeConditionTransItemComponent extends BaseItemComponent<ServiceFeeConditionTransItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  invoiceRevenueTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  serviceFeeCategoryOptions: SelectItems[] = [];

  constructor(
    public service: ServiceFeeConditionTransService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: ServiceFeeConditionTransItemCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    let passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.parentId = this.custom.setParentKey(passingObj);
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.baseDropdown.getServiceFeeCategoryEnumDropDown(this.serviceFeeCategoryOptions);
  }
  getById(): Observable<ServiceFeeConditionTransItemView> {
    return this.service.getServiceFeeConditionTransById(this.id);
  }
  getInitialData(): Observable<ServiceFeeConditionTransItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.custom.parentProductType = result.invoiceRevenueType_ProductType;
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.custom.parentProductType = result.invoiceRevenueType_ProductType;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin([this.baseDropdown.getInvoiceRevenueTypeByProductTypeDropDown(this.custom.parentProductType.toString())]).subscribe(
      ([invoiceRevenueType]) => {
        this.invoiceRevenueTypeOptions = invoiceRevenueType;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model, this.isWorkFlowMode()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateServiceFeeConditionTrans(this.model), isColsing);
    } else {
      super.onCreate(this.service.createServiceFeeConditionTrans(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onServiceFeeTypeChange(): void {
    this.custom.setModelByInvoiceRevenueType(this.model, this.invoiceRevenueTypeOptions);
  }
}
