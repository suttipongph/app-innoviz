import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceFeeConditionTransItemComponent } from './service-fee-condition-trans-item.component';

describe('ServiceFeeConditionTransItemViewComponent', () => {
  let component: ServiceFeeConditionTransItemComponent;
  let fixture: ComponentFixture<ServiceFeeConditionTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ServiceFeeConditionTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceFeeConditionTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
