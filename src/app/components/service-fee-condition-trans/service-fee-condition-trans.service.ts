import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, ServiceFeeConditionTransItemView, ServiceFeeConditionTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { ProductType } from 'shared/constants';
@Injectable()
export class ServiceFeeConditionTransService {
  serviceKey = 'serviceFeeConditionTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getServiceFeeConditionTransToList(search: SearchParameter): Observable<SearchResult<ServiceFeeConditionTransListView>> {
    const url = `${this.servicePath}/GetServiceFeeConditionTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteServiceFeeConditionTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteServiceFeeConditionTrans`;
    return this.dataGateway.delete(url, row);
  }
  getServiceFeeConditionTransById(id: string): Observable<ServiceFeeConditionTransItemView> {
    const url = `${this.servicePath}/GetServiceFeeConditionTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createServiceFeeConditionTrans(vmModel: ServiceFeeConditionTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateServiceFeeConditionTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateServiceFeeConditionTrans(vmModel: ServiceFeeConditionTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateServiceFeeConditionTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getAccessModeByCreditAppRequestTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppRequestTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getInitialData(id: string): Observable<ServiceFeeConditionTransItemView> {
    const url = `${this.servicePath}/GetServiceFeeConditionTransInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getCreditAppRequestTableProductTypeById(id: string): Observable<ProductType> {
    const url = `${this.servicePath}/GetCreditAppRequestTableProductTypeById/id=${id}`;
    return this.dataGateway.get(url);
  }
}
