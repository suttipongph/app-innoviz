import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ContactPersonTransItemView, ContactPersonTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ContactPersonTransService {
  serviceKey = 'contactPersonTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getContactPersonTransToList(search: SearchParameter): Observable<SearchResult<ContactPersonTransListView>> {
    const url = `${this.servicePath}/GetContactPersonTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteContactPersonTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteContactPersonTrans`;
    return this.dataGateway.delete(url, row);
  }
  getContactPersonTransById(id: string): Observable<ContactPersonTransItemView> {
    const url = `${this.servicePath}/GetContactPersonTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createContactPersonTrans(vmModel: ContactPersonTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateContactPersonTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateContactPersonTrans(vmModel: ContactPersonTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateContactPersonTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<ContactPersonTransItemView> {
    const url = `${this.servicePath}/GetContactPersonTransInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
}
