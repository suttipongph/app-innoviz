import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContactPersonTransService } from './contact-person-trans.service';
import { ContactPersonTransListComponent } from './contact-person-trans-list/contact-person-trans-list.component';
import { ContactPersonTransItemComponent } from './contact-person-trans-item/contact-person-trans-item.component';
import { ContactPersonTransItemCustomComponent } from './contact-person-trans-item/contact-person-trans-item-custom.component';
import { ContactPersonTransListCustomComponent } from './contact-person-trans-list/contact-person-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ContactPersonTransListComponent, ContactPersonTransItemComponent],
  providers: [ContactPersonTransService, ContactPersonTransItemCustomComponent, ContactPersonTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ContactPersonTransListComponent, ContactPersonTransItemComponent]
})
export class ContactPersonTransComponentModule {}
