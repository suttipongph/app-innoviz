import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactPersonTransItemComponent } from './contact-person-trans-item.component';

describe('ContactPersonTransItemViewComponent', () => {
  let component: ContactPersonTransItemComponent;
  let fixture: ComponentFixture<ContactPersonTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ContactPersonTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactPersonTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
