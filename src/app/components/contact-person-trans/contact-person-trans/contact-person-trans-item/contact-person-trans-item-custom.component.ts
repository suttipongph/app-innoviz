import { Observable, of } from 'rxjs';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { ContactPersonTransItemView, RelatedPersonTableItemView } from 'shared/models/viewModel';

const firstGroup = [
  'NAME',
  'IDENTIFICATION_TYPE',
  'TAX_ID',
  'PASSPORT_ID',
  'WORK_PERMIT_ID',
  'POSITION',
  'PHONE',
  'EXTENSION',
  'MOBILE',
  'FAX',
  'EMAIL',
  'LINE_ID',
  'REF_TYPE',
  'REF_ID'
];

const secondGroup = ['RELATED_PERSON_TABLE_GUID'];

export class ContactPersonTransItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: ContactPersonTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    if (isUpdateMode(model.contactPersonTransGUID)) {
      fieldAccessing.push({ filedIds: secondGroup, readonly: true });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ContactPersonTransItemView): Observable<boolean> {
    return of(!canCreate);
  }
  onRelatedPersonChange(model: ContactPersonTransItemView, option: SelectItems[]): void {
    this.setValueByRelatedPerson(model, option);
  }
  setValueByRelatedPerson(model: ContactPersonTransItemView, option: SelectItems[]): void {
    const row = option.find((o) => o.value === model.relatedPersonTableGUID).rowData as RelatedPersonTableItemView;
    model.relatedPersonTable_Name = row.name;
    model.relatedPersonTable_IdentificationType = row.identificationType;
    model.relatedPersonTable_TaxId = row.taxId;
    model.relatedPersonTable_PassportId = row.passportId;
    model.relatedPersonTable_WorkPermitId = row.workPermitId;
    model.relatedPersonTable_Position = row.position;
    model.relatedPersonTable_Phone = row.phone;
    model.relatedPersonTable_Extension = row.extension;
    model.relatedPersonTable_Fax = row.fax;
    model.relatedPersonTable_Mobile = row.mobile;
    model.relatedPersonTable_LineId = row.lineId;
    model.relatedPersonTable_Email = row.email;
  }
}
