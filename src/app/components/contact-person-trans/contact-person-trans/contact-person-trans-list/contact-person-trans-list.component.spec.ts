import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ContactPersonTransListComponent } from './contact-person-trans-list.component';

describe('ContactPersonTransListViewComponent', () => {
  let component: ContactPersonTransListComponent;
  let fixture: ComponentFixture<ContactPersonTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ContactPersonTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactPersonTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
