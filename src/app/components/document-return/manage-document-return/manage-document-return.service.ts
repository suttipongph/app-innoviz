import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { DocumentReturnTableItemView, ManageDocumentReturnView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ManageDocumentReturnService {
  serviceKey = 'manageDocumentReturnGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getManageDocumentReturnById(id: string): Observable<DocumentReturnTableItemView> {
    const url = `${this.servicePath}/GetManageDocumentReturnById/id=${id}`;
    return this.dataGateway.get(url);
  }
  getManageDocumentReturnInitialData(id: string, statusId: string): Observable<DocumentReturnTableItemView> {
    const url = `${this.servicePath}/GetManageDocumentReturnInitialData/id=${id}/statusId=${statusId}`;
    return this.dataGateway.get(url);
  }
  createManageDocumentReturn(vmModel: DocumentReturnTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateManageDocumentReturn`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateManageDocumentReturn(vmModel: DocumentReturnTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateManageDocumentReturnStatus`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getDocumentStatusPostValidation(vmModel: DocumentReturnTableItemView): Observable<boolean> {
    const url = `${this.servicePath}/getDocumentStatusPostValidation`;
    return this.dataGateway.post(url, vmModel);
  }
  getDocumentStatusCloseValidation(vmModel: DocumentReturnTableItemView): Observable<boolean> {
    const url = `${this.servicePath}/getDocumentStatusCloseValidation`;
    return this.dataGateway.post(url, vmModel);
  }
}
