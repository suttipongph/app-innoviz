import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';

import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { Confirmation } from 'shared/models/primeModel';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { DocumentReturnTableItemView } from 'shared/models/viewModel';
import { ManageDocumentReturnService } from '../manage-document-return.service';
import { ManageDocumentReturnCustomComponent } from './manage-document-return-custom.component';
@Component({
  selector: 'manage-document-return',
  templateUrl: './manage-document-return.component.html',
  styleUrls: ['./manage-document-return.component.scss']
})
export class ManageDocumentReturnComponent extends BaseItemComponent<DocumentReturnTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  contactToOptions: SelectItems[] = [];

  constructor(
    private service: ManageDocumentReturnService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: ManageDocumentReturnCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.custom.setStatusId(this.getPassingObject(this.currentActivatedRoute));
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.isUpdateMode = true;
    this.setInitialCreatingData();

    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getContactToEnumDropDown(this.contactToOptions);
  }
  getById(): Observable<DocumentReturnTableItemView> {
    return this.service.getManageDocumentReturnById(this.id);
  }
  getInitialData(): Observable<DocumentReturnTableItemView> {
    return this.custom.getInitialData(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    const confirmation: Confirmation = {
      header: this.baseService.translate.instant('CONFIRM.CONFIRM'),
      message: this.baseService.translate.instant('CONFIRM.90011', [this.model.originalDocumentStatus, this.model.documentStatus_Values])
    };
    this.baseService.notificationService.showConfirmDialog(confirmation);
    this.baseService.notificationService.isAccept.subscribe((isConfirm) => {
      if (isConfirm) {
        super.onExecuteFunction(this.service.updateManageDocumentReturn(this.model));
      }
    });
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation(this.model);
  }
  onClose(): void {
    super.onBack();
  }
}
