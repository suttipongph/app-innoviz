import { Observable, of } from 'rxjs';
import { DocumentReturnStatus, EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { DocumentReturnTableItemView, ManageDocumentReturnView } from 'shared/models/viewModel';

const firstGroup = ['DOCUMENT_RETURN_ID', 'CONTACT_TO', 'CONTACT_PERSON_NAME', 'ORIGINAL_DOCUMENT_STATUS', 'DOCUMENT_STATUS_GUID'];

export class ManageDocumentReturnCustomComponent {
  statusId: string;
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: DocumentReturnTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(model: DocumentReturnTableItemView): Observable<boolean> {
    switch (model.documentStatus_StatusId) {
      case DocumentReturnStatus.Draft:
        return this.baseService.service.getDocumentStatusPostValidation(model);
        break;
      case DocumentReturnStatus.Posted:
        return this.baseService.service.getDocumentStatusCloseValidation(model);
      default:
        return of(true);
    }
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(id): Observable<DocumentReturnTableItemView> {
    return this.baseService.service.getManageDocumentReturnInitialData(id, this.statusId);
  }
  setStatusId(passObj: any): any {
    this.statusId = passObj.statusId;
  }
}
