import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageDocumentReturnComponent } from './manage-document-return.component';

describe('ManageDocumentReturnViewComponent', () => {
  let component: ManageDocumentReturnComponent;
  let fixture: ComponentFixture<ManageDocumentReturnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManageDocumentReturnComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageDocumentReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
