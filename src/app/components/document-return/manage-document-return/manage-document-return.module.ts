import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ManageDocumentReturnService } from './manage-document-return.service';
import { ManageDocumentReturnCustomComponent } from './manage-document-return/manage-document-return-custom.component';
import { ManageDocumentReturnComponent } from './manage-document-return/manage-document-return.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ManageDocumentReturnComponent],
  providers: [ManageDocumentReturnService, ManageDocumentReturnCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ManageDocumentReturnComponent]
})
export class ManageDocumentReturnComponentModule {}
