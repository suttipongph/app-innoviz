import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, DocumentReturnTableItemView, DocumentReturnTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class DocumentReturnTableService {
  serviceKey = 'documentReturnTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getDocumentReturnTableToList(search: SearchParameter): Observable<SearchResult<DocumentReturnTableListView>> {
    const url = `${this.servicePath}/GetDocumentReturnTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteDocumentReturnTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteDocumentReturnTable`;
    return this.dataGateway.delete(url, row);
  }
  getDocumentReturnTableById(id: string): Observable<DocumentReturnTableItemView> {
    const url = `${this.servicePath}/GetDocumentReturnTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createDocumentReturnTable(vmModel: DocumentReturnTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateDocumentReturnTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateDocumentReturnTable(vmModel: DocumentReturnTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateDocumentReturnTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  validateIsManualNumberSeq(companyId: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeq/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
  initialDataDocumentReturnTable(companyId: string): Observable<DocumentReturnTableItemView> {
    const url = `${this.servicePath}/GetInitialDataDocumentReturnTable/id=${companyId}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByParent(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByDocumentReturnTable/id=${id}`;
    return this.dataGateway.get(url);
  }
}
