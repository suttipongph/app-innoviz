import { DocumentReturnLineComponentModule } from 'components/document-return/document-return-line/document-return-line.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DocumentReturnTableService } from './document-return-table.service';
import { DocumentReturnTableListComponent } from './document-return-table-list/document-return-table-list.component';
import { DocumentReturnTableItemComponent } from './document-return-table-item/document-return-table-item.component';
import { DocumentReturnTableItemCustomComponent } from './document-return-table-item/document-return-table-item-custom.component';
import { DocumentReturnTableListCustomComponent } from './document-return-table-list/document-return-table-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, DocumentReturnLineComponentModule],
  declarations: [DocumentReturnTableListComponent, DocumentReturnTableItemComponent],
  providers: [DocumentReturnTableService, DocumentReturnTableItemCustomComponent, DocumentReturnTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [DocumentReturnTableListComponent, DocumentReturnTableItemComponent]
})
export class DocumentReturnTableComponentModule {}
