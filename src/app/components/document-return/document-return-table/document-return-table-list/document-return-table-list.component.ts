import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { AuthorizedPersonTransListView, DocumentReturnTableListView } from 'shared/models/viewModel';
import { DocumentReturnTableService } from '../document-return-table.service';
import { DocumentReturnTableListCustomComponent } from './document-return-table-list-custom.component';

@Component({
  selector: 'document-return-table-list',
  templateUrl: './document-return-table-list.component.html',
  styleUrls: ['./document-return-table-list.component.scss']
})
export class DocumentReturnTableListComponent extends BaseListComponent<DocumentReturnTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  addressTransOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  contactToOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  documentReturnMethodOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];

  employeeTableOptions: SelectItems[] = [];

  constructor(public custom: DocumentReturnTableListCustomComponent, private service: DocumentReturnTableService) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.parentName = this.uiService.getMasterRoute(1);
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getContactToEnumDropDown(this.contactToOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getDocumentReturnMethodDropDown(this.documentReturnMethodOptions);
    this.baseDropdown.getEmployeeTableDropDown(this.employeeTableOptions);
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'documentReturnTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.DOCUMENT_RETURN_ID',
        textKey: 'documentReturnId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.REQUESTOR_ID',
        textKey: 'employeeTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.employeeTableOptions,
        sortingKey: 'employeeTable_EmployeeTableId',
        searchingKey: 'requestorGUID'
      },
      {
        label: 'LABEL.CONTACT_TO',
        textKey: 'contactTo',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.contactToOptions
      },
      {
        label: 'LABEL.CONTACT_PERSON_NAME',
        textKey: 'contactPersonName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentStatusOptions,
        sortingKey: 'documentStatusGUID',
        searchingKey: 'documentStatusGUID'
      },
      {
        label: 'LABEL.RETURN_METHOD_ID',
        textKey: 'documentReturnMethod_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentReturnMethodOptions,
        sortingKey: 'documentReturnMethod_DocumentReturnMethodId',
        searchingKey: 'documentReturnMethodGUID'
      },
      {
        label: 'LABEL.EXPECTED_RETURN_DATE',
        textKey: 'expectedReturnDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<DocumentReturnTableListView>> {
    return this.service.getDocumentReturnTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteDocumentReturnTable(row));
  }
  getRowAuthorize(row: DocumentReturnTableListView[]): void {
    if (this.custom.getRowAuthorize(row)) {
      row.forEach((item) => {
        item.rowAuthorize = this.getSingleRowAuthorize(item.rowAuthorize, item);
      });
    } else {
      super.getRowAuthorize(row);
    }
  }
}
