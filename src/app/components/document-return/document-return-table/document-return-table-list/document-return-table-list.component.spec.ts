import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DocumentReturnTableListComponent } from './document-return-table-list.component';

describe('DocumentReturnTableListViewComponent', () => {
  let component: DocumentReturnTableListComponent;
  let fixture: ComponentFixture<DocumentReturnTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DocumentReturnTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentReturnTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
