import { DocumentReturnStatus } from './../../../../shared/constants/enumsStatus';
import { Observable } from 'rxjs';
import { AccessMode, ROUTE_MASTER_GEN } from 'shared/constants';
import { BaseServiceModel } from 'shared/models/systemModel';
import { DocumentReturnTableListView } from 'shared/models/viewModel';

export class DocumentReturnTableListCustomComponent {
  baseService: BaseServiceModel<any>;
  parentName = null;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  getRowAuthorize(row: DocumentReturnTableListView[]): boolean {
    let isSetRow: boolean = true;
    if (this.parentName === 'documentreturntable') {
      row.forEach((item) => {
        const accessMode: AccessMode = item.documentStatus_StatusId === DocumentReturnStatus.Draft ? AccessMode.full : AccessMode.viewer;
        item.rowAuthorize = accessMode;
      });
    } else {
      isSetRow = false;
    }
    return isSetRow;
  }
  
}
