import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, DocumentReturnStatus, RefType, ROUTE_REPORT_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { DocumentReturnTableItemView, RefIdParm } from 'shared/models/viewModel';
import { DocumentReturnTableService } from '../document-return-table.service';
import { DocumentReturnTableItemCustomComponent } from './document-return-table-item-custom.component';
@Component({
  selector: 'document-return-table-item',
  templateUrl: './document-return-table-item.component.html',
  styleUrls: ['./document-return-table-item.component.scss']
})
export class DocumentReturnTableItemComponent extends BaseItemComponent<DocumentReturnTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  addressTransOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  contactToOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  documentReturnMethodOptions: SelectItems[] = [];
  employeeTableOptions: SelectItems[] = [];

  constructor(
    private service: DocumentReturnTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: DocumentReturnTableItemCustomComponent
  ) {
    super();

    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.custom.baseDropdown = this.baseDropdown;
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getContactToEnumDropDown(this.contactToOptions);
  }
  getById(): Observable<DocumentReturnTableItemView> {
    return this.service.getDocumentReturnTableById(this.id);
  }
  getInitialData(): Observable<DocumentReturnTableItemView> {
    return this.custom.getInitialData(this.userDataService.getCurrentCompanyGUID());
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.model.contactTo = null;
      this.onAsyncRunner(result);
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(
      this.custom.getAddressTransDropdown(model),
      this.baseDropdown.getBuyerTableDropDown(),
      this.baseDropdown.getCustomerTableDropDown(),
      this.baseDropdown.getDocumentReturnMethodDropDown(),
      this.baseDropdown.getEmployeeTableDropDown()
    ).subscribe(
      ([addressTrans, buyerTable, customerTable, documentReturnMethod, employeeTable]) => {
        this.addressTransOptions = addressTrans;
        this.buyerTableOptions = buyerTable;
        this.customerTableOptions = customerTable;
        this.documentReturnMethodOptions = documentReturnMethod;
        this.employeeTableOptions = employeeTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model, this.service).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  setRelatedInfoOptions(): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        command: () => {
          const refIdParm: RefIdParm = { refType: RefType.DocumentReturn, refGUID: this.model.documentReturnTableGUID };
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: refIdParm
          });
        }
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }

  setFunctionOptions(model: DocumentReturnTableItemView): void {
    const canVisitPost = this.custom.getPostCanVisit(model);
    const canVisitClose = this.custom.getCloseCanVisit(model);

    const canCreate = this.custom.getCanCreate(model);
    const canUpdate = this.custom.getCanUpdate(model);
    const canDelete = this.custom.getCanDelete(model);
    const callerObject = this.model;

    this.functionItems = [
      {
        label: 'LABEL.PRINT',
        items: [
          {
            label: 'LABEL.LABEL',
            visible: true,
            disabled: false,
            command: () => this.toReport({ path: ROUTE_REPORT_GEN.PRINT_DOCUMENT_RETURE_LABER_LETTER })
          }
        ]
      },
      {
        label: 'LABEL.MANAGE',
        items: [
          {
            label: 'LABEL.POST',
            disabled: !canVisitPost,
            command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.POST_DOCUMENT_RETURN, parameters: { statusId: DocumentReturnStatus.Posted } })
          },
          {
            label: 'LABEL.CLOSE',
            disabled: !canVisitClose,
            command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.CLOSE_DOCUMENT_RETURN, parameters: { statusId: DocumentReturnStatus.Closed } })
          }
        ]
      }
    ];
    super.setFunctionOptionsMapping();
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateDocumentReturnTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createDocumentReturnTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  contactToChange(): any {
    this.custom.contactToChange(this.model).subscribe((result) => {
      super.setBaseFieldAccessing(result);
    });
  }
  customerChange(): any {
    this.custom.customerChange(this.model, this.customerTableOptions).subscribe((addressTrans) => {
      this.addressTransOptions = addressTrans;
    });
  }
  buyerChange(): any {
    this.custom.buyerChange(this.model, this.buyerTableOptions).subscribe((addressTrans) => {
      this.addressTransOptions = addressTrans;
    });
  }
  contactAddressChange(): any {
    this.custom.contactAddressChange(this.model, this.addressTransOptions);
  }
}
