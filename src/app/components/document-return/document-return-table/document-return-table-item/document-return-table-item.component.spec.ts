import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentReturnTableItemComponent } from './document-return-table-item.component';

describe('DocumentReturnTableItemViewComponent', () => {
  let component: DocumentReturnTableItemComponent;
  let fixture: ComponentFixture<DocumentReturnTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DocumentReturnTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentReturnTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
