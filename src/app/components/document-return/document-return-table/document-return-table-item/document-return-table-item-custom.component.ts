import { TranslateService } from '@ngx-translate/core';
import { AppInjector } from 'app-injector';
import { BaseDropdownComponent } from 'core/components/base/base.dropdown.component';
import { NotificationService } from 'core/services/notification.service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ContactTo, DocumentReturnStatus, EmptyGuid } from 'shared/constants';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { DocumentReturnTableItemView, CustomerTableItemView, BuyerTableItemView, AddressTransItemView } from 'shared/models/viewModel';
import { DocumentReturnTableService } from '../document-return-table.service';

const secondGroup = ['DOCUMENT_RETURN_ID'];
const firstGroup = ['DOCUMENT_STATUS_GUID'];
const customerGroup = ['CUSTOMER_TABLE_GUID'];
const buyerGroup = ['BUYER_TABLE_GUID'];
const specificGroup = ['ADDRESS_TRANS_GUID'];
const condition2 = ['REQUESTOR_GUID', 'CONTACT_TO'];
const condition3 = [
  'CONTACT_PERSON_NAME',
  'ADDRESS',
  'DOCUMENT_RETURN_METHOD_GUID',
  'EXPECTED_RETURN_DATE',
  'ACTUAL_RETURN_DATE',
  'POST_NUMBER',
  'POST_ACKNOWLEDGEMENT_NO',
  'RETURN_BY_GUID',
  'RECEIVED_BY'
];
export class DocumentReturnTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  isManunal: boolean = false;
  buyerMandatory = false;
  customerMandatory = false;

  notificationService = AppInjector.get(NotificationService);
  translateService = AppInjector.get(TranslateService);
  public baseDropdown: BaseDropdownComponent;
  getFieldAccessing(model: DocumentReturnTableItemView, service: DocumentReturnTableService): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return new Observable((observer) => {
      if (isNullOrUndefOrEmptyGUID(model.documentReturnTableGUID)) {
        this.customerMandatory = false;
        this.buyerMandatory = false;
        fieldAccessing.push({ filedIds: customerGroup, readonly: true });
        fieldAccessing.push({ filedIds: buyerGroup, readonly: true });
        this.validateIsManualNumberSeq(service, model.companyGUID).subscribe(
          (result) => {
            this.isManunal = result;
            fieldAccessing.push({ filedIds: secondGroup, readonly: !this.isManunal });
            observer.next(fieldAccessing);
          },
          (error) => {
            this.notificationService.showErrorMessageFromResponse(error);
          }
        );
      } else {
        fieldAccessing.push({ filedIds: secondGroup, readonly: true });
        if (model.documentStatus_StatusId === DocumentReturnStatus.Draft) {
          if (model.contactTo === ContactTo.Buyer) {
            this.buyerMandatory = true;
            fieldAccessing.push({ filedIds: buyerGroup, readonly: false });
          } else {
            this.buyerMandatory = false;
            fieldAccessing.push({ filedIds: buyerGroup, readonly: true });
          }
          if (model.contactTo === ContactTo.Customer) {
            this.customerMandatory = true;
            fieldAccessing.push({ filedIds: customerGroup, readonly: false });
          }
          if (model.contactTo === ContactTo.Specific) {
            fieldAccessing.push({ filedIds: specificGroup, readonly: true });
          }
        } else if (model.documentStatus_StatusId === DocumentReturnStatus.Posted) {
          fieldAccessing.push({ filedIds: condition2, readonly: true });
          fieldAccessing.push({ filedIds: buyerGroup, readonly: true });
          fieldAccessing.push({ filedIds: secondGroup, readonly: true });
          fieldAccessing.push({ filedIds: customerGroup, readonly: true });
        } else {
          fieldAccessing.push({ filedIds: condition2, readonly: true });
          fieldAccessing.push({ filedIds: condition3, readonly: true });
          fieldAccessing.push({ filedIds: specificGroup, readonly: true });
          fieldAccessing.push({ filedIds: buyerGroup, readonly: true });
          fieldAccessing.push({ filedIds: secondGroup, readonly: true });
          fieldAccessing.push({ filedIds: customerGroup, readonly: true });
        }
        observer.next(fieldAccessing);
      }
    });
  }
  validateIsManualNumberSeq(service: DocumentReturnTableService, companyId: string): Observable<boolean> {
    return service.validateIsManualNumberSeq(companyId);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: DocumentReturnTableItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(companyId: string): Observable<DocumentReturnTableItemView> {
    return this.baseService.service.initialDataDocumentReturnTable(companyId);
  }
  getAddressTransDropdown(model: DocumentReturnTableItemView): Observable<SelectItems[]> {
    if (model.contactTo === ContactTo.Buyer) {
      return this.baseDropdown.getAddressTransByBuyerDropDown(model.buyerTableGUID);
    } else if (model.contactTo === ContactTo.Customer) {
      return this.baseDropdown.getAddressTransByCustomerDropDown(model.customerTableGUID);
    } else {
      return this.baseDropdown.getAddressTransDropDown();
    }
  }
  contactToChange(model: DocumentReturnTableItemView): any {
    const fieldAccessing: FieldAccessing[] = [];

    if (model.contactTo === ContactTo.Customer) {
      model.customerTableGUID = null;
      model.buyerTableGUID = null;
      model.addressTransGUID = null;
      model.address = null;
      this.customerMandatory = true;
      this.buyerMandatory = false;
      fieldAccessing.push({ filedIds: customerGroup, readonly: false });
      fieldAccessing.push({ filedIds: buyerGroup, readonly: true });
      fieldAccessing.push({ filedIds: specificGroup, readonly: false });
      return new Observable((observer) => {
        observer.next(fieldAccessing);
      });
    } else if (model.contactTo === ContactTo.Buyer) {
      model.customerTableGUID = null;
      model.buyerTableGUID = null;
      model.addressTransGUID = null;
      model.address = null;
      this.customerMandatory = false;
      this.buyerMandatory = true;
      fieldAccessing.push({ filedIds: customerGroup, readonly: true });
      fieldAccessing.push({ filedIds: buyerGroup, readonly: false });
      fieldAccessing.push({ filedIds: specificGroup, readonly: false });
      return new Observable((observer) => {
        observer.next(fieldAccessing);
      });
    } else {
      model.customerTableGUID = null;
      model.buyerTableGUID = null;
      model.addressTransGUID = null;
      model.address = null;
      this.customerMandatory = false;
      this.buyerMandatory = false;
      fieldAccessing.push({ filedIds: specificGroup, readonly: true });
      fieldAccessing.push({ filedIds: customerGroup, readonly: false });
      fieldAccessing.push({ filedIds: buyerGroup, readonly: false });
      return new Observable((observer) => {
        observer.next(fieldAccessing);
      });
    }
  }
  customerChange(model: DocumentReturnTableItemView, customerTableOptions: SelectItems[]): Observable<SelectItems[]> {
    const rowData: CustomerTableItemView = isNullOrUndefOrEmptyGUID(model.customerTableGUID)
      ? new CustomerTableItemView()
      : (customerTableOptions.find((v) => v.value === model.customerTableGUID).rowData as CustomerTableItemView);
    model.contactPersonName = rowData.name;
    model.address = null;
    return this.baseDropdown.getAddressTransByCustomerDropDown(model.customerTableGUID);
  }
  buyerChange(model: DocumentReturnTableItemView, buyerTableOptions: SelectItems[]): Observable<SelectItems[]> {
    const rowData: BuyerTableItemView = isNullOrUndefOrEmptyGUID(model.buyerTableGUID)
      ? new BuyerTableItemView()
      : (buyerTableOptions.find((v) => v.value === model.buyerTableGUID).rowData as BuyerTableItemView);
    model.contactPersonName = rowData.buyerName;
    model.address = null;
    return this.baseDropdown.getAddressTransByBuyerDropDown(model.buyerTableGUID);
  }
  contactAddressChange(model: DocumentReturnTableItemView, addressTransOptions: SelectItems[]): any {
    const rowData: AddressTransItemView = isNullOrUndefOrEmptyGUID(model.addressTransGUID)
      ? new AddressTransItemView()
      : (addressTransOptions.find((v) => v.value === model.addressTransGUID).rowData as AddressTransItemView);
    if ((isNullOrUndefined(rowData.address1) || rowData.address1 == '') && (isNullOrUndefined(rowData.address2) || rowData.address2 == '')) {
      model.address = '';
    } else {
      model.address = rowData.address1 + ' ' + rowData.address2;
    }
  }
  getPostCanVisit(model: DocumentReturnTableItemView): boolean {
    return model.documentStatus_StatusId == DocumentReturnStatus.Draft;
  }
  getCloseCanVisit(model: DocumentReturnTableItemView): boolean {
    return model.documentStatus_StatusId == DocumentReturnStatus.Posted;
  }
  getCanCreate(model: DocumentReturnTableItemView): boolean {
    return model.documentStatus_StatusId == DocumentReturnStatus.Draft;
  }
  getCanUpdate(model: DocumentReturnTableItemView): boolean {
    return model.documentStatus_StatusId <= DocumentReturnStatus.Posted;
  }
  getCanDelete(model: DocumentReturnTableItemView): boolean {
    return model.documentStatus_StatusId == DocumentReturnStatus.Draft;
  }
}
