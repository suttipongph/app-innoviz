import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, DocumentReturnLineItemView, DocumentReturnLineListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class DocumentReturnLineService {
  serviceKey = 'documentReturnLineGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getDocumentReturnLineToList(search: SearchParameter): Observable<SearchResult<DocumentReturnLineListView>> {
    const url = `${this.servicePath}/GetDocumentReturnLineList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteDocumentReturnLine(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteDocumentReturnLine`;
    return this.dataGateway.delete(url, row);
  }
  getDocumentReturnLineById(id: string): Observable<DocumentReturnLineItemView> {
    const url = `${this.servicePath}/GetDocumentReturnLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
  initialDatatDocumentReturnLine(id: string): Observable<DocumentReturnLineItemView> {
    const url = `${this.servicePath}/GetInitialDatatDocumentReturnLine/id=${id}`;
    return this.dataGateway.get(url);
  }
  createDocumentReturnLine(vmModel: DocumentReturnLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateDocumentReturnLine`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateDocumentReturnLine(vmModel: DocumentReturnLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateDocumentReturnLine`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getAccessModeByParent(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByDocumentReturnTable/id=${id}`;
    return this.dataGateway.get(url);
  }
}
