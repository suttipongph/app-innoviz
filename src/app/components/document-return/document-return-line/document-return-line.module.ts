import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DocumentReturnLineService } from './document-return-line.service';
import { DocumentReturnLineListComponent } from './document-return-line-list/document-return-line-list.component';
import { DocumentReturnLineItemComponent } from './document-return-line-item/document-return-line-item.component';
import { DocumentReturnLineItemCustomComponent } from './document-return-line-item/document-return-line-item-custom.component';
import { DocumentReturnLineListCustomComponent } from './document-return-line-list/document-return-line-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [DocumentReturnLineListComponent, DocumentReturnLineItemComponent],
  providers: [DocumentReturnLineService, DocumentReturnLineItemCustomComponent, DocumentReturnLineListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [DocumentReturnLineListComponent, DocumentReturnLineItemComponent]
})
export class DocumentReturnLineComponentModule {}
