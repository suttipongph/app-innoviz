import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { DocumentReturnLineItemView } from 'shared/models/viewModel';
import { DocumentReturnLineService } from '../document-return-line.service';
import { DocumentReturnLineItemCustomComponent } from './document-return-line-item-custom.component';
@Component({
  selector: 'document-return-line-item',
  templateUrl: './document-return-line-item.component.html',
  styleUrls: ['./document-return-line-item.component.scss']
})
export class DocumentReturnLineItemComponent extends BaseItemComponent<DocumentReturnLineItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  addressTransOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  documentTypeOptions: SelectItems[] = [];

  constructor(
    private service: DocumentReturnLineService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: DocumentReturnLineItemCustomComponent
  ) {
    super();
    this.custom.baseDropdown = this.baseDropdown;
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiService.getKey('documentreturntable');
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<DocumentReturnLineItemView> {
    return this.service.getDocumentReturnLineById(this.id);
  }
  getInitialData(): Observable<DocumentReturnLineItemView> {
    return this.custom.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner(result);
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(
      this.baseDropdown.getAddressTransByBuyerDropDown(model.buyerTableGUID),
      this.baseDropdown.getBuyerTableDropDown(),
      this.baseDropdown.getDocumentTypeDropDown()
    ).subscribe(
      ([addressTrans, buyerTable, documentType]) => {
        this.addressTransOptions = addressTrans;
        this.buyerTableOptions = buyerTable;
        this.documentTypeOptions = documentType;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  // setFunctionOptions(): void {
  //   this.functionItems = [
  //     { label: 'LABEL.PRINT', items: [{ label: 'LABEL.LABEL', command: () => this.toFunction({ path: ROUTE_RELATED_GEN.LABEL }) }] }
  //   ];
  //   super.setFunctionOptionsMapping();
  // }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateDocumentReturnLine(this.model), isColsing);
    } else {
      super.onCreate(this.service.createDocumentReturnLine(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  buyerChange(): any{
    this.custom.buyerChange(this.model, this.buyerTableOptions).subscribe(addressTrans => {
      this.addressTransOptions = addressTrans;
    });
  }
  contactAddressChange(): any{
    this.custom.contactAddressChange(this.model, this.addressTransOptions);
  }
}
