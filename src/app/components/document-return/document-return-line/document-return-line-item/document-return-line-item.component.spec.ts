import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentReturnLineItemComponent } from './document-return-line-item.component';

describe('DocumentReturnLineItemViewComponent', () => {
  let component: DocumentReturnLineItemComponent;
  let fixture: ComponentFixture<DocumentReturnLineItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DocumentReturnLineItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentReturnLineItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
