import { BaseDropdownComponent } from 'core/components/base/base.dropdown.component';
import { Observable, of } from 'rxjs';
import { EmptyGuid, DocumentReturnStatus, ContactTo } from 'shared/constants';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { AddressTransItemView, BuyerTableItemView, DocumentReturnLineItemView } from 'shared/models/viewModel';

const firstGroup = ['LINE_NUM', 'DOCUMENT_RETURN_TABLE_GUID'];
const buyerGroup = ['BUYER_TABLE_GUID'];
const postedGroup = ['BUYER_TABLE_GUID', 'DOCUMENT_TYPE_GUID', 'DOCUMENT_NO'];
const closedGroup = ['BUYER_TABLE_GUID', 'DOCUMENT_TYPE_GUID', 'DOCUMENT_NO', 'CONTACT_PERSON_NAME', 'ADDRESS_TRANS_GUID', 'ADDRESS'];

export class DocumentReturnLineItemCustomComponent {
  baseService: BaseServiceModel<any>;
  public baseDropdown: BaseDropdownComponent;

  getFieldAccessing(model: DocumentReturnLineItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return new Observable((observer) => {
      if (isNullOrUndefOrEmptyGUID(model.documentReturnLineGUID)) {
        if (model.documentReturnTable_ContactTo === ContactTo.Buyer) {
          fieldAccessing.push({ filedIds: buyerGroup, readonly: true });
        }
        observer.next(fieldAccessing);
      } else {
        if (model.documentReturnTable_DocumentStatus_StatusId === DocumentReturnStatus.Draft) {
          if (model.documentReturnTable_ContactTo === ContactTo.Buyer) {
            fieldAccessing.push({ filedIds: buyerGroup, readonly: true });
          }
        }
        if (model.documentReturnTable_DocumentStatus_StatusId === DocumentReturnStatus.Posted) {
          fieldAccessing.push({ filedIds: postedGroup, readonly: true });
        }
        if (model.documentReturnTable_DocumentStatus_StatusId === DocumentReturnStatus.Closed) {
          fieldAccessing.push({ filedIds: closedGroup, readonly: true });
        }
        observer.next(fieldAccessing);
      }
    });
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: DocumentReturnLineItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(parentKey: string): Observable<DocumentReturnLineItemView> {
    return this.baseService.service.initialDatatDocumentReturnLine(parentKey);
  }
  buyerChange(model: DocumentReturnLineItemView, buyerTableOptions: SelectItems[]): Observable<SelectItems[]> {
    const rowData: BuyerTableItemView = isNullOrUndefOrEmptyGUID(model.buyerTableGUID)
      ? new BuyerTableItemView()
      : (buyerTableOptions.find((v) => v.value === model.buyerTableGUID).rowData as BuyerTableItemView);
    model.contactPersonName = rowData.buyerName;
    model.address = null;
    return this.baseDropdown.getAddressTransByBuyerDropDown(model.buyerTableGUID);
  }
  contactAddressChange(model: DocumentReturnLineItemView, addressTransOptions: SelectItems[]): any {
    const rowData: AddressTransItemView = isNullOrUndefOrEmptyGUID(model.addressTransGUID)
      ? new AddressTransItemView()
      : (addressTransOptions.find((v) => v.value === model.addressTransGUID).rowData as AddressTransItemView);
    if ((isNullOrUndefined(rowData.address1) || rowData.address1 == '') && (isNullOrUndefined(rowData.address2) || rowData.address2 == '')) {
      model.address = '';
    } else {
      model.address = rowData.address1 + ' ' + rowData.address2;
    }
  }
}
