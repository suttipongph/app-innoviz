import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AccessMode } from 'shared/constants';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView, DocumentReturnLineListView } from 'shared/models/viewModel';

export class DocumentReturnLineListCustomComponent {
  baseService: BaseServiceModel<any>;
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  getAccessModeByParent(parentId: string): Observable<any> {
    return this.baseService.service.getAccessModeByParent(parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
      })
    );
  }
  getCanCreate(canCreate: boolean): boolean {
    return this.accessModeView.canCreate && canCreate;
  }
  getCanDelete(canDelete: boolean): boolean {
    return this.accessModeView.canDelete && canDelete;
  }
  getRowAuthorize(row: DocumentReturnLineListView[]): boolean {
    let isSetRow: boolean = true;
    if (this.parentName === 'documentreturntable') {
      row.forEach((item) => {
        const accessMode: AccessMode = this.accessModeView.canDelete ? AccessMode.full : AccessMode.viewer;
        item.rowAuthorize = accessMode;
      });
    } else {
      isSetRow = false;
    }
    return isSetRow;
  }
}
