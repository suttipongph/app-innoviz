import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DocumentReturnLineListComponent } from './document-return-line-list.component';

describe('DocumentReturnLineListViewComponent', () => {
  let component: DocumentReturnLineListComponent;
  let fixture: ComponentFixture<DocumentReturnLineListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DocumentReturnLineListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentReturnLineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
