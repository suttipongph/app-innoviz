import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { DocumentReturnLineListView } from 'shared/models/viewModel';
import { DocumentReturnLineService } from '../document-return-line.service';
import { DocumentReturnLineListCustomComponent } from './document-return-line-list-custom.component';

@Component({
  selector: 'document-return-line-list',
  templateUrl: './document-return-line-list.component.html',
  styleUrls: ['./document-return-line-list.component.scss']
})
export class DocumentReturnLineListComponent extends BaseListComponent<DocumentReturnLineListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  addressTransOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  documentTypeOptions: SelectItems[] = [];

  constructor(public custom: DocumentReturnLineListCustomComponent, private service: DocumentReturnLineService) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.custom.parentName = this.uiService.getMasterRoute(1);
    this.parentId = this.uiService.getKey('documentreturntable');
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.custom.getAccessModeByParent(this.parentId).subscribe((result) => {
      this.setDataGridOption();
    });
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
    this.baseDropdown.getDocumentTypeDropDown(this.documentTypeOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreate(true);
    this.option.canView = true;
    this.option.canDelete = this.custom.getCanDelete(true);
    this.option.key = 'documentReturnLineGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.LINE',
        textKey: 'lineNum',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerTableOptions,
        sortingKey: 'buyerTable_buyerId',
        searchingKey: 'buyerTableGUID'
      },
      {
        label: 'LABEL.CONTACT_PERSON_NAME',
        textKey: 'contactPersonName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DOCUMENT_TYPE',
        textKey: 'documentType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentTypeOptions,
        sortingKey: 'documentType_DocumentTypeId',
        searchingKey: 'documentTypeGUID'
      },
      {
        label: 'LABEL.DOCUMENT_NO',
        textKey: 'documentNo',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'DocumentReturnTableGUID',
        type: ColumnType.MASTER,
        visibility: false,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<DocumentReturnLineListView>> {
    return this.service.getDocumentReturnLineToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteDocumentReturnLine(row));
  }
  getRowAuthorize(row: DocumentReturnLineListView[]): void {
    if (this.custom.getRowAuthorize(row)) {
      row.forEach((item) => {
        item.rowAuthorize = this.getSingleRowAuthorize(item.rowAuthorize, item);
      });
    } else {
      super.getRowAuthorize(row);
    }
  }
}
