import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { RptPrintDocumentReturnLabelLetterReportView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class PrintDocumentReturnLabelLetterService {
  serviceKey = 'printDocumentReturnLabelLetterGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getPrintDocumentReturnLabelLetterToList(search: SearchParameter): Observable<SearchResult<RptPrintDocumentReturnLabelLetterReportView>> {
    const url = `${this.servicePath}/GetPrintDocumentReturnLabelLetterList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deletePrintDocumentReturnLabelLetter(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeletePrintDocumentReturnLabelLetter`;
    return this.dataGateway.delete(url, row);
  }
  getPrintDocumentReturnLabelLetterById(id: string): Observable<RptPrintDocumentReturnLabelLetterReportView> {
    const url = `${this.servicePath}/GetPrintDocumentReturnLabelLetterById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createPrintDocumentReturnLabelLetter(vmModel: RptPrintDocumentReturnLabelLetterReportView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreatePrintDocumentReturnLabelLetter`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updatePrintDocumentReturnLabelLetter(vmModel: RptPrintDocumentReturnLabelLetterReportView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdatePrintDocumentReturnLabelLetter`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
