import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { RptPrintDocumentReturnLabelLetterReportView } from 'shared/models/viewModel';

const firstGroup = ['DOCUMENT_RETURN_ID', 'CONTACT_TO', 'CONTACT_PERSON_NAME', 'ADDRESS'];

export class RptPrintDocumentReturnLabelLetterReportCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: RptPrintDocumentReturnLabelLetterReportView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<RptPrintDocumentReturnLabelLetterReportView> {
    let model = new RptPrintDocumentReturnLabelLetterReportView();
    model.printDocumentReturnLabelLetterGUID = EmptyGuid;
    return of(model);
  }
}
