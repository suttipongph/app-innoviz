import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RptPrintDocumentReturnLabelLetterReportComponent } from './print-document-return-label-letter-report.component';

describe('PrintDocumentReturnLabelLetterReportViewComponent', () => {
  let component: RptPrintDocumentReturnLabelLetterReportComponent;
  let fixture: ComponentFixture<RptPrintDocumentReturnLabelLetterReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RptPrintDocumentReturnLabelLetterReportComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RptPrintDocumentReturnLabelLetterReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
