import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrintDocumentReturnLabelLetterService } from './print-document-return-label-letter.service';
import { RptPrintDocumentReturnLabelLetterReportComponent } from './print-document-return-label-letter/print-document-return-label-letter-report.component';
import { RptPrintDocumentReturnLabelLetterReportCustomComponent } from './print-document-return-label-letter/print-document-return-label-letter-report-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [RptPrintDocumentReturnLabelLetterReportComponent],
  providers: [PrintDocumentReturnLabelLetterService, RptPrintDocumentReturnLabelLetterReportCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [RptPrintDocumentReturnLabelLetterReportComponent]
})
export class PrintDocumentReturnLabelLetterComponentModule {}
