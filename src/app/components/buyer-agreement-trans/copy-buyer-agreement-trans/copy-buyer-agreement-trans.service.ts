import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CopyBuyerAgreementTransView } from 'shared/models/viewModel';
import { PageInformationModel } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CopyBuyerAgreementTransService {
  serviceKey = 'copyBuyerAgreementTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCopyBuyerAgreementTransById(id: string): Observable<CopyBuyerAgreementTransView> {
    const url = `${this.servicePath}/GetCopyBuyerAgreementTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  getCopyBuyerAgreementTransByRefTypeValidation(data: CopyBuyerAgreementTransView): Observable<boolean> {
    const url = `${this.servicePath}/GetCopyBuyerAgreementTransByRefTypeValidation`;
    return this.dataGateway.post(url, data);
  }
  CopyBuyerAgreementTrans(vmModel: CopyBuyerAgreementTransView): Observable<CopyBuyerAgreementTransView> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vmModel);
  }
}
