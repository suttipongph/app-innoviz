import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { CopyBuyerAgreementTransView } from 'shared/models/viewModel';
import { CopyBuyerAgreementTransService } from '../copy-buyer-agreement-trans.service';
import { CopyBuyerAgreementTransCustomComponent } from './copy-buyer-agreement-trans-custom.component';
@Component({
  selector: 'copy-buyer-agreement-trans',
  templateUrl: './copy-buyer-agreement-trans.component.html',
  styleUrls: ['./copy-buyer-agreement-trans.component.scss']
})
export class CopyBuyerAgreementTransComponent extends BaseItemComponent<CopyBuyerAgreementTransView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  buyerAgreementTableOptions: SelectItems[] = [];

  constructor(
    private service: CopyBuyerAgreementTransService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CopyBuyerAgreementTransCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialUpdatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<CopyBuyerAgreementTransView> {
    return this.service.getCopyBuyerAgreementTransById(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {}

  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getBuyerAgreementTableByBuyerAndCustomerDropDown([model.buyerTableGUID, model.customerTableGUID])).subscribe(
      ([buyerAgreementTable]) => {
        this.buyerAgreementTableOptions = buyerAgreementTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing().subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {}
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.custom.getModelParam(this.model, this.id).subscribe((model) => {
          this.onSubmit(true, model);
        });
      }
    });
  }
  onSubmit(isColsing: boolean, model: CopyBuyerAgreementTransView): void {
    super.onExecuteFunction(this.service.CopyBuyerAgreementTrans(model));
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation(this.model);
  }
}
