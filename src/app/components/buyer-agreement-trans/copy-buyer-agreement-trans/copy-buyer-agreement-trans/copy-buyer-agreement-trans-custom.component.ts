import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { switchMap } from 'rxjs/operators';
import { EmptyGuid, RefType, ROUTE_MASTER_GEN } from 'shared/constants';
import { BaseServiceModel, FieldAccessing, TranslateModel } from 'shared/models/systemModel';
import { CopyBuyerAgreementTransView } from 'shared/models/viewModel';
import { CopyBuyerAgreementTransService } from '../copy-buyer-agreement-trans.service';

const firstGroup = [];

export class CopyBuyerAgreementTransCustomComponent {
  baseService: BaseServiceModel<CopyBuyerAgreementTransService>;
  getFieldAccessing(): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getDataValidation(model: CopyBuyerAgreementTransView): Observable<boolean> {
    model.resultLabel = ['LABEL.BUYER_AGREEMENT_TRANSACTION', 'LABEL.CREDIT_APPLICATION_REQUEST_LINE'];
    return this.baseService.service.getCopyBuyerAgreementTransByRefTypeValidation(model);
  }
  getModelParam(model: CopyBuyerAgreementTransView, id: string): Observable<CopyBuyerAgreementTransView> {
    model.refGUID = id;
    model.refType = RefType.CreditAppRequestLine;
    model.resultLabel = ['LABEL.BUYER_AGREEMENT_TRANSACTION'];
    return of(model);
  }
}
