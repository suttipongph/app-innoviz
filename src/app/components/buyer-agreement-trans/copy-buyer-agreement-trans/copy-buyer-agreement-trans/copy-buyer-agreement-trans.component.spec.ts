import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopyBuyerAgreementTransComponent } from './copy-buyer-agreement-trans.component';

describe('CopyBuyerAgreementTransViewComponent', () => {
  let component: CopyBuyerAgreementTransComponent;
  let fixture: ComponentFixture<CopyBuyerAgreementTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CopyBuyerAgreementTransComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyBuyerAgreementTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
