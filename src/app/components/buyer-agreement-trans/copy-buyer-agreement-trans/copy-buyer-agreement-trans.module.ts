import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CopyBuyerAgreementTransService } from './copy-buyer-agreement-trans.service';
import { CopyBuyerAgreementTransComponent } from './copy-buyer-agreement-trans/copy-buyer-agreement-trans.component';
import { CopyBuyerAgreementTransCustomComponent } from './copy-buyer-agreement-trans/copy-buyer-agreement-trans-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CopyBuyerAgreementTransComponent],
  providers: [CopyBuyerAgreementTransService, CopyBuyerAgreementTransCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CopyBuyerAgreementTransComponent]
})
export class CopyBuyerAgreementTransComponentModule {}
