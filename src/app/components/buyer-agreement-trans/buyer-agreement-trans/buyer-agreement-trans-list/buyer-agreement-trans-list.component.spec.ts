import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BuyerAgreementTransListComponent } from './buyer-agreement-trans-list.component';

describe('BuyerAgreementTransListViewComponent', () => {
  let component: BuyerAgreementTransListComponent;
  let fixture: ComponentFixture<BuyerAgreementTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BuyerAgreementTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerAgreementTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
