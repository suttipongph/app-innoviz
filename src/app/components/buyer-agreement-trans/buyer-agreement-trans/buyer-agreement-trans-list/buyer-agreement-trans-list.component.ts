import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, ROUTE_FUNCTION_GEN, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { BuyerAgreementTransListView } from 'shared/models/viewModel';
import { BuyerAgreementTransService } from '../buyer-agreement-trans.service';
import { BuyerAgreementTransListCustomComponent } from './buyer-agreement-trans-list-custom.component';

@Component({
  selector: 'buyer-agreement-trans-list',
  templateUrl: './buyer-agreement-trans-list.component.html',
  styleUrls: ['./buyer-agreement-trans-list.component.scss']
})
export class BuyerAgreementTransListComponent extends BaseListComponent<BuyerAgreementTransListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  buyerAgreementLineOptions: SelectItems[] = [];
  buyerAgreementTableOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];

  constructor(public custom: BuyerAgreementTransListCustomComponent, private service: BuyerAgreementTransService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.custom.setAccessModeByParentStatus(this.isWorkFlowMode()).subscribe((result) => {
      this.setDataGridOption();
      this.setFunctionOptions();
    });
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBuyerAgreementLineDropDown(this.buyerAgreementLineOptions);
    this.baseDropdown.getBuyerAgeementTableByBuyerAgreementTransDropDown(this.parentId, this.buyerAgreementTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'buyerAgreementTransGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.BUYER_AGREEMENT_ID',
        textKey: 'buyerAgreementTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.ASC,
        masterList: this.buyerAgreementTableOptions,
        sortingKey: 'buyerAgreementTable_BuyerAgreementId',
        searchingKey: 'buyerAgreementTableGUID'
      },
      {
        label: 'LABEL.BUYER_AGREEMENT_LINE_ID',
        textKey: 'buyerAgreementLine_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.ASC,
        masterList: this.buyerAgreementLineOptions,
        sortingKey: 'buyerAgreementLine_Period',
        searchingKey: 'buyerAgreementLineGUID'
      },
      {
        label: 'LABEL.DUE_DATE',
        textKey: 'buyerAgreementLine_DueDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.AMOUNT',
        textKey: 'buyerAgreementLine_Amount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<BuyerAgreementTransListView>> {
    return this.service.getBuyerAgreementTransToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteBuyerAgreementTrans(row));
  }
  setFunctionOptions(): void {
    this.functionItems = [
      {
        label: 'LABEL.COPY_BUYER_AGREEMENT_TRANS',
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.COPY_BUYER_AGREEMENT_TRANS }),
        disabled: !this.custom.accessModeView.canCreate
      }
    ];

    super.setFunctionOptionsMapping();
  }
}
