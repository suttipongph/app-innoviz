import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BuyerAgreementTransService } from './buyer-agreement-trans.service';
import { BuyerAgreementTransListComponent } from './buyer-agreement-trans-list/buyer-agreement-trans-list.component';
import { BuyerAgreementTransItemComponent } from './buyer-agreement-trans-item/buyer-agreement-trans-item.component';
import { BuyerAgreementTransItemCustomComponent } from './buyer-agreement-trans-item/buyer-agreement-trans-item-custom.component';
import { BuyerAgreementTransListCustomComponent } from './buyer-agreement-trans-list/buyer-agreement-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BuyerAgreementTransListComponent, BuyerAgreementTransItemComponent],
  providers: [BuyerAgreementTransService, BuyerAgreementTransItemCustomComponent, BuyerAgreementTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BuyerAgreementTransListComponent, BuyerAgreementTransItemComponent]
})
export class BuyerAgreementTransComponentModule {}
