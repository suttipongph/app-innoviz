import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, BuyerAgreementTransItemView, BuyerAgreementTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class BuyerAgreementTransService {
  serviceKey = 'buyerAgreementTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBuyerAgreementTransToList(search: SearchParameter): Observable<SearchResult<BuyerAgreementTransListView>> {
    const url = `${this.servicePath}/GetBuyerAgreementTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBuyerAgreementTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBuyerAgreementTrans`;
    return this.dataGateway.delete(url, row);
  }
  getBuyerAgreementTransById(id: string): Observable<BuyerAgreementTransItemView> {
    const url = `${this.servicePath}/GetBuyerAgreementTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBuyerAgreementTrans(vmModel: BuyerAgreementTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBuyerAgreementTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBuyerAgreementTrans(vmModel: BuyerAgreementTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBuyerAgreementTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getAccessModeByCreditAppRequestTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppRequestTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getInitialData(id: string): Observable<BuyerAgreementTransItemView> {
    const url = `${this.servicePath}/GetBuyerAgreementTransInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getWithdrawalTableAcessMode(withdrawalId: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetWithdrawalTableAcessMode/withdrawalId=${withdrawalId}`;
    return this.dataGateway.get(url);
  }
}
