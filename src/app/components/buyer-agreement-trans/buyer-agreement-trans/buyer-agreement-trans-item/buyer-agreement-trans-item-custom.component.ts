import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { CreditAppRequestStatus, EmptyGuid } from 'shared/constants';
import { isNullOrUndefOrEmpty, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, BuyerAgreementLineItemView, BuyerAgreementTransItemView } from 'shared/models/viewModel';

const firstGroup = ['DUE_DATE', 'AMOUNT', 'REF_TYPE', 'REF_ID'];
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const CREDIT_APP_REQUEST_LINE = 'creditapprequestline-child';
const CREDIT_APP_LINE = 'creditappline-child';
const BUYER_MATCHING = 'buyermatching';
const LOAN_REQUEST = 'loanrequest';
const WITHDRAWAL_TABLE_WITHDRAWAL = 'withdrawaltablewithdrawal';
const WITHDRAWAL_TABLE_TERM_EXTENSION = 'withdrawaltabletermextension';
export class BuyerAgreementTransItemCustomComponent {
  baseService: BaseServiceModel<any>;
  parentName = null;
  parentId = null;
  accessModeView: AccessModeView = new AccessModeView();
  isBuyerMatching: boolean = false;
  isLoanRequest: boolean = false;

  getFieldAccessing(model: BuyerAgreementTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BuyerAgreementTransItemView, isWorkFlowMode: boolean): Observable<boolean> {
    return this.setAccessModeByParentStatus(model, isWorkFlowMode).pipe(
      map((result) => (isUpdateMode(model.buyerAgreementTransGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  setAccessModeByParentStatus(model: BuyerAgreementTransItemView, isWorkFlowMode: boolean): Observable<AccessModeView> {
    this.parentId = this.baseService.uiService.getRelatedInfoOriginTableKey();
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
      case CREDIT_APP_REQUEST_LINE:
        this.checkIsBuyerMacthingOrLoanRequest();
        return this.getAccessModeByCreditAppRequestTable(isWorkFlowMode);
      case CREDIT_APP_LINE:
        this.accessModeView.canCreate = false;
        this.accessModeView.canDelete = false;
        this.accessModeView.canView = false;
        return of(this.accessModeView);
      case WITHDRAWAL_TABLE_WITHDRAWAL:
      case WITHDRAWAL_TABLE_TERM_EXTENSION:
        return this.getWithdrawalTableAcessMode(this.baseService.uiService.getRelatedInfoParentTableKey());
      default:
        return of(this.accessModeView);
    }
  }
  getInitialData(): Observable<BuyerAgreementTransItemView> {
    let model = new BuyerAgreementTransItemView();
    model.buyerAgreementTransGUID = EmptyGuid;
    return of(model);
  }
  clearBuyerAgreementLine(model: BuyerAgreementTransItemView) {
    model.buyerAgreementLineGUID = null;
    model.buyerAgreementLine_DueDate = null;
    model.buyerAgreementLine_Amount = null;
  }
  setBuyerAgreementLine(model: BuyerAgreementTransItemView, item: SelectItems[]) {
    if (!isNullOrUndefOrEmpty(model.buyerAgreementLineGUID)) {
      let buyerAgreementLine: BuyerAgreementLineItemView = item.find(
        (t) => (t.rowData as BuyerAgreementLineItemView).buyerAgreementLineGUID == model.buyerAgreementLineGUID
      ).rowData as BuyerAgreementLineItemView;
      model.buyerAgreementLine_DueDate = buyerAgreementLine.dueDate;
      model.buyerAgreementLine_Amount = buyerAgreementLine.amount;
    } else {
      this.clearBuyerAgreementLine(model);
    }
  }
  getBuyerAgreementLineDropDown(model: BuyerAgreementTransItemView) {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
      case CREDIT_APP_REQUEST_LINE:
        if (!isNullOrUndefOrEmptyGUID(model.buyerAgreementTableGUID)) {
          return this.baseService.baseDropdown.getBuyerAgreementLineByBuyerAgreementTransDropDown(model.buyerAgreementTableGUID);
        } else {
          return of([]);
        }
      case CREDIT_APP_LINE:
        return this.baseService.baseDropdown.getBuyerAgreementLineDropDown();
      case WITHDRAWAL_TABLE_WITHDRAWAL:
      case WITHDRAWAL_TABLE_TERM_EXTENSION:
        if (!isNullOrUndefOrEmptyGUID(model.buyerAgreementTableGUID)) {
          return this.baseService.baseDropdown.getBuyerAgreementLineByBuyerAgreementTransDropDown(model.buyerAgreementTableGUID);
        } else {
          return of([]);
        }
      default:
        return of([]);
    }
  }

  getBuyerAgreementTableDropDown(parentId) {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
      case CREDIT_APP_REQUEST_LINE:
        return this.baseService.baseDropdown.getBuyerAgeementTableByBuyerAgreementTransDropDown(parentId);
      case CREDIT_APP_LINE:
        return this.baseService.baseDropdown.getBuyerAgreementTableDropDown();
      case WITHDRAWAL_TABLE_WITHDRAWAL:
      case WITHDRAWAL_TABLE_TERM_EXTENSION:
        return this.baseService.baseDropdown.getBuyerAgreementTableDropDown();
      default:
        return of([]);
    }
  }
  getAccessModeByCreditAppRequestTable(isWorkFlowMode: boolean): Observable<AccessModeView> {
    return this.baseService.service.getAccessModeByCreditAppRequestTable(this.parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
        const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
        this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
        this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
        this.accessModeView.canView = true;
      })
    );
  }
  getWithdrawalTableAcessMode(parentId: string): Observable<AccessModeView> {
    return this.baseService.service.getWithdrawalTableAcessMode(parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
      })
    );
  }

  checkIsBuyerMacthingOrLoanRequest(): void {
    this.isBuyerMatching = window.location.href.match(BUYER_MATCHING) ? true : false;
    this.isLoanRequest = window.location.href.match(LOAN_REQUEST) ? true : false;
    if (this.isBuyerMatching) {
      this.parentId = this.baseService.uiService.getKey(BUYER_MATCHING);
    } else if (this.isLoanRequest) {
      this.parentId = this.baseService.uiService.getKey(LOAN_REQUEST);
    }
  }
}
