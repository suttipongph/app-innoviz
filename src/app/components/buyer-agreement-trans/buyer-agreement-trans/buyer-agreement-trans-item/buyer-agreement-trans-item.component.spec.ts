import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyerAgreementTransItemComponent } from './buyer-agreement-trans-item.component';

describe('BuyerAgreementTransItemViewComponent', () => {
  let component: BuyerAgreementTransItemComponent;
  let fixture: ComponentFixture<BuyerAgreementTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BuyerAgreementTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerAgreementTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
