import { isNull } from '@angular/compiler/src/output/output_ast';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmpty, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { BuyerAgreementTransItemView } from 'shared/models/viewModel';
import { BuyerAgreementTransService } from '../buyer-agreement-trans.service';
import { BuyerAgreementTransItemCustomComponent } from './buyer-agreement-trans-item-custom.component';
@Component({
  selector: 'buyer-agreement-trans-item',
  templateUrl: './buyer-agreement-trans-item.component.html',
  styleUrls: ['./buyer-agreement-trans-item.component.scss']
})
export class BuyerAgreementTransItemComponent extends BaseItemComponent<BuyerAgreementTransItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  buyerAgreementLineOptions: SelectItems[] = [];
  buyerAgreementTableOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];

  constructor(
    private service: BuyerAgreementTransService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: BuyerAgreementTransItemCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<BuyerAgreementTransItemView> {
    return this.service.getBuyerAgreementTransById(this.id);
  }
  getInitialData(): Observable<BuyerAgreementTransItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    let tempModel: BuyerAgreementTransItemView = this.isUpdateMode ? model : this.model;
    forkJoin(this.custom.getBuyerAgreementLineDropDown(tempModel), this.custom.getBuyerAgreementTableDropDown(this.parentId)).subscribe(
      ([buyerAgreementLine, buyerAgreementTable]) => {
        this.buyerAgreementLineOptions = buyerAgreementLine;
        this.buyerAgreementTableOptions = buyerAgreementTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model, this.isWorkFlowMode()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateBuyerAgreementTrans(this.model), isColsing);
    } else {
      super.onCreate(this.service.createBuyerAgreementTrans(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }

  onBuyerAgreementTableChange() {
    this.custom.clearBuyerAgreementLine(this.model);
    if (!isNullOrUndefined(this.model.buyerAgreementTableGUID)) {
      this.baseDropdown.getBuyerAgreementLineByBuyerAgreementTransDropDown(this.model.buyerAgreementTableGUID).subscribe((result) => {
        this.buyerAgreementLineOptions = result;
      });
    }
  }
  onBuyerAgreementLineChange() {
    this.custom.setBuyerAgreementLine(this.model, this.buyerAgreementLineOptions);
  }
}
