import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CAReqBuyerCreditOutstandingListView } from 'shared/models/viewModel';
import { CAReqBuyerCreditOutstandingService } from '../ca-req-buyer-credit-outstanding.service';
import { CAReqBuyerCreditOutstandingListCustomComponent } from './ca-req-buyer-credit-outstanding-list-custom.component';

@Component({
  selector: 'ca-req-buyer-credit-outstanding-list',
  templateUrl: './ca-req-buyer-credit-outstanding-list.component.html',
  styleUrls: ['./ca-req-buyer-credit-outstanding-list.component.scss']
})
export class CAReqBuyerCreditOutstandingListComponent extends BaseListComponent<CAReqBuyerCreditOutstandingListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  buyerTableOptions: SelectItems[] = [];
  methodOfPaymenOptions: SelectItems[] = [];
  billingResponsibleByOptions: SelectItems[] = [];
  assignmentMethodOptions: SelectItems[] = [];
  constructor(
    public custom: CAReqBuyerCreditOutstandingListCustomComponent,
    private service: CAReqBuyerCreditOutstandingService,
    public currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.parentId = this.custom.getRelatedInfoParentTableKey(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
    this.baseDropdown.getAssignmentMethodDropDown(this.assignmentMethodOptions);
    this.baseDropdown.getBillingResponsibleByDropDown(this.billingResponsibleByOptions);
    this.baseDropdown.getMethodOfPaymentDropDown(this.methodOfPaymenOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = false;
    this.option.canDelete = false;
    this.option.key = 'caReqBuyerCreditOutstandingGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.LINE',
        textKey: 'lineNum',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.ASC,
        format: this.INTEGER
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'buyerTableGUID',
        sortingKey: 'buyerTable_BuyerId',
        masterList: this.buyerTableOptions
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'status',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.APPROVED_CREDIT_LIMIT_LINE',
        textKey: 'approvedCreditLimitLine',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: 'LABEL.AR_BALANCE',
        textKey: 'arBalance',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: 'LABEL.MAXIMUM_PURCHASE_PCT',
        textKey: 'maxPurchasePct',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: 'LABEL.ASSIGNMENT_METHOD_ID',
        textKey: 'assignmentMethod_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'assignmentMethodGUID',
        sortingKey: 'assignmentMethod_AssignmentMethodId',
        masterList: this.assignmentMethodOptions
      },
      {
        label: 'LABEL.BILLING_RESPONSIBLE_BY_ID',
        textKey: 'billingResponsibleBy_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'billingResponsibleByGUID',
        sortingKey: 'billingResponsibleBy_BillingResponsibleById',
        masterList: this.billingResponsibleByOptions
      },
      {
        label: 'LABEL.METHOD_OF_PAYMENT_ID',
        textKey: 'methodOfPayment_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'methodOfPaymentGUID',
        sortingKey: 'methodOfPayment_MethodOfPaymentId',
        masterList: this.methodOfPaymenOptions
      },
      {
        label: null,
        textKey: 'creditAppTable_CreditAppId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'creditAppRequestTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CAReqBuyerCreditOutstandingListView>> {
    return this.service.getCAReqBuyerCreditOutstandingToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCAReqBuyerCreditOutstanding(row));
  }
}
