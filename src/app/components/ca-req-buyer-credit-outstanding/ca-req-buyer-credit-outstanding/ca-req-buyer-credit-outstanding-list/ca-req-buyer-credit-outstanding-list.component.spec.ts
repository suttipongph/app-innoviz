import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CAReqBuyerCreditOutstandingListComponent } from './ca-req-buyer-credit-outstanding-list.component';

describe('CAReqBuyerCreditOutstandingListViewComponent', () => {
  let component: CAReqBuyerCreditOutstandingListComponent;
  let fixture: ComponentFixture<CAReqBuyerCreditOutstandingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CAReqBuyerCreditOutstandingListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CAReqBuyerCreditOutstandingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
