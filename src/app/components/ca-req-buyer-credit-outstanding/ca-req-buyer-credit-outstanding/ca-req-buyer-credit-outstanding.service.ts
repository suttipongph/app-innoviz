import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CAReqBuyerCreditOutstandingListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CAReqBuyerCreditOutstandingService {
  serviceKey = 'caReqBuyerCreditOutstandingGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCAReqBuyerCreditOutstandingToList(search: SearchParameter): Observable<SearchResult<CAReqBuyerCreditOutstandingListView>> {
    const url = `${this.servicePath}/GetCAReqBuyerCreditOutstandingList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCAReqBuyerCreditOutstanding(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCAReqBuyerCreditOutstanding`;
    return this.dataGateway.delete(url, row);
  }
}
