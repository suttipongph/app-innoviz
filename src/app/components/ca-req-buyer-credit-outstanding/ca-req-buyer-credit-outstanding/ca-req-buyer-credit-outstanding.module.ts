import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CAReqBuyerCreditOutstandingService } from './ca-req-buyer-credit-outstanding.service';
import { CAReqBuyerCreditOutstandingListComponent } from './ca-req-buyer-credit-outstanding-list/ca-req-buyer-credit-outstanding-list.component';
import { CAReqBuyerCreditOutstandingListCustomComponent } from './ca-req-buyer-credit-outstanding-list/ca-req-buyer-credit-outstanding-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CAReqBuyerCreditOutstandingListComponent],
  providers: [CAReqBuyerCreditOutstandingService, CAReqBuyerCreditOutstandingListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CAReqBuyerCreditOutstandingListComponent]
})
export class CAReqBuyerCreditOutstandingComponentModule {}
