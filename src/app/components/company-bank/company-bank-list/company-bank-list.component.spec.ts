import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CompanyBankListComponent } from './company-bank-list.component';

describe('CompanyBankListViewComponent', () => {
  let component: CompanyBankListComponent;
  let fixture: ComponentFixture<CompanyBankListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CompanyBankListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyBankListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
