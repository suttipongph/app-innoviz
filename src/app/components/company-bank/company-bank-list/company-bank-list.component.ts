import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CompanyBankListView } from 'shared/models/viewModel';
import { CompanyBankService } from '../company-bank.service';
import { CompanyBankListCustomComponent } from './company-bank-list-custom.component';

@Component({
  selector: 'company-bank-list',
  templateUrl: './company-bank-list.component.html',
  styleUrls: ['./company-bank-list.component.scss']
})
export class CompanyBankListComponent extends BaseListComponent<CompanyBankListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  bankGroupOptions: SelectItems[] = [];
  bankTypeOptions: SelectItems[] = [];
  defualtBitOption: SelectItems[] = [];

  constructor(public custom: CompanyBankListCustomComponent, private service: CompanyBankService, public uiControllerService: UIControllerService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getDefaultBitDropDown(this.defualtBitOption);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBankGroupDropDown(this.bankGroupOptions);
    this.baseDropdown.getBankTypeDropDown(this.bankTypeOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'companyBankGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.FINANCIAL_INSTITUTION_ID',
        textKey: 'bankGroup_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.bankGroupOptions,
        sortingKey: 'bankGroup_BankGroupId',
        searchingKey: 'bankGroupGUID'
      },
      {
        label: 'LABEL.BANK_ACCOUNT_TYPE_ID',
        textKey: 'bankType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.bankTypeOptions,
        sortingKey: 'bankType_BankTypeId',
        searchingKey: 'bankTypeGUID'
      },
      {
        label: 'LABEL.ACCOUNT_NUMBER',
        textKey: 'accountNumber',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.ACCOUNT_NAME',
        textKey: 'bankAccountName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PRIMARY',
        textKey: 'primary',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defualtBitOption
      },
      {
        label: 'LABEL.INACTIVE',
        textKey: 'inActive',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defualtBitOption
      },
      {
        label: null,
        textKey: 'companyGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CompanyBankListView>> {
    return this.service.getCompanyBankToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCompanyBank(row));
  }
}
