import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CompanyBankItemView } from 'shared/models/viewModel';

const firstGroup = [];

export class CompanyBankItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CompanyBankItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CompanyBankItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<CompanyBankItemView> {
    let model: CompanyBankItemView = new CompanyBankItemView();
    model.companyBankGUID = EmptyGuid;
    model.primary = false;
    model.inActive = false;
    return of(model);
  }
}
