import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyBankItemComponent } from './company-bank-item.component';

describe('CompanyBankItemViewComponent', () => {
  let component: CompanyBankItemComponent;
  let fixture: ComponentFixture<CompanyBankItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CompanyBankItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyBankItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
