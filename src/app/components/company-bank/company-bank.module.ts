import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CompanyBankService } from './company-bank.service';
import { CompanyBankListComponent } from './company-bank-list/company-bank-list.component';
import { CompanyBankItemComponent } from './company-bank-item/company-bank-item.component';
import { CompanyBankItemCustomComponent } from './company-bank-item/company-bank-item-custom.component';
import { CompanyBankListCustomComponent } from './company-bank-list/company-bank-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CompanyBankListComponent, CompanyBankItemComponent],
  providers: [CompanyBankService, CompanyBankItemCustomComponent, CompanyBankListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CompanyBankListComponent, CompanyBankItemComponent]
})
export class CompanyBankComponentModule {}
