import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { CompanyBankItemView, CompanyBankListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CompanyBankService {
  serviceKey = 'companyBankGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCompanyBankToList(search: SearchParameter): Observable<SearchResult<CompanyBankListView>> {
    const url = `${this.servicePath}/GetCompanyBankList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCompanyBank(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCompanyBank`;
    return this.dataGateway.delete(url, row);
  }
  getCompanyBankById(id: string): Observable<CompanyBankItemView> {
    const url = `${this.servicePath}/GetCompanyBankById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCompanyBank(vmModel: CompanyBankItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCompanyBank`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCompanyBank(vmModel: CompanyBankItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCompanyBank`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
