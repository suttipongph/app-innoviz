import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JointVentureTransItemComponent } from './joint-venture-trans-item.component';

describe('JointVentureTransItemViewComponent', () => {
  let component: JointVentureTransItemComponent;
  let fixture: ComponentFixture<JointVentureTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [JointVentureTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JointVentureTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
