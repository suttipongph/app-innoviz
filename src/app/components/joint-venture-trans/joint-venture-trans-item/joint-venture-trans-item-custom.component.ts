import { Observable, of } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { BusinessCollateralAgreementStatus } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { AccessModeView, JointVentureTransItemView } from 'shared/models/viewModel';
import { JointVentureTransService } from '../joint-venture-trans.service';

const firstGroup = ['REF_TYPE', 'REF_ID'];
const MAIN_AGREEMENT_TABLE = 'mainagreementtable';
const CUSTOMER_TABLE = 'customertable';
const ASSIGNMENT_AGREEMENT_TABLE = 'assignmentagreementtable';
const BUSINESS_COLLATERAL_AGM_TABLE = 'businesscollateralagmtable';
const ADDENDUM_MAIN_AGREEMENT_TABLE = 'addendummainagreementtable';
const NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE = 'noticeofcancellationmainagreementtable';
const ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE = 'addendumbusinesscollateralagmtable';
const NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE = 'noticeofcancellation';
const NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE = 'noticeofcancellationbusinesscollateralagmtable';

export class JointVentureTransItemCustomComponent {
  baseService: BaseServiceModel<JointVentureTransService>;
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  getFieldAccessing(model: JointVentureTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: JointVentureTransItemView): Observable<boolean> {
    return this.setAccessModeByParentStatus(model).pipe(
      map((result) => (isUpdateMode(model.jointVentureTransGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  setAccessModeByParentStatus(model: JointVentureTransItemView): Observable<AccessModeView> {
    switch (this.parentName) {
      case MAIN_AGREEMENT_TABLE:
      case ADDENDUM_MAIN_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE:
        return this.baseService.service.getAccessModeByMainAgreementTable(this.baseService.uiService.getRelatedInfoParentTableKey()).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
          })
        );
      case CUSTOMER_TABLE:
        this.accessModeView.canCreate = true;
        this.accessModeView.canDelete = true;
        this.accessModeView.canView = true;
        return of(this.accessModeView);

      case BUSINESS_COLLATERAL_AGM_TABLE:
      case ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
      case NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
        const statusCreate = model.documentStatus_StatusCode < BusinessCollateralAgreementStatus.Sent;
        this.accessModeView.canCreate = statusCreate;
        this.accessModeView.canDelete = true;
        this.accessModeView.canView = true;

        return of(this.accessModeView);
      case ASSIGNMENT_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE:
        return this.baseService.service.getAccessModeByAssignmentTable(this.baseService.uiService.getRelatedInfoParentTableKey()).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
          })
        );
      default:
        return of(this.accessModeView);
    }
  }
}
