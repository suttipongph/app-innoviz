import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { JointVentureTransListComponent } from './joint-venture-trans-list.component';

describe('JointVentureTransListViewComponent', () => {
  let component: JointVentureTransListComponent;
  let fixture: ComponentFixture<JointVentureTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [JointVentureTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JointVentureTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
