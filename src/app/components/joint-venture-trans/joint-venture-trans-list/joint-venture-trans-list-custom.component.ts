import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { BusinessCollateralAgreementStatus } from 'shared/constants';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView } from 'shared/models/viewModel';
import { JointVentureTransService } from '../joint-venture-trans.service';
const MAIN_AGREEMENT_TABLE = 'mainagreementtable';
const CUSTOMER_TABLE = 'customertable';
const ASSIGNMENT_AGREEMENT_TABLE = 'assignmentagreementtable';
const BUSINESS_COLLATERAL_AGM_TABLE = 'businesscollateralagmtable';
const ADDENDUM_MAIN_AGREEMENT_TABLE = 'addendummainagreementtable';
const NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE = 'noticeofcancellationmainagreementtable';
const ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE = 'addendumbusinesscollateralagmtable';
const NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE = 'noticeofcancellation';
const NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE = 'noticeofcancellationbusinesscollateralagmtable';

export class JointVentureTransListCustomComponent {
  baseService: BaseServiceModel<JointVentureTransService>;
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setAccessModeByParentStatus(): Observable<AccessModeView> {
    let parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
    switch (this.parentName) {
      case MAIN_AGREEMENT_TABLE:
      case ADDENDUM_MAIN_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE:
        return this.baseService.service.getAccessModeByMainAgreementTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            this.accessModeView.canView = true;
          })
        );
      case CUSTOMER_TABLE:
        this.accessModeView.canCreate = true;
        this.accessModeView.canDelete = true;
        this.accessModeView.canView = true;
        return of(this.accessModeView);
      case BUSINESS_COLLATERAL_AGM_TABLE:
      case ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
      case NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
        return this.baseService.service.getAccessModeByBusinessCollateralAgmTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            this.accessModeView.canView = true;
          })
        );
      case ASSIGNMENT_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE:
        return this.baseService.service.getAccessModeByAssignmentTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            this.accessModeView.canView = true;
          })
        );
      default:
        return of(this.accessModeView);
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
}
