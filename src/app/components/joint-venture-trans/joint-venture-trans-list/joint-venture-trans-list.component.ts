import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { AccessMode, BusinessCollateralAgreementStatus, ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { JointVentureTransListView } from 'shared/models/viewModel';
import { JointVentureTransService } from '../joint-venture-trans.service';
import { JointVentureTransListCustomComponent } from './joint-venture-trans-list-custom.component';
const BUSINESS_COLLATERAL_AGM_TABLE = 'businesscollateralagmtable';
@Component({
  selector: 'joint-venture-trans-list',
  templateUrl: './joint-venture-trans-list.component.html',
  styleUrls: ['./joint-venture-trans-list.component.scss']
})
export class JointVentureTransListComponent extends BaseListComponent<JointVentureTransListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  authorizedPersonTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  statuscode;

  constructor(
    public custom: JointVentureTransListCustomComponent,
    private service: JointVentureTransService,
    public uiControllerService: UIControllerService,
    public currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.custom.setAccessModeByParentStatus().subscribe((result) => {
      this.setDataGridOption();
    });
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getAuthorizedPersonTypeDropDown(this.authorizedPersonTypeOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = true;
    this.option.key = 'jointVentureTransGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.ORDERING',
        textKey: 'ordering',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.AUTHORIZED_PERSON_TYPE_ID',
        textKey: 'authorizedPersonType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.authorizedPersonTypeOptions,
        sortingKey: 'authorizedPersonType_AuthorizedPersonTypeId',
        searchingKey: 'authorizedPersonTypeGUID'
      },
      {
        label: 'LABEL.NAME',
        textKey: 'name',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.OPERATED_BY',
        textKey: 'operatedBy',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<JointVentureTransListView>> {
    return this.service.getJointVentureTransToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteJointVentureTrans(row));
  }
  getRowAuthorize(row: JointVentureTransListView[]): void {
    if (this.custom.parentName === BUSINESS_COLLATERAL_AGM_TABLE) {
      row.forEach((item) => {
        let accessMode: AccessMode = item.documentStatus_StatusCode < BusinessCollateralAgreementStatus.Signed ? AccessMode.full : AccessMode.creator;
        item.rowAuthorize = this.getSingleRowAuthorize(accessMode, item);
      });
    } else {
      super.getRowAuthorize(row);
    }
  }
}
