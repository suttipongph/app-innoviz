import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { AccessModeView, JointVentureTransItemView, JointVentureTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class JointVentureTransService {
  serviceKey = 'jointVentureTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getJointVentureTransToList(search: SearchParameter): Observable<SearchResult<JointVentureTransListView>> {
    const url = `${this.servicePath}/GetJointVentureTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteJointVentureTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteJointVentureTrans`;
    return this.dataGateway.delete(url, row);
  }
  getJointVentureTransById(id: string): Observable<JointVentureTransItemView> {
    const url = `${this.servicePath}/GetJointVentureTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createJointVentureTrans(vmModel: JointVentureTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateJointVentureTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateJointVentureTrans(vmModel: JointVentureTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateJointVentureTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<JointVentureTransItemView> {
    const url = `${this.servicePath}/GetJointVentureTransInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByMainAgreementTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByMainAgreementTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByAssignmentTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByAssignmentTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByBusinessCollateralAgmTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByBusinessCollateralAgmTable/id=${id}`;
    return this.dataGateway.get(url);
  }
}
