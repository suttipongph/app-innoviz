import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JointVentureTransService } from './joint-venture-trans.service';
import { JointVentureTransListComponent } from './joint-venture-trans-list/joint-venture-trans-list.component';
import { JointVentureTransItemComponent } from './joint-venture-trans-item/joint-venture-trans-item.component';
import { JointVentureTransItemCustomComponent } from './joint-venture-trans-item/joint-venture-trans-item-custom.component';
import { JointVentureTransListCustomComponent } from './joint-venture-trans-list/joint-venture-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [JointVentureTransListComponent, JointVentureTransItemComponent],
  providers: [JointVentureTransService, JointVentureTransItemCustomComponent, JointVentureTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [JointVentureTransListComponent, JointVentureTransItemComponent]
})
export class JointVentureTransComponentModule {}
