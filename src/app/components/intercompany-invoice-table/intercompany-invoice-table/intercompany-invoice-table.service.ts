import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { IntercompanyInvoiceTableItemView, IntercompanyInvoiceTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class IntercompanyInvoiceTableService {
  serviceKey = 'intercompanyInvoiceTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getIntercompanyInvoiceTableToList(search: SearchParameter): Observable<SearchResult<IntercompanyInvoiceTableListView>> {
    const url = `${this.servicePath}/GetIntercompanyInvoiceTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteIntercompanyInvoiceTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteIntercompanyInvoiceTable`;
    return this.dataGateway.delete(url, row);
  }
  getIntercompanyInvoiceTableById(id: string): Observable<IntercompanyInvoiceTableItemView> {
    const url = `${this.servicePath}/GetIntercompanyInvoiceTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createIntercompanyInvoiceTable(vmModel: IntercompanyInvoiceTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateIntercompanyInvoiceTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateIntercompanyInvoiceTable(vmModel: IntercompanyInvoiceTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateIntercompanyInvoiceTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getValidateAdjustFunction(id: string): Observable<boolean> {
    const url = `${this.servicePath}/GetValidateAdjustFunction/id=${id}`;
    return this.dataGateway.get(url);
  }
  getValidateAdjustButton(id: string): Observable<boolean> {
    const url = `${this.servicePath}/GetValidateAdjustBottonฺ/id=${id}`;
    return this.dataGateway.get(url);
  }
}
