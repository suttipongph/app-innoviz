import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IntercompanyInvoiceTableService } from './intercompany-invoice-table.service';
import { IntercompanyInvoiceTableListComponent } from './intercompany-invoice-table-list/intercompany-invoice-table-list.component';
import { IntercompanyInvoiceTableItemComponent } from './intercompany-invoice-table-item/intercompany-invoice-table-item.component';
import { IntercompanyInvoiceTableItemCustomComponent } from './intercompany-invoice-table-item/intercompany-invoice-table-item-custom.component';
import { IntercompanyInvoiceTableListCustomComponent } from './intercompany-invoice-table-list/intercompany-invoice-table-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [IntercompanyInvoiceTableListComponent, IntercompanyInvoiceTableItemComponent],
  providers: [IntercompanyInvoiceTableService, IntercompanyInvoiceTableItemCustomComponent, IntercompanyInvoiceTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [IntercompanyInvoiceTableListComponent, IntercompanyInvoiceTableItemComponent]
})
export class IntercompanyInvoiceTableComponentModule {}
