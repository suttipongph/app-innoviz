import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { AdjustIntercompanyInvoiceView } from 'shared/models/viewModel';

const firstGroup = ['INTERCOMPANY_INVOICE_ID', 'ORIGINAL_INVOICE_AMOUNT', 'BALANCE'];

export class AdjustIntercompanyInvoiceCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: AdjustIntercompanyInvoiceView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<AdjustIntercompanyInvoiceView> {
    let model = new AdjustIntercompanyInvoiceView();
    model.adjustIntercompanyInvoiceGUID = EmptyGuid;
    return of(model);
  }
}
