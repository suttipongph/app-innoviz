import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdjustIntercompanyInvoiceComponent } from './adjust-intercompany-invoice.component';

describe('AdjustIntercompanyInvoiceViewComponent', () => {
  let component: AdjustIntercompanyInvoiceComponent;
  let fixture: ComponentFixture<AdjustIntercompanyInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdjustIntercompanyInvoiceComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdjustIntercompanyInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
