import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AdjustIntercompanyInvoiceView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class AdjustIntercompanyInvoiceService {
  serviceKey = 'adjustIntercompanyInvoiceGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getAdjustIntercompanyInvoiceToList(search: SearchParameter): Observable<SearchResult<AdjustIntercompanyInvoiceView>> {
    const url = `${this.servicePath}/GetAdjustIntercompanyInvoiceList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteAdjustIntercompanyInvoice(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteAdjustIntercompanyInvoice`;
    return this.dataGateway.delete(url, row);
  }
  getAdjustIntercompanyInvoiceById(id: string): Observable<AdjustIntercompanyInvoiceView> {
    const url = `${this.servicePath}/GetAdjustIntercompanyInvoiceById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createAdjustIntercompanyInvoice(vmModel: AdjustIntercompanyInvoiceView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateAdjustIntercompanyInvoice`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateAdjustIntercompanyInvoice(vmModel: AdjustIntercompanyInvoiceView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateAdjustIntercompanyInvoice`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getIntercompanyById(id: string): Observable<AdjustIntercompanyInvoiceView> {
    const url = `${this.servicePath}/GetIntercompanyById/id=${id}`;
    return this.dataGateway.get(url);
  }
}
