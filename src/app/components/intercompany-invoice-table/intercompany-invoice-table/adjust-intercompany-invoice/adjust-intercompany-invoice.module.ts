import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdjustIntercompanyInvoiceService } from './adjust-intercompany-invoice.service';
import { AdjustIntercompanyInvoiceComponent } from './adjust-intercompany-invoice/adjust-intercompany-invoice.component';
import { AdjustIntercompanyInvoiceCustomComponent } from './adjust-intercompany-invoice/adjust-intercompany-invoice-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [AdjustIntercompanyInvoiceComponent],
  providers: [AdjustIntercompanyInvoiceService, AdjustIntercompanyInvoiceCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [AdjustIntercompanyInvoiceComponent]
})
export class AdjustIntercompanyInvoiceComponentModule {}
