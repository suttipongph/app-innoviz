import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IntercompanyInvoiceTableListComponent } from './intercompany-invoice-table-list.component';

describe('IntercompanyInvoiceTableListViewComponent', () => {
  let component: IntercompanyInvoiceTableListComponent;
  let fixture: ComponentFixture<IntercompanyInvoiceTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IntercompanyInvoiceTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntercompanyInvoiceTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
