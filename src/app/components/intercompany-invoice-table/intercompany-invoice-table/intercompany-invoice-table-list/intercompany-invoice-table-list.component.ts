import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { IntercompanyInvoiceTableListView } from 'shared/models/viewModel';
import { IntercompanyInvoiceTableService } from '../intercompany-invoice-table.service';
import { IntercompanyInvoiceTableListCustomComponent } from './intercompany-invoice-table-list-custom.component';

@Component({
  selector: 'intercompany-invoice-table-list',
  templateUrl: './intercompany-invoice-table-list.component.html',
  styleUrls: ['./intercompany-invoice-table-list.component.scss']
})
export class IntercompanyInvoiceTableListComponent extends BaseListComponent<IntercompanyInvoiceTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  productTypeOptions: SelectItems[] = [];
  customerOptions: SelectItems[] = [];
  creditappOption: SelectItems[] = [];
  invoiceRevenueTypeOption: SelectItems[] = [];

  constructor(public custom: IntercompanyInvoiceTableListCustomComponent, private service: IntercompanyInvoiceTableService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCustomerTableDropDown(this.customerOptions);
    this.baseDropdown.getCreditAppTableDropDown(this.creditappOption);
    this.baseDropdown.getInvoiceRevenueTypeDropDown(this.invoiceRevenueTypeOption);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'intercompanyInvoiceTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.INTERCOMPANY_INVOICE_ID',
        textKey: 'intercompanyInvoiceId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerOptions,
        searchingKey: 'customerTableGUID',
        sortingKey: 'CustomerTable_CustomerId'
      },
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: 'LABEL.CREDIT_APPLICATION_ID',
        textKey: 'creditAppTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditappOption,
        searchingKey: 'creditAppTableGUID',
        sortingKey: 'CreditAppTable_CreditAppId'
      },
      {
        label: 'LABEL.DOCUMENT_ID',
        textKey: 'documentId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.ISSUED_DATE',
        textKey: 'issuedDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DUE_DATE',
        textKey: 'dueDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.SERVICE_FEE_TYPE_ID',
        textKey: 'invoiceRevenueType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.invoiceRevenueTypeOption,
        searchingKey: 'invoiceRevenueTypeGUID',
        sortingKey: 'InvoiceRevenueType_InvoiceRevenueTypeId'
      },
      {
        label: 'LABEL.INVOICE_AMOUNT',
        textKey: 'invoiceAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.SETTLE_AMOUNT',
        textKey: 'settleAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BALANCE',
        textKey: 'balance',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<IntercompanyInvoiceTableListView>> {
    return this.service.getIntercompanyInvoiceTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteIntercompanyInvoiceTable(row));
  }
}
