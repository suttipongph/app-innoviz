import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { IntercompanyInvoiceTableItemView } from 'shared/models/viewModel';
import { IntercompanyInvoiceTableService } from '../intercompany-invoice-table.service';
import { IntercompanyInvoiceTableItemCustomComponent } from './intercompany-invoice-table-item-custom.component';
@Component({
  selector: 'intercompany-invoice-table-item',
  templateUrl: './intercompany-invoice-table-item.component.html',
  styleUrls: ['./intercompany-invoice-table-item.component.scss']
})
export class IntercompanyInvoiceTableItemComponent extends BaseItemComponent<IntercompanyInvoiceTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  productTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];

  intercompanyInvoiceMenuVisibility: boolean = false;

  constructor(
    private service: IntercompanyInvoiceTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: IntercompanyInvoiceTableItemCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<IntercompanyInvoiceTableItemView> {
    return this.service.getIntercompanyInvoiceTableById(this.id);
  }
  getInitialData(): Observable<IntercompanyInvoiceTableItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions(result);
        this.setFunctionOptions(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setRelatedInfoOptions(model);
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateIntercompanyInvoiceTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createIntercompanyInvoiceTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  setRelatedInfoOptions(model: IntercompanyInvoiceTableItemView): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.SETTLEMENT',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.INTERCOMPANY_INVOICE_SETTLEMENT,
            parameters: {
              AllowCreate: false,
              AllowUpdate: false,
              AllowDelete: false,
              balance: this.model.balance
            }
          }),
        disabled: false,
        visible: true
      },

      {
        label: 'LABEL.ADJUSTMENT_HISTORY',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.INTERCOMPANY_INVOICE_ADJUSTMENT,
            parameters: {
              AllowCreate: false,
              AllowUpdate: false,
              AllowDelete: false
            }
          }),
        disabled: false,
        visible: true
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(model: IntercompanyInvoiceTableItemView): void {
    this.functionItems = [
      {
        label: 'LABEL.TRANSACTION_ADJUSTMENT',
        command: () => this.setPreparingFunctionDataAndValidation(ROUTE_FUNCTION_GEN.TRANSACTION_ADJUSTMENT)
      }
    ];
    super.setFunctionOptionsMapping();
  }
  setPreparingFunctionDataAndValidation(path: string): void {
    this.custom.setPreparingFunctionDataAndValidation(this.model).subscribe((res) => {
      if (res) {
        this.toFunction({
          path: path
        });
      }
    });
  }
}
