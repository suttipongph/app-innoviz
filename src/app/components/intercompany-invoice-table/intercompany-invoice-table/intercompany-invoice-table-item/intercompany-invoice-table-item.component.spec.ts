import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntercompanyInvoiceTableItemComponent } from './intercompany-invoice-table-item.component';

describe('IntercompanyInvoiceTableItemViewComponent', () => {
  let component: IntercompanyInvoiceTableItemComponent;
  let fixture: ComponentFixture<IntercompanyInvoiceTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IntercompanyInvoiceTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntercompanyInvoiceTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
