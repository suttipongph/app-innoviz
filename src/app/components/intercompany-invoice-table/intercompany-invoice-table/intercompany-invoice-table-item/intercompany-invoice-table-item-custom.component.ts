import { IntercompanyInvoiceTableService } from './../intercompany-invoice-table.service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { EmptyGuid, IntercompanyStatus, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { IntercompanyInvoiceTableItemView, IntercompanyInvoiceAdjustmentItemView } from 'shared/models/viewModel';

const firstGroup = [
  'INTERCOMPANY_INVOICE_ID',
  'CUSTOMER_TABLE_GUID',
  'CUSTOMER_NAME',
  'INVOICE_TYPE_GUID',
  'DOCUMENT_STATUS_GUID',
  'INTERCOMPANY_GUID',
  'PRODUCT_TYPE',
  'CREDIT_APP_TABLE_GUID',
  'ISSUED_DATE',
  'DUE_DATE',
  'DOCUMENT_ID',
  'CURRENCY_GUID',
  'EXCHANGE_RATE',
  'INVOICE_ADDRESS',
  'TAX_BRANCH_ID',
  'TAX_BRANCH_NAME',
  'INVOICE_REVENUE_TYPE_GUID',
  'INVOICE_TEXT',
  'TAX_TABLE_GUID',
  'WITHHOLDING_TAX_TABLE_GUID',
  'INVOICE_AMOUNT',
  'SETTLE_AMOUNT',
  'BALANCE',
  'CN_REASON_GUID',
  'ORIG_INVOICE_ID',
  'ORIG_INVOICE_AMOUNT',
  'ORIG_TAX_INVOICE_ID',
  'ORIG_TAX_INVOICE_AMOUNT',
  'DIMENSION1GUID',
  'DIMENSION2GUID',
  'DIMENSION3GUID',
  'DIMENSION4GUID',
  'DIMENSION5GUID',
  'REF_TYPE',
  'REF_ID',
  'REF_GUID',
  'INTERCOMPANY_INVOICE_TABLE_GUID'
];

export class IntercompanyInvoiceTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: IntercompanyInvoiceTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: IntercompanyInvoiceTableItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(): Observable<IntercompanyInvoiceTableItemView> {
    let model = new IntercompanyInvoiceTableItemView();
    model.intercompanyInvoiceTableGUID = EmptyGuid;
    return of(model);
  }
  setPreparingFunctionDataAndValidation(model: IntercompanyInvoiceTableItemView): Observable<boolean> {
    return this.baseService.service.getValidateAdjustFunction(model.intercompanyInvoiceTableGUID).pipe(
      map((res) => {
        if (res) {
          return { IntercompanyInvoiceTableItemView: res };
        } else {
          return null;
        }
      })
    );
  }
}
