import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PurchaseTableItemView, PurchaseTableListView, RetentionConditionTransItemView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class PurchaseTableService {
  serviceKey = 'purchaseTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getPurchaseTableToList(search: SearchParameter): Observable<SearchResult<PurchaseTableListView>> {
    const url = `${this.servicePath}/GetPurchaseTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deletePurchaseTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeletePurchaseTable`;
    return this.dataGateway.delete(url, row);
  }
  getPurchaseTableById(id: string): Observable<PurchaseTableItemView> {
    const url = `${this.servicePath}/GetPurchaseTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createPurchaseTable(vmModel: PurchaseTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreatePurchaseTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updatePurchaseTable(vmModel: PurchaseTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdatePurchaseTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  validateIsManualNumberSeq(companyId: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeqPurchaseTable/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
  getInitialData(id: string): Observable<PurchaseTableItemView> {
    const url = `${this.servicePath}/GetPurchaseTableInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }

  isPurchaseLineByPurchaseTableEmpty(id: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsPurchaseLineByPurchaseTableEmpty/id=${id}`;
    return this.dataGateway.get(url);
  }
  getRetentionConditionTransByPurchase(id: string): Observable<RetentionConditionTransItemView> {
    const url = `${this.servicePath}/GetRetentionConditionTransByPurchase/id=${id}`;
    return this.dataGateway.get(url);
  }
}
