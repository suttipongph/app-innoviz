import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PurchaseTableService } from './purchase-table.service';
import { PurchaseTableListComponent } from './purchase-table-list/purchase-table-list.component';
import { PurchaseTableItemComponent } from './purchase-table-item/purchase-table-item.component';
import { PurchaseTableItemCustomComponent } from './purchase-table-item/purchase-table-item-custom.component';
import { PurchaseTableListCustomComponent } from './purchase-table-list/purchase-table-list-custom.component';
import { PurchaseLineComponentModule } from 'components/purchase/purchase-line/purchase-line.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, PurchaseLineComponentModule],
  declarations: [PurchaseTableListComponent, PurchaseTableItemComponent],
  providers: [PurchaseTableService, PurchaseTableItemCustomComponent, PurchaseTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PurchaseTableListComponent, PurchaseTableItemComponent]
})
export class PurchaseTableComponentModule {}
