import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { EmptyGuid, PurchaseStatus, RetentionCalculateBase } from 'shared/constants';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, PathParamModel } from 'shared/models/systemModel';
import { AccessModeView, PurchaseTableItemView, RetentionConditionTransItemView } from 'shared/models/viewModel';

const firstGroup = [
  'PRODUCT_TYPE',
  'CREDIT_APP_TABLE_GUID',
  'CUSTOMER_TABLE_GUID',
  'TOTAL_INTEREST_PCT',
  'NET_PAID',
  'DOCUMENT_STATUS_GUID',
  'ROLLBILL',
  'RECEIPT_TEMP_TABLE_GUID',
  'RETENTION_CALCULATE_BASE',
  'RETENTION_PCT',
  'SUM_PURCHASE_AMOUNT',
  'OVER_CREDIT_BUYER',
  'OVER_CREDIT_CA',
  'OVER_CREDIT_CA_LINE',
  'DIFF_CHEQUE_ISSUED_NAME',
  'ADDITIONAL_PURCHASE',
  'DOCUMENT_REASON_GUID'
];
const secoundGroup = ['PURCHASE_ID'];
const thirdGroup = ['PURCHASE_DATE'];
const PURCHASE_TABLE_PURCHASE = 'purchasetablepurchase';

export class PurchaseTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  isManual = false;
  isNumbersSeqNotSet = false;
  title: string = null;
  accessModeView: AccessModeView = new AccessModeView();
  getFieldAccessing(model: PurchaseTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });

    if (!isUpdateMode(model.purchaseTableGUID)) {
      return this.baseService.service.validateIsManualNumberSeq(model.companyGUID).pipe(
        map((result: boolean) => {
          this.isManual = result;
          fieldAccessing.push({ filedIds: secoundGroup, readonly: !this.isManual });
          return fieldAccessing;
        }),
        catchError((err) => {
          this.baseService.notificationService.showErrorMessageFromResponse(err);
          this.isNumbersSeqNotSet = true;
          return fieldAccessing;
        })
      );
    } else {
      fieldAccessing.push({ filedIds: secoundGroup, readonly: true });
      return this.baseService.service.isPurchaseLineByPurchaseTableEmpty(model.purchaseTableGUID).pipe(
        map((result: boolean) => {
          fieldAccessing.push({ filedIds: thirdGroup, readonly: !result });
          return fieldAccessing;
        }),
        catchError((err) => {
          this.baseService.notificationService.showErrorMessageFromResponse(err);
          return fieldAccessing;
        })
      );
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: PurchaseTableItemView, isWorkflowMode: boolean): Observable<boolean> {
    return this.getAccessModeByParent(model, isWorkflowMode).pipe(
      map((result) => {
        const accessright = isNullOrUndefOrEmptyGUID(model.purchaseTableGUID) ? canCreate : canUpdate;
        const accessLogic = !this.isNumbersSeqNotSet && (isNullOrUndefOrEmptyGUID(model.purchaseTableGUID) ? result.canCreate : result.canView);
        return !(accessLogic && accessright);
      })
    );
  }
  getInitialData(): Observable<PurchaseTableItemView> {
    const model = new PurchaseTableItemView();
    model.purchaseTableGUID = EmptyGuid;
    return of(model);
  }
  getAccessModeByParent(model: PurchaseTableItemView, isWorkflowMode: boolean): Observable<AccessModeView> {
    const accessMode = new AccessModeView();
    const condition1 = model.documentStatus_StatusId < PurchaseStatus.WaitingForOperationStaff;
    const condition2 = model.documentStatus_StatusId === PurchaseStatus.WaitingForOperationStaff && isWorkflowMode;
    accessMode.canView = condition1 || condition2;
    accessMode.canCreate = condition1 || condition2;
    return of(accessMode);
  }

  setRollBill(model: PurchaseTableItemView) {
    const activateName = this.baseService.uiService.getRelatedInfoActiveTableName();
    if (activateName === PURCHASE_TABLE_PURCHASE) {
      model.rollbill = false;
    } else {
      model.rollbill = true;
      model.retentionFixedAmount = 0;
    }
  }

  setModelByRetentionCalculateBase(model: PurchaseTableItemView) {
    if (!isNullOrUndefined(model.retentionCalculateBase)) {
      if (model.retentionCalculateBase === RetentionCalculateBase.FixedAmount) {
        this.baseService.service.getRetentionConditionTransByPurchase((result) => {
          const item: RetentionConditionTransItemView = result;
          model.retentionFixedAmount = item.retentionAmount;
        });
      }
    }
  }
  setTitle() {
    const parentName = this.baseService.uiService.getRelatedInfoActiveTableName();
    if (parentName === 'purchasetablerollbill') {
      this.title = 'LABEL.ROLL_BILL';
    } else {
      this.title = 'LABEL.PURCHASE';
    }
  }
  getWorkFlowPath(model: PurchaseTableItemView): PathParamModel {
    const ACTION_WORKFLOW = 'actionworkflow';
    const START_WORKFLOW = 'startworkflow';
    return model.documentStatus_StatusId === PurchaseStatus.Draft ? { path: START_WORKFLOW } : { path: ACTION_WORKFLOW };
  }
  getDisabledWorkFlow(model: PurchaseTableItemView): boolean {
    return !(model.documentStatus_StatusId >= PurchaseStatus.Draft && model.documentStatus_StatusId <= PurchaseStatus.WaitingForRetry);
  }
  getPrintPurchase(model: PurchaseTableItemView): boolean {
    if (model.rollbill === false) {
      return false;
    } else {
      return true;
    }
  }
  getPrintPurchaseVisible(model: PurchaseTableItemView): boolean {
    if (model.rollbill === false) {
      return true;
    } else {
      return false;
    }
  }
  getPrintRollbillPurchase(model: PurchaseTableItemView): boolean {
    if (model.rollbill === true) {
      return true;
    } else {
      return false;
    }
  }
  getPrintRollbillPurchaseDisabled(model: PurchaseTableItemView): boolean {
    if (model.rollbill === true) {
      return false;
    } else {
      return true;
    }
  }
}
