import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseTableItemComponent } from './purchase-table-item.component';

describe('PurchaseTableItemViewComponent', () => {
  let component: PurchaseTableItemComponent;
  let fixture: ComponentFixture<PurchaseTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PurchaseTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
