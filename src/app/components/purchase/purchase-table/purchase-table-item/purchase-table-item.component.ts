import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import {
  Dimension,
  EmptyGuid,
  PurchaseStatus,
  ReceivedFrom,
  RefType,
  ROUTE_FUNCTION_GEN,
  ROUTE_RELATED_GEN,
  ROUTE_REPORT_GEN,
  SuspenseInvoiceType
} from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { MenuItem } from 'shared/models/primeModel';
import { FormValidationModel, IvzDropdownOnFocusModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, LedgerDimensionItemView, PurchaseTableItemView } from 'shared/models/viewModel';
import { PurchaseTableService } from '../purchase-table.service';
import { PurchaseTableItemCustomComponent } from './purchase-table-item-custom.component';
@Component({
  selector: 'purchase-table-item',
  templateUrl: './purchase-table-item.component.html',
  styleUrls: ['./purchase-table-item.component.scss']
})
export class PurchaseTableItemComponent extends BaseItemComponent<PurchaseTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  documentStatusOptions: SelectItems[] = [];
  ledgerDimensionOptions1: SelectItems[] = [];
  ledgerDimensionOptions2: SelectItems[] = [];
  ledgerDimensionOptions3: SelectItems[] = [];
  ledgerDimensionOptions4: SelectItems[] = [];
  ledgerDimensionOptions5: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  employeeActiveOption: SelectItems[] = [];
  retentionCalculateBaseOptions: SelectItems[] = [];
  accessMode: AccessModeView = new AccessModeView();

  constructor(private service: PurchaseTableService, private currentActivatedRoute: ActivatedRoute, public custom: PurchaseTableItemCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {
    this.custom.setTitle();
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRetentionCalculateBaseEnumDropDown(this.retentionCalculateBaseOptions);
  }
  getById(): Observable<PurchaseTableItemView> {
    return this.service.getPurchaseTableById(this.id);
  }
  getInitialData(): Observable<PurchaseTableItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions(result);
        this.setFunctionOptions(result);
        this.setWorkflowOption(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.custom.setRollBill(this.model);
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: PurchaseTableItemView): void {
    forkJoin([
      this.baseDropdown.getDocumentStatusDropDown(),
      this.baseDropdown.getLedgerDimensionDropDown(),
      this.baseDropdown.getEmployeeFilterActiveStatusDropdown(model?.operReportSignatureGUID)
    ]).subscribe(
      ([documentStatus, ledgerDimension, employeeActiveOption]) => {
        this.documentStatusOptions = documentStatus;
        this.employeeActiveOption = employeeActiveOption;
        this.setLedgerDimension(ledgerDimension);
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model, this.isWorkFlowMode()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  setRelatedInfoOptions(model: PurchaseTableItemView): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: {
              refGUID: model.purchaseTableGUID,
              refType: RefType.PurchaseTable
            }
          })
      },
      {
        label: 'LABEL.MEMO',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.MEMO,
            parameters: {
              refGUID: model.purchaseTableGUID,
              productType: model.productType
            }
          })
      },
      {
        label: 'LABEL.PDC',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.PDC,
            parameters: {
              refId: model.purchaseId,
              documentStatus_StatusId: model.documentStatus_StatusId
            }
          })
      },
      {
        label: 'LABEL.SERVICE_FEE',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.SERVICE_FEE_TRANS,
            parameters: {
              refId: model.purchaseId,
              taxDate: model.purchaseDate,
              productType: model.productType,
              invoiceTable_CustomerTableGUID: model.customerTableGUID
            }
          })
      },
      {
        label: 'LABEL.INVOICE_SETTLEMENT',
        visible: !model.rollbill,
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.INVOICE_SETTLEMENT,
            parameters: {
              refGUID: model.purchaseTableGUID,
              refType: RefType.PurchaseTable,
              refId: model.purchaseId,
              invoiceTable_CustomerTableGUID: model.customerTableGUID,
              suspenseInvoiceType: SuspenseInvoiceType.None,
              productInvoice: false,
              productType: null,
              receivedFrom: ReceivedFrom.Customer
            }
          })
      },
      {
        label: 'LABEL.SUSPENSE_SETTLEMENT',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.SUSPENSE_SETTLEMENT,
            parameters: {
              refGUID: model.purchaseTableGUID,
              refType: RefType.PurchaseTable,
              refId: model.purchaseId,
              invoiceTable_CustomerTableGUID: model.customerTableGUID,
              suspenseInvoiceType: SuspenseInvoiceType.SuspenseAccount,
              productInvoice: false,
              productType: null,
              receivedFrom: ReceivedFrom.Customer
            }
          })
      },
      //   { label: 'LABEL.PAYMENT_DETAIL', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.PAYMENT_DETAIL }) },
      {
        label: 'LABEL.PAYMENT_DETAIL',
        visible: !model.rollbill,
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.PAYMENT_DETAIL,
            parameters: {
              refId: model.purchaseId,
              totalPaymentAmount: model.netPaid,
              customerTableGUID: model.customerTableGUID
            }
          })
      },
      {
        label: 'LABEL.INQUIRY',
        items: [
          {
            label: 'LABEL.VENDOR_PAYMENT',
            visible: !model.rollbill,
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.VENDOR_PAYMENT_TRANS,
                parameters: {
                  refId: model.purchaseId,
                  refGUID: model.purchaseTableGUID,
                  refType: RefType.PurchaseTable,
                  tableLabel: this.custom.title
                }
              })
          },
          {
            label: 'LABEL.COLLECTION',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.RECEIPT_TEMP_TABLE,
                parameters: {
                  refGUID: model.receiptTempTableGUID,
                  refType: RefType.PurchaseTable
                }
              })
          }
        ]
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(model: PurchaseTableItemView): void {
    const printpurchase = this.custom.getPrintPurchase(model);
    const printpurchasevisible = this.custom.getPrintPurchaseVisible(model);
    const printrollbillpurchase = this.custom.getPrintRollbillPurchase(model);
    const printrollbillpurchasedisabled = this.custom.getPrintRollbillPurchaseDisabled(model);
    this.functionItems = [
      {
        label: 'LABEL.POST',
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.POST_PURCHASE }),
        disabled: model.documentStatus_StatusId !== PurchaseStatus.Approved
      },
      {
        label: 'LABEL.CANCEL',
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.CANCEL_PURCHASE }),
        disabled: model.documentStatus_StatusId !== PurchaseStatus.Draft && model.documentStatus_StatusId !== PurchaseStatus.Approved
      },
      {
        label: 'LABEL.SEND_PURCHASE_EMAIL',
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.PURCHASE_TABLE_SEND_EMAIL }),
        disabled: model.documentStatus_StatusId !== PurchaseStatus.Posted
      },
      {
        label: 'LABEL.UPDATE_STATUS',
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.UPDATE_STATUS_ROLLBILL_PURCHASE }),
        visible: model.rollbill == true,
        disabled: model.documentStatus_StatusId !== PurchaseStatus.Draft || model.rollbill == false
      },
      {
        label: 'LABEL.PRINT',
        items: [
          {
            label: 'LABEL.PURCHASE',
            command: () =>
              this.toReport({
                path: ROUTE_REPORT_GEN.PRINT_PURCHASE
              }),
            disabled: printpurchase,
            visible: printpurchasevisible
          },
          {
            label: 'LABEL.ROLL_BILL_PURCHASE',
            command: () =>
              this.toReport({
                path: ROUTE_REPORT_GEN.PRINT_PURCHASE_ROLLBILL
              }),
            disabled: printrollbillpurchasedisabled,
            visible: printrollbillpurchase
          }
        ]
      }
    ];
    const enableFunctionsWFMode: MenuItem[] = [{ label: 'LABEL.PRINT', items: [{ label: 'LABEL.PURCHASE' }] }];
    super.setFunctionOptionsMapping(enableFunctionsWFMode);
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updatePurchaseTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createPurchaseTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  setLedgerDimension(ledgerDimension: SelectItems[]): void {
    this.ledgerDimensionOptions1 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension1);
    this.ledgerDimensionOptions2 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension2);
    this.ledgerDimensionOptions3 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension3);
    this.ledgerDimensionOptions4 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension4);
    this.ledgerDimensionOptions5 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension5);
  }

  //#region WF
  setWorkflowOption(model: PurchaseTableItemView): void {
    super.setK2WorkflowOption(model, this.custom.getWorkFlowPath(model), this.custom.getDisabledWorkFlow(model));
  }
  //#endregion WF

  onEmployeeTableDropdownFocus(e: IvzDropdownOnFocusModel): void {
    super.setDropdownOptionOnFocus(e, this.model, this.baseDropdown.getEmployeeFilterActiveStatusDropdown()).subscribe((result) => {
      this.employeeActiveOption = result;
    });
  }
}
