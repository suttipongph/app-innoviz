import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkFlowPurchaseComponent } from './work-flow-purchase.component';

describe('WorkFlowPurchaseTableViewComponent', () => {
  let component: WorkFlowPurchaseComponent;
  let fixture: ComponentFixture<WorkFlowPurchaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WorkFlowPurchaseComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkFlowPurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
