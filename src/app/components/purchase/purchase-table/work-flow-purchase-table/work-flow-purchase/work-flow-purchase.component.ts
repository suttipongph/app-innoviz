import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseWorkflowComponent } from 'core/components/base-workflow/base-workflow.component';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { WorkFlowPurchaseService } from '../work-flow-purchase.service';
import { WorkFlowPurchaseCustomComponent } from './work-flow-purchase-custom.component';
import { WorkflowPurchaseView } from 'shared/models/viewModel';
import { getPurchaseWorkflowName } from 'shared/config/globalvar.config';
import { RefType } from 'shared/constants';

@Component({
  selector: 'work-flow-purchase',
  templateUrl: './work-flow-purchase.component.html',
  styleUrls: ['./work-flow-purchase.component.scss']
})
export class WorkFlowPurchaseComponent extends BaseWorkflowComponent<WorkflowPurchaseView> {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  actionNameOptions: SelectItems[] = [];
  documentReasonOptions: SelectItems[] = [];

  isStartWorkflow: boolean;
  constructor(
    private service: WorkFlowPurchaseService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: WorkFlowPurchaseCustomComponent,
    public uiService: UIControllerService
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.workflowName = getPurchaseWorkflowName();
    this.isStartWorkflow = uiService.getWorkflowActiveTableName() === 'startworkflow';
    this.custom.parentId = this.id;
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    if (!this.isStartWorkflow) {
      super.workflowGetTaskBySerialNumber().subscribe(
        (result) => {
          this.custom.setActivityName(result);
          this.checkAccessMode();
          this.checkPageMode();
        },
        (error) => {
          throw error;
        }
      );
    } else {
      this.checkAccessMode();
      this.checkPageMode();
    }
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<WorkflowPurchaseView> {
    return this.custom.getWorkFlowPurchaseTableById();
  }
  getInitialData(): Observable<WorkflowPurchaseView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin([this.baseDropdown.getDocumentReasonDropDown(RefType.PurchaseTable.toString())]).subscribe(
      ([documentReason]) => {
        this.documentReasonOptions = documentReason;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {}
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true, this.model);
      }
    });
  }
  onSubmit(isColsing: boolean, model: WorkflowPurchaseView): void {
    const param = this.custom.setParam(this.model, this.workflowName, this.comment);
    if (this.isStartWorkflow) {
      model.workflowInstance = super.setStartWorkflowParm(param);
      super.onExecuteFunction(this.service.startWorkflow(model));
    } else {
      model.workflowInstance = super.setActionWorkflowParm(param);
      super.onExecuteFunction(this.service.actionWorkflow(model));
    }
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onActionChange(): any {
    this.model.documentReasonGUID = null;
    super.setBaseFieldAccessing(this.custom.setReason(this.workflow.action));
  }
}
