import { Observable, of } from 'rxjs';
import { EmptyGuid, PurchaseStatus } from 'shared/constants';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { TaskItemView, WorkflowPurchaseView } from 'shared/models/viewModel';
import { WorkFlowPurchaseService } from '../work-flow-purchase.service';

const firstGroup = ['PURCHASE_ID', 'ROLLBILL', 'CREDIT_APPLICATION_ID', 'CUSTOMER_ID'];
export class WorkFlowPurchaseCustomComponent {
  baseService: BaseServiceModel<any>;
  showActionGroup: boolean = false;
  activityName: string = 'Submit workflow';
  requireAssistMD: boolean = false;
  parentName: string = null;
  parentId: string = null;
  activeName: string = null;
  isReasonMandatory = false;
  getFieldAccessing(model: WorkflowPurchaseView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    if (model.actionName == 'Cancel') {
      this.isReasonMandatory = true;
      fieldAccessing.push({ filedIds: ['DOCUMENT_REASON_GUID'], readonly: false });
    } else {
      this.isReasonMandatory = false;
      fieldAccessing.push({ filedIds: ['DOCUMENT_REASON_GUID'], readonly: true });
    }
    this.setFieldGroup(model);
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(getCanAction: boolean, model: WorkflowPurchaseView): Observable<boolean> {
    return of(!getCanAction);
  }
  getInitialData(): Observable<WorkflowPurchaseView> {
    let model = new WorkflowPurchaseView();
    model.purchaseTableGUID = EmptyGuid;
    return of(model);
  }
  setFieldGroup(model: WorkflowPurchaseView): void {
    this.showActionGroup = model.documentStatus_StatusId > PurchaseStatus.Draft;
  }
  setActivityName(taskItem: TaskItemView): void {
    this.activityName = taskItem.actInstDestDisplayName;
  }
  setParam(model: WorkflowPurchaseView, workflowName: string, comment: string): object {
    let ob = new Object();
    ob['ParmDocGUID'] = model.purchaseTableGUID;
    if (model.documentStatus_StatusId === PurchaseStatus.Draft) {
      let i = 0;
      ['Purchase', 'Submitted by', 'Customer', 'Purchase date'].forEach((item) => {
        model.parmFolio = model.parmFolio.replace(`{{${i}}}`, item);
        i++;
      });

      ob['ParmFolio'] = model.parmFolio;
      ob['ProcName'] = workflowName;
      ob['ParmCompany'] = model.companyGUID;
      ob['ParmDocGUID'] = model.purchaseTableGUID;
      ob['ParmDocID'] = model.parmDocID;
      ob['ParmRollbill'] = model.parmRollbill;
      ob['ParmSumPurchaseAmount'] = model.parmSumPurchaseAmount;
      ob['ParmOverCreditBuyer'] = model.parmOverCreditBuyer;
      ob['ParmOverCreditCA'] = model.parmOverCreditCA;
      ob['ParmOverCreditCALine'] = model.parmOverCreditCALine;
      ob['ParmDiffChequeIssueName'] = model.parmDiffChequeIssuedName;
      ob['ParmCreditAppTableGUID'] = model.parmCreditAppTableGUID;
      ob['ParmProductType'] = model.parmProductType;
      ob['ParmDocumentReasonGUID'] = model.documentReasonGUID;
    } else if (
      model.documentStatus_StatusId >= PurchaseStatus.WaitingForOperationStaff &&
      model.documentStatus_StatusId <= PurchaseStatus.WaitingForRetry
    ) {
      ob['ParmDocumentReasonGUID'] = model.documentReasonGUID;
      ob['ActionName'] = model.actionName;
      ob['Comment'] = comment;
      ob['ProcessInstanceId'] = model.processInstanceId;
    }
    return ob;
  }
  getWorkFlowPurchaseTableById(): Observable<any> {
    return (this.baseService.service as WorkFlowPurchaseService).getWorkFlowPurchaseById(this.parentId);
  }

  setReason(action: string): FieldAccessing[] {
    if (action == 'Cancel') {
      this.isReasonMandatory = true;
      return [{ filedIds: ['DOCUMENT_REASON_GUID'], readonly: false }];
    } else {
      this.isReasonMandatory = false;
      return [{ filedIds: ['DOCUMENT_REASON_GUID'], readonly: true }];
    }
  }
}
