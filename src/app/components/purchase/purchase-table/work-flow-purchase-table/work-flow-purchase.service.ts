import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { Observable } from 'rxjs';
@Injectable()
export class WorkFlowPurchaseService {
  serviceKey = 'workFlowPurchaseGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getWorkFlowPurchaseToList(search: SearchParameter): Observable<SearchResult<any>> {
    const url = `${this.servicePath}/GetWorkFlowPurchaseList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteWorkFlowPurchase(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteWorkFlowPurchase`;
    return this.dataGateway.delete(url, row);
  }
  getWorkFlowPurchaseById(id: string): Observable<any> {
    const url = `${this.servicePath}/GetWorkFlowPurchaseById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createWorkFlowPurchase(vmModel: any): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateWorkFlowPurchase`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateWorkFlowPurchase(vmModel: any): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateWorkFlowPurchase`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  startWorkflow(vmModel: any): Observable<any> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vmModel);
  }
  actionWorkflow(vmModel: any): Observable<any> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vmModel);
  }
}
