import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'shared/shared.module';
import { WorkFlowPurchaseService } from './work-flow-purchase.service';
import { WorkFlowPurchaseComponent } from './work-flow-purchase/work-flow-purchase.component';
import { WorkFlowPurchaseCustomComponent } from './work-flow-purchase/work-flow-purchase-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [WorkFlowPurchaseComponent],
  providers: [WorkFlowPurchaseService, WorkFlowPurchaseCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [WorkFlowPurchaseComponent]
})
export class WorkFlowPurchaseComponentModule {}
