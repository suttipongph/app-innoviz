import { Observable } from 'rxjs';
import { BaseServiceModel } from 'shared/models/systemModel';

export class PurchaseTableListCustomComponent {
  baseService: BaseServiceModel<any>;
  title: string = null;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setTitle() {
    let parentName = this.baseService.uiService.getRelatedInfoActiveTableName();
    if (parentName == 'purchasetablerollbill') {
      this.title = 'LABEL.ROLL_BILL';
    } else {
      this.title = 'LABEL.PURCHASE';
    }
  }
}
