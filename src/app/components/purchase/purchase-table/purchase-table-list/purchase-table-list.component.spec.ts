import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PurchaseTableListComponent } from './purchase-table-list.component';

describe('PurchaseTableListViewComponent', () => {
  let component: PurchaseTableListComponent;
  let fixture: ComponentFixture<PurchaseTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PurchaseTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
