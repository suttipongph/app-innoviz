import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { PurchaseTableListView } from 'shared/models/viewModel';
import { PurchaseTableService } from '../purchase-table.service';
import { PurchaseTableListCustomComponent } from './purchase-table-list-custom.component';

@Component({
  selector: 'purchase-table-list',
  templateUrl: './purchase-table-list.component.html',
  styleUrls: ['./purchase-table-list.component.scss']
})
export class PurchaseTableListComponent extends BaseListComponent<PurchaseTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  documentStatusOptions: SelectItems[] = [];
  ledgerDimensionOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  retentionCalculateBaseOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];

  constructor(public custom: PurchaseTableListCustomComponent, private service: PurchaseTableService, private currentActivatedRoute: ActivatedRoute) {
    super();
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {
    this.custom.setTitle();
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRetentionCalculateBaseEnumDropDown(this.retentionCalculateBaseOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'purchaseTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.PURCHASE_ID',
        textKey: 'purchaseId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC,
        searchingKey: 'purchaseId',
        sortingKey: 'purchaseId'
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId'
      },
      {
        label: 'LABEL.PURCHASE_DATE',
        textKey: 'purchaseDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'documentStatusGUID',
        sortingKey: 'documentStatus_StatusId',
        masterList: this.documentStatusOptions
      },
      {
        label: null,
        textKey: 'creditAppTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<PurchaseTableListView>> {
    return this.service.getPurchaseTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deletePurchaseTable(row));
  }
}
