import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PurchaseLineService } from './purchase-line.service';
import { PurchaseLineListComponent } from './purchase-line-list/purchase-line-list.component';
import { PurchaseLineItemComponent } from './purchase-line-item/purchase-line-item.component';
import { PurchaseLineItemCustomComponent } from './purchase-line-item/purchase-line-item-custom.component';
import { PurchaseLineListCustomComponent } from './purchase-line-list/purchase-line-list-custom.component';
import { ListPurchaseLineOutstandingComponentModule } from 'components/list-purchase-line-outstanding/list-purchase-line-outstanding.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, ListPurchaseLineOutstandingComponentModule],
  declarations: [PurchaseLineListComponent, PurchaseLineItemComponent],
  providers: [PurchaseLineService, PurchaseLineItemCustomComponent, PurchaseLineListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PurchaseLineListComponent, PurchaseLineItemComponent]
})
export class PurchaseLineComponentModule {}
