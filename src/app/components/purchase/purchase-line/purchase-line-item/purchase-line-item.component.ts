import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { Dimension, PurchaseStatus, RefType, ROUTE_FUNCTION_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, IvzDropdownOnFocusModel, PageInformationModel, RowIdentity, SelectItems } from 'shared/models/systemModel';
import { LedgerDimensionItemView, PurchaseLineItemView } from 'shared/models/viewModel';
import { PurchaseLineRollBillInitialDataParm } from 'shared/models/viewModel/purchaseLineRollBillInitialDataParm';
import { PurchaseLineService } from '../purchase-line.service';
import { PurchaseLineItemCustomComponent } from './purchase-line-item-custom.component';
@Component({
  selector: 'purchase-line-item',
  templateUrl: './purchase-line-item.component.html',
  styleUrls: ['./purchase-line-item.component.scss']
})
export class PurchaseLineItemComponent extends BaseItemComponent<PurchaseLineItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  assignmentAgreementTableOptions: SelectItems[] = [];
  buyerAgreementOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  chequeTableCustomerOptions: SelectItems[] = [];
  chequeTableBuyerOptions: SelectItems[] = [];
  ledgerDimensionOptions1: SelectItems[] = [];
  ledgerDimensionOptions2: SelectItems[] = [];
  ledgerDimensionOptions3: SelectItems[] = [];
  ledgerDimensionOptions4: SelectItems[] = [];
  ledgerDimensionOptions5: SelectItems[] = [];
  purchaseFeeCalculateBaseOptions: SelectItems[] = [];
  purchaseLineOptions: SelectItems[] = [];
  buyerInvoiceTableOptions: SelectItems[] = [];
  methodOfPaymentOptions: SelectItems[] = [];
  retentionCalculateBaseOptions: SelectItems[] = [];

  //#region  rollBill
  outStandingPageInfo: PageInformationModel = null;
  isShowOutstandingList = false;
  isShowItemView = false;
  //#endregion

  constructor(private service: PurchaseLineService, private currentActivatedRoute: ActivatedRoute, public custom: PurchaseLineItemCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiService.getRelatedInfoActiveTableKey();
    this.custom.isRollBill = this.uiService.getRelatedInfoActiveTableName() == 'purchasetablerollbill';
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {
    this.baseDropdown.getPurchaseFeeCalculateBaseEnumDropDown(this.purchaseFeeCalculateBaseOptions);
    this.baseDropdown.getRetentionCalculateBaseEnumDropDown(this.retentionCalculateBaseOptions);
  }
  getById(): Observable<PurchaseLineItemView> {
    return this.service.getPurchaseLineById(this.id);
  }
  getInitialData(): Observable<PurchaseLineItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions(result);
        this.setFunctionOptions(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    let tempModel: PurchaseLineItemView = this.isUpdateMode ? model : this.model;
    let purchaseTableGUID = tempModel.purchaseTableGUID;
    let customerTableGUID = tempModel.purchaseTable_CustomerTableGUID;
    let creditAppTableGUID = tempModel.purchaseTable_CreditAppTableGUID;
    let purchaseDate = tempModel.purchaseTable_PurchaseDate;
    let creditAppLineGUID = tempModel.creditAppLineGUID;
    let buyerTableGUID = tempModel.buyerTableGUID;
    forkJoin([
      this.custom.getBuyerAgreementDropDown(tempModel),
      this.baseDropdown.getBuyerTableByPurchaseLineDropDown([creditAppTableGUID, purchaseDate]),
      this.baseDropdown.getChequeTableByPurchaseLineCustomerDropDown([purchaseTableGUID, customerTableGUID], tempModel.customerPDCTableGUID),
      this.baseDropdown.getChequeTableByPurchaseLineBuyerDropDown([purchaseTableGUID, buyerTableGUID], tempModel.buyerPDCTableGUID),
      this.baseDropdown.getLedgerDimensionDropDown(),
      this.baseDropdown.getPurchaseLineDropDown(),
      this.custom.getBuyerInvoiceTableByCreditAppLineDropDown(creditAppLineGUID),
      this.custom.getAssignmentAgreementTableDropDown(tempModel),
      this.baseDropdown.getMethodOfPaymentDropDown()
    ]).subscribe(
      ([
        buyerAgreementTable,
        buyerTable,
        chequeTableCustomer,
        chequeTableBuyer,
        ledgerDimension,
        purchaseLine,
        buyerInvoiceTable,
        assignmentAgreementTable,
        methodOfPayment
      ]) => {
        this.buyerAgreementOptions = buyerAgreementTable;
        this.buyerTableOptions = buyerTable;
        this.chequeTableCustomerOptions = chequeTableCustomer;
        this.chequeTableBuyerOptions = chequeTableBuyer;
        this.purchaseLineOptions = purchaseLine;
        this.buyerInvoiceTableOptions = buyerInvoiceTable;
        this.assignmentAgreementTableOptions = assignmentAgreementTable;
        this.methodOfPaymentOptions = methodOfPayment;
        this.setLedgerDimension(ledgerDimension);
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }

        // rollbill
        if (this.custom.isRollBill && !this.isUpdateMode) {
          this.outStandingPageInfo = this.childPaths[0];
          this.isShowOutstandingList = true;
        } else {
          this.setFieldAccessing();
        }
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model, this.isWorkFlowMode()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  setRelatedInfoOptions(model: PurchaseLineItemView): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.VERIFICATION',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.VERIFICATION_TRANS,
            parameters: {
              refId: model.lineNum,
              buyerTableGUID: model.buyerTableGUID,
              creditAppTableGUID: model.purchaseTable_CreditAppTableGUID
            }
          })
      },
      {
        label: 'LABEL.INQUIRY',
        items: [
          { label: 'LABEL.INVOICE', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.INVOICE_TABLE }), disabled: false },
          {
            label: 'LABEL.PRODUCT_SETTLEMENT',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.PRODUCT_SETTLEMENT,
                parameters: {
                  refGUID: model.purchaseLineGUID,
                  productInvoice: false,
                  productType: null,
                  originalRefGUID: this.getOriginalId(model),
                  refType: RefType.PurchaseLine
                }
              })
          },
          {
            label: 'LABEL.INTEREST_REALIZATION',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.INTEREST_REALIZED_TRANSACTION }),
            disabled: false
          },
          {
            label: 'LABEL.ROLL_BILL_PURCHASE_LINE',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.INQUIRY_ROLL_BILL_PURCHASE_LINE }),
            disabled: model.rollBill
          }
        ]
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  getOriginalId(model: PurchaseLineItemView): any {
    if (model.originalPurchaseLineGUID) {
      return model.originalPurchaseLineGUID;
    } else {
      return model.purchaseLineGUID;
    }
  }
  setFunctionOptions(model: PurchaseLineItemView): void {
    this.functionItems = [
      {
        label: 'LABEL.ADDITIONAL_PURCHASE',
        disabled: !(model.documentStatus_StatusId === PurchaseStatus.Posted) && !model.rollBill,
        command: () => {
          this.service.validateAdditionalPurchaseMenuBtn(this.model).subscribe((result) => {
            if (result) {
              this.toFunction({ path: ROUTE_FUNCTION_GEN.ADDITIONAL_PURCHASE });
            }
          });
        }
      }
      //   { label: 'LABEL.UPDATE_METHOD_OF_PAYMENT', command: () => this.toFunction({ path: ROUTE_RELATED_GEN.UPDATE_METHOD_OF_PAYMENT }) }
    ];
    super.setFunctionOptionsMapping();
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updatePurchaseLine(this.model), isColsing);
    } else {
      super.onCreate(this.service.createPurchaseLine(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onBuyerChange() {
    this.custom.setModelByBuyer(this.model, this.buyerTableOptions);
    this.setBuyerInvoiceTableDropDown();
    this.setChequeTableBuyerDropdown();
    this.setBuyerAgreementDropDown();
    this.setAssignmentAgreementTableDropDown();
  }
  onBuyerInvoiceChange() {
    this.custom.setModelByBuyerInvoice(this.model);
  }
  onDueDateChange() {
    this.custom.setModelByDueDate(this.model);
  }
  onLinePurchasePctChange() {
    this.custom.setModelByLinePurchasePct(this.model);
  }
  onCustomerPDCChange() {
    this.custom.setModelByCustomerPDC(this.model, this.chequeTableCustomerOptions);
  }
  getLinePurchasePctValidation() {
    if (this.custom.isRollBill == false) {
      return this.custom.purchaseCondition1(this.model);
    }
  }

  getInterestAmountValidation() {
    return this.custom.shareCondition1(this.model);
  }
  getSettleInterestAmountValidation() {
    return this.custom.shareCondition1(this.model);
  }
  getBillingFeeAmountValidation() {
    return this.custom.shareCondition2(this.model);
  }
  getSettleBillingFeeAmountValidation() {
    return this.custom.shareCondition2(this.model);
  }
  onReceiptFeeAmountChange() {
    return this.custom.shareCondition3(this.model);
  }
  onSettleReceiptFeeAmountChange() {
    return this.custom.shareCondition3(this.model);
  }
  // Remove by spec R016
  //   getBillingDateChangeValidation() {
  //     return this.custom.shareCondition4(this.model);
  //   }
  //   getCollectionDateValidation() {
  //     return this.custom.shareCondition5(this.model);
  //   }
  getDueDateValidation() {
    return this.custom.shareCondition6(this.model);
  }

  onPurchaseFeeAmountChange() {
    return this.custom.shareCondition7(this.model);
  }

  //#region rollBill
  onSubmitList(row: RowIdentity) {
    this.isShowItemView = true;
    this.isShowOutstandingList = false;
    let input = new PurchaseLineRollBillInitialDataParm();
    input.purchaseTableGUID = this.parentId;
    input.purchaseLineGUID = row.guid;
    this.service.getInitialRollBillData(input).subscribe((result) => {
      this.custom.setDeafultDataByListOutstandingPurchase(this.model, result);
      this.setBuyerInvoiceTableDropDown();
      this.setChequeTableBuyerDropdown();
      this.setBuyerAgreementDropDown();
      this.setAssignmentAgreementTableDropDown();
      this.setFieldAccessing();
      this.isShowItemView = true;
      this.isShowOutstandingList = false;
    });
  }
  //#endregion
  setLedgerDimension(ledgerDimension: SelectItems[]): void {
    this.ledgerDimensionOptions1 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension1);
    this.ledgerDimensionOptions2 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension2);
    this.ledgerDimensionOptions3 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension3);
    this.ledgerDimensionOptions4 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension4);
    this.ledgerDimensionOptions5 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension5);
  }

  onSettlePurchaseFeeAmountChange() {
    this.custom.setModelBySettlePurchaseFeeAmount(this.model);
  }

  onBuyerPDCDropdownFocus(e: IvzDropdownOnFocusModel): void {
    super
      .setDropdownOptionOnFocus(
        e,
        this.model,
        this.baseDropdown.getChequeTableByPurchaseLineBuyerDropDown(
          [this.model.purchaseTableGUID, this.model.buyerTableGUID],
          this.model.buyerPDCTableGUID
        )
      )
      .subscribe((result) => {
        this.chequeTableBuyerOptions = result;
      });
  }

  onCustomerPDCDropdownFocus(e: IvzDropdownOnFocusModel): void {
    super
      .setDropdownOptionOnFocus(
        e,
        this.model,
        this.baseDropdown.getChequeTableByPurchaseLineCustomerDropDown(
          [this.model.purchaseTableGUID, this.model.purchaseTable_CustomerTableGUID],

          this.model.customerPDCTableGUID
        )
      )
      .subscribe((result) => {
        this.chequeTableCustomerOptions = result;
      });
  }

  //#region update dropdown pattern
  setChequeTableBuyerDropdown(): void {
    if (!isNullOrUndefOrEmptyGUID(this.model.buyerTableGUID)) {
      this.baseService.baseDropdown
        .getChequeTableByPurchaseLineBuyerDropDown([this.model.purchaseTableGUID, this.model.buyerTableGUID])
        .subscribe((result) => {
          this.chequeTableBuyerOptions = result;
        });
    } else {
      this.chequeTableBuyerOptions.length = 0;
    }
  }
  setBuyerAgreementDropDown() {
    if (!isNullOrUndefOrEmptyGUID(this.model.purchaseTable_CustomerTableGUID)) {
      if (this.model.creditLitmitType_ValidateBuyerAgreement) {
        this.baseService.baseDropdown.getBuyerAgeementTableByPurchaseLineValidateBuyerDropDown([this.model.creditAppLineGUID]).subscribe((result) => {
          this.buyerAgreementOptions = result;
        });
      } else if (!isNullOrUndefOrEmptyGUID(this.model.buyerTableGUID)) {
        this.baseService.baseDropdown
          .getBuyerAgeementTableByPurchaseLineDropDown([this.model.purchaseTable_CustomerTableGUID, this.model.buyerTableGUID])
          .subscribe((result) => {
            this.buyerAgreementOptions = result;
          });
      } else {
        this.buyerAgreementOptions.length = 0;
      }
    } else {
      this.buyerAgreementOptions.length = 0;
    }
  }
  setAssignmentAgreementTableDropDown() {
    if (!isNullOrUndefOrEmptyGUID(this.model.buyerTableGUID)) {
      this.baseService.baseDropdown
        .getAssignmentAgreementTableByPurchaseLineDropDown([
          this.model.purchaseTable_CustomerTableGUID,
          this.model.buyerTableGUID,
          this.model.assignmentMethod_AssignmentMethodGUID
        ])
        .subscribe((result) => {
          this.assignmentAgreementTableOptions = result;
        });
    } else {
      this.assignmentAgreementTableOptions.length = 0;
    }
  }
  setBuyerInvoiceTableDropDown() {
    if (!isNullOrUndefOrEmptyGUID(this.model.creditAppLineGUID)) {
      this.baseService.baseDropdown.getBuyerInvoiceTableByCreditAppLineDropDown(this.model.creditAppLineGUID).subscribe((result) => {
        this.buyerInvoiceTableOptions = result;
      });
    } else {
      this.buyerInvoiceTableOptions.length = 0;
      this.model.buyerInvoiceTableGUID = null;
      this.custom.clearModelByBuyerInvoice(this.model);
    }
  }
  //#endregion
}
