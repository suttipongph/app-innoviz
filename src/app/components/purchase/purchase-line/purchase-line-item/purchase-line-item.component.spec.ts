import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseLineItemComponent } from './purchase-line-item.component';

describe('PurchaseLineItemViewComponent', () => {
  let component: PurchaseLineItemComponent;
  let fixture: ComponentFixture<PurchaseLineItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PurchaseLineItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseLineItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
