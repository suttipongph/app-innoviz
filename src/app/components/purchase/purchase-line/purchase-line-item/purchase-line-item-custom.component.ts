import { isNull } from '@angular/compiler/src/output/output_ast';
import { TranslateService } from '@ngx-translate/core';
import { AppInjector } from 'app-injector';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { isDateFromGreaterThanDateTo } from 'shared/functions/date.function';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import {
  AccessModeView,
  BuyerInvoiceTableItemView,
  BuyerTableItemView,
  ChequeTableItemView,
  CreditAppLineItemView,
  PurchaseLineItemView
} from 'shared/models/viewModel';
import { PurchaseLineService } from '../purchase-line.service';

const firstGroup = [
  'PURCHASE_TABLE_GUID',
  'LINE_NUM',
  'REF_PURCHASE_LINE_GUID',
  'CLOSED_FOR_ADDITIONAL_PURCHASE',
  'BUYER_INVOICE_AMOUNT',
  'CREDIT_APP_LINE_GUID',
  'PURCHASE_LINE_INVOICE_TABLE_GUID',
  'INTEREST_DATE',
  'INTEREST_DAY',
  'PURCHASE_PCT',
  'PURCHASE_AMOUNT',
  'LINE_PURCHASE_AMOUNT',
  'RESERVE_AMOUNT',
  'NET_PURCHASE_AMOUNT',
  'INTEREST_AMOUNT',
  'INTEREST_REFUND_AMOUNT',
  'PURCHASE_FEE_PCT',
  'PURCHASE_FEE_CALCULATE_BASE',
  'RETENTION_CALCULATE_BASE',
  'RETENTION_PCT',
  'CUSTOMER_PDC_DATE',
  'OUTSTANDING_BUYER_INVOICE_AMOUNT',
  'ROLLBILL_PURCHASE_LINE_GUID',
  'ORIGINAL_PURCHASE_LINE_GUID',
  'ORIGINAL_INTEREST_DATE',
  'ROLLBILL_INTEREST_DAY',
  'ROLLBILL_INTEREST_PCT',
  'ROLLBILL_INTEREST_AMOUNT',
  'NUMBER_OF_ROLLBILL',
  'PURCHASE_LINE_GUID',
  'SETTLE_ROLL_BILL_INTEREST_AMOUNT'
];

const secoundGroup = ['BUYER_TABLE_GUID'];
const thirdGroup = ['BUYER_INVOICE_TABLE_GUID', 'BUYER_AGREEMENT_TABLE_GUID'];
const rollBillGroup = [
  'PURCHASE_TABLE_GUID',
  'LINE_NUM',
  'BUYER_TABLE_GUID',
  'BUYER_INVOICE_TABLE_GUID',
  'BUYER_INVOICE_AMOUNT',
  'OUTSTANDING_BUYER_INVOICE_AMOUNT',
  'BUYER_AGREEMENT_TABLE_GUID',
  'REF_PURCHASE_LINE_GUID',
  'CLOSED_FOR_ADDITIONAL_PURCHASE',
  'PURCHASE_LINE_INVOICE_TABLE_GUID',
  'CREDIT_APP_LINE_GUID',
  'INTEREST_DATE',
  'INTEREST_DAY',
  'PURCHASE_PCT',
  'LINE_PURCHASE_PCT',
  'PURCHASE_AMOUNT',
  'LINE_PURCHASE_AMOUNT',
  'RESERVE_AMOUNT',
  'NET_PURCHASE_AMOUNT',
  'INTEREST_AMOUNT',
  'INTEREST_REFUND_AMOUNT',
  'PURCHASE_FEE_PCT',
  'PURCHASE_FEE_CALCULATE_BASE',
  'RETENTION_CALCULATE_BASE',
  'RETENTION_PCT',
  'ASSIGNMENT_AGREEMENT_TABLE_GUID',
  'ASSIGNMENT_AMOUNT',
  'CUSTOMER_PDC_DATE',
  'METHOD_OF_PAYMENT_GUID',
  'ROLLBILL_PURCHASE_LINE_GUID',
  'ORIGINAL_PURCHASE_LINE_GUID',
  'ORIGINAL_INTEREST_DATE',
  'ROLLBILL_INTEREST_DAY',
  'ROLLBILL_INTEREST_PCT',
  'NUMBER_OF_ROLLBILL'
];

export class PurchaseLineItemCustomComponent {
  baseService: BaseServiceModel<any>;
  translateService = AppInjector.get(TranslateService);
  accessModeView: AccessModeView = new AccessModeView();

  isRollBill = false;
  isBuyerAgreementRequired = false;
  isAssignmentAgreementRequired = false;
  getFieldAccessing(model: PurchaseLineItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (this.isRollBill) {
      fieldAccessing.push({ filedIds: rollBillGroup, readonly: true });
      fieldAccessing.push({ filedIds: secoundGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      if (isUpdateMode(model.purchaseLineGUID) && model.refPurchaseLineGUID != null) {
        fieldAccessing.push({ filedIds: thirdGroup, readonly: true });
        fieldAccessing.push({ filedIds: secoundGroup, readonly: true });
      } else {
        fieldAccessing.push({ filedIds: secoundGroup, readonly: false });
      }
      if (model.creditLitmitType_ValidateBuyerAgreement) {
        this.isBuyerAgreementRequired = true;
      } else {
        this.isBuyerAgreementRequired = false;
      }

      // Purchase R10
      if (model.assignmentMethod_ValidateAssignmentBalance) {
        this.isAssignmentAgreementRequired = true;
      } else {
        this.isAssignmentAgreementRequired = false;
      }
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: PurchaseLineItemView, isWorkflowMode: boolean): Observable<boolean> {
    const accessright = isNullOrUndefOrEmptyGUID(model.purchaseLineGUID) ? canCreate : canUpdate;
    let service: PurchaseLineService = this.baseService.service;
    return service.getPurchaseTableAccessMode(model.purchaseTableGUID, isWorkflowMode).pipe(
      map((result) => {
        let accessLogic = isNullOrUndefOrEmptyGUID(model.purchaseLineGUID) ? result.canCreate : result.canView;
        return !(accessLogic && accessright);
      })
    );
  }

  getBuyerAgreementDropDown(model: PurchaseLineItemView) {
    if (model.creditLitmitType_ValidateBuyerAgreement && !isNullOrUndefOrEmptyGUID(model.creditAppLineGUID)) {
      return this.baseService.baseDropdown.getBuyerAgeementTableByPurchaseLineValidateBuyerDropDown([model.creditAppLineGUID]);
    } else if (!isNullOrUndefOrEmptyGUID(model.buyerTableGUID) && !isNullOrUndefOrEmptyGUID(model.purchaseTable_CustomerTableGUID)) {
      return this.baseService.baseDropdown.getBuyerAgeementTableByPurchaseLineDropDown([model.purchaseTable_CustomerTableGUID, model.buyerTableGUID]);
    } else {
      return of([]);
    }
  }

  getBuyerInvoiceTableByCreditAppLineDropDown(creditAppLineGUID: string) {
    if (!isNullOrUndefOrEmptyGUID(creditAppLineGUID)) {
      return this.baseService.baseDropdown.getBuyerInvoiceTableByCreditAppLineDropDown(creditAppLineGUID);
    } else {
      return of([]);
    }
  }
  getAssignmentAgreementTableDropDown(model: PurchaseLineItemView) {
    if (
      (!isNullOrUndefOrEmptyGUID(model.purchaseTable_CustomerTableGUID) && !isNullOrUndefOrEmptyGUID(model.buyerTableGUID)) ||
      !isNullOrUndefOrEmptyGUID(model.assignmentMethod_AssignmentMethodGUID)
    ) {
      return this.baseService.baseDropdown.getAssignmentAgreementTableByPurchaseLineDropDown(
        [model.purchaseTable_CustomerTableGUID, model.buyerTableGUID, model.assignmentMethod_AssignmentMethodGUID],
        model.assignmentAgreementTableGUID
      );
    } else {
      return of([]);
    }
  }
  setModelByDueDate(model: PurchaseLineItemView) {
    if (!isNullOrUndefined(model.dueDate)) {
      this.baseService.service.getPurchaseLineByDueDate(model).subscribe(
        (result) => {
          model.interestDate = result.interestDate;
          model.interestDay = result.interestDay;
          model.interestAmount = result.interestAmount;
        },
        (error) => {
          this.baseService.notificationService.showErrorMessageFromResponse(error);
          this.clearModelByDueDate(model);
        }
      );
    } else {
      this.clearModelByDueDate(model);
    }
  }
  setModelByLinePurchasePct(model: PurchaseLineItemView) {
    if (!isNullOrUndefined(model.linePurchasePct) && !isNullOrUndefined(model.buyerInvoiceTableGUID)) {
      this.baseService.service.getPurchaseLineByLinePurchasePct(model).subscribe(
        (result) => {
          model.purchasePct = result.purchasePct;
          model.linePurchaseAmount = result.linePurchaseAmount;
          model.purchaseAmount = result.purchaseAmount;
          model.netPurchaseAmount = result.netPurchaseAmount;
          model.interestAmount = result.interestAmount;
          model.retentionAmount = result.retentionAmount;
          model.purchaseFeeAmount = result.purchaseFeeAmount;
          model.reserveAmount = result.reserveAmount;
        },
        (error) => {
          this.baseService.notificationService.showErrorMessageFromResponse(error);
          this.clearModelByLinePurchasePct(model);
        }
      );
    } else {
      this.clearModelByLinePurchasePct(model);
    }
  }

  setModelByBuyer(model: PurchaseLineItemView, option: SelectItems[]) {
    model.buyerAgreementTableGUID = null;
    model.buyerInvoiceTableGUID = null;
    model.buyerPDCTableGUID = null;
    model.assignmentAgreementTableGUID = null;
    if (!isNullOrUndefOrEmptyGUID(model.buyerTableGUID)) {
      let item: BuyerTableItemView = option.find((t) => t.value == model.buyerTableGUID).rowData as BuyerTableItemView;
      model.creditAppLineGUID = item.creditAppLine_CreditAppLineGUID;
      model.creditAppLine_Values = item.creditAppLine_Values;
      model.creditAppLine_MaxPurchasePct = item.creditAppLine_MaxPurchasePct;
      model.purchaseFeeCalculateBase = item.purchaseLine_PurchaseFeeCalculateBase;
      model.methodOfPaymentGUID = item.methodOfPayment_MethodOfPaymentGUID;
      model.methodOfPayment_Values = item.methodOfPayment_Values;
      model.purchaseFeePct = item.creditAppLine_PurchaseFeePct;

      model.interestAmount = 0;
      model.collectionDate = null;
    } else {
      model.creditAppLineGUID = null;
      model.creditAppLine_Values = '';
      model.creditAppLine_MaxPurchasePct = 0;
      model.purchaseFeeCalculateBase = null;
      model.methodOfPaymentGUID = null;
      model.methodOfPayment_Values = '';
      model.purchaseFeePct = 0;
      model.interestAmount = 0;
      model.collectionDate = null;
    }
  }
  setModelByBuyerInvoice(model: PurchaseLineItemView) {
    if (!isNullOrUndefined(model.buyerInvoiceTableGUID)) {
      this.baseService.service.getPurchaseLineByBuyerInvoice(model).subscribe(
        (result) => {
          model.buyerInvoiceAmount = result.buyerInvoiceAmount;
          model.buyerAgreementTableGUID = result.buyerAgreementTableGUID;
          model.purchasePct = result.purchasePct;
          model.assignmentAmount = result.assignmentAmount;
          model.purchaseFeeAmount = result.purchaseFeeAmount;
          model.retentionAmount = result.retentionAmount;
          model.reserveAmount = result.reserveAmount;
          //R09
          model.linePurchaseAmount = result.linePurchaseAmount;
          model.purchaseAmount = result.purchaseAmount;
          model.netPurchaseAmount = result.netPurchaseAmount;
          model.interestAmount = result.interestAmount;

          // R10
          model.settleInterestAmount = 0;
          model.purchaseFeeAmount = 0;

          model.interestAmount = result.interestAmount;
          model.collectionDate = result.collectionDate;
        },
        (error) => {
          this.baseService.notificationService.showErrorMessageFromResponse(error);
          this.clearModelByBuyerInvoice(model);
        }
      );
    } else {
      this.clearModelByBuyerInvoice(model);
    }
  }

  setModelByCustomerPDC(model: PurchaseLineItemView, option: SelectItems[]) {
    if (!isNullOrUndefined(model.customerPDCTableGUID)) {
      let item: ChequeTableItemView = option.find((t) => t.value == model.customerPDCTableGUID).rowData as ChequeTableItemView;
      model.customerPDCDate = item.chequeDate;
    } else {
      model.customerPDCDate = null;
    }
  }

  clearModelByBuyerInvoice(model: PurchaseLineItemView) {
    model.buyerInvoiceAmount = 0;
    model.buyerAgreementTableGUID = null;
    model.purchasePct = 0;
    model.assignmentAmount = 0;
    model.purchaseFeeAmount = 0;
    model.retentionAmount = 0;
    model.reserveAmount = 0;

    model.linePurchaseAmount = 0;
    model.purchaseAmount = 0;
    model.netPurchaseAmount = 0;
    model.interestAmount = 0;

    model.settleInterestAmount = 0;
    model.settlePurchaseFeeAmount = 0;
  }

  clearModelByDueDate(model: PurchaseLineItemView) {
    model.interestDate = null;
    model.interestDay = 0;
    model.interestAmount = 0;
  }
  clearModelByLinePurchasePct(model: PurchaseLineItemView) {
    model.purchasePct = 0;
    model.linePurchaseAmount = 0;
    model.purchaseAmount = 0;
    model.netPurchaseAmount = 0;
    model.interestAmount = 0;
    model.retentionAmount = 0;
    model.purchaseFeeAmount = 0;
  }

  // Settle interest amount ต้องน้อยกว่า Interest amount + Interest Refund amount
  shareCondition1(model: PurchaseLineItemView): any {
    let condition = model.settleInterestAmount > model.interestRefundAmount + model.interestAmount;
    if (condition) {
      return {
        code: 'ERROR.90031',
        parameters: [this.translateService.instant('LABEL.SETTLE_INTEREST_AMOUNT'), this.translateService.instant('LABEL.INTEREST_AMOUNT')]
      };
    }
    return null;
  }
  // Settle billing fee amount ต้องน้อยกว่า Billing fee amount
  shareCondition2(model: PurchaseLineItemView): any {
    let condition = model.settleBillingFeeAmount > model.billingFeeAmount;
    if (condition) {
      return {
        code: 'ERROR.90031',
        parameters: [this.translateService.instant('LABEL.SETTLE_BILLING_FEE_AMOUNT'), this.translateService.instant('LABEL.BILLING_FEE_AMOUNT')]
      };
    }
    return null;
  }
  // Settle receipt fee amount ต้องน้อยกว่า Receipt fee amount
  shareCondition3(model: PurchaseLineItemView): any {
    let condition = model.settleReceiptFeeAmount > model.receiptFeeAmount;
    if (condition) {
      return {
        code: 'ERROR.90031',
        parameters: [this.translateService.instant('LABEL.SETTLE_RECEIPT_FEE_AMOUNT'), this.translateService.instant('LABEL.RECEIPT_FEE_AMOUNT')]
      };
    }
    return null;
  }
  // ระบุ date ที่มากว่า Purchase date
  shareCondition4(model: PurchaseLineItemView): any {
    if (!isNullOrUndefined(model.billingDate)) {
      let condition = isDateFromGreaterThanDateTo(model.purchaseTable_PurchaseDate, model.billingDate);
      if (condition) {
        return {
          code: 'ERROR.90155',
          parameters: [this.translateService.instant('LABEL.BILLING_DATE'), this.translateService.instant('LABEL.PURCHASE_DATE')]
        };
      }
    }
    return null;
  }
  shareCondition5(model: PurchaseLineItemView): any {
    if (!isNullOrUndefined(model.collectionDate)) {
      let condition = isDateFromGreaterThanDateTo(model.purchaseTable_PurchaseDate, model.collectionDate);
      if (condition) {
        return {
          code: 'ERROR.90155',
          parameters: [this.translateService.instant('LABEL.COLLECTION_DATE'), this.translateService.instant('LABEL.PURCHASE_DATE')]
        };
      }
    }
    return null;
  }
  shareCondition6(model: PurchaseLineItemView): any {
    if (!isNullOrUndefined(model.dueDate)) {
      let condition = isDateFromGreaterThanDateTo(model.purchaseTable_PurchaseDate, model.dueDate);
      if (condition) {
        return {
          code: 'ERROR.90155',
          parameters: [this.translateService.instant('LABEL.DUE_DATE'), this.translateService.instant('LABEL.PURCHASE_DATE')]
        };
      }
    }
    return null;
  }
  shareCondition7(model: PurchaseLineItemView): any {
    let condition = model.settlePurchaseFeeAmount > model.purchaseFeeAmount;
    if (condition) {
      return {
        code: 'ERROR.90031',
        parameters: [this.translateService.instant('LABEL.SETTLE_PURCHASE_FEE_AMOUNT'), this.translateService.instant('LABEL.PURCHASE_FEE_AMOUNT')]
      };
    }
    return null;
  }
  purchaseCondition1(model: PurchaseLineItemView): any {
    if (!isNullOrUndefined(model.creditAppLineGUID)) {
      let condition = model.purchasePct > model.creditAppLine_MaxPurchasePct;
      if (condition) {
        return {
          code: 'ERROR.90031',
          parameters: [this.translateService.instant('LABEL.PURCHASE_PCT'), this.translateService.instant('LABEL.MAXIMUM_PURCHASE_PCT')]
        };
      }
    } else {
      return {
        code: 'ERROR.90031',
        parameters: [this.translateService.instant('LABEL.PURCHASE_PCT'), this.translateService.instant('LABEL.MAXIMUM_PURCHASE_PCT')]
      };
    }
  }

  setDeafultDataByListOutstandingPurchase(model: PurchaseLineItemView, input: PurchaseLineItemView) {
    model.rollbillPurchaseLineGUID = input.rollbillPurchaseLineGUID;
    model.buyerTableGUID = input.buyerTableGUID;
    model.creditAppLineGUID = input.creditAppLineGUID;
    model.buyerInvoiceTableGUID = input.buyerInvoiceTableGUID;
    model.buyerInvoiceAmount = input.buyerInvoiceAmount;
    model.buyerAgreementTableGUID = input.buyerAgreementTableGUID;
    model.refPurchaseLineGUID = input.refPurchaseLineGUID;
    model.refPurchaseLine_Values = input.refPurchaseLine_Values;
    model.purchasePct = input.purchasePct;
    model.linePurchasePct = input.linePurchasePct;
    model.purchaseAmount = input.purchaseAmount;
    model.linePurchaseAmount = input.linePurchaseAmount;
    model.purchaseFeePct = input.purchaseFeePct;
    model.purchaseFeeCalculateBase = input.purchaseFeeCalculateBase;
    model.assignmentAgreementTableGUID = input.assignmentAgreementTableGUID;
    model.assignmentAmount = input.assignmentAmount;
    model.methodOfPaymentGUID = input.methodOfPaymentGUID;
    model.methodOfPayment_Values = input.methodOfPayment_Values;
    model.originalPurchaseLineGUID = input.originalPurchaseLineGUID;
    model.originalInterestDate = input.originalInterestDate;
    model.rollbillInterestPct = input.rollbillInterestPct;
    model.rollbillInterestAmount = input.rollbillInterestAmount;
    model.rollbillInterestDay = input.rollbillInterestDay;
    model.numberOfRollbill = input.numberOfRollbill;
    model.outstandingBuyerInvoiceAmount = input.outstandingBuyerInvoiceAmount;
    model.creditAppLine_Values = input.creditAppLine_Values;
    model.reserveAmount = input.reserveAmount;
    model.netPurchaseAmount = input.netPurchaseAmount;
  }
  setModelBySettlePurchaseFeeAmount(model: PurchaseLineItemView) {
    model.netPurchaseAmount = model.linePurchaseAmount - model.settlePurchaseFeeAmount;
  }
}
