import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PurchaseLineListComponent } from './purchase-line-list.component';

describe('PurchaseLineListViewComponent', () => {
  let component: PurchaseLineListComponent;
  let fixture: ComponentFixture<PurchaseLineListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PurchaseLineListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseLineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
