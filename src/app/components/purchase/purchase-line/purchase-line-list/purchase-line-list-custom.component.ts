import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView } from 'shared/models/viewModel';
import { PurchaseLineService } from '../purchase-line.service';

export class PurchaseLineListCustomComponent {
  baseService: BaseServiceModel<any>;
  accessModeView: AccessModeView = new AccessModeView();
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }

  getAccessModeByPurchaseTable(purchaseTableGUID: string, isWorkflowMode: boolean) {
    let service: PurchaseLineService = this.baseService.service;
    return service.getPurchaseTableAccessModeForPurchaseLine(purchaseTableGUID, isWorkflowMode).pipe(tap((result) => (this.accessModeView = result)));
  }

  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
}
