import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { PurchaseLineListView } from 'shared/models/viewModel';
import { PurchaseLineService } from '../purchase-line.service';
import { PurchaseLineListCustomComponent } from './purchase-line-list-custom.component';

@Component({
  selector: 'purchase-line-list',
  templateUrl: './purchase-line-list.component.html',
  styleUrls: ['./purchase-line-list.component.scss']
})
export class PurchaseLineListComponent extends BaseListComponent<PurchaseLineListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  buyerTableOptions: SelectItems[] = [];
  methodOfPaymentOptions: SelectItems[] = [];
  purchaseFeeCalculateBaseOptions: SelectItems[] = [];

  constructor(public custom: PurchaseLineListCustomComponent, private service: PurchaseLineService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.accessRightChildPage = 'PurchaseLineListPage';
    this.parentId = this.uiService.getKey(this.uiService.getRelatedInfoActiveTableName());
  }
  ngOnInit(): void {
    this.accessRightChildPage = 'PurchaseLineListPage';
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getPurchaseFeeCalculateBaseEnumDropDown(this.purchaseFeeCalculateBaseOptions);
    this.custom.getAccessModeByPurchaseTable(this.parentId, this.isWorkFlowMode()).subscribe(() => this.setDataGridOption());
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
    this.baseDropdown.getMethodOfPaymentDropDown(this.methodOfPaymentOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = true;
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'purchaseLineGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.LINE',
        textKey: 'lineNum',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerTableOptions,
        searchingKey: 'buyerTableGUID',
        sortingKey: 'buyerTable_BuyerId'
      },
      {
        label: 'LABEL.BUYER_INVOICE_ID',
        textKey: 'buyerInvoiceTable_Values',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'buyerInvoiceTableGUID',
        sortingKey: 'buyerInvoiceTable_BuyerInvoiceId'
      },
      {
        label: 'LABEL.DUE_DATE',
        textKey: 'dueDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BILLING_DATE',
        textKey: 'billingDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.COLLECTION_DATE',
        textKey: 'collectionDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BUYER_INVOICE_AMOUNT',
        textKey: 'buyerInvoiceAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: 'LABEL.PURCHASE_AMOUNT',
        textKey: 'purchaseAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: 'LABEL.METHOD_OF_PAYMENT_ID',
        textKey: 'methodOfPayment_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.methodOfPaymentOptions,
        searchingKey: 'methodOfPaymentGUID',
        sortingKey: 'methodOfPayment_MethodOfPaymentId'
      },
      {
        label: null,
        textKey: 'purchaseTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<PurchaseLineListView>> {
    return this.service.getPurchaseLineToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deletePurchaseLine(row));
  }
}
