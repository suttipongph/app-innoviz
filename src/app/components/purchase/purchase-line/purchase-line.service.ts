import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, PurchaseLineItemView, PurchaseLineListView, PurchaseTableItemView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { PurchaseStatus } from 'shared/constants';
import { PurchaseLineRollBillInitialDataParm } from 'shared/models/viewModel/purchaseLineRollBillInitialDataParm';
@Injectable()
export class PurchaseLineService {
  serviceKey = 'purchaseLineGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getPurchaseLineToList(search: SearchParameter): Observable<SearchResult<PurchaseLineListView>> {
    const url = `${this.servicePath}/GetPurchaseLineList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deletePurchaseLine(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeletePurchaseLine`;
    return this.dataGateway.delete(url, row);
  }
  getPurchaseLineById(id: string): Observable<PurchaseLineItemView> {
    const url = `${this.servicePath}/GetPurchaseLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createPurchaseLine(vmModel: PurchaseLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreatePurchaseLine`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updatePurchaseLine(vmModel: PurchaseLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdatePurchaseLine`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<PurchaseLineItemView> {
    const url = `${this.servicePath}/GetPurchaseLineInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getInitialRollBillData(vmModel: PurchaseLineRollBillInitialDataParm): Observable<PurchaseLineItemView> {
    const url = `${this.servicePath}/GetPurchaseLineRollBillInitialData`;
    return this.dataGateway.post(url, vmModel);
  }
  getCalcInterestAmount(vmModel: PurchaseLineItemView): Observable<number> {
    const url = `${this.servicePath}/GetCalcInterestAmount`;
    return this.dataGateway.post(url, vmModel);
  }
  getPurchaseLineByDueDate(vmModel: PurchaseLineItemView): Observable<PurchaseLineItemView> {
    const url = `${this.servicePath}/GetPurchaseLineByDueDate`;
    return this.dataGateway.post(url, vmModel);
  }
  getPurchaseLineByLinePurchasePct(vmModel: PurchaseLineItemView): Observable<PurchaseLineItemView> {
    const url = `${this.servicePath}/GetPurchaseLineByLinePurchasePct`;
    return this.dataGateway.post(url, vmModel);
  }
  getPurchaseLineByBuyerInvoice(vmModel: PurchaseLineItemView): Observable<PurchaseLineItemView> {
    const url = `${this.servicePath}/GetPurchaseLineByBuyerInvoice`;
    return this.dataGateway.post(url, vmModel);
  }
  getPurchaseTableById(id: string): Observable<PurchaseTableItemView> {
    const url = `${this.servicePath}/GetPurchaseTableById/id=${id}`;
    return this.dataGateway.get(url);
  }

  getPurchaseTableAccessMode(purchaseId: string, isWorkflowMode: boolean): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetPurchaseTableAccessMode/purchaseId=${purchaseId}/isWorkflowMode=${isWorkflowMode}`;
    return this.dataGateway.get(url);
  }
  getPurchaseTableAccessModeForPurchaseLine(purchaseId: string, isWorkflowMode: boolean): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetPurchaseTableAccessModeForPurchaseLine/purchaseId=${purchaseId}/isWorkflowMode=${isWorkflowMode}`;
    return this.dataGateway.get(url);
  }
  validateAdditionalPurchaseMenuBtn(vwModel: PurchaseLineItemView): Observable<boolean> {
    const url = `${this.servicePath}/ValidateAdditionalPurchaseMenuBtn`;
    return this.dataGateway.post(url, vwModel);
  }
}
