import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { UpdateStatusRollbillPurchaseView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class UpdateStatusRollbillPurchaseService {
  serviceKey = 'updateStatusRollbillPurchaseGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getUpdateStatusRollbillPurchaseById(id: string): Observable<UpdateStatusRollbillPurchaseView> {
    const url = `${this.servicePath}/GetUpdateStatusRollbillPurchaseById/id=${id}`;
    return this.dataGateway.get(url);
  }
  updateUpdateStatusRollbillPurchase(vmModel: UpdateStatusRollbillPurchaseView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateStatusRollbillPurchase`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
