import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateStatusRollbillPurchaseService } from './update-status-rollbill-purchase.service';
import { UpdateStatusRollbillPurchaseComponent } from './update-status-rollbill-purchase/update-status-rollbill-purchase.component';
import { UpdateStatusRollbillPurchaseCustomComponent } from './update-status-rollbill-purchase/update-status-rollbill-purchase-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [UpdateStatusRollbillPurchaseComponent],
  providers: [UpdateStatusRollbillPurchaseService, UpdateStatusRollbillPurchaseCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [UpdateStatusRollbillPurchaseComponent]
})
export class UpdateStatusRollbillPurchaseComponentModule {}
