import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateStatusRollbillPurchaseComponent } from './update-status-rollbill-purchase.component';

describe('UpdateStatusRollbillPurchaseViewComponent', () => {
  let component: UpdateStatusRollbillPurchaseComponent;
  let fixture: ComponentFixture<UpdateStatusRollbillPurchaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UpdateStatusRollbillPurchaseComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateStatusRollbillPurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
