import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { UpdateStatusRollbillPurchaseView } from 'shared/models/viewModel';

const firstGroup = ['PURCHASE_ID'];

export class UpdateStatusRollbillPurchaseCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: UpdateStatusRollbillPurchaseView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<UpdateStatusRollbillPurchaseView> {
    let model = new UpdateStatusRollbillPurchaseView();
    model.updateStatusRollbillPurchaseGUID = EmptyGuid;
    return of(model);
  }
}
