import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SendPurchaseEmailService } from './send-purchase-email.service';
import { SendPurchaseEmailComponent } from './send-purchase-email/send-purchase-email.component';
import { SendPurchaseEmailCustomComponent } from './send-purchase-email/send-purchase-email-custom.component';
@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [SendPurchaseEmailComponent],
  providers: [SendPurchaseEmailService, SendPurchaseEmailCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [SendPurchaseEmailComponent]
})
export class SendPurchaseEmailComponentModule {}
