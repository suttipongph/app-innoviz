import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { SendPurchaseEmailView } from 'shared/models/viewModel';
@Injectable()
export class SendPurchaseEmailService {
  serviceKey = 'sendPurchaseEmailGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getSendPurchaseEmailById(id: string): Observable<SendPurchaseEmailView> {
    const url = `${this.servicePath}/GetSendPurchaseEmailById/id=${id}`;
    return this.dataGateway.get(url);
  }
  startWorkflow(vmModel: any): Observable<any> {
    const url = `${this.servicePath}/StartWorkflow`;
    return this.dataGateway.post(url, vmModel);
  }
}
