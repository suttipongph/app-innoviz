import { Observable, of } from 'rxjs';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { SendPurchaseEmailView } from 'shared/models/viewModel';

const firstGroup = ['PURCHASE_ID', 'PURCHASE_DATE', 'DOCUMENT_STATUS', 'CUSTOMER_ID', 'ADDITIONAL_PURCHASE'];

export class SendPurchaseEmailCustomComponent {
  baseService: BaseServiceModel<any>;
  parentId: string = null;
  getFieldAccessing(model: SendPurchaseEmailView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: SendPurchaseEmailView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<SendPurchaseEmailView> {
    let model = new SendPurchaseEmailView();
    return of(model);
  }

  setParam(model: SendPurchaseEmailView, workflowName: string, comment: string): object {
    let ob = new Object();
    ob['ParmDocGUID'] = model.purchaseTableGUID;
    let i = 0;
    ['Purchase', 'Submitted by', 'Customer', 'Purchase date'].forEach((item) => {
      model.parmFolio = model.parmFolio.replace(`{{${i}}}`, item);
      i++;
    });
    ob['ParmFolio'] = model.parmFolio;
    ob['ProcName'] = model.procName;
    ob['ParmCompany'] = model.parmCompany;
    ob['ParmDocGUID'] = model.parmDocGUID;
    ob['ParmDocID'] = model.parmDocID;
    ob['ParmCustomerEmail'] = model.parmCustomerEmail;
    ob['ParmMarketingUser'] = model.parmMarketingUser;
    ob['ParmSubmitPurchaseUser'] = model.parmSubmitPurchaseUser;
    ob['ParmPurchaseDate'] = model.parmPurchaseDate;
    ob['ParmCustomerName'] = model.parmCustomerName;
    ob['ParmPurchaseId'] = model.parmPurchaseId;

    return ob;
  }
}
