import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendPurchaseEmailComponent } from './send-purchase-email.component';

describe('SendPurchaseEmailViewComponent', () => {
  let component: SendPurchaseEmailComponent;
  let fixture: ComponentFixture<SendPurchaseEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SendPurchaseEmailComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendPurchaseEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
