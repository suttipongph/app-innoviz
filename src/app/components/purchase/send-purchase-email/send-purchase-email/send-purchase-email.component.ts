import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseWorkflowComponent } from 'core/components/base-workflow/base-workflow.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { getPurchaseEmailWorkflowName } from 'shared/config/globalvar.config';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { SendPurchaseEmailView } from 'shared/models/viewModel';
import { SendPurchaseEmailService } from '../send-purchase-email.service';
import { SendPurchaseEmailCustomComponent } from './send-purchase-email-custom.component';
@Component({
  selector: 'send-purchase-email',
  templateUrl: './send-purchase-email.component.html',
  styleUrls: ['./send-purchase-email.component.scss']
})
export class SendPurchaseEmailComponent extends BaseWorkflowComponent<SendPurchaseEmailView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  constructor(
    private service: SendPurchaseEmailService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: SendPurchaseEmailCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.workflowName = getPurchaseEmailWorkflowName();
    this.custom.parentId = this.id;
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<SendPurchaseEmailView> {
    return this.service.getSendPurchaseEmailById(this.id);
  }
  getInitialData(): Observable<SendPurchaseEmailView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false, this.model);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        const header: TranslateModel = { code: 'CONFIRM.CONFIRM' };
        const message: TranslateModel = { code: 'CONFIRM.90122', parameters: ['LABEL.PURCHASE'] };
        this.notificationService.showManualConfirmDialog(header, message);
        this.notificationService.isAccept.subscribe((isConfirm) => {
          if (isConfirm) {
            this.onSubmit(true, this.model);
          }
          this.notificationService.isAccept.observers = [];
        });
      }
    });
  }
  onSubmit(isColsing: boolean, model: SendPurchaseEmailView): void {
    const param = this.custom.setParam(this.model, this.workflowName, this.comment);
    model.workflowInstance = super.setStartWorkflowParm(param);

    super.onExecuteFunction(this.service.startWorkflow(model));
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onClose(): void {
    super.onBack();
  }
}
