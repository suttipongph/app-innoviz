import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PrintPurchaseReportView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class PrintPurchaseService {
  serviceKey = 'printPurchaseGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getPrintPurchaseById(id: string): Observable<PrintPurchaseReportView> {
    const url = `${this.servicePath}/GetPrintPurchaseById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createPrintPurchase(vmModel: PrintPurchaseReportView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreatePrintPurchase`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
}
