import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { PrintPurchaseReportView } from 'shared/models/viewModel';

const firstGroup = ['PURCHASE_ID', 'PURCHASE_DATE', 'DOCUMENT_STATUS', 'CUSTOMER_ID', 'ROLLBILL', 'ADDITIONAL_PURCHASE'];

export class PrintPurchaseReportCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: PrintPurchaseReportView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<PrintPurchaseReportView> {
    let model = new PrintPurchaseReportView();
    model.printPurchaseGUID = EmptyGuid;
    return of(model);
  }
}
