import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintPurchaseReportComponent } from './print-purchase-report.component';

describe('PrintPurchaseReportViewComponent', () => {
  let component: PrintPurchaseReportComponent;
  let fixture: ComponentFixture<PrintPurchaseReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrintPurchaseReportComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintPurchaseReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
