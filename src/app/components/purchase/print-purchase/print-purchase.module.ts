import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrintPurchaseService } from './print-purchase.service';
import { PrintPurchaseReportComponent } from './print-purchase/print-purchase-report.component';
import { PrintPurchaseReportCustomComponent } from './print-purchase/print-purchase-report-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PrintPurchaseReportComponent],
  providers: [PrintPurchaseService, PrintPurchaseReportCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PrintPurchaseReportComponent]
})
export class PrintPurchaseComponentModule {}
