import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { PrintPurchaseRollbillReportView } from 'shared/models/viewModel/printPurchaseRollbillReportView';

const firstGroup = ['PURCHASE_ID', 'PURCHASE_DATE', 'DOCUMENT_STATUS', 'CUSTOMER_ID', 'ROLLBILL', 'ADDITIONAL_PURCHASE'];
export class PrintPurchaseRollbillReportCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: PrintPurchaseRollbillReportView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<PrintPurchaseRollbillReportView> {
    const model = new PrintPurchaseRollbillReportView();
    model.printPurchaseRollbillGUID = EmptyGuid;
    return of(model);
  }
}
