import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintPurchaseRollbillReportComponent } from './print-purchase-rollbill-report.component';

describe('PrintPurchaseRollbillReportViewComponent', () => {
  let component: PrintPurchaseRollbillReportComponent;
  let fixture: ComponentFixture<PrintPurchaseRollbillReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrintPurchaseRollbillReportComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintPurchaseRollbillReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
