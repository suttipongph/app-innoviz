import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrintPurchaseRollbillService } from './print-purchase-rollbill.service';
import { PrintPurchaseRollbillReportComponent } from './print-purchase-rollbill/print-purchase-rollbill-report.component';
import { PrintPurchaseRollbillReportCustomComponent } from './print-purchase-rollbill/print-purchase-rollbill-report-custom.component';
@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PrintPurchaseRollbillReportComponent],
  providers: [PrintPurchaseRollbillService, PrintPurchaseRollbillReportCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PrintPurchaseRollbillReportComponent]
})
export class PrintPurchaseRollbillComponentModule {}
