import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PrintPurchaseRollbillReportView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class PrintPurchaseRollbillService {
  serviceKey = 'printPurchaseRollbillGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getPrintPurchaseRollbillById(id: string): Observable<PrintPurchaseRollbillReportView> {
    const url = `${this.servicePath}/GetPrintPurchaseRollbillById/id=${id}`;
    return this.dataGateway.get(url);
  }
}
