import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdditionalPurchaseService } from './additional-purchase.service';
import { AdditionalPurchaseComponent } from './additional-purchase/additional-purchase.component';
import { AdditionalPurchaseCustomComponent } from './additional-purchase/additional-purchase-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [AdditionalPurchaseComponent],
  providers: [AdditionalPurchaseService, AdditionalPurchaseCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [AdditionalPurchaseComponent]
})
export class AdditionalPurchaseComponentModule {}
