import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isDateFromGreaterThanDateTo } from 'shared/functions/date.function';
import { isUndefinedOrZeroLength, round } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, TranslateModel } from 'shared/models/systemModel';
import { AdditionalPurchaseParamView } from 'shared/models/viewModel';

const firstGroup = [
  'REF_PURCHASE_ID',
  'REF_PURCHASE_DATE',
  'REF_DUE_DATE',
  'REF_PURCHASE_PCT',
  'REF_PURCHASE_AMOUNT',
  'PURCHASE_PCT',
  'LINE_PURCHASE_AMOUNT',
  'PURCHASE_AMOUNT'
];

export class AdditionalPurchaseCustomComponent {
  baseService: BaseServiceModel<any>;
  toggleValidateMaxPurchasePct: boolean = false;
  getFieldAccessing(model: AdditionalPurchaseParamView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<AdditionalPurchaseParamView> {
    let model = new AdditionalPurchaseParamView();
    return of(model);
  }
  onValidatePurchaseDate(model: AdditionalPurchaseParamView): TranslateModel {
    if (isDateFromGreaterThanDateTo(model.refPurchaseDate, model.purchaseDate)) {
      return {
        code: 'ERROR.DATE_CANNOT_LESS_THAN',
        parameters: [this.baseService.translate.instant('LABEL.PURCHASE_DATE'), this.baseService.translate.instant('LABEL.REFERENCE_PURCHASE_DATE')]
      };
    } else if (!isUndefinedOrZeroLength(model.dueDate) && isDateFromGreaterThanDateTo(model.purchaseDate, model.dueDate)) {
      return {
        code: 'ERROR.DATE_CANNOT_LESS_THAN',
        parameters: [this.baseService.translate.instant('LABEL.DUE_DATE'), this.baseService.translate.instant('LABEL.PURCHASE_DATE')]
      };
    } else {
      return null;
    }
  }
  onValidateDueDate(model: AdditionalPurchaseParamView): TranslateModel {
    if (isDateFromGreaterThanDateTo(model.refDueDate, model.dueDate)) {
      return {
        code: 'ERROR.DATE_CANNOT_LESS_THAN',
        parameters: [this.baseService.translate.instant('LABEL.DUE_DATE'), this.baseService.translate.instant('LABEL.REFERENCE_DUE_DATE')]
      };
    } else if (!isUndefinedOrZeroLength(model.purchaseDate) && isDateFromGreaterThanDateTo(model.purchaseDate, model.dueDate)) {
      return {
        code: 'ERROR.DATE_CANNOT_LESS_THAN',
        parameters: [this.baseService.translate.instant('LABEL.DUE_DATE'), this.baseService.translate.instant('LABEL.PURCHASE_DATE')]
      };
    } else {
      return null;
    }
  }
  onValidateMaxPurchasePct(model: AdditionalPurchaseParamView): TranslateModel {
    if (!this.toggleValidateMaxPurchasePct) {
      return null;
    } else {
      return {
        code: 'ERROR.90031',
        parameters: [this.baseService.translate.instant('LABEL.LINE_PURCHASE_PCT'), this.baseService.translate.instant('LABEL.MAXIMUM_PURCHASE_PCT')]
      };
    }
  }
  validateMaxPurchasePct(model: AdditionalPurchaseParamView): Observable<boolean> {
    return this.baseService.service.validateMaxPurchasePct(model);
  }
  onLinePurchasePctChange(model: AdditionalPurchaseParamView): AdditionalPurchaseParamView {
    const e = model.linePurchasePct;
    const callerPurchasePct = model.refPurchasePct;
    const callerPurchaseAmount = model.refPurchaseAmount;
    const callerBuyerInvoiceAmount = model.refBuyerInvoiceAmount;
    model.purchasePct = callerPurchasePct + e;
    model.linePurchaseAmount = round(callerBuyerInvoiceAmount * (e / 100), 2);
    model.purchaseAmount = callerPurchaseAmount + model.linePurchaseAmount;
    return model;
  }
}
