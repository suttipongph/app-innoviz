import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdditionalPurchaseComponent } from './additional-purchase.component';

describe('AdditionalPurchaseViewComponent', () => {
  let component: AdditionalPurchaseComponent;
  let fixture: ComponentFixture<AdditionalPurchaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdditionalPurchaseComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdditionalPurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
