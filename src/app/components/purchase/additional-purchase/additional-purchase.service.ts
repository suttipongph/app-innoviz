import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AdditionalPurchaseParamView, AdditionalPurchaseResultView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class AdditionalPurchaseService {
  serviceKey = 'additionalPurchaseGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getAdditionalPurchaseById(id: string): Observable<AdditionalPurchaseParamView> {
    const url = `${this.servicePath}/GetAdditionalPurchaseById/id=${id}`;
    return this.dataGateway.get(url);
  }
  additionalPurchase(model: AdditionalPurchaseParamView): Observable<AdditionalPurchaseResultView> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, model);
  }
  validateMaxPurchasePct(model: AdditionalPurchaseParamView): Observable<boolean> {
    const url = `${this.servicePath}/ValidateMaxPurchasePct`;
    return this.dataGateway.post(url, model);
  }
}
