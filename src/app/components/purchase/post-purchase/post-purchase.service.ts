import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { PostPurchaseParamView, PostPurchaseResultView } from 'shared/models/viewModel/postPurchaseView';
@Injectable()
export class PostPurchaseService {
  serviceKey = 'postPurchaseGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }

  getPostPurchaseById(id: string): Observable<PostPurchaseParamView> {
    const url = `${this.servicePath}/GetPostPurchaseById/id=${id}`;
    return this.dataGateway.get(url);
  }
  postPurchase(vmModel: PostPurchaseParamView): Observable<PostPurchaseResultView> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vmModel);
  }
}
