import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PostPurchaseService } from './post-purchase.service';
import { PostPurchaseComponent } from './post-purchase/post-purchase.component';
import { PostPurchaseCustomComponent } from './post-purchase/post-purchase-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PostPurchaseComponent],
  providers: [PostPurchaseService, PostPurchaseCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PostPurchaseComponent]
})
export class PostPurchaseComponentModule {}
