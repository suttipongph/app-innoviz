import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { PostPurchaseParamView } from 'shared/models/viewModel';
import { PostPurchaseService } from '../post-purchase.service';
import { PostPurchaseCustomComponent } from './post-purchase-custom.component';
@Component({
  selector: 'post-purchase',
  templateUrl: './post-purchase.component.html',
  styleUrls: ['./post-purchase.component.scss']
})
export class PostPurchaseComponent extends BaseItemComponent<PostPurchaseParamView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  constructor(private service: PostPurchaseService, private currentActivatedRoute: ActivatedRoute, public custom: PostPurchaseCustomComponent) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<PostPurchaseParamView> {
    return this.service.getPostPurchaseById(this.id);
  }
  getInitialData(): Observable<PostPurchaseParamView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        const header: TranslateModel = { code: 'CONFIRM.CONFIRM' };
        const message: TranslateModel = { code: 'CONFIRM.90004', parameters: ['LABEL.PURCHASE'] };
        this.notificationService.showManualConfirmDialog(header, message);
        this.notificationService.isAccept.subscribe((isConfirm) => {
          if (isConfirm) {
            this.onSubmit(true);
          }
          this.notificationService.isAccept.observers = [];
        });
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    super.onExecuteFunction(this.service.postPurchase(this.model));
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onClose(): void {
    super.onBack();
  }
}
