import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { PostPurchaseParamView } from 'shared/models/viewModel';

const firstGroup = ['PURCHASE_ID', 'PURCHASE_DATE', 'DOCUMENT_STATUS', 'CUSTOMER_ID', 'ROLLBILL', 'ADDITIONAL_PURCHASE'];

export class PostPurchaseCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: PostPurchaseParamView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<PostPurchaseParamView> {
    let model = new PostPurchaseParamView();
    model.purchaseTableGUID = EmptyGuid;
    return of(model);
  }
}
