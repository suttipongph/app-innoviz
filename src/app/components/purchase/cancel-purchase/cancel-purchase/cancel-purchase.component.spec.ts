import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelPurchaseComponent } from './cancel-purchase.component';

describe('CancelPurchaseViewComponent', () => {
  let component: CancelPurchaseComponent;
  let fixture: ComponentFixture<CancelPurchaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CancelPurchaseComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelPurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
