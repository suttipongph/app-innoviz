import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CancelPurchaseView } from 'shared/models/viewModel/cancelPurchaseView';

const firstGroup = ['PURCHASE_ID', 'PURCHASE_DATE', 'DOCUMENT_STATUS', 'CUSTOMER_ID', 'ROLLBILL', 'ADDITIONAL_PURCHASE'];

export class CancelPurchaseCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CancelPurchaseView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CancelPurchaseView): Observable<boolean> {
    return of(false);
  }
  getInitialData(): Observable<CancelPurchaseView> {
    let model = new CancelPurchaseView();
    model.purchaseTableGUID = EmptyGuid;
    return of(model);
  }
}
