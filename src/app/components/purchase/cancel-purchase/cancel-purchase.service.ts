import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { CancelPurchaseResultView, CancelPurchaseView } from 'shared/models/viewModel/cancelPurchaseView';
@Injectable()
export class CancelPurchaseService {
  serviceKey = 'cancelPurchaseGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCancelPurchaseById(id: string): Observable<CancelPurchaseView> {
    const url = `${this.servicePath}/GetCancelPurchaseById/id=${id}`;
    return this.dataGateway.get(url);
  }
  cancelPurchase(vmModel: CancelPurchaseView): Observable<CancelPurchaseResultView> {
    const url = `${this.servicePath}/CancelPurchase`;
    return this.dataGateway.post(url, vmModel);
  }
}
