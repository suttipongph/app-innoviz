import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CancelPurchaseService } from './cancel-purchase.service';
import { CancelPurchaseComponent } from './cancel-purchase/cancel-purchase.component';
import { CancelPurchaseCustomComponent } from './cancel-purchase/cancel-purchase-custom.component';
@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CancelPurchaseComponent],
  providers: [CancelPurchaseService, CancelPurchaseCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CancelPurchaseComponent]
})
export class CancelPurchaseComponentModule {}
