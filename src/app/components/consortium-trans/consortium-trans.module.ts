import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConsortiumTransService } from './consortium-trans.service';
import { ConsortiumTransListComponent } from './consortium-trans-list/consortium-trans-list.component';
import { ConsortiumTransItemComponent } from './consortium-trans-item/consortium-trans-item.component';
import { ConsortiumTransItemCustomComponent } from './consortium-trans-item/consortium-trans-item-custom.component';
import { ConsortiumTransListCustomComponent } from './consortium-trans-list/consortium-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ConsortiumTransListComponent, ConsortiumTransItemComponent],
  providers: [ConsortiumTransService, ConsortiumTransItemCustomComponent, ConsortiumTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ConsortiumTransListComponent, ConsortiumTransItemComponent]
})
export class ConsortiumTransComponentModule {}
