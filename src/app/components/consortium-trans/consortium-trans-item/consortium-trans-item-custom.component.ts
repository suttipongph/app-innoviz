import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, ConsortiumLineItemView, ConsortiumTransItemView } from 'shared/models/viewModel';
import { ConsortiumTransService } from '../consortium-trans.service';

const MAIN_AGREEMENT_TABLE = 'mainagreementtable';
const ASSIGNMENT_AGREEMENT = 'assignmentagreementtable';
const BUSINESS_COLLATERAL_AGM_TABLE = 'businesscollateralagmtable';
const ADDENDUM_MAIN_AGREEMENT_TABLE = 'addendummainagreementtable';
const NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE = 'noticeofcancellationmainagreementtable';
const ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE = 'addendumbusinesscollateralagmtable';
const NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE = 'noticeofcancellationbusinesscollateralagmtable';
const firstGroup = ['CONSORTIUM_TRANS_GUID', 'REF_TYPE', 'REF_ID', 'REF_GUID'];
const readOnlyOnViewGroup = ['CONSORTIUM_LINE_GUID'];
const NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT = 'noticeofcancellation';

export class ConsortiumTransItemCustomComponent {
  baseService: BaseServiceModel<ConsortiumTransService>;
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  parentConsortiumTableGUID = null;
  getFieldAccessing(model: ConsortiumTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    if (isUpdateMode(model.consortiumTransGUID)) {
      fieldAccessing.push({ filedIds: readOnlyOnViewGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: readOnlyOnViewGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ConsortiumTransItemView): Observable<boolean> {
    return this.setAccessModeByParentStatus(model).pipe(
      map((result) => (isUpdateMode(model.consortiumTransGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  setAccessModeByParentStatus(model: ConsortiumTransItemView): Observable<AccessModeView> {
    let parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
    switch (this.parentName) {
      case MAIN_AGREEMENT_TABLE:
      case ADDENDUM_MAIN_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE:
        return this.baseService.service.getAccessModeByMainAgreementTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
          })
        );
      case ASSIGNMENT_AGREEMENT:
      case NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT:
        return this.baseService.service.getAccessModeByAssignmentTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
          })
        );
      case BUSINESS_COLLATERAL_AGM_TABLE:
      case ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
      case NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
        return this.baseService.service.getAccessModeByBusinessCollatealAgm(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
          })
        );
      default:
        return of(this.accessModeView);
    }
  }
  clearModelByConsortiumLine(model: ConsortiumTransItemView) {
    model.customerName = '';
    model.remark = '';
    model.operatedBy = '';
    model.address = '';
    model.authorizedPersonTypeGUID = null;
  }
  setModelValueByConsortiumLine(model: ConsortiumTransItemView, option: SelectItems[]) {
    if (!isNullOrUndefOrEmptyGUID(model.consortiumLineGUID)) {
      let consortiumLine: ConsortiumLineItemView = option.find((t) => t.value == model.consortiumLineGUID).rowData as ConsortiumLineItemView;
      model.customerName = consortiumLine.customerName;
      model.remark = consortiumLine.remark;
      model.operatedBy = consortiumLine.operatedBy;
      model.address = consortiumLine.address;
      model.authorizedPersonTypeGUID = consortiumLine.authorizedPersonTypeGUID;
    }
  }
  setConsortiumTableGUID(model: ConsortiumTransItemView) {
    switch (this.parentName) {
      case MAIN_AGREEMENT_TABLE:
      case ADDENDUM_MAIN_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE:
        this.parentConsortiumTableGUID = model.mainAgreementTable_ConsortiumTableGUID;
        break;
      case BUSINESS_COLLATERAL_AGM_TABLE:
      case ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
      case NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
        this.parentConsortiumTableGUID = model.mainAgreementTable_ConsortiumTableGUID;
        break;
      default:
        this.parentConsortiumTableGUID = null;
        break;
    }
  }
  setConsortiumLineOption(): Observable<SelectItems[]> {
    if (!isNullOrUndefOrEmptyGUID(this.parentConsortiumTableGUID)) {
      return this.baseService.baseDropdown.getConsortiumLineDropDownByConsortiumTable(this.parentConsortiumTableGUID);
    } else {
      return of([]);
    }
  }
}
