import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { ConsortiumTransItemView } from 'shared/models/viewModel';
import { ConsortiumTransService } from '../consortium-trans.service';
import { ConsortiumTransItemCustomComponent } from './consortium-trans-item-custom.component';
@Component({
  selector: 'consortium-trans-item',
  templateUrl: './consortium-trans-item.component.html',
  styleUrls: ['./consortium-trans-item.component.scss']
})
export class ConsortiumTransItemComponent extends BaseItemComponent<ConsortiumTransItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  authorizedPersonTypeOptions: SelectItems[] = [];
  consortiumLineOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];

  constructor(
    private service: ConsortiumTransService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: ConsortiumTransItemCustomComponent,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoOriginTableName();
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<ConsortiumTransItemView> {
    return this.service.getConsortiumTransById(this.id);
  }
  getInitialData(): Observable<ConsortiumTransItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.custom.setConsortiumTableGUID(result);
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.custom.setConsortiumTableGUID(result);
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin([this.baseDropdown.getAuthorizedPersonTypeDropDown(), this.custom.setConsortiumLineOption()]).subscribe(
      ([authorizedPersonType, consortiumLine]) => {
        this.authorizedPersonTypeOptions = authorizedPersonType;
        this.consortiumLineOptions = consortiumLine;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateConsortiumTrans(this.model), isColsing);
    } else {
      super.onCreate(this.service.createConsortiumTrans(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onConsortiumLineChange() {
    this.custom.clearModelByConsortiumLine(this.model);
    this.custom.setModelValueByConsortiumLine(this.model, this.consortiumLineOptions);
  }
}
