import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsortiumTransItemComponent } from './consortium-trans-item.component';

describe('ConsortiumTransItemViewComponent', () => {
  let component: ConsortiumTransItemComponent;
  let fixture: ComponentFixture<ConsortiumTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConsortiumTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsortiumTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
