import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ConsortiumTransListComponent } from './consortium-trans-list.component';

describe('ConsortiumTransListViewComponent', () => {
  let component: ConsortiumTransListComponent;
  let fixture: ComponentFixture<ConsortiumTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConsortiumTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsortiumTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
