import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, pipe } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { ConsortiumTransListView } from 'shared/models/viewModel';
import { ConsortiumTransService } from '../consortium-trans.service';
import { ConsortiumTransListCustomComponent } from './consortium-trans-list-custom.component';

@Component({
  selector: 'consortium-trans-list',
  templateUrl: './consortium-trans-list.component.html',
  styleUrls: ['./consortium-trans-list.component.scss']
})
export class ConsortiumTransListComponent extends BaseListComponent<ConsortiumTransListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  authorizedPersonTypeOptions: SelectItems[] = [];
  consortiumLineOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];

  constructor(
    public custom: ConsortiumTransListCustomComponent,
    private service: ConsortiumTransService,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.custom.setAccessModeByParentStatus().subscribe((result) => {
      this.setDataGridOption();
    });
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getAuthorizedPersonTypeDropDown(this.authorizedPersonTypeOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'consortiumTransGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.ORDERING',
        textKey: 'ordering',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.AUTHORIZED_PERSON_TYPE',
        textKey: 'authorizedPersonType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.authorizedPersonTypeOptions,
        sortingKey: 'authorizedPersonType_AuthorizedPersonTypeId',
        searchingKey: 'authorizedPersonTypeGUID'
      },
      {
        label: 'LABEL.CUSTOMER_NAME',
        textKey: 'customerName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.OPERATED_BY',
        textKey: 'operatedBy',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<ConsortiumTransListView>> {
    return this.service.getConsortiumTransToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteConsortiumTrans(row));
  }
}
