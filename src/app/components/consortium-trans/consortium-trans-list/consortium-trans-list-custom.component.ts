import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView } from 'shared/models/viewModel';
import { ConsortiumTransService } from '../consortium-trans.service';
const MAIN_AGREEMENT_TABLE = 'mainagreementtable';
const ASSIGNMENT_AGREEMENT = 'assignmentagreementtable';
const BUSINESS_COLLATERAL_AGM = 'businesscollateralagmtable';
const ADDENDUM_MAIN_AGREEMENT_TABLE = 'addendummainagreementtable';
const NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE = 'noticeofcancellationmainagreementtable';
const ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE = 'addendumbusinesscollateralagmtable';
const NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT = 'noticeofcancellation';
const NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE = 'noticeofcancellationbusinesscollateralagmtable';

export class ConsortiumTransListCustomComponent {
  baseService: BaseServiceModel<ConsortiumTransService>;
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setAccessModeByParentStatus(): Observable<AccessModeView> {
    let parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
    switch (this.parentName) {
      case MAIN_AGREEMENT_TABLE:
      case ADDENDUM_MAIN_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE:
        return this.baseService.service.getAccessModeByMainAgreementTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            this.accessModeView.canView = true;
          })
        );
      case ASSIGNMENT_AGREEMENT:
      case NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT:
        return this.baseService.service.getAccessModeByAssignmentTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            this.accessModeView.canView = true;
          })
        );
      case BUSINESS_COLLATERAL_AGM:
      case ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
      case NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
        return this.baseService.service.getAccessModeByBusinessCollatealAgm(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            this.accessModeView.canView = true;
          })
        );
      default:
        return of(this.accessModeView);
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
}
