import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, ConsortiumTransItemView, ConsortiumTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ConsortiumTransService {
  serviceKey = 'consortiumTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getConsortiumTransToList(search: SearchParameter): Observable<SearchResult<ConsortiumTransListView>> {
    const url = `${this.servicePath}/GetConsortiumTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteConsortiumTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteConsortiumTrans`;
    return this.dataGateway.delete(url, row);
  }
  getConsortiumTransById(id: string): Observable<ConsortiumTransItemView> {
    const url = `${this.servicePath}/GetConsortiumTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createConsortiumTrans(vmModel: ConsortiumTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateConsortiumTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateConsortiumTrans(vmModel: ConsortiumTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateConsortiumTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<ConsortiumTransItemView> {
    const url = `${this.servicePath}/GetConsortiumTransInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByMainAgreementTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByMainAgreementTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByAssignmentTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByAssignmentTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByBusinessCollatealAgm(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByBusinessCollateralAgmTable/id=${id}`;
    return this.dataGateway.get(url);
  }
}
