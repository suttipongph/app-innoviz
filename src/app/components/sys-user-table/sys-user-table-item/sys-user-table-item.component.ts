import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, AppConst, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode, transformLabel, isUndefinedOrZeroLength } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems, TranslateModel, ResponseModel } from 'shared/models/systemModel';
import {
  SysUserTableItemView,
  UserEmployeeMappingView,
  SysUserCompanyMappingView,
  CompanyItemView,
  SysUserRolesView,
  BranchItemView,
  EmployeeTableItemView
} from 'shared/models/viewModel';
import { SysUserTableService } from '../sys-user-table.service';
import { SysUserTableItemCustomComponent } from './sys-user-table-item-custom.component';
import { Steps } from 'primeng/steps';
import { SysUserTableFormComponent } from '../sys-user-table-form/sys-user-table-form.component';
import { SysRoleTableItemView } from 'shared/models/viewModel/sysRoleTableItemView';
import { map, tap } from 'rxjs/operators';
import { ValidationService } from 'shared/services/validation.service';
@Component({
  selector: 'sys-user-table-item',
  templateUrl: './sys-user-table-item.component.html',
  styleUrls: ['./sys-user-table-item.component.scss']
})
export class SysUserTableItemComponent extends BaseItemComponent<SysUserTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  isUndefinedOrZeroLength = isUndefinedOrZeroLength;
  companyOptions: SelectItems[] = [];
  branchOptions: SelectItems[] = [];
  // adUserOptions: SelectItems[] = [];
  roleOptions: SelectItems[] = [];

  userFormAsyncRunnerDone: boolean = false;
  asyncRunnerDone: boolean = false;
  @ViewChild('stepper') stepper: Steps;
  activeIndex: number = 0;
  selectedIndex: number = 0;
  currentIndex: number = 0;
  stepItems = [];
  @ViewChild('userForm') userForm: SysUserTableFormComponent;
  sourceCompanies: SelectItems[] = [];
  targetCompanies: SelectItems[] = [];

  selectedCompany: string;
  selectedCompanyLabel: string;
  prevSelectedCompany: string;
  prevSelectedCompanyLabel: string;

  sourceBranches: SelectItems[] = [];
  targetBranches: SelectItems[] = [];

  intermediateBranches: any = {};
  intermediateRoles: any = {};

  intermediateEmployees: any = {};

  isEmployeeOptionsLoaded: boolean = false;

  allEmployees: SelectItems[];
  employeeTableOption: SelectItems[] = [];
  targetEmployee: string;

  sourceRoles: SelectItems[] = [];
  targetRoles: SelectItems[] = [];

  errMsg: any[] = [];

  companyChangeFlag: boolean = false;
  branchChangeFlag: boolean = false;
  roleChangeFlag: boolean = false;
  employeeChangeFlag: boolean = false;
  validateStepOneFlag: boolean = false;
  validateEmployeeFlag: boolean = false;
  toggleValidateEmployee: boolean = false;

  validateStepFiveResult: boolean = true;
  isViewPage: boolean = false;
  constructor(
    private service: SysUserTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: SysUserTableItemCustomComponent,
    private validationService: ValidationService
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];

    this.stepItems = transformLabel([
      { label: 'LABEL.USER_INFO' },
      { label: 'LABEL.MAP_COMPANY_TO_USER' },
      { label: 'LABEL.MAP_BRANCH_TO_USER' },
      { label: 'LABEL.MAP_ROLE_TO_USER' },
      { label: 'LABEL.MAP_EMPLOYEE_TO_USER' }
    ]);

    this.custom.userFormAsyncRunnerDoneSubject.subscribe((value) => {
      this.userFormAsyncRunnerDone = value;
      if (value && this.userFormAsyncRunnerDone) {
        // this.userForm.getUserFormValidation();
        this.initUserFormValues();
      }
    });
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
    this.userForm.accessRight = this.accessRight;
  }
  onEnumLoader(): void {}
  getById(): Observable<SysUserTableItemView> {
    return this.service.getSysUserTableById(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions(result);
        this.userForm.vwModel = result;
        this.userForm.parentAccessRight = this.accessRight;
        this.userForm.setInitialUpdatingData();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.onAsyncRunner();
    this.model.id = EmptyGuid;
    super.setDefaultValueSystemFields();
  }
  onAsyncRunner(model?: any): void {
    this.asyncRunnerDone = false;
    forkJoin(
      this.baseDropdown.getCompanyDropDown(),
      this.baseDropdown.getBranchToDropDown(),
      this.baseDropdown.getSysRoleTableDropDown()
      // this.custom.getAdUserOptions(model)
    ).subscribe(
      ([
        company,
        branch,
        role
        // adUser
      ]) => {
        this.companyOptions = company;
        this.branchOptions = branch;
        this.roleOptions = role;
        // this.adUserOptions = adUser;
        // if (!isUndefinedOrZeroLength(this.userForm)) {
        //   this.userForm.adUserNameOptions = this.adUserOptions;
        // }
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.initIntermediateValues();
        this.initUserFormValues();
        this.setFieldAccessing();
        this.asyncRunnerDone = true;
      },
      (error) => {
        this.asyncRunnerDone = true;
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isViewPage = res;
    });
    this.custom.getFieldAccessing(this.model, this.isView).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {}
  setFunctionOptions(model: SysUserTableItemView): void {
    let disableActiveUser: boolean = false;
    let disableInactiveUser: boolean = false;

    // set disable functions
    disableActiveUser = model.inActive ? false : true;
    disableInactiveUser = model.inActive ? true : false;

    this.functionItems = [
      {
        label: 'LABEL.ACTIVE',
        disabled: disableActiveUser,
        command: () =>
          this.toFunction({
            path: ROUTE_FUNCTION_GEN.ACTIVE_USER,
            parameters: { sysUserTableGUID: model.id, inActive: false, title: 'LABEL.ACTIVE_USER', confirmMessage: 'CONFIRM.00035' }
          })
      },
      {
        label: 'LABEL.IN_ACTIVE',
        disabled: disableInactiveUser,
        command: () =>
          this.toFunction({
            path: ROUTE_FUNCTION_GEN.INACTIVE_USER,
            parameters: { sysUserTableGUID: model.id, inActive: true, title: 'LABEL.INACTIVE_USER', confirmMessage: 'CONFIRM.00036' }
          })
      }
    ];
    super.setFunctionOptionsMapping();
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    this.prepareDataForSubmit();
    if (this.isUpdateMode) {
      this.onUpdate(this.service.updateSysUserTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createSysUserTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    if (this.currentIndex === 4) {
      this.errMsg = [];
      this.updateIntermediateEmployees();
      //this.toggle

      const validateBranch = this.validateThirdStep();
      const validateRole = this.validateFourthStep();

      return this.validateFifthStep().pipe(
        map((result) => {
          return validateRole && validateBranch && this.validateStepFiveResult;
        })
      );
    } else {
      return of(false);
    }
    // return this.custom.getDataValidation();
  }
  onUpdate($update: Observable<ResponseModel>, isClosing: boolean) {
    $update.subscribe((result) => {
      this.id = result.key;
      result.model.guidStamp = this.model['guidStamp'];
      this.model = result.model;
      modelRegister(this.model);
      this.notificationService.showSaveSuccess();
      this.custom.updateCurrentUserSettings(result);
      isClosing ? this.toList() : this.toReload();
    });
  }
  // field change ##############################################################
  onEmployeeChange(e) {
    this.targetEmployee = e;
    this.employeeChangeFlag = true;
    this.validateEmployeeFlag = false;
    this.validateEmployee().subscribe((result) => {
      this.toggleValidateEmployee = !result;
      this.validateEmployeeFlag = true;
    });
  }
  onUserFormChange(formModel: SysUserTableItemView) {
    this.model = formModel;
  }
  onCompanyPickListChange(event) {
    this.companyChangeFlag = true;
  }
  onBranchPickListChange(event) {
    this.branchChangeFlag = true;
  }
  onRolePickListChange(event) {
    this.roleChangeFlag = true;
  }
  onStepOneAsyncRunnerDone(value: boolean) {}
  // validate field ############################################################
  validateEmployeeOnField(): TranslateModel {
    // if (isUndefinedOrZeroLength(this.allEmployees)) {
    //   return null;
    // }
    if (isUndefinedOrZeroLength(this.employeeTableOption)) {
      return null;
    }
    if (!this.toggleValidateEmployee) {
      return null;
    } else {
      let empLabel = this.employeeTableOption.find((item) => item.value === this.targetEmployee).label;
      return {
        code: 'ERROR.00307',
        parameters: [empLabel]
      };
    }
  }
  // ======== stepper ============
  initIntermediateValues(): void {
    // init intermediate branch
    if (!isUndefinedOrZeroLength(this.model.sysUserCompanyMappingList)) {
      this.convertCompanyMappingsToIntermediate();
    }
    // init intermediate role
    if (!isUndefinedOrZeroLength(this.model.sysUserRolesViewList)) {
      this.convertUserRolesToIntermediate();
    }
    // init intermediate employee
    if (!isUndefinedOrZeroLength(this.model.employeeMappingList)) {
      // if (!isUndefinedOrZeroLength(this.allEmployees)) {
      this.convertEmployeeMappingToIntermediate();
      // }
    }

    this.initCompanyPickList();
  }
  onStepsChange(index: number): void {
    this.selectedIndex = index;
  }
  onStepperClick(event): void {
    this.validateStepOneFlag = false;
    // clicked to different step
    if (this.selectedIndex !== this.currentIndex) {
      if (this.currentIndex === 0) {
        // validate step 0
        this.errMsg = [];
        // force stepper to stay at step0 until validated
        let selected = this.selectedIndex;
        this.userFormAsyncRunnerDone = false;
        this.forceClickToIndex(event, 0);

        const isUserFormValid = this.validateFirstStep();
        if (isUserFormValid) {
          if (selected > 1) {
            // validate step 1
            const isCompanyMappingValid = this.validateSecondStep();
            if (isCompanyMappingValid) {
              // force to selected index
              // init step selected index
              this.forceClickToIndex(event, selected);
              this.initCurrentIndex();
            } else {
              // force to step 1
              this.forceClickToIndex(event, 1);
            }
          } else if (selected === 1) {
            this.forceClickToIndex(event, 1);
            this.initCurrentIndex();
          }
          // else {
          //   // step backwards
          //   this.currentIndex = this.selectedIndex;
          // }
        } else {
          // user form not valid
          if (!isUndefinedOrZeroLength(this.userForm)) {
            this.userForm.model = this.model;
            this.initUserFormValues();
          }
          // call formValidation

          this.validateStepOneFlag = true;
          // this.userFormAsyncRunnerDone = false;
          this.forceClickToIndex(event, 0);
        }
        // end currentIndex === 0
      } else if (this.currentIndex === 1) {
        if (this.selectedIndex < this.currentIndex) {
          // step backwards
          this.currentIndex = this.selectedIndex;
          this.errMsg = [];
          // this.initUserFormValues();
        } else {
          this.errMsg = [];
          let isCompanyMappingValid = this.validateSecondStep();
          if (isCompanyMappingValid) {
            this.updateIntermediateKeys();
            this.forceClickToIndex(event, this.selectedIndex);
            this.initCurrentIndex();
          } else {
            this.forceClickToIndex(event, 1);
          }
        }
      } else if (this.currentIndex > 1) {
        this.updateIntermediateCurrentIndex();

        this.currentIndex = this.selectedIndex;
        this.initCurrentIndex();
      }
    } else {
      // selectedIndex === currentIndex
      this.currentIndex = this.selectedIndex;
    }
  }
  next(): void {
    if (this.currentIndex === 0) {
      if (!this.validateFirstStep()) {
        if (!isUndefinedOrZeroLength(this.userForm)) {
          this.userForm.model = this.model;
        }
        this.validateStepOneFlag = true;
        return;
      } else {
        this.activeIndex++;
        this.currentIndex = this.currentIndex + 1;
        this.initCurrentIndex();
      }
    } else if (this.currentIndex !== 0 && this.currentIndex < 2) {
      if (!this.validateCurrentIndex()) {
        return;
      } else {
        this.updateIntermediateCurrentIndex();
      }
      this.activeIndex++;
      this.currentIndex = this.currentIndex < 4 ? this.currentIndex + 1 : 0;
      this.initCurrentIndex();
    } else {
      if (this.currentIndex !== 0) {
        this.updateIntermediateCurrentIndex();
      }

      this.activeIndex++;
      this.currentIndex = this.currentIndex < 4 ? this.currentIndex + 1 : 0;
      this.initCurrentIndex();
    }
  }
  prev(): void {
    this.updateIntermediateCurrentIndex();
    this.activeIndex--;
    this.currentIndex = this.currentIndex > 0 ? this.currentIndex - 1 : 4;
    this.initCurrentIndex();
  }
  onCompanyChange(event, mapping): void {
    this.selectedCompanyLabel = this.companyOptions.find((item) => item.value === this.selectedCompany).label;
    if (!isNullOrUndefined(this.prevSelectedCompany)) {
      this.updateIntermediateCurrentIndex();
    }
    this.prepareCurrentIndexInput();
    this.prevSelectedCompany = event.value;
    this.prevSelectedCompanyLabel = this.companyOptions.find((item) => item.value === this.prevSelectedCompany).label;

    if (this.currentIndex === 4) {
      this.custom
        .getEmployeeTableOptions(this.selectedCompany)
        //.finally(() => {})
        .subscribe(
          (result) => {
            this.employeeTableOption = result;
            this.isEmployeeOptionsLoaded = true;
            this.convertEmployeeMappingToIntermediate();
          },
          (error) => {
            this.notificationService.showErrorMessageFromResponse(error);
          }
        );
    }
  }
  forceClickToIndex(event, index: number): void {
    this.currentIndex = index;
    this.stepper.itemClick(event, this.stepItems[index], index);
  }

  initCompanyPickList(): void {
    if (this.sourceCompanies.length === 0 && this.targetCompanies.length === 0) {
      if (isUpdateMode(this.id)) {
        this.prepareCompanyPickList();
      } else {
        if (this.targetCompanies.length === 0) {
          for (let i = 0; i < this.companyOptions.length; i++) {
            this.sourceCompanies.push(this.companyOptions[i]);
          }
        }
      }
    }
  }
  initUserFormValues(): void {
    if (!isUndefinedOrZeroLength(this.userForm)) {
      this.userForm.accessRight = this.accessRight;
      // this.userForm.adUserNameOptions = this.adUserOptions;
      this.userForm.setFieldAccessing();
    }
  }
  initFirstStep(): void {
    if (!isUndefinedOrZeroLength(this.userForm)) {
      this.initUserFormValues();
      this.userForm.model = this.model;
    }
  }
  initSecondStep(): void {
    this.initCompanyPickList();
  }
  initThirdStep(): void {
    this.prevSelectedCompany = null;
    this.selectedCompany = null;
    this.selectedCompanyLabel = null;
    this.sourceBranches = [];
    this.targetBranches = [];
  }
  initFourthStep(): void {
    this.prevSelectedCompany = null;
    this.selectedCompany = null;
    this.selectedCompanyLabel = null;
    this.sourceRoles = [];
    this.targetRoles = [];
  }
  initFifthStep(): void {
    this.prevSelectedCompany = null;
    this.selectedCompany = null;
    this.selectedCompanyLabel = null;
    this.targetEmployee = null;

    this.toggleValidateEmployee = false;
    this.validateEmployeeFlag = false;
    let that = this;
    setTimeout(function () {
      that.validateEmployeeFlag = true;
    }, 10);

    this.employeeTableOption = [];
  }
  initCurrentIndex() {
    this.errMsg = [];
    switch (this.currentIndex) {
      case 0:
        this.initFirstStep();
        break;
      case 1:
        // this.initSecondStep();
        break;
      case 2:
        this.initThirdStep();
        break;
      case 3:
        this.initFourthStep();
        break;
      case 4:
        this.initFifthStep();
        break;
      default:
        // do nothing
        break;
    }
  }
  checkPickListDirty() {
    return this.companyChangeFlag || this.branchChangeFlag || this.roleChangeFlag;
  }

  validateCurrentIndex(): boolean {
    switch (this.currentIndex) {
      // case 0:
      //   return this.validateFirstStep();
      case 1:
        return this.validateSecondStep();
      case 2:
        return this.validateThirdStep();
      case 3:
        return this.validateFourthStep();
    }
    return false;
  }

  validateUserName(): boolean {
    if (isUndefinedOrZeroLength(this.model.userName)) {
      return false;
    }

    return true;
  }
  validateEmployee(): Observable<boolean> {
    if (isUndefinedOrZeroLength(this.targetEmployee)) {
      return of(true);
    } else {
      let itemToValidate = new UserEmployeeMappingView();
      itemToValidate.employeeTableGUID = this.targetEmployee;
      if (isUpdateMode(this.id)) {
        itemToValidate.userId = this.id;
      }
      return this.service.validateUserEmployeeMapping(itemToValidate).pipe(
        map((result) => {
          return result;
        })
      );
      // return of(false);
    }
  }
  validateFirstStep(): boolean {
    let validateUserName: boolean = this.validateUserName();
    this.validateStepOneFlag = validateUserName;

    return validateUserName;
    // return this.validationService.getFormValidation();
    // return this.userForm.getUserFormValidation();
  }
  validateSecondStep(): boolean {
    let msg = this.translate.instant('ERROR.00075');
    if (isUndefinedOrZeroLength(this.targetCompanies)) {
      this.errMsg.push({ code: 'ERROR.00075', message: msg });
      return false;
    }
    const index = this.errMsg.findIndex((item) => item.code === 'ERROR.00075');
    if (index > -1) {
      this.errMsg.splice(index, 1);
    }
    return true;
  }
  validateThirdStep(): boolean {
    let result = false;
    if (isNullOrUndefined(this.intermediateBranches)) {
      result = false;
    } else {
      let len = this.targetCompanies.length;
      result = true;
      for (let i = 0; i < len; i++) {
        let companyGUID = this.targetCompanies[i].value;
        let interBranch = this.intermediateBranches[companyGUID];
        if (isUndefinedOrZeroLength(interBranch)) {
          result = false;
          break;
        }
      }
    }
    let msg = this.translate.instant('ERROR.00076');
    if (result === false) {
      this.errMsg.push({ code: 'ERROR.00076', message: msg });
      return false;
    }
    const index = this.errMsg.findIndex((item) => item.code === 'ERROR.00076');
    if (index > -1) {
      this.errMsg.splice(index, 1);
    }
    return true;
  }
  validateFourthStep(): boolean {
    let result = false;
    if (isNullOrUndefined(this.intermediateRoles)) {
      result = false;
    } else {
      let len = this.targetCompanies.length;
      result = true;
      for (let i = 0; i < len; i++) {
        let companyGUID = this.targetCompanies[i].value;
        let interRole = this.intermediateRoles[companyGUID];
        if (isUndefinedOrZeroLength(interRole)) {
          result = false;
          break;
        }
      }
    }
    let msg = this.translate.instant('ERROR.00077');
    if (result === false) {
      this.errMsg.push({ code: 'ERROR.00077', message: msg });
      return false;
    }
    const index = this.errMsg.findIndex((item) => item.code === 'ERROR.00077');
    if (index > -1) {
      this.errMsg.splice(index, 1);
    }
    return true;
  }
  validateFifthStep(): Observable<UserEmployeeMappingView[]> {
    const notnullValidate = this.validateNotNullFifthStep();
    let itemsToValidate = this.convertIntermediateToUserEmployeeMappingList();
    return this.service.validateUserEmployeeMappings(itemsToValidate).pipe(
      tap((result) => {
        if (!isUndefinedOrZeroLength(result)) {
          let msg = this.translate.instant('ERROR.00659');
          this.errMsg.push({ code: 'ERROR.00659', message: msg });
          this.validateStepFiveResult = notnullValidate && false;
        } else {
          const index = this.errMsg.findIndex((item) => item.code === 'ERROR.00659');
          if (index > -1) {
            this.errMsg.splice(index, 1);
          }
          this.validateStepFiveResult = notnullValidate && true;
        }
      })
    );
    //return of(null);
  }
  validateNotNullFifthStep(): boolean {
    let result = false;
    if (isNullOrUndefined(this.intermediateEmployees)) {
      result = false;
    } else {
      const len = this.targetCompanies.length;
      result = true;
      for (let i = 0; i < len; i++) {
        const companyGUID = this.targetCompanies[i].value;
        const interEmp = this.intermediateEmployees[companyGUID];
        if (isUndefinedOrZeroLength(interEmp)) {
          result = false;
          break;
        }
      }
    }
    const msg = this.translate.instant('ERROR.90072');
    if (result === false) {
      this.errMsg.push({ code: 'ERROR.90072', message: msg });
      return false;
    }
    const index = this.errMsg.findIndex((item) => item.code === 'ERROR.90072');
    if (index > -1) {
      this.errMsg.splice(index, 1);
    }
    return true;
  }
  prepareCurrentIndexInput() {
    switch (this.currentIndex) {
      case 2: // step = map branch
        this.prepareBranchPickList();
        break;
      case 3:
        this.prepareRolePickList();
        break;
      case 4:
        this.prepareEmployeeDropdown();
        break;
      default:
        // do nothing
        break;
    }
  }
  prepareEmployeeDropdown() {
    this.errMsg = [];
    this.toggleValidateEmployee = false;
    this.validateEmployeeFlag = false;
    let that = this;
    setTimeout(function () {
      that.validateEmployeeFlag = true;
    }, 10);

    // if (!isUndefinedOrZeroLength(this.allEmployees)) {
    const companyGUID = this.selectedCompany;
    // this.employeeTableOption = this.allEmployees.filter((item) => item.rowData.companyGUID === companyGUID);
    let interEmp = this.intermediateEmployees[companyGUID];
    if (!isUndefinedOrZeroLength(interEmp)) {
      // this.targetEmployee = interEmp;
      this.onEmployeeChange(interEmp);
    } else {
      this.targetEmployee = null;
    }
    // } else {
    //   this.employeeTableOption = [];
    //   this.targetEmployee = null;
    // }
  }
  prepareCompanyPickList() {
    let companies = Object.getOwnPropertyNames(this.intermediateBranches);
    this.targetCompanies = this.companyOptions.filter((x) => companies.some((y) => y === x.value));
    this.sourceCompanies = this.companyOptions.filter((x) => !companies.some((y) => y === x.value));
  }
  prepareBranchPickList() {
    const companyGUID = this.selectedCompany;
    let branches = this.branchOptions.filter((item) => item['rowData'].companyGUID === companyGUID);
    if (isNullOrUndefined(this.intermediateBranches)) {
      this.targetBranches = [];
      this.sourceBranches = branches;
      return;
    }
    let interBranch: SelectItems[] = this.intermediateBranches[companyGUID];
    if (!isNullOrUndefined(interBranch)) {
      this.targetBranches = branches.filter((x) => interBranch.some((y) => y.value === x.value));
      this.sourceBranches = branches.filter((x) => !interBranch.some((y) => y.value === x.value));
    } else {
      this.targetBranches = [];
      this.sourceBranches = branches;
    }
  }

  prepareRolePickList() {
    const companyGUID = this.selectedCompany;
    let roles = this.roleOptions.filter((item) => item['rowData'].companyGUID === companyGUID);
    if (isNullOrUndefined(this.intermediateRoles)) {
      this.targetRoles = [];
      this.sourceRoles = roles;
      return;
    }
    let interRole: SelectItems[] = this.intermediateRoles[companyGUID];
    if (!isNullOrUndefined(interRole)) {
      this.targetRoles = roles.filter((x) => interRole.some((y) => y.value === x.value));
      this.sourceRoles = roles.filter((x) => !interRole.some((y) => y.value === x.value));
    } else {
      this.targetRoles = [];
      this.sourceRoles = roles;
    }
  }
  updateIntermediateCurrentIndex() {
    switch (this.currentIndex) {
      case 1: // step = map company
        this.updateIntermediateKeys();
        break;
      case 2: // step = map branch
        this.updateIntermediateBranches();
        break;
      case 3: // step = map role
        this.updateIntermediateRoles();
        break;
      case 4: // step = map employee
        this.updateIntermediateEmployees();
      default:
        // do nothing;
        break;
    }
  }
  updateIntermediateKeys() {
    if (!isNullOrUndefined(this.intermediateBranches) && !isNullOrUndefined(this.intermediateRoles)) {
      let interBranches = {};
      let interRoles = {};
      for (let i = 0; i < this.targetCompanies.length; i++) {
        let companyGUID = this.targetCompanies[i].value;
        interBranches[companyGUID] = this.intermediateBranches[companyGUID];
        interRoles[companyGUID] = this.intermediateRoles[companyGUID];
      }
      this.intermediateBranches = interBranches;
      this.intermediateRoles = interRoles;
    }
  }
  updateIntermediateBranches() {
    if (isNullOrUndefined(this.prevSelectedCompany)) {
      return;
    }
    let companyGUID = this.prevSelectedCompany;
    if (isNullOrUndefined(this.intermediateBranches)) {
      this.intermediateBranches = {};
    }
    this.intermediateBranches[companyGUID] = [];
    for (let i = 0; i < this.targetBranches.length; i++) {
      let item = this.targetBranches[i];
      this.intermediateBranches[companyGUID].push(item);
    }
  }
  updateIntermediateRoles() {
    if (isNullOrUndefined(this.prevSelectedCompany)) {
      return;
    }
    let companyGUID = this.prevSelectedCompany;
    if (isNullOrUndefined(this.intermediateRoles)) {
      this.intermediateRoles = {};
    }
    this.intermediateRoles[companyGUID] = [];
    for (let i = 0; i < this.targetRoles.length; i++) {
      let item = this.targetRoles[i];
      this.intermediateRoles[companyGUID].push(item);
    }
  }
  updateIntermediateEmployees() {
    if (isNullOrUndefined(this.prevSelectedCompany)) {
      return;
    }
    let companyGUID = this.prevSelectedCompany;
    if (isNullOrUndefined(this.intermediateEmployees)) {
      this.intermediateEmployees = {};
    }
    this.intermediateEmployees[companyGUID] = this.targetEmployee;
  }
  prepareDataForSubmit() {
    // prepare data for submit
    let companymappings = this.convertIntermediateToSysUserCompanyMappingList();
    let userroles = this.convertIntermediateToSysUserRolesList();

    let employees = this.convertIntermediateToUserEmployeeMappingList();

    this.model.sysUserCompanyMappingList = companymappings;
    this.model.sysUserRolesViewList = userroles;

    this.model.employeeMappingList = employees;
  }

  convertCompanyMappingsToIntermediate() {
    this.intermediateBranches = {};
    let sysusercompanymappings = this.model.sysUserCompanyMappingList;
    const len = sysusercompanymappings.length;
    for (let i = 0; i < len; i++) {
      let item = sysusercompanymappings[i];
      let company: CompanyItemView = new CompanyItemView();
      let branch: BranchItemView = new BranchItemView();

      branch.branchGUID = item.branchGUID;
      branch.branchId = item.branch_BranchId;
      branch.name = item.branch_Name;
      branch.companyGUID = item.companyGUID;

      let selectItem: SelectItems = this.branchOptions.find((item) => item.value === branch.branchGUID);

      company.companyGUID = item.companyGUID;
      company.companyId = item.company_Name;
      company.name = item.company_Name;

      if (isNullOrUndefined(this.intermediateBranches[company.companyGUID])) {
        this.intermediateBranches[company.companyGUID] = [];
      }

      if (!isUndefinedOrZeroLength(selectItem)) {
        this.intermediateBranches[company.companyGUID].push(selectItem);
      }
    }
  }
  convertIntermediateToSysUserCompanyMappingList(): SysUserCompanyMappingView[] {
    const companyLength = this.targetCompanies.length;
    let result: SysUserCompanyMappingView[] = [];
    // company
    for (let i = 0; i < companyLength; i++) {
      let companyItem = this.targetCompanies[i];
      let companyGUID = companyItem.value;
      let branches = this.intermediateBranches[companyGUID];
      // branch
      for (let j = 0; j < branches.length; j++) {
        let branchItem = branches[j];
        let companyMapping: SysUserCompanyMappingView = new SysUserCompanyMappingView();
        companyMapping.companyGUID = companyItem.value;
        companyMapping.company_CompanyId = companyItem['rowData'].companyId;
        companyMapping.company_Name = companyItem['rowData'].name;
        companyMapping.branchGUID = branchItem.value;
        companyMapping.branch_BranchId = branchItem['rowData'].branchId;
        companyMapping.branch_Name = branchItem['rowData'].name;
        companyMapping.userGUID = this.id;
        if (isUpdateMode(this.id)) {
          companyMapping.sysUserTable_Name = this.model.name;
          companyMapping.sysUserTable_UserName = this.model.userName;
        }
        result.push(companyMapping);
      }
    }
    return result;
  }
  convertUserRolesToIntermediate() {
    this.intermediateRoles = {};
    let sysuserroles = this.model.sysUserRolesViewList;
    const len = sysuserroles.length;
    for (let i = 0; i < len; i++) {
      let item = sysuserroles[i];
      let company: CompanyItemView = new CompanyItemView();
      let role: SysRoleTableItemView = new SysRoleTableItemView();
      role.id = item.roleId;
      role.displayName = item.sysRoleTable_DisplayName;
      role.name = item.sysRoleTable_Name;
      role.companyGUID = item.sysRoleTable_CompanyGUID;
      role.companyId = item.sysRoleTable_Company_CompanyId;
      // role.company_Name = item.sysRoleTable_Company_Name;

      role.siteLoginType = item.sysRoleTable_SiteLoginType;

      const selectItem = { label: role.displayName, value: role.id, rowData: role };
      company.companyGUID = item.sysRoleTable_CompanyGUID;
      company.companyId = item.sysRoleTable_Company_CompanyId;
      company.name = item.sysRoleTable_Company_Name;

      if (isNullOrUndefined(this.intermediateRoles[company.companyGUID])) {
        this.intermediateRoles[company.companyGUID] = [];
      }
      this.intermediateRoles[company.companyGUID].push(selectItem);
    }
  }
  convertIntermediateToSysUserRolesList(): SysUserRolesView[] {
    const companyLength = this.targetCompanies.length;
    let result: SysUserRolesView[] = [];
    // company
    for (let i = 0; i < companyLength; i++) {
      let companyItem = this.targetCompanies[i];
      let companyGUID = companyItem.value;
      let roles = this.intermediateRoles[companyGUID];
      // role
      for (let j = 0; j < roles.length; j++) {
        let roleItem: SysRoleTableItemView = roles[j]['rowData'];
        let userrole: SysUserRolesView = new SysUserRolesView();
        userrole.roleId = roleItem.id;
        userrole.sysRoleTable_Name = roleItem.name;
        userrole.sysRoleTable_DisplayName = roleItem.displayName;
        userrole.sysRoleTable_CompanyGUID = roleItem.companyGUID;
        userrole.sysRoleTable_Company_CompanyId = roleItem.companyId;
        // userrole.sysRoleTable_Company_Name = roleItem.company_Name;
        userrole.userId = this.id;
        userrole.sysRoleTable_SiteLoginType = roleItem.siteLoginType;

        if (isUpdateMode(this.id)) {
          userrole.sysUserTable_UserName = this.model.userName;
        }
        result.push(userrole);
      }
    }
    return result;
  }
  // convert from UserEmployeeMappingView to intermediate values
  convertEmployeeMappingToIntermediate() {
    this.intermediateEmployees = {};
    if (isUndefinedOrZeroLength(this.model.employeeMappingList)) {
      this.model.employeeMappingList = [];
    }
    let employeeMappings = this.model.employeeMappingList;

    const len = employeeMappings.length;
    for (let i = 0; i < len; i++) {
      let item = employeeMappings[i];
      // let emp = this.employeeTableOption.find((e) => e.value === item.employeeTableGUID);

      // if (!isUndefinedOrZeroLength(emp)) {
      this.intermediateEmployees[item.companyGUID] = item.employeeTableGUID;
      // }
    }
  }
  // convert intermediates back to UserEmployeeMappings
  convertIntermediateToUserEmployeeMappingList(): EmployeeTableItemView[] {
    const companyLength = this.targetCompanies.length;
    let result: EmployeeTableItemView[] = [];
    // company
    for (let i = 0; i < companyLength; i++) {
      let companyItem = this.targetCompanies[i];
      let companyGUID = companyItem.value;
      let empGuid = this.intermediateEmployees[companyGUID];
      if (!isUndefinedOrZeroLength(empGuid)) {
        // employee
        let employee: EmployeeTableItemView = new EmployeeTableItemView();
        employee.employeeTableGUID = empGuid;
        employee.companyGUID = companyGUID;
        if (isUpdateMode(this.id)) {
          employee.userId = this.id;
        }
        result.push(employee);
      }
    }
    return result;
  }
  resetWizard() {
    this.activeIndex = 0;
    this.currentIndex = 0;

    this.sourceCompanies = [];
    this.targetCompanies = [];

    this.selectedCompany = undefined;
    this.prevSelectedCompany = undefined;

    this.sourceBranches = [];
    this.targetBranches = [];

    this.intermediateBranches = {};
    this.intermediateRoles = {};
    this.intermediateEmployees = {};

    this.sourceRoles = [];
    this.targetRoles = [];

    this.employeeTableOption = [];
    this.targetEmployee = null;

    this.selectedIndex = 0;

    this.errMsg = [];

    this.companyChangeFlag = false;
    this.branchChangeFlag = false;
    this.roleChangeFlag = false;
    this.employeeChangeFlag = false;
  }
}
