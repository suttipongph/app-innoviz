import { Observable, of, Subject } from 'rxjs';
import { FieldAccessing, BaseServiceModel, SelectItems } from 'shared/models/systemModel';
import { AccessLevelModel, SysUserTableItemView } from 'shared/models/viewModel';
import { isUpdateMode, isUndefinedOrZeroLength } from 'shared/functions/value.function';
import { AppConst } from 'shared/constants';
import { tap, catchError, switchMap } from 'rxjs/operators';

const firstGroup = ['USER_NAME'];
const systemGroup = ['IN_ACTIVE'];
const allFields = ['USER_NAME', 'NAME', 'EMAIL', 'PHONE_NUMBER', 'IN_ACTIVE', 'LANGUAGE'];
export class SysUserTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  isRequired: boolean = true;
  userFormAsyncRunnerDoneSubject = new Subject<boolean>();
  userAccessRightSubject = new Subject<AccessLevelModel>();
  stepOneIsViewSubject = new Subject<boolean>();
  adUserOptions: SelectItems[] = [];
  getFieldAccessing(model: SysUserTableItemView, isView: boolean): Observable<FieldAccessing[]> {
    if (isView) {
      this.isRequired = false;
      return of([{ filedIds: allFields, readonly: true }]);
    } else {
      const fieldAccessing: FieldAccessing[] = [];
      fieldAccessing.push({ filedIds: systemGroup, readonly: true });
      if (isUpdateMode(model.id)) {
        fieldAccessing.push({ filedIds: firstGroup, readonly: true });
        this.isRequired = false;
      } else {
        fieldAccessing.push({ filedIds: firstGroup, readonly: false });
        this.isRequired = true;
      }
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: SysUserTableItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getAdUserOptions(model: SysUserTableItemView): Observable<SelectItems[]> {
    if (!isUndefinedOrZeroLength(model) && isUpdateMode(model.id)) {
      return of([{ label: model.userName, value: model.userName }]);
    } else {
      return this.baseService.baseDropdown.getAdUserDropDown();
    }
  }
  getEmployeeTableOptions(companyGUID: string): Observable<SelectItems[]> {
    if (!isUndefinedOrZeroLength(companyGUID)) {
      return this.baseService.baseDropdown.getEmployeeTableByCompanyDropDown(companyGUID);
    } else {
      return of([]);
    }
  }
  updateCurrentUserSettings(user): void {
    if (this.baseService.userDataService.getUserIdFromToken() === user.id) {
      if (!isUndefinedOrZeroLength(user.languageId)) {
        this.baseService.userDataService.setAppLanguage(user.languageId);
        this.baseService.translate.use(user.languageId);
        this.baseService.userDataService.languageChangeSubject.next(true);
      }

      // set show system log
      const showSystemLog = user.showSystemLog ? AppConst.TRUE : AppConst.FALSE;
      this.baseService.userDataService.setShowSystemLog(showSystemLog);
    }
  }
}
