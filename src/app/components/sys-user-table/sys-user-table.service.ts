import { Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { SysUserTableItemView, SysUserTableListView, UserEmployeeMappingView } from 'shared/models/viewModel';

@Injectable()
export class SysUserTableService {
  serviceKey = 'id';
  servicePath = '';

  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getSysUserTableToList(search: SearchParameter): Observable<SearchResult<SysUserTableListView>> {
    const url = `${this.servicePath}/GetSysUserTableList`;
    console.log('service.getSysUserTableToList', search)
    return this.dataGateway.getList(url, search);
  }
  deleteSysUserTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteSysUserTable`;
    return this.dataGateway.delete(url, row);
  }
  getSysUserTableById(id: string): Observable<SysUserTableItemView> {
    const url = `${this.servicePath}/GetSysUserTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createSysUserTable(vmModel: SysUserTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateSysUserTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateSysUserTable(vmModel: SysUserTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateSysUserTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  validateUserEmployeeMapping(parm: UserEmployeeMappingView): Observable<boolean> {
    const url = `${this.servicePath}/ValidateUserEmployeeMapping`;
    return this.dataGateway.post(url, parm);
  }
  validateUserEmployeeMappings(parm: UserEmployeeMappingView[]): Observable<UserEmployeeMappingView[]> {
    const url = `${this.servicePath}/ValidateUserEmployeeMappings`;
    return this.dataGateway.post(url, parm);
  }
}
