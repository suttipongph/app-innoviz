import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { SysUserTableListView } from 'shared/models/viewModel';
import { SysUserTableService } from '../sys-user-table.service';
import { SysUserTableListCustomComponent } from './sys-user-table-list-custom.component';

@Component({
  selector: 'sys-user-table-list',
  templateUrl: './sys-user-table-list.component.html',
  styleUrls: ['./sys-user-table-list.component.scss']
})
export class SysUserTableListComponent extends BaseListComponent<SysUserTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  sysLanguageOptions: SelectItems[] = [];
  defaultBitOptions: SelectItems[] = [];
  constructor(private service: SysUserTableService, public custom: SysUserTableListCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getSysLanguageEnumDropDown(this.sysLanguageOptions);
    this.baseDropdown.getDefaultBitDropDown(this.defaultBitOptions);
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'id';
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.USER_NAME',
        textKey: 'userName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.NAME',
        textKey: 'name',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.EMAIL',
        textKey: 'email',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PHONE_NUMBER',
        textKey: 'phoneNumber',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.LANGUAGE_ID',
        textKey: 'languageId',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.sysLanguageOptions
      },
      {
        label: 'LABEL.IN_ACTIVE',
        textKey: 'inActive',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defaultBitOptions
      }
      // {
      //   label: null,
      //   textKey: 'SysUserTable',
      //   type: ColumnType.CONTROLLER,
      //   visibility: false,
      //   sorting: SortType.NONE
      // }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<SysUserTableListView>> {
    return this.service.getSysUserTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteSysUserTable(row));
  }
}
