import { Observable, of } from 'rxjs';
import { isNullOrUndefined, isUndefinedOrZeroLength } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { SysUserTableActivationParm } from 'shared/models/viewModel';

const firstGroup = ['DOCUMENT_ID', 'NUMBER'];

export class InactiveUserCustomComponent {
  baseService: BaseServiceModel<any>;
  passedParam: SysUserTableActivationParm = new SysUserTableActivationParm();
  title: string = '';
  confirmMessage: string = '';
  getFieldAccessing(model: SysUserTableActivationParm): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<SysUserTableActivationParm> {
    if (isUndefinedOrZeroLength(this.passedParam)) {
      this.passedParam = new SysUserTableActivationParm();
    }
    return of(this.passedParam);
  }
  setPassParameter(passingObj: any): void {
    if (!isNullOrUndefined(passingObj.sysUserTableGUID)) {
      this.passedParam.sysUserTableGUID = passingObj.sysUserTableGUID;
    }
    if (!isNullOrUndefined(passingObj.inActive)) {
      this.passedParam.inActive = passingObj.inActive;
    }
    if (!isNullOrUndefined(passingObj.confirmMessage)) {
      this.confirmMessage = passingObj.confirmMessage;
    }
    if (!isNullOrUndefined(passingObj.title)) {
      this.title = passingObj.title;
    }
  }
}
