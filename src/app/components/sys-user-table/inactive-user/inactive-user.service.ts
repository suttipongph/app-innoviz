import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { SysUserTableActivationParm } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class InactiveUserService {
  serviceKey = 'cancelBatchInstanceGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getInactiveUserToList(search: SearchParameter): Observable<SearchResult<SysUserTableActivationParm>> {
    const url = `${this.servicePath}/GetInactiveUserList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteInactiveUser(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteInactiveUser`;
    return this.dataGateway.delete(url, row);
  }
  getInactiveUserById(id: string): Observable<SysUserTableActivationParm> {
    const url = `${this.servicePath}/GetInactiveUserById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createInactiveUser(vmModel: SysUserTableActivationParm): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateInactiveUser`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateInactiveUser(vmModel: SysUserTableActivationParm): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateInactiveUser`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }

  getInactiveUserInitialData(vwModel: SysUserTableActivationParm): Observable<SysUserTableActivationParm> {
    const url = `${this.servicePath}/GetInactiveUserInitialData`;
    return this.dataGateway.post(url, vwModel);
  }
  inactiveUser(vwModel: SysUserTableActivationParm): Observable<boolean> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vwModel);
  }
}
