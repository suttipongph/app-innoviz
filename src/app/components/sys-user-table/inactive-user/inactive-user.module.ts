import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InactiveUserService } from './inactive-user.service';
import { InactiveUserComponent } from './inactive-user/inactive-user.component';
import { InactiveUserCustomComponent } from './inactive-user/inactive-user-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [InactiveUserComponent],
  providers: [InactiveUserService, InactiveUserCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [InactiveUserComponent]
})
export class InactiveUserComponentModule {}
