import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SysUserTableService } from './sys-user-table.service';
import { SysUserTableListComponent } from './sys-user-table-list/sys-user-table-list.component';
import { SysUserTableItemComponent } from './sys-user-table-item/sys-user-table-item.component';
import { SysUserTableItemCustomComponent } from './sys-user-table-item/sys-user-table-item-custom.component';
import { SysUserTableListCustomComponent } from './sys-user-table-list/sys-user-table-list-custom.component';
import { SysUserTableFormComponent } from './sys-user-table-form/sys-user-table-form.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [SysUserTableListComponent, SysUserTableItemComponent, SysUserTableFormComponent],
  providers: [SysUserTableService, SysUserTableItemCustomComponent, SysUserTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [SysUserTableListComponent, SysUserTableItemComponent]
})
export class SysUserTableComponentModule {}
