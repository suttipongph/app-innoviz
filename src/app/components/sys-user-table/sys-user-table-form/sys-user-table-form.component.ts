import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode, isUndefinedOrZeroLength } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { SysUserTableService } from '../sys-user-table.service';
import { SysUserTableItemView, AccessLevelModel } from 'shared/models/viewModel';
import { SysUserTableItemCustomComponent } from '../sys-user-table-item/sys-user-table-item-custom.component';
import { ValidationService } from 'shared/services/validation.service';
@Component({
  selector: 'sys-user-table-form',
  templateUrl: './sys-user-table-form.component.html',
  styleUrls: ['./sys-user-table-form.component.scss']
})
export class SysUserTableFormComponent extends BaseItemComponent<SysUserTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  @Input() vwModel: SysUserTableItemView = new SysUserTableItemView();
  @Input() parentAccessRight: AccessLevelModel;
  @Input() id: string;
  @Output() formModelChange = new EventEmitter<SysUserTableItemView>();
  @Input()
  set validateStepOneFlag(value: boolean) {
    this.validateUserFormFlag = value;
    if (!isUndefinedOrZeroLength(this.adUserNameOptions) && this.validateUserFormFlag) {
      this.getFormValidation();
    }
  }
  validateUserFormFlag: boolean = false;
  sysLanguageOptions: SelectItems[] = [];
  adUserNameOptions: SelectItems[] = [];
  constructor(
    private service: SysUserTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: SysUserTableItemCustomComponent,
    private validationService: ValidationService
  ) {
    super();
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.custom.userAccessRightSubject.subscribe((res) => {
      this.accessRight = res;
      this.setFieldAccessing();
      if (this.validateUserFormFlag) {
        this.getUserFormValidation();
      }
    });
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    // super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getSysLanguageEnumDropDown(this.sysLanguageOptions);
  }
  getById(): Observable<SysUserTableItemView> {
    //return this.service.getSysUserTableById(this.id);
    return of(this.vwModel);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.model = this.vwModel;
    this.model.id = EmptyGuid;
    super.setDefaultValueSystemFields();
    this.onAsyncRunner();
  }
  onAsyncRunner(model?: any): void {
    forkJoin(this.custom.getAdUserOptions(model)).subscribe(
      ([adUserName]) => {
        this.adUserNameOptions = adUserName;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.custom.userFormAsyncRunnerDoneSubject.next(true);
        if (!isNullOrUndefined(this.parentAccessRight)) {
          this.accessRight = this.parentAccessRight;
        }
        this.setFieldAccessing();
        if (this.validateUserFormFlag) {
          this.getUserFormValidation();
        }
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    if (!isUndefinedOrZeroLength(this.accessRight)) {
      this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
        this.isView = res;
      });
      this.custom.getFieldAccessing(this.model, this.isView).subscribe((res) => {
        super.setBaseFieldAccessing(res);
      });
    }
  }
  setRelatedInfoOptions(): void {}
  setFunctionOptions(): void {}
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateSysUserTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createSysUserTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }

  getUserFormValidation(): boolean {
    return this.validationService.getFormValidation();
  }

  // field change ##############################################################
  onModelChange(e) {
    this.formModelChange.emit(this.model);
  }

  onUserNameChange(e) {
    const newValue = this.adUserNameOptions.find((val) => val.value === e);
    this.model.userName = e;
    this.model.normalizedUserName = !isUndefinedOrZeroLength(e) ? e.toUpperCase() : null;
    if (!isUndefinedOrZeroLength(newValue)) {
      this.model.name = newValue['rowData'].displayName;
      this.model.email = newValue['rowData'].emailAddress;
      this.model.normalizedEmail = !isUndefinedOrZeroLength(this.model.email) ? this.model.email.toUpperCase() : null;
      this.model.phoneNumber = newValue['rowData'].voiceTelephoneNumber;
    } else {
      this.model.name = null;
      this.model.email = null;
      this.model.normalizedEmail = null;
      this.model.phoneNumber = null;
    }
    // this.toggleValidateUserName = true;
    this.onModelChange(e);
  }
  onEmailChange(e) {
    this.model.email = e;
    this.model.normalizedEmail = e.toUpperCase();
    this.onModelChange(e);
  }
}
