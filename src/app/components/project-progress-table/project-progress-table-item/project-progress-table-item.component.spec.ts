import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectProgressTableItemComponent } from './project-progress-table-item.component';

describe('ProjectProgressTableItemViewComponent', () => {
  let component: ProjectProgressTableItemComponent;
  let fixture: ComponentFixture<ProjectProgressTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProjectProgressTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectProgressTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
