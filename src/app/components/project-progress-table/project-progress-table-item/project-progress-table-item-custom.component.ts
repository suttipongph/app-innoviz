import { isNull } from '@angular/compiler/src/output/output_ast';
import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { ProjectProgressTableItemView } from 'shared/models/viewModel';

const firstGroup = ['WITHDRAWAL_TABLE_GUID', 'PROJECT_PROGRESS_TABLE_GUID'];
const secGroup = ['PROJECT_PROGRESS_ID'];

export class ProjectProgressTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: ProjectProgressTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isNullOrUndefOrEmptyGUID(model.projectProgressTableGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      fieldAccessing.push({ filedIds: secGroup, readonly: true });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ProjectProgressTableItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<ProjectProgressTableItemView> {
    let model = new ProjectProgressTableItemView();
    model.projectProgressTableGUID = EmptyGuid;
    return of(model);
  }
}
