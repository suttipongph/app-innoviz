import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProjectProgressTableService } from './project-progress-table.service';
import { ProjectProgressTableListComponent } from './project-progress-table-list/project-progress-table-list.component';
import { ProjectProgressTableItemComponent } from './project-progress-table-item/project-progress-table-item.component';
import { ProjectProgressTableItemCustomComponent } from './project-progress-table-item/project-progress-table-item-custom.component';
import { ProjectProgressTableListCustomComponent } from './project-progress-table-list/project-progress-table-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ProjectProgressTableListComponent, ProjectProgressTableItemComponent],
  providers: [ProjectProgressTableService, ProjectProgressTableItemCustomComponent, ProjectProgressTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ProjectProgressTableListComponent, ProjectProgressTableItemComponent]
})
export class ProjectProgressTableComponentModule {}
