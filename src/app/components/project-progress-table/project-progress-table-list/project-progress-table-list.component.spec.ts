import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProjectProgressTableListComponent } from './project-progress-table-list.component';

describe('ProjectProgressTableListViewComponent', () => {
  let component: ProjectProgressTableListComponent;
  let fixture: ComponentFixture<ProjectProgressTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProjectProgressTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectProgressTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
