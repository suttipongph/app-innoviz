import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ProjectProgressTableItemView, ProjectProgressTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ProjectProgressTableService {
  serviceKey = 'projectProgressTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getProjectProgressTableToList(search: SearchParameter): Observable<SearchResult<ProjectProgressTableListView>> {
    const url = `${this.servicePath}/GetProjectProgressTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteProjectProgressTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteProjectProgressTable`;
    return this.dataGateway.delete(url, row);
  }
  getProjectProgressTableById(id: string): Observable<ProjectProgressTableItemView> {
    const url = `${this.servicePath}/GetProjectProgressTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createProjectProgressTable(vmModel: ProjectProgressTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateProjectProgressTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateProjectProgressTable(vmModel: ProjectProgressTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateProjectProgressTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<ProjectProgressTableItemView> {
    const url = `${this.servicePath}/GetProjectProgressInitialDataTable/id=${id}`;
    return this.dataGateway.get(url);
  }
}
