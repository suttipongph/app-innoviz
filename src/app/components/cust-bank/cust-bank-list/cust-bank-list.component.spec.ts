import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CustBankListComponent } from './cust-bank-list.component';

describe('CustBankListViewComponent', () => {
  let component: CustBankListComponent;
  let fixture: ComponentFixture<CustBankListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustBankListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustBankListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
