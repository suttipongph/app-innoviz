import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustBankService } from './cust-bank.service';
import { CustBankListComponent } from './cust-bank-list/cust-bank-list.component';
import { CustBankItemComponent } from './cust-bank-item/cust-bank-item.component';
import { CustBankItemCustomComponent } from './cust-bank-item/cust-bank-item-custom.component';
import { CustBankListCustomComponent } from './cust-bank-list/cust-bank-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CustBankListComponent, CustBankItemComponent],
  providers: [CustBankService, CustBankItemCustomComponent, CustBankListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CustBankListComponent, CustBankItemComponent]
})
export class CustBankComponentModule {}
