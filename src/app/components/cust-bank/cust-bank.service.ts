import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { CustBankItemView, CustBankListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CustBankService {
  serviceKey = 'custBankGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCustBankToList(search: SearchParameter): Observable<SearchResult<CustBankListView>> {
    const url = `${this.servicePath}/GetCustBankList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCustBank(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCustBank`;
    return this.dataGateway.delete(url, row);
  }
  getCustBankById(id: string): Observable<CustBankItemView> {
    const url = `${this.servicePath}/GetCustBankById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCustBank(vmModel: CustBankItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCustBank`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCustBank(vmModel: CustBankItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCustBank`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<CustBankItemView> {
    const url = `${this.servicePath}/GetCustBankInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
}
