import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CustBankItemView } from 'shared/models/viewModel';

const firstGroup = ['CUSTOMER_TABLE_GUID'];

export class CustBankItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CustBankItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.custBankGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CustBankItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(parentId: string): Observable<CustBankItemView> {
    let model: CustBankItemView = new CustBankItemView();
    model.custBankGUID = EmptyGuid;
    model.customerTableGUID = parentId;
    model.bankAccountControl = false;
    model.pdc = false;
    model.primary = false;
    model.inActive = false;
    return of(model);
  }
}
