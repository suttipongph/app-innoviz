import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustBankItemComponent } from './cust-bank-item.component';

describe('CustBankItemViewComponent', () => {
  let component: CustBankItemComponent;
  let fixture: ComponentFixture<CustBankItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustBankItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustBankItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
