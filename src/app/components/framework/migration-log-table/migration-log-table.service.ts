import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { MigrationLogTableView } from 'shared/models/viewModel/migrationLogTableView';
@Injectable()
export class MigrationLogTableService {
  serviceKey = 'id';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getMigrationLogTableToList(search: SearchParameter): Observable<SearchResult<MigrationLogTableView>> {
    const url = `${this.servicePath}/GetMigrationLogTableList`;
    return this.dataGateway.getList(url, search);
  }
}
