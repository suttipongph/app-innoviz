import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MigrationLogTableService } from './migration-log-table.service';
import { MigrationLogTableCustomComponent } from './migration-log-table/migration-log-table-custom.component';
import { MigrationLogTableComponent } from './migration-log-table/migration-log-table.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [MigrationLogTableComponent],
  providers: [MigrationLogTableService, MigrationLogTableCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [MigrationLogTableComponent]
})
export class MigrationLogTableComponentModule {}
