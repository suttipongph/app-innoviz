import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable, of } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import { stringToDate } from 'shared/functions/date.function';
import { isNullOrUndefOrEmpty } from 'shared/functions/value.function';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems,
  TranslateModel
} from 'shared/models/systemModel';
import { MigrationLogParm } from 'shared/models/viewModel/migrationLogParm';
import { MigrationLogTableView } from 'shared/models/viewModel/migrationLogTableView';
import { MigrationLogTableService } from '../migration-log-table.service';
import { MigrationLogTableCustomComponent } from './migration-log-table-custom.component';

@Component({
  selector: 'migration-log-table',
  templateUrl: './migration-log-table.component.html',
  styleUrls: ['./migration-log-table.component.scss']
})
export class MigrationLogTableComponent extends BaseListComponent<MigrationLogTableView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  migrationOption: SelectItems[] = [];
  advModel: MigrationLogParm = new MigrationLogParm();
  constructor(public custom: MigrationLogTableCustomComponent, private service: MigrationLogTableService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {
    this.baseDropdown.getMigrationTableDropDown(this.migrationOption);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.isAdvance = true;
    this.option.canCreate = false;
    this.option.canView = false;
    this.option.canDelete = false;
    this.option.key = 'id';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.MIGRATION_LOG_START_TIME',
        textKey: 'logStartDateTime',
        type: ColumnType.DATE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.MIGRATION_LOG_END_TIME',
        textKey: 'logEndDateTime',
        type: ColumnType.DATE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.MIGRATION_LOG_NAME',
        textKey: 'migrationName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.MIGRATION_LOG_MeSSAGE',
        textKey: 'logMessage',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      { label: null, textKey: 'id', type: ColumnType.STRING, visibility: false, sorting: SortType.ASC }
    ];
    const addvances: ColumnModel[] = [
      {
        label: null,
        textKey: 'migrationFromDate',
        type: ColumnType.VARIABLE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'migrationToDate',
        type: ColumnType.VARIABLE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'migrationTable',
        type: ColumnType.VARIABLE,
        visibility: true,
        sorting: SortType.NONE
      }
    ];

    this.option.columns = columns;
    this.option.advanceColumns = addvances;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<MigrationLogTableView>> {
    if (this.onValidateAll()) {
      return this.service.getMigrationLogTableToList(search);
    } else {
      return of(new SearchResult<MigrationLogTableView>());
    }
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {}
  // -------------------- onvalidate -----------------------------------------
  showErrorMessageFromManual(code: string) {
    const message: TranslateModel = { code: 'ERROR.ERROR' };
    const error: TranslateModel[] = [{ code: code }];
    this.notificationService.showErrorMessageFromManual(message, error);
  }
  onValidateAll() {
    const _mandatory = this.valdateMandatiry();
    const _date = this.onValidateDate();
    return _mandatory && _date;
  }
  valdateMandatiry() {
    if (isNullOrUndefOrEmpty(this.advModel.migrationTable)) {
      return false;
    }
    if (isNullOrUndefOrEmpty(this.advModel.migrationFromDate)) {
      return false;
    }
    if (isNullOrUndefOrEmpty(this.advModel.migrationToDate)) {
      return false;
    }
    return true;
  }
  onValidateDate() {
    if (
      stringToDate(this.advModel.migrationFromDate) > stringToDate(this.advModel.migrationToDate) &&
      !isNullOrUndefOrEmpty(this.advModel.migrationFromDate) &&
      !isNullOrUndefOrEmpty(this.advModel.migrationToDate)
    ) {
      this.showErrorMessageFromManual('ERROR.00475');
      return false;
    }
    return true;
  }
}
