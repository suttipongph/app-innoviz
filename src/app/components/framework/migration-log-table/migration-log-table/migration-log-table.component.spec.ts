import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MigrationLogTableComponent } from './migration-log-table.component';

describe('MigrationLogTableComponent', () => {
  let component: MigrationLogTableComponent;
  let fixture: ComponentFixture<MigrationLogTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MigrationLogTableComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MigrationLogTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
