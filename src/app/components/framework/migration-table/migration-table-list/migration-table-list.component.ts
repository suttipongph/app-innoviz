import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { MigrationTableListView } from 'shared/models/viewModel/migrationTableListView';
import { MigrationTableService } from '../migration-table.service';
import { MigrationTableListCustomComponent } from './migration-table-list-custom.component';

@Component({
  selector: 'migration-table-list',
  templateUrl: './migration-table-list.component.html',
  styleUrls: ['./migration-table-list.component.scss']
})
export class MigrationTableListComponent extends BaseListComponent<MigrationTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  constructor(public custom: MigrationTableListCustomComponent, private service: MigrationTableService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'migrationTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.MIGRATION_ID',
        textKey: 'migrationGUID',
        type: ColumnType.STRING,
        visibility: false,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.MIGRATION_GROUP_NAME',
        textKey: 'groupName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.MIGRATION_GROUP_ORDER',
        textKey: 'groupOrder',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.MIGRATION_TABLE_NAME',
        textKey: 'migrationName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.MIGRATION_TABLE_ORDER',
        textKey: 'migrationOrder',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.MIGRATION_MODIFY_DATE',
        textKey: 'modifiedDateTime',
        type: ColumnType.DATE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.MIGRATION_RESULT',
        textKey: 'migrationResult',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<MigrationTableListView>> {
    return this.service.getMigrationTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {}
}
