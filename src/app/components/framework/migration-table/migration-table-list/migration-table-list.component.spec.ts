import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MigrationTableListComponent } from './migration-table-list.component';

describe('MigrationTableListComponent', () => {
  let component: MigrationTableListComponent;
  let fixture: ComponentFixture<MigrationTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MigrationTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MigrationTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
