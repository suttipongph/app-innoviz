import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MigrationTableService } from './migration-table.service';
import { MigrationTableListComponent } from './migration-table-list/migration-table-list.component';
import { MigrationTableListCustomComponent } from './migration-table-list/migration-table-list-custom.component';
import { MigrationTableItemComponent } from './migration-table-item/migration-table-item.component';
import { MigrationTableItemCustomComponent } from './migration-table-item/migration-table-item-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [MigrationTableListComponent, MigrationTableItemComponent],
  providers: [MigrationTableService, MigrationTableItemCustomComponent, MigrationTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [MigrationTableListComponent, MigrationTableItemComponent]
})
export class MigrationTableComponentModule {}
