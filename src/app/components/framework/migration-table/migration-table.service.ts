import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { MigrationTableItemView } from 'shared/models/viewModel/migrationTableItemView';
import { MigrationTableListView } from 'shared/models/viewModel/migrationTableListView';
@Injectable()
export class MigrationTableService {
  serviceKey = 'migrationTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getMigrationTableToList(search: SearchParameter): Observable<SearchResult<MigrationTableListView>> {
    const url = `${this.servicePath}/GetMigrationTableList`;
    return this.dataGateway.getList(url, search);
  }
  getMigrationTableById(id: string): Observable<MigrationTableItemView> {
    const url = `${this.servicePath}/GetMigrationTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  getExecuteMigration(vmModel: MigrationTableItemView): Observable<MigrationTableItemView> {
    const url = `${this.servicePath}/GetExecuteMigration`;
    return this.dataGateway.post(url, vmModel);
  }
}
