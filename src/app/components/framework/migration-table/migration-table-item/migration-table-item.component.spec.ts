import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MigrationTableItemComponent } from './migration-table-item.component';

describe('MigrationItemComponent', () => {
  let component: MigrationTableItemComponent;
  let fixture: ComponentFixture<MigrationTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MigrationTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MigrationTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
