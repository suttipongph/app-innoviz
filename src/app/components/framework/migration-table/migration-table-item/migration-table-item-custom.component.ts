import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { MigrationTableItemView } from 'shared/models/viewModel/migrationTableItemView';
const PRODUCT_SETTLED_TRANS = 'ProductSettledTrans';

const firstGroup = ['MIGRATION_ID', 'MIGRATION_GROUP_NAME', 'MIGRATION_TABLE_NAME', 'MIGRATION_RESULT'];

export class MigrationTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  remark: string = null;
  getFieldAccessing(model: MigrationTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: MigrationTableItemView): Observable<boolean> {
    return of(false);
  }
  getInitialData(): Observable<MigrationTableItemView> {
    let model = new MigrationTableItemView();
    model.migrationTableGUID = EmptyGuid;
    return of(model);
  }
  setRemark(migrateName: any) {
    switch (migrateName) {
      case PRODUCT_SETTLED_TRANS:
        this.remark = 'LABEL.PRODUCT_SETTLED_TRANS_REMARK';
        break;
      default:
        this.remark = null;
        break;
    }
  }
}
