import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { UpdateChequeReferenceResultView, UpdateChequeReferenceView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class UpdateChequeReferenceService {
  serviceKey = 'updateChequeReferenceGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  updateChequeReference(): Observable<UpdateChequeReferenceResultView> {
    const url = `${this.servicePath}/UpdateChequeReference`;
    return this.dataGateway.get(url);
  }
  ValidateUpdateChequeReference(): Observable<boolean> {
    const url = `${this.servicePath}/ValidateUpdateChequeReference`;
    return this.dataGateway.get(url);
  }
}
