import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { UpdateChequeReferenceView } from 'shared/models/viewModel';

const firstGroup = ['LIST_CHEQUE_REF_TYPE', 'CHEQUE_REF_GUID'];

export class UpdateChequeReferenceCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: UpdateChequeReferenceView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return this.baseService.service.ValidateUpdateChequeReference();
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<UpdateChequeReferenceView> {
    let model = new UpdateChequeReferenceView();
    model.updateChequeReferenceGUID = EmptyGuid;
    return of(model);
  }
}
