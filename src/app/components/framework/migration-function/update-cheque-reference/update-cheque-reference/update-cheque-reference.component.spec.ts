import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateChequeReferenceComponent } from './update-cheque-reference.component';

describe('UpdateChequeReferenceViewComponent', () => {
  let component: UpdateChequeReferenceComponent;
  let fixture: ComponentFixture<UpdateChequeReferenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UpdateChequeReferenceComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateChequeReferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
