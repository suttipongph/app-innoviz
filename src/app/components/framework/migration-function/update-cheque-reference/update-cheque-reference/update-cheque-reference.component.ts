import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { Confirmation } from 'shared/models/primeModel';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { UpdateChequeReferenceView } from 'shared/models/viewModel';
import { UpdateChequeReferenceService } from '../update-cheque-reference.service';
import { UpdateChequeReferenceCustomComponent } from './update-cheque-reference-custom.component';
@Component({
  selector: 'update-cheque-reference',
  templateUrl: './update-cheque-reference.component.html',
  styleUrls: ['./update-cheque-reference.component.scss']
})
export class UpdateChequeReferenceComponent extends BaseItemComponent<UpdateChequeReferenceView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  listChequeRefTypeOptions: SelectItems[] = [];

  constructor(
    private service: UpdateChequeReferenceService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: UpdateChequeReferenceCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.listChequeRefTypeOptions);
  }
  getById(): Observable<UpdateChequeReferenceView> {
    return null;
  }
  getInitialData(): Observable<UpdateChequeReferenceView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    let result = new UpdateChequeReferenceView();
    result.chequeRefGUID = null;
    result.listChequeRefType = [45, 56];
    this.onAsyncRunner(result);
    this.setRelatedInfoOptions();
    this.setFunctionOptions();
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    const confirmation: Confirmation = {
      header: this.baseService.translate.instant('CONFIRM.CONFIRM'),
      message: this.baseService.translate.instant('CONFIRM.90017', [
        this.baseService.translate.instant('LABEL.REFERENCE_ID'),
        this.baseService.translate.instant('LABEL.CHEQUE')
      ])
    };
    this.baseService.notificationService.showConfirmDialog(confirmation);
    this.baseService.notificationService.isAccept.subscribe((isConfirm) => {
      if (isConfirm) {
        super.onExecuteFunction(this.service.updateChequeReference());
      }
    });
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onClose(): void {
    super.onBack();
  }
}
