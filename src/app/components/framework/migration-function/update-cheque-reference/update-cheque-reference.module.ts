import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateChequeReferenceService } from './update-cheque-reference.service';
import { UpdateChequeReferenceComponent } from './update-cheque-reference/update-cheque-reference.component';
import { UpdateChequeReferenceCustomComponent } from './update-cheque-reference/update-cheque-reference-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [UpdateChequeReferenceComponent],
  providers: [UpdateChequeReferenceService, UpdateChequeReferenceCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [UpdateChequeReferenceComponent]
})
export class UpdateChequeReferenceComponentModule {}
