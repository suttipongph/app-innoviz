import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenWithdrawalLineInvoiceComponent } from './gen-withdrawal-line-invoice.component';

describe('GenWithdrawalLineInvoiceViewComponent', () => {
  let component: GenWithdrawalLineInvoiceComponent;
  let fixture: ComponentFixture<GenWithdrawalLineInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GenWithdrawalLineInvoiceComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenWithdrawalLineInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
