import { Observable, of } from 'rxjs';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { GenWDLineInvoiceView } from 'shared/models/viewModel/genWDLineInvoiceView';

const firstGroup = ['DOCUMENT_STATUS_GUID', 'WITHDRAWAL_LINE_INVOICE_TABLE_GUID'];

export class GenWithdrawalLineInvoiceCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: GenWDLineInvoiceView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(companyGUID): Observable<boolean> {
    return this.baseService.service.validateGenWDLineInvoice(companyGUID);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<GenWDLineInvoiceView> {
    let model = new GenWDLineInvoiceView();
    return of(model);
  }
}
