import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GenWithdrawalLineInvoiceService } from './gen-withdrawal-line-invoice.service';
import { GenWithdrawalLineInvoiceComponent } from './gen-withdrawal-line-invoice/gen-withdrawal-line-invoice.component';
import { GenWithdrawalLineInvoiceCustomComponent } from './gen-withdrawal-line-invoice/gen-withdrawal-line-invoice-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [GenWithdrawalLineInvoiceComponent],
  providers: [GenWithdrawalLineInvoiceService, GenWithdrawalLineInvoiceCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [GenWithdrawalLineInvoiceComponent]
})
export class GenWithdrawalLineInvoiceComponentModule {}
