import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { GenWDLineInvoiceResultView } from 'shared/models/viewModel/genWDLineInvoiceView';
@Injectable()
export class GenWithdrawalLineInvoiceService {
  serviceKey = 'genWithdrawalLineInvoiceGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  genWDLineInvoice(companyGUID: string): Observable<GenWDLineInvoiceResultView> {
    const url = `${this.servicePath}/GenWDLineInvoice/companyGUID=${companyGUID}`;
    return this.dataGateway.get(url);
  }
  validateGenWDLineInvoice(companyGUID: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateGenWDLineInvoice/companyGUID=${companyGUID}`;
    return this.dataGateway.get(url);
  }
}
