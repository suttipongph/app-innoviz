import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CompanyItemView, ManageAgreementView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class GenerateDummyRecordService {
  serviceKey = 'companyGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getGenerateDummyRecord(company: CompanyItemView): Observable<boolean> {
    const url = `${this.servicePath}/GetGenerateDummyRecord`;
    return this.dataGateway.post(url, company);
  }
}
