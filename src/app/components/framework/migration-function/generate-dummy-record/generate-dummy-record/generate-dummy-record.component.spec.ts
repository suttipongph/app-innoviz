import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateDummyRecordComponent } from './generate-dummy-record.component';

describe('ManageAgreementViewComponent', () => {
  let component: GenerateDummyRecordComponent;
  let fixture: ComponentFixture<GenerateDummyRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GenerateDummyRecordComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateDummyRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
