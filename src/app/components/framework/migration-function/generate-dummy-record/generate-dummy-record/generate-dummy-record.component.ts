import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, ManageAgreementAction, RefType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { CompanyItemView, ManageAgreementView } from 'shared/models/viewModel';
import { GenerateDummyRecordService } from '../generate-dummy-record.service';
import { GenerateDummyRecordCustomComponent } from './generate-dummy-record-custom.component';
@Component({
  selector: 'generate-dummy-record',
  templateUrl: './generate-dummy-record.component.html',
  styleUrls: ['./generate-dummy-record.component.scss']
})
export class GenerateDummyRecordComponent extends BaseItemComponent<CompanyItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  agreementDocTypeOptions: SelectItems[] = [];
  documentReasonOptions: SelectItems[] = [];
  action: number;
  parmCompanyGUID: string;

  constructor(
    private service: GenerateDummyRecordService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: GenerateDummyRecordCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parmCompanyGUID = this.baseService.userDataService.getCurrentCompanyGUID();
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialUpdatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getAgreementDocTypeEnumDropDown(this.agreementDocTypeOptions);
  }
  getById(): Observable<CompanyItemView> {
    return null;
  }
  getInitialData(): Observable<CompanyItemView> {
    return null;
  }
  setInitialUpdatingData(): void {}
  setInitialCreatingData(): void {}

  onAsyncRunner(model?: any): void {}

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model, this.action).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  onSave(validation: FormValidationModel): void {}
  onSaveAndClose(validation: FormValidationModel): void {
    this.onSubmit(true, this.parmCompanyGUID);
  }
  onSubmit(isColsing: boolean, companyGUID: string): void {
    this.model.companyGUID = companyGUID;
    super.onExecuteFunction(this.service.getGenerateDummyRecord(this.model));
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
