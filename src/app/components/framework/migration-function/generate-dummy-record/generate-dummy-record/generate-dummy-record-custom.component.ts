import { Observable, of } from 'rxjs';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CompanyItemView } from 'shared/models/viewModel';
import { GenerateDummyRecordService } from '../generate-dummy-record.service';

export class GenerateDummyRecordCustomComponent {
  baseService: BaseServiceModel<GenerateDummyRecordService>;
  getFieldAccessing(model: CompanyItemView, action: number): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
}
