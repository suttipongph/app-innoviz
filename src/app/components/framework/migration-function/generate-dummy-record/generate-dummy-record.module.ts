import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GenerateDummyRecordService } from './generate-dummy-record.service';
import { GenerateDummyRecordComponent } from './generate-dummy-record/generate-dummy-record.component';
import { GenerateDummyRecordCustomComponent } from './generate-dummy-record/generate-dummy-record-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [GenerateDummyRecordComponent],
  providers: [GenerateDummyRecordService, GenerateDummyRecordCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [GenerateDummyRecordComponent]
})
export class GenerateDummyRecordComponentModule {}
