import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GenPurchaseLineInvoiceService } from './gen-purchase-line-invoice.service';
import { GenPurchaseLineInvoiceComponent } from './gen-purchase-line-invoice/gen-purchase-line-invoice.component';
import { GenPurchaseLineInvoiceCustomComponent } from './gen-purchase-line-invoice/gen-purchase-line-invoice-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [GenPurchaseLineInvoiceComponent],
  providers: [GenPurchaseLineInvoiceService, GenPurchaseLineInvoiceCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [GenPurchaseLineInvoiceComponent]
})
export class GenPurchaseLineInvoiceComponentModule {}
