import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenPurchaseLineInvoiceComponent } from './gen-purchase-line-invoice.component';

describe('GenPurchaseLineInvoiceViewComponent', () => {
  let component: GenPurchaseLineInvoiceComponent;
  let fixture: ComponentFixture<GenPurchaseLineInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GenPurchaseLineInvoiceComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenPurchaseLineInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
