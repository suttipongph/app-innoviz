import { Observable, of } from 'rxjs';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { GenPULineInvoiceView } from 'shared/models/viewModel/genPULineInvoiceView';

const firstGroup = ['DOCUMENT_STATUS_GUID', 'PURCHASE_LINE_INVOICE_TABLE_GUID'];

export class GenPurchaseLineInvoiceCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: GenPULineInvoiceView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(companyGUID): Observable<boolean> {
    return this.baseService.service.validateGenPULineInvoice(companyGUID);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<GenPULineInvoiceView> {
    let model = new GenPULineInvoiceView();
    return of(model);
  }
}
