import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { GenPULineInvoiceResultView } from 'shared/models/viewModel/genPULineInvoiceView';
@Injectable()
export class GenPurchaseLineInvoiceService {
  serviceKey = 'genPurchaseLineInvoiceGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  genPULineInvoice(companyGUID: string): Observable<GenPULineInvoiceResultView> {
    const url = `${this.servicePath}/GenPULineInvoice/companyGUID=${companyGUID}`;
    return this.dataGateway.get(url);
  }
  validateGenPULineInvoice(companyGUID: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateGenPULineInvoice/companyGUID=${companyGUID}`;
    return this.dataGateway.get(url);
  }
}
