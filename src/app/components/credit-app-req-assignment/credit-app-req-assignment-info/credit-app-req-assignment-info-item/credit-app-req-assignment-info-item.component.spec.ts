import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditAppReqAssignmentInfoItemComponent } from './credit-app-req-assignment-info-item.component';

describe('CreditAppReqAssignmentInfoItemViewComponent', () => {
  let component: CreditAppReqAssignmentInfoItemComponent;
  let fixture: ComponentFixture<CreditAppReqAssignmentInfoItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditAppReqAssignmentInfoItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppReqAssignmentInfoItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
