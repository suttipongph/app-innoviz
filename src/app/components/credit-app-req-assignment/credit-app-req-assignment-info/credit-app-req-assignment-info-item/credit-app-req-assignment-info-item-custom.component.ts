import { Observable, of } from 'rxjs';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CreditAppReqAssignmentInfoItemView } from 'shared/models/viewModel';

const firstGroup = ['CREDIT_APPLICATION_REQUEST_ID', 'CUSTOMER_ID'];

export class CreditAppReqAssignmentInfoItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CreditAppReqAssignmentInfoItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CreditAppReqAssignmentInfoItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(passingObj: CreditAppReqAssignmentInfoItemView): Observable<CreditAppReqAssignmentInfoItemView> {
    return of(passingObj);
  }
}
