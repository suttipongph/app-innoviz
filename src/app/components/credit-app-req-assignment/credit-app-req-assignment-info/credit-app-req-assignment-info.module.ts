import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreditAppReqAssignmentInfoService } from './credit-app-req-assignment-info.service';
import { CreditAppReqAssignmentInfoItemComponent } from './credit-app-req-assignment-info-item/credit-app-req-assignment-info-item.component';
import { CreditAppReqAssignmentInfoItemCustomComponent } from './credit-app-req-assignment-info-item/credit-app-req-assignment-info-item-custom.component';
import { CreditAppReqAssignmentComponentModule } from '../credit-app-req-assignment/credit-app-req-assignment.module';
import { ExistingAssignmentAgreementComponentModule } from '../existing-assignment-agreement/existing-assignment-agreement.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    CreditAppReqAssignmentComponentModule,
    ExistingAssignmentAgreementComponentModule
  ],
  declarations: [CreditAppReqAssignmentInfoItemComponent],
  providers: [CreditAppReqAssignmentInfoService, CreditAppReqAssignmentInfoItemCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CreditAppReqAssignmentInfoItemComponent]
})
export class CreditAppReqAssignmentInfoComponentModule {}
