import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CreditAppReqAssignmentInfoItemView, CreditAppReqAssignmentInfoListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CreditAppReqAssignmentInfoService {
  serviceKey = 'creditAppReqAssignmentInfoGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCreditAppReqAssignmentInfoToList(search: SearchParameter): Observable<SearchResult<CreditAppReqAssignmentInfoListView>> {
    const url = `${this.servicePath}/GetCreditAppReqAssignmentInfoList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCreditAppReqAssignmentInfo(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCreditAppReqAssignmentInfo`;
    return this.dataGateway.delete(url, row);
  }
  getCreditAppReqAssignmentInfoById(id: string): Observable<CreditAppReqAssignmentInfoItemView> {
    const url = `${this.servicePath}/GetCreditAppReqAssignmentInfoById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCreditAppReqAssignmentInfo(vmModel: CreditAppReqAssignmentInfoItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCreditAppReqAssignmentInfo`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCreditAppReqAssignmentInfo(vmModel: CreditAppReqAssignmentInfoItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCreditAppReqAssignmentInfo`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
