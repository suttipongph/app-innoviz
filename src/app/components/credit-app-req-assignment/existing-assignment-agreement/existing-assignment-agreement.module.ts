import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExistingAssignmentAgreementService } from './existing-assignment-agreement.service';
import { ExistingAssignmentAgreementListComponent } from './existing-assignment-agreement-list/existing-assignment-agreement-list.component';
import { ExistingAssignmentAgreementListCustomComponent } from './existing-assignment-agreement-list/existing-assignment-agreement-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ExistingAssignmentAgreementListComponent],
  providers: [ExistingAssignmentAgreementService, ExistingAssignmentAgreementListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ExistingAssignmentAgreementListComponent]
})
export class ExistingAssignmentAgreementComponentModule {}
