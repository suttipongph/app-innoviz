import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ExistingAssignmentAgreementListComponent } from './existing-assignment-agreement-list.component';

describe('ExistingAssignmentAgreementListViewComponent', () => {
  let component: ExistingAssignmentAgreementListComponent;
  let fixture: ComponentFixture<ExistingAssignmentAgreementListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExistingAssignmentAgreementListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExistingAssignmentAgreementListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
