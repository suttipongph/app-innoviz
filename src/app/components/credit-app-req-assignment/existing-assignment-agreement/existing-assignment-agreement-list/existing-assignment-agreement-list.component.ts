import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable, of } from 'rxjs';
import { ColumnType, RefType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { AssignmentAgreementOutstandingView } from 'shared/models/viewModel';
import { ExistingAssignmentAgreementService } from '../existing-assignment-agreement.service';
import { ExistingAssignmentAgreementListCustomComponent } from './existing-assignment-agreement-list-custom.component';

@Component({
  selector: 'existing-assignment-agreement-list',
  templateUrl: './existing-assignment-agreement-list.component.html',
  styleUrls: ['./existing-assignment-agreement-list.component.scss']
})
export class ExistingAssignmentAgreementListComponent extends BaseListComponent<AssignmentAgreementOutstandingView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  buyerTableOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  agreementDocTypeOptions: SelectItems[] = [];
  assignmentMethodOptions: SelectItems[] = [];
  originKey: string = null;
  passingObj: AssignmentAgreementOutstandingView;
  constructor(
    public custom: ExistingAssignmentAgreementListCustomComponent,
    private service: ExistingAssignmentAgreementService,
    public currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.originKey = this.uiService.getRelatedInfoOriginTableKey();
  }
  ngOnInit(): void {
    this.accessRightChildPage = 'ExistingAssignmentAgreementListPage';
    this.passingObj = this.getPassingObject(this.currentActivatedRoute) as AssignmentAgreementOutstandingView;
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getAgreementDocTypeEnumDropDown(this.agreementDocTypeOptions);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
    this.baseDropdown.getAssignmentMethodDropDown(this.assignmentMethodOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = false;
    this.option.canDelete = false;
    this.option.key = 'existingAssignmentAgreementGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.INTERNAL_ASSIGNMENT_AGREEMENT_ID',
        textKey: 'internalAssignmentAgreementId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.ASSIGNMENT_AGREEMENT_ID',
        textKey: 'assignmentAgreementId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'buyerTableGUID',
        sortingKey: 'buyerTable_BuyerId',
        masterList: this.buyerTableOptions
      },
      {
        label: 'LABEL.ASSIGNMENT_AGREEMENT_AMOUNT',
        textKey: 'assignmentAgreementAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: 'LABEL.REMAINING_AMOUNT',
        textKey: 'remainingAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: 'LABEL.ASSIGNMENT_METHOD_ID',
        textKey: 'assignmentMethod_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'assignmentMethodGUID',
        sortingKey: 'assignmentMethod_InternalAssignmentAgreementId',
        masterList: this.assignmentMethodOptions
      },
      {
        label: 'LABEL.AGREEMENT_DATE',
        textKey: 'agreementDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'documentStatusGUID',
        sortingKey: 'documentStatus_StatusId',
        masterList: this.documentStatusOptions
      },
      {
        label: 'LABEL.AGREEMENT_DOCUMENT_TYPE',
        textKey: 'agreementDocType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.agreementDocTypeOptions
      },
      {
        label: null,
        textKey: 'customerTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.passingObj.customerTableGUID
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<AssignmentAgreementOutstandingView>> {
    return this.service.getExistingAssignmentAgreementToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteExistingAssignmentAgreement(row));
  }
}
