import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  AssignmentAgreementOutstandingView,
  ExistingAssignmentAgreementItemView,
  ExistingAssignmentAgreementListView
} from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ExistingAssignmentAgreementService {
  serviceKey = 'existingAssignmentAgreementGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getExistingAssignmentAgreementToList(search: SearchParameter): Observable<SearchResult<AssignmentAgreementOutstandingView>> {
    const url = `${this.servicePath}/GetExistingAssignmentAgreementList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteExistingAssignmentAgreement(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteExistingAssignmentAgreement`;
    return this.dataGateway.delete(url, row);
  }
  getExistingAssignmentAgreementById(id: string): Observable<ExistingAssignmentAgreementItemView> {
    const url = `${this.servicePath}/GetExistingAssignmentAgreementById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createExistingAssignmentAgreement(vmModel: ExistingAssignmentAgreementItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateExistingAssignmentAgreement`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateExistingAssignmentAgreement(vmModel: ExistingAssignmentAgreementItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateExistingAssignmentAgreement`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
