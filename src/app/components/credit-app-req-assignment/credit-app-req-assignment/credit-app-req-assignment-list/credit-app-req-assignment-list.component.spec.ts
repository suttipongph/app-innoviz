import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditAppReqAssignmentListComponent } from './credit-app-req-assignment-list.component';

describe('CreditAppReqAssignmentListViewComponent', () => {
  let component: CreditAppReqAssignmentListComponent;
  let fixture: ComponentFixture<CreditAppReqAssignmentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditAppReqAssignmentListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppReqAssignmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
