import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable, of } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CreditAppReqAssignmentListView } from 'shared/models/viewModel';
import { CreditAppReqAssignmentService } from '../credit-app-req-assignment.service';
import { CreditAppReqAssignmentListCustomComponent } from './credit-app-req-assignment-list-custom.component';

@Component({
  selector: 'credit-app-req-assignment-list',
  templateUrl: './credit-app-req-assignment-list.component.html',
  styleUrls: ['./credit-app-req-assignment-list.component.scss']
})
export class CreditAppReqAssignmentListComponent extends BaseListComponent<CreditAppReqAssignmentListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  assignmentAgreementTableOptions: SelectItems[] = [];
  assignmentMethodOptions: SelectItems[] = [];
  buyerAgreementTableOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  defaultBitOptions: SelectItems[] = [];
  constructor(
    public custom: CreditAppReqAssignmentListCustomComponent,
    private service: CreditAppReqAssignmentService,
    public currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
  }
  ngOnInit(): void {
    this.accessRightChildPage = 'CreditAppReqAssignmentListPage';
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setAccessModeByParent(passingObj, this.isWorkFlowMode()).subscribe((result) => {
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getDefaultBitDropDown(this.defaultBitOptions);
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getAssignmentAgreementTableDropDown(this.assignmentAgreementTableOptions);
    this.baseDropdown.getAssignmentMethodDropDown(this.assignmentMethodOptions);
    this.baseDropdown.getBuyerAgreementTableDropDown(this.buyerAgreementTableOptions);
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'creditAppReqAssignmentGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.IS_NEW',
        textKey: 'isNew',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defaultBitOptions
      },
      {
        label: 'LABEL.ASSIGNMENT_AGREEMENT_ID',
        textKey: 'assignmentAgreementTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'assignmentAgreementTableGUID',
        sortingKey: 'assignmentAgreementTable_InternalAssignmentAgreementId',
        masterList: this.assignmentAgreementTableOptions
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'buyerTable_Values',
        sortingKey: 'buyerTable_BuyerId',
        masterList: this.buyerTableOptions
      },
      {
        label: 'LABEL.ASSIGNMENT_AGREEMENT_AMOUNT',
        textKey: 'assignmentAgreementAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.ASSIGNMENT_METHOD_ID',
        textKey: 'assignmentMethod_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'assignmentAgreementTableGUID',
        sortingKey: 'assignmentAgreementTable_InternalAssignmentAgreementId',
        masterList: this.assignmentMethodOptions
      },
      {
        label: 'LABEL.BUYER_AGREEMENT_ID',
        textKey: 'buyerAgreementTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'buyerAgreementTableGUID',
        sortingKey: 'buyerAgreementTable_BuyerAgreementId',
        masterList: this.buyerAgreementTableOptions
      },
      {
        label: 'LABEL.BUYER_AGREEMENT_AMOUNT',
        textKey: 'buyerAgreementAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.REMAINING_AMOUNT',
        textKey: 'remainingAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'creditAppRequestTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.custom.originKey
      },
      {
        label: null,
        textKey: 'CreatedDateTime',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.ASC
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CreditAppReqAssignmentListView>> {
    return this.service.getCreditAppReqAssignmentToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCreditAppReqAssignment(row));
  }
}
