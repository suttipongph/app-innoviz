import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  AccessModeView,
  AssignmentAgreementOutstandingView,
  CreditAppReqAssignmentItemView,
  CreditAppReqAssignmentListView
} from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CreditAppReqAssignmentService {
  serviceKey = 'creditAppReqAssignmentGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCreditAppReqAssignmentToList(search: SearchParameter): Observable<SearchResult<CreditAppReqAssignmentListView>> {
    const url = `${this.servicePath}/GetCreditAppReqAssignmentList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCreditAppReqAssignment(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCreditAppReqAssignment`;
    return this.dataGateway.delete(url, row);
  }
  getCreditAppReqAssignmentById(id: string): Observable<CreditAppReqAssignmentItemView> {
    const url = `${this.servicePath}/GetCreditAppReqAssignmentById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCreditAppReqAssignment(vmModel: CreditAppReqAssignmentItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCreditAppReqAssignment`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCreditAppReqAssignment(vmModel: CreditAppReqAssignmentItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCreditAppReqAssignment`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  GetAssignmentAgreementOutstanding(refGUID: string, reftype: number): Observable<AssignmentAgreementOutstandingView[]> {
    const url = `${this.servicePath}/GetAssignmentAgreementOutstanding/RefGUID=${refGUID}/RefType=${reftype}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByCreditAppRequestTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppRequestTable/id=${id}`;
    return this.dataGateway.get(url);
  }
}
