import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { AssignmentAgreementStatus } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmpty, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { CreditAppReqAssignmentItemView } from 'shared/models/viewModel';
import { CreditAppReqAssignmentService } from '../credit-app-req-assignment.service';
import { CreditAppReqAssignmentItemCustomComponent } from './credit-app-req-assignment-item-custom.component';
@Component({
  selector: 'credit-app-req-assignment-item',
  templateUrl: './credit-app-req-assignment-item.component.html',
  styleUrls: ['./credit-app-req-assignment-item.component.scss']
})
export class CreditAppReqAssignmentItemComponent extends BaseItemComponent<CreditAppReqAssignmentItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  assignmentAgreementTableOptions: SelectItems[] = [];
  assignmentMethodOptions: SelectItems[] = [];
  buyerAgreementTableOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  passingObj: any;

  constructor(
    private service: CreditAppReqAssignmentService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CreditAppReqAssignmentItemCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {
    this.passingObj = this.getPassingObject(this.currentActivatedRoute);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<CreditAppReqAssignmentItemView> {
    return this.service.getCreditAppReqAssignmentById(this.id);
  }
  getInitialData(): Observable<CreditAppReqAssignmentItemView> {
    return this.custom.getInitialData(this.passingObj);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: CreditAppReqAssignmentItemView): void {
    const tempModel = isNullOrUndefOrEmpty(model) ? this.model : model;
    forkJoin(
      this.baseDropdown.getAssignmentAgreementTableByCustomerAndStatusDropDown([tempModel.customerTableGUID, AssignmentAgreementStatus.Signed]),
      this.baseDropdown.getAssignmentMethodDropDown(),
      this.baseDropdown.getBuyerTableDropDown()
    ).subscribe(
      ([assignmentAgreementTable, assignmentMethod, buyerTable]) => {
        this.assignmentAgreementTableOptions = assignmentAgreementTable;
        this.assignmentMethodOptions = assignmentMethod;
        this.buyerTableOptions = buyerTable;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model, this.isWorkFlowMode()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateCreditAppReqAssignment(this.model), isColsing);
    } else {
      super.onCreate(this.service.createCreditAppReqAssignment(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onBuyerChange(): void {
    this.custom.onBuyerChange(this.model, this.buyerAgreementTableOptions);
    if (!isNullOrUndefOrEmptyGUID(this.model.buyerTableGUID && this.model.customerTableGUID)) {
      this.baseDropdown
        .getBuyerAgreementTableByBuyerAndCustomerDropDown([this.model.buyerTableGUID, this.model.customerTableGUID])
        .subscribe((result) => {
          this.buyerAgreementTableOptions = result;
        });
    }
  }
  onIsNewChange(): void {
    this.custom.onIsNewChange(this.model).subscribe(() => {
      this.onIsNewBinding();
    });
  }
  onIsNewBinding(): void {
    super.setBaseFieldAccessing(this.custom.getFieldAccessingByIsNew(this.model.isNew));
  }
  onBuyerAgreementChange(): void {
    this.custom.onBuyerAgreementChange(this.model, this.buyerAgreementTableOptions);
  }
  onAssignmentAgreementChange(): void {
    this.custom.onAssignmentAgreementChange(this.model);
  }
}
