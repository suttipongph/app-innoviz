import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditAppReqAssignmentItemComponent } from './credit-app-req-assignment-item.component';

describe('CreditAppReqAssignmentItemViewComponent', () => {
  let component: CreditAppReqAssignmentItemComponent;
  let fixture: ComponentFixture<CreditAppReqAssignmentItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditAppReqAssignmentItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppReqAssignmentItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
