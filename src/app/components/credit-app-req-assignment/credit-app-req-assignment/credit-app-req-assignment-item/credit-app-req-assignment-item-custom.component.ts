import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { CreditAppRequestStatus, EmptyGuid, RefType } from 'shared/constants';
import { isNullOrUndefOrEmpty, isUndefinedOrZeroLength, checkDataHasValue, isUpdateMode } from 'shared/functions/value.function';
import { Confirmation } from 'shared/models/primeModel';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import {
  AccessModeView,
  AssignmentAgreementOutstandingView,
  BuyerAgreementTableItemView,
  CreditAppReqAssignmentItemView
} from 'shared/models/viewModel';
import { CreditAppReqAssignmentService } from '../credit-app-req-assignment.service';

const firstGroup = ['CUSTOMER_TABLE_GUID', 'CREDIT_APP_REQUEST_TABLE_GUID', 'REMAINING_AMOUNT'];
const condition1 = ['ASSIGNMENT_AGREEMENT_TABLE_GUID'];
// tslint:disable-next-line:variable-name
const condition1_2 = [
  'DESCRIPTION',
  'BUYER_TABLE_GUID',
  'REFERENCE_AGREEMENT_ID',
  'ASSIGNMENT_AGREEMENT_AMOUNT',
  'BUYER_AGREEMENT_TABLE_GUID',
  'BUYER_AGREEMENT_AMOUNT',
  'ASSIGNMENT_METHOD_GUID'
];
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const CREDIT_APP_TABLE = 'creditapptable';
export class CreditAppReqAssignmentItemCustomComponent {
  baseService: BaseServiceModel<any>;
  isRequireAssignmentAgreement: boolean = false;
  accessModeView: AccessModeView = new AccessModeView();
  originKey: string = null;
  originName: string = null;
  getFieldAccessing(model: CreditAppReqAssignmentItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CreditAppReqAssignmentItemView, isWorkFlowMode: boolean): Observable<boolean> {
    return this.setAccessModeByParent(model, isWorkFlowMode).pipe(
      map((result) => (isUpdateMode(model.creditAppReqAssignmentGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  setAccessModeByParent(passingObj: CreditAppReqAssignmentItemView, isWorkFlowMode: boolean): Observable<AccessModeView> {
    this.originKey = this.baseService.uiService.getRelatedInfoOriginTableKey();
    this.originName = this.baseService.uiService.getRelatedInfoOriginTableName();
    switch (this.originName) {
      case CREDIT_APP_REQUEST_TABLE:
        return this.getAccessModeByCreditAppRequestTable(isWorkFlowMode);
      case CREDIT_APP_TABLE:
        this.originKey = passingObj.creditAppRequestTableGUID;
        return this.getAccessModeByCreditAppRequestTable(isWorkFlowMode);
      default:
        this.accessModeView.canCreate = false;
        this.accessModeView.canView = false;
        this.accessModeView.canDelete = false;
        return of(this.accessModeView);
    }
  }
  getAccessModeByCreditAppRequestTable(isWorkFlowMode: boolean): Observable<AccessModeView> {
    return this.baseService.service.getAccessModeByCreditAppRequestTable(this.originKey).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
        const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
        this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
        this.accessModeView.canView = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
      })
    );
  }
  getInitialData(passObject: CreditAppReqAssignmentItemView): Observable<CreditAppReqAssignmentItemView> {
    const model = new CreditAppReqAssignmentItemView();
    model.creditAppReqAssignmentGUID = EmptyGuid;
    model.creditAppRequestTableGUID = passObject.creditAppRequestTableGUID;
    model.customerTableGUID = passObject.customerTableGUID;
    model.creditAppRequestTable_Values = passObject.creditAppRequestTable_Values;
    model.customerTable_Values = passObject.customerTable_Values;
    model.isNew = true;
    return of(model);
  }
  onBuyerChange(model: CreditAppReqAssignmentItemView, buyerAgreementTableOptions: SelectItems[]): void {
    model.buyerAgreementTableGUID = null;
    buyerAgreementTableOptions.length = 0;
  }
  onIsNewChange(model: CreditAppReqAssignmentItemView): Observable<any> {
    const fieldsHasValue = checkDataHasValue(model, [
      'isNew',
      'remainingAmount',
      'customerTableGUID',
      'creditAppRequestTableGUID',
      'assignmentAgreementTableGUID',
      'creditAppReqAssignmentGUID',
      'creditAppRequestTable_Values',
      'customerTable_Values'
    ]);
    if (!isUndefinedOrZeroLength(fieldsHasValue)) {
      const confirmation: Confirmation = {
        header: this.baseService.translate.instant('CONFIRM.CONFIRM'),
        message: this.baseService.translate.instant('CONFIRM.90007', [
          this.baseService.translate.instant('LABEL.CREDIT_APPLICATION_REQUEST_ASSIGNMENT')
        ])
      };
      return new Observable((observer) => {
        this.baseService.notificationService.showConfirmDialog(confirmation);
        this.baseService.notificationService.isAccept.subscribe((isConfirm) => {
          if (isConfirm) {
            model.description = '';
            model.buyerTableGUID = null;
            model.referenceAgreementId = null;
            model.assignmentMethodGUID = null;
            model.assignmentAgreementTableGUID = null;
            model.assignmentAgreementAmount = 0;
            model.buyerAgreementTableGUID = null;
            model.buyerAgreementAmount = 0;
            model.remainingAmount = 0;
          } else {
            model.isNew = !model.isNew;
          }
          this.baseService.notificationService.isAccept.observers = [];
          observer.next();
        });
      });
    } else {
      return of(true);
    }
  }
  getFieldAccessingByIsNew(isNew: boolean): FieldAccessing[] {
    this.isRequireAssignmentAgreement = !isNew;
    return [
      { filedIds: condition1, readonly: isNew },
      { filedIds: condition1_2, readonly: !isNew }
    ];
  }
  onBuyerAgreementChange(model: CreditAppReqAssignmentItemView, buyerAgreementTableOptions: SelectItems[]): void {
    const buyerAgreement = !isNullOrUndefOrEmpty(model.buyerAgreementTableGUID)
      ? (buyerAgreementTableOptions.find((f) => f.value === model.buyerAgreementTableGUID).rowData as BuyerAgreementTableItemView)
      : new BuyerAgreementTableItemView();
    model.buyerAgreementAmount = buyerAgreement.buyerAgreementAmount;
  }
  onAssignmentAgreementChange(model: CreditAppReqAssignmentItemView): void {
    if (!isNullOrUndefOrEmpty(model.assignmentAgreementTableGUID)) {
      (this.baseService.service as CreditAppReqAssignmentService)
        .GetAssignmentAgreementOutstanding(model.assignmentAgreementTableGUID, RefType.AssignmentAgreement)
        .subscribe((result) => {
          const assignmentAgreementOutstanding = !isUndefinedOrZeroLength(result) ? result[0] : new AssignmentAgreementOutstandingView();
          model.description = assignmentAgreementOutstanding.description;
          model.buyerTableGUID = assignmentAgreementOutstanding.buyerTableGUID;
          model.referenceAgreementId = assignmentAgreementOutstanding.referenceAgreementId;
          model.assignmentMethodGUID = assignmentAgreementOutstanding.assignmentMethodGUID;
          model.assignmentAgreementAmount = assignmentAgreementOutstanding.assignmentAgreementAmount;
          model.remainingAmount = assignmentAgreementOutstanding.remainingAmount;
        });
    } else {
      model.description = '';
      model.buyerTableGUID = null;
      model.referenceAgreementId = '';
      model.assignmentMethodGUID = null;
      model.assignmentAgreementAmount = 0;
      model.remainingAmount = 0;
    }
  }
}
