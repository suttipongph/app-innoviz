import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreditAppReqAssignmentService } from './credit-app-req-assignment.service';
import { CreditAppReqAssignmentListComponent } from './credit-app-req-assignment-list/credit-app-req-assignment-list.component';
import { CreditAppReqAssignmentItemComponent } from './credit-app-req-assignment-item/credit-app-req-assignment-item.component';
import { CreditAppReqAssignmentItemCustomComponent } from './credit-app-req-assignment-item/credit-app-req-assignment-item-custom.component';
import { CreditAppReqAssignmentListCustomComponent } from './credit-app-req-assignment-list/credit-app-req-assignment-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CreditAppReqAssignmentListComponent, CreditAppReqAssignmentItemComponent],
  providers: [CreditAppReqAssignmentService, CreditAppReqAssignmentItemCustomComponent, CreditAppReqAssignmentListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CreditAppReqAssignmentListComponent, CreditAppReqAssignmentItemComponent]
})
export class CreditAppReqAssignmentComponentModule {}
