import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OwnerTransService } from './owner-trans.service';
import { OwnerTransListComponent } from './owner-trans-list/owner-trans-list.component';
import { OwnerTransItemComponent } from './owner-trans-item/owner-trans-item.component';
import { OwnerTransItemCustomComponent } from './owner-trans-item/owner-trans-item-custom.component';
import { OwnerTransListCustomComponent } from './owner-trans-list/owner-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [OwnerTransListComponent, OwnerTransItemComponent],
  providers: [OwnerTransService, OwnerTransItemCustomComponent, OwnerTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [OwnerTransListComponent, OwnerTransItemComponent]
})
export class OwnerTransComponentModule {}
