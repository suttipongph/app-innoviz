import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { OwnerTransListComponent } from './owner-trans-list.component';

describe('OwnerTransListViewComponent', () => {
  let component: OwnerTransListComponent;
  let fixture: ComponentFixture<OwnerTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OwnerTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnerTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
