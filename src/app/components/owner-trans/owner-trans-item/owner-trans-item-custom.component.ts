import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { CreditAppRequestStatus } from 'shared/constants';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, OwnerTransItemView, RelatedPersonTableItemView } from 'shared/models/viewModel';

const firstGroup = [
  'NAME',
  'IDENTIFICATION_TYPE',
  'TAX_ID',
  'PASSPORT_ID',
  'WORK_PERMIT_ID',
  'BACKGROUND_SUMMARY',
  'DATE_OF_BIRTH',
  'AGE',
  'PHONE',
  'EXTENSION',
  'MOBILE',
  'FAX',
  'EMAIL',
  'LINE_ID',
  'REF_TYPE',
  'REF_ID'
];

const secondGroup = ['RELATED_PERSON_TABLE_GUID'];
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const CUSTOMER_TABLE = 'customertable';
const CREDIT_APP_TABLE = 'creditapptable';

export class OwnerTransItemCustomComponent {
  baseService: BaseServiceModel<any>;
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  getFieldAccessing(model: OwnerTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    if (isUpdateMode(model.ownerTransGUID)) {
      fieldAccessing.push({ filedIds: secondGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: secondGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: OwnerTransItemView, isWorkFlowMode: boolean): Observable<boolean> {
    return this.setAccessModeByParentStatus(model, isWorkFlowMode).pipe(
      map((result) => (isUpdateMode(model.ownerTransGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  setAccessModeByParentStatus(model: OwnerTransItemView, isWorkFlowMode: boolean): Observable<AccessModeView> {
    let parentId = this.baseService.uiService.getRelatedInfoOriginTableKey();
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        return this.baseService.service.getAccessModeByCreditAppRequestTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
            this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
            this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
            this.accessModeView.canView = isDraftStatus ? true : this.accessModeView.canView && isWorkFlowMode;
          })
        );
      case CUSTOMER_TABLE:
        this.accessModeView.canCreate = true;
        this.accessModeView.canDelete = false;
        this.accessModeView.canView = true;
        return of(this.accessModeView);
      case CREDIT_APP_TABLE:
        this.accessModeView.canCreate = false;
        this.accessModeView.canDelete = false;
        this.accessModeView.canView = false;
        return of(this.accessModeView);
      default:
        return of(this.accessModeView);
    }
  }
  onRelatedPersonChange(model: OwnerTransItemView, option: SelectItems[]): void {
    this.setValueByRelatedPerson(model, option);
  }
  setValueByRelatedPerson(model: OwnerTransItemView, option: SelectItems[]): void {
    if (!isNullOrUndefined(model.relatedPersonTableGUID)) {
      const row = option.find((o) => o.value === model.relatedPersonTableGUID).rowData as RelatedPersonTableItemView;
      model.relatedPersonTable_Name = row.name;
      model.relatedPersonTable_IdentificationType = row.identificationType;
      model.relatedPersonTable_TaxId = row.taxId;
      model.relatedPersonTable_PassportId = row.passportId;
      model.relatedPersonTable_WorkPermitId = row.workPermitId;
      model.relatedPersonTable_DateOfBirth = row.dateOfBirth;
      model.relatedPersonTable_Phone = row.phone;
      model.relatedPersonTable_Extension = row.extension;
      model.relatedPersonTable_DateOfBirth = row.dateOfBirth;
      model.relatedPersonTable_Fax = row.fax;
      model.relatedPersonTable_Mobile = row.mobile;
      model.relatedPersonTable_LineId = row.lineId;
      model.relatedPersonTable_Email = row.email;
      model.relatedPersonTable_BackgroundSummary = row.backgroundSummary;
      model.age = row.age;
    } else {
      model.relatedPersonTable_Name = '';
      model.relatedPersonTable_IdentificationType = null;
      model.relatedPersonTable_TaxId = '';
      model.relatedPersonTable_PassportId = '';
      model.relatedPersonTable_WorkPermitId = '';
      model.relatedPersonTable_DateOfBirth = null;
      model.relatedPersonTable_Phone = '';
      model.relatedPersonTable_Extension = '';
      model.relatedPersonTable_DateOfBirth = null;
      model.relatedPersonTable_Fax = '';
      model.relatedPersonTable_Mobile = '';
      model.relatedPersonTable_LineId = '';
      model.relatedPersonTable_Email = '';
      model.relatedPersonTable_BackgroundSummary = '';
      model.age = 0;
    }
  }
  getRelatedPersonTableDropDown(model: OwnerTransItemView): Observable<SelectItems[]> {
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        return this.baseService.baseDropdown.getRelatedPersonTableByCreditAppRequestTableOwnerTransDropDown(model.refGUID);
      default:
        return this.baseService.baseDropdown.getRelatedPersonTableDropDown();
    }
  }
}
