import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnerTransItemComponent } from './owner-trans-item.component';

describe('OwnerTransItemViewComponent', () => {
  let component: OwnerTransItemComponent;
  let fixture: ComponentFixture<OwnerTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OwnerTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnerTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
