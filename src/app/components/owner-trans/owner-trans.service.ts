import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, OwnerTransItemView, OwnerTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class OwnerTransService {
  serviceKey = 'ownerTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getOwnerTransToList(search: SearchParameter): Observable<SearchResult<OwnerTransListView>> {
    const url = `${this.servicePath}/GetOwnerTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteOwnerTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteOwnerTrans`;
    return this.dataGateway.delete(url, row);
  }
  getOwnerTransById(id: string): Observable<OwnerTransItemView> {
    const url = `${this.servicePath}/GetOwnerTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createOwnerTrans(vmModel: OwnerTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateOwnerTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateOwnerTrans(vmModel: OwnerTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateOwnerTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<OwnerTransItemView> {
    const url = `${this.servicePath}/GetOwnerTransInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByCreditAppRequestTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppRequestTable/id=${id}`;
    return this.dataGateway.get(url);
  }
}
