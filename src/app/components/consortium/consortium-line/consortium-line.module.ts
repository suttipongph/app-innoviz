import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConsortiumLineService } from './consortium-line.service';
import { ConsortiumLineListComponent } from './consortium-line-list/consortium-line-list.component';
import { ConsortiumLineItemComponent } from './consortium-line-item/consortium-line-item.component';
import { ConsortiumLineItemCustomComponent } from './consortium-line-item/consortium-line-item-custom.component';
import { ConsortiumLineListCustomComponent } from './consortium-line-list/consortium-line-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ConsortiumLineListComponent, ConsortiumLineItemComponent],
  providers: [ConsortiumLineService, ConsortiumLineItemCustomComponent, ConsortiumLineListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ConsortiumLineListComponent, ConsortiumLineItemComponent]
})
export class ConsortiumLineComponentModule {}
