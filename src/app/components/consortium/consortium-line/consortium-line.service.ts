import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { ConsortiumLineItemView, ConsortiumLineListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ConsortiumLineService {
  serviceKey = 'consortiumLineGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getConsortiumLineToList(search: SearchParameter): Observable<SearchResult<ConsortiumLineListView>> {
    const url = `${this.servicePath}/GetConsortiumLineList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteConsortiumLine(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteConsortiumLine`;
    return this.dataGateway.delete(url, row);
  }
  getConsortiumLineById(id: string): Observable<ConsortiumLineItemView> {
    const url = `${this.servicePath}/GetConsortiumLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createConsortiumLine(vmModel: ConsortiumLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateConsortiumLine`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateConsortiumLine(vmModel: ConsortiumLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateConsortiumLine`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<ConsortiumLineItemView> {
    const url = `${this.servicePath}/GetConsortiumLineInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
}
