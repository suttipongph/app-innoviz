import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { FieldAccessing } from 'shared/models/systemModel';
import { ConsortiumLineItemView } from 'shared/models/viewModel';

const firstGroup = ['CONSORTIUM_TABLE_GUID'];

export class ConsortiumLineItemCustomComponent {
  getFieldAccessing(model: ConsortiumLineItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ConsortiumLineItemView): Observable<boolean> {
    return of(!canCreate);
  }
}
