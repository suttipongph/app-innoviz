import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsortiumLineItemComponent } from './consortium-line-item.component';

describe('ConsortiumLineItemViewComponent', () => {
  let component: ConsortiumLineItemComponent;
  let fixture: ComponentFixture<ConsortiumLineItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConsortiumLineItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsortiumLineItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
