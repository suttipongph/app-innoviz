import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ConsortiumLineListComponent } from './consortium-line-list.component';

describe('ConsortiumLineListViewComponent', () => {
  let component: ConsortiumLineListComponent;
  let fixture: ComponentFixture<ConsortiumLineListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConsortiumLineListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsortiumLineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
