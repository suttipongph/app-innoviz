import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConsortiumTableService } from './consortium-table.service';
import { ConsortiumTableListComponent } from './consortium-table-list/consortium-table-list.component';
import { ConsortiumTableItemComponent } from './consortium-table-item/consortium-table-item.component';
import { ConsortiumTableItemCustomComponent } from './consortium-table-item/consortium-table-item-custom.component';
import { ConsortiumTableListCustomComponent } from './consortium-table-list/consortium-table-list-custom.component';
import { ConsortiumLineComponentModule } from '../consortium-line/consortium-line.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, ConsortiumLineComponentModule],
  declarations: [ConsortiumTableListComponent, ConsortiumTableItemComponent],
  providers: [ConsortiumTableService, ConsortiumTableItemCustomComponent, ConsortiumTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ConsortiumTableListComponent, ConsortiumTableItemComponent]
})
export class ConsortiumTableComponentModule {}
