import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { ConsortiumTableItemView, ConsortiumTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ConsortiumTableService {
  serviceKey = 'consortiumTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getConsortiumTableToList(search: SearchParameter): Observable<SearchResult<ConsortiumTableListView>> {
    const url = `${this.servicePath}/GetConsortiumTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteConsortiumTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteConsortiumTable`;
    return this.dataGateway.delete(url, row);
  }
  getConsortiumTableById(id: string): Observable<ConsortiumTableItemView> {
    const url = `${this.servicePath}/GetConsortiumTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createConsortiumTable(vmModel: ConsortiumTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateConsortiumTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateConsortiumTable(vmModel: ConsortiumTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateConsortiumTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  validateIsManualNumberSeq(companyId: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeq/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
}
