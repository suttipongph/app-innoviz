import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ConsortiumTableListComponent } from './consortium-table-list.component';

describe('ConsortiumTableListViewComponent', () => {
  let component: ConsortiumTableListComponent;
  let fixture: ComponentFixture<ConsortiumTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConsortiumTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsortiumTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
