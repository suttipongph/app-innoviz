import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { ConsortiumTableListView } from 'shared/models/viewModel';
import { ConsortiumTableService } from '../consortium-table.service';
import { ConsortiumTableListCustomComponent } from './consortium-table-list-custom.component';

@Component({
  selector: 'consortium-table-list',
  templateUrl: './consortium-table-list.component.html',
  styleUrls: ['./consortium-table-list.component.scss']
})
export class ConsortiumTableListComponent extends BaseListComponent<ConsortiumTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  documentStatusOptions: SelectItems[] = [];

  constructor(public custom: ConsortiumTableListCustomComponent, private service: ConsortiumTableService) {
    super();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'consortiumTableGUID';
    const columns: ColumnModel[] = [
      {
        label: this.translate.instant('LABEL.CONSORTIUM_ID'),
        textKey: 'consortiumId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: this.translate.instant('LABEL.DESCRIPTION'),
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<ConsortiumTableListView>> {
    return this.service.getConsortiumTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteConsortiumTable(row));
  }
}
