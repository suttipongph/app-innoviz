import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsortiumTableItemComponent } from './consortium-table-item.component';

describe('ConsortiumTableItemViewComponent', () => {
  let component: ConsortiumTableItemComponent;
  let fixture: ComponentFixture<ConsortiumTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConsortiumTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsortiumTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
