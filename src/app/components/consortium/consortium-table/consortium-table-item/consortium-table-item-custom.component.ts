import { AppInjector } from 'app-injector';
import { NotificationService } from 'core/services/notification.service';
import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { ConsortiumTableItemView } from 'shared/models/viewModel';
import { ConsortiumTableService } from '../consortium-table.service';

const firstGroup = ['CONSORTIUM_ID'];
export class ConsortiumTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  isManual: boolean = false;
  notificationService = AppInjector.get(NotificationService);
  getFieldAccessing(model: ConsortiumTableItemView, consortiumTableService: ConsortiumTableService): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    return new Observable((observer) => {
      if (!isUpdateMode(model.consortiumTableGUID)) {
        this.validateIsManualNumberSeq(consortiumTableService, model.companyGUID).subscribe(
          (result) => {
            this.isManual = result;
            fieldAccessing.push({ filedIds: firstGroup, readonly: !this.isManual });
            observer.next(fieldAccessing);
          },
          (error) => {
            this.notificationService.showErrorMessageFromResponse(error);
          }
        );
      } else {
        fieldAccessing.push({ filedIds: firstGroup, readonly: true });
        observer.next(fieldAccessing);
      }
    });
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ConsortiumTableItemView): Observable<boolean> {
    return of(!canCreate);
  }
  validateIsManualNumberSeq(consortiumTableService: ConsortiumTableService, companyId: string): Observable<boolean> {
    return consortiumTableService.validateIsManualNumberSeq(companyId);
  }
  getInitialData(): Observable<ConsortiumTableItemView> {
    let model: ConsortiumTableItemView = new ConsortiumTableItemView();
    model.consortiumTableGUID = EmptyGuid;
    return of(model);
  }
}
