import { Observable } from 'rxjs';
import { BaseServiceModel } from 'shared/models/systemModel';
import { CAReqAssignmentOutstandingListView } from 'shared/models/viewModel';
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const AMEND_CA = 'amendca';
const AMEND_CA_LINE = 'amendcaline';
const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';
const REVIEW_CREDIT_APP_REQUEST = 'reviewcreditapprequest';
const CLOSE_CREDIT_LIMIT = 'closecreditlimit';
export class CAReqAssignmentOutstandingListCustomComponent {
  baseService: BaseServiceModel<any>;
  parentName: string = null;
  parentId: string = null;
  isBuyerMatching: boolean = false;
  isLoanRequest: boolean = false;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  getRelatedInfoParentTableKey(passingObj: CAReqAssignmentOutstandingListView): string {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        this.parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
        break;
      case AMEND_CA:
      case AMEND_CA_LINE:
      case REVIEW_CREDIT_APP_REQUEST:
      case CLOSE_CREDIT_LIMIT:
        this.parentId = passingObj.creditAppRequestTableGUID;
        break;
      case BUYER_MATCHING:
      case LOAN_REQUEST:
        this.checkIsBuyerMacthingOrLoanRequest();
        break;
      default:
        this.parentId = null;
        break;
    }
    return this.parentId;
  }
  checkIsBuyerMacthingOrLoanRequest(): void {
    this.isBuyerMatching = window.location.href.match(BUYER_MATCHING) ? true : false;
    this.isLoanRequest = window.location.href.match(LOAN_REQUEST) ? true : false;
    if (this.isBuyerMatching) {
      this.parentId = this.baseService.uiService.getKey(BUYER_MATCHING);
    } else if (this.isLoanRequest) {
      this.parentId = this.baseService.uiService.getKey(LOAN_REQUEST);
    }
  }
}
