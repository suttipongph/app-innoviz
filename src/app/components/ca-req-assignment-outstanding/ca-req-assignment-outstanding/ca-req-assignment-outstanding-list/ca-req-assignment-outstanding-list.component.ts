import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CAReqAssignmentOutstandingListView } from 'shared/models/viewModel';
import { CAReqAssignmentOutstandingService } from '../ca-req-assignment-outstanding.service';
import { CAReqAssignmentOutstandingListCustomComponent } from './ca-req-assignment-outstanding-list-custom.component';

@Component({
  selector: 'ca-req-assignment-outstanding-list',
  templateUrl: './ca-req-assignment-outstanding-list.component.html',
  styleUrls: ['./ca-req-assignment-outstanding-list.component.scss']
})
export class CAReqAssignmentOutstandingListComponent extends BaseListComponent<CAReqAssignmentOutstandingListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  customerTableOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  assignmentAgreementTableOptions: SelectItems[] = [];
  constructor(
    public custom: CAReqAssignmentOutstandingListCustomComponent,
    private service: CAReqAssignmentOutstandingService,
    public currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.parentId = this.custom.getRelatedInfoParentTableKey(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
    this.baseDropdown.getAssignmentAgreementTableDropDown(this.assignmentAgreementTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = false;
    this.option.canDelete = false;
    this.option.key = 'caReqAssignmentOutstandingGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.CUSTOMER',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId',
        masterList: this.customerTableOptions
      },
      {
        label: 'LABEL.INTERNAL_ASSIGNMENT_AGREEMENT_ID',
        textKey: 'assignmentAgreement_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.ASC,
        searchingKey: 'assignmentAgreementTableGUID',
        sortingKey: 'assignmentAgreement_InternalAssignmentAgreementId',
        masterList: this.assignmentAgreementTableOptions
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'buyerTableGUID',
        sortingKey: 'buyerTable_BuyerId',
        masterList: this.buyerTableOptions
      },
      {
        label: 'LABEL.ASSIGNMENT_AGREEMENT_AMOUNT',
        textKey: 'assignmentAgreementAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: 'LABEL.SETTLED_AMOUNT',
        textKey: 'settleAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: 'LABEL.REMAINING_AMOUNT',
        textKey: 'remainingAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: null,
        textKey: 'creditAppRequestTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CAReqAssignmentOutstandingListView>> {
    return this.service.getCAReqAssignmentOutstandingToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCAReqAssignmentOutstanding(row));
  }
}
