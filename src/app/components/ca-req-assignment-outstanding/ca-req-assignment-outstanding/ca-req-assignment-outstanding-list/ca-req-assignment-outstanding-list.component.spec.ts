import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CAReqAssignmentOutstandingListComponent } from './ca-req-assignment-outstanding-list.component';

describe('CAReqAssignmentOutstandingListViewComponent', () => {
  let component: CAReqAssignmentOutstandingListComponent;
  let fixture: ComponentFixture<CAReqAssignmentOutstandingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CAReqAssignmentOutstandingListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CAReqAssignmentOutstandingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
