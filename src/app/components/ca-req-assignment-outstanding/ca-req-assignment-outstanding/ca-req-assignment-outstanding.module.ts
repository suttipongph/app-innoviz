import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CAReqAssignmentOutstandingService } from './ca-req-assignment-outstanding.service';
import { CAReqAssignmentOutstandingListComponent } from './ca-req-assignment-outstanding-list/ca-req-assignment-outstanding-list.component';
import { CAReqAssignmentOutstandingListCustomComponent } from './ca-req-assignment-outstanding-list/ca-req-assignment-outstanding-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CAReqAssignmentOutstandingListComponent],
  providers: [CAReqAssignmentOutstandingService, CAReqAssignmentOutstandingListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CAReqAssignmentOutstandingListComponent]
})
export class CAReqAssignmentOutstandingComponentModule {}
