import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CAReqAssignmentOutstandingListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CAReqAssignmentOutstandingService {
  serviceKey = 'caReqAssignmentOutstandingGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCAReqAssignmentOutstandingToList(search: SearchParameter): Observable<SearchResult<CAReqAssignmentOutstandingListView>> {
    const url = `${this.servicePath}/GetCAReqAssignmentOutstandingList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCAReqAssignmentOutstanding(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCAReqAssignmentOutstanding`;
    return this.dataGateway.delete(url, row);
  }
}
