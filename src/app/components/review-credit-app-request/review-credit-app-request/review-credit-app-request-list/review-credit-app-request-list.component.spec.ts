import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReviewCreditAppRequestListComponent } from './review-credit-app-request-list.component';

describe('ReviewCreditAppRequestListViewComponent', () => {
  let component: ReviewCreditAppRequestListComponent;
  let fixture: ComponentFixture<ReviewCreditAppRequestListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReviewCreditAppRequestListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewCreditAppRequestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
