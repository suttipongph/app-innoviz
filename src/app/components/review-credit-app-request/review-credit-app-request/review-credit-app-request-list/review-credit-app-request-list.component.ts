import { Component, Input } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, CreditAppRequestType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CreditAppRequestTableListView } from 'shared/models/viewModel';
import { ReviewCreditAppRequestService } from '../review-credit-app-request.service';
import { ReviewCreditAppRequestListCustomComponent } from './review-credit-app-request-list-custom.component';

@Component({
  selector: 'review-credit-app-request-list',
  templateUrl: './review-credit-app-request-list.component.html',
  styleUrls: ['./review-credit-app-request-list.component.scss']
})
export class ReviewCreditAppRequestListComponent extends BaseListComponent<CreditAppRequestTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  documentStatusOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  creditAppTableOptions: SelectItems[] = [];
  constructor(public custom: ReviewCreditAppRequestListCustomComponent, private service: ReviewCreditAppRequestService) {
    super();
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
    this.baseDropdown.getCreditAppTableDropDown(this.creditAppTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'creditAppRequestTableGUID';
    const columns: ColumnModel[] = [
      {
        label: this.translate.instant('LABEL.CREDIT_APPLICATION_REQUEST_ID'),
        textKey: 'creditAppRequestId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: this.translate.instant('LABEL.DESCRIPTION'),
        textKey: 'creditAppRequestId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: this.translate.instant('LABEL.REQUEST_DATE'),
        textKey: 'requestDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: this.translate.instant('LABEL.STATUS'),
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'documentStatusGUID',
        sortingKey: 'documentStatus_StatusId',
        masterList: this.documentStatusOptions
      },
      {
        label: this.translate.instant('LABEL.CUSTOMER_ID'),
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId'
      },
      {
        label: this.translate.instant('LABEL.BUYER_ID'),
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerTableOptions,
        searchingKey: 'creditAppRequestLine_BuyerTableGUID',
        sortingKey: 'creditAppRequestLine_BuyerId'
      },
      {
        label: this.translate.instant('LABEL.REFERENCE_CREDIT_APPLICATION_ID'),
        textKey: 'refCreditAppTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditAppTableOptions,
        searchingKey: 'refCreditAppTableGUID',
        sortingKey: 'refCreditAppTable_CreditAppId'
      },
      {
        label: this.translate.instant('LABEL.BUYER_CREDIT_LIMIT'),
        textKey: 'buyerCreditLimit',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: this.translate.instant('LABEL.CREDIT_LIMIT_LINE_REQUEST'),
        textKey: 'creditLimitLineRequest',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: null,
        textKey: 'creditAppRequestType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: CreditAppRequestType.ReviewBuyerCreditLimit.toString()
      },
      {
        label: null,
        textKey: 'creditAppLineGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.DESC,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CreditAppRequestTableListView>> {
    return this.service.getReviewCreditAppRequestToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteReviewCreditAppRequest(row));
  }
  getRowAuthorize(row: CreditAppRequestTableListView[]): void {
    if (this.custom.getRowAuthorize(row)) {
      row.forEach((item) => {
        item.rowAuthorize = this.getSingleRowAuthorize(item.rowAuthorize, item);
      });
    } else {
      super.getRowAuthorize(row);
    }
  }
}
