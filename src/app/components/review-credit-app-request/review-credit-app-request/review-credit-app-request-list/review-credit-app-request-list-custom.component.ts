import { Observable } from 'rxjs';
import { AccessMode, CreditAppRequestStatus } from 'shared/constants';
import { BaseServiceModel } from 'shared/models/systemModel';
import { CreditAppRequestTableListView } from 'shared/models/viewModel';

export class ReviewCreditAppRequestListCustomComponent {
  baseService: BaseServiceModel<any>;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  getRowAuthorize(row: CreditAppRequestTableListView[]): boolean {
    let isSetRow: boolean = true;
    row.forEach((item) => {
      const accessMode: AccessMode = item.documentStatus_StatusId === CreditAppRequestStatus.Draft ? AccessMode.full : AccessMode.creator;
      item.rowAuthorize = accessMode;
    });
    return isSetRow;
  }
}
