import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewCreditAppRequestItemComponent } from './review-credit-app-request-item.component';

describe('ReviewCreditAppRequestItemViewComponent', () => {
  let component: ReviewCreditAppRequestItemComponent;
  let fixture: ComponentFixture<ReviewCreditAppRequestItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReviewCreditAppRequestItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewCreditAppRequestItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
