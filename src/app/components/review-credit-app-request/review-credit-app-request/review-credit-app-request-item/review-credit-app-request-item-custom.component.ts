import { Observable, of } from 'rxjs';
import { CreditAppRequestStatus, EmptyGuid, RefType } from 'shared/constants';
import { toMapModel } from 'shared/functions/model.function';
import { isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CreditAppRequestTableItemView } from 'shared/models/viewModel';
import { PrintSetBookDocTransView } from 'shared/models/viewModel/printSetBookDocTransView';

const firstGroup = [
  'CREDIT_APP_REQUEST_TYPE',
  'DOCUMENT_STATUS_GUID',
  'CUSTOMER_ID',
  'BUYER_ID',
  'REF_CREDIT_APP_TABLE_GUID',
  'REF_CREDIT_APP_LINE_GUID',
  'BUYER_CREDIT_LIMIT',
  'CREDIT_LIMIT_LINE_REQUEST',
  'CUSTOMER_BUYER_OUTSTANDING',
  'ALL_CUSTOMER_BUYER_OUTSTANDING',
  'DOCUMENT_REASON_GUID',
  'APPROVED_DATE',
  'DOCUMENT_REMARK'
];
const NUMBER_SEQ = ['CREDIT_APP_REQUEST_ID'];

export class ReviewCreditAppRequestItemCustomComponent {
  baseService: BaseServiceModel<any>;
  isManual: boolean = false;
  getFieldAccessing(model: CreditAppRequestTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    return new Observable((observer) => {
      if (!this.canActiveByStatus(model.documentStatus_StatusId)) {
        observer.next(fieldAccessing);
        return;
      }
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      if (!isUpdateMode(model.creditAppRequestTableGUID)) {
        this.validateIsManualNumberSeq(model.productType).subscribe(
          (result) => {
            this.isManual = result;
            fieldAccessing.push({ filedIds: NUMBER_SEQ, readonly: !this.isManual });
            observer.next(fieldAccessing);
          },
          (error) => {
            observer.next(fieldAccessing);
          }
        );
      } else {
        fieldAccessing.push({ filedIds: NUMBER_SEQ, readonly: true });
        observer.next(fieldAccessing);
      }
    });
  }
  validateIsManualNumberSeq(productType: number): Observable<boolean> {
    return this.baseService.service.validateIsManualNumberSeq(productType);
  }
  canActiveByStatus(status: string): boolean {
    return status === CreditAppRequestStatus.Draft;
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CreditAppRequestTableItemView): Observable<boolean> {
    const accessright = isNullOrUndefOrEmptyGUID(model.creditAppRequestTableGUID) ? canCreate : canUpdate;
    const accessLogic = this.canActiveByStatus(model.documentStatus_StatusId);
    return of(!(accessLogic && accessright));
  }
  statusIsDraft(model: CreditAppRequestTableItemView): boolean {
    return model.documentStatus_StatusId === CreditAppRequestStatus.Draft;
  }
  setPrintSetBookDocTransData(model: CreditAppRequestTableItemView): PrintSetBookDocTransView{
    const paramModel = new PrintSetBookDocTransView();
    toMapModel(model, paramModel);
    paramModel.refType = RefType.CreditAppRequestTable;
    paramModel.refGUID = model.creditAppRequestTableGUID;
    paramModel.creditAppRequestId = model.creditAppRequestId;
    paramModel.creditAppRequestDescription = model.description;
    paramModel.creditAppRequestType = model.creditAppRequestType;
   // paramModel.creditAppRequestId = model.creditLimitTypeGUID;
    return paramModel;
  }
}
