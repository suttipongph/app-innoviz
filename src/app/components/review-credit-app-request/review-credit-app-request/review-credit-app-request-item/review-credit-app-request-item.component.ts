import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { RefType, ROUTE_FUNCTION_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { CreditAppRequestTableItemView, RefIdParm, ReviewCreditAppRequestItemView } from 'shared/models/viewModel';
import { PrintSetBookDocTransView } from 'shared/models/viewModel/printSetBookDocTransView';
import { ReviewCreditAppRequestService } from '../review-credit-app-request.service';
import { ReviewCreditAppRequestItemCustomComponent } from './review-credit-app-request-item-custom.component';
@Component({
  selector: 'review-credit-app-request-item',
  templateUrl: './review-credit-app-request-item.component.html',
  styleUrls: ['./review-credit-app-request-item.component.scss']
})
export class ReviewCreditAppRequestItemComponent extends BaseItemComponent<CreditAppRequestTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  creditAppRequestTypeOptions: SelectItems[] = [];

  constructor(
    private service: ReviewCreditAppRequestService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: ReviewCreditAppRequestItemCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getCreditAppRequestTypeEnumDropDown(this.creditAppRequestTypeOptions);
  }
  getById(): Observable<CreditAppRequestTableItemView> {
    return this.service.getReviewCreditAppRequestById(this.id);
  }
  getInitialData(): Observable<CreditAppRequestTableItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions(result);
        this.setFunctionOptions(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(model: CreditAppRequestTableItemView): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        command: () => {
          const refIdParm: RefIdParm = { refType: RefType.CreditAppRequestTable, refGUID: this.model.creditAppRequestTableGUID };
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: refIdParm
          });
        }
      },
      {
        label: 'LABEL.BOOKMARK_DOCUMENT',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.BOOKMARK_DOCUMENT_TRANS,
            parameters: {
              refGUID: this.model.creditAppRequestLine_CreditAppRequestLineGuid,
              refType: RefType.CreditAppRequestLine
            }
          })
      },
      {
        label: 'LABEL.FINANCIAL',
        items: [
          {
            label: 'LABEL.FINANCIAL_STATEMENT',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.FINANCIAL_STATEMENT_TRANS,
                parameters: {
                  refGUID: this.model.creditAppRequestLine_CreditAppRequestLineGuid,
                  refType: RefType.CreditAppRequestLine,
                  creditAppRequestTableGUID: this.model.creditAppRequestTableGUID
                }
              })
          }
        ]
      },
      {
        label: 'LABEL.INQUIRY',
        items: [
          {
            label: 'LABEL.ASSIGNMENT_AGREEMENT_OUTSTANDING',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.ASSIGNMENT_AGREEMENT_OUTSTANDING,
                parameters: { creditAppRequestTableGUID: this.model.creditAppRequestTableGUID }
              })
          },
          {
            label: 'LABEL.CA_BUYER_CREDIT_OUTSTANDING',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.CA_BUYER_CREDIT_OUTSTANDING,
                parameters: { creditAppRequestTableGUID: this.model.creditAppRequestTableGUID }
              })
          },
          {
            label: 'LABEL.CREDIT_OUTSTANDING',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.CREDIT_OUTSTANDING,
                parameters: { creditAppRequestTableGUID: this.model.creditAppRequestTableGUID }
              })
          }
        ]
      },
      {
        label: 'LABEL.MEMO',
        command: () =>
          this.toRelatedInfo({ path: ROUTE_RELATED_GEN.MEMO, parameters: { refGUID: this.model.creditAppRequestLine_CreditAppRequestLineGuid } })
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(model: CreditAppRequestTableItemView): void {
    this.functionItems = [
      {
        label: 'LABEL.CANCEL_CREDIT_APPLICATION_REQUEST',
        disabled: !this.custom.statusIsDraft(model),
        command: () =>
          this.toFunction({
            path: ROUTE_FUNCTION_GEN.CANCEL_CREDIT_APPLICATION_REQUEST,
            parameters: { creditAppRequestGUID: model.creditAppRequestTableGUID }
          })
      },
      {
        label: 'LABEL.UPDATE_REVIEW_RESULT',
        disabled: !this.custom.statusIsDraft(model),
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.UPDATE_REVIEW_RESULT })
      },
      {
        label: 'LABEL.PRINT',
        items: [
          {
            label: 'LABEL.BOOKMARK_DOCUMENT',
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.PRINT_SET_BOOKMARK_DOCUMENT_TRANSACTION,
                parameters: {
                  // refType: RefType.BusinessCollateralAgreement,
                  // refGuid: this.id,
                  model: this.setPrintSetBookDocTransData()
                }
              })
          }
        ]
      }
    ];
    super.setFunctionOptionsMapping();
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateReviewCreditAppRequest(this.model), isColsing);
    } else {
      super.onCreate(this.service.createReviewCreditAppRequest(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  setPrintSetBookDocTransData(): PrintSetBookDocTransView {
    return this.custom.setPrintSetBookDocTransData(this.model);
  }
}
