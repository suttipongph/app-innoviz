import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CreditAppRequestTableItemView, CreditAppRequestTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ReviewCreditAppRequestService {
  serviceKey = 'creditAppRequestTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getReviewCreditAppRequestToList(search: SearchParameter): Observable<SearchResult<CreditAppRequestTableListView>> {
    const url = `${this.servicePath}/GetReviewCreditAppRequestList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteReviewCreditAppRequest(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteReviewCreditAppRequest`;
    return this.dataGateway.delete(url, row);
  }
  getReviewCreditAppRequestById(id: string): Observable<CreditAppRequestTableItemView> {
    const url = `${this.servicePath}/GetReviewCreditAppRequestById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createReviewCreditAppRequest(vmModel: CreditAppRequestTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateReviewCreditAppRequest`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateReviewCreditAppRequest(vmModel: CreditAppRequestTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateReviewCreditAppRequest`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(creditAppLineId: string): Observable<CreditAppRequestTableItemView> {
    const url = `${this.servicePath}/GetReviewCreditAppRequestInitialData/creditAppLineId=${creditAppLineId}`;
    return this.dataGateway.get(url);
  }
  validateIsManualNumberSeq(productType: number): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeq/productType=${productType}`;
    return this.dataGateway.get(url);
  }
}
