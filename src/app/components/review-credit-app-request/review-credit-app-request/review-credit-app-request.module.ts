import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReviewCreditAppRequestService } from './review-credit-app-request.service';
import { ReviewCreditAppRequestListComponent } from './review-credit-app-request-list/review-credit-app-request-list.component';
import { ReviewCreditAppRequestItemComponent } from './review-credit-app-request-item/review-credit-app-request-item.component';
import { ReviewCreditAppRequestItemCustomComponent } from './review-credit-app-request-item/review-credit-app-request-item-custom.component';
import { ReviewCreditAppRequestListCustomComponent } from './review-credit-app-request-list/review-credit-app-request-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ReviewCreditAppRequestListComponent, ReviewCreditAppRequestItemComponent],
  providers: [ReviewCreditAppRequestService, ReviewCreditAppRequestItemCustomComponent, ReviewCreditAppRequestListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ReviewCreditAppRequestListComponent, ReviewCreditAppRequestItemComponent]
})
export class ReviewCreditAppRequestComponentModule {}
