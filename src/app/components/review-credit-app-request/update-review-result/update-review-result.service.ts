import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { UpdateReviewResultView } from 'shared/models/viewModel/updateReviewResultView';
@Injectable()
export class UpdateReviewResultService {
  serviceKey = 'updateReviewResultGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getUpdateReviewResultById(id: string): Observable<UpdateReviewResultView> {
    const url = `${this.servicePath}/GetUpdateReviewResultById/id=${id}`;
    return this.dataGateway.get(url);
  }
  updateReviewResult(vmModel: UpdateReviewResultView): Observable<UpdateReviewResultView> {
    const url = `${this.servicePath}/UpdateReviewResult`;
    return this.dataGateway.post(url, vmModel);
  }
}
