import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { DocumentReasonItemView } from 'shared/models/viewModel';
import { UpdateReviewResultView } from 'shared/models/viewModel/updateReviewResultView';

const firstGroup = ['CREDIT_APP_REQUEST_ID'];

export class UpdateReviewResultCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: UpdateReviewResultView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: UpdateReviewResultView): Observable<boolean> {
    return of(false);
  }
  getInitialData(): Observable<UpdateReviewResultView> {
    let model = new UpdateReviewResultView();
    model.creditAppRequestTableGUID = EmptyGuid;
    return of(model);
  }
}
