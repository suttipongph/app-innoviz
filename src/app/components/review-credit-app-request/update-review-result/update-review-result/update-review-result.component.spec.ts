import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateReviewResultComponent } from './update-review-result.component';

describe('UpdateReviewResultViewComponent', () => {
  let component: UpdateReviewResultComponent;
  let fixture: ComponentFixture<UpdateReviewResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UpdateReviewResultComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateReviewResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
