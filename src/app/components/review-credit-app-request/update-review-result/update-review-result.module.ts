import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateReviewResultService } from './update-review-result.service';
import { UpdateReviewResultComponent } from './update-review-result/update-review-result.component';
import { UpdateReviewResultCustomComponent } from './update-review-result/update-review-result-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [UpdateReviewResultComponent],
  providers: [UpdateReviewResultService, UpdateReviewResultCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [UpdateReviewResultComponent]
})
export class UpdateReviewResultComponentModule {}
