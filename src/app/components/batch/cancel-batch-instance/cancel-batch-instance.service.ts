import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CancelBatchInstanceView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CancelBatchInstanceService {
  serviceKey = 'cancelBatchInstanceGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCancelBatchInstanceToList(search: SearchParameter): Observable<SearchResult<CancelBatchInstanceView>> {
    const url = `${this.servicePath}/GetCancelBatchInstanceList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCancelBatchInstance(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCancelBatchInstance`;
    return this.dataGateway.delete(url, row);
  }
  getCancelBatchInstanceById(id: string): Observable<CancelBatchInstanceView> {
    const url = `${this.servicePath}/GetCancelBatchInstanceById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCancelBatchInstance(vmModel: CancelBatchInstanceView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCancelBatchInstance`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCancelBatchInstance(vmModel: CancelBatchInstanceView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCancelBatchInstance`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  cancelBatchInstance(vwModel: CancelBatchInstanceView): Observable<boolean> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vwModel);
  }
  getCancelBatchInstanceInitialData(vwModel: CancelBatchInstanceView): Observable<CancelBatchInstanceView> {
    const url = `${this.servicePath}/GetCancelBatchInstanceInitialData`;
    return this.dataGateway.post(url, vwModel);
  }
}
