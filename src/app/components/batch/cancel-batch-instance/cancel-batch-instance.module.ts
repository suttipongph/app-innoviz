import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CancelBatchInstanceService } from './cancel-batch-instance.service';
import { CancelBatchInstanceComponent } from './cancel-batch-instance/cancel-batch-instance.component';
import { CancelBatchInstanceCustomComponent } from './cancel-batch-instance/cancel-batch-instance-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CancelBatchInstanceComponent],
  providers: [CancelBatchInstanceService, CancelBatchInstanceCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CancelBatchInstanceComponent]
})
export class CancelBatchInstanceComponentModule {}
