import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelBatchInstanceComponent } from './cancel-batch-instance.component';

describe('CancelBatchInstanceViewComponent', () => {
  let component: CancelBatchInstanceComponent;
  let fixture: ComponentFixture<CancelBatchInstanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CancelBatchInstanceComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelBatchInstanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
