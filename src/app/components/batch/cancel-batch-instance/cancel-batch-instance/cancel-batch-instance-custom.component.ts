import { Observable, of } from 'rxjs';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CancelBatchInstanceView } from 'shared/models/viewModel';

const firstGroup = [];

export class CancelBatchInstanceCustomComponent {
  baseService: BaseServiceModel<any>;

  getFieldAccessing(model: CancelBatchInstanceView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(id: string): Observable<CancelBatchInstanceView> {
    let model = new CancelBatchInstanceView();
    model.instanceHistoryId = id;
    return of(model);
  }
}
