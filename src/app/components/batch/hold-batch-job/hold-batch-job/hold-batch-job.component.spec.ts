import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HoldBatchJobComponent } from './hold-batch-job.component';

describe('HoldBatchJobViewComponent', () => {
  let component: HoldBatchJobComponent;
  let fixture: ComponentFixture<HoldBatchJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HoldBatchJobComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HoldBatchJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
