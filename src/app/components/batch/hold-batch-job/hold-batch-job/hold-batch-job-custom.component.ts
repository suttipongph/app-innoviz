import { Observable, of } from 'rxjs';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { HoldBatchJobView } from 'shared/models/viewModel';

const firstGroup = ['DOCUMENT_ID', 'NUMBER'];

export class HoldBatchJobCustomComponent {
  baseService: BaseServiceModel<any>;

  getFieldAccessing(model: HoldBatchJobView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(id: string): Observable<HoldBatchJobView> {
    let model = new HoldBatchJobView();
    model.jobId = id;
    return of(model);
  }
}
