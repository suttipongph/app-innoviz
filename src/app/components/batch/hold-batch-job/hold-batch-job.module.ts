import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HoldBatchJobService } from './hold-batch-job.service';
import { HoldBatchJobComponent } from './hold-batch-job/hold-batch-job.component';
import { HoldBatchJobCustomComponent } from './hold-batch-job/hold-batch-job-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [HoldBatchJobComponent],
  providers: [HoldBatchJobService, HoldBatchJobCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [HoldBatchJobComponent]
})
export class HoldBatchJobComponentModule {}
