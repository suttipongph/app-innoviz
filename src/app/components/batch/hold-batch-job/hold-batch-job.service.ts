import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HoldBatchJobView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class HoldBatchJobService {
  serviceKey = 'sendVendorInvoiceStagingGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getHoldBatchJobToList(search: SearchParameter): Observable<SearchResult<HoldBatchJobView>> {
    const url = `${this.servicePath}/GetHoldBatchJobList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteHoldBatchJob(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteHoldBatchJob`;
    return this.dataGateway.delete(url, row);
  }
  getHoldBatchJobById(id: string): Observable<HoldBatchJobView> {
    const url = `${this.servicePath}/GetHoldBatchJobById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createHoldBatchJob(vmModel: HoldBatchJobView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateHoldBatchJob`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateHoldBatchJob(vmModel: HoldBatchJobView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateHoldBatchJob`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  holdBatchJob(vwModel: HoldBatchJobView): Observable<boolean> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vwModel);
  }
  getHoldBatchJobInitialData(vwModel: HoldBatchJobView): Observable<HoldBatchJobView> {
    const url = `${this.servicePath}/GetHoldBatchJobInitialData`;
    return this.dataGateway.post(url, vwModel);
  }
}
