import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CleanUpBatchHistoryView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CleanUpBatchHistoryService {
  serviceKey = 'cleanUpBatchHistoryGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCleanUpBatchHistoryToList(search: SearchParameter): Observable<SearchResult<CleanUpBatchHistoryView>> {
    const url = `${this.servicePath}/GetCleanUpBatchHistoryList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCleanUpBatchHistory(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCleanUpBatchHistory`;
    return this.dataGateway.delete(url, row);
  }
  getCleanUpBatchHistoryById(id: string): Observable<CleanUpBatchHistoryView> {
    const url = `${this.servicePath}/GetCleanUpBatchHistoryById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCleanUpBatchHistory(vmModel: CleanUpBatchHistoryView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCleanUpBatchHistory`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCleanUpBatchHistory(vmModel: CleanUpBatchHistoryView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCleanUpBatchHistory`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  cleanUpBatchHistory(vwModel: CleanUpBatchHistoryView): Observable<boolean> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vwModel);
  }
  getCleanUpBatchHistoryInitialData(vwModel: CleanUpBatchHistoryView): Observable<CleanUpBatchHistoryView> {
    const url = `${this.servicePath}/GetCleanUpBatchHistoryInitialData`;
    return this.dataGateway.post(url, vwModel);
  }
}
