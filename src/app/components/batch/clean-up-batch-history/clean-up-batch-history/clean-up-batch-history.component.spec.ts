import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CleanUpBatchHistoryComponent } from './clean-up-batch-history.component';

describe('CleanUpBatchHistoryViewComponent', () => {
  let component: CleanUpBatchHistoryComponent;
  let fixture: ComponentFixture<CleanUpBatchHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CleanUpBatchHistoryComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CleanUpBatchHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
