import { Observable, of } from 'rxjs';
import { isDateFromGreaterThanDateTo } from 'shared/functions/date.function';
import { isNullOrUndefined, isUndefinedOrZeroLength } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CleanUpBatchHistoryView } from 'shared/models/viewModel';

const firstGroup = ['DOCUMENT_ID', 'NUMBER'];

export class CleanUpBatchHistoryCustomComponent {
  baseService: BaseServiceModel<any>;
  refId: string = '';
  refType: number = 0;
  refGUID: string = '';
  tableLabel: string = '';
  getFieldAccessing(model: CleanUpBatchHistoryView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(model: CleanUpBatchHistoryView): Observable<boolean> {
    const passValidate: boolean = !(
      isUndefinedOrZeroLength(model.instanceState) &&
      isUndefinedOrZeroLength(model.fromCreatedDate) &&
      isUndefinedOrZeroLength(model.toCreatedDate)
    );
    return of(passValidate);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(id: string): Observable<CleanUpBatchHistoryView> {
    let model = new CleanUpBatchHistoryView();
    model.jobId = id;
    return of(model);
  }

  getCreatedDateValidation(model: CleanUpBatchHistoryView): any {
    const condition = isDateFromGreaterThanDateTo(model.fromCreatedDate, model.toCreatedDate);

    if (condition) {
      return {
        code: 'ERROR.DATE_CANNOT_LESS_THAN',
        parameters: [this.baseService.translate.instant('LABEL.FROM_CREATED_DATE'), this.baseService.translate.instant('LABEL.TO_CREATED_DATE')]
      };
    }
    return null;
  }
}
