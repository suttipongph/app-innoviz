import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, BatchInstanceState } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { CleanUpBatchHistoryView } from 'shared/models/viewModel';
import { CleanUpBatchHistoryService } from '../clean-up-batch-history.service';
import { CleanUpBatchHistoryCustomComponent } from './clean-up-batch-history-custom.component';
@Component({
  selector: 'clean-up-batch-history',
  templateUrl: './clean-up-batch-history.component.html',
  styleUrls: ['./clean-up-batch-history.component.scss']
})
export class CleanUpBatchHistoryComponent extends BaseItemComponent<CleanUpBatchHistoryView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  instanceStateOptions: SelectItems[] = [];
  constructor(
    private service: CleanUpBatchHistoryService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CleanUpBatchHistoryCustomComponent
  ) {
    super();
    this.baseService.service = this.service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialCreatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getBatchInstanceStateEnumDropDown(this.instanceStateOptions);
    this.instanceStateOptions = this.instanceStateOptions.filter(
      (item) => item.value !== BatchInstanceState.EXECUTING && item.value !== BatchInstanceState.RETRYING
    );
  }
  getById(): Observable<CleanUpBatchHistoryView> {
    return this.service.getCleanUpBatchHistoryById(this.id);
  }
  getInitialData(): Observable<CleanUpBatchHistoryView> {
    return this.custom.getInitialData(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      } else {
        const topic: TranslateModel = { code: 'ERROR.ERROR' };
        const contents: TranslateModel[] = [{ code: 'ERROR.00476' }];
        this.notificationService.showErrorMessageFromManual(topic, contents);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    super.onExecuteFunction(this.service.cleanUpBatchHistory(this.model));
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation(this.model);
  }
  onClose(): void {
    super.onBack();
  }
  onDateFromDateToValidation(): void {
    return this.custom.getCreatedDateValidation(this.model);
  }
}
