import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CleanUpBatchHistoryService } from './clean-up-batch-history.service';
import { CleanUpBatchHistoryComponent } from './clean-up-batch-history/clean-up-batch-history.component';
import { CleanUpBatchHistoryCustomComponent } from './clean-up-batch-history/clean-up-batch-history-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CleanUpBatchHistoryComponent],
  providers: [CleanUpBatchHistoryService, CleanUpBatchHistoryCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CleanUpBatchHistoryComponent]
})
export class CleanUpBatchHistoryComponentModule {}
