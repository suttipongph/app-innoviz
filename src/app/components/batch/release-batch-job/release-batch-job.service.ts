import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ReleaseBatchJobView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ReleaseBatchJobService {
  serviceKey = 'sendVendorInvoiceStagingGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getReleaseBatchJobToList(search: SearchParameter): Observable<SearchResult<ReleaseBatchJobView>> {
    const url = `${this.servicePath}/GetReleaseBatchJobList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteReleaseBatchJob(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteReleaseBatchJob`;
    return this.dataGateway.delete(url, row);
  }
  getReleaseBatchJobById(id: string): Observable<ReleaseBatchJobView> {
    const url = `${this.servicePath}/GetReleaseBatchJobById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createReleaseBatchJob(vmModel: ReleaseBatchJobView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateReleaseBatchJob`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateReleaseBatchJob(vmModel: ReleaseBatchJobView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateReleaseBatchJob`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  releaseBatchJob(vwModel: ReleaseBatchJobView): Observable<boolean> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vwModel);
  }
  getReleaseBatchJobInitialData(vwModel: ReleaseBatchJobView): Observable<ReleaseBatchJobView> {
    const url = `${this.servicePath}/GetReleaseBatchJobInitialData`;
    return this.dataGateway.post(url, vwModel);
  }
}
