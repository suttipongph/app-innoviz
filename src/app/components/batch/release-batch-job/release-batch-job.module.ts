import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReleaseBatchJobService } from './release-batch-job.service';
import { ReleaseBatchJobComponent } from './release-batch-job/release-batch-job.component';
import { ReleaseBatchJobCustomComponent } from './release-batch-job/release-batch-job-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ReleaseBatchJobComponent],
  providers: [ReleaseBatchJobService, ReleaseBatchJobCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ReleaseBatchJobComponent]
})
export class ReleaseBatchJobComponentModule {}
