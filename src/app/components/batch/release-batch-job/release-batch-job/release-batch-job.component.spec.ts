import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReleaseBatchJobComponent } from './release-batch-job.component';

describe('ReleaseBatchJobViewComponent', () => {
  let component: ReleaseBatchJobComponent;
  let fixture: ComponentFixture<ReleaseBatchJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReleaseBatchJobComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReleaseBatchJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
