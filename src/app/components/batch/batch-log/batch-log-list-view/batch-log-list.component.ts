import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, Format, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import {} from 'shared/models/viewModel';
import { BatchLogService } from '../batch-log.service';
import { BatchLogListCustomComponent } from './batch-log-list-custom.component';
import { BatchInstanceLogView } from 'shared/models/viewModel/batchInstanceLogView';
import { BatchService } from 'core/services/batch.service';

@Component({
  selector: 'batch-log-list',
  templateUrl: './batch-log-list.component.html',
  styleUrls: ['./batch-log-list.component.scss']
})
export class BatchLogListComponent extends BaseListComponent<BatchInstanceLogView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  @Input('parentKey') parentKey: string;
  batchResultStatusOptions: SelectItems[] = [];
  constructor(private service: BatchService, public custom: BatchLogListCustomComponent) {
    super();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true));
  }
  onEnumLoader(): void {
    this.baseDropdown.getBatchResultStatusEnumDropDown(this.batchResultStatusOptions);
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'id';
    this.option.canCreate = false;
    this.option.canView = true;
    this.option.canDelete = false;
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.TIME_STAMP',
        textKey: 'timeStamp',
        //subTextKey: 'timeStampSort',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.DESC,
        format: Format.DATE_TIME
      },
      {
        label: 'LABEL.ID',
        textKey: 'id',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'status',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.batchResultStatusOptions
      },
      {
        label: 'LABEL.REFERENCE',
        textKey: 'reference',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.MESSAGE',
        textKey: 'message',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.VALUES',
        textKey: 'values',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'instanceHistoryId',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentKey
      }
      // {
      //   label: null,
      //   textKey: 'BatchInstanceLog',
      //   type: ColumnType.CONTROLLER,
      //   visibility: false,
      //   sorting: SortType.NONE
      // }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<BatchInstanceLogView>> {
    return this.service.getBatchInstanceLogList(this.parentKey, search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    // super.onDelete(row, this.service.deleteBatchLog(row));
  }
}
