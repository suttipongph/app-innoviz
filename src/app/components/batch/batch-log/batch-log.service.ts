import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';

@Injectable()
export class BatchLogService {
  serviceKey = 'batchLogGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  // getBatchLogToList(search: SearchParameter): Observable<SearchResult<BatchLogListView>> {
  //   const url = `${this.servicePath}/GetBatchLogList/${search.branchFilterMode}`;
  //   return this.dataGateway.getList(url, search);
  // }
  // deleteBatchLog(row: RowIdentity): Observable<boolean> {
  //   const url = `${this.servicePath}/DeleteBatchLog`;
  //   return this.dataGateway.delete(url, row);
  // }
  // getBatchLogById(id: string): Observable<BatchLogItemView> {
  //   const url = `${this.servicePath}/GetBatchLogById/id=${id}`;
  //   return this.dataGateway.get(url);
  // }
  // createBatchLog(vmModel: BatchLogItemView): Observable<ResponseModel> {
  //   const url = `${this.servicePath}/CreateBatchLog`;
  //   return this.dataGateway.create(url, vmModel, this.serviceKey);
  // }
  // updateBatchLog(vmModel: BatchLogItemView): Observable<ResponseModel> {
  //   const url = `${this.servicePath}/UpdateBatchLog`;
  //   return this.dataGateway.update(url, vmModel, this.serviceKey);
  // }
}
