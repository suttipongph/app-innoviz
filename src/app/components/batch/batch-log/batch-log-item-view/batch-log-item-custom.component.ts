import { Observable, of } from 'rxjs';
import { FieldAccessing } from 'shared/models/systemModel';
import { BatchInstanceLogView } from 'shared/models/viewModel';

const firstGroup = [];
export class BatchLogItemCustomComponent {
  getFieldAccessing(model: BatchInstanceLogView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(): Observable<boolean> {
    return of(!false);
  }
}
