import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BatchLogService } from './batch-log.service';
import { BatchLogListComponent } from './batch-log-list-view/batch-log-list.component';
import { BatchLogItemComponent } from './batch-log-item-view/batch-log-item.component';
import { BatchLogItemCustomComponent } from './batch-log-item-view/batch-log-item-custom.component';
import { BatchLogListCustomComponent } from './batch-log-list-view/batch-log-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BatchLogListComponent, BatchLogItemComponent],
  providers: [BatchLogService, BatchLogItemCustomComponent, BatchLogListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BatchLogListComponent, BatchLogItemComponent]
})
export class BatchLogComponentModule {}
