import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { BatchService } from 'core/services/batch.service';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { BatchInstanceState, EmptyGuid, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { BatchHistoryView } from 'shared/models/viewModel';
import { BatchHistoryItemCustomComponent } from './batch-history-item-custom.component';
@Component({
  selector: 'batch-history-item',
  templateUrl: './batch-history-item.component.html',
  styleUrls: ['./batch-history-item.component.scss']
})
export class BatchHistoryItemComponent extends BaseItemComponent<BatchHistoryView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  batchInstanceStateOptions: SelectItems[] = [];
  constructor(private service: BatchService, private currentActivatedRoute: ActivatedRoute, public custom: BatchHistoryItemCustomComponent) {
    super();
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {
    this.baseDropdown.getBatchInstanceStateEnumDropDown(this.batchInstanceStateOptions);
  }
  getById(): Observable<BatchHistoryView> {
    return this.service.getBatchInstanceHistoryByKey(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.service.validateRetryFunction(result).subscribe((retryDisabled) => {
          this.custom.retryDisabled = retryDisabled;
          this.setFunctionOptions(result);
        });
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.onAsyncRunner();
    this.model.instanceHistoryId = EmptyGuid;
    super.setDefaultValueSystemFields();
  }
  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight().subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {}
  setFunctionOptions(model: BatchHistoryView): void {
    const cancelDisabled: boolean = this.custom.getCancelDisabled(model);
    const retryDisabled: boolean = this.custom.getRetryDisabled(cancelDisabled);
    this.functionItems = [
      {
        label: 'LABEL.CANCEL',
        disabled: cancelDisabled,
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.CANCEL_BATCH_INSTANCE })
      },
      {
        label: 'LABEL.RETRY',
        disabled: retryDisabled,
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.RETRY_BATCH_INSTANCE })
      }
    ];
    super.setFunctionOptionsMapping();
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    // if (this.isUpdateMode) {
    //   super.onUpdate(this.service.updateBatchHistory(this.model), isColsing);
    // } else {
    //   super.onCreate(this.service.createBatchHistory(this.model), isColsing);
    // }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
