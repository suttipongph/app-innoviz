import { Observable, of } from 'rxjs';
import { BatchInstanceState } from 'shared/constants';
import { FieldAccessing } from 'shared/models/systemModel';
import { BatchHistoryView } from 'shared/models/viewModel';

const firstGroup = [];
export class BatchHistoryItemCustomComponent {
  retryDisabled: boolean = true;
  getFieldAccessing(model: BatchHistoryView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(): Observable<boolean> {
    return of(!false);
  }

  getRetryDisabled(cancelDisabled: boolean): boolean {
    return cancelDisabled || this.retryDisabled;
  }
  getCancelDisabled(model: BatchHistoryView): boolean {
    const cancelDisabled: boolean =
      model.instanceState === BatchInstanceState.EXECUTING ||
      model.instanceState === BatchInstanceState.RETRYING ||
      model.instanceState === BatchInstanceState.CANCELLED ||
      model.instanceState === BatchInstanceState.RETRIED ||
      model.instanceState === BatchInstanceState.COMPLETED ||
      model.instanceState === BatchInstanceState.RETRY_COMPLETED;
    return cancelDisabled;
  }
}
