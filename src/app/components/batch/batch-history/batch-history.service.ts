import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';

@Injectable()
export class BatchHistoryService {
  serviceKey = 'batchHistoryGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  // getBatchHistoryToList(search: SearchParameter): Observable<SearchResult<BatchHistoryListView>> {
  //   const url = `${this.servicePath}/GetBatchHistoryList/${search.branchFilterMode}`;
  //   return this.dataGateway.getList(url, search);
  // }
  // deleteBatchHistory(row: RowIdentity): Observable<boolean> {
  //   const url = `${this.servicePath}/DeleteBatchHistory`;
  //   return this.dataGateway.delete(url, row);
  // }
  // getBatchHistoryById(id: string): Observable<BatchHistoryItemView> {
  //   const url = `${this.servicePath}/GetBatchHistoryById/id=${id}`;
  //   return this.dataGateway.get(url);
  // }
  // createBatchHistory(vmModel: BatchHistoryItemView): Observable<ResponseModel> {
  //   const url = `${this.servicePath}/CreateBatchHistory`;
  //   return this.dataGateway.create(url, vmModel, this.serviceKey);
  // }
  // updateBatchHistory(vmModel: BatchHistoryItemView): Observable<ResponseModel> {
  //   const url = `${this.servicePath}/UpdateBatchHistory`;
  //   return this.dataGateway.update(url, vmModel, this.serviceKey);
  // }
}
