import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { BatchHistoryListCustomComponent } from './batch-history-list-custom.component';
import { BatchService } from 'core/services/batch.service';
import { BatchHistoryView } from 'shared/models/viewModel/batchHistoryView';

@Component({
  selector: 'batch-history-list',
  templateUrl: './batch-history-list.component.html',
  styleUrls: ['./batch-history-list.component.scss']
})
export class BatchHistoryListComponent extends BaseListComponent<BatchHistoryView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  @Input('parentKey') parentKey: string;
  batchInstanceStateOptions: SelectItems[] = [];
  defaultBitOptions: SelectItems[] = [];
  constructor(private service: BatchService, public custom: BatchHistoryListCustomComponent) {
    super();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true));
  }
  onEnumLoader(): void {
    this.baseDropdown.getBatchInstanceStateEnumDropDown(this.batchInstanceStateOptions);
    this.baseDropdown.getDefaultBitDropDown(this.defaultBitOptions);
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'instanceHistoryId';
    this.option.canCreate = false;
    this.option.canView = true;
    this.option.canDelete = false;
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.INSTANCE_STATE',
        textKey: 'instanceState',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.batchInstanceStateOptions
      },
      {
        label: 'LABEL.CONTROLLER_URL',
        textKey: 'controllerUrl',
        type: ColumnType.STRING,
        visibility: false,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.ACTUAL_START_TIME',
        textKey: 'actualStartDateTime',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.FINISHED_DATE_TIME',
        textKey: 'finishedDateTime',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.ANY_FAILED_LOG_STATUS',
        textKey: 'anyFailedLogStatus',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defaultBitOptions
      },
      {
        label: null,
        textKey: 'jobId',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentKey
      }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<BatchHistoryView>> {
    return this.service.getBatchInstanceHistoryList(this.parentKey, search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    //super.onDelete(row, this.service.deleteBatchHistory(row));
  }
}
