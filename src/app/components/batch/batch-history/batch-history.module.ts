import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BatchHistoryService } from './batch-history.service';
import { BatchHistoryItemCustomComponent } from './batch-history-item-view/batch-history-item-custom.component';
import { BatchHistoryListCustomComponent } from './batch-history-list-view/batch-history-list-custom.component';
import { BatchHistoryListComponent } from './batch-history-list-view/batch-history-list.component';
import { BatchHistoryItemComponent } from './batch-history-item-view/batch-history-item.component';
import { BatchLogComponentModule } from '../batch-log/batch-log.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, BatchLogComponentModule],
  declarations: [BatchHistoryListComponent, BatchHistoryItemComponent],
  providers: [BatchHistoryService, BatchHistoryItemCustomComponent, BatchHistoryListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BatchHistoryListComponent, BatchHistoryItemComponent]
})
export class BatchHistoryComponentModule {}
