import { Observable, of } from 'rxjs';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { RetryBatchInstanceView } from 'shared/models/viewModel';

const firstGroup = [];

export class RetryBatchInstanceCustomComponent {
  baseService: BaseServiceModel<any>;

  getFieldAccessing(model: RetryBatchInstanceView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(id: string): Observable<RetryBatchInstanceView> {
    let model = new RetryBatchInstanceView();
    model.instanceHistoryId = id;
    return of(model);
  }
}
