import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetryBatchInstanceComponent } from './retry-batch-instance.component';

describe('RetryBatchInstanceViewComponent', () => {
  let component: RetryBatchInstanceComponent;
  let fixture: ComponentFixture<RetryBatchInstanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RetryBatchInstanceComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetryBatchInstanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
