import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { RetryBatchInstanceView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class RetryBatchInstanceService {
  serviceKey = 'sendVendorInvoiceStagingGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getRetryBatchInstanceToList(search: SearchParameter): Observable<SearchResult<RetryBatchInstanceView>> {
    const url = `${this.servicePath}/GetRetryBatchInstanceList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteRetryBatchInstance(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteRetryBatchInstance`;
    return this.dataGateway.delete(url, row);
  }
  getRetryBatchInstanceById(id: string): Observable<RetryBatchInstanceView> {
    const url = `${this.servicePath}/GetRetryBatchInstanceById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createRetryBatchInstance(vmModel: RetryBatchInstanceView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateRetryBatchInstance`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateRetryBatchInstance(vmModel: RetryBatchInstanceView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateRetryBatchInstance`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  retryBatchInstance(vwModel: RetryBatchInstanceView): Observable<boolean> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vwModel);
  }
  getRetryBatchInstanceInitialData(vwModel: RetryBatchInstanceView): Observable<RetryBatchInstanceView> {
    const url = `${this.servicePath}/GetRetryBatchInstanceInitialData`;
    return this.dataGateway.post(url, vwModel);
  }
}
