import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RetryBatchInstanceService } from './retry-batch-instance.service';
import { RetryBatchInstanceComponent } from './retry-batch-instance/retry-batch-instance.component';
import { RetryBatchInstanceCustomComponent } from './retry-batch-instance/retry-batch-instance-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [RetryBatchInstanceComponent],
  providers: [RetryBatchInstanceService, RetryBatchInstanceCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [RetryBatchInstanceComponent]
})
export class RetryBatchInstanceComponentModule {}
