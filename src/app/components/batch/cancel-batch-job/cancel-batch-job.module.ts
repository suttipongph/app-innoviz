import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CancelBatchJobService } from './cancel-batch-job.service';
import { CancelBatchJobComponent } from './cancel-batch-job/cancel-batch-job.component';
import { CancelBatchJobCustomComponent } from './cancel-batch-job/cancel-batch-job-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CancelBatchJobComponent],
  providers: [CancelBatchJobService, CancelBatchJobCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CancelBatchJobComponent]
})
export class CancelBatchJobComponentModule {}
