import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelBatchJobComponent } from './cancel-batch-job.component';

describe('CancelBatchJobViewComponent', () => {
  let component: CancelBatchJobComponent;
  let fixture: ComponentFixture<CancelBatchJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CancelBatchJobComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelBatchJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
