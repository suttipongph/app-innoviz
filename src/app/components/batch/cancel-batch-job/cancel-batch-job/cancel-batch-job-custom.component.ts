import { Observable, of } from 'rxjs';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CancelBatchJobView } from 'shared/models/viewModel';

const firstGroup = ['DOCUMENT_ID', 'NUMBER'];

export class CancelBatchJobCustomComponent {
  baseService: BaseServiceModel<any>;
  refId: string = '';
  refType: number = 0;
  refGUID: string = '';
  tableLabel: string = '';
  getFieldAccessing(model: CancelBatchJobView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(id: string): Observable<CancelBatchJobView> {
    let model = new CancelBatchJobView();
    model.jobId = id;
    return of(model);
  }
}
