import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CancelBatchJobView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CancelBatchJobService {
  serviceKey = 'cancelBatchJobGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCancelBatchJobToList(search: SearchParameter): Observable<SearchResult<CancelBatchJobView>> {
    const url = `${this.servicePath}/GetCancelBatchJobList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCancelBatchJob(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCancelBatchJob`;
    return this.dataGateway.delete(url, row);
  }
  getCancelBatchJobById(id: string): Observable<CancelBatchJobView> {
    const url = `${this.servicePath}/GetCancelBatchJobById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCancelBatchJob(vmModel: CancelBatchJobView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCancelBatchJob`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCancelBatchJob(vmModel: CancelBatchJobView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCancelBatchJob`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  cancelBatchJob(vwModel: CancelBatchJobView): Observable<boolean> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vwModel);
  }
  getCancelBatchJobInitialData(vwModel: CancelBatchJobView): Observable<CancelBatchJobView> {
    const url = `${this.servicePath}/GetCancelBatchJobInitialData`;
    return this.dataGateway.post(url, vwModel);
  }
}
