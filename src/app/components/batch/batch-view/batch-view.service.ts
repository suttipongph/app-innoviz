import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
// import { BatchViewItemView, BatchViewListView } from 'shared/models/viewModel';

@Injectable()
export class BatchViewService {
  serviceKey = 'batchViewGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  // getBatchViewToList(search: SearchParameter): Observable<SearchResult<BatchViewListView>> {
  //   const url = `${this.servicePath}/GetBatchViewList/${search.branchFilterMode}`;
  //   return this.dataGateway.getList(url, search);
  // }
  // deleteBatchView(row: RowIdentity): Observable<boolean> {
  //   const url = `${this.servicePath}/DeleteBatchView`;
  //   return this.dataGateway.delete(url, row);
  // }
  // getBatchViewById(id: string): Observable<BatchViewItemView> {
  //   const url = `${this.servicePath}/GetBatchViewById/id=${id}`;
  //   return this.dataGateway.get(url);
  // }
  // createBatchView(vmModel: BatchViewItemView): Observable<ResponseModel> {
  //   const url = `${this.servicePath}/CreateBatchView`;
  //   return this.dataGateway.create(url, vmModel, this.serviceKey);
  // }
  // updateBatchView(vmModel: BatchViewItemView): Observable<ResponseModel> {
  //   const url = `${this.servicePath}/UpdateBatchView`;
  //   return this.dataGateway.update(url, vmModel, this.serviceKey);
  // }
}
