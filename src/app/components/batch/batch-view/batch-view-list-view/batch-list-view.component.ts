import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { AccessMode, BatchJobStatus, ColumnType, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { BatchView } from 'shared/models/viewModel';
import { BatchViewListCustomComponent } from './batch-list-view-custom.component';
import { BatchService } from 'core/services/batch.service';

@Component({
  selector: 'batch-list-view',
  templateUrl: './batch-list-view.component.html',
  styleUrls: ['./batch-list-view.component.scss']
})
export class BatchListViewComponent extends BaseListComponent<BatchView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  jobStatusOptions: SelectItems[] = [];
  instanceStateOptions: SelectItems[] = [];
  constructor(private service: BatchService, public custom: BatchViewListCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getBatchJobStatusEnumDropDown(this.jobStatusOptions);
    this.baseDropdown.getBatchInstanceStateEnumDropDown(this.instanceStateOptions);
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'jobId';
    this.option.canCreate = false;
    this.option.canView = true;
    this.option.canDelete = this.getCanDelete();
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.JOB_STATUS',
        textKey: 'jobStatus',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.jobStatusOptions
      },
      {
        label: 'LABEL.CONTROLLER_URL',
        textKey: 'controllerUrl',
        type: ColumnType.STRING,
        visibility: false,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.LAST_EXECUTED_INSTANCE_STATUS',
        textKey: 'lastExecutedInstanceState',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.instanceStateOptions
      },
      {
        label: 'LABEL.NEXT_SCHEDULED_TIME',
        textKey: 'nextScheduledDateTime',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BATCH_LAST_EXECUTED_DATE_TIME',
        textKey: 'lastExecutedDateTime',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.CREATED_BY',
        textKey: 'createdBy',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<BatchView>> {
    return this.service.getBatchViewList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteBatchJob(row));
  }
  getRowAuthorize(row: BatchView[]): void {
    row.forEach((item) => {
      const businessRowAuth =
        item.jobStatus !== BatchJobStatus.CANCELLED && item.jobStatus !== BatchJobStatus.ENDED ? AccessMode.viewer : AccessMode.full;
      item.rowAuthorize = this.getSingleRowAuthorize(businessRowAuth, item);
    });
  }
}
