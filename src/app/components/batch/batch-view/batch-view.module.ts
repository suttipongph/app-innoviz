import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BatchViewService } from './batch-view.service';
import { BatchListViewComponent } from './batch-view-list-view/batch-list-view.component';
import { BatchItemViewComponent } from './batch-view-item-view/batch-item-view.component';
import { BatchViewItemCustomComponent } from './batch-view-item-view/batch-item-view-custom.component';
import { BatchViewListCustomComponent } from './batch-view-list-view/batch-list-view-custom.component';
import { BatchHistoryComponentModule } from '../batch-history/batch-history.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, BatchHistoryComponentModule],
  declarations: [BatchListViewComponent, BatchItemViewComponent],
  providers: [BatchViewService, BatchViewItemCustomComponent, BatchViewListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BatchListViewComponent, BatchItemViewComponent]
})
export class BatchViewComponentModule {}
