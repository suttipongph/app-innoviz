import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { BatchService } from 'core/services/batch.service';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import {
  BatchIntervalType,
  BatchJobStatus,
  BATCH_DAYS,
  BATCH_HOURS,
  BATCH_MINUTES,
  BATCH_MONTHS,
  BATCH_YEARS,
  EmptyGuid,
  ROUTE_FUNCTION_GEN
} from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { getBeautifyJsonString, isNullOrUndefined, isUndefinedOrZeroLength, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { BatchView } from 'shared/models/viewModel';
import { BatchViewService } from '../batch-view.service';
import { BatchViewItemCustomComponent } from './batch-item-view-custom.component';
@Component({
  selector: 'batch-item-view',
  templateUrl: './batch-item-view.component.html',
  styleUrls: ['./batch-item-view.component.scss']
})
export class BatchItemViewComponent extends BaseItemComponent<BatchView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  batchIntervalType = BatchIntervalType;
  isUndefinedOrZeroLength = isUndefinedOrZeroLength;
  batchJobStatusOptions: SelectItems[] = [];
  batchIntervalTypeOptions: SelectItems[] = [];
  batchDayOfWeekOptions: SelectItems[] = [];
  minuteOption: SelectItems[] = [];
  hourOption: SelectItems[] = [];
  dayOption: SelectItems[] = [];
  monthOption: SelectItems[] = [];
  yearOption: SelectItems[] = [];
  intervalValue: string[] = [];
  intervalValueOption: SelectItems[] = [];
  constructor(private service: BatchService, private currentActivatedRoute: ActivatedRoute, public custom: BatchViewItemCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];

    this.minuteOption = BATCH_MINUTES;
    this.hourOption = BATCH_HOURS;
    this.dayOption = BATCH_DAYS;
    this.monthOption = BATCH_MONTHS;
    this.yearOption = BATCH_YEARS;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {
    this.baseDropdown.getBatchJobStatusEnumDropDown(this.batchJobStatusOptions);
    this.baseDropdown.getBatchIntervalTypeEnumDropDown(this.batchIntervalTypeOptions);
    this.baseDropdown.getBatchDayOfWeekEnumDropDown(this.batchDayOfWeekOptions);
  }
  getById(): Observable<BatchView> {
    return this.service.getBatchViewByJobId(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    // this.onAsyncRunner();
    // this.model.batchViewGUID = EmptyGuid;
    // super.setDefaultValueSystemFields();
  }
  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          this.model.paramValues = getBeautifyJsonString(this.model.paramValues, true);
          modelRegister(this.model);
        }
        this.setInvervalValueOption(this.model.intervalType, this.model.intervalCount);
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight().subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {}
  setFunctionOptions(model: BatchView): void {
    const cancelJobDisabled: boolean = model.isFinalized;
    const holdJobDisabled: boolean = model.jobStatus !== BatchJobStatus.WAITING;
    const releaseJobDisabled: boolean = model.jobStatus !== BatchJobStatus.ON_HOLD;
    const cleanUpHistoryDisabled: boolean = model.jobStatus !== BatchJobStatus.ENDED && this.model.jobStatus !== BatchJobStatus.CANCELLED;
    this.functionItems = [
      {
        label: 'LABEL.CANCEL',
        disabled: cancelJobDisabled,
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.CANCEL_BATCH_JOB })
      },
      {
        label: 'LABEL.HOLD',
        disabled: holdJobDisabled,
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.HOLD_BATCH_JOB })
      },
      {
        label: 'LABEL.RELEASE',
        disabled: releaseJobDisabled,
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.RELEASE_BATCH_JOB })
      },
      {
        label: 'LABEL.CLEAN_UP_BATCH_HISTORY',
        disabled: cleanUpHistoryDisabled,
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.CLEAN_UP_BATCH_HISTORY })
      }
    ];
    super.setFunctionOptionsMapping();
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    // if (this.isUpdateMode) {
    //   super.onUpdate(this.service.updateBatchView(this.model), isColsing);
    // } else {
    //   super.onCreate(this.service.createBatchView(this.model), isColsing);
    // }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  setInvervalValueOption(intervalType: number, intervalCount: string) {
    if (!isUndefinedOrZeroLength(intervalCount)) {
      this.intervalValue = intervalCount.split(',');
    } else {
      this.intervalValue = [Number(intervalCount).toString()];
    }
    switch (intervalType) {
      case BatchIntervalType.MINUTES:
        this.intervalValueOption = this.minuteOption;
        break;
      case BatchIntervalType.HOURS:
        this.intervalValueOption = this.hourOption;
        break;
      case BatchIntervalType.DAYS:
        this.intervalValueOption = this.dayOption;
        break;
      case BatchIntervalType.MONTHS:
        this.intervalValueOption = this.monthOption;
        break;
      case BatchIntervalType.YEARS:
        this.intervalValueOption = this.yearOption;
        break;
      case BatchIntervalType.DAYOFWEEK:
        this.intervalValueOption = this.batchDayOfWeekOptions;
        break;
    }
  }
}
