import { Observable, of } from 'rxjs';
import { hasJsonStructure } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { BatchView } from 'shared/models/viewModel';

const firstGroup = ['BATCH_REPEAT_EVERY'];
export class BatchViewItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: BatchView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(): Observable<boolean> {
    return of(!false);
  }

  // setFunction() {
  //   let cancelJobDisabled: boolean = this.model.isFinalized;
  //   let holdJobDisabled: boolean = this.model.jobStatus !== BatchJobStatus.WAITING;
  //   let releaseJobDisabled: boolean = this.model.jobStatus !== BatchJobStatus.ON_HOLD;
  //   let cleanUpHistoryDisabled: boolean = this.model.jobStatus !== BatchJobStatus.ENDED && this.model.jobStatus !== BatchJobStatus.CANCELLED;
  //   this.itemsFunction = [
  //     {
  //       label: this.translate.get('LABEL.CANCEL')['value'],
  //       disabled: cancelJobDisabled,
  //       command: () => this.setOpenFunction(ROUTE_FUNCTIONS.CANCELBATCHJOB),
  //       visible: this.uiService.setAuthorizedBySite([AppConst.BACK])
  //     },
  //     {
  //       label: this.translate.get('LABEL.HOLD')['value'],
  //       disabled: holdJobDisabled,
  //       command: () => this.setOpenFunction(ROUTE_FUNCTIONS.HOLDBATCHJOB),
  //       visible: this.uiService.setAuthorizedBySite([AppConst.BACK])
  //     },
  //     {
  //       label: this.translate.get('LABEL.RELEASE')['value'],
  //       disabled: releaseJobDisabled,
  //       command: () => this.setOpenFunction(ROUTE_FUNCTIONS.RELEASEBATCHJOB),
  //       visible: this.uiService.setAuthorizedBySite([AppConst.BACK])
  //     },
  //     {
  //       label: this.translate.get('LABEL.CLEAN_UP_BATCH_HISTORY')['value'],
  //       disabled: cleanUpHistoryDisabled,
  //       command: () => this.setOpenFunction(ROUTE_FUNCTIONS.CLEANUPBATCHHISTORY),
  //       visible: this.uiService.setAuthorizedBySite([AppConst.BACK])
  //     }
  //   ];
  // }
}
