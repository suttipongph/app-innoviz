import { Observable } from 'rxjs';
import { BaseServiceModel } from 'shared/models/systemModel';
import { CAReqRetentionOutstandingListView } from 'shared/models/viewModel';
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const AMEND_CA = 'amendca';
export class CAReqRetentionOutstandingListCustomComponent {
  baseService: BaseServiceModel<any>;
  parentName: string = null;
  parentId: string = null;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  getRelatedInfoParentTableKey(passingObj: CAReqRetentionOutstandingListView): string {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        this.parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
        break;
      case AMEND_CA:
        this.parentId = passingObj.creditAppRequestTableGUID;
        break;
      default:
        this.parentId = null;
        break;
    }
    return this.parentId;
  }
}
