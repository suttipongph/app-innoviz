import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CAReqRetentionOutstandingListComponent } from './ca-req-retention-outstanding-list.component';

describe('CAReqRetentionOutstandingListViewComponent', () => {
  let component: CAReqRetentionOutstandingListComponent;
  let fixture: ComponentFixture<CAReqRetentionOutstandingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CAReqRetentionOutstandingListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CAReqRetentionOutstandingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
