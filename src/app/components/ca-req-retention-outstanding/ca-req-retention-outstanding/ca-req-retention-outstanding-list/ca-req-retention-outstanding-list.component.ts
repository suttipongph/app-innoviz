import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CAReqRetentionOutstandingListView } from 'shared/models/viewModel';
import { CAReqRetentionOutstandingService } from '../ca-req-retention-outstanding.service';
import { CAReqRetentionOutstandingListCustomComponent } from './ca-req-retention-outstanding-list-custom.component';

@Component({
  selector: 'ca-req-retention-outstanding-list',
  templateUrl: './ca-req-retention-outstanding-list.component.html',
  styleUrls: ['./ca-req-retention-outstanding-list.component.scss']
})
export class CAReqRetentionOutstandingListComponent extends BaseListComponent<CAReqRetentionOutstandingListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  productTypeOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  creditAppTableOptions: SelectItems[] = [];

  constructor(
    public custom: CAReqRetentionOutstandingListCustomComponent,
    private service: CAReqRetentionOutstandingService,
    public currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.parentId = this.custom.getRelatedInfoParentTableKey(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getCreditAppTableDropDown(this.creditAppTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = false;
    this.option.canDelete = false;
    this.option.key = 'caReqRetentionOutstandingGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: 'LABEL.CREDIT_APPLICATION_ID',
        textKey: 'creditAppTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'creditAppTableGUID',
        sortingKey: 'creditAppTable_CreditAppId',
        masterList: this.creditAppTableOptions
      },
      {
        label: 'LABEL.CUSTOMER',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId',
        masterList: this.customerTableOptions
      },
      {
        label: 'LABEL.MAXIMUM_RETENTION',
        textKey: 'maximumRetention',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: 'LABEL.ACCUMULATE_RETENTION_AMOUNT',
        textKey: 'accumRetentionAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: 'LABEL.REMAINING_AMOUNT',
        textKey: 'remainingAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: null,
        textKey: 'creditAppRequestTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CAReqRetentionOutstandingListView>> {
    return this.service.getCAReqRetentionOutstandingToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCAReqRetentionOutstanding(row));
  }
}
