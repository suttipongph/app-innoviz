import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CAReqRetentionOutstandingService } from './ca-req-retention-outstanding.service';
import { CAReqRetentionOutstandingListComponent } from './ca-req-retention-outstanding-list/ca-req-retention-outstanding-list.component';
import { CAReqRetentionOutstandingListCustomComponent } from './ca-req-retention-outstanding-list/ca-req-retention-outstanding-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CAReqRetentionOutstandingListComponent],
  providers: [CAReqRetentionOutstandingService, CAReqRetentionOutstandingListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CAReqRetentionOutstandingListComponent]
})
export class CAReqRetentionOutstandingComponentModule {}
