import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CAReqRetentionOutstandingListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CAReqRetentionOutstandingService {
  serviceKey = 'caReqRetentionOutstandingGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCAReqRetentionOutstandingToList(search: SearchParameter): Observable<SearchResult<CAReqRetentionOutstandingListView>> {
    const url = `${this.servicePath}/GetCAReqRetentionOutstandingList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCAReqRetentionOutstanding(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCAReqRetentionOutstanding`;
    return this.dataGateway.delete(url, row);
  }
}
