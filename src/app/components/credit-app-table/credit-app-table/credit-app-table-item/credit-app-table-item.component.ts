import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, RefType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { CreditAppTableItemView, RefIdParm } from 'shared/models/viewModel';
import { CreditAppTableService } from '../credit-app-table.service';
import { CreditAppTableItemCustomComponent } from './credit-app-table-item-custom.component';
@Component({
  selector: 'credit-app-table-item',
  templateUrl: './credit-app-table-item.component.html',
  styleUrls: ['./credit-app-table-item.component.scss']
})
export class CreditAppTableItemComponent extends BaseItemComponent<CreditAppTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  creditLimitExpirationOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  purchaseFeeCalculateBaseOptions: SelectItems[] = [];

  constructor(
    private service: CreditAppTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CreditAppTableItemCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {
    this.baseDropdown.getCreditLimitExpirationEnumDropDown(this.creditLimitExpirationOptions);
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getPurchaseFeeCalculateBaseEnumDropDown(this.purchaseFeeCalculateBaseOptions);
  }
  getById(): Observable<CreditAppTableItemView> {
    return this.service.getCreditAppTableById(this.id);
  }
  getInitialData(): Observable<CreditAppTableItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions(result);
        this.setFunctionOptions(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  setRelatedInfoOptions(model: CreditAppTableItemView): void {
    const isFactoring = this.custom.isFactoring(model);
    const isProjectFinance = this.custom.isProjectFinance(model);
    const notHasParentCreditLimitType = this.custom.isNotHasParentCreditLimitType(model);
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        command: () => {
          const refIdParm: RefIdParm = { refType: RefType.CreditAppTable, refGUID: this.model.creditAppTableGUID };
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: refIdParm
          });
        }
      },
      {
        label: 'LABEL.MANAGE',
        items: [
          {
            label: 'LABEL.SERVICE_FEE_CONDITION',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.SERVICE_FEE_CONDITION_TRANS })
          },
          {
            label: 'LABEL.RETENTION_CONDITION',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.RETENTION_CONDITION_TRANS })
          }
        ]
      },
      {
        label: 'LABEL.RELATED_PERSON',
        items: [
          {
            label: 'LABEL.AUTHORIZED_PERSON',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.AUTHORIZED_PERSON_TRANS })
          },
          {
            label: 'LABEL.GUARANTOR',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.GUARANTOR_TRANS })
          },
          {
            label: 'LABEL.OWNER',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.OWNER_TRANS })
          }
        ]
      },
      {
        label: 'LABEL.INQUIRY',
        visible: notHasParentCreditLimitType,
        items: [
          {
            label: 'LABEL.ASSIGNMENT_AGREEMENT_OUTSTANDING'
            // command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.ASSIGNMENT_AGREEMENT_OUTSTANDING })
          },
          {
            label: 'LABEL.CREDIT_APPLICATION_TRANSACTIONS',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.CREDIT_APPLICATION_TRANSACTION })
          },
          {
            label: 'LABEL.CUSTOMER_BUYER_CREDIT_OUTSTANDING',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.CUSTOMER_BUYER_CREDIT_OUTSTANDING })
          },
          {
            label: 'LABEL.PROCESS_TRANSACTIONS',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.PROCESS_TRANSACTION })
          },
          {
            label: 'LABEL.RETENTION_TRANSACTIONS',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.RETENTION_TRANSACTION })
          }
        ]
      },
      {
        label: 'LABEL.AMEND_CA',
        visible: notHasParentCreditLimitType,
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.AMEND_CA })
      },
      {
        label: 'LABEL.VERIFICATION',
        visible: notHasParentCreditLimitType,
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.VERIFICATION_TABLE })
      },
      {
        label: 'LABEL.PURCHASE',
        visible: isFactoring && notHasParentCreditLimitType,
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.PURCHASE_TABLE_PURCHASE
          })
      },
      {
        label: 'LABEL.ROLL_BILL_PURCHASE',
        visible: isFactoring && notHasParentCreditLimitType,
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.PURCHASE_TABLE_ROLL_BILL })
      },
      {
        label: 'LABEL.WITHDRAWAL',
        visible: isProjectFinance && notHasParentCreditLimitType,
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.WITHDRAWAL_TABLE_WITHDRAWAL })
      },
      {
        label: 'LABEL.BUYER_MATCHING',
        visible: isFactoring && model.creditLimitType_BuyerMatchingAndLoanRequest && notHasParentCreditLimitType,
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.BUYER_MATCHING })
      },
      {
        label: 'LABEL.LOAN_REQUEST',
        visible: isProjectFinance && model.creditLimitType_BuyerMatchingAndLoanRequest && notHasParentCreditLimitType,
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.LOAN_REQUEST })
      },
      {
        label: 'LABEL.LEASING',
        visible: notHasParentCreditLimitType,
        items: [
          {
            label: 'LABEL.APPLICATION'
            // ,command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.APPLICATION }) }]
          }
        ]
      },
      {
        label: 'LABEL.CLOSE_CREDIT_LIMIT',
        visible: notHasParentCreditLimitType,
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.CLOSE_CREDIT_LIMIT })
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(model: CreditAppTableItemView): void {
    const canActiveUpdateExpectedSigningDate = this.custom.canActiveUpdateExpectedSigningDate(model);
    const isCreditLimitExpirationEqualByBuyerAgreement = this.custom.isCreditLimitExpirationEqualByBuyerAgreement(model);
    this.functionItems = [
      {
        label: 'LABEL.UPDATE_EXPECTED_SIGNING_DATE',
        disabled: canActiveUpdateExpectedSigningDate,
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.UPDATE_EXPECTED_SIGNING_DATE })
      },
      {
        label: 'LABEL.EXTEND_EXPIRY_DATE',
        disabled: isCreditLimitExpirationEqualByBuyerAgreement,
        visible: !isCreditLimitExpirationEqualByBuyerAgreement,
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.EXTEND_EXPIRY_DATE })
      }
    ];
    super.setFunctionOptionsMapping();
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateCreditAppTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createCreditAppTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
