import { Observable, of } from 'rxjs';
import { CreditLimitExpiration, EmptyGuid, ProductType } from 'shared/constants';
import { datetoString, isDateFromGreaterThanDateTo } from 'shared/functions/date.function';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { CreditAppTableItemView, CreditLimitTypeItemView } from 'shared/models/viewModel';

const firstGroup = [
  'CREDIT_APP_ID',
  'DESCRIPTION',
  'PRODUCT_TYPE',
  'PRODUCT_SUB_TYPE_GUID',
  'CREDIT_LIMIT_TYPE_GUID',
  'APPROVED_DATE',
  'EXPECTED_AGM_SIGNING_DATE',
  'DOCUMENT_REASON_GUID',
  'REF_CREDIT_APP_REQUEST_TABLE_GUID',
  'CUSTOMER_TABLE_GUID',
  'CONSORTIUM_TABLE_GUID',
  'TAX_IDENTIFICATION_ID',
  'DATE_OF_ESTABLISH',
  'LINE_OF_BUSINESS_GUID',
  'BUSINESS_TYPE_GUID',
  'INTRODUCED_BY_GUID',
  'REGISTERED_ADDRESS_GUID',
  'UNBOUND_REGISTERED_ADDRESS',
  'SALES_AVG_PER_MONTH',
  'BANK_ACCOUNT_CONTROL_GUID',
  'PDenC_BANK_GUID',
  'APPROVED_CREDIT_LIMIT',
  'CREDIT_LIMIT_EXPIRATION',
  'START_DATE',
  'CA_CONDITION',
  'TOTAL_INTEREST_PCT',
  'INTEREST_ADJUSTMENT',
  'INTEREST_TYPE_GUID',
  'CREDIT_REQUEST_FEE_PCT',
  'INACTIVE_DATE',
  'CREDIT_LIMIT_REMARK',
  'EXPIRY_DATE',
  'UNBOUND_MAILING_RECEIPT_ADDRESS',
  'MAILING_RECEIP_ADDRESS_GUID',
  'INVOICE_ADDRESS_GUID',
  'UNBOUND_RECEIPT_ADDRESS',
  'RECEIPT_ADDRESS_GUID',
  'RECEIPT_CONTACT_PERSON_GUID',
  'UNBOUND_BILLING_ADDRESS',
  'BILLING_ADDRESS_GUID',
  'BILLING_CONTACT_PERSON_GUID',
  'UNBOUND_INVOICE_ADDRESS',
  'NUMBER_OF_REGISTERED_BUYER',
  'PURCHASE_FEE_CALCULATE_BASE',
  'PURCHASE_FEE_PCT',
  'MAX_RETENTION_AMOUNT',
  'MAX_RETENTION_PCT',
  'MAX_PURCHASE_PCT',
  'DIMENSION5GUID',
  'DIMENSION4GUID',
  'DIMENSION3GUID',
  'DIMENSION2GUID',
  'DIMENSION1GUID',
  'APPLICATION_TABLE_GUID',
  'EXTENSION_SERVICE_FEE_COND_TEMPLATE_GUID',
  'CREDIT_TERM_GUID',
  'ASSIGNMENT_METHOD_GUID',
  'PURCHASE_WITHDRAWAL_CONDITION'
];

export class CreditAppTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CreditAppTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CreditAppTableItemView): Observable<boolean> {
    const accessright = isNullOrUndefOrEmptyGUID(model.creditAppTableGUID) ? canCreate : canUpdate;
    const accessLogic = true;
    return of(!(accessLogic && accessright));
  }
  getInitialData(): Observable<CreditAppTableItemView> {
    let model = new CreditAppTableItemView();
    model.creditAppTableGUID = EmptyGuid;
    return of(model);
  }
  //#region  function & related condition
  isProjectFinance(model: CreditAppTableItemView): boolean {
    return model.productType === ProductType.ProjectFinance;
  }
  isFactoring(model: CreditAppTableItemView): boolean {
    return model.productType === ProductType.Factoring;
  }
  isNotHasParentCreditLimitType(model: CreditAppTableItemView): boolean {
    return isNullOrUndefOrEmptyGUID(model.creditLimitType_ParentCreditLimitTypeGUID);
  }

  isSystemDateLessThanDate(date: string): boolean {
    return !isDateFromGreaterThanDateTo(date, datetoString(new Date())); // value for disabled
  }
  canActiveUpdateExpectedSigningDate(model: CreditAppTableItemView): boolean {
    const canActive = isNullOrUndefined(model.inactiveDate)
      ? true
      : isDateFromGreaterThanDateTo(model.expiryDate, datetoString(new Date())) &&
        isDateFromGreaterThanDateTo(model.inactiveDate, datetoString(new Date()));
    return !canActive; // value for disabled
  }
  isCreditLimitExpirationEqualByBuyerAgreement(model: CreditAppTableItemView): boolean {
    return !(model.creditLimitExpiration === CreditLimitExpiration.ByBuyerAgreement); // value for disabled
  }

  //#endregion
}
