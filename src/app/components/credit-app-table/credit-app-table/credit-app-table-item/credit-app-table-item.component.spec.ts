import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditAppTableItemComponent } from './credit-app-table-item.component';

describe('CreditAppTableItemViewComponent', () => {
  let component: CreditAppTableItemComponent;
  let fixture: ComponentFixture<CreditAppTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditAppTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
