import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreditAppTableService } from './credit-app-table.service';
import { CreditAppTableListComponent } from './credit-app-table-list/credit-app-table-list.component';
import { CreditAppTableItemComponent } from './credit-app-table-item/credit-app-table-item.component';
import { CreditAppTableItemCustomComponent } from './credit-app-table-item/credit-app-table-item-custom.component';
import { CreditAppTableListCustomComponent } from './credit-app-table-list/credit-app-table-list-custom.component';
import { CreditAppLineComponentModule } from '../credit-app-line/credit-app-line.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, CreditAppLineComponentModule],
  declarations: [CreditAppTableListComponent, CreditAppTableItemComponent],
  providers: [CreditAppTableService, CreditAppTableItemCustomComponent, CreditAppTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CreditAppTableListComponent, CreditAppTableItemComponent]
})
export class CreditAppTableComponentModule {}
