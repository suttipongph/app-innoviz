import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CreditAppTableListView } from 'shared/models/viewModel';
import { CreditAppTableService } from '../credit-app-table.service';
import { CreditAppTableListCustomComponent } from './credit-app-table-list-custom.component';

@Component({
  selector: 'credit-app-table-list',
  templateUrl: './credit-app-table-list.component.html',
  styleUrls: ['./credit-app-table-list.component.scss']
})
export class CreditAppTableListComponent extends BaseListComponent<CreditAppTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  creditLimitExpirationOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  purchaseFeeCalculateBaseOptions: SelectItems[] = [];
  creditAppRequestTableOptions: SelectItems[] = [];
  creditLimitTypeOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];

  constructor(public custom: CreditAppTableListCustomComponent, private service: CreditAppTableService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getCreditLimitExpirationEnumDropDown(this.creditLimitExpirationOptions);
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getPurchaseFeeCalculateBaseEnumDropDown(this.purchaseFeeCalculateBaseOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCreditAppRequestTableDropDown(this.creditAppRequestTableOptions);
    this.baseDropdown.getCreditLimitTypeDropDown(this.creditLimitTypeOptions);
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'creditAppTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.CREDIT_APPLICATION_ID',
        textKey: 'creditAppId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId'
      },
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: 'LABEL.CREDIT_LIMIT_TYPE_ID',
        textKey: 'creditLimitType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditLimitTypeOptions,
        searchingKey: 'creditLimitTypeGUID',
        sortingKey: 'creditLimitType_CreditLimitTypeId'
      },
      {
        label: 'LABEL.APPROVED_DATE',
        textKey: 'approvedDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.REFERENCE_CREDIT_APPLICATION_REQUEST_ID',
        textKey: 'refCreditAppRequestTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditAppRequestTableOptions,
        searchingKey: 'refCreditAppRequestTableGUID',
        sortingKey: 'refCreditAppRequestTable_CreditAppRequestId'
      },
      {
        label: 'LABEL.APPROVED_CREDIT_LIMIT',
        textKey: 'approvedCreditLimit',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: 'LABEL.INACTIVE_DATE',
        textKey: 'inactiveDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.START_DATE',
        textKey: 'startDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.EXPIRY_DATE',
        textKey: 'expiryDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CreditAppTableListView>> {
    return this.service.getCreditAppTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCreditAppTable(row));
  }
}
