import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditAppTableListComponent } from './credit-app-table-list.component';

describe('CreditAppTableListViewComponent', () => {
  let component: CreditAppTableListComponent;
  let fixture: ComponentFixture<CreditAppTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditAppTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
