import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CreditAppTableItemView, CreditAppTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CreditAppTableService {
  serviceKey = 'creditAppTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCreditAppTableToList(search: SearchParameter): Observable<SearchResult<CreditAppTableListView>> {
    const url = `${this.servicePath}/GetCreditAppTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCreditAppTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCreditAppTable`;
    return this.dataGateway.delete(url, row);
  }
  getCreditAppTableById(id: string): Observable<CreditAppTableItemView> {
    const url = `${this.servicePath}/GetCreditAppTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCreditAppTable(vmModel: CreditAppTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCreditAppTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCreditAppTable(vmModel: CreditAppTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCreditAppTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
