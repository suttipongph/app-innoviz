import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { UpdateExpectedAgmSignDateView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class UpdateExpectedAgmSignDateService {
  serviceKey = 'updateExpectedAgmSignDateGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getUpdateExpectedAgmSignDateById(id: string): Observable<UpdateExpectedAgmSignDateView> {
    const url = `${this.servicePath}/GetUpdateExpectedSigningDateById/id=${id}`;
    return this.dataGateway.get(url);
  }
  updateExpectedAgmSignDate(vmModel: UpdateExpectedAgmSignDateView): Observable<ResponseModel> {
    const url = `${this.servicePath}`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
