import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateExpectedAgmSignDateService } from './update-expected-agm-sign-date.service';
import { UpdateExpectedAgmSignDateComponent } from './update-expected-agm-sign-date/update-expected-agm-sign-date.component';
import { UpdateExpectedAgmSignDateCustomComponent } from './update-expected-agm-sign-date/update-expected-agm-sign-date-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [UpdateExpectedAgmSignDateComponent],
  providers: [UpdateExpectedAgmSignDateService, UpdateExpectedAgmSignDateCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [UpdateExpectedAgmSignDateComponent]
})
export class UpdateExpectedAgmSignDateComponentModule {}
