import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { UpdateExpectedAgmSignDateView } from 'shared/models/viewModel';
import { UpdateExpectedAgmSignDateService } from '../update-expected-agm-sign-date.service';

const firstGroup = ['CREDIT_APP_ID', 'CUSTOMER_ID'];

export class UpdateExpectedAgmSignDateCustomComponent {
  baseService: BaseServiceModel<UpdateExpectedAgmSignDateService>;
  getFieldAccessing(): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<UpdateExpectedAgmSignDateView> {
    let model = new UpdateExpectedAgmSignDateView();
    return of(model);
  }
  getRemakReason(id: string, options: SelectItems[]): Observable<SelectItems> {
    const item = options.find((f) => f.value === id);
    return of(item);
  }
}
