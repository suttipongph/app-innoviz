import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateExpectedAgmSignDateComponent } from './update-expected-agm-sign-date.component';

describe('UpdateExpectedAgmSignDateViewComponent', () => {
  let component: UpdateExpectedAgmSignDateComponent;
  let fixture: ComponentFixture<UpdateExpectedAgmSignDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UpdateExpectedAgmSignDateComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateExpectedAgmSignDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
