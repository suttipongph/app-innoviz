import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, RefType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { UpdateExpectedAgmSignDateView } from 'shared/models/viewModel';
import { UpdateExpectedAgmSignDateService } from '../update-expected-agm-sign-date.service';
import { UpdateExpectedAgmSignDateCustomComponent } from './update-expected-agm-sign-date-custom.component';
@Component({
  selector: 'update-expected-agm-sign-date',
  templateUrl: './update-expected-agm-sign-date.component.html',
  styleUrls: ['./update-expected-agm-sign-date.component.scss']
})
export class UpdateExpectedAgmSignDateComponent extends BaseItemComponent<UpdateExpectedAgmSignDateView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  documentReasonOptions: SelectItems[] = [];

  constructor(
    private service: UpdateExpectedAgmSignDateService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: UpdateExpectedAgmSignDateCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialUpdatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<UpdateExpectedAgmSignDateView> {
    return this.service.getUpdateExpectedAgmSignDateById(this.id);
  }
  getInitialData(): Observable<UpdateExpectedAgmSignDateView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getDocumentReasonDropDown(RefType.CreditAppTable.toString())).subscribe(
      ([documentReason]) => {
        this.documentReasonOptions = documentReason;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing().subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {}
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true, this.model);
      }
    });
  }
  onSubmit(isColsing: boolean, model: UpdateExpectedAgmSignDateView): void {
    super.onExecuteFunction(this.service.updateExpectedAgmSignDate(model));
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
