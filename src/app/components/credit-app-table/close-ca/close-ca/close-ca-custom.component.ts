import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { FieldAccessing } from 'shared/models/systemModel';
import { CloseCAView } from 'shared/models/viewModel';

const firstGroup = ['CREDIT_APPLICATION_ID', 'CUSTOMER_TABLE_GUID'];

export class CloseCACustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CloseCAView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CloseCAView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<CloseCAView> {
    let model = new CloseCAView();
    model.closeCAGUID = EmptyGuid;
    return of(model);
  }
}
