import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CloseCAComponent } from './close-ca.component';

describe('CloseCAViewComponent', () => {
  let component: CloseCAComponent;
  let fixture: ComponentFixture<CloseCAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CloseCAComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloseCAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
