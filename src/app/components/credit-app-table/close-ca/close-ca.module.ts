import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CloseCAService } from './close-ca.service';
import { CloseCAListComponent } from './close-ca-list/close-ca-list.component';
import { CloseCAItemComponent } from './close-ca-item/close-ca-item.component';
import { CloseCAItemCustomComponent } from './close-ca-item/close-ca-item-custom.component';
import { CloseCAListCustomComponent } from './close-ca-list/close-ca-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CloseCAListComponent, CloseCAItemComponent],
  providers: [CloseCAService, CloseCAItemCustomComponent, CloseCAListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CloseCAListComponent, CloseCAItemComponent]
})
export class CloseCAComponentModule {}
