import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CloseCAItemView, CloseCAListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CloseCAService {
  serviceKey = 'closeCAGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCloseCAToList(search: SearchParameter): Observable<SearchResult<CloseCAListView>> {
    const url = `${this.servicePath}/GetCloseCAList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCloseCA(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCloseCA`;
    return this.dataGateway.delete(url, row);
  }
  getCloseCAById(id: string): Observable<CloseCAItemView> {
    const url = `${this.servicePath}/GetCloseCAById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCloseCA(vmModel: CloseCAItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCloseCA`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCloseCA(vmModel: CloseCAItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCloseCA`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
