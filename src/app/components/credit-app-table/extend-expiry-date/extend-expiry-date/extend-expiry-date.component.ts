import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, RefType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { ExtendExpiryDateResultView, ExtendExpiryDateParamView } from 'shared/models/viewModel';
import { ExtendExpiryDateService } from '../extend-expiry-date.service';
import { ExtendExpiryDateCustomComponent } from './extend-expiry-date-custom.component';
@Component({
  selector: 'extend-expiry-date',
  templateUrl: './extend-expiry-date.component.html',
  styleUrls: ['./extend-expiry-date.component.scss']
})
export class ExtendExpiryDateComponent extends BaseItemComponent<ExtendExpiryDateParamView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  documentReasonOptions: SelectItems[] = [];

  constructor(
    private service: ExtendExpiryDateService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: ExtendExpiryDateCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialUpdatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<ExtendExpiryDateParamView> {
    return this.service.getExtendExpiryDateById(this.id);
  }
  getInitialData(): Observable<ExtendExpiryDateParamView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {}

  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getDocumentReasonDropDown(RefType.CreditAppTable.toString())).subscribe(
      ([documentReason]) => {
        this.documentReasonOptions = documentReason;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing().subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {}
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.custom.getModelParam(this.model).subscribe((model) => {
          this.onSubmit(true, model);
        });
      }
    });
  }
  onSubmit(isColsing: boolean, model: ExtendExpiryDateResultView): void {
    super.onExecuteFunction(this.service.extendExpiryDate(model));
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
