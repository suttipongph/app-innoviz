import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { ExtendExpiryDateParamView, ExtendExpiryDateResultView } from 'shared/models/viewModel';
import { ExtendExpiryDateService } from '../extend-expiry-date.service';

const firstGroup = ['CREDIT_APP_ID', 'CUSTOMER_TABLE_GUID', 'ORIGINAL_EXPIRY_DATE', 'EXPIRY_DATE'];

export class ExtendExpiryDateCustomComponent {
  baseService: BaseServiceModel<ExtendExpiryDateService>;
  getFieldAccessing(): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getModelParam(model: ExtendExpiryDateParamView): Observable<ExtendExpiryDateResultView> {
    const param: ExtendExpiryDateResultView = new ExtendExpiryDateResultView();
    param.creditAppTableGUID = model.creditAppTableGUID;
    param.expiryDate = model.expiryDate;
    param.originalExpiryDate = model.originalExpiryDate;
    param.creditLimitExpiration = model.creditLimitExpiration;

    return of(param);
  }
  getRemakReason(id: string, options: SelectItems[]): Observable<SelectItems> {
    const item = options.find((f) => f.value === id);
    return of(item);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<ExtendExpiryDateParamView> {
    let model = new ExtendExpiryDateParamView();
    return of(model);
  }
}
