import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExtendExpiryDateService } from './extend-expiry-date.service';
import { ExtendExpiryDateComponent } from './extend-expiry-date/extend-expiry-date.component';
import { ExtendExpiryDateCustomComponent } from './extend-expiry-date/extend-expiry-date-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ExtendExpiryDateComponent],
  providers: [ExtendExpiryDateService, ExtendExpiryDateCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ExtendExpiryDateComponent]
})
export class ExtendExpiryDateComponentModule {}
