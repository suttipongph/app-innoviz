import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ExtendExpiryDateParamView, ExtendExpiryDateResultView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ExtendExpiryDateService {
  serviceKey = 'extendExpiryDateGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getExtendExpiryDateById(id: string): Observable<ExtendExpiryDateParamView> {
    const url = `${this.servicePath}/GetExtendExpiryDateById/id=${id}`;
    return this.dataGateway.get(url);
  }
  extendExpiryDate(vmModel: ExtendExpiryDateResultView): Observable<ResponseModel> {
    const url = `${this.servicePath}`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
