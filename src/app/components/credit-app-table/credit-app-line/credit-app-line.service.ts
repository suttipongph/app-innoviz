import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CreditAppLineItemView, CreditAppLineListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CreditAppLineService {
  serviceKey = 'creditAppLineGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCreditAppLineToList(search: SearchParameter): Observable<SearchResult<CreditAppLineListView>> {
    const url = `${this.servicePath}/GetCreditAppLineList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCreditAppLine(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCreditAppLine`;
    return this.dataGateway.delete(url, row);
  }
  getCreditAppLineById(id: string): Observable<CreditAppLineItemView> {
    const url = `${this.servicePath}/GetCreditAppLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCreditAppLine(vmModel: CreditAppLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCreditAppLine`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCreditAppLine(vmModel: CreditAppLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCreditAppLine`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
