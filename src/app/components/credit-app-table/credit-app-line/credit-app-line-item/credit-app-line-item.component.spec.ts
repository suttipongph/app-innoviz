import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditAppLineItemComponent } from './credit-app-line-item.component';

describe('CreditAppLineItemViewComponent', () => {
  let component: CreditAppLineItemComponent;
  let fixture: ComponentFixture<CreditAppLineItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditAppLineItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppLineItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
