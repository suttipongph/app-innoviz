import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CreditAppLineItemView } from 'shared/models/viewModel';

const firstGroup = [];

export class CreditAppLineItemCustomComponent {
  baseService: BaseServiceModel<any>;
  title: string = 'LABEL.CREDIT_APPLICATION_LINE';
  titleOfReviewCaAllLine: string = 'LABEL.REVIEW_ACTIVE_CREDIT_APPLICATION_LINE';
  isReviewActiveCreditAppLine: boolean = false;
  getFieldAccessing(model: CreditAppLineItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    this.setTitle();
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CreditAppLineItemView): Observable<boolean> {
    const accessright = isNullOrUndefOrEmptyGUID(model.creditAppLineGUID) ? canCreate : canUpdate;
    const accessLogic = false;
    return of(!(accessLogic && accessright));
  }
  getInitialData(): Observable<CreditAppLineItemView> {
    let model = new CreditAppLineItemView();
    model.creditAppLineGUID = EmptyGuid;
    return of(model);
  }
  setTitle(): void {
    this.isReviewActiveCreditAppLine = window.location.href.match('reviewactivecreditappline') ? true : false;
    if (this.isReviewActiveCreditAppLine) {
      this.title = this.titleOfReviewCaAllLine;
    }
    this.title = this.baseService.translate.instant(this.title);
  }
}
