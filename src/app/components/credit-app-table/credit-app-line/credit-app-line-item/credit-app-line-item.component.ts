import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { RefType, ROUTE_RELATED_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { CreditAppLineItemView, RefIdParm } from 'shared/models/viewModel';
import { CreditAppLineService } from '../credit-app-line.service';
import { CreditAppLineItemCustomComponent } from './credit-app-line-item-custom.component';
@Component({
  selector: 'credit-app-line-item',
  templateUrl: './credit-app-line-item.component.html',
  styleUrls: ['./credit-app-line-item.component.scss']
})
export class CreditAppLineItemComponent extends BaseItemComponent<CreditAppLineItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  billingDayOptions: SelectItems[] = [];
  methodOfBillingOptions: SelectItems[] = [];
  purchaseFeeCalculateBaseOptions: SelectItems[] = [];
  receiptDayOptions: SelectItems[] = [];
  dayOfMonthOptions: SelectItems[] = [];
  constructor(private service: CreditAppLineService, private currentActivatedRoute: ActivatedRoute, public custom: CreditAppLineItemCustomComponent) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getDayOfMonthEnumDropDown(this.dayOfMonthOptions);
    this.baseDropdown.getMethodOfBillingEnumDropDown(this.methodOfBillingOptions);
    this.baseDropdown.getPurchaseFeeCalculateBaseEnumDropDown(this.purchaseFeeCalculateBaseOptions);
  }
  getById(): Observable<CreditAppLineItemView> {
    return this.service.getCreditAppLineById(this.id);
  }
  getInitialData(): Observable<CreditAppLineItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {
    if (this.custom.isReviewActiveCreditAppLine) {
      this.relatedInfoItems = [
        {
          label: 'LABEL.REVIEW_CREDIT_APP_REQUEST',
          command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.REVIEW_CREDIT_APP_REQUEST })
        }
      ];
      super.setRelatedinfoOptionsMapping();
      return;
    }
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        command: () => {
          const refIdParm: RefIdParm = { refType: RefType.CreditAppLine, refGUID: this.model.creditAppLineGUID };
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: refIdParm
          });
        }
      },
      ,
      {
        label: 'LABEL.MANAGE',
        items: [
          {
            label: 'LABEL.SERVICE_FEE_CONDITION',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.SERVICE_FEE_CONDITION_TRANS })
          },
          {
            label: 'LABEL.DOCUMENT_CONDITION',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.DOCUMENT_CONDITION_INFO })
          }
        ]
      },
      {
        label: 'LABEL.BUYER_AGREEMENT_TRANSACTION',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.BUYER_AGREEMENT_TRANS })
      },
      {
        label: 'LABEL.BUYER_INVOICE',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.BUYER_INVOICE })
      },
      {
        label: 'LABEL.AMEND_CA_LINE',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.AMEND_CA_LINE
          })
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateCreditAppLine(this.model), isColsing);
    } else {
      super.onCreate(this.service.createCreditAppLine(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
