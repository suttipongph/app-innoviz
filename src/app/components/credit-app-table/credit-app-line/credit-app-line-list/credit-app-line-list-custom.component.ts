import { Observable, of } from 'rxjs';
import { BaseServiceModel, SelectItems } from 'shared/models/systemModel';
import { AccessLevelModel } from 'shared/models/viewModel';
export class CreditAppLineListCustomComponent {
  baseService: BaseServiceModel<any>;
  isReviewActiveCreditAppLine: boolean = false;
  title = 'LABEL.CREDIT_APPLICATION_LINE';
  titleOfReviewCaAllLine = 'LABEL.REVIEW_ACTIVE_CREDIT_APPLICATION_LINE';
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  getAccessRight(accessRightChildPage: string): Observable<AccessLevelModel> {
    this.isReviewActiveCreditAppLine = window.location.href.match('reviewactivecreditappline') ? true : false;
    this.title = this.isReviewActiveCreditAppLine
      ? this.baseService.translate.instant(this.titleOfReviewCaAllLine)
      : this.baseService.translate.instant(this.title);
    if (this.isReviewActiveCreditAppLine) {
      return this.baseService.accessService.getAccessRight();
    }
    return this.baseService.accessService.getNestedComponentAccessRight(true, accessRightChildPage);
  }
  getBusinessSegmentDropDown(businessSegmentOption: SelectItems[]): Observable<SelectItems[]> {
    if (!this.isReviewActiveCreditAppLine) {
      return this.baseService.baseDropdown.getBusinessSegmentDropDown(businessSegmentOption);
    }
    return of([]);
  }
}
