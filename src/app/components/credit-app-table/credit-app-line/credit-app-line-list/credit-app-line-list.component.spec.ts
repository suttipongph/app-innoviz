import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditAppLineListComponent } from './credit-app-line-list.component';

describe('CreditAppLineListViewComponent', () => {
  let component: CreditAppLineListComponent;
  let fixture: ComponentFixture<CreditAppLineListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditAppLineListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppLineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
