import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CreditAppLineListView } from 'shared/models/viewModel';
import { CreditAppLineService } from '../credit-app-line.service';
import { CreditAppLineListCustomComponent } from './credit-app-line-list-custom.component';

@Component({
  selector: 'credit-app-line-list',
  templateUrl: './credit-app-line-list.component.html',
  styleUrls: ['./credit-app-line-list.component.scss']
})
export class CreditAppLineListComponent extends BaseListComponent<CreditAppLineListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  billingDayOptions: SelectItems[] = [];
  methodOfBillingOptions: SelectItems[] = [];
  purchaseFeeCalculateBaseOptions: SelectItems[] = [];
  receiptDayOptions: SelectItems[] = [];
  dayOfMonthOptions: SelectItems[] = [];
  buyerTableOption: SelectItems[] = [];
  businessSegmentOption: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  creditAppTableOptions: SelectItems[] = [];

  constructor(public custom: CreditAppLineListCustomComponent, private service: CreditAppLineService) {
    super();
    this.custom.baseService = this.baseService;
    this.accessRightChildPage = 'CreditAppLineListPage';
    this.parentId = this.uiService.getKey('creditapptable');
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getDayOfMonthEnumDropDown(this.dayOfMonthOptions);
    this.baseDropdown.getMethodOfBillingEnumDropDown(this.methodOfBillingOptions);
    this.baseDropdown.getPurchaseFeeCalculateBaseEnumDropDown(this.purchaseFeeCalculateBaseOptions);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.custom.getAccessRight(this.accessRightChildPage));
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOption);
    this.custom.getBusinessSegmentDropDown(this.businessSegmentOption);
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    if (this.custom.isReviewActiveCreditAppLine) {
      this.baseDropdown.getCreditAppTableDropDown(this.creditAppTableOptions);
    }
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'creditAppLineGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.CREDIT_APPLICATION_ID',
        textKey: 'creditAppTable_Values',
        type: ColumnType.MASTER,
        visibility: false,
        sorting: SortType.NONE,
        sortingKey: 'creditAppTable_creditAppId',
        searchingKey: 'creditAppTableGUID',
        masterList: this.creditAppTableOptions
      },
      {
        label: 'LABEL.LINE',
        textKey: 'lineNum',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.REVIEWED_DATE',
        textKey: 'reviewDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.HIDDEN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId'
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerTableOption,
        searchingKey: 'buyerTableGUID',
        sortingKey: 'buyerTable_BuyerId'
      },
      {
        label: 'LABEL.BUSINESS_SEGMENT_ID',
        textKey: 'businessSegment_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.businessSegmentOption,
        searchingKey: 'businessSegmentGUID',
        sortingKey: 'businessSegment_BusinessSegmentId'
      },
      {
        label: 'LABEL.APRROVED_CREDIT_LIMIT_LINE',
        textKey: 'approvedCreditLimitLine',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: null,
        textKey: 'creditapptableGUID',
        type: ColumnType.MASTER,
        visibility: false,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CreditAppLineListView>> {
    return this.service.getCreditAppLineToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCreditAppLine(row));
  }
}
