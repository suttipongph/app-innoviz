import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreditAppLineService } from './credit-app-line.service';
import { CreditAppLineListComponent } from './credit-app-line-list/credit-app-line-list.component';
import { CreditAppLineItemComponent } from './credit-app-line-item/credit-app-line-item.component';
import { CreditAppLineItemCustomComponent } from './credit-app-line-item/credit-app-line-item-custom.component';
import { CreditAppLineListCustomComponent } from './credit-app-line-list/credit-app-line-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CreditAppLineListComponent, CreditAppLineItemComponent],
  providers: [CreditAppLineService, CreditAppLineItemCustomComponent, CreditAppLineListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CreditAppLineListComponent, CreditAppLineItemComponent]
})
export class CreditAppLineComponentModule {}
