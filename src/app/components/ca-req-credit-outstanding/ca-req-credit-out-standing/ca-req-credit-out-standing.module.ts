import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CAReqCreditOutStandingService } from './ca-req-credit-out-standing.service';
import { CAReqCreditOutStandingListComponent } from './ca-req-credit-out-standing-list/ca-req-credit-out-standing-list.component';
import { CAReqCreditOutStandingListCustomComponent } from './ca-req-credit-out-standing-list/ca-req-credit-out-standing-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CAReqCreditOutStandingListComponent],
  providers: [CAReqCreditOutStandingService, CAReqCreditOutStandingListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CAReqCreditOutStandingListComponent]
})
export class CAReqCreditOutStandingComponentModule {}
