import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CAReqCreditOutStandingListView } from 'shared/models/viewModel';
import { CAReqCreditOutStandingService } from '../ca-req-credit-out-standing.service';
import { CAReqCreditOutStandingListCustomComponent } from './ca-req-credit-out-standing-list-custom.component';

@Component({
  selector: 'ca-req-credit-out-standing-list',
  templateUrl: './ca-req-credit-out-standing-list.component.html',
  styleUrls: ['./ca-req-credit-out-standing-list.component.scss']
})
export class CAReqCreditOutStandingListComponent extends BaseListComponent<CAReqCreditOutStandingListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  productTypeOptions: SelectItems[] = [];
  creditLimitTypeOptions: SelectItems[] = [];
  creditAppTableOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  constructor(
    public custom: CAReqCreditOutStandingListCustomComponent,
    private service: CAReqCreditOutStandingService,
    public currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.parentId = this.custom.getRelatedInfoParentTableKey(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getCreditAppTableDropDown(this.creditAppTableOptions);
    this.baseDropdown.getCreditLimitTypeDropDown(this.creditLimitTypeOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = false;
    this.option.canDelete = false;
    this.option.key = 'caReqCreditOutStandingGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: 'LABEL.CREDIT_LIMIT_TYPE_ID',
        textKey: 'creditLimitType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'creditLimitTypeGUID',
        sortingKey: 'creditLimitType_CreditLimitTypeId',
        masterList: this.creditLimitTypeOptions
      },
      {
        label: 'LABEL.CREDIT_APPLICATION_ID',
        textKey: 'creditAppTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.ASC,
        searchingKey: 'creditAppTableGUID',
        sortingKey: 'creditAppTable_CreditAppId',
        masterList: this.creditAppTableOptions
      },
      {
        label: 'LABEL.CUSTOMER',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId',
        masterList: this.customerTableOptions
      },
      {
        label: 'LABEL.AS_OF_DATE',
        textKey: 'asOfDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.APPROVED_CREDIT_LIMIT',
        textKey: 'approvedCreditLimit',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: 'LABEL.CREDIT_LIMIT_BALANCE',
        textKey: 'creditLimitBalance',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: 'LABEL.AR_BALANCE',
        textKey: 'arBalance',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: 'LABEL.ACCUMULATE_RETENTION_AMOUNT',
        textKey: 'accumRetentionAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: 'LABEL.RESERVE_TO_BE_REFUND',
        textKey: 'reserveToBeRefund',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: null,
        textKey: 'creditAppRequestTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CAReqCreditOutStandingListView>> {
    return this.service.getCAReqCreditOutStandingToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCAReqCreditOutStanding(row));
  }
}
