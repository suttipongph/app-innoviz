import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CAReqCreditOutStandingListComponent } from './ca-req-credit-out-standing-list.component';

describe('CAReqCreditOutStandingListViewComponent', () => {
  let component: CAReqCreditOutStandingListComponent;
  let fixture: ComponentFixture<CAReqCreditOutStandingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CAReqCreditOutStandingListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CAReqCreditOutStandingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
