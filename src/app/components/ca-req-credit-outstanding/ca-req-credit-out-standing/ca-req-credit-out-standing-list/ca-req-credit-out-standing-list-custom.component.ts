import { Observable } from 'rxjs';
import { BaseServiceModel } from 'shared/models/systemModel';
import { CAReqCreditOutStandingListView } from 'shared/models/viewModel';
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const AMEND_CA = 'amendca';
const AMEND_CA_LINE = 'amendcaline';
const BUYER_MATCHING = 'buyermatching';
const LOAN_REQUEST = 'loanrequest';
const REVIEW_CREDIT_APP_REQUEST = 'reviewcreditapprequest';
const CLOSE_CREDIT_LIMIT = 'closecreditlimit';
export class CAReqCreditOutStandingListCustomComponent {
  baseService: BaseServiceModel<any>;
  parentName: string = null;
  parentId: string = null;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  getRelatedInfoParentTableKey(passingObj: CAReqCreditOutStandingListView): string {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        this.parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
        break;
      case AMEND_CA:
      case AMEND_CA_LINE:
      case REVIEW_CREDIT_APP_REQUEST:
      case CLOSE_CREDIT_LIMIT:
        this.parentId = passingObj.creditAppRequestTableGUID;
        break;
      case BUYER_MATCHING:
        this.parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
        break;
      case LOAN_REQUEST:
        this.parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
        break;
      default:
        this.parentId = null;
        break;
    }

    return this.parentId;
  }
}
