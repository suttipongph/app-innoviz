import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CAReqCreditOutStandingListView } from 'shared/models/viewModel';
import { PageInformationModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CAReqCreditOutStandingService {
  serviceKey = 'caReqCreditOutStandingGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCAReqCreditOutStandingToList(search: SearchParameter): Observable<SearchResult<CAReqCreditOutStandingListView>> {
    const url = `${this.servicePath}/GetCAReqCreditOutStandingList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCAReqCreditOutStanding(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCAReqCreditOutStanding`;
    return this.dataGateway.delete(url, row);
  }
}
