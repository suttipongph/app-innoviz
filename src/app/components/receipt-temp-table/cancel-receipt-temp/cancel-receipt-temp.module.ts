import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CancelReceiptTempService } from './cancel-receipt-temp.service';
import { CancelReceiptTempComponent } from './cancel-receipt-temp/cancel-receipt-temp.component';
import { CancelReceiptTempCustomComponent } from './cancel-receipt-temp/cancel-receipt-temp-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CancelReceiptTempComponent],
  providers: [CancelReceiptTempService, CancelReceiptTempCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CancelReceiptTempComponent]
})
export class CancelReceiptTempComponentModule {}
