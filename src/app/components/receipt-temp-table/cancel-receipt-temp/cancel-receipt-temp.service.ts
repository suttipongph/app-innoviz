import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { CancelReceiptTempResultView, CancelReceiptTempView } from 'shared/models/viewModel/cancelReceiptTempView';
@Injectable()
export class CancelReceiptTempService {
  serviceKey = 'cancelReceiptTempGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCancelReceiptTempById(id: string): Observable<CancelReceiptTempView> {
    const url = `${this.servicePath}/GetCancelReceiptTempById/id=${id}`;
    return this.dataGateway.get(url);
  }
  cancelReceiptTemp(vmModel: CancelReceiptTempView): Observable<CancelReceiptTempResultView> {
    const url = `${this.servicePath}/CancelReceiptTemp`;
    return this.dataGateway.post(url, vmModel);
  }
}
