import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelReceiptTempComponent } from './cancel-receipt-temp.component';

describe('CancelReceiptTempViewComponent', () => {
  let component: CancelReceiptTempComponent;
  let fixture: ComponentFixture<CancelReceiptTempComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CancelReceiptTempComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelReceiptTempComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
