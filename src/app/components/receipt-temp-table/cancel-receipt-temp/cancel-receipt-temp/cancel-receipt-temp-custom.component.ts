import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CancelReceiptTempView } from 'shared/models/viewModel/cancelReceiptTempView';

const firstGroup = [
  'RECEIPT_TEMP_ID',
  'RECEIPT_DATE',
  'TRANS_DATE',
  'PRODUCT_TYPE',
  'RECEIVED_FROM',
  'CUSTOMER_ID',
  'BUYER_ID',
  'RECEIPT_AMOUNT',
  'SETTLE_FEE_AMOUNT',
  'SETTLE_AMOUNT',
  'SUSPENSE_INVOICE_TYPE_GUID',
  'SUSPENSE_AMOUNT',
  'CREDIT_APP_TABLE_GUID'
];

export class CancelReceiptTempCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CancelReceiptTempView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CancelReceiptTempView): Observable<boolean> {
    return of(false);
  }
  getInitialData(): Observable<CancelReceiptTempView> {
    let model = new CancelReceiptTempView();
    model.receiptTempTableGUID = EmptyGuid;
    return of(model);
  }
}
