import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PostReceiptTempService } from './post-receipt-temp.service';
import { PostReceiptTempComponent } from './post-receipt-temp/post-receipt-temp.component';
import { PostReceiptTempCustomComponent } from './post-receipt-temp/post-receipt-temp-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PostReceiptTempComponent],
  providers: [PostReceiptTempService, PostReceiptTempCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PostReceiptTempComponent]
})
export class PostReceiptTempComponentModule {}
