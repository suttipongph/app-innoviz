import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostReceiptTempComponent } from './post-receipt-temp.component';

describe('PostReceiptTempViewComponent', () => {
  let component: PostReceiptTempComponent;
  let fixture: ComponentFixture<PostReceiptTempComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PostReceiptTempComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostReceiptTempComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
