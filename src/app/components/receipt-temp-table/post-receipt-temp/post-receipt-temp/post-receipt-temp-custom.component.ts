import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { PostReceiptTempParamView } from 'shared/models/viewModel/postReceiptTempParamView';

const firstGroup = [
  'RECEIPT_TEMP_ID',
  'RECEIPT_DATE',
  'TRANS_DATE',
  'PRODUCT_TYPE',
  'RECEIVED_FROM',
  'CUSTOMER_ID',
  'BUYER_ID',
  'RECEIPT_AMOUNT',
  'SETTLE_FEE_AMOUNT',
  'SETTLE_AMOUNT',
  'SUSPENSE_INVOICE_TYPE_GUID',
  'SUSPENSE_AMOUNT',
  'CREDIT_APP_TABLE_GUID'
];

export class PostReceiptTempCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: PostReceiptTempParamView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: PostReceiptTempParamView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<PostReceiptTempParamView> {
    let model = new PostReceiptTempParamView();
    model.receiptTempTableGUID = EmptyGuid;
    return of(model);
  }
}
