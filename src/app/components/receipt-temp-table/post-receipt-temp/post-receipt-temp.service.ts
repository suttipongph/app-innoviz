import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { PostReceiptTempParamView } from 'shared/models/viewModel/postReceiptTempParamView';
@Injectable()
export class PostReceiptTempService {
  serviceKey = 'postReceiptTempGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getPostReceiptTempById(id: string): Observable<PostReceiptTempParamView> {
    const url = `${this.servicePath}/GetPostReceiptTempById/id=${id}`;
    return this.dataGateway.get(url);
  }
  postReceiptTemp(vmModel: PostReceiptTempParamView): Observable<PostReceiptTempParamView> {
    const url = `${this.servicePath}/PostReceiptTemp`;
    return this.dataGateway.post(url, vmModel);
  }
}
