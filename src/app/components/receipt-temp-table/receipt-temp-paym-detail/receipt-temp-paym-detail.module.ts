import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReceiptTempPaymDetailService } from './receipt-temp-paym-detail.service';
import { ReceiptTempPaymDetailListComponent } from './receipt-temp-paym-detail-list/receipt-temp-paym-detail-list.component';
import { ReceiptTempPaymDetailItemComponent } from './receipt-temp-paym-detail-item/receipt-temp-paym-detail-item.component';
import { ReceiptTempPaymDetailItemCustomComponent } from './receipt-temp-paym-detail-item/receipt-temp-paym-detail-item-custom.component';
import { ReceiptTempPaymDetailListCustomComponent } from './receipt-temp-paym-detail-list/receipt-temp-paym-detail-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ReceiptTempPaymDetailListComponent, ReceiptTempPaymDetailItemComponent],
  providers: [ReceiptTempPaymDetailService, ReceiptTempPaymDetailItemCustomComponent, ReceiptTempPaymDetailListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ReceiptTempPaymDetailListComponent, ReceiptTempPaymDetailItemComponent]
})
export class ReceiptTempPaymDetailComponentModule {}
