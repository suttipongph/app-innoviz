import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiptTempPaymDetailItemComponent } from './receipt-temp-paym-detail-item.component';

describe('ReceiptTempPaymDetailItemViewComponent', () => {
  let component: ReceiptTempPaymDetailItemComponent;
  let fixture: ComponentFixture<ReceiptTempPaymDetailItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReceiptTempPaymDetailItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptTempPaymDetailItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
