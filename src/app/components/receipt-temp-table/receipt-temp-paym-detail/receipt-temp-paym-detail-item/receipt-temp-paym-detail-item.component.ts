import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { PaymentType, ReceivedFrom, RefType, ROUTE_RELATED_GEN, SuspenseInvoiceType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { ReceiptTempPaymDetailItemView } from 'shared/models/viewModel';
import { ReceiptTempPaymDetailService } from '../receipt-temp-paym-detail.service';
import { ReceiptTempPaymDetailItemCustomComponent } from './receipt-temp-paym-detail-item-custom.component';
@Component({
  selector: 'receipt-temp-paym-detail-item',
  templateUrl: './receipt-temp-paym-detail-item.component.html',
  styleUrls: ['./receipt-temp-paym-detail-item.component.scss']
})
export class ReceiptTempPaymDetailItemComponent extends BaseItemComponent<ReceiptTempPaymDetailItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  bankGroupOptions: SelectItems[] = [];
  chequeTableOptions: SelectItems[] = [];
  methodOfPaymentOptions: SelectItems[] = [];
  receiptTempTableOptions: SelectItems[] = [];

  constructor(
    private service: ReceiptTempPaymDetailService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: ReceiptTempPaymDetailItemCustomComponent
  ) {
    super();
    this.baseService.service = this.service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiService.getKey('receipttemptable');
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<ReceiptTempPaymDetailItemView> {
    return this.service.getReceiptTempPaymDetailById(this.id);
  }
  getInitialData(): Observable<ReceiptTempPaymDetailItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions(result);
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    let tempModel: ReceiptTempPaymDetailItemView = this.isUpdateMode ? model : this.model;
    forkJoin([
      this.baseDropdown.getBankGroupDropDown(),
      this.custom.getChequeTableByReceiptTempPaymDetailDropDown(tempModel),
      this.baseDropdown.getMethodOfPaymentDropDown(),
      this.baseDropdown.getReceiptTempTableDropDown()
    ]).subscribe(
      ([bankGroup, chequeTable, methodOfPayment, receiptTempTable]) => {
        this.bankGroupOptions = bankGroup;
        this.chequeTableOptions = chequeTable;
        this.methodOfPaymentOptions = methodOfPayment;
        this.receiptTempTableOptions = receiptTempTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(model: ReceiptTempPaymDetailItemView): void {
    const isReceivedFromBuyer = model.receiptTempTable_ReceivedFrom === ReceivedFrom.Buyer ? true : false;
    this.relatedInfoItems = [
      {
        label: 'LABEL.SUSPENSE_SETTLEMENT',
        disabled: model.methodOfPayment_PaymentType !== PaymentType.SuspenseAccount,
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.SUSPENSE_SETTLEMENT,
            parameters: {
              refGUID: model.receiptTempTableGUID,
              refType: RefType.ReceiptTemp,
              refId: model.receiptTempTable_ReceiptTempId,
              invoiceTable_CustomerTableGUID: model.receiptTempTable_CustomerTableGUID,
              suspenseInvoiceType: SuspenseInvoiceType.SuspenseAccount,
              productInvoice: isReceivedFromBuyer,
              productType: model.receiptTempTable_ProductType,
              buyerTableGUID: isReceivedFromBuyer ? model.receiptTempTable_BuyerTableGUID : null,
              receivedFrom: model.receiptTempTable_ReceivedFrom,
              refReceiptTempPaymDetailGUID: model.receiptTempPaymDetailGUID
            }
          })
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateReceiptTempPaymDetail(this.model), isColsing);
    } else {
      super.onCreate(this.service.createReceiptTempPaymDetail(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onChequeTableChange() {
    this.custom.setModelByonChequeTable(this.model, this.chequeTableOptions);
  }
  onChequeBankGroupChange() {
    this.model.chequeBranch = '';
  }
  onMethodOfPaymentChange() {
    super.setBaseFieldAccessing(this.custom.setFieldAccessingByMethodOfPayment(this.model, this.methodOfPaymentOptions));
  }
}
