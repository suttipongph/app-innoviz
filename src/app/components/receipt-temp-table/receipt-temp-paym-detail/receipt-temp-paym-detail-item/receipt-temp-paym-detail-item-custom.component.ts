import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { PaymentType } from 'shared/constants';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { ChequeTableItemView, MethodOfPaymentItemView, ReceiptTempPaymDetailItemView } from 'shared/models/viewModel';
import { ReceiptTempPaymDetailService } from '../receipt-temp-paym-detail.service';

const firstGroup = ['RECEIPT_TEMP_TABLE_GUID', 'COMPANY_BANK', 'RECEIPT_TEMP_PAYM_DETAIL_GUID'];
const secoundGroup = ['CHEQUE_TABLE_GUID', 'CHEQUE_BANK_GROUP_GUID', 'CHEQUE_BRANCH', 'TRANSFER_REFERENCE'];
const thirdGroup = ['METHOD_OF_PAYMENT_GUID'];
const forthGroup = ['RECEIPT_AMOUNT'];
const PURCHASE_TABLE_PURCHASE = 'purchasetablepurchase';
const PURCHASE_TABLE_ROLL_BILL = 'purchasetablerollbill';
const WITHDRAWAL_TABLE_WITHDRAWAL = 'withdrawaltablewithdrawal';
const WITHDRAWAL_TABLE_TERM_EXTENSION = 'withdrawaltabletermextension';
export class ReceiptTempPaymDetailItemCustomComponent {
  baseService: BaseServiceModel<any>;
  parentId: string = null;
  parentName: string = null;
  getFieldAccessing(model: ReceiptTempPaymDetailItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    if (isUpdateMode(model.receiptTempPaymDetailGUID)) {
      fieldAccessing.push({ filedIds: thirdGroup, readonly: true });
      if (model.methodOfPayment_PaymentType == PaymentType.SuspenseAccount) {
        fieldAccessing.push({ filedIds: secoundGroup, readonly: true });
        fieldAccessing.push({ filedIds: forthGroup, readonly: true });
      } else {
        fieldAccessing.push({ filedIds: secoundGroup, readonly: false });
        fieldAccessing.push({ filedIds: forthGroup, readonly: false });
      }
    } else {
      fieldAccessing.push({ filedIds: thirdGroup, readonly: false });
      fieldAccessing.push({ filedIds: secoundGroup, readonly: false });
      fieldAccessing.push({ filedIds: forthGroup, readonly: true });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ReceiptTempPaymDetailItemView): Observable<boolean> {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    const accessright = isNullOrUndefOrEmptyGUID(model.receiptTempPaymDetailGUID) ? canCreate : canUpdate;
    let service: ReceiptTempPaymDetailService = this.baseService.service;
    const inquiryPath = [PURCHASE_TABLE_PURCHASE, PURCHASE_TABLE_ROLL_BILL, WITHDRAWAL_TABLE_WITHDRAWAL, WITHDRAWAL_TABLE_TERM_EXTENSION];
    if (inquiryPath.some((s) => s === this.parentName)) {
      return of(true);
    } else {
      return service.getReceiptTempTableAccessMode(model.receiptTempTableGUID).pipe(
        map((result) => {
          let accessLogic = isNullOrUndefOrEmptyGUID(model.receiptTempPaymDetailGUID) ? result.canCreate : result.canView;
          return !(accessLogic && accessright);
        })
      );
    }
  }
  getChequeTableByReceiptTempPaymDetailDropDown(model: ReceiptTempPaymDetailItemView) {
    if (
      !isNullOrUndefOrEmptyGUID(model.receiptTempTable_CustomerTableGUID) &&
      !isNullOrUndefOrEmptyGUID(model.receiptTempTable_ReceivedFrom) &&
      !isNullOrUndefined(model.receiptTempTable_ReceivedFrom)
    ) {
      return this.baseService.baseDropdown.getChequeTableByReceiptTempPaymDetailDropDown([
        model.receiptTempTable_CustomerTableGUID,
        model.receiptTempTable_BuyerTableGUID,
        model.receiptTempTable_ReceivedFrom.toString()
      ]);
    } else {
      return of([]);
    }
  }
  setModelByonChequeTable(model: ReceiptTempPaymDetailItemView, chequeTableOptions: SelectItems[]) {
    if (!isNullOrUndefOrEmptyGUID(model.chequeTableGUID)) {
      let chequeTable: ChequeTableItemView = chequeTableOptions.find((t) => t.value == model.chequeTableGUID).rowData as ChequeTableItemView;
      model.chequeBankGroupGUID = chequeTable.chequeBankGroupGUID;
      model.chequeBranch = chequeTable.chequeBranch;
      model.receiptAmount = chequeTable.amount;
    } else {
      model.chequeBankGroupGUID = null;
      model.chequeBranch = '';
      model.receiptAmount = 0;
    }
  }

  setFieldAccessingByMethodOfPayment(model: ReceiptTempPaymDetailItemView, methodOfPaymentOptions: SelectItems[]): FieldAccessing[] {
    if (!isNullOrUndefOrEmptyGUID(model.methodOfPaymentGUID)) {
      let methodOfPayment: MethodOfPaymentItemView = methodOfPaymentOptions.find((t) => t.value == model.methodOfPaymentGUID)
        .rowData as MethodOfPaymentItemView;
      model.methodOfPayment_PaymentType = methodOfPayment.paymentType;
      if (methodOfPayment.paymentType == PaymentType.SuspenseAccount) {
        model.receiptAmount = 0;
        return [
          { filedIds: secoundGroup, readonly: true },
          { filedIds: forthGroup, readonly: true }
        ];
      } else {
        return [
          { filedIds: secoundGroup, readonly: false },
          { filedIds: forthGroup, readonly: false }
        ];
      }
    } else {
      model.receiptAmount = 0;
      model.methodOfPayment_PaymentType = null;
      return [];
    }
  }
}
