import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, ReceiptTempPaymDetailItemView, ReceiptTempPaymDetailListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ReceiptTempPaymDetailService {
  serviceKey = 'receiptTempPaymDetailGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getReceiptTempPaymDetailToList(search: SearchParameter): Observable<SearchResult<ReceiptTempPaymDetailListView>> {
    const url = `${this.servicePath}/GetReceiptTempPaymDetailList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteReceiptTempPaymDetail(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteReceiptTempPaymDetail`;
    return this.dataGateway.delete(url, row);
  }
  getReceiptTempPaymDetailById(id: string): Observable<ReceiptTempPaymDetailItemView> {
    const url = `${this.servicePath}/GetReceiptTempPaymDetailById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createReceiptTempPaymDetail(vmModel: ReceiptTempPaymDetailItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateReceiptTempPaymDetail`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateReceiptTempPaymDetail(vmModel: ReceiptTempPaymDetailItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateReceiptTempPaymDetail`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(companyId: string): Observable<any> {
    const url = `${this.servicePath}/GetReceiptTempPaymDetailInitialData/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
  getReceiptTempTableAccessMode(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetReceiptTempTableAccessMode/id=${id}`;
    return this.dataGateway.get(url);
  }
}
