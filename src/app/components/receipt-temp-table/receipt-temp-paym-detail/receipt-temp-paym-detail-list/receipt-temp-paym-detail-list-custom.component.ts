import { tap } from 'rxjs/operators';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView } from 'shared/models/viewModel';
import { ReceiptTempPaymDetailService } from '../receipt-temp-paym-detail.service';
const PURCHASE_TABLE_PURCHASE = 'purchasetablepurchase';
const PURCHASE_TABLE_ROLL_BILL = 'purchasetablerollbill';
const WITHDRAWAL_TABLE_WITHDRAWAL = 'withdrawaltablewithdrawal';
const WITHDRAWAL_TABLE_TERM_EXTENSION = 'withdrawaltabletermextension';

export class ReceiptTempPaymDetailListCustomComponent {
  baseService: BaseServiceModel<any>;
  accessModeView: AccessModeView = new AccessModeView();
  parentName: string = null;
  getAccessModeByReceiptTempTable(receiptTempPaymDetailGUID: string) {
    let service: ReceiptTempPaymDetailService = this.baseService.service;
    return service.getReceiptTempTableAccessMode(receiptTempPaymDetailGUID).pipe(tap((result) => (this.accessModeView = result)));
  }

  getCanCreateByParent(accessRight: boolean): boolean {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    const inquiryPath = [PURCHASE_TABLE_PURCHASE, PURCHASE_TABLE_ROLL_BILL, WITHDRAWAL_TABLE_WITHDRAWAL, WITHDRAWAL_TABLE_TERM_EXTENSION];
    return accessRight && this.accessModeView.canCreate && !inquiryPath.some(s => s === this.parentName);
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
}
