import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReceiptTempPaymDetailListComponent } from './receipt-temp-paym-detail-list.component';

describe('ReceiptTempPaymDetailListViewComponent', () => {
  let component: ReceiptTempPaymDetailListComponent;
  let fixture: ComponentFixture<ReceiptTempPaymDetailListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReceiptTempPaymDetailListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptTempPaymDetailListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
