import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AccessMode, ColumnType, SortType } from 'shared/constants';
import { isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { ReceiptTempPaymDetailListView } from 'shared/models/viewModel';
import { ReceiptTempPaymDetailService } from '../receipt-temp-paym-detail.service';
import { ReceiptTempPaymDetailListCustomComponent } from './receipt-temp-paym-detail-list-custom.component';

@Component({
  selector: 'receipt-temp-paym-detail-list',
  templateUrl: './receipt-temp-paym-detail-list.component.html',
  styleUrls: ['./receipt-temp-paym-detail-list.component.scss']
})
export class ReceiptTempPaymDetailListComponent extends BaseListComponent<ReceiptTempPaymDetailListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  @Output() deleteEmit = new EventEmitter<boolean>();
  bankGroupOptions: SelectItems[] = [];
  chequeTableOptions: SelectItems[] = [];
  methodOfPaymentOptions: SelectItems[] = [];
  receiptTempTableOptions: SelectItems[] = [];

  constructor(public custom: ReceiptTempPaymDetailListCustomComponent, private service: ReceiptTempPaymDetailService) {
    super();
    this.baseService.service = this.service;
    this.custom.baseService = this.baseService;
    this.accessRightChildPage = 'ReceiptTempPaymDetailListPage';
    this.parentId = this.uiService.getKey('receipttemptable');
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
    this.custom.getAccessModeByReceiptTempTable(this.parentId).subscribe(() => this.setDataGridOption());
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getChequeTableDropDown(this.chequeTableOptions);
    this.baseDropdown.getMethodOfPaymentDropDown(this.methodOfPaymentOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = true;
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'receiptTempPaymDetailGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.METHOD_OF_PAYMENT_ID',
        textKey: 'methodOfPayment_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.ASC,
        sortingKey: 'methodOfPayment_MethodOfPaymentId',
        searchingKey: 'methodOfPaymentGUID',
        masterList: this.methodOfPaymentOptions
      },
      {
        label: 'LABEL.RECEIPT_AMOUNT',
        textKey: 'receiptAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: 'LABEL.CHEQUE_ID',
        textKey: 'chequeTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        sortingKey: 'chequeTable_ChequeNo',
        searchingKey: 'chequeTableGUID',
        masterList: this.chequeTableOptions
      },
      {
        label: null,
        textKey: 'receiptTempTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<ReceiptTempPaymDetailListView>> {
    return this.service.getReceiptTempPaymDetailToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteReceiptTempPaymDetail(row).pipe(tap(() => this.deleteEmit.emit(true))));
  }
  getRowAuthorize(row: ReceiptTempPaymDetailListView[]): void {
    row.forEach((item) => {
      let accessMode: AccessMode = isNullOrUndefOrEmptyGUID(item.invoiceSettlementDetail_RefReceiptTempPaymDetailGUID)
        ? AccessMode.full
        : AccessMode.viewer;
      item.rowAuthorize = this.getSingleRowAuthorize(accessMode, item);
    });
  }
}
