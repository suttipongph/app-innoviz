import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PrintReceiptTempReportView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class PrintReceiptTempService {
  serviceKey = 'printReceiptTempGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getPrintReceiptTempToList(search: SearchParameter): Observable<SearchResult<PrintReceiptTempReportView>> {
    const url = `${this.servicePath}/GetPrintReceiptTempList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deletePrintReceiptTemp(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeletePrintReceiptTemp`;
    return this.dataGateway.delete(url, row);
  }
  getPrintReceiptTempById(id: string): Observable<PrintReceiptTempReportView> {
    const url = `${this.servicePath}/GetPrintReceiptTempById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createPrintReceiptTemp(vmModel: PrintReceiptTempReportView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreatePrintReceiptTemp`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updatePrintReceiptTemp(vmModel: PrintReceiptTempReportView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdatePrintReceiptTemp`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
