import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintReceiptTempReportComponent } from './print-receipt-temp-report.component';

describe('PrintReceiptTempReportViewComponent', () => {
  let component: PrintReceiptTempReportComponent;
  let fixture: ComponentFixture<PrintReceiptTempReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrintReceiptTempReportComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintReceiptTempReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
