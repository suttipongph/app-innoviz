import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { PrintReceiptTempReportView } from 'shared/models/viewModel';

const firstGroup = [
  'RECEIPT_TEMP_ID',
  'RECEIPT_DATE',
  'TRANS_DATE',
  'PRODUCT_TYPE',
  'RECEIVED_FROM',
  'CUSTOMER_ID',
  'BUYER_ID',
  'RECEIPT_AMOUNT',
  'SETTLE_FEE_AMOUNT',
  'SETTLE_AMOUNT',
  'SUSPENSE_INVOICE_TYPE_GUID',
  'SUSPENSE_AMOUNT',
  'CREDIT_APP_TABLE_GUID'
];

export class PrintReceiptTempReportCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: PrintReceiptTempReportView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<PrintReceiptTempReportView> {
    let model = new PrintReceiptTempReportView();
    model.printReceiptTempGUID = EmptyGuid;
    return of(model);
  }
}
