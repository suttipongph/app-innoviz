import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrintReceiptTempService } from './print-receipt-temp.service';
import { PrintReceiptTempReportComponent } from './print-receipt-temp/print-receipt-temp-report.component';
import { PrintReceiptTempReportCustomComponent } from './print-receipt-temp/print-receipt-temp-report-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PrintReceiptTempReportComponent],
  providers: [PrintReceiptTempService, PrintReceiptTempReportCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PrintReceiptTempReportComponent]
})
export class PrintReceiptTempComponentModule {}
