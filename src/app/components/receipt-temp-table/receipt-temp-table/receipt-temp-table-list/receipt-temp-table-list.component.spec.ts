import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReceiptTempTableListComponent } from './receipt-temp-table-list.component';

describe('ReceiptTempTableListViewComponent', () => {
  let component: ReceiptTempTableListComponent;
  let fixture: ComponentFixture<ReceiptTempTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReceiptTempTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptTempTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
