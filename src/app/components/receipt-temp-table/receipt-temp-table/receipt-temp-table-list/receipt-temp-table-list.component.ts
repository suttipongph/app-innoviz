import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, RefType, SortType } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { ReceiptTempTableListView } from 'shared/models/viewModel';
import { ReceiptTempTableService } from '../receipt-temp-table.service';
import { ReceiptTempTableListCustomComponent } from './receipt-temp-table-list-custom.component';
import { isNullOrUndefOrEmpty } from './../../../../shared/functions/value.function';
import { EmptyGuid } from './../../../../shared/constants/constant';

@Component({
  selector: 'receipt-temp-table-list',
  templateUrl: './receipt-temp-table-list.component.html',
  styleUrls: ['./receipt-temp-table-list.component.scss']
})
export class ReceiptTempTableListComponent extends BaseListComponent<ReceiptTempTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  buyerTableOptions: SelectItems[] = [];
  currencyOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  receivedFromOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];

  constructor(public custom: ReceiptTempTableListCustomComponent, private service: ReceiptTempTableService,
    private currentActivatedRoute: ActivatedRoute) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {
    let passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setPassParameter(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void { }
  checkPageMode(): void { }
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getReceivedFromEnumDropDown(this.receivedFromOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
    this.baseDropdown.getCurrencyDropDown(this.currencyOptions);
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'receiptTempTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.COLLECTION_ID',
        textKey: 'receiptTempId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.RECEIPT_DATE',
        textKey: 'receiptDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.TRANSACTION_DATE',
        textKey: 'transDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: 'LABEL.RECEIVED_FROM',
        textKey: 'receivedFrom',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.receivedFromOptions
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId'
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerTableOptions,
        searchingKey: 'buyerTableGUID',
        sortingKey: 'buyerTable_BuyerId'
      },
      {
        label: 'LABEL.RECEIPT_AMOUNT',
        textKey: 'receiptAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: 'LABEL.CURRENCY_ID',
        textKey: 'currency_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.currencyOptions,
        searchingKey: 'currencyGUID',
        sortingKey: 'currency_CurrencyId'
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentStatusOptions,
        sortingKey: 'documentStatus_StatusId',
        searchingKey: 'documentStatusGUID'
      }
    ];
    if (!isNullOrUndefined(this.custom.refType) && (this.custom.refType === RefType.PurchaseTable || this.custom.refType === RefType.WithdrawalTable)) {
      columns.push({
        label: null,
        textKey: 'receiptTempTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: isNullOrUndefOrEmpty(this.custom.refGUID) ? EmptyGuid : this.custom.refGUID
      });
    }
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<ReceiptTempTableListView>> {
    return this.service.getReceiptTempTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteReceiptTempTable(row));
  }
}
