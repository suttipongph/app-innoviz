import { Observable } from 'rxjs';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel, ColumnModel } from 'shared/models/systemModel';
import { ColumnType, RefType, SortType } from 'shared/constants';

export class ReceiptTempTableListCustomComponent {
  baseService: BaseServiceModel<any>;
  refGUID: string = '';
  refType: number = 0;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => { });
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    const inquiry = [RefType.PurchaseTable, RefType.WithdrawalTable];
    return accessRight && !inquiry.some(s => s === this.refType);
  }
  setPassParameter(passingObj: any): void {
    if (!isNullOrUndefined(passingObj.refGUID)) {
      this.refGUID = passingObj.refGUID;
    }
    if (!isNullOrUndefined(passingObj.refType)) {
      this.refType = passingObj.refType;
    }
  }
}
