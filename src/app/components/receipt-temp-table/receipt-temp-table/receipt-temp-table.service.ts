import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ReceiptTempTableItemView, ReceiptTempTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ReceiptTempTableService {
  serviceKey = 'receiptTempTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getReceiptTempTableToList(search: SearchParameter): Observable<SearchResult<ReceiptTempTableListView>> {
    const url = `${this.servicePath}/GetReceiptTempTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteReceiptTempTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteReceiptTempTable`;
    return this.dataGateway.delete(url, row);
  }
  getReceiptTempTableById(id: string): Observable<ReceiptTempTableItemView> {
    const url = `${this.servicePath}/GetReceiptTempTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createReceiptTempTable(vmModel: ReceiptTempTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateReceiptTempTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateReceiptTempTable(vmModel: ReceiptTempTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateReceiptTempTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  validateIsManualNumberSeq(companyId: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeqReceiptTempTable/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
  validateIsInvoiceSettlementOrPaymentDetailCreated(id: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsInvoiceSettlementOrPaymentDetailCreated/id=${id}`;
    return this.dataGateway.get(url);
  }
  getInitialData(companyId: string): Observable<any> {
    const url = `${this.servicePath}/GetReceiptTempTableInitialData/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
  getExChangeRate(vmModel: ReceiptTempTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/GetExChangeRate`;
    return this.dataGateway.post(url, vmModel);
  }
  getReceiptTempTableAccessMode(id: string): Observable<ReceiptTempTableItemView> {
    const url = `${this.servicePath}/GetReceiptTempTableAccessMode/id=${id}`;
    return this.dataGateway.get(url);
  }
}
