import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import {
  Dimension,
  DocumentProcessStatus,
  ProductType,
  ReceivedFrom,
  RefType,
  ROUTE_FUNCTION_GEN,
  ROUTE_RELATED_GEN,
  ROUTE_REPORT_GEN,
  SuspenseInvoiceType,
  TemporaryReceiptStatus
} from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { LedgerDimensionItemView, ReceiptTempTableItemView, RefIdParm } from 'shared/models/viewModel';
import { ReceiptTempTableService } from '../receipt-temp-table.service';
import { ReceiptTempTableItemCustomComponent } from './receipt-temp-table-item-custom.component';
@Component({
  selector: 'receipt-temp-table-item',
  templateUrl: './receipt-temp-table-item.component.html',
  styleUrls: ['./receipt-temp-table-item.component.scss']
})
export class ReceiptTempTableItemComponent extends BaseItemComponent<ReceiptTempTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  buyerReceiptTableOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  creditAppTableOptions: SelectItems[] = [];
  currencyOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  invoiceTypeOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  receiptTempRefTypeOptions: SelectItems[] = [];
  receivedFromOptions: SelectItems[] = [];

  ledgerDimensionOptions1: SelectItems[] = [];
  ledgerDimensionOptions2: SelectItems[] = [];
  ledgerDimensionOptions3: SelectItems[] = [];
  ledgerDimensionOptions4: SelectItems[] = [];
  ledgerDimensionOptions5: SelectItems[] = [];

  constructor(
    private service: ReceiptTempTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: ReceiptTempTableItemCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {
    let passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setPassParameter(passingObj);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getReceiptTempRefTypeEnumDropDown(this.receiptTempRefTypeOptions);
    this.baseDropdown.getReceivedFromEnumDropDown(this.receivedFromOptions);
  }
  getById(): Observable<ReceiptTempTableItemView> {
    return this.service.getReceiptTempTableById(this.id);
  }
  getInitialData(): Observable<ReceiptTempTableItemView> {
    return this.service.getInitialData(this.userDataService.getCurrentCompanyGUID());
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions(result);
        this.setFunctionOptions(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    let tempModel: ReceiptTempTableItemView = this.isUpdateMode ? model : this.model;
    forkJoin([
      this.custom.getBuyerReceiptDropDown(tempModel),
      this.baseDropdown.getBuyerTableDropDown(),
      this.custom.getCreditAppTableDropDown(tempModel),
      this.baseDropdown.getCurrencyDropDown(),
      this.baseDropdown.getCustomerTableDropDown(),
      this.custom.getInvoiceTypeDropDown(tempModel),
      this.baseDropdown.getLedgerDimensionDropDown()
    ]).subscribe(
      ([buyerReceiptTable, buyerTable, creditAppTable, currency, customerTable, invoiceType, ledgerDimension]) => {
        this.buyerReceiptTableOptions = buyerReceiptTable;
        this.buyerTableOptions = buyerTable;
        this.creditAppTableOptions = creditAppTable;
        this.currencyOptions = currency;
        this.customerTableOptions = customerTable;
        this.invoiceTypeOptions = invoiceType;
        this.setLedgerDimension(ledgerDimension);

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  setRelatedInfoOptions(model: ReceiptTempTableItemView): void {
    const isReceivedFromBuyer = model.receivedFrom === ReceivedFrom.Buyer ? true : false;
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        command: () => {
          const refIdParm: RefIdParm = { refType: RefType.ReceiptTemp, refGUID: this.model.receiptTempTableGUID };
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: refIdParm
          });
        }
      },
      {
        label: 'LABEL.INVOICE_SETTLEMENT',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.INVOICE_SETTLEMENT,
            parameters: {
              refGUID: model.receiptTempTableGUID,
              refType: RefType.ReceiptTemp,
              refId: model.receiptTempId,
              invoiceTable_CustomerTableGUID: model.customerTableGUID,
              suspenseInvoiceType: SuspenseInvoiceType.None,
              productInvoice: isReceivedFromBuyer,
              productType: model.productType,
              buyerTableGUID: isReceivedFromBuyer ? model.buyerTableGUID : null,
              receivedFrom: model.receivedFrom
            }
          })
      },
      {
        label: 'LABEL.RECEIPT_TABLE',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.RECEIPT_TABLE
          })
      },
      {
        label: 'LABEL.MEMO',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.MEMO
          })
      },
      {
        label: 'LABEL.SERVICE_FEE',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.SERVICE_FEE_TRANS,
            parameters: {
              refId: model.receiptTempId,
              taxDate: model.receiptDate,
              productType: model.productType,
              invoiceTable_CustomerTableGUID: model.customerTableGUID
            }
          })
      },
      {
        label: 'LABEL.BUYER_RECEIPT',
        disabled: !isReceivedFromBuyer,
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.BUYER_RECEIPT_TABLE })
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(model: ReceiptTempTableItemView): void {
    const getprintdisible = this.custom.getPrintDisible(model);
    this.functionItems = [
      {
        label: 'LABEL.POST',
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.POST_RECEIPT_TEMP }),
        disabled: model.documentStatus_StatusId !== TemporaryReceiptStatus.Draft
      },
      {
        label: 'LABEL.CANCEL',
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.CANCEL_RECEIPT_TEMP }),
        disabled: model.documentStatus_StatusId !== TemporaryReceiptStatus.Draft
      },
      {
        label: 'LABEL.PRINT',
        disabled: getprintdisible,
        items: [
          {
            label: 'LABEL.COLLECTION',
            disabled: getprintdisible,
            command: () =>
              this.toReport({
                path: ROUTE_REPORT_GEN.PRINT_RECEIPT_TEMP
              })
          }
        ]
      }
    ];
    super.setFunctionOptionsMapping();
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateReceiptTempTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createReceiptTempTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }

  onProductTypeChange() {
    this.model.creditAppTableGUID = null;
    this.model.suspenseInvoiceTypeGUID = null;
    this.setCreditAppTableDropDown();
    this.setInvoiceTypeDropDown();
  }

  onBuyerChange() {
    this.model.buyerReceiptTableGUID = null;
    this.setBuyerReceiptDropDown();
  }
  onCustomerChange() {
    this.model.buyerReceiptTableGUID = null;
    this.model.creditAppTableGUID = null;
    this.setBuyerReceiptDropDown();
    this.setCreditAppTableDropDown();
  }

  onReceivedFromChange() {
    super.setBaseFieldAccessing(this.custom.setFieldAccessingByReceivedFrom(this.model.receivedFrom));
  }
  onInvoiceTypeChange() {
    super.setBaseFieldAccessing(this.custom.setFieldAccessingByInvoiceType(this.model, this.invoiceTypeOptions));
  }
  onSuspenseAmountChange() {
    this.custom.setMandatoryBySuspenseAmount(this.model);
  }
  onCurrencyChange() {
    this.custom.setExchangeRateByCurrency(this.model);
  }
  onReceiptTempPaymDeleted(e) {
    this.toReload();
  }
  onSuspenseInvoiceTypeChange() {
    super.setBaseFieldAccessing(this.custom.setFieldAccessingBySuspenseInvoiceType(this.model, this.invoiceTypeOptions));
  }
  setLedgerDimension(ledgerDimension: SelectItems[]): void {
    this.ledgerDimensionOptions1 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension1);
    this.ledgerDimensionOptions2 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension2);
    this.ledgerDimensionOptions3 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension3);
    this.ledgerDimensionOptions4 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension4);
    this.ledgerDimensionOptions5 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension5);
  }
  //#region update pattern dropdown
  setInvoiceTypeDropDown() {
    if (!isNullOrUndefined(this.model.productType)) {
      if (this.model.productType == ProductType.None) {
        this.baseService.baseDropdown.getInvoiceTypeByReceiptTempTableNoneProductTypeDropDown().subscribe((result) => {
          this.invoiceTypeOptions = result;
        });
      } else {
        this.baseService.baseDropdown.getInvoiceTypeByReceiptTempTableProductTypeDropDown(this.model.productType.toString()).subscribe((result) => {
          this.invoiceTypeOptions = result;
        });
      }
    } else {
      this.invoiceTypeOptions.length = 0;
    }
  }
  setCreditAppTableDropDown() {
    if (!isNullOrUndefOrEmptyGUID(this.model.customerTableGUID) && !isNullOrUndefined(this.model.productType)) {
      this.baseService.baseDropdown
        .getCreditAppTableByReceiptTempTableDropDown([this.model.customerTableGUID, this.model.productType.toString()])
        .subscribe((result) => {
          this.creditAppTableOptions = result;
        });
    } else {
      this.creditAppTableOptions.length = 0;
    }
  }

  setBuyerReceiptDropDown() {
    if (!isNullOrUndefOrEmptyGUID(this.model.customerTableGUID) && !isNullOrUndefOrEmptyGUID(this.model.buyerTableGUID)) {
      this.baseService.baseDropdown
        .getBuyerReceiptTableByReceiptTempTableDropDown([this.model.customerTableGUID, this.model.buyerTableGUID])
        .subscribe((result) => {
          this.buyerReceiptTableOptions = result;
        });
    } else {
      this.buyerReceiptTableOptions.length = 0;
    }
  }
  //#endregion
}
