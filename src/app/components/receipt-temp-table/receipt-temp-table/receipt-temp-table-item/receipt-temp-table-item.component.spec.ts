import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiptTempTableItemComponent } from './receipt-temp-table-item.component';

describe('ReceiptTempTableItemViewComponent', () => {
  let component: ReceiptTempTableItemComponent;
  let fixture: ComponentFixture<ReceiptTempTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReceiptTempTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptTempTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
