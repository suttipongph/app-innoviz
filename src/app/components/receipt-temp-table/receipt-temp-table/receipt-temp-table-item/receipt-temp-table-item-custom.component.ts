import { isNull } from '@angular/compiler/src/output/output_ast';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { DocumentProcessStatus, ProductType, ReceivedFrom, RefType, TemporaryReceiptStatus } from 'shared/constants';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { InvoiceTypeItemView, ReceiptTempTableItemView } from 'shared/models/viewModel';

const firstGroup = [
  'DOCUMENT_STATUS_GUID',
  'DOCUMENT_REASON_GUID',
  'RECEIPT_AMOUNT',
  'SETTLE_FEE_AMOUNT',
  'SETTLE_AMOUNT',
  'EXCHANGE_RATE',
  'RECEIPT_AMOUNT_MST',
  'RECEIPT_TEMP_REF_TYPE',
  'MAIN_RECEIPT_TEMP_GUID',
  'REF_RECEIPT_TEMP_GUID',
  'RECEIPT_TEMP_TABLE_GUID'
];

const secoundGroup = ['RECEIPT_TEMP_ID'];
const thirdGroup = ['BUYER_RECEIPT_TABLE_GUID'];
const forthGroup = ['CREDIT_APP_TABLE_GUID'];
const generalGroup = ['RECEIPT_DATE', 'TRANS_DATE', 'PRODUCT_TYPE', 'RECEIVED_FROM', 'CUSTOMER_TABLE_GUID', 'BUYER_TABLE_GUID', 'CURRENCY_GUID'];

export class ReceiptTempTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  isManual = false;
  isNumbersSeqNotSet = false;

  isBuyerMandatory = false;
  isCreditAppMandatory = false;
  isInvoiceTypeMandatory = false;
  refGUID: string = '';
  refType: number = 0;

  getFieldAccessing(model: ReceiptTempTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    fieldAccessing.push({ filedIds: thirdGroup, readonly: true });
    this.isBuyerMandatory = false;

    if (!isUpdateMode(model.receiptTempTableGUID)) {
      fieldAccessing.push({ filedIds: forthGroup, readonly: true });
      fieldAccessing.push({ filedIds: generalGroup, readonly: false });

      return this.baseService.service.validateIsManualNumberSeq(model.companyGUID).pipe(
        map((result: boolean) => {
          this.isManual = result;
          fieldAccessing.push({ filedIds: secoundGroup, readonly: !this.isManual });
          return fieldAccessing;
        }),
        catchError((err) => {
          this.baseService.notificationService.showErrorMessageFromResponse(err);
          this.isNumbersSeqNotSet = true;
          return fieldAccessing;
        })
      );
    } else {
      fieldAccessing.push({ filedIds: secoundGroup, readonly: true });
      if (model.receivedFrom == ReceivedFrom.Customer) {
        this.isBuyerMandatory = false;
        fieldAccessing.push({ filedIds: thirdGroup, readonly: true });
      } else {
        this.isBuyerMandatory = true;
        fieldAccessing.push({ filedIds: thirdGroup, readonly: false });
      }

      if (model.invoiceType_ValidateDirectReceiveByCA == true) {
        this.isCreditAppMandatory = true;
        fieldAccessing.push({ filedIds: forthGroup, readonly: false });
      } else {
        this.isCreditAppMandatory = false;
        fieldAccessing.push({ filedIds: forthGroup, readonly: true });
      }

      return this.baseService.service.validateIsInvoiceSettlementOrPaymentDetailCreated(model.receiptTempTableGUID).pipe(
        map((result: boolean) => {
          fieldAccessing.push({ filedIds: generalGroup, readonly: result });
          if (model.receivedFrom != ReceivedFrom.Customer) {
            fieldAccessing.push({ filedIds: thirdGroup, readonly: result });
          }
          return fieldAccessing;
        }),
        catchError((err) => {
          this.baseService.notificationService.showErrorMessageFromResponse(err);
          fieldAccessing.push({ filedIds: generalGroup, readonly: true });
          fieldAccessing.push({ filedIds: thirdGroup, readonly: true });
          return fieldAccessing;
        })
      );
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ReceiptTempTableItemView): Observable<boolean> {
    const accessright = isNullOrUndefOrEmptyGUID(model.receiptTempTableGUID) ? canCreate : canUpdate;
    const accessLogic = isNullOrUndefOrEmptyGUID(model.receiptTempTableGUID) ? true : model.documentStatus_StatusId == TemporaryReceiptStatus.Draft;
    const inquiry = [RefType.PurchaseTable, RefType.WithdrawalTable];
    if (inquiry.some((s) => s === this.refType)) {
      return of(true);
    } else {
      return of(!(accessLogic && accessright));
    }
  }

  // dropdown
  getBuyerReceiptDropDown(model: ReceiptTempTableItemView) {
    if (!isNullOrUndefOrEmptyGUID(model.customerTableGUID) && !isNullOrUndefOrEmptyGUID(model.buyerTableGUID)) {
      return this.baseService.baseDropdown.getBuyerReceiptTableByReceiptTempTableDropDown([model.customerTableGUID, model.buyerTableGUID]);
    } else {
      return of([]);
    }
  }
  getCreditAppTableDropDown(model: ReceiptTempTableItemView) {
    if (!isNullOrUndefOrEmptyGUID(model.customerTableGUID) && !isNullOrUndefined(model.productType)) {
      return this.baseService.baseDropdown.getCreditAppTableByReceiptTempTableDropDown([model.customerTableGUID, model.productType.toString()]);
    } else {
      return of([]);
    }
  }
  getInvoiceTypeDropDown(model: ReceiptTempTableItemView) {
    if (!isNullOrUndefined(model.productType)) {
      if (model.productType == ProductType.None) {
        return this.baseService.baseDropdown.getInvoiceTypeByReceiptTempTableNoneProductTypeDropDown();
      } else {
        return this.baseService.baseDropdown.getInvoiceTypeByReceiptTempTableProductTypeDropDown(model.productType.toString());
      }
    } else {
      return of([]);
    }
  }

  setFieldAccessingByReceivedFrom(receivedFrom: number): FieldAccessing[] {
    switch (receivedFrom) {
      case ReceivedFrom.Customer:
        this.isBuyerMandatory = false;
        return [{ filedIds: thirdGroup, readonly: true }];
      case ReceivedFrom.Buyer:
        this.isBuyerMandatory = true;
        return [{ filedIds: thirdGroup, readonly: false }];
      default:
        return [];
    }
  }
  setFieldAccessingByInvoiceType(model: ReceiptTempTableItemView, invoiceTypeOptions: SelectItems[]): FieldAccessing[] {
    if (!isNullOrUndefOrEmptyGUID(model.suspenseInvoiceTypeGUID)) {
      let invoiceType: InvoiceTypeItemView = invoiceTypeOptions.find((t) => t.value == model.suspenseInvoiceTypeGUID).rowData as InvoiceTypeItemView;
      if (invoiceType.validateDirectReceiveByCA) {
        this.isCreditAppMandatory = true;
        return [{ filedIds: forthGroup, readonly: false }];
      } else {
        this.isCreditAppMandatory = false;
        return [{ filedIds: forthGroup, readonly: true }];
      }
    } else {
      this.isCreditAppMandatory = false;
      return [];
    }
  }

  setMandatoryBySuspenseAmount(model: ReceiptTempTableItemView) {
    if (model.suspenseAmount > 0) {
      this.isInvoiceTypeMandatory = true;
    } else {
      this.isInvoiceTypeMandatory = false;
    }
  }

  setExchangeRateByCurrency(model: ReceiptTempTableItemView) {
    if (!isNullOrUndefOrEmptyGUID(model.currencyGUID)) {
      this.baseService.service.getExChangeRate(model).subscribe(
        (result) => {
          model.exchangeRate = result.exchangeRate;
        },
        (error) => {
          this.baseService.notificationService.showErrorMessageFromResponse(error);
          model.exchangeRate = 0;
        }
      );
    } else {
      model.exchangeRate = 0;
    }
  }
  getPrintDisible(model: ReceiptTempTableItemView): boolean {
    if (model.documentStatus_StatusId !== TemporaryReceiptStatus.Cancelled && model.process_Id === DocumentProcessStatus.TemporaryReceipt) {
      return false;
    } else {
      return true;
    }
  }
  setFieldAccessingBySuspenseInvoiceType(model: ReceiptTempTableItemView, invoiceTypeOptions: SelectItems[]): FieldAccessing[] {
    if (!isNullOrUndefOrEmptyGUID(model.suspenseInvoiceTypeGUID)) {
      let invoiceType: InvoiceTypeItemView = invoiceTypeOptions.find((t) => t.value == model.suspenseInvoiceTypeGUID).rowData as InvoiceTypeItemView;
      model.invoiceType_ValidateDirectReceiveByCA = invoiceType.validateDirectReceiveByCA;
      if (invoiceType.validateDirectReceiveByCA) {
        this.isCreditAppMandatory = true;
        return [{ filedIds: forthGroup, readonly: false }];
      } else {
        this.isCreditAppMandatory = false;
        return [{ filedIds: forthGroup, readonly: true }];
      }
    } else {
      this.isCreditAppMandatory = false;
      return [{ filedIds: forthGroup, readonly: true }];
    }
  }
  setPassParameter(passingObj: any): void {
    if (!isNullOrUndefined(passingObj)) {
      if (!isNullOrUndefined(passingObj.refGUID)) {
        this.refGUID = passingObj.refGUID;
      }
      if (!isNullOrUndefined(passingObj.refType)) {
        this.refType = passingObj.refType;
      }
    }
  }
}
