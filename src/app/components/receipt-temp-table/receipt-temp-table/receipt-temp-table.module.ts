import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReceiptTempTableService } from './receipt-temp-table.service';
import { ReceiptTempTableListComponent } from './receipt-temp-table-list/receipt-temp-table-list.component';
import { ReceiptTempTableItemComponent } from './receipt-temp-table-item/receipt-temp-table-item.component';
import { ReceiptTempTableItemCustomComponent } from './receipt-temp-table-item/receipt-temp-table-item-custom.component';
import { ReceiptTempTableListCustomComponent } from './receipt-temp-table-list/receipt-temp-table-list-custom.component';
import { ReceiptTempPaymDetailComponentModule } from '../receipt-temp-paym-detail/receipt-temp-paym-detail.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, ReceiptTempPaymDetailComponentModule],
  declarations: [ReceiptTempTableListComponent, ReceiptTempTableItemComponent],
  providers: [ReceiptTempTableService, ReceiptTempTableItemCustomComponent, ReceiptTempTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ReceiptTempTableListComponent, ReceiptTempTableItemComponent]
})
export class ReceiptTempTableComponentModule {}
