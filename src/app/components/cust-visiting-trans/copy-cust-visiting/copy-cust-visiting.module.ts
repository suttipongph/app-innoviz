import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CopyCustVisitingService } from './copy-cust-visiting.service';
import { CopyCustVisitingComponent } from './copy-cust-visiting/copy-cust-visiting.component';
import { CopyCustVisitingCustomComponent } from './copy-cust-visiting/copy-cust-visiting-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CopyCustVisitingComponent],
  providers: [CopyCustVisitingService, CopyCustVisitingCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CopyCustVisitingComponent]
})
export class CopyCustVisitingComponentModule {}
