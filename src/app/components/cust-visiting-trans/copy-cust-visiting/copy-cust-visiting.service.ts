import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CopyCustVisitingView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CopyCustVisitingService {
  serviceKey = 'copyCustVisitingGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCopyCustVisitingById(id: string): Observable<CopyCustVisitingView> {
    const url = `${this.servicePath}/GetCopyCustVisitingById/id=${id}`;
    return this.dataGateway.get(url);
  }
  getCopyCustVisitingByRefTypeValidation(data: CopyCustVisitingView): Observable<boolean> {
    const url = `${this.servicePath}/GetCopyCustVisitingByRefTypeValidation`;
    return this.dataGateway.post(url, data);
  }
  copyCustVisiting(vmModel: CopyCustVisitingView): Observable<CopyCustVisitingView> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vmModel);
  }
}
