import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { CopyCustVisitingView } from 'shared/models/viewModel';
import { CopyCustVisitingService } from '../copy-cust-visiting.service';
import { CopyCustVisitingCustomComponent } from './copy-cust-visiting-custom.component';
@Component({
  selector: 'copy-cust-visiting',
  templateUrl: './copy-cust-visiting.component.html',
  styleUrls: ['./copy-cust-visiting.component.scss']
})
export class CopyCustVisitingComponent extends BaseItemComponent<CopyCustVisitingView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  custVisitingTransOptions: SelectItems[] = [];

  constructor(
    private service: CopyCustVisitingService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CopyCustVisitingCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.baseService.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialUpdatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<CopyCustVisitingView> {
    return this.service.getCopyCustVisitingById(this.id);
  }

  getInitialData(): Observable<CopyCustVisitingView> {
    return null;
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {}

  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getCustomerVisitingTransByRefType(model.customerTableGUID)).subscribe(
      ([custVisitingTrans]) => {
        this.custVisitingTransOptions = custVisitingTrans;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing().subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {}
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.custom.getModelParam(this.model, this.id).subscribe((model) => {
          this.onSubmit(true, model);
        });
      }
    });
  }
  onSubmit(isColsing: boolean, model: CopyCustVisitingView): void {
    super.onExecuteFunction(this.service.copyCustVisiting(model));
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation(this.model);
  }
}
