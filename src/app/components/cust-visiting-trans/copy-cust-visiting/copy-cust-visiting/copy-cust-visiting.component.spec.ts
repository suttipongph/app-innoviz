import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopyCustVisitingComponent } from './copy-cust-visiting.component';

describe('CopyCustVisitingViewComponent', () => {
  let component: CopyCustVisitingComponent;
  let fixture: ComponentFixture<CopyCustVisitingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CopyCustVisitingComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyCustVisitingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
