import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { EmptyGuid, RefType, ROUTE_MASTER_GEN } from 'shared/constants';
import { BaseServiceModel, FieldAccessing, TranslateModel } from 'shared/models/systemModel';
import { CopyCustVisitingView } from 'shared/models/viewModel';
import { CopyCustVisitingService } from '../copy-cust-visiting.service';

const firstGroup = [];

export class CopyCustVisitingCustomComponent {
  baseService: BaseServiceModel<CopyCustVisitingService>;
  getFieldAccessing(): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getDataValidation(model: CopyCustVisitingView): Observable<boolean> {
    return of(true);
  }
  getModelParam(model: CopyCustVisitingView, parentGUID: string): Observable<CopyCustVisitingView> {
    model.refGUID = parentGUID;
    model.refType = RefType.CreditAppRequestTable;
    model.resultLabel = 'LABEL.CUSTOMER_VISITING_TRANS';
    return of(model);
  }
}
