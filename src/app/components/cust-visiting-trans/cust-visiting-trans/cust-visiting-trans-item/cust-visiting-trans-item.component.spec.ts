import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustVisitingTransItemComponent } from './cust-visiting-trans-item.component';

describe('CustVisitingTransItemViewComponent', () => {
  let component: CustVisitingTransItemComponent;
  let fixture: ComponentFixture<CustVisitingTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustVisitingTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustVisitingTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
