import { Component, Input } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, ROUTE_FUNCTION_GEN, SortType } from 'shared/constants';
import { MenuItem } from 'shared/models/primeModel';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CustVisitingTransListView } from 'shared/models/viewModel';
import { CustVisitingTransService } from '../cust-visiting-trans.service';
import { CustVisitingTransListCustomComponent } from './cust-visiting-trans-list-custom.component';

@Component({
  selector: 'cust-visiting-trans-list',
  templateUrl: './cust-visiting-trans-list.component.html',
  styleUrls: ['./cust-visiting-trans-list.component.scss']
})
export class CustVisitingTransListComponent extends BaseListComponent<CustVisitingTransListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  employeeTableOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];

  constructor(
    public custom: CustVisitingTransListCustomComponent,
    private service: CustVisitingTransService,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.setAccessModeByParentStatus(this.parentId, this.isWorkFlowMode()).subscribe((result) => {
      this.setDataGridOption();
      this.setFunctionOptions();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getEmployeeTableDropDown(this.employeeTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'custVisitingTransGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.DATE_OF_VISITED',
        textKey: 'custVisitingDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.SALSE_RESPONSIBLE_BY',
        textKey: 'employeeTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.employeeTableOptions,
        sortingKey: 'employeeTable_EmployeeId',
        searchingKey: 'responsibleByGUID'
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CustVisitingTransListView>> {
    return this.service.getCustVisitingTransToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCustVisitingTrans(row));
  }
  setFunctionOptions(): void {
    this.functionItems = [
      {
        label: 'LABEL.COPY_CUSTOMER_VISITING',
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.COPY_CUST_VISITING }),
        disabled: !this.custom.accessModeView.canCreate,
        visible: !this.custom.isCustomerParent
      }
    ];
    const enableFunctionsWFMode: MenuItem[] = [{ label: 'LABEL.COPY_CUSTOMER_VISITING' }];
    super.setFunctionOptionsMapping(enableFunctionsWFMode);
  }
}
