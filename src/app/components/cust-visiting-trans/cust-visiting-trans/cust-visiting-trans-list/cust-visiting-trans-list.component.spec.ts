import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CustVisitingTransListComponent } from './cust-visiting-trans-list.component';

describe('CustVisitingTransListViewComponent', () => {
  let component: CustVisitingTransListComponent;
  let fixture: ComponentFixture<CustVisitingTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustVisitingTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustVisitingTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
