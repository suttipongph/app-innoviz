import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, CustVisitingTransItemView, CustVisitingTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CustVisitingTransService {
  serviceKey = 'custVisitingTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCustVisitingTransToList(search: SearchParameter): Observable<SearchResult<CustVisitingTransListView>> {
    const url = `${this.servicePath}/GetCustVisitingTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCustVisitingTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCustVisitingTrans`;
    return this.dataGateway.delete(url, row);
  }
  getCustVisitingTransById(id: string): Observable<CustVisitingTransItemView> {
    const url = `${this.servicePath}/GetCustVisitingTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCustVisitingTrans(vmModel: CustVisitingTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCustVisitingTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCustVisitingTrans(vmModel: CustVisitingTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCustVisitingTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<CustVisitingTransItemView> {
    const url = `${this.servicePath}/GetCustVisitingTransInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByCreditAppRequestTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppRequestTable/id=${id}`;
    return this.dataGateway.get(url);
  }
}
