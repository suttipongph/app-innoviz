import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustVisitingTransService } from './cust-visiting-trans.service';
import { CustVisitingTransListComponent } from './cust-visiting-trans-list/cust-visiting-trans-list.component';
import { CustVisitingTransItemComponent } from './cust-visiting-trans-item/cust-visiting-trans-item.component';
import { CustVisitingTransItemCustomComponent } from './cust-visiting-trans-item/cust-visiting-trans-item-custom.component';
import { CustVisitingTransListCustomComponent } from './cust-visiting-trans-list/cust-visiting-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CustVisitingTransListComponent, CustVisitingTransItemComponent],
  providers: [CustVisitingTransService, CustVisitingTransItemCustomComponent, CustVisitingTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CustVisitingTransListComponent, CustVisitingTransItemComponent]
})
export class CustVisitingTransComponentModule {}
