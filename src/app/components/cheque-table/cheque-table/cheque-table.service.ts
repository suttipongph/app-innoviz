import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, ChequeTableItemView, ChequeTableListView, DocumentStatusItemView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult, SelectItems } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ChequeTableService {
  serviceKey = 'chequeTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getChequeTableToList(search: SearchParameter): Observable<SearchResult<ChequeTableListView>> {
    const url = `${this.servicePath}/GetChequeTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteChequeTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteChequeTable`;
    return this.dataGateway.delete(url, row);
  }
  getChequeTableById(id: string): Observable<ChequeTableItemView> {
    const url = `${this.servicePath}/GetChequeTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createChequeTable(vmModel: ChequeTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateChequeTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateChequeTable(vmModel: ChequeTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateChequeTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<ChequeTableItemView> {
    const url = `${this.servicePath}/GetChequeTableInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getPurchaseTableAccessMode(purchaseId: string, isWorkflowMode: boolean): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetPurchaseTableAccessMode/purchaseId=${purchaseId}/isWorkflowMode=${isWorkflowMode}`;
    return this.dataGateway.get(url);
  }
  getWithdrawalTableAccessMode(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetWithdrawalTableAccessMode/id=${id}`;
    return this.dataGateway.get(url);
  }
  getChequeRefPDCDropdown(model: ChequeTableItemView): Observable<SelectItems[]> {
    const url = `${this.servicePath}/GetChequeRefPDCDropdown/refType=${model.refType}/customerId=${model.customerTableGUID}/buyerId=${model.buyerTableGUID}`;
    return this.dataGateway.get(url);
  }
  setDocumentStatusValidation(vmModel: ChequeTableItemView): Observable<boolean> {
    const url = `${this.servicePath}/GetDocumentStatusValidation`;
    return this.dataGateway.post(url, vmModel);
  }
  setDeposittStatusValidation(vmModel: ChequeTableItemView): Observable<boolean> {
    const url = `${this.servicePath}/GetDeposittStatusValidation`;
    return this.dataGateway.post(url, vmModel);
  }
  getBounceStatusValidation(vmModel: ChequeTableItemView): Observable<boolean> {
    const url = `${this.servicePath}/GetBounceStatusValidation`;
    return this.dataGateway.post(url, vmModel);
  }
  getReplaceStatusValidation(vmModel: ChequeTableItemView): Observable<boolean> {
    const url = `${this.servicePath}/GetReplaceStatusValidation`;
    return this.dataGateway.post(url, vmModel);
  }
  getReturnStatusValidation(vmModel: ChequeTableItemView): Observable<boolean> {
    const url = `${this.servicePath}/GetReturnStatusValidation`;
    return this.dataGateway.post(url, vmModel);
  }
  getCancelStatusValidation(vmModel: ChequeTableItemView): Observable<boolean> {
    const url = `${this.servicePath}/GetCancelStatusValidation`;
    return this.dataGateway.post(url, vmModel);
  }
}
