import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DepositChequeService } from './deposit-cheque.service';
import { DepositChequeComponent } from './deposit-cheque/deposit-cheque.component';

import { DepositChequeCustomComponent } from './deposit-cheque/deposit-cheque-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [DepositChequeComponent],
  providers: [DepositChequeService, DepositChequeCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [DepositChequeComponent]
})
export class DepositChequeComponentModule {}
