import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { DepositChequeView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class DepositChequeService {
  serviceKey = 'depositChequeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getDepositChequeToList(search: SearchParameter): Observable<SearchResult<DepositChequeView>> {
    const url = `${this.servicePath}/GetDepositChequeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteDepositCheque(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteDepositCheque`;
    return this.dataGateway.delete(url, row);
  }
  getDepositChequeById(id: string): Observable<DepositChequeView> {
    const url = `${this.servicePath}/GetDepositChequeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createDepositCheque(vmModel: DepositChequeView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateDepositCheque`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateDepositCheque(vmModel: DepositChequeView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateDepositCheque`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<DepositChequeView> {
    const url = `${this.servicePath}/getInitialDataDepositStatus/id=${id}`;
    return this.dataGateway.get(url);
  }
  updateDepositStatusCheque(vmModel: DepositChequeView): Observable<ResponseModel> {
    const url = `${this.servicePath}/updateDepositStatusCheque`;
    return this.dataGateway.post(url, vmModel);
  }
}
