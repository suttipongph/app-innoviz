import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { DepositChequeView } from 'shared/models/viewModel';

const firstGroup = [
  'RECEIVED_FROM',
  'CUSTOMER_TABLE_GUID',
  'BUYER_TABLE_GUID',
  'DOCUMENT_STATUS_GUID',
  'REMARK',
  'CHEQUE_DATE',
  'CHEQUE_NO',
  'CHEQUE_BANK_ACC_NO',
  'CHEQUE_BANK_GROUP_GUID',
  'CHEQUE_BRANCH',
  'AMOUNT',
  'PDC',
  'ISSUED_NAME',
  'RECIPIENT_NAME',
  'EXPECTED_DEPOSIT_DATE'
];

export class DepositChequeCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: DepositChequeView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: DepositChequeView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<DepositChequeView> {
    let model = new DepositChequeView();
    model.depositChequeGUID = EmptyGuid;
    return of(model);
  }
}
