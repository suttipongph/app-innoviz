import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReplaceChequeService } from './replace-cheque.service';
import { ReplaceChequeComponent } from './replace-cheque/replace-cheque.component';

import { ReplaceChequeCustomComponent } from './replace-cheque/replace-cheque-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ReplaceChequeComponent],
  providers: [ReplaceChequeService, ReplaceChequeCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ReplaceChequeComponent]
})
export class ReplaceChequeComponentModule {}
