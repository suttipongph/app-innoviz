import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReplaceChequeComponent } from './replace-cheque.component';

describe('ReplaceChequeViewComponent', () => {
  let component: ReplaceChequeComponent;
  let fixture: ComponentFixture<ReplaceChequeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReplaceChequeComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplaceChequeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
