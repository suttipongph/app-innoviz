import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { ReplaceChequeView } from 'shared/models/viewModel';

const firstGroup = [
  'RECEIVED_FROM',
  'CUSTOMER_TABLE_GUID',
  'BUYER_TABLE_GUID',
  'DOCUMENT_STATUS_GUID',
  'REMARK',
  'CHEQUE_DATE',
  'CHEQUE_NO',
  'CHEQUE_BANK_ACC_NO',
  'CHEQUE_BANK_GROUP_GUID',
  'CHEQUE_BRANCH',
  'AMOUNT',
  'PDC',
  'ISSUED_NAME',
  'RECIPIENT_NAME'
];

export class ReplaceChequeCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: ReplaceChequeView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ReplaceChequeView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<ReplaceChequeView> {
    let model = new ReplaceChequeView();
    model.replaceChequeGUID = EmptyGuid;
    return of(model);
  }
}
