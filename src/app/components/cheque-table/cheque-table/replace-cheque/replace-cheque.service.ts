import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ReplaceChequeView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ReplaceChequeService {
  serviceKey = 'replaceChequeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getReplaceChequeToList(search: SearchParameter): Observable<SearchResult<ReplaceChequeView>> {
    const url = `${this.servicePath}/GetReplaceChequeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteReplaceCheque(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteReplaceCheque`;
    return this.dataGateway.delete(url, row);
  }
  getReplaceChequeById(id: string): Observable<ReplaceChequeView> {
    const url = `${this.servicePath}/GetReplaceChequeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createReplaceCheque(vmModel: ReplaceChequeView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateReplaceCheque`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateReplaceCheque(vmModel: ReplaceChequeView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateReplaceCheque`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<ReplaceChequeView> {
    const url = `${this.servicePath}/GetInitialDataReplaceStatus/id=${id}`;
    return this.dataGateway.get(url);
  }
  updateDepositStatusCheque(vmModel: ReplaceChequeView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateReplaceStatusCheque`;
    return this.dataGateway.post(url, vmModel);
  }
}
