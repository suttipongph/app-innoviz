import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChequeTableService } from './cheque-table.service';
import { ChequeTableListComponent } from './cheque-table-list/cheque-table-list.component';
import { ChequeTableItemComponent } from './cheque-table-item/cheque-table-item.component';
import { ChequeTableItemCustomComponent } from './cheque-table-item/cheque-table-item-custom.component';
import { ChequeTableListCustomComponent } from './cheque-table-list/cheque-table-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ChequeTableListComponent, ChequeTableItemComponent],
  providers: [ChequeTableService, ChequeTableItemCustomComponent, ChequeTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ChequeTableListComponent, ChequeTableItemComponent]
})
export class ChequeTableComponentModule {}
