import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BounceChequeComponent } from './bounce-cheque.component';

describe('BounceChequeViewComponent', () => {
  let component: BounceChequeComponent;
  let fixture: ComponentFixture<BounceChequeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BounceChequeComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BounceChequeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
