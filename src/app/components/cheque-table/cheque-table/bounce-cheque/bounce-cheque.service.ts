import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BounceChequeView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class BounceChequeService {
  serviceKey = 'bounceChequeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBounceChequeToList(search: SearchParameter): Observable<SearchResult<BounceChequeView>> {
    const url = `${this.servicePath}/GetBounceChequeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBounceCheque(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBounceCheque`;
    return this.dataGateway.delete(url, row);
  }
  getBounceChequeById(id: string): Observable<BounceChequeView> {
    const url = `${this.servicePath}/GetBounceChequeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBounceCheque(vmModel: BounceChequeView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBounceCheque`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBounceCheque(vmModel: BounceChequeView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBounceCheque`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<BounceChequeView> {
    const url = `${this.servicePath}/getInitialDataBounceStatus/id=${id}`;
    return this.dataGateway.get(url);
  }
  updateDepositStatusCheque(vmModel: BounceChequeView): Observable<ResponseModel> {
    const url = `${this.servicePath}/updateBounceStatusCheque`;
    return this.dataGateway.post(url, vmModel);
  }
}
