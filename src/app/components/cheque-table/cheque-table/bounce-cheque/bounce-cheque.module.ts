import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BounceChequeService } from './bounce-cheque.service';
import { BounceChequeComponent } from './bounce-cheque/bounce-cheque.component';

import { BounceChequeCustomComponent } from './bounce-cheque/bounce-cheque-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BounceChequeComponent],
  providers: [BounceChequeService, BounceChequeCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BounceChequeComponent]
})
export class BounceChequeComponentModule {}
