import { AccessModeView } from './../../../../shared/models/viewModel/accessModeView';
import { Observable, of } from 'rxjs';
import { ChequeDocumentStatus, EmptyGuid, ReceivedFrom, RefType } from 'shared/constants';
import { isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, PathParamModel, SelectItems } from 'shared/models/systemModel';
import { ChequeTableItemView, CustomerTableItemView, DocumentStatusItemView } from 'shared/models/viewModel';
import { map, tap } from 'rxjs/operators';
import { ChequeTableService } from '../cheque-table.service';
import { SelectItem } from 'shared/models/primeModel';

const CONDITION3 = [
  'RECEIVED_FROM',
  'CUSTOMER_TABLE_GUID',
  'BUYER_TABLE_GUID',
  'CHEQUE_DATE',
  'CHEQUE_NO',
  'CHEQUE_BANK_ACC_NO',
  'CHEQUE_BANK_GROUP_GUID',
  'CHEQUE_BRANCH',
  'AMOUNT',
  'PDC',
  'ISSUED_NAME',
  'RECIPIENT_NAME',
  'EXPECTED_DEPOSIT_DATE',
  'REF_PDCGUID'
];
const CONDITION1 = [
  'RECEIVED_FROM',
  'CUSTOMER_TABLE_GUID',
  'BUYER_TABLE_GUID',
  'CHEQUE_DATE',
  'CHEQUE_NO',
  'CHEQUE_BANK_ACC_NO',
  'CHEQUE_BANK_GROUP_GUID',
  'CHEQUE_BRANCH',
  'AMOUNT',
  'PDC',
  'ISSUED_NAME',
  'RECIPIENT_NAME',
  'EXPECTED_DEPOSIT_DATE',
  'COMPLETED_DATE',
  'REF_PDCGUID',
  'REF_TYPE',
  'REF_ID',
  'REF_PDCGUID'
];
const CONDITION4 = ['REF_TYPE', 'REF_ID'];
const CONDITION5 = ['COMPLETED_DATE'];
const CONDITION6 = ['REF_TYPE', 'REF_ID', 'COMPLETED_DATE', 'CHEQUE_TABLE_GUID', 'REF_GUID'];
const DOCUMENTSTATUS = ['DOCUMENT_STATUS_GUID'];

const PURCHASE_TABLE_PURCHASE = 'purchasetablepurchase';
const WITHDRAWAL_TABLE = 'withdrawaltablewithdrawal';
const INQUIRY_WITHDRAWALLINEOUTSTAND = 'inquirywithdrawallineoutstand';

export class ChequeTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  isComplete: any;
  isBuyer: boolean = false;
  isCustomer: boolean;
  accessModeView: AccessModeView = new AccessModeView();
  parentName: string = null;
  getFieldAccessing(model: ChequeTableItemView): Observable<FieldAccessing[]> {
    this.isComplete = false;
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: DOCUMENTSTATUS, readonly: true });
    if (model.receivedFrom === ReceivedFrom.Buyer) {
      this.isBuyer = true;
      this.isCustomer = true;
    }
    if (model.receivedFrom === ReceivedFrom.Customer) {
      this.isBuyer = false;
      this.isCustomer = true;
    }
    if (!isNullOrUndefOrEmptyGUID(model.chequeTableGUID)) {
      fieldAccessing.push({ filedIds: CONDITION4, readonly: true });
      if (model.buyerTableGUID === null) {
        this.isBuyer = false;
        this.isCustomer = true;
      }
      if (
        model.documentStatus_StatusId === ChequeDocumentStatus.Created.toString() ||
        model.documentStatus_StatusId === ChequeDocumentStatus.Deposit.toString()
      ) {
        if (model.documentStatus_StatusId === ChequeDocumentStatus.Completed) {
          this.isComplete = true;
          fieldAccessing.push({ filedIds: CONDITION3, readonly: true });
          fieldAccessing.push({ filedIds: CONDITION5, readonly: false });
          return of(fieldAccessing);
        } else {
          this.isComplete = false;
          fieldAccessing.push({ filedIds: CONDITION3, readonly: false });
          fieldAccessing.push({ filedIds: CONDITION1, readonly: false });
          fieldAccessing.push({ filedIds: CONDITION4, readonly: true });
          fieldAccessing.push({ filedIds: CONDITION5, readonly: true });
          return of(fieldAccessing);
        }
      } else {
        fieldAccessing.push({ filedIds: CONDITION1, readonly: true });
        return of(fieldAccessing);
      }
    } else {
      fieldAccessing.push({ filedIds: CONDITION6, readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ChequeTableItemView, isWorkflowMode: boolean): Observable<boolean> {
    return this.setAccessModeByParent(model, isWorkflowMode).pipe(
      map((result) => (isUpdateMode(model.chequeTableGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  getInitialData(): Observable<ChequeTableItemView> {
    let model = new ChequeTableItemView();
    model.chequeTableGUID = EmptyGuid;
    model.refType = RefType.Cheque;
    return of(model);
  }
  onReciveFormChange(e: any, model: ChequeTableItemView): void {
    this.isBuyer = e === ReceivedFrom.Buyer ? true : false;
    model.customerTableGUID = null;
    model.buyerTableGUID = null;
  }
  onCustomerChange(model: ChequeTableItemView, customerTableOptions: SelectItems[]): any {
    if (!isNullOrUndefOrEmptyGUID(model.customerTableGUID)) {
      let customerTable: CustomerTableItemView = customerTableOptions.find((t) => t.value == model.customerTableGUID)
        .rowData as CustomerTableItemView;
      model.issuedName = customerTable.name;
    } else {
      model.issuedName = '';
    }
  }
  setAccessModeByParent(model: ChequeTableItemView, isWorkflowMode: boolean): Observable<AccessModeView> {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    const parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
    switch (this.parentName) {
      case PURCHASE_TABLE_PURCHASE:
        return this.getPurchaseTableAccessMode(parentId, isWorkflowMode);
      case WITHDRAWAL_TABLE:
        return this.getWithdrawalTableAccessMode(parentId);
      case INQUIRY_WITHDRAWALLINEOUTSTAND:
        this.accessModeView.canCreate = false;
        this.accessModeView.canView = false;
        return of(this.accessModeView);
      default:
        return of(this.getAccessModeByChequeTable());
    }
  }
  getPurchaseTableAccessMode(purchaseId: string, isWorkflowMode: boolean): Observable<AccessModeView> {
    return (this.baseService.service as ChequeTableService).getPurchaseTableAccessMode(purchaseId, isWorkflowMode).pipe(
      tap((result) => {
        this.accessModeView = result;
      })
    );
  }
  getWithdrawalTableAccessMode(withdrawalId: string): Observable<AccessModeView> {
    return (this.baseService.service as ChequeTableService).getWithdrawalTableAccessMode(withdrawalId).pipe(
      tap((result) => {
        this.accessModeView = result;
      })
    );
  }
  getAccessModeByChequeTable(): AccessModeView {
    this.accessModeView.canCreate = true;
    this.accessModeView.canView = true;

    return this.accessModeView;
  }
  isManegeDeposit(model: ChequeTableItemView): boolean {
    if (model.documentStatus_StatusId === ChequeDocumentStatus.Created || model.documentStatus_StatusId === ChequeDocumentStatus.Bounced) {
      return false;
    } else {
      return true;
    }
  }
  isManegeComplete(model: ChequeTableItemView): boolean {
    if (model.documentStatus_StatusId === ChequeDocumentStatus.Deposit.toString()) {
      return false;
    } else {
      return true;
    }
  }
  isManegeBounce(model: ChequeTableItemView): boolean {
    if (model.documentStatus_StatusId === ChequeDocumentStatus.Deposit.toString()) {
      return false;
    } else {
      return true;
    }
  }
  isManegeReplace(model: ChequeTableItemView): boolean {
    if (
      model.documentStatus_StatusId === ChequeDocumentStatus.Created.toString() ||
      model.documentStatus_StatusId === ChequeDocumentStatus.Bounced.toString()
    ) {
      return false;
    } else {
      return true;
    }
  }
  isManegeReturn(model: ChequeTableItemView): boolean {
    if (
      model.documentStatus_StatusId === ChequeDocumentStatus.Created.toString() ||
      model.documentStatus_StatusId === ChequeDocumentStatus.Bounced.toString()
    ) {
      return false;
    } else {
      return true;
    }
  }
  isManegeCancel(model: ChequeTableItemView): boolean {
    if (model.documentStatus_StatusId === ChequeDocumentStatus.Created.toString()) {
      return false;
    } else {
      return true;
    }
  }
  onPDCChange(e: any, documentStatusOptions: SelectItems[], model: ChequeTableItemView): void {
    const row = documentStatusOptions.find((o) => o.value === e).rowData as DocumentStatusItemView;
    model.documentStatus_StatusId = row.description;
    this.isComplete = row.description === ChequeDocumentStatus.Completed ? true : false;
  }
  getPDCChange(e: any, model: ChequeTableItemView, customerTableOptions: SelectItems[]): void {
    const row = customerTableOptions.find((o) => o.value === model.customerTableGUID).rowData as CustomerTableItemView;
    if (e) {
      model.issuedName = row.name;
    }
  }
  setDocumentStatusValidation(service: ChequeTableService, path: string, model: ChequeTableItemView): Observable<any> {
    return service.setDocumentStatusValidation(model).pipe(
      map((res) => {
        if (res) {
          return { ChequeTableItemView: res };
        } else {
          return null;
        }
      })
    );
  }
  setDeposittStatusValidation(service: ChequeTableService, path: string, model: ChequeTableItemView): Observable<any> {
    return service.setDeposittStatusValidation(model).pipe(
      map((res) => {
        if (res) {
          return { ChequeTableItemView: res };
        } else {
          return null;
        }
      })
    );
  }
  getBounceStatusValidation(service: ChequeTableService, path: string, model: ChequeTableItemView): Observable<any> {
    return service.getBounceStatusValidation(model).pipe(
      map((res) => {
        if (res) {
          return { ChequeTableItemView: res };
        } else {
          return null;
        }
      })
    );
  }
  getReplaceStatusValidation(service: ChequeTableService, path: string, model: ChequeTableItemView): Observable<any> {
    return service.getReplaceStatusValidation(model).pipe(
      map((res) => {
        if (res) {
          return { ChequeTableItemView: res };
        } else {
          return null;
        }
      })
    );
  }
  getReturnStatusValidation(service: ChequeTableService, path: string, model: ChequeTableItemView): Observable<any> {
    return service.getReturnStatusValidation(model).pipe(
      map((res) => {
        if (res) {
          return { ChequeTableItemView: res };
        } else {
          return null;
        }
      })
    );
  }
  getCancelStatusValidation(service: ChequeTableService, path: string, model: ChequeTableItemView): Observable<any> {
    return service.getCancelStatusValidation(model).pipe(
      map((res) => {
        if (res) {
          return { ChequeTableItemView: res };
        } else {
          return null;
        }
      })
    );
  }
}
