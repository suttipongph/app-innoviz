import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChequeTableItemComponent } from './cheque-table-item.component';

describe('ChequeTableItemViewComponent', () => {
  let component: ChequeTableItemComponent;
  let fixture: ComponentFixture<ChequeTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChequeTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChequeTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
