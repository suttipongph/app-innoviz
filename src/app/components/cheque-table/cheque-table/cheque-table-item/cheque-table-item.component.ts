import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { BuyerTableAuthorizedPersonTransModule } from 'pages/master-information/buyer-table/relatedinfo/authorized-person-trans/buyer-table-authorized-person-trans.module';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import {
  EmptyGuid,
  ROUTE_RELATED_GEN,
  ROUTE_FUNCTION_GEN,
  PurchaseStatus,
  ChequeDocumentStatus,
  AccessMode,
  DocumentProcessStatus,
  ROUTE_FUNCTIONS
} from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { ChequeTableItemView } from 'shared/models/viewModel';
import { ChequeTableService } from '../cheque-table.service';
import { ChequeTableItemCustomComponent } from './cheque-table-item-custom.component';
@Component({
  selector: 'cheque-table-item',
  templateUrl: './cheque-table-item.component.html',
  styleUrls: ['./cheque-table-item.component.scss']
})
export class ChequeTableItemComponent extends BaseItemComponent<ChequeTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  bankGroupOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  chequeTableOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  receivedFromOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  PDCOptions: SelectItems[] = [];

  constructor(private service: ChequeTableService, private currentActivatedRoute: ActivatedRoute, public custom: ChequeTableItemCustomComponent) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getReceivedFromEnumDropDown(this.receivedFromOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<ChequeTableItemView> {
    return this.service.getChequeTableById(this.id);
  }
  getInitialData(): Observable<ChequeTableItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      this.PDCOptions = null;
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    let tempModel = isNullOrUndefined(model) ? this.model : model;
    forkJoin([
      this.baseDropdown.getBankGroupDropDown(),
      this.baseDropdown.getBuyerTableDropDown(),
      this.baseDropdown.getCustomerTableDropDown(),
      this.baseDropdown.getDocumentStatusDropDown(),
      this.getChequeTableDropdownInit(tempModel)
    ]).subscribe(
      ([bankGroup, buyerTable, customerTable, documentStatus, pdc]) => {
        this.bankGroupOptions = bankGroup;
        this.buyerTableOptions = buyerTable;
        this.customerTableOptions = customerTable;
        this.documentStatusOptions = documentStatus;
        this.PDCOptions = pdc;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model, this.isWorkFlowMode()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  setFunctionOptions(model: ChequeTableItemView): void {
    const isManegeDeposit = this.custom.isManegeDeposit(model);
    const isManegeComplete = this.custom.isManegeComplete(model);
    const isManegeBounce = this.custom.isManegeBounce(model);
    const isManegeReplace = this.custom.isManegeReplace(model);
    const isManegeReturn = this.custom.isManegeReturn(model);
    const isManegeCancel = this.custom.isManegeCancel(model);
    this.functionItems = [
      {
        label: 'LABEL.MANAGE',
        items: [
          { label: 'LABEL.DEPOSIT', command: () => this.setDeposittStatusValidation(ROUTE_FUNCTION_GEN.DEPOSIT), disabled: isManegeDeposit },
          { label: 'LABEL.COMPLETE', command: () => this.setDocumentStatusValidation(ROUTE_FUNCTION_GEN.COMPLETE), disabled: isManegeComplete },
          { label: 'LABEL.BOUNCE', command: () => this.setBounceStatusValidation(ROUTE_FUNCTION_GEN.BOUNCE), disabled: isManegeBounce },
          { label: 'LABEL.REPLACE', command: () => this.setReplaceStatusValidation(ROUTE_FUNCTION_GEN.REPLACE), disabled: isManegeReplace },
          { label: 'LABEL.RETURN', command: () => this.setReturnStatusValidation(ROUTE_FUNCTION_GEN.CHEQUE_TABLE_RETURN), disabled: isManegeReturn },
          { label: 'LABEL.CANCEL', command: () => this.setCancelStatusValidation(ROUTE_FUNCTION_GEN.CHEQUE_TABLE_CANCEL), disabled: isManegeCancel }
        ]
      }
    ];
    super.setFunctionOptionsMapping();
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateChequeTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createChequeTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  getRowAuthorize(row: ChequeTableItemView[]): void {
    row.forEach((item) => {
      let accessMode: AccessMode = item.document_StatusId === ChequeDocumentStatus.Cancelled ? AccessMode.full : AccessMode.creator;
    });
  }
  onReciveFormChange(e: any): any {
    this.custom.onReciveFormChange(e, this.model);
  }
  onBuyerChange(): any {
    this.getChequeTableDropdown();
  }
  onCustomerChange(): any {
    this.custom.onCustomerChange(this.model, this.customerTableOptions);
    this.getChequeTableDropdown();
  }
  onReferanceType(): any {
    this.getChequeTableDropdown();
  }
  onPDCChange(e: any): any {
    this.custom.getPDCChange(e, this.model, this.customerTableOptions);
  }
  setDocumentStatusValidation(path: string): void {
    this.custom.setDocumentStatusValidation(this.service, path, this.model).subscribe((res) => {
      if (!isNullOrUndefined(res)) {
        this.toFunction({ path: path });
      }
    });
  }
  setDeposittStatusValidation(path: string): void {
    this.custom.setDeposittStatusValidation(this.service, path, this.model).subscribe((res) => {
      if (!isNullOrUndefined(res)) {
        this.toFunction({ path: path });
      }
    });
  }
  setBounceStatusValidation(path: string): void {
    this.custom.getBounceStatusValidation(this.service, path, this.model).subscribe((res) => {
      if (!isNullOrUndefined(res)) {
        this.toFunction({ path: path });
      }
    });
  }
  setReplaceStatusValidation(path: string): void {
    this.custom.getReplaceStatusValidation(this.service, path, this.model).subscribe((res) => {
      if (!isNullOrUndefined(res)) {
        this.toFunction({ path: path });
      }
    });
  }
  setReturnStatusValidation(path: string): void {
    this.custom.getReturnStatusValidation(this.service, path, this.model).subscribe((res) => {
      if (!isNullOrUndefined(res)) {
        this.toFunction({ path: path });
      }
    });
  }
  setCancelStatusValidation(path: string): void {
    this.custom.getCancelStatusValidation(this.service, path, this.model).subscribe((res) => {
      if (!isNullOrUndefined(res)) {
        this.toFunction({ path: path });
      }
    });
  }

  getChequeTableDropdownInit(model: ChequeTableItemView) {
    if (isNullOrUndefOrEmptyGUID(model.customerTableGUID) && isNullOrUndefOrEmptyGUID(model.buyerTableGUID)) {
      return this.baseDropdown.getChequeTableByCustomerBuyerReplacedDropDown([
        model.customerTableGUID,
        model.buyerTableGUID,
        model.refType.toString()
      ]);
    } else {
      return this.baseDropdown.getChequeTableByCustomerBuyerReplacedDropDown([
        model.customerTableGUID,
        model.buyerTableGUID,
        model.refType.toString()
      ]);
    }
  }

  getChequeTableDropdown(): void {
    if (isNullOrUndefOrEmptyGUID(this.model.customerTableGUID) && isNullOrUndefOrEmptyGUID(this.model.buyerTableGUID)) {
      this.baseDropdown
        .getChequeTableByCustomerBuyerReplacedDropDown([this.model.customerTableGUID, this.model.buyerTableGUID, this.model.refType.toString()])
        .subscribe((result) => {
          this.PDCOptions = result;
        });
    } else {
      this.baseDropdown
        .getChequeTableByCustomerBuyerReplacedDropDown([this.model.customerTableGUID, this.model.buyerTableGUID, this.model.refType.toString()])
        .subscribe((result) => {
          this.PDCOptions = result;
        });
    }
  }
}
