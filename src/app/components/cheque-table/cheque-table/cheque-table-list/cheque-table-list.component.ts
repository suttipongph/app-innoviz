import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { ChequeTableListView } from 'shared/models/viewModel';
import { ChequeTableService } from '../cheque-table.service';
import { ChequeTableListCustomComponent } from './cheque-table-list-custom.component';

@Component({
  selector: 'cheque-table-list',
  templateUrl: './cheque-table-list.component.html',
  styleUrls: ['./cheque-table-list.component.scss']
})
export class ChequeTableListComponent extends BaseListComponent<ChequeTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  bankGroupOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  chequeTableOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  defualtBitOption: SelectItems[] = [];

  parentName;
  id: string;

  constructor(public custom: ChequeTableListCustomComponent, private currentActivatedRoute: ActivatedRoute, private service: ChequeTableService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentName = this.uiService.getRelatedInfoActiveTableName();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.setAccessModeByParentStatus(this.id, this.isWorkFlowMode()).subscribe((result) => {
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getDefaultBitDropDown(this.defualtBitOption);
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreate(this.getCanCreate());
    this.option.canView = true;
    this.option.canDelete = this.custom.getCanDelete(this.getCanDelete());
    this.option.key = 'chequeTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        sortingKey: 'customerTable_CustomerId',
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        sortingKey: 'buyerTableGUID',
        searchingKey: 'buyerTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerTableOptions
      },
      {
        label: 'LABEL.CHEQUE_DATE',
        textKey: 'chequeDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CHEQUE_NO.',
        textKey: 'chequeNo',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.CHEQUE_BANK_ACCOUNT_NO.',
        textKey: 'chequeBankAccNo',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PDC',
        textKey: 'pdc',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defualtBitOption
      },
      {
        label: 'LABEL.AMOUNT',
        textKey: 'amount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.ISSUED_NAME',
        textKey: 'issuedName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.RECIPIENT_NAME',
        textKey: 'recipientName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        sortingKey: 'documentStatus_StatusId',
        searchingKey: 'documentStatusGUID',
        masterList: this.documentStatusOptions
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<ChequeTableListView>> {
    return this.service.getChequeTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteChequeTable(row));
  }
  getRowAuthorize(row: ChequeTableListView[]): void {
    if (this.custom.getRowAuthorize(row, this.parentName)) {
      row.forEach((item) => {
        item.rowAuthorize = this.getSingleRowAuthorize(item.rowAuthorize, item);
      });
    } else {
      super.getRowAuthorize(row);
    }
  }
}
