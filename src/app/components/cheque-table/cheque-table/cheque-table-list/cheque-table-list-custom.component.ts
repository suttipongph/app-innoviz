import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AccessMode, ColumnType, EmptyGuid, RefType, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { ChequeDocumentStatus } from 'shared/constants/enumsStatus';
import { BaseServiceModel, ColumnModel } from 'shared/models/systemModel';
import { AccessModeView, ChequeTableListView } from 'shared/models/viewModel';
import { ChequeTableService } from '../cheque-table.service';
const WITHDRAWAL_TABLE = 'withdrawaltablewithdrawal';
const PURCHASE_TABLE_PURCHASE = 'purchasetablepurchase';

export class ChequeTableListCustomComponent {
  baseService: BaseServiceModel<any>;
  allowUpdate: boolean;
  allowCreate: boolean;

  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  accessRight: boolean;
  model = new ChequeTableListView();
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setAccessModeByParentStatus(parentId: string, isWorkflowMode: boolean): Observable<AccessModeView> {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();    
    switch (this.parentName) {
      case PURCHASE_TABLE_PURCHASE:
        return this.getPurchaseTableAccessMode(parentId, isWorkflowMode);
      case WITHDRAWAL_TABLE:
        return this.getWithdrawalTableAccessMode(parentId);
      default:
        this.accessModeView.canCreate = true;
        this.accessModeView.canDelete = true;
        this.accessModeView.canView = true;
        return of(this.accessModeView);
    }
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
  getCanCreate(accessRight: boolean): any {
    switch (this.parentName) {
      case 'inquirypurchaselineoutstanding':
        return false;
      case 'inquirywithdrawallineoutstand':
        return false;
      case 'purchasetablepurchase':
          return this.accessModeView.canCreate && accessRight;
      default:
        return true;
    }
  }
  getCanDelete(accessRight: boolean): any {
    switch (this.parentName) {
      case 'inquirypurchaselineoutstanding':
        return false;
      case 'inquirywithdrawallineoutstand':
        return false;
      default:
        return true;
    }
  }
  getRowAuthorize(row: ChequeTableListView[], parentName: string): boolean {
    let isSetRow: boolean = true;
    if (parentName === ROUTE_RELATED_GEN.PURCHASE_TABLE_PURCHASE) {
      row.forEach((item) => {
        const accessMode: AccessMode = item.documentStatus_StatusId === ChequeDocumentStatus.Created && this.accessModeView.canDelete ? AccessMode.full : AccessMode.viewer;
        item.rowAuthorize = accessMode;
      });
    } else {
      row.forEach((item) => {
        const accessMode: AccessMode = item.documentStatus_StatusId === ChequeDocumentStatus.Created && this.accessModeView.canDelete ? AccessMode.full : AccessMode.viewer;
        item.rowAuthorize = accessMode;
      });
    }
    return isSetRow;
  }
  getPurchaseTableAccessMode(purchaseId: string, isWorkflowMode: boolean): Observable<AccessModeView> {
    return (this.baseService.service as ChequeTableService).getPurchaseTableAccessMode(purchaseId, isWorkflowMode).pipe(
            tap((result) => {        
        this.accessModeView = result;
        this.accessModeView.canView = true;
      })
    );
  }
  getWithdrawalTableAccessMode(withdrawalId: string): Observable<AccessModeView> {
    return (this.baseService.service as ChequeTableService).getWithdrawalTableAccessMode(withdrawalId).pipe(
      tap((result) => {
        this.accessModeView = result;
        this.accessModeView.canView = true;
      })
    );
  }
}
