import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ChequeTableListComponent } from './cheque-table-list.component';

describe('ChequeTableListViewComponent', () => {
  let component: ChequeTableListComponent;
  let fixture: ComponentFixture<ChequeTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChequeTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChequeTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
