import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CompleteChequeView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CompleteChequeService {
  serviceKey = 'completeChequeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCompleteChequeToList(search: SearchParameter): Observable<SearchResult<CompleteChequeView>> {
    const url = `${this.servicePath}/GetCompleteChequeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCompleteCheque(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCompleteCheque`;
    return this.dataGateway.delete(url, row);
  }
  getCompleteChequeById(id: string): Observable<CompleteChequeView> {
    const url = `${this.servicePath}/GetCompleteChequeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCompleteCheque(vmModel: CompleteChequeView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCompleteCheque`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCompleteCheque(vmModel: CompleteChequeView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCompleteCheque`;
    return this.dataGateway.post(url, vmModel);
  }
  getInitialData(id: string): Observable<CompleteChequeView> {
    const url = `${this.servicePath}/getInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
}
