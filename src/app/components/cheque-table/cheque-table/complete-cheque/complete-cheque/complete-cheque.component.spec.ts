import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompleteChequeComponent } from './complete-cheque.component';

describe('CompleteChequeViewComponent', () => {
  let component: CompleteChequeComponent;
  let fixture: ComponentFixture<CompleteChequeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CompleteChequeComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompleteChequeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
