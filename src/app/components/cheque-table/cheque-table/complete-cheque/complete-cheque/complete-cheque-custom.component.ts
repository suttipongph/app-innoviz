import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { dateTimetoString, datetoString } from 'shared/functions/date.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CompleteChequeView } from 'shared/models/viewModel';

const firstGroup = [
  'RECEIVED_FROM',
  'CUSTOMER_TABLE_GUID',
  'BUYER_TABLE_GUID',
  'DOCUMENT_STATUS_GUID',
  'REMARK',
  'CHEQUE_DATE',
  'CHEQUE_NO',
  'CHEQUE_BANK_ACC_NO',
  'CHEQUE_BANK_GROUP_GUID',
  'CHEQUE_BRANCH',
  'AMOUNT',
  'PDC',
  'ISSUED_NAME',
  'RECIPIENT_NAME'
];

export class CompleteChequeCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CompleteChequeView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });

    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<CompleteChequeView> {
    let model = new CompleteChequeView();
    model.completeChequeGUID = EmptyGuid;

    return of(model);
  }
  getDateComplete(model: CompleteChequeView) {
    model.completedDate = datetoString(new Date());
    return model;
  }
}
