import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { ChequeStatus } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode, setDisabledFunction } from 'shared/functions/value.function';
import { Confirmation } from 'shared/models/primeModel';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { CompleteChequeView } from 'shared/models/viewModel';
import { CompleteChequeService } from '../complete-cheque.service';
import { CompleteChequeCustomComponent } from './complete-cheque-custom.component';
@Component({
  selector: 'complete-cheque',
  templateUrl: './complete-cheque.component.html',
  styleUrls: ['./complete-cheque.component.scss']
})
export class CompleteChequeComponent extends BaseItemComponent<CompleteChequeView> implements BaseItemInterface {
  receivedFromOptions: SelectItems[] = [];
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  constructor(private service: CompleteChequeService, private currentActivatedRoute: ActivatedRoute, public custom: CompleteChequeCustomComponent) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialCreatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getReceivedFromEnumDropDown(this.receivedFromOptions);
  }
  getById(): Observable<CompleteChequeView> {
    return this.service.getCompleteChequeById(this.id);
  }
  getInitialData(): Observable<CompleteChequeView> {
    return this.service.getInitialData(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.custom.getDateComplete(result);
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });

    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    const confirmation: Confirmation = {
      header: this.baseService.translate.instant('CONFIRM.CONFIRM'),
      message: this.baseService.translate.instant('CONFIRM.90011', [
        this.baseService.translate.instant('LABEL.CHEQUE'),
        this.model.documentStatus_Values
      ])
    };
    this.baseService.notificationService.showConfirmDialog(confirmation);
    this.baseService.notificationService.isAccept.subscribe((isConfirm) => {
      if (isConfirm) {
        super.onExecuteFunction(this.service.updateCompleteCheque(this.model));
      }
    });
  }

  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
