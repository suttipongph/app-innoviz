import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CompleteChequeService } from './complete-cheque.service';
import { CompleteChequeComponent } from './complete-cheque/complete-cheque.component';
import { CompleteChequeCustomComponent } from './complete-cheque/complete-cheque-custom.component';


@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CompleteChequeComponent],
  providers: [CompleteChequeService, CompleteChequeCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CompleteChequeComponent]
})
export class CompleteChequeComponentModule {}
