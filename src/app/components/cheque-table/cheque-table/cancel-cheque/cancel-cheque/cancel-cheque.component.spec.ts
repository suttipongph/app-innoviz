import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelChequeComponent } from './cancel-cheque.component';

describe('CancelChequeViewComponent', () => {
  let component: CancelChequeComponent;
  let fixture: ComponentFixture<CancelChequeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CancelChequeComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelChequeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
