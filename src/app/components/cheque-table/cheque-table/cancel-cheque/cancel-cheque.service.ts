import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CancelChequeView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CancelChequeService {
  serviceKey = 'cancelChequeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCancelChequeToList(search: SearchParameter): Observable<SearchResult<CancelChequeView>> {
    const url = `${this.servicePath}/GetCancelChequeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCancelCheque(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCancelCheque`;
    return this.dataGateway.delete(url, row);
  }
  getCancelChequeById(id: string): Observable<CancelChequeView> {
    const url = `${this.servicePath}/GetCancelChequeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCancelCheque(vmModel: CancelChequeView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCancelCheque`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCancelCheque(vmModel: CancelChequeView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCancelCheque`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<CancelChequeView> {
    const url = `${this.servicePath}/getInitialDataCancelStatus/id=${id}`;
    return this.dataGateway.get(url);
  }
  updateCancelStatusCheque(vmModel: CancelChequeView): Observable<ResponseModel> {
    const url = `${this.servicePath}/updateCancelStatusCheque`;
    return this.dataGateway.post(url, vmModel);
  }
}
