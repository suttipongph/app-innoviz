import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CancelChequeService } from './cancel-cheque.service';
import { CancelChequeComponent } from './cancel-cheque/cancel-cheque.component';
import { CancelChequeCustomComponent } from './cancel-cheque/cancel-cheque-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CancelChequeComponent],
  providers: [CancelChequeService, CancelChequeCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CancelChequeComponent]
})
export class CancelChequeComponentModule {}
