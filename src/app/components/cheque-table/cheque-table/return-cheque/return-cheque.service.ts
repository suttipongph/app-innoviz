import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ReturnChequeView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ReturnChequeService {
  serviceKey = 'returnChequeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getReturnChequeToList(search: SearchParameter): Observable<SearchResult<ReturnChequeView>> {
    const url = `${this.servicePath}/GetReturnChequeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteReturnCheque(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteReturnCheque`;
    return this.dataGateway.delete(url, row);
  }
  getReturnChequeById(id: string): Observable<ReturnChequeView> {
    const url = `${this.servicePath}/GetReturnChequeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createReturnCheque(vmModel: ReturnChequeView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateReturnCheque`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateReturnCheque(vmModel: ReturnChequeView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateReturnCheque`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<ReturnChequeView> {
    const url = `${this.servicePath}/GetInitialDataReturnStatus/id=${id}`;
    return this.dataGateway.get(url);
  }
  updateReturnStatusCheque(vmModel: ReturnChequeView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateReturnStatusCheque`;
    return this.dataGateway.post(url, vmModel);
  }
}
