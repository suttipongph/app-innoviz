import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReturnChequeService } from './return-cheque.service';
import { ReturnChequeComponent } from './return-cheque/return-cheque.component';
import { ReturnChequeCustomComponent } from './return-cheque/return-cheque-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ReturnChequeComponent],
  providers: [ReturnChequeService, ReturnChequeCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ReturnChequeComponent]
})
export class ReturnChequeComponentModule {}
