import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { PostCustomerRefundView } from 'shared/models/viewModel/postCustomerRefundView';
@Injectable()
export class PostCustomerRefundService {
  serviceKey = 'customerRefundTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getPostCustomerRefundById(id: string): Observable<PostCustomerRefundView> {
    const url = `${this.servicePath}/GetPostCustomerRefundById/id=${id}`;
    return this.dataGateway.get(url);
  }
  postCustomerRefund(vmModel: PostCustomerRefundView): Observable<PostCustomerRefundView> {
    const url = `${this.servicePath}/PostCustomerRefund`;
    return this.dataGateway.post(url, vmModel);
  }
}
