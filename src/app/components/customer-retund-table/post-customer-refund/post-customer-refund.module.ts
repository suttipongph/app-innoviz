import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PostCustomerRefundService } from './post-customer-refund.service';
import { PostCustomerRefundComponent } from './post-customer-refund/post-customer-refund.component';
import { PostCustomerRefundCustomComponent } from './post-customer-refund/post-customer-refund-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PostCustomerRefundComponent],
  providers: [PostCustomerRefundService, PostCustomerRefundCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PostCustomerRefundComponent]
})
export class PostCustomerRefundComponentModule {}
