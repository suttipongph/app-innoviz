import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostCustomerRefundComponent } from './post-customer-refund.component';

describe('PostCustomerRefundViewComponent', () => {
  let component: PostCustomerRefundComponent;
  let fixture: ComponentFixture<PostCustomerRefundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PostCustomerRefundComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostCustomerRefundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
