import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { PostCustomerRefundView } from 'shared/models/viewModel/postCustomerRefundView';

const firstGroup = [
  'CUSTOMER_REFUND_ID',
  'DOCUMENT_STATUS',
  'CUSTOMER_ID',
  'TRANS_DATE',
  'PAYMENT_AMOUNT',
  'SUSPENSE_AMOUNT',
  'SERVICE_FEE_AMOUNT',
  'SETTLE_AMOUNT',
  'NET_AMOUNT',
  'RETENTION_AMOUNT'
];

export class PostCustomerRefundCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: PostCustomerRefundView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: PostCustomerRefundView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<PostCustomerRefundView> {
    let model = new PostCustomerRefundView();
    model.customerRefundTableGUID = EmptyGuid;
    return of(model);
  }
}
