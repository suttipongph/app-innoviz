import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { CustomerRefundTableItemView } from 'shared/models/viewModel/customerRefundTableItemView';
import { CustomerRefundTableListView } from 'shared/models/viewModel/customerRefundTableListView';
import { AccessModeView, RetentionConditionTransItemView } from 'shared/models/viewModel';
@Injectable()
export class CustomerRefundTableService {
  serviceKey = 'customerRefundTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCustomerRefundTableToList(search: SearchParameter): Observable<SearchResult<CustomerRefundTableListView>> {
    const url = `${this.servicePath}/GetCustomerRefundTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCustomerRefundTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCustomerRefundTable`;
    return this.dataGateway.delete(url, row);
  }
  getCustomerRefundTableById(id: string): Observable<CustomerRefundTableItemView> {
    const url = `${this.servicePath}/GetCustomerRefundTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCustomerRefundTable(vmModel: CustomerRefundTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCustomerRefundTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCustomerRefundTable(vmModel: CustomerRefundTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCustomerRefundTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  validateIsManualNumberSeq(companyId: string, suspenseInvoiceType: number): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeqCustomerRefundTable/companyId=${companyId}/suspenseInvoiceType=${suspenseInvoiceType}`;
    return this.dataGateway.get(url);
  }
  getInitialData(companyId: string, suspenseInvoiceType: number): Observable<any> {
    const url = `${this.servicePath}/GetCustomerRefundTableInitialData/companyId=${companyId}/suspenseInvoiceType=${suspenseInvoiceType}`;
    return this.dataGateway.get(url);
  }
  getExChangeRate(vmModel: CustomerRefundTableItemView): Observable<CustomerRefundTableItemView> {
    const url = `${this.servicePath}/GetExChangeRate`;
    return this.dataGateway.post(url, vmModel);
  }
  getCustomerRefundTableAccessMode(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetCustomerRefundTableAccessMode/id=${id}`;
    return this.dataGateway.get(url);
  }
  getRetentionConditionTransByCustomerRefund(id: string): Observable<RetentionConditionTransItemView> {
    const url = `${this.servicePath}/GetRetentionConditionTransByCustomerRefund/id=${id}`;
    return this.dataGateway.get(url);
  }
  validateIsNotInvoiceSettlementDetail(id: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsNotInvoiceSettlementDetail/id=${id}`;
    return this.dataGateway.get(url);
  }
}
