import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import {
  EmptyGuid,
  ROUTE_RELATED_GEN,
  ROUTE_FUNCTION_GEN,
  Dimension,
  RefType,
  SuspenseInvoiceType,
  ReceivedFrom,
  CustomerRefundStatus,
  ROUTE_REPORT_GEN
} from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, IvzDropdownOnFocusModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { LedgerDimensionItemView, RefIdParm } from 'shared/models/viewModel';
import { CustomerRefundTableItemView } from 'shared/models/viewModel/customerRefundTableItemView';
import { CustomerRefundTableService } from '../customer-refund-table.service';
import { CustomerRefundTableItemCustomComponent } from './customer-refund-table-item-custom.component';
@Component({
  selector: 'customer-refund-table-item',
  templateUrl: './customer-refund-table-item.component.html',
  styleUrls: ['./customer-refund-table-item.component.scss']
})
export class CustomerRefundTableItemComponent extends BaseItemComponent<CustomerRefundTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  creditAppTableOptions: SelectItems[] = [];
  currencyOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  retentionCalculateBaseOptions: SelectItems[] = [];
  suspenseInvoiceTypeOptions: SelectItems[] = [];
  ledgerDimensionOptions1: SelectItems[] = [];
  ledgerDimensionOptions2: SelectItems[] = [];
  ledgerDimensionOptions3: SelectItems[] = [];
  ledgerDimensionOptions4: SelectItems[] = [];
  ledgerDimensionOptions5: SelectItems[] = [];
  employeeTableOptions: SelectItems[] = [];

  constructor(
    private service: CustomerRefundTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CustomerRefundTableItemCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {
    this.custom.getSuspenseAccountBymasterRoute();
    this.custom.setTitle();
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRetentionCalculateBaseEnumDropDown(this.retentionCalculateBaseOptions);
    this.baseDropdown.getSuspenseInvoiceTypeEnumDropDown(this.suspenseInvoiceTypeOptions);
  }
  getById(): Observable<CustomerRefundTableItemView> {
    return this.service.getCustomerRefundTableById(this.id);
  }
  getInitialData(): Observable<CustomerRefundTableItemView> {
    return this.service.getInitialData(this.userDataService.getCurrentCompanyGUID(), this.custom.suspenseInvoiceType);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions(result);
        this.setFunctionOptions(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: CustomerRefundTableItemView): void {
    forkJoin(
      this.custom.getCreditAppTableByCustomerRefundTableDropDown(isNullOrUndefined(model) ? this.model : model),
      this.baseDropdown.getCurrencyDropDown(),
      this.baseDropdown.getCustomerTableDropDown(),
      this.baseDropdown.getDocumentStatusDropDown(),
      this.baseDropdown.getLedgerDimensionDropDown(),
      this.baseDropdown.getEmployeeFilterActiveStatusDropdown(model?.operReportSignatureGUID)
    ).subscribe(
      ([creditAppTable, currency, customerTable, documentStatus, ledgerDimension, employeeTableActive]) => {
        this.creditAppTableOptions = creditAppTable;
        this.currencyOptions = currency;
        this.customerTableOptions = customerTable;
        this.documentStatusOptions = documentStatus;
        this.employeeTableOptions = employeeTableActive;
        this.setLedgerDimension(ledgerDimension);

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
    this.custom.setRequireCreditApplicationId(this.model);
    this.custom.getVisibleFieldSet(this.model.suspenseInvoiceType).subscribe((res) => {
      super.setVisibilityFieldSet(res);
    });
  }

  setRelatedInfoOptions(model: CustomerRefundTableItemView): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        command: () => {
          const refIdParm: RefIdParm = { refType: RefType.CustomerRefund, refGUID: this.model.customerRefundTableGUID };
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: refIdParm
          });
        }
      },
      { label: 'LABEL.MEMO', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.MEMO }) },
      {
        label: 'LABEL.SERVICE_FEE',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.SERVICE_FEE_TRANS,
            parameters: {
              refId: model.customerRefundId,
              taxDate: model.transDate,
              productType: model.productType,
              invoiceTable_CustomerTableGUID: model.customerTableGUID
            }
          })
      },
      {
        label: 'LABEL.INVOICE_SETTLEMENT',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.INVOICE_SETTLEMENT,
            parameters: {
              refGUID: model.customerRefundTableGUID,
              refType: RefType.CustomerRefund,
              refId: model.customerRefundId,
              invoiceTable_CustomerTableGUID: model.customerTableGUID,
              suspenseInvoiceType: SuspenseInvoiceType.None,
              productInvoice: false,
              productType: null,
              receivedFrom: ReceivedFrom.Customer
            }
          })
      },
      {
        label: 'LABEL.PAYMENT_DETAIL',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.PAYMENT_DETAIL,
            parameters: {
              refId: model.customerRefundId,
              totalPaymentAmount: model.suspenseAmount - model.serviceFeeAmount - model.settleAmount - model.retentionAmount,
              customerTableGUID: model.customerTableGUID
            }
          })
      },
      // { label: 'LABEL.RECEIPT', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.RECEIPT }) },
      {
        label: 'LABEL.INQUIRY',
        items: [
          {
            label: 'LABEL.VENDOR_PAYMENT',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.VENDOR_PAYMENT_TRANS,
                parameters: {
                  refId: model.customerRefundId,
                  refGUID: model.customerRefundTableGUID,
                  refType: RefType.CustomerRefund,
                  tableLabel: this.custom.title
                }
              })
          }
        ]
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(model: CustomerRefundTableItemView): void {
    const getretentionrefundvisible = this.custom.getRetentionRefundVisible(model);
    const getretentionrefunddisible = this.custom.getRetentionRefundDisable(model);
    const getreserverefundvisible = this.custom.getReserveRefundVisible(model);
    const getreserverefunddisible = this.custom.getReserveRefundDisible(model);
    this.functionItems = [
      {
        label: 'LABEL.POST',
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.POST_CUSTOMER_REFUND }),
        disabled: model.documentStatus_StatusId !== CustomerRefundStatus.Draft
      },
      {
        label: 'LABEL.CANCEL',
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.CANCEL_CUSTOMER_REFUND }),
        disabled: model.documentStatus_StatusId !== CustomerRefundStatus.Draft
      },
      {
        label: 'LABEL.PRINT',
        items: [
          {
            label: 'LABEL.RESERVE_REFUND',
            command: () =>
              this.toReport({
                path: ROUTE_REPORT_GEN.PRINT_CUSTOMER_REFUND_RESERVE
              }),
            disabled: getreserverefunddisible,
            visible: getreserverefundvisible
          },
          {
            label: 'LABEL.RETENTION_REFUND',
            disabled: getretentionrefunddisible,
            visible: getretentionrefundvisible,
            command: () =>
              this.toReport({
                path: ROUTE_REPORT_GEN.PRINT_CUSTOMER_REFUND_RETENTION
              })
          }
        ]
      }
    ];
    super.setFunctionOptionsMapping();
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateCustomerRefundTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createCustomerRefundTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  setLedgerDimension(ledgerDimension: SelectItems[]): void {
    this.ledgerDimensionOptions1 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension1);
    this.ledgerDimensionOptions2 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension2);
    this.ledgerDimensionOptions3 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension3);
    this.ledgerDimensionOptions4 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension4);
    this.ledgerDimensionOptions5 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension5);
  }
  onCurrencyChange() {
    this.custom.setExchangeRateByCurrency(this.model);
  }
  onRetentionAmountChange() {
    this.custom.setRequireCreditApplicationId(this.model);
  }
  onCustomerChange() {
    this.custom.clearCustomerValue(this.model);
    this.creditAppTableOptions.length = 0;
    if (!isNullOrUndefOrEmptyGUID(this.model.customerTableGUID)) {
      this.custom.setCreditAppDropDown(this.model).subscribe((result) => {
        this.creditAppTableOptions = result;
      });
    }
  }
  onCreditAppChange() {
    this.custom.clearCreditAppValue(this.model);
    if (!isNullOrUndefOrEmptyGUID(this.model.creditAppTableGUID)) {
      this.custom.setCreditAppValue(this.model, this.creditAppTableOptions);
    }
  }
  onEmployeeTableDropdownFocus(e: IvzDropdownOnFocusModel): void {
    super.setDropdownOptionOnFocus(e, this.model, this.baseDropdown.getEmployeeFilterActiveStatusDropdown()).subscribe((result) => {
      this.employeeTableOptions = result;
    });
  }
  onSuspenseSettlementDetailDeleted(e) {
    this.toReload();
  }
}
