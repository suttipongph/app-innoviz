import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerRefundTableItemComponent } from './customer-refund-table-item.component';

describe('CustomerRefundTableItemViewComponent', () => {
  let component: CustomerRefundTableItemComponent;
  let fixture: ComponentFixture<CustomerRefundTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustomerRefundTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerRefundTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
