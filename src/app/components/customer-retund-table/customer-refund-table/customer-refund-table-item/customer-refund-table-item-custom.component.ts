import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { CustomerRefundStatus, EmptyGuid, ProductType, RetentionCalculateBase, ROUTE_MASTER_GEN, SuspenseInvoiceType } from 'shared/constants';
import { Guid, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, CreditAppTableItemView } from 'shared/models/viewModel';
import { CustomerRefundTableItemView } from 'shared/models/viewModel/customerRefundTableItemView';
import { RetentionConditionTransItemView } from 'shared/models/viewModel/retentionConditionTransItemView';
const RESERVE_REFUND = 'reserverefund';
const RETENTION_REFUND = 'retentionrefund';
const SUSPENSE_REFUND = 'suspenserefund';

const firstGroup = [
  'PRODUCT_TYPE',
  'SUSPENSE_INVOICE_TYPE',
  'DOCUMENT_STATUS_GUID',
  'DOCUMENT_REASON_GUID',
  'PAYMENT_AMOUNT',
  'SUSPENSE_AMOUNT',
  'SETTLED_FEE_AMOUNT',
  'SETTLE_AMOUNT',
  'NET_AMOUNT',
  'EXCHANGE_RATE',
  'RETENTION_CALCULATE_BASE',
  'RETENTION_PCT',
  'CUSTOMER_REFUND_TABLE_GUID'
];
const secondGroup = ['CUSTOMER_REFUND_ID'];
const thirdGroup = ['CREDIT_APP_TABLE_GUID'];
const forthGroup = ['CUSTOMER_TABLE_GUID'];

export class CustomerRefundTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  title: string = null;
  suspenseInvoiceType: SuspenseInvoiceType = null;
  isManual = false;
  isNumbersSeqNotSet = false;
  isRequiredCon1: boolean = false;
  isRequiredCon2: boolean = false;
  accessModeView: AccessModeView = new AccessModeView();
  getFieldAccessing(model: CustomerRefundTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    if (!isUpdateMode(model.customerRefundTableGUID)) {
      if (model.suspenseInvoiceType == SuspenseInvoiceType.ToBeRefunded) {
        fieldAccessing.push({ filedIds: thirdGroup, readonly: false });
      } else {
        fieldAccessing.push({ filedIds: thirdGroup, readonly: true });
      }
      fieldAccessing.push({ filedIds: forthGroup, readonly: false });
      return new Observable((observer) => {
        this.validateIsManualNumberSeq(model).subscribe((result) => {
          this.isManual = result;
          this.isRequiredCon1 = result;
          fieldAccessing.push({ filedIds: secondGroup, readonly: !this.isManual });
          observer.next(fieldAccessing);
        });
      });
    } else {
      this.isRequiredCon1 = false;
      fieldAccessing.push({ filedIds: secondGroup, readonly: true });
      fieldAccessing.push({ filedIds: forthGroup, readonly: true });
      if (model.suspenseInvoiceType == SuspenseInvoiceType.ToBeRefunded) {
        return new Observable((observer) => {
          this.validateIsNotInvoiceSettlementDetail(model).subscribe((result) => {
            fieldAccessing.push({ filedIds: thirdGroup, readonly: !result });
            observer.next(fieldAccessing);
          });
        });
      } else {
        fieldAccessing.push({ filedIds: thirdGroup, readonly: true });
        return of(fieldAccessing);
      }
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CustomerRefundTableItemView): Observable<boolean> {
    const accessright = isNullOrUndefOrEmptyGUID(model.customerRefundTableGUID) ? canCreate : canUpdate;
    const accessLogic = isNullOrUndefOrEmptyGUID(model.customerRefundTableGUID) ? true : model.documentStatus_StatusId == CustomerRefundStatus.Draft;
    return of(!(accessLogic && accessright));
  }
  getInitialData(): Observable<CustomerRefundTableItemView> {
    let model = new CustomerRefundTableItemView();
    model.customerRefundTableGUID = EmptyGuid;
    return of(model);
  }
  getSuspenseAccountBymasterRoute() {
    const masterRoute = this.baseService.uiService.getMasterRoute(2);
    switch (masterRoute) {
      case SUSPENSE_REFUND:
        this.suspenseInvoiceType = SuspenseInvoiceType.SuspenseAccount;
        break;
      case RESERVE_REFUND:
        this.suspenseInvoiceType = SuspenseInvoiceType.ToBeRefunded;
        break;
      case RETENTION_REFUND:
        this.suspenseInvoiceType = SuspenseInvoiceType.RetentionReturn;
        break;
    }
  }
  setTitle() {
    switch (this.suspenseInvoiceType) {
      case SuspenseInvoiceType.SuspenseAccount:
        this.title = 'LABEL.SUSPENSE_REFUND';
        break;
      case SuspenseInvoiceType.ToBeRefunded:
        this.title = 'LABEL.RESERVE_REFUND';
        break;
      case SuspenseInvoiceType.RetentionReturn:
        this.title = 'LABEL.RETENTION_REFUND';
        break;
    }
  }
  getVisibleFieldSet(suspenseInvoiceType: number): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: ['GENERAL', 'AMOUNT', 'FINANCIAL_DIMENSIONS'], invisible: false });
    fieldAccessing.push({ filedIds: ['RETENTION'], invisible: true });
    switch (suspenseInvoiceType) {
      case SuspenseInvoiceType.ToBeRefunded:
        fieldAccessing.push({ filedIds: ['RETENTION'], invisible: false });
        break;
    }
    return of(fieldAccessing);
  }
  setExchangeRateByCurrency(model: CustomerRefundTableItemView) {
    if (!isNullOrUndefOrEmptyGUID(model.currencyGUID)) {
      this.baseService.service.getExChangeRate(model).subscribe(
        (result) => {
          model.exchangeRate = result.exchangeRate;
        },
        (error) => {
          this.baseService.notificationService.showErrorMessageFromResponse(error);
          model.exchangeRate = 0;
        }
      );
    } else {
      model.exchangeRate = 0;
    }
  }
  setRequireCreditApplicationId(model: CustomerRefundTableItemView) {
    if (model.suspenseInvoiceType == SuspenseInvoiceType.ToBeRefunded && model.productType == ProductType.Factoring && model.retentionAmount > 0) {
      this.isRequiredCon2 = true;
    } else {
      this.isRequiredCon2 = false;
    }
  }
  validateIsManualNumberSeq(model: CustomerRefundTableItemView): Observable<boolean> {
    return this.baseService.service.validateIsManualNumberSeq(model.companyGUID, model.suspenseInvoiceType).pipe(
      map((res) => res),
      catchError((e) => {
        this.baseService.notificationService.showErrorMessageFromResponse(e);
        return e;
      })
    );
  }
  validateIsNotInvoiceSettlementDetail(model: CustomerRefundTableItemView): Observable<boolean> {
    return this.baseService.service.validateIsNotInvoiceSettlementDetail(model.customerRefundTableGUID).pipe(
      map((res) => res),
      catchError((e) => {
        this.baseService.notificationService.showErrorMessageFromResponse(e);
        return e;
      })
    );
  }
  setCreditAppDropDown(model: CustomerRefundTableItemView): Observable<SelectItems[]> {
    if (!isNullOrUndefOrEmptyGUID(model.customerTableGUID)) {
      return this.baseService.baseDropdown.getCreditAppTableByCustomerRefundTableDropDown([model.customerTableGUID, model.productType.toString()]);
    } else {
      return of([]);
    }
  }
  setCreditAppValue(model: CustomerRefundTableItemView, option: SelectItems[]) {
    let creditAppTable: CreditAppTableItemView = option.find(
      (t) => (t.rowData as CreditAppTableItemView).creditAppTableGUID === model.creditAppTableGUID
    ).rowData as CreditAppTableItemView;
    model.dimension1GUID = creditAppTable.dimension1GUID;
    model.dimension2GUID = creditAppTable.dimension2GUID;
    model.dimension3GUID = creditAppTable.dimension3GUID;
    model.dimension4GUID = creditAppTable.dimension4GUID;
    model.dimension5GUID = creditAppTable.dimension5GUID;
    this.baseService.service.getRetentionConditionTransByCustomerRefund(model.creditAppTableGUID).subscribe(
      (result) => {
        let item: RetentionConditionTransItemView = result;
        if (item != null) {
          model.retentionCalculateBase = item.retentionCalculateBase;
          model.retentionPct = item.retentionPct;
          if (item.retentionCalculateBase == RetentionCalculateBase.FixedAmount) {
            model.retentionAmount = item.retentionAmount;
          } else if (item.retentionCalculateBase == RetentionCalculateBase.NetReserveAmount) {
            model.retentionAmount = model.netAmount * (item.retentionPct / 100);
          } else {
            model.retentionAmount = 0;
          }
        } else {
          model.retentionCalculateBase = 0;
          model.retentionPct = 0;
          model.retentionAmount = 0;
        }
      },
      (error) => {
        this.baseService.notificationService.showErrorMessageFromResponse(error);
        model.retentionCalculateBase = 0;
        model.retentionPct = 0;
        model.retentionAmount = 0;
      }
    );
  }
  clearCustomerValue(model: CustomerRefundTableItemView) {
    model.creditAppTableGUID = null;
    this.clearCreditAppValue(model);
  }
  clearCreditAppValue(model: CustomerRefundTableItemView) {
    model.retentionCalculateBase = 0;
    model.retentionPct = 0;
    model.retentionAmount = 0;
    model.dimension1GUID = null;
    model.dimension2GUID = null;
    model.dimension3GUID = null;
    model.dimension4GUID = null;
    model.dimension5GUID = null;
  }
  getCreditAppTableByCustomerRefundTableDropDown(model: CustomerRefundTableItemView): Observable<SelectItems[]> {
    let customerTableGUID = model.customerTableGUID != null ? model.customerTableGUID : EmptyGuid;
    return this.baseService.baseDropdown.getCreditAppTableByCustomerRefundTableDropDown([customerTableGUID, model.productType.toString()]);
  }

  getRetentionRefundVisible(model: CustomerRefundTableItemView): boolean {
    if (model.suspenseInvoiceType === SuspenseInvoiceType.RetentionReturn) {
      return true;
    } else {
      return false;
    }
  }
  getReserveRefundVisible(model: CustomerRefundTableItemView): boolean {
    if (model.suspenseInvoiceType === SuspenseInvoiceType.ToBeRefunded) {
      return true;
    } else {
      return false;
    }
  }
  getRetentionRefundDisable(model: CustomerRefundTableItemView): boolean {
    if (model.suspenseInvoiceType === SuspenseInvoiceType.RetentionReturn && model.documentStatus_StatusId !== CustomerRefundStatus.Cancelled) {
      return false;
    } else {
      return true;
    }
  }
  getReserveRefundDisible(model: CustomerRefundTableItemView): boolean {
    if (model.suspenseInvoiceType === SuspenseInvoiceType.ToBeRefunded && model.documentStatus_StatusId !== CustomerRefundStatus.Cancelled) {
      return false;
    } else {
      return true;
    }
  }
}
