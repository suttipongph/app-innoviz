import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomerRefundTableService } from './customer-refund-table.service';
import { CustomerRefundTableListComponent } from './customer-refund-table-list/customer-refund-table-list.component';
import { CustomerRefundTableItemComponent } from './customer-refund-table-item/customer-refund-table-item.component';
import { CustomerRefundTableItemCustomComponent } from './customer-refund-table-item/customer-refund-table-item-custom.component';
import { CustomerRefundTableListCustomComponent } from './customer-refund-table-list/customer-refund-table-list-custom.component';
import { InvoiceSettlementDetailComponentModule } from 'components/invoice-settlement-detail/invoice-settlement-detail/invoice-settlement-detail.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, InvoiceSettlementDetailComponentModule],
  declarations: [CustomerRefundTableListComponent, CustomerRefundTableItemComponent],
  providers: [CustomerRefundTableService, CustomerRefundTableItemCustomComponent, CustomerRefundTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CustomerRefundTableListComponent, CustomerRefundTableItemComponent]
})
export class CustomerRefundTableComponentModule {}
