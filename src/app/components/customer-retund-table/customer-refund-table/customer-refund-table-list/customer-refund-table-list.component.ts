import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { BracketType, ColumnType, Operators, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CustomerRefundTableListView } from 'shared/models/viewModel/customerRefundTableListView';
import { CustomerRefundTableService } from '../customer-refund-table.service';
import { CustomerRefundTableListCustomComponent } from './customer-refund-table-list-custom.component';

@Component({
  selector: 'customer-refund-table-list',
  templateUrl: './customer-refund-table-list.component.html',
  styleUrls: ['./customer-refund-table-list.component.scss']
})
export class CustomerRefundTableListComponent extends BaseListComponent<CustomerRefundTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  creditAppTableOptions: SelectItems[] = [];
  currencyOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  ledgerDimensionOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  retentionCalculateBaseOptions: SelectItems[] = [];
  suspenseInvoiceTypeOptions: SelectItems[] = [];

  constructor(public custom: CustomerRefundTableListCustomComponent, private service: CustomerRefundTableService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {
    this.custom.getSuspenseAccountBymasterRoute();
    this.custom.setTitle();
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRetentionCalculateBaseEnumDropDown(this.retentionCalculateBaseOptions);
    this.baseDropdown.getSuspenseInvoiceTypeEnumDropDown(this.suspenseInvoiceTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCurrencyDropDown(this.currencyOptions);
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'customerRefundTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.CUSTOMER_REFUND_ID',
        textKey: 'customerRefundId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.TRANSACTION_DATE',
        textKey: 'transDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentStatusOptions,
        sortingKey: 'documentStatus_StatusId',
        searchingKey: 'documentStatusGUID'
      },
      {
        label: 'LABEL.PAYMENT_AMOUNT',
        textKey: 'paymentAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.SUSPENSE_AMOUNT',
        textKey: 'suspenseAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.NET_AMOUNT',
        textKey: 'netAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        disabledFilter: true
      },
      {
        label: 'LABEL.CURRENCY_ID',
        textKey: 'currency_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.currencyOptions,
        searchingKey: 'currencyGUID',
        sortingKey: 'currency_CurrencyId'
      },
      {
        label: null,
        textKey: 'suspenseInvoiceType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.custom.suspenseInvoiceType.toString()
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CustomerRefundTableListView>> {
    return this.service.getCustomerRefundTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCustomerRefundTable(row));
  }
}
