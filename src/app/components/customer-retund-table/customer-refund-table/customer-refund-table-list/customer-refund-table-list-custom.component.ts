import { Observable } from 'rxjs';
import { ROUTE_MASTER_GEN, SuspenseInvoiceType } from 'shared/constants';
import { BaseServiceModel } from 'shared/models/systemModel';
import { CustomerRefundTableService } from '../customer-refund-table.service';
const RESERVE_REFUND = 'reserverefund';
const RETENTION_REFUND = 'retentionrefund';
const SUSPENSE_REFUND = 'suspenserefund';

export class CustomerRefundTableListCustomComponent {
  baseService: BaseServiceModel<CustomerRefundTableService>;
  title: string = null;
  suspenseInvoiceType: SuspenseInvoiceType = null;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  getSuspenseAccountBymasterRoute() {
    const masterRoute = this.baseService.uiService.getMasterRoute(2);
    switch (masterRoute) {
      case SUSPENSE_REFUND:
        this.suspenseInvoiceType = SuspenseInvoiceType.SuspenseAccount;
        break;
      case RESERVE_REFUND:
        this.suspenseInvoiceType = SuspenseInvoiceType.ToBeRefunded;
        break;
      case RETENTION_REFUND:
        this.suspenseInvoiceType = SuspenseInvoiceType.RetentionReturn;
        break;
    }
  }
  setTitle() {
    const masterRoute = this.baseService.uiService.getMasterRoute(2);
    switch (this.suspenseInvoiceType) {
      case SuspenseInvoiceType.SuspenseAccount:
        this.title = 'LABEL.SUSPENSE_REFUND';
        break;
      case SuspenseInvoiceType.ToBeRefunded:
        this.title = 'LABEL.RESERVE_REFUND';
        break;
      case SuspenseInvoiceType.RetentionReturn:
        this.title = 'LABEL.RETENTION_REFUND';
        break;
    }
  }
}
