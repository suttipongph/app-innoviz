import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CustomerRefundTableListComponent } from './customer-refund-table-list.component';

describe('CustomerRefundTableListViewComponent', () => {
  let component: CustomerRefundTableListComponent;
  let fixture: ComponentFixture<CustomerRefundTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustomerRefundTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerRefundTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
