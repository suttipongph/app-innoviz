import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrintCustomerRefundReserveService } from './print-customer-refund-reserve.service';
import { PrintCustomerRefundReserveReportCustomComponent } from './print-customer-refund-reserve/print-customer-refund-reserve-report-custom.component';
import { PrintCustomerRefundReserveReportComponent } from './print-customer-refund-reserve/print-customer-refund-reserve-report.component';
@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PrintCustomerRefundReserveReportComponent],
  providers: [PrintCustomerRefundReserveService, PrintCustomerRefundReserveReportCustomComponent, PrintCustomerRefundReserveReportComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PrintCustomerRefundReserveReportComponent]
})
export class PrintCustomerRefundReserveComponentModule {}
