import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintCustomerRefundReserveReportComponent } from './print-customer-refund-reserve-report.component';

describe('PrintCustomerRefundReserveReportViewComponent', () => {
  let component: PrintCustomerRefundReserveReportComponent;
  let fixture: ComponentFixture<PrintCustomerRefundReserveReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrintCustomerRefundReserveReportComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintCustomerRefundReserveReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
