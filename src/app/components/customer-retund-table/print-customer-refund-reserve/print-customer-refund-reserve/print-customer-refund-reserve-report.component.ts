import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseReportComponent } from 'core/components/base-report/base-report.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { forkJoin, Observable, of } from 'rxjs';
import { REPORT_NAME } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel } from 'shared/models/systemModel';
import { PrintCustomerRefundReserveReportView } from 'shared/models/viewModel/rptPrintCustomerRefundReserveReportView';
import { PrintCustomerRefundReserveService } from '../print-customer-refund-reserve.service';
import { PrintCustomerRefundReserveReportCustomComponent } from './print-customer-refund-reserve-report-custom.component';

@Component({
  selector: 'print-customer-refund-reserve-report',
  templateUrl: './print-customer-refund-reserve-report.component.html',
  styleUrls: ['./print-customer-refund-reserve-report.component.scss']
})
export class PrintCustomerRefundReserveReportComponent
  extends BaseReportComponent<PrintCustomerRefundReserveReportView>
  implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  constructor(
    private service: PrintCustomerRefundReserveService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: PrintCustomerRefundReserveReportCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.reportName = REPORT_NAME.D106_CUSTOMERREFUND_RESERVE;
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<PrintCustomerRefundReserveReportView> {
    return this.service.getPrintCustomerRefundReserveById(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {}

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  onSave(validation: FormValidationModel): void {}
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true, this.model);
      }
    });
  }
  onSubmit(isColsing: boolean, model: PrintCustomerRefundReserveReportView): void {
    super.submitReportParameters(model);
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
