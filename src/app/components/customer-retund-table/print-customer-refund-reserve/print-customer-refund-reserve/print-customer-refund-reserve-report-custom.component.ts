import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { PrintCustomerRefundReserveReportView } from 'shared/models/viewModel';

const firstGroup = [
  'CUSTOMER_REFUND_ID',
  'DOCUMENT_STATUS',
  'CUSTOMER_ID',
  'TRANS_DATE',
  'PAYMENT_AMOUNT',
  'SUSPENSE_AMOUNT',
  'SERVICE_FEE_AMOUNT',
  'SETTLE_AMOUNT',
  'NET_AMOUNT',
  'RETENTION_AMOUNT'
];

export class PrintCustomerRefundReserveReportCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: PrintCustomerRefundReserveReportView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<PrintCustomerRefundReserveReportView> {
    let model = new PrintCustomerRefundReserveReportView();
    model.printCustomerRefundReserveGUID = EmptyGuid;
    return of(model);
  }
}
