import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PrintCustomerRefundReserveReportView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class PrintCustomerRefundReserveService {
  serviceKey = 'printCustomerRefundReserveGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getPrintCustomerRefundReserveToList(search: SearchParameter): Observable<SearchResult<PrintCustomerRefundReserveReportView>> {
    const url = `${this.servicePath}/GetPrintCustomerRefundReserveList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deletePrintCustomerRefundReserve(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeletePrintCustomerRefundReserve`;
    return this.dataGateway.delete(url, row);
  }
  getPrintCustomerRefundReserveById(id: string): Observable<PrintCustomerRefundReserveReportView> {
    const url = `${this.servicePath}/GetPrintCustomerRefundReserveById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createPrintCustomerRefundReserve(vmModel: PrintCustomerRefundReserveReportView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreatePrintCustomerRefundReserve`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updatePrintCustomerRefundReserve(vmModel: PrintCustomerRefundReserveReportView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdatePrintCustomerRefundReserve`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
