import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { CancelCustomerRefundView } from 'shared/models/viewModel/cancelCustomerRefundView';
@Injectable()
export class CancelCustomerRefundService {
  serviceKey = 'cancelCustomerRefundGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCancelCustomerRefundById(id: string): Observable<CancelCustomerRefundView> {
    const url = `${this.servicePath}/GetCancelCustomerRefundById/id=${id}`;
    return this.dataGateway.get(url);
  }
  cancelCustomerRefund(vmModel: CancelCustomerRefundView): Observable<CancelCustomerRefundView> {
    const url = `${this.servicePath}/CancelCustomerRefund`;
    return this.dataGateway.post(url, vmModel);
  }
}
