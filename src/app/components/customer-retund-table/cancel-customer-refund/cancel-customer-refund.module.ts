import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CancelCustomerRefundService } from './cancel-customer-refund.service';
import { CancelCustomerRefundComponent } from './cancel-customer-refund/cancel-customer-refund.component';
import { CancelCustomerRefundCustomComponent } from './cancel-customer-refund/cancel-customer-refund-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CancelCustomerRefundComponent],
  providers: [CancelCustomerRefundService, CancelCustomerRefundCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CancelCustomerRefundComponent]
})
export class CancelCustomerRefundComponentModule {}
