import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CancelCustomerRefundView } from 'shared/models/viewModel/cancelCustomerRefundView';

const firstGroup = [
  'CUSTOMER_REFUND_ID',
  'DOCUMENT_STATUS',
  'CUSTOMER_ID',
  'TRANS_DATE',
  'PAYMENT_AMOUNT',
  'SUSPENSE_AMOUNT',
  'SERVICE_FEE_AMOUNT',
  'SETTLE_AMOUNT',
  'NET_AMOUNT',
  'RETENTION_AMOUNT'
];

export class CancelCustomerRefundCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CancelCustomerRefundView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CancelCustomerRefundView): Observable<boolean> {
    return of(false);
  }
  getInitialData(): Observable<CancelCustomerRefundView> {
    let model = new CancelCustomerRefundView();
    model.customerRefundTableGUID = EmptyGuid;
    return of(model);
  }
}
