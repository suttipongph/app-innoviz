import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelCustomerRefundComponent } from './cancel-customer-refund.component';

describe('CancelCustomerRefundViewComponent', () => {
  let component: CancelCustomerRefundComponent;
  let fixture: ComponentFixture<CancelCustomerRefundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CancelCustomerRefundComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelCustomerRefundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
