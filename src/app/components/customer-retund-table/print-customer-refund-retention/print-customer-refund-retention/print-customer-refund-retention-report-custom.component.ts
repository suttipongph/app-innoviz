import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { PrintCustomerRefundRetentionReportView } from 'shared/models/viewModel';

const firstGroup = [
  'CUSTOMER_REFUND_ID',
  'DOCUMENT_STATUS',
  'CUSTOMER_ID',
  'TRANS_DATE',
  'PAYMENT_AMOUNT',
  'SUSPENSE_AMOUNT',
  'SERVICE_FEE_AMOUNT',
  'SETTLE_AMOUNT',
  'NET_AMOUNT',
  'RETENTION_AMOUNT'
];

export class PrintCustomerRefundRetentionReportCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: PrintCustomerRefundRetentionReportView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<PrintCustomerRefundRetentionReportView> {
    let model = new PrintCustomerRefundRetentionReportView();
    model.printCustomerRefundRetentionGUID = EmptyGuid;
    return of(model);
  }
}
