import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintCustomerRefundRetentionReportComponent } from './print-customer-refund-retention-report.component';

describe('PrintCustomerRefundRetentionReportViewComponent', () => {
  let component: PrintCustomerRefundRetentionReportComponent;
  let fixture: ComponentFixture<PrintCustomerRefundRetentionReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrintCustomerRefundRetentionReportComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintCustomerRefundRetentionReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
