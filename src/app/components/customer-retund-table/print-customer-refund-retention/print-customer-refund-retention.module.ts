import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrintCustomerRefundRetentionService } from './print-customer-refund-retention.service';
import { PrintCustomerRefundRetentionReportCustomComponent } from './print-customer-refund-retention/print-customer-refund-retention-report-custom.component';
import { PrintCustomerRefundRetentionReportComponent } from './print-customer-refund-retention/print-customer-refund-retention-report.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PrintCustomerRefundRetentionReportComponent],
  providers: [PrintCustomerRefundRetentionService, PrintCustomerRefundRetentionReportComponent, PrintCustomerRefundRetentionReportCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PrintCustomerRefundRetentionReportComponent]
})
export class PrintCustomerRefundRetentionComponentModule {}
