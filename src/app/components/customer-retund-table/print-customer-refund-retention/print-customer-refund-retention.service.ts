import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PrintCustomerRefundRetentionReportView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class PrintCustomerRefundRetentionService {
  serviceKey = 'printCustomerRefundRetentionGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getPrintCustomerRefundRetentionToList(search: SearchParameter): Observable<SearchResult<PrintCustomerRefundRetentionReportView>> {
    const url = `${this.servicePath}/GetPrintCustomerRefundRetentionList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deletePrintCustomerRefundRetention(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeletePrintCustomerRefundRetention`;
    return this.dataGateway.delete(url, row);
  }
  getPrintCustomerRefundRetentionById(id: string): Observable<PrintCustomerRefundRetentionReportView> {
    const url = `${this.servicePath}/GetPrintCustomerRefundRetentionById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createPrintCustomerRefundRetention(vmModel: PrintCustomerRefundRetentionReportView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreatePrintCustomerRefundRetention`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updatePrintCustomerRefundRetention(vmModel: PrintCustomerRefundRetentionReportView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdatePrintCustomerRefundRetention`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
