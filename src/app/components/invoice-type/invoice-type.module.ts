import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InvoiceTypeService } from './invoice-type.service';
import { InvoiceTypeListComponent } from './invoice-type-list/invoice-type-list.component';
import { InvoiceTypeItemComponent } from './invoice-type-item/invoice-type-item.component';
import { InvoiceTypeItemCustomComponent } from './invoice-type-item/invoice-type-item-custom.component';
import { InvoiceTypeListCustomComponent } from './invoice-type-list/invoice-type-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [InvoiceTypeListComponent, InvoiceTypeItemComponent],
  providers: [InvoiceTypeService, InvoiceTypeItemCustomComponent, InvoiceTypeListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [InvoiceTypeListComponent, InvoiceTypeItemComponent]
})
export class InvoiceTypeComponentModule {}
