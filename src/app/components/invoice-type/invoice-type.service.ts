import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { InvoiceTypeItemView, InvoiceTypeListView } from 'shared/models/viewModel';

@Injectable()
export class InvoiceTypeService {
  serviceKey = 'invoiceTypeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getInvoiceTypeToList(search: SearchParameter): Observable<SearchResult<InvoiceTypeListView>> {
    const url = `${this.servicePath}/GetInvoiceTypeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteInvoiceType(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteInvoiceType`;
    return this.dataGateway.delete(url, row);
  }
  getInvoiceTypeById(id: string): Observable<InvoiceTypeItemView> {
    const url = `${this.servicePath}/GetInvoiceTypeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createInvoiceType(vmModel: InvoiceTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateInvoiceType`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateInvoiceType(vmModel: InvoiceTypeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateInvoiceType`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
