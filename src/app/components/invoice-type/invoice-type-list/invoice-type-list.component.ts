import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { InvoiceTypeListView } from 'shared/models/viewModel';
import { InvoiceTypeService } from '../invoice-type.service';
import { InvoiceTypeListCustomComponent } from './invoice-type-list-custom.component';

@Component({
  selector: 'invoice-type-list',
  templateUrl: './invoice-type-list.component.html',
  styleUrls: ['./invoice-type-list.component.scss']
})
export class InvoiceTypeListComponent extends BaseListComponent<InvoiceTypeListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  defaultBitOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  suspenseInvoiceTypeOptions: SelectItems[] = [];
  autoGenInvoiceRevenueTypeOptions: SelectItems[] = [];
  constructor(private service: InvoiceTypeService, public custom: InvoiceTypeListCustomComponent) {
    super();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.setDataGridOption();
    this.baseDropdown.getDefaultBitDropDown(this.defaultBitOptions);
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getSuspenseinvoicetypeDropdown(this.suspenseInvoiceTypeOptions);
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getInvoiceRevenueTypeDropDown(this.autoGenInvoiceRevenueTypeOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'invoiceTypeGUID';
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.INVOICE_TYPE_ID',
        textKey: 'invoiceTypeId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: 'LABEL.DIRECT_RECEIPT',
        textKey: 'directReceipt',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defaultBitOptions
      },
      {
        label: 'LABEL.VALIDATE_DIRECT_RECEIVE_BY_CA',
        textKey: 'validateDirectReceiveByCA',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defaultBitOptions
      },
      {
        label: 'LABEL.SUSPENSE_INVOICE_TYPE',
        textKey: 'suspenseInvoiceType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.suspenseInvoiceTypeOptions
      },
      {
        label: 'LABEL.PRODUCT_INVOICE',
        textKey: 'productInvoice',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defaultBitOptions
      },
      {
        label: 'LABEL.AUTOGEN_INVOICE_REVENUETYPE',
        textKey: 'autoGenInvoiceRevenueType_values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.autoGenInvoiceRevenueTypeOptions
      }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<InvoiceTypeListView>> {
    return this.service.getInvoiceTypeToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteInvoiceType(row));
  }
}
