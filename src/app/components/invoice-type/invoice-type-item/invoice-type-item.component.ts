import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ProductType, SuspenseInvoiceType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { InvoiceTypeItemView } from 'shared/models/viewModel';
import { InvoiceTypeService } from '../invoice-type.service';
import { InvoiceTypeItemCustomComponent } from './invoice-type-item-custom.component';
@Component({
  selector: 'invoice-type-item',
  templateUrl: './invoice-type-item.component.html',
  styleUrls: ['./invoice-type-item.component.scss']
})
export class InvoiceTypeItemComponent extends BaseItemComponent<InvoiceTypeItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  productTypeOptions: SelectItems[] = [];
  suspenseInvoiceTypeOptions: SelectItems[] = [];
  autoGenInvoiceRevenueTypeOptions: SelectItems[] = [];
  constructor(private service: InvoiceTypeService, private currentActivatedRoute: ActivatedRoute, public custom: InvoiceTypeItemCustomComponent) {
    super();
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getSuspenseinvoicetypeDropdown(this.suspenseInvoiceTypeOptions);
  }
  getById(): Observable<InvoiceTypeItemView> {
    return this.service.getInvoiceTypeById(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.model.invoiceTypeGUID = EmptyGuid;
    this.onAsyncRunner();
    super.setDefaultValueSystemFields();
  }
  onAsyncRunner(model?: any): void {
    const productType = isNullOrUndefined(model) ? this.model.productType : model.productType;
    forkJoin([this.baseDropdown.getInvoiceRevenueTypeByProductTypeDropDown(productType)]).subscribe(
      ([invoiceRevenueType]) => {
        this.autoGenInvoiceRevenueTypeOptions = invoiceRevenueType;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {}
  setFunctionOptions(): void {}
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateInvoiceType(this.model), isColsing);
    } else {
      super.onCreate(this.service.createInvoiceType(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onProductTypeChange() {
    this.onProductTypeBinding();
    this.custom.setRequireField(this.model);
    super.setBaseFieldAccessing(this.custom.setFieldAccessingByProductType(this.model));
  }
  onSuspenseInvoiceTypeChange() {
    this.custom.setRequireField(this.model);
    super.setBaseFieldAccessing(this.custom.setFieldAccessingBySuspenseInvoiceType(this.model));
  }
  onProductInvoiceChange() {
    this.custom.setRequireField(this.model);
  }
  onProductTypeBinding() {
    this.model.autoGenInvoiceRevenueTypeGUID = null;
    if (!isNullOrUndefined(this.model.productType)) {
      this.baseDropdown.getInvoiceRevenueTypeByProductTypeDropDown(this.model.productType.toString()).subscribe((res) => {
        this.autoGenInvoiceRevenueTypeOptions = res;
      });
    } else {
      this.autoGenInvoiceRevenueTypeOptions.length = 0;
    }
  }
}
