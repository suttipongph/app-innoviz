import { Observable, of } from 'rxjs';
import { ProductType, SuspenseInvoiceType } from 'shared/constants';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FieldAccessing } from 'shared/models/systemModel';
import { InvoiceTypeItemView } from 'shared/models/viewModel';

const firstGroup = ['INVOICE_TYPE_ID'];
const procuctInvoice = ['PRODUCT_INVOICE'];
const suspenseInvoiceType = ['SUSPENSE_INVOICE_TYPE'];
export class InvoiceTypeItemCustomComponent {
  isRequiredCon1: boolean = false;
  getFieldAccessing(model: InvoiceTypeItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.invoiceTypeGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
    }

    if (model.productType === ProductType.None) {
      fieldAccessing.push({ filedIds: suspenseInvoiceType, readonly: true });
    } else if (model.suspenseInvoiceType !== SuspenseInvoiceType.None) {
      fieldAccessing.push({ filedIds: procuctInvoice, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: procuctInvoice, readonly: false });
    }

    // set mandatory autoGenInvoiceRevenueType
    this.setRequireField(model);
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: InvoiceTypeItemView): Observable<boolean> {
    return of(!canCreate);
  }

  setRequireField(model: InvoiceTypeItemView) {
    // set mandatory autoGenInvoiceRevenueType
    if (model.productType === ProductType.None) {
      model.productInvoice = false;
      model.suspenseInvoiceType = SuspenseInvoiceType.None;
      this.isRequiredCon1 = false;
    } else if (model.suspenseInvoiceType !== SuspenseInvoiceType.None) {
      model.productInvoice = false;
      this.isRequiredCon1 = true;
    } else if (model.productInvoice == true) {
      this.isRequiredCon1 = true;
    } else {
      this.isRequiredCon1 = false;
    }
  }
  setFieldAccessingByProductType(model: InvoiceTypeItemView): FieldAccessing[] {
    if (model.productType === ProductType.None) {
      return [
        { filedIds: procuctInvoice, readonly: true },
        { filedIds: suspenseInvoiceType, readonly: true }
      ];
    } else {
      return [
        { filedIds: procuctInvoice, readonly: false },
        { filedIds: suspenseInvoiceType, readonly: false }
      ];
    }
  }
  setFieldAccessingBySuspenseInvoiceType(model: InvoiceTypeItemView): FieldAccessing[] {
    if (model.suspenseInvoiceType !== SuspenseInvoiceType.None) {
      return [{ filedIds: procuctInvoice, readonly: true }];
    } else {
      return [{ filedIds: procuctInvoice, readonly: false }];
    }
  }
}
