import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreditAppTableService } from './credit-app-table.service';
import { CreditAppTableListComponent } from './credit-app-table-list/credit-app-table-list.component';
import { CreditAppTableListCustomComponent } from './credit-app-table-list/credit-app-table-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CreditAppTableListComponent],
  providers: [CreditAppTableService, CreditAppTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CreditAppTableListComponent]
})
export class CreditAppTableComponentModule {}
