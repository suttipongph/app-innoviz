import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrintReceiptService } from './print-receipt.service';
import { PrintReceiptReportComponent } from './print-receipt/print-receipt-report.component';
import { PrintReceiptReportCustomComponent } from './print-receipt/print-receipt-report-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PrintReceiptReportComponent],
  providers: [PrintReceiptService, PrintReceiptReportCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PrintReceiptReportComponent]
})
export class PrintReceiptComponentModule {}
