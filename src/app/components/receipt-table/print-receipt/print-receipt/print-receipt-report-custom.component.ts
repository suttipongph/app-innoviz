import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { PrintReceiptReportView } from 'shared/models/viewModel';

const firstGroup = ['RECEIPT_ID', 'RECEIPT_DATE', 'TRANS_DATE', 'CUSTOMER_ID', 'SETTLE_BASE_AMOUNT', 'SETTLE_TAX_AMOUNT', 'SETTLE_AMOUNT'];

export class PrintReceiptReportCustomComponent {
  baseService: BaseServiceModel<any>;
  isCopy = false;
  isOriginal = false;
  getFieldAccessing(model: PrintReceiptReportView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<PrintReceiptReportView> {
    const model = new PrintReceiptReportView();
    model.printReceiptGUID = EmptyGuid;
    return of(model);
  }
  getLabel(label: string): any {
    switch (label) {
      case 'copy':
        this.isCopy = true;
        break;
      case 'original':
        this.isOriginal = true;
        break;
    }
  }
}
