import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintReceiptReportComponent } from './print-receipt-report.component';

describe('PrintReceiptReportViewComponent', () => {
  let component: PrintReceiptReportComponent;
  let fixture: ComponentFixture<PrintReceiptReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrintReceiptReportComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintReceiptReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
