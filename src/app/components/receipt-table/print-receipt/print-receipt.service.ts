import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { PrintReceiptReportView } from 'shared/models/viewModel';
@Injectable()
export class PrintReceiptService {
  serviceKey = 'printReceiptGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }

  getPrintReceiptById(id: string): Observable<PrintReceiptReportView> {
    const url = `${this.servicePath}/GetPrintReceiptById/id=${id}`;
    return this.dataGateway.get(url);
  }
}
