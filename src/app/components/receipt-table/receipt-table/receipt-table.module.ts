import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReceiptTableService } from './receipt-table.service';
import { ReceiptTableListComponent } from './receipt-table-list/receipt-table-list.component';
import { ReceiptTableItemComponent } from './receipt-table-item/receipt-table-item.component';
import { ReceiptTableItemCustomComponent } from './receipt-table-item/receipt-table-item-custom.component';
import { ReceiptTableListCustomComponent } from './receipt-table-list/receipt-table-list-custom.component';
import { ReceiptLineComponentModule } from '../receipt-line/receipt-line.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, ReceiptLineComponentModule],
  declarations: [ReceiptTableListComponent, ReceiptTableItemComponent],
  providers: [ReceiptTableService, ReceiptTableItemCustomComponent, ReceiptTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ReceiptTableListComponent, ReceiptTableItemComponent]
})
export class ReceiptTableComponentModule {}
