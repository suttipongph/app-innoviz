import { Observable, of } from 'rxjs';
import { EmptyGuid, ReceiptStatus } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { ReceiptTableItemView } from 'shared/models/viewModel';

const firstGroup = [
  'RECEIPT_ID',
  'RECEIPT_DATE',
  'TRANS_DATE',
  'CUSTOMER_TABLE_GUID',
  'RECEIPT_TEMP_TABLE_GUID',
  'METHOD_OF_PAYMENT_GUID',
  'CURRENCY_GUID',
  'EXCHANGE_RATE',
  'DOCUMENT_STATUS_GUID',
  'UNBOUND_RECEIPT_ADDRESS',
  'RECEIPT_ADDRESS1',
  'RECEIPT_ADDRESS2',
  'REMARK',
  'SETTLE_BASE_AMOUNT',
  'SETTLE_TAX_AMOUNT',
  'SETTLE_AMOUNT',
  'SETTLE_BASE_AMOUNT_MST',
  'SETTLE_TAX_AMOUNT_MST',
  'SETTLE_AMOUNT_MST',
  'OVER_UNDER_AMOUNT',
  'CHEQUE_DATE',
  'CHEQUE_NO',
  'CHEQUE_BANK_GROUP_GUID',
  'CHEQUE_BRANCH',
  'RECEIPT_TABLE_GUID',
  'INVOICE_SETTLEMENT_DETAIL_GUID',
  'REF_TYPE',
  'REF_ID'
];

export class ReceiptTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: ReceiptTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ReceiptTableItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(): Observable<ReceiptTableItemView> {
    const model = new ReceiptTableItemView();
    model.receiptTableGUID = EmptyGuid;
    return of(model);
  }

  getPrintReceiptVisible(model: ReceiptTableItemView): boolean {
    if (model.documentStatus_Values !== ReceiptStatus.Cancelled) {
      return true;
    }
  }
  getPrintReceipttDisabled(model: ReceiptTableItemView): boolean {
    if (model.documentStatus_Values !== ReceiptStatus.Cancelled) {
      return false;
    } else if (model.documentStatus_Values === ReceiptStatus.Cancelled) {
      return true;
    }
  }
}
