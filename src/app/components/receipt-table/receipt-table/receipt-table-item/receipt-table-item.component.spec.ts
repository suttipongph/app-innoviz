import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiptTableItemComponent } from './receipt-table-item.component';

describe('ReceiptTableItemViewComponent', () => {
  let component: ReceiptTableItemComponent;
  let fixture: ComponentFixture<ReceiptTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReceiptTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
