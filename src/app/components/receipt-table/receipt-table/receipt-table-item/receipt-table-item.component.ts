import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, ROUTE_REPORT_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { ReceiptTableItemView } from 'shared/models/viewModel';
import { ReceiptTableService } from '../receipt-table.service';
import { ReceiptTableItemCustomComponent } from './receipt-table-item-custom.component';
@Component({
  selector: 'receipt-table-item',
  templateUrl: './receipt-table-item.component.html',
  styleUrls: ['./receipt-table-item.component.scss']
})
export class ReceiptTableItemComponent extends BaseItemComponent<ReceiptTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  refTypeOptions: SelectItems[] = [];
  constructor(private service: ReceiptTableService, private currentActivatedRoute: ActivatedRoute, public custom: ReceiptTableItemCustomComponent) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<ReceiptTableItemView> {
    return this.service.getReceiptTableById(this.id);
  }
  getInitialData(): Observable<ReceiptTableItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions(result);
        this.setFunctionOptions(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  setRelatedInfoOptions(model: ReceiptTableItemView): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.TAX_INVOICE',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.TAX_INVOICE
          })
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }

  setFunctionOptions(model: ReceiptTableItemView): void {
    const getprintreceiptvisible = this.custom.getPrintReceiptVisible(model);
    const getprintreceiptdisabled = this.custom.getPrintReceipttDisabled(model);
    this.functionItems = [
      {
        label: 'LABEL.PRINT',
        items: [
          {
            label: 'LABEL.COPY',
            command: () =>
              this.toReport({
                path: ROUTE_REPORT_GEN.PRINT_COPY
              }),
            disabled: getprintreceiptdisabled,
            visible: getprintreceiptvisible
          },
          {
            label: 'LABEL.ORIGINAL',
            command: () =>
              this.toReport({
                path: ROUTE_REPORT_GEN.PRINT_ORIGINAL
              }),
            disabled: getprintreceiptdisabled,
            visible: getprintreceiptvisible
          }
        ]
      }
    ];
    super.setFunctionOptionsMapping();
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateReceiptTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createReceiptTable(this.model), isColsing);
    }
  }

  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
