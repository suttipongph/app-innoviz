import { COLLECTION_FEE_TABLE } from './../../../../shared/constants/textKey';
import { Observable } from 'rxjs';
import { ColumnType, SortType, ROUTE_RELATED_GEN } from 'shared/constants';
import { BaseServiceModel, ColumnModel } from 'shared/models/systemModel';

export class ReceiptTableListCustomComponent {
  baseService: BaseServiceModel<any>;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  getParentKey(): string {
    const activeParentName = this.baseService.uiService.getRelatedInfoActiveTableName();
    if (activeParentName) {
      switch (activeParentName) {
        case ROUTE_RELATED_GEN.COLLECTION_FOLLOW_UP:
          return this.baseService.uiService.getKey(activeParentName);
        default:
          return null;
      }
    } else {
      return null;
    }
  }
}
