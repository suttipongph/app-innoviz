import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReceiptTableListComponent } from './receipt-table-list.component';

describe('ReceiptTableListViewComponent', () => {
  let component: ReceiptTableListComponent;
  let fixture: ComponentFixture<ReceiptTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReceiptTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
