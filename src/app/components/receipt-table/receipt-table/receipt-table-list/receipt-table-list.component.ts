import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { ReceiptTableListView } from 'shared/models/viewModel';
import { ReceiptTableService } from '../receipt-table.service';
import { ReceiptTableListCustomComponent } from './receipt-table-list-custom.component';

@Component({
  selector: 'receipt-table-list',
  templateUrl: './receipt-table-list.component.html',
  styleUrls: ['./receipt-table-list.component.scss']
})
export class ReceiptTableListComponent extends BaseListComponent<ReceiptTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  documentStatusOptions: SelectItems[] = [];
  customerTableOption: SelectItems[] = [];
  invoiceSettlementOptions: SelectItems[] = [];
  currencyOptions: SelectItems[] = [];

  constructor(
    public custom: ReceiptTableListCustomComponent,
    private service: ReceiptTableService,
    public uiControllerService: UIControllerService,
    public currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.parentId = this.custom.getParentKey();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCustomerTableByDropDown(this.customerTableOption);
    this.baseDropdown.getInvoiceSettlementDetaileDropDown(this.invoiceSettlementOptions);
    this.baseDropdown.getCurrencyDropDown(this.currencyOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'receiptTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.RECEIPT_ID',
        textKey: 'receiptId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.RECEIPT_DATE',
        textKey: 'receiptDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.TRANSACTION_DATE',
        textKey: 'transDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOption,
        sortingKey: 'customerTableGUID',
        searchingKey: 'customerTable_Values'
      },
      {
        label: 'LABEL.INVOICE_SETTLEMENT_ID',
        textKey: 'invoiceSettlementDetail_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.invoiceSettlementOptions,
        sortingKey: 'invoiceSettlementDetailGUID',
        searchingKey: 'invoiceSettlementDetail_Values'
      },
      {
        label: 'LABEL.CURRENCY_ID',
        textKey: 'currency_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.currencyOptions,
        sortingKey: 'currencyGUID',
        searchingKey: 'currency_Values'
      },
      {
        label: 'LABEL.SETTLE_BASE_AMOUNT',
        textKey: 'settleBaseAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.SETTLE_TAX_AMOUNT',
        textKey: 'settleTaxAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.SETTLE_AMOUNT',
        textKey: 'settleAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentStatusOptions,
        sortingKey: 'documentStatus_StatusId',
        searchingKey: 'documentStatusGUID'
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<ReceiptTableListView>> {
    return this.service.getReceiptTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteReceiptTable(row));
  }
}
