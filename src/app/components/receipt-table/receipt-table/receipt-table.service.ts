import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ReceiptTableItemView, ReceiptTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ReceiptTableService {
  serviceKey = 'receiptTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getReceiptTableToList(search: SearchParameter): Observable<SearchResult<ReceiptTableListView>> {
    const url = `${this.servicePath}/GetReceiptTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteReceiptTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteReceiptTable`;
    return this.dataGateway.delete(url, row);
  }
  getReceiptTableById(id: string): Observable<ReceiptTableItemView> {
    const url = `${this.servicePath}/GetReceiptTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createReceiptTable(vmModel: ReceiptTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateReceiptTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateReceiptTable(vmModel: ReceiptTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateReceiptTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
