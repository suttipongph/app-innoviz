import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReceiptLineListComponent } from './receipt-line-list.component';

describe('ReceiptLineListViewComponent', () => {
  let component: ReceiptLineListComponent;
  let fixture: ComponentFixture<ReceiptLineListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReceiptLineListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptLineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
