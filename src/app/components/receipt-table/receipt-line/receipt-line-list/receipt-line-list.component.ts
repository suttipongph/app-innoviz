import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { ReceiptLineListView } from 'shared/models/viewModel';
import { ReceiptLineService } from '../receipt-line.service';
import { ReceiptLineListCustomComponent } from './receipt-line-list-custom.component';

@Component({
  selector: 'receipt-line-list',
  templateUrl: './receipt-line-list.component.html',
  styleUrls: ['./receipt-line-list.component.scss']
})
export class ReceiptLineListComponent extends BaseListComponent<ReceiptLineListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  invoiceTableOption: SelectItems[] = [];
  invoiceTypeOptions: SelectItems[] = [];

  constructor(public custom: ReceiptLineListCustomComponent, private service: ReceiptLineService, public uiControllerService: UIControllerService) {
    super();
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getKey('receipttable');

    console.log('parentId', this.parentId);
  }
  ngOnInit(): void {
    this.accessRightChildPage = 'ReceiptLineListPage';
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getInvoiceTableDropDown(this.invoiceTableOption);
    this.baseDropdown.getInvoiceTypeDropDown(this.invoiceTypeOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'receiptLineGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.LINE',
        textKey: 'lineNum',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.INVOICE_ID',
        textKey: 'invoiceTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.invoiceTableOption,
        searchingKey: 'invoiceTableGUID',
        sortingKey: 'invoiceTable_InvoiceId'
      },
      {
        label: 'LABEL.INVOICE_AMOUNT',
        textKey: 'invoiceAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.SETTLE_BASE_AMOUNT',
        textKey: 'settleBaseAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.SETTLE_TAX_AMOUNT',
        textKey: 'settleTaxAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.SETTLE_AMOUNT',
        textKey: 'settleAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.WHT_AMOUNT',
        textKey: 'whtAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'receiptTableGUID',
        type: ColumnType.MASTER,
        visibility: false,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<ReceiptLineListView>> {
    return this.service.getReceiptLineToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteReceiptLine(row));
  }
}
