import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiptLineItemComponent } from './receipt-line-item.component';

describe('ReceiptLineItemViewComponent', () => {
  let component: ReceiptLineItemComponent;
  let fixture: ComponentFixture<ReceiptLineItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReceiptLineItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptLineItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
