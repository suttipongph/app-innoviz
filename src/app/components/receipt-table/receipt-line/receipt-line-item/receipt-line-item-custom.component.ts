import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { ReceiptLineItemView } from 'shared/models/viewModel';

const firstGroup = [
  'RECEIPT_TABLE_GUID',
  'LINE_NUM',
  'INVOICE_TABLE_GUID',
  'DUE_DATE',
  'INVOICE_AMOUNT',
  'TAX_AMOUNT',
  'TAX_TABLE_GUID',
  'WITHHOLDING_TAX_TABLE_GUID',
  'SETTLE_BASE_AMOUNT',
  'SETTLE_TAX_AMOUNT',
  'SETTLE_AMOUNT',
  'WHT_AMOUNT',
  'SETTLE_BASE_AMOUNT_MST',
  'SETTLE_TAX_AMOUNT_MST',
  'SETTLE_AMOUNT_MST',
  'RECEIPT_LINE_GUID'
];

export class ReceiptLineItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: ReceiptLineItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ReceiptLineItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(): Observable<ReceiptLineItemView> {
    let model = new ReceiptLineItemView();
    model.receiptLineGUID = EmptyGuid;
    return of(model);
  }
}
