import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReceiptLineService } from './receipt-line.service';
import { ReceiptLineListComponent } from './receipt-line-list/receipt-line-list.component';
import { ReceiptLineItemComponent } from './receipt-line-item/receipt-line-item.component';
import { ReceiptLineItemCustomComponent } from './receipt-line-item/receipt-line-item-custom.component';
import { ReceiptLineListCustomComponent } from './receipt-line-list/receipt-line-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ReceiptLineListComponent, ReceiptLineItemComponent],
  providers: [ReceiptLineService, ReceiptLineItemCustomComponent, ReceiptLineListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ReceiptLineListComponent, ReceiptLineItemComponent]
})
export class ReceiptLineComponentModule {}
