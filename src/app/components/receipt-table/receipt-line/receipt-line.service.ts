import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ReceiptLineItemView, ReceiptLineListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ReceiptLineService {
  serviceKey = 'receiptLineGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getReceiptLineToList(search: SearchParameter): Observable<SearchResult<ReceiptLineListView>> {
    const url = `${this.servicePath}/GetReceiptLineList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteReceiptLine(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteReceiptLine`;
    return this.dataGateway.delete(url, row);
  }
  getReceiptLineById(id: string): Observable<ReceiptLineItemView> {
    const url = `${this.servicePath}/GetReceiptLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createReceiptLine(vmModel: ReceiptLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateReceiptLine`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateReceiptLine(vmModel: ReceiptLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateReceiptLine`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
