import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RelatedPersonTableListComponent } from './related-person-table-list.component';

describe('RelatedPersonTableListViewComponent', () => {
  let component: RelatedPersonTableListComponent;
  let fixture: ComponentFixture<RelatedPersonTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RelatedPersonTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatedPersonTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
