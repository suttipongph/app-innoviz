import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { RelatedPersonTableListView } from 'shared/models/viewModel';
import { RelatedPersonTableService } from '../related-person-table.service';
import { RelatedPersonTableListCustomComponent } from './related-person-table-list-custom.component';

@Component({
  selector: 'related-person-table-list',
  templateUrl: './related-person-table-list.component.html',
  styleUrls: ['./related-person-table-list.component.scss']
})
export class RelatedPersonTableListComponent extends BaseListComponent<RelatedPersonTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  customerTableOptions: SelectItems[] = [];
  genderOptions: SelectItems[] = [];
  identificationTypeOptions: SelectItems[] = [];
  maritalStatusOptions: SelectItems[] = [];
  nationalityOptions: SelectItems[] = [];
  occupationOptions: SelectItems[] = [];
  raceOptions: SelectItems[] = [];
  recordTypeOptions: SelectItems[] = [];
  registrationTypeOptions: SelectItems[] = [];

  constructor(public custom: RelatedPersonTableListCustomComponent, private service: RelatedPersonTableService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getIdentificationTypeEnumDropDown(this.identificationTypeOptions);
    this.baseDropdown.getRecordTypeEnumDropDown(this.recordTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'relatedPersonTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.RELATED_PERSON_ID',
        textKey: 'relatedPersonId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.NAME',
        textKey: 'name',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.OPERATED_BY',
        textKey: 'operatedBy',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.RECORD_TYPE',
        textKey: 'recordType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.recordTypeOptions
      },
      {
        label: 'LABEL.TAX_ID',
        textKey: 'taxId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PASSPORT_ID',
        textKey: 'passportId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.POSITION',
        textKey: 'position',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.COMPANY_NAME',
        textKey: 'companyName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<RelatedPersonTableListView>> {
    return this.service.getRelatedPersonTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteRelatedPersonTable(row));
  }
}
