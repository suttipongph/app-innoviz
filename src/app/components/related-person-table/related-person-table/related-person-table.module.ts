import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RelatedPersonTableService } from './related-person-table.service';
import { RelatedPersonTableListComponent } from './related-person-table-list/related-person-table-list.component';
import { RelatedPersonTableItemComponent } from './related-person-table-item/related-person-table-item.component';
import { RelatedPersonTableItemCustomComponent } from './related-person-table-item/related-person-table-item-custom.component';
import { RelatedPersonTableListCustomComponent } from './related-person-table-list/related-person-table-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [RelatedPersonTableListComponent, RelatedPersonTableItemComponent],
  providers: [RelatedPersonTableService, RelatedPersonTableItemCustomComponent, RelatedPersonTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [RelatedPersonTableListComponent, RelatedPersonTableItemComponent]
})
export class RelatedPersonTableComponentModule {}
