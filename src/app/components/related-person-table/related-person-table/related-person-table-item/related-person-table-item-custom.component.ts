import { Observable, of } from 'rxjs';
import { EmptyGuid, IdentificationType, RecordType } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { RelatedPersonTableItemView } from 'shared/models/viewModel';
import { RelatedPersonTableService } from '../related-person-table.service';
import { datetoString, isDateFromGreaterThanDateTo } from 'shared/functions/date.function';
import { NotificationService } from 'core/services/notification.service';
import { AppInjector } from 'app-injector';
import { TranslateService } from '@ngx-translate/core';
const firstGroup = ['RELATED_PERSON_ID'];
const secondGroup = ['PASSPORT_ID', 'WORK_PERMIT_ID'];
const thridGroup = ['TAX_ID'];
const fourtGroup = ['REGISTRATION_TYPE_GUID', 'DATE_OF_ESTABLISHMENT', 'PAID_UP_CAPITAL', 'REGISTERED_CAPITAL'];
const fifthGroup = [
  'IDENTIFICATION_TYPE',
  'DATE_OF_BIRTH',
  'NATIONALITY_GUID',
  'RACE_GUID',
  'GENDER_GUID',
  'MARITAL_STATUS_GUID',
  'SPOUSE_NAME',
  'OCCUPATION_GUID',
  'POSITION',
  'WORK_EXPERIENCE_YEAR',
  'WORK_EXPERIENCE_MONTH',
  'COMPANY_NAME',
  'INCOME',
  'OTHER_SOURCE_INCOME',
  'OTHER_INCOME',
  'PHONE',
  'EXTENSION',
  'MOBILE',
  'FAX',
  'EMAIL',
  'LINE_ID'
];
const sixthGroup = ['AGE'];
export class RelatedPersonTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  isManunal: boolean = false;
  isTax: boolean = false;
  isPerson: boolean = false;
  notificationService = AppInjector.get(NotificationService);
  translateService = AppInjector.get(TranslateService);

  getFieldAccessing(model: RelatedPersonTableItemView, relatedPersonTableService: RelatedPersonTableService): Observable<any> {
    let fieldAccessing: FieldAccessing[] = [];
    return new Observable((observer) => {
      // set firstGroup
      fieldAccessing.push({ filedIds: sixthGroup, readonly: true });
      fieldAccessing.push(...this.setFieldAccessingByIdentificationType(model.identificationType));
      fieldAccessing.push(...this.setFieldAccessingByRecordType(model.recordType));
      if (!isUpdateMode(model.relatedPersonTableGUID)) {
        this.validateIsManualNumberSeq(relatedPersonTableService, model.companyGUID).subscribe(
          (result) => {
            this.isManunal = result;
            fieldAccessing.push({ filedIds: firstGroup, readonly: !this.isManunal });
            observer.next(fieldAccessing);
          },
          (error) => {
            this.notificationService.showErrorMessageFromResponse(error);
          }
        );
      } else {
        fieldAccessing.push({ filedIds: firstGroup, readonly: true });
        observer.next(fieldAccessing);
      }
    });
  }
  getInitialData(): Observable<RelatedPersonTableItemView> {
    let model: RelatedPersonTableItemView = new RelatedPersonTableItemView();
    model.relatedPersonTableGUID = EmptyGuid;
    model.recordType = RecordType.Person;
    model.identificationType = IdentificationType.TaxId;
    return of(model);
  }
  setFieldAccessingByIdentificationType(value: number): FieldAccessing[] {
    this.conditionByIdentificationType(value);
    return [
      { filedIds: secondGroup, readonly: this.isTax },
      { filedIds: thridGroup, readonly: !this.isTax }
    ];
  }
  setFieldAccessingByRecordType(value: number): FieldAccessing[] {
    this.conditionByRecordType(value);
    return [
      { filedIds: fourtGroup, readonly: this.isPerson },
      { filedIds: fifthGroup, readonly: !this.isPerson }
    ];
  }
  conditionByIdentificationType(value: number): void {
    this.isTax = value === IdentificationType.TaxId ? true : false;
  }
  conditionByRecordType(value: number): void {
    this.isPerson = value === RecordType.Person ? true : false;
  }
  validateIsManualNumberSeq(relatedPersonTableService: RelatedPersonTableService, companyId: string): Observable<boolean> {
    return relatedPersonTableService.validateIsManualNumberSeq(companyId);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: RelatedPersonTableItemView): Observable<boolean> {
    return of(!canCreate);
  }
  setDefualtValue(relatedPersonTableService: RelatedPersonTableService, model: RelatedPersonTableItemView): void {
    model.identificationType = IdentificationType.TaxId;
    model.recordType = RecordType.Person;
  }
  validateDatefBirth(date: string, notificationService: NotificationService): any {
    if (isDateFromGreaterThanDateTo(date, datetoString(new Date()))) {
      return {
        code: 'ERROR.DATE_CANNOT_GREATER_THAN_SYSTEM_DATE',
        parameters: [this.translateService.instant('LABEL.DATE_OF_BIRTH')]
      };
    }
    return null;
  }
  setValueByIdentificationType(model: RelatedPersonTableItemView): void {
    if (this.isTax) {
      model.passportId = '';
      model.workPermitId = '';
    } else {
      model.taxId = '';
    }
  }
  setValueByRecordType(model: RelatedPersonTableItemView): void {
    if (this.isPerson) {
      model.registrationTypeGUID = null;
      model.dateOfEstablishment = null;
      model.paidUpCapital = 0;
      model.registeredCapital = 0;
    } else {
      model.age = 0;
      model.nationalityGUID = null;
      model.raceGUID = null;
      model.genderGUID = null;
      model.maritalStatusGUID = null;
      model.spouseName = '';
      model.occupationGUID = null;
      model.position = '';
      model.workExperienceMonth = 0;
      model.workExperienceYear = 0;
      model.companyName = '';
      model.income = 0;
      model.otherSourceIncome = '';
      model.otherIncome = 0;
      model.phone = '';
      model.extension = '';
      model.mobile = '';
      model.fax = '';
      model.email = '';
      model.lineId = '';
      model.dateOfBirth = null;
      model.identificationType = IdentificationType.TaxId;
    }
  }
}
