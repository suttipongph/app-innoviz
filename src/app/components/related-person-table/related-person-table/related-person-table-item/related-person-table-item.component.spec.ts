import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelatedPersonTableItemComponent } from './related-person-table-item.component';

describe('RelatedPersonTableItemViewComponent', () => {
  let component: RelatedPersonTableItemComponent;
  let fixture: ComponentFixture<RelatedPersonTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RelatedPersonTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatedPersonTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
