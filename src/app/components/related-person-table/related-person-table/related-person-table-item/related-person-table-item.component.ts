import { ThrowStmt } from '@angular/compiler';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { AppConst, EmptyGuid, ROUTE_RELATED_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { RelatedPersonTableItemView } from 'shared/models/viewModel';
import { RelatedPersonTableService } from '../related-person-table.service';
import { RelatedPersonTableItemCustomComponent } from './related-person-table-item-custom.component';
@Component({
  selector: 'related-person-table-item',
  templateUrl: './related-person-table-item.component.html',
  styleUrls: ['./related-person-table-item.component.scss']
})
export class RelatedPersonTableItemComponent extends BaseItemComponent<RelatedPersonTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  genderOptions: SelectItems[] = [];
  identificationTypeOptions: SelectItems[] = [];
  maritalStatusOptions: SelectItems[] = [];
  nationalityOptions: SelectItems[] = [];
  occupationOptions: SelectItems[] = [];
  raceOptions: SelectItems[] = [];
  recordTypeOptions: SelectItems[] = [];
  registrationTypeOptions: SelectItems[] = [];

  constructor(
    private service: RelatedPersonTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: RelatedPersonTableItemCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getIdentificationTypeEnumDropDown(this.identificationTypeOptions);
    this.baseDropdown.getRecordTypeEnumDropDown(this.recordTypeOptions);
  }
  getById(): Observable<RelatedPersonTableItemView> {
    return this.service.getRelatedPersonTableById(this.id);
  }
  getInitialData(): Observable<RelatedPersonTableItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }
  onAsyncRunner(model?: any): void {
    forkJoin(
      this.baseDropdown.getGenderDropDown(),
      this.baseDropdown.getMaritalStatusDropDown(),
      this.baseDropdown.getNationalityDropDown(),
      this.baseDropdown.getOccupationDropDown(),
      this.baseDropdown.getRaceDropDown(),
      this.baseDropdown.getRegistrationTypeDropDown()
    ).subscribe(
      ([gender, maritalStatus, nationality, occupation, race, registrationType]) => {
        this.genderOptions = gender;
        this.maritalStatusOptions = maritalStatus;
        this.nationalityOptions = nationality;
        this.occupationOptions = occupation;
        this.raceOptions = race;
        this.registrationTypeOptions = registrationType;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model, this.service).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateRelatedPersonTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createRelatedPersonTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onIdentificationTypeChange(e: any): void {
    super.setBaseFieldAccessing(this.custom.setFieldAccessingByIdentificationType(e));
    this.custom.setValueByIdentificationType(this.model);
  }
  onRecoedTypeChange(e: any): void {
    super.setBaseFieldAccessing(this.custom.setFieldAccessingByRecordType(e));
    this.custom.setValueByRecordType(this.model);
    this.onIdentificationTypeChange(this.model.identificationType);
  }
  onIdentificationTypeBinding(e: any): void {
    super.setBaseFieldAccessing(this.custom.setFieldAccessingByIdentificationType(e));
  }
  onRecordTypeBinding(e: any): void {
    super.setBaseFieldAccessing(this.custom.setFieldAccessingByRecordType(e));
  }
  onDateOfBirthChange(e): void {
    return this.custom.validateDatefBirth(e, this.notificationService);
  }
  setRelatedInfoOptions(): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.MEMO',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.MEMO }),
        visible: this.uiService.setAuthorizedBySite([AppConst.BACK])
      },
      {
        label: 'LABEL.MANAGE',
        items: [
          {
            label: 'LABEL.ADDRESS',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.ADDRESS_TRANS })
          }
        ]
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
}
