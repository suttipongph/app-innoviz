import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { RelatedPersonTableItemView, RelatedPersonTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class RelatedPersonTableService {
  serviceKey = 'relatedPersonTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getRelatedPersonTableToList(search: SearchParameter): Observable<SearchResult<RelatedPersonTableListView>> {
    const url = `${this.servicePath}/GetRelatedPersonTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteRelatedPersonTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteRelatedPersonTable`;
    return this.dataGateway.delete(url, row);
  }
  getRelatedPersonTableById(id: string): Observable<RelatedPersonTableItemView> {
    const url = `${this.servicePath}/GetRelatedPersonTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createRelatedPersonTable(vmModel: RelatedPersonTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateRelatedPersonTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateRelatedPersonTable(vmModel: RelatedPersonTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateRelatedPersonTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  validateIsManualNumberSeq(companyId: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeq/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
}
