import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { BuyerAgreementLineItemView, BuyerAgreementLineListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class BuyerAgreementLineService {
  serviceKey = 'buyerAgreementLineGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBuyerAgreementLineToList(search: SearchParameter): Observable<SearchResult<BuyerAgreementLineListView>> {
    const url = `${this.servicePath}/GetBuyerAgreementLineList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBuyerAgreementLine(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBuyerAgreementLine`;
    return this.dataGateway.delete(url, row);
  }
  getBuyerAgreementLineById(id: string): Observable<BuyerAgreementLineItemView> {
    const url = `${this.servicePath}/GetBuyerAgreementLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBuyerAgreementLine(vmModel: BuyerAgreementLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBuyerAgreementLine`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBuyerAgreementLine(vmModel: BuyerAgreementLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBuyerAgreementLine`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<BuyerAgreementLineItemView> {
    const url = `${this.servicePath}/GetBuyerAgreementLineInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
}
