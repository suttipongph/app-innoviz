import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BuyerAgreementLineService } from './buyer-agreement-line.service';
import { BuyerAgreementLineListComponent } from './buyer-agreement-line-list/buyer-agreement-line-list.component';
import { BuyerAgreementLineItemComponent } from './buyer-agreement-line-item/buyer-agreement-line-item.component';
import { BuyerAgreementLineItemCustomComponent } from './buyer-agreement-line-item/buyer-agreement-line-item-custom.component';
import { BuyerAgreementLineListCustomComponent } from './buyer-agreement-line-list/buyer-agreement-line-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BuyerAgreementLineListComponent, BuyerAgreementLineItemComponent],
  providers: [BuyerAgreementLineService, BuyerAgreementLineItemCustomComponent, BuyerAgreementLineListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BuyerAgreementLineListComponent, BuyerAgreementLineItemComponent]
})
export class BuyerAgreementLineComponentModule {}
