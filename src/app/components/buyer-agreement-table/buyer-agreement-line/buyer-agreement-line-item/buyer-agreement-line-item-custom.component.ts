import { Observable, of } from 'rxjs';
import { FieldAccessing } from 'shared/models/systemModel';
import { BuyerAgreementLineItemView } from 'shared/models/viewModel';

const firstGroup = ['BUYER_AGREEMENT_TABLE_GUID'];

export class BuyerAgreementLineItemCustomComponent {
  getFieldAccessing(model: BuyerAgreementLineItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BuyerAgreementLineItemView): Observable<boolean> {
    return of(!canCreate);
  }
}
