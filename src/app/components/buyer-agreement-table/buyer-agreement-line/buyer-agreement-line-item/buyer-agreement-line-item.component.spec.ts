import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyerAgreementLineItemComponent } from './buyer-agreement-line-item.component';

describe('BuyerAgreementLineItemViewComponent', () => {
  let component: BuyerAgreementLineItemComponent;
  let fixture: ComponentFixture<BuyerAgreementLineItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BuyerAgreementLineItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerAgreementLineItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
