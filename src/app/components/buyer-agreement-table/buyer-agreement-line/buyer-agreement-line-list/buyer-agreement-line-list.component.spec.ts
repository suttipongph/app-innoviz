import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BuyerAgreementLineListComponent } from './buyer-agreement-line-list.component';

describe('BuyerAgreementLineListViewComponent', () => {
  let component: BuyerAgreementLineListComponent;
  let fixture: ComponentFixture<BuyerAgreementLineListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BuyerAgreementLineListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerAgreementLineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
