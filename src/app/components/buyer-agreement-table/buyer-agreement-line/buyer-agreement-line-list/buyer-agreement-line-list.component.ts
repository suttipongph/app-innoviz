import { Component, Input, Output, EventEmitter } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { BuyerAgreementLineListView } from 'shared/models/viewModel';
import { BuyerAgreementLineService } from '../buyer-agreement-line.service';
import { BuyerAgreementLineListCustomComponent } from './buyer-agreement-line-list-custom.component';

@Component({
  selector: 'buyer-agreement-line-list',
  templateUrl: './buyer-agreement-line-list.component.html',
  styleUrls: ['./buyer-agreement-line-list.component.scss']
})
export class BuyerAgreementLineListComponent extends BaseListComponent<BuyerAgreementLineListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  @Output() deletedEmit = new EventEmitter<any>();
  buyerAgreementTableOptions: SelectItems[] = [];

  constructor(public custom: BuyerAgreementLineListCustomComponent, private service: BuyerAgreementLineService) {
    super();
    this.parentId = this.uiService.getKey('buyeragreementtable');
  }
  ngOnInit(): void {
    this.accessRightChildPage = 'BuyerAgreementLineListPage';
    this.redirectPath = ROUTE_RELATED_GEN.BUYER_AGREEMENT_LINE;
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'buyerAgreementLineGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.PERIOD',
        textKey: 'period',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.AMOUNT',
        textKey: 'amount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: null,
        textKey: 'buyerAgreementTableGUID',
        type: ColumnType.MASTER,
        visibility: false,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<BuyerAgreementLineListView>> {
    return this.service.getBuyerAgreementLineToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteBuyerAgreementLine(row));
  }
  onDeleted(e: any): void {
    this.deletedEmit.emit(e);
  }
}
