import { TranslateService } from '@ngx-translate/core';
import { AppInjector } from 'app-injector';
import { NotificationService } from 'core/services/notification.service';
import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isDateFromGreaterThanDateTo } from 'shared/functions/date.function';
import { isUpdateMode } from 'shared/functions/value.function';
import { FieldAccessing, TranslateModel } from 'shared/models/systemModel';
import { BuyerAgreementTableItemView } from 'shared/models/viewModel';
import { BuyerAgreementTableService } from '../buyer-agreement-table.service';

const firstGroup = ['BUYER_TABLE_GUID', 'BUYER_AGREEMENT_AMOUNT'];
const secondGroup = ['BUYER_AGREEMENT_ID'];

export class BuyerAgreementTableItemCustomComponent {
  isManunal: boolean = false;
  notificationService = AppInjector.get(NotificationService);
  translateService = AppInjector.get(TranslateService);

  getFieldAccessing(model: BuyerAgreementTableItemView, service: BuyerAgreementTableService): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return new Observable((observer) => {
      if (!isUpdateMode(model.buyerAgreementTableGUID)) {
        this.validateIsManualNumberSeq(service, model.companyGUID).subscribe(
          (result) => {
            this.isManunal = result;
            fieldAccessing.push({ filedIds: secondGroup, readonly: !this.isManunal });
            observer.next(fieldAccessing);
          },
          (error) => {
            this.notificationService.showErrorMessageFromResponse(error);
          }
        );
      } else {
        fieldAccessing.push({ filedIds: secondGroup, readonly: true });
        observer.next(fieldAccessing);
      }
    });
  }
  validateIsManualNumberSeq(service: BuyerAgreementTableService, companyId: string): Observable<boolean> {
    return service.validateIsManualNumberSeq(companyId);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BuyerAgreementTableItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(parentId: string): Observable<BuyerAgreementTableItemView> {
    let model: BuyerAgreementTableItemView = new BuyerAgreementTableItemView();
    model.buyerAgreementTableGUID = EmptyGuid;
    model.buyerTableGUID = parentId;
    return of(model);
  }
  onValidateDate(model: BuyerAgreementTableItemView): TranslateModel {
    // if (isNullOrUndefOrEmpty(value) && !this.firstLoadFlag) {
    //   return {
    //     code: 'ERROR.00639'
    //   };
    // } else
    if (isDateFromGreaterThanDateTo(model.startDate, model.endDate)) {
      return {
        code: 'ERROR.DATE_CANNOT_LESS_THAN',
        parameters: [this.translateService.instant('LABEL.START_DATE'), this.translateService.instant('LABEL.END_DATE')]
      };
    } else {
      return null;
    }
  }
}
