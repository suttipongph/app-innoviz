import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyerAgreementTableItemComponent } from './buyer-agreement-table-item.component';

describe('BuyerAgreementTableItemViewComponent', () => {
  let component: BuyerAgreementTableItemComponent;
  let fixture: ComponentFixture<BuyerAgreementTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BuyerAgreementTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerAgreementTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
