import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BuyerAgreementTableListComponent } from './buyer-agreement-table-list.component';

describe('BuyerAgreementTableListViewComponent', () => {
  let component: BuyerAgreementTableListComponent;
  let fixture: ComponentFixture<BuyerAgreementTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BuyerAgreementTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerAgreementTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
