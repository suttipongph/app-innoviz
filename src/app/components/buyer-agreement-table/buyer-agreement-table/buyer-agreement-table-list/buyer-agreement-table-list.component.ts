import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { BuyerAgreementTableListView } from 'shared/models/viewModel';
import { BuyerAgreementTableService } from '../buyer-agreement-table.service';
import { BuyerAgreementTableListCustomComponent } from './buyer-agreement-table-list-custom.component';

@Component({
  selector: 'buyer-agreement-table-list',
  templateUrl: './buyer-agreement-table-list.component.html',
  styleUrls: ['./buyer-agreement-table-list.component.scss']
})
export class BuyerAgreementTableListComponent extends BaseListComponent<BuyerAgreementTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  buyerTableOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];

  constructor(public custom: BuyerAgreementTableListCustomComponent, private service: BuyerAgreementTableService) {
    super();
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'buyerAgreementTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId'
      },
      {
        label: 'LABEL.BUYER_AGREEMENT_ID',
        textKey: 'buyerAgreementId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.REFERENCE_AGREEMENT_ID',
        textKey: 'referenceAgreementID',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.START_DATE',
        textKey: 'startDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.END_DATE',
        textKey: 'endDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BUYER_AGREEMENT_AMOUNT',
        textKey: 'buyerAgreementAmount',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'buyerTableGUID',
        type: ColumnType.MASTER,
        visibility: false,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<BuyerAgreementTableListView>> {
    return this.service.getBuyerAgreementTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteBuyerAgreementTable(row));
  }
}
