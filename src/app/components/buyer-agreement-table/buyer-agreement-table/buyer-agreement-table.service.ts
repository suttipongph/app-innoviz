import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { BuyerAgreementTableItemView, BuyerAgreementTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class BuyerAgreementTableService {
  serviceKey = 'buyerAgreementTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBuyerAgreementTableToList(search: SearchParameter): Observable<SearchResult<BuyerAgreementTableListView>> {
    const url = `${this.servicePath}/GetBuyerAgreementTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBuyerAgreementTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBuyerAgreementTable`;
    return this.dataGateway.delete(url, row);
  }
  getBuyerAgreementTableById(id: string): Observable<BuyerAgreementTableItemView> {
    const url = `${this.servicePath}/GetBuyerAgreementTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBuyerAgreementTable(vmModel: BuyerAgreementTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBuyerAgreementTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBuyerAgreementTable(vmModel: BuyerAgreementTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBuyerAgreementTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<BuyerAgreementTableItemView> {
    const url = `${this.servicePath}/GetBuyerAgreementTableInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  validateIsManualNumberSeq(companyId: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeq/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
}
