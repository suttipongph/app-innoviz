import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BuyerAgreementTableService } from './buyer-agreement-table.service';
import { BuyerAgreementTableListComponent } from './buyer-agreement-table-list/buyer-agreement-table-list.component';
import { BuyerAgreementTableItemComponent } from './buyer-agreement-table-item/buyer-agreement-table-item.component';
import { BuyerAgreementTableItemCustomComponent } from './buyer-agreement-table-item/buyer-agreement-table-item-custom.component';
import { BuyerAgreementTableListCustomComponent } from './buyer-agreement-table-list/buyer-agreement-table-list-custom.component';
import { BuyerAgreementLineComponentModule } from '../buyer-agreement-line/buyer-agreement-line.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, BuyerAgreementLineComponentModule],
  declarations: [BuyerAgreementTableListComponent, BuyerAgreementTableItemComponent],
  providers: [BuyerAgreementTableService, BuyerAgreementTableItemCustomComponent, BuyerAgreementTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BuyerAgreementTableListComponent, BuyerAgreementTableItemComponent]
})
export class BuyerAgreementTableComponentModule {}
