import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ListInvoiceOutstandingListView } from 'shared/models/viewModel';
import { PageInformationModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { CustomerRefundTableItemView } from 'shared/models/viewModel/customerRefundTableItemView';
@Injectable()
export class ListInvoiceOutstandingService {
  serviceKey = 'listInvoiceOutstandingGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getListInvoiceOutstandingToList(search: SearchParameter): Observable<SearchResult<ListInvoiceOutstandingListView>> {
    const url = `${this.servicePath}/GetListInvoiceOutstandingList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteListInvoiceOutstanding(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteListInvoiceOutstanding`;
    return this.dataGateway.delete(url, row);
  }
  getCustomerRefundTableById(id: string): Observable<CustomerRefundTableItemView> {
    const url = `${this.servicePath}/GetCustomerRefundTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
}
