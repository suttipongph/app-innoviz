import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListInvoiceOutstandingService } from './list-invoice-outstanding.service';
import { ListInvoiceOutstandingListComponent } from './list-invoice-outstanding-list/list-invoice-outstanding-list.component';
import { ListInvoiceOutstandingListCustomComponent } from './list-invoice-outstanding-list/list-invoice-outstanding-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ListInvoiceOutstandingListComponent],
  providers: [ListInvoiceOutstandingService, ListInvoiceOutstandingListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ListInvoiceOutstandingListComponent]
})
export class ListInvoiceOutstandingComponentModule {}
