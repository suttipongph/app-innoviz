import { Observable, of } from 'rxjs';
import { ReceivedFrom, RefType, SuspenseInvoiceType } from 'shared/constants';
import { isNullOrUndefined, isNullOrUndefOrEmpty } from 'shared/functions/value.function';
import { BaseServiceModel } from 'shared/models/systemModel';
import { ListInvoiceOutstandingListView } from 'shared/models/viewModel';
import { ListInvoiceOutstandingService } from '../list-invoice-outstanding.service';
const SUSPENSE_REFUND = 'suspenserefund';
const RETENTION_REFUND = 'retentionrefund';
const RESERVE_REFUND = 'reserverefund';
const allRefund = [SUSPENSE_REFUND, RETENTION_REFUND, RESERVE_REFUND];
export class ListInvoiceOutstandingListCustomComponent {
  baseService: BaseServiceModel<any>;
  title: string = 'LABEL.LIST_OUTSTANDING_INVOICE';
  model: ListInvoiceOutstandingListView = new ListInvoiceOutstandingListView();
  isSuspensesettlement: boolean = false;
  isCustomer: boolean = false;
  isReceipt: boolean = false;
  suspenseInvoiceType: SuspenseInvoiceType = null;
  suspenseInvoiceTypeCondition2: SuspenseInvoiceType = null;

  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setTitle(): void {
    this.isSuspensesettlement = window.location.href.match('suspensesettlement') ? true : false;
    if (this.isSuspensesettlement) {
      this.title = 'LABEL.LIST_OUTSTANDING_SUSPENSE_INVOICE';
    }
  }
  setVariable(): void {
    this.isReceipt = this.model.refType === RefType.ReceiptTemp;
    this.isCustomer = this.model.receivedFrom === ReceivedFrom.Customer;
  }
  setValueByPassParameter(model: ListInvoiceOutstandingListView): Observable<ListInvoiceOutstandingListView> {
    this.setTitle();
    this.getSuspenseAccountBymasterRoute(model);
    if (allRefund.some((s) => window.location.href.match(s)) && isNullOrUndefined(model)) {
      const refund = this.baseService.uiService.getKey('refund');
      const parentKey = this.baseService.uiService.getKey(refund);
      return this.getCustomerRefundTableById(parentKey);
    } else {
      this.model = model;
      this.setVariable();
    }
    return of(this.model);
  }
  getCustomerRefundTableById(parentKey: string): Observable<ListInvoiceOutstandingListView> {
    return new Observable((observable) => {
      (this.baseService.service as ListInvoiceOutstandingService).getCustomerRefundTableById(parentKey).subscribe((result) => {
        this.model.invoiceTable_CustomerTableGUID = result.customerTableGUID;
        this.model.suspenseInvoiceType = result.suspenseInvoiceType;
        this.model.productInvoice = false;
        this.model.refGUID = result.customerRefundTableGUID;
        this.model.refType = RefType.CustomerRefund;
        observable.next(this.model);
      });
    });
  }
  getSuspenseAccountBymasterRoute(model: ListInvoiceOutstandingListView) {
    if (this.isSuspensesettlement) {
      const masterRoute = this.baseService.uiService.getMasterRoute(2);
      switch (masterRoute) {
        case SUSPENSE_REFUND:
          this.suspenseInvoiceType = SuspenseInvoiceType.SuspenseAccount;
          break;
        case RESERVE_REFUND:
          this.suspenseInvoiceType = SuspenseInvoiceType.SuspenseAccount;
          this.suspenseInvoiceTypeCondition2 = SuspenseInvoiceType.ToBeRefunded;
          break;
        case RETENTION_REFUND:
          this.suspenseInvoiceType = SuspenseInvoiceType.SuspenseAccount;
          this.suspenseInvoiceTypeCondition2 = SuspenseInvoiceType.RetentionReturn;
          break;
        default:
          this.suspenseInvoiceType = model.suspenseInvoiceType;
          break;
      }
    } else {
      this.suspenseInvoiceType = model.suspenseInvoiceType;
    }
  }
}
