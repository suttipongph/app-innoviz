import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ListInvoiceOutstandingListComponent } from './list-invoice-outstanding-list.component';

describe('ListInvoiceOutstandingListViewComponent', () => {
  let component: ListInvoiceOutstandingListComponent;
  let fixture: ComponentFixture<ListInvoiceOutstandingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListInvoiceOutstandingListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListInvoiceOutstandingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
