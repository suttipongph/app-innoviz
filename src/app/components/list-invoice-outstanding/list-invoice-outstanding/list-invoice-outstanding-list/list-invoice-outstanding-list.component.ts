import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LineOfBusinessComponentModule } from 'components/setup/line-of-business/line-of-business.module';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable, of } from 'rxjs';
import { BracketType, ColumnType, Operators, RefType, SortType } from 'shared/constants';
import { isNullOrUndefOrEmpty } from 'shared/functions/value.function';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { ListInvoiceOutstandingListView } from 'shared/models/viewModel';
import { ListInvoiceOutstandingService } from '../list-invoice-outstanding.service';
import { ListInvoiceOutstandingListCustomComponent } from './list-invoice-outstanding-list-custom.component';

@Component({
  selector: 'list-invoice-outstanding-list',
  templateUrl: './list-invoice-outstanding-list.component.html',
  styleUrls: ['./list-invoice-outstanding-list.component.scss']
})
export class ListInvoiceOutstandingListComponent extends BaseListComponent<ListInvoiceOutstandingListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  @Output() rowEmit = new EventEmitter<RowIdentity>();
  productTypeOptions: SelectItems[] = [];
  buyerInvoiceTableOptions: SelectItems[] = [];
  buyerAgreementTableOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  invoiceTableOptions: SelectItems[] = [];
  invoiceTypeOptions: SelectItems[] = [];
  defaultBitOptions: SelectItems[] = [];
  suspenseInvoiceTypeOptions: SelectItems[] = [];
  constructor(
    public custom: ListInvoiceOutstandingListCustomComponent,
    private service: ListInvoiceOutstandingService,
    public currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setValueByPassParameter(passingObj).subscribe(() => {
      this.setDataGridOption();
    });
  }
  ngAfterViewInit(): void {
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getDefaultBitDropDown(this.defaultBitOptions);
    this.baseDropdown.getSuspenseInvoiceTypeEnumDropDown(this.suspenseInvoiceTypeOptions);
  }

  checkAccessMode(): void {}
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getInvoiceTableDropDown(this.invoiceTableOptions);
    this.baseDropdown.getInvoiceTypeDropDown(this.invoiceTypeOptions);
    if (!this.custom.isSuspensesettlement) {
      this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
      this.baseDropdown.getBuyerInvoiceTableDropDown(this.buyerInvoiceTableOptions);
      this.baseDropdown.getBuyerAgreementTableDropDown(this.buyerAgreementTableOptions);
    }
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.canCreate = false;
    this.option.canCreate = false;
    this.option.canView = true;
    this.option.canDelete = false;
    this.option.key = 'invoiceTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions,
        duplicateKey: 'productType'
      },
      {
        label: 'LABEL.INVOICE_ID',
        textKey: 'invoiceTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'invoiceTableGUID',
        sortingKey: 'invoiceTable_InvoiceId',
        masterList: this.invoiceTableOptions
      },
      {
        label: 'LABEL.INVOICE_TYPE_ID',
        textKey: 'invoiceType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'invoiceTypeGUID',
        sortingKey: 'invoiceType_InvoiceTypeId',
        masterList: this.invoiceTypeOptions
      },
      {
        label: this.custom.isSuspensesettlement ? null : 'LABEL.PRODUCT_INVOICE',
        textKey: 'productInvoice',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defaultBitOptions,
        duplicateKey: 'productInvoice'
      },
      {
        label: this.custom.isSuspensesettlement ? 'LABEL.SUSPENSE_INVOICE_TYPE' : null,
        textKey: 'suspenseInvoiceType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.suspenseInvoiceTypeOptions,
        duplicateKey: 'suspenseInvoiceType'
      },
      {
        label: 'LABEL.INVOICE_AMOUNT',
        textKey: 'invoiceAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: 'LABEL.DUE_DATE',
        textKey: 'dueDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: this.custom.isSuspensesettlement ? null : 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'buyerTableGUID',
        sortingKey: 'buyerTable_BuyerId',
        masterList: this.buyerTableOptions
      },
      {
        label: this.custom.isSuspensesettlement ? null : 'LABEL.DOCUMENT_ID',
        textKey: 'documentId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: this.custom.isSuspensesettlement ? null : 'LABEL.BUYER_AGREEMENT_ID',
        textKey: 'buyerAgreementTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'buyerAgreementTableGUID',
        sortingKey: 'buyerAgreementTable_BuyerAgreementId',
        masterList: this.buyerAgreementTableOptions
      },
      {
        label: this.custom.isSuspensesettlement ? null : 'LABEL.BUYER_INVOICE_ID',
        textKey: 'buyerInvoiceTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'buyerInvoiceTableGUID',
        sortingKey: 'buyerInvoiceTable_BuyerInvoiceId',
        masterList: this.buyerInvoiceTableOptions
      },
      {
        label: 'LABEL.BALANCE_AMOUNT',
        textKey: 'balanceAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: null,
        textKey: 'refType',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.custom.model.refType.toString()
      },
      {
        label: null,
        textKey: 'exitsInvoice',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: 'false'
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        bracket: BracketType.DoubleEnd,
        equalityOperator: Operators.NOT_EQUAL,
        parentKey: this.custom.model.refGUID
      },
      {
        label: null,
        textKey: 'invoiceTable_CustomerTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.custom.model.invoiceTable_CustomerTableGUID
      },
      {
        label: null,
        textKey: 'suspenseInvoiceType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        bracket: this.custom.suspenseInvoiceTypeCondition2 != null ? BracketType.SingleStart : BracketType.None,
        parentKey: this.custom.suspenseInvoiceType != null ? this.custom.suspenseInvoiceType.toString() : null
      },
      {
        label: null,
        textKey: 'suspenseInvoiceType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        operator: Operators.OR,
        bracket: this.custom.suspenseInvoiceTypeCondition2 != null ? BracketType.SingleEnd : BracketType.None,
        parentKey: this.custom.suspenseInvoiceTypeCondition2 != null ? this.custom.suspenseInvoiceTypeCondition2.toString() : null,
        duplicateKey: 'suspenseInvoiceTypeCondition2'
      },
      {
        label: null,
        textKey: 'productInvoice',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.custom.model.productInvoice.toString(), // AND ( productInvoice == @0
        bracket: this.custom.isCustomer && this.custom.isReceipt && !this.custom.isSuspensesettlement ? BracketType.SingleStart : BracketType.None,
        duplicateKey: 'productInvoice2'
      }
    ];
    if (!this.custom.isSuspensesettlement && this.custom.isReceipt) {
      if (this.custom.isCustomer) {
        columns.push({
          label: null,
          textKey: 'productInvoice',
          type: ColumnType.MASTER,
          visibility: this.custom.isCustomer ? true : false,
          sorting: SortType.NONE,
          parentKey: (!this.custom.model.productInvoice).toString(),
          operator: Operators.OR,
          bracket: BracketType.SingleStart // AND ( productInvoice == @0 OR ( productInvoice == @1
        });
      }
      columns.push({
        label: null,
        textKey: 'productType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: !isNullOrUndefOrEmpty(this.custom.model.productType) ? this.custom.model.productType.toString() : null,
        bracket: this.custom.isCustomer ? BracketType.DoubleEnd : BracketType.None
      });
      // isReqcept && isCustomer: // AND ( productInvoice == @0 OR ( productInvoice == @1 AND productType == @2))
      // isReqcept && isBuyer (is not isCustomer): productType == @2
      if (!this.custom.isCustomer) {
        columns.push({
          label: null,
          textKey: 'buyerTableGUID',
          type: ColumnType.MASTER,
          visibility: true,
          sorting: SortType.NONE,
          parentKey: this.custom.model.buyerTableGUID
        });
      }
    }
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<ListInvoiceOutstandingListView>> {
    return this.service.getListInvoiceOutstandingToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    this.rowEmit.emit(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteListInvoiceOutstanding(row));
  }
}
