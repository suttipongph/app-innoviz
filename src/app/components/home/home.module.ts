import { HomeTaskListComponent } from './home-task-list/home-task-list.component';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeTaskListCustomComponent } from './home-task-list/home-task-list-custom.component';
import { HomeItemComponent } from './home-item/home-item.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [HomeTaskListComponent, HomeItemComponent],
  providers: [HomeTaskListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [HomeTaskListComponent, HomeItemComponent]
})
export class HomeComponentModule {}
