import { Component, Input, OnInit } from '@angular/core';
import { BaseComponent } from 'core/components/base/base.component';
import { ColumnType, K2WorklistStatus, SortType } from 'shared/constants';
import { ColumnModel, OptionModel, PageInformationModel } from 'shared/models/systemModel';

@Component({
  selector: 'home-item',
  templateUrl: './home-item.component.html',
  styleUrls: ['./home-item.component.scss']
})
export class HomeItemComponent extends BaseComponent implements OnInit {
  openedOption: OptionModel;
  activeOption: OptionModel;
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
  }
  constructor() {
    super();
  }

  ngOnInit(): void {
    this.setOption();
  }
  setOption(): void {
    this.openedOption = new OptionModel();
    this.openedOption.key = 'formURL';
    const openedColumns: ColumnModel[] = [
      {
        label: null,
        textKey: 'status',
        type: ColumnType.STRING,
        visibility: false,
        sorting: SortType.NONE,
        parentKey: K2WorklistStatus.Open
      }
    ];
    this.openedOption.columns = openedColumns;

    this.activeOption = new OptionModel();
    this.activeOption.key = 'formURL';
    const activeColumns: ColumnModel[] = [
      {
        label: null,
        textKey: 'status',
        type: ColumnType.STRING,
        visibility: false,
        sorting: SortType.NONE,
        parentKey: K2WorklistStatus.Available
      }
    ];
    this.activeOption.columns = activeColumns;
  }
}
