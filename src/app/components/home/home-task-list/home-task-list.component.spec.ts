import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeTaskListComponent } from './home-task-list.component';

describe('HomeListComponent', () => {
  let component: HomeTaskListComponent;
  let fixture: ComponentFixture<HomeTaskListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HomeTaskListComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeTaskListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
