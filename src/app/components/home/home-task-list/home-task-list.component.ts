import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { ColumnType, SortType } from 'shared/constants';
import { isUndefinedOrZeroLength } from 'shared/functions/value.function';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  TranslateModel
} from 'shared/models/systemModel';
import { TaskItemView, TaskListParm, WorkflowInstanceView } from 'shared/models/viewModel';
import { HomeTaskListCustomComponent } from './home-task-list-custom.component';

@Component({
  selector: 'home-task-list',
  templateUrl: './home-task-list.component.html',
  styleUrls: ['./home-task-list.component.scss']
})
export class HomeTaskListComponent extends BaseListComponent<TaskItemView> implements BaseListInterface {
  inputId: string;
  title: string;
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.inputId = isUndefinedOrZeroLength(param.pageInputId) ? 'TASK_LIST' : param.pageInputId;
    this.title = this.custom.setTitle(this.inputId);
  }

  constructor(public custom: HomeTaskListCustomComponent) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {
    this.title = this.custom.setTitle(this.inputId);
    this.custom.setIsViewWorkflow();
  }
  ngAfterViewInit(): void {
    if (this.custom.isViewWorkflow) {
      this.checkAccessMode();
      this.onEnumLoader();
    }
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'formURL';
    this.option.canCreate = false;
    this.option.canView = true;
    this.option.canDelete = false;
    const columns: ColumnModel[] = [
      { label: 'LABEL.WORKFLOW_NAME', textKey: 'name', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE },
      { label: 'LABEL.ACTIVITY_NAME', textKey: 'actInstDestDisplayName', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE },
      { label: 'LABEL.FOLIO', textKey: 'folio', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE },
      { label: 'LABEL.START_DATE_TIME', textKey: 'startDate', type: ColumnType.DATERANGE, visibility: true, sorting: SortType.NONE },
      { label: 'LABEL.ORIGINATOR', textKey: 'originator', type: ColumnType.STRING, visibility: true, sorting: SortType.NONE, disabledFilter: true },
      {
        label: 'LABEL.ORIGINAL_DESTINATION',
        textKey: 'allocatedUser',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE,
        disabledFilter: true
      },
      {
        label: null,
        textKey: 'status',
        type: ColumnType.STRING,
        visibility: false,
        sorting: SortType.NONE,
        parentKey: this.custom.worklistItemStatus
      }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<TaskItemView>> {
    const instance: WorkflowInstanceView = new WorkflowInstanceView();
    instance.impersonateUser = this.userDataService.getUsernameFromToken();
    const parm: TaskListParm = new TaskListParm();
    parm.search = search;
    parm.workflow = instance;

    return this.k2Service.getTaskList(parm).pipe(
      catchError((error) => {
        const message: TranslateModel = { code: 'ERROR.ERROR', parameters: [] };
        const content: TranslateModel = { code: 'ERROR.00663', parameters: [] };
        this.notificationService.showError({ topic: message, content: content });
        throw error;
      })
    );
  }
  onCreate(row: RowIdentity): void {}
  onView(row: RowIdentity): void {
    this.router.navigate(['/']).then((result) => {
      window.location.href = row.guid;
    });
  }
  onDelete(row: RowIdentity): void {}
}
