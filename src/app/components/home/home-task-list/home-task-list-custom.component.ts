import { Observable } from 'rxjs';
import { isInterfacedWithK2 } from 'shared/config/globalvar.config';
import { AppConst } from 'shared/constants';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView } from 'shared/models/viewModel';

export class HomeTaskListCustomComponent {
  baseService: BaseServiceModel<any>;
  accessModeView: AccessModeView = new AccessModeView();
  isViewWorkflow: boolean = false;
  title: string = '';
  worklistItemStatus: string = '';
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setIsViewWorkflow(): boolean {
    if (isInterfacedWithK2() && this.baseService.userDataService.getSiteLogin() !== AppConst.OB) {
      this.isViewWorkflow = true;
    }
    return this.isViewWorkflow;
  }
  setTitle(inputId: string): string {
    const label = inputId !== 'ACTIVE_TASK_LIST' ? ('OPENED_TASK_LIST' ? 'LABEL.OPENED_TASK_LIST' : 'LABEL.TASK_LIST') : 'LABEL.ACTIVE_TASK_LIST';
    return label;
  }
}
