import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NCBTransListComponent } from './ncb-trans-list.component';

describe('NCBTransListViewComponent', () => {
  let component: NCBTransListComponent;
  let fixture: ComponentFixture<NCBTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NCBTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NCBTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
