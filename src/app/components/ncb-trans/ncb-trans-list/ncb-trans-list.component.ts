import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, CreditAppRequestStatus, SortType } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { NCBTransListView } from 'shared/models/viewModel';
import { NCBTransService } from '../ncb-trans.service';
import { NCBTransListCustomComponent } from './ncb-trans-list-custom.component';
@Component({
  selector: 'ncb-trans-list',
  templateUrl: './ncb-trans-list.component.html',
  styleUrls: ['./ncb-trans-list.component.scss']
})
export class NCBTransListComponent extends BaseListComponent<NCBTransListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  bankGroupOptions: SelectItems[] = [];
  creditTypeOptions: SelectItems[] = [];
  ncbAccountStatusOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];

  constructor(
    public custom: NCBTransListCustomComponent,
    public service: NCBTransService,
    private uiControllerService: UIControllerService,
    private currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoOriginTableName();
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setRefTypeRefGUID(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.setAccessModeByParentStatus(this.isWorkFlowMode()).subscribe((result) => {
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBankGroupDropDown(this.bankGroupOptions);
    this.baseDropdown.getCreditTypeDropDown(this.creditTypeOptions);
    this.baseDropdown.getNCBAccountStatusDropDown(this.ncbAccountStatusOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'ncbTransGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.CREDIT_TYPE_ID',
        textKey: 'creditType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.ASC,
        masterList: this.creditTypeOptions,
        sortingKey: 'creditType_CreditTypeId',
        searchingKey: 'creditTypeGUID'
      },
      {
        label: 'LABEL.CREDIT_LIMIT',
        textKey: 'creditLimit',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: 'LABEL.OUTSTANDING_AR',
        textKey: 'outstandingAR',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: 'LABEL.MONTHLY_REPAYMENT',
        textKey: 'monthlyRepayment',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: 'LABEL.END_DATE',
        textKey: 'endDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.FINANCIAL_INSTITUTION_ID',
        textKey: 'bankGroup_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.bankGroupOptions,
        sortingKey: 'bankGroup_BankGroupId',
        searchingKey: 'bankGroupGUID'
      },
      {
        label: 'LABEL.NCB_ACCOUNT_STATUS_ID',
        textKey: 'ncbAccountStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.DESC,
        masterList: this.ncbAccountStatusOptions,
        sortingKey: 'ncbAccountStatus_NCBAccStatusId',
        searchingKey: 'ncbAccountStatusGUID'
      },
      {
        label: null,
        textKey: 'createdDateTime',
        type: ColumnType.DATE,
        visibility: false,
        sorting: SortType.DESC,
        sortingKey: 'createdDateTime',
        searchingKey: 'createdDateTime'
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: isNullOrUndefined(this.custom.refGUID) ? this.parentId : this.custom.refGUID
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<NCBTransListView>> {
    return this.service.getNCBTransToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteNCBTrans(row));
  }
}
