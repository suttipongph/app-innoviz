import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CreditAppRequestStatus, RefType } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel, RefTypeModel } from 'shared/models/systemModel';
import { AccessModeView } from 'shared/models/viewModel';

const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
export class NCBTransListCustomComponent {
  refGUID: string;
  refType: RefType;
  baseService: BaseServiceModel<any>;
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setAccessModeByParentStatus(isWorkFlowMode: boolean): Observable<AccessModeView> {
    let parentId = this.baseService.uiService.getRelatedInfoOriginTableKey();
    this.parentName = this.baseService.uiService.getRelatedInfoOriginTableName();
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        return this.baseService.service.getAccessModeByCreditAppRequestTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
            this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
            this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
            this.accessModeView.canView = true;
          })
        );
      default:
        if (this.refType === RefType.CreditAppRequestTable) {
          return this.baseService.service.getAccessModeByCreditAppRequestTable(this.refGUID).pipe(
            tap((result) => {
              this.accessModeView = result as AccessModeView;
              const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
              this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
              this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
              this.accessModeView.canView = true;
            })
          );
        } else if (this.refType === RefType.AuthorizedPersonTrans) {
          return this.baseService.service.getAccessModeCARequestTableByAuthorizedperson(this.refGUID).pipe(
            tap((result) => {
              this.accessModeView = result as AccessModeView;
              const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
              this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
              this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
              this.accessModeView.canView = true;
            })
          );
        } else {
          return of(this.accessModeView);
        }
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
  setRefTypeRefGUID(passingObj: RefTypeModel): void {
    if (!isNullOrUndefined(passingObj)) {
      this.refType = passingObj.refType;
      this.refGUID = passingObj.refGUID;
    }
  }
}
