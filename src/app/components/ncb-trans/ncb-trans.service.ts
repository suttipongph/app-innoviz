import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, NCBTransItemView, NCBTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class NCBTransService {
  serviceKey = 'ncbTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getNCBTransToList(search: SearchParameter): Observable<SearchResult<NCBTransListView>> {
    const url = `${this.servicePath}/GetNCBTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteNCBTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteNCBTrans`;
    return this.dataGateway.delete(url, row);
  }
  getNCBTransById(id: string): Observable<NCBTransItemView> {
    const url = `${this.servicePath}/GetNCBTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createNCBTrans(vmModel: NCBTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateNCBTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateNCBTrans(vmModel: NCBTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateNCBTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getAccessModeByCreditAppRequestTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppRequestTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getInitialData(id: string): Observable<NCBTransItemView> {
    const url = `${this.servicePath}/GetNCBTransInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeCARequestTableByAuthorizedperson(authorizedId: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/getAccessModeCARequestTableByAuthorizedperson/id=${authorizedId}`;
    return this.dataGateway.get(url);
  }
}
