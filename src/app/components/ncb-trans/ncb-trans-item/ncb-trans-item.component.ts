import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { NCBTransItemView } from 'shared/models/viewModel';
import { NCBTransService } from '../ncb-trans.service';
import { NCBTransItemCustomComponent } from './ncb-trans-item-custom.component';
@Component({
  selector: 'ncb-trans-item',
  templateUrl: './ncb-trans-item.component.html',
  styleUrls: ['./ncb-trans-item.component.scss']
})
export class NCBTransItemComponent extends BaseItemComponent<NCBTransItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  bankGroupOptions: SelectItems[] = [];
  creditTypeOptions: SelectItems[] = [];
  ncbAccountStatusOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];

  constructor(public service: NCBTransService, private currentActivatedRoute: ActivatedRoute, public custom: NCBTransItemCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
    this.custom.originName = this.uiService.getRelatedInfoOriginTableName();
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setRefTypeRefGUID(passingObj);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkPageMode();
    this.checkAccessMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<NCBTransItemView> {
    return this.service.getNCBTransById(this.id);
  }
  getInitialData(): Observable<NCBTransItemView> {
    return this.service.getInitialData(isNullOrUndefOrEmptyGUID(this.custom.refGUID) ? this.parentId : this.custom.refGUID);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(
      this.baseDropdown.getBankGroupDropDown(),
      this.baseDropdown.getCreditTypeDropDown(),
      this.baseDropdown.getNCBAccountStatusDropDown()
    ).subscribe(
      ([bankGroup, creditType, ncbAccountStatus]) => {
        this.bankGroupOptions = bankGroup;
        this.creditTypeOptions = creditType;
        this.ncbAccountStatusOptions = ncbAccountStatus;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model, this.isWorkFlowMode()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateNCBTrans(this.model), isColsing);
    } else {
      super.onCreate(this.service.createNCBTrans(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
