import { TranslateService } from '@ngx-translate/core';
import { AppInjector } from 'app-injector';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { CreditAppRequestStatus, EmptyGuid, RefType } from 'shared/constants';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, RefTypeModel } from 'shared/models/systemModel';
import { AccessModeView, NCBTransItemView } from 'shared/models/viewModel';

const firstGroup = ['REF_TYPE', 'REF_ID'];
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const CREDIT_APP_TABLE = 'creditapptable';
const AMEND_CA = 'amendca';
const BUYER_MATCHING = 'buyermatching';
const LOAN_REQUEST = 'loanrequest';
export class NCBTransItemCustomComponent {
  refGUID: string;
  refType: RefType;
  translateService = AppInjector.get(TranslateService);
  baseService: BaseServiceModel<any>;
  originName = null;
  accessModeView: AccessModeView = new AccessModeView();
  getFieldAccessing(model: NCBTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: NCBTransItemView, isWorkFlowMode: boolean): Observable<boolean> {
    return this.setAccessModeByParentStatus(model, isWorkFlowMode).pipe(
      map((result) => (isUpdateMode(model.ncbTransGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  getInitialData(): Observable<NCBTransItemView> {
    let model = new NCBTransItemView();
    model.ncbTransGUID = EmptyGuid;
    return of(model);
  }
  setRefTypeRefGUID(passingObj: RefTypeModel): void {
    if (!isNullOrUndefined(passingObj)) {
      this.refType = passingObj.refType;
      this.refGUID = passingObj.refGUID;
    }
  }
  setAccessModeByParentStatus(model: NCBTransItemView, isWorkFlowMode: boolean): Observable<AccessModeView> {
    let parentId = this.baseService.uiService.getRelatedInfoOriginTableKey();
    this.originName = this.baseService.uiService.getRelatedInfoOriginTableName();
    const parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (this.originName) {
      case CREDIT_APP_REQUEST_TABLE:
        return this.baseService.service.getAccessModeByCreditAppRequestTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
            this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
            this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
            this.accessModeView.canView = isDraftStatus ? true : this.accessModeView.canView && isWorkFlowMode;
          })
        );
      case CREDIT_APP_TABLE:
        if (parentName === AMEND_CA || parentName === BUYER_MATCHING || parentName === LOAN_REQUEST) {
          return this.baseService.service.getAccessModeByCreditAppRequestTable(this.refGUID).pipe(
            tap((result) => {
              this.accessModeView = result as AccessModeView;
              const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
              this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
              this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
              this.accessModeView.canView = isDraftStatus ? true : this.accessModeView.canView && isWorkFlowMode;
            })
          );
        } else if (this.refType === RefType.AuthorizedPersonTrans) {
          return this.baseService.service.getAccessModeCARequestTableByAuthorizedperson(this.refGUID).pipe(
            tap((result) => {
              this.accessModeView = result as AccessModeView;
              const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
              this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
              this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
              this.accessModeView.canView = isDraftStatus ? true : this.accessModeView.canView && isWorkFlowMode;
            })
          );
        } else {
          return this.baseService.service.getAccessModeByCreditAppRequestTable(parentId).pipe(
            tap((result) => {
              this.accessModeView = result as AccessModeView;
              const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
              this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
              this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
              this.accessModeView.canView = isDraftStatus ? true : this.accessModeView.canView && isWorkFlowMode;
            })
          );
        }
      default:
        if (this.refType === RefType.AuthorizedPersonTrans) {
          return this.baseService.service.getAccessModeByCreditAppRequestTable(this.refGUID).pipe(
            tap((result) => {
              this.accessModeView = result as AccessModeView;
              const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
              this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
              this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
              this.accessModeView.canView = isDraftStatus ? true : this.accessModeView.canView && isWorkFlowMode;
            })
          );
        } else {
          return of(this.accessModeView);
        }
    }
  }
}
