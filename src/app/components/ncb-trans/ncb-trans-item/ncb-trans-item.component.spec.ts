import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NCBTransItemComponent } from './ncb-trans-item.component';

describe('NCBTransItemViewComponent', () => {
  let component: NCBTransItemComponent;
  let fixture: ComponentFixture<NCBTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NCBTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NCBTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
