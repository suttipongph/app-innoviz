import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NCBTransService } from './ncb-trans.service';
import { NCBTransListComponent } from './ncb-trans-list/ncb-trans-list.component';
import { NCBTransItemComponent } from './ncb-trans-item/ncb-trans-item.component';
import { NCBTransItemCustomComponent } from './ncb-trans-item/ncb-trans-item-custom.component';
import { NCBTransListCustomComponent } from './ncb-trans-list/ncb-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [NCBTransListComponent, NCBTransItemComponent],
  providers: [NCBTransService, NCBTransItemCustomComponent, NCBTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [NCBTransListComponent, NCBTransItemComponent]
})
export class NCBTransComponentModule {}
