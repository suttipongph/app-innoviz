import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { StagingTableIntercoInvSettleItemView, StagingTableIntercoInvSettleListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class StagingTableIntercoInvSettleService {
  serviceKey = 'stagingTableIntercoInvSettleGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getStagingTableIntercoInvSettleToList(search: SearchParameter): Observable<SearchResult<StagingTableIntercoInvSettleListView>> {
    const url = `${this.servicePath}/GetStagingTableIntercoInvSettleList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteStagingTableIntercoInvSettle(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteStagingTableIntercoInvSettle`;
    return this.dataGateway.delete(url, row);
  }
  getStagingTableIntercoInvSettleById(id: string): Observable<StagingTableIntercoInvSettleItemView> {
    const url = `${this.servicePath}/GetStagingTableIntercoInvSettleById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createStagingTableIntercoInvSettle(vmModel: StagingTableIntercoInvSettleItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateStagingTableIntercoInvSettle`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateStagingTableIntercoInvSettle(vmModel: StagingTableIntercoInvSettleItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateStagingTableIntercoInvSettle`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
