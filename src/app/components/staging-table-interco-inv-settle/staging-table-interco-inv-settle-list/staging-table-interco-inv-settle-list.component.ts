import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { StagingTableIntercoInvSettleListView } from 'shared/models/viewModel';
import { StagingTableIntercoInvSettleService } from '../staging-table-interco-inv-settle.service';
import { StagingTableIntercoInvSettleListCustomComponent } from './staging-table-interco-inv-settle-list-custom.component';

@Component({
  selector: 'staging-table-interco-inv-settle-list',
  templateUrl: './staging-table-interco-inv-settle-list.component.html',
  styleUrls: ['./staging-table-interco-inv-settle-list.component.scss']
})
export class StagingTableIntercoInvSettleListComponent extends BaseListComponent<StagingTableIntercoInvSettleListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  interfaceStatusOptions: SelectItems[] = [];
  recordTypeOptions: SelectItems[] = [];

  constructor(public custom: StagingTableIntercoInvSettleListCustomComponent, private service: StagingTableIntercoInvSettleService) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.custom.baseService.uiService = this.uiService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getInterfaceStatusEnumDropDown(this.interfaceStatusOptions);
    this.baseDropdown.getRecordTypeEnumDropDown(this.recordTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'stagingTableIntercoInvSettleGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.INTERCOMPANY_INVOICE_SETTLEMENT_ID',
        textKey: 'intercompanyInvoiceSettlementId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.INVOICE_DATE',
        textKey: 'invoiceDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DUE_DATE',
        textKey: 'dueDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.METHOD_OF_PAYMENT_ID',
        textKey: 'methodOfPaymentId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.SETTLE_INVOICE_AMOUNT',
        textKey: 'settleInvoiceAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.FEE_LEDGER_ACCOUNT',
        textKey: 'feeLedgerAccount',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.NAME',
        textKey: 'name',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.INTERFACE_STAGING_BATCH_ID',
        textKey: 'interfaceStagingBatchId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.INTERFACE_STATUS',
        textKey: 'interfaceStatus',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.interfaceStatusOptions
      }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<StagingTableIntercoInvSettleListView>> {
    return this.service.getStagingTableIntercoInvSettleToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteStagingTableIntercoInvSettle(row));
  }
}
