import { Observable } from 'rxjs';
import { ColumnType, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { BaseServiceModel, ColumnModel } from 'shared/models/systemModel';
import { IntercompanyInvoiceSettlementItemView } from 'shared/models/viewModel';

export class StagingTableIntercoInvSettleListCustomComponent {
  baseService: BaseServiceModel<any>;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
}
