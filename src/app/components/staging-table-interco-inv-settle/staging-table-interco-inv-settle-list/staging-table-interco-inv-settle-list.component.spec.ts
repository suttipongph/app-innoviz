import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StagingTableIntercoInvSettleListComponent } from './staging-table-interco-inv-settle-list.component';

describe('StagingTableIntercoInvSettleListViewComponent', () => {
  let component: StagingTableIntercoInvSettleListComponent;
  let fixture: ComponentFixture<StagingTableIntercoInvSettleListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StagingTableIntercoInvSettleListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StagingTableIntercoInvSettleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
