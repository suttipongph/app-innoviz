import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StagingTableIntercoInvSettleService } from './staging-table-interco-inv-settle.service';
import { StagingTableIntercoInvSettleListComponent } from './staging-table-interco-inv-settle-list/staging-table-interco-inv-settle-list.component';
import { StagingTableIntercoInvSettleItemComponent } from './staging-table-interco-inv-settle-item/staging-table-interco-inv-settle-item.component';
import { StagingTableIntercoInvSettleItemCustomComponent } from './staging-table-interco-inv-settle-item/staging-table-interco-inv-settle-item-custom.component';
import { StagingTableIntercoInvSettleListCustomComponent } from './staging-table-interco-inv-settle-list/staging-table-interco-inv-settle-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [StagingTableIntercoInvSettleListComponent, StagingTableIntercoInvSettleItemComponent],
  providers: [StagingTableIntercoInvSettleService, StagingTableIntercoInvSettleItemCustomComponent, StagingTableIntercoInvSettleListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [StagingTableIntercoInvSettleListComponent, StagingTableIntercoInvSettleItemComponent]
})
export class StagingTableIntercoInvSettleComponentModule {}
