import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { StagingTableIntercoInvSettleItemView } from 'shared/models/viewModel';

const firstGroup = [
  'INTERCOMPANY_INVOICE_SETTLEMENT_ID',
  'INVOICE_DATE',
  'DUE_DATE',
  'METHOD_OF_PAYMENT_ID',
  'TAX_CODE',
  'SETTLE_INVOICE_AMOUNT',
  'FEE_LEDGER_ACCOUNT',
  'INVOICE_TEXT',
  'ORIG_INVOICE_ID',
  'ORIG_INVOICE_AMOUNT',
  'CN_REASON_ID',
  'COMPANY_ID',
  'RECORD_TYPE',
  'CUSTOMER_ID',
  'NAME',
  'ALT_NAME',
  'CUST_GROUP_ID',
  'TAX_ID',
  'CURRENCY_ID',
  'CUST_PHONE_VALUE',
  'CUST_FAX_VALUE',
  'ADDRESS1',
  'COUNTRY_ID',
  'PROVINCE_ID',
  'DISTRICT_ID',
  'SUB_DISTRICT_ID',
  'POSTAL_CODE',
  'TAX_BRANCH_ID',
  'DIMENSION_CODE1',
  'DIMENSION_CODE2',
  'DIMENSION_CODE3',
  'DIMENSION_CODE4',
  'DIMENSION_CODE5',
  'INTERFACE_STAGING_BATCH_ID',
  'INTERFACE_STATUS',
  'INTERCOMPANY_INVOICE_SETTLEMENT_GUID',
  'STAGING_TABLE_INTERCO_INV_SETTLE_GUID',
  'COMPANY_TAX_BRANCH_ID'
];

export class StagingTableIntercoInvSettleItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: StagingTableIntercoInvSettleItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: StagingTableIntercoInvSettleItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(): Observable<StagingTableIntercoInvSettleItemView> {
    let model = new StagingTableIntercoInvSettleItemView();
    model.stagingTableIntercoInvSettleGUID = EmptyGuid;
    return of(model);
  }
}
