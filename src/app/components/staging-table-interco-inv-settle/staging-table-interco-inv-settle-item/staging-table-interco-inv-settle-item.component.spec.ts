import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StagingTableIntercoInvSettleItemComponent } from './staging-table-interco-inv-settle-item.component';

describe('StagingTableIntercoInvSettleItemViewComponent', () => {
  let component: StagingTableIntercoInvSettleItemComponent;
  let fixture: ComponentFixture<StagingTableIntercoInvSettleItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StagingTableIntercoInvSettleItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StagingTableIntercoInvSettleItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
