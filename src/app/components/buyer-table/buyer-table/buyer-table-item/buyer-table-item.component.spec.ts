import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyerTableItemComponent } from './buyer-table-item.component';

describe('BuyerTableItemViewComponent', () => {
  let component: BuyerTableItemComponent;
  let fixture: ComponentFixture<BuyerTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BuyerTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
