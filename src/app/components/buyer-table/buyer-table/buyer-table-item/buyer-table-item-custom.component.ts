import { AppInjector } from 'app-injector';
import { NotificationService } from 'core/services/notification.service';
import { Observable, of } from 'rxjs';
import { IdentificationType } from 'shared/constants';
import { isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { BuyerTableItemView } from 'shared/models/viewModel';
import { BuyerTableService } from '../buyer-table.service';

const firstGroup = ['BUYER_ID'];
const secondGroup = ['PASSPORT_ID', 'WORK_PERMIT_ID'];
const thridGroup = ['TAX_ID'];
const readonlyForever = ['BLACKLIST_STATUS_GUID'];

export class BuyerTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  isManunal: boolean = false;
  isTax: boolean = false;
  notificationService = AppInjector.get(NotificationService);
  getFieldAccessing(model: BuyerTableItemView, buyerTableService: BuyerTableService): Observable<any> {
    let fieldAccessing: FieldAccessing[] = [{ filedIds: readonlyForever, readonly: true }];
    return new Observable((observer) => {
      // set firstGroup
      fieldAccessing.push(...this.setFieldAccessingByIdentificationType(model.identificationType));
      if (!isUpdateMode(model.buyerTableGUID)) {
        this.validateIsManualNumberSeq(buyerTableService, model.companyGUID).subscribe(
          (result) => {
            this.isManunal = result;
            fieldAccessing.push({ filedIds: firstGroup, readonly: !this.isManunal });
            observer.next(fieldAccessing);
          },
          (error) => {
            this.notificationService.showErrorMessageFromResponse(error);
          }
        );
      } else {
        fieldAccessing.push({ filedIds: firstGroup, readonly: true });
        observer.next(fieldAccessing);
      }
    });
  }
  setFieldAccessingByIdentificationType(value: number): FieldAccessing[] {
    this.conditionByIdentificationType(value);
    return [
      { filedIds: secondGroup, readonly: this.isTax },
      { filedIds: thridGroup, readonly: !this.isTax }
    ];
  }

  conditionByIdentificationType(value: number): void {
    this.isTax = value === IdentificationType.TaxId ? true : false;
  }

  validateIsManualNumberSeq(buyerTableService: BuyerTableService, companyId: string): Observable<boolean> {
    return buyerTableService.validateIsManualNumberSeq(companyId);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BuyerTableItemView): Observable<boolean> {
    const accessright = isNullOrUndefOrEmptyGUID(model.buyerTableGUID) ? canCreate : canUpdate;
    const accessLogic = true;
    return of(!(accessLogic && accessright));
  }
  setValueByIdentificationType(model: BuyerTableItemView): void {
    if (this.isTax) {
      model.passportId = '';
      model.workPermitId = '';
    } else {
      model.taxId = '';
    }
  }
}
