import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { RefType, ROUTE_FUNCTION_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { BuyerTableItemView, RefIdParm } from 'shared/models/viewModel';
import { BuyerTableService } from '../buyer-table.service';
import { BuyerTableItemCustomComponent } from './buyer-table-item-custom.component';
@Component({
  selector: 'buyer-table-item',
  templateUrl: './buyer-table-item.component.html',
  styleUrls: ['./buyer-table-item.component.scss']
})
export class BuyerTableItemComponent extends BaseItemComponent<BuyerTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  blacklistStatusOptions: SelectItems[] = [];
  businessSegmentOptions: SelectItems[] = [];
  businessSizeOptions: SelectItems[] = [];
  businessTypeOptions: SelectItems[] = [];
  creditScoringOptions: SelectItems[] = [];
  currencyOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  identificationTypeOptions: SelectItems[] = [];
  kycOptions: SelectItems[] = [];
  lineOfBusinessOptions: SelectItems[] = [];

  constructor(private service: BuyerTableService, private currentActivatedRoute: ActivatedRoute, public custom: BuyerTableItemCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getIdentificationTypeEnumDropDown(this.identificationTypeOptions);
  }
  getById(): Observable<BuyerTableItemView> {
    return this.service.getBuyerTableById(this.id);
  }
  getInitialData(): Observable<BuyerTableItemView> {
    return this.service.getInitialData(this.userDataService.getCurrentCompanyGUID());
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }
  onAsyncRunner(model?: any): void {
    forkJoin(
      this.baseDropdown.getBlacklistStatusDropDown(),
      this.baseDropdown.getBusinessSegmentDropDown(),
      this.baseDropdown.getBusinessSizeDropDown(),
      this.baseDropdown.getBusinessTypeDropDown(),
      this.baseDropdown.getCreditScoringDropDown(),
      this.baseDropdown.getCurrencyDropDown(),
      this.baseDropdown.getDocumentStatusDropDown(),
      this.baseDropdown.getKYCSetupDropDown(),
      this.baseDropdown.getLineOfBusinessDropDown()
    ).subscribe(
      ([blacklistStatus, businessSegment, businessSize, businessType, creditScoring, currency, documentStatus, kyc, lineOfBusiness]) => {
        this.blacklistStatusOptions = blacklistStatus;
        this.businessSegmentOptions = businessSegment;
        this.businessSizeOptions = businessSize;
        this.businessTypeOptions = businessType;
        this.creditScoringOptions = creditScoring;
        this.currencyOptions = currency;
        this.documentStatusOptions = documentStatus;
        this.kycOptions = kyc;
        this.lineOfBusinessOptions = lineOfBusiness;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model, this.service).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        command: () => {
          const refIdParm: RefIdParm = { refType: RefType.Buyer, refGUID: this.model.buyerTableGUID };
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: refIdParm
          });
        }
      },
      {
        label: 'LABEL.BUYER_AGREEMENT',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.BUYER_AGREEMENT })
      },
      {
        label: 'LABEL.BUYER_CREDIT_LIMIT_BY_PRODUCT',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.BUYER_CREDIT_LIMIT_BY_PRODUCT })
      },
      {
        label: 'LABEL.MEMO',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.MEMO })
      },
      {
        label: 'LABEL.BUYER_TRANSACTION',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.BUYER_TRANSACTION })
      },
      {
        label: 'LABEL.RELATED_PERSON',
        items: [
          {
            label: 'LABEL.AUTHORIZED_PERSON',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.AUTHORIZED_PERSON_TRANS })
          },
          {
            label: 'LABEL.CONTACT_PERSON',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.CONTACT_PERSON })
          }
        ]
      },
      {
        label: 'LABEL.MANAGE',
        items: [
          {
            label: 'LABEL.ADDRESS',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.ADDRESS_TRANS })
          },
          {
            label: 'LABEL.CONTACT',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.CONTACT })
          },
          {
            label: 'LABEL.DOCUMENT_CONDITION',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.DOCUMENT_CONDITION_INFO })
          }
        ]
      },
      {
        label: 'LABEL.FINANCIAL',
        items: [
          {
            label: 'LABEL.FINANCIAL_STATEMENT',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.FINANCIAL_STATEMENT_TRANS,
                parameters: {
                  refType: RefType.Buyer,
                  refGUID: this.model.buyerTableGUID
                }
              })
          }
        ]
      },
      {
        label: 'LABEL.INQUIRY',
        items: [
          {
            label: 'LABEL.INQUIRY_ASSIGNMENT_AGREEMENT_OUTSTANDING',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.INQUIRY_ASSIGNMENT_AGREEMENT_OUTSTANDING,
                parameters: {
                  refGUID: this.model.buyerTableGUID,
                  refType: RefType.Buyer
                }
              })
          },
          {
            label: 'LABEL.BUYER_CREDIT_OF_ALL_CUSTOMER',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.BUYER_CREDIT_OF_ALL_CUSTOMER,
                parameters: {
                  refGUID: this.model.buyerTableGUID,
                  refType: RefType.Buyer
                }
              })
          }
        ]
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(): void {
    this.functionItems = [
      {
        label: 'LABEL.UPDATE_BLACKLIST_AND_WATCH_LIST_STATUS',
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.BUYERTABLE_UPDATE_BLACKLIST_AND_WATCHLIST_STATUS })
      }
    ];
    super.setFunctionOptionsMapping();
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateBuyerTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createBuyerTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onIdentificationTypeChange(e: any): void {
    super.setBaseFieldAccessing(this.custom.setFieldAccessingByIdentificationType(e));
    this.custom.setValueByIdentificationType(this.model);
  }
  onIdentificationTypeBinding(e: any): void {
    super.setBaseFieldAccessing(this.custom.setFieldAccessingByIdentificationType(e));
  }
}
