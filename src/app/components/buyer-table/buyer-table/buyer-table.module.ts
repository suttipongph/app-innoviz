import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BuyerTableService } from './buyer-table.service';
import { BuyerTableListComponent } from './buyer-table-list/buyer-table-list.component';
import { BuyerTableItemComponent } from './buyer-table-item/buyer-table-item.component';
import { BuyerTableItemCustomComponent } from './buyer-table-item/buyer-table-item-custom.component';
import { BuyerTableListCustomComponent } from './buyer-table-list/buyer-table-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BuyerTableListComponent, BuyerTableItemComponent],
  providers: [BuyerTableService, BuyerTableItemCustomComponent, BuyerTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BuyerTableListComponent, BuyerTableItemComponent]
})
export class ComponentBuyerTableModule {}
