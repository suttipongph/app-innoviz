import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BuyerTableItemView, BuyerTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class BuyerTableService {
  serviceKey = 'buyerTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBuyerTableToList(search: SearchParameter): Observable<SearchResult<BuyerTableListView>> {
    const url = `${this.servicePath}/GetBuyerTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBuyerTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBuyerTable`;
    return this.dataGateway.delete(url, row);
  }
  getBuyerTableById(id: string): Observable<BuyerTableItemView> {
    const url = `${this.servicePath}/GetBuyerTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBuyerTable(vmModel: BuyerTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBuyerTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBuyerTable(vmModel: BuyerTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBuyerTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  validateIsManualNumberSeq(companyId: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeq/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
  getInitialData(companyId: string): Observable<any> {
    const url = `${this.servicePath}/GetBuyerTableInitialData/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
}
