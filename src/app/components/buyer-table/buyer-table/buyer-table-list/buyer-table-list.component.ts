import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { BuyerTableListView } from 'shared/models/viewModel';
import { BuyerTableService } from '../buyer-table.service';
import { BuyerTableListCustomComponent } from './buyer-table-list-custom.component';

@Component({
  selector: 'buyer-table-list',
  templateUrl: './buyer-table-list.component.html',
  styleUrls: ['./buyer-table-list.component.scss']
})
export class BuyerTableListComponent extends BaseListComponent<BuyerTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  blacklistStatusOptions: SelectItems[] = [];
  businessSegmentOptions: SelectItems[] = [];
  businessSizeOptions: SelectItems[] = [];
  businessTypeOptions: SelectItems[] = [];
  creditScoringOptions: SelectItems[] = [];
  currencyOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  identificationTypeOptions: SelectItems[] = [];
  kycOptions: SelectItems[] = [];
  lineOfBusinessOptions: SelectItems[] = [];

  constructor(public custom: BuyerTableListCustomComponent, private service: BuyerTableService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getIdentificationTypeEnumDropDown(this.identificationTypeOptions);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'buyerTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BUYER_NAME',
        textKey: 'buyerName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PASSPORT_ID',
        textKey: 'passportId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.TAX_ID',
        textKey: 'taxId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentStatusOptions,
        searchingKey: 'documentStatusGUID',
        sortingKey: 'documentStatus_StatusId'
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<BuyerTableListView>> {
    return this.service.getBuyerTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteBuyerTable(row));
  }
}
