import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BuyerTableListComponent } from './buyer-table-list.component';

describe('BuyerTableListViewComponent', () => {
  let component: BuyerTableListComponent;
  let fixture: ComponentFixture<BuyerTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BuyerTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
