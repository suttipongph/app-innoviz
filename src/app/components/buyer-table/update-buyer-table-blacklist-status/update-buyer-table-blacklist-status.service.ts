import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { UpdateBuyerTableBlacklistStatusParamView, UpdateBuyerTableBlacklistStatusResultView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class UpdateBuyerTableBlacklistStatusService {
  serviceKey = 'customerTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getUpdateBuyerTableBlacklistStatusById(id: string): Observable<UpdateBuyerTableBlacklistStatusResultView> {
    const url = `${this.servicePath}/GetUpdateBuyerTableBlacklistAndWatchlistStatusById/id=${id}`;
    return this.dataGateway.get(url);
  }
  updateBuyerTableBlacklistStatus(vmModel: UpdateBuyerTableBlacklistStatusParamView): Observable<UpdateBuyerTableBlacklistStatusResultView> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vmModel);
  }
}
