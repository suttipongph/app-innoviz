import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateBuyerTableBlacklistStatusService } from './update-buyer-table-blacklist-status.service';
import { UpdateBuyerTableBlacklistStatusComponent } from './update-buyer-table-blacklist-status/update-buyer-table-blacklist-status.component';
import { UpdateBuyerTableBlacklistStatusCustomComponent } from './update-buyer-table-blacklist-status/update-buyer-table-blacklist-status-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [UpdateBuyerTableBlacklistStatusComponent],
  providers: [UpdateBuyerTableBlacklistStatusService, UpdateBuyerTableBlacklistStatusCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [UpdateBuyerTableBlacklistStatusComponent]
})
export class UpdateBuyerTableBlacklistStatusComponentModule {}
