import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateBuyerTableBlacklistStatusComponent } from './update-buyer-table-blacklist-status.component';

describe('UpdateBuyerTableBlacklistStatusViewComponent', () => {
  let component: UpdateBuyerTableBlacklistStatusComponent;
  let fixture: ComponentFixture<UpdateBuyerTableBlacklistStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UpdateBuyerTableBlacklistStatusComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateBuyerTableBlacklistStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
