import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { RefType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { UpdateBuyerTableBlacklistStatusParamView, UpdateBuyerTableBlacklistStatusResultView } from 'shared/models/viewModel';
import { UpdateBuyerTableBlacklistStatusService } from '../update-buyer-table-blacklist-status.service';
import { UpdateBuyerTableBlacklistStatusCustomComponent } from './update-buyer-table-blacklist-status-custom.component';
@Component({
  selector: 'update-buyer-table-blacklist-status',
  templateUrl: './update-buyer-table-blacklist-status.component.html',
  styleUrls: ['./update-buyer-table-blacklist-status.component.scss']
})
export class UpdateBuyerTableBlacklistStatusComponent
  extends BaseItemComponent<UpdateBuyerTableBlacklistStatusResultView>
  implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  documentReasonOptions: SelectItems[] = [];
  blacklistStatusOptions: SelectItems[] = [];

  constructor(
    private service: UpdateBuyerTableBlacklistStatusService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: UpdateBuyerTableBlacklistStatusCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialUpdatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<UpdateBuyerTableBlacklistStatusResultView> {
    return this.service.getUpdateBuyerTableBlacklistStatusById(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {}

  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getDocumentReasonDropDown(RefType.Buyer.toString()), this.baseDropdown.getBlacklistStatusDropDown()).subscribe(
      ([documentReason, blacklistStatus]) => {
        this.documentReasonOptions = documentReason;
        this.blacklistStatusOptions = blacklistStatus;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing().subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {}
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.custom.getModelParam(this.model).subscribe((model) => {
          this.onSubmit(true, model);
        });
      }
    });
  }
  onSubmit(isColsing: boolean, model: UpdateBuyerTableBlacklistStatusParamView): void {
    super.onExecuteFunction(this.service.updateBuyerTableBlacklistStatus(model));
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onBlacklistStatusChange() {
    return this.custom.validateBlacklistStatus(this.model);
  }
}
