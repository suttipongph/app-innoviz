import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import {
  UpdateBuyerTableBlacklistStatusParamView,
  UpdateBuyerTableBlacklistStatusResultView,
  UpdateCustomerTableBlacklistStatusParamView,
  UpdateCustomerTableBlacklistStatusResultView
} from 'shared/models/viewModel';

const firstGroup = ['BUYER_ID', 'ORIGINAL_BLACKLIST_STATUS'];

export class UpdateBuyerTableBlacklistStatusCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getModelParam(model: UpdateBuyerTableBlacklistStatusResultView): Observable<UpdateBuyerTableBlacklistStatusParamView> {
    const param: UpdateBuyerTableBlacklistStatusParamView = new UpdateBuyerTableBlacklistStatusParamView();
    param.buyerTableGUID = model.buyerTableGUID;
    param.blacklistStatusGUID = model.blacklistStatusGUID;
    return of(param);
  }
  getRemakReason(id: string, options: SelectItems[]): Observable<SelectItems> {
    const item = options.find((f) => f.value === id);
    return of(item);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  validateBlacklistStatus(model: UpdateBuyerTableBlacklistStatusResultView): any {
    if (model.originalBlacklistStatusGUID != null && model.originalBlacklistStatusGUID == model.blacklistStatusGUID) {
      return {
        code: 'ERROR.90021',
        parameters: []
      };
    }
    return null;
  }
}
