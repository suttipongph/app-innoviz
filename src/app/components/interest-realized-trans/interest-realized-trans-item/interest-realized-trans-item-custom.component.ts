import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { InterestRealizedTransItemView } from 'shared/models/viewModel';

const firstGroup = [
  'DOCUMENT_ID',
  'PRODUCT_TYPE',
  'LINE_NUM',
  'START_DATE',
  'END_DATE',
  'ACCOUNTING_DATE',
  'INTEREST_DAY',
  'ACC_INTEREST_AMOUNT',
  'INTEREST_PER_DAY',
  'ACCRUED',
  'REF_TYPE',
  'REF_ID',
  'REF_GUID',
  'INTEREST_REALIZED_TRANS_GUID',
  'DIMENSION1GUID',
  'DIMENSION2GUID',
  'DIMENSION3GUID',
  'DIMENSION4GUID',
  'DIMENSION5GUID',
  'REF_PROCESSTRANS_GUID',
  'PROCESS_TRANSACTION_DATE'
];

export class InterestRealizedTransItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: InterestRealizedTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: InterestRealizedTransItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(): Observable<InterestRealizedTransItemView> {
    let model = new InterestRealizedTransItemView();
    model.interestRealizedTransGUID = EmptyGuid;
    return of(model);
  }
}
