import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterestRealizedTransItemComponent } from './interest-realized-trans-item.component';

describe('InterestRealizedTransItemViewComponent', () => {
  let component: InterestRealizedTransItemComponent;
  let fixture: ComponentFixture<InterestRealizedTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InterestRealizedTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestRealizedTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
