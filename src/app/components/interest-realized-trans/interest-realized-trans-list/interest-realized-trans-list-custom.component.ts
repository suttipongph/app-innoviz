import { Observable } from 'rxjs';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { BaseServiceModel } from 'shared/models/systemModel';

export class InterestRealizedTransListCustomComponent {
  baseService: BaseServiceModel<any>;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  getParentKey(): string {
    let activeParentName = this.baseService.uiService.getRelatedInfoActiveTableName();
    switch (activeParentName) {
      case ROUTE_RELATED_GEN.PURCHASE_TABLE_PURCHASE:
        return this.baseService.uiService.getKey(ROUTE_RELATED_GEN.PURCHASE_LINE_PURCHASE);
      case ROUTE_RELATED_GEN.PURCHASE_TABLE_ROLL_BILL:
        return this.baseService.uiService.getKey(ROUTE_RELATED_GEN.PURCHASE_LINE_ROLL_BILL);
    }
    let parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (parentName) {
      case ROUTE_RELATED_GEN.WITHDRAWAL_LINE_WITHDRAWAL:
      case ROUTE_RELATED_GEN.WITHDRAWAL_LINE_TERM_EXTENSION:
        return this.baseService.uiService.getRelatedInfoParentTableKey();
    }
  }
}
