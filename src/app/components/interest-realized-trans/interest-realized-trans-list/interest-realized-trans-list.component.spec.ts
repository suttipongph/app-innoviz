import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InterestRealizedTransListComponent } from './interest-realized-trans-list.component';

describe('InterestRealizedTransListViewComponent', () => {
  let component: InterestRealizedTransListComponent;
  let fixture: ComponentFixture<InterestRealizedTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InterestRealizedTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestRealizedTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
