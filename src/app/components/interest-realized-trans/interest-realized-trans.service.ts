import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { InterestRealizedTransItemView, InterestRealizedTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class InterestRealizedTransService {
  serviceKey = 'interestRealizedTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getInterestRealizedTransToList(search: SearchParameter): Observable<SearchResult<InterestRealizedTransListView>> {
    const url = `${this.servicePath}/GetInterestRealizedTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteInterestRealizedTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteInterestRealizedTrans`;
    return this.dataGateway.delete(url, row);
  }
  getInterestRealizedTransById(id: string): Observable<InterestRealizedTransItemView> {
    const url = `${this.servicePath}/GetInterestRealizedTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createInterestRealizedTrans(vmModel: InterestRealizedTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateInterestRealizedTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateInterestRealizedTrans(vmModel: InterestRealizedTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateInterestRealizedTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
