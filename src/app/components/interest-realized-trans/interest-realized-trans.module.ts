import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InterestRealizedTransService } from './interest-realized-trans.service';
import { InterestRealizedTransListComponent } from './interest-realized-trans-list/interest-realized-trans-list.component';
import { InterestRealizedTransItemComponent } from './interest-realized-trans-item/interest-realized-trans-item.component';
import { InterestRealizedTransItemCustomComponent } from './interest-realized-trans-item/interest-realized-trans-item-custom.component';
import { InterestRealizedTransListCustomComponent } from './interest-realized-trans-list/interest-realized-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [InterestRealizedTransListComponent, InterestRealizedTransItemComponent],
  providers: [InterestRealizedTransService, InterestRealizedTransItemCustomComponent, InterestRealizedTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [InterestRealizedTransListComponent, InterestRealizedTransItemComponent]
})
export class InterestRealizedTransComponentModule {}
