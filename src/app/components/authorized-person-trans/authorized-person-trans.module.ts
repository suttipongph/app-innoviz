import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthorizedPersonTransService } from './authorized-person-trans.service';
import { AuthorizedPersonTransListComponent } from './authorized-person-trans-list/authorized-person-trans-list.component';
import { AuthorizedPersonTransItemComponent } from './authorized-person-trans-item/authorized-person-trans-item.component';
import { AuthorizedPersonTransItemCustomComponent } from './authorized-person-trans-item/authorized-person-trans-item-custom.component';
import { AuthorizedPersonTransListCustomComponent } from './authorized-person-trans-list/authorized-person-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [AuthorizedPersonTransListComponent, AuthorizedPersonTransItemComponent],
  providers: [AuthorizedPersonTransService, AuthorizedPersonTransItemCustomComponent, AuthorizedPersonTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [AuthorizedPersonTransListComponent, AuthorizedPersonTransItemComponent]
})
export class AuthorizedPersonTransComponentModule {}
