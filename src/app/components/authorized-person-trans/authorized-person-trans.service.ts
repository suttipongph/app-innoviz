import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { AccessModeView, AuthorizedPersonTransItemView, AuthorizedPersonTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class AuthorizedPersonTransService {
  serviceKey = 'authorizedPersonTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getAuthorizedPersonTransToList(search: SearchParameter): Observable<SearchResult<AuthorizedPersonTransListView>> {
    const url = `${this.servicePath}/GetAuthorizedPersonTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteAuthorizedPersonTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteAuthorizedPersonTrans`;
    return this.dataGateway.delete(url, row);
  }
  getAuthorizedPersonTransById(id: string): Observable<AuthorizedPersonTransItemView> {
    const url = `${this.servicePath}/GetAuthorizedPersonTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createAuthorizedPersonTrans(vmModel: AuthorizedPersonTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateAuthorizedPersonTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateAuthorizedPersonTrans(vmModel: AuthorizedPersonTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateAuthorizedPersonTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<AuthorizedPersonTransItemView> {
    const url = `${this.servicePath}/GetAuthorizedPersonTransInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeAuthorizedPersonTransByCreditAppRequestTable(creditAppRequestId: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeAuthorizedPersonTransByCreditAppRequestTable/creditAppRequestId=${creditAppRequestId}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByCreditAppRequestTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppRequestTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAuthorizedPersonTransInitialListData(amendCAId: string): Observable<AuthorizedPersonTransListView> {
    const url = `${this.servicePath}/GetAuthorizedPersonTransInitialListData/amendCAId=${amendCAId}`;
    return this.dataGateway.get(url);
  }
}
