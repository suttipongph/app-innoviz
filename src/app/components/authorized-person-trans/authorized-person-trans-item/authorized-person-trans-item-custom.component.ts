import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConst, CreditAppRequestStatus, RefType } from 'shared/constants';
import { isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, AuthorizedPersonTransItemView, RelatedPersonTableItemView } from 'shared/models/viewModel';

const firstGroup = [
  'NAME',
  'IDENTIFICATION_TYPE',
  'TAX_ID',
  'PASSPORT_ID',
  'WORK_PERMIT_ID',
  'POSITION',
  'PHONE',
  'EXTENSION',
  'MOBILE',
  'FAX',
  'EMAIL',
  'LINE_ID',
  'DATE_OF_BIRTH',
  'AGE',
  'NATIONALITY_ID',
  'RACE_ID',
  'REF_TYPE',
  'REF_ID',
  'BACKGROUND_SUMMARY'
];
const secondGroup = ['RELATED_PERSON_TABLE_GUID'];
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const CUSTOMER_TABLE = 'customertable';
const BUYER_TABLE = 'buyertable';
const CREDIT_APP_TABLE = 'creditapptable';
const AMEND_CA = 'amendca';
const inAciveoOnly = ['IN_ACTIVE'];
const REPLACE = ['REPLACE'];
const REPLACED_AUTHORIZED_PERSON_TRANS_GUID = ['REPLACED_AUTHORIZED_PERSON_TRANS_GUID'];

export class AuthorizedPersonTransItemCustomComponent {
  baseService: BaseServiceModel<any>;
  parentId: string = null;
  parentName: string = null;
  currentName: string = null;
  accessModeView: AccessModeView = new AccessModeView();
  addGetFieldAccessingByParent: boolean = false;
  originName: string = null;
  enableNCB: boolean = false;
  getFieldAccessing(model: AuthorizedPersonTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.authorizedPersonTransGUID)) {
      fieldAccessing.push({ filedIds: secondGroup, readonly: true });

      if (this.addGetFieldAccessingByParent) {
        fieldAccessing.push(...this.getFieldAccessingByParent(model));
      } else {
        fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      }
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    }
    fieldAccessing.push({ filedIds: REPLACE, readonly: !this.addGetFieldAccessingByParent });
    fieldAccessing.push({ filedIds: REPLACED_AUTHORIZED_PERSON_TRANS_GUID, readonly: !this.addGetFieldAccessingByParent });
    return of(fieldAccessing);
  }
  getFieldAccessingByParent(model: AuthorizedPersonTransItemView): FieldAccessing[] {
    const fieldAccessing: FieldAccessing[] = [];
    if (!isNullOrUndefOrEmptyGUID(model.refAuthorizedPersonTransGUID)) {
      fieldAccessing.push(
        ...[
          { filedIds: [AppConst.ALL_ID], readonly: true },
          { filedIds: inAciveoOnly, readonly: false },
          { filedIds: REPLACE, readonly: false },
          { filedIds: REPLACED_AUTHORIZED_PERSON_TRANS_GUID, readonly: !model.replace }
        ]
      );
    } else {
      fieldAccessing.push(
        ...[
          { filedIds: firstGroup, readonly: true },
          { filedIds: REPLACE, readonly: true },
          { filedIds: REPLACED_AUTHORIZED_PERSON_TRANS_GUID, readonly: true }
        ]
      );
    }
    return fieldAccessing;
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: AuthorizedPersonTransItemView, isWorkFlowMode: boolean): Observable<boolean> {
    return this.setAccessModeByParent(model, isWorkFlowMode).pipe(
      map((result) => (isUpdateMode(model.authorizedPersonTransGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  onRelatedPersonChange(model: AuthorizedPersonTransItemView, option: SelectItems[]): void {
    this.setValueByRelatedPerson(model, option);
  }
  setDefaultValue(model: AuthorizedPersonTransItemView): void {
    model.relatedPersonTable_IdentificationType = null;
  }
  setValueByRelatedPerson(model: AuthorizedPersonTransItemView, option: SelectItems[]): void {
    const row = isNullOrUndefOrEmptyGUID(model.relatedPersonTableGUID)
      ? new RelatedPersonTableItemView()
      : (option.find((o) => o.value === model.relatedPersonTableGUID).rowData as RelatedPersonTableItemView);
    model.relatedPersonTable_Name = row.name;
    model.relatedPersonTable_IdentificationType = row.identificationType;
    model.relatedPersonTable_TaxId = row.taxId;
    model.relatedPersonTable_PassportId = row.passportId;
    model.relatedPersonTable_WorkPermitId = row.workPermitId;
    model.relatedPersonTable_DateOfBirth = row.dateOfBirth;
    model.relatedPersonTable_Phone = row.phone;
    model.relatedPersonTable_Extension = row.extension;
    model.relatedPersonTable_Fax = row.fax;
    model.relatedPersonTable_Mobile = row.mobile;
    model.relatedPersonTable_LineId = row.lineId;
    model.relatedPersonTable_Email = row.email;
    model.relatedPersonTable_NationalityId = row.nationality_Values;
    model.relatedPersonTable_RaceId = row.race_Values;
    model.relatedPersonTable_Age = row.age;
    model.relatedPersonTable_BackgroundSummary = row.backgroundSummary;
  }
  setAccessModeByParent(model: AuthorizedPersonTransItemView, isWorkFlowMode: boolean): Observable<AccessModeView> {
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        return this.getAccessModeByCreditAppRequestTable(isWorkFlowMode);
      case CUSTOMER_TABLE:
        this.accessModeView.canCreate = true;
        this.accessModeView.canDelete = false;
        this.accessModeView.canView = true;
        return of(this.accessModeView);
      case BUYER_TABLE:
        this.accessModeView.canCreate = true;
        this.accessModeView.canDelete = false;
        this.accessModeView.canView = true;
        return of(this.accessModeView);
      case CREDIT_APP_TABLE:
        return this.getAccessModeByCreditAppTable(isWorkFlowMode);
      default:
        return of(this.accessModeView);
    }
  }
  getAccessModeByCreditAppTable(isWorkFlowMode: boolean): Observable<AccessModeView> {
    switch (this.currentName) {
      case AMEND_CA:
        return this.getAccessModeAuthorizedPersonTransByCreditAppRequestTable(isWorkFlowMode);
      default:
        // CREDIT_APP_TABLE
        this.accessModeView.canCreate = false;
        this.accessModeView.canDelete = false;
        this.accessModeView.canView = false;
        return of(this.accessModeView);
    }
  }
  getAccessModeByCreditAppRequestTable(isWorkFlowMode: boolean): Observable<AccessModeView> {
    return this.baseService.service.getAccessModeByCreditAppRequestTable(this.parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
        const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
        this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
        this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
        this.accessModeView.canView = isDraftStatus ? true : this.accessModeView.canView && isWorkFlowMode;
      })
    );
  }
  getAccessModeAuthorizedPersonTransByCreditAppRequestTable(isWorkFlowMode: boolean): Observable<AccessModeView> {
    return this.baseService.service.getAccessModeAuthorizedPersonTransByCreditAppRequestTable(this.parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
        const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
        this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
        this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
        this.accessModeView.canView = isDraftStatus ? true : this.accessModeView.canView && isWorkFlowMode;
      })
    );
  }
  getRelatedInfoTableKey(): string {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    this.currentName = this.baseService.uiService.getRelatedInfoActiveTableName();
    this.originName = this.baseService.uiService.getRelatedInfoOriginTableName();
    switch (this.currentName) {
      case AMEND_CA:
        this.parentId = this.baseService.uiService.getRelatedInfoActiveTableKey();
        this.addGetFieldAccessingByParent = true;
        break;
      default:
        this.parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
        break;
    }
    if (this.parentName === CREDIT_APP_REQUEST_TABLE || this.currentName === AMEND_CA) {
      this.enableNCB = true;
    }
    return this.parentId;
  }
  getRelatedPersonTableDropDown(model: AuthorizedPersonTransItemView): Observable<SelectItems[]> {
    switch (this.currentName) {
      case AMEND_CA:
        return this.baseService.baseDropdown.getRelatedPersonTableByCreditAppTableDropDown(model.refGUID);
      default:
        if (this.parentName === CREDIT_APP_REQUEST_TABLE) {
          return this.baseService.baseDropdown.getRelatedPersonTableByCreditAppRequestTableAuthorizedPersonTransDropDown(model.refGUID);
        } else {
          return this.baseService.baseDropdown.getRelatedPersonTableDropDown();
        }
    }
  }

  getReplacedAuthorizedPersonTrans(model: AuthorizedPersonTransItemView): Observable<SelectItems[]> {
    switch (this.currentName) {
      case AMEND_CA:
        return this.baseService.baseDropdown.getRelatedPersonTableByAmendCADropDown(model.refGUID);
      default:
        return of([]);
    }
  }
  onReplaceChange(model: AuthorizedPersonTransItemView): FieldAccessing[] {
    if (!model.replace) {
      model.replacedAuthorizedPersonTransGUID = null;
    }
    return [{ filedIds: REPLACED_AUTHORIZED_PERSON_TRANS_GUID, readonly: !model.replace }];
  }
}
