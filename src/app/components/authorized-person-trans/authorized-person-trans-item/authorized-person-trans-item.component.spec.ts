import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorizedPersonTransItemComponent } from './authorized-person-trans-item.component';

describe('AuthorizedPersonTransItemViewComponent', () => {
  let component: AuthorizedPersonTransItemComponent;
  let fixture: ComponentFixture<AuthorizedPersonTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AuthorizedPersonTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorizedPersonTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
