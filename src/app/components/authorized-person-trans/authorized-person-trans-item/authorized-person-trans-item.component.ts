import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { RefType, ROUTE_RELATED_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmpty, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { AuthorizedPersonTransItemView } from 'shared/models/viewModel';
import { AuthorizedPersonTransService } from '../authorized-person-trans.service';
import { AuthorizedPersonTransItemCustomComponent } from './authorized-person-trans-item-custom.component';
@Component({
  selector: 'authorized-person-trans-item',
  templateUrl: './authorized-person-trans-item.component.html',
  styleUrls: ['./authorized-person-trans-item.component.scss']
})
export class AuthorizedPersonTransItemComponent extends BaseItemComponent<AuthorizedPersonTransItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  authorizedPersonTypeOptions: SelectItems[] = [];
  identificationTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  relatedPersonTableOptions: SelectItems[] = [];
  authorizedPersonTransOptions: SelectItems[] = [];
  replacedAuthorizedPersonTransOptions: SelectItems[] = [];
  CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
  constructor(
    public service: AuthorizedPersonTransService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: AuthorizedPersonTransItemCustomComponent,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.custom.getRelatedInfoTableKey();
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getIdentificationTypeEnumDropDown(this.identificationTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<AuthorizedPersonTransItemView> {
    return this.service.getAuthorizedPersonTransById(this.id);
  }
  getInitialData(): Observable<AuthorizedPersonTransItemView> {
    return this.service.getInitialData(this.parentId);
  }

  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
      this.custom.setDefaultValue(this.model);
    });
  }

  onAsyncRunner(model?: any): void {
    const tempModel = isNullOrUndefOrEmpty(model) ? this.model : model;
    forkJoin([
      this.baseDropdown.getAuthorizedPersonTypeDropDown(),
      this.custom.getRelatedPersonTableDropDown(tempModel),
      this.custom.getReplacedAuthorizedPersonTrans(tempModel)
    ]).subscribe(
      ([authorizedPersonType, relatedPersonTable, replacedAuthorizedPersonTrans]) => {
        this.authorizedPersonTypeOptions = authorizedPersonType;
        this.relatedPersonTableOptions = relatedPersonTable;
        this.replacedAuthorizedPersonTransOptions = replacedAuthorizedPersonTrans;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model, this.isWorkFlowMode()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {
    if (this.custom.enableNCB) {
      // set hide related button because no item
      this.relatedInfoItems = [
        {
          label: 'LABEL.MANAGE',
          items: [
            {
              label: 'LABEL.NCB',
              command: () =>
                this.toRelatedInfo({
                  path: ROUTE_RELATED_GEN.NCB_TRANS,
                  parameters: {
                    refGUID: this.model.authorizedPersonTransGUID,
                    refType: RefType.AuthorizedPersonTrans
                  }
                })
            }
          ]
        }
      ];
    }

    super.setRelatedinfoOptionsMapping();
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateAuthorizedPersonTrans(this.model), isColsing);
    } else {
      super.onCreate(this.service.createAuthorizedPersonTrans(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onReplaceChange(): void {
    super.setBaseFieldAccessing(this.custom.onReplaceChange(this.model));
  }
}
