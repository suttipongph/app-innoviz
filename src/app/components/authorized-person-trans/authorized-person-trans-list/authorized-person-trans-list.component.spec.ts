import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AuthorizedPersonTransListComponent } from './authorized-person-trans-list.component';

describe('AuthorizedPersonTransListViewComponent', () => {
  let component: AuthorizedPersonTransListComponent;
  let fixture: ComponentFixture<AuthorizedPersonTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AuthorizedPersonTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorizedPersonTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
