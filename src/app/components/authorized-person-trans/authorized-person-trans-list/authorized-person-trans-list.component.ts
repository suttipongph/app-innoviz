import { Component, Input } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { AuthorizedPersonTransListView } from 'shared/models/viewModel';
import { AuthorizedPersonTransService } from '../authorized-person-trans.service';
import { AuthorizedPersonTransListCustomComponent } from './authorized-person-trans-list-custom.component';

@Component({
  selector: 'authorized-person-trans-list',
  templateUrl: './authorized-person-trans-list.component.html',
  styleUrls: ['./authorized-person-trans-list.component.scss']
})
export class AuthorizedPersonTransListComponent extends BaseListComponent<AuthorizedPersonTransListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  authorizedPersonTypeOptions: SelectItems[] = [];
  identificationTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  relatedPersonTableOptions: SelectItems[] = [];
  defualtBitOption: SelectItems[] = [];

  constructor(
    public custom: AuthorizedPersonTransListCustomComponent,
    public service: AuthorizedPersonTransService,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {
    this.accessRightChildPage = 'AuthorizedPersonTransListPage';
    this.checkAccessMode();
  }
  ngAfterViewInit(): void {
    this.onEnumLoader();
    this.custom.setInitialListData(this.isWorkFlowMode()).subscribe((result) => {
      this.model = result as AuthorizedPersonTransListView;
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getIdentificationTypeEnumDropDown(this.identificationTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.baseDropdown.getDefaultBitDropDown(this.defualtBitOption);
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.custom.checkAccessMode(this.accessRightChildPage));
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getAuthorizedPersonTypeDropDown(this.authorizedPersonTypeOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'authorizedPersonTransGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.ORDERING',
        textKey: 'ordering',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.NAME',
        textKey: 'relatedPersonTable_Name',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.TAX_ID',
        textKey: 'relatedPersonTable_TaxId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PASSPORT_ID',
        textKey: 'relatedPersonTable_PassportId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.AUTHORIZED_PERSON_TYPE_ID',
        textKey: 'authorizedPersonType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.authorizedPersonTypeOptions,
        searchingKey: 'authorizedPersonTypeGUID',
        sortingKey: 'authorizedPersonType_AuthorizedPersonTypeId'
      },
      {
        label: 'LABEL.INACTIVE',
        textKey: 'inActive',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defualtBitOption
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.model.refGUID
      },
      {
        label: null,
        textKey: 'refType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.model.refType.toString()
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<AuthorizedPersonTransListView>> {
    return this.service.getAuthorizedPersonTransToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteAuthorizedPersonTrans(row));
  }
  getRowAuthorize(row: AuthorizedPersonTransListView[]): void {
    if (this.custom.getRowAuthorize(row)) {
      row.forEach((item) => {
        item.rowAuthorize = this.getSingleRowAuthorize(item.rowAuthorize, item);
      });
    } else {
      super.getRowAuthorize(row);
    }
  }
}
