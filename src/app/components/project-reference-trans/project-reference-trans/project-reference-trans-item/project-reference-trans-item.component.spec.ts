import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectReferenceTransItemComponent } from './project-reference-trans-item.component';

describe('ProjectReferenceTransItemViewComponent', () => {
  let component: ProjectReferenceTransItemComponent;
  let fixture: ComponentFixture<ProjectReferenceTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProjectReferenceTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectReferenceTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
