import { Observable, of } from 'rxjs';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { ProjectReferenceTransItemView } from 'shared/models/viewModel';

const firstGroup = ['REF_TYPE', 'REF_ID'];

const secondGroup = ['PROJECT_COMPANY_NAME', 'PROJECT_NAME'];

export class ProjectReferenceTransItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: ProjectReferenceTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    if (isUpdateMode(model.projectReferenceTransGUID)) {
      fieldAccessing.push({ filedIds: secondGroup, readonly: true });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ProjectReferenceTransItemView): Observable<boolean> {
    return isUpdateMode(model.projectReferenceTransGUID) ? of(!canUpdate) : of(!canCreate);
  }
}
