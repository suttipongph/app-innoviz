import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProjectReferenceTransListComponent } from './project-reference-trans-list.component';

describe('ProjectReferenceTransListViewComponent', () => {
  let component: ProjectReferenceTransListComponent;
  let fixture: ComponentFixture<ProjectReferenceTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProjectReferenceTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectReferenceTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
