import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { ProjectReferenceTransListView } from 'shared/models/viewModel';
import { ProjectReferenceTransService } from '../project-reference-trans.service';
import { ProjectReferenceTransListCustomComponent } from './project-reference-trans-list-custom.component';

@Component({
  selector: 'project-reference-trans-list',
  templateUrl: './project-reference-trans-list.component.html',
  styleUrls: ['./project-reference-trans-list.component.scss']
})
export class ProjectReferenceTransListComponent extends BaseListComponent<ProjectReferenceTransListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  refTypeOptions: SelectItems[] = [];

  constructor(public custom: ProjectReferenceTransListCustomComponent, private service: ProjectReferenceTransService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'projectReferenceTransGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.PROJECT_COMPANY_NAME',
        textKey: 'projectCompanyName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PROJECT_NAME',
        textKey: 'projectName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PROJECT_VALUE',
        textKey: 'projectValue',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: 'LABEL.PROJECT_COMPLETION__YEAR',
        textKey: 'projectCompletion',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE,
        format: this.INTEGER
      },
      {
        label: 'LABEL.PROJECT_STATUS',
        textKey: 'projectStatus',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<ProjectReferenceTransListView>> {
    return this.service.getProjectReferenceTransToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteProjectReferenceTrans(row));
  }
}
