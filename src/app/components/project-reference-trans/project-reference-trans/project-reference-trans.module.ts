import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProjectReferenceTransService } from './project-reference-trans.service';
import { ProjectReferenceTransListComponent } from './project-reference-trans-list/project-reference-trans-list.component';
import { ProjectReferenceTransItemComponent } from './project-reference-trans-item/project-reference-trans-item.component';
import { ProjectReferenceTransItemCustomComponent } from './project-reference-trans-item/project-reference-trans-item-custom.component';
import { ProjectReferenceTransListCustomComponent } from './project-reference-trans-list/project-reference-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ProjectReferenceTransListComponent, ProjectReferenceTransItemComponent],
  providers: [ProjectReferenceTransService, ProjectReferenceTransItemCustomComponent, ProjectReferenceTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ProjectReferenceTransListComponent, ProjectReferenceTransItemComponent]
})
export class ProjectReferenceTransComponentModule {}
