import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { ProjectReferenceTransItemView, ProjectReferenceTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ProjectReferenceTransService {
  serviceKey = 'projectReferenceTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getProjectReferenceTransToList(search: SearchParameter): Observable<SearchResult<ProjectReferenceTransListView>> {
    const url = `${this.servicePath}/GetProjectReferenceTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteProjectReferenceTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteProjectReferenceTrans`;
    return this.dataGateway.delete(url, row);
  }
  getProjectReferenceTransById(id: string): Observable<ProjectReferenceTransItemView> {
    const url = `${this.servicePath}/GetProjectReferenceTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createProjectReferenceTrans(vmModel: ProjectReferenceTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateProjectReferenceTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateProjectReferenceTrans(vmModel: ProjectReferenceTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateProjectReferenceTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<ProjectReferenceTransItemView> {
    const url = `${this.servicePath}/GetProjectReferenceTransInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
}
