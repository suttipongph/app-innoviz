import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GuarantorTransListComponent } from './guarantor-trans-list.component';

describe('GuarantorTransListViewComponent', () => {
  let component: GuarantorTransListComponent;
  let fixture: ComponentFixture<GuarantorTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GuarantorTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuarantorTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
