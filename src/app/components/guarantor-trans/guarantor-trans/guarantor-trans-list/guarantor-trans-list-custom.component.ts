import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AccessMode, CreditAppRequestStatus, RefType } from 'shared/constants';
import { isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessLevelModel, AccessModeView, GuarantorTransListView } from 'shared/models/viewModel';

const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const CUSTOMER_TABLE = 'customertable';
const CREDIT_APP_TABLE = 'creditapptable';
const AMEND_CA = 'amendca';
export class GuarantorTransListCustomComponent {
  baseService: BaseServiceModel<any>;
  parentId: string;
  parentName: string;
  currentName: string;
  model: GuarantorTransListView = new GuarantorTransListView();
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setInitialListData(isWorkFlowMode: boolean): Observable<GuarantorTransListView> {
    this.model = new GuarantorTransListView();
    this.model.accessModeView = new AccessModeView();
    this.getRelatedInfoTableKey();
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        return this.baseService.service.getAccessModeByCreditAppRequestTable(this.parentId).pipe(
          map((result) => {
            this.model.accessModeView = result as AccessModeView;
            this.model.refType = RefType.CreditAppRequestTable;
            const isDraftStatus = this.model.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
            this.model.accessModeView.canCreate = isDraftStatus ? true : this.model.accessModeView.canCreate && isWorkFlowMode;
            this.model.accessModeView.canDelete = isDraftStatus ? true : this.model.accessModeView.canDelete && isWorkFlowMode;
            this.model.accessModeView.canView = true;
            return this.model;
          })
        );
      case CUSTOMER_TABLE:
        this.model.accessModeView.canCreate = true;
        this.model.accessModeView.canDelete = false;
        this.model.accessModeView.canView = true;
        this.model.refType = RefType.Customer;
        return of(this.model);
      case CREDIT_APP_TABLE:
        return this.getAccessModeCaseCreditAppTable(isWorkFlowMode);
      default:
        return of(this.model);
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.model.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.model.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.model.accessModeView.canDelete;
  }
  checkAccessMode(accessRightChildPage: string): Observable<AccessLevelModel> {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    this.currentName = this.baseService.uiService.getRelatedInfoActiveTableName();
    switch (this.currentName) {
      case AMEND_CA:
        return this.baseService.accessService.getNestedComponentAccessRight(true, accessRightChildPage);
      default:
        return this.baseService.accessService.getAccessRight();
    }
  }
  getRelatedInfoTableKey(): void {
    switch (this.currentName) {
      case AMEND_CA:
        this.parentId = this.baseService.uiService.getRelatedInfoActiveTableKey();
        break;
      default:
        this.parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
        break;
    }
    this.model.refGUID = this.parentId;
  }
  getAccessModeCaseCreditAppTable(isWorkFlowMode: boolean): Observable<GuarantorTransListView> {
    switch (this.currentName) {
      case AMEND_CA:
        return this.getGuarantorTransInitialListData(isWorkFlowMode);
      default:
        this.model.accessModeView.canCreate = false;
        this.model.accessModeView.canDelete = false;
        this.model.accessModeView.canView = true;
        this.model.refType = RefType.CreditAppTable;
        return of(this.model);
    }
  }
  getGuarantorTransInitialListData(isWorkFlowMode: boolean): Observable<GuarantorTransListView> {
    return this.baseService.service.getGuarantorTransInitialListData(this.parentId).pipe(
      tap((result) => {
        this.model = result as GuarantorTransListView;
        this.model.accessModeView = this.model.accessModeView;
        const isDraftStatus = this.model.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
        this.model.accessModeView.canCreate = isDraftStatus ? true : this.model.accessModeView.canCreate && isWorkFlowMode;
        this.model.accessModeView.canDelete = isDraftStatus ? true : this.model.accessModeView.canDelete && isWorkFlowMode;
        this.model.accessModeView.canView = true;
      })
    );
  }
  getRowAuthorize(row: GuarantorTransListView[]): boolean {
    let isSetRow: boolean = true;
    if (this.currentName === AMEND_CA) {
      row.forEach((item) => {
        const accessMode: AccessMode = isNullOrUndefOrEmptyGUID(item.refGuarantorTransGUID) ? AccessMode.full : AccessMode.creator;
        item.rowAuthorize = accessMode;
      });
    } else {
      isSetRow = false;
    }
    return isSetRow;
  }
}
