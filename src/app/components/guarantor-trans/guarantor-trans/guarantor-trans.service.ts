import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { AccessModeView, GuarantorTransItemView, GuarantorTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class GuarantorTransService {
  serviceKey = 'guarantorTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getGuarantorTransToList(search: SearchParameter): Observable<SearchResult<GuarantorTransListView>> {
    const url = `${this.servicePath}/GetGuarantorTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteGuarantorTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteGuarantorTrans`;
    return this.dataGateway.delete(url, row);
  }
  getGuarantorTransById(id: string): Observable<GuarantorTransItemView> {
    const url = `${this.servicePath}/GetGuarantorTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createGuarantorTrans(vmModel: GuarantorTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateGuarantorTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateGuarantorTrans(vmModel: GuarantorTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateGuarantorTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<GuarantorTransItemView> {
    const url = `${this.servicePath}/GetGuarantorTransInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByCreditAppRequestTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppRequestTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getGuarantorTransInitialListData(amendCAId: string): Observable<GuarantorTransListView> {
    const url = `${this.servicePath}/GetGuarantorTransInitialListData/amendCAId=${amendCAId}`;
    return this.dataGateway.get(url);
  }
  getAccessModeGuarantorTransByCreditAppRequestTable(creditAppRequestId: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeGuarantorTransByCreditAppRequestTable/creditAppRequestId=${creditAppRequestId}`;
    return this.dataGateway.get(url);
  }
}
