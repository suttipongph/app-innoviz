import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GuarantorTransService } from './guarantor-trans.service';
import { GuarantorTransListComponent } from './guarantor-trans-list/guarantor-trans-list.component';
import { GuarantorTransItemComponent } from './guarantor-trans-item/guarantor-trans-item.component';
import { GuarantorTransItemCustomComponent } from './guarantor-trans-item/guarantor-trans-item-custom.component';
import { GuarantorTransListCustomComponent } from './guarantor-trans-list/guarantor-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [GuarantorTransListComponent, GuarantorTransItemComponent],
  providers: [GuarantorTransService, GuarantorTransItemCustomComponent, GuarantorTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [GuarantorTransListComponent, GuarantorTransItemComponent]
})
export class GuarantorTransComponentModule {}
