import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConst, CreditAppRequestStatus } from 'shared/constants';
import { isNullOrUndefOrEmpty, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, GuarantorTransItemView, RelatedPersonTableItemView } from 'shared/models/viewModel';

const firstGroup = [
  'RELATED_PERSON_TABLE_GUID',
  'NAME',
  'IDENTIFICATION_TYPE',
  'TAX_ID',
  'PASSPORT_ID',
  'WORK_PERMIT_ID',
  'BACKGROUND_SUMMARY',
  'DATE_OF_BIRTH',
  'AGE',
  'NATIONALITY_ID',
  'RACE_ID',
  'PHONE',
  'EXTENSION',
  'MOBILE',
  'FAX',
  'EMAIL',
  'LINE_ID',
  'AFFILIATE_REF_CUSTOMER_ID',
  'REF_TYPE',
  'REF_ID',
  'AGE'
];

const secondGroup = ['RELATED_PERSON_TABLE_GUID'];
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const CUSTOMER_TABLE = 'customertable';
const CREDIT_APP_TABLE = 'creditapptable';
const AMEND_CA = 'amendca';
const inAciveoOnly = ['IN_ACTIVE'];
export class GuarantorTransItemCustomComponent {
  baseService: BaseServiceModel<any>;
  parentId: string;
  parentName: string;
  currentName: string;
  accessModeView: AccessModeView = new AccessModeView();
  addGetFieldAccessingByParent: boolean = false;
  getFieldAccessing(model: GuarantorTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    if (isUpdateMode(model.guarantorTransGUID)) {
      fieldAccessing.push({ filedIds: secondGroup, readonly: true });
      if (this.addGetFieldAccessingByParent) {
        fieldAccessing.push(...this.getFieldAccessingByParent(model));
      }
    } else {
      fieldAccessing.push({ filedIds: secondGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getFieldAccessingByParent(model: GuarantorTransItemView): FieldAccessing[] {
    const fieldAccessing: FieldAccessing[] = [];
    if (!isNullOrUndefOrEmptyGUID(model.refGuarantorTransGUID)) {
      fieldAccessing.push(
        ...[
          { filedIds: [AppConst.ALL_ID], readonly: true },
          { filedIds: inAciveoOnly, readonly: false }
        ]
      );
    }
    return fieldAccessing;
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: GuarantorTransItemView, isWorkFlowMode: boolean): Observable<boolean> {
    return this.setAccessModeByParentStatus(model, isWorkFlowMode).pipe(
      map((result) => (isUpdateMode(model.guarantorTransGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  setAccessModeByParentStatus(model: GuarantorTransItemView, isWorkFlowMode: boolean): Observable<AccessModeView> {
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        return this.baseService.service.getAccessModeByCreditAppRequestTable(this.parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
            this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
            this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
            this.accessModeView.canView = isDraftStatus ? true : this.accessModeView.canView && isWorkFlowMode;
          })
        );
      case CUSTOMER_TABLE:
        this.accessModeView.canCreate = true;
        this.accessModeView.canDelete = false;
        this.accessModeView.canView = true;
        return of(this.accessModeView);
      case CREDIT_APP_TABLE:
        return this.getAccessModeByCreditAppTable(isWorkFlowMode);
      default:
        return of(this.accessModeView);
    }
  }
  getAccessModeByCreditAppTable(isWorkFlowMode: boolean): Observable<AccessModeView> {
    switch (this.currentName) {
      case AMEND_CA:
        return this.getAccessModeGuarantorTransByCreditAppRequestTable(isWorkFlowMode);
      default:
        // CREDIT_APP_TABLE
        this.accessModeView.canCreate = false;
        this.accessModeView.canDelete = false;
        this.accessModeView.canView = false;
        return of(this.accessModeView);
    }
  }
  getAccessModeGuarantorTransByCreditAppRequestTable(isWorkFlowMode: boolean): Observable<AccessModeView> {
    return this.baseService.service.getAccessModeGuarantorTransByCreditAppRequestTable(this.parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
        const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
        this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
        this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
        this.accessModeView.canView = isDraftStatus ? true : this.accessModeView.canView && isWorkFlowMode;
      })
    );
  }
  onRelatedPersonChange(model: GuarantorTransItemView, relatedPersonTableOptions: SelectItems[]): void {
    if (!isNullOrUndefOrEmpty(model.relatedPersonTableGUID)) {
      let relatedPersonTableItemView: RelatedPersonTableItemView = relatedPersonTableOptions.find((t) => t.value == model.relatedPersonTableGUID)
        .rowData as RelatedPersonTableItemView;
      model.relatedPersonTable_Name = relatedPersonTableItemView.name;
      model.relatedPersonTable_IdentificationType = relatedPersonTableItemView.identificationType;
      model.relatedPersonTable_TaxId = relatedPersonTableItemView.taxId;
      model.relatedPersonTable_PassportId = relatedPersonTableItemView.passportId;
      model.relatedPersonTable_WorkPermitId = relatedPersonTableItemView.workPermitId;
      model.relatedPersonTable_DateOfBirth = relatedPersonTableItemView.dateOfBirth;
      model.relatedPersonTable_NationalityId = relatedPersonTableItemView.nationality_NationalityId;
      model.relatedPersonTable_RaceId = relatedPersonTableItemView.race_RaceId;
      model.relatedPersonTable_Phone = relatedPersonTableItemView.phone;
      model.relatedPersonTable_Extension = relatedPersonTableItemView.extension;
      model.relatedPersonTable_Mobile = relatedPersonTableItemView.mobile;
      model.relatedPersonTable_Fax = relatedPersonTableItemView.fax;
      model.relatedPersonTable_Email = relatedPersonTableItemView.email;
      model.relatedPersonTable_LineId = relatedPersonTableItemView.lineId;
      model.relatedPersonTable_BackgroundSummary = relatedPersonTableItemView.backgroundSummary;
      model.age = relatedPersonTableItemView.age;
    } else {
      model.relatedPersonTable_Name = '';
      model.relatedPersonTable_IdentificationType = null;
      model.relatedPersonTable_TaxId = '';
      model.relatedPersonTable_PassportId = '';
      model.relatedPersonTable_WorkPermitId = '';
      model.relatedPersonTable_DateOfBirth = null;
      model.relatedPersonTable_NationalityId = '';
      model.relatedPersonTable_RaceId = '';
      model.relatedPersonTable_Phone = '';
      model.relatedPersonTable_Extension = '';
      model.relatedPersonTable_Mobile = '';
      model.relatedPersonTable_Fax = '';
      model.relatedPersonTable_Email = '';
      model.relatedPersonTable_LineId = '';
      model.relatedPersonTable_BackgroundSummary = '';
    }
  }
  getRelatedInfoTableKey(): string {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    this.currentName = this.baseService.uiService.getRelatedInfoActiveTableName();
    switch (this.currentName) {
      case AMEND_CA:
        this.parentId = this.baseService.uiService.getRelatedInfoActiveTableKey();
        this.addGetFieldAccessingByParent = true;
        break;
      default:
        this.parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
        break;
    }
    return this.parentId;
  }
  getRelatedPersonTableDropDown(model: GuarantorTransItemView): Observable<SelectItems[]> {
    switch (this.currentName) {
      case AMEND_CA:
        return this.baseService.baseDropdown.getRelatedPersonTableByCreditAppRequestTableGuarantorDropDown(model.refGUID);
      default:
        if (this.parentName === CREDIT_APP_REQUEST_TABLE) {
          return this.baseService.baseDropdown.getRelatedPersonTableByCreditAppRequestTableGuarantorDropDown(model.refGUID);
        } else {
          return this.baseService.baseDropdown.getRelatedPersonTableDropDown();
        }
    }
  }
}
