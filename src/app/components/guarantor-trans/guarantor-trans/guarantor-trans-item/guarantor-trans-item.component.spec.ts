import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuarantorTransItemComponent } from './guarantor-trans-item.component';

describe('GuarantorTransItemViewComponent', () => {
  let component: GuarantorTransItemComponent;
  let fixture: ComponentFixture<GuarantorTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GuarantorTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuarantorTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
