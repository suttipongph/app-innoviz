import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmpty, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { GuarantorTransItemView } from 'shared/models/viewModel';
import { GuarantorTransService } from '../guarantor-trans.service';
import { GuarantorTransItemCustomComponent } from './guarantor-trans-item-custom.component';
@Component({
  selector: 'guarantor-trans-item',
  templateUrl: './guarantor-trans-item.component.html',
  styleUrls: ['./guarantor-trans-item.component.scss']
})
export class GuarantorTransItemComponent extends BaseItemComponent<GuarantorTransItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  guarantorTypeOptions: SelectItems[] = [];
  identificationTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  relatedPersonTableOptions: SelectItems[] = [];

  constructor(
    private service: GuarantorTransService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: GuarantorTransItemCustomComponent,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.custom.getRelatedInfoTableKey();
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getIdentificationTypeEnumDropDown(this.identificationTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<GuarantorTransItemView> {
    return this.service.getGuarantorTransById(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }
  onAsyncRunner(model?: any): void {
    const tempModel = isNullOrUndefOrEmpty(model) ? this.model : model;
    forkJoin(this.baseDropdown.getGuarantorTypeDropDown(), this.custom.getRelatedPersonTableDropDown(tempModel)).subscribe(
      ([guarantorType, relatedPersonTable]) => {
        this.guarantorTypeOptions = guarantorType;
        this.relatedPersonTableOptions = relatedPersonTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model, this.isWorkFlowMode()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateGuarantorTrans(this.model), isColsing);
    } else {
      super.onCreate(this.service.createGuarantorTrans(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  getInitialData(): Observable<GuarantorTransItemView> {
    return this.service.getInitialData(this.parentId);
  }
}
