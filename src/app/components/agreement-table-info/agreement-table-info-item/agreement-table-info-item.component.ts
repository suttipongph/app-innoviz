import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { AgreementTableInfoItemView } from 'shared/models/viewModel';
import { AgreementTableInfoService } from '../agreement-table-info.service';
import { AgreementTableInfoItemCustomComponent } from './agreement-table-info-item-custom.component';
@Component({
  selector: 'agreement-table-info-item',
  templateUrl: './agreement-table-info-item.component.html',
  styleUrls: ['./agreement-table-info-item.component.scss']
})
export class AgreementTableInfoItemComponent extends BaseItemComponent<AgreementTableInfoItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  addressTransOptions: SelectItems[] = [];
  authorizedPersonTransOptions: SelectItems[] = [];
  authorizedPersonTypeOptions: SelectItems[] = [];
  companyBankOptions: SelectItems[] = [];
  companySignatureOptions: SelectItems[] = [];
  employeeTableOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];

  constructor(
    private service: AgreementTableInfoService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: AgreementTableInfoItemCustomComponent,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<AgreementTableInfoItemView> {
    return this.service.getAgreementTableInfoById(this.id);
  }
  getInitialData(): Observable<AgreementTableInfoItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.custom.setParentValue(result);
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.custom.setParentValue(result);
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin([
      this.custom.getCustomerAddressDropdown(),
      this.custom.getAuthorizedPersonTransDropdown(),
      this.baseDropdown.getAuthorizedPersonTypeDropDown(),
      this.baseDropdown.getCompanyBankDropDown(),
      this.custom.getCompanySignatureDropDown(),
      this.baseDropdown.getEmployeeTableDropDown()
    ]).subscribe(
      ([addressTrans, authorizedPersonTrans, authorizedPersonType, companyBank, companySignature, employeeTable]) => {
        this.addressTransOptions = addressTrans;
        this.authorizedPersonTransOptions = authorizedPersonTrans;
        this.authorizedPersonTypeOptions = authorizedPersonType;
        this.companyBankOptions = companyBank;
        this.companySignatureOptions = companySignature;
        this.employeeTableOptions = employeeTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateAgreementTableInfo(this.model), isColsing);
    } else {
      super.onCreate(this.service.createAgreementTableInfo(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onCustomerAddressChange() {
    this.custom.clearModelByCustomerAddress(this.model);
    this.custom.setModelByCustomerAddress(this.model, this.addressTransOptions);
  }
  onBuyerAddressChange() {
    this.custom.clearModelByBuyerAddress(this.model);
    this.custom.setModelByBuyerAddress(this.model, this.addressTransOptions);
  }
}
