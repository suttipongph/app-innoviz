import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgreementTableInfoItemComponent } from './agreement-table-info-item.component';

describe('AgreementTableInfoItemViewComponent', () => {
  let component: AgreementTableInfoItemComponent;
  let fixture: ComponentFixture<AgreementTableInfoItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AgreementTableInfoItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgreementTableInfoItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
