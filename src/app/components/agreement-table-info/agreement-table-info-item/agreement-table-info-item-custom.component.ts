import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { EmptyGuid, ProductType } from 'shared/constants';
import { isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, AddressTransItemView, AgreementTableInfoItemView } from 'shared/models/viewModel';
import { AgreementTableInfoService } from '../agreement-table-info.service';

const MAIN_AGREEMENT_TABLE = 'mainagreementtable';
const ASSIGNMENT_AGREEMENT_TABLE = 'assignmentagreementtable';
const BUSINESS_COLLATERAL_AGM_TABLE = 'businesscollateralagmtable';
const ADDENDUM_MAIN_AGREEMENT_TABLE = 'addendummainagreementtable';
const NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE = 'noticeofcancellationmainagreementtable';
const NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE = 'noticeofcancellation';
const ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE = 'addendumbusinesscollateralagmtable';
const NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE = 'noticeofcancellationbusinesscollateralagmtable';

const firstGroup = [
  'REF_TYPE',
  'REF_ID',
  'UNBOUND_CUSTOMER_ADDRESS',
  'CUSTOMER_ADDRESS1',
  'CUSTOMER_ADDRESS2',
  'UNBOUND_BUYER_ADDRESS',
  'BUYER_ADDRESS1',
  'BUYER_ADDRESS2',
  'UNBOUND_COMPANY_ADDRESS',
  'COMPANY_ADDRESS1',
  'COMPANY_ADDRESS2',
  'REF_GUID'
];

const readOnlyByProductType = [
  'BUYER_ADDRESS_GUID',
  'AUTHORIZED_PERSON_TRANS_BUYER1GUID',
  'AUTHORIZED_PERSON_TRANS_BUYER2GUID',
  'AUTHORIZED_PERSON_TRANS_BUYER3GUID',
  'AUTHORIZED_PERSON_TYPE_BUYER_GUID'
];

export class AgreementTableInfoItemCustomComponent {
  baseService: BaseServiceModel<AgreementTableInfoService>;
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  parentCustomerTableGUID = null;
  parentCompanyDeafultBranch = null;
  // MainAgreement
  parentCreditAppTableGUID = null;
  parentBuyerTableGUID = null;
  activeIndex: number = 0;

  getFieldAccessing(model: AgreementTableInfoItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    if (model.parentTable_ProductType !== ProductType.Leasing && model.parentTable_ProductType !== ProductType.HirePurchase) {
      fieldAccessing.push({ filedIds: readOnlyByProductType, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: readOnlyByProductType, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: AgreementTableInfoItemView): Observable<boolean> {
    return this.setAccessModeByParentStatus(model).pipe(
      map((result) => (isUpdateMode(model.agreementTableInfoGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  setAccessModeByParentStatus(model: AgreementTableInfoItemView): Observable<AccessModeView> {
    let parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
    switch (this.parentName) {
      case MAIN_AGREEMENT_TABLE:
      case ADDENDUM_MAIN_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE:
        return this.baseService.service.getAccessModeByMainAgreementTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
          })
        );
      case ASSIGNMENT_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE:
        return this.baseService.service.getAccessModeByAssignmentAgreementTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
          })
        );
      case BUSINESS_COLLATERAL_AGM_TABLE:
      case ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
      case NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
        return this.baseService.service.getAccessModeByBusinessCollateralAgmTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
          })
        );
      default:
        return of(this.accessModeView);
    }
  }
  getInitialData(): Observable<AgreementTableInfoItemView> {
    let model = new AgreementTableInfoItemView();
    model.agreementTableInfoGUID = EmptyGuid;
    return of(model);
  }
  getCustomerAddressDropdown(): Observable<SelectItems[]> {
    return this.baseService.baseDropdown.getAddressTransByCustomerDropDown(this.parentCustomerTableGUID);
  }
  getAuthorizedPersonTransDropdown(): Observable<SelectItems[]> {
    switch (this.parentName) {
      case MAIN_AGREEMENT_TABLE:
      case ADDENDUM_MAIN_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE:
        return this.baseService.baseDropdown.getAuthorizedPersonTransByAgreementTableInfoDropDown([
          this.parentCreditAppTableGUID,
          this.parentCustomerTableGUID
        ]);
      case ASSIGNMENT_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE:
        return this.baseService.baseDropdown.getAuthorizedPersonTransByCustomerDropDown(this.parentCustomerTableGUID);
      case BUSINESS_COLLATERAL_AGM_TABLE:
      case ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
      case NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
        return this.baseService.baseDropdown.getAuthorizedPersonTransByCreditAppDropDown(this.parentCreditAppTableGUID);

      default:
        return of([]);
    }
  }
  getAuthorizedPersonTransBuyerDropdown(): Observable<SelectItems[]> {
    switch (this.parentName) {
      case MAIN_AGREEMENT_TABLE:
      case ADDENDUM_MAIN_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE:
        return this.baseService.baseDropdown.getAuthorizedPersonTransByBuyerDropDown(this.parentBuyerTableGUID);

      case ASSIGNMENT_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE:
        return this.baseService.baseDropdown.getAuthorizedPersonTransByBuyerDropDown(this.parentBuyerTableGUID);

      case BUSINESS_COLLATERAL_AGM_TABLE:
      case ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
      case NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
        return of([]);
      default:
        return of([]);
    }
  }
  getCompanySignatureDropDown(): Observable<SelectItems[]> {
    return this.baseService.baseDropdown.getEmployeeTableWithCompanySignatureDropDown();
  }
  setParentValue(model: AgreementTableInfoItemView) {
    this.parentCustomerTableGUID = model.parentTable_CustomerTableGUID;
    this.parentCreditAppTableGUID = model.parentTable_CreditAppTableGUID;
    this.parentBuyerTableGUID = model.parentTable_CreditAppTableGUID;
    this.parentCompanyDeafultBranch = model.company_DefaultBranchGUID;
  }
  clearModelByCustomerAddress(model: AgreementTableInfoItemView) {
    model.customerAddress1 = '';
    model.customerAddress2 = '';
    model.unboundCustomerAddress = '';
  }
  setModelByCustomerAddress(model: AgreementTableInfoItemView, option: SelectItems[]) {
    if (!isNullOrUndefOrEmptyGUID(model.customerAddressGUID)) {
      let customerAddress: AddressTransItemView = option.find((t) => t.value == model.customerAddressGUID).rowData as AddressTransItemView;
      model.customerAddress1 = customerAddress.address1;
      model.customerAddress2 = customerAddress.address2;
      model.unboundCustomerAddress = customerAddress.unboundAddress;
    }
  }
  clearModelByBuyerAddress(model: AgreementTableInfoItemView) {
    model.buyerAddress1 = '';
    model.buyerAddress2 = '';
    model.unboundBuyerAddress = '';
  }
  setModelByBuyerAddress(model: AgreementTableInfoItemView, option: SelectItems[]) {
    if (!isNullOrUndefOrEmptyGUID(model.buyerAddressGUID)) {
      let customerAddress: AddressTransItemView = option.find((t) => t.value == model.buyerAddressGUID).rowData as AddressTransItemView;
      model.buyerAddress1 = customerAddress.address1;
      model.buyerAddress2 = customerAddress.address2;
      model.unboundBuyerAddress = customerAddress.unboundAddress;
    }
  }
}
