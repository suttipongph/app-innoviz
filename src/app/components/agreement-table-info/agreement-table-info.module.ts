import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgreementTableInfoService } from './agreement-table-info.service';
import { AgreementTableInfoListComponent } from './agreement-table-info-list/agreement-table-info-list.component';
import { AgreementTableInfoItemComponent } from './agreement-table-info-item/agreement-table-info-item.component';
import { AgreementTableInfoItemCustomComponent } from './agreement-table-info-item/agreement-table-info-item-custom.component';
import { AgreementTableInfoListCustomComponent } from './agreement-table-info-list/agreement-table-info-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [AgreementTableInfoListComponent, AgreementTableInfoItemComponent],
  providers: [AgreementTableInfoService, AgreementTableInfoItemCustomComponent, AgreementTableInfoListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [AgreementTableInfoListComponent, AgreementTableInfoItemComponent]
})
export class AgreementTableInfoComponentModule {}
