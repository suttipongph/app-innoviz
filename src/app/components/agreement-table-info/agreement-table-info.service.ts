import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, AgreementTableInfoItemView, AgreementTableInfoListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class AgreementTableInfoService {
  serviceKey = 'agreementTableInfoGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getAgreementTableInfoToList(search: SearchParameter): Observable<SearchResult<AgreementTableInfoListView>> {
    const url = `${this.servicePath}/GetAgreementTableInfoList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteAgreementTableInfo(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteAgreementTableInfo`;
    return this.dataGateway.delete(url, row);
  }
  getAgreementTableInfoById(id: string): Observable<AgreementTableInfoItemView> {
    const url = `${this.servicePath}/GetAgreementTableInfoById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createAgreementTableInfo(vmModel: AgreementTableInfoItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateAgreementTableInfo`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateAgreementTableInfo(vmModel: AgreementTableInfoItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateAgreementTableInfo`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getAgreementTableInfoByMainAgreement(id: string): Observable<AgreementTableInfoItemView> {
    const url = `${this.servicePath}/GetAgreementTableInfoByMainAgreement/id=${id}`;
    return this.dataGateway.get(url);
  }
  getInitialData(id: string): Observable<AgreementTableInfoItemView> {
    const url = `${this.servicePath}/GetAgreementTableInfoInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByMainAgreementTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByMainAgreementTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByAssignmentAgreementTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByAssignmentTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByBusinessCollateralAgmTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByBusinessCollateralAgmTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAgreementTableInfoByAssignmentAgreement(id: string): Observable<AgreementTableInfoItemView> {
    const url = `${this.servicePath}/GetAgreementTableInfoByAssignmentAgreement/id=${id}`;
    return this.dataGateway.get(url);
  }
}
