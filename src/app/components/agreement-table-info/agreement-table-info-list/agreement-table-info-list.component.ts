import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, EmptyGuid, SortType } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { AgreementTableInfoListView } from 'shared/models/viewModel';
import { AgreementTableInfoService } from '../agreement-table-info.service';
import { AgreementTableInfoListCustomComponent } from './agreement-table-info-list-custom.component';

@Component({
  selector: 'agreement-table-info-list',
  templateUrl: './agreement-table-info-list.component.html',
  styleUrls: ['./agreement-table-info-list.component.scss']
})
export class AgreementTableInfoListComponent extends BaseListComponent<AgreementTableInfoListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  addressTransOptions: SelectItems[] = [];
  authorizedPersonTransOptions: SelectItems[] = [];
  authorizedPersonTypeOptions: SelectItems[] = [];
  companyBankOptions: SelectItems[] = [];
  companySignatureOptions: SelectItems[] = [];
  employeeTableOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];

  constructor(
    public custom: AgreementTableInfoListCustomComponent,
    private service: AgreementTableInfoService,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.skipListPage();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.custom.setAccessModeByParentStatus().subscribe((result) => {
      this.setDataGridOption();
    });
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'agreementTableInfoGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<AgreementTableInfoListView>> {
    return this.service.getAgreementTableInfoToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteAgreementTableInfo(row));
  }

  skipListPage() {
    let row = new RowIdentity();
    this.custom.getIsUpdateModeByParent(this.parentId).subscribe((result) => {
      if (!result) {
        row.guid = EmptyGuid;
        this.onCreate(row);
      } else {
        row.guid = result.agreementTableInfoGUID;
        this.onView(row);
      }
    });
  }
}
