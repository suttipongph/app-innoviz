import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AgreementTableInfoListComponent } from './agreement-table-info-list.component';

describe('AgreementTableInfoListViewComponent', () => {
  let component: AgreementTableInfoListComponent;
  let fixture: ComponentFixture<AgreementTableInfoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AgreementTableInfoListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgreementTableInfoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
