import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView } from 'shared/models/viewModel';
import { AgreementTableInfoService } from '../agreement-table-info.service';

const MAIN_AGREEMENT_TABLE = 'mainagreementtable';
const ASSIGNMENT_AGREEMENT_TABLE = 'assignmentagreementtable';
const ADDENDUM_MAIN_AGREEMENT_TABLE = 'addendummainagreementtable';
const NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE = 'noticeofcancellationmainagreementtable';
const NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE = 'noticeofcancellation';
export class AgreementTableInfoListCustomComponent {
  baseService: BaseServiceModel<AgreementTableInfoService>;
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setAccessModeByParentStatus(): Observable<AccessModeView> {
    let parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
    switch (this.parentName) {
      case MAIN_AGREEMENT_TABLE:
      case ADDENDUM_MAIN_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE:
        return this.baseService.service.getAccessModeByMainAgreementTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            this.accessModeView.canView = true;
          })
        );
      case ASSIGNMENT_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE:
        return this.baseService.service.getAccessModeByAssignmentAgreementTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            this.accessModeView.canView = true;
          })
        );
      default:
        this.accessModeView.canView = true;
        return of(this.accessModeView);
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
  getIsUpdateModeByParent(id: string): Observable<any> {
    switch (this.parentName) {
      case MAIN_AGREEMENT_TABLE:
      case ADDENDUM_MAIN_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE:
        return this.baseService.service.getAgreementTableInfoByMainAgreement(id).pipe(
          map((result) => {
            if (isNullOrUndefined(result)) {
              return false;
            } else {
              return true;
            }
          })
        );
      case ASSIGNMENT_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE:
        return this.baseService.service.getAgreementTableInfoByAssignmentAgreement(id).pipe(
          map((result) => {
            if (isNullOrUndefined(result)) {
              return false;
            } else {
              return true;
            }
          })
        );
    }
  }
}
