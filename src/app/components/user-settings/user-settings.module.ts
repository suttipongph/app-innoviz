import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserSettingsComponent } from './user-settings.component';
import { UserSettingsCustomComponent } from './user-settings-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [UserSettingsComponent],
  providers: [UserSettingsCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [UserSettingsComponent]
})
export class UserSettingsComponentModule {}
