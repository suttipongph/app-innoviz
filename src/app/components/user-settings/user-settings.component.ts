import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { AuthService } from 'core/services/auth.service';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { tap, catchError, map } from 'rxjs/operators';
import { IVZFileUploadComponent } from 'shared/components/ivz-file-upload/ivz-file-upload.component';
import { EmptyGuid } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUndefinedOrZeroLength, isUpdateMode } from 'shared/functions/value.function';
import { FileUpload, FormValidationModel, PageInformationModel, ResponseModel, SelectItems } from 'shared/models/systemModel';
import { SysUserCompanyMappingView, SysUserSettingsItemView } from 'shared/models/viewModel';
import { UserSettingsCustomComponent } from './user-settings-custom.component';
@Component({
  selector: 'user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss']
})
export class UserSettingsComponent extends BaseItemComponent<SysUserSettingsItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
  }

  sysLanguageOptions: SelectItems[] = [];
  companyOptions: SelectItems[] = [];
  fileUpload: FileUpload = { fileInfos: [] };
  @ViewChild('fileUploadCmpnt') fileComponent: IVZFileUploadComponent;
  constructor(private currentActivatedRoute: ActivatedRoute, public custom: UserSettingsCustomComponent, private authService: AuthService) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.userDataService.getUserIdFromToken();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getSysLanguageEnumDropDown(this.sysLanguageOptions);
  }
  getById(): Observable<SysUserSettingsItemView> {
    return this.userDataService.loadUserSettings(this.id).pipe(
      catchError((error) => {
        this.notificationService.showErrorMessageFromResponse(error);
        throw error;
      })
    );
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {}
  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          this.setInitialCompanyBranchDropdown(this.model);
          this.getFileUpload(model);
          this.custom.setShowSystemLog(this.model);
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight(false, true, this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {}
  setFunctionOptions(): void {
    // this.itemsFunction = [
    //   {
    //     label: this.translate.get('LABEL.CLEAR_USAGE_DATA')['value'],
    //     command: () => this.setOpenFunction(ROUTE_FUNCTIONS.CLEARUSAGEDATA)
    //   }
    // ];
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    this.setFileUpload();
    if (this.isUpdateMode) {
      super.onUpdate(this.updateUserSettings(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  setInitialCompanyBranchDropdown(model: SysUserSettingsItemView): void {
    this.companyOptions = this.custom.getInitialCompanyDropdown(model);
  }

  getFileUpload(model: SysUserSettingsItemView): void {
    if (!isUndefinedOrZeroLength(model.userImage)) {
      this.fileUpload.fileInfos = [];
      const userImg = this.custom.getFileInformationFromModel(model);
      this.fileUpload.fileInfos.push(userImg);

      this.fileComponent.setFileFromExternal(userImg);
    }
  }

  setFileUpload(): void {
    if (isUndefinedOrZeroLength(this.fileUpload.fileInfos) || isUndefinedOrZeroLength(this.fileUpload.fileInfos[0])) {
      return;
    } else {
      const file = this.fileUpload.fileInfos[0].base64.split(',');
      this.model.userImage = file[file.length - 1];
    }
  }
  updateUserSettings(model: SysUserSettingsItemView): Observable<ResponseModel> {
    return this.userDataService.updateUserData(model).pipe(
      tap((user) => {
        if (!isUndefinedOrZeroLength(user.languageId)) {
          this.userDataService.setAppLanguage(user.languageId);
          this.translate.use(user.languageId);
          this.userDataService.languageChangeSubject.next(true);
        }
        this.custom.setShowSystemLog(user);

        if (!isUndefinedOrZeroLength(user.userImage)) {
          this.getFileUpload(user);
          this.userDataService.setUserImageContent(`data:image/png;base64,${this.model.userImage}`);
          this.authService.setDisplaySubject.next(true);
        }
      }),
      map((result) => {
        return { key: result['id'], model: result };
      }),
      catchError((error) => {
        throw error;
      })
    );
  }
  onFileChange(e): void {
    this.fileUpload = e;
    this.fileDirty = true;
  }
  onCompanyChange(e): void {
    this.model.defaultBranchGUID = null;
  }
  toList(): void {
    const site = this.userDataService.getSiteLogin();
    this.router.navigate([`/${site}/home`]);
  }
}
