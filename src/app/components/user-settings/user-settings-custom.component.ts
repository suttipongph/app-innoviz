import { Observable, of } from 'rxjs';
import { AppConst } from 'shared/constants';
import { isUndefinedOrZeroLength } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, FileInformation, SelectItems } from 'shared/models/systemModel';
import { SysUserCompanyMappingView, SysUserSettingsItemView } from 'shared/models/viewModel';

const firstGroup = ['IN_ACTIVE', 'USER_NAME'];
export class UserSettingsCustomComponent {
  baseService: BaseServiceModel<SysUserSettingsItemView>;
  pictureType: string = '.jpg,.jpeg,.png';
  getFieldAccessing(model: SysUserSettingsItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: SysUserSettingsItemView): Observable<boolean> {
    return of(false);
  }
  getInitialCompanyDropdown(model: SysUserSettingsItemView): SelectItems[] {
    const ucm = model.sysUserCompanyMappings;
    const companies = [];
    let seen = {};
    for (let i = 0; i < ucm.length; i++) {
      const companyGUID = ucm[i].companyGUID;
      if (seen[companyGUID] !== 1) {
        seen[companyGUID] = 1;
        companies.push({ label: ucm[i].company_Values, value: ucm[i].companyGUID });
      }
    }
    return companies;
  }
  getBranchDropdown(companyGUID: string, ucm: SysUserCompanyMappingView[]): SelectItems[] {
    const branches = [];
    if (!isUndefinedOrZeroLength(companyGUID)) {
      const filtered = ucm.filter((f) => f.companyGUID === companyGUID);
      if (!isUndefinedOrZeroLength(filtered)) {
        for (let i = 0; i < filtered.length; i++) {
          branches.push({ label: filtered[i].branch_Values, value: filtered[i].branchGUID });
        }
      }
    }
    return branches;
  }
  getFileInformationFromModel(model: SysUserSettingsItemView): FileInformation {
    const userImg = new FileInformation();
    const contentType = 'data:image/png;base64,';
    userImg.base64 = contentType + model.userImage;
    userImg.isRemovable = false;
    userImg.isPreviewable = true;
    userImg.contentType = contentType;
    userImg.fileName = 'Image.png';
    userImg.fileDisplayName = 'Image.png';
    return userImg;
  }
  setShowSystemLog(model: SysUserSettingsItemView): void {
    this.baseService.userDataService.setShowSystemLog(model.showSystemLog ? AppConst.TRUE : AppConst.FALSE);
  }
}
