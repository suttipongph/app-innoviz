import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { AccessModeView, CustBusinessCollateralItemView } from 'shared/models/viewModel';

const firstGroup = [
  'CUST_BUSINESS_COLLATERAL_ID',
  'CUSTOMER_TABLE_GUID',
  'CREDIT_APP_REQUEST_TABLE_GUID',
  'BUSINESS_COLLATERAL_AGM_ID',
  'BUSINESS_COLLATERAL_AGM_LINE_GUID',
  'BUSINESS_COLLATERAL_TYPE_GUID',
  'BUSINESS_COLLATERAL_SUB_TYPE_GUID',
  'DESCRIPTION',
  'BUSINESS_COLLATERAL_VALUE',
  'CANCELLED',
  'BUYER_ID',
  'BUYER_NAME',
  'BUYER_TAX_IDENTIFICATION_ID',
  'BANK_GROUP_GUID',
  'BANK_TYPE_GUID',
  'ACCOUNT_NUMBER',
  'REF_AGREEMENT_ID',
  'REF_AGREEMENT_DATE',
  'PREFERENTIAL_CREDITOR_NUMBER',
  'LESSEE',
  'LESSOR',
  'REGISTRATION_PLATE_NUMBER',
  'MACHINE_NUMBER',
  'MACHINE_REGISTERED_STATUS',
  'CHASSIS_NUMBER',
  'REGISTERED_PLACE',
  'GUARANTEE_AMOUNT',
  'QUANTITY',
  'UNIT',
  'PROJECT_NAME',
  'TITLE_DEED_NUMBER',
  'TITLE_DEED_SUB_DISTRICT',
  'TITLE_DEED_DISTRICT',
  'TITLE_DEED_PROVINCE',
  'CAPITAL_VALUATION',
  'VALUATION_COMMITTEE',
  'DATE_OF_VALUATION',
  'OWNERSHIP',
  'CUST_BUSINESS_COLLATERAL_GUID'
];
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const CUSTOMER_TABLE = 'customertable';
const AMEND_CA = 'amendca';
export class CustBusinessCollateralItemCustomComponent {
  baseService: BaseServiceModel<any>;
  parentName: string;
  accessModeView: AccessModeView = new AccessModeView();
  getFieldAccessing(model: CustBusinessCollateralItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CustBusinessCollateralItemView): Observable<boolean> {
    return this.setAccessModeByParentStatus().pipe(
      map((result) => (isUpdateMode(model.custBusinessCollateralGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  getInitialData(): Observable<CustBusinessCollateralItemView> {
    let model = new CustBusinessCollateralItemView();
    model.custBusinessCollateralGUID = EmptyGuid;
    return of(model);
  }
  setAccessModeByParentStatus(): Observable<AccessModeView> {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        this.accessModeView.canCreate = false;
        this.accessModeView.canView = false;
        this.accessModeView.canDelete = false;
        return of(this.accessModeView);
      case CUSTOMER_TABLE:
        this.accessModeView.canCreate = true;
        this.accessModeView.canView = true;
        this.accessModeView.canDelete = true;
        return of(this.accessModeView);
      case AMEND_CA:
        this.accessModeView.canCreate = false;
        this.accessModeView.canView = false;
        this.accessModeView.canDelete = false;
        return of(this.accessModeView);
      default:
        return of(this.accessModeView);
    }
  }
}
