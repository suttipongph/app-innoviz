import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustBusinessCollateralItemComponent } from './cust-business-collateral-item.component';

describe('CustBusinessCollateralItemViewComponent', () => {
  let component: CustBusinessCollateralItemComponent;
  let fixture: ComponentFixture<CustBusinessCollateralItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustBusinessCollateralItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustBusinessCollateralItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
