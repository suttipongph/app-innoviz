import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustBusinessCollateralService } from './cust-business-collateral.service';
import { CustBusinessCollateralListComponent } from './cust-business-collateral-list/cust-business-collateral-list.component';
import { CustBusinessCollateralItemComponent } from './cust-business-collateral-item/cust-business-collateral-item.component';
import { CustBusinessCollateralItemCustomComponent } from './cust-business-collateral-item/cust-business-collateral-item-custom.component';
import { CustBusinessCollateralListCustomComponent } from './cust-business-collateral-list/cust-business-collateral-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CustBusinessCollateralListComponent, CustBusinessCollateralItemComponent],
  providers: [CustBusinessCollateralService, CustBusinessCollateralItemCustomComponent, CustBusinessCollateralListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CustBusinessCollateralListComponent, CustBusinessCollateralItemComponent]
})
export class CustBusinessCollateralComponentModule {}
