import { Observable } from 'rxjs';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessLevelModel, CustBusinessCollateralListView } from 'shared/models/viewModel';
const CUSTOMER_TABLE = 'customertable';
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const AMEND_CA = 'amendca';
const AMEND_CA_LINE = 'amendcaline';

export class CustBusinessCollateralListCustomComponent {
  baseService: BaseServiceModel<any>;
  parentName: string;
  parentId: string;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }

  checkAccessMode(accessRightChildPage: string): Observable<AccessLevelModel> {
    if (this.baseService.uiService.getMasterRoute() === 'masterinformation') {
      return this.baseService.accessService.getAccessRight();
    } else {
      return this.baseService.accessService.getNestedComponentAccessRight(true, accessRightChildPage);
    }
  }
  getRelatedInfoParentTableKey(passingObj: CustBusinessCollateralListView): string {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (this.parentName) {
      case CUSTOMER_TABLE:
        this.parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
        break;
      case CREDIT_APP_REQUEST_TABLE:
        this.parentId = passingObj.customerTableGUID;
        break;
      case AMEND_CA:
      case AMEND_CA_LINE:
        this.parentId = passingObj.customerTableGUID;
        break;
      default:
        this.parentId = null;
        break;
    }
    return this.parentId;
  }
}
