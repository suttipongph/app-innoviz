import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CustBusinessCollateralListComponent } from './cust-business-collateral-list.component';

describe('CustBusinessCollateralListViewComponent', () => {
  let component: CustBusinessCollateralListComponent;
  let fixture: ComponentFixture<CustBusinessCollateralListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustBusinessCollateralListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustBusinessCollateralListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
