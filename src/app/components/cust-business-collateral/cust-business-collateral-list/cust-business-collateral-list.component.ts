import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CustBusinessCollateralListView } from 'shared/models/viewModel';
import { CustBusinessCollateralService } from '../cust-business-collateral.service';
import { CustBusinessCollateralListCustomComponent } from './cust-business-collateral-list-custom.component';

@Component({
  selector: 'cust-business-collateral-list',
  templateUrl: './cust-business-collateral-list.component.html',
  styleUrls: ['./cust-business-collateral-list.component.scss']
})
export class CustBusinessCollateralListComponent extends BaseListComponent<CustBusinessCollateralListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  defaultOptions: SelectItems[] = [];
  businessCollateralSubType: SelectItems[] = [];
  businessCollateralType: SelectItems[] = [];

  constructor(
    public custom: CustBusinessCollateralListCustomComponent,
    private service: CustBusinessCollateralService,
    public currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {
    this.accessRightChildPage = 'BusinessCollateralListPage';
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.parentId = this.custom.getRelatedInfoParentTableKey(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getDefaultBitDropDown(this.defaultOptions);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.custom.checkAccessMode(this.accessRightChildPage));
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBusinessCollateralTypeDropDown(this.businessCollateralType);
    this.baseDropdown.getBusinessCollateralSubTypeDropDown(this.businessCollateralSubType);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'custBusinessCollateralGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.CUSTOMER_BUSINESS_COLLATERAL_ID',
        textKey: 'custBusinessCollateralId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DBD_REGISTRATION_DATE',
        textKey: 'dbdRegistrationDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BUSINESS_COLLATERAL_TYPE_ID',
        textKey: 'businessCollateralType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.businessCollateralType,
        searchingKey: 'businessCollateralTypeGUID',
        sortingKey: 'businessCollateralType_BusinessCollateralTypeId'
      },
      {
        label: 'LABEL.BUSINESS_COLLATERAL_SUBTYPE_ID',
        textKey: 'businessCollateralSubType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.businessCollateralSubType,
        searchingKey: 'businessCollateralSubTypeGUID',
        sortingKey: 'businessCollateralSubType_BusinessCollateralSubTypeId'
      },
      {
        label: 'LABEL.BUYER_NAME',
        textKey: 'buyerName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CANCELLED',
        textKey: 'cancelled',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defaultOptions
      },
      {
        label: 'LABEL.BUSINESS_COLLATERAL_VALUE',
        textKey: 'businessCollateralValue',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'customerTableGUID',
        type: ColumnType.MASTER,
        visibility: false,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CustBusinessCollateralListView>> {
    return this.service.getCustBusinessCollateralToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCustBusinessCollateral(row));
  }
}
