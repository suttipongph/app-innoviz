import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CustBusinessCollateralItemView, CustBusinessCollateralListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CustBusinessCollateralService {
  serviceKey = 'custBusinessCollateralGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCustBusinessCollateralToList(search: SearchParameter): Observable<SearchResult<CustBusinessCollateralListView>> {
    const url = `${this.servicePath}/GetCustBusinessCollateralList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCustBusinessCollateral(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCustBusinessCollateral`;
    return this.dataGateway.delete(url, row);
  }
  getCustBusinessCollateralById(id: string): Observable<CustBusinessCollateralItemView> {
    const url = `${this.servicePath}/GetCustBusinessCollateralById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCustBusinessCollateral(vmModel: CustBusinessCollateralItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCustBusinessCollateral`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCustBusinessCollateral(vmModel: CustBusinessCollateralItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCustBusinessCollateral`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
