import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportAgingComponent } from './report-aging.component';

describe('ReportAgingViewComponent', () => {
  let component: ReportAgingComponent;
  let fixture: ComponentFixture<ReportAgingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReportAgingComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportAgingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
