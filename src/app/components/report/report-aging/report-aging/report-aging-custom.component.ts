import { Observable, of } from 'rxjs';
import { ColumnType, EmptyGuid, ProductType, RefType, SortType, SuspenseInvoiceType } from 'shared/constants';
import { dateTimetoStringLong } from 'shared/functions/date.function';
import { cloneObject, isSelectAll, isUndefinedOrZeroLength } from 'shared/functions/value.function';
import { BaseServiceModel, ColumnExportModel, ColumnModel, FieldAccessing } from 'shared/models/systemModel';
import { ReportAgingView } from 'shared/models/viewModel/reportAgingView';
import { TranslateService } from '@ngx-translate/core';
import { AppInjector } from 'app-injector';
const EXPORT_AGING_FACTORING = 'exportagingfactoring';
const EXPORT_AGING_PROJECT_FINANCE = 'exportagingprojectfinance';

const firstGroup = ['PRODUCT_TYPE', 'SUSPENSE_INVOICE_TYPE', 'REF_TYPE'];

export class ReportAgingCustomComponent {
  baseService: BaseServiceModel<any>;
  translateService = AppInjector.get(TranslateService);
  title: string = null;
  getFieldAccessing(model: ReportAgingView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  setTitle() {
    const masterRoute = this.baseService.uiService.getMasterRoute(3);
    switch (masterRoute) {
      case EXPORT_AGING_FACTORING:
        this.title = 'LABEL.AGING_FACTORING';
        break;
      case EXPORT_AGING_PROJECT_FINANCE:
        this.title = 'LABEL.AGING_PROJECT_FINANCE';
        break;
    }
  }
  getExportFileName(): string {
    return this.translateService.instant(this.title);
  }
  setColumn(model: ReportAgingView) {
    const masterRoute = this.baseService.uiService.getMasterRoute(3);
    const columnsAgingFactoring: ColumnExportModel[] = [
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerId'
      },
      {
        label: 'LABEL.CUSTOMER_NAME',
        textKey: 'customerName'
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerId'
      },
      {
        label: 'LABEL.BUYER_NAME',
        textKey: 'buyerName'
      },
      {
        label: 'LABEL.INVOICE_TYPE',
        textKey: 'invoiceTypeId'
      },
      {
        label: 'LABEL.INVOICE_ID',
        textKey: 'invoiceId'
      },
      {
        label: 'LABEL.PURCHASE_ID',
        textKey: 'documentId'
      },
      {
        label: 'LABEL.BUYER_INVOICE_ID',
        textKey: 'buyerInvoiceId'
      },
      {
        label: 'LABEL.INVOICE_DATE',
        textKey: 'buyerInvoiceDate',
        type: ColumnType.DATE
      },
      {
        label: 'LABEL.PURCHASE_DATE',
        textKey: 'purchaseDate',
        type: ColumnType.DATE
      },
      {
        label: 'LABEL.INTEREST_DATE',
        textKey: 'interestDate',
        type: ColumnType.DATE
      },
      {
        label: 'LABEL.AS_OF_DATE',
        textKey: 'asOfDate',
        type: ColumnType.DATE
      },
      {
        label: 'LABEL.DAYS',
        textKey: 'days',
        type: ColumnType.INT
      },
      {
        label: 'LABEL.INVOICE_AMOUNT',
        textKey: 'invoiceAmount',
        type: ColumnType.DECIMAL
      },
      {
        label: 'LABEL.PURCHASE_AMOUNT',
        textKey: 'purchaseAmount',
        type: ColumnType.DECIMAL
      },
      {
        label: 'LABEL.RESERVE_AMOUNT',
        textKey: 'reserveAmount',
        type: ColumnType.DECIMAL
      },
      {
        label: 'LABEL.AR_OUTSTANDING_INVOICE_AMT',
        textKey: 'arOutstanding',
        type: ColumnType.DECIMAL
      },
      {
        label: 'LABEL.AR_OUTSTANDING_PURCHASE_AMT',
        textKey: 'purchaseOutstanding',
        type: ColumnType.DECIMAL
      },
      {
        label: 'LABEL.UNEARNED_INTEREST_OUTSTANDING',
        textKey: 'unearnedInterestBalance',
        type: ColumnType.DECIMAL
      },
      {
        label: 'LABEL.ACCURED_INTEREST_OUTSTANDING',
        textKey: 'accruedIntBalance',
        type: ColumnType.DECIMAL
      },
      {
        label: 'LABEL.UNEARNED_FEE_OUTSTANDING',
        textKey: 'feeBalance',
        type: ColumnType.DECIMAL
      },
      { label: 'LABEL.NET_AR', textKey: 'netAR', type: ColumnType.DECIMAL },
      {
        label: model.agingReportSetup_Description_LineNum1,
        textKey: 'netARBucket1',
        type: ColumnType.DECIMAL
      },
      {
        label: model.agingReportSetup_Description_LineNum2,
        textKey: 'netARBucket2',
        type: ColumnType.DECIMAL
      },
      {
        label: model.agingReportSetup_Description_LineNum3,
        textKey: 'netARBucket3',
        type: ColumnType.DECIMAL
      },
      {
        label: model.agingReportSetup_Description_LineNum4,
        textKey: 'netARBucket4',
        type: ColumnType.DECIMAL
      },
      {
        label: model.agingReportSetup_Description_LineNum5,
        textKey: 'netARBucket5',
        type: ColumnType.DECIMAL
      },
      {
        label: model.agingReportSetup_Description_LineNum6,
        textKey: 'netARBucket6',
        type: ColumnType.DECIMAL
      },
      {
        label: 'LABEL.RESPONSIBLE_BY',
        textKey: 'responsibleName'
      },
      {
        label: 'LABEL.BUSINESS_UNIT',
        textKey: 'businessUnitId'
      },
      {
        label: 'LABEL.BUSINESS_TYPE',
        textKey: 'businessTypeDesc'
      },
      {
        label: 'LABEL.CREDIT_APPLICATION_ID',
        textKey: 'creditAppId'
      },
      {
        label: 'LABEL.MAIN_AGREEMENT_ID',
        textKey: 'mainAgreementId'
      }
    ];

    const columnsAgingProjectFinance: ColumnExportModel[] = [
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerId'
      },
      {
        label: 'LABEL.CUSTOMER_NAME',
        textKey: 'customerName'
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerId'
      },
      {
        label: 'LABEL.BUYER_NAME',
        textKey: 'buyerName'
      },
      {
        label: 'LABEL.INVOICE_TYPE',
        textKey: 'invoiceTypeId'
      },
      {
        label: 'LABEL.INVOICE_ID',
        textKey: 'invoiceId'
      },
      {
        label: 'LABEL.WITHDRAWAL_ID',
        textKey: 'documentId'
      },
      {
        label: 'LABEL.WITHDRAWAL_DATE',
        textKey: 'withdrawalDate',
        type: ColumnType.DATE
      },
      {
        label: 'LABEL.INTEREST_DATE',
        textKey: 'interestDate',
        type: ColumnType.DATE
      },
      {
        label: 'LABEL.AS_OF_DATE',
        textKey: 'asOfDate',
        type: ColumnType.DATE
      },
      {
        label: 'LABEL.DAYS',
        textKey: 'days',
        type: ColumnType.INT
      },
      {
        label: 'LABEL.INVOICE_AMOUNT',
        textKey: 'invoiceAmount',
        type: ColumnType.DECIMAL
      },
      {
        label: 'LABEL.AR_OUTSTANDING_INVOICE_AMT',
        textKey: 'arOutstanding',
        type: ColumnType.DECIMAL
      },
      {
        label: 'LABEL.UNEARNED_INTEREST_OUTSTANDING',
        textKey: 'unearnedInterestBalance',
        type: ColumnType.DECIMAL
      },
      {
        label: 'LABEL.ACCURED_INTEREST_OUTSTANDING',
        textKey: 'accruedIntBalance',
        type: ColumnType.DECIMAL
      },
      {
        label: 'LABEL.UNEARNED_FEE_OUTSTANDING',
        textKey: 'feeBalance',
        type: ColumnType.DECIMAL
      },
      { label: 'LABEL.NET_AR', textKey: 'netAR', type: ColumnType.DECIMAL },
      {
        label: model.agingReportSetup_Description_LineNum1,
        textKey: 'netARBucket1',
        type: ColumnType.DECIMAL
      },
      {
        label: model.agingReportSetup_Description_LineNum2,
        textKey: 'netARBucket2',
        type: ColumnType.DECIMAL
      },
      {
        label: model.agingReportSetup_Description_LineNum3,
        textKey: 'netARBucket3',
        type: ColumnType.DECIMAL
      },
      {
        label: model.agingReportSetup_Description_LineNum4,
        textKey: 'netARBucket4',
        type: ColumnType.DECIMAL
      },
      {
        label: model.agingReportSetup_Description_LineNum5,
        textKey: 'netARBucket5',
        type: ColumnType.DECIMAL
      },
      {
        label: model.agingReportSetup_Description_LineNum6,
        textKey: 'netARBucket6',
        type: ColumnType.DECIMAL
      },
      {
        label: 'LABEL.RESPONSIBLE_BY',
        textKey: 'responsibleName'
      },
      {
        label: 'LABEL.BUSINESS_UNIT',
        textKey: 'businessUnitId'
      },
      {
        label: 'LABEL.BUSINESS_TYPE',
        textKey: 'businessTypeDesc'
      },
      {
        label: 'LABEL.CREDIT_APPLICATION_ID',
        textKey: 'creditAppId'
      },
      {
        label: 'LABEL.MAIN_AGREEMENT_ID',
        textKey: 'mainAgreementId'
      }
    ];
    switch (masterRoute) {
      case EXPORT_AGING_FACTORING:
        return columnsAgingFactoring;
      case EXPORT_AGING_PROJECT_FINANCE:
        return columnsAgingProjectFinance;
    }
  }
}
