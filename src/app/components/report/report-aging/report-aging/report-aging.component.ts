import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, ColumnType, SortType, AccessMode } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { cloneObject, isNullOrUndefined, isSelectAll, isUndefinedOrZeroLength, isUpdateMode } from 'shared/functions/value.function';
import {
  ColumnModel,
  FormValidationModel,
  OptionExportModel,
  OptionModel,
  PageInformationModel,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CustomerAgingView } from 'shared/models/viewModel/CustomerAgingView';
import { ReportAgingView } from 'shared/models/viewModel/reportAgingView';
import { ReportAgingService } from '../report-aging.service';
import { ReportAgingCustomComponent } from './report-aging-custom.component';
@Component({
  selector: 'report-aging',
  templateUrl: './report-aging.component.html',
  styleUrls: ['./report-aging.component.scss']
})
export class ReportAgingComponent extends BaseItemComponent<ReportAgingView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  searchResult: SearchResult<CustomerAgingView> = new SearchResult();
  buyerTableOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  suspenseInvoiceTypeOptions: SelectItems[] = [];

  constructor(private service: ReportAgingService, private currentActivatedRoute: ActivatedRoute, public custom: ReportAgingCustomComponent) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {
    this.custom.setTitle();
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialCreatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.baseDropdown.getSuspenseInvoiceTypeEnumDropDown(this.suspenseInvoiceTypeOptions);
  }
  getById(): Observable<ReportAgingView> {
    return of(new ReportAgingView());
  }
  getInitialData(): Observable<ReportAgingView> {
    return this.service.GetReportAgingInitialData(this.userDataService.getCurrentCompanyGUID());
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin([this.baseDropdown.getBuyerTableDropDown(), this.baseDropdown.getCustomerTableDropDown()]).subscribe(
      ([buyerTable, customerTable]) => {
        this.buyerTableOptions = buyerTable;
        this.customerTableOptions = customerTable;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    let option = new OptionExportModel();
    option.exportFileName = this.custom.getExportFileName();
    option.columns = this.custom.setColumn(this.model);
    this.service.getReportAgingForExport(this.model).subscribe((result) => {
      this.fileService.getReportToExcel(result, option);
    });
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onClose(): void {
    super.onBack();
  }
}
