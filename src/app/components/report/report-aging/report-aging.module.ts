import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReportAgingService } from './report-aging.service';
import { ReportAgingComponent } from './report-aging/report-aging.component';
import { ReportAgingCustomComponent } from './report-aging/report-aging-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ReportAgingComponent],
  providers: [ReportAgingService, ReportAgingCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ReportAgingComponent]
})
export class ReportAgingComponentModule {}
