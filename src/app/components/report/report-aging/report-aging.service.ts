import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { FileInformation, PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { ReportAgingView } from 'shared/models/viewModel/reportAgingView';
@Injectable()
export class ReportAgingService {
  serviceKey = 'reportAgingGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getReportAgingForExport(vwModel: ReportAgingView): Observable<any> {
    const url = `${this.servicePath}/GetExportReportAging`;
    return this.dataGateway.post(url, vwModel);
  }
  GetReportAgingInitialData(companyId: string): Observable<any> {
    const url = `${this.servicePath}/GetReportAgingInitialData/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
}
