import { Observable, of } from 'rxjs';
import { EmptyGuid, ProductType, RefType, SuspenseInvoiceType } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { ReportAccruedInterestView } from 'shared/models/viewModel/reportAccruedInterestView';
const firstGroup = ['PRODUCT_TYPE', 'SUSPENSE_INVOICE_TYPE', 'REF_TYPE'];

export class ReportAccruedInterestCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: ReportAccruedInterestView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<ReportAccruedInterestView> {
    let model = new ReportAccruedInterestView();
    model.companyGUID = this.baseService.userDataService.getCurrentCompanyGUID();
    model.productType = [ProductType.Factoring, ProductType.ProjectFinance];
    model.suspenseInvoiceType = SuspenseInvoiceType.None;
    model.refType = [RefType.PurchaseTable, RefType.PurchaseLine, RefType.WithdrawalTable, RefType.WithdrawalLine];
    return of(model);
  }
}
