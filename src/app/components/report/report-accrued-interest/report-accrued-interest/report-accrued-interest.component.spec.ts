import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportAccruedInterestComponent } from './report-accrued-interest.component';

describe('ReportAccruedInterestViewComponent', () => {
  let component: ReportAccruedInterestComponent;
  let fixture: ComponentFixture<ReportAccruedInterestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReportAccruedInterestComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportAccruedInterestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
