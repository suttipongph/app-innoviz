import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { UserDataService } from 'core/services/user-data.service';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, ProductType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { ReportAccruedInterestView } from 'shared/models/viewModel/reportAccruedInterestView';
import { ReportAccruedInterestService } from '../report-accrued-interest.service';
import { ReportAccruedInterestCustomComponent } from './report-accrued-interest-custom.component';
@Component({
  selector: 'report-accrued-interest',
  templateUrl: './report-accrued-interest.component.html',
  styleUrls: ['./report-accrued-interest.component.scss']
})
export class ReportAccruedInterestComponent extends BaseItemComponent<ReportAccruedInterestView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  buyerTableOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  invoiceTypeOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  suspenseInvoiceTypeOptions: SelectItems[] = [];

  constructor(
    private service: ReportAccruedInterestService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: ReportAccruedInterestCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialCreatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.baseDropdown.getSuspenseInvoiceTypeEnumDropDown(this.suspenseInvoiceTypeOptions);
  }
  getById(): Observable<ReportAccruedInterestView> {
    return of(new ReportAccruedInterestView());
  }
  getInitialData(): Observable<ReportAccruedInterestView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin([
      this.baseDropdown.getBuyerTableDropDown(),
      this.baseDropdown.getCustomerTableDropDown(),
      this.baseDropdown.getInvoiceTypeByProductTypeDropDown([
        ProductType.None.toString(),
        ProductType.Factoring.toString(),
        ProductType.ProjectFinance.toString()
      ])
    ]).subscribe(
      ([buyerTable, customerTable, invoiceType]) => {
        this.buyerTableOptions = buyerTable;
        this.customerTableOptions = customerTable;
        this.invoiceTypeOptions = invoiceType;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    this.service.getReportAccruedInterestForExport(this.model).subscribe((result) => {
      this.fileService.exportAsExcelFile(result.base64, result.fileName);
      super.onBack();
    });
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onClose(): void {
    super.onBack();
  }
}
