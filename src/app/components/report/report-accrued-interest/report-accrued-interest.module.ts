import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReportAccruedInterestService } from './report-accrued-interest.service';
import { ReportAccruedInterestComponent } from './report-accrued-interest/report-accrued-interest.component';
import { ReportAccruedInterestCustomComponent } from './report-accrued-interest/report-accrued-interest-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ReportAccruedInterestComponent],
  providers: [ReportAccruedInterestService, ReportAccruedInterestCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ReportAccruedInterestComponent]
})
export class ReportAccruedInterestComponentModule {}
