import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { FileInformation, PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { ReportAccruedInterestView } from 'shared/models/viewModel/reportAccruedInterestView';
@Injectable()
export class ReportAccruedInterestService {
  serviceKey = 'reportAccruedInterestGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getReportAccruedInterestForExport(vwModel: ReportAccruedInterestView): Observable<FileInformation> {
    const url = `${this.servicePath}/GetExportReportAccruedInterest`;
    return this.dataGateway.post(url, vwModel);
  }
}
