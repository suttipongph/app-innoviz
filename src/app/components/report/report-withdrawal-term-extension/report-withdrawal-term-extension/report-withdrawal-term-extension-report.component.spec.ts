import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RptReportWithdrawalTermExtensionReportComponent } from './report-withdrawal-term-extension-report.component';

describe('ReportWithdrawalTermExtensionReportViewComponent', () => {
  let component: RptReportWithdrawalTermExtensionReportComponent;
  let fixture: ComponentFixture<RptReportWithdrawalTermExtensionReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RptReportWithdrawalTermExtensionReportComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RptReportWithdrawalTermExtensionReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
