import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { RptReportWithdrawalTermExtensionReportView } from 'shared/models/viewModel/rptReportWithdrawalTermExtensionReportView';

const firstGroup = [];

export class RptReportWithdrawalTermExtensionReportCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: RptReportWithdrawalTermExtensionReportView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<RptReportWithdrawalTermExtensionReportView> {
    let model = new RptReportWithdrawalTermExtensionReportView();
    model.reportWithdrawalTermExtensionGUID = EmptyGuid;
    return of(model);
  }
}
