import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReportWithdrawalTermExtensionService } from './report-withdrawal-term-extension.service';
import { RptReportWithdrawalTermExtensionReportComponent } from './report-withdrawal-term-extension/report-withdrawal-term-extension-report.component';
import { RptReportWithdrawalTermExtensionReportCustomComponent } from './report-withdrawal-term-extension/report-withdrawal-term-extension-report-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [RptReportWithdrawalTermExtensionReportComponent],
  providers: [ReportWithdrawalTermExtensionService, RptReportWithdrawalTermExtensionReportCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [RptReportWithdrawalTermExtensionReportComponent]
})
export class ReportWithdrawalTermExtensionComponentModule {}
