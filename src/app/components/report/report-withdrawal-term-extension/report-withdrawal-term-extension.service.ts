import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { RptReportWithdrawalTermExtensionReportView } from 'shared/models/viewModel/rptReportWithdrawalTermExtensionReportView';
@Injectable()
export class ReportWithdrawalTermExtensionService {
  serviceKey = 'reportWithdrawalTermExtensionGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
}
