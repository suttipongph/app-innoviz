import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RptReportServiceFeeFactRealizationReportComponent } from './report-service-fee-fact-realization-report.component';

describe('ReportServiceFeeFactRealizationReportViewComponent', () => {
  let component: RptReportServiceFeeFactRealizationReportComponent;
  let fixture: ComponentFixture<RptReportServiceFeeFactRealizationReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RptReportServiceFeeFactRealizationReportComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RptReportServiceFeeFactRealizationReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
