import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { RptReportServiceFeeFactRealizationReportView } from 'shared/models/viewModel/rptReportServiceFeeFactRealizationReportView';

const firstGroup = [];

export class RptReportServiceFeeFactRealizationReportCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: RptReportServiceFeeFactRealizationReportView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<RptReportServiceFeeFactRealizationReportView> {
    let model = new RptReportServiceFeeFactRealizationReportView();
    model.reportServiceFeeFactRealizationGUID = EmptyGuid;
    return of(model);
  }
}
