import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseReportComponent } from 'core/components/base-report/base-report.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { forkJoin, Observable, of } from 'rxjs';
import { REPORT_NAME } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { RptReportServiceFeeFactRealizationReportView } from 'shared/models/viewModel/rptReportServiceFeeFactRealizationReportView';
import { ReportServiceFeeFactRealizationService } from '../report-service-fee-fact-realization.service';
import { RptReportServiceFeeFactRealizationReportCustomComponent } from './report-service-fee-fact-realization-report-custom.component';

@Component({
  selector: 'report-service-fee-fact-realization-report',
  templateUrl: './report-service-fee-fact-realization-report.component.html',
  styleUrls: ['./report-service-fee-fact-realization-report.component.scss']
})
export class RptReportServiceFeeFactRealizationReportComponent
  extends BaseReportComponent<RptReportServiceFeeFactRealizationReportView>
  implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  buyerTableOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];

  constructor(
    private service: ReportServiceFeeFactRealizationService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: RptReportServiceFeeFactRealizationReportCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.reportName = REPORT_NAME.RA02_SERVICEFEEFACTORINGREALIZAITION;
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<RptReportServiceFeeFactRealizationReportView> {
    return of(new RptReportServiceFeeFactRealizationReportView());
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {}

  onAsyncRunner(model?: any): void {
    forkJoin([this.baseDropdown.getBuyerTableDropDown(), this.baseDropdown.getCustomerTableDropDown()]).subscribe(
      ([buyerTable, customerTable]) => {
        this.buyerTableOptions = buyerTable;
        this.customerTableOptions = customerTable;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  onSave(validation: FormValidationModel): void {}
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true, this.model);
      }
    });
  }
  onSubmit(isColsing: boolean, model: RptReportServiceFeeFactRealizationReportView): void {
    super.submitReportParameters(model);
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
