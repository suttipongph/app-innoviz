import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReportServiceFeeFactRealizationService } from './report-service-fee-fact-realization.service';
import { RptReportServiceFeeFactRealizationReportComponent } from './report-service-fee-fact-realization/report-service-fee-fact-realization-report.component';
import { RptReportServiceFeeFactRealizationReportCustomComponent } from './report-service-fee-fact-realization/report-service-fee-fact-realization-report-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [RptReportServiceFeeFactRealizationReportComponent],
  providers: [ReportServiceFeeFactRealizationService, RptReportServiceFeeFactRealizationReportCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [RptReportServiceFeeFactRealizationReportComponent]
})
export class ReportServiceFeeFactRealizationComponentModule {}
