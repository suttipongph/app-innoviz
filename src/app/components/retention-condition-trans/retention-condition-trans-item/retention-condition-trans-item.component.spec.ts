import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetentionConditionTransItemComponent } from './retention-condition-trans-item.component';

describe('RetentionConditionTransItemViewComponent', () => {
  let component: RetentionConditionTransItemComponent;
  let fixture: ComponentFixture<RetentionConditionTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RetentionConditionTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetentionConditionTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
