import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { RetentionCalculateBase } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { RetentionConditionTransItemView } from 'shared/models/viewModel';
import { RetentionConditionTransService } from '../retention-condition-trans.service';
import { RetentionConditionTransItemCustomComponent } from './retention-condition-trans-item-custom.component';
@Component({
  selector: 'retention-condition-trans-item',
  templateUrl: './retention-condition-trans-item.component.html',
  styleUrls: ['./retention-condition-trans-item.component.scss']
})
export class RetentionConditionTransItemComponent extends BaseItemComponent<RetentionConditionTransItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  productTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  retentionCalculateBaseOptions: SelectItems[] = [];
  retentionDeductionMethodOptions: SelectItems[] = [];

  constructor(
    private service: RetentionConditionTransService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: RetentionConditionTransItemCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.baseDropdown.getRetentionCalculateBaseEnumDropDown(this.retentionCalculateBaseOptions);
    this.baseDropdown.getRetentionDeductionMethodEnumDropDown(this.retentionDeductionMethodOptions);
    this.custom.retentionCalculateBaseOptionsCopy = Object.assign([], this.retentionCalculateBaseOptions);
  }
  getById(): Observable<RetentionConditionTransItemView> {
    return this.service.getRetentionConditionTransById(this.id);
  }
  getInitialData(): Observable<RetentionConditionTransItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.model.retentionDeductionMethod = null;
      this.model.retentionCalculateBase = null;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    let tempModel: RetentionConditionTransItemView = this.isUpdateMode ? model : this.model;
    forkJoin(
      this.custom.getRetentionCalculateBaseOptionsByRdm(tempModel, this.service),
      this.custom.getRetentionCalculateMethodOptionsByRdm(tempModel, this.service)
    ).subscribe(
      ([result, result2]) => {
        this.retentionCalculateBaseOptions = this.custom.retentionCalculateBaseOptionsCopy.filter((t) => result.includes(t.value));
        this.retentionDeductionMethodOptions = this.retentionDeductionMethodOptions.filter((t) => result2.includes(t.value));
        this.model.retentionCalculateBase = this.retentionCalculateBaseOptions.length > 1 ? this.retentionCalculateBaseOptions[1].value : 0;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model, this.isWorkFlowMode()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateRetentionConditionTrans(this.model), isColsing);
    } else {
      super.onCreate(this.service.createRetentionConditionTrans(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onRetentionDeductionMethodChange() {
    this.model.retentionPct = 0;
    this.model.retentionAmount = 0;
    this.model.retentionCalculateBase = 0;
    if (!isNullOrUndefOrEmptyGUID(this.model.retentionDeductionMethod)) {
      this.setRetentionCalculateBaseOptions();
    }
  }
  onRetentionCalculateBaseChange() {
    this.model.retentionPct = 0;
    this.model.retentionAmount = 0;
    this.setFieldAccessingByRetentionDeductionMethod();
  }
  setFieldAccessingByRetentionDeductionMethod() {
    this.custom.getFieldAccessingByRetentionDeductionMethod(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  setRetentionCalculateBaseOptions() {
    this.custom.getRetentionCalculateBaseOptionsByRdm(this.model, this.service).subscribe((result) => {
      this.retentionCalculateBaseOptions = this.custom.retentionCalculateBaseOptionsCopy.filter((t) => result.includes(t.value));
      this.model.retentionCalculateBase = this.retentionCalculateBaseOptions.length > 0 ? this.retentionCalculateBaseOptions[0].value : null;
      this.setFieldAccessingByRetentionDeductionMethod();
    });
  }
}
