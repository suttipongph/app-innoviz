import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { CreditAppRequestStatus, RetentionCalculateBase, RetentionDeductionMethod } from 'shared/constants';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, RetentionConditionTransItemView } from 'shared/models/viewModel';
import { RetentionConditionTransService } from '../retention-condition-trans.service';

const firstGroup = ['PRODUCT_TYPE', 'REF_TYPE', 'REF_ID'];
const retention_calculate_base = ['RETENTION_CALCULATE_BASE'];
const retention_amount = ['RETENTION_AMOUNT'];
const retention_pct = ['RETENTION_PCT'];
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const CREDIT_APP_TABLE = 'creditapptable';
export class RetentionConditionTransItemCustomComponent {
  baseService: BaseServiceModel<any>;
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  isCalcBaseMandatory = false;
  isAmountMandatory = false;
  isPctMandatory = false;
  retentionCalculateBaseOptionsCopy: SelectItems[] = [];
  getFieldAccessing(model: RetentionConditionTransItemView): Observable<FieldAccessing[]> {
    return this.getFieldAccessingByRetentionDeductionMethodInit(model).pipe(
      map((result) => {
        result.push({ filedIds: firstGroup, readonly: true });
        return result;
      }),
      catchError((err) => {
        this.baseService.notificationService.showErrorMessageFromResponse(err);
        return of([{ filedIds: firstGroup, readonly: true }]);
      })
    );
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: RetentionConditionTransItemView, isWorkFlowMode: boolean): Observable<boolean> {
    return this.setAccessModeByParentStatus(model, isWorkFlowMode).pipe(
      map((result) => (isUpdateMode(model.retentionConditionTransGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  setAccessModeByParentStatus(model: RetentionConditionTransItemView, isWorkFlowMode: boolean): Observable<AccessModeView> {
    let parentId = this.baseService.uiService.getRelatedInfoOriginTableKey();
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        return this.baseService.service.getAccessModeByCreditAppRequestTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
            this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
            this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
            this.accessModeView.canView = isDraftStatus ? true : this.accessModeView.canView && isWorkFlowMode;
          })
        );
      case CREDIT_APP_TABLE:
        this.accessModeView.canCreate = false;
        this.accessModeView.canDelete = false;
        this.accessModeView.canView = false;
        return of(this.accessModeView);
      default:
        return of(this.accessModeView);
    }
  }

  getFieldAccessingByRetentionDeductionMethodInit(model: RetentionConditionTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];

    if (isUpdateMode(model.retentionConditionTransGUID) && model.retentionDeductionMethod != RetentionDeductionMethod.CustomerSpecific) {
      if (model.retentionCalculateBase == RetentionCalculateBase.FixedAmount) {
        fieldAccessing.push({ filedIds: retention_amount, readonly: false });
        fieldAccessing.push({ filedIds: retention_calculate_base, readonly: false });
        this.isCalcBaseMandatory = true;
        this.isAmountMandatory = true;
      } else {
        fieldAccessing.push({ filedIds: retention_pct, readonly: false });
        fieldAccessing.push({ filedIds: retention_calculate_base, readonly: false });
        this.isPctMandatory = true;
        this.isCalcBaseMandatory = true;
      }
    } else {
      this.isAmountMandatory = false;
      this.isPctMandatory = false;
      this.isCalcBaseMandatory = false;
      fieldAccessing.push({ filedIds: retention_calculate_base, readonly: true });
      fieldAccessing.push({ filedIds: retention_amount, readonly: true });
      fieldAccessing.push({ filedIds: retention_pct, readonly: true });
    }
    return of(fieldAccessing);
  }
  getFieldAccessingByRetentionDeductionMethod(model: RetentionConditionTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (model.retentionDeductionMethod != RetentionDeductionMethod.CustomerSpecific) {
      fieldAccessing.push({ filedIds: retention_calculate_base, readonly: false });
      this.isCalcBaseMandatory = true;
      if (model.retentionCalculateBase == RetentionCalculateBase.FixedAmount) {
        fieldAccessing.push({ filedIds: retention_amount, readonly: false });
        fieldAccessing.push({ filedIds: retention_pct, readonly: true });
        this.isAmountMandatory = true;
        this.isPctMandatory = false;
      } else {
        fieldAccessing.push({ filedIds: retention_amount, readonly: true });
        fieldAccessing.push({ filedIds: retention_pct, readonly: false });
        this.isAmountMandatory = false;
        this.isPctMandatory = true;
      }
    } else {
      fieldAccessing.push({ filedIds: retention_calculate_base, readonly: true });
      fieldAccessing.push({ filedIds: retention_amount, readonly: true });
      fieldAccessing.push({ filedIds: retention_pct, readonly: true });
      this.isAmountMandatory = false;
      this.isPctMandatory = false;
      this.isCalcBaseMandatory = false;
    }
    return of(fieldAccessing);
  }
  getRetentionCalculateBaseOptionsByRdm(model: RetentionConditionTransItemView, service: RetentionConditionTransService): Observable<number[]> {
    if (isNullOrUndefined(model.retentionDeductionMethod)) {
      return of([]);
    } else {
      return service.GetRetentionCalculateBase(model);
    }
  }
  getRetentionCalculateMethodOptionsByRdm(model: RetentionConditionTransItemView, service: RetentionConditionTransService): Observable<number[]> {
    if (isNullOrUndefined(model.productType)) {
      return of([]);
    } else {
      let item = new RetentionConditionTransItemView();
      if (!isUpdateMode(model.retentionConditionTransGUID)) {
        item.companyGUID = this.baseService.userDataService.getCurrentCompanyGUID();
        item.productType = model.productType;
        item.retentionCalculateBase = 0;
        item.retentionDeductionMethod = 0;
        item.refType = 0;
        return service.getRetentionCalculateMethod(item);
      } else {
        return service.getRetentionCalculateMethod(model);
      }
    }
  }
}
