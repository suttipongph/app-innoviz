import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { RetentionConditionTransListView } from 'shared/models/viewModel';
import { RetentionConditionTransService } from '../retention-condition-trans.service';
import { RetentionConditionTransListCustomComponent } from './retention-condition-trans-list-custom.component';

@Component({
  selector: 'retention-condition-trans-list',
  templateUrl: './retention-condition-trans-list.component.html',
  styleUrls: ['./retention-condition-trans-list.component.scss']
})
export class RetentionConditionTransListComponent extends BaseListComponent<RetentionConditionTransListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  productTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  retentionCalculateBaseOptions: SelectItems[] = [];
  retentionDeductionMethodOptions: SelectItems[] = [];

  constructor(public custom: RetentionConditionTransListCustomComponent, private service: RetentionConditionTransService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.setAccessModeByParentStatus(this.parentId, this.isWorkFlowMode()).subscribe((result) => {
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.baseDropdown.getRetentionCalculateBaseEnumDropDown(this.retentionCalculateBaseOptions);
    this.baseDropdown.getRetentionDeductionMethodEnumDropDown(this.retentionDeductionMethodOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'retentionConditionTransGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: 'LABEL.RETENTION_DEDUCTION_METHOD',
        textKey: 'retentionDeductionMethod',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.ASC,
        masterList: this.retentionDeductionMethodOptions
      },
      {
        label: 'LABEL.RETENTION_CALCULATE_BASE',
        textKey: 'retentionCalculateBase',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.retentionCalculateBaseOptions
      },
      {
        label: 'LABEL.RETENTION_PCT',
        textKey: 'retentionPct',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE,
        format: this.PERCENT_5_2_POS
      },
      {
        label: 'LABEL.RETENTION_AMOUNT',
        textKey: 'retentionAmount',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<RetentionConditionTransListView>> {
    return this.service.getRetentionConditionTransToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteRetentionConditionTrans(row));
  }
}
