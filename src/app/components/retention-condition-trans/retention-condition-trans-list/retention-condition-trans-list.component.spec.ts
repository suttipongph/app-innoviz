import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RetentionConditionTransListComponent } from './retention-condition-trans-list.component';

describe('RetentionConditionTransListViewComponent', () => {
  let component: RetentionConditionTransListComponent;
  let fixture: ComponentFixture<RetentionConditionTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RetentionConditionTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetentionConditionTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
