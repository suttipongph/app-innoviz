import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RetentionConditionTransService } from './retention-condition-trans.service';
import { RetentionConditionTransListComponent } from './retention-condition-trans-list/retention-condition-trans-list.component';
import { RetentionConditionTransItemComponent } from './retention-condition-trans-item/retention-condition-trans-item.component';
import { RetentionConditionTransItemCustomComponent } from './retention-condition-trans-item/retention-condition-trans-item-custom.component';
import { RetentionConditionTransListCustomComponent } from './retention-condition-trans-list/retention-condition-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [RetentionConditionTransListComponent, RetentionConditionTransItemComponent],
  providers: [RetentionConditionTransService, RetentionConditionTransItemCustomComponent, RetentionConditionTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [RetentionConditionTransListComponent, RetentionConditionTransItemComponent]
})
export class RetentionConditionTransComponentModule {}
