import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, RetentionConditionTransItemView, RetentionConditionTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { RetentionDeductionMethod } from 'shared/constants';
@Injectable()
export class RetentionConditionTransService {
  serviceKey = 'retentionConditionTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getRetentionConditionTransToList(search: SearchParameter): Observable<SearchResult<RetentionConditionTransListView>> {
    const url = `${this.servicePath}/GetRetentionConditionTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteRetentionConditionTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteRetentionConditionTrans`;
    return this.dataGateway.delete(url, row);
  }
  getRetentionConditionTransById(id: string): Observable<RetentionConditionTransItemView> {
    const url = `${this.servicePath}/GetRetentionConditionTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createRetentionConditionTrans(vmModel: RetentionConditionTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateRetentionConditionTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateRetentionConditionTrans(vmModel: RetentionConditionTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateRetentionConditionTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<RetentionConditionTransItemView> {
    const url = `${this.servicePath}/GetRetentionConditionTransInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByCreditAppRequestTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppRequestTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  GetRetentionCalculateBase(rdm: RetentionConditionTransItemView): Observable<number[]> {
    const url = `${this.servicePath}/GetRetentionCalculateBase`;
    return this.dataGateway.post(url, rdm);
  }
  getRetentionCalculateMethod(rdm: RetentionConditionTransItemView): Observable<number[]> {
    const url = `${this.servicePath}/GetRetentionCalculateMethod`;
    return this.dataGateway.post(url, rdm);
  }
}
