import { Component, Input } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CreditAppRequestLineAmendListView } from 'shared/models/viewModel';
import { CreditAppRequestLineAmendService } from '../credit-app-request-line-amend.service';
import { CreditAppRequestLineAmendListCustomComponent } from './credit-app-request-line-amend-list-custom.component';

@Component({
  selector: 'credit-app-request-line-amend-list',
  templateUrl: './credit-app-request-line-amend-list.component.html',
  styleUrls: ['./credit-app-request-line-amend-list.component.scss']
})
export class CreditAppRequestLineAmendListComponent extends BaseListComponent<CreditAppRequestLineAmendListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  creditAppRequestTypeOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];

  constructor(public custom: CreditAppRequestLineAmendListCustomComponent, private service: CreditAppRequestLineAmendService) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getCreditAppRequestTypeEnumDropDown(this.creditAppRequestTypeOptions);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'creditAppRequestLineAmendGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.CREDIT_APPLICATION_REQUEST_ID',
        textKey: 'creditAppRequestTable_CreditAppRequestId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'creditAppRequestTable_Description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CREDIT_APPLICATION_REQUEST_TYPE',
        textKey: 'creditAppRequestTable_CreditAppRequestType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditAppRequestTypeOptions
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'creditAppRequestTable_DocumentStatusGUID',
        sortingKey: 'documentStatus_StatusId'
      },
      {
        label: 'LABEL.REQUEST_DATE',
        textKey: 'creditAppRequestTable_RequestDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        sortingKey: 'customerTable_CustomerId',
        searchingKey: 'creditAppRequestTable_CustomerTableGUID'
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerTableOptions,
        sortingKey: 'buyerTable_BuyerId',
        searchingKey: 'creditAppRequestTable_BuyerTableGUID'
      },
      {
        label: null,
        textKey: 'creditAppRequestLine_RefCreditAppLineGUID',
        type: ColumnType.MASTER,
        visibility: false,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CreditAppRequestLineAmendListView>> {
    return this.service.getCreditAppRequestLineAmendToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCreditAppRequestLineAmend(row));
  }
}
