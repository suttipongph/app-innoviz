import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditAppRequestLineAmendListComponent } from './credit-app-request-line-amend-list.component';

describe('CreditAppRequestLineAmendListViewComponent', () => {
  let component: CreditAppRequestLineAmendListComponent;
  let fixture: ComponentFixture<CreditAppRequestLineAmendListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditAppRequestLineAmendListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppRequestLineAmendListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
