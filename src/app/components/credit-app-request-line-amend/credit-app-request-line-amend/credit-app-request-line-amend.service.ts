import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  AccessModeView,
  CreditAppRequestLineAmendItemView,
  CreditAppRequestLineAmendListView,
  CreditAppRequestLineItemView
} from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CreditAppRequestLineAmendService {
  serviceKey = 'creditAppRequestLineAmendGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCreditAppRequestLineAmendToList(search: SearchParameter): Observable<SearchResult<CreditAppRequestLineAmendListView>> {
    const url = `${this.servicePath}/GetCreditAppRequestLineAmendList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCreditAppRequestLineAmend(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCreditAppRequestLineAmend`;
    return this.dataGateway.delete(url, row);
  }
  getCreditAppRequestLineAmendById(id: string): Observable<CreditAppRequestLineAmendItemView> {
    const url = `${this.servicePath}/GetCreditAppRequestLineAmendById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCreditAppRequestLineAmend(vmModel: CreditAppRequestLineAmendItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCreditAppRequestLineAmend`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCreditAppRequestLineAmend(vmModel: CreditAppRequestLineAmendItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCreditAppRequestLineAmend`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(creditAppLineId: string): Observable<CreditAppRequestLineAmendItemView> {
    const url = `${this.servicePath}/GetAmendCaLineInitialData/creditAppLineId=${creditAppLineId}`;
    return this.dataGateway.get(url);
  }
  validateIsManualNumberSeq(productType: number): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeq/productType=${productType}`;
    return this.dataGateway.get(url);
  }
  updateCreditAppRequestLineByAmendRate(vmModel: CreditAppRequestLineAmendItemView): Observable<CreditAppRequestLineItemView> {
    const url = `${this.servicePath}/UpdateCreditAppRequestLineByAmendRate`;
    return this.dataGateway.post(url, vmModel);
  }
  updateCreditAppRequestLineByAmendAssignmentMethod(vmModel: CreditAppRequestLineAmendItemView): Observable<CreditAppRequestLineItemView> {
    const url = `${this.servicePath}/UpdateCreditAppRequestLineByAmendAssignmentMethod`;
    return this.dataGateway.post(url, vmModel);
  }
  updateCreditAppRequestLineByAmendBillingInformation(vmModel: CreditAppRequestLineAmendItemView): Observable<CreditAppRequestLineItemView> {
    const url = `${this.servicePath}/UpdateCreditAppRequestLineByAmendBillingInformation`;
    return this.dataGateway.post(url, vmModel);
  }
  updateCreditAppRequestLineByAmendReceiptInformation(vmModel: CreditAppRequestLineAmendItemView): Observable<CreditAppRequestLineItemView> {
    const url = `${this.servicePath}/UpdateCreditAppRequestLineByAmendReceiptInformation`;
    return this.dataGateway.post(url, vmModel);
  }
  updateCreditAppRequestLineByAmendBillingDocumentCondition(vmModel: CreditAppRequestLineAmendItemView): Observable<any> {
    const url = `${this.servicePath}/UpdateCreditAppRequestLineByAmendBillingDocumentCondition`;
    return this.dataGateway.post(url, vmModel);
  }
  updateCreditAppRequestLineByAmendReceiptDocumentCondition(vmModel: CreditAppRequestLineAmendItemView): Observable<any> {
    const url = `${this.servicePath}/UpdateCreditAppRequestLineByAmendReceiptDocumentCondition`;
    return this.dataGateway.post(url, vmModel);
  }
  getAccessModeByCreditAppRequestTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeAmendCaLineByCreditAppRequestTable/id=${id}`;
    return this.dataGateway.get(url);
  }
}
