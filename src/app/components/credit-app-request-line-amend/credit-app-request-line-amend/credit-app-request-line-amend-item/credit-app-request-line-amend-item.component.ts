import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, CreditAppRequestType, RefType } from 'shared/constants';
import { getModelRegistered, modelRegister } from 'shared/functions/model.function';
import { getDropDownLabel, isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { CreditAppRequestLineAmendItemView, RefIdParm } from 'shared/models/viewModel';
import { PrintSetBookDocTransView } from 'shared/models/viewModel/printSetBookDocTransView';
import { CreditAppRequestLineAmendService } from '../credit-app-request-line-amend.service';
import { CreditAppRequestLineAmendItemCustomComponent } from './credit-app-request-line-amend-item-custom.component';
@Component({
  selector: 'credit-app-request-line-amend-item',
  templateUrl: './credit-app-request-line-amend-item.component.html',
  styleUrls: ['./credit-app-request-line-amend-item.component.scss']
})
export class CreditAppRequestLineAmendItemComponent extends BaseItemComponent<CreditAppRequestLineAmendItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  creditAppRequestTypeOptions: SelectItems[] = [];
  purchaseFeeCalculateBaseOptions: SelectItems[] = [];
  originalPurchaseFeeCalculateBaseOptions: SelectItems[] = [];
  assignmentMethodOptions: SelectItems[] = [];
  addressOptions: SelectItems[] = [];
  methodOfPaymentOptions: SelectItems[] = [];
  receiptAddressOptions: SelectItems[] = [];
  blacklistStatusOptions: SelectItems[] = [];
  kycSetupOptions: SelectItems[] = [];
  creditScoringOptions: SelectItems[] = [];
  constructor(
    private service: CreditAppRequestLineAmendService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CreditAppRequestLineAmendItemCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {
    this.baseDropdown.getCreditAppRequestTypeEnumDropDown(this.creditAppRequestTypeOptions, [
      CreditAppRequestType.AmendCustomerBuyerCreditLimit,
      CreditAppRequestType.AmendBuyerInfo
    ]);
    this.baseDropdown.getPurchaseFeeCalculateBaseEnumDropDown(this.purchaseFeeCalculateBaseOptions);
  }
  getById(): Observable<CreditAppRequestLineAmendItemView> {
    return this.service.getCreditAppRequestLineAmendById(this.id);
  }
  getInitialData(): Observable<CreditAppRequestLineAmendItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions(result);
        this.setFunctionOptions(result);
        this.setWorkflowOption(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(
      this.baseDropdown.getAssignmentMethodDropDown(),
      this.baseDropdown.getBlacklistStatusDropDown(),
      this.baseDropdown.getCreditScoringDropDown(),
      this.baseDropdown.getKYCSetupDropDown()
    ).subscribe(
      ([assignmentMethod, blacklistStatus, creditScoring, kycSetup]) => {
        this.assignmentMethodOptions = assignmentMethod;
        this.blacklistStatusOptions = blacklistStatus;
        this.creditScoringOptions = creditScoring;
        this.kycSetupOptions = kycSetup;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
          this.getAddressTransByBuyerDropDown();
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model, this.isWorkFlowMode()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(model: CreditAppRequestLineAmendItemView): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        command: () => {
          const refIdParm: RefIdParm = {
            refType: RefType.CreditAppRequestTable,
            refGUID: model.creditAppRequestTable_CreditAppRequestTableGUID
          };
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: refIdParm
          });
        }
      },
      {
        label: 'LABEL.FINANCIAL',
        items: [
          {
            label: 'LABEL.FINANCIAL_STATEMENT',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.FINANCIAL_STATEMENT_TRANS,
                parameters: {
                  refGUID: model.creditAppRequestLineGUID,
                  refType: RefType.CreditAppRequestLine,
                  creditAppRequestTableGUID: model.creditAppRequestTable_CreditAppRequestTableGUID
                }
              })
          }
        ]
      },
      {
        label: 'LABEL.MANAGE',
        items: [
          {
            label: 'LABEL.ASSIGNMENT',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.ASSIGNMENT,
                parameters: {
                  creditAppRequestTable_Values: getDropDownLabel(
                    this.model.creditAppRequestTable_CreditAppRequestId,
                    this.model.creditAppRequestTable_Description
                  ),
                  customerTable_Values: this.model.customerTable_Values,
                  customerTableGUID: this.model.creditAppRequestTable_CustomerTableGUID,
                  creditAppRequestTableGUID: this.model.creditAppRequestTable_CreditAppRequestTableGUID
                }
              })
          },
          {
            label: 'LABEL.BUSINESS_COLLATERAL',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.BUSINESS_COLLATERAL,
                parameters: {
                  customerTableGUID: model.creditAppRequestTable_CustomerTableGUID,
                  customerTable_Values: this.model.customerTable_Values,
                  creditAppRequestTableGUID: model.creditAppRequestTable_CreditAppRequestTableGUID,
                  creditAppRequestTable_Values: getDropDownLabel(
                    model.creditAppRequestTable_CreditAppRequestId,
                    model.creditAppRequestTable_Description
                  )
                }
              })
          },
          {
            label: 'LABEL.SERVICE_FEE_CONDITION',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.SERVICE_FEE_CONDITION_TRANS,
                parameters: {
                  refGUID: model.creditAppRequestLineGUID,
                  creditAppRequestTable_CreditAppRequestTableGUID: model.creditAppRequestTable_CreditAppRequestTableGUID
                }
              })
          }
        ]
      },
      {
        label: 'LABEL.MEMO',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.MEMO })
      },
      {
        label: 'LABEL.BOOKMARK_DOCUMENT',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.BOOKMARK_DOCUMENT_TRANS,
            parameters: {
              refGUID: model.creditAppRequestTable_CreditAppRequestTableGUID,
              refType: RefType.CreditAppRequestTable
            }
          })
      },
      {
        label: 'LABEL.INQUIRY',
        items: [
          {
            label: 'LABEL.ASSIGNMENT_AGREEMENT_OUTSTANDING',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.ASSIGNMENT_AGREEMENT_OUTSTANDING,
                parameters: { creditAppRequestTableGUID: this.model.creditAppRequestTable_CreditAppRequestTableGUID }
              })
          },
          {
            label: 'LABEL.CA_BUYER_CREDIT_OUTSTANDING',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.CA_BUYER_CREDIT_OUTSTANDING,
                parameters: { creditAppRequestTableGUID: this.model.creditAppRequestTable_CreditAppRequestTableGUID }
              })
          },
          {
            label: 'LABEL.CREDIT_OUTSTANDING',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.CREDIT_OUTSTANDING,
                parameters: { creditAppRequestTableGUID: this.model.creditAppRequestTable_CreditAppRequestTableGUID }
              })
          }
        ]
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(model: CreditAppRequestLineAmendItemView): void {
    this.functionItems = [
      {
        label: 'LABEL.CANCEL_CREDIT_APPLICATION_REQUEST',
        disabled: !this.custom.isDraft(model),
        command: () =>
          this.toFunction({
            path: ROUTE_FUNCTION_GEN.CANCEL_CREDIT_APPLICATION_REQUEST,
            parameters: { creditAppRequestGUID: model.creditAppRequestTable_CreditAppRequestTableGUID }
          })
      },
      {
        label: 'LABEL.PRINT',
        items: [
          {
            label: 'LABEL.BOOKMARK_DOCUMENT',
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.PRINT_SET_BOOKMARK_DOCUMENT_TRANSACTION,
                parameters: {
                  model: this.setPrintSetBookDocTransData()
                }
              })
          }
        ]
      },
      {
        label: 'LABEL.UPDATE_STATUS',
        visible: this.custom.setIsAmendBuyerInfo(model),
        disabled: !this.custom.isDraft(model),
        command: () =>
          this.toFunction({
            path: ROUTE_FUNCTION_GEN.UPDATE_STATUS_AMEND_BUYER_INFO,
            parameters: {
              creditAppRequestId: model.creditAppRequestTable_CreditAppRequestId,
              description: model.creditAppRequestTable_Description,
              creditAppRequestType: model.creditAppRequestTable_CreditAppRequestType,
              customerId: model.customerTable_Values,
              approvedDate: model.creditAppRequestTable_ApprovedDate,
              updateStatusAmendBuyerInfoGUID: model.creditAppRequestLineGUID,
              creditAppRequestTableGUID: model.creditAppRequestTable_CreditAppRequestTableGUID
            }
          })
      }
    ];
    super.setFunctionOptionsMapping();
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateCreditAppRequestLineAmend(this.model), isColsing);
    } else {
      super.onCreate(this.service.createCreditAppRequestLineAmend(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onAmendRateChange(): void {
    this.custom.onAmendRateChange(this.model).subscribe((result) => {
      super.setBaseFieldAccessing([result]);
    });
  }
  onAmendAssignmentMethodeChange(): void {
    this.custom.onAmendAssignmentMethodeChange(this.model).subscribe((result) => {
      super.setBaseFieldAccessing([result]);
    });
  }
  onAmendBillingInformationChange(): void {
    this.custom.onAmendBillingInformationChange(this.model).subscribe((result) => {
      super.setBaseFieldAccessing([result]);
    });
  }

  onAmendBillingDocumentConditionChange(): void {
    this.custom.onAmendBillingDocumentConditionChange(this.model);
  }
  onAmendReceiptInformationChange(): void {
    this.custom.onAmendReceiptInformationChange(this.model).subscribe((result) => {
      super.setBaseFieldAccessing([result]);
    });
    this.onAmendReceiptInformationBinding();
  }
  onAmendReceiptInformationBinding(): void {
    this.baseDropdown.getMethodOfPaymentDropDown(this.methodOfPaymentOptions);
  }
  onAmendReceiptDocumentConditionChange(): void {
    this.custom.onAmendReceiptDocumentConditionChange(this.model);
  }
  onAssignmentMethodChange(): void {
    this.custom.onAssignmentMethodChange(this.model, this.assignmentMethodOptions);
  }
  onMethodOfPaymentChange(): void {
    this.custom.onMethodOfPaymentChange(this.model, this.methodOfPaymentOptions);
  }
  onBillingAddressChange(): void {
    this.custom.onBillingAddressChange(this.model, this.addressOptions);
  }
  onReceiptAddressChange(): void {
    this.custom.onReceiptAddressChange(this.model, this.addressOptions);
  }
  getAddressTransByBuyerDropDown(): void {
    if (!isNullOrUndefOrEmptyGUID(this.model.creditAppRequestLine_BuyerTableGUID)) {
      this.baseDropdown.getAddressTransByBuyerDropDown(this.model.creditAppRequestLine_BuyerTableGUID).subscribe((result) => {
        this.addressOptions = result;
      });
    } else {
      this.addressOptions.length = 0;
    }
  }
  setWorkflowOption(model: CreditAppRequestLineAmendItemView): void {
    super.setK2WorkflowOption(model, this.custom.getWorkFlowPath(model), this.custom.getDisabledWorkFlow(model));
  }
  setPrintSetBookDocTransData(): PrintSetBookDocTransView {
    return this.custom.setPrintSetBookDocTransData(this.model);
  }
}
