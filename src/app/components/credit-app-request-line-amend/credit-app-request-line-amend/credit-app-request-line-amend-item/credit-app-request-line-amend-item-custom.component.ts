import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConst, CreditAppRequestStatus, CreditAppRequestType, EmptyGuid, ProductType, RefType, WORKFLOW } from 'shared/constants';
import { toMapModel } from 'shared/functions/model.function';
import { getProductType, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { Confirmation, SelectItem } from 'shared/models/primeModel';
import { BaseServiceModel, FieldAccessing, PathParamModel, SelectItems } from 'shared/models/systemModel';
import {
  AccessModeView,
  AddressTransItemView,
  AssignmentMethodItemView,
  CreditAppRequestLineAmendItemView,
  CreditAppRequestLineItemView,
  CreditAppRequestTableItemView,
  MethodOfPaymentItemView
} from 'shared/models/viewModel';
import { PrintSetBookDocTransView } from 'shared/models/viewModel/printSetBookDocTransView';
import { CreditAppRequestLineAmendService } from '../credit-app-request-line-amend.service';

const NUMBER_SEQ = ['CREDIT_APP_REQUEST_ID'];
const firstGroup = [
  'DOCUMENT_STATUS',
  'CUSTOMER_ID',
  'BUYER_ID',
  'CUSTOMER_CREDIT_LIMIT',
  'REF_CREDIT_APP_ID',
  'ORIGINAL_CREDIT_LIMIT_LINE_REQUEST',
  'BUYER_CREDIT_LIMIT',
  'APPROVED_CREDIT_LIMIT_LINE_REQUEST',
  'ORIGINAL_MAX_PURCHASE_PCT',
  'ORIGINAL_PURCHASE_FEE_PCT',
  'ORIGINAL_PURCHASE_FEE_CALCULATE_BASE',
  'ORIGINAL_ASSIGNMENT_METHOD_GUID',
  'ORIGINAL_ASSIGNMENT_METHOD_REMARK',
  'ORIGINAL_BILLING_ADDRESS_GUID',
  'UNBOUND_ORIGINAL_BILLING_ADDRESS',
  'UNBOUND_NEW_BILLING_ADDRESS',
  'ORIGINAL_METHOD_OF_PAYMENT_GUID',
  'ORIGINAL_RECEIPT_REMARK',
  'ORIGINAL_RECEIPT_ADDRESS_GUID',
  'UNBOUND_ORIGINAL_RECEIPT_ADDRESS',
  'UNBOUND_NEW_RECEIPT_ADDRESS',
  'ALL_CUSTOMER_BUYER_OUTSTANDING',
  'CUSTOMER_BUYER_OUTSTANDING',
  'BLACKLIST_STATUS_GUID'
];
const READONLY_WHEN_UPDATE = ['CREDIT_APP_REQUEST_TYPE', 'CREDIT_APP_REQUEST_ID'];
const CONDITION1_2 = ['CREDIT_COMMENT', 'APPROVER_COMMENT', 'APPROVED_DATE'];
const condition3 = ['CREDIT_COMMENT'];
const condition4 = ['APPROVED_DATE', 'CREDIT_COMMENT', 'APPROVER_COMMENT', 'APPROVED_CREDIT_LIMIT_LINE_REQUEST'];
const condition5 = ['APPROVED_DATE', 'APPROVER_COMMENT', 'APPROVED_CREDIT_LIMIT_LINE_REQUEST'];
const RATE = ['NEW_MAX_PURCHASE_PCT', 'NEW_PURCHASE_FEE_PCT', 'NEW_PURCHASE_FEE_CALCULATE_BASE'];
const ASSIGNMENT_METHOD = ['NEW_ASSIGNMENT_METHOD_GUID', 'NEW_ASSIGNMENT_METHOD_REMARK'];
const BILLING_INFORMATION = ['NEW_BILLING_ADDRESS_GUID'];
const RECEIPT_INFORMATION = ['NEW_METHOD_OF_PAYMENT_GUID', 'NEW_RECEIPT_REMARK', 'NEW_RECEIPT_ADDRESS_GUID'];
const messageCopy = 'CONFIRM.90006';
const messageClear = 'CONFIRM.90007';
const labelAmendRate = 'LABEL.AMEND_RATE';
const labelAmendAssignmentMethod = 'LABEL.ASSIGNMENT_METHOD';
const labelAmendBillingInformation = 'LABEL.BILLING_INFORMATION';
const labelAmendBillingDocumentCondition = 'LABEL.BILLING_DOCUMENT_CONDITION_TRANSACTIONS';
const labelAmendReceiptInformation = 'LABEL.RECEIPT_INFORMATION';
const labelAmendReceiptDocumentCondition = 'LABEL.RECEIPT_DOCUMENT_CONDITION_TRANSACTIONS';
const CREDIT_APP_LINE = 'creditappline-child';
export class CreditAppRequestLineAmendItemCustomComponent {
  baseService: BaseServiceModel<any>;
  isAmendBuyerInfo: boolean = false;
  productType: ProductType = null;
  isManual: boolean = false;
  activeIndex: number = 0;
  checkAmendRate: boolean = true;
  checkAmendAssignmentMethod: boolean = true;
  checkAmendBillingInformation: boolean = true;
  checkAmendBillingDocumentCondition: boolean = true;
  checkAmendReceiptInformation: boolean = true;
  checkAmendReceiptDocumentCondition: boolean = true;
  accessModeView: AccessModeView = new AccessModeView();
  getFieldAccessing(model: CreditAppRequestLineAmendItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    const masterRoute = this.baseService.uiService.getMasterRoute();
    this.productType = Number(getProductType(masterRoute));
    return new Observable((observer) => {
      this.setIsAmendBuyerInfo(model);
      if (!this.canActiveByStatus(model.documentStatus_StatusId)) {
        this.setFieldAccessingByAmendRate(model.amendRate);
        this.setFieldAccessingByAmendAssignmentMethod(model.amendAssignmentMethod);
        this.setFieldAccessingByAmendBillingInformation(model.amendBillingInformation);
        this.setFieldAccessingByAmendReceiptInformation(model.amendReceiptInformation);
        this.setFieldAccessingByAmendBillingDocumentCondition(model.amendBillingDocumentCondition);
        this.setFieldAccessingByAmendReceiptDocumentCondition(model.amendReceiptDocumentCondition);
        observer.next(fieldAccessing);
        return;
      }
      fieldAccessing.push(...this.setFieldAccessingByStatus(model));
      if (!isUpdateMode(model.creditAppRequestLineAmendGUID)) {
        this.validateIsManualNumberSeq(this.productType).subscribe(
          (isManual) => {
            this.isManual = isManual;
            fieldAccessing.push({ filedIds: NUMBER_SEQ, readonly: !this.isManual });
            observer.next(fieldAccessing);
          },
          (error) => {
            observer.next(fieldAccessing);
          }
        );
      } else {
        fieldAccessing.push({ filedIds: READONLY_WHEN_UPDATE, readonly: true });
        observer.next(fieldAccessing);
      }
    });
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CreditAppRequestLineAmendItemView, isWorkFlowMode: boolean): Observable<boolean> {
    this.setAccessModeByParent(model, false);
    return this.setAccessModeByParent(model, isWorkFlowMode).pipe(
      map((result) => (isUpdateMode(model.creditAppRequestLineAmendGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  setAccessModeByParent(model: CreditAppRequestLineAmendItemView, isWorkFlowMode: boolean): Observable<AccessModeView> {
    const parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (parentName) {
      case CREDIT_APP_LINE:
        if (!isUpdateMode(model.creditAppRequestLineAmendGUID)) {
          this.accessModeView.canCreate = true;
          this.accessModeView.canView = true;
          return of(this.accessModeView);
        }
        return this.getAccessModeByCreditAppRequestTable(model.creditAppRequestTable_CreditAppRequestTableGUID, isWorkFlowMode);
      default:
        return of(this.accessModeView);
    }
  }
  getAccessModeByCreditAppRequestTable(parentId: string, isWorkFlowMode: boolean): Observable<AccessModeView> {
    return this.baseService.service.getAccessModeByCreditAppRequestTable(parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
        const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
        const accessBySN = isDraftStatus ? true : isWorkFlowMode;
        this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && accessBySN;
        this.accessModeView.canView = isDraftStatus ? true : this.accessModeView.canView && accessBySN;
      })
    );
  }
  validateIsManualNumberSeq(productType: number): Observable<boolean> {
    return this.baseService.service.validateIsManualNumberSeq(productType);
  }
  canActiveByStatus(status: string): boolean {
    return (
      status <= CreditAppRequestStatus.Draft ||
      status === CreditAppRequestStatus.WaitingForMarketingStaff ||
      status === CreditAppRequestStatus.WaitingForCreditStaff ||
      status === CreditAppRequestStatus.WaitingForCreditHead ||
      status === CreditAppRequestStatus.WaitingForAssistMD
    );
  }
  setFieldAccessingByStatus(model: CreditAppRequestLineAmendItemView): FieldAccessing[] {
    const fieldAccessing: FieldAccessing[] = [];
    // <= 110
    if (model.documentStatus_StatusId <= CreditAppRequestStatus.WaitingForMarketingStaff) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      fieldAccessing.push(this.setFieldAccessingByAmendRate(model.amendRate));
      fieldAccessing.push(this.setFieldAccessingByAmendAssignmentMethod(model.amendAssignmentMethod));
      fieldAccessing.push(this.setFieldAccessingByAmendBillingInformation(model.amendBillingInformation));
      fieldAccessing.push(this.setFieldAccessingByAmendReceiptInformation(model.amendReceiptInformation));
      this.setFieldAccessingByAmendBillingDocumentCondition(model.amendBillingDocumentCondition);
      this.setFieldAccessingByAmendReceiptDocumentCondition(model.amendReceiptDocumentCondition);
      const isAmendBuyerInfo = model.creditAppRequestTable_CreditAppRequestType === CreditAppRequestType.AmendBuyerInfo;
      fieldAccessing.push({ filedIds: CONDITION1_2, readonly: !isAmendBuyerInfo });
    } else if (model.documentStatus_StatusId === CreditAppRequestStatus.WaitingForCreditStaff) {
      fieldAccessing.push({ filedIds: [AppConst.ALL_ID], readonly: true }, { filedIds: condition3, readonly: false });
    } else if (model.documentStatus_StatusId === CreditAppRequestStatus.WaitingForCreditHead) {
      fieldAccessing.push({ filedIds: [AppConst.ALL_ID], readonly: true }, { filedIds: condition4, readonly: false });
    } else if (model.documentStatus_StatusId === CreditAppRequestStatus.WaitingForAssistMD) {
      fieldAccessing.push({ filedIds: [AppConst.ALL_ID], readonly: true }, { filedIds: condition5, readonly: false });
    }
    return fieldAccessing;
  }
  setFieldAccessingByAmendRate(amendRate: boolean): FieldAccessing {
    this.checkAmendRate = !amendRate;
    return { filedIds: RATE, readonly: !amendRate };
  }
  setFieldAccessingByAmendAssignmentMethod(amendAssignmentMethod: boolean): FieldAccessing {
    this.checkAmendAssignmentMethod = !amendAssignmentMethod;
    return { filedIds: ASSIGNMENT_METHOD, readonly: !amendAssignmentMethod };
  }
  setFieldAccessingByAmendBillingInformation(amendBillingInformation: boolean): FieldAccessing {
    this.checkAmendBillingInformation = !amendBillingInformation;
    return { filedIds: BILLING_INFORMATION, readonly: !amendBillingInformation };
  }
  setFieldAccessingByAmendBillingDocumentCondition(amendBillingDocumentCondition: boolean): void {
    this.checkAmendBillingDocumentCondition = !amendBillingDocumentCondition;
  }
  setFieldAccessingByAmendReceiptInformation(amendReceiptInformation: boolean): FieldAccessing {
    this.checkAmendReceiptInformation = !amendReceiptInformation;
    return { filedIds: RECEIPT_INFORMATION, readonly: !amendReceiptInformation };
  }
  setFieldAccessingByAmendReceiptDocumentCondition(amendReceiptDocumentCondition: boolean): void {
    this.checkAmendReceiptDocumentCondition = !amendReceiptDocumentCondition;
  }
  setIsAmendBuyerInfo(model: CreditAppRequestLineAmendItemView): boolean {
    this.isAmendBuyerInfo = model.creditAppRequestTable_CreditAppRequestType === CreditAppRequestType.AmendBuyerInfo;
    return this.isAmendBuyerInfo;
  }
  onAmendRateChange(model: CreditAppRequestLineAmendItemView): Observable<FieldAccessing> {
    const message = model.amendRate ? messageCopy : messageClear;
    const fieldAccessing: FieldAccessing[] = [];
    return new Observable((observer) => {
      this.showConfirmDialog(message, labelAmendRate).subscribe((result) => {
        if (!result) {
          // not confirm
          model.amendRate = !model.amendRate;
        } else {
          // confirm
          this.updateCreditAppRequestLineByAmendRate(model);
          if (model.amendRate && this.activeIndex !== 0) {
            this.activeIndex = 0;
          }
        }
        this.checkAmendRate = !model.amendRate;
        return observer.next(this.setFieldAccessingByAmendRate(model.amendRate));
      });
    });
  }
  onAmendAssignmentMethodeChange(model: CreditAppRequestLineAmendItemView): Observable<FieldAccessing> {
    const message = model.amendAssignmentMethod ? messageCopy : messageClear;
    return new Observable((observer) => {
      this.showConfirmDialog(message, labelAmendAssignmentMethod).subscribe((result) => {
        if (!result) {
          // not confirm
          model.amendAssignmentMethod = !model.amendAssignmentMethod;
        } else {
          // confirm
          this.updateCreditAppRequestLineByAmendAssignmentMethod(model);
          this.activeIndex = 1;
        }
        this.checkAmendAssignmentMethod = !model.amendAssignmentMethod;
        return observer.next(this.setFieldAccessingByAmendAssignmentMethod(model.amendAssignmentMethod));
      });
    });
  }
  onAmendBillingInformationChange(model: CreditAppRequestLineAmendItemView): Observable<FieldAccessing> {
    const message = model.amendBillingInformation ? messageCopy : messageClear;
    return new Observable((observer) => {
      this.showConfirmDialog(message, labelAmendBillingInformation).subscribe((result) => {
        if (!result) {
          // not confirm
          model.amendBillingInformation = !model.amendBillingInformation;
        } else {
          // confirm
          this.updateCreditAppRequestLineByAmendBillingInformation(model);
          this.activeIndex = 2;
        }
        this.checkAmendBillingInformation = !model.amendBillingInformation;
        return observer.next(this.setFieldAccessingByAmendBillingInformation(model.amendBillingInformation));
      });
    });
  }
  onAmendBillingDocumentConditionChange(model: CreditAppRequestLineAmendItemView): void {
    const message = model.amendBillingDocumentCondition ? messageCopy : messageClear;
    this.showConfirmDialog(message, labelAmendBillingDocumentCondition).subscribe((result) => {
      if (!result) {
        // not confirm
        model.amendBillingDocumentCondition = !model.amendBillingDocumentCondition;
      } else {
        // confirm
        this.updateCreditAppRequestLineByAmendBillingDocumentCondition(model);
        this.activeIndex = 3;
      }
    });
  }
  onAmendReceiptInformationChange(model: CreditAppRequestLineAmendItemView): Observable<FieldAccessing> {
    const message = model.amendReceiptInformation ? messageCopy : messageClear;
    return new Observable((observer) => {
      this.showConfirmDialog(message, labelAmendReceiptInformation).subscribe((result) => {
        if (!result) {
          // not confirm
          model.amendReceiptInformation = !model.amendReceiptInformation;
        } else {
          // confirm
          this.updateCreditAppRequestLineByAmendReceiptInformation(model);
          this.activeIndex = 4;
        }
        this.checkAmendReceiptInformation = !model.amendReceiptInformation;
        return observer.next(this.setFieldAccessingByAmendReceiptInformation(model.amendReceiptInformation));
      });
    });
  }
  onAmendReceiptDocumentConditionChange(model: CreditAppRequestLineAmendItemView): void {
    const message = model.amendReceiptDocumentCondition ? messageCopy : messageClear;
    const fieldAccessing: FieldAccessing[] = [];
    this.showConfirmDialog(message, labelAmendReceiptDocumentCondition).subscribe((result) => {
      if (!result) {
        // not confirm
        model.amendReceiptDocumentCondition = !model.amendReceiptDocumentCondition;
      } else {
        // confirm
        this.updateCreditAppRequestLineByAmendReceiptDocumentCondition(model);
        this.activeIndex = 5;
      }
    });
  }
  showConfirmDialog(message: string, label: string): Observable<any> {
    const confirmation: Confirmation = {
      header: this.baseService.translate.instant('CONFIRM.CONFIRM'),
      message: this.baseService.translate.instant(message, [this.baseService.translate.instant(label)])
    };
    return new Observable((observer) => {
      this.baseService.notificationService.showConfirmDialog(confirmation);
      this.baseService.notificationService.isAccept.subscribe((isConfirm) => {
        this.amendCusInfoAction(label, isConfirm);
        this.baseService.notificationService.isAccept.observers = [];
        observer.next(isConfirm);
      });
    });
  }
  amendCusInfoAction(actionField: string, isConfirm: boolean): void {
    switch (actionField) {
      case labelAmendRate:
        break;
      default:
        break;
    }
  }
  updateCreditAppRequestLineByAmendRate(model: CreditAppRequestLineAmendItemView): void {
    (this.baseService.service as CreditAppRequestLineAmendService).updateCreditAppRequestLineByAmendRate(model).subscribe((result) => {
      model.creditAppRequestLine_NewMaxPurchasePct = result.maxPurchasePct;
      model.creditAppRequestLine_NewPurchaseFeePct = result.purchaseFeePct;
      model.creditAppRequestLine_NewPurchaseFeeCalculateBase = result.purchaseFeeCalculateBase;
    });
  }
  updateCreditAppRequestLineByAmendAssignmentMethod(model: CreditAppRequestLineAmendItemView): void {
    (this.baseService.service as CreditAppRequestLineAmendService).updateCreditAppRequestLineByAmendAssignmentMethod(model).subscribe((result) => {
      model.creditAppRequestLine_AssignmentMethodGUID = result.assignmentMethodGUID;
      model.creditAppRequestLine_NewAssignmentMethodRemark = result.assignmentMethodRemark;
      if (!model.amendAssignmentMethod && this.activeIndex === 1) {
        this.activeIndex = 0;
      }
    });
  }
  updateCreditAppRequestLineByAmendBillingInformation(model: CreditAppRequestLineAmendItemView): void {
    (this.baseService.service as CreditAppRequestLineAmendService).updateCreditAppRequestLineByAmendBillingInformation(model).subscribe((result) => {
      model.creditAppRequestLine_NewBillingAddressGUID = result.billingAddressGUID;
      if (!model.amendBillingInformation && this.activeIndex === 2) {
        this.activeIndex = 0;
      }
    });
  }
  updateCreditAppRequestLineByAmendReceiptInformation(model: CreditAppRequestLineAmendItemView): void {
    (this.baseService.service as CreditAppRequestLineAmendService).updateCreditAppRequestLineByAmendReceiptInformation(model).subscribe((result) => {
      model.creditAppRequestLine_NewMethodOfPaymentGUID = result.methodOfPaymentGUID;
      model.creditAppRequestLine_NewReceiptRemark = result.receiptRemark;
      model.creditAppRequestLine_NewReceiptAddressGUID = result.receiptAddressGUID;
      if (!model.amendReceiptInformation && this.activeIndex === 4) {
        this.activeIndex = 0;
      }
    });
  }
  updateCreditAppRequestLineByAmendBillingDocumentCondition(model: CreditAppRequestLineAmendItemView): void {
    (this.baseService.service as CreditAppRequestLineAmendService)
      .updateCreditAppRequestLineByAmendBillingDocumentCondition(model)
      .subscribe((result) => {
        if (!model.amendBillingDocumentCondition && this.activeIndex === 3) {
          this.activeIndex = 0;
        }
        this.checkAmendBillingDocumentCondition = !model.amendBillingDocumentCondition;
      });
  }
  updateCreditAppRequestLineByAmendReceiptDocumentCondition(model: CreditAppRequestLineAmendItemView): void {
    (this.baseService.service as CreditAppRequestLineAmendService)
      .updateCreditAppRequestLineByAmendReceiptDocumentCondition(model)
      .subscribe((result) => {
        if (!model.amendReceiptDocumentCondition && this.activeIndex === 5) {
          this.activeIndex = 0;
        }
        this.checkAmendReceiptDocumentCondition = !model.amendReceiptDocumentCondition;
      });
  }
  onAssignmentMethodChange(model: CreditAppRequestLineAmendItemView, assignmentMethodOptions: SelectItems[]): void {
    const assignmentMethod = !isNullOrUndefOrEmptyGUID(model.creditAppRequestLine_AssignmentMethodGUID)
      ? (assignmentMethodOptions.find((f) => f.value === model.creditAppRequestLine_AssignmentMethodGUID).rowData as AssignmentMethodItemView)
      : new AssignmentMethodItemView();
    model.creditAppRequestLine_NewAssignmentMethodRemark = assignmentMethod.description;
  }
  onMethodOfPaymentChange(model: CreditAppRequestLineAmendItemView, methodOfPaymentOptions: SelectItems[]): void {
    const methodOfPayment = !isNullOrUndefOrEmptyGUID(model.creditAppRequestLine_NewMethodOfPaymentGUID)
      ? (methodOfPaymentOptions.find((f) => f.value === model.creditAppRequestLine_NewMethodOfPaymentGUID).rowData as MethodOfPaymentItemView)
      : new MethodOfPaymentItemView();
    model.creditAppRequestLine_NewReceiptRemark = methodOfPayment.description;
  }
  onBillingAddressChange(model: CreditAppRequestLineAmendItemView, addressOption: SelectItems[]): void {
    model.unboundNewBillingAddress = this.getAddressValue(model.creditAppRequestLine_NewBillingAddressGUID, addressOption);
  }
  onReceiptAddressChange(model: CreditAppRequestLineAmendItemView, addressOption: SelectItems[]): void {
    model.unboundNewReceiptAddress = this.getAddressValue(model.creditAppRequestLine_NewReceiptAddressGUID, addressOption);
  }
  getAddressValue(value: string, addressOption: SelectItems[]): string {
    const address = !isNullOrUndefOrEmptyGUID(value)
      ? (addressOption.find((f) => f.value === value).rowData as AddressTransItemView)
      : new AddressTransItemView();
    return `${address.address1} ${address.address2}`;
  }
  getWorkFlowPath(model: CreditAppRequestLineAmendItemView): PathParamModel {
    const ACTION_WORKFLOW = 'actionworkflow';
    const START_WORKFLOW = 'startworkflow';

    const PATH = model.documentStatus_StatusId === CreditAppRequestStatus.Draft ? START_WORKFLOW : ACTION_WORKFLOW;
    return {
      path: PATH,
      parameters: {
        creditAppRequestTableGUID: model.creditAppRequestTable_CreditAppRequestTableGUID,
        [WORKFLOW.ACTIONHISTORY_REFGUID]: model.creditAppRequestTable_CreditAppRequestTableGUID,
        refCreditAppLineGUID: model.creditAppRequestLine_RefCreditAppLineGUID
      }
    };
  }
  getDisabledWorkFlow(model: CreditAppRequestLineAmendItemView): boolean {
    return !(
      model.documentStatus_StatusId >= CreditAppRequestStatus.Draft && model.documentStatus_StatusId <= CreditAppRequestStatus.WaitingForRetry
    );
  }
  setPrintSetBookDocTransData(model: CreditAppRequestLineAmendItemView): PrintSetBookDocTransView {
    const paramModel = new PrintSetBookDocTransView();
    toMapModel(model, paramModel);
    paramModel.refType = RefType.CreditAppRequestTable;
    paramModel.refGUID = model.creditAppRequestTable_CreditAppRequestTableGUID;
    paramModel.refType = RefType.CreditAppRequestTable;
    paramModel.creditAppRequestId = model.creditAppRequestTable_CreditAppRequestId;
    paramModel.creditAppRequestDescription = model.creditAppRequestTable_Description;
    paramModel.creditLimitTypeId = model.creditAppRequestTable_CustomerCreditLimit.toString();
    paramModel.creditAppRequestType = model.creditAppRequestTable_CreditAppRequestType;
    return paramModel;
  }
  isDraft(model: CreditAppRequestLineAmendItemView): boolean {
    return model.documentStatus_StatusId === CreditAppRequestStatus.Draft;
  }
  isReturn(): void {
    return;
  }
}
