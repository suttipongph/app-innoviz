import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditAppRequestLineAmendItemComponent } from './credit-app-request-line-amend-item.component';

describe('CreditAppRequestLineAmendItemViewComponent', () => {
  let component: CreditAppRequestLineAmendItemComponent;
  let fixture: ComponentFixture<CreditAppRequestLineAmendItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditAppRequestLineAmendItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppRequestLineAmendItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
