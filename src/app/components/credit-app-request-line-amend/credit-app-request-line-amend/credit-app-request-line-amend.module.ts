import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreditAppRequestLineAmendService } from './credit-app-request-line-amend.service';
import { CreditAppRequestLineAmendListComponent } from './credit-app-request-line-amend-list/credit-app-request-line-amend-list.component';
import { CreditAppRequestLineAmendItemComponent } from './credit-app-request-line-amend-item/credit-app-request-line-amend-item.component';
import { CreditAppRequestLineAmendItemCustomComponent } from './credit-app-request-line-amend-item/credit-app-request-line-amend-item-custom.component';
import { CreditAppRequestLineAmendListCustomComponent } from './credit-app-request-line-amend-list/credit-app-request-line-amend-list-custom.component';
import { DocumentConditionTransComponentModule } from 'components/document-condition-trans/document-condition-trans/document-condition-trans.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, DocumentConditionTransComponentModule],
  declarations: [CreditAppRequestLineAmendListComponent, CreditAppRequestLineAmendItemComponent],
  providers: [CreditAppRequestLineAmendService, CreditAppRequestLineAmendItemCustomComponent, CreditAppRequestLineAmendListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CreditAppRequestLineAmendListComponent, CreditAppRequestLineAmendItemComponent]
})
export class CreditAppRequestLineAmendComponentModule {}
