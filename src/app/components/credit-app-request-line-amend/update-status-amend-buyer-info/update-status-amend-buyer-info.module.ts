import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateStatusAmendBuyerInfoService } from './update-status-amend-buyer-info.service';
import { UpdateStatusAmendBuyerInfoComponent } from './update-status-amend-buyer-info/update-status-amend-buyer-info.component';
import { UpdateStatusAmendBuyerInfoCustomComponent } from './update-status-amend-buyer-info/update-status-amend-buyer-info-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [UpdateStatusAmendBuyerInfoComponent],
  providers: [UpdateStatusAmendBuyerInfoService, UpdateStatusAmendBuyerInfoCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [UpdateStatusAmendBuyerInfoComponent]
})
export class UpdateStatusAmendBuyerInfoComponentModule {}
