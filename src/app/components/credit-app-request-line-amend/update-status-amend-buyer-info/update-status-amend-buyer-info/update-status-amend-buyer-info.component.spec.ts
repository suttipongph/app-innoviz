import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateStatusAmendBuyerInfoComponent } from './update-status-amend-buyer-info.component';

describe('UpdateStatusAmendBuyerInfoViewComponent', () => {
  let component: UpdateStatusAmendBuyerInfoComponent;
  let fixture: ComponentFixture<UpdateStatusAmendBuyerInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UpdateStatusAmendBuyerInfoComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateStatusAmendBuyerInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
