import { Observable, of } from 'rxjs';
import { ApprovalDecision, EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { UpdateStatusAmendBuyerInfoView } from 'shared/models/viewModel';

const firstGroup = ['CREDIT_APP_REQUEST_ID', 'DESCRIPTION', 'CREDIT_APP_REQUEST_TYPE', 'CUSTOMER_ID'];

export class UpdateStatusAmendBuyerInfoCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: UpdateStatusAmendBuyerInfoView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(passingObject: UpdateStatusAmendBuyerInfoView): Observable<UpdateStatusAmendBuyerInfoView> {
    let model = new UpdateStatusAmendBuyerInfoView();
    model.updateStatusAmendBuyerInfoGUID = passingObject.updateStatusAmendBuyerInfoGUID;
    model.creditAppRequestId = passingObject.creditAppRequestId;
    model.description = passingObject.description;
    model.creditAppRequestType = passingObject.creditAppRequestType;
    model.customerId = passingObject.customerId;
    model.approvedDate = passingObject.approvedDate;
    model.approvalDecision = ApprovalDecision.None;
    model.creditAppRequestTableGUID = passingObject.creditAppRequestTableGUID;
    return of(model);
  }
}
