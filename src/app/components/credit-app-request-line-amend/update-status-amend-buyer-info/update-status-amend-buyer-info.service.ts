import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { UpdateStatusAmendBuyerInfoView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class UpdateStatusAmendBuyerInfoService {
  serviceKey = 'updateStatusAmendBuyerInfoGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getUpdateStatusAmendBuyerInfoToList(search: SearchParameter): Observable<SearchResult<UpdateStatusAmendBuyerInfoView>> {
    const url = `${this.servicePath}/GetUpdateStatusAmendBuyerInfoList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteUpdateStatusAmendBuyerInfo(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteUpdateStatusAmendBuyerInfo`;
    return this.dataGateway.delete(url, row);
  }
  getUpdateStatusAmendBuyerInfoById(id: string): Observable<UpdateStatusAmendBuyerInfoView> {
    const url = `${this.servicePath}/GetUpdateStatusAmendBuyerInfoById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createUpdateStatusAmendBuyerInfo(vmModel: UpdateStatusAmendBuyerInfoView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateUpdateStatusAmendBuyerInfo`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateStatusAmendBuyerInfo(vmModel: UpdateStatusAmendBuyerInfoView): Observable<ResponseModel> {
    const url = `${this.servicePath}`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
