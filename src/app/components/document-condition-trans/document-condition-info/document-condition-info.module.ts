import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DocumentConditionInfoService } from './document-condition-info.service';
import { DocumentConditionInfoItemComponent } from './document-condition-info-item/document-condition-info-item.component';
import { DocumentConditionInfoItemCustomComponent } from './document-condition-info-item/document-condition-info-item-custom.component';
import { DocumentConditionTransComponentModule } from '../document-condition-trans/document-condition-trans.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, DocumentConditionTransComponentModule],
  declarations: [DocumentConditionInfoItemComponent],
  providers: [DocumentConditionInfoService, DocumentConditionInfoItemCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [DocumentConditionInfoItemComponent]
})
export class DocumentConditionInfoComponentModule {}
