import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class DocumentConditionInfoService {
  serviceKey = 'documentConditionInfoGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getInitialData(companyId: string): Observable<any> {
    const url = `${this.servicePath}/GetDocumentConditionTransInitialData/id=${companyId}`;
    return this.dataGateway.get(url);
  }
}
