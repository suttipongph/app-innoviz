import { Observable, of } from 'rxjs';
import { FieldAccessing } from 'shared/models/systemModel';
import { DocumentConditionInfoItemView } from 'shared/models/viewModel';

const firstGroup = ['REF_TYPE', 'REF_ID'];

export class DocumentConditionInfoItemCustomComponent {
  getFieldAccessing(model: DocumentConditionInfoItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: DocumentConditionInfoItemView): Observable<boolean> {
    return of(!canCreate);
  }
}
