import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentConditionInfoItemComponent } from './document-condition-info-item.component';

describe('DocumentConditionInfoItemViewComponent', () => {
  let component: DocumentConditionInfoItemComponent;
  let fixture: ComponentFixture<DocumentConditionInfoItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DocumentConditionInfoItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentConditionInfoItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
