import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, of } from 'rxjs';
import { DocConVerifyType } from 'shared/constants';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { DocumentConditionInfoItemView, DocumentConditionTransItemView } from 'shared/models/viewModel';
import { DocumentConditionInfoService } from '../document-condition-info.service';
import { DocumentConditionInfoItemCustomComponent } from './document-condition-info-item-custom.component';
@Component({
  selector: 'document-condition-info-item',
  templateUrl: './document-condition-info-item.component.html',
  styleUrls: ['./document-condition-info-item.component.scss']
})
export class DocumentConditionInfoItemComponent extends BaseItemComponent<DocumentConditionInfoItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  refTypeOptions: SelectItems[] = [];
  DocConVerifyType = DocConVerifyType;
  constructor(
    private service: DocumentConditionInfoService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: DocumentConditionInfoItemCustomComponent,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialCreatingData();
    this.onEnumLoader();
    this.getGroups();
    this.setFieldAccessing();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<DocumentConditionInfoItemView> {
    return null;
  }
  setInitialUpdatingData(): void {}
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model.refType = result.refType;
      this.model.refId = result.refId;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }
  onAsyncRunner(model?: any): void {}

  setFieldAccessing(): void {
    this.isView = true;
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {}
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  getInitialData(): Observable<DocumentConditionTransItemView> {
    return this.service.getInitialData(this.parentId);
  }
}
