import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentConditionTransItemComponent } from './document-condition-trans-item.component';

describe('DocumentConditionTransItemViewComponent', () => {
  let component: DocumentConditionTransItemComponent;
  let fixture: ComponentFixture<DocumentConditionTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DocumentConditionTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentConditionTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
