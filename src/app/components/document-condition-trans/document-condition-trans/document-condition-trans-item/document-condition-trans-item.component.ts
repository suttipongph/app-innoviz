import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { DocumentConditionTransItemView } from 'shared/models/viewModel';
import { DocumentConditionTransService } from '../document-condition-trans.service';
import { DocumentConditionTransItemCustomComponent } from './document-condition-trans-item-custom.component';
@Component({
  selector: 'document-condition-trans-item',
  templateUrl: './document-condition-trans-item.component.html',
  styleUrls: ['./document-condition-trans-item.component.scss']
})
export class DocumentConditionTransItemComponent extends BaseItemComponent<DocumentConditionTransItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  docConVerifyTypeOptions: SelectItems[] = [];
  documentTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];

  constructor(
    private service: DocumentConditionTransService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: DocumentConditionTransItemCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.custom.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getDocConVerifyTypeEnumDropDown(this.docConVerifyTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<DocumentConditionTransItemView> {
    return this.service.getDocumentConditionTransById(this.id);
  }
  getInitialData(): Observable<DocumentConditionTransItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.custom.setDefaultValue(this.model, this.path);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
      this.custom.setDefaultValue(this.model, this.path);
    });
  }
  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getDocumentTypeDropDown()).subscribe(
      ([documentType]) => {
        this.documentTypeOptions = documentType;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model, this.isWorkFlowMode()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateDocumentConditionTrans(this.model), isColsing);
    } else {
      super.onCreate(this.service.createDocumentConditionTrans(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
