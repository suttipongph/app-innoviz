import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConst, CreditAppRequestStatus, DocConVerifyType, ROUTE_RELATED_GEN } from 'shared/constants';
import { isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { AccessModeView, DocumentConditionTransItemView } from 'shared/models/viewModel';
import { DocumentConditionTransService } from '../document-condition-trans.service';

const firstGroup = ['DOC_CON_VERIFY_TYPE', 'REF_TYPE', 'REF_ID', 'REF_DOCUMENT_CONDITION_TRANS_GUID'];
const inAciveOnly = ['INACTIVE'];
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const CREDIT_APP_REQUEST_LINE = 'creditapprequestline-child';
const BUYER_TABLE = 'buyertable';
const CREDIT_APP_LINE = 'creditappline-child';
const AMEND_CA_LINE = 'amendcaline';
const BUYER_MATCHING = 'buyermatching';
const LOAN_REQUEST = 'loanrequest';

export class DocumentConditionTransItemCustomComponent {
  title: string = null;
  baseService: BaseServiceModel<any>;
  parentName: string = null;
  parentId: string = null;
  activeName: string = null;
  addGetFieldAccessingByParent: boolean = false;
  accessModeView: AccessModeView = new AccessModeView();
  isBuyerMatching: boolean = false;
  isLoanRequest: boolean = false;
  originKey: string = null;

  getFieldAccessing(model: DocumentConditionTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    if (isUpdateMode(model.documentConditionTransGUID)) {
      if (this.addGetFieldAccessingByParent) {
        fieldAccessing.push(...this.getFieldAccessingByParent(model));
      }
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: DocumentConditionTransItemView, isWorkFlowMode: boolean): Observable<boolean> {
    return this.setAccessModeByParentStatus(model, isWorkFlowMode).pipe(
      map((result) => (isUpdateMode(model.documentConditionTransGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  setAccessModeByParentStatus(model: DocumentConditionTransItemView, isWorkFlowMode: boolean): Observable<AccessModeView> {
    this.originKey = this.baseService.uiService.getRelatedInfoOriginTableKey();
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        return this.getAccessModeByCreditAppRequestTable(this.originKey, isWorkFlowMode);
      case CREDIT_APP_REQUEST_LINE:
        this.checkIsBuyerMacthingOrLoanRequest();
        return this.getAccessModeByCreditAppRequestTable(this.originKey, isWorkFlowMode);
      case BUYER_TABLE:
        this.accessModeView.canCreate = true;
        this.accessModeView.canDelete = true;
        this.accessModeView.canView = true;
        return of(this.accessModeView);
      case CREDIT_APP_LINE:
        return this.getAccessModeByCreditAppLine(isWorkFlowMode);
      default:
        return of(this.accessModeView);
    }
  }
  setDefaultValue(model: DocumentConditionTransItemView, path): void {
    if (path === ROUTE_RELATED_GEN.BILLING_DOCUMENT_CONDITION_TRANS_BILLING) {
      this.title = 'LABEL.BILLING_DOCUMENT_CONDITION_TRANSACTIONS';
      model.docConVerifyType = DocConVerifyType.Billing;
    } else {
      this.title = 'LABEL.RECEIPT_DOCUMENT_CONDITION_TRANSACTIONS';
      model.docConVerifyType = DocConVerifyType.Receipt;
    }
  }
  getAccessModeByCreditAppRequestTable(parentId: string, isWorkFlowMode: boolean): Observable<AccessModeView> {
    return (this.baseService.service as DocumentConditionTransService).getAccessModeByCreditAppRequestTable(parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
        const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
        this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
        this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
        this.accessModeView.canView = isDraftStatus ? true : this.accessModeView.canView && isWorkFlowMode;
      })
    );
  }
  getAccessModeByCreditAppRequestLine(parentId: string, isWorkFlowMode: boolean): Observable<AccessModeView> {
    return (this.baseService.service as DocumentConditionTransService).getAccessModeByCreditAppRequestLine(parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
        const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
        this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
        this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
        this.accessModeView.canView = isDraftStatus ? true : this.accessModeView.canView && isWorkFlowMode;
      })
    );
  }
  getAccessModeByCreditAppLine(isWorkFlowMode: boolean): Observable<AccessModeView> {
    switch (this.activeName) {
      case AMEND_CA_LINE:
        return this.getAccessModeByCreditAppRequestLine(this.parentId, isWorkFlowMode);
      default:
        this.accessModeView.canCreate = false;
        this.accessModeView.canDelete = false;
        this.accessModeView.canView = false;
        return of(this.accessModeView);
    }
  }
  getRelatedInfoParentTableKey(): string {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    this.activeName = this.baseService.uiService.getRelatedInfoActiveTableName();
    switch (this.activeName) {
      case AMEND_CA_LINE:
        this.parentId = this.baseService.uiService.getRelatedInfoActiveTableKey();
        this.addGetFieldAccessingByParent = true;
        return this.parentId;
      default:
        this.parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
        return this.parentId;
    }
  }
  getFieldAccessingByParent(model: DocumentConditionTransItemView): FieldAccessing[] {
    const fieldAccessing: FieldAccessing[] = [];
    if (!isNullOrUndefOrEmptyGUID(model.refDocumentConditionTransGUID)) {
      fieldAccessing.push(
        ...[
          { filedIds: [AppConst.ALL_ID], readonly: true },
          { filedIds: inAciveOnly, readonly: false }
        ]
      );
    }
    return fieldAccessing;
  }
  checkIsBuyerMacthingOrLoanRequest(): void {
    this.isBuyerMatching = window.location.href.match(BUYER_MATCHING) ? true : false;
    this.isLoanRequest = window.location.href.match(LOAN_REQUEST) ? true : false;
    if (this.isBuyerMatching) {
      this.originKey = this.baseService.uiService.getKey(BUYER_MATCHING);
    } else if (this.isLoanRequest) {
      this.originKey = this.baseService.uiService.getKey(LOAN_REQUEST);
    }
  }
}
