import { Component, Input } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, DocConVerifyType, ROUTE_FUNCTION_GEN, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { DocumentConditionTransListView } from 'shared/models/viewModel';
import { DocumentConditionTransService } from '../document-condition-trans.service';
import { DocumentConditionTransListCustomComponent } from './document-condition-trans-list-custom.component';

@Component({
  selector: 'document-condition-trans-list',
  templateUrl: './document-condition-trans-list.component.html',
  styleUrls: ['./document-condition-trans-list.component.scss']
})
export class DocumentConditionTransListComponent extends BaseListComponent<DocumentConditionTransListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  docConVerifyTypeOptions: SelectItems[] = [];
  documentTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  defualtBitOption: SelectItems[] = [];
  title: string = null;
  verifyType: DocConVerifyType;
  element_id: string = 'DOCUMENT_CONDITION_TRANS';
  constructor(
    public custom: DocumentConditionTransListCustomComponent,
    private service: DocumentConditionTransService,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {
    if (this.path === ROUTE_RELATED_GEN.BILLING_DOCUMENT_CONDITION_TRANS_BILLING) {
      this.title = 'LABEL.BILLING_DOCUMENT_CONDITION_TRANSACTIONS';
      this.verifyType = DocConVerifyType.Billing;
      this.accessRightChildPage = 'BillingDocumentConditionTransListPage';
      this.redirectPath = 'billingdocumentconditiontrans-child';
      this.element_id = 'BILLING_DOCUMENT_CONDITION_TRANS';
    } else {
      this.title = 'LABEL.RECEIPT_DOCUMENT_CONDITION_TRANSACTIONS';
      this.verifyType = DocConVerifyType.Receipt;
      this.accessRightChildPage = 'ReceiptDocumentConditionTransListPage';
      this.redirectPath = 'receiptdocumentconditiontrans-child';
      this.element_id = 'RECEIPT_DOCUMENT_CONDITION_TRANS';
    }
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.setInitialListData(this.isWorkFlowMode()).subscribe((result) => {
      this.model = result;
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getDocConVerifyTypeEnumDropDown(this.docConVerifyTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.baseDropdown.getDefaultBitDropDown(this.defualtBitOption);
    this.setFunctionOptions();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getDocumentTypeDropDown(this.documentTypeOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'documentConditionTransGUID';
    const columns: ColumnModel[] = [
      {
        label: this.translate.instant('LABEL.DOCUMENT_TYPE_ID'),
        textKey: 'documentType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.ASC,
        masterList: this.documentTypeOptions,
        sortingKey: 'documentType_DocumentTypeId',
        searchingKey: 'documentTypeGUID'
      },
      {
        label: this.translate.instant('LABEL.MANDATORY'),
        textKey: 'mandatory',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defualtBitOption
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.model.refGUID
      },
      {
        label: null,
        textKey: 'docConVerifyType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.docConVerifyTypeOptions,
        parentKey: this.verifyType.toString()
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<DocumentConditionTransListView>> {
    return this.service.getDocumentConditionTransToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteDocumentConditionTrans(row));
  }
  setFunctionOptions(): void {
    if (this.path === ROUTE_RELATED_GEN.BILLING_DOCUMENT_CONDITION_TRANS_BILLING) {
      this.functionItems = [
        {
          label: 'LABEL.COPY_DOCUMENT_CONDITION_TEMPLATE',
          command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.COPY_DOCUMENT_CONDITION_TEMPLATE_BILLING })
        }
      ];
    } else if (this.path === ROUTE_RELATED_GEN.RECEIPT_DOCUMENT_CONDITION_TRANS_RECEIPT) {
      this.functionItems = [
        {
          label: 'LABEL.COPY_DOCUMENT_CONDITION_TEMPLATE',
          command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.COPY_DOCUMENT_CONDITION_TEMPLATE_RECEIPT })
        }
      ];
    }
    super.setFunctionOptionsMapping();
  }
  getRowAuthorize(row: DocumentConditionTransListView[]): void {
    if (this.custom.getRowAuthorize(row)) {
      row.forEach((item) => {
        item.rowAuthorize = this.getSingleRowAuthorize(item.rowAuthorize, item);
      });
    } else {
      super.getRowAuthorize(row);
    }
  }
}
