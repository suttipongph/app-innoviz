import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AccessMode, CreditAppRequestStatus, ROUTE_FUNCTION_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { MenuItem } from 'shared/models/primeModel';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView, DocumentConditionTransListView } from 'shared/models/viewModel';
import { DocumentConditionTransService } from '../document-condition-trans.service';

const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const CREDIT_APP_REQUEST_LINE = 'creditapprequestline-child';
const BUYER_TABLE = 'buyertable';
const CREDIT_APP_LINE = 'creditappline-child';
const AMEND_CA_LINE = 'amendcaline';
const BUYER_MATCHING = 'buyermatching';
const LOAN_REQUEST = 'loanrequest';

export class DocumentConditionTransListCustomComponent {
  baseService: BaseServiceModel<any>;
  parentName: string = null;
  parentId: string = null;
  activeName: string = null;
  activeKey: string = null;
  originKey: string = null;

  model: DocumentConditionTransListView;
  hasFunction = false;
  isBuyerMatching: boolean = false;
  isLoanRequest: boolean = false;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setInitialListData(isWorkFlowMode: boolean): Observable<DocumentConditionTransListView> {
    this.model = new DocumentConditionTransListView();
    this.model.accessModeView = new AccessModeView();
    this.parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    this.activeName = this.baseService.uiService.getRelatedInfoActiveTableName();
    this.activeKey = this.baseService.uiService.getRelatedInfoActiveTableKey();
    this.originKey = this.baseService.uiService.getRelatedInfoOriginTableKey();
    this.model.refGUID = this.parentId;
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        return this.getAccessModeByCreditAppRequestTable(this.originKey, isWorkFlowMode);
      case CREDIT_APP_REQUEST_LINE:
        this.checkIsBuyerMacthingOrLoanRequest();
        return this.getAccessModeByCreditAppRequestTable(this.originKey, isWorkFlowMode);
      case BUYER_TABLE:
        this.model.accessModeView.canCreate = true;
        this.model.accessModeView.canDelete = true;
        this.model.accessModeView.canView = true;
        this.hasFunction = true;
        return of(this.model);
      case CREDIT_APP_LINE:
        return this.getAccessModeByCreditAppLine(this.activeKey, isWorkFlowMode);
      default:
        return of(this.model);
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.model.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.model.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.model.accessModeView.canDelete;
  }
  getAccessModeByCreditAppRequestTable(parentId: string, isWorkFlowMode: boolean): Observable<DocumentConditionTransListView> {
    return new Observable((observable) => {
      (this.baseService.service as DocumentConditionTransService).getAccessModeByCreditAppRequestTable(parentId).subscribe((result) => {
        this.model.accessModeView = result as AccessModeView;
        const isDraftStatus = this.model.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
        this.model.accessModeView.canCreate = isDraftStatus ? true : this.model.accessModeView.canCreate && isWorkFlowMode;
        this.model.accessModeView.canDelete = isDraftStatus ? true : this.model.accessModeView.canDelete && isWorkFlowMode;
        this.model.accessModeView.canView = true;
        observable.next(this.model);
      });
    });
  }
  getDocumentConditionTransInitialListData(amendCaLineId: string, isWorkFlowMode: boolean): Observable<DocumentConditionTransListView> {
    return new Observable((observable) => {
      (this.baseService.service as DocumentConditionTransService).getDocumentConditionTransInitialListData(amendCaLineId).subscribe((result) => {
        this.model.accessModeView = result.accessModeView;
        this.model.refGUID = result.refGUID;
        const isDraftStatus = this.model.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
        this.model.accessModeView.canCreate = isDraftStatus ? true : this.model.accessModeView.canCreate && isWorkFlowMode;
        this.model.accessModeView.canDelete = isDraftStatus ? true : this.model.accessModeView.canDelete && isWorkFlowMode;
        this.model.accessModeView.canView = true;
        observable.next(this.model);
      });
    });
  }
  getAccessModeByCreditAppLine(amendCaLineId: string, isWorkFlowMode: boolean): Observable<DocumentConditionTransListView> {
    switch (this.activeName) {
      case AMEND_CA_LINE:
        return this.getDocumentConditionTransInitialListData(amendCaLineId, isWorkFlowMode);
      default:
        this.model.accessModeView.canCreate = false;
        this.model.accessModeView.canDelete = false;
        this.model.accessModeView.canView = true;
        return of(this.model);
    }
  }
  getRowAuthorize(row: DocumentConditionTransListView[]): boolean {
    let isSetRow: boolean = true;
    if (this.activeName === AMEND_CA_LINE) {
      row.forEach((item) => {
        const accessMode: AccessMode = isNullOrUndefOrEmptyGUID(item.refDocumentConditionTransGUID) ? AccessMode.full : AccessMode.creator;
        item.rowAuthorize = accessMode;
      });
    } else {
      isSetRow = false;
    }
    return isSetRow;
  }
  checkIsBuyerMacthingOrLoanRequest(): void {
    this.isBuyerMatching = window.location.href.match(BUYER_MATCHING) ? true : false;
    this.isLoanRequest = window.location.href.match(LOAN_REQUEST) ? true : false;
    if (this.isBuyerMatching) {
      this.originKey = this.baseService.uiService.getKey(BUYER_MATCHING);
    } else if (this.isLoanRequest) {
      this.originKey = this.baseService.uiService.getKey(LOAN_REQUEST);
    }
  }
}
