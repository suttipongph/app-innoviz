import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DocumentConditionTransListComponent } from './document-condition-trans-list.component';

describe('DocumentConditionTransListViewComponent', () => {
  let component: DocumentConditionTransListComponent;
  let fixture: ComponentFixture<DocumentConditionTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DocumentConditionTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentConditionTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
