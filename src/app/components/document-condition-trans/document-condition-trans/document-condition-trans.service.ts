import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { AccessModeView, DocumentConditionTransItemView, DocumentConditionTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class DocumentConditionTransService {
  serviceKey = 'documentConditionTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getDocumentConditionTransToList(search: SearchParameter): Observable<SearchResult<DocumentConditionTransListView>> {
    const url = `${this.servicePath}/GetDocumentConditionTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteDocumentConditionTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteDocumentConditionTrans`;
    return this.dataGateway.delete(url, row);
  }
  getDocumentConditionTransById(id: string): Observable<DocumentConditionTransItemView> {
    const url = `${this.servicePath}/GetDocumentConditionTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createDocumentConditionTrans(vmModel: DocumentConditionTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateDocumentConditionTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateDocumentConditionTrans(vmModel: DocumentConditionTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateDocumentConditionTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(companyId: string): Observable<any> {
    const url = `${this.servicePath}/GetDocumentConditionTransInitialData/id=${companyId}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByCreditAppRequestTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppRequestTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getDocumentConditionTransInitialListData(id: string): Observable<DocumentConditionTransListView> {
    const url = `${this.servicePath}/GetDocumentConditionTransInitialListData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByCreditAppRequestLine(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppRequestLine/id=${id}`;
    return this.dataGateway.get(url);
  }
}
