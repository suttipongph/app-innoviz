import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DocumentConditionTransService } from './document-condition-trans.service';
import { DocumentConditionTransListComponent } from './document-condition-trans-list/document-condition-trans-list.component';
import { DocumentConditionTransItemComponent } from './document-condition-trans-item/document-condition-trans-item.component';
import { DocumentConditionTransItemCustomComponent } from './document-condition-trans-item/document-condition-trans-item-custom.component';
import { DocumentConditionTransListCustomComponent } from './document-condition-trans-list/document-condition-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [DocumentConditionTransListComponent, DocumentConditionTransItemComponent],
  providers: [DocumentConditionTransService, DocumentConditionTransItemCustomComponent, DocumentConditionTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [DocumentConditionTransListComponent, DocumentConditionTransItemComponent]
})
export class DocumentConditionTransComponentModule {}
