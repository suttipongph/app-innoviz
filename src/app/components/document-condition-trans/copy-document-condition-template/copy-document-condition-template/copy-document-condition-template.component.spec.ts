import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopyDocumentConditionTemplateComponent } from './copy-document-condition-template.component';

describe('CopyDocumentConditionTemplateViewComponent', () => {
  let component: CopyDocumentConditionTemplateComponent;
  let fixture: ComponentFixture<CopyDocumentConditionTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CopyDocumentConditionTemplateComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyDocumentConditionTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
