import { Observable, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { DocConVerifyType, EmptyGuid, RefType, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { BaseServiceModel, DocConVerifyTypeModel, FieldAccessing, RefTypeModel, TranslateModel } from 'shared/models/systemModel';
import { CopyDocumentConditionTemplateParamView, CopyDocumentConditionTemplateResultView } from 'shared/models/viewModel';
import { CopyDocumentConditionTemplateService } from '../copy-document-condition-template.service';

const firstGroup = [];

export class CopyDocumentConditionTemplateCustomComponent {
  baseService: BaseServiceModel<CopyDocumentConditionTemplateService>;
  getFieldAccessing(): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(id: string): Observable<CopyDocumentConditionTemplateResultView> {
    let model = new CopyDocumentConditionTemplateResultView();
    model.RefGUID = id;
    return of(model);
  }
  getModelParam(model: CopyDocumentConditionTemplateResultView, path: string): Observable<CopyDocumentConditionTemplateParamView> {
    const param: CopyDocumentConditionTemplateParamView = new CopyDocumentConditionTemplateParamView();
    param.documentConditionTemplateTableGUID = model.documentConditionTemplateTableGUID;
    if (path === ROUTE_FUNCTION_GEN.COPY_DOCUMENT_CONDITION_TEMPLATE_BILLING) {
      param.DocConVerifyType = DocConVerifyType.Billing;
      param.DocConVerifyTypeLabel = 'ENUM.BILLING';
      (param.RefType = RefType.Buyer), (param.CallerLabel = 'LABEL.BUYER');
    } else if (path === ROUTE_FUNCTION_GEN.COPY_DOCUMENT_CONDITION_TEMPLATE_RECEIPT) {
      param.DocConVerifyType = DocConVerifyType.Receipt;
      param.DocConVerifyTypeLabel = 'ENUM.RECEIPT';
      (param.RefType = RefType.Buyer), (param.CallerLabel = 'LABEL.BUYER');
    }
    param.RefGUID = model.RefGUID;
    return of(param);
  }
  getValidateIfExist(model: CopyDocumentConditionTemplateResultView, path: string): Observable<boolean> {
    return this.baseService.service.getDocumentConditionTransValidation(model).pipe(
      switchMap((result) => {
        if (result) {
          return of(result);
        } else {
          const translate: TranslateModel = {
            code: 'CONFIRM.90002',
            parameters: ['LABEL.DOCUMENT_CONDITION_TRANSACTION']
          };
          this.baseService.notificationService.showDeletionDialogManual(translate);
          return this.baseService.notificationService.isAccept.asObservable();
        }
      })
    );
  }
}
