import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { CopyDocumentConditionTemplateParamView, CopyDocumentConditionTemplateResultView } from 'shared/models/viewModel';
import { CopyDocumentConditionTemplateService } from '../copy-document-condition-template.service';
import { CopyDocumentConditionTemplateCustomComponent } from './copy-document-condition-template-custom.component';
@Component({
  selector: 'copy-document-condition-template',
  templateUrl: './copy-document-condition-template.component.html',
  styleUrls: ['./copy-document-condition-template.component.scss']
})
export class CopyDocumentConditionTemplateComponent extends BaseItemComponent<CopyDocumentConditionTemplateResultView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  documentConditionTemplateTableOptions: SelectItems[] = [];

  constructor(
    private service: CopyDocumentConditionTemplateService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CopyDocumentConditionTemplateCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialUpdatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<CopyDocumentConditionTemplateResultView> {
    return this.custom.getInitialData(this.id);
  }
  getInitialData(): Observable<CopyDocumentConditionTemplateResultView> {
    return this.custom.getInitialData(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getDocumentConditionTemplateTableDropDown()).subscribe(
      ([documentConditionTemplateTable]) => {
        this.documentConditionTemplateTableOptions = documentConditionTemplateTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing().subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {}
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.custom.getValidateIfExist(this.model, this.path).subscribe((res2) => {
          if (res2) {
            this.custom.getModelParam(this.model, this.path).subscribe((model) => {
              this.onSubmit(true, model);
            });
          }
        });
      }
    });
  }
  onSubmit(isColsing: boolean, model: CopyDocumentConditionTemplateParamView): void {
    super.onExecuteFunction(this.service.CopyDocumentConditionTemplate(model));
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
