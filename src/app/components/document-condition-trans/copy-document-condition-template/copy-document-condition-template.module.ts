import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CopyDocumentConditionTemplateService } from './copy-document-condition-template.service';
import { CopyDocumentConditionTemplateComponent } from './copy-document-condition-template/copy-document-condition-template.component';
import { CopyDocumentConditionTemplateCustomComponent } from './copy-document-condition-template/copy-document-condition-template-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CopyDocumentConditionTemplateComponent],
  providers: [CopyDocumentConditionTemplateService, CopyDocumentConditionTemplateCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CopyDocumentConditionTemplateComponent]
})
export class CopyDocumentConditionTemplateComponentModule {}
