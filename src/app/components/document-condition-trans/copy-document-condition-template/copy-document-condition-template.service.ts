import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CopyDocumentConditionTemplateParamView, CopyDocumentConditionTemplateResultView } from 'shared/models/viewModel';
import {
  DocConVerifyTypeModel,
  PageInformationModel,
  RefTypeModel,
  ResponseModel,
  RowIdentity,
  SearchParameter,
  SearchResult
} from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CopyDocumentConditionTemplateService {
  serviceKey = 'copyDocumentConditionTemplateGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getDocumentConditionTransValidation(vmModel: CopyDocumentConditionTemplateResultView): Observable<boolean> {
    const url = `${this.servicePath}/GetDocumentConditonTransValidation`;
    return this.dataGateway.post(url, vmModel);
  }
  CopyDocumentConditionTemplate(vmModel: CopyDocumentConditionTemplateParamView): Observable<CopyDocumentConditionTemplateResultView> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vmModel);
  }
}
