import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RetentionTransListComponent } from './retention-trans-list.component';

describe('RetentionTransListViewComponent', () => {
  let component: RetentionTransListComponent;
  let fixture: ComponentFixture<RetentionTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RetentionTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetentionTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
