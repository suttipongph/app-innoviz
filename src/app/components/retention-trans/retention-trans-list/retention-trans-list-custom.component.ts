import { Observable, of } from 'rxjs';
import { BaseServiceModel, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { AccessModeView, RetentionTransListView } from 'shared/models/viewModel';
import { RetentionTransService } from '../retention-trans.service';
const RETENTION_TRANS = 'retentiontransaction';
const CUSTOMER_TABLE = 'customertable';
export class RetentionTransListCustomComponent {
  baseService: BaseServiceModel<any>;
  parentId: string;
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  activeName: string;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  getListCustom(Search: SearchParameter, service: RetentionTransService): Observable<SearchResult<RetentionTransListView>> {
    this.activeName = this.baseService.uiService.getRelatedInfoActiveTableName();
    switch (this.parentName) {
      case RETENTION_TRANS:
        return service.getRetentionTransToList(Search);
      case CUSTOMER_TABLE:
        return service.getRetentionTransToListByCustomer(Search);
      default:
        return service.getRetentionTransToList(Search);
    }
  }
  setAccessModeByParentStatus(parentName: string): Observable<AccessModeView> {
    switch (this.parentName) {
      case CUSTOMER_TABLE:
        this.accessModeView.canCreate = false;
        this.accessModeView.canDelete = false;
        this.accessModeView.canView = false;
        return of(this.accessModeView);
      default:
        return of(this.accessModeView);
    }
  }
  getCanView(canView: boolean): boolean {
    switch (this.parentName) {
      case CUSTOMER_TABLE:
        return true;
      default:
        return canView;
    }
    return;
  }
}
