import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { AccessMode, ColumnType, CUSTOMER_TABLE, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { RetentionTransListView } from 'shared/models/viewModel';
import { RetentionTransService } from '../retention-trans.service';
import { RetentionTransListCustomComponent } from './retention-trans-list-custom.component';

@Component({
  selector: 'retention-trans-list',
  templateUrl: './retention-trans-list.component.html',
  styleUrls: ['./retention-trans-list.component.scss']
})
export class RetentionTransListComponent extends BaseListComponent<RetentionTransListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  buyerTableOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  creditAppTableOptions: SelectItems[] = [];
  buyerAgreementOptions: SelectItems[] = [];

  constructor(public custom: RetentionTransListCustomComponent, private service: RetentionTransService) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
    this.baseDropdown.getCreditAppTableDropDown(this.creditAppTableOptions);
    this.baseDropdown.getBuyerAgreementTableDropDown(this.buyerAgreementOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.custom.getCanView(this.getCanView());
    this.option.canDelete = false;
    this.option.key = 'retentionTransGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.CREDIT_APPLICATION_ID',
        textKey: 'creditAppTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'creditAppTableGUID',
        masterList: this.creditAppTableOptions,
        sortingKey: 'creditAppTable_CreditAppId'
      },
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions,
        sortingKey: 'productType',
        searchingKey: 'productType'
      },
      {
        label: 'LABEL.TRANSACTION_DATE',
        textKey: 'transDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.ASC,
        sortingKey: 'transDate',
        searchingKey: 'transDate'
      },
      {
        label: 'LABEL.AMOUNT',
        textKey: 'amount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        sortingKey: 'amount',
        searchingKey: 'amount'
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'buyerTableGUID',
        masterList: this.buyerTableOptions,
        sortingKey: 'buyerTable_BuyerId'
      },
      {
        label: 'LABEL.BUYER_AGREEMENT_ID',
        textKey: 'buyerAgreementTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerAgreementOptions,
        sortingKey: 'buyerAgreementTable_ReferenceAgreementID',
        searchingKey: 'buyerAgreementTableGUID'
      },
      {
        label: 'LABEL.REFERENCE_TYPE',
        textKey: 'refType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.refTypeOptions,
        sortingKey: 'refType',
        searchingKey: 'refType'
      },
      {
        label: 'LABEL.DOCUMENT_ID',
        textKey: 'documentId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE,
        sortingKey: 'documentId',
        searchingKey: 'documentId'
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }

  getList(search: SearchParameter): Observable<SearchResult<RetentionTransListView>> {
    return this.custom.getListCustom(search, this.service);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteRetentionTrans(row));
  }
  getRowAuthorize(row: RetentionTransListView[]): void {
    if (this.custom.parentName === 'customertable') {
      row.forEach((item) => {
        let accessMode: AccessMode = AccessMode.editor;
        item.rowAuthorize = this.getSingleRowAuthorize(accessMode, item);
      });
    } else {
      super.getRowAuthorize(row);
    }
  }
}
