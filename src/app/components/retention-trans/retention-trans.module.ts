import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RetentionTransService } from './retention-trans.service';
import { RetentionTransListComponent } from './retention-trans-list/retention-trans-list.component';
import { RetentionTransItemComponent } from './retention-trans-item/retention-trans-item.component';
import { RetentionTransItemCustomComponent } from './retention-trans-item/retention-trans-item-custom.component';
import { RetentionTransListCustomComponent } from './retention-trans-list/retention-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [RetentionTransListComponent, RetentionTransItemComponent],
  providers: [RetentionTransService, RetentionTransItemCustomComponent, RetentionTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [RetentionTransListComponent, RetentionTransItemComponent]
})
export class RetentionTransComponentModule {}
