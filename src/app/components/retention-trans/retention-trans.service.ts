import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { RetentionTransItemView, RetentionTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class RetentionTransService {
  serviceKey = 'retentionTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getRetentionTransToList(search: SearchParameter): Observable<SearchResult<RetentionTransListView>> {
    const url = `${this.servicePath}/GetRetentionTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteRetentionTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteRetentionTrans`;
    return this.dataGateway.delete(url, row);
  }
  getRetentionTransById(id: string): Observable<RetentionTransItemView> {
    const url = `${this.servicePath}/GetRetentionTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createRetentionTrans(vmModel: RetentionTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateRetentionTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateRetentionTrans(vmModel: RetentionTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateRetentionTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getRetentionTransToListByCustomer(search: SearchParameter): Observable<SearchResult<RetentionTransListView>> {
    const url = `${this.servicePath}/GetRetentionTransListByCustomer/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
}
