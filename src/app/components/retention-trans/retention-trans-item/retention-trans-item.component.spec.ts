import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetentionTransItemComponent } from './retention-trans-item.component';

describe('RetentionTransItemViewComponent', () => {
  let component: RetentionTransItemComponent;
  let fixture: ComponentFixture<RetentionTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RetentionTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetentionTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
