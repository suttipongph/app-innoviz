import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { RetentionTransItemView } from 'shared/models/viewModel';

const firstGroup = [
  'CUSTOMER_TABLE_GUID',
  'CREDIT_APP_TABLE_GUID',
  'PRODUCT_TYPE',
  'TRANS_DATE',
  'AMOUNT',
  'BUYER_TABLE_GUID',
  'BUYER_AGREEMENT_TABLE_GUID',
  'REF_TYPE',
  'REF_ID',
  'DOCUMENT_ID',
  'REF_GUID',
  'RETENTION_TRANS_GUID'
];

export class RetentionTransItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: RetentionTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: RetentionTransItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(): Observable<RetentionTransItemView> {
    let model = new RetentionTransItemView();
    model.retentionTransGUID = EmptyGuid;
    return of(model);
  }
}
