import { TranslateService } from '@ngx-translate/core';
import { AppInjector } from 'app-injector';
import { TreeModule } from 'primeng/tree';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { EmptyGuid, PaidToType } from 'shared/constants';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { SelectItem } from 'shared/models/primeModel';
import { BaseServiceModel, FieldAccessing, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { AccessModeView, CustomerTableItemView, PaymentDetailItemView } from 'shared/models/viewModel';
import { PaymentDetailService } from '../payment-detail.service';
const PURCHASE_TABLE = 'purchasetablepurchase';
const WITHDRAWAL_TABLE_WITHDRAWAL = 'withdrawaltablewithdrawal';
const WITHDRAWAL_TABLE_TERM_EXTENSION = 'withdrawaltabletermextension';
const RESERVE_REFUND = 'reserverefund';
const RETENTION_REFUND = 'retentionrefund';
const SUSPENSE_REFUND = 'suspenserefund';

const firstGroup = [
  'INVOICE_TYPE_GUID',
  'SUSPENSE_TRANSFER',
  'CUSTOMER_TABLE_GUID',
  'VENDOR_TABLE_GUID',
  'REF_TYPE',
  'REF_ID',
  'TOTAL_PAYMENT_AMOUNT',
  'REF_GUID',
  'PAYMENT_DETAIL_GUID'
];
const suspenseGroup = ['INVOICE_TYPE_GUID', 'SUSPENSE_TRANSFER'];
const vendorGroup = ['VENDOR_TABLE_GUID'];
const suspenseTransferGroup = ['CUSTOMER_TABLE_GUID'];

export class PaymentDetailItemCustomComponent {
  baseService: BaseServiceModel<PaymentDetailService>;
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  refId: string = null;
  totalPaymentAmount: number = 0;
  customerTableGUID: string = null;
  isRequiredCon1: boolean = false;
  isRequiredCon2: boolean = false;
  translateService = AppInjector.get(TranslateService);
  getFieldAccessing(model: PaymentDetailItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    switch (model.paidToType) {
      case PaidToType.Customer:
        fieldAccessing.push({ filedIds: suspenseGroup, readonly: true });
        fieldAccessing.push({ filedIds: vendorGroup, readonly: true });
        this.isRequiredCon1 = true;
        this.isRequiredCon2 = false;
        break;
      case PaidToType.Vendor:
        fieldAccessing.push({ filedIds: suspenseGroup, readonly: true });
        fieldAccessing.push({ filedIds: vendorGroup, readonly: false });
        this.isRequiredCon1 = true;
        this.isRequiredCon2 = false;
        break;
      case PaidToType.Suspense:
        fieldAccessing.push({ filedIds: suspenseGroup, readonly: false });
        fieldAccessing.push({ filedIds: vendorGroup, readonly: true });
        this.isRequiredCon1 = false;
        this.isRequiredCon2 = true;
        break;
      default:
        fieldAccessing.push({ filedIds: suspenseGroup, readonly: true });
        fieldAccessing.push({ filedIds: vendorGroup, readonly: true });
        this.isRequiredCon1 = false;
        this.isRequiredCon2 = false;
        break;
    }
    if (model.suspenseTransfer) {
      fieldAccessing.push({ filedIds: suspenseTransferGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(model: PaymentDetailItemView): Observable<boolean> {
    if (model.paymentAmount == 0) {
      const topic: TranslateModel = { code: 'ERROR.ERROR' };
      const contents: TranslateModel[] = [{ code: 'ERROR.90160', parameters: ['LABEL.PAYMENT_AMOUNT'] }];
      this.baseService.notificationService.showErrorMessageFromManual(topic, contents);
      return of(false);
    }
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: PaymentDetailItemView, isWorkflowMode: boolean): Observable<boolean> {
    const accessright = isNullOrUndefOrEmptyGUID(model.paymentDetailGUID) ? canCreate : canUpdate;
    switch (this.parentName) {
      case PURCHASE_TABLE:
        return this.baseService.service.getPurchaseTableAccessMode(model.refGUID, isWorkflowMode).pipe(
          map((result) => {
            return !(result.canView && accessright);
          })
        );
      case WITHDRAWAL_TABLE_WITHDRAWAL:
      case WITHDRAWAL_TABLE_TERM_EXTENSION:
        return this.baseService.service.getWithdrawalTableAccessMode(model.refGUID).pipe(
          map((result) => {
            return !(result.canView && accessright);
          })
        );
      case SUSPENSE_REFUND:
      case RESERVE_REFUND:
      case RETENTION_REFUND:
        return this.baseService.service.getCustomerRefundTableAccessMode(model.refGUID).pipe(
          map((result) => {
            return !(result.canView && accessright);
          })
        );
      default:
        return of(
          !isUpdateMode(model.paymentDetailGUID) ? !(this.accessModeView.canCreate && canCreate) : !(this.accessModeView.canView && canUpdate)
        );
    }
  }
  setPassParameter(passingObj: PaymentDetailItemView) {
    if (!isNullOrUndefined(passingObj)) {
      this.refId = passingObj.refId;
      this.totalPaymentAmount = passingObj.totalPaymentAmount;
      this.accessModeView = passingObj.accessModeView;
      this.customerTableGUID = passingObj.customerTableGUID;
    }
  }
  setModelByParameter(model: PaymentDetailItemView): void {
    model.refId = this.refId;
    model.totalPaymentAmount = this.totalPaymentAmount;
    if (!isUpdateMode(model.paymentDetailGUID)) {
      model.paidToType = null;
    }
  }
  setDefaultValue(model: PaymentDetailItemView, customerTableOptions: SelectItems[]) {
    switch (model.paidToType) {
      case PaidToType.Customer:
        var customerTable = customerTableOptions.find((f) => f.value === this.customerTableGUID).rowData as CustomerTableItemView;
        model.customerTableGUID = this.customerTableGUID;
        model.vendorTableGUID = customerTable.vendorTableGUID;
        break;
      case PaidToType.Suspense:
        model.customerTableGUID = this.customerTableGUID;
        break;
      default:
        model.customerTableGUID = null;
        model.vendorTableGUID = null;
        break;
    }
    model.suspenseTransfer = false;
    model.invoiceTypeGUID = null;
  }
  onSuspenseTransferChange(model: PaymentDetailItemView) {
    if (model.suspenseTransfer) {
      model.customerTableGUID = null;
    } else {
      model.customerTableGUID = this.customerTableGUID;
    }
  }
  onValidateVendor(model: PaymentDetailItemView): TranslateModel {
    if (model.paidToType == PaidToType.Customer && isNullOrUndefined(model.vendorTableGUID)) {
      return {
        code: 'ERROR.MANDATORY_FIELD',
        parameters: []
      };
    } else {
      return null;
    }
  }
}
