import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentDetailItemComponent } from './payment-detail-item.component';

describe('PaymentDetailItemViewComponent', () => {
  let component: PaymentDetailItemComponent;
  let fixture: ComponentFixture<PaymentDetailItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PaymentDetailItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentDetailItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
