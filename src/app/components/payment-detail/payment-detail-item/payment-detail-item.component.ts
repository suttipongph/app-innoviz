import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, PaidToType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { PaymentDetailItemView } from 'shared/models/viewModel';
import { PaymentDetailService } from '../payment-detail.service';
import { PaymentDetailItemCustomComponent } from './payment-detail-item-custom.component';
@Component({
  selector: 'payment-detail-item',
  templateUrl: './payment-detail-item.component.html',
  styleUrls: ['./payment-detail-item.component.scss']
})
export class PaymentDetailItemComponent extends BaseItemComponent<PaymentDetailItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  customerTableOptions: SelectItems[] = [];
  invoiceTypeOptions: SelectItems[] = [];
  paidToTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  vendorTableOptions: SelectItems[] = [];

  constructor(
    private service: PaymentDetailService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: PaymentDetailItemCustomComponent,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
  }
  ngOnInit(): void {
    let passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setPassParameter(passingObj);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getPaidToTypeEnumDropDown(this.paidToTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<PaymentDetailItemView> {
    return this.service.getPaymentDetailById(this.id);
  }
  getInitialData(): Observable<PaymentDetailItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      result.paidToType = null;
      this.model = result;
      this.custom.setModelByParameter(this.model);
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(
      this.baseDropdown.getCustomerTableDropDown(),
      this.baseDropdown.getInvoiceTypeDropDown(),
      this.baseDropdown.getVendorTableDropDown()
    ).subscribe(
      ([customerTable, invoiceType, vendorTable]) => {
        this.customerTableOptions = customerTable;
        this.invoiceTypeOptions = invoiceType;
        this.vendorTableOptions = vendorTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          this.custom.setModelByParameter(this.model);
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model, this.isWorkFlowMode()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updatePaymentDetail(this.model), isColsing);
    } else {
      super.onCreate(this.service.createPaymentDetail(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation(this.model);
  }
  onPaidToTypeChange(paidToType: PaidToType) {
    this.custom.setDefaultValue(this.model, this.customerTableOptions);
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  onSuspenseTransferChange() {
    this.custom.onSuspenseTransferChange(this.model);
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
}
