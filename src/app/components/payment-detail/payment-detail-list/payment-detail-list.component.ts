import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { PaymentDetailListView } from 'shared/models/viewModel';
import { PaymentDetailService } from '../payment-detail.service';
import { PaymentDetailListCustomComponent } from './payment-detail-list-custom.component';

@Component({
  selector: 'payment-detail-list',
  templateUrl: './payment-detail-list.component.html',
  styleUrls: ['./payment-detail-list.component.scss']
})
export class PaymentDetailListComponent extends BaseListComponent<PaymentDetailListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  customerTableOptions: SelectItems[] = [];
  invoiceTypeOptions: SelectItems[] = [];
  paidToTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  vendorTableOptions: SelectItems[] = [];
  defualtBitOption: SelectItems[] = [];

  constructor(
    public custom: PaymentDetailListCustomComponent,
    private service: PaymentDetailService,
    public uiControllerService: UIControllerService,
    private currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.setAccessModeByParentStatus(this.parentId, this.isWorkFlowMode()).subscribe((result) => {
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getPaidToTypeEnumDropDown(this.paidToTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.baseDropdown.getDefaultBitDropDown(this.defualtBitOption);
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getInvoiceTypeDropDown(this.invoiceTypeOptions);
    this.baseDropdown.getVendorTableDropDown(this.vendorTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'paymentDetailGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.PAID_TO_TYPE',
        textKey: 'paidToType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.ASC,
        masterList: this.paidToTypeOptions,
        sortingOrder: 1
      },
      {
        label: 'LABEL.INVOICE_TYPE',
        textKey: 'invoiceType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.invoiceTypeOptions,
        searchingKey: 'invoiceTypeGUID',
        sortingKey: 'invoiceType_InvoiceTypeId'
      },
      {
        label: 'LABEL.SUSPENSE_TRANSFER',
        textKey: 'suspenseTransfer',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defualtBitOption
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId'
      },
      {
        label: 'LABEL.VENDOR_ID',
        textKey: 'vendorTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.vendorTableOptions,
        searchingKey: 'vendorTableGUID',
        sortingKey: 'vendorTable_VendorId'
      },
      {
        label: 'LABEL.PAYMENT_AMOUNT',
        textKey: 'paymentAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.ASC,
        sortingOrder: 2
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<PaymentDetailListView>> {
    return this.service.getPaymentDetailToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deletePaymentDetail(row));
  }
}
