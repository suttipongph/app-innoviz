import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView } from 'shared/models/viewModel';
import { PaymentDetailService } from '../payment-detail.service';
const PURCHASE_TABLE = 'purchasetablepurchase';
const WITHDRAWAL_TABLE_WITHDRAWAL = 'withdrawaltablewithdrawal';
const WITHDRAWAL_TABLE_TERM_EXTENSION = 'withdrawaltabletermextension';
const RESERVE_REFUND = 'reserverefund';
const RETENTION_REFUND = 'retentionrefund';
const SUSPENSE_REFUND = 'suspenserefund';

export class PaymentDetailListCustomComponent {
  baseService: BaseServiceModel<PaymentDetailService>;
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setAccessModeByParentStatus(parentId: string, isWorkflowMode: boolean): Observable<AccessModeView> {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (this.parentName) {
      case PURCHASE_TABLE:
        return this.baseService.service
          .getPurchaseTableAccessMode(parentId, isWorkflowMode)
          .pipe(tap((result) => ((this.accessModeView = result), (this.accessModeView.canView = true))));
      case WITHDRAWAL_TABLE_WITHDRAWAL:
      case WITHDRAWAL_TABLE_TERM_EXTENSION:
        return this.baseService.service
          .getWithdrawalTableAccessMode(parentId)
          .pipe(tap((result) => ((this.accessModeView = result), (this.accessModeView.canView = true))));
      case SUSPENSE_REFUND:
      case RESERVE_REFUND:
      case RETENTION_REFUND:
        return this.baseService.service
          .getCustomerRefundTableAccessMode(parentId)
          .pipe(tap((result) => ((this.accessModeView = result), (this.accessModeView.canView = true))));
    }
    return of(this.accessModeView);
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
}
