import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, PaymentDetailItemView, PaymentDetailListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class PaymentDetailService {
  serviceKey = 'paymentDetailGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getPaymentDetailToList(search: SearchParameter): Observable<SearchResult<PaymentDetailListView>> {
    const url = `${this.servicePath}/GetPaymentDetailList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deletePaymentDetail(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeletePaymentDetail`;
    return this.dataGateway.delete(url, row);
  }
  getPaymentDetailById(id: string): Observable<PaymentDetailItemView> {
    const url = `${this.servicePath}/GetPaymentDetailById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createPaymentDetail(vmModel: PaymentDetailItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreatePaymentDetail`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updatePaymentDetail(vmModel: PaymentDetailItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdatePaymentDetail`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<PaymentDetailItemView> {
    const url = `${this.servicePath}/GetPaymentDetailInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getPurchaseTableAccessMode(purchaseId: string, isWorkflowMode: boolean): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetPurchaseTableAccessMode/purchaseId=${purchaseId}/isWorkflowMode=${isWorkflowMode}`;
    return this.dataGateway.get(url);
  }
  getWithdrawalTableAccessMode(withdrawalId: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetWithdrawalTableAccessMode/withdrawalId=${withdrawalId}`;
    return this.dataGateway.get(url);
  }
  getCustomerRefundTableAccessMode(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetCustomerRefundTableAccessMode/id=${id}`;
    return this.dataGateway.get(url);
  }
}
