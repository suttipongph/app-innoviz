import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaymentDetailService } from './payment-detail.service';
import { PaymentDetailListComponent } from './payment-detail-list/payment-detail-list.component';
import { PaymentDetailItemComponent } from './payment-detail-item/payment-detail-item.component';
import { PaymentDetailItemCustomComponent } from './payment-detail-item/payment-detail-item-custom.component';
import { PaymentDetailListCustomComponent } from './payment-detail-list/payment-detail-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PaymentDetailListComponent, PaymentDetailItemComponent],
  providers: [PaymentDetailService, PaymentDetailItemCustomComponent, PaymentDetailListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PaymentDetailListComponent, PaymentDetailItemComponent]
})
export class PaymentDetailComponentModule {}
