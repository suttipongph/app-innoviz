import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AgreementTableInfoTextItemView, AgreementTableInfoTextListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class AgreementTableInfoTextService {
  serviceKey = 'agreementTableInfoTextGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getAgreementTableInfoTextToList(search: SearchParameter): Observable<SearchResult<AgreementTableInfoTextListView>> {
    const url = `${this.servicePath}/GetAgreementTableInfoTextList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteAgreementTableInfoText(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteAgreementTableInfoText`;
    return this.dataGateway.delete(url, row);
  }
  getAgreementTableInfoTextById(id: string): Observable<AgreementTableInfoTextItemView> {
    const url = `${this.servicePath}/GetAgreementTableInfoTextById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createAgreementTableInfoText(vmModel: AgreementTableInfoTextItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateAgreementTableInfoText`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateAgreementTableInfoText(vmModel: AgreementTableInfoTextItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateAgreementTableInfoText`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
