import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { AgreementTableInfoTextItemView } from 'shared/models/viewModel';

const firstGroup = [];

export class AgreementTableInfoTextItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: AgreementTableInfoTextItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: AgreementTableInfoTextItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<AgreementTableInfoTextItemView> {
    let model = new AgreementTableInfoTextItemView();
    model.agreementTableInfoTextGUID = EmptyGuid;
    return of(model);
  }
}
