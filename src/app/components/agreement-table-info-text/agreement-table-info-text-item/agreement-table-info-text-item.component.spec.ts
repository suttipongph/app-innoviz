import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgreementTableInfoTextItemComponent } from './agreement-table-info-text-item.component';

describe('AgreementTableInfoTextItemViewComponent', () => {
  let component: AgreementTableInfoTextItemComponent;
  let fixture: ComponentFixture<AgreementTableInfoTextItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AgreementTableInfoTextItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgreementTableInfoTextItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
