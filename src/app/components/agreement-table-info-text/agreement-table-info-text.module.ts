import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgreementTableInfoTextService } from './agreement-table-info-text.service';
import { AgreementTableInfoTextListComponent } from './agreement-table-info-text-list/agreement-table-info-text-list.component';
import { AgreementTableInfoTextItemComponent } from './agreement-table-info-text-item/agreement-table-info-text-item.component';
import { AgreementTableInfoTextItemCustomComponent } from './agreement-table-info-text-item/agreement-table-info-text-item-custom.component';
import { AgreementTableInfoTextListCustomComponent } from './agreement-table-info-text-list/agreement-table-info-text-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [AgreementTableInfoTextListComponent, AgreementTableInfoTextItemComponent],
  providers: [AgreementTableInfoTextService, AgreementTableInfoTextItemCustomComponent, AgreementTableInfoTextListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [AgreementTableInfoTextListComponent, AgreementTableInfoTextItemComponent]
})
export class AgreementTableInfoTextComponentModule {}
