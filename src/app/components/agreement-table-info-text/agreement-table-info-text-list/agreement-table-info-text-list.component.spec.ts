import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AgreementTableInfoTextListComponent } from './agreement-table-info-text-list.component';

describe('AgreementTableInfoTextListViewComponent', () => {
  let component: AgreementTableInfoTextListComponent;
  let fixture: ComponentFixture<AgreementTableInfoTextListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AgreementTableInfoTextListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgreementTableInfoTextListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
