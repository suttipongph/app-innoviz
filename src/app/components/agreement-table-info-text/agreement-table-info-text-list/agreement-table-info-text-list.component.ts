import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { AgreementTableInfoTextListView } from 'shared/models/viewModel';
import { AgreementTableInfoTextService } from '../agreement-table-info-text.service';
import { AgreementTableInfoTextListCustomComponent } from './agreement-table-info-text-list-custom.component';

@Component({
  selector: 'agreement-table-info-text-list',
  templateUrl: './agreement-table-info-text-list.component.html',
  styleUrls: ['./agreement-table-info-text-list.component.scss']
})
export class AgreementTableInfoTextListComponent extends BaseListComponent<AgreementTableInfoTextListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  constructor(public custom: AgreementTableInfoTextListCustomComponent, private service: AgreementTableInfoTextService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'agreementTableInfoTextGUID';
    const columns: ColumnModel[] = [];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<AgreementTableInfoTextListView>> {
    return this.service.getAgreementTableInfoTextToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteAgreementTableInfoText(row));
  }
}
