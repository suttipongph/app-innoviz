import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InquiryRollbillPurchaseLineService } from './inquiry-rollbill-purchase-line.service';
import { InquiryRollbillPurchaseLineListComponent } from './inquiry-rollbill-purchase-line-list/inquiry-rollbill-purchase-line-list.component';
import { InquiryRollbillPurchaseLineItemComponent } from './inquiry-rollbill-purchase-line-item/inquiry-rollbill-purchase-line-item.component';
import { InquiryRollbillPurchaseLineItemCustomComponent } from './inquiry-rollbill-purchase-line-item/inquiry-rollbill-purchase-line-item-custom.component';
import { InquiryRollbillPurchaseLineListCustomComponent } from './inquiry-rollbill-purchase-line-list/inquiry-rollbill-purchase-line-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [InquiryRollbillPurchaseLineListComponent, InquiryRollbillPurchaseLineItemComponent],
  providers: [InquiryRollbillPurchaseLineService, InquiryRollbillPurchaseLineItemCustomComponent, InquiryRollbillPurchaseLineListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [InquiryRollbillPurchaseLineListComponent, InquiryRollbillPurchaseLineItemComponent]
})
export class InquiryRollbillPurchaseLineComponentModule {}
