import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InquiryRollbillPurchaseLineListComponent } from './inquiry-rollbill-purchase-line-list.component';

describe('InquiryRollbillPurchaseLineListViewComponent', () => {
  let component: InquiryRollbillPurchaseLineListComponent;
  let fixture: ComponentFixture<InquiryRollbillPurchaseLineListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InquiryRollbillPurchaseLineListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryRollbillPurchaseLineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
