import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, EmptyGuid, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { InquiryRollbillPurchaseLineListView } from 'shared/models/viewModel';
import { InquiryRollbillPurchaseLineService } from '../inquiry-rollbill-purchase-line.service';
import { InquiryRollbillPurchaseLineListCustomComponent } from './inquiry-rollbill-purchase-line-list-custom.component';

@Component({
  selector: 'inquiry-rollbill-purchase-line-list',
  templateUrl: './inquiry-rollbill-purchase-line-list.component.html',
  styleUrls: ['./inquiry-rollbill-purchase-line-list.component.scss']
})
export class InquiryRollbillPurchaseLineListComponent extends BaseListComponent<InquiryRollbillPurchaseLineListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  constructor(public custom: InquiryRollbillPurchaseLineListCustomComponent, private service: InquiryRollbillPurchaseLineService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.service.getPurchaseLineById(this.baseService.uiService.getKey(ROUTE_RELATED_GEN.PURCHASE_LINE_PURCHASE)).subscribe((result) => {
      this.parentId = isNullOrUndefined(result.originalPurchaseLineGUID) ? EmptyGuid : result.originalPurchaseLineGUID;
      this.onEnumLoader();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'purchaseLineGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.PURCHASE_ID',
        textKey: 'purchaseTable_PurchaseId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.LINE',
        textKey: 'lineNum',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PURCHASE_DATE',
        textKey: 'purchaseTable_PurchaseDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.NUMBER_OF_ROLL_BILL',
        textKey: 'numberOfRollbill',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.OUTSTANDING_BUYER_INVOICE_AMOUNT',
        textKey: 'outstandingBuyerInvoiceAmount ',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.LINE_PURCHASE_AMOUNT',
        textKey: 'linePurchaseAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DUE_DATE',
        textKey: 'dueDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.INTEREST_DATE',
        textKey: 'interestDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'documentStatus_Description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'purchaseLineGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<InquiryRollbillPurchaseLineListView>> {
    return this.service.getInquiryRollbillPurchaseLineToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteInquiryRollbillPurchaseLine(row));
  }
}
