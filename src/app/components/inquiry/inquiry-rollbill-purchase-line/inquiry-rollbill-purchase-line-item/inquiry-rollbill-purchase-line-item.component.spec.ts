import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InquiryRollbillPurchaseLineItemComponent } from './inquiry-rollbill-purchase-line-item.component';

describe('InquiryRollbillPurchaseLineItemViewComponent', () => {
  let component: InquiryRollbillPurchaseLineItemComponent;
  let fixture: ComponentFixture<InquiryRollbillPurchaseLineItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InquiryRollbillPurchaseLineItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryRollbillPurchaseLineItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
