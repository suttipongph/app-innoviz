import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { InquiryRollbillPurchaseLineItemView } from 'shared/models/viewModel';

const firstGroup = [
  'PURCHASE_ID',
  'LINE_NUM',
  'PURCHASE_DATE',
  'NUMBER_OF_ROLLBILL',
  'OUTSTANDING_BUYER_INVOICE_AMOUNT',
  'LINE_PURCHASE_AMOUNT',
  'DUE_DATE',
  'INTEREST_DATE',
  'PURCHASE_STATUS',
  'PURCHASE_LINE_GUID'
];

export class InquiryRollbillPurchaseLineItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: InquiryRollbillPurchaseLineItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: InquiryRollbillPurchaseLineItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(): Observable<InquiryRollbillPurchaseLineItemView> {
    let model = new InquiryRollbillPurchaseLineItemView();
    model.inquiryRollbillPurchaseLineGUID = EmptyGuid;
    return of(model);
  }
}
