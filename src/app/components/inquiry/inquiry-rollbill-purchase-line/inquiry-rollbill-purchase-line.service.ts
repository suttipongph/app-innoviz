import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { InquiryRollbillPurchaseLineItemView, InquiryRollbillPurchaseLineListView, PurchaseLineItemView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class InquiryRollbillPurchaseLineService {
  serviceKey = 'inquiryRollbillPurchaseLineGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getInquiryRollbillPurchaseLineToList(search: SearchParameter): Observable<SearchResult<InquiryRollbillPurchaseLineListView>> {
    const url = `${this.servicePath}/GetInquiryRollbillPurchaseLineList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteInquiryRollbillPurchaseLine(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteInquiryRollbillPurchaseLine`;
    return this.dataGateway.delete(url, row);
  }
  getInquiryRollbillPurchaseLineById(id: string): Observable<InquiryRollbillPurchaseLineItemView> {
    const url = `${this.servicePath}/GetInquiryRollbillPurchaseLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createInquiryRollbillPurchaseLine(vmModel: InquiryRollbillPurchaseLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateInquiryRollbillPurchaseLine`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateInquiryRollbillPurchaseLine(vmModel: InquiryRollbillPurchaseLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateInquiryRollbillPurchaseLine`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getPurchaseLineById(id: string): Observable<PurchaseLineItemView> {
    const url = `${this.servicePath}/GetPurchaseLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
}
