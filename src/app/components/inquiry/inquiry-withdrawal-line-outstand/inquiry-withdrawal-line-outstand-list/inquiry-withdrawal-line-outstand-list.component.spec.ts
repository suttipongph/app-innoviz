import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InquiryWithdrawalLineOutstandListComponent } from './inquiry-withdrawal-line-outstand-list.component';

describe('InquiryWithdrawalLineOutstandListViewComponent', () => {
  let component: InquiryWithdrawalLineOutstandListComponent;
  let fixture: ComponentFixture<InquiryWithdrawalLineOutstandListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InquiryWithdrawalLineOutstandListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryWithdrawalLineOutstandListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
