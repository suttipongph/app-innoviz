import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { InquiryWithdrawalLineOutstandListView } from 'shared/models/viewModel/inquiryWithdrawalLineOutstandListView';
import { InquiryWithdrawalLineOutstandService } from '../inquiry-withdrawal-line-outstand.service';
import { InquiryWithdrawalLineOutstandListCustomComponent } from './inquiry-withdrawal-line-outstand-list-custom.component';

@Component({
  selector: 'inquiry-withdrawal-line-outstand-list',
  templateUrl: './inquiry-withdrawal-line-outstand-list.component.html',
  styleUrls: ['./inquiry-withdrawal-line-outstand-list.component.scss']
})
export class InquiryWithdrawalLineOutstandListComponent
  extends BaseListComponent<InquiryWithdrawalLineOutstandListView>
  implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  buyerTableOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  methodOfPaymentOptions: SelectItems[] = [];
  withdrawalTableOptions: SelectItems[] = [];
  constructor(public custom: InquiryWithdrawalLineOutstandListCustomComponent, private service: InquiryWithdrawalLineOutstandService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getMethodOfPaymentDropDown(this.methodOfPaymentOptions);
    this.baseDropdown.getWithdrawalTableDropDown(this.withdrawalTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'withdrawalLineGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.WITHDRAWAL_ID',
        textKey: 'withdrawalTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.ASC,
        masterList: this.withdrawalTableOptions,
        searchingKey: 'withdrawalTableGUID',
        sortingKey: 'withdrawalTable_WithdrawalId'
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId'
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerTableOptions,
        searchingKey: 'buyerTableGUID',
        sortingKey: 'buyerTable_BuyerId'
      },
      {
        label: 'LABEL.DUE_DATE',
        textKey: 'dueDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BILLING_DATE',
        textKey: 'billingDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.COLLECTION_DATE',
        textKey: 'collectionDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.WITHDRAWAL_AMOUNT',
        textKey: 'withdrawalAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.OUTSTANDING',
        textKey: 'outstanding',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.METHOD_OF_PAYMENT_ID',
        textKey: 'methodOfPayment_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.methodOfPaymentOptions,
        searchingKey: 'methodOfPaymentGUID',
        sortingKey: 'methodOfPayment_MethodOfPaymentId'
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<InquiryWithdrawalLineOutstandListView>> {
    return this.service.getInquiryWithdrawalLineOutstandToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteInquiryWithdrawalLineOutstand(row));
  }
}
