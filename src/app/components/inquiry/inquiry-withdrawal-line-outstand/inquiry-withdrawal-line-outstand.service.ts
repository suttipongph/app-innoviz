import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { InquiryWithdrawalLineOutstandItemView } from 'shared/models/viewModel/inquiryWithdrawalLineOutstandItemView';
import { InquiryWithdrawalLineOutstandListView } from 'shared/models/viewModel/inquiryWithdrawalLineOutstandListView';
@Injectable()
export class InquiryWithdrawalLineOutstandService {
  serviceKey = 'withdrawalLineGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getInquiryWithdrawalLineOutstandToList(search: SearchParameter): Observable<SearchResult<InquiryWithdrawalLineOutstandListView>> {
    const url = `${this.servicePath}/GetInquiryWithdrawalLineOutstandList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteInquiryWithdrawalLineOutstand(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteInquiryWithdrawalLineOutstand`;
    return this.dataGateway.delete(url, row);
  }
  getInquiryWithdrawalLineOutstandById(id: string): Observable<InquiryWithdrawalLineOutstandItemView> {
    const url = `${this.servicePath}/GetInquiryWithdrawalLineOutstandById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createInquiryWithdrawalLineOutstand(vmModel: InquiryWithdrawalLineOutstandItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateInquiryWithdrawalLineOutstand`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateInquiryWithdrawalLineOutstand(vmModel: InquiryWithdrawalLineOutstandItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateInquiryWithdrawalLineOutstand`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
