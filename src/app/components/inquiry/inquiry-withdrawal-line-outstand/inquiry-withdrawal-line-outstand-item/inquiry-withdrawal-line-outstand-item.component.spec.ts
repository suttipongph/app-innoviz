import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InquiryWithdrawalLineOutstandItemComponent } from './inquiry-withdrawal-line-outstand-item.component';

describe('InquiryWithdrawalLineOutstandItemViewComponent', () => {
  let component: InquiryWithdrawalLineOutstandItemComponent;
  let fixture: ComponentFixture<InquiryWithdrawalLineOutstandItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InquiryWithdrawalLineOutstandItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryWithdrawalLineOutstandItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
