import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { InquiryWithdrawalLineOutstandItemView } from 'shared/models/viewModel/inquiryWithdrawalLineOutstandItemView';

const firstGroup = [
  'WITHDRAWAL_TABLE_ID',
  'CUSTOMER_ID',
  'BUYER_ID',
  'BUYER_AGREEMENT_TRANS',
  'WITHDRAWAL_AMOUNT',
  'OUTSTANDING',
  'METHOD_OF_PAYMENT_ID',
  'DUE_DATE',
  'CUSTOMER_PDC_DATE',
  'BILLING_DATE',
  'COLLECTION_DATE',
  'CREDIT_APP_TABLE_BANK_ACCOUNT_CONTROL_ID',
  'CREDIT_APP_TABLE_PDC_BANK_ID',
  'WITHDRAWAL_LINE_GUID'
];

export class InquiryWithdrawalLineOutstandItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: InquiryWithdrawalLineOutstandItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: InquiryWithdrawalLineOutstandItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(): Observable<InquiryWithdrawalLineOutstandItemView> {
    let model = new InquiryWithdrawalLineOutstandItemView();
    model.withdrawalLineGUID = EmptyGuid;
    return of(model);
  }
}
