import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { AccessModeView } from 'shared/models/viewModel';
import { InquiryWithdrawalLineOutstandItemView } from 'shared/models/viewModel/inquiryWithdrawalLineOutstandItemView';
import { InquiryWithdrawalLineOutstandService } from '../inquiry-withdrawal-line-outstand.service';
import { InquiryWithdrawalLineOutstandItemCustomComponent } from './inquiry-withdrawal-line-outstand-item-custom.component';
@Component({
  selector: 'inquiry-withdrawal-line-outstand-item',
  templateUrl: './inquiry-withdrawal-line-outstand-item.component.html',
  styleUrls: ['./inquiry-withdrawal-line-outstand-item.component.scss']
})
export class InquiryWithdrawalLineOutstandItemComponent
  extends BaseItemComponent<InquiryWithdrawalLineOutstandItemView>
  implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  constructor(
    private service: InquiryWithdrawalLineOutstandService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: InquiryWithdrawalLineOutstandItemCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<InquiryWithdrawalLineOutstandItemView> {
    return this.service.getInquiryWithdrawalLineOutstandById(this.id);
  }
  getInitialData(): Observable<InquiryWithdrawalLineOutstandItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions(result);
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  setRelatedInfoOptions(model: InquiryWithdrawalLineOutstandItemView): void {
    const accessModeView = { canCreate: true, canView: true, canDelete: true } as AccessModeView;
    this.relatedInfoItems = [
      {
        label: 'LABEL.COLLECTION_FOLLOW_UP',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.COLLECTION_FOLLOW_UP,
            parameters: {
              accessModeView: accessModeView
            }
          })
      },
      {
        label: 'LABEL.PDC',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.PDC })
      },
      {
        label: 'LABEL.MESSENGER_REQUEST',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.MESSENGER_REQUEST
          })
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateInquiryWithdrawalLineOutstand(this.model), isColsing);
    } else {
      super.onCreate(this.service.createInquiryWithdrawalLineOutstand(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
