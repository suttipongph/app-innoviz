import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InquiryWithdrawalLineOutstandService } from './inquiry-withdrawal-line-outstand.service';
import { InquiryWithdrawalLineOutstandListComponent } from './inquiry-withdrawal-line-outstand-list/inquiry-withdrawal-line-outstand-list.component';
import { InquiryWithdrawalLineOutstandItemComponent } from './inquiry-withdrawal-line-outstand-item/inquiry-withdrawal-line-outstand-item.component';
import { InquiryWithdrawalLineOutstandItemCustomComponent } from './inquiry-withdrawal-line-outstand-item/inquiry-withdrawal-line-outstand-item-custom.component';
import { InquiryWithdrawalLineOutstandListCustomComponent } from './inquiry-withdrawal-line-outstand-list/inquiry-withdrawal-line-outstand-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [InquiryWithdrawalLineOutstandListComponent, InquiryWithdrawalLineOutstandItemComponent],
  providers: [
    InquiryWithdrawalLineOutstandService,
    InquiryWithdrawalLineOutstandItemCustomComponent,
    InquiryWithdrawalLineOutstandListCustomComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [InquiryWithdrawalLineOutstandListComponent, InquiryWithdrawalLineOutstandItemComponent]
})
export class InquiryWithdrawalLineOutstandComponentModule {}
