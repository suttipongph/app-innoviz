import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { InquiryCreditOutstandingByCustListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchCondition, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { getRelatedItemCondition } from 'shared/functions/value.function';
@Injectable()
export class InquiryCreditOutstandingByCustService {
  serviceKey = 'inquiryCreditOutstandingByCustGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getInquiryCreditOutstandingListByCustToList(search: SearchParameter): Observable<SearchResult<InquiryCreditOutstandingByCustListView>> {
    const url = `${this.servicePath}/getInquiryCreditOutstandingListByCustToList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
}
