import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InquiryCreditOutstandingByCustService } from './inquiry-credit-outstanding-by-cust.service';
import { InquiryCreditOutstandingByCustListComponent } from './inquiry-credit-outstanding-by-cust-list/inquiry-credit-outstanding-by-cust-list.component';
import { InquiryCreditOutstandingByCustListCustomComponent } from './inquiry-credit-outstanding-by-cust-list/inquiry-credit-outstanding-by-cust-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [InquiryCreditOutstandingByCustListComponent],
  providers: [
    InquiryCreditOutstandingByCustService,
    InquiryCreditOutstandingByCustListCustomComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [InquiryCreditOutstandingByCustListComponent]
})
export class InquiryCreditOutstandingByCustComponentModule {}
