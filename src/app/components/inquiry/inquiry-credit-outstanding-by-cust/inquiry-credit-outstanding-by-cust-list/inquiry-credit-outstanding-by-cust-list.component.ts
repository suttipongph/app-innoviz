import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import { datetoString, getMaxDate, setDate } from 'shared/functions/date.function';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { InquiryCreditOutstandingByCustListView } from 'shared/models/viewModel';
import { InquiryCreditOutstandingByCustService } from '../inquiry-credit-outstanding-by-cust.service';
import { InquiryCreditOutstandingByCustListCustomComponent } from './inquiry-credit-outstanding-by-cust-list-custom.component';

@Component({
  selector: 'inquiry-credit-outstanding-by-cust-list',
  templateUrl: './inquiry-credit-outstanding-by-cust-list.component.html',
  styleUrls: ['./inquiry-credit-outstanding-by-cust-list.component.scss']
})
export class InquiryCreditOutstandingByCustListComponent
  extends BaseListComponent<InquiryCreditOutstandingByCustListView>
  implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  productTypeOptions: SelectItems[] = [];
  creditLimitTypeOptions: SelectItems[] = [];
  creditAppTableOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];

  constructor(
    public custom: InquiryCreditOutstandingByCustListCustomComponent,
    private service: InquiryCreditOutstandingByCustService,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.parentId = this.uiControllerService.getRelatedInfoOriginTableKey();
  }
  ngOnInit(): void {
    this.custom.setTitle();
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCreditLimitTypeDropDown(this.creditLimitTypeOptions);
    this.baseDropdown.getCreditAppTableDropDown(this.creditAppTableOptions);
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = false;
    this.option.canDelete = false;
    this.option.key = 'inquiryCreditOutstandingByCustGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: 'LABEL.CREDIT_LIMIT_TYPE_ID',
        textKey: 'creditLimitType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditLimitTypeOptions,
        searchingKey: 'creditLimitTypeGUID',
        sortingKey: 'creditLimitType_CreditLimitTypeId'
      },
      {
        label: 'LABEL.CREDIT_APPLICATION_ID',
        textKey: 'creditAppTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditAppTableOptions,
        searchingKey: 'creditAppTableGUID',
        sortingKey: 'creditAppTable_CreditAppId'
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'customerTableGUID',
        masterList: this.customerTableOptions,
        sortingKey: 'customerTable_CustomerId'
      },
      {
        label: 'LABEL.AS_OF_DATE',
        textKey: 'asOfDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE,
        values: [datetoString(setDate(new Date(), 1)), getMaxDate()],
        minDate: setDate(new Date(), 1)
      },
      {
        label: 'LABEL.APPROVED_CREDIT_LIMIT',
        textKey: 'approvedCreditLimit',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5,
        sortingKey: 'approvedCreditLimit',
        searchingKey: 'approvedCreditLimit'
      },
      {
        label: 'LABEL.CREDIT_LIMIT_BALANCE',
        textKey: 'creditLimitBalance',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5,
        sortingKey: 'creditLimitBalance',
        searchingKey: 'creditLimitBalance'
      },
      {
        label: 'LABEL.AR_BALANCE',
        textKey: 'arBalance',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5,
        sortingKey: 'arBalance',
        searchingKey: 'arBalance'
      },
      {
        label: 'LABEL.ACCUMULATE_RETENTION_AMOUNT',
        textKey: 'accumRetentionAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5,
        sortingKey: 'accumRetentionAmount',
        searchingKey: 'accumRetentionAmount'
      },
      {
        label: 'LABEL.RESERVE_TO_BE_REFUND',
        textKey: 'reserveToBeRefund',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5,
        sortingKey: 'reserveToBeRefund',
        searchingKey: 'reserveToBeRefund'
      },
      {
        label: null,
        textKey: 'customerTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<InquiryCreditOutstandingByCustListView>> {
    return this.service.getInquiryCreditOutstandingListByCustToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {}
}
