import { Observable } from 'rxjs';
import { ROUTE_RELATED_GEN } from 'shared/constants';
import { BaseServiceModel } from 'shared/models/systemModel';
import { InquiryCreditOutstandingByCustService } from '../inquiry-credit-outstanding-by-cust.service';

export class InquiryCreditOutstandingByCustListCustomComponent {
  baseService: BaseServiceModel<InquiryCreditOutstandingByCustService>;
  title: string = null;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setTitle() {
    let activeParentName = this.baseService.uiService.getRelatedInfoActiveTableName();
    switch (activeParentName) {
      case ROUTE_RELATED_GEN.BUYER_CREDIT_OF_ALL_CUSTOMER:
        this.title = 'LABEL.BUYER_CREDIT_OF_ALL_CUSTOMER';
        break;
      default:
        this.title = 'LABEL.CREDIT_OUTSTANDING';
        break;
    }
  }
}
