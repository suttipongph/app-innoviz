import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InquiryCreditOutstandingByCustListComponent } from './inquiry-credit-outstanding-by-cust-list.component';

describe('InquiryCreditOutstandingByCustListViewComponent', () => {
  let component: InquiryCreditOutstandingByCustListComponent;
  let fixture: ComponentFixture<InquiryCreditOutstandingByCustListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InquiryCreditOutstandingByCustListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryCreditOutstandingByCustListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
