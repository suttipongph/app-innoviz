import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InquiryRetentionOutstandingService } from './inquiry-retention-outstanding.service';
import { InquiryRetentionOutstandingListComponent } from './inquiry-retention-outstanding-list/inquiry-retention-outstanding-list.component';
import { InquiryRetentionOutstandingItemComponent } from './inquiry-retention-outstanding-item/inquiry-retention-outstanding-item.component';
import { InquiryRetentionOutstandingItemCustomComponent } from './inquiry-retention-outstanding-item/inquiry-retention-outstanding-item-custom.component';
import { InquiryRetentionOutstandingListCustomComponent } from './inquiry-retention-outstanding-list/inquiry-retention-outstanding-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [InquiryRetentionOutstandingListComponent, InquiryRetentionOutstandingItemComponent],
  providers: [InquiryRetentionOutstandingService, InquiryRetentionOutstandingItemCustomComponent, InquiryRetentionOutstandingListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [InquiryRetentionOutstandingListComponent, InquiryRetentionOutstandingItemComponent]
})
export class InquiryRetentionOutstandingComponentModule {}
