import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InquiryRetentionOutstandingListComponent } from './inquiry-retention-outstanding-list.component';

describe('InquiryRetentionOutstandingListViewComponent', () => {
  let component: InquiryRetentionOutstandingListComponent;
  let fixture: ComponentFixture<InquiryRetentionOutstandingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InquiryRetentionOutstandingListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryRetentionOutstandingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
