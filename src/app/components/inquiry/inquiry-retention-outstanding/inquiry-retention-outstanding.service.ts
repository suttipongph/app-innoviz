import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { InquiryRetentionOutstandingItemView, InquiryRetentionOutstandingListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class InquiryRetentionOutstandingService {
  serviceKey = 'inquiryRetentionOutstandingGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getInquiryRetentionOutstandingToList(search: SearchParameter): Observable<SearchResult<InquiryRetentionOutstandingListView>> {
    const url = `${this.servicePath}/GetInquiryRetentionOutstandingList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteInquiryRetentionOutstanding(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteInquiryRetentionOutstanding`;
    return this.dataGateway.delete(url, row);
  }
  getInquiryRetentionOutstandingById(id: string): Observable<InquiryRetentionOutstandingItemView> {
    const url = `${this.servicePath}/GetInquiryRetentionOutstandingById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createInquiryRetentionOutstanding(vmModel: InquiryRetentionOutstandingItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateInquiryRetentionOutstanding`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateInquiryRetentionOutstanding(vmModel: InquiryRetentionOutstandingItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateInquiryRetentionOutstanding`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
