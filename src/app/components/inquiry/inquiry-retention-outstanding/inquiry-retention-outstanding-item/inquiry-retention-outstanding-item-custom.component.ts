import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { InquiryRetentionOutstandingItemView } from 'shared/models/viewModel';

const firstGroup = ['CUSTOMER_ID', 'CREDIT_APP_ID', 'PRODUCT_TYPE', 'MAXIMUM_RETENTION', 'ACCUM_RETENTION_AMOUNT', 'REMAINING_AMOUNT'];

export class InquiryRetentionOutstandingItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: InquiryRetentionOutstandingItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: InquiryRetentionOutstandingItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(): Observable<InquiryRetentionOutstandingItemView> {
    let model = new InquiryRetentionOutstandingItemView();
    model.inquiryRetentionOutstandingGUID = EmptyGuid;
    return of(model);
  }
}
