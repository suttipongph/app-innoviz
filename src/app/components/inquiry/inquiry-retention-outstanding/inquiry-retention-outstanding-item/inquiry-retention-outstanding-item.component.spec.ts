import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InquiryRetentionOutstandingItemComponent } from './inquiry-retention-outstanding-item.component';

describe('InquiryRetentionOutstandingItemViewComponent', () => {
  let component: InquiryRetentionOutstandingItemComponent;
  let fixture: ComponentFixture<InquiryRetentionOutstandingItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InquiryRetentionOutstandingItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryRetentionOutstandingItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
