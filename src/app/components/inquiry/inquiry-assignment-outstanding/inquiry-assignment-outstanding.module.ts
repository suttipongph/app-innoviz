import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InquiryAssignmentOutstandingService } from './inquiry-assignment-outstanding.service';
import { InquiryAssignmentOutstandingListComponent } from './inquiry-assignment-outstanding-list/inquiry-assignment-outstanding-list.component';
import { InquiryAssignmentOutstandingItemComponent } from './inquiry-assignment-outstanding-item/inquiry-assignment-outstanding-item.component';
import { InquiryAssignmentOutstandingItemCustomComponent } from './inquiry-assignment-outstanding-item/inquiry-assignment-outstanding-item-custom.component';
import { InquiryAssignmentOutstandingListCustomComponent } from './inquiry-assignment-outstanding-list/inquiry-assignment-outstanding-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [InquiryAssignmentOutstandingListComponent, InquiryAssignmentOutstandingItemComponent],
  providers: [InquiryAssignmentOutstandingService, InquiryAssignmentOutstandingItemCustomComponent, InquiryAssignmentOutstandingListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [InquiryAssignmentOutstandingListComponent, InquiryAssignmentOutstandingItemComponent]
})
export class InquiryAssignmentOutstandingComponentModule {}
