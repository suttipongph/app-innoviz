import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { InquiryAssignmentOutstandingListView } from 'shared/models/viewModel/inquiryAssignmentOutstandingListView';
import { InquiryAssignmentOutstandingService } from '../inquiry-assignment-outstanding.service';
import { InquiryAssignmentOutstandingListCustomComponent } from './inquiry-assignment-outstanding-list-custom.component';

@Component({
  selector: 'inquiry-assignment-outstanding-list',
  templateUrl: './inquiry-assignment-outstanding-list.component.html',
  styleUrls: ['./inquiry-assignment-outstanding-list.component.scss']
})
export class InquiryAssignmentOutstandingListComponent extends BaseListComponent<InquiryAssignmentOutstandingListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  buyerTableOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];

  constructor(public custom: InquiryAssignmentOutstandingListCustomComponent, private service: InquiryAssignmentOutstandingService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = false;
    this.option.canDelete = false;
    this.option.key = 'inquiryAssignmentOutstandingGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.INTERNAL_ASSIGNMENT_AGREEMENT_ID',
        textKey: 'internalAssignmentAgreementId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId'
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerTableOptions,
        searchingKey: 'buyerTableGUID',
        sortingKey: 'buyerTable_BuyerId'
      },
      {
        label: 'LABEL.ASSIGNMENT_AGREEMENT_AMOUNT',
        textKey: 'assignmentAgreementAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.SETTLED_AMOUNT',
        textKey: 'settledAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.REMAINING_AMOUNT',
        textKey: 'remainingAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      }
    ];

    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<InquiryAssignmentOutstandingListView>> {
    return this.service.getInquiryAssignmentOutstandingToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteInquiryAssignmentOutstanding(row));
  }
}
