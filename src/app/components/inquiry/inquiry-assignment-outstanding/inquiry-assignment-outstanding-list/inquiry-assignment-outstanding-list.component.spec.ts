import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InquiryAssignmentOutstandingListComponent } from './inquiry-assignment-outstanding-list.component';

describe('InquiryAssignmentOutstandingListViewComponent', () => {
  let component: InquiryAssignmentOutstandingListComponent;
  let fixture: ComponentFixture<InquiryAssignmentOutstandingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InquiryAssignmentOutstandingListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryAssignmentOutstandingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
