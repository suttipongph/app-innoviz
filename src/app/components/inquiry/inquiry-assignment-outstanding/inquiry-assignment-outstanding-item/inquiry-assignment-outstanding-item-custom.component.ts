import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { InquiryAssignmentOutstandingItemView } from 'shared/models/viewModel/inquiryAssignmentOutstandingItemView';

const firstGroup = [
  'INTERNAL_ASSIGNMENT_AGREEMENT_ID',
  'CUSTOMER_ID',
  'BUYER_ID',
  'ASSIGNMENT_AGREEMENT_AMOUNT',
  'SETTLE_AMOUNT',
  'REMAINING_AMOUNT'
];

export class InquiryAssignmentOutstandingItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: InquiryAssignmentOutstandingItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: InquiryAssignmentOutstandingItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(): Observable<InquiryAssignmentOutstandingItemView> {
    let model = new InquiryAssignmentOutstandingItemView();
    model.inquiryAssignmentOutstandingGUID = EmptyGuid;
    return of(model);
  }
}
