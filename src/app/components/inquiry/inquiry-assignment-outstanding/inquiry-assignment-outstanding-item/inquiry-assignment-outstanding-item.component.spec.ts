import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InquiryAssignmentOutstandingItemComponent } from './inquiry-assignment-outstanding-item.component';

describe('InquiryAssignmentOutstandingItemViewComponent', () => {
  let component: InquiryAssignmentOutstandingItemComponent;
  let fixture: ComponentFixture<InquiryAssignmentOutstandingItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InquiryAssignmentOutstandingItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryAssignmentOutstandingItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
