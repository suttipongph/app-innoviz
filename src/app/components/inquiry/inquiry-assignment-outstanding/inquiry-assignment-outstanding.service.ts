import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { InquiryAssignmentOutstandingListView } from 'shared/models/viewModel/inquiryAssignmentOutstandingListView';
import { InquiryAssignmentOutstandingItemView } from 'shared/models/viewModel/inquiryAssignmentOutstandingItemView';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class InquiryAssignmentOutstandingService {
  serviceKey = 'inquiryAssignmentOutstandingGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getInquiryAssignmentOutstandingToList(search: SearchParameter): Observable<SearchResult<InquiryAssignmentOutstandingListView>> {
    const url = `${this.servicePath}/GetInquiryAssignmentOutstandingList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteInquiryAssignmentOutstanding(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteInquiryAssignmentOutstanding`;
    return this.dataGateway.delete(url, row);
  }
  getInquiryAssignmentOutstandingById(id: string): Observable<InquiryAssignmentOutstandingItemView> {
    const url = `${this.servicePath}/GetInquiryAssignmentOutstandingById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createInquiryAssignmentOutstanding(vmModel: InquiryAssignmentOutstandingItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateInquiryAssignmentOutstanding`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateInquiryAssignmentOutstanding(vmModel: InquiryAssignmentOutstandingItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateInquiryAssignmentOutstanding`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
