import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InquiryCALineOutstandingByCAService } from './inquiry-ca-line-outstanding-by-ca.service';
import { InquiryCALineOutstandingByCAListComponent } from './inquiry-ca-line-outstanding-by-ca-list/inquiry-ca-line-outstanding-by-ca-list.component';
import { InquiryCALineOutstandingByCAListCustomComponent } from './inquiry-ca-line-outstanding-by-ca-list/inquiry-ca-line-outstanding-by-ca-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [InquiryCALineOutstandingByCAListComponent],
  providers: [InquiryCALineOutstandingByCAService, InquiryCALineOutstandingByCAListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [InquiryCALineOutstandingByCAListComponent]
})
export class InquiryCALineOutstandingByCAComponentModule {}
