import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { InquiryCALineOutstandingByCAListView } from 'shared/models/viewModel';
import { PageInformationModel, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class InquiryCALineOutstandingByCAService {
  serviceKey = 'inquiryCALineOutstandingByCAGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getInquiryCALineOutstandingByCAToList(search: SearchParameter): Observable<SearchResult<InquiryCALineOutstandingByCAListView>> {
    const url = `${this.servicePath}/GetInquiryCALineOutstandingByCAList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
}
