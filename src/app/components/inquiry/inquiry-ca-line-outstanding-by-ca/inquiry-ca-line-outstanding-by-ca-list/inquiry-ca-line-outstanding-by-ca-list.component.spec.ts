import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InquiryCALineOutstandingByCAListComponent } from './inquiry-ca-line-outstanding-by-ca-list.component';

describe('InquiryCALineOutstandingByCAListViewComponent', () => {
  let component: InquiryCALineOutstandingByCAListComponent;
  let fixture: ComponentFixture<InquiryCALineOutstandingByCAListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InquiryCALineOutstandingByCAListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryCALineOutstandingByCAListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
