import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { InquiryCALineOutstandingByCAListView } from 'shared/models/viewModel';
import { InquiryCALineOutstandingByCAService } from '../inquiry-ca-line-outstanding-by-ca.service';
import { InquiryCALineOutstandingByCAListCustomComponent } from './inquiry-ca-line-outstanding-by-ca-list-custom.component';

@Component({
  selector: 'inquiry-ca-line-outstanding-by-ca-list',
  templateUrl: './inquiry-ca-line-outstanding-by-ca-list.component.html',
  styleUrls: ['./inquiry-ca-line-outstanding-by-ca-list.component.scss']
})
export class InquiryCALineOutstandingByCAListComponent extends BaseListComponent<InquiryCALineOutstandingByCAListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  productTypeOptions: SelectItems[] = [];
  creditLimitTypeOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  creditAppTableOptions: SelectItems[] = [];
  constructor(
    public custom: InquiryCALineOutstandingByCAListCustomComponent,
    private service: InquiryCALineOutstandingByCAService,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.parentId = this.uiControllerService.getRelatedInfoOriginTableKey();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCreditAppTableDropDown(this.creditAppTableOptions);
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getCreditLimitTypeDropDown(this.creditLimitTypeOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = false;
    this.option.canDelete = false;
    this.option.key = 'creditAppTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.LINE',
        textKey: 'lineNum',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: 'LABEL.CREDIT_LIMIT_TYPE_ID',
        textKey: 'creditLimitType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        sortingKey: 'creditLimitTypeId',
        searchingKey: 'creditLimitTypeGUID',
        masterList: this.creditLimitTypeOptions
      },
      {
        label: 'LABEL.CREDIT_APPLICATION_ID',
        textKey: 'creditAppId',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        sortingKey: 'creditAppId',
        searchingKey: 'creditAppTableGUID',
        masterList: this.creditAppTableOptions
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerId',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        sortingKey: 'customerId',
        searchingKey: 'customerTableGUID',
        masterList: this.customerTableOptions
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerId',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.ASC,
        sortingKey: 'buyerId',
        searchingKey: 'buyerTableGUID',
        masterList: this.buyerTableOptions
      },
      {
        label: 'LABEL.APPROVED_CREDIT_LIMIT_LINE',
        textKey: 'approvedCreditLimitLine',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5
      },
      {
        label: 'LABEL.CREDIT_LIMIT_BALANCE',
        textKey: 'creditLimitBalance',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5,
        disabledFilter: true
      },
      {
        label: 'LABEL.AR_BALANCE',
        textKey: 'arBalance',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_16_5,
        disabledFilter: true
      },
      {
        label: 'LABEL.START_DATE',
        textKey: 'startDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.EXPIRY_DATE',
        textKey: 'expiryDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.ASC
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<InquiryCALineOutstandingByCAListView>> {
    return this.service.getInquiryCALineOutstandingByCAToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {}
}
