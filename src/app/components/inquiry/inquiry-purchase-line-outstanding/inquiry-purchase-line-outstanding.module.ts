import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InquiryPurchaseLineOutstandingService } from './inquiry-purchase-line-outstanding.service';
import { InquiryPurchaseLineOutstandingListComponent } from './inquiry-purchase-line-outstanding-list/inquiry-purchase-line-outstanding-list.component';
import { InquiryPurchaseLineOutstandingItemComponent } from './inquiry-purchase-line-outstanding-item/inquiry-purchase-line-outstanding-item.component';
import { InquiryPurchaseLineOutstandingItemCustomComponent } from './inquiry-purchase-line-outstanding-item/inquiry-purchase-line-outstanding-item-custom.component';
import { InquiryPurchaseLineOutstandingListCustomComponent } from './inquiry-purchase-line-outstanding-list/inquiry-purchase-line-outstanding-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [InquiryPurchaseLineOutstandingListComponent, InquiryPurchaseLineOutstandingItemComponent],
  providers: [
    InquiryPurchaseLineOutstandingService,
    InquiryPurchaseLineOutstandingItemCustomComponent,
    InquiryPurchaseLineOutstandingListCustomComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [InquiryPurchaseLineOutstandingListComponent, InquiryPurchaseLineOutstandingItemComponent]
})
export class InquiryPurchaseLineOutstandingComponentModule {}
