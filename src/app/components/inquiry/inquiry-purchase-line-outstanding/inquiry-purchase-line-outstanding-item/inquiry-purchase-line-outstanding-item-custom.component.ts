import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { InquiryPurchaseLineOutstandingItemView } from 'shared/models/viewModel/inquiryPurchaseLineOutstandingItemView';

const firstGroup = [
  'PURCHASE_ID',
  'CUSTOMER_ID',
  'BUYER_ID',
  'BUYER_INVOICE_ID',
  'INVOICE_AMOUNT',
  'PURCHASE_AMOUNT',
  'OUTSTANDING',
  'METHOD_OF_PAYMENT_ID',
  'DUE_DATE',
  'CUSTOMER_PDC_DATE',
  'BILLING_DATE',
  'COLLECTION_DATE',
  'CREDIT_APP_TABLE_BANK_ACCOUNT_CONTROL_ID',
  'CREDIT_APP_TABLE_PDC_BANK_ID',
  'PURCHASE_LINE_GUID'
];

export class InquiryPurchaseLineOutstandingItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: InquiryPurchaseLineOutstandingItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: InquiryPurchaseLineOutstandingItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(): Observable<InquiryPurchaseLineOutstandingItemView> {
    let model = new InquiryPurchaseLineOutstandingItemView();
    model.purchaseLineGUID = EmptyGuid;
    return of(model);
  }
}
