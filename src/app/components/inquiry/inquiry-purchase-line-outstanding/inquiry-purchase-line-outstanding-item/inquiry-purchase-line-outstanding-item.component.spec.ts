import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InquiryPurchaseLineOutstandingItemComponent } from './inquiry-purchase-line-outstanding-item.component';

describe('InquiryPurchaseLineOutstandingItemViewComponent', () => {
  let component: InquiryPurchaseLineOutstandingItemComponent;
  let fixture: ComponentFixture<InquiryPurchaseLineOutstandingItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InquiryPurchaseLineOutstandingItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryPurchaseLineOutstandingItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
