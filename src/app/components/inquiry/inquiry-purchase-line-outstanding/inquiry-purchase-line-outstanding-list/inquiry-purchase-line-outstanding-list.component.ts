import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { InquiryPurchaseLineOutstandingListView } from 'shared/models/viewModel/inquiryPurchaseLineOutstandingListView';
import { InquiryPurchaseLineOutstandingService } from '../inquiry-purchase-line-outstanding.service';
import { InquiryPurchaseLineOutstandingListCustomComponent } from './inquiry-purchase-line-outstanding-list-custom.component';

@Component({
  selector: 'inquiry-purchase-line-outstanding-list',
  templateUrl: './inquiry-purchase-line-outstanding-list.component.html',
  styleUrls: ['./inquiry-purchase-line-outstanding-list.component.scss']
})
export class InquiryPurchaseLineOutstandingListComponent
  extends BaseListComponent<InquiryPurchaseLineOutstandingListView>
  implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  y;
  buyerTableOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  methodOfPaymentOptions: SelectItems[] = [];
  purchaseTableOptions: SelectItems[] = [];
  constructor(public custom: InquiryPurchaseLineOutstandingListCustomComponent, private service: InquiryPurchaseLineOutstandingService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getMethodOfPaymentDropDown(this.methodOfPaymentOptions);
    this.baseDropdown.getPurchaseTableDropDown(this.purchaseTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'purchaseLineGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.PURCHASE_ID',
        textKey: 'purchaseTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.ASC,
        masterList: this.purchaseTableOptions,
        searchingKey: 'purchaseTableGUID',
        sortingKey: 'purchaseTable_PurchaseId'
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId'
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerTableOptions,
        searchingKey: 'buyerTableGUID',
        sortingKey: 'buyerTable_BuyerId'
      },
      {
        label: 'LABEL.DUE_DATE',
        textKey: 'dueDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BILLING_DATE',
        textKey: 'billingDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.COLLECTION_DATE',
        textKey: 'collectionDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.INVOICE_AMOUNT',
        textKey: 'invoiceAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: 'LABEL.PURCHASE_AMOUNT',
        textKey: 'purchaseAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: 'LABEL.OUTSTANDING',
        textKey: 'outstanding',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: 'LABEL.METHOD_OF_PAYMENT_ID',
        textKey: 'methodOfPayment_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.methodOfPaymentOptions,
        searchingKey: 'methodOfPaymentGUID',
        sortingKey: 'methodOfPayment_MethodOfPaymentId'
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<InquiryPurchaseLineOutstandingListView>> {
    return this.service.getInquiryPurchaseLineOutstandingToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteInquiryPurchaseLineOutstanding(row));
  }
}
