import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InquiryPurchaseLineOutstandingListComponent } from './inquiry-purchase-line-outstanding-list.component';

describe('InquiryPurchaseLineOutstandingListViewComponent', () => {
  let component: InquiryPurchaseLineOutstandingListComponent;
  let fixture: ComponentFixture<InquiryPurchaseLineOutstandingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InquiryPurchaseLineOutstandingListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryPurchaseLineOutstandingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
