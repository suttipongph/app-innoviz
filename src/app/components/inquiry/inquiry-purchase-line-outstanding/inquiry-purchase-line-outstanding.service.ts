import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { InquiryPurchaseLineOutstandingListView } from 'shared/models/viewModel/inquiryPurchaseLineOutstandingListView';
import { InquiryPurchaseLineOutstandingItemView } from 'shared/models/viewModel/inquiryPurchaseLineOutstandingItemView';
@Injectable()
export class InquiryPurchaseLineOutstandingService {
  serviceKey = 'purchaseLineGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getInquiryPurchaseLineOutstandingToList(search: SearchParameter): Observable<SearchResult<InquiryPurchaseLineOutstandingListView>> {
    const url = `${this.servicePath}/GetInquiryPurchaseLineOutstandingList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteInquiryPurchaseLineOutstanding(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteInquiryPurchaseLineOutstanding`;
    return this.dataGateway.delete(url, row);
  }
  getInquiryPurchaseLineOutstandingById(id: string): Observable<InquiryPurchaseLineOutstandingItemView> {
    const url = `${this.servicePath}/GetInquiryPurchaseLineOutstandingById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createInquiryPurchaseLineOutstanding(vmModel: InquiryPurchaseLineOutstandingItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateInquiryPurchaseLineOutstanding`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateInquiryPurchaseLineOutstanding(vmModel: InquiryPurchaseLineOutstandingItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateInquiryPurchaseLineOutstanding`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
