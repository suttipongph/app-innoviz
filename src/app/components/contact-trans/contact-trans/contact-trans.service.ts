import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ContactTransItemView, ContactTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ContactTransService {
  serviceKey = 'contactTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getContactTransToList(search: SearchParameter): Observable<SearchResult<ContactTransListView>> {
    const url = `${this.servicePath}/GetContactTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteContactTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteContactTrans`;
    return this.dataGateway.delete(url, row);
  }
  getContactTransById(id: string): Observable<ContactTransItemView> {
    const url = `${this.servicePath}/GetContactTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createContactTrans(vmModel: ContactTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateContactTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateContactTrans(vmModel: ContactTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateContactTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<ContactTransItemView> {
    const url = `${this.servicePath}/GetContactTransInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
}
