import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContactTransService } from './contact-trans.service';
import { ContactTransListComponent } from './contact-trans-list/contact-trans-list.component';
import { ContactTransItemComponent } from './contact-trans-item/contact-trans-item.component';
import { ContactTransItemCustomComponent } from './contact-trans-item/contact-trans-item-custom.component';
import { ContactTransListCustomComponent } from './contact-trans-list/contact-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ContactTransListComponent, ContactTransItemComponent],
  providers: [ContactTransService, ContactTransItemCustomComponent, ContactTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ContactTransListComponent, ContactTransItemComponent]
})
export class ContactTransComponentModule {}
