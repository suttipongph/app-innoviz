import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ContactTransListComponent } from './contact-trans-list.component';

describe('ContactTransListViewComponent', () => {
  let component: ContactTransListComponent;
  let fixture: ComponentFixture<ContactTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ContactTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
