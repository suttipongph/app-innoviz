import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactTransItemComponent } from './contact-trans-item.component';

describe('ContactTransItemViewComponent', () => {
  let component: ContactTransItemComponent;
  let fixture: ComponentFixture<ContactTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ContactTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
