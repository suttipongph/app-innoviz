import { Observable, of } from 'rxjs';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { ContactTransItemView } from 'shared/models/viewModel';

const firstGroup = ['REF_TYPE', 'REF_ID'];

export class ContactTransItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: ContactTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ContactTransItemView): Observable<boolean> {
    return of(!canCreate);
  }
  setDefaultValue(model: ContactTransItemView): void {
    model.contactType = null;
  }
}
