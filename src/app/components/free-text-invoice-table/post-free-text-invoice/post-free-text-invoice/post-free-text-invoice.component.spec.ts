import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostFreeTextInvoiceComponent } from './post-free-text-invoice.component';

describe('PostFreeTextInvoiceViewComponent', () => {
  let component: PostFreeTextInvoiceComponent;
  let fixture: ComponentFixture<PostFreeTextInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PostFreeTextInvoiceComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostFreeTextInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
