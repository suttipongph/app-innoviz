import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { PostFreeTextInvoiceView } from 'shared/models/viewModel/postFreeTextInvoiceView';

const firstGroup = [
  'FREE_TEXT_INVOICE_ID',
  'CUSTOMER_ID',
  'CUSTOMER_NAME',
  'INVOICE_TYPE_ID',
  'ISSUED_DATE',
  'DUE_DATE',
  'CURRENCY_ID',
  'INVOICE_REVENUE_TYPE_ID',
  'INVOICE_AMOUNT_BEFORE_TAX',
  'TAX_AMOUNT',
  'INVOICE_AMOUNT',
  'WHT_AMOUNT'
];

export class PostFreeTextInvoiceCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: PostFreeTextInvoiceView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<PostFreeTextInvoiceView> {
    let model = new PostFreeTextInvoiceView();
    model.freeTextInvoiceTableGUID = EmptyGuid;
    return of(model);
  }
}
