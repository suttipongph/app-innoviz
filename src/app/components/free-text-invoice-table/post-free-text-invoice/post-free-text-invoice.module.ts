import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PostFreeTextInvoiceService } from './post-free-text-invoice.service';
import { PostFreeTextInvoiceComponent } from './post-free-text-invoice/post-free-text-invoice.component';
import { PostFreeTextInvoiceCustomComponent } from './post-free-text-invoice/post-free-text-invoice-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PostFreeTextInvoiceComponent],
  providers: [PostFreeTextInvoiceService, PostFreeTextInvoiceCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PostFreeTextInvoiceComponent]
})
export class PostFreeTextInvoiceComponentModule {}
