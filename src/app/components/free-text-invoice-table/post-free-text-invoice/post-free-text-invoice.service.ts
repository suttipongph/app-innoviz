import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { PostFreeTextInvoiceResultView, PostFreeTextInvoiceView } from 'shared/models/viewModel/postFreeTextInvoiceView';
@Injectable()
export class PostFreeTextInvoiceService {
  serviceKey = 'freeTextInvoiceTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getPostFreeTextInvoiceById(id: string): Observable<PostFreeTextInvoiceView> {
    const url = `${this.servicePath}/GetPostFreeTextInvoiceById/id=${id}`;
    return this.dataGateway.get(url);
  }
  postFreeTextInvoice(vmModel: PostFreeTextInvoiceView): Observable<PostFreeTextInvoiceResultView> {
    const url = `${this.servicePath}/PostFreeTextInvoice`;
    return this.dataGateway.post(url, vmModel);
  }
}
