import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CustomFreeTextInvoiceTableItemView, FreeTextInvoiceTableItemView, FreeTextInvoiceTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class FreeTextInvoiceTableService {
  serviceKey = 'freeTextInvoiceTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getFreeTextInvoiceTableToList(search: SearchParameter): Observable<SearchResult<FreeTextInvoiceTableListView>> {
    const url = `${this.servicePath}/GetFreeTextInvoiceTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteFreeTextInvoiceTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteFreeTextInvoiceTable`;
    return this.dataGateway.delete(url, row);
  }
  getFreeTextInvoiceTableById(id: string): Observable<FreeTextInvoiceTableItemView> {
    const url = `${this.servicePath}/GetFreeTextInvoiceTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createFreeTextInvoiceTable(vmModel: FreeTextInvoiceTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateFreeTextInvoiceTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateFreeTextInvoiceTable(vmModel: FreeTextInvoiceTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateFreeTextInvoiceTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getFreeTextInvoiceNumberSequence(companyId: string): Observable<any> {
    const url = `${this.servicePath}/GetFreeTextInvoiceNumberSequence/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
  getInitialCreateData(): Observable<FreeTextInvoiceTableItemView> {
    const url = `${this.servicePath}/GetInitialCreateData`;
    return this.dataGateway.get(url);
  }
  getAmount(vmModel: CustomFreeTextInvoiceTableItemView): Observable<CustomFreeTextInvoiceTableItemView> {
    const url = `${this.servicePath}/GetAmount`;
    return this.dataGateway.post(url, vmModel);
  }
}
