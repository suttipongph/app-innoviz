import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FreeTextInvoiceTableService } from './free-text-invoice-table.service';
import { FreeTextInvoiceTableListComponent } from './free-text-invoice-table-list/free-text-invoice-table-list.component';
import { FreeTextInvoiceTableItemComponent } from './free-text-invoice-table-item/free-text-invoice-table-item.component';
import { FreeTextInvoiceTableItemCustomComponent } from './free-text-invoice-table-item/free-text-invoice-table-item-custom.component';
import { FreeTextInvoiceTableListCustomComponent } from './free-text-invoice-table-list/free-text-invoice-table-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [FreeTextInvoiceTableListComponent, FreeTextInvoiceTableItemComponent],
  providers: [FreeTextInvoiceTableService, FreeTextInvoiceTableItemCustomComponent, FreeTextInvoiceTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [FreeTextInvoiceTableListComponent, FreeTextInvoiceTableItemComponent]
})
export class FreeTextInvoiceTableComponentModule {}
