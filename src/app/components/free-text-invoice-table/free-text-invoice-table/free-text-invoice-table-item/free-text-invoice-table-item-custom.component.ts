import { CustomerTableItemView } from './../../../../shared/models/viewModel/customerTableItemView';
import { AddressTransItemView } from './../../../../shared/models/viewModel/addressTransItemView';
import { Observable, of } from 'rxjs';
import { EmptyGuid, FreeTextInvoiceDocumentStatus, ProductType } from 'shared/constants';
import { isNullOrUndefined, isNullOrUndefOrEmpty, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import {
  FreeTextInvoiceTableItemView,
  InvoiceRevenueTypeItemView,
  InvoiceTableItemView,
  CurrencyItemView,
  CustomFreeTextInvoiceTableItemView
} from 'shared/models/viewModel';
import { BaseDropdownComponent } from 'core/components/base/base.dropdown.component';
import { getMaxDate, isDateFromGreaterThanDateTo, setDateString } from 'shared/functions/date.function';
import { TranslateService } from '@ngx-translate/core';
import { AppInjector } from 'app-injector';

const firstGroup = [
  'CUSTOMER_NAME',
  'DOCUMENT_STATUS_GUID',
  'EXCHANGE_RATE',
  'UNBOUND_INVOICE_ADDRESS',
  'INVOICE_ADDRESS1',
  'INVOICE_ADDRESS2',
  'TAX_BRANCH_ID',
  'TAX_BRANCH_NAME',
  'UNBOUND_MAILING_INVOICE_ADDRESS',
  'MAILING_INVOICE_ADDRESS1',
  'MAILING_INVOICE_ADDRESS2',
  'TAX_AMOUNT',
  'INVOICE_AMOUNT',
  'WHT_AMOUNT',
  'CN_REASON_GUID',
  'ORIG_INVOICE',
  'ORIG_INVOICE_ID',
  'ORIG_INVOICE_AMOUNT',
  'ORIG_TAX_INVOICE_ID',
  'ORIG_TAX_INVOICE_AMOUNT',
  'INVOICE_TABLE_GUID',
  'FREE_TEXT_INVOICE_TABLE_GUID'
];

const condition1 = ['FREE_TEXT_INVOICE_ID'];
const condition2 = ['CN_REASON_GUID', 'ORIG_INVOICE', 'ORIG_INVOICE_ID', 'ORIG_INVOICE_AMOUNT', 'ORIG_TAX_INVOICE_ID', 'ORIG_TAX_INVOICE_AMOUNT'];

export class FreeTextInvoiceTableItemCustomComponent {
  cNReasonIdRequire = false;
  originalInvoiceIdRequire = false;
  originalInvoiceAmountRequire = false;
  baseService: BaseServiceModel<any>;
  translateService = AppInjector.get(TranslateService);
  taxValueError: string = null;
  WithholdingTaxValueError: string = null;

  getFieldAccessing(model: FreeTextInvoiceTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    this.getMandatoryCondition(model);
    return new Observable((observer) => {
      // set firstGroup
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      if (isNullOrUndefOrEmptyGUID(model.freeTextInvoiceTableGUID)) {
        this.getFreeTextInvoiceNumberSequence(model.companyGUID).subscribe((result) => {
          // this.isManunal = result;
          fieldAccessing.push({ filedIds: condition1, readonly: !result });
          if (model.invoiceAmountBeforeTax < 0) {
            fieldAccessing.push({ filedIds: condition2, readonly: false });
          } else {
            fieldAccessing.push({ filedIds: condition2, readonly: true });
          }
          observer.next(fieldAccessing);
        });
      } else {
        fieldAccessing.push({ filedIds: firstGroup, readonly: true });
        fieldAccessing.push({ filedIds: condition1, readonly: true });
        if (model.documentStatus_StatusId === FreeTextInvoiceDocumentStatus.Draft) {
          if (model.invoiceAmountBeforeTax < 0) {
            fieldAccessing.push({ filedIds: condition2, readonly: false });
          } else {
            fieldAccessing.push({ filedIds: condition2, readonly: true });
          }
        }
        observer.next(fieldAccessing);
      }
    });
  }
  getMandatoryCondition(model: FreeTextInvoiceTableItemView): void {
    if (model.invoiceAmountBeforeTax < 0) {
      this.cNReasonIdRequire = true;
      this.originalInvoiceIdRequire = true;
      this.originalInvoiceAmountRequire = true;
    } else {
      this.cNReasonIdRequire = false;
      this.originalInvoiceIdRequire = false;
      this.originalInvoiceAmountRequire = false;
    }
  }
  onInvoiceAddressChange(model: FreeTextInvoiceTableItemView, addressTransOptions: SelectItems[], e: any): void {
    const rowData: AddressTransItemView = isNullOrUndefOrEmptyGUID(e)
      ? new AddressTransItemView()
      : (addressTransOptions.find((v) => v.value === e).rowData as AddressTransItemView);
    model.unboundInvoiceAddress = rowData.address1 + ' ' + rowData.address2;
    model.invoiceAddress1 = rowData.address1;
    model.invoiceAddress2 = rowData.address2;
    model.taxBranchId = rowData.taxBranchId;
    model.taxBranchName = rowData.taxBranchName;
  }
  onMailingInvoiceAddressChange(model: FreeTextInvoiceTableItemView, addressTransOptions: SelectItems[], e: any): void {
    const rowData: AddressTransItemView = isNullOrUndefOrEmptyGUID(e)
      ? new AddressTransItemView()
      : (addressTransOptions.find((v) => v.value === e).rowData as AddressTransItemView);
    model.unboundMailingInvoiceAddress = rowData.address1 + ' ' + rowData.address2;
    model.mailingInvoiceAddress1 = rowData.address1;
    model.mailingInvoiceAddress2 = rowData.address2;
  }
  setUnboundMailingAddress(model: FreeTextInvoiceTableItemView, addressTransOptions: SelectItems[]): void {
    const rowData: AddressTransItemView = isNullOrUndefOrEmptyGUID(model.mailingInvoiceAddressGUID)
      ? new AddressTransItemView()
      : (addressTransOptions.find((v) => v.value === model.mailingInvoiceAddressGUID).rowData as AddressTransItemView);
    model.unboundMailingInvoiceAddress = rowData.address1 + ' ' + rowData.address2;
  }
  setUnboundInvoiceAddress(model: FreeTextInvoiceTableItemView, addressTransOptions: SelectItems[]): void {
    const rowData: AddressTransItemView = isNullOrUndefOrEmptyGUID(model.invoiceAddressGUID)
      ? new AddressTransItemView()
      : (addressTransOptions.find((v) => v.value === model.invoiceAddressGUID).rowData as AddressTransItemView);
    model.unboundInvoiceAddress = rowData.address1 + ' ' + rowData.address2;
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: FreeTextInvoiceTableItemView): Observable<boolean> {
    let isDraftStatus = true;
    if (model.documentStatus_StatusId === FreeTextInvoiceDocumentStatus.Draft) {
      isDraftStatus = true;
    } else {
      isDraftStatus = false;
    }
    return of(!canCreate || !isDraftStatus);
  }
  getInitialData(): Observable<FreeTextInvoiceTableItemView> {
    const model = new FreeTextInvoiceTableItemView();
    model.freeTextInvoiceTableGUID = EmptyGuid;
    return of(model);
  }
  getFreeTextInvoiceNumberSequence(companyId: string): Observable<boolean> {
    return this.baseService.service.getFreeTextInvoiceNumberSequence(companyId);
  }
  getCreditAppTableByDropDown(model: FreeTextInvoiceTableItemView, baseDropdown: BaseDropdownComponent): Observable<SelectItems[]> {
    if (model.customerTableGUID === null || model.productType === null || model.issuedDate === null) {
      return of([]);
    } else {
      return baseDropdown.getCreditAppTableByDropDown(
        [model.customerTableGUID, model.productType.toString()],
        [setDateString(model.issuedDate, 0), getMaxDate()]
      );
    }
  }
  onCustomerChange(event: any, model: FreeTextInvoiceTableItemView, customerOption: SelectItems[]): void {
    this.clearField(model);
    this.setDefaultValue(event, model, customerOption);
  }
  clearField(model: FreeTextInvoiceTableItemView): any {
    model.creditAppTableGUID = null;
    model.invoiceAddressGUID = null;
    model.unboundInvoiceAddress = null;
    model.unboundMailingInvoiceAddress = null;
    model.mailingInvoiceAddressGUID = null;
    model.origInvoice = null;
    model.origInvoiceId = null;
    model.origInvoiceAmount = null;
    model.origTaxInvoiceId = null;
    model.origTaxInvoiceAmount = null;
  }
  setDefaultValue(event: any, model: FreeTextInvoiceTableItemView, customerOption: SelectItems[]): void {
    const rowData: CustomerTableItemView = isNullOrUndefOrEmptyGUID(event)
      ? new CustomerTableItemView()
      : (customerOption.find((v) => v.value === event).rowData as CustomerTableItemView);
    model.customerName = rowData.name;
    model.dimension1GUID = rowData.dimension1GUID;
    model.dimension2GUID = rowData.dimension2GUID;
    model.dimension3GUID = rowData.dimension3GUID;
    model.dimension4GUID = rowData.dimension4GUID;
    model.dimension5GUID = rowData.dimension5GUID;
    model.currencyGUID = rowData.currencyGUID;
  }
  onProductTypeChange(event: any, model: FreeTextInvoiceTableItemView, productTypeOption: SelectItems[]): void {
    model.invoiceTypeGUID = null;
    model.creditAppTableGUID = null;
    model.invoiceRevenueTypeGUID = null;
    model.origInvoice = null;
    model.origInvoiceId = null;
    model.origInvoiceAmount = null;
    model.origTaxInvoiceId = null;
    model.origTaxInvoiceAmount = null;
  }
  onIsuedDate(event: any, model: FreeTextInvoiceTableItemView): any {
    model.dueDate = event;
  }
  getExchangeRate(event: any, model: FreeTextInvoiceTableItemView, currencyOptions: SelectItems[]): void {
    const rowData: CurrencyItemView = isNullOrUndefOrEmptyGUID(event)
      ? new CurrencyItemView()
      : (currencyOptions.find((v) => v.value === event).rowData as CurrencyItemView);
    model.exchangeRate = rowData.exchangeRate_Rate;
  }
  onServiceFeeTypeChange(event: any, model: FreeTextInvoiceTableItemView, invoiceRevenueTypeOptions: SelectItems[]): void {
    const rowData: InvoiceRevenueTypeItemView = isNullOrUndefOrEmptyGUID(event)
      ? new InvoiceRevenueTypeItemView()
      : (invoiceRevenueTypeOptions.find((v) => v.value === event).rowData as InvoiceRevenueTypeItemView);
    if (!isNullOrUndefOrEmptyGUID(event)) {
      model.invoiceText = rowData.description;
      model.taxTableGUID = rowData.feeTaxGUID;
      model.withholdingTaxTableGUID = rowData.feeWHTGUID;
    }
  }
  onOriginalInvoiceChange(event: any, model: FreeTextInvoiceTableItemView, invoiceTableOptions: SelectItems[]): void {
    const rowData: InvoiceTableItemView = isNullOrUndefOrEmptyGUID(event)
      ? new InvoiceTableItemView()
      : (invoiceTableOptions.find((v) => v.value === event).rowData as InvoiceTableItemView);
    model.origInvoiceId = null;
    model.origInvoiceAmount = null;
    model.origTaxInvoiceId = null;
    model.origTaxInvoiceAmount = null;
    model.origInvoiceId = rowData.invoiceId;
    model.origInvoiceAmount = rowData.invoiceAmount;
    model.origTaxInvoiceId = rowData.invoiceId;
    model.origTaxInvoiceAmount = rowData.invoiceAmount;
  }
  onAmountBeforeTaxChange(event: any, model: FreeTextInvoiceTableItemView): void {
    if (event >= 0) {
      model.cnReasonGUID = null;
      model.origInvoice = null;
      model.origInvoiceId = null;
      model.origInvoiceAmount = null;
      model.origTaxInvoiceId = null;
      model.origTaxInvoiceAmount = null;
    }
    this.calAmount(model);
  }
  calAmount(model: FreeTextInvoiceTableItemView): void {
    this.getTaxAmount(model);
  }
  getTaxAmount(model: FreeTextInvoiceTableItemView): void {
    const customModel = new CustomFreeTextInvoiceTableItemView();
    customModel.withholdingTaxTableGUID = model.withholdingTaxTableGUID;
    customModel.taxTableGUID = model.taxTableGUID;
    customModel.issuedDate = model.issuedDate;
    customModel.invoiceAmountBeforeTax = model.invoiceAmountBeforeTax;
    this.taxValueError = null;
    this.WithholdingTaxValueError = null;
    this.baseService.service.getAmount(customModel).subscribe((result: FreeTextInvoiceTableItemView) => {
      model.taxAmount = result.taxAmount;
      model.invoiceAmount = result.invoiceAmount;
      model.whtAmount = result.whtAmount;
      this.taxValueError = result.taxValueError;
      this.WithholdingTaxValueError = result.withholdingTaxValueError;
    });
  }
  onValidateDate(model: FreeTextInvoiceTableItemView): any {
    if (isDateFromGreaterThanDateTo(model.issuedDate, model.dueDate)) {
      return {
        code: 'ERROR.DATE_CANNOT_LESS_THAN',
        parameters: [this.translateService.instant('LABEL.START_DATE'), this.translateService.instant('LABEL.END_DATE')]
      };
    } else {
      return null;
    }
  }
  onTaxCodeValidate(): any {
    if (!isNullOrUndefOrEmpty(this.taxValueError)) {
      return {
        code: 'ERROR.90007',
        parameters: [this.translateService.instant('LABEL.TAX_CODE'), this.taxValueError]
      };
    } else {
      return null;
    }
  }
  onWHDTaxCodeValidate(): any {
    if (!isNullOrUndefOrEmpty(this.WithholdingTaxValueError)) {
      return {
        code: 'ERROR.90007',
        parameters: [this.translateService.instant('LABEL.WITHHOLDING_TAX_CODE'), this.WithholdingTaxValueError]
      };
    } else {
      return null;
    }
  }
  getAddressTransByDropDown(customerTableGUID: string, refType: string): Observable<SelectItems[]> {
    if (isNullOrUndefOrEmptyGUID(customerTableGUID)) {
      return of([]);
    } else {
      return this.baseService.baseDropdown.getAddressTransByDropDown([customerTableGUID, refType]);
    }
  }
  getInvoiceTableByDropDown(model: FreeTextInvoiceTableItemView, baseDropdown: BaseDropdownComponent): Observable<SelectItems[]> {
    if (isNullOrUndefOrEmptyGUID(model.customerTableGUID) || model.productType === null) {
      return of([]);
    } else {
      return baseDropdown.getInvoiceTableByDropDown([model.customerTableGUID, model.productType.toString()]);
    }
  }
  getInvoiceRevenueTypeByDropDown(model: FreeTextInvoiceTableItemView, baseDropdown: BaseDropdownComponent): Observable<SelectItems[]> {
    if (model.productType === null) {
      return of([]);
    } else {
      return baseDropdown.getInvoiceRevenueTypeByDropDown(model.productType.toString());
    }
  }
  getInvoiceTypeByProductTypeDropDown(model: FreeTextInvoiceTableItemView, baseDropdown: BaseDropdownComponent): Observable<SelectItems[]> {
    if (model.productType === null) {
      return baseDropdown.getInvoiceTypeByProductTypeDropDown([ProductType.None.toString()]);
    } else {
      return baseDropdown.getInvoiceTypeByProductTypeDropDown([model.productType.toString(), ProductType.None.toString()]);
    }
  }
}
