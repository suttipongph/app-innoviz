import { RefType, ProductType, Dimension } from './../../../../shared/constants/enumsBusiness';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, SuspenseInvoiceType, FreeTextInvoiceStatus } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { FreeTextInvoiceTableItemView, LedgerDimensionItemView, RefIdParm } from 'shared/models/viewModel';
import { FreeTextInvoiceTableService } from '../free-text-invoice-table.service';
import { FreeTextInvoiceTableItemCustomComponent } from './free-text-invoice-table-item-custom.component';
@Component({
  selector: 'free-text-invoice-table-item',
  templateUrl: './free-text-invoice-table-item.component.html',
  styleUrls: ['./free-text-invoice-table-item.component.scss']
})
export class FreeTextInvoiceTableItemComponent extends BaseItemComponent<FreeTextInvoiceTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  addressTransOptions: SelectItems[] = [];
  creditAppTableOptions: SelectItems[] = [];
  currencyOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  documentReasonOptions: SelectItems[] = [];
  invoiceRevenueTypeOptions: SelectItems[] = [];
  invoiceTableOptions: SelectItems[] = [];
  invoiceTypeOptions: SelectItems[] = [];
  ledgerDimensionOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  taxTableOptions: SelectItems[] = [];
  withholdingTaxTableOptions: SelectItems[] = [];
  addressMailingInvoiceOptions: SelectItems[] = [];
  addressUnboundMailingInvoiceOptions: SelectItems[] = [];
  ledgerDimensionOptions1: SelectItems[] = [];
  ledgerDimensionOptions2: SelectItems[] = [];
  ledgerDimensionOptions3: SelectItems[] = [];
  ledgerDimensionOptions4: SelectItems[] = [];
  ledgerDimensionOptions5: SelectItems[] = [];
  constructor(
    private service: FreeTextInvoiceTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: FreeTextInvoiceTableItemCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.custom.baseService.baseDropdown = this.baseDropdown;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
  }
  getById(): Observable<FreeTextInvoiceTableItemView> {
    return this.service.getFreeTextInvoiceTableById(this.id);
  }
  getInitialData(): Observable<FreeTextInvoiceTableItemView> {
    return this.service.getInitialCreateData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner(result);
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: FreeTextInvoiceTableItemView): void {
    forkJoin(
      this.custom.getAddressTransByDropDown(model.customerTableGUID, RefType.Customer.toString()),
      this.custom.getCreditAppTableByDropDown(model, this.baseDropdown),
      this.baseDropdown.getCurrencyDropDown(),
      this.baseDropdown.getCustomerTableDropDown(),
      this.baseDropdown.getDocumentReasonDropDown(RefType.Invoice.toString()),
      this.custom.getInvoiceRevenueTypeByDropDown(model, this.baseDropdown),
      this.custom.getInvoiceTableByDropDown(model, this.baseDropdown),
      this.custom.getInvoiceTypeByProductTypeDropDown(model, this.baseDropdown),
      this.baseDropdown.getLedgerDimensionDropDown(),
      this.baseDropdown.getTaxTableDropDown(),
      this.baseDropdown.getWithholdingTaxTableDropDown()
    ).subscribe(
      ([
        addressTrans,
        creditAppTable,
        currency,
        customerTable,
        documentReason,
        invoiceRevenueType,
        invoiceTable,
        invoiceType,
        ledgerDimension,
        taxTable,
        withholdingTaxTable
      ]) => {
        this.addressTransOptions = addressTrans;
        this.creditAppTableOptions = creditAppTable;
        this.currencyOptions = currency;
        this.customerTableOptions = customerTable;
        this.documentReasonOptions = documentReason;
        this.invoiceRevenueTypeOptions = invoiceRevenueType;
        this.invoiceTableOptions = invoiceTable;
        this.invoiceTypeOptions = invoiceType;
        this.setLedgerDimension(ledgerDimension);
        this.taxTableOptions = taxTable;
        this.withholdingTaxTableOptions = withholdingTaxTable;

        if (!isNullOrUndefined(model)) {
          this.setUnboundField(model);
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setUnboundField(model: FreeTextInvoiceTableItemView): void {
    this.custom.setUnboundMailingAddress(model, this.addressTransOptions);
    this.custom.setUnboundInvoiceAddress(model, this.addressTransOptions);
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  setRelatedInfoOptions(): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        command: () => {
          const refIdParm: RefIdParm = { refType: RefType.FreeTextInvoice, refGUID: this.model.freeTextInvoiceTableGUID };
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: refIdParm
          });
        }
      },
      { label: 'LABEL.INVOICE', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.INVOICE_TABLE }) },
      { label: 'LABEL.TAX_INVOICE', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.TAX_INVOICE }) }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(model?: FreeTextInvoiceTableItemView): void {
    this.functionItems = [
      {
        label: 'LABEL.POST',
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.POST_FREE_TEXT_INVOICE }),
        disabled: !(model.documentStatus_StatusId == FreeTextInvoiceStatus.Draft)
      }
    ];
    super.setFunctionOptionsMapping();
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateFreeTextInvoiceTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createFreeTextInvoiceTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }

  onInvoiceAddressChange(e: any): void {
    this.custom.onInvoiceAddressChange(this.model, this.addressTransOptions, e);
  }
  onMailingInvoiceAddressChange(e: any): void {
    this.custom.onMailingInvoiceAddressChange(this.model, this.addressTransOptions, e);
  }
  onCustomerChange(event: string): void {
    this.custom.onCustomerChange(event, this.model, this.customerTableOptions);
    this.getNewOptionByCustomer(this.model);
    this.custom.getExchangeRate(this.model.currencyGUID, this.model, this.currencyOptions);
  }
  getNewOptionByCustomer(model?: FreeTextInvoiceTableItemView): void {
    forkJoin(
      this.custom.getAddressTransByDropDown(model.customerTableGUID, RefType.Customer.toString()),
      this.custom.getCreditAppTableByDropDown(model, this.baseDropdown),
      this.custom.getInvoiceTableByDropDown(model, this.baseDropdown)
    ).subscribe(
      ([addressTrans, creditAppTable, invoiceTable]) => {
        this.addressTransOptions = addressTrans;
        this.creditAppTableOptions = creditAppTable;
        this.invoiceTableOptions = invoiceTable;
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  onProductTypeChange(event: any): void {
    this.custom.onProductTypeChange(event, this.model, this.productTypeOptions);
    this.getNewOptionProductType(this.model);
  }
  getNewOptionProductType(model?: FreeTextInvoiceTableItemView): void {
    forkJoin(
      this.custom.getInvoiceRevenueTypeByDropDown(model, this.baseDropdown),
      this.custom.getInvoiceTypeByProductTypeDropDown(model, this.baseDropdown),
      this.custom.getCreditAppTableByDropDown(model, this.baseDropdown),
      this.custom.getInvoiceTableByDropDown(model, this.baseDropdown)
    ).subscribe(
      ([invoiceRevenueType, invoiceType, creditAppTable, invoiceTable]) => {
        this.invoiceRevenueTypeOptions = invoiceRevenueType;
        this.invoiceTypeOptions = invoiceType;
        this.creditAppTableOptions = creditAppTable;
        this.invoiceTableOptions = invoiceTable;
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  onIsuedDate(event: string): void {
    this.getNewOptionIsuedDate(this.model);
    this.custom.onIsuedDate(event, this.model);
  }
  getNewOptionIsuedDate(model?: FreeTextInvoiceTableItemView): void {
    forkJoin(this.custom.getCreditAppTableByDropDown(model, this.baseDropdown)).subscribe(
      ([creditAppTable]) => {
        this.creditAppTableOptions = creditAppTable;
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  onCurrencyChange(event: any): void {
    this.custom.getExchangeRate(event, this.model, this.currencyOptions);
  }
  onServiceFeeTypeChange(event: any): void {
    this.custom.onServiceFeeTypeChange(event, this.model, this.invoiceRevenueTypeOptions);
  }
  onOriginalInvoiceChange(event: any): void {
    this.custom.onOriginalInvoiceChange(event, this.model, this.invoiceTableOptions);
  }
  onAmountBeforeTaxChange(event: any): void {
    this.custom.onAmountBeforeTaxChange(event, this.model);
    this.setFieldAccessing();
  }
  onTaxCodeChange(event: any): void {
    this.custom.onAmountBeforeTaxChange(event, this.model);
    this.setFieldAccessing();
  }
  onWHDTaxCodeChange(event: any): void {
    this.custom.onAmountBeforeTaxChange(event, this.model);
    this.setFieldAccessing();
  }
  onValidateDate(): any {
    return this.custom.onValidateDate(this.model);
  }
  onWHDTaxCodeValidate(): any {
    return this.custom.onWHDTaxCodeValidate();
  }
  onTaxCodeValidate(): any {
    return this.custom.onTaxCodeValidate();
  }
  setLedgerDimension(ledgerDimension: SelectItems[]): void {
    this.ledgerDimensionOptions1 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension1);
    this.ledgerDimensionOptions2 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension2);
    this.ledgerDimensionOptions3 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension3);
    this.ledgerDimensionOptions4 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension4);
    this.ledgerDimensionOptions5 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension5);
  }
}
