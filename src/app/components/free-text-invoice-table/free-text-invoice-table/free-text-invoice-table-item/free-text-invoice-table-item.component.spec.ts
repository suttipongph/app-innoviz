import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreeTextInvoiceTableItemComponent } from './free-text-invoice-table-item.component';

describe('FreeTextInvoiceTableItemViewComponent', () => {
  let component: FreeTextInvoiceTableItemComponent;
  let fixture: ComponentFixture<FreeTextInvoiceTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FreeTextInvoiceTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreeTextInvoiceTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
