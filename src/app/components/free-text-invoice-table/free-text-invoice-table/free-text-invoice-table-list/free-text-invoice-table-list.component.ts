import { makeStateKey } from '@angular/platform-browser';
import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { FreeTextInvoiceTableListView } from 'shared/models/viewModel';
import { FreeTextInvoiceTableService } from '../free-text-invoice-table.service';
import { FreeTextInvoiceTableListCustomComponent } from './free-text-invoice-table-list-custom.component';

@Component({
  selector: 'free-text-invoice-table-list',
  templateUrl: './free-text-invoice-table-list.component.html',
  styleUrls: ['./free-text-invoice-table-list.component.scss']
})
export class FreeTextInvoiceTableListComponent extends BaseListComponent<FreeTextInvoiceTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  addressTransOptions: SelectItems[] = [];
  creditAppTableOptions: SelectItems[] = [];
  currencyOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  documentReasonOptions: SelectItems[] = [];
  invoiceRevenueTypeOptions: SelectItems[] = [];
  invoiceTableOptions: SelectItems[] = [];
  invoiceTypeOptions: SelectItems[] = [];
  ledgerDimensionOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  taxTableOptions: SelectItems[] = [];
  withholdingTaxTableOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];

  constructor(public custom: FreeTextInvoiceTableListCustomComponent, private service: FreeTextInvoiceTableService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCreditAppTableDropDown(this.creditAppTableOptions);
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getInvoiceTypeDropDown(this.invoiceTypeOptions);
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'freeTextInvoiceTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.FREE_TEXT_INVOICE_ID',
        textKey: 'freeTextInvoiceId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        sortingKey: 'customerTable_CustomerId',
        searchingKey: 'customerTableGUID',
        masterList: this.customerTableOptions
      },
      {
        label: 'LABEL.CREDIT_APPLICATION_ID',
        textKey: 'creditAppTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        sortingKey: 'creditAppTable_CreditAppId',
        searchingKey: 'creditAppTableGUID',
        masterList: this.creditAppTableOptions
      },
      {
        label: 'LABEL.ISSUED_DATE',
        textKey: 'issuedDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.INVOICE_TYPE_ID',
        textKey: 'invoiceType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        sortingKey: 'invoiceType_InvoiceTypeId',
        searchingKey: 'invoiceTypeGUID',
        masterList: this.invoiceTypeOptions
      },
      {
        label: 'LABEL.INVOICE_AMOUNT',
        textKey: 'invoiceAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentStatusOptions,
        searchingKey: 'documentStatusGUID',
        sortingKey: 'documentStatus_StatusId'
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<FreeTextInvoiceTableListView>> {
    return this.service.getFreeTextInvoiceTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteFreeTextInvoiceTable(row));
  }

  getRowAuthorize(row: FreeTextInvoiceTableListView[]): void {
    if (this.custom.getRowAuthorize(row)) {
      row.forEach((item) => {
        item.rowAuthorize = this.getSingleRowAuthorize(item.rowAuthorize, item);
      });
    } else {
      super.getRowAuthorize(row);
    }
  }
}
