import { Observable } from 'rxjs';
import { AccessMode, FreeTextInvoiceDocumentStatus } from 'shared/constants';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView, FreeTextInvoiceTableListView } from 'shared/models/viewModel';

export class FreeTextInvoiceTableListCustomComponent {
  accessModeView: AccessModeView = new AccessModeView();

  baseService: BaseServiceModel<any>;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  getRowAuthorize(row: FreeTextInvoiceTableListView[]): boolean {
    const isSetRow: boolean = true;

    row.forEach((item) => {
      const accessMode: AccessMode = item.documentStatus_StatusId === FreeTextInvoiceDocumentStatus.Draft ? AccessMode.full : AccessMode.viewer;
      item.rowAuthorize = accessMode;
    });
    return isSetRow;
  }
}
