import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FreeTextInvoiceTableListComponent } from './free-text-invoice-table-list.component';

describe('FreeTextInvoiceTableListViewComponent', () => {
  let component: FreeTextInvoiceTableListComponent;
  let fixture: ComponentFixture<FreeTextInvoiceTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FreeTextInvoiceTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreeTextInvoiceTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
