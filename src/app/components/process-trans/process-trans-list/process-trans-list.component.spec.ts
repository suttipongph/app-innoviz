import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProcessTransListComponent } from './process-trans-list.component';

describe('ProcessTransListViewComponent', () => {
  let component: ProcessTransListComponent;
  let fixture: ComponentFixture<ProcessTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProcessTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
