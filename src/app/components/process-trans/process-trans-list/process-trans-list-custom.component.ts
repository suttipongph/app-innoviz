import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView } from 'shared/models/viewModel';
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const  CREDIT_APP_TABLE = 'creditapptable';

export class ProcessTransListCustomComponent {
  baseService: BaseServiceModel<any>;
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setAccessModeByParentStatus(parentId: string): Observable<AccessModeView> {
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        return this.baseService.service.getAccessModeByCreditAppRequestTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
          })
        );
      case CREDIT_APP_TABLE:
        this.accessModeView.canCreate = false;
        this.accessModeView.canDelete = false;
        this.accessModeView.canView = true;
        return of(this.accessModeView);
      default:
        return of(this.accessModeView);
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }

}
