import { REF_TYPE } from './../../../shared/constants/enumConstant';
import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { selector } from 'rxjs-compat/operator/publish';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { ProcessTransListView } from 'shared/models/viewModel';
import { ProcessTransService } from '../process-trans.service';
import { ProcessTransListCustomComponent } from './process-trans-list-custom.component';

@Component({
  selector: 'process-trans-list',
  templateUrl: './process-trans-list.component.html',
  styleUrls: ['./process-trans-list.component.scss']
})
export class ProcessTransListComponent extends BaseListComponent<ProcessTransListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  productTypeOptions: SelectItems[] = [];
  reftypeOption: SelectItems[] = [];
  stagingBatchStatus: SelectItems[] = [];
  processTransType: SelectItems[] = [];
  defualtBitOption: SelectItems[] = [];
  customerTableOption: SelectItems[] = [];

  creditApplicationOption: SelectItems[] = [];
  invoiceNumberOption: SelectItems[] = [];
  stagingNumberOption: SelectItems[] = [];

  constructor(public custom: ProcessTransListCustomComponent, private service: ProcessTransService, public uiControllerService: UIControllerService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    // this.custom.setAccessModeByParentStatus(this.parentId).subscribe((result) => {
    //   this.setDataGridOption();
    // });
    this.setDataGridOption();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.reftypeOption);
    this.baseDropdown.getStagingBatchStatusEnumDropDown(this.stagingBatchStatus);
    this.baseDropdown.getProcessTransTypeEnumDropDown(this.processTransType);
    this.baseDropdown.getDefaultBitDropDown(this.defualtBitOption);
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOption);
    this.baseDropdown.getCreditAppTableDropDown(this.creditApplicationOption);
    this.baseDropdown.getInvoiceTableDropDown(this.invoiceNumberOption);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = true;
    console.log(this.getCanView());
    this.option.canDelete = false;
    this.option.key = 'processTransGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOption,
        sortingKey: 'customerTable_CustomerId',
        searchingKey: 'customerTableGUID'
      },
      {
        label: 'LABEL.CREDIT_APPLICATION_ID',
        textKey: 'creditAppTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditApplicationOption,
        sortingKey: 'creditAppTable_CreditappId',
        searchingKey: 'creditAppTableGUID'
      },
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: 'LABEL.TRANSACTION_DATE',
        textKey: 'transDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.TRANSACTION_TYPE',
        textKey: 'processTransType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.processTransType
      },
      {
        label: 'LABEL.AMOUNT',
        textKey: 'amount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.TAX_AMOUNT',
        textKey: 'taxAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.REF_TYPE',
        textKey: 'refType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.reftypeOption
      },
      {
        label: 'LABEL.REF_ID',
        textKey: 'refId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.INVOICE_TABLE_GUID',
        textKey: 'invoiceTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.invoiceNumberOption,
        sortingKey: 'invoiceTable_InvoiceId',
        searchingKey: 'invoiceTableGUID'
      },
      {
        label: 'LABEL.STAGING_BATCH_ID',
        textKey: 'stagingBatchId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.stagingNumberOption
      },
      {
        label: 'LABEL.STAGING_BATCH_STATUS',
        textKey: 'stagingBatchStatus',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.stagingBatchStatus
      },
      {
        label: 'LABEL.REVERT',
        textKey: 'revert',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defualtBitOption
      },
      {
        label: 'LABEL.REF_GUID',
        textKey: 'refGUID',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<ProcessTransListView>> {
    return this.service.getProcessTransToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteProcessTrans(row));
  }
}
