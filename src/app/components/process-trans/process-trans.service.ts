import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ProcessTransItemView, ProcessTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ProcessTransService {
  serviceKey = 'processTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getProcessTransToList(search: SearchParameter): Observable<SearchResult<ProcessTransListView>> {
    const url = `${this.servicePath}/GetProcessTransList/${search.branchFilterMode}`;
    console.log(url);
    return this.dataGateway.getList(url, search);
  }
  deleteProcessTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteProcessTrans`;
    return this.dataGateway.delete(url, row);
  }
  getProcessTransById(id: string): Observable<ProcessTransItemView> {
    const url = `${this.servicePath}/GetProcessTransById/id=${id}`;
    console.log(this.servicePath);
    return this.dataGateway.get(url);
  }
  createProcessTrans(vmModel: ProcessTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateProcessTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateProcessTrans(vmModel: ProcessTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateProcessTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
