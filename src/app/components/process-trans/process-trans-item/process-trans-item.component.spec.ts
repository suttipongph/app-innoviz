import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessTransItemComponent } from './process-trans-item.component';

describe('ProcessTransItemViewComponent', () => {
  let component: ProcessTransItemComponent;
  let fixture: ComponentFixture<ProcessTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProcessTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
