import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { ProcessTransItemView } from 'shared/models/viewModel';
import { ProcessTransService } from '../process-trans.service';

const firstGroup = [
  'CUSTOMER_TABLE_GUID',
  'CREDIT_APP_TABLE_GUID',
  'PRODUCT_TYPE',
  'TRANS_DATE',
  'PROCESS_TRANS_TYPE',
  'AR_LEDGER_ACCOUNT',
  'AMOUNT',
  'AMOUNT_MST',
  'TAX_AMOUNT',
  'TAX_AMOUNT_MST',
  'CURRENCY_GUID',
  'EXCHANGE_RATE',
  'DOCUMENT_REASON_GUID',
  'REF_TAX_INVOICE_GUID',
  'TAX_TABLE_GUID',
  'ORIG_TAX_TABLE_GUID',
  'REF_TYPE',
  'REF_GUID',
  'REF_ID',
  'DOCUMENT_ID',
  'PAYMENT_HISTORY_GUID',
  'REVERT',
  'REF_PROCESS_TRANS_GUID',
  'STAGING_BATCH_ID',
  'STAGING_BATCH_STATUS',
  'PROCESS_TRANS_GUID',
  'PAYMENT_DETAIL_GUID'
];

export class ProcessTransItemCustomComponent {
  baseService: BaseServiceModel<ProcessTransService>;
  getFieldAccessing(model: ProcessTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ProcessTransItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(): Observable<ProcessTransItemView> {
    let model = new ProcessTransItemView();
    model.processTransGUID = EmptyGuid;
    return of(model);
  }
}
