import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProcessTransService } from './process-trans.service';
import { ProcessTransListComponent } from './process-trans-list/process-trans-list.component';
import { ProcessTransItemComponent } from './process-trans-item/process-trans-item.component';
import { ProcessTransItemCustomComponent } from './process-trans-item/process-trans-item-custom.component';
import { ProcessTransListCustomComponent } from './process-trans-list/process-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ProcessTransListComponent, ProcessTransItemComponent],
  providers: [ProcessTransService, ProcessTransItemCustomComponent, ProcessTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ProcessTransListComponent, ProcessTransItemComponent]
})
export class ProcessTransComponentModule {}
