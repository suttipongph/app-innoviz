import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreditAppRequestTableAmendService } from './credit-app-request-table-amend.service';
import { CreditAppRequestTableAmendListComponent } from './credit-app-request-table-amend-list/credit-app-request-table-amend-list.component';
import { CreditAppRequestTableAmendItemComponent } from './credit-app-request-table-amend-item/credit-app-request-table-amend-item.component';
import { CreditAppRequestTableAmendItemCustomComponent } from './credit-app-request-table-amend-item/credit-app-request-table-amend-item-custom.component';
import { CreditAppRequestTableAmendListCustomComponent } from './credit-app-request-table-amend-list/credit-app-request-table-amend-list-custom.component';
import { AuthorizedPersonTransComponentModule } from 'components/authorized-person-trans/authorized-person-trans.module';
import { GuarantorTransComponentModule } from 'components/guarantor-trans/guarantor-trans/guarantor-trans.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, AuthorizedPersonTransComponentModule, GuarantorTransComponentModule],
  declarations: [CreditAppRequestTableAmendListComponent, CreditAppRequestTableAmendItemComponent],
  providers: [CreditAppRequestTableAmendService, CreditAppRequestTableAmendItemCustomComponent, CreditAppRequestTableAmendListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CreditAppRequestTableAmendListComponent, CreditAppRequestTableAmendItemComponent]
})
export class CreditAppRequestTableAmendComponentModule {}
