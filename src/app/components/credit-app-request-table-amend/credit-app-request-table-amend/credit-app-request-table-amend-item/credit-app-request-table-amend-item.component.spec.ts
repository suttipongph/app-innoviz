import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditAppRequestTableAmendItemComponent } from './credit-app-request-table-amend-item.component';

describe('CreditAppRequestTableAmendItemViewComponent', () => {
  let component: CreditAppRequestTableAmendItemComponent;
  let fixture: ComponentFixture<CreditAppRequestTableAmendItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditAppRequestTableAmendItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppRequestTableAmendItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
