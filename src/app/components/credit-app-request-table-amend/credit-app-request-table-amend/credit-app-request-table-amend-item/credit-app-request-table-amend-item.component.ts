import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { ROUTE_RELATED_GEN, CreditAppRequestType, RefType, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { getDropDownLabel, isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { CreditAppRequestTableAmendItemView, RefIdParm } from 'shared/models/viewModel';
import { PrintSetBookDocTransView } from 'shared/models/viewModel/printSetBookDocTransView';
import { CreditAppRequestTableAmendService } from '../credit-app-request-table-amend.service';
import { CreditAppRequestTableAmendItemCustomComponent } from './credit-app-request-table-amend-item-custom.component';
@Component({
  selector: 'credit-app-request-table-amend-item',
  templateUrl: './credit-app-request-table-amend-item.component.html',
  styleUrls: ['./credit-app-request-table-amend-item.component.scss']
})
export class CreditAppRequestTableAmendItemComponent extends BaseItemComponent<CreditAppRequestTableAmendItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  creditAppRequestTypeOptions: SelectItems[] = [];
  interestTypeOptions: SelectItems[] = [];
  purchaseFeeCalculateBaseOptions: SelectItems[] = [];
  blacklistStatusGUIDOptions: SelectItems[] = [];
  creditScoringGUIDOptions: SelectItems[] = [];
  kycguidOptions: SelectItems[] = [];
  documentReasonOptions: SelectItems[] = [];
  invoiceTableOptions: SelectItems[] = [];
  constructor(
    private service: CreditAppRequestTableAmendService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CreditAppRequestTableAmendItemCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {
    this.baseDropdown.getCreditAppRequestTypeEnumDropDown(this.creditAppRequestTypeOptions, [
      CreditAppRequestType.ActiveAmendCustomerCreditLimit,
      CreditAppRequestType.AmendCustomerInfo
    ]);
    this.baseDropdown.getPurchaseFeeCalculateBaseEnumDropDown(this.purchaseFeeCalculateBaseOptions);
  }
  getById(): Observable<CreditAppRequestTableAmendItemView> {
    return this.service.getCreditAppRequestTableAmendById(this.id);
  }
  getInitialData(): Observable<CreditAppRequestTableAmendItemView> {
    this.custom.setFirstBinding();
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions(result);
        this.setFunctionOptions(result);
        this.setWorkflowOption(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }
  onAsyncRunner(model?: CreditAppRequestTableAmendItemView): void {
    forkJoin(
      this.baseDropdown.getInterestTypeDropDown(),
      this.baseDropdown.getCreditScoringDropDown(),
      this.baseDropdown.getBlacklistStatusDropDown(),
      this.baseDropdown.getKYCSetupDropDown(),
      this.baseDropdown.getOriginalInvoiceDropDown(model?.creditAppRequestTableGUID),
      this.baseDropdown.getDocumentReasonDropDown(RefType.Invoice.toString())
    ).subscribe(
      ([interestType, creditScoring, blacklistStatus, kycguid, originalInvoice, documentReason]) => {
        this.interestTypeOptions = interestType;
        this.creditScoringGUIDOptions = creditScoring;
        this.blacklistStatusGUIDOptions = blacklistStatus;
        this.kycguidOptions = kycguid;
        this.invoiceTableOptions = originalInvoice;
        this.documentReasonOptions = documentReason;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          this.custom.setFirstAmend();
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model, this.isWorkFlowMode()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  setRelatedInfoOptions(model: CreditAppRequestTableAmendItemView): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        command: () => {
          const refIdParm: RefIdParm = { refType: RefType.CreditAppRequestTable, refGUID: this.model.creditAppRequestTableGUID };
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: refIdParm
          });
        }
      },
      {
        label: 'LABEL.MEMO',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.MEMO,
            parameters: {
              refGUID: this.model.creditAppRequestTableGUID,
              refType: RefType.CreditAppRequestTable
            }
          })
      },
      {
        label: 'LABEL.BOOKMARK_DOCUMENT',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.BOOKMARK_DOCUMENT_TRANS,
            parameters: {
              refGUID: this.model.creditAppRequestTableGUID,
              refType: RefType.CreditAppRequestTable
            }
          })
      },
      {
        label: 'LABEL.MANAGE',
        items: [
          {
            label: 'LABEL.ASSIGNMENT',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.ASSIGNMENT,
                parameters: {
                  creditAppRequestTable_Values: getDropDownLabel(
                    this.model.creditAppRequestTable_CreditAppRequestId,
                    this.model.creditAppRequestTable_Description
                  ),
                  customerTable_Values: this.model.customerTable_Values,
                  customerTableGUID: this.model.creditAppRequestTable_CustomerTableGUID,
                  creditAppRequestTableGUID: this.model.creditAppRequestTableGUID
                }
              })
          },
          {
            label: 'LABEL.BUSINESS_COLLATERAL',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.BUSINESS_COLLATERAL,
                parameters: {
                  customerTableGUID: this.model.creditAppRequestTable_CustomerTableGUID,
                  creditAppRequestTableGUID: this.model.creditAppRequestTableGUID,
                  creditAppRequestTable_Values: getDropDownLabel(
                    this.model.creditAppRequestTable_CreditAppRequestId,
                    this.model.creditAppRequestTable_Description
                  ),
                  customerTable_Values: this.model.customerTable_Values
                }
              })
          },
          {
            label: 'LABEL.SERVICE_FEE_CONDITION',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.SERVICE_FEE_CONDITION_TRANS,
                parameters: { refGUID: model.creditAppRequestTableGUID }
              })
          }
        ]
      },
      {
        label: 'LABEL.FINANCIAL',
        items: [
          {
            label: 'LABEL.FINANCIAL_CREDIT',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.FINANCIAL_CREDIT,
                parameters: { refGUID: this.model.creditAppRequestTableGUID, refType: RefType.CreditAppRequestTable }
              })
          },
          {
            label: 'LABEL.FINANCIAL_STATEMENT',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.FINANCIAL_STATEMENT_TRANS,
                parameters: { refGUID: this.model.creditAppRequestTableGUID, refType: RefType.CreditAppRequestTable }
              })
          },
          {
            label: 'LABEL.NCB_TRANS',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.NCB_TRANS,
                parameters: { refGUID: this.model.creditAppRequestTableGUID, refType: RefType.CreditAppRequestTable }
              })
          },
          {
            label: 'LABEL.TAX_REPORT',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.TAX_REPORT_TRANS,
                parameters: { refGUID: this.model.creditAppRequestTableGUID, refType: RefType.CreditAppRequestTable }
              })
          }
        ]
      },
      {
        label: 'LABEL.INQUIRY',
        items: [
          {
            label: 'LABEL.ASSIGNMENT_AGREEMENT_OUTSTANDING',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.ASSIGNMENT_AGREEMENT_OUTSTANDING,
                parameters: { creditAppRequestTableGUID: this.model.creditAppRequestTableGUID }
              })
          },
          {
            label: 'LABEL.CA_BUYER_CREDIT_OUTSTANDING',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.CA_BUYER_CREDIT_OUTSTANDING,
                parameters: { creditAppRequestTableGUID: this.model.creditAppRequestTableGUID }
              })
          },
          {
            label: 'LABEL.CREDIT_OUTSTANDING',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.CREDIT_OUTSTANDING,
                parameters: { creditAppRequestTableGUID: this.model.creditAppRequestTableGUID }
              })
          },
          {
            label: 'LABEL.RETENTION_OUTSTANDING',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.RETENTION_OUTSTANDING,
                parameters: { creditAppRequestTableGUID: this.model.creditAppRequestTableGUID }
              })
          }
        ]
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(model: CreditAppRequestTableAmendItemView): void {
    this.functionItems = [
      {
        label: 'LABEL.CANCEL_CREDIT_APPLICATION_REQUEST',
        disabled: !this.custom.statusIsDraft(model),
        command: () =>
          this.toFunction({
            path: ROUTE_FUNCTION_GEN.CANCEL_CREDIT_APPLICATION_REQUEST,
            parameters: { creditAppRequestGUID: model.creditAppRequestTableGUID }
          })
      },
      {
        label: 'LABEL.PRINT',
        items: [
          {
            label: 'LABEL.BOOKMARK_DOCUMENT',
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.PRINT_SET_BOOKMARK_DOCUMENT_TRANSACTION,
                parameters: {
                  // refType: RefType.BusinessCollateralAgreement,
                  // refGuid: this.id,
                  model: this.setPrintSetBookDocTransData()
                }
              })
          }
        ]
      }
    ];
    super.setFunctionOptionsMapping();
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateCreditAppRequestTableAmend(this.model), isColsing);
    } else {
      super.onCreate(this.service.createCreditAppRequestTableAmend(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onAmendRateChange(): void {
    this.custom.onAmendRateChange(this.model).subscribe((result) => {
      super.setBaseFieldAccessing(result);
    });
  }
  onAmendAuthorizedPersonChange(): void {
    this.custom.onAmendAuthorizedPersonChange(this.model);
  }
  onAmendAuthorizedPersonBinding(): void {
    this.custom.onAmendAuthorizedPersonBinding(this.model);
  }
  onAmendGuarantorChange(): void {
    this.custom.onAmendGuarantorChange(this.model);
  }
  onAmendGuarantorBinding(): void {
    this.custom.onAmendGuarantorBinding(this.model);
  }
  onAmendRateBinding(): void {
    this.custom.onAmendRateBinding(this.model);
  }
  onNewCreditLimitChange(): void {
    super.setBaseFieldAccessing(this.custom.onNewCreditLimitChange(this.model));
  }
  onRequestDateChange(): void {
    this.custom.getInterestTypeValueByCreditAppRequestAmend(this.model);
    this.custom.GetCustomerCreditLimit(this.model);
    super.setBaseFieldAccessing(this.custom.isRequestDateGraterThanEqualInActiveDate(this.model));
  }
  onNewInterestTypeChange(): void {
    this.custom.getInterestTypeValueByCreditAppRequestAmend(this.model);
  }
  onNewInterestAdjustmentPctChange(): void {
    this.custom.calcNewTotalInterestPct(this.model);
  }
  onNewMaxRetentionPctChange(): void {
    this.custom.onNewMaxRetentionPctChange(this.model);
  }
  setWorkflowOption(model: CreditAppRequestTableAmendItemView): void {
    super.setK2WorkflowOption(model, this.custom.getWorkFlowPath(model), this.custom.getDisabledWorkFlow(model));
  }
  setPrintSetBookDocTransData(): PrintSetBookDocTransView {
    return this.custom.setPrintSetBookDocTransData(this.model);
  }
  onApprovedCreditLimitRequestChange(): void {
    super.setBaseFieldAccessing(this.custom.onApprovedCreditLimitRequestChange(this.model));
  }
  onOrigInvoiceChange(): void {
    this.custom.onOrigInvoiceChange(this.model, this.invoiceTableOptions);
  }
  onCreditLimitRequestFeeAmountChange(): void {
    super.setBaseFieldAccessing(this.custom.onCreditLimitRequestFeeAmountChange(this.model));
  }
}
