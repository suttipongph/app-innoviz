import { Confirmation } from 'primeng/api';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConst, CreditAppRequestStatus, CreditAppRequestType, EmptyGuid, ProductType, RefType, WORKFLOW } from 'shared/constants';
import { isDateFromGreaterThanDateTo, isDateFromGreaterThanEqualDateTo } from 'shared/functions/date.function';
import { toMapModel } from 'shared/functions/model.function';
import { getProductType, isNullOrUndefOrEmpty, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, PathParamModel, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, CreditAppRequestTableAmendItemView, CreditAppRequestTableItemView, InvoiceTableItemView } from 'shared/models/viewModel';
import { PrintSetBookDocTransView } from 'shared/models/viewModel/printSetBookDocTransView';
import { CreditAppRequestTableAmendService } from '../credit-app-request-table-amend.service';

const firstGroup = [
  'DOCUMENT_STATUS',
  'CUSTOMER_ID',
  'CUSTOMER_CREDIT_LIMIT',
  'START_DATE',
  'EXPIRY_DATE',
  'REF_CREDIT_APP_ID',
  'ORIGINAL_CREDIT_LIMIT',
  'CREDIT_LIMIT_REQUEST_FEE_PCT',
  'MAX_RETENTION_PCT',
  'APPROVED_CREDIT_LIMIT',
  'ORIGINAL_INTEREST_TYPE_GUID',
  'ORIGINAL_INTEREST_ADJUSTMENT_PCT',
  'ORIGINAL_TOTAL_INTEREST_PCT',
  'ORIGINAL_CREDIT_LIMIT_REQUEST_FEE_AMOUNT',
  'ORIGINAL_MAX_RETENTION_PCT',
  'ORIGINAL_MAX_RETENTION_AMOUNT',
  'ORIGINAL_MAX_PURCHASE_PCT',
  'ORIGINAL_PURCHASE_FEE_PCT',
  'ORIGINAL_PURCHASE_FEE_CALCULATE_BASE',
  'APPROVED_DATE',
  'CREDIT_COMMENT',
  'APPROVER_COMMENT',
  'NEW_TOTAL_INTEREST_PCT'
];
const CONDITION2 = ['CREDIT_APP_REQUEST_TYPE'];
const READONLY_WHEN_UPDATE = ['CREDIT_APP_REQUEST_TYPE', 'CREDIT_APP_REQUEST_ID'];
const messageCopy = 'CONFIRM.90006';
const messageClear = 'CONFIRM.90007';
const labelAmendRate = 'LABEL.AMEND_RATE';
const labelAmendAuthorizedPerson = 'LABEL.AMEND_AUTHORIZED_PERSON';
const labelAmendGuarantor = 'LABEL.AMEND_GUARANTOR';
const rateGroup = [
  'NEW_INTEREST_TYPE_GUID',
  'NEW_INTEREST_ADJUSTMENT_PCT',
  'CREDIT_LIMIT_REQUEST_FEE_PCT',
  'NEW_CREDIT_LIMIT_REQUEST_FEE_AMOUNT',
  'NEW_MAX_RETENTION_PCT',
  'NEW_MAX_RETENTION_AMOUNT',
  'NEW_MAX_PURCHASE_PCT',
  'NEW_PURCHASE_FEE_CALCULATE_BASE'
];
const NUMBER_SEQ = ['CREDIT_APP_REQUEST_ID'];
const CONDITION3 = ['CREDIT_COMMENT'];
const CONDITION4 = ['CREDIT_COMMENT', 'APPROVER_COMMENT', 'APPROVED_CREDIT_LIMIT', 'APPROVED_DATE'];
const CONDITION4_1_AND_CONDITION5_1 = ['CREDIT_LIMIT_REQUEST_FEE_AMOUNT', 'MAX_RETENTION_AMOUNT'];
const CONDITION5 = ['APPROVER_COMMENT', 'APPROVED_CREDIT_LIMIT', 'APPROVED_DATE'];
const AMEND_RATE_CONDTION1 = ['NEW_MAX_PURCHASE_PCT', 'NEW_PURCHASE_FEE_PCT', 'NEW_PURCHASE_FEE_CALCULATE_BASE'];
const CREDIT_NOTE_REFERENCE_GROUP = [
  'CN_REASON_GUID',
  'ORIG_INVOICE',
  'ORIG_INVOICE_ID',
  'ORIG_INVOICE_AMOUNT',
  'ORIG_TAX_INVOICE_ID',
  'ORIG_TAX_INVOICE_AMOUNT'
];
const AMEND_CA = 'amendca';

export class CreditAppRequestTableAmendItemCustomComponent {
  baseService: BaseServiceModel<any>;
  isActiveAmendCustomerCreditLimit: boolean = true;
  isAmendCustomerInfo: boolean = false;
  checkAmendRate: boolean = false;
  checkAmendAuthorizedPerson: boolean = false;
  checkAmendGuarantor: boolean = false;
  productType: ProductType = null;
  isManual: boolean = false;
  accessModeView: AccessModeView = new AccessModeView();
  parentName: string;
  currentName: string;
  interestTypeValue: number = 0;
  activeIndex: number = 0;
  mandatoryWhenCreditLimitRequestFeeAmountIsLessThanZero: boolean = false;
  firstBinding: boolean = false;
  openAuthorizedPersonList: boolean = false;
  authorizedPersonChange: boolean = false;
  openGuarantorList: boolean = false;
  guarantorChange: boolean = false;

  getFieldAccessing(model: CreditAppRequestTableAmendItemView): Observable<FieldAccessing[]> {
    const masterRoute = this.baseService.uiService.getMasterRoute();
    this.productType = Number(getProductType(masterRoute));
    this.setGroupControl(model);
    const fieldAccessing: FieldAccessing[] = [];
    return new Observable((observer) => {
      if (!this.canActiveByStatus(model.documentStatus_StatusId)) {
        observer.next(fieldAccessing);
        return;
      }
      fieldAccessing.push(...this.setFieldAccessingByStatus(model));
      if (!isUpdateMode(model.creditAppRequestTableAmendGUID)) {
        if (isDateFromGreaterThanEqualDateTo(model.creditAppRequestTable_RequestDate, model.creditAppTable_InactiveDate)) {
          fieldAccessing.push({ filedIds: CONDITION2, readonly: true });
        }
        this.validateIsManualNumberSeq(this.productType).subscribe(
          (isManual) => {
            this.isManual = isManual;
            fieldAccessing.push({ filedIds: NUMBER_SEQ, readonly: !this.isManual });
            observer.next(fieldAccessing);
          },
          (error) => {
            observer.next(fieldAccessing);
          }
        );
      } else {
        fieldAccessing.push({ filedIds: READONLY_WHEN_UPDATE, readonly: true });
        observer.next(fieldAccessing);
      }
    });
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(
    canCreate: boolean,
    canUpdate: boolean,
    model: CreditAppRequestTableAmendItemView,
    isWorkFlowMode: boolean
  ): Observable<boolean> {
    return this.setAccessModeByParent(model, isWorkFlowMode).pipe(
      map((result) => (isUpdateMode(model.creditAppRequestTableAmendGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  getInitialData(): Observable<CreditAppRequestTableAmendItemView> {
    const model = new CreditAppRequestTableAmendItemView();
    model.creditAppRequestTableAmendGUID = EmptyGuid;
    return of(model);
  }
  validateIsManualNumberSeq(productType: number): Observable<boolean> {
    return this.baseService.service.validateIsManualNumberSeq(productType);
  }
  canActiveByStatus(status: string): boolean {
    return (
      status <= CreditAppRequestStatus.Draft ||
      status === CreditAppRequestStatus.WaitingForMarketingStaff ||
      status === CreditAppRequestStatus.WaitingForCreditStaff ||
      status === CreditAppRequestStatus.WaitingForCreditHead ||
      status === CreditAppRequestStatus.WaitingForAssistMD
    );
  }
  setAccessModeByParent(model: CreditAppRequestTableAmendItemView, isWorkFlowMode: boolean): Observable<AccessModeView> {
    const parentId = this.baseService.uiService.getRelatedInfoActiveTableKey();
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    this.currentName = this.baseService.uiService.getRelatedInfoActiveTableName();
    switch (this.currentName) {
      case AMEND_CA:
        if (!isUpdateMode(model.creditAppRequestTableAmendGUID)) {
          this.accessModeView.canCreate = true;
          this.accessModeView.canView = true;
          return of(this.accessModeView);
        }
        return this.getAccessModeByCreditAppRequestTable(parentId, isWorkFlowMode);
      default:
        return of(this.accessModeView);
    }
  }
  getAccessModeByCreditAppRequestTable(parentId: string, isWorkFlowMode: boolean): Observable<AccessModeView> {
    return this.baseService.service.getAccessModeByCreditAppRequestTable(parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
        const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
        const accessBySN = isDraftStatus ? true : isWorkFlowMode;
        this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && accessBySN;
        this.accessModeView.canView = isDraftStatus ? true : this.accessModeView.canView && accessBySN;
      })
    );
  }
  setFieldAcessCreditNoteReferenceGroup(model: CreditAppRequestTableAmendItemView): FieldAccessing[] {
    const CreditLimitRequestFeeAmountIsLessThanZero = model.creditAppRequestTable_CreditLimitRequestFeeAmount < 0;
    const newCreditLimitRequestFeeAmountIsLessThanZero = model.creditAppRequestTable_NewCreditLimitRequestFeeAmount < 0;
    let creditNoteReferenceGroup = true;
    if (
      (this.isActiveAmendCustomerCreditLimit && CreditLimitRequestFeeAmountIsLessThanZero) ||
      (this.isAmendCustomerInfo && newCreditLimitRequestFeeAmountIsLessThanZero)
    ) {
      creditNoteReferenceGroup = false;
    }
    this.mandatoryWhenCreditLimitRequestFeeAmountIsLessThanZero = !creditNoteReferenceGroup;
    return [{ filedIds: CREDIT_NOTE_REFERENCE_GROUP, readonly: creditNoteReferenceGroup }];
  }
  setFieldAccessingByStatus(model: CreditAppRequestTableAmendItemView): FieldAccessing[] {
    const fieldAccessing: FieldAccessing[] = [];
    let setCreditNoteReferenceGroup = false;
    // <= 110
    if (model.documentStatus_StatusId <= CreditAppRequestStatus.WaitingForMarketingStaff) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      if (!model.amendRate) {
        fieldAccessing.push({ filedIds: rateGroup, readonly: true });
        fieldAccessing.push({ filedIds: AMEND_RATE_CONDTION1, readonly: true });
      } else {
        fieldAccessing.push({ filedIds: rateGroup, readonly: false });
        if (this.productType !== ProductType.Factoring) {
          fieldAccessing.push({ filedIds: AMEND_RATE_CONDTION1, readonly: true });
        } else {
          fieldAccessing.push({ filedIds: AMEND_RATE_CONDTION1, readonly: false });
        }
      }
      setCreditNoteReferenceGroup = true;
    } else if (model.documentStatus_StatusId === CreditAppRequestStatus.WaitingForCreditStaff && this.isActiveAmendCustomerCreditLimit) {
      // 130
      fieldAccessing.push(
        ...[
          { filedIds: [AppConst.ALL_ID], readonly: true },
          { filedIds: CONDITION3, readonly: false }
        ]
      );
    } else if (model.documentStatus_StatusId === CreditAppRequestStatus.WaitingForCreditHead && this.isActiveAmendCustomerCreditLimit) {
      // 140
      fieldAccessing.push(
        ...[
          { filedIds: [AppConst.ALL_ID], readonly: true },
          { filedIds: CONDITION4, readonly: false },
          { filedIds: CONDITION4_1_AND_CONDITION5_1, readonly: false }
        ]
      );
      setCreditNoteReferenceGroup = true;
    } else if (model.documentStatus_StatusId === CreditAppRequestStatus.WaitingForAssistMD) {
      // 150
      fieldAccessing.push(
        ...[
          { filedIds: [AppConst.ALL_ID], readonly: true },
          { filedIds: CONDITION5, readonly: false }
        ]
      );
      if (this.isActiveAmendCustomerCreditLimit) {
        fieldAccessing.push({ filedIds: CONDITION4_1_AND_CONDITION5_1, readonly: false });
      }
      setCreditNoteReferenceGroup = true;
    } else if (
      (model.documentStatus_StatusId >= CreditAppRequestStatus.WaitingForCreditStaff ||
        model.documentStatus_StatusId <= CreditAppRequestStatus.WaitingForCreditHead) &&
      this.isAmendCustomerInfo
    ) {
      // 140
      fieldAccessing.push(
        ...[
          { filedIds: [AppConst.ALL_ID], readonly: true },
          { filedIds: CONDITION4, readonly: false }
        ]
      );
      setCreditNoteReferenceGroup = true;
    }
    if (setCreditNoteReferenceGroup) {
      fieldAccessing.push(...this.setFieldAcessCreditNoteReferenceGroup(model));
    }
    return fieldAccessing;
  }
  setGroupControl(model: CreditAppRequestTableAmendItemView): void {
    this.isActiveAmendCustomerCreditLimit = model.creditAppRequestTable_CreditAppRequestType === CreditAppRequestType.ActiveAmendCustomerCreditLimit;
    this.isAmendCustomerInfo = model.creditAppRequestTable_CreditAppRequestType === CreditAppRequestType.AmendCustomerInfo;
  }
  onAmendRateChange(model: CreditAppRequestTableAmendItemView): Observable<FieldAccessing[]> {
    const message = model.amendRate ? messageCopy : messageClear;
    const fieldAccessing: FieldAccessing[] = [];
    return new Observable((observer) => {
      this.showConfirmDialog(message, labelAmendRate).subscribe((result) => {
        if (!result) {
          // not confirm
          model.amendRate = !model.amendRate;
        } else {
          // confirm

          if (model.amendRate) {
            this.getCreditAppRequestTableAmendUnboundForAmendRate(model);
          } else {
            this.updateCreditAppRequestTableByAmendCA(model);
          }
          this.activeIndex = 0;
          return observer.next(this.setFieldAccessingByStatus(model));
        }
        this.checkAmendRate = !model.amendRate;
      });
    });
  }
  onAmendAuthorizedPersonChange(model: CreditAppRequestTableAmendItemView): void {
    this.authorizedPersonChange = true;
    const message = model.amendAuthorizedPerson ? messageCopy : messageClear;
    this.showConfirmDialog(message, labelAmendAuthorizedPerson).subscribe((result) => {
      if (!result) {
        // not confirm
        model.amendAuthorizedPerson = !model.amendAuthorizedPerson;
        this.checkAmendAuthorizedPerson = !model.amendAuthorizedPerson;
      } else {
        // confirm\
        if (model.amendAuthorizedPerson) {
          this.copyAuthorizedPersonTrans(model);
        } else {
          this.deleteAuthorizedPersonTrans(model);
        }
        this.activeIndex = 1;
      }
      this.openAuthorizedPersonList = model.amendAuthorizedPerson;
    });
  }
  onAmendAuthorizedPersonBinding(model: CreditAppRequestTableAmendItemView): void {
    this.checkAmendAuthorizedPerson = !model.amendAuthorizedPerson && isUpdateMode(model.creditAppRequestTableAmendGUID);
    if (!this.authorizedPersonChange) {
      this.openAuthorizedPersonList = model.amendAuthorizedPerson;
    }
  }
  onAmendGuarantorBinding(model: CreditAppRequestTableAmendItemView): void {
    this.checkAmendGuarantor = !model.amendGuarantor && isUpdateMode(model.creditAppRequestTableAmendGUID);
    if (!this.guarantorChange) {
      this.openGuarantorList = model.amendGuarantor;
    }
  }
  onAmendRateBinding(model: CreditAppRequestTableAmendItemView): void {
    this.checkAmendRate = !model.amendRate && isUpdateMode(model.creditAppRequestTableAmendGUID);
  }

  onAmendGuarantorChange(model: CreditAppRequestTableAmendItemView): void {
    this.guarantorChange = true;
    const message = model.amendGuarantor ? messageCopy : messageClear;
    this.showConfirmDialog(message, labelAmendGuarantor).subscribe((result) => {
      if (!result) {
        model.amendGuarantor = !model.amendGuarantor;
        this.checkAmendGuarantor = !model.amendGuarantor;
      } else {
        if (model.amendGuarantor) {
          this.copyGuarantorTrans(model);
        } else {
          this.deleteGuarantorTrans(model);
        }
        this.activeIndex = 2;
      }
      this.openGuarantorList = model.amendGuarantor;
    });
  }
  showConfirmDialog(message: string, label: string): Observable<any> {
    const confirmation: Confirmation = {
      header: this.baseService.translate.instant('CONFIRM.CONFIRM'),
      message: this.baseService.translate.instant(message, [this.baseService.translate.instant(label)])
    };
    return new Observable((observer) => {
      this.baseService.notificationService.showConfirmDialog(confirmation);
      this.baseService.notificationService.isAccept.subscribe((isConfirm) => {
        this.amendCusInfoAction(label, isConfirm);
        this.baseService.notificationService.isAccept.observers = [];
        observer.next(isConfirm);
      });
    });
  }
  amendCusInfoAction(actionField: string, isConfirm: boolean): void {
    switch (actionField) {
      case labelAmendRate:
        break;
      case labelAmendAuthorizedPerson:
        break;
      case labelAmendGuarantor:
        break;
      default:
        break;
    }
  }
  onNewCreditLimitChange(model: CreditAppRequestTableAmendItemView): FieldAccessing[] {
    return this.calcCreditLimitRequestFeeAmountAndMaxRetentionAmount(model, model.creditAppRequestTable_NewCreditLimit);
  }
  getInterestTypeValueByCreditAppRequestAmend(model: CreditAppRequestTableAmendItemView): void {
    if (model.amendRate) {
      if (
        isNullOrUndefOrEmptyGUID(model.creditAppRequestTable_NewInterestTypeGUID) ||
        isNullOrUndefOrEmptyGUID(model.creditAppRequestTable_RequestDate)
      ) {
        this.interestTypeValue = 0;
        this.calcNewTotalInterestPct(model);
        return;
      }
      this.baseService.service.getInterestTypeValueByCreditAppRequestAmend(model).subscribe(
        (result) => {
          this.interestTypeValue = result;
          this.calcNewTotalInterestPct(model);
        },
        (error) => {
          model.creditAppRequestTable_NewInterestTypeGUID = null;
          this.calcNewTotalInterestPct(model);
        }
      );
    }
  }
  GetCustomerCreditLimit(model: CreditAppRequestTableAmendItemView): void {
    if (!isNullOrUndefOrEmpty(model.creditAppRequestTable_RequestDate)) {
      // fix for valide post model
      const newMOdel = Object.assign({}, model);
      newMOdel.creditAppRequestTable_CreditAppRequestType = 1;
      this.baseService.service.GetCustomerCreditLimit(newMOdel).subscribe((result) => {
        model.creditAppRequestTable_CustomerCreditLimit = result;
      });
    }
  }
  calcNewTotalInterestPct(model: CreditAppRequestTableAmendItemView): void {
    model.creditAppRequestTable_NewTotalInterestPct = model.creditAppRequestTable_NewInterestAdjustmentPct + this.interestTypeValue;
  }
  onNewMaxRetentionPctChange(model: CreditAppRequestTableAmendItemView): void {
    model.creditAppRequestTable_NewMaxRetentionAmount = (model.creditAppRequestTable_NewTotalInterestPct / 100) * model.originalCreditLimit;
  }
  updateCreditAppRequestTableByAmendCA(model: CreditAppRequestTableAmendItemView): void {
    (this.baseService.service as CreditAppRequestTableAmendService).updateCreditAppRequestTableByAmendCA(model).subscribe((result) => {
      model.creditAppRequestTable_NewInterestTypeGUID = result.interestTypeGUID;
      model.creditAppRequestTable_NewInterestAdjustmentPct = result.interestAdjustment;
      model.creditAppRequestTable_NewTotalInterestPct = result.totalInterestPct;
      //   model.creditAppRequestTable_NewCreditLimitRequestFeePct = result.creditRequestFeePct;
      model.creditAppRequestTable_NewMaxRetentionPct = result.maxRetentionPct;
      model.creditAppRequestTable_NewMaxRetentionAmount = result.maxRetentionAmount;
      model.creditAppRequestTable_NewMaxPurchasePct = result.maxPurchasePct;
      model.creditAppRequestTable_NewPurchaseFeePct = result.purchaseFeePct;
      model.creditAppRequestTable_NewPurchaseFeeCalculateBase = result.purchaseFeeCalculateBase;
      this.checkAmendRate = !model.amendRate;
      this.activeIndex = 0;
    });
  }
  getCreditAppRequestTableAmendUnboundForAmendRate(model: CreditAppRequestTableAmendItemView): void {
    (this.baseService.service as CreditAppRequestTableAmendService).getCreditAppRequestTableAmendUnboundForAmendRate(model).subscribe((result) => {
      model.creditAppRequestTable_NewInterestTypeGUID = result.creditAppRequestTable_NewInterestTypeGUID;
      model.creditAppRequestTable_NewInterestAdjustmentPct = result.creditAppRequestTable_NewInterestAdjustmentPct;
      model.creditAppRequestTable_NewTotalInterestPct = result.creditAppRequestTable_NewTotalInterestPct;
      // model.creditAppRequestTable_NewCreditLimitRequestFeePct = result.creditAppRequestTable_NewCreditLimitRequestFeePct;
      model.creditAppRequestTable_NewMaxRetentionPct = result.creditAppRequestTable_NewMaxRetentionPct;
      model.creditAppRequestTable_NewMaxRetentionAmount = result.creditAppRequestTable_NewMaxRetentionAmount;
      model.creditAppRequestTable_NewMaxPurchasePct = result.creditAppRequestTable_NewMaxPurchasePct;
      model.creditAppRequestTable_NewPurchaseFeePct = result.creditAppRequestTable_NewPurchaseFeePct;
      model.creditAppRequestTable_NewPurchaseFeeCalculateBase = result.creditAppRequestTable_NewPurchaseFeeCalculateBase;
      this.checkAmendRate = !model.amendRate;
    });
  }
  copyAuthorizedPersonTrans(model: CreditAppRequestTableAmendItemView): void {
    (this.baseService.service as CreditAppRequestTableAmendService).createAuthorizedPersonTrans(model).subscribe(() => {
      this.checkAmendAuthorizedPerson = !model.amendAuthorizedPerson;
    });
  }
  deleteAuthorizedPersonTrans(model: CreditAppRequestTableAmendItemView): void {
    (this.baseService.service as CreditAppRequestTableAmendService).deleteAuthorizedPersonTrans(model).subscribe(() => {
      this.checkAmendAuthorizedPerson = !model.amendAuthorizedPerson;
      this.activeIndex = 0;
    });
  }
  copyGuarantorTrans(model: CreditAppRequestTableAmendItemView): void {
    (this.baseService.service as CreditAppRequestTableAmendService).createGuarantorTrans(model).subscribe(() => {
      this.checkAmendGuarantor = !model.amendGuarantor;
    });
  }
  deleteGuarantorTrans(model: CreditAppRequestTableAmendItemView): void {
    (this.baseService.service as CreditAppRequestTableAmendService).deleteGuarantorTrans(model).subscribe(() => {
      this.checkAmendGuarantor = !model.amendGuarantor;
      this.activeIndex = 0;
    });
  }
  getWorkFlowPath(model: CreditAppRequestTableAmendItemView): PathParamModel {
    const ACTION_WORKFLOW = 'actionworkflow';
    const START_WORKFLOW = 'startworkflow';
    const PATH = model.documentStatus_StatusId === CreditAppRequestStatus.Draft ? START_WORKFLOW : ACTION_WORKFLOW;
    return {
      path: PATH,
      parameters: { creditAppRequestTableGUID: model.creditAppRequestTableGUID, [WORKFLOW.ACTIONHISTORY_REFGUID]: model.creditAppRequestTableGUID }
    };
  }
  getDisabledWorkFlow(model: CreditAppRequestTableAmendItemView): boolean {
    return !(
      model.documentStatus_StatusId >= CreditAppRequestStatus.Draft && model.documentStatus_StatusId <= CreditAppRequestStatus.WaitingForRetry
    );
  }
  setPrintSetBookDocTransData(model: CreditAppRequestTableAmendItemView): PrintSetBookDocTransView {
    const paramModel = new PrintSetBookDocTransView();
    toMapModel(model, paramModel);
    paramModel.refType = RefType.CreditAppRequestTable;
    paramModel.creditAppRequestId = model.creditAppRequestTable_CreditAppRequestId;
    paramModel.creditAppRequestDescription = model.creditAppRequestTable_Description;
    paramModel.creditLimitTypeId = model.creditAppRequestTable_CustomerCreditLimit.toString();
    paramModel.creditAppRequestType = model.creditAppRequestTable_CreditAppRequestType;
    return paramModel;
  }
  onApprovedCreditLimitRequestChange(model: CreditAppRequestTableAmendItemView): FieldAccessing[] {
    return this.calcCreditLimitRequestFeeAmountAndMaxRetentionAmount(model, model.creditAppRequestTable_ApprovedCreditLimitRequest);
  }
  calcCreditLimitRequestFeeAmountAndMaxRetentionAmount(
    model: CreditAppRequestTableAmendItemView,
    newCreditLimitOrApprovedCreditLimitRequest: number
  ): FieldAccessing[] {
    const difference = newCreditLimitOrApprovedCreditLimitRequest - model.originalCreditLimit;
    model.creditAppRequestTable_CreditLimitRequestFeeAmount = difference * (model.creditAppRequestTable_CreditLimitRequestFeePct / 100);
    model.creditAppRequestTable_MaxRetentionAmount = newCreditLimitOrApprovedCreditLimitRequest * (model.creditAppRequestTable_MaxRetentionPct / 100);
    return this.onCreditLimitRequestFeeAmountChange(model);
  }
  onOrigInvoiceChange(model: CreditAppRequestTableAmendItemView, option: SelectItems[]): void {
    const invoice = !isNullOrUndefOrEmptyGUID(model.origInvoice)
      ? (option.find((f) => f.value === model.origInvoice).rowData as InvoiceTableItemView)
      : new InvoiceTableItemView();
    model.origInvoiceId = invoice.invoiceId;
    model.origTaxInvoiceId = invoice.invoiceId;
    model.origInvoiceAmount = invoice.invoiceAmount;
    model.origTaxInvoiceAmount = invoice.invoiceAmount;
  }
  onCreditLimitRequestFeeAmountChange(model: CreditAppRequestTableAmendItemView): FieldAccessing[] {
    if (model.creditAppRequestTable_CreditLimitRequestFeeAmount >= 0) {
      model.cnReasonGUID = null;
      model.origInvoice = null;
      model.origInvoiceId = '';
      model.origTaxInvoiceId = '';
      model.origInvoiceAmount = 0;
      model.origTaxInvoiceAmount = 0;
    }
    return this.setFieldAcessCreditNoteReferenceGroup(model);
  }
  setFirstBinding(): void {
    this.firstBinding = true;
  }
  setFirstAmend(): void {
    if (this.firstBinding) {
      this.checkAmendRate = this.firstBinding;
      this.checkAmendAuthorizedPerson = this.firstBinding;
      this.checkAmendGuarantor = this.firstBinding;
      this.firstBinding = false;
    }
  }
  statusIsDraft(model: CreditAppRequestTableAmendItemView): boolean {
    return model.documentStatus_StatusId === CreditAppRequestStatus.Draft;
  }
  isRequestDateGraterThanEqualInActiveDate(model: CreditAppRequestTableAmendItemView): FieldAccessing[] {
    const greaterThanEqual = isDateFromGreaterThanEqualDateTo(model.creditAppRequestTable_RequestDate, model.creditAppTable_InactiveDate);
    model.creditAppRequestTable_CreditAppRequestType = greaterThanEqual ? CreditAppRequestType.ActiveAmendCustomerCreditLimit : null;
    return [{ filedIds: CONDITION2, readonly: greaterThanEqual }];
  }
}
