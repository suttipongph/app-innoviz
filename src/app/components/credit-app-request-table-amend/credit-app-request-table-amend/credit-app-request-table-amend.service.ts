import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  AccessModeView,
  CreditAppRequestTableAmendItemView,
  CreditAppRequestTableAmendListView,
  CreditAppRequestTableItemView
} from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CreditAppRequestTableAmendService {
  serviceKey = 'creditAppRequestTableAmendGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCreditAppRequestTableAmendToList(search: SearchParameter): Observable<SearchResult<CreditAppRequestTableAmendListView>> {
    const url = `${this.servicePath}/GetCreditAppRequestTableAmendList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCreditAppRequestTableAmend(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCreditAppRequestTableAmend`;
    return this.dataGateway.delete(url, row);
  }
  getCreditAppRequestTableAmendById(id: string): Observable<CreditAppRequestTableAmendItemView> {
    const url = `${this.servicePath}/GetCreditAppRequestTableAmendById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCreditAppRequestTableAmend(vmModel: CreditAppRequestTableAmendItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCreditAppRequestTableAmend`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCreditAppRequestTableAmend(vmModel: CreditAppRequestTableAmendItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCreditAppRequestTableAmend`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(creditAppId: string): Observable<CreditAppRequestTableAmendItemView> {
    const url = `${this.servicePath}/GetCreditAppRequestTableAmnendInitialData/CreditAppId=${creditAppId}`;
    return this.dataGateway.get(url);
  }
  validateIsManualNumberSeq(productType: number): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeq/productType=${productType}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByCreditAppRequestTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeAmendCaByCreditAppRequestTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getInterestTypeValueByCreditAppRequestAmend(vmModel: CreditAppRequestTableAmendItemView): Observable<number> {
    const url = `${this.servicePath}/GetInterestTypeValueByCreditAppRequestAmend`;
    return this.dataGateway.post(url, vmModel);
  }
  updateCreditAppRequestTableByAmendCA(vmModel: CreditAppRequestTableAmendItemView): Observable<CreditAppRequestTableItemView> {
    const url = `${this.servicePath}/UpdateCreditAppRequestTableByAmendCA`;
    return this.dataGateway.post(url, vmModel);
  }
  getCreditAppRequestTableAmendUnboundForAmendRate(vmModel: CreditAppRequestTableAmendItemView): Observable<CreditAppRequestTableAmendItemView> {
    const url = `${this.servicePath}/GetCreditAppRequestTableAmendUnboundForAmendRate`;
    return this.dataGateway.post(url, vmModel);
  }
  createAuthorizedPersonTrans(vmModel: CreditAppRequestTableAmendItemView): Observable<CreditAppRequestTableAmendItemView> {
    const url = `${this.servicePath}/CreateAuthorizedPersonTransByAmendCa`;
    return this.dataGateway.post(url, vmModel);
  }
  deleteAuthorizedPersonTrans(vmModel: CreditAppRequestTableAmendItemView): Observable<CreditAppRequestTableAmendItemView> {
    const url = `${this.servicePath}/DeleteAuthorizedPersonTransByAmendCa`;
    return this.dataGateway.post(url, vmModel);
  }
  createGuarantorTrans(vmModel: CreditAppRequestTableAmendItemView): Observable<CreditAppRequestTableAmendItemView> {
    const url = `${this.servicePath}/CreateGuarantorTransByAmendCa`;
    return this.dataGateway.post(url, vmModel);
  }
  deleteGuarantorTrans(vmModel: CreditAppRequestTableAmendItemView): Observable<CreditAppRequestTableAmendItemView> {
    const url = `${this.servicePath}/DeleteGuarantorTransByAmendCa`;
    return this.dataGateway.post(url, vmModel);
  }
  GetCustomerCreditLimit(vmModel: CreditAppRequestTableAmendItemView): Observable<number> {
    const url = `${this.servicePath}/GetCustomerCreditLimit`;
    return this.dataGateway.post(url, vmModel);
  }
}
