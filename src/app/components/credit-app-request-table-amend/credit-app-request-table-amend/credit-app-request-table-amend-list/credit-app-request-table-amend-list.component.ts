import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, CreditAppRequestType, DocumentProcessStatus, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CreditAppRequestTableAmendListView } from 'shared/models/viewModel';
import { CreditAppRequestTableAmendService } from '../credit-app-request-table-amend.service';
import { CreditAppRequestTableAmendListCustomComponent } from './credit-app-request-table-amend-list-custom.component';

@Component({
  selector: 'credit-app-request-table-amend-list',
  templateUrl: './credit-app-request-table-amend-list.component.html',
  styleUrls: ['./credit-app-request-table-amend-list.component.scss']
})
export class CreditAppRequestTableAmendListComponent extends BaseListComponent<CreditAppRequestTableAmendListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  creditAppRequestTypeOptions: SelectItems[] = [];
  interestTypeOptions: SelectItems[] = [];
  purchaseFeeCalculateBaseOptions: SelectItems[] = [];
  constructor(public custom: CreditAppRequestTableAmendListCustomComponent, private service: CreditAppRequestTableAmendService) {
    super();
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getCreditAppRequestTypeEnumDropDown(this.creditAppRequestTypeOptions, [
      CreditAppRequestType.ActiveAmendCustomerCreditLimit,
      CreditAppRequestType.AmendCustomerInfo
    ]);
    this.baseDropdown.getPurchaseFeeCalculateBaseEnumDropDown(this.purchaseFeeCalculateBaseOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'creditAppRequestTableAmendGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.CREDIT_APPLICATION_REQUEST_ID',
        textKey: 'creditAppRequestTable_CreditAppRequestId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'creditAppRequestTable_Description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CREDIT_APPLICATION_REQUEST_TYPE',
        textKey: 'creditAppRequestTable_CreditAppRequestType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditAppRequestTypeOptions
      },
      {
        label: 'LABEL.REQUEST_DATE',
        textKey: 'creditAppRequestTable_RequestDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_CustomerId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'documentStatus_Description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'CreditAppRequestTable_RefCreditAppTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CreditAppRequestTableAmendListView>> {
    return this.service.getCreditAppRequestTableAmendToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCreditAppRequestTableAmend(row));
  }
}
