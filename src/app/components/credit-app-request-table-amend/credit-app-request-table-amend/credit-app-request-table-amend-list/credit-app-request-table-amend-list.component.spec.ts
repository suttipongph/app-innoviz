import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditAppRequestTableAmendListComponent } from './credit-app-request-table-amend-list.component';

describe('CreditAppRequestTableAmendListViewComponent', () => {
  let component: CreditAppRequestTableAmendListComponent;
  let fixture: ComponentFixture<CreditAppRequestTableAmendListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditAppRequestTableAmendListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppRequestTableAmendListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
