import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BranchService } from './branch.service';
import { BranchListComponent } from './branch-list/branch-list.component';
import { BranchItemComponent } from './branch-item/branch-item.component';
import { BranchItemCustomComponent } from './branch-item/branch-item-custom.component';
import { BranchListCustomComponent } from './branch-list/branch-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BranchListComponent, BranchItemComponent],
  providers: [BranchService, BranchItemCustomComponent, BranchListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BranchListComponent, BranchItemComponent]
})
export class BranchComponentModule {}
