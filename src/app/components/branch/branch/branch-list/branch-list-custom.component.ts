import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel } from 'shared/models/systemModel';
const COMPANY_TABLE = 'company';
export class BranchListCustomComponent {
  baseService: BaseServiceModel<any>;

  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }

  getIsUpdateModeByParent(id: string): Observable<any> {
    return this.baseService.service.getBranchById(id).pipe(
      map((result) => {
        if (isNullOrUndefined(result)) {
          return false;
        } else {
          return true;
        }
      })
    );
  }
}
