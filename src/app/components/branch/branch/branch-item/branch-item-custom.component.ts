import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { BranchItemView } from 'shared/models/viewModel';

const firstGroup = ['BRANCH_ID'];

export class BranchItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: BranchItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BranchItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(): Observable<BranchItemView> {
    let model = new BranchItemView();
    model.branchGUID = EmptyGuid;
    return of(model);
  }
}
