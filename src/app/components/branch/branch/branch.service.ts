import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BranchItemView, BranchListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class BranchService {
  serviceKey = 'branchGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBranchToList(search: SearchParameter): Observable<SearchResult<BranchListView>> {
    const url = `${this.servicePath}/GetBranchList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBranch(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBranch`;
    return this.dataGateway.delete(url, row);
  }
  getBranchById(id: string): Observable<BranchItemView> {
    const url = `${this.servicePath}/GetBranchById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBranch(vmModel: BranchItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBranch`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBranch(vmModel: BranchItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBranch`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
