import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GenMainAgmNoticeOfCancelService } from './gen-main-agm-notice-of-cancel.service';
import { MainAgmNoticeOfCancelComponent } from './gen-main-agm-notice-of-cancel/gen-main-agm-notice-of-cancel.component';
import { GenMainAgmNoticeOfCancelCustomComponent } from './gen-main-agm-notice-of-cancel/gen-main-agm-notice-of-cancel-custom.component';
@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [MainAgmNoticeOfCancelComponent],
  providers: [GenMainAgmNoticeOfCancelService, GenMainAgmNoticeOfCancelCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [MainAgmNoticeOfCancelComponent]
})
export class GenMainAgmNoticeOfCancelComponentModule {}
