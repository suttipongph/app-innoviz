import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenMainAgmNoticeOfCancelComponent } from './gen-main-agm-notice-of-cancel.component';

describe('GenMainAgmNoticeOfCancelViewComponent', () => {
  let component: GenMainAgmNoticeOfCancelComponent;
  let fixture: ComponentFixture<GenMainAgmNoticeOfCancelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GenMainAgmNoticeOfCancelComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenMainAgmNoticeOfCancelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
