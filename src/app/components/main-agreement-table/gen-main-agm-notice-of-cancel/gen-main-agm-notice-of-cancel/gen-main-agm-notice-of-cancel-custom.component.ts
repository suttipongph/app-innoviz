import { Observable, of } from 'rxjs';
import { EmptyGuid, ProductType } from 'shared/constants';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { GenMainAgmNoticeOfCancelResultView, GenMainAgmNoticeOfCancelView } from 'shared/models/viewModel';
import { GenMainAgmNoticeOfCancelService } from '../gen-main-agm-notice-of-cancel.service';

const firstGroup = [
  'INTERNAL_MAIN_AGREEMENT_ID',
  'MAIN_AGREEMENT_ID',
  'MAIN_AGREEMENT_DESCRIPTION',
  'AGREEMENT_DATE',
  'AGREEMENT_DOC_TYPE',
  'CUSTOMER_GUID',
  'CUSTOMER_NAME',
  'BUYER_TABLE_GUID',
  'BUYER_NAME'
];
const condition1 = ['NOTICE_OF_CANCELLATION_INTERNAL_MAIN_AGREEMENT_ID'];

export class GenMainAgmNoticeOfCancelCustomComponent {
  baseService: BaseServiceModel<any>;
  isMandatory_noticeOfCancellationInternalMainAgreementId: boolean = false;
  getFieldAccessing(model: GenMainAgmNoticeOfCancelView, service: GenMainAgmNoticeOfCancelService): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return new Observable((observer) => {
      this.validateIsManualNumberSeq(service, this.getProductType()).subscribe((result) => {
        if (result) {
          fieldAccessing.push({ filedIds: condition1, readonly: false });
          this.isMandatory_noticeOfCancellationInternalMainAgreementId = true;
          observer.next(fieldAccessing);
        } else {
          fieldAccessing.push({ filedIds: condition1, readonly: true });
          this.isMandatory_noticeOfCancellationInternalMainAgreementId = false;
          observer.next(fieldAccessing);
        }
      });
    });
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: GenMainAgmNoticeOfCancelView): Observable<boolean> {
    return of(canCreate);
  }
  getInitialData(): Observable<GenMainAgmNoticeOfCancelView> {
    let model = new GenMainAgmNoticeOfCancelView();
    model.mainAgreementTableGUID = EmptyGuid;
    return of(model);
  }
  validateIsManualNumberSeq(service: GenMainAgmNoticeOfCancelService, productType: number): Observable<boolean> {
    return service.validateIsManualNumberSeq(productType);
  }
  getRemakReason(id: string, options: SelectItems[]): Observable<SelectItems> {
    const item = options.find((f) => f.value === id);
    return of(item);
  }
  getModelParam(model: GenMainAgmNoticeOfCancelView, id: string): Observable<GenMainAgmNoticeOfCancelResultView> {
    const param: GenMainAgmNoticeOfCancelView = new GenMainAgmNoticeOfCancelView();
    param.mainAgreementTableGUID = id;
    param.agreementDate = model.agreementDate;
    param.agreementDocType = model.agreementDocType;
    param.buyerTableGUID = model.buyerTableGUID;
    param.buyerName = model.buyerName;
    param.cancelDate = model.cancelDate;
    param.customerGUID = model.customerGUID;
    param.customerName = model.customerName;
    param.description = model.description;
    param.documentReasonGUID = model.documentReasonGUID;
    param.reasonRemark = model.reasonRemark;
    param.internalMainAgreementId = model.internalMainAgreementId;
    param.mainAgreementDescription = model.mainAgreementDescription;
    param.noticeOfCancellationInternalMainAgreementId = model.noticeOfCancellationInternalMainAgreementId;
    param.companyGUID = model.companyGUID;

    return of(param);
  }
  getProductType(): number {
    const masterRoute = this.baseService.uiService.getMasterRoute();
    let productType = 0;
    switch (masterRoute) {
      case 'factoring':
        productType = ProductType.Factoring;
        break;
      case 'bond':
        productType = ProductType.Bond;
        break;
      case 'hirepurchase':
        productType = ProductType.HirePurchase;
        break;
      case 'lcdlc':
        productType = ProductType.LC_DLC;
        break;
      case 'leasing':
        productType = ProductType.Leasing;
        break;
      case 'projectfinance':
        productType = ProductType.ProjectFinance;
        break;
      default:
        break;
    }
    return productType;
  }
}
