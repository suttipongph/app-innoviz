import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { GenMainAgmNoticeOfCancelResultView, GenMainAgmNoticeOfCancelView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult, SelectItems } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class GenMainAgmNoticeOfCancelService {
  serviceKey = 'genMainAgmNoticeOfCancelGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getGenMainAgmNoticeOfCancelById(id: string): Observable<GenMainAgmNoticeOfCancelView> {
    const url = `${this.servicePath}/GetGenMainAgmNoticeOfCancelById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createGenMainAgmNoticeOfCancel(vmModel: GenMainAgmNoticeOfCancelView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateGenMainAgmNoticeOfCancel`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  validateIsManualNumberSeq(productType: number): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeq/productType=${productType}`;
    return this.dataGateway.get(url);
  }
  GetCreditAppRequestTableDropDownNoticeOfCancel(id: string): Observable<SelectItems[]> {
    const url = `${this.servicePath}/GetCreditAppRequestTableDropDownAddendum/id=${id}`;
    return this.dataGateway.get(url);
  }
  getCreditAppRequestIDCById(id: string): Observable<GenMainAgmNoticeOfCancelView> {
    const url = `${this.servicePath}/GetCreditAppRequestIDCById/id=${id}`;
    return this.dataGateway.get(url);
  }
}
