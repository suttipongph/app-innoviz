import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  AgreementTableInfoItemView,
  BuyerAgreementTableItemView,
  MainAgreementTableItemView,
  MainAgreementTableListView
} from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { PrintSetBookDocTransParm } from 'shared/models/viewModel/printSetBookDocTransParm';
@Injectable()
export class MainAgreementTableService {
  serviceKey = 'mainAgreementTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getMainAgreementTableToList(search: SearchParameter): Observable<SearchResult<MainAgreementTableListView>> {
    const url = `${this.servicePath}/GetMainAgreementTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteMainAgreementTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteMainAgreementTable`;
    return this.dataGateway.delete(url, row);
  }
  getMainAgreementTableById(id: string): Observable<MainAgreementTableItemView> {
    const url = `${this.servicePath}/GetMainAgreementTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createMainAgreementTable(vmModel: MainAgreementTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateMainAgreementTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateMainAgreementTable(vmModel: MainAgreementTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateMainAgreementTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(productType: number): Observable<MainAgreementTableItemView> {
    const url = `${this.servicePath}/GetMainAgreementTableInitialData/productType=${productType}`;
    return this.dataGateway.get(url);
  }
  validateIsManualNumberSeqMainAgreement(productType: number): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeqMainAgreement/productType=${productType}`;
    return this.dataGateway.get(url);
  }
  validateIsManualNumberSeqInternalMainAgreement(productType: number): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeqInternalMainAgreement/productType=${productType}`;
    return this.dataGateway.get(url);
  }
  getBuyerAgreementTableByBuyerTable(model: MainAgreementTableItemView): Observable<MainAgreementTableItemView> {
    const url = `${this.servicePath}/GetBuyerAgreementTableByBuyerTable`;
    return this.dataGateway.post(url, model);
  }
  getAgreementTableInfoByMainAgreement(id: string): Observable<AgreementTableInfoItemView> {
    const url = `${this.servicePath}/GetAgreementTableInfoByMainAgreement/id=${id}`;
    return this.dataGateway.get(url);
  }
  getVisibleManageMenu(id: string): Observable<number[]> {
    const url = `${this.servicePath}/GetVisibleManageMenu/id=${id}`;
    return this.dataGateway.get(url);
  }
  getGenerateAddendumValidation(vmModel: MainAgreementTableItemView): Observable<boolean> {
    const url = `${this.servicePath}/GetGenerateAddendumValidation`;
    return this.dataGateway.post(url, vmModel);
  }
  getGenerateLoanRequestAGMValidation(vmModel: MainAgreementTableItemView): Observable<boolean> {
    const url = `${this.servicePath}/getGenerateLoanRequestAGMValidation`;
    return this.dataGateway.post(url, vmModel);
  }
  getGenerateNoticeOfCancellationValidation(vmModel: MainAgreementTableItemView): Observable<boolean> {
    const url = `${this.servicePath}/GetGenerateNoticeOfCancellationValidation`;
    return this.dataGateway.post(url, vmModel);
  }
  getPrintSetBookDocTransValidation(vmModel: PrintSetBookDocTransParm): Observable<boolean> {
    const url = `${this.servicePath}/GetPrintSetBookDocTransValidation`;
    return this.dataGateway.post(url, vmModel);
  }
}
