import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GenMainAgmAddendumService } from './main-agm-addendum.service';
import { GenMainAgmAddendumComponent } from './main-agm-addendum/main-agm-addendum.component';
import { GenMainAgmAddendumCustomComponent } from './main-agm-addendum/main-agm-addendum-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [GenMainAgmAddendumComponent],
  providers: [GenMainAgmAddendumService, GenMainAgmAddendumCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [GenMainAgmAddendumComponent]
})
export class GenMainAgmAddendumComponentModule {}
