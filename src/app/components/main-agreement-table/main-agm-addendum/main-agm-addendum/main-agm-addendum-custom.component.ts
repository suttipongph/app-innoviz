import { Observable, of } from 'rxjs';
import { EmptyGuid, ProductType } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { GenMainAgmAddendumView } from 'shared/models/viewModel';
import { GenMainAgmAddendumService } from '../main-agm-addendum.service';

const firstGroup = [
  'INTERNAL_MAIN_AGREEMENT_ID',
  'MAIN_AGREEMENT_ID',
  'MAIN_AGREEMENT_DESCRIPTION',
  'CUSTOMER_ID',
  'CUSTOMER_NAME',
  'BUYER_ID',
  'BUYER_NAME',
  'AGREEMENT_DOC_TYPE',
  'CREDIT_APP_REQUEST_TYPE',
  'CREDIT_LIMIT_TYPE_GUID',
  'REQUEST_DATE',
  'CREDIT_APP_REQUEST_DESCRIPTION',
  'CUSTOMER_GUID',
  'BUYER_GUID'
];

const condition1 = ['ADDENDUM_INTERNAL_MAIN_AGREEMENT_ID'];

export class GenMainAgmAddendumCustomComponent {
  baseService: BaseServiceModel<any>;
  isMandatory_Addendum: boolean = false;
  getFieldAccessing(model: GenMainAgmAddendumView, genMainAgmAddendumService: GenMainAgmAddendumService): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return new Observable((observer) => {
      this.validateIsManualNumberSeq(genMainAgmAddendumService, this.getProductType()).subscribe((result) => {
        if (result) {
          fieldAccessing.push({ filedIds: condition1, readonly: false });
          this.isMandatory_Addendum = true;
          observer.next(fieldAccessing);
        } else {
          fieldAccessing.push({ filedIds: condition1, readonly: true });
          this.isMandatory_Addendum = false;
          observer.next(fieldAccessing);
        }
      });
    });
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: GenMainAgmAddendumView): Observable<boolean> {
    return of(canCreate);
  }
  getInitialData(): Observable<GenMainAgmAddendumView> {
    let model = new GenMainAgmAddendumView();
    model.mainAgreementTableGUID = EmptyGuid;
    return of(model);
  }
  validateIsManualNumberSeq(service: GenMainAgmAddendumService, productType: number): Observable<boolean> {
    return service.validateIsManualNumberSeq(productType);
  }
  getProductType(): number {
    const masterRoute = this.baseService.uiService.getMasterRoute();
    let productType = 0;
    switch (masterRoute) {
      case 'factoring':
        productType = ProductType.Factoring;
        break;
      case 'bond':
        productType = ProductType.Bond;
        break;
      case 'hirepurchase':
        productType = ProductType.HirePurchase;
        break;
      case 'lcdlc':
        productType = ProductType.LC_DLC;
        break;
      case 'leasing':
        productType = ProductType.Leasing;
        break;
      case 'projectfinance':
        productType = ProductType.ProjectFinance;
        break;
      default:
        break;
    }
    return productType;
  }
}
