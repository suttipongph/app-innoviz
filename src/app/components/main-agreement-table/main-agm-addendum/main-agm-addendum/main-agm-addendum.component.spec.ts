import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenMainAgmAddendumComponent } from './main-agm-addendum.component';

describe('GenMainAgmAddendumViewComponent', () => {
  let component: GenMainAgmAddendumComponent;
  let fixture: ComponentFixture<GenMainAgmAddendumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GenMainAgmAddendumComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenMainAgmAddendumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
