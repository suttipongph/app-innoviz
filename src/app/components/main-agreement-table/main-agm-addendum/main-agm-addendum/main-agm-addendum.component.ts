import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { GenMainAgmAddendumView } from 'shared/models/viewModel';
import { GenMainAgmAddendumService } from '../main-agm-addendum.service';
import { GenMainAgmAddendumCustomComponent } from './main-agm-addendum-custom.component';
@Component({
  selector: 'main-agm-addendum',
  templateUrl: './main-agm-addendum.component.html',
  styleUrls: ['./main-agm-addendum.component.scss']
})
export class GenMainAgmAddendumComponent extends BaseItemComponent<GenMainAgmAddendumView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  agreementDocTypeOptions: SelectItems[] = [];
  creditAppRequestTableOptions: SelectItems[] = [];
  creditAppRequestTypeOptions: SelectItems[] = [];

  constructor(
    private service: GenMainAgmAddendumService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: GenMainAgmAddendumCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getAgreementDocTypeEnumDropDown(this.agreementDocTypeOptions);
    this.baseDropdown.getCreditAppRequestTypeEnumDropDown(this.creditAppRequestTypeOptions);
  }
  getById(): Observable<GenMainAgmAddendumView> {
    return this.service.getGenMainAgmAddendumById(this.id);
  }
  getInitialData(): Observable<GenMainAgmAddendumView> {
    return this.custom.getInitialData();
  }
  onClose(): void {
    super.onBack();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: GenMainAgmAddendumView): void {
    forkJoin(this.service.getCreditAppRequestTableDropDown(model.mainAgreementTableGUID)).subscribe(
      ([creditAppRequestTable]) => {
        this.creditAppRequestTableOptions = creditAppRequestTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model, this.service).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false, this.model);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true, this.model);
      }
    });
  }
  onSubmit(isColsing: boolean, model: GenMainAgmAddendumView): void {
    super.onExecuteFunction(this.service.createGenMainAgmAddendum(model));
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onCreditAppRequestIDChange(id: string): void {
    if (!isNullOrUndefined(id)) {
      this.service.getCreditAppRequestIDCById(id).subscribe((res) => {
        this.model.creditAppRequestType = res.creditAppRequestType;
        this.model.creditLimitType_Values = res.creditLimitType_Values;
        this.model.requestDate = res.requestDate;
        this.model.creditAppRequestDescription = res.creditAppRequestDescription;
      });
    }
  }
}
