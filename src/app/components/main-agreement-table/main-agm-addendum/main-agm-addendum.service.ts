import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CreditAppRequestTableItemView, GenMainAgmAddendumView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult, SelectItems } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class GenMainAgmAddendumService {
  serviceKey = 'genMainAgmAddendumGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getGenMainAgmAddendumById(id: string): Observable<GenMainAgmAddendumView> {
    const url = `${this.servicePath}/GetGenMainAgmAddendumById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createGenMainAgmAddendum(vmModel: GenMainAgmAddendumView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateGenMainAgmAddendum`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  getCreditAppRequestTableDropDown(id: string): Observable<SelectItems[]> {
    const url = `${this.servicePath}/GetCreditAppRequestTableDropDown/id=${id}`;
    return this.dataGateway.get(url);
  }
  getCreditAppRequestIDCById(id: string): Observable<GenMainAgmAddendumView> {
    const url = `${this.servicePath}/GetCreditAppRequestIDCById/id=${id}`;
    return this.dataGateway.get(url);
  }
  validateIsManualNumberSeq(productType: number): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeq/productType=${productType}`;
    return this.dataGateway.get(url);
  }
}
