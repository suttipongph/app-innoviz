import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import {
  AgreementDocType,
  EmptyGuid,
  MainAgreementStatus,
  ManageAgreementAction,
  RefType,
  ROUTE_FUNCTION_GEN,
  ROUTE_RELATED_GEN
} from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, MainAgreementTableItemView, ManageAgreementView, RefIdParm } from 'shared/models/viewModel';
import { MainAgreementTableService } from '../main-agreement-table.service';
import { MainAgreementTableItemCustomComponent } from './main-agreement-table-item-custom.component';
@Component({
  selector: 'main-agreement-table-item',
  templateUrl: './main-agreement-table-item.component.html',
  styleUrls: ['./main-agreement-table-item.component.scss']
})
export class MainAgreementTableItemComponent extends BaseItemComponent<MainAgreementTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  agreementDocTypeOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  creditAppTableOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  withdrawalTableOptions: SelectItems[] = [];
  manageMenuVisibility: number[] = [];

  constructor(
    private service: MainAgreementTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: MainAgreementTableItemCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.baseService.baseDropdown = this.baseDropdown;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {
    let passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setTitle(passingObj);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getAgreementDocTypeEnumDropDown(this.agreementDocTypeOptions);
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
  }
  getById(): Observable<MainAgreementTableItemView> {
    return this.service.getMainAgreementTableById(this.id);
  }
  getInitialData(): Observable<MainAgreementTableItemView> {
    return this.service.getInitialData(this.custom.getProductType());
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.service.getVisibleManageMenu(this.id).subscribe((res) => {
          this.manageMenuVisibility = res;
          this.setRelatedInfoOptions(result);
          this.setFunctionOptions(result);
        });
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    let tempModel: MainAgreementTableItemView = this.isUpdateMode ? model : this.model;
    forkJoin([
      this.custom.getCreditAppDropDown(tempModel),
      this.custom.getBuyerTableDropDown(tempModel),
      this.baseDropdown.getCustomerTableDropDown(),
      this.custom.getWithdrawalTableDropDown(tempModel)
    ]).subscribe(
      ([creditAppTable, buyerTable, customerTable, withdrawalTable]) => {
        this.creditAppTableOptions = creditAppTable;
        this.buyerTableOptions = buyerTable;
        this.customerTableOptions = customerTable;
        this.withdrawalTableOptions = withdrawalTable;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });

    this.custom.getVisibleFieldSet(this.model).subscribe((res) => {
      super.setVisibilityFieldSet(res);
    });
  }
  setRelatedInfoOptions(model: MainAgreementTableItemView): void {
    const statusIsLassThanSigned = this.custom.statusIsLassThanSigned(model);
    const statusIsLassThanOrEqualSigned = this.custom.statusIsLassThanOrEqualSigned(model);
    const allowCreate = statusIsLassThanSigned;
    const allowUpdate = statusIsLassThanSigned;
    const allowDelete = statusIsLassThanSigned;
    let _accessModeView = new AccessModeView();
    let guarantorAccessModeView = new AccessModeView();
    guarantorAccessModeView.canCreate = statusIsLassThanOrEqualSigned;
    guarantorAccessModeView.canView = true;
    guarantorAccessModeView.canDelete = false;
    _accessModeView.canCreate = allowCreate;
    _accessModeView.canView = allowUpdate;
    _accessModeView.canDelete = allowDelete;
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        command: () => {
          const refIdParm: RefIdParm = { refType: RefType.MainAgreement, refGUID: this.model.mainAgreementTableGUID };
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: refIdParm
          });
        }
      },
      {
        label: 'LABEL.ADDENDUM',
        visible: model.agreementDocType !== AgreementDocType.Addendum && model.agreementDocType !== AgreementDocType.NoticeOfCancellation,
        disabled: model.agreementDocType === AgreementDocType.Addendum || model.agreementDocType === AgreementDocType.NoticeOfCancellation,
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ADDENDUM_MAINAGREEMENT_TABLE,
            parameters: {
              agreementDocType: AgreementDocType.Addendum,
              parentDocType: model.agreementDocType,
              statusId: model.documentStatus_StatusId
            }
          })
      },
      {
        label: 'LABEL.AGREEMENT_INFORMATION',
        command: () => {
          this.service.getAgreementTableInfoByMainAgreement(this.id).subscribe((result) => {
            if (isNullOrUndefined(result)) {
              this.toRelatedInfo({ path: `${ROUTE_RELATED_GEN.AGREEMENT_TABLE_INFO}/${EmptyGuid}` });
            } else {
              this.toRelatedInfo({ path: `${ROUTE_RELATED_GEN.AGREEMENT_TABLE_INFO}/${result.agreementTableInfoGUID}` });
            }
          });
        }
      },
      {
        label: 'LABEL.BOOKMARK_DOCUMENT',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.BOOKMARK_DOCUMENT_TRANS, parameters: _accessModeView })
      },
      { label: 'LABEL.CONSORTIUM', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.CONSORTIUM_TRANS }) },
      {
        label: 'LABEL.JOINT_VENTURE',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.JOINT_VENTURE_TRANS })
      },
      {
        label: 'LABEL.MEMO',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.MEMO })
      },
      //   { label: 'LABEL.SERVICE_FEE', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.SERVICE_FEE }) },
      {
        label: 'LABEL.GUARANTOR_AGREEMENT',
        visible: model.agreementDocType !== AgreementDocType.Addendum && model.agreementDocType !== AgreementDocType.NoticeOfCancellation,
        disabled: model.agreementDocType === AgreementDocType.Addendum || model.agreementDocType === AgreementDocType.NoticeOfCancellation,
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.GUARANTOR_AGREEMENT_TABLE,
            parameters: {
              accessModeView: guarantorAccessModeView
            }
          })
      },
      {
        label: 'LABEL.NOTICE_OF_CANCELLATION',
        visible: model.agreementDocType !== AgreementDocType.Addendum && model.agreementDocType !== AgreementDocType.NoticeOfCancellation,
        disabled: model.agreementDocType === AgreementDocType.Addendum || model.agreementDocType === AgreementDocType.NoticeOfCancellation,
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION_MAINAGREEMENT_TABLE,
            parameters: {
              agreementDocType: AgreementDocType.NoticeOfCancellation,
              parentDocType: model.agreementDocType,
              statusId: model.documentStatus_StatusId
            }
          })
      },
      {
        label: 'LABEL.SERVICE_FEE',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.SERVICE_FEE_TRANS,
            parameters: {
              refId: model.internalMainAgreementId,
              taxDate: model.agreementDate,
              productType: model.productType,
              invoiceTable_CustomerTableGUID: model.customerTableGUID
            }
          })
      }
      //   { label: 'LABEL.GUARANTOR_AGREEMENT', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.GUARANTOR_AGREEMENT }) },
      //   { label: 'LABEL.ADDENDUM', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.ADDENDUM }) },
    ];

    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(model): void {
    const showFnLoanRequest = this.custom.isLoanrequestAGMCondition(model);
    this.functionItems = [
      {
        label: 'LABEL.MANAGE',
        items: [
          {
            label: 'LABEL.POST',
            disabled: !this.manageMenuVisibility.some((s) => s === ManageAgreementAction.Post),
            command: () => {
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.POST_AGREEMENT,
                parameters: {
                  refType: RefType.MainAgreement,
                  refGuid: this.id,
                  action: ManageAgreementAction.Post
                }
              });
            }
          },
          {
            label: 'LABEL.SEND',
            disabled: !this.manageMenuVisibility.some((s) => s === ManageAgreementAction.Send),
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.SEND_AGREEMENT,
                parameters: {
                  refType: RefType.MainAgreement,
                  refGuid: this.id,
                  action: ManageAgreementAction.Send
                }
              })
          },
          {
            label: 'LABEL.SIGN',
            disabled: !this.manageMenuVisibility.some((s) => s === ManageAgreementAction.Sign),
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.SIGN_AGREEMENT,
                parameters: {
                  refType: RefType.MainAgreement,
                  refGuid: this.id,
                  action: ManageAgreementAction.Sign
                }
              })
          },
          {
            label: 'LABEL.CLOSE',
            disabled: !this.manageMenuVisibility.some((s) => s === ManageAgreementAction.Close),
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.CLOSE_AGREEMENT,
                parameters: {
                  refType: RefType.MainAgreement,
                  refGuid: this.id,
                  action: ManageAgreementAction.Close
                }
              })
          },
          {
            label: 'LABEL.CANCEL',
            disabled: !this.manageMenuVisibility.some((s) => s === ManageAgreementAction.Cancel),
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.CANCEL_AGREEMENT,
                parameters: {
                  refType: RefType.MainAgreement,
                  refGuid: this.id,
                  action: ManageAgreementAction.Cancel
                }
              })
          }
        ]
      },
      {
        label: 'LABEL.GENERATE',
        visible: showFnLoanRequest,
        items: [
          {
            disabled: !showFnLoanRequest,
            visible: showFnLoanRequest,
            label: 'LABEL.LOAN_REQUEST_AGREEMENT',
            command: () => this.setPreparingFunctionDataAndValidation(ROUTE_FUNCTION_GEN.LOAN_REQUEST_AGREEMENT)
          }
          // { label: 'LABEL.ADDENDUM', command: () => this.setPreparingFunctionDataAndValidation(ROUTE_FUNCTION_GEN.ADDENDUM) },
          // {
          //   label: 'LABEL.NOTICE_OF_CANCELLATION',
          //   command: () => this.setPreparingFunctionDataAndValidation(ROUTE_FUNCTION_GEN.NOTICE_OF_CANCELLATION)
          // }
        ]
      },
      {
        label: 'LABEL.PRINT',
        items: [
          {
            label: 'LABEL.BOOKMARK_DOCUMENT',
            command: () => this.toFunction(this.custom.setPath(ROUTE_FUNCTION_GEN.PRINT_BOOKMARK_DOCUMENT, this.model))
          }
        ]
      }
    ];
    super.setFunctionOptionsMapping();
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateMainAgreementTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createMainAgreementTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onCustomerChange() {
    this.custom.clearCustomerValue(this.model);
    this.creditAppTableOptions.length = 0;
    if (!isNullOrUndefOrEmptyGUID(this.model.customerTableGUID)) {
      this.custom.setCustomerValue(this.model, this.customerTableOptions);
      this.setCreditAppDropDown();
    }
  }
  onCreditAppChange() {
    this.custom.clearCreditAppValue(this.model);
    this.buyerTableOptions.length = 0;
    if (!isNullOrUndefOrEmptyGUID(this.model.creditAppTableGUID)) {
      this.custom.setCreditAppValue(this.model, this.creditAppTableOptions);
      this.setBuyerTableDropDown();
    }
  }
  onBuyerTableChange() {
    this.custom.clearBuyerValue(this.model);
    if (!isNullOrUndefOrEmptyGUID(this.model.buyerTableGUID)) {
      this.custom.setBuyerValue(this.model, this.buyerTableOptions);
    }
  }
  onWithdrawalTableChange() {
    this.custom.clearWithdrawalValue(this.model);
    if (!isNullOrUndefOrEmptyGUID(this.model.withdrawalTableGUID)) {
      this.custom.setWithdrawalValue(this.model, this.withdrawalTableOptions);
    }
  }
  setPreparingFunctionDataAndValidation(path: string): void {
    this.custom.setPreparingFunctionDataAndValidation(path, this.model).subscribe((res) => {
      if (!isNullOrUndefined(res)) {
        this.toFunction(this.custom.setPath(path, this.model));
      }
    });
  }

  //#region update pattern dropdown
  setBuyerTableDropDown() {
    this.baseService.baseDropdown.getBuyerTableByMainAgreementTableDropDown(this.model.creditAppTableGUID).subscribe((result) => {
      this.buyerTableOptions = result;
    });
  }
  setCreditAppDropDown() {
    this.baseService.baseDropdown
      .getCreditAppTableByMainAgreementTableDropDown([
        this.model.customerTableGUID,
        this.model.productType.toString(),
        this.model.mainAgreementTableGUID
      ])
      .subscribe((result) => {
        this.creditAppTableOptions = result;
      });
  }
  //#endregion
}
