import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainAgreementTableItemComponent } from './main-agreement-table-item.component';

describe('MainAgreementTableItemViewComponent', () => {
  let component: MainAgreementTableItemComponent;
  let fixture: ComponentFixture<MainAgreementTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MainAgreementTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainAgreementTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
