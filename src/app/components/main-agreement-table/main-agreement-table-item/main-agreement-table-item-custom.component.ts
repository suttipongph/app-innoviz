import { forkJoin, Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AgreementDocType, EmptyGuid, MainAgreementStatus, ProductType, RefType, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { toMapModel } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, PathParamModel, SelectItems } from 'shared/models/systemModel';
import {
  BuyerTableItemView,
  CreditAppTableItemView,
  CustomerTableItemView,
  MainAgreementTableItemView,
  ManageAgreementView,
  WithdrawalTableItemView
} from 'shared/models/viewModel';
import { PrintSetBookDocTransParm } from 'shared/models/viewModel/printSetBookDocTransParm';
import { PrintSetBookDocTransView } from 'shared/models/viewModel/printSetBookDocTransView';

const readOnlyGroup = [
  'AGREEMENT_DOC_TYPE',
  'CUSTOMER_NAME',
  'DOCUMENT_STATUS_GUID',
  'SIGNING_DATE',
  'MAX_INTEREST_PCT',
  'DOCUMENT_REASON_GUID',
  'PRODUCT_TYPE',
  'CREDIT_APP_REQUEST_TABLE_GUID',
  'CREDIT_LIMIT_TYPE_GUID',
  'REVOLVING',
  'APPROVED_CREDIT_LIMIT',
  'APPROVED_CREDIT_LIMIT_LINE',
  'TOTAL_INTEREST_PCT',
  'CONSORTIUM_TABLE_GUID',
  'BUYER_NAME',
  'REF_MAIN_AGREEMENT_TABLE_GUID',
  'REF_MAIN_AGREEMENT_ID',
  'AGREEMENTEXTENSION',
  'MAIN_AGREEMENT_TABLE_GUID',
  'WITHDRAWAL_DUE_DATE'
];
const readOnlyOnViewGroup = ['CUSTOMER_TABLE_GUID', 'CREDIT_APP_TABLE_GUID'];
const condition1 = ['INTERNAL_MAIN_AGREEMENT_ID'];
const condition2 = ['MAIN_AGREEMENT_ID'];
const condition3 = ['WITHDRAWAL_TABLE_GUID'];
const condition4 = ['BUYER_TABLE_GUID'];

export class MainAgreementTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  isInternalMainAgreementManunal: boolean = false;
  title: string = null;
  getFieldAccessing(model: MainAgreementTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: readOnlyGroup, readonly: true });

    if (isUpdateMode(model.mainAgreementTableGUID)) {
      fieldAccessing.push({ filedIds: readOnlyOnViewGroup, readonly: true });
      fieldAccessing.push({ filedIds: condition1, readonly: true });
      this.isInternalMainAgreementManunal = false;
      // checkProductType
      if (model.productType == ProductType.ProjectFinance) {
        fieldAccessing.push({ filedIds: condition3, readonly: false });
      } else {
        fieldAccessing.push({ filedIds: condition3, readonly: true });
      }
      // checkBuyer
      if (model.productType == ProductType.Leasing || model.productType == ProductType.HirePurchase) {
        fieldAccessing.push({ filedIds: condition4, readonly: false });
      } else {
        fieldAccessing.push({ filedIds: condition4, readonly: true });
      }
      // checkNumberSeq
      if (model.documentStatus_StatusId <= MainAgreementStatus.Draft) {
        return this.validateIsManualNumberSeqMainAgreement(model.productType).pipe(
          map((result) => {
            fieldAccessing.push({ filedIds: condition2, readonly: !result });
            return fieldAccessing;
          })
        );
      } else if (model.documentStatus_StatusId >= MainAgreementStatus.Posted) {
        fieldAccessing.push({ filedIds: condition2, readonly: true });
        return of(fieldAccessing);
      } else {
        return of(fieldAccessing);
      }
    } else {
      fieldAccessing.push({ filedIds: readOnlyOnViewGroup, readonly: false });
      fieldAccessing.push({ filedIds: condition3, readonly: true });
      fieldAccessing.push({ filedIds: condition4, readonly: true });
      return forkJoin([
        this.validateIsManualNumberSeqInternalMainAgreement(model.productType),
        this.validateIsManualNumberSeqMainAgreement(model.productType)
      ]).pipe(
        map(([isInternalMainAgreementManunal, isMainAgreementManunal]) => {
          this.isInternalMainAgreementManunal = isInternalMainAgreementManunal;
          fieldAccessing.push({ filedIds: condition1, readonly: !isInternalMainAgreementManunal });
          fieldAccessing.push({ filedIds: condition2, readonly: !isMainAgreementManunal });
          return fieldAccessing;
        })
      );
    }
  }

  getVisibleFieldSet(model: MainAgreementTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (model.productType == ProductType.Leasing || model.productType == ProductType.HirePurchase) {
      fieldAccessing.push({ filedIds: ['BUYER_INFORMATION'], invisible: false });
    } else {
      fieldAccessing.push({ filedIds: ['BUYER_INFORMATION'], invisible: true });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: MainAgreementTableItemView): Observable<boolean> {
    const accessright = isNullOrUndefOrEmptyGUID(model.mainAgreementTableGUID) ? canCreate : canUpdate;
    let accessLogic = false;
    switch (model.agreementDocType) {
      case AgreementDocType.Addendum:
      case AgreementDocType.NoticeOfCancellation:
        accessLogic = false;
      default:
        accessLogic = model.documentStatus_StatusId < MainAgreementStatus.Signed;
    }
    return of(!(accessLogic && accessright));
  }
  getInitialData(): Observable<MainAgreementTableItemView> {
    let model = new MainAgreementTableItemView();
    model.mainAgreementTableGUID = EmptyGuid;
    return of(model);
  }
  setTitle(passingObj: any) {
    if (!isNullOrUndefined(passingObj)) {
      let agreementDocType: AgreementDocType = passingObj.agreementDocType;
      switch (agreementDocType) {
        case AgreementDocType.Addendum:
          this.title = 'LABEL.ADDENDUM';
          break;
        case AgreementDocType.NoticeOfCancellation:
          this.title = 'LABEL.NOTICE_OF_CANCELLATION';
          break;
        default:
          this.title = 'LABEL.MAIN_AGREEMENT';
          break;
      }
    } else {
      this.title = 'LABEL.MAIN_AGREEMENT';
    }
  }
  validateIsManualNumberSeqInternalMainAgreement(productType: number): Observable<boolean> {
    return this.baseService.service.validateIsManualNumberSeqInternalMainAgreement(productType).pipe(
      map((res) => res),
      catchError((e) => {
        this.baseService.notificationService.showErrorMessageFromResponse(e);
        return e;
      })
    );
  }
  validateIsManualNumberSeqMainAgreement(productType: number): Observable<boolean> {
    return this.baseService.service.validateIsManualNumberSeqMainAgreement(productType).pipe(
      map((res) => res),
      catchError((e) => {
        this.baseService.notificationService.showErrorMessageFromResponse(e);
        return e;
      })
    );
  }
  getProductType(): number {
    const masterRoute = this.baseService.uiService.getMasterRoute();
    let productType = 0;
    switch (masterRoute) {
      case 'factoring':
        productType = ProductType.Factoring;
        break;
      case 'bond':
        productType = ProductType.Bond;
        break;
      case 'hirepurchase':
        productType = ProductType.HirePurchase;
        break;
      case 'lcdlc':
        productType = ProductType.LC_DLC;
        break;
      case 'leasing':
        productType = ProductType.Leasing;
        break;
      case 'projectfinance':
        productType = ProductType.ProjectFinance;
        break;
      default:
        break;
    }
    return productType;
  }
  clearCustomerValue(model: MainAgreementTableItemView) {
    model.customerName = '';
    model.customerAltName = '';
    model.creditAppTableGUID = null;
    model.buyerTableGUID = null;
    this.clearCreditAppValue(model);
    this.clearBuyerValue(model);
  }
  clearCreditAppValue(model: MainAgreementTableItemView) {
    model.creditAppRequestTableGUID = null;
    model.creditAppRequestTable_Values = '';
    model.creditLimitTypeGUID = null;
    model.creditLimitType_Values = '';
    model.creditLimitType_Revolving = false;
    model.approvedCreditLimit = 0;
    model.approvedCreditLimitLine = 0;
    model.totalInterestPct = 0;
    model.consortiumTableGUID = null;
    model.consortiumTable_Values = '';
    model.buyerTableGUID = null;

    this.clearBuyerValue(model);
  }
  clearBuyerValue(model: MainAgreementTableItemView) {
    model.buyerName = '';
    model.buyerAgreementDescription = '';
    model.buyerAgreementReferenceId = '';
  }
  clearWithdrawalValue(model: MainAgreementTableItemView) {
    model.withdrawalTable_DueDate = null;
  }
  setCustomerValue(model: MainAgreementTableItemView, option: SelectItems[]) {
    let customerTable: CustomerTableItemView = option.find((t) => (t.rowData as CustomerTableItemView).customerTableGUID === model.customerTableGUID)
      .rowData as CustomerTableItemView;
    model.customerTable_CustomerId = customerTable.customerId;
    model.customerName = customerTable.name;
    model.customerAltName = customerTable.altName;
  }
  setCreditAppValue(model: MainAgreementTableItemView, option: SelectItems[]) {
    let creditAppTable: CreditAppTableItemView = option.find(
      (t) => (t.rowData as CreditAppTableItemView).creditAppTableGUID === model.creditAppTableGUID
    ).rowData as CreditAppTableItemView;
    model.creditAppRequestTableGUID = creditAppTable.refCreditAppRequestTableGUID;
    model.creditAppRequestTable_Values = creditAppTable.refCreditAppRequestTable_Values;
    model.creditLimitTypeGUID = creditAppTable.creditLimitTypeGUID;
    model.creditLimitType_Values = creditAppTable.creditLimitType_Values;
    model.creditLimitType_Revolving = creditAppTable.creditLimitType_Revolving;
    model.approvedCreditLimit = creditAppTable.approvedCreditLimit;
    model.approvedCreditLimitLine = creditAppTable.sumApprovedCreditLimitLine;
    model.totalInterestPct = creditAppTable.totalInterestPct;
    model.consortiumTableGUID = creditAppTable.consortiumTableGUID;
    model.consortiumTable_Values = creditAppTable.consortiumTable_Values;
  }
  setBuyerValue(model: MainAgreementTableItemView, option: SelectItems[]) {
    this.baseService.service.getBuyerAgreementTableByBuyerTable(model).subscribe((result) => {
      let buyerTable: BuyerTableItemView = option.find((t) => (t.rowData as BuyerTableItemView).buyerTableGUID == model.buyerTableGUID)
        .rowData as BuyerTableItemView;
      model.buyerName = buyerTable.buyerName;
      model.buyerAgreementDescription = result.buyerAgreementDescription;
      model.buyerAgreementReferenceId = result.buyerAgreementReferenceId;
    });
  }
  setWithdrawalValue(model: MainAgreementTableItemView, option: SelectItems[]) {
    let withdrawalTable: WithdrawalTableItemView = option.find(
      (t) => (t.rowData as WithdrawalTableItemView).withdrawalTableGUID == model.withdrawalTableGUID
    ).rowData as WithdrawalTableItemView;
    model.withdrawalTable_DueDate = withdrawalTable.dueDate;
  }
  getCreditAppDropDown(model: MainAgreementTableItemView) {
    if (!isNullOrUndefOrEmptyGUID(model.customerTableGUID)) {
      if (isUpdateMode(model.mainAgreementTableGUID)) {
        return this.baseService.baseDropdown.getCreditAppTableDropDown();
      } else {
        return this.baseService.baseDropdown.getCreditAppTableByMainAgreementTableDropDown([
          model.customerTableGUID,
          model.productType.toString(),
          model.mainAgreementTableGUID
        ]);
      }
    } else {
      return of([]);
    }
  }
  getBuyerTableDropDown(model: MainAgreementTableItemView) {
    if (!isNullOrUndefOrEmptyGUID(model.creditAppTableGUID)) {
      return this.baseService.baseDropdown.getBuyerTableByMainAgreementTableDropDown(model.creditAppTableGUID);
    } else {
      return of([]);
    }
  }
  getWithdrawalTableDropDown(model: MainAgreementTableItemView) {
    if (!isNullOrUndefOrEmptyGUID(model.creditAppTableGUID)) {
      return this.baseService.baseDropdown.getWithdrawalTableByMainAgreementTableDropDown(model.creditAppTableGUID);
    } else {
      return of([]);
    }
  }
  statusIsLassThanSigned(model: MainAgreementTableItemView): boolean {
    return model.documentStatus_StatusId < MainAgreementStatus.Signed;
  }
  statusIsLassThanOrEqualSigned(model: MainAgreementTableItemView): boolean {
    return model.documentStatus_StatusId <= MainAgreementStatus.Signed;
  }
  getPrintSetBookDocTransParamModel(model: MainAgreementTableItemView): PrintSetBookDocTransView {
    const paramModel = new PrintSetBookDocTransView();
    toMapModel(model, paramModel);
    paramModel.refType = RefType.MainAgreement;
    paramModel.refGUID = model.mainAgreementTableGUID;
    paramModel.internalAgreementId = model.internalMainAgreementId;
    paramModel.agreementId = model.mainAgreementId;
    paramModel.agreementDescription = model.description;
    return paramModel;
  }
  setPath(path: string, model: MainAgreementTableItemView): PathParamModel {
    switch (path) {
      case ROUTE_FUNCTION_GEN.PRINT_BOOKMARK_DOCUMENT:
        return {
          path: path,
          parameters: {
            model: this.getPrintSetBookDocTransParamModel(model)
          }
        };
      default:
        return {
          path: path
        };
    }
  }
  setPreparingFunctionDataAndValidation(path: string, model: MainAgreementTableItemView): Observable<any> {
    switch (path) {
      case ROUTE_FUNCTION_GEN.ADDENDUM:
        return this.baseService.service.getGenerateAddendumValidation(model).pipe(
          map((res) => {
            if (res) {
              return { MainAgreementTableItemView: res };
            } else {
              return null;
            }
          })
        );
      case ROUTE_FUNCTION_GEN.NOTICE_OF_CANCELLATION:
        return this.baseService.service.getGenerateNoticeOfCancellationValidation(model).pipe(
          map((res) => {
            if (res) {
              return { MainAgreementTableItemView: res };
            } else {
              return null;
            }
          })
        );
      case ROUTE_FUNCTION_GEN.LOAN_REQUEST_AGREEMENT:
        return this.baseService.service.getGenerateLoanRequestAGMValidation(model);
      default:
        break;
    }
  }
  isLoanrequestAGMCondition(model: MainAgreementTableItemView): boolean {
    if (
      model.agreementDocType === AgreementDocType.New &&
      model.documentStatus_StatusId === MainAgreementStatus.Signed &&
      model.productType === ProductType.ProjectFinance
    ) {
      return true;
    } else {
      return false;
    }
  }
}
