import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { AgreementDocType, ColumnType, MainAgreementStatus, ROUTE_FUNCTION_GEN, SortType } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { MainAgreementTableItemView, MainAgreementTableListView } from 'shared/models/viewModel';
import { MainAgreementTableService } from '../main-agreement-table.service';
import { MainAgreementTableListCustomComponent } from './main-agreement-table-list-custom.component';

@Component({
  selector: 'main-agreement-table-list',
  templateUrl: './main-agreement-table-list.component.html',
  styleUrls: ['./main-agreement-table-list.component.scss']
})
export class MainAgreementTableListComponent extends BaseListComponent<MainAgreementTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  agreementDocTypeOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  creditAppTableOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  withdrawalTableOptions: SelectItems[] = [];
  defualtBitOption: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  ParentModel: MainAgreementTableItemView = null;
  passingObj: any;

  constructor(
    public custom: MainAgreementTableListCustomComponent,
    private service: MainAgreementTableService,
    private currentActivatedRoute: ActivatedRoute,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {
    this.passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setTitle(this.passingObj);
    this.custom.checkNoticeOfCancel(this.passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    if (this.custom.isShowFunction) {
      this.custom.getParentModel(this.service, this.parentId).subscribe((result) => {
        this.ParentModel = result;
        this.setFunctionOptions();
      });
    }
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getAgreementDocTypeEnumDropDown(this.agreementDocTypeOptions);
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getDefaultBitDropDown(this.defualtBitOption);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
    this.baseDropdown.getCreditAppTableDropDown(this.creditAppTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'mainAgreementTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.INTERNAL_MAIN_AGREEMENT_ID',
        textKey: 'internalMainAgreementId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC,
        sortingKey: 'internalMainAgreementId',
        searchingKey: 'internalMainAgreementId'
      },
      {
        label: 'LABEL.MAIN_AGREEMENT_ID',
        textKey: 'mainAgreementId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.AGREEMENT_DATE',
        textKey: 'agreementDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CREDIT_APPLICATION_ID',
        textKey: 'creditAppTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditAppTableOptions,
        searchingKey: 'creditAppTableGUID',
        sortingKey: 'creditAppTable_CreditAppId'
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId'
      },
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: 'LABEL.AGREEMENT_DOCUMENT_TYPE',
        textKey: 'agreementDocType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.agreementDocTypeOptions
      },
      {
        label: 'LABEL.CREDIT_LIMIT_TYPE_ID',
        textKey: 'creditLimitType_CreditLimitTypeId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.REVOLVING',
        textKey: 'creditLimitType_Revolving',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defualtBitOption
      },
      {
        label: 'LABEL.STATUS_ID',
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentStatusOptions,
        sortingKey: 'documentStatus_StatusId',
        searchingKey: 'documentStatusGUID'
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<MainAgreementTableListView>> {
    return this.service.getMainAgreementTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteMainAgreementTable(row));
  }
  setFunctionOptions(): void {
    let agreementDocType: AgreementDocType = this.passingObj.agreementDocType;
    let parentDocType: AgreementDocType = this.passingObj.parentDocType;
    let statusId: string = this.passingObj.statusId;
    switch (agreementDocType) {
      case AgreementDocType.NoticeOfCancellation:
        this.functionItems = [
          {
            label: 'LABEL.GENERATE',
            items: [
              {
                label: 'LABEL.NOTICE_OF_CANCELLATION',
                visible: true,
                disabled: !(parentDocType == AgreementDocType.New && statusId == MainAgreementStatus.Signed),
                command: () => this.setPreparingFunctionDataAndValidation(ROUTE_FUNCTION_GEN.NOTICE_OF_CANCELLATION)
              }
            ]
          }
        ];
        break;
      case AgreementDocType.Addendum:
        this.functionItems = [
          {
            label: 'LABEL.GENERATE',
            items: [
              {
                label: 'LABEL.ADDENDUM',
                visible: true,
                disabled: !(parentDocType == AgreementDocType.New && statusId == MainAgreementStatus.Signed),
                command: () => this.setPreparingFunctionDataAndValidation(ROUTE_FUNCTION_GEN.ADDENDUM)
              }
            ]
          }
        ];
        break;
      default:
        this.functionItems = [
          {
            label: 'LABEL.GENERATE',
            items: []
          }
        ];
        break;
    }

    super.setFunctionOptionsMapping();
  }
  setPreparingFunctionDataAndValidation(path: string): void {
    this.custom.setPreparingFunctionDataAndValidation(this.service, path, this.ParentModel).subscribe((res) => {
      if (!isNullOrUndefined(res)) {
        this.toFunction(this.custom.setPath(path));
      }
    });
  }
}
