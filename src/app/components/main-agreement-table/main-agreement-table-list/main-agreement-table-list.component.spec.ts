import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MainAgreementTableListComponent } from './main-agreement-table-list.component';

describe('MainAgreementTableListViewComponent', () => {
  let component: MainAgreementTableListComponent;
  let fixture: ComponentFixture<MainAgreementTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MainAgreementTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainAgreementTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
