import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AgreementDocType, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel, PathParamModel } from 'shared/models/systemModel';
import { MainAgreementTableItemView } from 'shared/models/viewModel';
import { MainAgreementTableService } from '../main-agreement-table.service';
export class MainAgreementTableListCustomComponent {
  baseService: BaseServiceModel<any>;
  title: string = null;
  isShowFunction: boolean = false;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setTitle(passingObj: any) {
    if (!isNullOrUndefined(passingObj)) {
      let agreementDocType: AgreementDocType = passingObj.agreementDocType;
      switch (agreementDocType) {
        case AgreementDocType.NoticeOfCancellation:
          this.title = 'LABEL.NOTICE_OF_CANCELLATION';
          break;
        case AgreementDocType.Addendum:
          this.title = 'LABEL.ADDENDUM';
          break;
        default:
          this.title = 'LABEL.MAIN_AGREEMENT';
          break;
      }
    } else {
      this.title = 'LABEL.MAIN_AGREEMENT';
    }
  }
  checkNoticeOfCancel(passingObj: any): any {
    if (!isNullOrUndefined(passingObj)) {
      let agreementDocType: AgreementDocType = passingObj.agreementDocType;
      switch (agreementDocType) {
        case AgreementDocType.NoticeOfCancellation:
          this.isShowFunction = true;
          break;
        case AgreementDocType.Addendum:
          this.isShowFunction = true;
          break;
        default:
          this.isShowFunction = false;
          break;
      }
    } else {
      this.isShowFunction = false;
    }
  }
  setPreparingFunctionDataAndValidation(service: MainAgreementTableService, path: string, model: MainAgreementTableItemView): Observable<any> {
    switch (path) {
      case ROUTE_FUNCTION_GEN.ADDENDUM:
        return service.getGenerateAddendumValidation(model).pipe(
          map((res) => {
            if (res) {
              return { MainAgreementTableItemView: res };
            } else {
              return null;
            }
          })
        );
      case ROUTE_FUNCTION_GEN.NOTICE_OF_CANCELLATION:
        return service.getGenerateNoticeOfCancellationValidation(model).pipe(
          map((res) => {
            if (res) {
              return { MainAgreementTableItemView: res };
            } else {
              return null;
            }
          })
        );
      default:
        break;
    }
  }
  setPath(path: string): PathParamModel {
        return {
          path: path
        };
  }
  getParentModel(service: MainAgreementTableService, id: string): Observable<MainAgreementTableItemView> {
   return service.getMainAgreementTableById(id);
  }
}
