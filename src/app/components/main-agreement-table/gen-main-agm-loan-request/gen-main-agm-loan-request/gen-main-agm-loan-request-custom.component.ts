import { GenMainAgmLoanRequestService } from './../gen-main-agm-loan-request.service';
import { Observable, of } from 'rxjs';
import { EmptyGuid, ProductType } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { GenMainAgmLoanRequestResultView, GenMainAgmLoanRequestView } from 'shared/models/viewModel';

const firstGroup = [
  'INTERNAL_MAIN_AGREEMENT_ID',
  'MAIN_AGREEMENT_ID',
  'MAIN_AGREEMENT_DESCRIPTION',
  'CUSTOMER_GUID',
  'CUSTOMER_NAME',
  'BUYER_GUID',
  'BUYER_NAME',
  'CREDIT_APP_TABLE_GUID',
  'CREDIT_APP_REQUEST_TYPE',
  'CREDIT_LIMIT_TYPE_GUID',
  'REQUEST_DATE',
  'CREDIT_APP_REQUEST_DESCRIPTION'
];

const secondGroup = ['LOAN_REQUEST_INTERNAL_MAIN_AGREEMENT_ID'];

export class GenMainAgmLoanRequestCustomComponent {
  baseService: BaseServiceModel<any>;
  isMandatory_loanrequestInternalMainAgreementId: boolean = false;
  getFieldAccessing(model: GenMainAgmLoanRequestView, genMainAgmLoanRequestService: GenMainAgmLoanRequestService): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    return new Observable((observer) => {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      this.validateIsManualNumberSeq(genMainAgmLoanRequestService, this.getProductType()).subscribe((result) => {
        if (result) {
          fieldAccessing.push({ filedIds: secondGroup, readonly: false });
          this.isMandatory_loanrequestInternalMainAgreementId = true;
          observer.next(fieldAccessing);
        } else {
          fieldAccessing.push({ filedIds: secondGroup, readonly: true });
          this.isMandatory_loanrequestInternalMainAgreementId = false;
          observer.next(fieldAccessing);
        }
      });
    });
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<GenMainAgmLoanRequestView> {
    let model = new GenMainAgmLoanRequestView();
    model.mainAgreementTableGUID = EmptyGuid;
    return of(model);
  }
  validateIsManualNumberSeq(service: GenMainAgmLoanRequestService, productType: number): Observable<boolean> {
    return service.validateIsManualNumberSeq(productType);
  }
  getProductType(): number {
    const masterRoute = this.baseService.uiService.getMasterRoute();
    let productType = 0;
    switch (masterRoute) {
      case 'factoring':
        productType = ProductType.Factoring;
        break;
      case 'bond':
        productType = ProductType.Bond;
        break;
      case 'hirepurchase':
        productType = ProductType.HirePurchase;
        break;
      case 'lcdlc':
        productType = ProductType.LC_DLC;
        break;
      case 'leasing':
        productType = ProductType.Leasing;
        break;
      case 'projectfinance':
        productType = ProductType.ProjectFinance;
        break;
      default:
        break;
    }
    return productType;
  }
  getModelParam(model: GenMainAgmLoanRequestView, id: string): Observable<GenMainAgmLoanRequestResultView> {
    const param: GenMainAgmLoanRequestView = new GenMainAgmLoanRequestView();
    param.mainAgreementTableGUID = id;
    param.agreementDate = model.agreementDate;
    param.buyerGUID = model.buyerGUID;
    param.buyerName = model.buyerName;
    param.customerGUID = model.customerGUID;
    param.customerName = model.customerName;
    param.description = model.description;
    param.internalMainAgreementId = model.internalMainAgreementId;
    param.mainAgreementDescription = model.mainAgreementDescription;
    param.companyGUID = model.companyGUID;
    param.creditAppRequestDescription = model.creditAppRequestDescription;
    param.creditAppRequestTableGUID = model.creditAppRequestTableGUID;
    param.creditAppRequestType = model.creditAppRequestType;
    param.creditAppTableGUID = model.creditAppTableGUID;
    param.creditLimitTypeGUID = model.creditLimitTypeGUID;
    param.loanrequestInternalMainAgreementId = model.loanrequestInternalMainAgreementId;
    param.mainAgreementDescription = model.mainAgreementDescription;
    param.mainAgreementId = model.mainAgreementId;
    param.requestDate = model.requestDate;

    return of(param);
  }
}
