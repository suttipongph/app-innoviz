import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenMainAgmLoanRequestComponent } from './gen-main-agm-loan-request.component';

describe('GenMainAgmLoanRequestViewComponent', () => {
  let component: GenMainAgmLoanRequestComponent;
  let fixture: ComponentFixture<GenMainAgmLoanRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GenMainAgmLoanRequestComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenMainAgmLoanRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
