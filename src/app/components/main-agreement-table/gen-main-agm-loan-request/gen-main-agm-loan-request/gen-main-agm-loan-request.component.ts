import { GenMainAgmLoanRequestService } from './../gen-main-agm-loan-request.service';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { GenMainAgmLoanRequestView } from 'shared/models/viewModel';
import { GenMainAgmLoanRequestCustomComponent } from './gen-main-agm-loan-request-custom.component';
@Component({
  selector: 'gen-main-agm-loan-request',
  templateUrl: './gen-main-agm-loan-request.component.html',
  styleUrls: ['./gen-main-agm-loan-request.component.scss']
})
export class GenMainAgmLoanRequestComponent extends BaseItemComponent<GenMainAgmLoanRequestView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  creditAppRequestTableOptions: SelectItems[] = [];
  creditAppRequestTypeOptions: SelectItems[] = [];

  constructor(
    private service: GenMainAgmLoanRequestService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: GenMainAgmLoanRequestCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getCreditAppRequestTypeEnumDropDown(this.creditAppRequestTypeOptions);
  }
  getById(): Observable<GenMainAgmLoanRequestView> {
    return this.service.getGenMainAgmLoanRequestById(this.id);
  }
  getInitialData(): Observable<GenMainAgmLoanRequestView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(this.service.getCreditAppRequestTableDropDown(model.mainAgreementTableGUID)).subscribe(
      ([creditAppRequestTable]) => {
        this.creditAppRequestTableOptions = creditAppRequestTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model, this.service).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false, this.model);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.custom.getModelParam(this.model, this.id).subscribe((model) => {
          this.onSubmit(true, model);
        });
      }
    });
  }
  onSubmit(isColsing: boolean, model: GenMainAgmLoanRequestView): void {
    super.onExecuteFunction(this.service.createGenMainAgmLoanRequest(model));
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onCreditAppRequestIDChange(id: string): void {
    if (!isNullOrUndefined(id)) {
      this.service.getCreditAppRequestIDCById(id).subscribe((res) => {
        this.model.creditAppRequestType = res.creditAppRequestType;
        this.model.creditLimitType_Values = res.creditLimitType_Values;
        this.model.requestDate = res.requestDate;
        this.model.creditAppRequestDescription = res.creditAppRequestDescription;
      });
    }
  }
}
