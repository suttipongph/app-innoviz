import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult, SelectItems } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { GenMainAgmLoanRequestView } from 'shared/models/viewModel';
@Injectable()
export class GenMainAgmLoanRequestService {
  serviceKey = 'genMainAgmLoanRequestGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getGenMainAgmLoanRequestToList(search: SearchParameter): Observable<SearchResult<GenMainAgmLoanRequestView>> {
    const url = `${this.servicePath}/GetGenMainAgmLoanRequestList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteGenMainAgmLoanRequest(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteGenMainAgmLoanRequest`;
    return this.dataGateway.delete(url, row);
  }
  getGenMainAgmLoanRequestById(id: string): Observable<GenMainAgmLoanRequestView> {
    const url = `${this.servicePath}/GetGenMainAgmLoanRequestById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createGenMainAgmLoanRequest(vmModel: GenMainAgmLoanRequestView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateGenMainAgmLoanRequest`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateGenMainAgmLoanRequest(vmModel: GenMainAgmLoanRequestView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateGenMainAgmLoanRequest`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getCreditAppRequestTableDropDown(id: string): Observable<SelectItems[]> {
    console.log(id);
    const url = `${this.servicePath}/GetCreditAppRequestTableDropDownLoanRequest/id=${id}`;
    return this.dataGateway.get(url);
  }
  validateIsManualNumberSeq(productType: number): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeq/productType=${productType}`;
    return this.dataGateway.get(url);
  }
  getCreditAppRequestIDCById(id: string): Observable<GenMainAgmLoanRequestView> {
    const url = `${this.servicePath}/GetCreditAppRequestIDCById/id=${id}`;
    return this.dataGateway.get(url);
  }
}
