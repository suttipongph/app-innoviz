import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GenMainAgmLoanRequestService } from './gen-main-agm-loan-request.service';
import { GenMainAgmLoanRequestView } from 'shared/models/viewModel';
import { GenMainAgmLoanRequestCustomComponent } from './gen-main-agm-loan-request/gen-main-agm-loan-request-custom.component';
import { GenMainAgmLoanRequestComponent } from './gen-main-agm-loan-request/gen-main-agm-loan-request.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [GenMainAgmLoanRequestComponent],
  providers: [GenMainAgmLoanRequestService, GenMainAgmLoanRequestCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [GenMainAgmLoanRequestComponent]
})
export class GenMainAgmLoanRequestComponentModule {}
