import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainAgreementTableService } from './main-agreement-table.service';
import { MainAgreementTableListComponent } from './main-agreement-table-list/main-agreement-table-list.component';
import { MainAgreementTableItemComponent } from './main-agreement-table-item/main-agreement-table-item.component';
import { MainAgreementTableItemCustomComponent } from './main-agreement-table-item/main-agreement-table-item-custom.component';
import { MainAgreementTableListCustomComponent } from './main-agreement-table-list/main-agreement-table-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [MainAgreementTableListComponent, MainAgreementTableItemComponent],
  providers: [MainAgreementTableService, MainAgreementTableItemCustomComponent, MainAgreementTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [MainAgreementTableListComponent, MainAgreementTableItemComponent]
})
export class MainAgreementTableComponentModule {}
