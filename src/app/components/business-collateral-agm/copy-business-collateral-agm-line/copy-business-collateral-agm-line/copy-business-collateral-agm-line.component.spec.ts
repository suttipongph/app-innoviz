import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopyBusinessCollateralAgmLineComponent } from './copy-business-collateral-agm-line.component';

describe('CopyBusinessCollateralAgmLineViewComponent', () => {
  let component: CopyBusinessCollateralAgmLineComponent;
  let fixture: ComponentFixture<CopyBusinessCollateralAgmLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CopyBusinessCollateralAgmLineComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyBusinessCollateralAgmLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
