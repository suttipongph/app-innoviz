import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CopyBusinessCollateralAgmLineView } from 'shared/models/viewModel';

const firstGroup = ['INTERNAL_BUSINEE_COLLATERAL_ID'];

export class CopyBusinessCollateralAgmLineCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CopyBusinessCollateralAgmLineView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<CopyBusinessCollateralAgmLineView> {
    let model = new CopyBusinessCollateralAgmLineView();
    model.copyBusinessCollateralAgmLineGUID = EmptyGuid;
    return of(model);
  }
}
