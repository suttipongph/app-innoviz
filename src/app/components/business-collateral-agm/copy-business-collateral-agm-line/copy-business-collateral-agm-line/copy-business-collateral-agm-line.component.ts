import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { Confirmation } from 'shared/models/primeModel';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { CopyBusinessCollateralAgmLineView } from 'shared/models/viewModel';
import { CopyBusinessCollateralAgmLineService } from '../copy-business-collateral-agm-line.service';
import { CopyBusinessCollateralAgmLineCustomComponent } from './copy-business-collateral-agm-line-custom.component';
@Component({
  selector: 'copy-business-collateral-agm-line',
  templateUrl: './copy-business-collateral-agm-line.component.html',
  styleUrls: ['./copy-business-collateral-agm-line.component.scss']
})
export class CopyBusinessCollateralAgmLineComponent extends BaseItemComponent<CopyBusinessCollateralAgmLineView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  constructor(
    private service: CopyBusinessCollateralAgmLineService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CopyBusinessCollateralAgmLineCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<CopyBusinessCollateralAgmLineView> {
    return this.service.getCopyBusinessCollateralAgmLineById(this.id);
  }
  getInitialData(): Observable<CopyBusinessCollateralAgmLineView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSaveAndClose(validation: FormValidationModel): void {
    if (validation.isValid) {
      this.getDataValidation().subscribe((res) => {
        if (res) {
          const confirmation: Confirmation = {
            header: this.baseService.translate.instant('CONFIRM.CONFIRM'),
            message: this.baseService.translate.instant('CONFIRM.90002', [
              this.baseService.translate.instant('LABEL.BUSINESS_COLLATERAL_AGREEMENT_LINE')
            ])
          };
          this.baseService.notificationService.showConfirmDialog(confirmation);
          this.baseService.notificationService.isAccept.subscribe((isConfirm) => {
            if (isConfirm) {
              this.onSubmit(true);
            }
          });
        } else {
          this.onSubmit(true);
        }
      });
    }
  }
  onSubmit(isColsing: boolean): void {
    super.onExecuteFunction(this.service.updateCopyBusinessCollateralAgmLine(this.model));
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  getDataValidation(): Observable<boolean> {
    return this.service.ValidateCopyBusinessCollateralAgmLine(this.model.copyBusinessCollateralAgmLineGUID);
  }
  onClose(): void {
    super.onBack();
  }
}
