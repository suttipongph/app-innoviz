import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CopyBusinessCollateralAgmLineView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CopyBusinessCollateralAgmLineService {
  serviceKey = 'copyBusinessCollateralAgmLineGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCopyBusinessCollateralAgmLineToList(search: SearchParameter): Observable<SearchResult<CopyBusinessCollateralAgmLineView>> {
    const url = `${this.servicePath}/GetCopyBusinessCollateralAgmLineList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCopyBusinessCollateralAgmLine(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCopyBusinessCollateralAgmLine`;
    return this.dataGateway.delete(url, row);
  }
  getCopyBusinessCollateralAgmLineById(id: string): Observable<CopyBusinessCollateralAgmLineView> {
    const url = `${this.servicePath}/GetCopyBusinessCollateralAgmLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCopyBusinessCollateralAgmLine(vmModel: CopyBusinessCollateralAgmLineView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCopyBusinessCollateralAgmLine`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCopyBusinessCollateralAgmLine(vmModel: CopyBusinessCollateralAgmLineView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCopyBusinessCollateralAgmLine`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  ValidateCopyBusinessCollateralAgmLine(id: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateCopyBusinessCollateralAgmLine/id=${id}`;
    return this.dataGateway.get(url);
  }
}
