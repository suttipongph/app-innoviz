import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CopyBusinessCollateralAgmLineService } from './copy-business-collateral-agm-line.service';
import { CopyBusinessCollateralAgmLineComponent } from './copy-business-collateral-agm-line/copy-business-collateral-agm-line.component';
import { CopyBusinessCollateralAgmLineCustomComponent } from './copy-business-collateral-agm-line/copy-business-collateral-agm-line-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CopyBusinessCollateralAgmLineComponent],
  providers: [CopyBusinessCollateralAgmLineService, CopyBusinessCollateralAgmLineCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CopyBusinessCollateralAgmLineComponent]
})
export class CopyBusinessCollateralAgmLineComponentModule {}
