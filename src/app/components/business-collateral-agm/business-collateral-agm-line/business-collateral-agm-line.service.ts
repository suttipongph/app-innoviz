import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, BusinessCollateralAgmLineItemView, BusinessCollateralAgmLineListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class BusinessCollateralAgmLineService {
  serviceKey = 'businessCollateralAgmLineGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBusinessCollateralAgmLineToList(search: SearchParameter): Observable<SearchResult<BusinessCollateralAgmLineListView>> {
    const url = `${this.servicePath}/GetBusinessCollateralAgmLineList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBusinessCollateralAgmLine(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBusinessCollateralAgmLine`;
    return this.dataGateway.delete(url, row);
  }
  getBusinessCollateralAgmLineById(id: string): Observable<BusinessCollateralAgmLineItemView> {
    const url = `${this.servicePath}/GetBusinessCollateralAgmLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBusinessCollateralAgmLine(vmModel: BusinessCollateralAgmLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBusinessCollateralAgmLine`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBusinessCollateralAgmLine(vmModel: BusinessCollateralAgmLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBusinessCollateralAgmLine`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(businessCollateralAgmId: string): Observable<BusinessCollateralAgmLineItemView> {
    const url = `${this.servicePath}/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId=${businessCollateralAgmId}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByParent(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id=${id}`;
    return this.dataGateway.get(url);
  }
}
