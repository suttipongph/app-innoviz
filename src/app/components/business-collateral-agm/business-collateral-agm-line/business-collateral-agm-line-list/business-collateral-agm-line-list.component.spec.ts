import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessCollateralAgmLineListComponent } from './business-collateral-agm-line-list.component';

describe('BusinessCollateralAgmLineListViewComponent', () => {
  let component: BusinessCollateralAgmLineListComponent;
  let fixture: ComponentFixture<BusinessCollateralAgmLineListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BusinessCollateralAgmLineListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCollateralAgmLineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
