import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView } from 'shared/models/viewModel';
export class BusinessCollateralAgmLineListCustomComponent {
  baseService: BaseServiceModel<any>;
  parentId = null;
  accessModeView: AccessModeView = new AccessModeView();
  title = null;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  getAccessModeByParent(parentId: string): Observable<any> {
    return this.baseService.service.getAccessModeByParent(parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
        this.accessModeView.canView = true;
      })
    );
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }

  getParentKey(): string {
    let activeParentName = this.baseService.uiService.getRelatedInfoActiveTableName();
    switch (activeParentName) {
      case ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
      case ROUTE_RELATED_GEN.ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
        return this.baseService.uiService.getKey(activeParentName);
      default:
        return this.baseService.uiService.getKey(ROUTE_MASTER_GEN.BUSINESS_COLLATERAL_AGM_TABLE);
    }
  }

  setTitle() {
    let activeParentName = this.baseService.uiService.getRelatedInfoActiveTableName();
    switch (activeParentName) {
      case ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
        this.title = 'LABEL.NOTICE_OF_CANCELLATION_LINE';
        break;
      case ROUTE_RELATED_GEN.ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
        this.title = 'LABEL.ADDENDUM_LINE';
        break;
      default:
        this.title = 'LABEL.BUSINESS_COLLATERAL_AGREEMENT_LINE';
        break;
    }
  }
}
