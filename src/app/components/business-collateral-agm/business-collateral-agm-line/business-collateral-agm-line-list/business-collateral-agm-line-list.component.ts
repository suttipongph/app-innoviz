import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { BusinessCollateralAgmLineListView } from 'shared/models/viewModel';
import { BusinessCollateralAgmLineService } from '../business-collateral-agm-line.service';
import { BusinessCollateralAgmLineListCustomComponent } from './business-collateral-agm-line-list-custom.component';

@Component({
  selector: 'business-collateral-agm-line-list',
  templateUrl: './business-collateral-agm-line-list.component.html',
  styleUrls: ['./business-collateral-agm-line-list.component.scss']
})
export class BusinessCollateralAgmLineListComponent extends BaseListComponent<BusinessCollateralAgmLineListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  bankGroupOptions: SelectItems[] = [];
  bankTypeOptions: SelectItems[] = [];
  businessCollateralSubTypeOptions: SelectItems[] = [];
  businessCollateralTypeOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  creditAppReqBusinessCollateralOptions: SelectItems[] = [];
  defaultOptions: SelectItems[] = [];

  constructor(public custom: BusinessCollateralAgmLineListCustomComponent, private service: BusinessCollateralAgmLineService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.custom.getParentKey();
  }
  ngOnInit(): void {
    this.custom.setTitle();
    this.accessRightChildPage = 'BusinessCollateralAgmLineListPage';
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.getAccessModeByParent(this.parentId).subscribe((result) => {
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getDefaultBitDropDown(this.defaultOptions);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBusinessCollateralSubTypeDropDown(this.businessCollateralSubTypeOptions);
    this.baseDropdown.getBusinessCollateralTypeDropDown(this.businessCollateralTypeOptions);
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'businessCollateralAgmLineGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.LINE',
        textKey: 'lineNum',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BUSINESS_COLLATERAL_TYPE_ID',
        textKey: 'businessCollateralType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.businessCollateralTypeOptions,
        searchingKey: 'businessCollateralTypeGUID',
        sortingKey: 'businessCollateralType_BusinessCollateralTypeId'
      },
      {
        label: 'LABEL.BUSINESS_COLLATERAL_SUBTYPE_ID',
        textKey: 'businessCollateralSubType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.businessCollateralSubTypeOptions,
        searchingKey: 'businessCollateralSubTypeGUID',
        sortingKey: 'businessCollateralSubType_BusinessCollateralSubTypeId'
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BUSINESS_COLLATERAL_VALUE',
        textKey: 'businessCollateralValue',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: 'LABEL.CANCELLED',
        textKey: 'cancelled',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defaultOptions
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerTableOptions,
        searchingKey: 'buyerTableGUID',
        sortingKey: 'buyerTable_BuyerId'
      },
      {
        label: null,
        textKey: 'businessCollateralAgmTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<BusinessCollateralAgmLineListView>> {
    return this.service.getBusinessCollateralAgmLineToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteBusinessCollateralAgmLine(row));
  }
}
