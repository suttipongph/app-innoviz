import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BusinessCollateralAgmLineService } from './business-collateral-agm-line.service';
import { BusinessCollateralAgmLineListComponent } from './business-collateral-agm-line-list/business-collateral-agm-line-list.component';
import { BusinessCollateralAgmLineItemComponent } from './business-collateral-agm-line-item/business-collateral-agm-line-item.component';
import { BusinessCollateralAgmLineItemCustomComponent } from './business-collateral-agm-line-item/business-collateral-agm-line-item-custom.component';
import { BusinessCollateralAgmLineListCustomComponent } from './business-collateral-agm-line-list/business-collateral-agm-line-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BusinessCollateralAgmLineListComponent, BusinessCollateralAgmLineItemComponent],
  providers: [BusinessCollateralAgmLineService, BusinessCollateralAgmLineItemCustomComponent, BusinessCollateralAgmLineListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BusinessCollateralAgmLineListComponent, BusinessCollateralAgmLineItemComponent]
})
export class BusinessCollateralAgmLineComponentModule {}
