import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessCollateralAgmLineItemComponent } from './business-collateral-agm-line-item.component';

describe('BusinessCollateralAgmLineItemViewComponent', () => {
  let component: BusinessCollateralAgmLineItemComponent;
  let fixture: ComponentFixture<BusinessCollateralAgmLineItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BusinessCollateralAgmLineItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCollateralAgmLineItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
