import { Confirmation } from 'primeng/api';
import { Observable, of } from 'rxjs';
import {
  AgreementDocType,
  AppConst,
  BusinessCollateralAgreementStatus,
  EmptyGuid,
  IdentificationType,
  ROUTE_MASTER_GEN,
  ROUTE_RELATED_GEN
} from 'shared/constants';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode, resetModelToDefualt } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { BusinessCollateralAgmLineItemView, BuyerTableItemView, CreditAppReqBusinessCollateralItemView } from 'shared/models/viewModel';

const firstGroup = [
  'BUSINESS_COLLATERAL_AGM_TABLE_GUID',
  'BUSINESS_COLLATERAL_AGM_ID',
  'LINE_NUM',
  'CREDIT_APP_REQ_BUSINESS_COLLATERAL_GUID',
  'BUYER_NAME',
  'CANCELLED'
];
const readOnlyViewGroup = ['CREDIT_APP_REQ_BUSINESS_COLLATERAL_GUID'];
const originalGroup = ['ORIGINAL_BUSINESS_COLLATERAL_AGREEMENT_TABLE_GUID', 'ORIGINAL_BUSINESS_COLLATERAL_AGREEMENT_LINE_GUID'];

export class BusinessCollateralAgmLineItemCustomComponent {
  baseService: BaseServiceModel<any>;
  businessCollateralAgmTableCreditAppRequestTableGUID = null;
  isNewConfirmChange: boolean = true;
  isShowOriginalInfomation: boolean = true;
  title: string = null;
  getFieldAccessing(model: BusinessCollateralAgmLineItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (model.businessCollateralAgmTable_AgreementDocType === AgreementDocType.NoticeOfCancellation) {
      this.isShowOriginalInfomation = true;
      fieldAccessing.push({ filedIds: [AppConst.ALL_ID], readonly: true });
      if (isNullOrUndefOrEmptyGUID(model.businessCollateralAgmLineGUID)) {
        fieldAccessing.push({ filedIds: originalGroup, readonly: false });
      }
    } else {
      this.isShowOriginalInfomation = false;
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      if (isNullOrUndefOrEmptyGUID(model.businessCollateralAgmLineGUID)) {
        fieldAccessing.push({ filedIds: readOnlyViewGroup, readonly: false });
      } else {
        fieldAccessing.push({ filedIds: readOnlyViewGroup, readonly: true });
      }
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  canActiveByStatus(status: string): boolean {
    return status < BusinessCollateralAgreementStatus.Signed;
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BusinessCollateralAgmLineItemView): Observable<boolean> {
    let accessright = false;
    if (model.businessCollateralAgmTable_AgreementDocType === AgreementDocType.NoticeOfCancellation) {
      accessright = isNullOrUndefOrEmptyGUID(model.businessCollateralAgmLineGUID) ? canCreate : false;
    } else {
      accessright = isNullOrUndefOrEmptyGUID(model.businessCollateralAgmLineGUID) ? canCreate : canUpdate;
    }
    const accessLogic = this.canActiveByStatus(model.documentStatus_StatusId);
    return of(!(accessLogic && accessright));
  }
  getInitialData(): Observable<BusinessCollateralAgmLineItemView> {
    let model = new BusinessCollateralAgmLineItemView();
    model.businessCollateralAgmLineGUID = EmptyGuid;
    return of(model);
  }
  getBusinessCollateralSubTypeDropDown(model: BusinessCollateralAgmLineItemView): Observable<SelectItems[]> {
    if (isNullOrUndefined(model)) {
      return of([]);
    } else {
      return this.baseService.baseDropdown.getBusinessCollateralSubTypeByBusinessCollateralTypDropDown(model.businessCollateralTypeGUID);
    }
  }
  clearModelByCollateralType(model: BusinessCollateralAgmLineItemView, businessCollateralTypeOption: SelectItems[]) {
    model.businessCollateralSubTypeGUID = null;
    businessCollateralTypeOption.length = 0;
  }
  clearModelOriginalbusinesscollateralAgmline(model: BusinessCollateralAgmLineItemView) {
    model.originalBusinessCollateralAgreementLineGUID = '';
  }
  clearModelByBuyer(model: BusinessCollateralAgmLineItemView) {
    model.buyerName = '';
    model.buyerTaxIdentificationId = '';
  }
  setModelByBuyer(model: BusinessCollateralAgmLineItemView, buyerTableOption: SelectItems[]) {
    if (!isNullOrUndefOrEmptyGUID(model.buyerTableGUID)) {
      let buyer: BuyerTableItemView = buyerTableOption.find((t) => t.value == model.buyerTableGUID).rowData as BuyerTableItemView;
      model.buyerName = buyer.buyerName;
      model.buyerTaxIdentificationId = buyer.identificationType === IdentificationType.TaxId ? buyer.taxId : buyer.passportId;
    }
  }
  clearModelByCreditAppRequestBusinessCollateral(model: BusinessCollateralAgmLineItemView) {
    resetModelToDefualt(model, this.baseService.dataGateway.types, [
      'businessCollateralAgmTable_InternalBusinessCollateralAgmId',
      'businessCollateralAgmTable_BusinessCollateralAgmId',
      'lineNum',
      'creditAppReqBusinessCollateralGUID',
      'businessCollateralAgmLineGUID',
      'businessCollateralAgmTableGUID'
    ]);
  }

  setModelByCreditAppRequestBusinessCollateral(model: BusinessCollateralAgmLineItemView, creditAppReqBusinessCollateralOption: SelectItems[]) {
    if (this.isModelUnset(model)) {
      const confirmation: Confirmation = {
        header: this.baseService.translate.instant('CONFIRM.CONFIRM'),
        message: this.baseService.translate.instant('CONFIRM.90007', [this.baseService.translate.instant('LABEL.BUSINESS_COLLATERAL_AGREEMENT_LINE')])
      };
      return new Observable((observer) => {
        this.baseService.notificationService.showConfirmDialog(confirmation);
        this.baseService.notificationService.isAccept.subscribe((isConfirm) => {
          this.isNewConfirmChange = isConfirm;
          if (isConfirm) {
            if (!isNullOrUndefOrEmptyGUID(model.creditAppReqBusinessCollateralGUID)) {
              this.setFieldByCreditApp(model, creditAppReqBusinessCollateralOption);
              return of(null);
            } else {
              this.clearModelByCreditAppRequestBusinessCollateral(model);
              return of(null);
            }
          }
          this.baseService.notificationService.isAccept.observers = [];
          observer.next();
        });
      });
    } else {
      this.setFieldByCreditApp(model, creditAppReqBusinessCollateralOption);
      return of(null);
    }
  }

  setFieldByCreditApp(model: BusinessCollateralAgmLineItemView, creditAppReqBusinessCollateralOption: SelectItems[]) {
    const item = isNullOrUndefOrEmptyGUID(model.creditAppReqBusinessCollateralGUID)
      ? new CreditAppReqBusinessCollateralItemView()
      : (creditAppReqBusinessCollateralOption.find((o) => o.value === model.creditAppReqBusinessCollateralGUID)
          .rowData as CreditAppReqBusinessCollateralItemView);

    model.businessCollateralTypeGUID = item.businessCollateralTypeGUID;
    model.businessCollateralSubTypeGUID = item.businessCollateralSubTypeGUID;
    model.businessCollateralValue = item.businessCollateralValue;
    model.description = item.description;
    model.buyerTableGUID = item.buyerTableGUID;
    model.buyerName = item.buyerName;
    model.buyerTaxIdentificationId = item.buyerTaxIdentificationId;
    model.bankGroupGUID = item.bankGroupGUID;
    model.bankTypeGUID = item.bankTypeGUID;
    model.accountNumber = item.accountNumber;
    model.refAgreementId = item.refAgreementId;
    model.refAgreementDate = item.refAgreementDate;
    model.preferentialCreditorNumber = item.preferentialCreditorNumber;
    model.lessee = item.lessee;
    model.lessor = item.lessor;
    model.registrationPlateNumber = item.registrationPlateNumber;
    model.machineNumber = item.machineNumber;
    model.machineRegisteredStatus = item.machineRegisteredStatus;
    model.chassisNumber = item.chassisNumber;
    model.registeredPlace = item.registeredPlace;
    model.guaranteeAmount = item.guaranteeAmount;
    model.quantity = item.quantity;
    model.unit = item.unit;
    model.projectName = item.projectName;
    model.titleDeedNumber = item.titleDeedNumber;
    model.titleDeedSubDistrict = item.titleDeedSubDistrict;
    model.titleDeedDistrict = item.titleDeedDistrict;
    model.titleDeedProvince = item.titleDeedProvince;
    model.capitalValuation = item.capitalValuation;
    model.valuationCommittee = item.valuationCommittee;
    model.dateOfValuation = item.dateOfValuation;
    model.ownership = item.ownership;
  }

  isModelUnset(model: BusinessCollateralAgmLineItemView): boolean {
    return (
      model.businessCollateralTypeGUID != null ||
      model.businessCollateralSubTypeGUID != null ||
      (model.description != '' && model.description != null) ||
      model.buyerTableGUID != null ||
      (model.buyerName != '' && model.buyerName != null) ||
      (model.buyerTaxIdentificationId != '' && model.buyerTaxIdentificationId != null) ||
      model.bankGroupGUID != null ||
      model.bankTypeGUID != null ||
      (model.accountNumber != '' && model.accountNumber != null) ||
      (model.refAgreementId != '' && model.refAgreementId != null) ||
      model.refAgreementDate != null ||
      model.preferentialCreditorNumber != 0 ||
      (model.lessee != '' && model.lessee != null) ||
      (model.lessor != '' && model.lessor != null) ||
      (model.registrationPlateNumber != '' && model.registrationPlateNumber != null) ||
      (model.machineNumber != '' && model.machineNumber != null) ||
      (model.machineRegisteredStatus != '' && model.machineRegisteredStatus != null) ||
      (model.chassisNumber != '' && model.chassisNumber != null) ||
      (model.registeredPlace != '' && model.registeredPlace != null) ||
      model.guaranteeAmount != 0 ||
      model.quantity != 0 ||
      (model.unit != '' && model.unit != null) ||
      (model.projectName != '' && model.projectName != null) ||
      (model.titleDeedNumber != '' && model.titleDeedNumber != null) ||
      (model.titleDeedSubDistrict != '' && model.titleDeedSubDistrict != null) ||
      (model.titleDeedDistrict != '' && model.titleDeedDistrict != null) ||
      (model.titleDeedProvince != '' && model.titleDeedProvince != null) ||
      model.capitalValuation != 0 ||
      (model.valuationCommittee != '' && model.valuationCommittee != null) ||
      model.dateOfValuation != null ||
      model.ownership != null
    );
  }
  getParentKey(): string {
    let activeParentName = this.baseService.uiService.getRelatedInfoActiveTableName();
    switch (activeParentName) {
      case ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
      case ROUTE_RELATED_GEN.ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
        return this.baseService.uiService.getKey(activeParentName);
      default:
        return this.baseService.uiService.getKey(ROUTE_MASTER_GEN.BUSINESS_COLLATERAL_AGM_TABLE);
    }
  }
  setTitle() {
    let activeParentName = this.baseService.uiService.getRelatedInfoActiveTableName();
    switch (activeParentName) {
      case ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
        this.title = 'LABEL.NOTICE_OF_CANCELLATION_LINE';
        break;
      case ROUTE_RELATED_GEN.ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
        this.title = 'LABEL.ADDENDUM_LINE';
        break;
      default:
        this.title = 'LABEL.BUSINESS_COLLATERAL_AGREEMENT_LINE';
        break;
    }
  }
  getBusinessCollateralAgmLineDropDown(model: BusinessCollateralAgmLineItemView): Observable<SelectItems[]> {
    if (isNullOrUndefOrEmptyGUID(model.businessCollateralAgmLineGUID)) {
      return of([]);
    } else {
      return this.baseService.baseDropdown.getBusinessCollateralAgmLineDropDown(model.originalBusinessCollateralAgreementTableGUID);
    }
  }
}
