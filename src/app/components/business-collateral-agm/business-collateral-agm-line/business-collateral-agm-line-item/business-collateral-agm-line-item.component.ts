import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, IvzDropdownOnFocusModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { BusinessCollateralAgmLineItemView } from 'shared/models/viewModel';
import { BusinessCollateralAgmLineService } from '../business-collateral-agm-line.service';
import { BusinessCollateralAgmLineItemCustomComponent } from './business-collateral-agm-line-item-custom.component';
@Component({
  selector: 'business-collateral-agm-line-item',
  templateUrl: './business-collateral-agm-line-item.component.html',
  styleUrls: ['./business-collateral-agm-line-item.component.scss']
})
export class BusinessCollateralAgmLineItemComponent extends BaseItemComponent<BusinessCollateralAgmLineItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  bankGroupOptions: SelectItems[] = [];
  bankTypeOptions: SelectItems[] = [];
  businessCollateralSubTypeOptions: SelectItems[] = [];
  businessCollateralTypeOptions: SelectItems[] = [];
  businessCollateralAgmLineOptions: SelectItems[] = [];
  businessCollateralAgmTableOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  creditAppReqBusinessCollateralOptions: SelectItems[] = [];
  businessCollateralStatusOptions: SelectItems[] = [];

  constructor(
    private service: BusinessCollateralAgmLineService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: BusinessCollateralAgmLineItemCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.custom.getParentKey();
  }
  ngOnInit(): void {
    this.custom.setTitle();
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<BusinessCollateralAgmLineItemView> {
    return this.service.getBusinessCollateralAgmLineById(this.id);
  }
  getInitialData(): Observable<BusinessCollateralAgmLineItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner(result);
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin([
      this.baseDropdown.getBankGroupDropDown(),
      this.baseDropdown.getBankTypeDropDown(),
      this.custom.getBusinessCollateralSubTypeDropDown(model),
      this.baseDropdown.getBusinessCollateralTypeDropDown(),
      this.baseDropdown.getBuyerTableDropDown(),
      this.baseDropdown.getCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown(
        model.businessCollateralAgmTable_CreditAppRequestTableGUID
      ),
      this.custom.getBusinessCollateralAgmLineDropDown(model),
      this.baseDropdown.getBusinessCollateralStatusDropDown(),
      this.baseDropdown.getBusinessCollateralAgmTableDropDown(model.businessCollateralAgmTableGUID),

      this.baseDropdown.getBusinessCollateralAgmTableDropDownByBusinessCollateralAgmTable(
        model.businessCollateralAgmTableGUID,
        model?.originalBusinessCollateralAgreementTableGUID
      )
    ]).subscribe(
      ([
        bankGroup,
        bankType,
        businessCollateralSubType,
        businessCollateralType,
        buyerTable,
        creditAppReqBusinessCollateral,
        businessCollateralAgmLine,
        businessCollateralAgmStatus,
        businessCollateralAgmTable
      ]) => {
        this.bankGroupOptions = bankGroup;
        this.bankTypeOptions = bankType;
        this.businessCollateralSubTypeOptions = businessCollateralSubType;
        this.businessCollateralTypeOptions = businessCollateralType;
        this.buyerTableOptions = buyerTable;
        this.creditAppReqBusinessCollateralOptions = creditAppReqBusinessCollateral;
        this.businessCollateralAgmLineOptions = businessCollateralAgmLine;
        this.businessCollateralStatusOptions = businessCollateralAgmStatus;
        this.businessCollateralAgmTableOptions = businessCollateralAgmTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateBusinessCollateralAgmLine(this.model), isColsing);
    } else {
      super.onCreate(this.service.createBusinessCollateralAgmLine(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onBusinessCollateralTypeChange() {
    this.custom.clearModelByCollateralType(this.model, this.businessCollateralSubTypeOptions);
    this.getBusinessCollateralSubTypeByBusinessCollateralTypDropDown();
  }
  onBuyerChange() {
    this.custom.clearModelByBuyer(this.model);
    this.custom.setModelByBuyer(this.model, this.buyerTableOptions);
  }
  onCreditAppReqBusinessCollateralChange() {
    this.custom.setModelByCreditAppRequestBusinessCollateral(this.model, this.creditAppReqBusinessCollateralOptions).subscribe(() => {
      this.getBusinessCollateralSubTypeByBusinessCollateralTypDropDown();
    });
  }
  getBusinessCollateralSubTypeByBusinessCollateralTypDropDown(): void {
    if (!isNullOrUndefOrEmptyGUID(this.model.businessCollateralTypeGUID)) {
      this.baseService.baseDropdown
        .getBusinessCollateralSubTypeByBusinessCollateralTypDropDown(this.model.businessCollateralTypeGUID)
        .subscribe((result) => {
          this.businessCollateralSubTypeOptions = result;
        });
    } else {
      this.businessCollateralSubTypeOptions.length = 0;
    }
  }
  onOriginalbusinesscollateralAgmIDChange() {
    this.custom.clearModelOriginalbusinesscollateralAgmline(this.model);
    this.getBusinessCollateralAgmLineDropDown();
  }
  getBusinessCollateralAgmTableDropDownFocus(e: IvzDropdownOnFocusModel): void {
    super
      .setDropdownOptionOnFocus(
        e,
        this.model,
        this.baseDropdown.getBusinessCollateralAgmTableDropDownByBusinessCollateralAgmTable(this.model.businessCollateralAgmTableGUID)
      )
      .subscribe((result) => {
        this.businessCollateralAgmTableOptions = result;
      });
  }
  getBusinessCollateralAgmLineDropDown(): void {
    if (!isNullOrUndefOrEmptyGUID(this.model.originalBusinessCollateralAgreementTableGUID)) {
      this.baseDropdown
        .getBusinessCollateralAgmLineDropDown(
          this.model.originalBusinessCollateralAgreementTableGUID,
          this.model.originalBusinessCollateralAgreementLineGUID
        )
        .subscribe((result) => {
          this.businessCollateralAgmLineOptions = result;
        });
    } else {
      this.businessCollateralAgmLineOptions.length = 0;
    }
  }
  getBusinessCollateralAgmLineDropDownFocus(e: IvzDropdownOnFocusModel): void {
    if (!isNullOrUndefOrEmptyGUID(this.model.originalBusinessCollateralAgreementTableGUID)) {
      super
        .setDropdownOptionOnFocus(
          e,
          this.model,
          this.baseDropdown.getBusinessCollateralAgmLineDropDown(this.model.originalBusinessCollateralAgreementTableGUID)
        )
        .subscribe((result) => {
          this.businessCollateralAgmLineOptions = result;
        });
    }
  }
}
