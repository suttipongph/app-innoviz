import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CancelBusinessCollateralAgmView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CancelBusinessCollateralAgmService {
  serviceKey = 'cancelBusinessCollateralAgmGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  deleteCancelBusinessCollateralAgm(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCancelBusinessCollateralAgm`;
    return this.dataGateway.delete(url, row);
  }
  getCancelBusinessCollateralAgmById(id: string): Observable<CancelBusinessCollateralAgmView> {
    const url = `${this.servicePath}/GetCancelBusinessCollateralAgmById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCancelBusinessCollateralAgm(vmModel: CancelBusinessCollateralAgmView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCancelBusinessCollateralAgm`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCancelBusinessCollateralAgm(vmModel: CancelBusinessCollateralAgmView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCancelBusinessCollateralAgm`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  validateIsManualNumberSeq(companyId: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeq/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
}
