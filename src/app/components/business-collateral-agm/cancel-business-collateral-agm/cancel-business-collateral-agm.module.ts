import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CancelBusinessCollateralAgmService } from './cancel-business-collateral-agm.service';
import { CancelBusinessCollateralAgmCustomComponent } from './cancel-business-collateral-agm/cancel-business-collateral-agm-custom.component';
import { CancelBusinessCollateralAgmComponent } from './cancel-business-collateral-agm/cancel-business-collateral-agm.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CancelBusinessCollateralAgmComponent],
  providers: [CancelBusinessCollateralAgmService, CancelBusinessCollateralAgmCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CancelBusinessCollateralAgmComponent]
})
export class CancelBusinessCollateralAgmComponentModule {}
