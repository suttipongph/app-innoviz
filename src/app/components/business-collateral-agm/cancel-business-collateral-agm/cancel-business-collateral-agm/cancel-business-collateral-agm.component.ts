import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, RefType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { CancelBusinessCollateralAgmView, DocumentReasonItemView } from 'shared/models/viewModel';
import { CancelBusinessCollateralAgmService } from '../cancel-business-collateral-agm.service';
import { CancelBusinessCollateralAgmCustomComponent } from './cancel-business-collateral-agm-custom.component';
@Component({
  selector: 'cancel-business-collateral-agm',
  templateUrl: './cancel-business-collateral-agm.component.html',
  styleUrls: ['./cancel-business-collateral-agm.component.scss']
})
export class CancelBusinessCollateralAgmComponent extends BaseItemComponent<CancelBusinessCollateralAgmView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  agreementDocTypeOptions: SelectItems[] = [];
  documentReasonOptions: SelectItems[] = [];

  constructor(
    private service: CancelBusinessCollateralAgmService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CancelBusinessCollateralAgmCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getAgreementDocTypeEnumDropDown(this.agreementDocTypeOptions);
  }
  getById(): Observable<CancelBusinessCollateralAgmView> {
    return this.service.getCancelBusinessCollateralAgmById(this.id);
  }
  getInitialData(): Observable<CancelBusinessCollateralAgmView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getDocumentReasonDropDown(RefType.BusinessCollateralAgreement.toString())).subscribe(
      ([documentReason]) => {
        this.documentReasonOptions = documentReason;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model, this.service).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false, this.model);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.custom.getModelParam(this.model, this.id).subscribe((model) => {
          this.onSubmit(true, model);
        });
      }
    });
  }
  onSubmit(isColsing: boolean, model: CancelBusinessCollateralAgmView): void {
    super.onExecuteFunction(this.service.createCancelBusinessCollateralAgm(model));
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onDocumentReasonChange(id: string): void {
    if (!isNullOrUndefined(id)) {
      this.custom.getRemakReason(id, this.documentReasonOptions).subscribe((res) => {
        this.model.reasonRemark = (res.rowData as DocumentReasonItemView).description;
      });
    }
  }
}
