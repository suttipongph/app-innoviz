import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelBusinessCollateralAgmComponent } from './cancel-business-collateral-agm.component';

describe('CancelBusinessCollateralAgmViewComponent', () => {
  let component: CancelBusinessCollateralAgmComponent;
  let fixture: ComponentFixture<CancelBusinessCollateralAgmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CancelBusinessCollateralAgmComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelBusinessCollateralAgmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
