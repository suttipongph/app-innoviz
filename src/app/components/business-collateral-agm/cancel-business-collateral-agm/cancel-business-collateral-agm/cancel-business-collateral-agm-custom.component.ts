import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { CancelBusinessCollateralAgmView } from 'shared/models/viewModel';
import { CancelBusinessCollateralAgmService } from '../cancel-business-collateral-agm.service';

const firstGroup = [
  'INTERNAL_BUSINEE_COLLATERAL_ID',
  'BUSINESS_COLLATERAL_AGM_ID',
  'BUSINESS_COLLATERAL_AGM_DESCRIPTION',
  'AGREEMENT_DOC_TYPE',
  'AGREEMENT_DATE',
  'CUSTOMER_ID',
  'CUSTOMER_NAME'
];
const secondGroup = ['NOTICE_OF_CANCELLATION_INTERNAL_BUSINEE_COLLATERAL_ID'];

export class CancelBusinessCollateralAgmCustomComponent {
  baseService: BaseServiceModel<any>;
  isManual: boolean = false;
  isMandatory_internalBusinessCollateralAgmId: boolean = true;
  getFieldAccessing(model: CancelBusinessCollateralAgmView, service: CancelBusinessCollateralAgmService): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return new Observable((observer) => {
      this.validateIsManualNumberSeq(service, model.companyGUID).subscribe((result) => {
        this.isManual = result;
        if (this.isManual) {
          fieldAccessing.push({ filedIds: secondGroup, readonly: false });
          observer.next(fieldAccessing);
        } else {
          fieldAccessing.push({ filedIds: secondGroup, readonly: true });
          this.isMandatory_internalBusinessCollateralAgmId = false;
          observer.next(fieldAccessing);
        }
      });
    });
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<CancelBusinessCollateralAgmView> {
    let model = new CancelBusinessCollateralAgmView();
    model.cancelBusinessCollateralAgmGUID = EmptyGuid;
    return of(model);
  }
  getRemakReason(id: string, options: SelectItems[]): Observable<SelectItems> {
    const item = options.find((f) => f.value === id);
    return of(item);
  }
  validateIsManualNumberSeq(service: CancelBusinessCollateralAgmService, id: string): Observable<boolean> {
    return service.validateIsManualNumberSeq(id);
  }
  getModelParam(model: CancelBusinessCollateralAgmView, id: string): Observable<CancelBusinessCollateralAgmView> {
    const param: CancelBusinessCollateralAgmView = new CancelBusinessCollateralAgmView();
    param.cancelBusinessCollateralAgmGUID = id;
    param.agreementDate = model.agreementDate;
    param.agreementDocType = model.agreementDocType;
    param.buyerTableGUID = model.buyerTableGUID;
    param.buyerName = model.buyerName;
    param.cancelDate = model.cancelDate;
    param.customerId = model.customerId;
    param.customerName = model.customerName;
    param.description = model.description;
    param.documentReasonGUID = model.documentReasonGUID;
    param.reasonRemark = model.reasonRemark;
    param.internalBusinessCollateralAgmId = model.internalBusinessCollateralAgmId;
    param.companyGUID = model.companyGUID;
    param.noticeOfCancellationInternalBusineeCollateralId = model.noticeOfCancellationInternalBusineeCollateralId;
    param.cancelDate = model.cancelDate;
    return of(param);
  }
}
