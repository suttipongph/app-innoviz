import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GenBusCollateralAgmAddendumService } from './gen-bus-collateral-agm-addendum.service';
import { GenBusCollateralAgmAddendumComponent } from './gen-bus-collateral-agm-addendum/gen-bus-collateral-agm-addendum.component';
import { GenBusCollateralAgmAddendumCustomComponent } from './gen-bus-collateral-agm-addendum/gen-bus-collateral-agm-addendum-custom.component';


@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [GenBusCollateralAgmAddendumComponent],
  providers: [GenBusCollateralAgmAddendumService, GenBusCollateralAgmAddendumCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [GenBusCollateralAgmAddendumComponent]
})
export class GenBusCollateralAgmAddendumComponentModule {}
