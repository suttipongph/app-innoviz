import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { GenBusCollateralAgmAddendumView } from 'shared/models/viewModel';
import { GenBusCollateralAgmAddendumService } from '../gen-bus-collateral-agm-addendum.service';
import { GenBusCollateralAgmAddendumCustomComponent } from './gen-bus-collateral-agm-addendum-custom.component';
@Component({
  selector: 'bus-collateral-agm-addendum',
  templateUrl: 'gen-bus-collateral-agm-addendum.component.html',
  styleUrls: ['gen-bus-collateral-agm-addendum.component.scss']
})
export class GenBusCollateralAgmAddendumComponent extends BaseItemComponent<GenBusCollateralAgmAddendumView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  agreementDocTypeOptions: SelectItems[] = [];
  creditAppRequestTableOptions: SelectItems[] = [];
  creditAppRequestTypeOptions: SelectItems[] = [];

  constructor(
    private service: GenBusCollateralAgmAddendumService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: GenBusCollateralAgmAddendumCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getAgreementDocTypeEnumDropDown(this.agreementDocTypeOptions);
    this.baseDropdown.getCreditAppRequestTypeEnumDropDown(this.creditAppRequestTypeOptions);
  }
  getById(): Observable<GenBusCollateralAgmAddendumView> {
    return this.service.getGenBusCollateralAgmAddendumById(this.id);
  }
  getInitialData(): Observable<GenBusCollateralAgmAddendumView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(this.service.GetCreditAppRequestTableDropDownAddendum(this.id)).subscribe(
      ([creditAppRequestTable]) => {
        this.creditAppRequestTableOptions = creditAppRequestTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model, this.service).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false, this.model);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.custom.getModelParam(this.model, this.id).subscribe((model) => {
          this.onSubmit(true, model);
        });
      }
    });
  }
  onSubmit(isColsing: boolean, model: GenBusCollateralAgmAddendumView): void {
    super.onExecuteFunction(this.service.createGenBusCollateralAgmAddendum(model));
  }

  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onCreditAppRequestIDChange(id: string): void {
    if (!isNullOrUndefined(id)) {
      this.service.getCreditAppRequestIDCById(id).subscribe((res) => {
        this.model.creditAppRequestType = res.creditAppRequestType;
        this.model.creditLimitType_Values = res.creditLimitType_Values;
        this.model.requestDate = res.requestDate;
        this.model.creditAppRequestDescription = res.creditAppRequestDescription;
      });
    }
  }
}
