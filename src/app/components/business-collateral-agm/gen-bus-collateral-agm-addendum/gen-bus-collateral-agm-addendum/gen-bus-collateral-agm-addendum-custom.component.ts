import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { GenBusCollateralAgmAddendumResultView, GenBusCollateralAgmAddendumView } from 'shared/models/viewModel';
import { GenBusCollateralAgmAddendumService } from '../gen-bus-collateral-agm-addendum.service';

const firstGroup = [
  'INTERNAL_BUSINEE_COLLATERAL_ID',
  'BUSINESS_COLLATERAL_AGM_ID',
  'BUSINESS_COLLATERAL_AGM_DESCRIPTION',
  'CUSTOMER_ID',
  'CUSTOMER_NAME',
  'AGREEMENT_DOC_TYPE',
  'AGREEMENT_DATE',
  'CREDIT_APP_REQUEST_DESCRIPTION',
  'CREDIT_APP_REQUEST_TYPE',
  'CREDIT_LIMIT_TYPE_GUID',
  'REQUEST_DATE',
  'ADDENDUM_INTERNAL_BUSINEE_COLLATERAL_ID'
];

const secondGroup = [
  'BUSINESS_COLLATERAL_AGM_ID',
  'BUSINESS_COLLATERAL_AGM_DESCRIPTION',
  'CUSTOMER_ID',
  'CUSTOMER_NAME',
  'AGREEMENT_DOC_TYPE',
  'AGREEMENT_DATE',
  'CREDIT_APP_REQUEST_DESCRIPTION',
  'CREDIT_APP_REQUEST_TYPE',
  'CREDIT_LIMIT_TYPE_GUID',
  'REQUEST_DATE'
];

export class GenBusCollateralAgmAddendumCustomComponent {
  baseService: BaseServiceModel<any>;
  isManual: boolean = false;
  isMandatory = false;
  getFieldAccessing(
    model: GenBusCollateralAgmAddendumView,
    genBusCollateralAgmAddendumService: GenBusCollateralAgmAddendumService
  ): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    return new Observable((observer) => {
      this.validateIsManualNumberSeq(genBusCollateralAgmAddendumService, model.companyGUID).subscribe((result) => {
        this.isManual = result;
        if (this.isManual) {
          fieldAccessing.push({ filedIds: secondGroup, readonly: true });
          this.isMandatory = true;
          observer.next(fieldAccessing);
        } else {
          fieldAccessing.push({ filedIds: firstGroup, readonly: true });
          this.isMandatory = false;
          observer.next(fieldAccessing);
        }
      });
    });
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<GenBusCollateralAgmAddendumView> {
    let model = new GenBusCollateralAgmAddendumView();
    model.genBusCollateralAgmAddendumGUID = EmptyGuid;
    return of(model);
  }
  validateIsManualNumberSeq(service: GenBusCollateralAgmAddendumService, id: string): Observable<boolean> {
    return service.validateIsManualNumberSeq(id);
  }
  getModelParam(model: GenBusCollateralAgmAddendumView, id: string): Observable<GenBusCollateralAgmAddendumResultView> {
    const param: GenBusCollateralAgmAddendumView = new GenBusCollateralAgmAddendumView();
    param.genBusCollateralAgmAddendumGUID = id;
    param.internalBusinessCollateralAgmId = model.internalBusinessCollateralAgmId;
    param.businessCollateralAgmId = model.businessCollateralAgmId;
    param.description = model.description;
    param.customerGUID = model.customerGUID;
    param.customerName = model.customerName;
    param.agreementDocType = model.agreementDocType;
    param.addendumInternalBusineeCollateralId = model.addendumInternalBusineeCollateralId;
    param.addendumDate = model.addendumDate;
    param.creditAppRequestTableGUID = model.creditAppRequestTableGUID;
    param.creditAppRequestDescription = model.creditAppRequestDescription;
    param.creditAppRequestType = model.creditAppRequestType;
    param.creditLimitTypeGUID = model.creditLimitTypeGUID;
    param.requestDate = model.requestDate;
    param.agreementDate = model.agreementDate;
    param.companyGUID = model.companyGUID;
    return of(param);
  }
}
