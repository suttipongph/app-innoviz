import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenBusCollateralAgmAddendumComponent } from './gen-bus-collateral-agm-addendum.component';

describe('GenBusCollateralAgmAddendumViewComponent', () => {
  let component: GenBusCollateralAgmAddendumComponent;
  let fixture: ComponentFixture<GenBusCollateralAgmAddendumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GenBusCollateralAgmAddendumComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenBusCollateralAgmAddendumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
