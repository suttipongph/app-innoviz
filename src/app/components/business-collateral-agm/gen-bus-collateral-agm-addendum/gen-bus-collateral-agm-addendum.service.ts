import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { GenBusCollateralAgmAddendumView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult, SelectItems } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class GenBusCollateralAgmAddendumService {
  serviceKey = 'genBusCollateralAgmAddendumGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getGenBusCollateralAgmAddendumToList(search: SearchParameter): Observable<SearchResult<GenBusCollateralAgmAddendumView>> {
    const url = `${this.servicePath}/GetGenBusCollateralAgmAddendumList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteGenBusCollateralAgmAddendum(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteGenBusCollateralAgmAddendum`;
    return this.dataGateway.delete(url, row);
  }
  getGenBusCollateralAgmAddendumById(id: string): Observable<GenBusCollateralAgmAddendumView> {
    const url = `${this.servicePath}/GetGenBusCollateralAgmAddendumById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createGenBusCollateralAgmAddendum(vmModel: GenBusCollateralAgmAddendumView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateGenBusCollateralAgmAddendum`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateGenBusCollateralAgmAddendum(vmModel: GenBusCollateralAgmAddendumView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateGenBusCollateralAgmAddendum`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  validateIsManualNumberSeq(companyId: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeq/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
  GetCreditAppRequestTableDropDownAddendum(id: string): Observable<SelectItems[]> {
    const url = `${this.servicePath}/GetCreditAppRequestTableDropDownAddendum/id=${id}`;
    return this.dataGateway.get(url);
  }
  getCreditAppRequestIDCById(id: string): Observable<GenBusCollateralAgmAddendumView> {
    const url = `${this.servicePath}/GetCreditAppRequestIDCById/id=${id}`;
    return this.dataGateway.get(url);
  }
}
