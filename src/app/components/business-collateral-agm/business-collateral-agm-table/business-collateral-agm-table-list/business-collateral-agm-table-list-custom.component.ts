import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AgreementDocType, ROUTE_FUNCTION_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel } from 'shared/models/systemModel';
import { BusinessCollateralAgmTableService } from '../business-collateral-agm-table.service';
export class BusinessCollateralAgmTableListCustomComponent {
  baseService: BaseServiceModel<any>;
  title: string = null;
  visibleGenAddendum: boolean = false;
  visibleGenNoticeOfCancellation: boolean = false;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setTitle(passingObj: any) {
    if (!isNullOrUndefined(passingObj)) {
      let agreementDocType: AgreementDocType = passingObj.agreementDocType;
      switch (agreementDocType) {
        case AgreementDocType.NoticeOfCancellation:
          this.title = 'LABEL.NOTICE_OF_CANCELLATION';
          break;
        case AgreementDocType.Addendum:
          this.title = 'LABEL.ADDENDUM';
          break;
        default:
          this.title = 'LABEL.BUSINESS_COLLATERAL_AGREEMENT';
          break;
      }
    } else {
      this.title = 'LABEL.BUSINESS_COLLATERAL_AGREEMENT';
    }
  }
  setPreparingFunctionDataAndValidation(): Observable<boolean> {
    const id = this.baseService.uiService.getKey('businesscollateralagmtable');
    return (this.baseService.service as BusinessCollateralAgmTableService).getGenerateAddendumValidation(id).pipe(
      tap((res) => {
        this.visibleGenAddendum = res;
      })
    );
  }
  
  setPreparingFunctionDataAndValidationNoticeOfCancellation(): Observable<boolean> {
    const id = this.baseService.uiService.getKey('businesscollateralagmtable');
    return (this.baseService.service as BusinessCollateralAgmTableService).getGenerateNoticOfCancellationValidation(id).pipe(
      tap((res) => {
        this.visibleGenNoticeOfCancellation = res;
      })
    );
  }
  getVisibleGeneratMenu(): Observable<boolean> {
    const id = this.baseService.uiService.getKey('businesscollateralagmtable');
    return (this.baseService.service as BusinessCollateralAgmTableService).getVisibleGeneratMenu(id).pipe(
      tap((res) => {
        this.visibleGenAddendum = res;
      })
    );
  }
  getIsAddendum(): boolean {
    const key = this.baseService.uiService.getRelatedInfoActiveTableName();
    switch (key) {
      case ROUTE_RELATED_GEN.ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
        return true;
      default:
        return false;
    }
  }
}
