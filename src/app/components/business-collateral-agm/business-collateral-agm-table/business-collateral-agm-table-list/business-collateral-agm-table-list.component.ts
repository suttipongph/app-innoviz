import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, ROUTE_FUNCTION_GEN, SortType } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { BusinessCollateralAgmTableListView } from 'shared/models/viewModel';
import { BusinessCollateralAgmTableService } from '../business-collateral-agm-table.service';
import { BusinessCollateralAgmTableListCustomComponent } from './business-collateral-agm-table-list-custom.component';

@Component({
  selector: 'business-collateral-agm-table-list',
  templateUrl: './business-collateral-agm-table-list.component.html',
  styleUrls: ['./business-collateral-agm-table-list.component.scss']
})
export class BusinessCollateralAgmTableListComponent extends BaseListComponent<BusinessCollateralAgmTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  agreementDocTypeOptions: SelectItems[] = [];
  creditAppRequestTableOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];

  creditAppTableOptions: SelectItems[] = [];

  constructor(
    public custom: BusinessCollateralAgmTableListCustomComponent,
    private service: BusinessCollateralAgmTableService,
    private currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
  }
  ngOnInit(): void {
    let passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setTitle(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.getVisibleGeneratMenu().subscribe((res) => {
      this.setFunctionOptions();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getAgreementDocTypeEnumDropDown(this.agreementDocTypeOptions);
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCreditAppRequestTableDropDown(this.creditAppRequestTableOptions);
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
    this.baseDropdown.getCreditAppTableDropDown(this.creditAppTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'businessCollateralAgmTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.INTERNAL_BUSINESS_COLLATERAL_AGREEMENT_ID',
        textKey: 'internalBusinessCollateralAgmId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.BUSINESS_COLLATERAL_AGREEMENT_ID',
        textKey: 'businessCollateralAgmId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.AGREEMENT_DOCUMENT_TYPE',
        textKey: 'agreementDocType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.agreementDocTypeOptions
      },
      {
        label: 'LABEL.CREDIT_APPLICATION_REQUEST_ID',
        textKey: 'creditAppRequestTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditAppRequestTableOptions,
        searchingKey: 'creditAppRequestTableGUID',
        sortingKey: 'creditAppRequestTable_CreditAppRequestId'
      },
      {
        label: 'LABEL.CREDIT_APPLICATION_ID',
        textKey: 'creditAppTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditAppTableOptions,
        searchingKey: 'creditAppTableGUID',
        sortingKey: 'creditAppTable_CreditAppId'
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId'
      },
      {
        label: 'LABEL.SIGNING_DATE',
        textKey: 'signingDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentStatusOptions,
        sortingKey: 'documentStatus_StatusId',
        searchingKey: 'documentStatusGUID'
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<BusinessCollateralAgmTableListView>> {
    return this.service.getBusinessCollateralAgmTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteBusinessCollateralAgmTable(row));
  }
  setFunctionOptions(): void {
    const isAddendum = this.custom.getIsAddendum();
    this.functionItems = [
      {
        label: 'LABEL.GENERATE',
        items: [
          {
            label: 'LABEL.ADDENDUM',
            visible: isAddendum,
            disabled: !this.custom.visibleGenAddendum,
            command: () => this.setPreparingFunctionDataAndValidation(ROUTE_FUNCTION_GEN.GEN_BUS_ADDENDUM)
          },
          {
            label: 'LABEL.NOTICE_OF_CANCELLATION',
            visible: !isAddendum,
            disabled: !this.custom.visibleGenAddendum,
            command: () =>
              this.setPreparingFunctionDataAndValidationNoticeOfCancellation(
                ROUTE_FUNCTION_GEN.GENERATE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM
              )
          }
        ]
      }
    ];

    super.setFunctionOptionsMapping();
  }
  setPreparingFunctionDataAndValidation(path: string): void {
    this.custom.setPreparingFunctionDataAndValidation().subscribe((res) => {
      if (!isNullOrUndefined(res)) {
        this.toFunction({
          path: path
        });
      }
    });
  }
  setPreparingFunctionDataAndValidationNoticeOfCancellation(path: string): void {
    this.custom.setPreparingFunctionDataAndValidationNoticeOfCancellation().subscribe((res) => {
      if (!isNullOrUndefined(res)) {
        this.toFunction({
          path: path
        });
      }
    });
  }
}
