import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessCollateralAgmTableListComponent } from './business-collateral-agm-table-list.component';

describe('BusinessCollateralAgmTableListViewComponent', () => {
  let component: BusinessCollateralAgmTableListComponent;
  let fixture: ComponentFixture<BusinessCollateralAgmTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BusinessCollateralAgmTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCollateralAgmTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
