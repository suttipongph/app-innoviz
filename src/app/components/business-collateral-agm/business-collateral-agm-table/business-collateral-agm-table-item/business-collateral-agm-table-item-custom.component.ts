import { forkJoin, Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AgreementDocType, BusinessCollateralAgreementStatus, EmptyGuid, ProductType, RefType, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { BusinessCollateralAgmTableService } from '../business-collateral-agm-table.service';
import {
  BusinessCollateralAgmTableItemView,
  CreditAppRequestTableItemView,
  CustomerTableItemView,
  ManageAgreementView
} from 'shared/models/viewModel';
import { toMapModel } from 'shared/functions/model.function';
import { PrintSetBookDocTransView } from 'shared/models/viewModel/printSetBookDocTransView';

const readOnlyGroup = [
  'INTERNAL_BUSINESS_COLLATERAL_AGM_ID',
  'AGREEMENT_DOC_TYPE',
  'CUSTOMER_NAME',
  'SIGNING_DATE',
  'DOCUMENT_STATUS_GUID',
  'DOCUMENT_REASON_GUID',
  'TOTAL_BUSINESS_COLLATERAL_VALUE',
  'PRODUCT_TYPE',
  'CREDIT_LIMIT_TYPE_GUID',
  'CREDIT_APP_TABLE_GUID',
  'REF_BUSINESS_COLLATERAL_AGM_TABLE_GUID',
  'AGREEMENT_EXTENSION',
  'CONSORTIUM_TABLE_GUID'
];

const readOnlyOnViewGroup = ['CUSTOMER_TABLE_GUID', 'CREDIT_APP_REQUEST_TABLE_GUID'];

const condition1 = ['BUSINESS_COLLATERAL_AGM_ID'];
const condition2 = ['INTERNAL_BUSINESS_COLLATERAL_AGM_ID'];

export class BusinessCollateralAgmTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  service: BusinessCollateralAgmTableService;
  isInternalBusinessCollateralAgmId = null;
  title: string = null;
  showFunctionNoti: boolean = false;
  showFunctionAdgendum: boolean = false;
  getFieldAccessing(model: BusinessCollateralAgmTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: readOnlyGroup, readonly: true });
    if (isUpdateMode(model.businessCollateralAgmTableGUID)) {
      fieldAccessing.push({ filedIds: readOnlyOnViewGroup, readonly: true });
      fieldAccessing.push({ filedIds: condition2, readonly: true });
      if (model.documentStatus_StatusId === BusinessCollateralAgreementStatus.Posted) {
        fieldAccessing.push({ filedIds: condition1, readonly: true });
        return of(fieldAccessing);
      } else {
        return forkJoin([this.validateIsManualNumberSeqBusinessCollateralAgm(model.companyGUID)]).pipe(
          map(([isBusinessCollateralAgmId]) => {
            fieldAccessing.push({ filedIds: condition1, readonly: !isBusinessCollateralAgmId });
            return fieldAccessing;
          })
        );
      }
    } else {
      fieldAccessing.push({ filedIds: readOnlyOnViewGroup, readonly: false });
      return forkJoin([
        this.validateIsManualNumberSeqInternalBusinessCollateralAgm(model.companyGUID),
        this.validateIsManualNumberSeqBusinessCollateralAgm(model.companyGUID)
      ]).pipe(
        map(([isInternalBusinessCollateralAgmId, isBusinessCollateralAgmId]) => {
          this.isInternalBusinessCollateralAgmId = isInternalBusinessCollateralAgmId;
          fieldAccessing.push({ filedIds: condition1, readonly: !isBusinessCollateralAgmId });
          fieldAccessing.push({ filedIds: condition2, readonly: !isInternalBusinessCollateralAgmId });
          return fieldAccessing;
        })
      );
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BusinessCollateralAgmTableItemView): Observable<boolean> {
    let accessright = isNullOrUndefOrEmptyGUID(model.creditAppTableGUID) ? canCreate : canUpdate;
    let accessLogic = model.documentStatus_StatusId < BusinessCollateralAgreementStatus.Signed;
    return of(!(accessLogic && accessright));
  }
  getProductType(): number {
    const masterRoute = this.baseService.uiService.getMasterRoute();
    let productType = 0;
    switch (masterRoute) {
      case 'factoring':
        productType = ProductType.Factoring;
        break;
      case 'bond':
        productType = ProductType.Bond;
        break;
      case 'hirepurchase':
        productType = ProductType.HirePurchase;
        break;
      case 'lcdlc':
        productType = ProductType.LC_DLC;
        break;
      case 'leasing':
        productType = ProductType.Leasing;
        break;
      case 'projectfinance':
        productType = ProductType.ProjectFinance;
        break;
      default:
        break;
    }
    return productType;
  }
  validateIsManualNumberSeqInternalBusinessCollateralAgm(companyId: string): Observable<boolean> {
    return this.baseService.service.validateIsManualNumberSeqInternalBusinessCollateralAgm(companyId).pipe(
      map((res) => res),
      catchError((e) => {
        this.baseService.notificationService.showErrorMessageFromResponse(e);
        return e;
      })
    );
  }
  validateIsManualNumberSeqBusinessCollateralAgm(companyId: string): Observable<boolean> {
    return this.baseService.service.validateIsManualNumberSeqBusinessCollateralAgm(companyId).pipe(
      map((res) => res),
      catchError((e) => {
        this.baseService.notificationService.showErrorMessageFromResponse(e);
        return e;
      })
    );
  }

  getCreditAppRequestTableDropDown(model: BusinessCollateralAgmTableItemView): Observable<SelectItems[]> {
    if (isNullOrUndefined(model)) {
      return of([]);
    } else {
      return this.baseService.baseDropdown.getCreditAppRequestTableDropDownByBusinessCollateralAgmTable([
        model.customerTableGUID,
        model.productType.toString()
      ]);
    }
  }

  clearModelByCustomer(model: BusinessCollateralAgmTableItemView, creditAppRequestTableOption: SelectItems[]) {
    model.creditAppRequestTableGUID = null;
    creditAppRequestTableOption.length = 0;
    this.clearModelByCreditAppRequestTable(model);
  }
  clearModelByCreditAppRequestTable(model: BusinessCollateralAgmTableItemView) {
    model.creditLimitTypeGUID = null;
    model.creditAppTableGUID = null;
    model.consortiumTableGUID = null;
    model.creditLimitType_Values = '';
    model.creditAppTable_Values = '';
    model.consortiumTable_Values = '';
  }
  setModelByCustomer(model: BusinessCollateralAgmTableItemView, option: SelectItems[]) {
    if (!isNullOrUndefOrEmptyGUID(model.customerTableGUID)) {
      let customerTable: CustomerTableItemView = option.find((t) => t.value == model.customerTableGUID).rowData as CustomerTableItemView;
      model.customerName = customerTable.name;
      model.customerAltName = customerTable.altName;
    }
  }
  setModelByCreditAppRequestTable(model: BusinessCollateralAgmTableItemView, option: SelectItems[]) {
    if (!isNullOrUndefOrEmptyGUID(model.creditAppRequestTableGUID)) {
      let creditAppRequestTable: CreditAppRequestTableItemView = option.find((t) => t.value == model.creditAppRequestTableGUID)
        .rowData as CreditAppRequestTableItemView;
      model.creditAppTableGUID = creditAppRequestTable.refCreditAppTableGUID;
      model.creditAppTable_Values = creditAppRequestTable.creditAppTable_Values;
      model.creditLimitTypeGUID = creditAppRequestTable.creditLimitTypeGUID;
      model.creditLimitType_Values = creditAppRequestTable.creditLimitType_Values;
      model.consortiumTableGUID = creditAppRequestTable.consortiumTableGUID;
      model.consortiumTable_Values = creditAppRequestTable.consortiumTable_Values;
    }
  }
  setCreditAppRequestDropDownByCustomer(model: BusinessCollateralAgmTableItemView): Observable<SelectItems[]> {
    if (!isNullOrUndefOrEmptyGUID(model.customerTableGUID)) {
      return this.baseService.baseDropdown.getCreditAppRequestTableDropDownByBusinessCollateralAgmTable([
        model.customerTableGUID,
        model.productType.toString()
      ]);
    } else {
      return of();
    }
  }
  setTitle(passingObj: any) {
    if (!isNullOrUndefined(passingObj)) {
      let agreementDocType: AgreementDocType = passingObj.agreementDocType;
      switch (agreementDocType) {
        case AgreementDocType.NoticeOfCancellation:
          this.title = 'LABEL.NOTICE_OF_CANCELLATION';
          break;
        case AgreementDocType.Addendum:
          this.title = 'LABEL.ADDENDUM';
          break;
        default:
          this.title = 'LABEL.BUSINESS_COLLATERAL_AGREEMENT';
          break;
      }
    } else {
      this.title = 'LABEL.BUSINESS_COLLATERAL_AGREEMENT';
    }
  }

  setPrintSetBookDocTransData(model: BusinessCollateralAgmTableItemView): PrintSetBookDocTransView {
    const paramModel = new PrintSetBookDocTransView();
    toMapModel(model, paramModel);
    paramModel.refType = RefType.BusinessCollateralAgreement;
    paramModel.refGUID = model.businessCollateralAgmTableGUID;
    paramModel.internalAgreementId = model.internalBusinessCollateralAgmId;
    paramModel.agreementId = model.businessCollateralAgmId;
    paramModel.agreementDescription = model.description;
    return paramModel;
  }
  setShowFunctionWithLabel(label: string): any {
    if (label) {
      switch (label) {
        case 'addendum':
          this.showFunctionAdgendum = true;
          break;
        case 'notice':
          this.showFunctionNoti = true;
          break;
        default:
          break;
      }
    }
  }
  getVisableFunctionCopyActiveBusiness(model: BusinessCollateralAgmTableItemView): boolean {
    return model.agreementDocType == AgreementDocType.NoticeOfCancellation;
  }
  getDisableFunctionCopyActiveBusiness(model: BusinessCollateralAgmTableItemView): boolean {
    return !(model.documentStatus_StatusId === BusinessCollateralAgreementStatus.Draft);
  }
  getVisableRelatedInfoAddendumAndNoticeOfCancellation(model: BusinessCollateralAgmTableItemView): boolean {
    return model.agreementDocType == AgreementDocType.New;
  }
}
