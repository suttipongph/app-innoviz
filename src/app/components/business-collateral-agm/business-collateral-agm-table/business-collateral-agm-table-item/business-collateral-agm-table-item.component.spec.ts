import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessCollateralAgmTableItemComponent } from './business-collateral-agm-table-item.component';

describe('BusinessCollateralAgmTableItemViewComponent', () => {
  let component: BusinessCollateralAgmTableItemComponent;
  let fixture: ComponentFixture<BusinessCollateralAgmTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BusinessCollateralAgmTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCollateralAgmTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
