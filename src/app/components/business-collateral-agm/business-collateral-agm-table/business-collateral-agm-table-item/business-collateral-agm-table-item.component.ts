import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import {
  AgreementDocType,
  BusinessCollateralAgreementStatus,
  EmptyGuid,
  ManageAgreementAction,
  RefType,
  ROUTE_FUNCTION_GEN,
  ROUTE_RELATED_GEN
} from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, BusinessCollateralAgmTableItemView, ManageAgreementView, RefIdParm } from 'shared/models/viewModel';
import { PrintSetBookDocTransView } from 'shared/models/viewModel/printSetBookDocTransView';
import { BusinessCollateralAgmTableService } from '../business-collateral-agm-table.service';
import { BusinessCollateralAgmTableItemCustomComponent } from './business-collateral-agm-table-item-custom.component';
@Component({
  selector: 'business-collateral-agm-table-item',
  templateUrl: './business-collateral-agm-table-item.component.html',
  styleUrls: ['./business-collateral-agm-table-item.component.scss']
})
export class BusinessCollateralAgmTableItemComponent extends BaseItemComponent<BusinessCollateralAgmTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  @Input() set pageLabel(param: string) {
    this.custom.setShowFunctionWithLabel(param);
  }
  agreementDocTypeOptions: SelectItems[] = [];
  creditAppRequestTableOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  manageMenuVisibility: number[] = [];
  manageGenerateMenuVisibility: boolean = false;
  constructor(
    private service: BusinessCollateralAgmTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: BusinessCollateralAgmTableItemCustomComponent,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.baseService.service = service;
    this.baseService.baseDropdown = this.baseDropdown;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {
    let passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setTitle(passingObj);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {
    this.baseDropdown.getAgreementDocTypeEnumDropDown(this.agreementDocTypeOptions);
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
  }
  getById(): Observable<BusinessCollateralAgmTableItemView> {
    return this.service.getBusinessCollateralAgmTableById(this.id);
  }
  getInitialData(): Observable<BusinessCollateralAgmTableItemView> {
    return this.service.getInitialData(this.custom.getProductType());
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.service.getVisibleManageMenu(this.id).subscribe((res) => {
          this.manageMenuVisibility = res;
          this.setRelatedInfoOptions(result);
          this.setFunctionOptions(result);
        });
        this.service.getVisibleGeneratMenu(this.parentId).subscribe((res) => {
          this.manageGenerateMenuVisibility = res;
          this.setRelatedInfoOptions(result);
          this.setFunctionOptions(result);
        });
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin([this.custom.getCreditAppRequestTableDropDown(model), this.baseDropdown.getCustomerTableDropDown()]).subscribe(
      ([creditAppRequestTable, customerTable]) => {
        this.creditAppRequestTableOptions = creditAppRequestTable;
        this.customerTableOptions = customerTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  setRelatedInfoOptions(model: BusinessCollateralAgmTableItemView): void {
    var visableRelatedInfoAddendumAndNoticeOfCancellation = this.custom.getVisableRelatedInfoAddendumAndNoticeOfCancellation(model);
    this.relatedInfoItems = [
      {
        label: 'LABEL.ADDENDUM',
        visible: visableRelatedInfoAddendumAndNoticeOfCancellation,
        disabled: !visableRelatedInfoAddendumAndNoticeOfCancellation,
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE,
            parameters: { agreementDocType: AgreementDocType.Addendum }
          })
      },
      {
        label: 'LABEL.ATTACHMENT',
        command: () => {
          const refIdParm: RefIdParm = { refType: RefType.BusinessCollateralAgreement, refGUID: this.model.businessCollateralAgmTableGUID };
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: refIdParm
          });
        }
      },
      {
        label: 'LABEL.AGREEMENT_INFORMATION',
        command: () => {
          this.service.getAgreementTableInfoByBusinessCollateralAgmTable(this.id).subscribe((result) => {
            if (isNullOrUndefined(result)) {
              this.toRelatedInfo({ path: `${ROUTE_RELATED_GEN.AGREEMENT_TABLE_INFO}/${EmptyGuid}` });
            } else {
              this.toRelatedInfo({ path: `${ROUTE_RELATED_GEN.AGREEMENT_TABLE_INFO}/${result.agreementTableInfoGUID}` });
            }
          });
        }
      },
      {
        label: 'LABEL.NOTICE_OF_CANCELLATION',
        visible: visableRelatedInfoAddendumAndNoticeOfCancellation,
        disabled: !visableRelatedInfoAddendumAndNoticeOfCancellation,
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE,
            parameters: { agreementDocType: AgreementDocType.NoticeOfCancellation }
          })
      },
      //   { label: 'LABEL.ATTACHMENT', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.ATTACHMENT }) },
      { label: 'LABEL.MEMO', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.MEMO }) },
      //   { label: 'LABEL.AGREEMENT_INFORMATION', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.AGREEMENT_INFORMATION }) },
      {
        label: 'LABEL.SERVICE_FEE_TRANSACTIONS',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.SERVICE_FEE_TRANS,
            parameters: {
              refId: model.internalBusinessCollateralAgmId,
              taxDate: model.agreementDate,
              productType: model.productType,
              invoiceTable_CustomerTableGUID: model.customerTableGUID
            }
          })
      },
      {
        label: 'LABEL.JOINT_VENTURE',
        command: () =>
          this.toRelatedInfo({ path: ROUTE_RELATED_GEN.JOINT_VENTURE_TRANS, parameters: { statusCode: this.model.documentStatus_StatusId } })
      },
      { label: 'LABEL.CONSORTIUM', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.CONSORTIUM_TRANS }) },
      { label: 'LABEL.BOOKMARK_DOCUMENT', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.BOOKMARK_DOCUMENT_TRANS }) }
      //   { label: 'LABEL.ADDENDUM', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.ADDENDUM }) },
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(model: BusinessCollateralAgmTableItemView): void {
    this.functionItems = [
      {
        label: 'LABEL.COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE',
        visible: this.custom.getVisableFunctionCopyActiveBusiness(model),
        disabled: this.custom.getDisableFunctionCopyActiveBusiness(model),
        command: () => {
          this.toFunction({ path: ROUTE_FUNCTION_GEN.COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE });
        }
      },
      {
        label: 'LABEL.MANAGE',
        items: [
          {
            label: 'LABEL.POST',
            disabled: !this.manageMenuVisibility.some((s) => s === ManageAgreementAction.Post),
            command: () => {
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.POST_AGREEMENT,
                parameters: {
                  refType: RefType.BusinessCollateralAgreement,
                  refGuid: this.id,
                  action: ManageAgreementAction.Post
                }
              });
            }
          },
          {
            label: 'LABEL.SEND',
            disabled: !this.manageMenuVisibility.some((s) => s === ManageAgreementAction.Send),
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.SEND_AGREEMENT,
                parameters: {
                  refType: RefType.BusinessCollateralAgreement,
                  refGuid: this.id,
                  action: ManageAgreementAction.Send
                }
              })
          },
          {
            label: 'LABEL.SIGN',
            disabled: !this.manageMenuVisibility.some((s) => s === ManageAgreementAction.Sign),
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.SIGN_AGREEMENT,
                parameters: {
                  refType: RefType.BusinessCollateralAgreement,
                  refGuid: this.id,
                  action: ManageAgreementAction.Sign
                }
              })
          },
          {
            label: 'LABEL.CLOSE',
            disabled: !this.manageMenuVisibility.some((s) => s === ManageAgreementAction.Close),
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.CLOSE_AGREEMENT,
                parameters: {
                  refType: RefType.BusinessCollateralAgreement,
                  refGuid: this.id,
                  action: ManageAgreementAction.Close
                }
              })
          },
          {
            label: 'LABEL.CANCEL',
            disabled: !this.manageMenuVisibility.some((s) => s === ManageAgreementAction.Cancel),
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.CANCEL_AGREEMENT,
                parameters: {
                  refType: RefType.BusinessCollateralAgreement,
                  refGuid: this.id,
                  action: ManageAgreementAction.Cancel
                }
              })
          }
        ]
      },
      {
        label: 'LABEL.PRINT',
        items: [
          {
            label: 'LABEL.BOOKMARK_DOCUMENT',
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.PRINT_SET_BOOKMARK_DOCUMENT_TRANSACTION,
                parameters: {
                  // refType: RefType.BusinessCollateralAgreement,
                  // refGuid: this.id,
                  model: this.setPrintSetBookDocTransData()
                }
              })
          }
        ]
      }
    ];
    super.setFunctionOptionsMapping();
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateBusinessCollateralAgmTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createBusinessCollateralAgmTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onCustomerChange() {
    this.custom.clearModelByCustomer(this.model, this.creditAppRequestTableOptions);
    this.custom.setModelByCustomer(this.model, this.customerTableOptions);
    this.custom.setCreditAppRequestDropDownByCustomer(this.model).subscribe((result) => {
      this.creditAppRequestTableOptions = result;
    });
  }
  onCreditAppRequestChange() {
    this.custom.clearModelByCreditAppRequestTable(this.model);
    this.custom.setModelByCreditAppRequestTable(this.model, this.creditAppRequestTableOptions);
  }
  setPrintSetBookDocTransData(): PrintSetBookDocTransView {
    return this.custom.setPrintSetBookDocTransData(this.model);
  }
}
