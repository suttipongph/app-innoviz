import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BusinessCollateralAgmTableService } from './business-collateral-agm-table.service';
import { BusinessCollateralAgmTableListComponent } from './business-collateral-agm-table-list/business-collateral-agm-table-list.component';
import { BusinessCollateralAgmTableItemComponent } from './business-collateral-agm-table-item/business-collateral-agm-table-item.component';
import { BusinessCollateralAgmTableItemCustomComponent } from './business-collateral-agm-table-item/business-collateral-agm-table-item-custom.component';
import { BusinessCollateralAgmTableListCustomComponent } from './business-collateral-agm-table-list/business-collateral-agm-table-list-custom.component';
import { BusinessCollateralAgmLineComponentModule } from '../business-collateral-agm-line/business-collateral-agm-line.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, BusinessCollateralAgmLineComponentModule],
  declarations: [BusinessCollateralAgmTableListComponent, BusinessCollateralAgmTableItemComponent],
  providers: [BusinessCollateralAgmTableService, BusinessCollateralAgmTableItemCustomComponent, BusinessCollateralAgmTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BusinessCollateralAgmTableListComponent, BusinessCollateralAgmTableItemComponent]
})
export class BusinessCollateralAgmTableComponentModule {}
