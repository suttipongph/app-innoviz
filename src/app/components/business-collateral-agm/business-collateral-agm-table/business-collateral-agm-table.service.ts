import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  AccessModeView,
  AgreementTableInfoItemView,
  BusinessCollateralAgmTableItemView,
  BusinessCollateralAgmTableListView
} from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class BusinessCollateralAgmTableService {
  serviceKey = 'businessCollateralAgmTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBusinessCollateralAgmTableToList(search: SearchParameter): Observable<SearchResult<BusinessCollateralAgmTableListView>> {
    const url = `${this.servicePath}/GetBusinessCollateralAgmTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBusinessCollateralAgmTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBusinessCollateralAgmTable`;
    return this.dataGateway.delete(url, row);
  }
  getBusinessCollateralAgmTableById(id: string): Observable<BusinessCollateralAgmTableItemView> {
    const url = `${this.servicePath}/GetBusinessCollateralAgmTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBusinessCollateralAgmTable(vmModel: BusinessCollateralAgmTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBusinessCollateralAgmTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBusinessCollateralAgmTable(vmModel: BusinessCollateralAgmTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBusinessCollateralAgmTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(productType: number): Observable<BusinessCollateralAgmTableItemView> {
    const url = `${this.servicePath}/GetBusinessCollateralAgmTableInitialData/productType=${productType}`;
    return this.dataGateway.get(url);
  }
  validateIsManualNumberSeqBusinessCollateralAgm(companyId: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
  validateIsManualNumberSeqInternalBusinessCollateralAgm(companyId: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
  getAgreementTableInfoByBusinessCollateralAgmTable(id: string): Observable<AgreementTableInfoItemView> {
    const url = `${this.servicePath}/GetAgreementTableInfoByBusinessCollateralAgmTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByAssignmentAgreementTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByAssignmentAgreementTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getGenerateAddendumValidation(id: string): Observable<boolean> {
    const url = `${this.servicePath}/GetGenerateAddendumValidation/id=${id}`;
    return this.dataGateway.get(url);
  }
  getGenerateNoticOfCancellationValidation(id: string): Observable<boolean> {
    const url = `${this.servicePath}/GetGenerateNoticeOfCancellationValidation/id=${id}`;
    return this.dataGateway.get(url);
  }
  getVisibleManageMenu(id: string): Observable<number[]> {
    const url = `${this.servicePath}/GetVisibleManageMenu/id=${id}`;
    return this.dataGateway.get(url);
  }
  getVisibleGeneratMenu(id: string): Observable<boolean> {
    const url = `${this.servicePath}/GetVisibleGenerateMenu/id=${id}`;
    return this.dataGateway.get(url);
  }
}
