import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateCustomerTableStatusService } from './update-customer-table-status.service';
import { UpdateCustomerTableStatusComponent } from './update-customer-table-status/update-customer-table-status.component';
import { UpdateCustomerTableStatusCustomComponent } from './update-customer-table-status/update-customer-table-status-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [UpdateCustomerTableStatusComponent],
  providers: [UpdateCustomerTableStatusService, UpdateCustomerTableStatusCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [UpdateCustomerTableStatusComponent]
})
export class UpdateCustomerTableStatusComponentModule {}
