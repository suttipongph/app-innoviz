import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCustomerTableStatusComponent } from './update-customer-table-status.component';

describe('UpdateCustomerTableStatusViewComponent', () => {
  let component: UpdateCustomerTableStatusComponent;
  let fixture: ComponentFixture<UpdateCustomerTableStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UpdateCustomerTableStatusComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCustomerTableStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
