import { Observable, of } from 'rxjs';
import { BaseServiceModel, FieldAccessing, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { UpdateCustomerTableStatusParamView, UpdateCustomerTableStatusResultView } from 'shared/models/viewModel';

const firstGroup = ['CUSTOMER_ID', 'ORIGINAL_STATUS_ID'];

export class UpdateCustomerTableStatusCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getModelParam(model: UpdateCustomerTableStatusResultView): Observable<UpdateCustomerTableStatusParamView> {
    const param: UpdateCustomerTableStatusParamView = new UpdateCustomerTableStatusParamView();
    param.customerTableGUID = model.customerTableGUID;
    param.documentStatusGUID = model.documentStatusGUID;

    return of(param);
  }

  getValidateStatus(id: string, oriId: string): TranslateModel {
    const error: TranslateModel = { code: 'ERROR.90021', parameters: [] };
    return id === oriId ? error : null;
  }
  getRemakReason(id: string, options: SelectItems[]): Observable<SelectItems> {
    const item = options.find((f) => f.value === id);
    return of(item);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
}
