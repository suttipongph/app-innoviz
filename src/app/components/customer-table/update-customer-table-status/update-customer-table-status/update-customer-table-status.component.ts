import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { RefType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems, TranslateModel } from 'shared/models/systemModel';
import {
  UpdateCustomerTableBlacklistStatusParamView,
  UpdateCustomerTableStatusParamView,
  UpdateCustomerTableStatusResultView
} from 'shared/models/viewModel';
import { UpdateCustomerTableStatusService } from '../update-customer-table-status.service';
import { UpdateCustomerTableStatusCustomComponent } from './update-customer-table-status-custom.component';
@Component({
  selector: 'update-customer-table-status',
  templateUrl: './update-customer-table-status.component.html',
  styleUrls: ['./update-customer-table-status.component.scss']
})
export class UpdateCustomerTableStatusComponent extends BaseItemComponent<UpdateCustomerTableStatusResultView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  // customerTableOptions: SelectItems[] = [];
  documentReasonOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];

  constructor(
    private service: UpdateCustomerTableStatusService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: UpdateCustomerTableStatusCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<UpdateCustomerTableStatusResultView> {
    return this.service.getUpdateCustomerStatusById(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {}

  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getDocumentReasonDropDown(RefType.Customer.toString()), this.baseDropdown.getDocumentStatusDropDown()).subscribe(
      ([documentReason, documentStatus]) => {
        this.documentReasonOptions = documentReason;
        this.documentStatusOptions = documentStatus;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing().subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  onSave(validation: FormValidationModel): void {}
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.custom.getModelParam(this.model).subscribe((model) => {
          this.onSubmit(true, model);
        });
      }
    });
  }
  onSubmit(isColsing: boolean, model: UpdateCustomerTableStatusParamView): void {
    super.onExecuteFunction(this.service.updateCustomerStatus(model));
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  getDocumentStatusValidation(): TranslateModel {
    return this.custom.getValidateStatus(this.model.documentStatusGUID, this.model.originalDocumentStatusGUID);
  }
}
