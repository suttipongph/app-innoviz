import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { UpdateCustomerTableStatusParamView, UpdateCustomerTableStatusResultView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class UpdateCustomerTableStatusService {
  serviceKey = 'customerTableStatusGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getUpdateCustomerStatusById(id: string): Observable<UpdateCustomerTableStatusResultView> {
    const url = `${this.servicePath}/GetUpdateCustomerTableStatusById/id=${id}`;
    return this.dataGateway.get(url);
  }
  updateCustomerStatus(vmModel: UpdateCustomerTableStatusParamView): Observable<ResponseModel> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vmModel);
  }
}
