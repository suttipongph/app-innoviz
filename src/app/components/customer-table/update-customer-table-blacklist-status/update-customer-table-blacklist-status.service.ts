import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { UpdateCustomerTableBlacklistStatusParamView, UpdateCustomerTableBlacklistStatusResultView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class UpdateCustomerTableBlacklistStatusService {
  serviceKey = 'customerTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getUpdateCustomerTableBlacklistStatusById(id: string): Observable<UpdateCustomerTableBlacklistStatusResultView> {
    const url = `${this.servicePath}/GetUpdateCustomerTableBlacklistStatusById/id=${id}`;
    return this.dataGateway.get(url);
  }
  updateCustomerTableBlacklistStatus(vmModel: UpdateCustomerTableBlacklistStatusParamView): Observable<ResponseModel> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vmModel);
  }
}
