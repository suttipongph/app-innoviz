import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateCustomerTableBlacklistStatusService } from './update-customer-table-blacklist-status.service';
import { UpdateCustomerTableBlacklistStatusComponent } from './update-customer-table-blacklist-status/update-customer-table-blacklist-status.component';
import { UpdateCustomerTableBlacklistStatusCustomComponent } from './update-customer-table-blacklist-status/update-customer-table-blacklist-status-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [UpdateCustomerTableBlacklistStatusComponent],
  providers: [UpdateCustomerTableBlacklistStatusService, UpdateCustomerTableBlacklistStatusCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [UpdateCustomerTableBlacklistStatusComponent]
})
export class UpdateCustomerTableBlacklistStatusComponentModule {}
