import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCustomerTableBlacklistStatusComponent } from './update-customer-table-blacklist-status.component';

describe('UpdateCustomerTableBlacklistStatusViewComponent', () => {
  let component: UpdateCustomerTableBlacklistStatusComponent;
  let fixture: ComponentFixture<UpdateCustomerTableBlacklistStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UpdateCustomerTableBlacklistStatusComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCustomerTableBlacklistStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
