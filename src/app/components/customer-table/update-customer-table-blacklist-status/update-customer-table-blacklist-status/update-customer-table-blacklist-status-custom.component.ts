import { Observable, of } from 'rxjs';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { UpdateCustomerTableBlacklistStatusParamView, UpdateCustomerTableBlacklistStatusResultView } from 'shared/models/viewModel';

const firstGroup = ['CUSTOMER_ID', 'ORIGINAL_BLACKLIST_STATUS'];

export class UpdateCustomerTableBlacklistStatusCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getModelParam(model: UpdateCustomerTableBlacklistStatusResultView): Observable<UpdateCustomerTableBlacklistStatusParamView> {
    const param: UpdateCustomerTableBlacklistStatusParamView = new UpdateCustomerTableBlacklistStatusParamView();
    param.customerTableGUID = model.customerTableGUID;
    param.blacklistStatusGUID = model.blacklistStatusGUID;
    return of(param);
  }
  getRemakReason(id: string, options: SelectItems[]): Observable<SelectItems> {
    const item = options.find((f) => f.value === id);
    return of(item);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(canAction);
  }
  validateBlacklistStatus(model: UpdateCustomerTableBlacklistStatusResultView): any {
    if (model.originalBlacklistStatusGUID != null && model.originalBlacklistStatusGUID == model.blacklistStatusGUID) {
      return {
        code: 'ERROR.90021',
        parameters: []
      };
    }
    return null;
  }
}
