import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { CustomerTableItemView, CustomerTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { EmployeeTableItemView } from 'shared/models/viewModel/employeeTableItemView';
import { CustomerStatus } from 'shared/constants';
import { DocumentStatusItemView } from 'shared/models/viewModel/documentStatusItemView';
@Injectable()
export class CustomerTableService {
  serviceKey = 'customerTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCustomerTableToList(search: SearchParameter): Observable<SearchResult<CustomerTableListView>> {
    const url = `${this.servicePath}/GetCustomerTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCustomerTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCustomerTable`;
    return this.dataGateway.delete(url, row);
  }
  getCustomerTableById(id: string): Observable<CustomerTableItemView> {
    const url = `${this.servicePath}/GetCustomerTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCustomerTable(vmModel: CustomerTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCustomerTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCustomerTable(vmModel: CustomerTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCustomerTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  validateIsManualNumberSeq(id: string): Observable<boolean> {
    const url: string = `${this.servicePath}/ValidateIsManualNumberSeq/custGroupId=${id}`;
    return this.dataGateway.get(url);
  }
  getEmployeeTableByUserNameAndCompany(userId: string, companyGuid: string): Observable<EmployeeTableItemView> {
    const url: string = `${this.servicePath}/GetEmployeeTableByUserNameAndCompany/${userId}/${companyGuid}`;
    return this.dataGateway.get(url);
  }
  getStatusGUIDByStatusId(statusId: CustomerStatus): Observable<string> {
    const url: string = `${this.servicePath}/GetStatusGUIDByStatusId/id=${statusId}`;
    return this.dataGateway.get(url);
  }
  isPassportIdDuplicate(customerId: string, passPortId: string): Observable<CustomerTableItemView> {
    const url: string = `${this.servicePath}/ValidatePassportIdDuplicate/${customerId}/${passPortId}`;
    return this.dataGateway.get(url);
  }
  isTaxIdDuplicate(customerId: string, taxId: string): Observable<CustomerTableItemView> {
    const url: string = `${this.servicePath}/ValidateTaxIdDuplicate/${customerId}/${taxId}`;
    return this.dataGateway.get(url);
  }
  getInitialData(id: string): Observable<CustomerTableItemView> {
    const url = `${this.servicePath}/GetCustomerTableInitialData/companyId=${id}`;
    return this.dataGateway.get(url);
  }
  getCustomerTableByCalcAge(vmModel: CustomerTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/GetCustomerTableByCalcAge`;
    return this.dataGateway.post(url, vmModel);
  }
}
