import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CustomerTableListComponent } from './customer-table-list.component';

describe('CustomerTableListViewComponent', () => {
  let component: CustomerTableListComponent;
  let fixture: ComponentFixture<CustomerTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustomerTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
