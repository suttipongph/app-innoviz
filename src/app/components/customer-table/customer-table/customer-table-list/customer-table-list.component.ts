import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CustomerTableListView } from 'shared/models/viewModel';
import { CustomerTableService } from '../customer-table.service';
import { CustomerTableListCustomComponent } from './customer-table-list-custom.component';

@Component({
  selector: 'customer-table-list',
  templateUrl: './customer-table-list.component.html',
  styleUrls: ['./customer-table-list.component.scss']
})
export class CustomerTableListComponent extends BaseListComponent<CustomerTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  blacklistStatusOptions: SelectItems[] = [];
  businessSegmentOptions: SelectItems[] = [];
  businessSizeOptions: SelectItems[] = [];
  businessTypeOptions: SelectItems[] = [];
  creditScoringOptions: SelectItems[] = [];
  currencyOptions: SelectItems[] = [];
  custGroupOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  employeeTableOptions: SelectItems[] = [];
  exposureGroupOptions: SelectItems[] = [];
  genderOptions: SelectItems[] = [];
  gradeClassificationOptions: SelectItems[] = [];
  identificationTypeOptions: SelectItems[] = [];
  introducedByOptions: SelectItems[] = [];
  kycOptions: SelectItems[] = [];
  ledgerDimensionOptions: SelectItems[] = [];
  lineOfBusinessOptions: SelectItems[] = [];
  maritalStatusOptions: SelectItems[] = [];
  methodOfPaymentOptions: SelectItems[] = [];
  nationalityOptions: SelectItems[] = [];
  ncbAccountStatusOptions: SelectItems[] = [];
  occupationOptions: SelectItems[] = [];
  parentCompanyOptions: SelectItems[] = [];
  raceOptions: SelectItems[] = [];
  recordTypeOptions: SelectItems[] = [];
  registrationTypeOptions: SelectItems[] = [];
  territoryOptions: SelectItems[] = [];
  vendorTableOptions: SelectItems[] = [];
  withholdingTaxGroupOptions: SelectItems[] = [];

  constructor(public custom: CustomerTableListCustomComponent, private service: CustomerTableService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getIdentificationTypeEnumDropDown(this.identificationTypeOptions);
    this.baseDropdown.getRecordTypeEnumDropDown(this.recordTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'customerTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.TAX_ID',
        textKey: 'taxID',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PASSPORT_ID',
        textKey: 'passportID',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.NAME',
        textKey: 'name',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.RECORD_TYPE',
        textKey: 'recordType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.recordTypeOptions
      },
      {
        label: 'LABEL.STATUS_ID',
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentStatusOptions,
        sortingKey: 'documentStatus_StatusId',
        searchingKey: 'documentStatusGUID'
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CustomerTableListView>> {
    return this.service.getCustomerTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCustomerTable(row));
  }
}
