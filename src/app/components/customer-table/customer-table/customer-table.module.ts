import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomerTableService } from './customer-table.service';
import { CustomerTableListComponent } from './customer-table-list/customer-table-list.component';
import { CustomerTableItemComponent } from './customer-table-item/customer-table-item.component';
import { CustomerTableItemCustomComponent } from './customer-table-item/customer-table-item-custom.component';
import { CustomerTableListCustomComponent } from './customer-table-list/customer-table-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CustomerTableListComponent, CustomerTableItemComponent],
  providers: [CustomerTableService, CustomerTableItemCustomComponent, CustomerTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CustomerTableListComponent, CustomerTableItemComponent]
})
export class CustomerTableComponentModule {}
