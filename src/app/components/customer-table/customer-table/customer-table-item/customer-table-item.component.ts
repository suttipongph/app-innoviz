import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { AppConst, Dimension, EmptyGuid, IdentificationType, RecordType, RefType, ROUTE_FUNCTION_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { CustomerTableItemView, CustTransListView, LedgerDimensionItemView, RefIdParm } from 'shared/models/viewModel';
import { CustomerTableService } from '../customer-table.service';
import { CustomerTableItemCustomComponent } from './customer-table-item-custom.component';
@Component({
  selector: 'customer-table-item',
  templateUrl: './customer-table-item.component.html',
  styleUrls: ['./customer-table-item.component.scss']
})
export class CustomerTableItemComponent extends BaseItemComponent<CustomerTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  blacklistStatusOptions: SelectItems[] = [];
  businessSegmentOptions: SelectItems[] = [];
  businessSizeOptions: SelectItems[] = [];
  businessTypeOptions: SelectItems[] = [];
  creditScoringOptions: SelectItems[] = [];
  currencyOptions: SelectItems[] = [];
  custGroupOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  employeeTableOptions: SelectItems[] = [];
  exposureGroupOptions: SelectItems[] = [];
  genderOptions: SelectItems[] = [];
  gradeClassificationOptions: SelectItems[] = [];
  identificationTypeOptions: SelectItems[] = [];
  introducedByOptions: SelectItems[] = [];
  kycSetupOptions: SelectItems[] = [];
  ledgerDimensionOptions1: SelectItems[] = [];
  ledgerDimensionOptions2: SelectItems[] = [];
  ledgerDimensionOptions3: SelectItems[] = [];
  ledgerDimensionOptions4: SelectItems[] = [];
  ledgerDimensionOptions5: SelectItems[] = [];
  lineOfBusinessOptions: SelectItems[] = [];
  maritalStatusOptions: SelectItems[] = [];
  methodOfPaymentOptions: SelectItems[] = [];
  nationalityOptions: SelectItems[] = [];
  ncbAccountStatusOptions: SelectItems[] = [];
  occupationOptions: SelectItems[] = [];
  parentCompanyOptions: SelectItems[] = [];
  raceOptions: SelectItems[] = [];
  recordTypeOptions: SelectItems[] = [];
  registrationTypeOptions: SelectItems[] = [];
  territoryOptions: SelectItems[] = [];
  vendorTableOptions: SelectItems[] = [];
  withholdingTaxGroupOptions: SelectItems[] = [];
  dayOfMonthOptions: SelectItems[] = [];

  constructor(private service: CustomerTableService, private currentActivatedRoute: ActivatedRoute, public custom: CustomerTableItemCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getIdentificationTypeEnumDropDown(this.identificationTypeOptions);
    this.baseDropdown.getRecordTypeEnumDropDown(this.recordTypeOptions);
    this.baseDropdown.getDayOfMonthEnumDropDown(this.dayOfMonthOptions);
  }
  getById(): Observable<CustomerTableItemView> {
    return this.service.getCustomerTableById(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }
  onAsyncRunner(model?: any): void {
    forkJoin(
      this.baseDropdown.getBlacklistStatusDropDown(),
      this.baseDropdown.getBusinessSegmentDropDown(),
      this.baseDropdown.getBusinessSizeDropDown(),
      this.baseDropdown.getBusinessTypeDropDown(),
      this.baseDropdown.getCreditScoringDropDown(),
      this.baseDropdown.getCurrencyDropDown(),
      this.baseDropdown.getCustGroupDropDown(),
      this.baseDropdown.getDocumentStatusDropDown(),
      this.baseDropdown.getEmployeeTableDropDown(),
      this.baseDropdown.getExposureGroupDropDown(),
      this.baseDropdown.getGenderDropDown(),
      this.baseDropdown.getGradeClassificationDropDown(),
      this.baseDropdown.getIntroducedByDropDown(),
      this.baseDropdown.getKYCSetupDropDown(),
      this.baseDropdown.getLedgerDimensionDropDown(),
      this.baseDropdown.getLineOfBusinessDropDown(),
      this.baseDropdown.getMaritalStatusDropDown(),
      this.baseDropdown.getMethodOfPaymentDropDown(),
      this.baseDropdown.getNationalityDropDown(),
      this.baseDropdown.getNCBAccountStatusDropDown(),
      this.baseDropdown.getOccupationDropDown(),
      this.baseDropdown.getParentCompanyDropDown(),
      this.baseDropdown.getRaceDropDown(),
      this.baseDropdown.getRegistrationTypeDropDown(),
      this.baseDropdown.getTerritoryDropDown(),
      this.baseDropdown.getVendorTableDropDown(),
      this.baseDropdown.getWithholdingTaxGroupDropDown()
    ).subscribe(
      ([
        blacklistStatus,
        businessSegment,
        businessSize,
        businessType,
        creditScoring,
        currency,
        custGroup,
        documentStatus,
        employeeTable,
        exposureGroup,
        gender,
        gradeClassification,
        introducedBy,
        kycSetup,
        ledgerDimension,
        lineOfBusiness,
        maritalStatus,
        methodOfPayment,
        nationality,
        ncbAccountStatus,
        occupation,
        parentCompany,
        race,
        registrationType,
        territory,
        vendorTable,
        withholdingTaxGroup
      ]) => {
        this.blacklistStatusOptions = blacklistStatus;
        this.businessSegmentOptions = businessSegment;
        this.businessSizeOptions = businessSize;
        this.businessTypeOptions = businessType;
        this.creditScoringOptions = creditScoring;
        this.currencyOptions = currency;
        this.custGroupOptions = custGroup;
        this.documentStatusOptions = documentStatus;
        this.employeeTableOptions = employeeTable;
        this.exposureGroupOptions = exposureGroup;
        this.genderOptions = gender;
        this.gradeClassificationOptions = gradeClassification;
        this.introducedByOptions = introducedBy;
        this.kycSetupOptions = kycSetup;
        this.lineOfBusinessOptions = lineOfBusiness;
        this.maritalStatusOptions = maritalStatus;
        this.methodOfPaymentOptions = methodOfPayment;
        this.nationalityOptions = nationality;
        this.ncbAccountStatusOptions = ncbAccountStatus;
        this.occupationOptions = occupation;
        this.parentCompanyOptions = parentCompany;
        this.raceOptions = race;
        this.registrationTypeOptions = registrationType;
        this.territoryOptions = territory;
        this.vendorTableOptions = vendorTable;
        this.withholdingTaxGroupOptions = withholdingTaxGroup;
        this.setLedgerDimension(ledgerDimension);

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        command: () => {
          const refIdParm: RefIdParm = { refType: RefType.Customer, refGUID: this.model.customerTableGUID };
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: refIdParm
          });
        }
      },
      {
        label: 'LABEL.MEMO',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.MEMO })
      },
      {
        label: 'LABEL.MANAGE',
        items: [
          {
            label: 'LABEL.ADDRESS',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.ADDRESS_TRANS })
          },
          {
            label: 'LABEL.BANK',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.CUST_BANK })
          },
          {
            label: 'LABEL.CONTACT_TRANSACTIONS',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.CONTACT })
          },
          {
            label: 'LABEL.JOINT_VENTURE',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.JOINT_VENTURE_TRANS })
          },
          { label: 'LABEL.BUSINESS_COLLATERAL', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.BUSINESS_COLLATERAL }) }
        ]
      },
      {
        label: 'LABEL.RELATED_PERSON',
        items: [
          {
            label: 'LABEL.CONTACT_PERSON',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.CONTACT_PERSON })
          },
          {
            label: 'LABEL.AUTHORIZED_PERSON',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.AUTHORIZED_PERSON_TRANS })
          },
          {
            label: 'LABEL.GUARANTOR',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.GUARANTOR_TRANS })
          },
          {
            label: 'LABEL.OWNER',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.OWNER_TRANS })
          }
        ]
      },
      {
        label: 'LABEL.FINANCIAL',
        items: [
          {
            label: 'LABEL.FINANCIAL_STATEMENT_TRANSACTION',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.FINANCIAL_STATEMENT_TRANS,
                parameters: {
                  refType: RefType.Customer,
                  refGUID: this.model.customerTableGUID
                }
              })
          },
          {
            label: 'LABEL.TAX_REPORT',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.TAX_REPORT_TRANS,
                parameters: {
                  refType: RefType.Customer,
                  refGUID: this.model.customerTableGUID
                }
              })
          }
        ]
      },
      {
        label: 'LABEL.INQUIRY',
        items: [
          {
            label: 'LABEL.CUSTOMER_TRANSACTIONS',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.CUST_TRANS
              })
          },
          {
            label: 'LABEL.RETENTION_TRANSACTIONS',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.RETENTION_TRANSACTION
              })
          },
          {
            label: 'LABEL.RETENTION_OUTSTANDING',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.RETENTION_OUTSTANDING
              })
          },
          {
            label: 'LABEL.ASSIGNMENT_AGREEMENT_OUTSTANDING',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.INQUIRY_ASSIGNMENT_AGREEMENT_OUTSTANDING
              })
          },
          {
            label: 'LABEL.CREDIT_OUTSTANDING',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.CREDIT_OUTSTANDING,
                parameters: {
                  refGUID: this.model.customerTableGUID,
                  refType: RefType.Customer
                }
              })
          }
        ]
      },
      {
        label: 'LABEL.CUSTOMER_CREDIT_LIMIT_BY_PRODUCT',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.CUSTOMER_CREDIT_LIMIT_BY_PRODUCT,
            parameters: { refGUID: this.model.customerTableGUID, refType: RefType.Customer }
          })
      },
      {
        label: 'LABEL.CUSTOMER_VISITING',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.CUST_VISITING_TRANS })
      },

      {
        label: 'LABEL.PROJECT_REFERENCE',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.PROJECT_REFERENCE })
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(): void {
    this.functionItems = [
      {
        label: 'LABEL.UPDATE_CUSTOMER_STATUS',
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.CUSTOMERTABLE_UPDATE_STATUS })
      },
      {
        label: 'LABEL.UPDATE_BLACKLIST_AND_WATCH_LIST_STATUS',
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.CUSTOMERTABLE_UPDATE_BLACKLIST_AND_WATCHLIST_STATUS })
      }
    ];
    super.setFunctionOptionsMapping();
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateCustomerTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createCustomerTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onRecordTypeChange(): void {
    this.custom.setModelByRecordType(this.model);
    this.custom.setIdentificationType(this.model);
    super.setBaseFieldAccessing(this.custom.setFieldAccessingByRecordType(this.model.recordType));
    super.setBaseFieldAccessing(this.custom.setFieldAccessingByIdentificationType(this.model.identificationType));
  }
  onIdentificationTypeChange(): void {
    this.custom.setIdentificationType(this.model);
    super.setBaseFieldAccessing(this.custom.setFieldAccessingByIdentificationType(this.model.identificationType));
  }
  onIdentificationTypeBinding(): void {
    super.setBaseFieldAccessing(this.custom.setFieldAccessingByIdentificationType(this.model.identificationType));
  }
  onRecordTypeBinding(): void {
    super.setBaseFieldAccessing(this.custom.setFieldAccessingByRecordType(this.model.recordType));
  }
  onCustGroupChange(): void {
    this.custom.setFieldAccessingByCustGroup(this.model).subscribe((result) => {
      super.setBaseFieldAccessing(result);
    });
  }
  onPassportIDChange(): void {
    this.custom.getIsPassportIdDuplicate(this.model);
  }
  onTaxIDChange(): void {
    this.custom.getIsTaxIdDuplicate(this.model);
  }
  getInitialData(): Observable<CustomerTableItemView> {
    return this.service.getInitialData(this.userDataService.getCurrentCompanyGUID());
  }
  getcusttranParamModel(): CustomerTableItemView {
    return this.custom.getcusttranParamModel(this.model);
  }

  onDateOfBirthChange() {
    this.custom.calcAge(this.model);
  }

  // validation
  onDateOfBirthValidation(): void {
    return this.custom.validateDateOfBirth(this.model.dateOfBirth);
  }
  onDateOfEstablishChange(): void {
    return this.custom.validateDateOfEstablish(this.model.dateOfEstablish);
  }
  onDateOfIssueChange(): void {
    return this.custom.validateDateOfIssue(this.model.dateOfIssue);
  }
  onDateOfExpiryChange(): void {
    return this.custom.validateDateOfExpiry(this.model.dateOfExpiry, this.model.dateOfIssue);
  }
  onPassportIDValidation(): void {
    return this.custom.validatePassportID(this.model.passportID, this.model);
  }
  onTaxIDValidation(): void {
    return this.custom.validateTaxID(this.model.taxID, this.model);
  }
  setLedgerDimension(ledgerDimension: SelectItems[]): void {
    this.ledgerDimensionOptions1 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension1);
    this.ledgerDimensionOptions2 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension2);
    this.ledgerDimensionOptions3 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension3);
    this.ledgerDimensionOptions4 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension4);
    this.ledgerDimensionOptions5 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension5);
  }
}
