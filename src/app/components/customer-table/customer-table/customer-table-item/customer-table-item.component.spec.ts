import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerTableItemComponent } from './customer-table-item.component';

describe('CustomerTableItemViewComponent', () => {
  let component: CustomerTableItemComponent;
  let fixture: ComponentFixture<CustomerTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustomerTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
