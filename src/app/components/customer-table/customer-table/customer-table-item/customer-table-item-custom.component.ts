import { TranslateService } from '@ngx-translate/core';
import { AppInjector } from 'app-injector';
import { NotificationService } from 'core/services/notification.service';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { IdentificationType, RecordType } from 'shared/constants';
import { datetoString, isDateFromGreaterThanDateTo, stringToDate } from 'shared/functions/date.function';
import { isNullOrUndefined, isNullOrUndefOrEmpty, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CustomerTableItemView, CustTransListView } from 'shared/models/viewModel';
import { CustomerTableService } from '../customer-table.service';

const numberSeqGroup = ['CUSTOMER_ID'];
const firstGroup = ['CUST_GROUP_GUID', 'DOCUMENT_STATUS_GUID', 'RESPONSIBLE_BY_GUID'];
const secondGroup = ['AGE', 'BLACKLIST_STATUS_GUID'];
const thirdGroup = ['PASSPORT_ID', 'WORK_PERMIT_ID'];

const condition1 = [
  'BUSINESS_SEGMENT_GUID',
  'REGISTRATION_TYPE_GUID',
  'PUBLIC_AR_PCT',
  'PRIVATE_AR_PCT',
  'DATE_OF_ESTABLISH',
  'PAID_UP_CAPITAL',
  'REGISTERED_CAPITAL',
  'LABOR',
  'FIXED_ASSET'
];
const condition2 = [
  'IDENTIFICATION_TYPE',
  'DATE_OF_BIRTH',
  'NATIONALITY_GUID',
  'RACE_GUID',
  'GENDER_GUID',
  'MARITAL_STATUS_GUID',
  'SPOUSE_NAME',
  'OCCUPATION_GUID',
  'POSITION',
  'WORK_EXPERIENCE_YEAR',
  'WORK_EXPERIENCE_MONTH',
  'COMPANY_NAME',
  'INCOME',
  'OTHER_SOURCE_INCOME',
  'OTHER_INCOME'
];
const condition3 = ['PASSPORT_ID', 'WORK_PERMIT_ID'];
const condition4 = ['TAX_ID'];

export class CustomerTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  isPassportIdMandatory: boolean = false;
  isTaxIdMandatory: boolean = false;
  isWorkPermitIdMandatory: boolean = false;

  isCondition4Required: boolean = false;
  isTax: boolean = false;
  notificationService = AppInjector.get(NotificationService);
  translateService = AppInjector.get(TranslateService);
  isManunal: boolean = false;
  getFieldAccessing(model: CustomerTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: numberSeqGroup, readonly: true });
    fieldAccessing.push({ filedIds: secondGroup, readonly: true });
    if (!isUpdateMode(model.customerTableGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: false });
      fieldAccessing.push({ filedIds: thirdGroup, readonly: true });
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      fieldAccessing.push({ filedIds: thirdGroup, readonly: false });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CustomerTableItemView): Observable<boolean> {
    const accessright = isNullOrUndefOrEmptyGUID(model.customerTableGUID) ? canCreate : canUpdate;
    return of(!accessright);
  }
  validateIsManualNumberSeq(userDataService: CustomerTableService, companyID: string): Observable<boolean> {
    return userDataService.validateIsManualNumberSeq(companyID);
  }
  setFieldAccessingByRecordType(recordType: number): FieldAccessing[] {
    switch (recordType) {
      case RecordType.Person:
        return [
          { filedIds: condition1, readonly: true },
          { filedIds: condition2, readonly: false }
        ];
      case RecordType.Organization:
        return [
          { filedIds: condition1, readonly: false },
          { filedIds: condition2, readonly: true }
        ];
      default:
        return [];
    }
  }
  setFieldAccessingByIdentificationType(identificationType: number): FieldAccessing[] {
    switch (identificationType) {
      case IdentificationType.TaxId:
        this.isTaxIdMandatory = true;
        this.isPassportIdMandatory = false;
        this.isWorkPermitIdMandatory = false;
        return [
          { filedIds: condition3, readonly: true },
          { filedIds: condition4, readonly: false }
        ];
      case IdentificationType.PassportId:
        this.isTaxIdMandatory = false;
        this.isPassportIdMandatory = true;
        this.isWorkPermitIdMandatory = false;
        return [
          { filedIds: condition3, readonly: false },
          { filedIds: condition4, readonly: true }
        ];
      default:
        return [];
    }
  }

  setFieldAccessingByCustGroup(model: CustomerTableItemView) {
    model.customerId = null;
    if (!isNullOrUndefined(model.custGroupGUID)) {
      let service: CustomerTableService = this.baseService.service;
      return service.validateIsManualNumberSeq(model.custGroupGUID).pipe(
        map((result) => {
          this.isManunal = result;
          this.getIsPassportIdDuplicate(model);
          return [{ filedIds: ['CUSTOMER_ID'], readonly: !result }];
        }),
        catchError((e) => {
          this.baseService.notificationService.showErrorMessageFromResponse(e);
          this.isManunal = true;
          this.getIsPassportIdDuplicate(model);
          return of([{ filedIds: ['CUSTOMER_ID'], readonly: false }]);
        })
      );
    } else {
      this.isManunal = true;
      this.getIsPassportIdDuplicate(model);
      return of([{ filedIds: ['CUSTOMER_ID'], readonly: false }]);
    }
  }
  validateDateOfBirth(date: string): any {
    if (isDateFromGreaterThanDateTo(date, datetoString(new Date()))) {
      return {
        code: 'ERROR.DATE_CANNOT_GREATER_THAN_SYSTEM_DATE',
        parameters: [this.translateService.instant('LABEL.DATE_OF_BIRTH')]
      };
    }
    return null;
  }
  validateDateOfEstablish(date: string): any {
    if (isDateFromGreaterThanDateTo(date, datetoString(new Date()))) {
      return {
        code: 'ERROR.DATE_CANNOT_GREATER_THAN_SYSTEM_DATE',
        parameters: [this.translateService.instant('LABEL.DATE_OF_ESTABLISH')]
      };
    }
    return null;
  }
  validateDateOfIssue(date: string): any {
    if (isDateFromGreaterThanDateTo(date, datetoString(new Date()))) {
      return {
        code: 'ERROR.DATE_CANNOT_GREATER_THAN_SYSTEM_DATE',
        parameters: [this.translateService.instant('LABEL.DATE_OF_ISSUE')]
      };
    }
    return null;
  }
  validateDateOfExpiry(dateOfExpiry: string, dateOfIssue: string): any {
    if (!isNullOrUndefOrEmptyGUID(dateOfExpiry) && !isNullOrUndefOrEmptyGUID(dateOfIssue)) {
      if (isDateFromGreaterThanDateTo(dateOfIssue, dateOfExpiry)) {
        return {
          code: 'ERROR.DATE_CANNOT_LESS_THAN',
          parameters: [this.translateService.instant('LABEL.DATE_OF_EXPIRY'), this.translateService.instant('LABEL.DATE_OF_ISSUE')]
        };
      } else {
        return null;
      }
    }
    return null;
  }

  getIsPassportIdDuplicate(model: CustomerTableItemView) {
    if (!isNullOrUndefOrEmpty(model.passportID)) {
      this.baseService.service.isPassportIdDuplicate(model.customerTableGUID, model.passportID).subscribe((result) => {
        model.isPassportIdDuplicate = result.isPassportIdDuplicate;
        model.passportDuplicateEmployeeName = result.passportDuplicateEmployeeName;
      });
    }
  }
  getIsTaxIdDuplicate(model: CustomerTableItemView) {
    if (!isNullOrUndefOrEmpty(model.taxID)) {
      this.baseService.service.isTaxIdDuplicate(model.customerTableGUID, model.taxID).subscribe((result) => {
        model.isTaxIdDuplicate = result.isTaxIdDuplicate;
        model.passportDuplicateEmployeeName = result.passportDuplicateEmployeeName;
      });
    }
  }
  validatePassportID(passportID: string, model: CustomerTableItemView): any {
    if (model.identificationType == IdentificationType.PassportId && !isNullOrUndefOrEmptyGUID(passportID)) {
      if (model.isPassportIdDuplicate) {
        return {
          code: 'ERROR.90006',
          parameters: [this.translateService.instant('LABEL.PASSPORT_ID'), model.passportDuplicateEmployeeName]
        };
      } else {
        return null;
      }
    } else {
      return null;
    }
  }
  validateTaxID(taxId: string, model: CustomerTableItemView): any {
    if (model.identificationType == IdentificationType.TaxId && !isNullOrUndefOrEmptyGUID(taxId)) {
      if (model.isTaxIdDuplicate) {
        return {
          code: 'ERROR.90006',
          parameters: [this.translateService.instant('LABEL.TAX_ID'), model.passportDuplicateEmployeeName]
        };
      } else {
        return null;
      }
    } else {
      return null;
    }
  }
  clearRegistrationGroup(model: CustomerTableItemView) {
    model.businessSegmentGUID = null;
    model.registrationTypeGUID = null;
    model.publicARPct = 0;
    model.privateARPct = 0;
    model.dateOfEstablish = null;
    model.paidUpCapital = 0;
    model.registeredCapital = 0;
    model.labor = 0;
    model.fixedAsset = 0;
  }
  clearPersonalInformationGroup(model: CustomerTableItemView) {
    model.dateOfBirth = null;
    model.nationalityGUID = null;
    model.raceGUID = null;
    model.genderGUID = null;
    model.maritalStatusGUID = null;
    model.spouseName = '';
    model.occupationGUID = null;
    model.position = null;
    model.workExperienceYear = 0;
    model.workExperienceMonth = 0;
    model.companyName = '';
    model.income = 0;
    model.otherSourceIncome = '';
    model.otherIncome = 0;
  }
  setModelByRecordType(model: CustomerTableItemView): void {
    switch (model.recordType) {
      case RecordType.Person:
        this.clearRegistrationGroup(model);
        break;
      case RecordType.Organization:
        this.clearPersonalInformationGroup(model);
        model.identificationType = IdentificationType.TaxId;
        break;
      default:
        break;
    }
  }
  setIdentificationType(model: CustomerTableItemView): void {
    switch (model.identificationType) {
      case IdentificationType.TaxId: {
        model.passportID = '';
        break;
      }
      case IdentificationType.PassportId: {
        model.taxID = '';
        break;
      }
    }
  }
  getcusttranParamModel(model: CustomerTableItemView): CustomerTableItemView {
    let paramModel = new CustomerTableItemView();
    paramModel = model;
    return paramModel;
  }

  calcAge(model: CustomerTableItemView): void {
    if (!isNullOrUndefined(model.dateOfBirth)) {
      this.baseService.service.getCustomerTableByCalcAge(model).subscribe((result) => {
        if (result < 0) {
          model.age = 0;
        } else {
          model.age = result;
        }
      });
    } else {
      model.age = 0;
    }
  }
}
