import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InvoiceTableListComponent } from './invoice-table-list.component';

describe('InvoiceTableListViewComponent', () => {
  let component: InvoiceTableListComponent;
  let fixture: ComponentFixture<InvoiceTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InvoiceTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
