import { ROUTE_RELATED_GEN } from './../../../../shared/constants/constantGen';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { RefType } from 'shared/constants';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView, InvoiceTableListView } from 'shared/models/viewModel';

const RECEIPTTEMPTABLE = 'receipttemptable';
const PURCHASE_LINE_PURCHASE = 'purchaselinepurchase-child';
const WITHDRAWAL_LINE = 'withdrawallinewithdrawal-child';
const SERVICE_FEE_TRANS = 'servicefeetrans';
const WITHDRAWAL_LINE_TERMEXTENSION = 'withdrawallinetermextension-child';

export class InvoiceTableListCustomComponent {
  uiControllerService: UIControllerService;
  baseService: BaseServiceModel<any>;
  parentId: string = null;
  parentName: string;

  model: InvoiceTableListView = new InvoiceTableListView();

  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }

  getRelatedInfoTableKey(): string {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    if (this.parentName) {
      this.parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
    } else {
      this.parentId = null;
    }
    return this.parentId;
  }

  setInitialListData(): Observable<InvoiceTableListView> {
    this.model = new InvoiceTableListView();
    this.getRelatedInfoTableKey();
    switch (this.parentName) {
      case RECEIPTTEMPTABLE:
        this.model.refType = RefType.ReceiptTemp;
        return of(this.model);
      case PURCHASE_LINE_PURCHASE:
        this.model.refType = RefType.PurchaseLine;
        return of(this.model);
      case SERVICE_FEE_TRANS:
        this.model.refType = RefType.ServiceFeeTrans;
        return of(this.model);
      case WITHDRAWAL_LINE:
        this.model.refType = RefType.WithdrawalLine;
        return of(this.model);
      case WITHDRAWAL_LINE_TERMEXTENSION:
        this.model.refType = RefType.WithdrawalLine;
        return of(this.model);
      case ROUTE_RELATED_GEN.FREE_TEXT_INVOICE_TABLE:
        this.model.refType = RefType.FreeTextInvoice;
        return of(this.model);
      default:
        this.model.refType = null;
        return of(this.model);
    }
  }
}
