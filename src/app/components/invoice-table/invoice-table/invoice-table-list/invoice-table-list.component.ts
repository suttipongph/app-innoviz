import { Component, Input, OnInit } from '@angular/core';
import { Selector } from '@ngrx/store';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { InvoiceTableListView } from 'shared/models/viewModel';
import { InvoiceTableService } from '../invoice-table.service';
import { InvoiceTableListCustomComponent } from './invoice-table-list-custom.component';

@Component({
  selector: 'invoice-table-list',
  templateUrl: './invoice-table-list.component.html',
  styleUrls: ['./invoice-table-list.component.scss']
})
export class InvoiceTableListComponent extends BaseListComponent<InvoiceTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  custTransStatusOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  suspenseInvoiceTypeOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  invoiceTypeOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  buyerInvoiceTableOptions: SelectItems[] = [];
  creditAppOptions: SelectItems[] = [];
  RefType: string = null;
  constructor(public custom: InvoiceTableListCustomComponent, private service: InvoiceTableService, public uiControllerService: UIControllerService) {
    super();
    this.custom.baseService = this.baseService;
    this.parentId = this.custom.getRelatedInfoTableKey(); //case related and inquiry
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.setInitialListData().subscribe((result) => {
      this.model = result;
      if (isNullOrUndefined(this.model.refType)) {
        this.RefType = null;
      } else {
        this.RefType = this.model.refType.toString();
      }
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getCustTransEnumDropDown(this.custTransStatusOptions);
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.baseDropdown.getSuspenseInvoiceTypeEnumDropDown(this.suspenseInvoiceTypeOptions);
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getInvoiceTypeDropDown(this.invoiceTypeOptions);
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
    this.baseDropdown.getBuyerInvoiceTableDropDown(this.buyerInvoiceTableOptions);
    this.baseDropdown.getCreditAppTableDropDown(this.creditAppOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = true;
    this.option.canDelete = false;
    this.option.key = 'invoiceTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.INVOICE_ID',
        textKey: 'invoiceId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId'
      },
      {
        label: 'LABEL.CREDIT_APPLICATION_ID',
        textKey: 'creditAppTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditAppOptions,
        searchingKey: 'creditAppTableGUID',
        sortingKey: 'CreditAppTable_CreditAppId'
      },
      {
        label: 'LABEL.ISSUED_DATE',
        textKey: 'issuedDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.INVOICE_TYPE_ID',
        textKey: 'invoiceType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.invoiceTypeOptions,
        searchingKey: 'invoiceTypeGUID',
        sortingKey: 'invoiceType_InvoiceTypeId'
      },
      {
        label: 'LABEL.DOCUMENT_ID',
        textKey: 'documentId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerTableOptions,
        searchingKey: 'buyerTableGUID',
        sortingKey: 'buyerTable_BuyerId'
      },
      {
        label: 'LABEL.BUYER_INVOICE_ID',
        textKey: 'buyerInvoiceTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerInvoiceTableOptions,
        searchingKey: 'buyerInvoiceTableGUID',
        sortingKey: 'buyerInvoiceTable_BuyerInvoiceId'
      },
      {
        label: 'LABEL.CUSTOMER_TRANSACTIONS_STATUS',
        textKey: 'custTransStatus',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.custTransStatusOptions
      },
      {
        label: null,
        textKey: 'dueDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        disabled: true,
        sorting: SortType.DESC
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      },
      {
        label: null,
        textKey: 'refType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.RefType
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<InvoiceTableListView>> {
    return this.service.getInvoiceTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteInvoiceTable(row));
  }
}
