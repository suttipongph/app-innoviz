import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InvoiceTableService } from './invoice-table.service';
import { InvoiceTableListComponent } from './invoice-table-list/invoice-table-list.component';
import { InvoiceTableItemComponent } from './invoice-table-item/invoice-table-item.component';
import { InvoiceTableItemCustomComponent } from './invoice-table-item/invoice-table-item-custom.component';
import { InvoiceTableListCustomComponent } from './invoice-table-list/invoice-table-list-custom.component';
import { InvoiceLineComponentModule } from '../invoice-line/invoice-line.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, InvoiceLineComponentModule],
  declarations: [InvoiceTableListComponent, InvoiceTableItemComponent],
  providers: [InvoiceTableService, InvoiceTableItemCustomComponent, InvoiceTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [InvoiceTableListComponent, InvoiceTableItemComponent]
})
export class InvoiceTableComponentModule {}
