import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, Dimension, RefType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { InvoiceTableItemView, LedgerDimensionItemView } from 'shared/models/viewModel';
import { InvoiceTableService } from '../invoice-table.service';
import { InvoiceTableItemCustomComponent } from './invoice-table-item-custom.component';
@Component({
  selector: 'invoice-table-item',
  templateUrl: './invoice-table-item.component.html',
  styleUrls: ['./invoice-table-item.component.scss']
})
export class InvoiceTableItemComponent extends BaseItemComponent<InvoiceTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  receiptTempTableOptions: SelectItems[] = [];
  custTransStatusOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  suspenseInvoiceTypeOptions: SelectItems[] = [];

  customerOption: SelectItems[] = [];
  invoiceTypeOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  productype: SelectItems[] = [];
  creditAppOptions: SelectItems[] = [];
  methodOfPaymentOption: SelectItems[] = [];
  buyerTableOption: SelectItems[] = [];
  buyerAgreementTableOption: SelectItems[] = [];
  buyerInvoiceTableOption: SelectItems[] = [];
  documentReasonOption: SelectItems[] = [];
  currencyOption: SelectItems[] = [];
  taxInvoiceTableOption: SelectItems[] = [];
  ledgerDimensionOptions1: SelectItems[] = [];
  ledgerDimensionOptions2: SelectItems[] = [];
  ledgerDimensionOptions3: SelectItems[] = [];
  ledgerDimensionOptions4: SelectItems[] = [];
  ledgerDimensionOptions5: SelectItems[] = [];
  refInvoiceOptions: SelectItems[] = [];

  constructor(
    private service: InvoiceTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: InvoiceTableItemCustomComponent,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {
    this.baseDropdown.getCustTransEnumDropDown(this.custTransStatusOptions);
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.baseDropdown.getSuspenseInvoiceTypeEnumDropDown(this.suspenseInvoiceTypeOptions);
  }
  getById(): Observable<InvoiceTableItemView> {
    return this.service.getInvoiceTableById(this.id);
  }
  getInitialData(): Observable<InvoiceTableItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions(result);
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin([
      this.baseDropdown.getCustomerTableDropDown(),
      this.baseDropdown.getInvoiceTypeDropDown(),
      this.baseDropdown.getDocumentStatusDropDown(),
      this.baseDropdown.getCreditAppTableDropDown(),
      this.baseDropdown.getMethodOfPaymentDropDown(),
      this.baseDropdown.getBuyerTableDropDown(),
      this.baseDropdown.getBuyerInvoiceTableDropDown(),
      this.baseDropdown.getBuyerAgreementTableDropDown(),
      this.baseDropdown.getDocumentReasonDropDown(RefType.Invoice.toString()),
      this.baseDropdown.getCurrencyDropDown(),
      this.baseDropdown.getLedgerDimensionDropDown(),
      this.custom.getRefInvoiceTableOptions(model),
      this.baseDropdown.getTaxInvoiceTableDropDown(),
      this.baseDropdown.getReceiptTempTableDropDown()
    ]).subscribe(
      ([
        customer,
        invoiceType,
        documentStatus,
        creditApp,
        methodOfPayment,
        buyerTable,
        buyerInvoiceTable,
        buyerAgreement,
        reason,
        currency,
        ledgerDimension,
        refInvoiceTable,
        taxinvoiceTable,
        receiptTempTable
      ]) => {
        this.customerOption = customer;
        this.invoiceTypeOptions = invoiceType;
        this.documentStatusOptions = documentStatus;
        this.creditAppOptions = creditApp;
        this.methodOfPaymentOption = methodOfPayment;
        this.buyerTableOption = buyerTable;
        this.buyerInvoiceTableOption = buyerInvoiceTable;
        this.buyerAgreementTableOption = buyerAgreement;
        this.documentReasonOption = reason;
        this.currencyOption = currency;
        this.refInvoiceOptions = refInvoiceTable;
        this.taxInvoiceTableOption = taxinvoiceTable;
        this.setLedgerDimension(ledgerDimension);
        this.receiptTempTableOptions = receiptTempTable;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setRelatedInfoOptions(model);
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  setRelatedInfoOptions(model: InvoiceTableItemView): void {
    this.relatedInfoItems = [
      // { label: 'LABEL.ATTACHMENT', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.ATTACHMENT }) },
      {
        label: 'LABEL.CUSTOMER_TRANSACTIONS',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.CUST_TRANS,
            parameters: {
              name: model.customerName,
              customerId: ''
            }
          })
      },
      {
        label: 'LABEL.INQUIRY',
        items: [{ label: 'LABEL.PAYMENT_HISTORY', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.PAYMENT_HISTORY }) }]
      },
      { label: 'LABEL.TAX_INVOICE', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.TAX_INVOICE }), disabled: false }
      // { label: 'LABEL.RECEIPT', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.RECEIPT }) }
    ];
    super.setRelatedinfoOptionsMapping();
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateInvoiceTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createInvoiceTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  setLedgerDimension(ledgerDimension: SelectItems[]): void {
    this.ledgerDimensionOptions1 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension1);
    this.ledgerDimensionOptions2 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension2);
    this.ledgerDimensionOptions3 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension3);
    this.ledgerDimensionOptions4 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension4);
    this.ledgerDimensionOptions5 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension5);
  }
}
