import { Observable, of } from 'rxjs';
import { EmptyGuid, RefType } from 'shared/constants';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { InvoiceTableItemView, InvoiceTableListView } from 'shared/models/viewModel';

const firstGroup = [
  'INVOICE_ID',
  'CUSTOMER_TABLE_GUID',
  'CUSTOMER_NAME',
  'INVOICE_TYPE_GUID',
  'DOCUMENT_STATUS_GUID',
  'PRODUCT_TYPE',
  'PRODUCT_INVOICE',
  'SUSPENSE_INVOICE_TYPE',
  'CREDIT_APP_TABLE_GUID',
  'ISSUED_DATE',
  'DUE_DATE',
  'METHOD_OF_PAYMENT_GUID',
  'DOCUMENT_ID',
  'BUYER_TABLE_GUID',
  'BUYER_INVOICE_TABLE_GUID',
  'BUYER_AGREEMENT_TABLE_GUID',
  'CUST_TRANS_STATUS',
  'REMARK',
  'ACCOUNTING_PERIOD',
  'MARKETING_PERIOD',
  'UNBOUND_INVOICE_ADDRESS',
  'INVOICE_ADDRESS1',
  'INVOICE_ADDRESS2',
  'TAX_BRANCH_ID',
  'TAX_BRANCH_NAME',
  'UNBOUND_MAILING_ADDRESS',
  'MAILING_INVOICE_ADDRESS1',
  'MAILING_INVOICE_ADDRESS2',
  'CURRENCY_GUID',
  'EXCHANGE_RATE',
  'INVOICE_AMOUNT_BEFORE_TAX',
  'TAX_AMOUNT',
  'INVOICE_AMOUNT',
  'WHT_AMOUNT',
  'INVOICE_AMOUNT_BEFORE_TAX_MST',
  'TAX_AMOUNT_MST',
  'INVOICE_AMOUNT_MST',
  'WHT_BASE_AMOUNT',
  'CN_REASON_GUID',
  'ORIG_INVOICE_ID',
  'ORIG_INVOICE_AMOUNT',
  'ORIG_TAX_INVOICE_ID',
  'ORIG_TAX_INVOICE_AMOUNT',
  'REF_TAX_INVOICE_GUID',
  'REF_INVOICE_GUID',
  'REF_TYPE',
  'REF_ID',
  'REF_GUID',
  'RECEIPT_TEMP_TABLE_GUID',
  'DIMENSION1GUID',
  'DIMENSION2GUID',
  'DIMENSION3GUID',
  'DIMENSION4GUID',
  'DIMENSION5GUID',
  'INVOICE_TABLE_GUID'
];
const WITHDRAWAL_LINE = 'withdrawallinewithdrawal-child';
const RECEIPTTEMPTABLE = 'receipttemptable';
const PURCHASE_LINE_PURCHASE = 'purchaselinepurchase-child';
const SERVICE_FEE_TRANS = 'servicefeetrans';
const WITHDRAWAL_LINE_TERMEXTENSION = 'withdrawallinetermextension-child';

export class InvoiceTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  parentName: string;
  model: InvoiceTableListView = new InvoiceTableListView();
  parentId: string = null;
  getFieldAccessing(model: InvoiceTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: InvoiceTableItemView): Observable<boolean> {
    const accessright = isNullOrUndefOrEmptyGUID(model.invoiceTableGUID) ? canCreate : canUpdate;
    const accessLogic = false;
    const parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (parentName) {
      case WITHDRAWAL_LINE:
        return of(true);
      default:
        return of(!(accessLogic && accessright));
    }
  }
  getInitialData(): Observable<InvoiceTableItemView> {
    let model = new InvoiceTableItemView();
    model.invoiceTableGUID = EmptyGuid;
    return of(model);
  }
  getRefInvoiceTableOptions(model: InvoiceTableItemView): Observable<SelectItems[]> {
    if (!isNullOrUndefined(model.refInvoiceGUID)) {
      return this.baseService.baseDropdown.getRefInvoiceTableDropDown(model.refInvoiceGUID);
    } else {
      return of([]);
    }
  }

  setInitialListData(): Observable<InvoiceTableListView> {
    this.model = new InvoiceTableListView();
    this.getRelatedInfoTableKey();
    switch (this.parentName) {
      case RECEIPTTEMPTABLE:
        this.model.refType = RefType.ReceiptTemp;
        return of(this.model);
      case PURCHASE_LINE_PURCHASE:
        this.model.refType = RefType.PurchaseLine;
        return of(this.model);
      case SERVICE_FEE_TRANS:
        this.model.refType = RefType.ServiceFeeTrans;
        return of(this.model);
      case WITHDRAWAL_LINE:
        this.model.refType = RefType.WithdrawalLine;
        return of(this.model);
      case WITHDRAWAL_LINE_TERMEXTENSION:
        this.model.refType = RefType.WithdrawalLine;
        return of(this.model);
      default:
        return of(this.model);
    }
  }

  getRelatedInfoTableKey(): string {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    if (this.parentName) {
      this.parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
    } else {
      this.parentId = null;
    }
    return this.parentId;
  }
}
