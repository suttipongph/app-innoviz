import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceTableItemComponent } from './invoice-table-item.component';

describe('InvoiceTableItemViewComponent', () => {
  let component: InvoiceTableItemComponent;
  let fixture: ComponentFixture<InvoiceTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InvoiceTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
