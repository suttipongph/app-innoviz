import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { InvoiceTableItemView, InvoiceTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class InvoiceTableService {
  serviceKey = 'invoiceTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getInvoiceTableToList(search: SearchParameter): Observable<SearchResult<InvoiceTableListView>> {
    const url = `${this.servicePath}/GetInvoiceTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteInvoiceTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteInvoiceTable`;
    return this.dataGateway.delete(url, row);
  }
  getInvoiceTableById(id: string): Observable<InvoiceTableItemView> {
    const url = `${this.servicePath}/GetInvoiceTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createInvoiceTable(vmModel: InvoiceTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateInvoiceTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateInvoiceTable(vmModel: InvoiceTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateInvoiceTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
