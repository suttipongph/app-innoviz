import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { InvoiceLineListView } from 'shared/models/viewModel';
import { InvoiceLineService } from '../invoice-line.service';
import { InvoiceLineListCustomComponent } from './invoice-line-list-custom.component';

@Component({
  selector: 'invoice-line-list',
  templateUrl: './invoice-line-list.component.html',
  styleUrls: ['./invoice-line-list.component.scss']
})
export class InvoiceLineListComponent extends BaseListComponent<InvoiceLineListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  invoiceRevenueTypeOptions: SelectItems[] = [];
  taxTableOptions: SelectItems[] = [];

  constructor(public custom: InvoiceLineListCustomComponent, private service: InvoiceLineService, private uiControllerService: UIControllerService) {
    super();
    this.custom.baseService = this.baseService;
    this.accessRightChildPage = 'InvoiceLineListPage';
    this.parentId = this.uiService.getKey('invoicetable');
  }
  ngOnInit(): void {
    this.accessRightChildPage = 'InvoiceLineListPage';
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getInvoiceRevenueTypeDropDown(this.invoiceRevenueTypeOptions);
    this.baseDropdown.getTaxTableDropDown(this.taxTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'invoiceLineGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.LINE',
        textKey: 'lineNum',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.ASC,
        searchingKey: 'lineNum'
      },
      {
        label: 'LABEL.INVOICE_TEXT',
        textKey: 'invoiceText',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.SERVICE_FEE_TYPE_ID',
        textKey: 'invoiceRevenueType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.invoiceRevenueTypeOptions,
        searchingKey: 'invoiceRevenueTypeGUID',
        sortingKey: 'invoiceRevenueType_RevenueTypeId'
      },
      {
        label: 'LABEL.TOTAL_AMOUNT_BEFORE_TAX',
        textKey: 'totalAmountBeforeTax',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.TAX_AMOUNT',
        textKey: 'taxAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.TOTAL_AMOUNT',
        textKey: 'totalAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.TAX_CODE',
        textKey: 'taxTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.taxTableOptions,
        searchingKey: 'taxTableGUID',
        sortingKey: 'taxTable_taxTabled'
      },
      {
        label: null,
        textKey: 'invoiceTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<InvoiceLineListView>> {
    return this.service.getInvoiceLineToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteInvoiceLine(row));
  }
}
