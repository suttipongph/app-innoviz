import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InvoiceLineListComponent } from './invoice-line-list.component';

describe('InvoiceLineListViewComponent', () => {
  let component: InvoiceLineListComponent;
  let fixture: ComponentFixture<InvoiceLineListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InvoiceLineListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceLineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
