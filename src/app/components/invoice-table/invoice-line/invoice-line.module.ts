import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InvoiceLineService } from './invoice-line.service';
import { InvoiceLineListComponent } from './invoice-line-list/invoice-line-list.component';
import { InvoiceLineItemComponent } from './invoice-line-item/invoice-line-item.component';
import { InvoiceLineItemCustomComponent } from './invoice-line-item/invoice-line-item-custom.component';
import { InvoiceLineListCustomComponent } from './invoice-line-list/invoice-line-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [InvoiceLineListComponent, InvoiceLineItemComponent],
  providers: [InvoiceLineService, InvoiceLineItemCustomComponent, InvoiceLineListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [InvoiceLineListComponent, InvoiceLineItemComponent]
})
export class InvoiceLineComponentModule {}
