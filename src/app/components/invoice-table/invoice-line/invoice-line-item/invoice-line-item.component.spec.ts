import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceLineItemComponent } from './invoice-line-item.component';

describe('InvoiceLineItemViewComponent', () => {
  let component: InvoiceLineItemComponent;
  let fixture: ComponentFixture<InvoiceLineItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InvoiceLineItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceLineItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
