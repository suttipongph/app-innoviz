import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { InvoiceLineItemView } from 'shared/models/viewModel';

const firstGroup = [
  'INVOICE_TABLE_GUID',
  'LINE_NUM',
  'INVOICE_TEXT',
  'INVOICE_REVENUE_TYPE_GUID',
  'QTY',
  'UNIT_PRICE',
  'TOTAL_AMOUNT_BEFORE_TAX',
  'TAX_AMOUNT',
  'WHT_AMOUNT',
  'TOTAL_AMOUNT',
  'WHT_BASE_AMOUNT',
  'PROD_UNIT_GUID',
  'TAX_TABLE_GUID',
  'WITHHOLDING_TAX_TABLE_GUID',
  'DIMENSION1GUID',
  'DIMENSION2GUID',
  'DIMENSION3GUID',
  'DIMENSION4GUID',
  'DIMENSION5GUID',
  'INVOICE_LINE_GUID'
];

const WITHDRAWAL_LINE='withdrawallinewithdrawal-child'

export class InvoiceLineItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: InvoiceLineItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: InvoiceLineItemView): Observable<boolean> {
    const accessright = isNullOrUndefOrEmptyGUID(model.invoiceLineGUID) ? canCreate : canUpdate;
    const accessLogic = false;
    const parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (parentName) {
      case WITHDRAWAL_LINE:
        return of(true);
      default:
        return of(!(accessLogic && accessright));
    } 
  }
  getInitialData(): Observable<InvoiceLineItemView> {
    let model = new InvoiceLineItemView();
    model.invoiceLineGUID = EmptyGuid;
    return of(model);
  }
}
