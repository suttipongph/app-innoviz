import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { InvoiceLineItemView, InvoiceLineListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class InvoiceLineService {
  serviceKey = 'invoiceLineGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getInvoiceLineToList(search: SearchParameter): Observable<SearchResult<InvoiceLineListView>> {
    const url = `${this.servicePath}/GetInvoiceLineList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteInvoiceLine(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteInvoiceLine`;
    return this.dataGateway.delete(url, row);
  }
  getInvoiceLineById(id: string): Observable<InvoiceLineItemView> {
    const url = `${this.servicePath}/GetInvoiceLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createInvoiceLine(vmModel: InvoiceLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateInvoiceLine`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateInvoiceLine(vmModel: InvoiceLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateInvoiceLine`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
