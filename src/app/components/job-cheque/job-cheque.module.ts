import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JobChequeService } from './job-cheque.service';
import { JobChequeListComponent } from './job-cheque-list/job-cheque-list.component';
import { JobChequeItemComponent } from './job-cheque-item/job-cheque-item.component';
import { JobChequeItemCustomComponent } from './job-cheque-item/job-cheque-item-custom.component';
import { JobChequeListCustomComponent } from './job-cheque-list/job-cheque-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [JobChequeListComponent, JobChequeItemComponent],
  providers: [JobChequeService, JobChequeItemCustomComponent, JobChequeListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [JobChequeListComponent, JobChequeItemComponent]
})
export class JobChequeComponentModule {}
