import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { JobChequeListView } from 'shared/models/viewModel';
import { JobChequeService } from '../job-cheque.service';
import { JobChequeListCustomComponent } from './job-cheque-list-custom.component';

@Component({
  selector: 'job-cheque-list',
  templateUrl: './job-cheque-list.component.html',
  styleUrls: ['./job-cheque-list.component.scss']
})
export class JobChequeListComponent extends BaseListComponent<JobChequeListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  bankGroupOptions: SelectItems[] = [];
  chequeSourceOptions: SelectItems[] = [];
  chequeTableOptions: SelectItems[] = [];
  collectionFollowUpOptions: SelectItems[] = [];
  messengerJobTableOptions: SelectItems[] = [];

  constructor(public custom: JobChequeListCustomComponent, private service: JobChequeService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getChequeSourceEnumDropDown(this.chequeSourceOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBankGroupDropDown();
    this.baseDropdown.getChequeTableDropDown();
    this.baseDropdown.getCollectionFollowUpDropDown();
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'jobChequeGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.CHEQUE_SOURCE',
        textKey: 'chequeSource',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.chequeSourceOptions
      },
      {
        label: 'LABEL.COLLECTION_FOLLOW_UP',
        textKey: 'collectionFollowUp_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.collectionFollowUpOptions
      },
      {
        label: 'LABEL.CHEQUE',
        textKey: 'chequeTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.chequeTableOptions
      },
      {
        label: 'LABEL.CHEQUE_FINANCIAL_INSTITUTE_ID',
        textKey: 'bankGroup_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.bankGroupOptions
      },
      {
        label: 'LABEL.CHEQUE_BRANCH',
        textKey: 'chequeBranch',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CHEQUE_DATE',
        textKey: 'chequeDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CHEQUE_NUMBER',
        textKey: 'chequeNo',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.AMOUNT',
        textKey: 'amount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<JobChequeListView>> {
    return this.service.getJobChequeToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteJobCheque(row));
  }
}
