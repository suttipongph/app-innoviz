import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { JobChequeListComponent } from './job-cheque-list.component';

describe('JobChequeListViewComponent', () => {
  let component: JobChequeListComponent;
  let fixture: ComponentFixture<JobChequeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [JobChequeListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobChequeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
