import { CollectionFollowUpItemView } from './../../../shared/models/viewModel/collectionFollowUpItemView';
import { CollateralStatusItemView } from './../../../shared/models/viewModel/collateralStatusItemView';
import { ChequeTableItemView } from './../../../shared/models/viewModel/chequeTableItemView';
import { takeWhile } from 'rxjs/operators';
import { isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { Observable, of } from 'rxjs';
import { ChequeSource, ChequeStatus, CollectionFollowUpResult, EmptyGuid, PaymentType, RefType } from 'shared/constants';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { JobChequeItemView } from 'shared/models/viewModel';
import { BaseDropdownComponent } from 'core/components/base/base.dropdown.component';

const firstGroup = ['MESSENGER_JOB_TABLE_GUID', 'JOB_CHEQUE_GUID'];
const condition1 = ['COLLECTION_FOLLOW_UP_GUID'];
const condition2 = ['CHEQUE_TABLE_GUID'];
export class JobChequeItemCustomComponent {
  baseService: BaseServiceModel<any>;
  isFollowUp = false;
  isCheque = false;

  getFieldAccessing(model: JobChequeItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    fieldAccessing.push({ filedIds: condition1, readonly: true });
    fieldAccessing.push({ filedIds: condition2, readonly: true });
    if (model.chequeSource === ChequeSource.CollectionFollowUp) {
      fieldAccessing.push({ filedIds: condition1, readonly: false });
      this.isFollowUp = true;
      this.isCheque = false;
    }
    if (model.chequeSource === ChequeSource.Cheque) {
      fieldAccessing.push({ filedIds: condition2, readonly: false });
      this.isCheque = true;
      this.isFollowUp = false;
    }

    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: JobChequeItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(id: string): Observable<JobChequeItemView> {
    return this.baseService.service.initialData(id);
  }
  getChequeTableDropDown(model: JobChequeItemView, baseDropdown: BaseDropdownComponent): Observable<SelectItems[]> {
    if (model.messengerJobTable_CustomerTableGUID !== null && model.messengerJobTable_BuyerTableGUID !== null) {
      return baseDropdown.getChequeTableByCustomerAndBuyerDropDown([
        model.messengerJobTable_CustomerTableGUID,
        model.messengerJobTable_BuyerTableGUID,
        ChequeStatus.Created
      ]);
    } else if (model.messengerJobTable_CustomerTableGUID !== null) {
      return baseDropdown.getChequeTableByCustomerDropDown([model.messengerJobTable_CustomerTableGUID, ChequeStatus.Created]);
    } else {
      return baseDropdown.getChequeTableByStatusDropDown([ChequeStatus.Created]);
    }
  }
  getCollectionFollowUpDropDown(model: JobChequeItemView, baseDropdown: BaseDropdownComponent): Observable<SelectItems[]> {
    return baseDropdown.getCollectionFollowUpByRefDropDown([
      model.messengerJobTable_RefGUID,
      PaymentType.Cheque.toString(),
      CollectionFollowUpResult.Confirmed.toString()
    ]);
  }
  chequeSourceChange(e: any, model: JobChequeItemView): any {
    model.collectionFollowUpGUID = null;
    model.chequeTableGUID = null;
    model.chequeBankGroupGUID = null;
    model.chequeBranch = null;
    model.chequeDate = null;
    model.chequeNo = null;
    model.chequeBankAccNo = null;
    model.recipientName = null;
    model.amount = null;
  }
  chequeChange(e: any, model: JobChequeItemView, chequeTableOptions: SelectItems[]): any {
    const row = chequeTableOptions.find((o) => o.value === e).rowData as ChequeTableItemView;
    model.chequeBankGroupGUID = row.chequeBankGroupGUID;
    model.chequeBranch = row.chequeBranch;
    model.chequeDate = row.chequeDate;
    model.chequeNo = row.chequeNo;
    model.chequeBankAccNo = row.chequeBankAccNo;
    model.recipientName = row.recipientName;
    model.amount = row.amount;
  }
  collectionFollowup(e: any, model: JobChequeItemView, collectionFollowUpOptions: SelectItems[]): any {
    const row = collectionFollowUpOptions.find((o) => o.value === e).rowData as CollectionFollowUpItemView;
    model.chequeBankGroupGUID = row.chequeBankGroupGUID;
    model.chequeBranch = row.chequeBranch;
    model.chequeDate = row.chequeDate;
    model.chequeNo = row.chequeNo;
    model.chequeBankAccNo = row.chequeBankAccNo;
    model.recipientName = row.recipientName;
    model.amount = row.chequeAmount;
  }
}
