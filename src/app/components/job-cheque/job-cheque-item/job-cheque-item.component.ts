import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { JobChequeItemView } from 'shared/models/viewModel';
import { JobChequeService } from '../job-cheque.service';
import { JobChequeItemCustomComponent } from './job-cheque-item-custom.component';
@Component({
  selector: 'job-cheque-item',
  templateUrl: './job-cheque-item.component.html',
  styleUrls: ['./job-cheque-item.component.scss']
})
export class JobChequeItemComponent extends BaseItemComponent<JobChequeItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  bankGroupOptions: SelectItems[] = [];
  chequeSourceOptions: SelectItems[] = [];
  chequeTableOptions: SelectItems[] = [];
  collectionFollowUpOptions: SelectItems[] = [];
  messengerJobTableOptions: SelectItems[] = [];

  constructor(private service: JobChequeService, private currentActivatedRoute: ActivatedRoute, public custom: JobChequeItemCustomComponent) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getChequeSourceEnumDropDown(this.chequeSourceOptions);
  }
  getById(): Observable<JobChequeItemView> {
    return this.service.getJobChequeById(this.id);
  }
  getInitialData(): Observable<JobChequeItemView> {
    return this.custom.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner(result);
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(
      this.baseDropdown.getBankGroupDropDown(),
      this.custom.getChequeTableDropDown(model, this.baseDropdown),
      this.custom.getCollectionFollowUpDropDown(model, this.baseDropdown),
      this.baseDropdown.getMessengerJobTableDropDown()
    ).subscribe(
      ([bankGroup, chequeTable, collectionFollowUp, messengerJobTable]) => {
        this.bankGroupOptions = bankGroup;
        this.chequeTableOptions = chequeTable;
        this.collectionFollowUpOptions = collectionFollowUp;
        this.messengerJobTableOptions = messengerJobTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateJobCheque(this.model), isColsing);
    } else {
      super.onCreate(this.service.createJobCheque(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  chequeSourceChange(e: any): any {
    this.custom.chequeSourceChange(e, this.model);
    this.setFieldAccessing();
  }
  chequeChange(e: any): any {
    this.custom.chequeChange(e, this.model, this.chequeTableOptions);
  }
  collectionFollowup(e: any): any {
    this.custom.collectionFollowup(e, this.model, this.collectionFollowUpOptions);
  }
}
