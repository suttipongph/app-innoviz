import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobChequeItemComponent } from './job-cheque-item.component';

describe('JobChequeItemViewComponent', () => {
  let component: JobChequeItemComponent;
  let fixture: ComponentFixture<JobChequeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [JobChequeItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobChequeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
