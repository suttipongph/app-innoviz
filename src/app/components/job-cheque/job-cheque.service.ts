import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { JobChequeItemView, JobChequeListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class JobChequeService {
  serviceKey = 'jobChequeGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getJobChequeToList(search: SearchParameter): Observable<SearchResult<JobChequeListView>> {
    const url = `${this.servicePath}/GetJobChequeList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteJobCheque(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteJobCheque`;
    return this.dataGateway.delete(url, row);
  }
  getJobChequeById(id: string): Observable<JobChequeItemView> {
    const url = `${this.servicePath}/GetJobChequeById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createJobCheque(vmModel: JobChequeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateJobCheque`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateJobCheque(vmModel: JobChequeItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateJobCheque`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  initialData(id: string): Observable<JobChequeItemView> {
    const url = `${this.servicePath}/GetJobChequeInitialdata/id=${id}`;
    return this.dataGateway.get(url);
  }
}
