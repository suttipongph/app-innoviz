import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CopyVerificationView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CopyVerificationService {
  serviceKey = 'copyVerificationGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  deleteCopyVerification(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCopyVerification`;
    return this.dataGateway.delete(url, row);
  }
  getCopyVerificationById(id: string): Observable<CopyVerificationView> {
    const url = `${this.servicePath}/GetCopyVerificationById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCopyVerification(vmModel: CopyVerificationView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCopyVerification`;

    return this.dataGateway.post(url, vmModel);
  }
  updateCopyVerification(vmModel: CopyVerificationView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCopyVerification`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  checkVerificationHesLineById(id: string): Observable<CopyVerificationView> {
    const url = `${this.servicePath}/CheckVerificationHesLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
  checkIsHasLineById(id: string): Observable<CopyVerificationView> {
    const url = `${this.servicePath}/CheckIsHassLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
}
