import { CopyVerificationComponent } from './copy-verification/copy-verification.component';
import { CopyVerificationCustomComponent } from './copy-verification/copy-verification-custom.component';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CopyVerificationService } from './copy-verification.service';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CopyVerificationComponent],
  providers: [CopyVerificationService, CopyVerificationCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CopyVerificationComponent]
})
export class CopyVerificationComponentModule {}
