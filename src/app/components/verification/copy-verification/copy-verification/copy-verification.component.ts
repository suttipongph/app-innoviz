import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { Confirmation } from 'shared/models/primeModel';
import { FormValidationModel, PageInformationModel, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { CopyVerificationView } from 'shared/models/viewModel';
import { CopyVerificationService } from '../copy-verification.service';
import { CopyVerificationCustomComponent } from './copy-verification-custom.component';
@Component({
  selector: 'copy-verification',
  templateUrl: './copy-verification.component.html',
  styleUrls: ['./copy-verification.component.scss']
})
export class CopyVerificationComponent extends BaseItemComponent<CopyVerificationView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  verificationTableOptions: SelectItems[] = [];

  constructor(
    private service: CopyVerificationService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CopyVerificationCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.custom.id = this.id;
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<CopyVerificationView> {
    return this.service.getCopyVerificationById(this.id);
  }
  getInitialData(): Observable<CopyVerificationView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;

      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getVerificationTableDropDown([model.buyerTableGUID, model.creditAppTableGUID])).subscribe(
      ([verificationTable]) => {
        this.verificationTableOptions = verificationTable;
        this.custom.verificationTableOptions = this.verificationTableOptions;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    if (validation.isValid) {
      this.getDataValidation().subscribe((res) => {
        if (!res) {
          this.onSubmit(true);
        } else {
          this.getDataHasLine().subscribe((res1) => {
            if (res1) {
              const confirmation: Confirmation = {
                header: this.baseService.translate.instant('CONFIRM.CONFIRM'),
                message: this.baseService.translate.instant('CONFIRM.90002', [this.baseService.translate.instant('LABEL.VERIFICATION_LINE')])
              };
              this.baseService.notificationService.showConfirmDialog(confirmation);
              this.baseService.notificationService.isAccept.subscribe((isConfirm) => {
                if (isConfirm) {
                  this.onSubmit(true);
                }
              });
            } else {
              this.onSubmit(true);
            }
          });
        }
      });
    }
  }
  onSubmit(isColsing: boolean): void {
    super.onExecuteFunction(this.service.createCopyVerification(this.model));
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation(this.model);
  }
  getDataHasLine(): Observable<boolean> {
    return this.custom.getDataHasLine(this.model);
  }
  onReferenceVerificationIdChange(e: any): any {
    this.custom.onReferenceVerificationIdChange(e, this.model, this.verificationTableOptions, this.id);
  }
  onReferenceVerificationChangeValidate(): any {
    return this.custom.onReferenceVerificationChangeValidate(this.model);
  }
  onClose(): void {
    super.onBack();
  }
}
