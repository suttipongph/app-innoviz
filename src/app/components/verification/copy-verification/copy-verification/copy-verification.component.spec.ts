import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopyVerificationComponent } from './copy-verification.component';

describe('CopyVerificationViewComponent', () => {
  let component: CopyVerificationComponent;
  let fixture: ComponentFixture<CopyVerificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CopyVerificationComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyVerificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
