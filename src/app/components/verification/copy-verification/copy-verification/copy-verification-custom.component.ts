import { TranslateService } from '@ngx-translate/core';
import { AppInjector } from 'app-injector';
import { NotificationService } from 'core/services/notification.service';
import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { CopyVerificationView, VerificationTableItemView } from 'shared/models/viewModel';

const firstGroup = ['VERIFICATION_DATE', 'CREDIT_APP_TABLE_GUID', 'CUSTOMER_TABLE_GUID', 'BUYER_TABLE_GUID', 'DOCUMENT_STATUS_GUID'];

export class CopyVerificationCustomComponent {
  baseService: BaseServiceModel<any>;
  translateService = AppInjector.get(TranslateService);
  verificationTableOptions: SelectItems[];
  id: string;

  getFieldAccessing(model: CopyVerificationView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(model: CopyVerificationView): Observable<boolean> {
    return this.baseService.service.checkVerificationHesLineById(model.copyVerificationGUID);
  }
  getDataHasLine(model: CopyVerificationView): Observable<boolean> {
    return this.baseService.service.checkIsHasLineById(model.verificationTableGUID);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CopyVerificationView): Observable<boolean> {
    return of(canCreate);
  }
  getInitialData(): Observable<CopyVerificationView> {
    let model = new CopyVerificationView();
    model.copyVerificationGUID = EmptyGuid;
    return of(model);
  }
  onReferenceVerificationIdChange(e: any, model: CopyVerificationView, verificationTableOptions: SelectItems[], id: string): any {
    const rowData = isNullOrUndefOrEmptyGUID(model.verificationTableGUID)
      ? new CopyVerificationView()
      : (verificationTableOptions.find((o) => o.value === model.copyVerificationGUID).rowData as VerificationTableItemView);
    model.copyVerificationGUID = rowData.verificationTableGUID;
    model.verificationDate = rowData.verificationDate;
    model.creditAppTable_Values = rowData.creditAppTable_Values;
    model.customerTable_Values = rowData.customerTable_Values;
    model.buyerTable_Values = rowData.buyerTable_Values;
    model.documentStatus_Values = rowData.documentStatus_Values;
  }
  onReferenceVerificationChangeValidate(model: CopyVerificationView): any {
    return this.referenceVerificationIsMatch(model.copyVerificationGUID, this.id);
  }
  referenceVerificationIsMatch(e: any, id: any): any {
    if (e === id) {
      const changedDes = this.verificationTableOptions.find((o) => o.value === e).rowData as VerificationTableItemView;
      const parentDes = this.verificationTableOptions.find((o) => o.value === id).rowData as VerificationTableItemView;
      return {
        code: 'ERROR.90027',
        parameters: ['[' + changedDes.verificationId + ']' + changedDes.description, '[' + parentDes.verificationId + ']' + parentDes.description]
      };
    } else {
      return null;
    }
  }
}
