import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { UpdateVerificationStatusParamView, UpdateVerificationStatusView } from 'shared/models/viewModel/updateVerificationStatusView';

const firstGroup = ['VERIFICATION_TABLE_GUID', 'ORIGINAL_DOCUMENT_STATUS'];

export class UpdateVerificationStatusCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getModelParam(model: UpdateVerificationStatusView): Observable<UpdateVerificationStatusParamView> {
    const param: UpdateVerificationStatusParamView = new UpdateVerificationStatusParamView();
    param.updateVerificationStatusGUID = model.verificationTableGUID;
    param.documentReasonGUID = model.documentReasonGUID;
    param.documentStatusGUID = model.documentStatusGUID;
    param.originalDocumentStatusGUID = model.originalDocumentStatusGUID;

    return of(param);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<UpdateVerificationStatusView> {
    let model = new UpdateVerificationStatusView();
    model.updateVerificationStatusGUID = EmptyGuid;
    return of(model);
  }
  setFieldAccessingByIdentificationType(e: any): FieldAccessing[] {
    throw new Error('Method not implemented.');
  }
  setValueByIdentificationType(model: UpdateVerificationStatusView) {}
  getValidateStatus(id: string, oriId: string): TranslateModel {
    const error: TranslateModel = { code: 'ERROR.90021', parameters: [] };
    return id === oriId ? error : null;
  }
  getRemakReason(id: string, options: SelectItems[]): Observable<SelectItems> {
    const item = options.find((f) => f.value === id);
    return of(item);
  }
}
