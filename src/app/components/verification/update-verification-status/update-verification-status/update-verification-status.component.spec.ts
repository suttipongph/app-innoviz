import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateVerificationStatusComponent } from './update-verification-status.component';

describe('UpdateVerificationStatusViewComponent', () => {
  let component: UpdateVerificationStatusComponent;
  let fixture: ComponentFixture<UpdateVerificationStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UpdateVerificationStatusComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateVerificationStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
