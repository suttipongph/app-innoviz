import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Confirmation } from 'primeng/api';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, RefType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { UpdateVerificationStatusParamView, UpdateVerificationStatusView } from 'shared/models/viewModel/updateVerificationStatusView';

import { UpdateVerificationStatusService } from '../update-verification-status.service';
import { UpdateVerificationStatusCustomComponent } from './update-verification-status-custom.component';
@Component({
  selector: 'update-verification-status',
  templateUrl: './update-verification-status.component.html',
  styleUrls: ['./update-verification-status.component.scss']
})
export class UpdateVerificationStatusComponent extends BaseItemComponent<UpdateVerificationStatusView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  documentReasonOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];

  constructor(
    private service: UpdateVerificationStatusService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: UpdateVerificationStatusCustomComponent,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiControllerService.getRelatedInfoActiveTableKey();
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<UpdateVerificationStatusView> {
    return this.service.getUpdateVerificationStatusById(this.parentId);
  }
  getInitialData(): Observable<UpdateVerificationStatusView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {}

  onAsyncRunner(model?: any): void {
    forkJoin(
      this.baseDropdown.getDocumentReasonDropDown(RefType.Verification.toString()),
      this.baseDropdown.getDocumentStatusByVerificationTableDropDown()
    ).subscribe(
      ([documentReason, documentStatus]) => {
        this.documentReasonOptions = documentReason;
        this.documentStatusOptions = documentStatus;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing().subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  onSave(validation: FormValidationModel): void {}
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.custom.getModelParam(this.model).subscribe((model) => {
          this.onSubmit(true, model);
        });
      }
    });
  }

  onSubmit(isColsing: boolean, model: UpdateVerificationStatusParamView): void {
    super.onExecuteFunction(this.service.updateUpdateVerificationStatus(model));
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  OnIdentificationTypeChange(e: any): void {
    super.setBaseFieldAccessing(this.custom.setFieldAccessingByIdentificationType(e));
    this.custom.setValueByIdentificationType(this.model);
  }
  getDocumentStatusValidation(): TranslateModel {
    return this.custom.getValidateStatus(this.model.documentStatusGUID, this.model.originalDocumentStatusGUID);
  }
}
