import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { UpdateVerificationStatusParamView, UpdateVerificationStatusView } from 'shared/models/viewModel/updateVerificationStatusView';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class UpdateVerificationStatusService {
  serviceKey = 'updateVerificationStatusGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getUpdateVerificationStatusToList(search: SearchParameter): Observable<SearchResult<UpdateVerificationStatusView>> {
    const url = `${this.servicePath}/GetUpdateVerificationStatusList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteUpdateVerificationStatus(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteUpdateVerificationStatus`;
    return this.dataGateway.delete(url, row);
  }
  getUpdateVerificationStatusById(id: string): Observable<UpdateVerificationStatusView> {
    const url = `${this.servicePath}/GetUpdateVerificationStatusById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createUpdateVerificationStatus(vmModel: UpdateVerificationStatusView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateUpdateVerificationStatus`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateUpdateVerificationStatus(vmModel: UpdateVerificationStatusParamView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateUpdateVerificationStatus`;
    return this.dataGateway.post(url, vmModel);
  }
  getUpdateverificationstatus(id: string): Observable<UpdateVerificationStatusView> {
    const url = `${this.servicePath}/getUpdateverificationstatus/id=${id}`;
    return this.dataGateway.get(url);
  }
}
