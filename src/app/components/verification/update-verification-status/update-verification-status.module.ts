import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateVerificationStatusService } from './update-verification-status.service';
import { UpdateVerificationStatusComponent } from './update-verification-status/update-verification-status.component';
import { UpdateVerificationStatusCustomComponent } from './update-verification-status/update-verification-status-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [UpdateVerificationStatusComponent],
  providers: [UpdateVerificationStatusService, UpdateVerificationStatusCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [UpdateVerificationStatusComponent]
})
export class UpdateVerificationStatusComponentModule {}
