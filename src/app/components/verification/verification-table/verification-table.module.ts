import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VerificationTableService } from './verification-table.service';
import { VerificationTableListComponent } from './verification-table-list/verification-table-list.component';
import { VerificationTableItemComponent } from './verification-table-item/verification-table-item.component';
import { VerificationTableItemCustomComponent } from './verification-table-item/verification-table-item-custom.component';
import { VerificationTableListCustomComponent } from './verification-table-list/verification-table-list-custom.component';
import { VerificationLineComponentModule } from '../verification-line/verification-line.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, VerificationLineComponentModule],
  declarations: [VerificationTableListComponent, VerificationTableItemComponent],
  providers: [VerificationTableService, VerificationTableItemCustomComponent, VerificationTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [VerificationTableListComponent, VerificationTableItemComponent]
})
export class VerificationTableComponentModule {}
