import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { VerificationTableItemView, VerificationTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class VerificationTableService {
  serviceKey = 'verificationTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getVerificationTableToList(search: SearchParameter): Observable<SearchResult<VerificationTableListView>> {
    const url = `${this.servicePath}/GetVerificationTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteVerificationTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteVerificationTable`;
    return this.dataGateway.delete(url, row);
  }
  getVerificationTableById(id: string): Observable<VerificationTableItemView> {
    const url = `${this.servicePath}/GetVerificationTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createVerificationTable(vmModel: VerificationTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateVerificationTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateVerificationTable(vmModel: VerificationTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateVerificationTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  validateIsManualNumberSeq(companyId: string): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeq/companyId=${companyId}`;
    return this.dataGateway.get(url);
  }
  getInitialData(id: string): Observable<any> {
    const url = `${this.servicePath}/GetVerificationTableInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
}
