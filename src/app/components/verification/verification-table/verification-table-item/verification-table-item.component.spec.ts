import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerificationTableItemComponent } from './verification-table-item.component';

describe('VerificationTableItemViewComponent', () => {
  let component: VerificationTableItemComponent;
  let fixture: ComponentFixture<VerificationTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VerificationTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
