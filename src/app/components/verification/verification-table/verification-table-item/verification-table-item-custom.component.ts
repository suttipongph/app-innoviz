import { VerificationLineService } from 'components/verification/verification-line/verification-line.service';
import { Observable, of } from 'rxjs';
import { VerificationStatus } from 'shared/constants';
import { toMapModel } from 'shared/functions/model.function';
import { isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { VerificationTableItemView } from 'shared/models/viewModel';
import { RptPrintVerificationReportView } from 'shared/models/viewModel/rptPrintVerificationReportView';
import { VerificationTableService } from '../verification-table.service';

const firstGroup = ['CREDIT_APP_TABLE_GUID', 'CUSTOMER_TABLE_GUID', 'DOCUMENT_STATUS_GUID'];
const Condition2 = ['ALL_ID'];
const verification = ['VERIFICATION_ID'];
const buyerGroup = ['BUYER_TABLE_GUID'];
let accessright = true;
let accesslogic = false;

export class VerificationTableItemCustomComponent {
  baseService: BaseServiceModel<VerificationTableService>;
  isManual: boolean = false;
  canclose = false;
  parentName = null;

  getFieldAccessing(model: VerificationTableItemView, verificationTableService: VerificationTableService): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];

    return new Observable((observer) => {
      this.validateIsManualNumberSeq(verificationTableService, model.companyGUID).subscribe(
        (result) => {
          fieldAccessing.push({ filedIds: firstGroup, readonly: true });

          if (isNullOrUndefOrEmptyGUID(model.verificationTableGUID)) {
            this.isManual = result;
            fieldAccessing.push({ filedIds: verification, readonly: !result });
            observer.next(fieldAccessing);
          } else {
            this.isManual = false;
            fieldAccessing.push({ filedIds: verification, readonly: true });
            if (model.documentStatus_StatusId >= VerificationStatus.Approved) {
              fieldAccessing.push({ filedIds: Condition2, readonly: true });
              observer.next(fieldAccessing);
            } else {
              if (model.totalVericationLine === 0) {
                fieldAccessing.push({ filedIds: buyerGroup, readonly: false });
                observer.next(fieldAccessing);
              } else {
                fieldAccessing.push({ filedIds: buyerGroup, readonly: true });
                observer.next(fieldAccessing);
              }
            }
            observer.next(fieldAccessing);
          }
        },
        (error) => {
          observer.next(fieldAccessing);
        }
      );
    });
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  canActiveByStatus(status: string): boolean {
    return status < VerificationStatus.Draft;
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: VerificationTableItemView): Observable<boolean> {
    if (!isNullOrUndefOrEmptyGUID(model.verificationTableGUID)) {
      if (model.documentStatus_StatusId === VerificationStatus.Draft) {
        accessright = true;
        accesslogic = true;
      } else if (model.documentStatus_StatusId !== VerificationStatus.Draft) {
        accessright = false;
        accesslogic = false;
      }
    } else {
      accessright = true;
      accesslogic = true;
    }
    return of(!(accessright && accesslogic));
  }
  getInitialData(verificationTableService: VerificationTableService, Id: string): Observable<VerificationTableItemView> {
    return verificationTableService.getInitialData(Id);
  }
  validateIsManualNumberSeq(service: VerificationTableService, companyId: string): Observable<boolean> {
    return service.validateIsManualNumberSeq(companyId);
  }
  isFactoring(model: VerificationTableItemView): boolean {
    return !(model.documentStatus_StatusId === VerificationStatus.Draft);
  }
  getPrintSetBookDocTransParamModel(model: VerificationTableItemView): RptPrintVerificationReportView {
    const paramModel = new RptPrintVerificationReportView();
    toMapModel(model, paramModel);
    paramModel.buyerId = model.buyerTable_Values;
    paramModel.creditAppId = model.creditAppTable_Values;
    paramModel.customerId = model.customerTable_Values;
    paramModel.documentStatus = model.documentStatus_Values;
    return paramModel;
  }
}
