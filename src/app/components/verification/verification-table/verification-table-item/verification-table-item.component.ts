import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, VerificationStatus, RefType, ROUTE_REPORT_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { RefIdParm, VerificationTableItemView } from 'shared/models/viewModel';
import { VerificationTableService } from '../verification-table.service';
import { VerificationTableItemCustomComponent } from './verification-table-item-custom.component';
@Component({
  selector: 'verification-table-item',
  templateUrl: './verification-table-item.component.html',
  styleUrls: ['./verification-table-item.component.scss']
})
export class VerificationTableItemComponent extends BaseItemComponent<VerificationTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  buyerTableOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  totalVericationLine = 0;
  disAble: boolean = true;

  constructor(
    private service: VerificationTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: VerificationTableItemCustomComponent,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {}
  getById(): Observable<VerificationTableItemView> {
    return this.service.getVerificationTableById(this.id);
  }
  getInitialData(): Observable<VerificationTableItemView> {
    return this.custom.getInitialData(this.service, this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin([this.baseDropdown.getBuyerTableByCreditAppTableDropDown(this.parentId), this.baseDropdown.getCustomerTableByDropDown()]).subscribe(
      ([buyerTable, customerTable]) => {
        this.buyerTableOptions = buyerTable;
        this.customerTableOptions = customerTable;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model, this.service).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  setRelatedInfoOptions(): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        command: () => {
          const refIdParm: RefIdParm = { refType: RefType.Verification, refGUID: this.model.verificationTableGUID };
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: refIdParm
          });
        }
      },
      {
        label: 'LABEL.MEMO',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.MEMO,
            parameters: { refGUID: this.model.verificationTableGUID, refType: RefType.Verification }
          })
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(item: VerificationTableItemView): void {
    const isFactoring = this.custom.isFactoring(item);
    this.functionItems = [
      {
        label: 'LABEL.UPDATE_VERIFICATION_STATUS',
        command: () => this.toFunction({ path: ROUTE_RELATED_GEN.UPDATE_VERIFICATION_STATUS }),
        disabled: isFactoring
      },
      {
        label: 'LABEL.COPY_VERIFICATION',
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.VERIFICATION_TABLE_COPY_VERIFICATION }),
        disabled: isFactoring
      },
      {
        label: 'LABEL.PRINT',
        command: () =>
          this.toReport({
            path: ROUTE_REPORT_GEN.PRINT_VERIFICATION,
            parameters: {
              model: this.custom.getPrintSetBookDocTransParamModel(this.model)
            }
          })
      }
    ];
    super.setFunctionOptionsMapping();
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateVerificationTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createVerificationTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
