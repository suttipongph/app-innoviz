import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { VerificationTableListComponent } from './verification-table-list.component';

describe('VerificationTableListViewComponent', () => {
  let component: VerificationTableListComponent;
  let fixture: ComponentFixture<VerificationTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VerificationTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
