import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { VerificationTableListView } from 'shared/models/viewModel';
import { VerificationTableService } from '../verification-table.service';
import { VerificationTableListCustomComponent } from './verification-table-list-custom.component';

@Component({
  selector: 'verification-table-list',
  templateUrl: './verification-table-list.component.html',
  styleUrls: ['./verification-table-list.component.scss']
})
export class VerificationTableListComponent extends BaseListComponent<VerificationTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  verificationTableOption: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  constructor(
    public custom: VerificationTableListCustomComponent,
    public service: VerificationTableService,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.setAccessModeByParentStatus(this.parentId).subscribe((result) => {
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
    this.baseDropdown.GetBuyerTableByCreditAppTableDropDown(this.buyerTableOptions);
    this.baseDropdown.getCustomerTableByDropDown(this.customerTableOptions);
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
    this.baseDropdown.getCustomerTableByDropDown(this.customerTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canDelete = false;
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.key = 'verificationTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.VERIFICATION_ID',
        textKey: 'verificationId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.VERIFICATION_DATE',
        textKey: 'verificationDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        sortingKey: 'customerTableGUID',
        searchingKey: 'customerTableGUID'
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerTableOptions,
        sortingKey: 'BuyerTableGUID',
        searchingKey: 'BuyerTableGUID'
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentStatusOptions,
        sortingKey: 'documentStatus_StatusId',
        searchingKey: 'documentStatusGUID'
      },
      {
        label: null,
        textKey: 'creditAppTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<VerificationTableListView>> {
    return this.service.getVerificationTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteVerificationTable(row));
  }
}
