import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintVerificationReportComponent } from './print-verification-report.component';

describe('PrintVerificationReportViewComponent', () => {
  let component: PrintVerificationReportComponent;
  let fixture: ComponentFixture<PrintVerificationReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrintVerificationReportComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintVerificationReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
