import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { toMapModel } from 'shared/functions/model.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { RptPrintVerificationReportParm } from 'shared/models/viewModel/rptPrintVerificationReportParm';
import { RptPrintVerificationReportView } from 'shared/models/viewModel/rptPrintVerificationReportView';

const firstGroup = ['VERIFICATION_ID', 'VERIFICATION_DATE', 'DOCUMENT_STATUS', 'CREDIT_APP_ID', 'CUSTOMER_ID', 'BUYER_ID'];

export class PrintVerificationReportCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: RptPrintVerificationReportView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getPrintVerification(model: RptPrintVerificationReportView, passingModel: any): Observable<RptPrintVerificationReportView> {
    model = new RptPrintVerificationReportView();
    toMapModel(passingModel, model);
    return of(model);
  }
  getModelParm(model: RptPrintVerificationReportView): Observable<RptPrintVerificationReportParm> {
    var parm = new RptPrintVerificationReportParm();
    parm.verificationTableGUID = model.verificationTableGUID;
    return of(parm);
  }
}
