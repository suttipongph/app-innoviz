import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseReportComponent } from 'core/components/base-report/base-report.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { forkJoin, Observable, of } from 'rxjs';
import { REPORT_NAME } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel } from 'shared/models/systemModel';
import { RptPrintVerificationReportParm } from 'shared/models/viewModel/rptPrintVerificationReportParm';
import { RptPrintVerificationReportView } from 'shared/models/viewModel/rptPrintVerificationReportView';
import { PrintVerificationService } from '../print-verification.service';
import { PrintVerificationReportCustomComponent } from './print-verification-report-custom.component';

@Component({
  selector: 'print-verification-report',
  templateUrl: './print-verification-report.component.html',
  styleUrls: ['./print-verification-report.component.scss']
})
export class PrintVerificationReportComponent extends BaseReportComponent<RptPrintVerificationReportView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  constructor(
    private service: PrintVerificationService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: PrintVerificationReportCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.reportName = REPORT_NAME.D102_VERIFICATION;
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.passingModel = passingObj['model'];
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialUpdatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<RptPrintVerificationReportView> {
    return this.custom.getPrintVerification(this.model, this.passingModel);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {}

  onAsyncRunner(model?: RptPrintVerificationReportView): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  onSave(validation: FormValidationModel): void {}
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.custom.getModelParm(this.model).subscribe((parm) => {
          this.onSubmit(true, parm);
        });
      }
    });
  }
  onSubmit(isColsing: boolean, model: RptPrintVerificationReportParm): void {
    super.submitReportParameters(model);
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
