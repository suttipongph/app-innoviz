import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrintVerificationService } from './print-verification.service';
import { PrintVerificationReportCustomComponent } from './print-verification/print-verification-report-custom.component';
import { PrintVerificationReportComponent } from './print-verification/print-verification-report.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PrintVerificationReportComponent],
  providers: [PrintVerificationService, PrintVerificationReportCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PrintVerificationReportComponent]
})
export class PrintVerificationComponentModule {}
