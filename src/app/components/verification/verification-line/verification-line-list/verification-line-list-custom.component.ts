import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AccessMode, VerificationStatus } from 'shared/constants';
import { isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView, VerificationLineItemView, VerificationLineListView, VerificationTableItemView } from 'shared/models/viewModel';
import { VerificationLineService } from '../verification-line.service';
const VERIFICATION_TABLE = 'verificationtable';
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const CUSTOMER_TABLE = 'customertable';
const CREDIT_APP_TABLE = 'creditapptable';
export class VerificationLineListCustomComponent {
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  baseService: BaseServiceModel<VerificationLineService>;
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: VerificationTableItemView): Observable<any> {
    const accessRight = isNullOrUndefOrEmptyGUID(model.verificationTableGUID) ? canCreate : canUpdate;
    const accesslogic = this.canActiveByStatus(model.documentStatus_StatusId);
    return of(!(accessRight && accesslogic));
  }
  canActiveByStatus(status: string): boolean {
    return status === VerificationStatus.Draft;
  }
  setAccessModeByParentStatus(parentId: string): Observable<AccessModeView> {
    switch (this.parentName) {
      case CUSTOMER_TABLE:
        this.accessModeView.canCreate = true;
        this.accessModeView.canDelete = false;
        this.accessModeView.canView = true;
        return of(this.accessModeView);
      case VERIFICATION_TABLE:
        this.accessModeView.canCreate = true;
        this.accessModeView.canDelete = false;
        this.accessModeView.canView = true;
        return of(this.accessModeView);
      default:
        return of(this.accessModeView);
    }
  }
  getAccessModeByParent(parentId: string): Observable<any> {
    return this.baseService.service.getAccessModeByParent(parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
      })
    );
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
  getRowAuthorize(row: VerificationLineListView[], canCreate: boolean): any {
    row.forEach((item) => {
      const accessMode: AccessMode = canCreate ? AccessMode.full : AccessMode.viewer;
      item.rowAuthorize = accessMode;
    });
  }
}
