import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, SortType, VerificationStatus } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { VerificationLineListView, VerificationTableItemView } from 'shared/models/viewModel';
import { VerificationLineService } from '../verification-line.service';
import { VerificationLineListCustomComponent } from './verification-line-list-custom.component';

@Component({
  selector: 'verification-line-list',
  templateUrl: './verification-line-list.component.html',
  styleUrls: ['./verification-line-list.component.scss']
})
export class VerificationLineListComponent extends BaseListComponent<VerificationLineListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  vendorTableOptions: SelectItems[] = [];
  verificationTypeOptions: SelectItems[] = [];
  defualtBitOption: SelectItems[] = [];

  constructor(
    public custom: VerificationLineListCustomComponent,
    public uiControllerService: UIControllerService,
    private service: VerificationLineService
  ) {
    super();
    this.baseService.service = this.service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiControllerService.getKey('verificationtable');
  }
  ngOnInit(): void {
    this.accessRightChildPage = 'VerificationLineListPage';
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.custom.getAccessModeByParent(this.parentId).subscribe((result) => {
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getDefaultBitDropDown(this.defualtBitOption);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getVendorTableDropDown(this.vendorTableOptions);
    this.baseDropdown.getVerificationTypeDropDown(this.verificationTypeOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'verificationLineGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'verificationTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      },
      {
        label: 'LABEL.VERIFICATION_TYPE_ID',
        textKey: 'verificationType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.ASC,
        sortingKey: 'verificationType_Values',
        masterList: this.verificationTypeOptions
      },
      {
        label: null,
        textKey: 'verdortable',
        type: ColumnType.MASTER,
        visibility: false,
        sorting: SortType.NONE,
        sortingKey: 'verdortable',

        masterList: this.vendorTableOptions
      },
      {
        label: 'LABEL.PASS',
        textKey: 'pass',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defualtBitOption
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<VerificationLineListView>> {
    return this.service.getVerificationLineToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteVerificationLine(row));
  }
  getRowAuthorize(row: VerificationLineListView[]): void {
    this.custom.getRowAuthorize(row, this.custom.getCanCreateByParent(this.getCanCreate()));
    row.forEach((item) => {
      item.rowAuthorize = this.getSingleRowAuthorize(item.rowAuthorize, item);
    });
  }
}
