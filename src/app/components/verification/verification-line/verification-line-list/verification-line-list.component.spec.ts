import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { VerificationLineListComponent } from './verification-line-list.component';

describe('VerificationLineListViewComponent', () => {
  let component: VerificationLineListComponent;
  let fixture: ComponentFixture<VerificationLineListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VerificationLineListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationLineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
