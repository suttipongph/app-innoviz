import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VerificationLineService } from './verification-line.service';
import { VerificationLineListComponent } from './verification-line-list/verification-line-list.component';
import { VerificationLineItemComponent } from './verification-line-item/verification-line-item.component';
import { VerificationLineItemCustomComponent } from './verification-line-item/verification-line-item-custom.component';
import { VerificationLineListCustomComponent } from './verification-line-list/verification-line-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [VerificationLineListComponent, VerificationLineItemComponent],
  providers: [VerificationLineService, VerificationLineItemCustomComponent, VerificationLineListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [VerificationLineListComponent, VerificationLineItemComponent]
})
export class VerificationLineComponentModule {}
