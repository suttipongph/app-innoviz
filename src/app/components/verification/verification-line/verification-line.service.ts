import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { VerificationLineItemView, VerificationLineListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class VerificationLineService {
  serviceKey = 'verificationLineGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getVerificationLineToList(search: SearchParameter): Observable<SearchResult<VerificationLineListView>> {
    const url = `${this.servicePath}/GetVerificationLineList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteVerificationLine(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteVerificationLine`;
    return this.dataGateway.delete(url, row);
  }
  getVerificationLineById(id: string): Observable<VerificationLineItemView> {
    const url = `${this.servicePath}/GetVerificationLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createVerificationLine(vmModel: VerificationLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateVerificationLine`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateVerificationLine(vmModel: VerificationLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateVerificationLine`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<any> {
    const url = `${this.servicePath}/GetVerificationLineInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByParent(id: string): Observable<any> {
    const url = `${this.servicePath}/GetAccessModeByParentVerificationLine/id=${id}`;
    return this.dataGateway.get(url);
  }
}
