import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerificationLineItemComponent } from './verification-line-item.component';

describe('VerificationLineItemViewComponent', () => {
  let component: VerificationLineItemComponent;
  let fixture: ComponentFixture<VerificationLineItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VerificationLineItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationLineItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
