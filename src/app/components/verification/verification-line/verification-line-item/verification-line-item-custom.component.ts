import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { EmptyGuid, VerificationStatus } from 'shared/constants';
import { isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { AccessModeView, VerificationLineItemView } from 'shared/models/viewModel';
import { VerificationLineService } from '../verification-line.service';
const VERIFICATION_TABLE = 'verificationtable';
const firstGroup = ['VERIFICATION_TABLE_GUID'];

export class VerificationLineItemCustomComponent {
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: VerificationLineItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (isUpdateMode(model.verificationLineGUID)) {
      if (model.documentStatus_Values >= VerificationStatus.Approved) {
        fieldAccessing.push({ filedIds: ['ALL_ID'], readonly: true });
        return of(fieldAccessing);
      } else {
        fieldAccessing.push({ filedIds: ['VERIFICATION_TABLE_GUID'], readonly: true });
        return of(fieldAccessing);
      }
    } else {
      fieldAccessing.push({ filedIds: ['VERIFICATION_TABLE_GUID'], readonly: true });
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: VerificationLineItemView): Observable<boolean> {
    const accessright = isNullOrUndefOrEmptyGUID(model.verificationTableGUID) ? canCreate : canUpdate;
    const accesslogic = model.documentStatus_Values >= VerificationStatus.Approved;
    return of(accessright && accesslogic);
  }
  getInitialData(verificationLineService: VerificationLineService, Id: string): Observable<VerificationLineItemView> {
    return verificationLineService.getInitialData(Id);
  }
  setAccessModeByParentStatus(model: VerificationLineItemView): Observable<AccessModeView> {
    let parentId = this.baseService.uiService.getRelatedInfoOriginTableKey();
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    switch (this.parentName) {
      case VERIFICATION_TABLE:
        this.accessModeView.canCreate = true;
        this.accessModeView.canDelete = false;
        this.accessModeView.canView = true;
        return of(this.accessModeView);
      default:
        return of(this.accessModeView);
    }
  }
}
