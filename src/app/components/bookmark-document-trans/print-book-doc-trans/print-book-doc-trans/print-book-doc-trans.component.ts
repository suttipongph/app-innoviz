import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUndefinedOrZeroLength, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { PrintBookDocTransParm } from 'shared/models/viewModel/printBookDocTransParm';
import { PrintBookDocTransView } from 'shared/models/viewModel/printBookDocTransView';
import { PrintBookDocTransService } from '../print-book-doc-trans.service';
import { PrintBookDocTransCustomComponent } from './print-book-doc-trans-custom.component';
@Component({
  selector: 'print-book-doc-trans',
  templateUrl: './print-book-doc-trans.component.html',
  styleUrls: ['./print-book-doc-trans.component.scss']
})
export class PrintBookDocTransComponent extends BaseItemComponent<PrintBookDocTransView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  refTypeOptions: SelectItems[] = [];
  printBookDocTransParm: PrintBookDocTransParm = new PrintBookDocTransParm();
  constructor(
    private service: PrintBookDocTransService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: PrintBookDocTransCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.passingModel = passingObj['model'];
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialUpdatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<PrintBookDocTransView> {
    return this.custom.getPrintBookDocTrans(this.model, this.passingModel);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {}

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        this.model = model;
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    this.printBookDocTransParm.documentTemplateTableGUID = this.model.documentTemplateTableGUID;
    this.printBookDocTransParm.refGUID = this.model.refGUID;
    this.printBookDocTransParm.bookmarkDocumentTransGUID = this.id;
    this.service.printBookDocTrans(this.printBookDocTransParm).subscribe((result) => {
      if (!isUndefinedOrZeroLength(result)) {
        this.baseService.fileService.saveAsFile(result.base64, result.fileName);
      }
      super.onBack();
    });
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
