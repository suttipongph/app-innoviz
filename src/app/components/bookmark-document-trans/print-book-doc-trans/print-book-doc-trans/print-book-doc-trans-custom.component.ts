import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { toMapModel } from 'shared/functions/model.function';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { PrintBookDocTransView } from 'shared/models/viewModel/printBookDocTransView';
import { PrintBookDocTransService } from '../print-book-doc-trans.service';

const firstGroup = ['REF_TYPE', 'REF_ID', 'DOCUMENT_STATUS', 'REFERENCE_EXTERNAL_ID', 'BOOKMARK_DOCUMENT_ID', 'BOOKMARK_DOCUMENT_TEMPLATE_ID'];

export class PrintBookDocTransCustomComponent {
  baseService: BaseServiceModel<PrintBookDocTransService>;
  getFieldAccessing(model: PrintBookDocTransView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: PrintBookDocTransView): Observable<boolean> {
    return of(!canCreate);
  }
  getPrintBookDocTrans(model: PrintBookDocTransView, passingModel: any): Observable<PrintBookDocTransView> {
    model = new PrintBookDocTransView();
    toMapModel(passingModel, model);
    return of(model);
  }
}
