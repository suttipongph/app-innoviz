import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintBookDocTransComponent } from './print-book-doc-trans.component';

describe('PrintBookDocTransViewComponent', () => {
  let component: PrintBookDocTransComponent;
  let fixture: ComponentFixture<PrintBookDocTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrintBookDocTransComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintBookDocTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
