import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrintBookDocTransService } from './print-book-doc-trans.service';
import { PrintBookDocTransComponent } from './print-book-doc-trans/print-book-doc-trans.component';
import { PrintBookDocTransCustomComponent } from './print-book-doc-trans/print-book-doc-trans-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PrintBookDocTransComponent],
  providers: [PrintBookDocTransService, PrintBookDocTransCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PrintBookDocTransComponent]
})
export class PrintBookDocTransComponentModule {}
