import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { FileInformation, PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { PrintBookDocTransParm } from 'shared/models/viewModel/printBookDocTransParm';
import { PrintBookDocTransView } from 'shared/models/viewModel/printBookDocTransView';
@Injectable()
export class PrintBookDocTransService {
  serviceKey = 'documentTemplateTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  printBookDocTrans(vmModel: PrintBookDocTransParm): Observable<FileInformation> {
    const url = `${this.servicePath}/PrintBookDocTrans`;
    return this.dataGateway.post(url, vmModel);
  }
}
