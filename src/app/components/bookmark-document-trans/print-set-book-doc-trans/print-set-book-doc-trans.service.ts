import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { FileInformation, PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { PrintSetBookDocTransParm } from 'shared/models/viewModel/printSetBookDocTransParm';
import { CreditAppRequestTableItemView } from 'shared/models/viewModel';
@Injectable()
export class PrintSetBookDocTransService {
  serviceKey = 'refGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getPrintSetBookDocTransByCreditAppReq(id:string): Observable<CreditAppRequestTableItemView> {
    const url = `${this.servicePath}/GetPrintBookmarkByCreditAppReqId/id=${id}`;
    return this.dataGateway.get(url);
  }
  printSetBookDocTrans(vmModel: PrintSetBookDocTransParm): Observable<FileInformation[]> {
    const url = `${this.servicePath}/PrintSetBookDocTrans`;
    return this.dataGateway.post(url, vmModel);
  }
}
