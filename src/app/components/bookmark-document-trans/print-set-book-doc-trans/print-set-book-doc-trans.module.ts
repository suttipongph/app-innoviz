import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrintSetBookDocTransService } from './print-set-book-doc-trans.service';
import { PrintSetBookDocTransComponent } from './print-set-book-doc-trans/print-set-book-doc-trans.component';
import { PrintSetBookDocTransCustomComponent } from './print-set-book-doc-trans/print-set-book-doc-trans-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PrintSetBookDocTransComponent],
  providers: [PrintSetBookDocTransService, PrintSetBookDocTransCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PrintSetBookDocTransComponent]
})
export class PrintSetBookDocTransComponentModule {}
