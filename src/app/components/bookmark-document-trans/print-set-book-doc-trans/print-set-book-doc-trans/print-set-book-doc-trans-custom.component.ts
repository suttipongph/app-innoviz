import { Observable, of } from 'rxjs';
import { EmptyGuid, RefType, ROUTE_MASTER_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { toMapModel } from 'shared/functions/model.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { PrintSetBookDocTransView } from 'shared/models/viewModel/printSetBookDocTransView';
import { PrintSetBookDocTransService } from '../print-set-book-doc-trans.service';
import { isNullOrUndefOrEmpty } from 'shared/functions/value.function';

const firstGroup = [
  'INTERNAL_AGREEMENT_ID',
  'AGREEMENT_ID',
  'AGREEMENT_DESCRIPTION',
  'REF_TYPE',
  'CREDIT_APP_REQUEST_ID',
  'CREDIT_APP_REQUEST_DESCRIPTION',
  'CREDIT_LIMIT_TYPE_ID',
  'CREDIT_APP_REQUEST_TYPE'
];
const MAIN_AGREEMENT_TABLE = 'mainagreementtable';
const ASSIGNMENT_AGREEMENT_TABLE = 'assignmentagreementtable';
const GUARANTOR_AGREEMENT_TABLE = 'guarantoragreementtable';
const BUSINESS_COLLATERALA_AGREEMENT = 'businesscollateralagmtable';
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';

export class PrintSetBookDocTransCustomComponent {
  baseService: BaseServiceModel<PrintSetBookDocTransService>;
  getFieldAccessing(model: PrintSetBookDocTransView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: PrintSetBookDocTransView): Observable<boolean> {
    return of(!canCreate);
  }
  getPrintSetBookDocTrans(model: PrintSetBookDocTransView, passingModel: any): Observable<PrintSetBookDocTransView> {
    model = new PrintSetBookDocTransView();
    toMapModel(passingModel, model);
    if (model.refType === RefType.CreditAppRequestTable && isNullOrUndefOrEmpty(model.creditAppRequestId)) {
      this.baseService.service.getPrintSetBookDocTransByCreditAppReq(model.refGUID).subscribe((res) => {
        (model.creditAppRequestType = res.creditAppRequestType),
          (model.creditAppRequestDescription = res.description),
          (model.creditAppRequestId = res.creditAppRequestId),
          (model.creditLimitTypeId = res.creditLimitType_CreditLimitTypeId);
      });
    }
    return of(model);
  }
  getVisibleFieldSet(refType: number): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: ['AGREEMENT', 'CREDIT_APPLICATION_REQUEST'], invisible: true });
    switch (refType) {
      case RefType.MainAgreement:
      case RefType.GuarantorAgreement:
      case RefType.AssignmentAgreement:
      case RefType.BusinessCollateralAgreement:
        fieldAccessing.push({ filedIds: ['AGREEMENT'], invisible: false });
        break;
      case RefType.CreditAppRequestTable:
        fieldAccessing.push({ filedIds: ['CREDIT_APPLICATION_REQUEST'], invisible: false });
        break;
    }

    return of(fieldAccessing);
  }
}
