import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintSetBookDocTransComponent } from './print-set-book-doc-trans.component';

describe('PrintSetBookDocTransViewComponent', () => {
  let component: PrintSetBookDocTransComponent;
  let fixture: ComponentFixture<PrintSetBookDocTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrintSetBookDocTransComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintSetBookDocTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
