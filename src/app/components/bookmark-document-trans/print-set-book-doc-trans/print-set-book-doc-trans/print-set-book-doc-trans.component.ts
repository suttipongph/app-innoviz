import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { map, tap } from 'rxjs/operators';
import { getBatchSize } from 'shared/config/globalvar.config';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUndefinedOrZeroLength, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { PrintSetBookDocTransParm } from 'shared/models/viewModel/printSetBookDocTransParm';
import { PrintSetBookDocTransView } from 'shared/models/viewModel/printSetBookDocTransView';
import { PrintSetBookDocTransService } from '../print-set-book-doc-trans.service';
import { PrintSetBookDocTransCustomComponent } from './print-set-book-doc-trans-custom.component';
@Component({
  selector: 'print-set-book-doc-trans',
  templateUrl: './print-set-book-doc-trans.component.html',
  styleUrls: ['./print-set-book-doc-trans.component.scss']
})
export class PrintSetBookDocTransComponent extends BaseItemComponent<PrintSetBookDocTransView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  creditAppRequestTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  printSetBookDocTransParm: PrintSetBookDocTransParm = new PrintSetBookDocTransParm();

  constructor(
    private service: PrintSetBookDocTransService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: PrintSetBookDocTransCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.passingModel = passingObj['model'];
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialUpdatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getCreditAppRequestTypeEnumDropDown(this.creditAppRequestTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<PrintSetBookDocTransView> {
    return this.custom.getPrintSetBookDocTrans(this.model, this.passingModel);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {}

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        this.model = model;
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
    this.custom.getVisibleFieldSet(this.model.refType).subscribe((res) => {
      super.setVisibilityFieldSet(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    this.printSetBookDocTransParm.refType = this.model.refType;
    this.printSetBookDocTransParm.refGUID = this.model.refGUID;
    this.printSetBookDocTransParm.documentTemplateTableGUID = this.model.documentTemplateTableGUID;
    this.printSetBookDocTransParm.bookmarkDocumentTransGUID = this.model.refGUID;
    this.service
      .printSetBookDocTrans(this.printSetBookDocTransParm)
      .pipe(
        tap(async (result) => {
          const batchSize = isNullOrUndefined(getBatchSize()) || getBatchSize() === 0 ? 5 : getBatchSize();
          var i = 0;
          for (i = 0; i < result.length; i++) {
            this.baseService.fileService.saveAsFile(result[i].base64, result[i].fileName);
            if ((i + 1) % batchSize == 0) {
              await this.pause(1000);
            }
          }
        })
      )
      .subscribe(() => {
        super.onBack();
      });
  }
  pause(msec) {
    return new Promise((resolve, reject) => {
      setTimeout(resolve, msec || 1000);
    });
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
