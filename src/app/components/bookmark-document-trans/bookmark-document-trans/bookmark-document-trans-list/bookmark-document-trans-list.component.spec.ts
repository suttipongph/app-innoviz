import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BookmarkDocumentTransListComponent } from './bookmark-document-trans-list.component';

describe('BookmarkDocumentTransListViewComponent', () => {
  let component: BookmarkDocumentTransListComponent;
  let fixture: ComponentFixture<BookmarkDocumentTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BookmarkDocumentTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkDocumentTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
