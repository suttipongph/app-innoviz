import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { AccessLevel, AccessMode, BookMarkDocumentStatus, ColumnType, ROUTE_FUNCTION_GEN, SortType } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { BookmarkDocumentTransListView } from 'shared/models/viewModel';
import { BookmarkDocumentTransService } from '../bookmark-document-trans.service';
import { BookmarkDocumentTransListCustomComponent } from './bookmark-document-trans-list-custom.component';
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const GUARANTOR_AGREEMENT_TABLE = 'guarantoragreementtable';
const BUSINESS_COLLATERALA_AGREEMENT = 'businesscollateralagmtable';
const AMEND_CA = 'amendca';
const AMEND_CA_LINE = 'amendcaline';
const REVIEW_CREDIT_APP_REQUEST = 'reviewcreditapprequest';
const CLOSR_CREDIT_LIMIT = 'closecreditlimit';
const MAIN_AGREEMENT_TABLE = 'mainagreementtable';
const ASSIGNMENT_AGREEMENT_TABLE = 'assignmentagreementtable';
const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';
const ADDENDUM_MAIN_AGREEMENT_TABLE = 'addendummainagreementtable';
const NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE = 'noticeofcancellationmainagreementtable';
const ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE = 'addendumbusinesscollateralagmtable';
const NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE = 'noticeofcancellationbusinesscollateralagmtable';
@Component({
  selector: 'bookmark-document-trans-list',
  templateUrl: './bookmark-document-trans-list.component.html',
  styleUrls: ['./bookmark-document-trans-list.component.scss']
})
export class BookmarkDocumentTransListComponent extends BaseListComponent<BookmarkDocumentTransListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  bookmarkDocumentOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  documentTemplateTableOptions: SelectItems[] = [];
  documentTemplateTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];

  constructor(
    public custom: BookmarkDocumentTransListCustomComponent,
    private service: BookmarkDocumentTransService,
    public uiControllerService: UIControllerService,
    private currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setRefTypeRefGUID(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.setFunctionOptions();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getDocumentTemplateTypeEnumDropDown(this.documentTemplateTypeOptions);
    this.custom.setAccessModeByParentStatus().subscribe((result) => {
      this.setDataGridOption();
      this.setFunctionOptions();
    });
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBookmarkDocumentDropDown(this.bookmarkDocumentOptions);
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
    this.baseDropdown.getBookmarkDocumentRefTypeEnumDropDown(this.documentTemplateTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'bookmarkDocumentTransGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.BOOKMARK_DOCUMENT_ID',
        textKey: 'bookmarkDocument_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.ASC,
        sortingOrder: 2,
        masterList: this.bookmarkDocumentOptions,
        sortingKey: 'bookmarkDocument_BookmarkDocumentId',
        searchingKey: 'bookmarkDocumentGUID'
      },
      {
        label: 'LABEL.DOCUMENT_TEMPLATE_TYPE_BOOKMARK',
        textKey: 'bookmarkDocument_DocumentTemplateType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.ASC,
        sortingOrder: 1,
        masterList: this.documentTemplateTypeOptions
      },
      {
        label: 'LABEL.DOCUMENT_TEMPLATE_ID_FILE_TEMPLATE',
        textKey: 'documentTemplateTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentTemplateTableOptions,
        sortingKey: 'documentTemplateTable_TemplateId',
        searchingKey: 'documentTemplateTableGUID'
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentStatusOptions,
        sortingKey: 'documentStatus_StatusId',
        searchingKey: 'documentStatusGUID'
      },
      {
        label: 'LABEL.REFERENCE_EXTERNAL_ID',
        textKey: 'referenceExternalId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: isNullOrUndefined(this.custom.refGUID) ? this.parentId : this.custom.refGUID
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<BookmarkDocumentTransListView>> {
    return this.service.getBookmarkDocumentTransToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteBookmarkDocumentTrans(row));
  }
  getRowAuthorize(row: BookmarkDocumentTransListView[]): void {
    switch (this.custom.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
      case GUARANTOR_AGREEMENT_TABLE:
      case BUSINESS_COLLATERALA_AGREEMENT:
      case AMEND_CA:
      case AMEND_CA_LINE:
      case REVIEW_CREDIT_APP_REQUEST:
      case CLOSR_CREDIT_LIMIT:
      case MAIN_AGREEMENT_TABLE:
      case ASSIGNMENT_AGREEMENT_TABLE:
      case LOAN_REQUEST:
      case BUYER_MATCHING:
      case ADDENDUM_MAIN_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE:
      case ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
      case NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
        row.forEach((item) => {
          let accessMode: AccessMode = item.documentStatus_StatusId === BookMarkDocumentStatus.Draft ? AccessMode.full : AccessMode.creator;
          item.rowAuthorize = this.getSingleRowAuthorize(accessMode, item);
        });
        break;
      default:
        super.getRowAuthorize(row);
    }
  }
  setFunctionOptions(): void {
    this.functionItems = [
      {
        label: 'LABEL.COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE',

        disabled: !this.custom.accessModeView.canCreate,
        command: () => this.getReftypeByActiveKey(ROUTE_FUNCTION_GEN.COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE)
      }
    ];
    super.setFunctionOptionsMapping();
  }
  getReftypeByActiveKey(route): any {
    this.toFunction({ path: route, parameters: { refType: this.custom.getReftypeByActiveKey() } });
  }
}
