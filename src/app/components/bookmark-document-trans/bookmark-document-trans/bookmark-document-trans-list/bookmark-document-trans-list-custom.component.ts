import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { RefType } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel, RefTypeModel } from 'shared/models/systemModel';
import { AccessModeView, BookmarkDocumentTransItemView } from 'shared/models/viewModel';
import { BookmarkDocumentTransService } from '../bookmark-document-trans.service';
const MAIN_AGREEMENT_TABLE = 'mainagreementtable';
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const ASSIGNMENT_AGREEMENT_TABLE = 'assignmentagreementtable';
const GUARANTOR_AGREEMENT_TABLE = 'guarantoragreementtable';
const BUSINESS_COLLATERALA_AGREEMENT = 'businesscollateralagmtable';
const AMEND_CA = 'amendca';
const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';
const REVIEW_CREDIT_APP_REQUEST = 'reviewcreditapprequest';
const CLOSR_CREDIT_LIMIT = 'closecreditlimit';
const ADDENDUM_MAIN_AGREEMENT_TABLE = 'addendummainagreementtable';
const NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE = 'noticeofcancellationmainagreementtable';
const ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE = 'addendumbusinesscollateralagmtable';
const NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE = 'noticeofcancellation';
const NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE = 'noticeofcancellationbusinesscollateralagmtable';
export class BookmarkDocumentTransListCustomComponent {
  baseService: BaseServiceModel<BookmarkDocumentTransService>;
  parentName = null;
  originId = null;
  accessModeView: AccessModeView = new AccessModeView();
  refGUID: string;
  refType: RefType;
  hasFunction = false;
  isBuyerMatching: boolean = false;
  isLoanRequest: boolean = false;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setAccessModeByParentStatus(): Observable<AccessModeView> {
    let parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
    let originId = this.baseService.uiService.getRelatedInfoOriginTableKey();

    switch (this.parentName) {
      case MAIN_AGREEMENT_TABLE:
      case ADDENDUM_MAIN_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE:
        this.hasFunction = true;
        return this.baseService.service.getAccessModeByMainAgreementTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            this.accessModeView.canView = true;
          })
        );
      case CREDIT_APP_REQUEST_TABLE:
        return this.baseService.service.getAccessModeByCreditAppRequestTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            this.accessModeView.canView = true;
          })
        );
      case ASSIGNMENT_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE:
        this.hasFunction = true;
        return this.baseService.service.getAccessModeByAssignmentTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            this.accessModeView.canView = true;
          })
        );
      case GUARANTOR_AGREEMENT_TABLE:
        this.hasFunction = true;

        return this.baseService.service.getAccessModeByGuarantorAgreementTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            this.accessModeView.canView = true;
          })
        );
      case BUSINESS_COLLATERALA_AGREEMENT:
      case ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
      case NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
        this.hasFunction = true;
        return this.baseService.service.getAccessModeByBusinessCollateralAgmTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            this.accessModeView.canView = true;
          })
        );
      case AMEND_CA:
        this.hasFunction = true;
        return this.baseService.service.getAccessModeByCreditAppRequestTable(this.refGUID).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            this.accessModeView.canView = true;
          })
        );
      case LOAN_REQUEST:
      case BUYER_MATCHING:
        this.checkIsBuyerMacthingOrLoanRequest();
        return this.getAccessModeByCreditAppRequestTableBuyerAndLoan();
      case REVIEW_CREDIT_APP_REQUEST:
        this.hasFunction = true;
        return this.baseService.service.getAccessModeByCreditAppRequestLine(this.refGUID).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            this.accessModeView.canView = true;
          })
        );
      case CLOSR_CREDIT_LIMIT:
        this.hasFunction = true;
        return this.baseService.service.getAccessModeByCreditAppRequestTable(this.refGUID).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            this.accessModeView.canView = true;
          })
        );
      default:
        if (this.refType === RefType.CreditAppRequestTable) {
          return this.baseService.service.getAccessModeByCreditAppRequestTable(this.refGUID).pipe(
            tap((result) => {
              this.accessModeView = result as AccessModeView;
              this.accessModeView.canView = true;
            })
          );
        } else {
          this.accessModeView.canView = true;
          return of(this.accessModeView);
        }
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
    //return true;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
  setRefTypeRefGUID(passingObj: RefTypeModel): void {
    if (!isNullOrUndefined(passingObj)) {
      this.refType = passingObj.refType;
      this.refGUID = passingObj.refGUID;
    }
  }
  getReftypeByActiveKey(): string {
    switch (this.parentName) {
      case ASSIGNMENT_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE:
        return RefType.AssignmentAgreement.toString();
      case MAIN_AGREEMENT_TABLE:
      case ADDENDUM_MAIN_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE:
        return RefType.MainAgreement.toString();
      case GUARANTOR_AGREEMENT_TABLE:
        return RefType.GuarantorAgreement.toString();
      case BUSINESS_COLLATERALA_AGREEMENT:
      case ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
      case NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
        return RefType.BusinessCollateralAgreement.toString();
      default:
        return null;
    }
  }
  checkIsBuyerMacthingOrLoanRequest(): void {
    this.isBuyerMatching = window.location.href.match(BUYER_MATCHING) ? true : false;
    this.isLoanRequest = window.location.href.match(LOAN_REQUEST) ? true : false;
    if (this.isBuyerMatching) {
      this.originId = this.baseService.uiService.getKey(BUYER_MATCHING);
    } else if (this.isLoanRequest) {
      this.originId = this.baseService.uiService.getKey(LOAN_REQUEST);
    }
  }

  getAccessModeByCreditAppRequestTableBuyerAndLoan() {
    return this.baseService.service.getAccessModeByCreditAppRequestTable(this.originId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
        this.accessModeView.canView = true;
      })
    );
  }
}
