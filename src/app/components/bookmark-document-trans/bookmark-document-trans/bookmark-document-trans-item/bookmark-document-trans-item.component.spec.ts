import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookmarkDocumentTransItemComponent } from './bookmark-document-trans-item.component';

describe('BookmarkDocumentTransItemViewComponent', () => {
  let component: BookmarkDocumentTransItemComponent;
  let fixture: ComponentFixture<BookmarkDocumentTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BookmarkDocumentTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkDocumentTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
