import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { BookMarkDocumentStatus, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { BookmarkDocumentTransItemView } from 'shared/models/viewModel';
import { PrintBookDocTransView } from 'shared/models/viewModel/printBookDocTransView';
import { BookmarkDocumentTransService } from '../bookmark-document-trans.service';
import { BookmarkDocumentTransItemCustomComponent } from './bookmark-document-trans-item-custom.component';
@Component({
  selector: 'bookmark-document-trans-item',
  templateUrl: './bookmark-document-trans-item.component.html',
  styleUrls: ['./bookmark-document-trans-item.component.scss']
})
export class BookmarkDocumentTransItemComponent extends BaseItemComponent<BookmarkDocumentTransItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  bookmarkDocumentOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  documentTemplateTableOptions: SelectItems[] = [];
  documentTemplateTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];

  constructor(
    private service: BookmarkDocumentTransService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: BookmarkDocumentTransItemCustomComponent,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoOriginTableName();
    this.custom.parentNameL2 = this.uiService.getRelatedInfoParentTableName();
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setRefTypeRefGUID(passingObj);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getDocumentTemplateTypeEnumDropDown(this.documentTemplateTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<BookmarkDocumentTransItemView> {
    return this.service.getBookmarkDocumentTransById(this.id);
  }
  getInitialData(): Observable<BookmarkDocumentTransItemView> {
    return this.custom.getInnitialDataCustom(this.parentId, this.service);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin([
      this.baseDropdown.getBookmarkDocumentDropDown(),
      this.baseDropdown.getDocumentStatusDropDown(),
      this.custom.getDocumentTemplateTableDropDown(model)
    ]).subscribe(
      ([bookmarkDocument, documentStatus, documentTemplateTable]) => {
        this.bookmarkDocumentOptions = bookmarkDocument;
        this.documentStatusOptions = documentStatus;
        this.documentTemplateTableOptions = documentTemplateTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setFunctionOptions(model: BookmarkDocumentTransItemView): void {
    this.functionItems = [
      {
        label: 'LABEL.PRINT_BOOKMARK_DOCUMENT_TRANSACTION',
        command: () => this.setPreparingFunctionDataAndValidation(ROUTE_FUNCTION_GEN.PRINT_BOOKMARK_DOCUMENT_TRANSACTION),
        disabled: !(model.documentStatus_StatusId != BookMarkDocumentStatus.Rejected)
      }
    ];
    super.setFunctionOptionsMapping([{ label: 'LABEL.PRINT_BOOKMARK_DOCUMENT_TRANSACTION' }]);
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateBookmarkDocumentTrans(this.model), isColsing);
    } else {
      super.onCreate(this.service.createBookmarkDocumentTrans(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onBookmarkDocumentChange() {
    this.custom.setDocumentTemplateType(this.model, this.bookmarkDocumentOptions).subscribe((result) => {
      this.documentTemplateTableOptions = result;
    });
  }
  getPrintBookDocTransParamModel(): PrintBookDocTransView {
    return this.custom.getPrintBookDocTransParamModel(
      this.model,
      this.documentStatusOptions,
      this.bookmarkDocumentOptions,
      this.documentTemplateTableOptions
    );
  }
  setPreparingFunctionDataAndValidation(path: string): void {
    this.custom.setPreparingFunctionDataAndValidation(path, this.model).subscribe((res) => {
      if (!isNullOrUndefined(res)) {
        this.toFunction({
          path: path,
          parameters: {
            model: this.getPrintBookDocTransParamModel()
          }
        });
      }
    });
  }
}
