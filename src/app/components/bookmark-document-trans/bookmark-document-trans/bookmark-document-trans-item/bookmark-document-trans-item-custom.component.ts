import { NumberInput } from '@angular/cdk/coercion';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { BookMarkDocumentStatus, DocumentTemplateType, RefType, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { toMapModel } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, RefTypeModel, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, BookmarkDocumentItemView, BookmarkDocumentTransItemView } from 'shared/models/viewModel';
import { PrintBookDocTransView } from 'shared/models/viewModel/printBookDocTransView';
import { BookmarkDocumentTransService } from '../bookmark-document-trans.service';

const firstGroup = ['REF_TYPE', 'REF_ID', 'DOCUMENT_TEMPLATE_TYPE', 'REF_GUID', 'BOOKMARK_DOCUMENT_TRANS_GUID'];
const condition1 = ['REFERENCE_EXTERNAL_ID', 'BOOKMARK_DOCUMENT_GUID', 'DOCUMENT_TEMPLATE_TABLE_GUID', 'REFERENCE_EXTERNAL_DATE'];

const MAIN_AGREEMENT_TABLE = 'mainagreementtable';
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const ASSIGNMENT_AGREEMENT_TABLE = 'assignmentagreementtable';
const GUARANTOR_AGREEMENT_TABLE = 'guarantoragreementtable';
const BUSINESS_COLLATERALA_AGREEMENT = 'businesscollateralagmtable';
const AMEND_CA = 'amendca';
const AMEND_CA_LINE = 'amendcaline';
const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';
const REVIEW_CREDIT_APP_REQUEST = 'reviewcreditapprequest';
const CLOSR_CREDIT_LIMIT = 'closecreditlimit';
const ADDENDUM_MAIN_AGREEMENT_TABLE = 'addendummainagreementtable';
const NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE = 'noticeofcancellationmainagreementtable';
const ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE = 'addendumbusinesscollateralagmtable';
const NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE = 'noticeofcancellation';
const NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE = 'noticeofcancellationbusinesscollateralagmtable';
export class BookmarkDocumentTransItemCustomComponent {
  baseService: BaseServiceModel<BookmarkDocumentTransService>;
  parentName = null;
  originId = null;

  parentNameL2 = null;
  accessModeView: AccessModeView = new AccessModeView();
  refGUID: string;
  refType: RefType;
  isBuyerMatching: boolean = false;
  isLoanRequest: boolean = false;
  getFieldAccessing(model: BookmarkDocumentTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    if (isUpdateMode(model.bookmarkDocumentTransGUID)) {
      if (model.documentStatus_StatusId !== BookMarkDocumentStatus.Draft) {
        fieldAccessing.push({ filedIds: condition1, readonly: true });
      } else {
        fieldAccessing.push({ filedIds: condition1, readonly: false });
      }
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: BookmarkDocumentTransItemView): Observable<boolean> {
    return this.setAccessModeByParentStatus(model).pipe(
      map((result) => (isUpdateMode(model.bookmarkDocumentTransGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  setAccessModeByParentStatus(model: BookmarkDocumentTransItemView): Observable<AccessModeView> {
    let parentId = this.baseService.uiService.getRelatedInfoOriginTableKey();
    this.originId = this.baseService.uiService.getRelatedInfoOriginTableKey();
    switch (this.parentNameL2) {
      case MAIN_AGREEMENT_TABLE:
      case ADDENDUM_MAIN_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE:
        return this.baseService.service.getAccessModeByMainAgreementTable(this.baseService.uiService.getRelatedInfoParentTableKey()).pipe(
          map((result: AccessModeView) => {
            let isStatusValid = model.documentStatus_StatusId === BookMarkDocumentStatus.Draft;
            result.canCreate = result.canCreate && isStatusValid;
            result.canDelete = result.canDelete && isStatusValid;
            result.canView = result.canView && isStatusValid;
            this.accessModeView = result;
            return result;
          })
        );
      case CREDIT_APP_REQUEST_TABLE:
        return this.baseService.service.getAccessModeByCreditAppRequestTable(parentId).pipe(
          map((result: AccessModeView) => {
            let isStatusValid = model.documentStatus_StatusId === BookMarkDocumentStatus.Draft;
            result.canCreate = result.canCreate && isStatusValid;
            result.canDelete = result.canDelete && isStatusValid;
            result.canView = result.canView && isStatusValid;
            this.accessModeView = result;
            return result;
          })
        );
      case ASSIGNMENT_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE:
        return this.baseService.service.getAccessModeByAssignmentTable(this.baseService.uiService.getRelatedInfoParentTableKey()).pipe(
          map((result: AccessModeView) => {
            let isStatusValid = model.documentStatus_StatusId === BookMarkDocumentStatus.Draft;
            result.canCreate = result.canCreate && isStatusValid;
            result.canDelete = result.canDelete && isStatusValid;
            result.canView = result.canView && isStatusValid;
            this.accessModeView = result;
            return result;
          })
        );
      case GUARANTOR_AGREEMENT_TABLE:
        let parentId2 = this.baseService.uiService.getRelatedInfoActiveTableKey();
        return this.baseService.service.getAccessModeByGuarantorAgreementTable(parentId2).pipe(
          map((result: AccessModeView) => {
            let isStatusValid = model.documentStatus_StatusId === BookMarkDocumentStatus.Draft;
            result.canCreate = result.canCreate && isStatusValid;
            result.canDelete = result.canDelete && isStatusValid;
            result.canView = result.canView && isStatusValid;
            this.accessModeView = result;
            return result;
          })
        );
      case BUSINESS_COLLATERALA_AGREEMENT:
      case ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
      case NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
        return this.baseService.service.getAccessModeByBusinessCollateralAgmTable(this.baseService.uiService.getRelatedInfoParentTableKey()).pipe(
          map((result: AccessModeView) => {
            let isStatusValid = model.documentStatus_StatusId === BookMarkDocumentStatus.Draft;
            result.canCreate = result.canCreate && isStatusValid;
            result.canDelete = result.canDelete && isStatusValid;
            result.canView = result.canView && isStatusValid;
            this.accessModeView = result;
            return result;
          })
        );
      case AMEND_CA:
        return this.baseService.service.getAccessModeByCreditAppRequestTable(this.refGUID).pipe(
          map((result: AccessModeView) => {
            let isStatusValid = model.documentStatus_StatusId === BookMarkDocumentStatus.Draft;
            result.canCreate = result.canCreate && isStatusValid;
            result.canDelete = result.canDelete && isStatusValid;
            result.canView = result.canView && isStatusValid;
            this.accessModeView = result;
            return result;
          })
        );
      case AMEND_CA_LINE:
        return this.baseService.service.getAccessModeByCreditAppRequestTable(this.refGUID).pipe(
          map((result: AccessModeView) => {
            let isStatusValid = model.documentStatus_StatusId === BookMarkDocumentStatus.Draft;
            result.canCreate = result.canCreate && isStatusValid;
            result.canDelete = result.canDelete && isStatusValid;
            result.canView = result.canView && isStatusValid;
            this.accessModeView = result;
            return result;
          })
        );
      case AMEND_CA:
        return this.baseService.service.getAccessModeByCreditAppRequestTable(this.refGUID).pipe(
          map((result: AccessModeView) => {
            let isStatusValid = model.documentStatus_StatusId === BookMarkDocumentStatus.Draft;
            result.canCreate = result.canCreate && isStatusValid;
            result.canDelete = result.canDelete && isStatusValid;
            result.canView = result.canView && isStatusValid;
            this.accessModeView = result;
            return result;
          })
        );
      case AMEND_CA_LINE:
        return this.baseService.service.getAccessModeByCreditAppRequestTable(this.refGUID).pipe(
          map((result: AccessModeView) => {
            let isStatusValid = model.documentStatus_StatusId === BookMarkDocumentStatus.Draft;
            result.canCreate = result.canCreate && isStatusValid;
            result.canDelete = result.canDelete && isStatusValid;
            result.canView = result.canView && isStatusValid;
            this.accessModeView = result;
            return result;
          })
        );
      case AMEND_CA:
        return this.baseService.service.getAccessModeByCreditAppRequestTable(this.refGUID).pipe(
          map((result: AccessModeView) => {
            let isStatusValid = model.documentStatus_StatusId === BookMarkDocumentStatus.Draft;
            result.canCreate = result.canCreate && isStatusValid;
            result.canDelete = result.canDelete && isStatusValid;
            result.canView = result.canView && isStatusValid;
            this.accessModeView = result;
            return result;
          })
        );
      case LOAN_REQUEST:
      case BUYER_MATCHING:
        this.checkIsBuyerMacthingOrLoanRequest();
        return this.baseService.service.getAccessModeByCreditAppRequestTable(this.originId).pipe(
          map((result: AccessModeView) => {
            let isStatusValid = model.documentStatus_StatusId === BookMarkDocumentStatus.Draft;
            result.canCreate = result.canCreate && isStatusValid;
            result.canDelete = result.canDelete && isStatusValid;
            result.canView = result.canView && isStatusValid;
            this.accessModeView = result;
            return result;
          })
        );
      case REVIEW_CREDIT_APP_REQUEST:
        return this.baseService.service.getAccessModeByCreditAppRequestLine(this.refGUID).pipe(
          map((result: AccessModeView) => {
            let isStatusValid = model.documentStatus_StatusId === BookMarkDocumentStatus.Draft;
            result.canCreate = result.canCreate && isStatusValid;
            result.canDelete = result.canDelete && isStatusValid;
            result.canView = result.canView && isStatusValid;
            this.accessModeView = result;
            return result;
          })
        );
      case CLOSR_CREDIT_LIMIT:
        return this.baseService.service.getAccessModeByCreditAppRequestTable(this.refGUID).pipe(
          map((result: AccessModeView) => {
            let isStatusValid = model.documentStatus_StatusId === BookMarkDocumentStatus.Draft;
            result.canCreate = result.canCreate && isStatusValid;
            result.canDelete = result.canDelete && isStatusValid;
            result.canView = result.canView && isStatusValid;
            this.accessModeView = result;
            return result;
          })
        );
        break;
      default:
        return of(this.accessModeView);
    }
  }
  setDocumentTemplateType(model: BookmarkDocumentTransItemView, bookMarkoption: SelectItems[]): Observable<SelectItems[]> {
    model.bookmarkDocument_DocumentTemplateType = 0;
    model.documentTemplateTableGUID = null;
    if (!isNullOrUndefOrEmptyGUID(model.bookmarkDocumentGUID)) {
      let item = bookMarkoption.find((t) => t.value === model.bookmarkDocumentGUID);
      const rowData = !isNullOrUndefOrEmptyGUID(item) ? (item.rowData as BookmarkDocumentItemView) : new BookmarkDocumentItemView();
      model.bookmarkDocument_DocumentTemplateType = rowData.documentTemplateType;

      return this.baseService.baseDropdown.getDocumentTemplateTableDropDownByDocumenttemplatetype(rowData.documentTemplateType.toString());
    } else {
      return of([]);
    }
  }

  getDocumentTemplateTableDropDown(model: BookmarkDocumentTransItemView): Observable<SelectItems[]> {
    if (!isNullOrUndefined(model)) {
      return this.baseService.baseDropdown.getDocumentTemplateTableDropDownByDocumenttemplatetype(
        model.bookmarkDocument_DocumentTemplateType.toString()
      );
    } else {
      return of([]);
    }
  }
  getInnitialDataCustom(id: string, service: BookmarkDocumentTransService): Observable<BookmarkDocumentTransItemView> {
    switch (this.parentNameL2) {
      case MAIN_AGREEMENT_TABLE:
      case ADDENDUM_MAIN_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE:
        return service.getInitialData(id);
      case GUARANTOR_AGREEMENT_TABLE:
        return service.getInitialDataByGuarantorAgreement(id);
      case BUSINESS_COLLATERALA_AGREEMENT:
      case ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
      case NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
        return service.getInitialDataByBusinessCollateralAgmTable(id);
      case ASSIGNMENT_AGREEMENT_TABLE:
      case NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE:
        return service.getInitialData(id);
      case AMEND_CA:
        return service.getInitialData(this.refGUID);
      case AMEND_CA_LINE:
        return service.getInitialData(this.refGUID);
      case REVIEW_CREDIT_APP_REQUEST:
        return service.getInitialDataByCreditAppLine(this.refGUID);
      default:
        return service.getInitialData(id);
    }
  }
  setRefTypeRefGUID(passingObj: RefTypeModel): void {
    if (!isNullOrUndefined(passingObj)) {
      this.refType = passingObj.refType;
      this.refGUID = passingObj.refGUID;
    }
  }
  setPreparingFunctionDataAndValidation(path: string, model: BookmarkDocumentTransItemView): Observable<any> {
    switch (path) {
      case ROUTE_FUNCTION_GEN.PRINT_BOOKMARK_DOCUMENT_TRANSACTION:
        return this.baseService.service.getPrintBookDocTransValidation(model).pipe(
          map((res) => {
            if (res) {
              return { BookmarkDocumentTransItemView: res };
            } else {
              return null;
            }
          })
        );
      default:
        break;
    }
  }
  getPrintBookDocTransParamModel(
    model: BookmarkDocumentTransItemView,
    documentStatusOptions: SelectItems[],
    bookmarkDocumentOptions: SelectItems[],
    documentTemplateTableOptions: SelectItems[]
  ): PrintBookDocTransView {
    const paramModel = new PrintBookDocTransView();
    toMapModel(model, paramModel);
    if (!isNullOrUndefined(model.documentStatusGUID)) {
      paramModel.documentStatus_Values = documentStatusOptions.find((f) => f.value === model.documentStatusGUID).label;
    }
    if (!isNullOrUndefined(model.bookmarkDocumentGUID)) {
      paramModel.bookmarkDocument_Values = bookmarkDocumentOptions.find((f) => f.value === model.bookmarkDocumentGUID).label;
    }
    if (!isNullOrUndefined(model.documentTemplateTableGUID)) {
      paramModel.documentTemplate_Values = documentTemplateTableOptions.find((f) => f.value === model.documentTemplateTableGUID).label;
    }
    return paramModel;
  }
  checkIsBuyerMacthingOrLoanRequest(): void {
    this.isBuyerMatching = window.location.href.match(BUYER_MATCHING) ? true : false;
    this.isLoanRequest = window.location.href.match(LOAN_REQUEST) ? true : false;
    if (this.isBuyerMatching) {
      this.originId = this.baseService.uiService.getKey(BUYER_MATCHING);
    } else if (this.isLoanRequest) {
      this.originId = this.baseService.uiService.getKey(LOAN_REQUEST);
    }
  }
}
