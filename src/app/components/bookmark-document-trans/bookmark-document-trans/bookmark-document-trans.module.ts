import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BookmarkDocumentTransService } from './bookmark-document-trans.service';
import { BookmarkDocumentTransListComponent } from './bookmark-document-trans-list/bookmark-document-trans-list.component';
import { BookmarkDocumentTransItemComponent } from './bookmark-document-trans-item/bookmark-document-trans-item.component';
import { BookmarkDocumentTransItemCustomComponent } from './bookmark-document-trans-item/bookmark-document-trans-item-custom.component';
import { BookmarkDocumentTransListCustomComponent } from './bookmark-document-trans-list/bookmark-document-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [BookmarkDocumentTransListComponent, BookmarkDocumentTransItemComponent],
  providers: [BookmarkDocumentTransService, BookmarkDocumentTransItemCustomComponent, BookmarkDocumentTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [BookmarkDocumentTransListComponent, BookmarkDocumentTransItemComponent]
})
export class BookmarkDocumentTransComponentModule {}
