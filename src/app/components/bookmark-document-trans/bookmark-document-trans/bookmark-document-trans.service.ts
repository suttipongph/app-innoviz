import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, BookmarkDocumentTransItemView, BookmarkDocumentTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class BookmarkDocumentTransService {
  serviceKey = 'bookmarkDocumentTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getBookmarkDocumentTransToList(search: SearchParameter): Observable<SearchResult<BookmarkDocumentTransListView>> {
    const url = `${this.servicePath}/GetBookmarkDocumentTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteBookmarkDocumentTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteBookmarkDocumentTrans`;
    return this.dataGateway.delete(url, row);
  }
  getBookmarkDocumentTransById(id: string): Observable<BookmarkDocumentTransItemView> {
    const url = `${this.servicePath}/GetBookmarkDocumentTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createBookmarkDocumentTrans(vmModel: BookmarkDocumentTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateBookmarkDocumentTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateBookmarkDocumentTrans(vmModel: BookmarkDocumentTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateBookmarkDocumentTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<BookmarkDocumentTransItemView> {
    const url = `${this.servicePath}/GetBookmarkDocumentTransInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getInitialDataByCreditAppLine(id: string): Observable<BookmarkDocumentTransItemView> {
    const url = `${this.servicePath}/GetBookmarkDocumentTransInitialDataByCreditAppLine/id=${id}`;
    return this.dataGateway.get(url);
  }
  getInitialDataByGuarantorAgreement(id: string): Observable<BookmarkDocumentTransItemView> {
    const url = `${this.servicePath}/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id=${id}`;
    return this.dataGateway.get(url);
  }
  getInitialDataByBusinessCollateralAgmTable(id: string): Observable<BookmarkDocumentTransItemView> {
    const url = `${this.servicePath}/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByMainAgreementTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByMainAgreementTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByCreditAppRequestTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppRequestTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByCreditAppRequestLine(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppRequestLine/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByAssignmentTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByAssignmentTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByGuarantorAgreementTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByGuarantorAgreementTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByBusinessCollateralAgmTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByBusinessCollateralAgmTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getPrintBookDocTransValidation(vmModel: BookmarkDocumentTransItemView): Observable<boolean> {
    const url = `${this.servicePath}/GetPrintBookDocTransValidation`;
    return this.dataGateway.post(url, vmModel);
  }
  getAccessModeByCreditAppTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getReftypeByActiveKey(id: string): Observable<string> {
    const url = `${this.servicePath}/getReftypeByActiveKey/id=${id}`;
    return this.dataGateway.get(url);
  }
}
