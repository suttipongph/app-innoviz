import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CopyBookmarkDocumentTemplateView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CopyBookmarkDocumentTemplateService {
  serviceKey = 'copyBookmarkDocumentTemplateGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCopyBookmarkDocumentTemplateToList(search: SearchParameter): Observable<SearchResult<CopyBookmarkDocumentTemplateView>> {
    const url = `${this.servicePath}/GetCopyBookmarkDocumentTemplateList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCopyBookmarkDocumentTemplate(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCopyBookmarkDocumentTemplate`;
    return this.dataGateway.delete(url, row);
  }
  getCopyBookmarkDocumentTemplateById(id: string): Observable<CopyBookmarkDocumentTemplateView> {
    const url = `${this.servicePath}/GetCopyBookmarkDocumentTemplateById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCopyBookmarkDocumentTemplate(vmModel: CopyBookmarkDocumentTemplateView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCopyBookmarkDocumentTemplate`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCopyBookmarkDocumentTemplate(vmModel: CopyBookmarkDocumentTemplateView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCopyBookmarkDocumentTemplate`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  CopyBookmarkDocumentFromTemplate(vmModel: CopyBookmarkDocumentTemplateView): Observable<CopyBookmarkDocumentTemplateView> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vmModel);
  }
  getValidation(vmModel: CopyBookmarkDocumentTemplateView): Observable<boolean> {
    const url = `${this.servicePath}/GetCopyBookmarkDocumentValidation`;
    return this.dataGateway.post(url, vmModel);
  }
}
