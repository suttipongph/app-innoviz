import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopyBookmarkDocumentTemplateComponent } from './copy-bookmark-document-template.component';

describe('CopyBookmarkDocumentTemplateViewComponent', () => {
  let component: CopyBookmarkDocumentTemplateComponent;
  let fixture: ComponentFixture<CopyBookmarkDocumentTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CopyBookmarkDocumentTemplateComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyBookmarkDocumentTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
