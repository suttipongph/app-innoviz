import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, RefType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { CopyBookmarkDocumentTemplateView } from 'shared/models/viewModel';
import { CopyBookmarkDocumentTemplateService } from '../copy-bookmark-document-template.service';
import { CopyBookmarkDocumentTemplateCustomComponent } from './copy-bookmark-document-template-custom.component';
@Component({
  selector: 'copy-bookmark-document-template',
  templateUrl: './copy-bookmark-document-template.component.html',
  styleUrls: ['./copy-bookmark-document-template.component.scss']
})
export class CopyBookmarkDocumentTemplateComponent extends BaseItemComponent<CopyBookmarkDocumentTemplateView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  bookmarkDocumentTemplateTableOptions: SelectItems[] = [];

  constructor(
    private service: CopyBookmarkDocumentTemplateService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CopyBookmarkDocumentTemplateCustomComponent
  ) {
    super();

    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiService.getRelatedInfoOriginTableKey();
    this.custom.getRefType(this.getPassingObject());
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<CopyBookmarkDocumentTemplateView> {
    return this.service.getCopyBookmarkDocumentTemplateById(this.id);
  }
  getInitialData(): Observable<CopyBookmarkDocumentTemplateView> {
    return this.custom.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getbookmarkDocumentTemplateTableDropDownByReftype()).subscribe(
      ([bookmarkDocumentTemplateTable]) => {
        this.bookmarkDocumentTemplateTableOptions = bookmarkDocumentTemplateTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(true, super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {}
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.custom.getModelParam(this.model, this.id).subscribe((model) => {
          this.onSubmit(true, model);
        });
      }
    });
  }
  onSubmit(isColsing: boolean, model: CopyBookmarkDocumentTemplateView): void {
    super.onExecuteFunction(this.service.CopyBookmarkDocumentFromTemplate(model));
  }

  getDataValidation(): Observable<boolean> {
    return this.service.getValidation(this.model);
  }
  onClose(): void {
    super.onBack();
  }
}
