import { Observable, of } from 'rxjs';
import { EmptyGuid, RefType } from 'shared/constants';
import { FieldAccessing } from 'shared/models/systemModel';
import { BaseServiceModel, RefTypeModel } from 'shared/models/systemModel';
import { CopyBookmarkDocumentTemplateView } from 'shared/models/viewModel';

const firstGroup = [];
const MAIN_AGREEMENT_TABLE = 'mainagreementtable';
const ASSIGNMENT_AGREEMENT_TABLE = 'assignmentagreementtable';
const GUARANTOR_AGREEMENT_TABLE = 'guarantoragreementtable';
const BUSINESS_COLLATERALA_AGREEMENT = 'businesscollateralagmtable';
const ADDENDUM_MAIN_AGREEMENT_TABLE = 'addendummainagreementtable';
const NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE = 'noticeofcancellationmainagreementtable';
const NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE = 'noticeofcancellation';
const NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE = 'noticeofcancellationbusinesscollateralagmtable';
const ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE = 'addendumbusinesscollateralagmtable';

export class CopyBookmarkDocumentTemplateCustomComponent {
  baseService: BaseServiceModel<any>;
  refType: null;
  getFieldAccessing(model: CopyBookmarkDocumentTemplateView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CopyBookmarkDocumentTemplateView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(refGUID: string): Observable<CopyBookmarkDocumentTemplateView> {
    let parentId = this.baseService.uiService.getRelatedInfoActiveTableKey();
    let parentName = this.baseService.uiService.getRelatedInfoActiveTableName();
    let originalName = this.baseService.uiService.getRelatedInfoOriginTableName();
    let model = new CopyBookmarkDocumentTemplateView();
    switch (originalName) {
      case ASSIGNMENT_AGREEMENT_TABLE:
        model.refType = RefType.AssignmentAgreement.toString();
        model.refGUID = refGUID;
        switch (parentName) {
          case NOTICE_OF_CANCELLATION_ASSIGNMENT_AGREEMENT_TABLE:
            model.refType = RefType.AssignmentAgreement.toString();
            model.refGUID = parentId;
            break;
          default:
        }
        break;
      case BUSINESS_COLLATERALA_AGREEMENT:
        model.refType = RefType.BusinessCollateralAgreement.toString();
        model.refGUID = refGUID;
        switch (parentName) {
          case ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE:
            model.refType = RefType.BusinessCollateralAgreement.toString();
            model.refGUID = parentId;
            break;
          case NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE:
            model.refType = RefType.BusinessCollateralAgreement.toString();
            model.refGUID = parentId;
            break;
          default:
        }
        break;
      case MAIN_AGREEMENT_TABLE:
        model.refType = RefType.MainAgreement.toString();
        model.refGUID = refGUID;
        switch (parentName) {
          case MAIN_AGREEMENT_TABLE:
            model.refType = RefType.MainAgreement.toString();
            model.refGUID = refGUID;
            break;
          case GUARANTOR_AGREEMENT_TABLE:
            model.refType = RefType.GuarantorAgreement.toString();
            model.refGUID = parentId;
            break;
          case ADDENDUM_MAIN_AGREEMENT_TABLE:
            model.refType = RefType.MainAgreement.toString();
            model.refGUID = parentId;
            break;
          case NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE:
            model.refType = RefType.MainAgreement.toString();
            model.refGUID = parentId;
            break;
          default:
        }

        break;

      default:
    }
    model.copyBookmarkDocumentTemplateGUID = EmptyGuid;
    return of(model);
  }
  getModelParam(model: CopyBookmarkDocumentTemplateView, parentGUID: string): Observable<CopyBookmarkDocumentTemplateView> {
    return of(model);
  }
  getRefType(passingObj: any): any {
    this.refType = passingObj.refType;
  }
}
