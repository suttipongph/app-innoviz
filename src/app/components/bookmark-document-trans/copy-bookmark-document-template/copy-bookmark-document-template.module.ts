import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CopyBookmarkDocumentTemplateService } from './copy-bookmark-document-template.service';
import { CopyBookmarkDocumentTemplateComponent } from './copy-bookmark-document-template/copy-bookmark-document-template.component';
import { CopyBookmarkDocumentTemplateCustomComponent } from './copy-bookmark-document-template/copy-bookmark-document-template-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CopyBookmarkDocumentTemplateComponent],
  providers: [CopyBookmarkDocumentTemplateService, CopyBookmarkDocumentTemplateCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CopyBookmarkDocumentTemplateComponent]
})
export class CopyBookmarkDocumentTemplateComponentModule {}
