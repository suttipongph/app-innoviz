import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { VendBankItemView, VendBankListView } from 'shared/models/viewModel';

@Injectable()
export class VendBankService {
  serviceKey = 'vendBankGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getVendBankToList(search: SearchParameter): Observable<SearchResult<VendBankListView>> {
    const url = `${this.servicePath}/GetVendBankList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteVendBank(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteVendBank`;
    return this.dataGateway.delete(url, row);
  }
  getVendBankById(id: string): Observable<VendBankItemView> {
    const url = `${this.servicePath}/GetVendBankById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createVendBank(vmModel: VendBankItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateVendBank`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateVendBank(vmModel: VendBankItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateVendBank`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
