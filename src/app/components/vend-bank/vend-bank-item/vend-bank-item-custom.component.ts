import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { VendBankItemView } from 'shared/models/viewModel';

const firstGroup = ['VENDOR_ID'];
export class VendBankItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: VendBankItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: VendBankItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getInitialData(parentId: string): Observable<VendBankItemView> {
    let model: VendBankItemView = new VendBankItemView();
    model.vendBankGUID = EmptyGuid;
    model.vendorTableGUID = parentId;
    return of(model);
  }
}
