import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VendBankService } from './vend-bank.service';
import { VendBankListComponent } from './vend-bank-list/vend-bank-list.component';
import { VendBankItemComponent } from './vend-bank-item/vend-bank-item.component';
import { VendBankItemCustomComponent } from './vend-bank-item/vend-bank-item-custom.component';
import { VendBankListCustomComponent } from './vend-bank-list/vend-bank-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [VendBankListComponent, VendBankItemComponent],
  providers: [VendBankService, VendBankItemCustomComponent, VendBankListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [VendBankListComponent, VendBankItemComponent]
})
export class VendBankComponentModule {}
