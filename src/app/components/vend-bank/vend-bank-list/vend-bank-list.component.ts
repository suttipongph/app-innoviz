import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { BANK_GROUP, ColumnType, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { VendBankListView } from 'shared/models/viewModel';
import { VendBankService } from '../vend-bank.service';
import { VendBankListCustomComponent } from './vend-bank-list-custom.component';
import { UIControllerService } from 'core/services/uiController.service';

@Component({
  selector: 'vend-bank-list',
  templateUrl: './vend-bank-list.component.html',
  styleUrls: ['./vend-bank-list.component.scss']
})
export class VendBankListComponent extends BaseListComponent<VendBankListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  bankGroupOptions: SelectItems[] = [];
  defualtBitOption: SelectItems[] = [];
  constructor(private service: VendBankService, public custom: VendBankListCustomComponent, private uiControllerService: UIControllerService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getDefaultBitDropDown(this.defualtBitOption);
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBankGroupDropDown(this.bankGroupOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'vendBankGUID';
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.BANK_ACCOUNT',
        textKey: 'bankAccount',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BANK_ACCOUNT_NAME',
        textKey: 'bankAccountName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.FINANCIAL_INSTITUTION_ID',
        textKey: BANK_GROUP.FK,
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.FINANCIAL_INSTITUTION_NAME',
        textKey: 'bankGroup_Description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.BANK_BRANCH',
        textKey: 'bankBranch',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PRIMARY',
        textKey: 'primary',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defualtBitOption
      },
      {
        label: null,
        textKey: 'vendorTableGUID',
        type: ColumnType.MASTER,
        visibility: false,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<VendBankListView>> {
    return this.service.getVendBankToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteVendBank(row));
  }
}
