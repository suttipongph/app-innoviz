import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { StagingTableItemView, StagingTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class StagingTableService {
  serviceKey = 'stagingTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getStagingTableToList(search: SearchParameter): Observable<SearchResult<StagingTableListView>> {
    const url = `${this.servicePath}/GetStagingTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteStagingTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteStagingTable`;
    return this.dataGateway.delete(url, row);
  }
  getStagingTableById(id: string): Observable<StagingTableItemView> {
    const url = `${this.servicePath}/GetStagingTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createStagingTable(vmModel: StagingTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateStagingTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateStagingTable(vmModel: StagingTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateStagingTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
