import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { StagingTableItemView } from 'shared/models/viewModel';

const firstGroup = [
  'STAGING_BATCH_ID',
  'REF_TYPE',
  'REF_GUID',
  'REF_ID',
  'TRANS_DATE',
  'PRODUCT_TYPE',
  'PROCESS_TRANS_TYPE',
  'ACCOUNT_TYPE',
  'ACCOUNT_NUM',
  'TRANS_TEXT',
  'AMOUNT_MST',
  'DIMENSION_CODE1',
  'DIMENSION_CODE2',
  'DIMENSION_CODE3',
  'DIMENSION_CODE4',
  'DIMENSION_CODE5',
  'COMPANY_TAX_BRANCH_ID',
  'TAX_INVOICE_ID',
  'TAX_CODE',
  'TAX_BASE_AMOUNT',
  'TAX_ID',
  'TAX_ACCOUNT_ID',
  'TAX_ACCOUNT_NAME',
  'TAX_BRANCH_ID',
  'TAX_ADDRESS',
  'VENDOR_TAX_INVOICE_ID',
  'WHT_CODE',
  'PROCESS_TRANS_GUID',
  'INTERFACE_STAGING_BATCH_ID',
  'INTERFACE_STATUS',
  'STAGING_TABLE_COMPANY_ID',
  'INVOICE_TABLE_GUID'
];

export class StagingTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: StagingTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: StagingTableItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(): Observable<StagingTableItemView> {
    let model = new StagingTableItemView();
    model.stagingTableGUID = EmptyGuid;
    return of(model);
  }
}
