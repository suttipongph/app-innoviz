import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StagingTableItemComponent } from './staging-table-item.component';

describe('StagingTableItemViewComponent', () => {
  let component: StagingTableItemComponent;
  let fixture: ComponentFixture<StagingTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StagingTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StagingTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
