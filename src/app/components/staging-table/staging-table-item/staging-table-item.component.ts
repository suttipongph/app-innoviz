import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { StagingTableItemView } from 'shared/models/viewModel';
import { StagingTableService } from '../staging-table.service';
import { StagingTableItemCustomComponent } from './staging-table-item-custom.component';
@Component({
  selector: 'staging-table-item',
  templateUrl: './staging-table-item.component.html',
  styleUrls: ['./staging-table-item.component.scss']
})
export class StagingTableItemComponent extends BaseItemComponent<StagingTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  accountTypeOptions: SelectItems[] = [];
  interfaceStatusOptions: SelectItems[] = [];
  processTransTypeOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  sourceRefTypeOptions: SelectItems[] = [];

  constructor(private service: StagingTableService, private currentActivatedRoute: ActivatedRoute, public custom: StagingTableItemCustomComponent) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getAccountTypeEnumDropDown(this.accountTypeOptions);
    this.baseDropdown.getInterfaceStatusEnumDropDown(this.interfaceStatusOptions);
    this.baseDropdown.getProcessTransTypeEnumDropDown(this.processTransTypeOptions);
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<StagingTableItemView> {
    return this.service.getStagingTableById(this.id);
  }
  getInitialData(): Observable<StagingTableItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions(result);
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  setRelatedInfoOptions(model: StagingTableItemView): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.VENDOR_INFO',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.STAGING_TABLE_VENDOR_INFO,
            parameters: {
              processTransGUID: model.processTransGUID != null ? model.processTransGUID : EmptyGuid
            }
          })
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateStagingTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createStagingTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
