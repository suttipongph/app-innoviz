import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StagingTableService } from './staging-table.service';
import { StagingTableListComponent } from './staging-table-list/staging-table-list.component';
import { StagingTableItemComponent } from './staging-table-item/staging-table-item.component';
import { StagingTableItemCustomComponent } from './staging-table-item/staging-table-item-custom.component';
import { StagingTableListCustomComponent } from './staging-table-list/staging-table-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [StagingTableListComponent, StagingTableItemComponent],
  providers: [StagingTableService, StagingTableItemCustomComponent, StagingTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [StagingTableListComponent, StagingTableItemComponent]
})
export class StagingTableComponentModule {}
