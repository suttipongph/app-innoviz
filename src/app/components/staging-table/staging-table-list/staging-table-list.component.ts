import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { StagingTableListView } from 'shared/models/viewModel';

import { StagingTableService } from '../staging-table.service';
import { StagingTableListCustomComponent } from './staging-table-list-custom.component';

@Component({
  selector: 'staging-table-list',
  templateUrl: './staging-table-list.component.html',
  styleUrls: ['./staging-table-list.component.scss']
})
export class StagingTableListComponent extends BaseListComponent<StagingTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  accountTypeOptions: SelectItems[] = [];
  interfaceStatusOptions: SelectItems[] = [];
  processTransTypeOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  sourceRefTypeOptions: SelectItems[] = [];
  processTransOptions: SelectItems[] = [];
  invoiceTableOptions: SelectItems[] = [];

  constructor(public custom: StagingTableListCustomComponent, private service: StagingTableService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getAccountTypeEnumDropDown(this.accountTypeOptions);
    this.baseDropdown.getInterfaceStatusEnumDropDown(this.interfaceStatusOptions);
    this.baseDropdown.getProcessTransTypeEnumDropDown(this.processTransTypeOptions);
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getProcessTransDropDown(this.processTransOptions);
    this.baseDropdown.getInvoiceTableDropDown(this.invoiceTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'stagingTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.STAGING_BATCH_ID',
        textKey: 'stagingBatchId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.REFERENCE_TYPE',
        textKey: 'refType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.refTypeOptions
      },
      {
        label: 'LABEL.REFERENCE_ID',
        textKey: 'refId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.TRANSACTION_DATE',
        textKey: 'transDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.TRANSACTION_TYPE',
        textKey: 'processTransType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.DESC,
        masterList: this.processTransTypeOptions
      },

      {
        label: 'LABEL.INVOICE_ID',
        textKey: 'invoiceTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.invoiceTableOptions,
        sortingKey: 'invoiceTable_invoiceId',
        searchingKey: 'invoiceTableGUID'
      },
      {
        label: 'LABEL.ACCOUNT_TYPE',
        textKey: 'accountType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.accountTypeOptions
      },
      {
        label: 'LABEL.ACCOUNT_NUMBER',
        textKey: 'accountNum',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.AMOUNT_MST',
        textKey: 'amountMST',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PROCESS_TRANSACTION_ID',
        textKey: 'processTrans_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.processTransOptions,
        sortingKey: 'processTrans_ProcessTransId',
        searchingKey: 'processTransGUID'
      },
      {
        label: 'LABEL.INTERFACE_STAGING_BATCH_ID',
        textKey: 'interfaceStagingBatchId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.INTERFACE_STATUS',
        textKey: 'interfaceStatus',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.interfaceStatusOptions
      },
      {
        label: 'LABEL.SOURCE_REFERENCE_TYPE',
        textKey: 'sourceRefType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.refTypeOptions
      },
      {
        label: 'LABEL.SOURCE_REFERENCE_ID',
        textKey: 'sourceRefId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<StagingTableListView>> {
    return this.service.getStagingTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteStagingTable(row));
  }
}
