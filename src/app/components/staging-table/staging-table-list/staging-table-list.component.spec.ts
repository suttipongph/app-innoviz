import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StagingTableListComponent } from './staging-table-list.component';

describe('StagingTableListViewComponent', () => {
  let component: StagingTableListComponent;
  let fixture: ComponentFixture<StagingTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StagingTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StagingTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
