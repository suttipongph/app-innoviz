import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { AccessModeView, CreditAppRequestTableItemView, CreditAppRequestTableListView } from 'shared/models/viewModel';
@Injectable()
export class ClosingCreditLimitRequestService {
  serviceKey = 'creditAppRequestTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getClosingCreditLimitRequestToList(search: SearchParameter): Observable<SearchResult<CreditAppRequestTableListView>> {
    const url = `${this.servicePath}/GetClosingCreditLimitRequestList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteClosingCreditLimitRequest(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteClosingCreditLimitRequest`;
    return this.dataGateway.delete(url, row);
  }
  getClosingCreditLimitRequestById(id: string): Observable<CreditAppRequestTableItemView> {
    const url = `${this.servicePath}/GetClosingCreditLimitRequestById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createClosingCreditLimitRequest(vmModel: CreditAppRequestTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateClosingCreditLimitRequest`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateClosingCreditLimitRequest(vmModel: CreditAppRequestTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateClosingCreditLimitRequest`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(CreditAppId: string): Observable<CreditAppRequestTableItemView> {
    const url = `${this.servicePath}/GetCloseCreditLimitCreditAppRequestTableInitialData/CreditAppId=${CreditAppId}`;
    return this.dataGateway.get(url);
  }
  validateIsManualNumberSeq(productType: number): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeq/productType=${productType}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByCreditAppTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppTable/id=${id}`;
    return this.dataGateway.get(url);
  }
}
