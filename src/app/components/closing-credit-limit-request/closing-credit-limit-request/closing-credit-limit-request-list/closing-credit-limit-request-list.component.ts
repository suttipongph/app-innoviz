import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, CreditAppRequestType, RefType, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CreditAppRequestTableListView } from 'shared/models/viewModel';
import { ClosingCreditLimitRequestService } from '../closing-credit-limit-request.service';
import { ClosingCreditLimitRequestListCustomComponent } from './closing-credit-limit-request-list-custom.component';

@Component({
  selector: 'closing-credit-limit-request-list',
  templateUrl: './closing-credit-limit-request-list.component.html',
  styleUrls: ['./closing-credit-limit-request-list.component.scss']
})
export class ClosingCreditLimitRequestListComponent extends BaseListComponent<CreditAppRequestTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  customerTableOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  creditAppTableOptions: SelectItems[] = [];
  constructor(public custom: ClosingCreditLimitRequestListCustomComponent, private service: ClosingCreditLimitRequestService) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();

    this.custom.setAccessModeByParentStatus(this.parentId).subscribe((result) => {
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {}

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
    this.baseDropdown.getCreditAppTableDropDown(this.creditAppTableOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'creditAppRequestTableGUID';
    const columns: ColumnModel[] = [
      {
        label: this.translate.instant('LABEL.CREDIT_APPLICATION_REQUEST_ID'),
        textKey: 'creditAppRequestId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: this.translate.instant('LABEL.DESCRIPTION'),
        textKey: 'creditAppRequestId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: this.translate.instant('LABEL.REQUEST_DATE'),
        textKey: 'requestDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: this.translate.instant('LABEL.STATUS'),
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'documentStatusGUID',
        sortingKey: 'documentStatus_StatusId',
        masterList: this.documentStatusOptions
      },
      {
        label: this.translate.instant('LABEL.CUSTOMER_ID'),
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId'
      },
      {
        label: this.translate.instant('LABEL.REFERENCE_CREDIT_APPLICATION_ID'),
        textKey: 'refCreditAppTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditAppTableOptions,
        searchingKey: 'refCreditAppTableGUID',
        sortingKey: 'refCreditAppTable_CreditAppId'
      },
      {
        label: null,
        textKey: 'creditAppRequestType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: CreditAppRequestType.CloseCustomerCreditLimit.toString()
      },
      {
        label: null,
        textKey: 'refCreditAppTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CreditAppRequestTableListView>> {
    return this.service.getClosingCreditLimitRequestToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteClosingCreditLimitRequest(row));
  }
  getRowAuthorize(row: CreditAppRequestTableListView[]): void {
    if (this.custom.getRowAuthorize(row)) {
      row.forEach((item) => {
        item.rowAuthorize = this.getSingleRowAuthorize(item.rowAuthorize, item);
      });
    } else {
      super.getRowAuthorize(row);
    }
  }
}
