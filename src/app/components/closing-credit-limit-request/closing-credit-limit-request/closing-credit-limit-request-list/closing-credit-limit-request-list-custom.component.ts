import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AccessMode, CreditAppRequestStatus } from 'shared/constants';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView, CreditAppRequestTableListView } from 'shared/models/viewModel';
import { ClosingCreditLimitRequestService } from '../closing-credit-limit-request.service';

export class ClosingCreditLimitRequestListCustomComponent {
  baseService: BaseServiceModel<any>;
  accessModeView: AccessModeView = new AccessModeView();

  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setAccessModeByParentStatus(parentId: string): Observable<AccessModeView> {
    return (this.baseService.service as ClosingCreditLimitRequestService).getAccessModeByCreditAppTable(parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
        this.accessModeView.canView = true;
      })
    );
  }

  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
  getRowAuthorize(row: CreditAppRequestTableListView[]): boolean {
    let isSetRow: boolean = true;
    row.forEach((item) => {
      const accessMode: AccessMode = item.documentStatus_StatusId === CreditAppRequestStatus.Draft ? AccessMode.full : AccessMode.creator;
      item.rowAuthorize = accessMode;
    });
    return isSetRow;
  }
}
