import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ClosingCreditLimitRequestListComponent } from './closing-credit-limit-request-list.component';

describe('ClosingCreditLimitRequestListViewComponent', () => {
  let component: ClosingCreditLimitRequestListComponent;
  let fixture: ComponentFixture<ClosingCreditLimitRequestListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ClosingCreditLimitRequestListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClosingCreditLimitRequestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
