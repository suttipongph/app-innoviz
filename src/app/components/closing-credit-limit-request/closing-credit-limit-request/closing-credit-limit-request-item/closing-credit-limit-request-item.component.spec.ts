import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClosingCreditLimitRequestItemComponent } from './closing-credit-limit-request-item.component';

describe('ClosingCreditLimitRequestItemViewComponent', () => {
  let component: ClosingCreditLimitRequestItemComponent;
  let fixture: ComponentFixture<ClosingCreditLimitRequestItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ClosingCreditLimitRequestItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClosingCreditLimitRequestItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
