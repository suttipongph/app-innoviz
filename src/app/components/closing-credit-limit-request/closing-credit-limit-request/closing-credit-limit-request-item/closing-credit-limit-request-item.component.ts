import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { RefType, ROUTE_FUNCTION_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { CreditAppRequestTableItemView, CreditAppRequestTableListView, RefIdParm } from 'shared/models/viewModel';
import { PrintSetBookDocTransView } from 'shared/models/viewModel/printSetBookDocTransView';
import { ClosingCreditLimitRequestService } from '../closing-credit-limit-request.service';
import { ClosingCreditLimitRequestItemCustomComponent } from './closing-credit-limit-request-item-custom.component';
@Component({
  selector: 'closing-credit-limit-request-item',
  templateUrl: './closing-credit-limit-request-item.component.html',
  styleUrls: ['./closing-credit-limit-request-item.component.scss']
})
export class ClosingCreditLimitRequestItemComponent extends BaseItemComponent<CreditAppRequestTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  creditAppRequestTypeOptions: SelectItems[] = [];
  kycguidOptions: SelectItems[] = [];
  creditScoringOptions: SelectItems[] = [];

  constructor(
    private service: ClosingCreditLimitRequestService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: ClosingCreditLimitRequestItemCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }

  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getCreditAppRequestTypeEnumDropDown(this.creditAppRequestTypeOptions);
  }
  getById(): Observable<CreditAppRequestTableItemView> {
    return this.service.getClosingCreditLimitRequestById(this.id);
  }
  getInitialData(): Observable<CreditAppRequestTableItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getKYCSetupDropDown(), this.baseDropdown.getCreditScoringDropDown()).subscribe(
      ([kyc, creditScroing]) => {
        this.kycguidOptions = kyc;
        this.creditScoringOptions = creditScroing;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateClosingCreditLimitRequest(this.model), isColsing);
    } else {
      super.onCreate(this.service.createClosingCreditLimitRequest(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  setRelatedInfoOptions(): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        command: () => {
          const refIdParm: RefIdParm = { refType: RefType.CreditAppRequestTable, refGUID: this.model.creditAppRequestTableGUID };
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: refIdParm
          });
        }
      },
      {
        label: 'LABEL.MEMO',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.MEMO })
      },
      {
        label: 'LABEL.BOOKMARK_DOCUMENT',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.BOOKMARK_DOCUMENT_TRANS,
            parameters: {
              refGUID: this.model.creditAppRequestTableGUID,
              refType: RefType.CreditAppRequestTable
            }
          })
      },
      {
        label: 'LABEL.INQUIRY',
        items: [
          {
            label: 'LABEL.ASSIGNMENT_AGREEMENT_OUTSTANDING',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.ASSIGNMENT_AGREEMENT_OUTSTANDING,
                parameters: { creditAppRequestTableGUID: this.model.creditAppRequestTableGUID }
              })
          },
          {
            label: 'LABEL.CA_BUYER_CREDIT_OUTSTANDING',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.CA_BUYER_CREDIT_OUTSTANDING,
                parameters: { creditAppRequestTableGUID: this.model.creditAppRequestTableGUID }
              })
          },
          {
            label: 'LABEL.CREDIT_OUTSTANDING',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.CREDIT_OUTSTANDING,
                parameters: { creditAppRequestTableGUID: this.model.creditAppRequestTableGUID }
              })
          }
        ]
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(model: CreditAppRequestTableItemView): void {
    this.functionItems = [
      {
        label: 'LABEL.CANCEL_CREDIT_APPLICATION_REQUEST',
        disabled: !this.custom.statusIsDraft(model),
        command: () =>
          this.toFunction({
            path: ROUTE_FUNCTION_GEN.CANCEL_CREDIT_APPLICATION_REQUEST,
            parameters: { creditAppRequestGUID: model.creditAppRequestTableGUID }
          })
      },
      {
        label: 'LABEL.UPDATE_CLOSING_RESULT',
        disabled: !this.custom.statusIsDraft(model),
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.UPDATE_CLOSING_RESULT })
      },
      {
        label: 'LABEL.PRINT',
        items: [
          {
            label: 'LABEL.BOOKMARK_DOCUMENT',
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.PRINT_SET_BOOKMARK_DOCUMENT_TRANSACTION,
                parameters: {
                  refType: RefType.CreditAppRequestTable,
                  refGuid: this.id,
                  model: this.setPrintSetBookDocTransData()
                }
              })
          }
        ]
      }
    ];
    super.setFunctionOptionsMapping();
  }
  setPrintSetBookDocTransData(): PrintSetBookDocTransView {
    return this.custom.setPrintSetBookDocTransData(this.model);
  }
}
