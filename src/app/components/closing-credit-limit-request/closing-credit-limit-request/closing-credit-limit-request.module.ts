import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClosingCreditLimitRequestService } from './closing-credit-limit-request.service';
import { ClosingCreditLimitRequestListComponent } from './closing-credit-limit-request-list/closing-credit-limit-request-list.component';
import { ClosingCreditLimitRequestItemComponent } from './closing-credit-limit-request-item/closing-credit-limit-request-item.component';
import { ClosingCreditLimitRequestItemCustomComponent } from './closing-credit-limit-request-item/closing-credit-limit-request-item-custom.component';
import { ClosingCreditLimitRequestListCustomComponent } from './closing-credit-limit-request-list/closing-credit-limit-request-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ClosingCreditLimitRequestListComponent, ClosingCreditLimitRequestItemComponent],
  providers: [ClosingCreditLimitRequestService, ClosingCreditLimitRequestItemCustomComponent, ClosingCreditLimitRequestListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ClosingCreditLimitRequestListComponent, ClosingCreditLimitRequestItemComponent]
})
export class ClosingCreditLimitRequestComponentModule {}
