import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateClosingResultService } from './update-closing-result.service';
import { UpdateClosingResultCustomComponent } from './update-closing-result/update-closing-result-custom.component';
import { UpdateClosingResultComponent } from './update-closing-result/update-closing-result.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [UpdateClosingResultComponent],
  providers: [UpdateClosingResultService, UpdateClosingResultCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [UpdateClosingResultComponent]
})
export class UpdateClosingResultComponentModule {}
