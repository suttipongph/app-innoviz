import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateClosingResultComponent } from './update-closing-result.component';

describe('UpdateClosingResultViewComponent', () => {
  let component: UpdateClosingResultComponent;
  let fixture: ComponentFixture<UpdateClosingResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UpdateClosingResultComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateClosingResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
