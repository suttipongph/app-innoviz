import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { DocumentReasonItemView } from 'shared/models/viewModel';
import { UpdateClosingResultView } from 'shared/models/viewModel/updateClosingResultView';

const firstGroup = ['CREDIT_APP_REQUEST_ID'];

export class UpdateClosingResultCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: UpdateClosingResultView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: UpdateClosingResultView): Observable<boolean> {
    return of(false);
  }
  getInitialData(): Observable<UpdateClosingResultView> {
    let model = new UpdateClosingResultView();
    model.creditAppRequestTableGUID = EmptyGuid;
    return of(model);
  }
}
