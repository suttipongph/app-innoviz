import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { UpdateClosingResultView } from 'shared/models/viewModel/updateClosingResultView';
@Injectable()
export class UpdateClosingResultService {
  serviceKey = 'updateClosingResultGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }

  getUpdateClosingResultById(id: string): Observable<UpdateClosingResultView> {
    const url = `${this.servicePath}/GetUpdateClosingResultById/id=${id}`;
    return this.dataGateway.get(url);
  }
  updateClosingResult(vmModel: UpdateClosingResultView): Observable<UpdateClosingResultView> {
    const url = `${this.servicePath}/UpdateClosingResult`;
    return this.dataGateway.post(url, vmModel);
  }
}
