import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinancialCreditTransItemComponent } from './financial-credit-trans-item.component';

describe('FinancialCreditTransItemViewComponent', () => {
  let component: FinancialCreditTransItemComponent;
  let fixture: ComponentFixture<FinancialCreditTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FinancialCreditTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialCreditTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
