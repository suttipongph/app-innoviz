import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { FinancialCreditTransListView } from 'shared/models/viewModel';
import { FinancialCreditTransService } from '../financial-credit-trans.service';
import { FinancialCreditTransListCustomComponent } from './financial-credit-trans-list-custom.component';
@Component({
  selector: 'financial-credit-trans-list',
  templateUrl: './financial-credit-trans-list.component.html',
  styleUrls: ['./financial-credit-trans-list.component.scss']
})
export class FinancialCreditTransListComponent extends BaseListComponent<FinancialCreditTransListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  bankGroupOptions: SelectItems[] = [];
  creditTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];

  constructor(
    public custom: FinancialCreditTransListCustomComponent,
    private service: FinancialCreditTransService,
    private currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoOriginTableName();
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setRefTypeRefGUID(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.setAccessModeByParentStatus(this.isWorkFlowMode()).subscribe((result) => {
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBankGroupDropDown(this.bankGroupOptions);
    this.baseDropdown.getCreditTypeDropDown(this.creditTypeOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'financialCreditTransGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.FINANCIAL_INSTITUTION_ID',
        textKey: 'bankGroup_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.bankGroupOptions,
        sortingKey: 'bankGroup_BankGroupId',
        searchingKey: 'bankGroupGUID'
      },
      {
        label: 'LABEL.CREDIT_TYPE_ID',
        textKey: 'creditType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditTypeOptions,
        sortingKey: 'creditType_CreditTypeId',
        searchingKey: 'creditTypeGUID'
      },
      {
        label: 'LABEL.AMOUNT',
        textKey: 'amount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: isNullOrUndefined(this.custom.refGUID) ? this.parentId : this.custom.refGUID
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<FinancialCreditTransListView>> {
    return this.service.getFinancialCreditTransToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteFinancialCreditTrans(row));
  }
}
