import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FinancialCreditTransListComponent } from './financial-credit-trans-list.component';

describe('FinancialCreditTransListViewComponent', () => {
  let component: FinancialCreditTransListComponent;
  let fixture: ComponentFixture<FinancialCreditTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FinancialCreditTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialCreditTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
