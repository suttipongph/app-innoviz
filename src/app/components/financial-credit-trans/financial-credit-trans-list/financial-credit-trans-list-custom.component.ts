import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { RefType } from 'shared/constants';
import { BaseServiceModel, RefTypeModel } from 'shared/models/systemModel';
import { CreditAppRequestStatus } from 'shared/constants';
import { AccessModeView } from 'shared/models/viewModel';
import { FinancialCreditTransService } from '../financial-credit-trans.service';
import { isNullOrUndefined } from 'shared/functions/value.function';
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
export class FinancialCreditTransListCustomComponent {
  refGUID: string;
  refType: RefType;
  baseService: BaseServiceModel<FinancialCreditTransService>;
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setAccessModeByParentStatus(isWorkFlowMode: boolean): Observable<AccessModeView> {
    let parentId = this.baseService.uiService.getRelatedInfoOriginTableKey();
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        return this.baseService.service.getAccessModeByCreditAppRequestTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
            this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
            this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
            this.accessModeView.canView = true;
          })
        );
      default:
        if (this.refType === RefType.CreditAppRequestTable) {
          return this.baseService.service.getAccessModeByCreditAppRequestTable(this.refGUID).pipe(
            tap((result) => {
              this.accessModeView = result as AccessModeView;
              const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
              this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
              this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
              this.accessModeView.canView = true;
            })
          );
        } else {
          return of(this.accessModeView);
        }
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
  setRefTypeRefGUID(passingObj: RefTypeModel): void {
    if (!isNullOrUndefined(passingObj)) {
      this.refType = passingObj.refType;
      this.refGUID = passingObj.refGUID;
    }
  }
}
