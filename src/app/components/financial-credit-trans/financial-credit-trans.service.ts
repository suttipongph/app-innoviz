import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, FinancialCreditTransItemView, FinancialCreditTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class FinancialCreditTransService {
  serviceKey = 'financialCreditTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getFinancialCreditTransToList(search: SearchParameter): Observable<SearchResult<FinancialCreditTransListView>> {
    const url = `${this.servicePath}/GetFinancialCreditTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteFinancialCreditTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteFinancialCreditTrans`;
    return this.dataGateway.delete(url, row);
  }
  getFinancialCreditTransById(id: string): Observable<FinancialCreditTransItemView> {
    const url = `${this.servicePath}/GetFinancialCreditTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createFinancialCreditTrans(vmModel: FinancialCreditTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateFinancialCreditTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateFinancialCreditTrans(vmModel: FinancialCreditTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateFinancialCreditTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getAccessModeByCreditAppRequestTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppRequestTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getInitialData(id: string): Observable<FinancialCreditTransItemView> {
    const url = `${this.servicePath}/GetFinancialCreditTransInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
}
