import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FinancialCreditTransService } from './financial-credit-trans.service';
import { FinancialCreditTransListComponent } from './financial-credit-trans-list/financial-credit-trans-list.component';
import { FinancialCreditTransItemComponent } from './financial-credit-trans-item/financial-credit-trans-item.component';
import { FinancialCreditTransItemCustomComponent } from './financial-credit-trans-item/financial-credit-trans-item-custom.component';
import { FinancialCreditTransListCustomComponent } from './financial-credit-trans-list/financial-credit-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [FinancialCreditTransListComponent, FinancialCreditTransItemComponent],
  providers: [FinancialCreditTransService, FinancialCreditTransItemCustomComponent, FinancialCreditTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [FinancialCreditTransListComponent, FinancialCreditTransItemComponent]
})
export class FinancialCreditTransComponentModule {}
