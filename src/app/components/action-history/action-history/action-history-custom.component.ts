import { Observable, of } from 'rxjs';
import { isUndefinedOrZeroLength } from 'shared/functions/value.function';
import { FieldAccessing } from 'shared/models/systemModel';
import { ActionHistoryListView } from 'shared/models/viewModel';

const firstGroup = [];
export class ActionHistoryCustomComponent {
  getFieldAccessing(model: ActionHistoryListView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ActionHistoryListView): Observable<boolean> {
    return of(!canCreate);
  }
  getDisplayContent(actionHists: ActionHistoryListView[]): ActionHistoryListView[] {
    if (isUndefinedOrZeroLength(actionHists)) {
      actionHists = [];
    }
    for (let i = 0; i < actionHists.length; i++) {
      let displayMsg: string = '';
      const actionHistory = actionHists[i];
      const activityName = isUndefinedOrZeroLength(actionHistory.activityName) ? '' : actionHistory.activityName;
      const actionName = isUndefinedOrZeroLength(actionHistory.actionName) ? '' : actionHistory.actionName;
      const comment = isUndefinedOrZeroLength(actionHistory.comment) ? '' : actionHistory.comment;
      displayMsg = displayMsg.concat(activityName, '\n', actionName, ':', comment);
      actionHists[i].displayContent = displayMsg;
    }
    return actionHists;
  }
}
