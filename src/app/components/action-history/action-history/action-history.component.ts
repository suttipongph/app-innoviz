import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { Observable } from 'rxjs';
import { WORKFLOW } from 'shared/constants';
import { isUpdateMode, isUndefinedOrZeroLength, isNullOrUndefined } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel } from 'shared/models/systemModel';
import { ActionHistoryListView } from 'shared/models/viewModel/actionHistoryView';
import { ActionHistoryCustomComponent } from './action-history-custom.component';

@Component({
  selector: 'action-history',
  templateUrl: './action-history.component.html',
  styleUrls: ['./action-history.component.scss']
})
export class ActionHistoryComponent extends BaseItemComponent<ActionHistoryListView[]> {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
  }
  isUndefinedOrZeroLength = isUndefinedOrZeroLength;
  constructor(private currentActivatedRoute: ActivatedRoute, public custom: ActionHistoryCustomComponent) {
    super();
    this.model = [];
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    if (!isUndefinedOrZeroLength(passingObj) && !isUndefinedOrZeroLength(passingObj[WORKFLOW.ACTIONHISTORY_REFGUID])) {
      this.id = passingObj[WORKFLOW.ACTIONHISTORY_REFGUID];
    }
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<ActionHistoryListView[]> {
    return this.k2Service.getActionHistoryByRefGUID(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {}
  onAsyncRunner(model?: any): void {
    this.model = this.custom.getDisplayContent(model);
  }
  setFieldAccessing(): void {}
  setRelatedInfoOptions(): void {}
  setFunctionOptions(): void {}
  onSave(validation: FormValidationModel): void {}
  onSaveAndClose(validation: FormValidationModel): void {}
  onSubmit(isColsing: boolean): void {}
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onClose(): void {
    super.onBack();
  }
}
