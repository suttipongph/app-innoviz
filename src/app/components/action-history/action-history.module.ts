import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActionHistoryComponent } from './action-history/action-history.component';
import { ActionHistoryCustomComponent } from './action-history/action-history-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ActionHistoryComponent],
  providers: [ActionHistoryCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ActionHistoryComponent]
})
export class ActionHistoryComponentModule {}
