import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IntercompanyInvoiceAdjustmentService } from './intercompany-invoice-adjustment.service';
import { IntercompanyInvoiceAdjustmentListComponent } from './intercompany-invoice-adjustment-list/intercompany-invoice-adjustment-list.component';
import { IntercompanyInvoiceAdjustmentItemComponent } from './intercompany-invoice-adjustment-item/intercompany-invoice-adjustment-item.component';
import { IntercompanyInvoiceAdjustmentItemCustomComponent } from './intercompany-invoice-adjustment-item/intercompany-invoice-adjustment-item-custom.component';
import { IntercompanyInvoiceAdjustmentListCustomComponent } from './intercompany-invoice-adjustment-list/intercompany-invoice-adjustment-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [IntercompanyInvoiceAdjustmentListComponent, IntercompanyInvoiceAdjustmentItemComponent],
  providers: [
    IntercompanyInvoiceAdjustmentService,
    IntercompanyInvoiceAdjustmentItemCustomComponent,
    IntercompanyInvoiceAdjustmentListCustomComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [IntercompanyInvoiceAdjustmentListComponent, IntercompanyInvoiceAdjustmentItemComponent]
})
export class IntercompanyInvoiceAdjustmentComponentModule {}
