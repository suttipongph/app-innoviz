import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, RefType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { IntercompanyInvoiceAdjustmentListView } from 'shared/models/viewModel';
import { IntercompanyInvoiceAdjustmentService } from '../intercompany-invoice-adjustment.service';
import { IntercompanyInvoiceAdjustmentListCustomComponent } from './intercompany-invoice-adjustment-list-custom.component';

@Component({
  selector: 'intercompany-invoice-adjustment-list',
  templateUrl: './intercompany-invoice-adjustment-list.component.html',
  styleUrls: ['./intercompany-invoice-adjustment-list.component.scss']
})
export class IntercompanyInvoiceAdjustmentListComponent
  extends BaseListComponent<IntercompanyInvoiceAdjustmentListView>
  implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  constructor(public custom: IntercompanyInvoiceAdjustmentListCustomComponent, private service: IntercompanyInvoiceAdjustmentService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'intercompanyInvoiceAdjustmentGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.ORIGINAL_AMOUNT',
        textKey: 'originalAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.ADJUSTMENT',
        textKey: 'adjustment',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.REASON_ID',
        textKey: 'documentReason_Values',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'documentReasonGUID',
        sortingKey: 'documentReason_DocumentReasonId'
      },
      {
        label: null,
        textKey: 'createdDateTime',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<IntercompanyInvoiceAdjustmentListView>> {
    return this.service.getIntercompanyInvoiceAdjustmentToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteIntercompanyInvoiceAdjustment(row));
  }
}
