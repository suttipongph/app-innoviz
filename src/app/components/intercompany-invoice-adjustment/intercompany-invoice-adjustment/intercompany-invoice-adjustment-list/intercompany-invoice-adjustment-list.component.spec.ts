import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IntercompanyInvoiceAdjustmentListComponent } from './intercompany-invoice-adjustment-list.component';

describe('IntercompanyInvoiceAdjustmentListViewComponent', () => {
  let component: IntercompanyInvoiceAdjustmentListComponent;
  let fixture: ComponentFixture<IntercompanyInvoiceAdjustmentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IntercompanyInvoiceAdjustmentListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntercompanyInvoiceAdjustmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
