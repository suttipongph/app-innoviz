import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { IntercompanyInvoiceAdjustmentItemView, IntercompanyInvoiceAdjustmentListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class IntercompanyInvoiceAdjustmentService {
  serviceKey = 'intercompanyInvoiceAdjustmentGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getIntercompanyInvoiceAdjustmentToList(search: SearchParameter): Observable<SearchResult<IntercompanyInvoiceAdjustmentListView>> {
    const url = `${this.servicePath}/GetIntercompanyInvoiceAdjustmentList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteIntercompanyInvoiceAdjustment(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteIntercompanyInvoiceAdjustment`;
    return this.dataGateway.delete(url, row);
  }
  getIntercompanyInvoiceAdjustmentById(id: string): Observable<IntercompanyInvoiceAdjustmentItemView> {
    const url = `${this.servicePath}/GetIntercompanyInvoiceAdjustmentById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createIntercompanyInvoiceAdjustment(vmModel: IntercompanyInvoiceAdjustmentItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateIntercompanyInvoiceAdjustment`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateIntercompanyInvoiceAdjustment(vmModel: IntercompanyInvoiceAdjustmentItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateIntercompanyInvoiceAdjustment`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
