import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntercompanyInvoiceAdjustmentItemComponent } from './intercompany-invoice-adjustment-item.component';

describe('IntercompanyInvoiceAdjustmentItemViewComponent', () => {
  let component: IntercompanyInvoiceAdjustmentItemComponent;
  let fixture: ComponentFixture<IntercompanyInvoiceAdjustmentItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IntercompanyInvoiceAdjustmentItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntercompanyInvoiceAdjustmentItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
