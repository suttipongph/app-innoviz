import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { IntercompanyInvoiceAdjustmentItemView } from 'shared/models/viewModel';

const firstGroup = [
  'INTERCOMPANY_INVOICE_TABLE_GUID',
  'ORIGINAL_AMOUNT',
  'ADJUSTMENT',
  'DOCUMENT_REASON_GUID',
  'INTERCOMPANY_INVOICE_ADJUSTMENT_GUID'
];

export class IntercompanyInvoiceAdjustmentItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: IntercompanyInvoiceAdjustmentItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: IntercompanyInvoiceAdjustmentItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(): Observable<IntercompanyInvoiceAdjustmentItemView> {
    let model = new IntercompanyInvoiceAdjustmentItemView();
    model.intercompanyInvoiceAdjustmentGUID = EmptyGuid;
    return of(model);
  }
}
