import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CreditAppTransItemView, CreditAppTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CreditAppTransService {
  serviceKey = 'creditAppTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCreditAppTransToList(search: SearchParameter): Observable<SearchResult<CreditAppTransListView>> {
    const url = `${this.servicePath}/GetCreditAppTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCreditAppTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCreditAppTrans`;
    return this.dataGateway.delete(url, row);
  }
  getCreditAppTransById(id: string): Observable<CreditAppTransItemView> {
    const url = `${this.servicePath}/GetCreditAppTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCreditAppTrans(vmModel: CreditAppTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCreditAppTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCreditAppTrans(vmModel: CreditAppTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCreditAppTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
