import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreditAppTransService } from './credit-app-trans.service';
import { CreditAppTransListComponent } from './credit-app-trans-list/credit-app-trans-list.component';
import { CreditAppTransItemComponent } from './credit-app-trans-item/credit-app-trans-item.component';
import { CreditAppTransItemCustomComponent } from './credit-app-trans-item/credit-app-trans-item-custom.component';
import { CreditAppTransListCustomComponent } from './credit-app-trans-list/credit-app-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CreditAppTransListComponent, CreditAppTransItemComponent],
  providers: [CreditAppTransService, CreditAppTransItemCustomComponent, CreditAppTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CreditAppTransListComponent, CreditAppTransItemComponent]
})
export class CreditAppTransComponentModule {}
