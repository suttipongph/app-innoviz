import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CreditAppTransItemView } from 'shared/models/viewModel';

const firstGroup = [
  'CREDIT_APP_TABLE_GUID',
  'PRODUCT_TYPE',
  'CUSTOMER_TABLE_GUID',
  'BUYER_TABLE_GUID',
  'CREDIT_APP_LINE_GUID',
  'TRANS_DATE',
  'INVOICE_AMOUNT',
  'CREDIT_DEDUCT_AMOUNT',
  'REF_TYPE',
  'DOCUMENT_ID',
  'REF_GUID',
  'CREDIT_APP_TRANS_GUID'
];

export class CreditAppTransItemCustomComponent {
  baseService: BaseServiceModel<any>;
  parentName = null;
  getFieldAccessing(model: CreditAppTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CreditAppTransItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(): Observable<CreditAppTransItemView> {
    let model = new CreditAppTransItemView();
    model.creditAppTransGUID = EmptyGuid;
    return of(model);
  }
}
