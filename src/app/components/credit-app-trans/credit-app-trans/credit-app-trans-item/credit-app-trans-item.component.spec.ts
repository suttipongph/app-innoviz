import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditAppTransItemComponent } from './credit-app-trans-item.component';

describe('CreditAppTransItemViewComponent', () => {
  let component: CreditAppTransItemComponent;
  let fixture: ComponentFixture<CreditAppTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditAppTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
