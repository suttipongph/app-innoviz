import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CreditAppTransListView } from 'shared/models/viewModel';
import { CreditAppTransService } from '../credit-app-trans.service';
import { CreditAppTransListCustomComponent } from './credit-app-trans-list-custom.component';

@Component({
  selector: 'credit-app-trans-list',
  templateUrl: './credit-app-trans-list.component.html',
  styleUrls: ['./credit-app-trans-list.component.scss']
})
export class CreditAppTransListComponent extends BaseListComponent<CreditAppTransListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  productTypeOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  creditAppTableOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  creditAppLineOptions: SelectItems[] = [];
  constructor(
    public custom: CreditAppTransListCustomComponent,
    private service: CreditAppTransService,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCreditAppTableDropDown(this.creditAppTableOptions);
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
    this.baseDropdown.getCreditAppLineDropDown([this.parentId], this.creditAppLineOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'creditAppTransGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'creditAppTransGUID',
        type: ColumnType.STRING,
        visibility: false,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CREDIT_APPLICATION_ID',
        textKey: 'creditAppTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditAppTableOptions,
        searchingKey: 'creditAppTableGUID',
        sortingKey: 'creditAppTable_CreditAppId',
        disabledFilter: true
      },
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions,
        sortingKey: 'productType',
        searchingKey: 'productType',
        disabledFilter: true
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        sortingKey: 'customerTable_CustomerId',
        searchingKey: 'customerTableGUID',
        disabledFilter: true
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerTableOptions,
        sortingKey: 'buyerTable_BuyerId',
        searchingKey: 'buyerTableGUID'
      },
      {
        label: 'LABEL.CREDIT_APPLICATION_LINE_ID',
        textKey: 'creditAppLine_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditAppLineOptions,
        sortingKey: 'creditAppLine_LineNum',
        searchingKey: 'creditAppLineGUID'
      },
      {
        label: 'LABEL.TRANSACTION_DATE',
        textKey: 'transDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.ASC,
        sortingKey: 'transDate',
        searchingKey: 'transDate'
      },
      {
        label: 'LABEL.INVOICE_AMOUNT',
        textKey: 'invoiceAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        sortingKey: 'invoiceAmount',
        searchingKey: 'invoiceAmount'
      },
      {
        label: 'LABEL.CREDIT_DEDUCT_AMOUNT',
        textKey: 'creditDeductAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        sortingKey: 'creditDeductAmount',
        searchingKey: 'creditDeductAmount'
      },
      {
        label: 'LABEL.REFERENCE_TYPE',
        textKey: 'refType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.refTypeOptions,
        sortingKey: 'refType',
        searchingKey: 'refType'
      },
      {
        label: 'LABEL.DOCUMENT_ID',
        textKey: 'documentId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE,
        sortingKey: 'documentId',
        searchingKey: 'documentId'
      },
      {
        label: null,
        textKey: 'creditAppTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CreditAppTransListView>> {
    return this.service.getCreditAppTransToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCreditAppTrans(row));
  }
}
