import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditAppTransListComponent } from './credit-app-trans-list.component';

describe('CreditAppTransListViewComponent', () => {
  let component: CreditAppTransListComponent;
  let fixture: ComponentFixture<CreditAppTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditAppTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
