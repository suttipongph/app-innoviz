import { GuarantorAgreementLineAffliateItemPage } from './../../../pages/main-agreement-table/relatedinfo/guarantor-agreement/guarantor-agreement-line-affliate-item/guarantor-agreement-line-affliate-item.page';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GuarantorAgreementTableService } from './guarantor-agreement-table.service';
import { GuarantorAgreementTableItemComponent } from './guarantor-agreement-table-item/guarantor-agreement-table-item.component';
import { GuarantorAgreementTableItemCustomComponent } from './guarantor-agreement-table-item/guarantor-agreement-table-item-custom.component';
import { GuarantorAgreementTableListCustomComponent } from './guarantor-agreement-table-list/guarantor-agreement-table-list-custom.component';
import { GuarantorAgreementTableListComponent } from './guarantor-agreement-table-list/guarantor-agreement-table-list.component';
import { GuarantorAgreementLineComponentModule } from '../guarantor-agreement-line/guarantor-agreement-line.module';
import { GuarantorAgreementLineAffiliateComponentModule } from '../guarantor-agreement-line-affiliate/guarantor-agreement-line-affiliate.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    GuarantorAgreementLineComponentModule,
    GuarantorAgreementLineAffiliateComponentModule
  ],
  declarations: [GuarantorAgreementTableListComponent, GuarantorAgreementTableItemComponent],
  providers: [GuarantorAgreementTableService, GuarantorAgreementTableItemCustomComponent, GuarantorAgreementTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [GuarantorAgreementTableListComponent, GuarantorAgreementTableItemComponent]
})
export class GuarantorAgreementTableComponentModule {}
