import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, GuarantorAgreementTableItemView, GuarantorAgreementTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class GuarantorAgreementTableService {
  serviceKey = 'guarantorAgreementTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getGuarantorAgreementTableToList(search: SearchParameter): Observable<SearchResult<GuarantorAgreementTableListView>> {
    const url = `${this.servicePath}/GetGuarantorAgreementTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteGuarantorAgreementTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteGuarantorAgreementTable`;
    return this.dataGateway.delete(url, row);
  }
  getGuarantorAgreementTableById(id: string): Observable<GuarantorAgreementTableItemView> {
    const url = `${this.servicePath}/GetGuarantorAgreementTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createGuarantorAgreementTable(vmModel: GuarantorAgreementTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateGuarantorAgreementTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateGuarantorAgreementTable(vmModel: GuarantorAgreementTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateGuarantorAgreementTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<GuarantorAgreementTableItemView> {
    const url = `${this.servicePath}/GetGuarantorAgreementTableInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  validateIsManualNumberSeq(productType: number): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeq/productType=${productType}`;
    return this.dataGateway.get(url);
  }
  validateIsManualGuarantorNumberSeq(productType: number): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualGuarantorNumberSeq/productType=${productType}`;
    return this.dataGateway.get(url);
  }
  validateIsManualInternalGuarantorNumberSeq(productType: number): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualInternalNumberSeq/productType=${productType}`;
    return this.dataGateway.get(url);
  }
  getGenerateExtendGuarantorAgreementValidation(id: string): Observable<boolean> {
    const url = `${this.servicePath}/GetGenerateExtendGuarantorAgreementValidation/id=${id}`;
    return this.dataGateway.get(url);
  }
  getVisibleManageMenu(id: string): Observable<number[]> {
    const url = `${this.servicePath}/GetVisibleManageMenu/id=${id}`;
    return this.dataGateway.get(url);
  }
  getVisibleGenerateFunction(id: string): Observable<boolean> {
    const url = `${this.servicePath}/GetVisibleGenerateFunction/id=${id}`;
    return this.dataGateway.get(url);
  }
}
