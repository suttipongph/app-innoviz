import { UIControllerService } from './../../../../core/services/uiController.service';
import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, ROUTE_FUNCTION_GEN, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { GuarantorAgreementTableListView } from 'shared/models/viewModel';
import { GuarantorAgreementTableService } from '../guarantor-agreement-table.service';
import { GuarantorAgreementTableListCustomComponent } from './guarantor-agreement-table-list-custom.component';
import { ActivatedRoute } from '@angular/router';
import { isNullOrUndefined } from 'shared/functions/value.function';

@Component({
  selector: 'guarantor-agreement-table-list',
  templateUrl: './guarantor-agreement-table-list.component.html',
  styleUrls: ['./guarantor-agreement-table-list.component.scss']
})
export class GuarantorAgreementTableListComponent extends BaseListComponent<GuarantorAgreementTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  agreementDocTypeOptions: SelectItems[] = [];
  employeeTableOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  affiliateOptions: SelectItems[] = [];
  parentTabName;
  isExtend: boolean;

  constructor(
    public custom: GuarantorAgreementTableListCustomComponent,
    private service: GuarantorAgreementTableService,
    public uiControllerService: UIControllerService,
    private currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
    this.custom.service = this.service;
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setPassParameter(passingObj, this.parentId);
    this.isExtend = this.custom.checkExtend(passingObj);
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.setFunctionOptions();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getAgreementDocTypeEnumDropDown(this.agreementDocTypeOptions);
    this.baseDropdown.getDefaultBitDropDown(this.affiliateOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = true;
    this.option.canDelete = false;
    this.option.key = 'guarantorAgreementTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.INTERNAL_GUARANTOR_AGREEMENT_ID',
        textKey: 'internalGuarantorAgreementId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.GUARANTOR_AGREEMENT_ID',
        textKey: 'guarantorAgreementId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE,
        sortingKey: 'guarantorAgreementId',
        searchingKey: 'guarantorAgreementId'
      },
      {
        label: 'LABEL.AGREEMENT_DOCUMENT_TYPE',
        textKey: 'agreementDocType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.agreementDocTypeOptions
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.AGREEMENT_DATE',
        textKey: 'agreementDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.EXPIRY_DATE',
        textKey: 'expiryDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentStatusOptions,
        sortingKey: 'documentStatusGUID',
        searchingKey: 'documentStatusGUID'
      },
      {
        label: 'LABEL.AFFILIATE',
        textKey: 'affiliate',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.affiliateOptions
      },
      {
        label: 'LABEL.PARENT_COMPANY_ID',
        textKey: 'company_CompanyId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE,
        sortingKey: 'parentCompanyGUID',
        searchingKey: 'parentCompanyGUID'
      }
    ];
    this.custom.getColumn(columns, this.parentId);
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<GuarantorAgreementTableListView>> {
    return this.service.getGuarantorAgreementTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteGuarantorAgreementTable(row));
  }
  setFunctionOptions(): void {
    if (this.isExtend) {
      this.custom.getVisibleGenerateFunction(this.parentId).subscribe((isShowFunction) => {
        this.functionItems = [
          {
            label: 'LABEL.GENERATE',
            items: [
              {
                label: 'LABEL.GENERATE_EXTEND_GUARANTOR',
                command: () => this.setPreparingFunctionDataAndValidation(ROUTE_FUNCTION_GEN.GENERATE_EXTEND_GUARANTOR)
              }
            ],
            visible: isShowFunction
          }
        ];
        super.setFunctionOptionsMapping();
      });
    } else {
      super.setFunctionOptionsMapping();
    }
  }
  setPreparingFunctionDataAndValidation(path: string): void {
    this.custom.setPreparingFunctionDataAndValidation(path).subscribe((res) => {
      if (!isNullOrUndefined(res)) {
        this.toFunction({
          path: path
        });
      }
    });
  }
}
