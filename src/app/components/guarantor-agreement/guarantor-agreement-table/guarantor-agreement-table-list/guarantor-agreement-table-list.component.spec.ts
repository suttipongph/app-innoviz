import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GuarantorAgreementTableListComponent } from './guarantor-agreement-table-list.component';

describe('GuarantorAgreementTableListViewComponent', () => {
  let component: GuarantorAgreementTableListComponent;
  let fixture: ComponentFixture<GuarantorAgreementTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GuarantorAgreementTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuarantorAgreementTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
