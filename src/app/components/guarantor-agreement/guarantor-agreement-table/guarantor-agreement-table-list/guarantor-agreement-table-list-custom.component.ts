import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ROUTE_FUNCTION_GEN } from 'shared/constants';
import { ColumnType, SortType } from 'shared/constants';
import { BaseServiceModel, ColumnModel } from 'shared/models/systemModel';
import { AccessModeView } from 'shared/models/viewModel';
import { GuarantorAgreementTableService } from '../guarantor-agreement-table.service';
export class GuarantorAgreementTableListCustomComponent {
  baseService: BaseServiceModel<any>;
  accessModeView: AccessModeView = new AccessModeView();
  service: GuarantorAgreementTableService;
  guarantorAgreementTableGUID;
  showFunction: boolean = false;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setPassParameter(passingObj: any, parentId: string): any {
    if (passingObj.accessModeView) {
      this.accessModeView = passingObj.accessModeView;
    }
  }

  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
  setPreparingFunctionDataAndValidation(path: string): Observable<any> {
    switch (path) {
      case ROUTE_FUNCTION_GEN.GENERATE_EXTEND_GUARANTOR:
        return this.service.getGenerateExtendGuarantorAgreementValidation(this.guarantorAgreementTableGUID).pipe(
          map((res) => {
            if (res) {
              return { guarantorAgreementTableItemView: res };
            } else {
              return null;
            }
          })
        );
      default:
        break;
    }
  }
  getColumn(columns: ColumnModel[], parentId: string): any {
    if (this.guarantorAgreementTableGUID) {
      const fromExtend = {
        label: null,
        textKey: 'refGuarantorAgreementTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.guarantorAgreementTableGUID
      };
      columns.push(fromExtend);
    } else {
      const fromMainAgreement = {
        label: null,
        textKey: 'MainAgreementTable_MainAgreementGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: parentId
      };
      columns.push(fromMainAgreement);
    }
  }
  checkExtend(model: any): any {
    if (model.guarantorAgreementTableGUID) {
      this.guarantorAgreementTableGUID = model.guarantorAgreementTableGUID;
      return true;
    } else {
      return false;
    }
  }
  getVisibleGenerateFunction(id: string): Observable<boolean> {
    return this.service.getVisibleGenerateFunction(id);
  }
}
