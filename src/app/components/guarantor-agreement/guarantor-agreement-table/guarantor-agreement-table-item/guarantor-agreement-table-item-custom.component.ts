import { TranslateService } from '@ngx-translate/core';
import { AppInjector } from 'app-injector';
import { NotificationService } from 'core/services/notification.service';
import { Observable, of } from 'rxjs';
import { count, map } from 'rxjs/operators';
import { AgreementDocType, AppConst, EmptyGuid, GuarantorAgreementStatus, MainAgreementStatus, RefType, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { dateTimetoString, stringToDate, stringToDateTime, datetoString } from 'shared/functions/date.function';
import { toMapModel } from 'shared/functions/model.function';
import { isUpdateMode, getProductType, isNullOrUndefOrEmptyGUID, isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { AccessModeView, GuarantorAgreementTableItemView, ManageAgreementView } from 'shared/models/viewModel';
import { PrintSetBookDocTransView } from 'shared/models/viewModel/printSetBookDocTransView';

const readOnlyOncreateGroup = [
  'MAIN_AGREEMENT_TABLE_GUID',
  'AGREEMENT_DOC_TYPE',
  'SIGNING_DATE',
  'DOCUMENT_REASON_GUID',
  'DOCUMENT_STATUS_GUID',
  'PARENT_COMPANY_GUID',
  'REF_GUARANTOR_AGREEMENT_TABLE_GUID',
  'REF_GUARANTOR_AGREEMENT_ID',
  'AGREEMENT_EXTENSION',
  'GUARANTOR_AGREEMENT_TABLE_GUID',
  'MAIN_AGREEMENT_ID'
];
const isManualInternalGuarantor = ['INTERNAL_GUARANTOR_AGREEMENT_ID'];
const isManualGuarantor = ['GUARANTOR_AGREEMENT_ID'];

const affiliate = ['AFFILIATE'];

export class GuarantorAgreementTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  isManualInternalGuarantorAgreementId: boolean;
  notificationService = AppInjector.get(NotificationService);
  translateService = AppInjector.get(TranslateService);
  accessModeView: AccessModeView = new AccessModeView();

  getFieldAccessing(model: GuarantorAgreementTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    const masterRoute = this.baseService.uiService.getMasterRoute();
    const productType = Number(getProductType(masterRoute));
    return new Observable((observer) => {
      if (isNullOrUndefOrEmptyGUID(model.guarantorAgreementTableGUID)) {
        this.validateIsManualInternalGuarantorNumberSeq(productType).subscribe(
          (result) => {
            fieldAccessing.push({ filedIds: readOnlyOncreateGroup, readonly: true });

            fieldAccessing.push({ filedIds: isManualInternalGuarantor, readonly: !result });
            this.isManualInternalGuarantorAgreementId = result;
            observer.next(fieldAccessing);
          },
          (error) => {
            observer.next(fieldAccessing);
          }
        );
        this.validateIsManualGuarantorNumberSeq(productType).subscribe(
          (result) => {
            fieldAccessing.push({ filedIds: readOnlyOncreateGroup, readonly: true });

            fieldAccessing.push({ filedIds: isManualGuarantor, readonly: !result });
            observer.next(fieldAccessing);
          },
          (error) => {
            observer.next(fieldAccessing);
          }
        );
      } else {
        this.isManualInternalGuarantorAgreementId = false;
        fieldAccessing.push({ filedIds: readOnlyOncreateGroup, readonly: true });
        fieldAccessing.push({ filedIds: isManualInternalGuarantor, readonly: true });
        if (model.haveLine > 0) {
          fieldAccessing.push({ filedIds: affiliate, readonly: true });
        }
        if (model.documentStatus_StatusId === GuarantorAgreementStatus.Draft) {
          this.validateIsManualGuarantorNumberSeq(productType).subscribe(
            (result) => {
              fieldAccessing.push({ filedIds: readOnlyOncreateGroup, readonly: true });

              fieldAccessing.push({ filedIds: isManualGuarantor, readonly: !result });
              observer.next(fieldAccessing);
            },
            (error) => {
              observer.next(fieldAccessing);
            }
          );
        } else {
          if (model.documentStatus_StatusId !== GuarantorAgreementStatus.Draft) {
            fieldAccessing.push({ filedIds: isManualGuarantor, readonly: true });
          }
          observer.next(fieldAccessing);
        }
      }
    });
  }
  validateIsManualGuarantorNumberSeq(productType: any): Observable<boolean> {
    return this.baseService.service.validateIsManualGuarantorNumberSeq(productType);
  }
  validateIsManualInternalGuarantorNumberSeq(productType: any): Observable<boolean> {
    return this.baseService.service.validateIsManualInternalGuarantorNumberSeq(productType);
  }
  validateIsManualNumberSeq(productType: any): Observable<boolean> {
    return this.baseService.service.validateIsManualNumberSeq(productType);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: GuarantorAgreementTableItemView): Observable<boolean> {
    let isStatusValid = model.documentStatus_StatusId < GuarantorAgreementStatus.Signed;
    this.accessModeView.canCreate = this.accessModeView.canCreate && isStatusValid;
    this.accessModeView.canDelete = this.accessModeView.canDelete && isStatusValid;
    this.accessModeView.canView = this.accessModeView.canView && isStatusValid;
    return of(!(this.accessModeView.canCreate && canCreate));
  }
  getInitialData(): Observable<GuarantorAgreementTableItemView> {
    const model = new GuarantorAgreementTableItemView();
    return of(model);
  }
  onAgreementYearChange(e: any, model: GuarantorAgreementTableItemView): any {
    if (e > 0) {
      const d = stringToDate(model.agreementDate);
      const year = d.getFullYear() + e;
      const month = d.getMonth();
      const day = d.getDate() - 1;
      const c = new Date(year, month, day);
      model.expiryDate = datetoString(c);
    }
  }
  onAgreementYearChangeValidate(model: GuarantorAgreementTableItemView): any {
    return this.validateAgreementYearIsZero(model.agreementYear, this.notificationService);
  }
  validateAgreementYearIsZero(agreementYear: any, notificationService: NotificationService): any {
    if (agreementYear === 0) {
      return {
        code: 'ERROR.GREATER_THAN_ZERO',
        parameters: [this.translateService.instant('LABEL.AGREEMENT_YEAR')]
      };
    } else {
      return null;
    }
  }
  onAgreementDateChange(e: any, model: GuarantorAgreementTableItemView): any {
    const d = stringToDate(e);
    const year = d.getFullYear() + model.agreementYear;
    const month = d.getMonth();
    const day = d.getDate() - 1;
    const c = new Date(year, month, day);
    model.expiryDate = datetoString(c);
  }
  isExtend(model: GuarantorAgreementTableItemView): any {
    return model.agreementDocType === AgreementDocType.Extend;
  }
  isCopy(model: GuarantorAgreementTableItemView): any {
    return model.documentStatus_StatusId === GuarantorAgreementStatus.Draft && model.agreementDocType === AgreementDocType.New;
  }
  setPassParameter(passingObj: any): any {
    if (!isNullOrUndefined(passingObj)) {
      this.accessModeView = passingObj.accessModeView;
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
  statusIsLassThanSigned(model: GuarantorAgreementTableItemView): any {
    return model.documentStatus_StatusId < GuarantorAgreementStatus.Signed;
  }
  setPreparingFunctionDataAndValidation(path: string, model: GuarantorAgreementTableItemView): Observable<any> {
    switch (path) {
      case ROUTE_FUNCTION_GEN.GENERATE_EXTEND_GUARANTOR:
        return this.baseService.service.getGenerateNoticeOfCancellationValidation(model).pipe(
          map((res) => {
            if (res) {
              return { guarantorAgreementTableItemView: model };
            } else {
              return null;
            }
          })
        );
      default:
        break;
    }
  }
  setPrintSetBookDocTransData(model: GuarantorAgreementTableItemView): PrintSetBookDocTransView {
    const paramModel = new PrintSetBookDocTransView();
    toMapModel(model, paramModel);
    paramModel.refType = RefType.GuarantorAgreement;
    paramModel.refGUID = model.guarantorAgreementTableGUID;
    paramModel.internalAgreementId = model.internalGuarantorAgreementId;
    paramModel.agreementDescription = model.description;
    paramModel.agreementId = model.guarantorAgreementId;
    return paramModel;
  }
}
