import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuarantorAgreementTableItemComponent } from './guarantor-agreement-table-item.component';

describe('GuarantorAgreementTableItemViewComponent', () => {
  let component: GuarantorAgreementTableItemComponent;
  let fixture: ComponentFixture<GuarantorAgreementTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GuarantorAgreementTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuarantorAgreementTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
