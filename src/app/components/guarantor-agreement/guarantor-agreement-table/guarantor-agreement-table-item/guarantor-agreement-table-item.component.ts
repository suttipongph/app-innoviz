import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, RefType, ManageAgreementAction } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, GuarantorAgreementTableItemView, ManageAgreementView, RefIdParm } from 'shared/models/viewModel';
import { PrintSetBookDocTransView } from 'shared/models/viewModel/printSetBookDocTransView';
import { GuarantorAgreementTableService } from '../guarantor-agreement-table.service';
import { GuarantorAgreementTableItemCustomComponent } from './guarantor-agreement-table-item-custom.component';
@Component({
  selector: 'guarantor-agreement-table-item',
  templateUrl: './guarantor-agreement-table-item.component.html',
  styleUrls: ['./guarantor-agreement-table-item.component.scss']
})
export class GuarantorAgreementTableItemComponent extends BaseItemComponent<GuarantorAgreementTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  agreementDocTypeOptions: SelectItems[] = [];
  employeeTableOptions: SelectItems[] = [];
  manageMenuVisibility: number[] = [];
  passingObj: any;

  constructor(
    private service: GuarantorAgreementTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: GuarantorAgreementTableItemCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.id = this.currentActivatedRoute.snapshot.params['id'];

    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {
    this.passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setPassParameter(this.passingObj);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {
    this.baseDropdown.getAgreementDocTypeEnumDropDown(this.agreementDocTypeOptions);
  }
  getById(): Observable<GuarantorAgreementTableItemView> {
    return this.service.getGuarantorAgreementTableById(this.id);
  }
  getInitialData(): Observable<GuarantorAgreementTableItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.service.getVisibleManageMenu(this.id).subscribe((res) => {
          this.manageMenuVisibility = res;
          this.setRelatedInfoOptions(result);
          this.setFunctionOptions(result);
        });
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getEmployeeTableDropDown()).subscribe(
      ([employeeTable]) => {
        this.employeeTableOptions = employeeTable;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  setRelatedInfoOptions(model: GuarantorAgreementTableItemView): void {
    const isExtend = this.custom.isExtend(model);
    const statusIsLassThanSigned = this.custom.statusIsLassThanSigned(model);
    const allowCreate = statusIsLassThanSigned;
    const allowUpdate = statusIsLassThanSigned;
    const allowDelete = statusIsLassThanSigned;
    const accessModeView = new AccessModeView();
    const accessModeViewExtend = new AccessModeView();
    accessModeViewExtend.canCreate = false;
    accessModeViewExtend.canView = true;
    accessModeViewExtend.canDelete = false;

    accessModeView.canCreate = allowCreate;
    accessModeView.canView = allowUpdate;
    accessModeView.canDelete = allowDelete;
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        command: () => {
          const refIdParm: RefIdParm = { refType: RefType.GuarantorAgreement, refGUID: this.model.guarantorAgreementTableGUID };
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: refIdParm
          });
        }
      },
      {
        label: 'LABEL.MEMO',
        disabled: false,
        visible: true,
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.MEMO })
        // ,
        // command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.MEMO })
      },
      {
        label: 'LABEL.BOOKMARK_DOCUMENT',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.BOOKMARK_DOCUMENT_TRANS })
      },
      // {
      //   label: 'LABEL.ASSIGNMENT_AGREEMENT_SETTLEMENT',
      //   command: () =>
      //     this.toRelatedInfo({
      //       path: ROUTE_RELATED_GEN.ASSIGNMENT_AGREEMENT_SETTLEMENT,
      //       parameters: {
      //         // readOnly: false,
      //         // AllowCreate: false,
      //         // AllowUpdate: false,
      //         // AllowDelete: false,
      //         // ReadOnly: false
      //         assignmentAgreementTableGUID: this.model.assignmentAgreementTableGUID
      //       }
      //     })
      // },
      {
        label: 'LABEL.EXTEND',
        disabled: isExtend,
        visible: !isExtend,
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.EXTEND,
            parameters: {
              readOnly: false,
              AllowCreate: allowCreate,
              AllowUpdate: allowUpdate,
              AllowDelete: allowDelete,
              ReadOnly: false,
              accessModeView: accessModeViewExtend,
              guarantorAgreementTableGUID: this.model.guarantorAgreementTableGUID
            }
          })
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(model: GuarantorAgreementTableItemView): void {
    const isCopy = this.custom.isCopy(model);

    this.functionItems = [
      {
        label: 'LABEL.COPY_GUARANTOR_FROM_CA',
        disabled: !isCopy,
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.GUARANTOR_AGREEMENT_TABLE_COPY_GUARANTOR_FROM_CA })
      },
      {
        label: 'LABEL.MANAGE',
        items: [
          {
            label: 'LABEL.POST',
            disabled: !this.manageMenuVisibility.some((s) => s === ManageAgreementAction.Post),
            command: () => {
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.POST_AGREEMENT,
                parameters: {
                  refType: RefType.GuarantorAgreement,
                  refGuid: this.id,
                  action: ManageAgreementAction.Post
                }
              });
            }
          },
          {
            label: 'LABEL.SEND',
            disabled: !this.manageMenuVisibility.some((s) => s === ManageAgreementAction.Send),
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.SEND_AGREEMENT,
                parameters: {
                  refType: RefType.GuarantorAgreement,
                  refGuid: this.id,
                  action: ManageAgreementAction.Send
                }
              })
          },
          {
            label: 'LABEL.SIGN',
            disabled: !this.manageMenuVisibility.some((s) => s === ManageAgreementAction.Sign),
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.SIGN_AGREEMENT,
                parameters: {
                  refType: RefType.GuarantorAgreement,
                  refGuid: this.id,
                  action: ManageAgreementAction.Sign
                }
              })
          },
          {
            label: 'LABEL.CLOSE',
            disabled: !this.manageMenuVisibility.some((s) => s === ManageAgreementAction.Close),
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.CLOSE_AGREEMENT,
                parameters: {
                  refType: RefType.GuarantorAgreement,
                  refGuid: this.id,
                  action: ManageAgreementAction.Close
                }
              })
          },

          {
            label: 'LABEL.CANCEL',
            disabled: !this.manageMenuVisibility.some((s) => s === ManageAgreementAction.Cancel),
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.CANCEL_AGREEMENT,
                parameters: {
                  refType: RefType.GuarantorAgreement,
                  refGuid: this.id,
                  action: ManageAgreementAction.Cancel
                }
              })
          }
        ]
      },
      {
        label: 'LABEL.PRINT',
        items: [
          {
            label: 'LABEL.BOOKMARK_DOCUMENT',
            command: () =>
              this.toFunction({
                path: ROUTE_FUNCTION_GEN.PRINT_SET_BOOKMARK_DOCUMENT_TRANSACTION,
                parameters: {
                  model: this.setPrintSetBookDocTransData()
                }
              })
          }
        ]
      }
    ];
    super.setFunctionOptionsMapping();
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateGuarantorAgreementTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createGuarantorAgreementTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onAgreementYearChange(e: any): any {
    this.custom.onAgreementYearChange(e, this.model);
  }
  onAgreementDateChange(e: any): any {
    this.custom.onAgreementDateChange(e, this.model);
  }
  setPrintSetBookDocTransData(): PrintSetBookDocTransView {
    return this.custom.setPrintSetBookDocTransData(this.model);
  }
}
