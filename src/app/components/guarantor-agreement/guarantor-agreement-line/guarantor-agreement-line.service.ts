import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, GuarantorAgreementLineItemView, GuarantorAgreementLineListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class GuarantorAgreementLineService {
  serviceKey = 'guarantorAgreementLineGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getGuarantorAgreementLineToList(search: SearchParameter): Observable<SearchResult<GuarantorAgreementLineListView>> {
    const url = `${this.servicePath}/GetGuarantorAgreementLineList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteGuarantorAgreementLine(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteGuarantorAgreementLine`;
    return this.dataGateway.delete(url, row);
  }
  getGuarantorAgreementLineById(id: string): Observable<GuarantorAgreementLineItemView> {
    const url = `${this.servicePath}/GetGuarantorAgreementLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createGuarantorAgreementLine(vmModel: GuarantorAgreementLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateGuarantorAgreementLine`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateGuarantorAgreementLine(vmModel: GuarantorAgreementLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateGuarantorAgreementLine`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<GuarantorAgreementLineItemView> {
    const url = `${this.servicePath}/GetGuarantorAgreementLineInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }

  getGuarantorAgreementLineAccessMode(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetGuarantorAgreementLineAccessMode/id=${id}`;
    return this.dataGateway.get(url);
  }
}
