import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuarantorAgreementLineItemComponent } from './guarantor-agreement-line-item.component';

describe('GuarantorAgreementLineItemViewComponent', () => {
  let component: GuarantorAgreementLineItemComponent;
  let fixture: ComponentFixture<GuarantorAgreementLineItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GuarantorAgreementLineItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuarantorAgreementLineItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
