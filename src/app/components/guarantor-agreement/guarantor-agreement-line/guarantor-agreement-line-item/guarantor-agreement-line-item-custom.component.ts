import { GuarantorAgreementStatus } from './../../../../shared/constants/enumsStatus';
import { CreditAppTableItemView } from './../../../../shared/models/viewModel/creditAppTableItemView';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, GuarantorAgreementLineItemView, GuarantorTransItemView } from 'shared/models/viewModel';

const firstGroup = ['GUARANTOR_AGREEMENT_TABLE_GUID', 'AFFILIATE', 'AGE', 'GUARANTOR_AGREEMENT_LINE_GUID', 'APPROVED_CREDIT_LIMIT'];
const condition2 = ['CUSTOMER_TABLE_GUID', 'CREDIT_APP_TABLE_GUID'];

export class GuarantorAgreementLineItemCustomComponent {
  baseService: BaseServiceModel<any>;
  accessModeView: AccessModeView = new AccessModeView();

  getFieldAccessing(model: GuarantorAgreementLineItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];

    if (!model.affiliate) {
      fieldAccessing.push({ filedIds: condition2, readonly: true });
    }
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: GuarantorAgreementLineItemView): Observable<boolean> {
    const statusLessThanSign = model.documentStatus_StatusId < GuarantorAgreementStatus.Signed;

    return of(!(canCreate && statusLessThanSign));
  }
  getInitialData(): Observable<GuarantorAgreementLineItemView> {
    let model = new GuarantorAgreementLineItemView();
    model.guarantorAgreementLineGUID = EmptyGuid;
    return of(model);
  }
  onGuarantorTransChange(model: GuarantorAgreementLineItemView, guarantorTranOption: SelectItems[]): any {
    model.name = null;
    model.operatedBy = null;
    model.taxId = null;
    model.passportId = null;
    model.workPermitId = null;
    model.dateOfIssue = null;
    model.dateOfBirth = null;
    model.age = null;
    model.nationalityGUID = null;
    model.raceGUID = null;
    model.maximumGuaranteeAmount = null;
    const rowData = isNullOrUndefOrEmptyGUID(model.guarantorTransGUID)
      ? new GuarantorTransItemView()
      : (guarantorTranOption.find((o) => o.value === model.guarantorTransGUID).rowData as GuarantorTransItemView);

    model.name = rowData.relatedPersonTable_Name;
    model.taxId = rowData.relatedPersonTable_TaxId;
    model.passportId = rowData.relatedPersonTable_PassportId;
    model.workPermitId = rowData.relatedPersonTable_WorkPermitId;
    model.dateOfIssue = rowData.relatedPersonTable_DateOfIssue;
    model.dateOfBirth = rowData.relatedPersonTable_DateOfBirth;
    model.age = rowData.age;
    model.nationalityGUID = rowData.relatedPersonTable_NationalityGUID;
    model.raceGUID = rowData.relatedPersonTable_RaceGUID;
    model.primaryAddress1 = rowData.relatedPersonTable_Address1;
    model.primaryAddress2 = rowData.relatedPersonTable_Address2;
    model.maximumGuaranteeAmount = rowData.maximumGuaranteeAmount;
    model.operatedBy = rowData.relatedPersonTable_OperatedBy;
  }
  setPassParameter(passingObj: any): any {
    if (!isNullOrUndefined(passingObj)) {
      this.accessModeView = passingObj.accessModeView;
    }
  }
  onCustomerChange(model: GuarantorAgreementLineItemView): any {
    model.creditAppTableGUID = null;
    model.approvedCreditLimit = null;
  }
  onCreditAppChange(e: any, model: GuarantorAgreementLineItemView, creditAppOption: SelectItems[]): any {
    const rowData = isNullOrUndefOrEmptyGUID(model.creditAppTableGUID)
      ? new CreditAppTableItemView()
      : (creditAppOption.find((o) => o.value === model.creditAppTableGUID).rowData as CreditAppTableItemView);
    model.approvedCreditLimit = rowData.approvedCreditLimit;
  }
  getRelatedInfoActiveTableKey(): string {
    const guaratorName = 'guarantoragreementtable';
    const extend = 'extend';
    const guarantorKey = this.baseService.uiService.getKey(guaratorName);
    const extendKey = this.baseService.uiService.getKey(extend);
    if (!isNullOrUndefOrEmptyGUID(extendKey)) {
      return extendKey;
    } else {
      return guarantorKey;
    }
  }
}
