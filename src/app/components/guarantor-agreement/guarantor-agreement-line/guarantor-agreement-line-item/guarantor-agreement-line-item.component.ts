import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { getProductType, isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, IvzDropdownOnFocusModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, GuarantorAgreementLineItemView } from 'shared/models/viewModel';
import { GuarantorAgreementLineService } from '../guarantor-agreement-line.service';
import { GuarantorAgreementLineItemCustomComponent } from './guarantor-agreement-line-item-custom.component';
@Component({
  selector: 'guarantor-agreement-line-item',
  templateUrl: './guarantor-agreement-line-item.component.html',
  styleUrls: ['./guarantor-agreement-line-item.component.scss']
})
export class GuarantorAgreementLineItemComponent extends BaseItemComponent<GuarantorAgreementLineItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  guarantorTransOptions: SelectItems[] = [];
  nationalityOptions: SelectItems[] = [];
  raceOptions: SelectItems[] = [];
  creditAppOptions: SelectItems[] = [];
  customerOption: SelectItems[] = [];
  passingObj: any;
  productType;
  constructor(
    private service: GuarantorAgreementLineService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: GuarantorAgreementLineItemCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.uiService = this.uiService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.custom.getRelatedInfoActiveTableKey();
    const masterRoute = this.baseService.uiService.getMasterRoute();
    this.productType = getProductType(masterRoute);
  }
  ngOnInit(): void {
    this.passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setPassParameter(this.passingObj);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {}
  getById(): Observable<GuarantorAgreementLineItemView> {
    return this.service.getGuarantorAgreementLineById(this.id);
  }
  getInitialData(): Observable<GuarantorAgreementLineItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner(result);
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(
      this.baseDropdown.getGuarantorTransDropDownByCreditAppTable(model.mainAgreement_CreditAppTableGUID, model?.guarantorTransGUID),
      this.baseDropdown.getNationalityDropDown(),
      this.baseDropdown.getRaceDropDown(),
      this.baseDropdown.getCreditAppDropdownByProductMainAgreementDropDown([this.productType, model.customerTableGUID]),
      this.baseDropdown.getCustomerTableByDropDown()
    ).subscribe(
      ([guarantorTrans, nationality, race, creditApp, customer]) => {
        this.guarantorTransOptions = guarantorTrans;
        this.nationalityOptions = nationality;
        this.raceOptions = race;
        this.creditAppOptions = creditApp;
        this.customerOption = customer;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateGuarantorAgreementLine(this.model), isColsing);
    } else {
      super.onCreate(this.service.createGuarantorAgreementLine(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onGuarantorTransChange(e: any): void {
    this.custom.onGuarantorTransChange(this.model, this.guarantorTransOptions);
  }
  onCreditAppChange(e: any): void {
    this.custom.onCreditAppChange(e, this.model, this.creditAppOptions);
  }
  onCustomerChange(): void {
    this.custom.onCustomerChange(this.model);
    this.baseDropdown.getCreditAppDropdownByProductMainAgreementDropDown([this.productType, this.model.customerTableGUID]).subscribe((result) => {
      this.creditAppOptions = result;
    });
  }
  onGuarantorTransDropdownFocus(e: IvzDropdownOnFocusModel): void {
    super
      .setDropdownOptionOnFocus(
        e,
        this.model,
        this.baseDropdown.getGuarantorTransDropDownByCreditAppTable(this.model.mainAgreement_CreditAppTableGUID)
      )
      .subscribe((result) => {
        this.guarantorTransOptions = result;
      });
  }
}
