import { Observable, of } from 'rxjs';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView, GuarantorAgreementLineListView } from 'shared/models/viewModel';
import { AccessMode } from 'shared/constants';
import { map } from 'rxjs/operators';

export class GuarantorAgreementLineListCustomComponent {
  baseService: BaseServiceModel<any>;
  accessModeView: AccessModeView = new AccessModeView();

  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setAccessModeByParentStatus(): Observable<AccessModeView> {
    const guaratorName = 'guarantoragreementtable';
    const extend = 'extend';
    const guarantorKey = this.baseService.uiService.getKey(guaratorName);
    const extendKey = this.baseService.uiService.getKey(extend);
    let parentKey = '';
    if (!isNullOrUndefOrEmptyGUID(extendKey)) {
      parentKey = extendKey;
    } else {
      parentKey = guarantorKey;
    }
    return this.baseService.service.getGuarantorAgreementLineAccessMode(parentKey);
  }
  setPassParameter(passingObj: any): any {
    if (!isNullOrUndefined(passingObj)) {
      this.accessModeView = passingObj.accessModeView;
    } else {
      this.accessModeView.canCreate = false;
      this.accessModeView.canView = true;
      this.accessModeView.canDelete = false;
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(model: GuarantorAgreementLineListView, accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
  getRowAuthorize(row: GuarantorAgreementLineListView[]): boolean {
    const isSetRow: boolean = true;
    row.forEach((item) => {
      if (this.accessModeView.canDelete === false) {
        item.rowAuthorize = AccessMode.creator;
      } else {
        const accessMode: AccessMode = item.isAffiliateCreated ? AccessMode.full : AccessMode.creator;
        item.rowAuthorize = accessMode;
      }
    });

    return isSetRow;
  }
  getParenActiveTabKey(): string {
    const guaratorName = 'guarantoragreementtable';
    const extend = 'extend';
    const guarantorKey = this.baseService.uiService.getKey(guaratorName);
    const extendKey = this.baseService.uiService.getKey(extend);
    if (!isNullOrUndefOrEmptyGUID(extendKey)) {
      return extendKey;
    } else {
      return guarantorKey;
    }
  }
}
