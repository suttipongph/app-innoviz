import { ROUTE_RELATED_GEN } from './../../../../shared/constants/constantGen';
import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, ROUTE_MASTER_GEN, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { GuarantorAgreementLineListView } from 'shared/models/viewModel';
import { GuarantorAgreementLineService } from '../guarantor-agreement-line.service';
import { GuarantorAgreementLineListCustomComponent } from './guarantor-agreement-line-list-custom.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'guarantor-agreement-line-list',
  templateUrl: './guarantor-agreement-line-list.component.html',
  styleUrls: ['./guarantor-agreement-line-list.component.scss']
})
export class GuarantorAgreementLineListComponent extends BaseListComponent<GuarantorAgreementLineListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  guarantorTransOptions: SelectItems[] = [];
  nationalityOptions: SelectItems[] = [];
  raceOptions: SelectItems[] = [];

  constructor(
    public custom: GuarantorAgreementLineListCustomComponent,
    private service: GuarantorAgreementLineService,
    private currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.custom.baseService.uiService = this.uiService;
    this.parentId = this.custom.getParenActiveTabKey();
  }
  ngOnInit(): void {
    this.accessRightChildPage = 'GuarantorAgreementLineListPage';
    this.redirectPath = ROUTE_RELATED_GEN.GUARANTOR_AGREEMENT_LINE;
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setPassParameter(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.setAccessModeByParentStatus().subscribe((result) => {
      this.custom.accessModeView = result;
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getGuarantorTransDropDown(this.parentId, this.guarantorTransOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = true;
    this.option.canDelete = true;
    this.option.key = 'guarantorAgreementLineGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.ORDERING',
        textKey: 'ordering',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.GUARANTOR_ID',
        textKey: 'guarantorTrans_Values',
        searchingKey: 'guarantorTransGUID',
        sortingKey: 'guarantorTransGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.guarantorTransOptions
      },
      {
        label: 'LABEL.TAX_ID',
        textKey: 'taxId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PASSPORT_ID',
        textKey: 'passportId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.NAME',
        textKey: 'name',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.OPERATED_BY',
        textKey: 'operatedBy',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: null,
        textKey: 'guarantorAgreementTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<GuarantorAgreementLineListView>> {
    return this.service.getGuarantorAgreementLineToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteGuarantorAgreementLine(row));
  }
  getRowAuthorize(row: GuarantorAgreementLineListView[]): void {
    if (this.custom.getRowAuthorize(row)) {
      row.forEach((item) => {
        item.rowAuthorize = this.getSingleRowAuthorize(item.rowAuthorize, item);
      });
    } else {
      super.getRowAuthorize(row);
    }
  }
}
