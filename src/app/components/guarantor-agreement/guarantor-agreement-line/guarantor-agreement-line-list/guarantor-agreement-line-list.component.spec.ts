import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GuarantorAgreementLineListComponent } from './guarantor-agreement-line-list.component';

describe('GuarantorAgreementLineListViewComponent', () => {
  let component: GuarantorAgreementLineListComponent;
  let fixture: ComponentFixture<GuarantorAgreementLineListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GuarantorAgreementLineListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuarantorAgreementLineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
