import { GuarantorAgreementLineAffiliateItemComponent } from './../guarantor-agreement-line-affiliate/guarantor-agreement-line-affiliate-item/guarantor-agreement-line-affiliate-item.component';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GuarantorAgreementLineService } from './guarantor-agreement-line.service';
import { GuarantorAgreementLineListComponent } from './guarantor-agreement-line-list/guarantor-agreement-line-list.component';
import { GuarantorAgreementLineItemComponent } from './guarantor-agreement-line-item/guarantor-agreement-line-item.component';
import { GuarantorAgreementLineItemCustomComponent } from './guarantor-agreement-line-item/guarantor-agreement-line-item-custom.component';
import { GuarantorAgreementLineListCustomComponent } from './guarantor-agreement-line-list/guarantor-agreement-line-list-custom.component';
import { GuarantorAgreementLineAffiliateComponentModule } from '../guarantor-agreement-line-affiliate/guarantor-agreement-line-affiliate.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, GuarantorAgreementLineAffiliateComponentModule],
  declarations: [GuarantorAgreementLineListComponent, GuarantorAgreementLineItemComponent],
  providers: [
    GuarantorAgreementLineService,
    GuarantorAgreementLineItemCustomComponent,
    GuarantorAgreementLineListCustomComponent,
    GuarantorAgreementLineAffiliateItemComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [GuarantorAgreementLineListComponent, GuarantorAgreementLineItemComponent]
})
export class GuarantorAgreementLineComponentModule {}
