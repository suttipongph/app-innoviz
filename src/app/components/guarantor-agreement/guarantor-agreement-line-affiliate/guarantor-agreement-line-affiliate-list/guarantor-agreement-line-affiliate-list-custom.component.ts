import { GuarantorAgreementLineAffiliateListView } from './../../../../shared/models/viewModel/guarantorAgreementLineAffiliateListView';
import { Observable, of } from 'rxjs';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView, GuarantorAgreementLineItemView } from 'shared/models/viewModel';
import { AccessMode, AgreementDocType, ROUTE_RELATED_GEN } from 'shared/constants';
import { tap } from 'rxjs/operators';
import { GuarantorAgreementLineAffiliateService } from '../guarantor-agreement-line-affiliate.service';

export class GuarantorAgreementLineAffiliateListCustomComponent {
  baseService: BaseServiceModel<any>;
  accessModeView: AccessModeView = new AccessModeView();
  accessModeViewFromPassParameter: AccessModeView = new AccessModeView();
  model: GuarantorAgreementLineAffiliateListView;
  cancreate: boolean;
  parentId = null;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setAccessModeByParent(): Observable<any> {
    return this.getAccessModeByGuarantor();
  }
  setPassParameter(passingObj: any): any {
    this.parentId = this.baseService.uiService.getKey(ROUTE_RELATED_GEN.GUARANTOR_AGREEMENT_TABLE);
    if (!isNullOrUndefined(passingObj)) {
      this.accessModeViewFromPassParameter = passingObj.accessModeView;
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate && this.accessModeViewFromPassParameter.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView && this.accessModeViewFromPassParameter.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete && this.accessModeViewFromPassParameter.canDelete;
  }
  getRowAuthorize(row: GuarantorAgreementLineAffiliateListView[]): boolean {
    const isSetRow: boolean = true;
    row.forEach((item) => {
      if (this.accessModeView.canDelete === false) {
        item.rowAuthorize = AccessMode.creator;
      } else {
        const accessMode: AccessMode = this.accessModeView.canView ? AccessMode.full : AccessMode.creator;
        item.rowAuthorize = accessMode;
      }
    });

    return isSetRow;
  }
  getAccessModeByGuarantor(): Observable<AccessModeView> {
    const guaratorName = 'guarantoragreementtable';
    const extend = 'extend';
    const guarantorKey = this.baseService.uiService.getKey(guaratorName);
    const extendKey = this.baseService.uiService.getKey(extend);
    let parentKey = '';
    if (!isNullOrUndefOrEmptyGUID(extendKey)) {
      parentKey = extendKey;
    } else {
      parentKey = guarantorKey;
    }
    return (this.baseService.service as GuarantorAgreementLineAffiliateService).getAccessModeGuarantorTable(parentKey).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
      })
    );
  }
}
