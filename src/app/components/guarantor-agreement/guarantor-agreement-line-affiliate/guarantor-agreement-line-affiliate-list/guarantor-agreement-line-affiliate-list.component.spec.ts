import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GuarantorAgreementLineAffiliateListComponent } from './guarantor-agreement-line-affiliate-list.component';

describe('GuarantorAgreementLineAffiliateListViewComponent', () => {
  let component: GuarantorAgreementLineAffiliateListComponent;
  let fixture: ComponentFixture<GuarantorAgreementLineAffiliateListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GuarantorAgreementLineAffiliateListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuarantorAgreementLineAffiliateListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
