import { ActivatedRoute } from '@angular/router';
import { ROUTE_MASTER } from './../../../../shared/constants/constant';
import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { GuarantorAgreementLineAffiliateListView } from 'shared/models/viewModel';
import { GuarantorAgreementLineAffiliateService } from '../guarantor-agreement-line-affiliate.service';
import { GuarantorAgreementLineAffiliateListCustomComponent } from './guarantor-agreement-line-affiliate-list-custom.component';

@Component({
  selector: 'guarantor-agreement-line-affiliate-list',
  templateUrl: './guarantor-agreement-line-affiliate-list.component.html',
  styleUrls: ['./guarantor-agreement-line-affiliate-list.component.scss']
})
export class GuarantorAgreementLineAffiliateListComponent
  extends BaseListComponent<GuarantorAgreementLineAffiliateListView>
  implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  constructor(
    public custom: GuarantorAgreementLineAffiliateListCustomComponent,
    private service: GuarantorAgreementLineAffiliateService,
    private currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.custom.baseService.uiService = this.uiService;
  }
  ngOnInit(): void {
    this.accessRightChildPage = 'GuarantorAgreementLineAffiliateListPage';
    this.redirectPath = ROUTE_RELATED_GEN.GUARANTOR_AGREEMENT_LINE_AFFLIATE;
    this.parentId = this.uiService.getKey(ROUTE_RELATED_GEN.GUARANTOR_AGREEMENT_LINE);
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setPassParameter(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.setAccessModeByParent().subscribe((result) => {
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'guarantorAgreementLineAffiliateGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.ORDERING',
        textKey: 'ordering',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.ASC
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customer_Values',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTableGUID'
      },
      {
        label: null,
        textKey: 'guarantorAgreementLineGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<GuarantorAgreementLineAffiliateListView>> {
    return this.service.getGuarantorAgreementLineAffiliateToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteGuarantorAgreementLineAffiliate(row));
  }
  getRowAuthorize(row: GuarantorAgreementLineAffiliateListView[]): void {
    if (this.custom.getRowAuthorize(row)) {
      row.forEach((item) => {
        item.rowAuthorize = this.getSingleRowAuthorize(item.rowAuthorize, item);
      });
    } else {
      super.getRowAuthorize(row);
    }
  }
}
