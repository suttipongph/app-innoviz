import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GuarantorAgreementLineAffiliateService } from './guarantor-agreement-line-affiliate.service';
import { GuarantorAgreementLineAffiliateListComponent } from './guarantor-agreement-line-affiliate-list/guarantor-agreement-line-affiliate-list.component';
import { GuarantorAgreementLineAffiliateItemComponent } from './guarantor-agreement-line-affiliate-item/guarantor-agreement-line-affiliate-item.component';
import { GuarantorAgreementLineAffiliateItemCustomComponent } from './guarantor-agreement-line-affiliate-item/guarantor-agreement-line-affiliate-item-custom.component';
import { GuarantorAgreementLineAffiliateListCustomComponent } from './guarantor-agreement-line-affiliate-list/guarantor-agreement-line-affiliate-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [GuarantorAgreementLineAffiliateListComponent, GuarantorAgreementLineAffiliateItemComponent],
  providers: [
    GuarantorAgreementLineAffiliateService,
    GuarantorAgreementLineAffiliateItemCustomComponent,
    GuarantorAgreementLineAffiliateListCustomComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [GuarantorAgreementLineAffiliateListComponent, GuarantorAgreementLineAffiliateItemComponent]
})
export class GuarantorAgreementLineAffiliateComponentModule {}
