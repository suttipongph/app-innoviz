import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, AgreementDocType, MainAgreementStatus, GuarantorAgreementStatus } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { getProductType, isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, GuarantorAgreementLineAffiliateItemView } from 'shared/models/viewModel';
import { GuarantorAgreementLineAffiliateService } from '../guarantor-agreement-line-affiliate.service';
import { GuarantorAgreementLineAffiliateItemCustomComponent } from './guarantor-agreement-line-affiliate-item-custom.component';
@Component({
  selector: 'guarantor-agreement-line-affiliate-item',
  templateUrl: './guarantor-agreement-line-affiliate-item.component.html',
  styleUrls: ['./guarantor-agreement-line-affiliate-item.component.scss']
})
export class GuarantorAgreementLineAffiliateItemComponent
  extends BaseItemComponent<GuarantorAgreementLineAffiliateItemView>
  implements BaseItemInterface {
  mainAgreementTableGUIDOptions: SelectItems[] = [];
  customerTableGUIDOptions: SelectItems[] = [];
  passingObj: any;
  guarantorId;
  mainAgreementKey;
  productType;
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  constructor(
    private service: GuarantorAgreementLineAffiliateService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: GuarantorAgreementLineAffiliateItemCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiService.getKey(ROUTE_RELATED_GEN.GUARANTOR_AGREEMENT_LINE);
    this.guarantorId = this.uiService.getKey(ROUTE_RELATED_GEN.GUARANTOR_AGREEMENT_TABLE);
    this.mainAgreementKey = this.uiService.getRelatedInfoActiveTableKey();
    const masterRoute = this.baseService.uiService.getMasterRoute();
    this.productType = getProductType(masterRoute);
  }
  ngOnInit(): void {
    this.passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setPassParameter(this.passingObj);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {}
  getById(): Observable<GuarantorAgreementLineAffiliateItemView> {
    return this.service.getGuarantorAgreementLineAffiliateById(this.id);
  }
  getInitialData(): Observable<GuarantorAgreementLineAffiliateItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner(result);
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: GuarantorAgreementLineAffiliateItemView): void {
    forkJoin(
      this.custom.getCustomerTableByGuarantorIdDropDown(model.guarantorAgreementTable_ParentCompanyGUID),
      this.custom.getMainAgreementTableByProductDropDown(
        this.productType,
        model.customerTableGUID,
        AgreementDocType.New.toString(),
        this.baseDropdown
      )
    ).subscribe(
      ([customerTable, mainAgreementTable]) => {
        this.mainAgreementTableGUIDOptions = mainAgreementTable;
        this.customerTableGUIDOptions = customerTable;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateGuarantorAgreementLineAffiliate(this.model), isColsing);
    } else {
      super.onCreate(this.service.createGuarantorAgreementLineAffiliate(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }

  onCustomerChange(e: any): any {
    this.custom.onCustomerChange(this.model);
    this.custom
      .getMainAgreementTableByProductDropDown(this.productType, this.model.customerTableGUID, AgreementDocType.New.toString(), this.baseDropdown)
      .subscribe((result) => {
        this.mainAgreementTableGUIDOptions = result;
      });
  }
}
