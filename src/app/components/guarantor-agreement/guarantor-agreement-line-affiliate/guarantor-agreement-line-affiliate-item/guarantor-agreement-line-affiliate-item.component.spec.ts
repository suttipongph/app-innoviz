import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuarantorAgreementLineAffiliateItemComponent } from './guarantor-agreement-line-affiliate-item.component';

describe('GuarantorAgreementLineAffiliateItemViewComponent', () => {
  let component: GuarantorAgreementLineAffiliateItemComponent;
  let fixture: ComponentFixture<GuarantorAgreementLineAffiliateItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GuarantorAgreementLineAffiliateItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuarantorAgreementLineAffiliateItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
