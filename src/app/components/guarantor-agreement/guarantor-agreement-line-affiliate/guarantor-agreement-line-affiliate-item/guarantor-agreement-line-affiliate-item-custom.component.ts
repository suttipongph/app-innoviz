import { GuarantorAgreementStatus } from './../../../../shared/constants/enumsStatus';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { Observable, of } from 'rxjs';
import { EmptyGuid, MainAgreementStatus } from 'shared/constants';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, GuarantorAgreementLineAffiliateItemView, GuarantorAgreementLineAffiliateListView } from 'shared/models/viewModel';
import { BaseDropdownComponent } from 'core/components/base/base.dropdown.component';

const firstGroup = ['GUARANTOR_AGREEMENT_LINE_GUID', 'GUARANTOR_AGREEMENT_LINE_AFFILIATE_GUID'];
const viewCondition = [
  'GUARANTOR_AGREEMENT_LINE_GUID',
  'GUARANTOR_AGREEMENT_LINE_AFFILIATE_GUID',
  'CUSTOMER_TABLE_GUID',
  'MAIN_AGREEMENT_TABLE_GUID'
];
export class GuarantorAgreementLineAffiliateItemCustomComponent {
  baseService: BaseServiceModel<any>;
  accessModeView: AccessModeView = new AccessModeView();

  getFieldAccessing(model: GuarantorAgreementLineAffiliateItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];

    if (isNullOrUndefOrEmptyGUID(model)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });

      return of(fieldAccessing);
    } else {
      if (model.documentStatus_StatusId >= GuarantorAgreementStatus.Signed) {
        fieldAccessing.push({ filedIds: viewCondition, readonly: true });
        return of(fieldAccessing);
      } else {
        fieldAccessing.push({ filedIds: firstGroup, readonly: true });
        return of(fieldAccessing);
      }
    }
  }

  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: GuarantorAgreementLineAffiliateItemView): Observable<boolean> {
    return of(!this.accessModeView.canCreate);
  }
  setPassParameter(passingObj: any): any {
    if (!isNullOrUndefined(passingObj)) {
      this.accessModeView = passingObj.accessModeView;
    }
  }
  onCustomerChange(model: GuarantorAgreementLineAffiliateItemView): any {
    model.mainAgreementTableGUID = null;
  }
  getMainAgreementTableByProductDropDown(
    productType: string,
    customerTableGUID: string,
    agreementDocType: string,
    baseDropdown: BaseDropdownComponent
  ): Observable<SelectItems[]> {
    if (isNullOrUndefOrEmptyGUID(customerTableGUID)) {
      return of([]);
    } else {
      return baseDropdown.getMainAgreementTableByProductDropDown([productType, customerTableGUID, agreementDocType]);
    }
  }
  getCustomerTableByGuarantorIdDropDown(parentCompany: string): Observable<SelectItems[]> {
    if (isNullOrUndefOrEmptyGUID(parentCompany)) {
      return of([]);
    } else {
      return this.baseService.baseDropdown.getCustomerTableByGuarantorIdDropDown(parentCompany);
    }
  }
}
