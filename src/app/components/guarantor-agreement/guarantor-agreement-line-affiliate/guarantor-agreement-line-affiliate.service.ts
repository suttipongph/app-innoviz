import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  AccessModeView,
  GuarantorAgreementLineAffiliateItemView,
  GuarantorAgreementLineAffiliateListView,
  GuarantorAgreementLineItemView,
  GuarantorAgreementTableItemView
} from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class GuarantorAgreementLineAffiliateService {
  serviceKey = 'guarantorAgreementLineAffiliateGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getGuarantorAgreementLineAffiliateToList(search: SearchParameter): Observable<SearchResult<GuarantorAgreementLineAffiliateListView>> {
    const url = `${this.servicePath}/GetGuarantorAgreementLineAffiliateList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteGuarantorAgreementLineAffiliate(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteGuarantorAgreementLineAffiliate`;
    return this.dataGateway.delete(url, row);
  }
  getGuarantorAgreementLineAffiliateById(id: string): Observable<GuarantorAgreementLineAffiliateItemView> {
    const url = `${this.servicePath}/GetGuarantorAgreementLineAffiliateById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createGuarantorAgreementLineAffiliate(vmModel: GuarantorAgreementLineAffiliateItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateGuarantorAgreementLineAffiliate`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateGuarantorAgreementLineAffiliate(vmModel: GuarantorAgreementLineAffiliateItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateGuarantorAgreementLineAffiliate`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<GuarantorAgreementLineAffiliateItemView> {
    const url = `${this.servicePath}/GetGuarantorAgreementLineAffiliateInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeGuarantorTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByGuarantorAgreement/id=${id}`;
    return this.dataGateway.get(url);
  }
}
