import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ExtendGuarantorAgreementResultView, ExtendGuarantorAgreementView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ExtendGuarantorAgreementService {
  serviceKey = 'extendGuarantorAgreementGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getExtendGuarantorAgreementToList(search: SearchParameter): Observable<SearchResult<ExtendGuarantorAgreementView>> {
    const url = `${this.servicePath}/GetExtendGuarantorAgreementList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteExtendGuarantorAgreement(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteExtendGuarantorAgreement`;
    return this.dataGateway.delete(url, row);
  }
  getExtendGuarantorAgreementById(id: string): Observable<ExtendGuarantorAgreementView> {
    const url = `${this.servicePath}/GetExtendGuarantorAgreementById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createExtendGuarantorAgreement(vmModel: ExtendGuarantorAgreementView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateExtendGuarantorAgreement`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateExtendGuarantorAgreement(vmModel: ExtendGuarantorAgreementView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateExtendGuarantorAgreement`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  generateExtendGuarantorAgreement(vmModel: ExtendGuarantorAgreementView): Observable<ExtendGuarantorAgreementResultView> {
    const url = `${this.servicePath}/GenerateExtendGuarantorAgreement`;
    return this.dataGateway.post(url, vmModel);
  }
  validateIsManualInternalGuarantorNumberSeq(productType: number): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualInternalNumberSeq/productType=${productType}`;
    return this.dataGateway.get(url);
  }
}
