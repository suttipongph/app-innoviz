import { Observable, of } from 'rxjs';
import { AgreementDocType, EmptyGuid } from 'shared/constants';
import { getProductType } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { ExtendGuarantorAgreementResultView, ExtendGuarantorAgreementView } from 'shared/models/viewModel';

const firstGroup = [
  'INTERNAL_GUARANTOR_AGREEMENT_ID',
  'INTERNAL_MAIN_AGREEMENT_ID',
  'GUARANTOR_AGREEMENT_ID',
  'DESCRIPTION',
  'AGREEMENT_DATE',
  'EXPIRY_DATE',
  'AFFILIATE',
  'AGREEMENT_DOC_TYPE',
  'AGREEMENT_EXTENSION',
  'EXTEND_INTERNAL_GUARANTOR_AGREEMENT_ID'
];
const isManualInternalGuarantor = ['EXTEND_INTERNAL_GUARANTOR_AGREEMENT_ID'];
export class ExtendGuarantorAgreementCustomComponent {
  baseService: BaseServiceModel<any>;
  isManualInternalGuarantorAgreementId = false;
  getFieldAccessing(model: ExtendGuarantorAgreementView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    const masterRoute = this.baseService.uiService.getMasterRoute();
    const productType = Number(getProductType(masterRoute));
    return new Observable((observer) => {
      this.validateIsManualInternalGuarantorNumberSeq(productType).subscribe(
        (result) => {
          fieldAccessing.push({ filedIds: firstGroup, readonly: true });
          fieldAccessing.push({ filedIds: isManualInternalGuarantor, readonly: !result });
          this.isManualInternalGuarantorAgreementId = result;
          observer.next(fieldAccessing);
        },
        (error) => {
          observer.next(fieldAccessing);
        }
      );
    });
  }
  getModelParam(model: ExtendGuarantorAgreementResultView): Observable<ExtendGuarantorAgreementView> {
    const masterRoute = this.baseService.uiService.getMasterRoute();
    const productType = Number(getProductType(masterRoute));
    const param: ExtendGuarantorAgreementView = new ExtendGuarantorAgreementView();
    param.guarantorAgreementTableGUID = model.guarantorAgreementTableGUID;
    param.agreementDocType = model.agreementDocType;
    param.documentReasonGUID = model.documentReasonGUID;
    param.reasonRemark = model.reasonRemark;
    param.affiliate = model.affiliate;
    param.agreementDate = model.agreementDate;
    param.agreementExtension = model.agreementExtension;
    param.description = model.description;
    param.expiryDate = model.expiryDate;
    param.extendInternalGuarantorAgreementId = model.extendInternalGuarantorAgreementId;
    param.internalGuarantorAgreementId = model.internalGuarantorAgreementId;
    param.internalMainAgreementId = model.internalMainAgreementId;
    param.newAgreementDate = model.newAgreementDate;
    param.newDescription = model.newDescription;
    param.documentStatusGUID = model.documentStatusGUID;
    param.productType = productType;
    param.companyGUID = model.companyGUID;
    return of(param);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<ExtendGuarantorAgreementView> {
    let model = new ExtendGuarantorAgreementView();
    model.guarantorAgreementTableGUID = EmptyGuid;
    return of(model);
  }
  getRemakReason(id: string, options: SelectItems[]): Observable<SelectItems> {
    const item = options.find((f) => f.value === id);
    return of(item);
  }
  validateIsManualInternalGuarantorNumberSeq(productType: any): Observable<boolean> {
    return this.baseService.service.validateIsManualInternalGuarantorNumberSeq(productType);
  }
  validateIsManualNumberSeq(productType: any): Observable<boolean> {
    return this.baseService.service.validateIsManualNumberSeq(productType);
  }
}
