import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtendGuarantorAgreementComponent } from './extend-guarantor-agreement.component';

describe('ExtendGuarantorAgreementViewComponent', () => {
  let component: ExtendGuarantorAgreementComponent;
  let fixture: ComponentFixture<ExtendGuarantorAgreementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExtendGuarantorAgreementComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtendGuarantorAgreementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
