import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExtendGuarantorAgreementService } from './extend-guarantor-agreement.service';
import { ExtendGuarantorAgreementComponent } from './extend-guarantor-agreement/extend-guarantor-agreement.component';
import { ExtendGuarantorAgreementCustomComponent } from './extend-guarantor-agreement/extend-guarantor-agreement-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ExtendGuarantorAgreementComponent],
  providers: [ExtendGuarantorAgreementService, ExtendGuarantorAgreementCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ExtendGuarantorAgreementComponent]
})
export class ExtendGuarantorAgreementComponentModule {}
