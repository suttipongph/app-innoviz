import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CopyGuarantorFromCAService } from './copy-guarantor-from-ca.service';
import { CopyGuarantorFromCAComponent } from './copy-guarantor-from-ca/copy-guarantor-from-ca.component';
import { CopyGuarantorFromCACustomComponent } from './copy-guarantor-from-ca/copy-guarantor-from-ca-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CopyGuarantorFromCAComponent],
  providers: [CopyGuarantorFromCAService, CopyGuarantorFromCACustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CopyGuarantorFromCAComponent]
})
export class CopyGuarantorFromCAComponentModule {}
