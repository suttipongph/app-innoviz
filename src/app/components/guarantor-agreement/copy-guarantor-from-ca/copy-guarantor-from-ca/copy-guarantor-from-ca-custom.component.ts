import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CopyGuarantorFromCAView } from 'shared/models/viewModel';

const firstGroup = [
  'INTERNAL_GUARANTOR_AGREEMENT_ID',
  'GUARANTOR_AGREEMENT_ID',
  'DESCRIPTION',
  'AFFILIATE',
  'CUSTOMER_TABLE_GUID',
  'CREDIT_APP_TABLE_GUID'
];
const secondGroup = ['CREDIT_APP_REQUEST_TABLE_GUID'];
export class CopyGuarantorFromCACustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CopyGuarantorFromCAView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CopyGuarantorFromCAView): Observable<boolean> {
    return of(true);
  }
  getInitialData(): Observable<CopyGuarantorFromCAView> {
    let model = new CopyGuarantorFromCAView();
    model.copyGuarantorFromCAGUID = EmptyGuid;
    return of(model);
  }
}
