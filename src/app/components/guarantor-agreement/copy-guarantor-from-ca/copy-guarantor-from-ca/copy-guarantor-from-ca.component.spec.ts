import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopyGuarantorFromCAComponent } from './copy-guarantor-from-ca.component';

describe('CopyGuarantorFromCAViewComponent', () => {
  let component: CopyGuarantorFromCAComponent;
  let fixture: ComponentFixture<CopyGuarantorFromCAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CopyGuarantorFromCAComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyGuarantorFromCAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
