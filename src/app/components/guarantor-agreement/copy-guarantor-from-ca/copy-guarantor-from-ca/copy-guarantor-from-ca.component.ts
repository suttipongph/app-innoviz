import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, CreditAppRequestStatus } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { Confirmation } from 'shared/models/primeModel';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { CopyGuarantorFromCAView } from 'shared/models/viewModel';
import { CopyGuarantorFromCAService } from '../copy-guarantor-from-ca.service';
import { CopyGuarantorFromCACustomComponent } from './copy-guarantor-from-ca-custom.component';
@Component({
  selector: 'copy-guarantor-from-ca',
  templateUrl: './copy-guarantor-from-ca.component.html',
  styleUrls: ['./copy-guarantor-from-ca.component.scss']
})
export class CopyGuarantorFromCAComponent extends BaseItemComponent<CopyGuarantorFromCAView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  creditApplicationRequestTableOptions: SelectItems[] = [];
  creditAppTableGUIDOptions: SelectItems[] = [];
  constructor(
    private service: CopyGuarantorFromCAService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CopyGuarantorFromCACustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.isUpdateMode = false;
    this.setInitialCreatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<CopyGuarantorFromCAView> {
    return this.service.getCopyGuarantorFromCAById(this.id);
  }
  getInitialData(): Observable<CopyGuarantorFromCAView> {
    return this.service.getCopyGuarantorFromCAById(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner(this.model);
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(
      this.baseDropdown.getCreditApplicationRequestTableDropDown([
        model.customerTable_CustomerTableGUID,
        model.mainAgreementTable_ProductType.toString(),
        model.mainAgreement_ConsortiumGUID,
        CreditAppRequestStatus.Approved.toString()
      ]),
      this.baseDropdown.getCreditAppTableDropDown()
    ).subscribe(
      ([creditApplicationRequestTable, creditAppTableGUIDOptions]) => {
        this.creditApplicationRequestTableOptions = creditApplicationRequestTable;
        this.creditAppTableGUIDOptions = creditAppTableGUIDOptions;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = false;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onClose(): void {
    super.onBack();
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.model.haveLine > 0) {
      const confirmation: Confirmation = {
        header: this.baseService.translate.instant('CONFIRM.CONFIRM'),
        message: this.baseService.translate.instant('CONFIRM.90002', [this.baseService.translate.instant('LABEL.GUARANTOR')])
      };
      this.baseService.notificationService.showConfirmDialog(confirmation);
      this.baseService.notificationService.isAccept.subscribe((isConfirm) => {
        if (isConfirm) {
          super.onExecuteFunction(this.service.createCopyGuarantorFromCA(this.model));
        }
      });
    } else {
      super.onExecuteFunction(this.service.createCopyGuarantorFromCA(this.model));
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
