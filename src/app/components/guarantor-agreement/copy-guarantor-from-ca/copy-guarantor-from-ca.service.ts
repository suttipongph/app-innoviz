import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CopyGuarantorFromCAView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CopyGuarantorFromCAService {
  serviceKey = 'copyGuarantorFromCAGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  // getCopyGuarantorFromCAToList(search: SearchParameter): Observable<SearchResult<CopyGuarantorFromCAListView>> {
  //   const url = `${this.servicePath}/GetCopyGuarantorFromCAList/${search.branchFilterMode}`;
  //   return this.dataGateway.getList(url, search);
  // }
  deleteCopyGuarantorFromCA(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCopyGuarantorFromCA`;
    return this.dataGateway.delete(url, row);
  }
  getCopyGuarantorFromCAById(id: string): Observable<CopyGuarantorFromCAView> {
    const url = `${this.servicePath}/GetCopyGuarantorFromCAById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCopyGuarantorFromCA(vmModel: CopyGuarantorFromCAView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCopyGuarantorFromCA`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCopyGuarantorFromCA(vmModel: CopyGuarantorFromCAView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCopyGuarantorFromCA`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
