import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CancelIntercompanyInvSettlementService } from './cancel-intercompany-inv-settlement.service';
import { CancelIntercompanyInvSettlementComponent } from './cancel-intercompany-inv-settlement/cancel-intercompany-inv-settlement.component';
import { CancelIntercompanyInvSettlementCustomComponent } from './cancel-intercompany-inv-settlement/cancel-intercompany-inv-settlement-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CancelIntercompanyInvSettlementComponent],
  providers: [CancelIntercompanyInvSettlementService, CancelIntercompanyInvSettlementCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CancelIntercompanyInvSettlementComponent]
})
export class CancelIntercompanyInvSettlementComponentModule {}
