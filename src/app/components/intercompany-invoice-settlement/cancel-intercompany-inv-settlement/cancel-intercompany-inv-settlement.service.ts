import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import {
  CancelIntercompanyInvSettlementResultView,
  CancelIntercompanyInvSettlementView
} from 'shared/models/viewModel/cancelIntercompanyInvSettlementView';
@Injectable()
export class CancelIntercompanyInvSettlementService {
  serviceKey = 'intercompanyInvoiceSettlementGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCancelIntercompanyInvSettlementById(id: string): Observable<CancelIntercompanyInvSettlementView> {
    const url = `${this.servicePath}/GetCancelIntercompanyInvSettlementById/id=${id}`;
    return this.dataGateway.get(url);
  }
  cancelIntercompanyInvSettlement(vmModel: CancelIntercompanyInvSettlementView): Observable<CancelIntercompanyInvSettlementResultView> {
    const url = `${this.servicePath}/CancelIntercompanyInvSettlement`;
    return this.dataGateway.post(url, vmModel);
  }
}
