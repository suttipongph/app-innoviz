import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelIntercompanyInvSettlementComponent } from './cancel-intercompany-inv-settlement.component';

describe('CancelIntercompanyInvSettlementViewComponent', () => {
  let component: CancelIntercompanyInvSettlementComponent;
  let fixture: ComponentFixture<CancelIntercompanyInvSettlementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CancelIntercompanyInvSettlementComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelIntercompanyInvSettlementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
