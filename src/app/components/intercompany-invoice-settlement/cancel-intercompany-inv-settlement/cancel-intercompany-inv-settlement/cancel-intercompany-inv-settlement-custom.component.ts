import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { CancelIntercompanyInvSettlementView } from 'shared/models/viewModel/cancelIntercompanyInvSettlementView';

const firstGroup = [
  'INTERCOMPANY_INVOICE_ID',
  'INTERCOMPANY_INVOICE_SETTLEMENT_ID',
  'DOCUMENT_STATUS',
  'SETTLE_AMOUNT',
  'SETTLE_WHT_AMOUNT',
  'SETTLE_INVOICE_AMOUNT',
  'METHOD_OF_PAYMENT_ID'
];

export class CancelIntercompanyInvSettlementCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: CancelIntercompanyInvSettlementView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<CancelIntercompanyInvSettlementView> {
    let model = new CancelIntercompanyInvSettlementView();
    model.intercompanyInvoiceSettlementGUID = EmptyGuid;
    return of(model);
  }
}
