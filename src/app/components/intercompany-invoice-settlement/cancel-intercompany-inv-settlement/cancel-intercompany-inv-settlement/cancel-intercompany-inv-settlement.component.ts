import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, RefType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { CancelIntercompanyInvSettlementView } from 'shared/models/viewModel/cancelIntercompanyInvSettlementView';
import { CancelIntercompanyInvSettlementService } from '../cancel-intercompany-inv-settlement.service';
import { CancelIntercompanyInvSettlementCustomComponent } from './cancel-intercompany-inv-settlement-custom.component';
@Component({
  selector: 'cancel-intercompany-inv-settlement',
  templateUrl: './cancel-intercompany-inv-settlement.component.html',
  styleUrls: ['./cancel-intercompany-inv-settlement.component.scss']
})
export class CancelIntercompanyInvSettlementComponent extends BaseItemComponent<CancelIntercompanyInvSettlementView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  documentReasonOptions: SelectItems[] = [];

  constructor(
    private service: CancelIntercompanyInvSettlementService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CancelIntercompanyInvSettlementCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<CancelIntercompanyInvSettlementView> {
    return this.service.getCancelIntercompanyInvSettlementById(this.id);
  }
  getInitialData(): Observable<CancelIntercompanyInvSettlementView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getDocumentReasonDropDown(RefType.Invoice.toString())).subscribe(
      ([documentReason]) => {
        this.documentReasonOptions = documentReason;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        const header: TranslateModel = { code: 'CONFIRM.CONFIRM' };
        const message: TranslateModel = { code: 'CONFIRM.90008', parameters: ['LABEL.INTERCOMPANY_INVOICE_SETTLEMENT'] };
        this.notificationService.showManualConfirmDialog(header, message);
        this.notificationService.isAccept.subscribe((isConfirm) => {
          if (isConfirm) {
            this.onSubmit(true);
          }
          this.notificationService.isAccept.observers = [];
        });
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    super.onExecuteFunction(this.service.cancelIntercompanyInvSettlement(this.model));
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onClose(): void {
    super.onBack();
  }
}
