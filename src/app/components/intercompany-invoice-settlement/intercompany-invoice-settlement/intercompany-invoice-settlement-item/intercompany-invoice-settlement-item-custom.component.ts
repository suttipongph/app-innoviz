import { TranslateService } from '@ngx-translate/core';
import { AppInjector } from 'app-injector';
import { NotificationService } from 'core/services/notification.service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppConst, EmptyGuid, IntercompanyInvoiceSettlementStatus, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { isDateFromGreaterThanDateTo } from 'shared/functions/date.function';
import { isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { IntercompanyInvoiceSettlementItemView, IntercompanyInvoiceTableItemView } from 'shared/models/viewModel';

const firstGroup = [
  'INTERCOMPANY_INVOICE_TABLE_GUID',
  'DOCUMENT_STATUS_GUID',
  'SETTLE_INVOICE_AMOUNT',
  'INTERCOMPANY_INVOICE_SETTLEMENT_ID',
  'STAGING_BATCH_STATUS',
  'INTERCOMPANY_INVOICE_SETTLEMENT_GUID',
  'REF_INTERCOMPANY_INVOICE_SETTLEMENT_GUID',
  'CN_REASON_GUID',
  'ORIG_INVOICE_ID',
  'ORIG_INVOICE_AMOUNT',
  'ORIG_TAX_INVOICE_ID',
  'ORIG_TAX_INVOICE_AMOUNT'
];

export class IntercompanyInvoiceSettlementItemCustomComponent {
  baseService: BaseServiceModel<any>;
  translateService = AppInjector.get(TranslateService);

  getFieldAccessing(model: IntercompanyInvoiceSettlementItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];

    if (isNullOrUndefOrEmptyGUID(model.intercompanyInvoiceSettlementGUID)) {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    } else {
      if (model.documentStatus_StatusId >= IntercompanyInvoiceSettlementStatus.Posted) {
        fieldAccessing.push({ filedIds: [AppConst.ALL_ID], readonly: true });
      } else {
        fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      }
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: IntercompanyInvoiceSettlementItemView): Observable<boolean> {
    if (model.documentStatus_StatusId === IntercompanyInvoiceSettlementStatus.Draft.toString()) {
      return of(false);
    } else {
      return of(true);
    }
  }
  getInitialData(): Observable<IntercompanyInvoiceSettlementItemView> {
    let model = new IntercompanyInvoiceSettlementItemView();
    model.intercompanyInvoiceSettlementGUID = EmptyGuid;
    return of(model);
  }
  sumSettleInvoiceAmount(e: any, model: IntercompanyInvoiceSettlementItemView): any {
    model.settleInvoiceAmount = model.settleAmount + model.settleWHTAmount;
  }
  transDateChangeValidate(model: IntercompanyInvoiceSettlementItemView, issuedDate: any, notis: NotificationService) {
    if (isDateFromGreaterThanDateTo(issuedDate, model.transDate)) {
      return {
        code: 'ERROR.DATE_CANNOT_LESS_THAN',
        parameters: [this.translateService.instant('LABEL.TRANSACTION_DATE'), this.translateService.instant('LABEL.ISSUED_DATE')]
      };
    } else {
      return null;
    }
  }
  settleAmountValidate(model: IntercompanyInvoiceSettlementItemView, balance: any, invoiceAmount: any, notis: NotificationService) {
    if (invoiceAmount > 0 && model.settleAmount > balance) {
      return {
        code: 'ERROR.90031',
        parameters: [this.translateService.instant('LABEL.SETTLE_AMOUNT'), this.translateService.instant('LABEL.BALANCE')]
      };
    } else if (invoiceAmount < 0 && model.settleAmount < balance) {
      return {
        code: 'ERROR.90131',
        parameters: [this.translateService.instant('LABEL.SETTLE_AMOUNT'), this.translateService.instant('LABEL.BALANCE')]
      };
    } else if ((invoiceAmount > 0 && model.settleAmount < 0) || (invoiceAmount < 0 && model.settleAmount > 0)) {
      return {
        code: 'ERROR.90080',
        parameters: [this.translateService.instant('LABEL.INVOICE_AMOUNT'), this.translateService.instant('LABEL.SETTLE_AMOUNT')]
      };
    } else {
      return null;
    }
  }
  settleWHTAmountValidate(model: IntercompanyInvoiceSettlementItemView, invoiceAmount: any, notis: NotificationService) {
    if ((invoiceAmount > 0 && model.settleWHTAmount < 0) || (invoiceAmount < 0 && model.settleWHTAmount > 0)) {
      return {
        code: 'ERROR.90080',
        parameters: [this.translateService.instant('LABEL.INVOICE_AMOUNT'), this.translateService.instant('LABEL.SETTLE_WITHHOLDING_TAX_AMOUNT')]
      };
    } else {
      return null;
    }
  }
  getDisableFunctionPost(model: IntercompanyInvoiceSettlementItemView): any {
    return !(model.documentStatus_StatusId === IntercompanyInvoiceSettlementStatus.Draft);
  }
  getDisableFunctionCancel(model: IntercompanyInvoiceSettlementItemView): any {
    return !(model.documentStatus_StatusId === IntercompanyInvoiceSettlementStatus.Posted);
  }
  setPreparingFunctionDataAndValidation(path: string, model: IntercompanyInvoiceSettlementItemView): Observable<boolean> {
    switch (path) {
      case ROUTE_FUNCTION_GEN.CANCEL_INTERCOMPANY_INV_SETTLEMENT:
        return this.baseService.service.getCancelIntercompanyInvSettlementMenuValidation(model).pipe(
          map((res) => {
            if (res) {
              return { IntercompanyInvoiceSettlementItemView: res };
            } else {
              return null;
            }
          })
        );
      default:
        break;
    }
  }
}
