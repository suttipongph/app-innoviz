import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { IntercompanyInvoiceSettlementItemView, IntercompanyInvoiceTableItemView } from 'shared/models/viewModel';
import { IntercompanyInvoiceSettlementService } from '../intercompany-invoice-settlement.service';
import { IntercompanyInvoiceSettlementItemCustomComponent } from './intercompany-invoice-settlement-item-custom.component';
@Component({
  selector: 'intercompany-invoice-settlement-item',
  templateUrl: './intercompany-invoice-settlement-item.component.html',
  styleUrls: ['./intercompany-invoice-settlement-item.component.scss']
})
export class IntercompanyInvoiceSettlementItemComponent
  extends BaseItemComponent<IntercompanyInvoiceSettlementItemView>
  implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  methodOfPaymentOptions: SelectItems[] = [];
  parentId = this.uiService.getRelatedInfoParentTableKey();
  passingObj: any;
  constructor(
    private service: IntercompanyInvoiceSettlementService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: IntercompanyInvoiceSettlementItemCustomComponent
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {
    this.passingObj = this.getPassingObject(this.currentActivatedRoute);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<IntercompanyInvoiceSettlementItemView> {
    return this.service.getIntercompanyInvoiceSettlementById(this.id);
  }
  getInitialData(): Observable<IntercompanyInvoiceSettlementItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getMethodOfPaymentDropDown()).subscribe(
      ([methodOfPayment]) => {
        this.methodOfPaymentOptions = methodOfPayment;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }

        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateIntercompanyInvoiceSettlement(this.model), isColsing);
    } else {
      super.onCreate(this.service.createIntercompanyInvoiceSettlement(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onSettleAmountChange(e: any): any {
    this.custom.sumSettleInvoiceAmount(e, this.model);
  }
  onSettleWHTAmountChange(e: any): any {
    this.custom.sumSettleInvoiceAmount(e, this.model);
  }

  onTransDateChangeValidate(): any {
    const issuedDate = this.model.intercomapnyInvoice_IssuedDate;

    return this.custom.transDateChangeValidate(this.model, issuedDate, this.notificationService);
  }
  onSettleAmountValidate(): any {
    const balance = this.passingObj.balance;
    const invoiceAmount = this.model.intercomapnyInvoice_InvoiceAmount;

    return this.custom.settleAmountValidate(this.model, balance, invoiceAmount, this.notificationService);
  }
  onSettleWHTAmountValidate(): any {
    const invoiceAmount = this.model.intercomapnyInvoice_InvoiceAmount;

    return this.custom.settleWHTAmountValidate(this.model, invoiceAmount, this.notificationService);
  }
  setFunctionOptions(model: IntercompanyInvoiceSettlementItemView): void {
    this.functionItems = [
      {
        label: 'LABEL.POST',
        disabled: this.custom.getDisableFunctionPost(model),
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.POST_INTERCOMPANY_INV_SETTLEMENT })
      },
      {
        label: 'LABEL.CANCEL',
        disabled: this.custom.getDisableFunctionCancel(model),
        command: () => this.setPreparingFunctionDataAndValidation(ROUTE_FUNCTION_GEN.CANCEL_INTERCOMPANY_INV_SETTLEMENT)
      }
    ];
    super.setFunctionOptionsMapping();
  }
  setRelatedInfoOptions(): void {
    this.relatedInfoItems = [
      {
        label: 'LABEL.INQUIRY',
        items: [
          {
            label: 'LABEL.INTERCOMPANY_INVOICE_SETTLEMENT_STAGING',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.INTERCOMPANY_INVOICE_SETTLEMENT_STAGING
              })
          }
        ]
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setPreparingFunctionDataAndValidation(path: string): void {
    this.custom.setPreparingFunctionDataAndValidation(path, this.model).subscribe((res) => {
      if (!isNullOrUndefined(res)) {
        this.toFunction({
          path: path
        });
      }
    });
  }
}
