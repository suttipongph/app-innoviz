import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntercompanyInvoiceSettlementItemComponent } from './intercompany-invoice-settlement-item.component';

describe('IntercompanyInvoiceSettlementItemViewComponent', () => {
  let component: IntercompanyInvoiceSettlementItemComponent;
  let fixture: ComponentFixture<IntercompanyInvoiceSettlementItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IntercompanyInvoiceSettlementItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntercompanyInvoiceSettlementItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
