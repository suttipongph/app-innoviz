import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IntercompanyInvoiceSettlementService } from './intercompany-invoice-settlement.service';
import { IntercompanyInvoiceSettlementListComponent } from './intercompany-invoice-settlement-list/intercompany-invoice-settlement-list.component';
import { IntercompanyInvoiceSettlementItemComponent } from './intercompany-invoice-settlement-item/intercompany-invoice-settlement-item.component';
import { IntercompanyInvoiceSettlementItemCustomComponent } from './intercompany-invoice-settlement-item/intercompany-invoice-settlement-item-custom.component';
import { IntercompanyInvoiceSettlementListCustomComponent } from './intercompany-invoice-settlement-list/intercompany-invoice-settlement-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [IntercompanyInvoiceSettlementListComponent, IntercompanyInvoiceSettlementItemComponent],
  providers: [
    IntercompanyInvoiceSettlementService,
    IntercompanyInvoiceSettlementItemCustomComponent,
    IntercompanyInvoiceSettlementListCustomComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [IntercompanyInvoiceSettlementListComponent, IntercompanyInvoiceSettlementItemComponent]
})
export class IntercompanyInvoiceSettlementComponentModule {}
