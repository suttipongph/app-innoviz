import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AccessModeView, IntercompanyInvoiceSettlementItemView, IntercompanyInvoiceSettlementListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { IntercompanyInvoiceSettlementComponentModule } from './intercompany-invoice-settlement.module';
@Injectable()
export class IntercompanyInvoiceSettlementService {
  serviceKey = 'intercompanyInvoiceSettlementGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getIntercompanyInvoiceSettlementToList(search: SearchParameter): Observable<SearchResult<IntercompanyInvoiceSettlementListView>> {
    const url = `${this.servicePath}/GetIntercompanyInvoiceSettlementList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteIntercompanyInvoiceSettlement(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteIntercompanyInvoiceSettlement`;
    return this.dataGateway.delete(url, row);
  }
  getIntercompanyInvoiceSettlementById(id: string): Observable<IntercompanyInvoiceSettlementItemView> {
    const url = `${this.servicePath}/GetIntercompanyInvoiceSettlementById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createIntercompanyInvoiceSettlement(vmModel: IntercompanyInvoiceSettlementItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateIntercompanyInvoiceSettlement`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateIntercompanyInvoiceSettlement(vmModel: IntercompanyInvoiceSettlementItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateIntercompanyInvoiceSettlement`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<IntercompanyInvoiceSettlementItemView> {
    const url = `${this.servicePath}/GetIntercompanyInvoiceSettlementInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByIntercompanyInvoiceTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByIntercompanyInvoiceTable/id=${id}`;
    return this.dataGateway.get(url);
  }
  getCancelIntercompanyInvSettlementMenuValidation(vmModel: IntercompanyInvoiceSettlementItemView): Observable<boolean> {
    const url = `${this.servicePath}/GetCancelIntercompanyInvSettlementMenuValidation`;
    return this.dataGateway.post(url, vmModel);
  }
}
