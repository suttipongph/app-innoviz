import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AccessMode, IntercompanyInvoiceSettlementStatus } from 'shared/constants';
import { isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView, IntercompanyInvoiceSettlementListView } from 'shared/models/viewModel';
import { IntercompanyInvoiceSettlementListComponent } from './intercompany-invoice-settlement-list.component';

export class IntercompanyInvoiceSettlementListCustomComponent {
  baseService: BaseServiceModel<any>;
  accessModeView: AccessModeView = new AccessModeView();
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setAccessModeByParentStatus(): Observable<AccessModeView> {
    let parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
    return this.baseService.service.getAccessModeByIntercompanyInvoiceTable(parentId).pipe(
      tap((result) => {
        this.accessModeView = result as AccessModeView;
      })
    );
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return this.accessModeView.canCreate && accessRight;
  }
  getRowAuthorize(row: IntercompanyInvoiceSettlementListView[]): boolean {
    let isSetRow: boolean = true;
    row.forEach((item) => {
      const accessMode: AccessMode =
        item.documentStatus_StatusId === IntercompanyInvoiceSettlementStatus.Draft ? AccessMode.full : AccessMode.creator;
      item.rowAuthorize = accessMode;
    });

    return isSetRow;
  }
}
