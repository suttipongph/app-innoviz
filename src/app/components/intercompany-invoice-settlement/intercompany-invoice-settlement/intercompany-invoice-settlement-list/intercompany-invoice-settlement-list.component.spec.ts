import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IntercompanyInvoiceSettlementListComponent } from './intercompany-invoice-settlement-list.component';

describe('IntercompanyInvoiceSettlementListViewComponent', () => {
  let component: IntercompanyInvoiceSettlementListComponent;
  let fixture: ComponentFixture<IntercompanyInvoiceSettlementListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IntercompanyInvoiceSettlementListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntercompanyInvoiceSettlementListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
