import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { IntercompanyInvoiceSettlementListView } from 'shared/models/viewModel';
import { IntercompanyInvoiceSettlementService } from '../intercompany-invoice-settlement.service';
import { IntercompanyInvoiceSettlementListCustomComponent } from './intercompany-invoice-settlement-list-custom.component';

@Component({
  selector: 'intercompany-invoice-settlement-list',
  templateUrl: './intercompany-invoice-settlement-list.component.html',
  styleUrls: ['./intercompany-invoice-settlement-list.component.scss']
})
export class IntercompanyInvoiceSettlementListComponent
  extends BaseListComponent<IntercompanyInvoiceSettlementListView>
  implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  defualtBitOption: SelectItems[] = [];
  methodOfPaymentOptions: SelectItems[] = [];
  constructor(public custom: IntercompanyInvoiceSettlementListCustomComponent, private service: IntercompanyInvoiceSettlementService) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getDefaultBitDropDown(this.defualtBitOption);
    this.custom.setAccessModeByParentStatus().subscribe((result) => {
      this.setDataGridOption();
    });
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getMethodOfPaymentDropDown(this.methodOfPaymentOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    this.option.key = 'intercompanyInvoiceSettlementGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.TRANSACTION_DATE',
        textKey: 'transDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'documentStatus_Values',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.WHT_SLIP_RECEIVED_FROM_CUSTOMER',
        textKey: 'whtSlipReceivedByCustomer',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defualtBitOption
      },
      {
        label: 'LABEL.SETTLE_AMOUNT',
        textKey: 'settleAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.SETTLE_WITHHOLDING_TAX_AMOUNT',
        textKey: 'settleWHTAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.SETTLE_INVOICE_AMOUNT',
        textKey: 'settleInvoiceAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.METHOD_OF_PAYMENT_ID',
        textKey: 'methodOfPayment_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.methodOfPaymentOptions,
        searchingKey: 'methodOfPaymentGUID',
        sortingKey: 'methodOfPayment_MethodOfPaymentId'
      },
      {
        label: 'LABEL.INTERCOMPANY_INVOICE_SETTLEMENT_ID',
        textKey: 'intercompanyInvoiceSettlementId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<IntercompanyInvoiceSettlementListView>> {
    return this.service.getIntercompanyInvoiceSettlementToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteIntercompanyInvoiceSettlement(row));
  }
  getRowAuthorize(row: IntercompanyInvoiceSettlementListView[]): void {
    if (this.custom.getRowAuthorize(row)) {
      row.forEach((item) => {
        item.rowAuthorize = this.getSingleRowAuthorize(item.rowAuthorize, item);
      });
    } else {
      super.getRowAuthorize(row);
    }
  }
}
