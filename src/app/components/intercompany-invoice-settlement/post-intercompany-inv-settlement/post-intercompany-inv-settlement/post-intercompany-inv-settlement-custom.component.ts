import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { PostIntercompanyInvSettlementView } from 'shared/models/viewModel/postIntercompanyInvSettlementView';

const firstGroup = ['INTERCOMPANY_INVOICE_ID', 'TRANS_DATE', 'DOCUMENT_STATUS', 'SETTLE_INVOICE_AMOUNT', 'METHOD_OF_PAYMENT_ID'];

export class PostIntercompanyInvSettlementCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: PostIntercompanyInvSettlementView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<PostIntercompanyInvSettlementView> {
    let model = new PostIntercompanyInvSettlementView();
    model.intercompanyInvoiceSettlementGUID = EmptyGuid;
    return of(model);
  }
}
