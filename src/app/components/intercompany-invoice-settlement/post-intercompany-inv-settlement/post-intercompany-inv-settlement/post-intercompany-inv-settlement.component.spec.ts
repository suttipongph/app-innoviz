import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostIntercompanyInvSettlementComponent } from './post-intercompany-inv-settlement.component';

describe('PostIntercompanyInvSettlementViewComponent', () => {
  let component: PostIntercompanyInvSettlementComponent;
  let fixture: ComponentFixture<PostIntercompanyInvSettlementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PostIntercompanyInvSettlementComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostIntercompanyInvSettlementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
