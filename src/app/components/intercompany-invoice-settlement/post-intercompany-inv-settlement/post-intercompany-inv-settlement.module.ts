import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PostIntercompanyInvSettlementService } from './post-intercompany-inv-settlement.service';
import { PostIntercompanyInvSettlementComponent } from './post-intercompany-inv-settlement/post-intercompany-inv-settlement.component';
import { PostIntercompanyInvSettlementCustomComponent } from './post-intercompany-inv-settlement/post-intercompany-inv-settlement-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PostIntercompanyInvSettlementComponent],
  providers: [PostIntercompanyInvSettlementService, PostIntercompanyInvSettlementCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PostIntercompanyInvSettlementComponent]
})
export class PostIntercompanyInvSettlementComponentModule {}
