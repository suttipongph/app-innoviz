import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import {
  PostIntercompanyInvSettlementResultView,
  PostIntercompanyInvSettlementView
} from 'shared/models/viewModel/postIntercompanyInvSettlementView';
@Injectable()
export class PostIntercompanyInvSettlementService {
  serviceKey = 'intercompanyInvoiceSettlementGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getPostIntercompanyInvSettlementById(id: string): Observable<PostIntercompanyInvSettlementView> {
    const url = `${this.servicePath}/GetPostIntercompanyInvSettlementById/id=${id}`;
    return this.dataGateway.get(url);
  }
  postIntercompanyInvSettlement(vmModel: PostIntercompanyInvSettlementView): Observable<PostIntercompanyInvSettlementResultView> {
    const url = `${this.servicePath}/PostIntercompanyInvSettlement`;
    return this.dataGateway.post(url, vmModel);
  }
}
