import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentHistoryItemComponent } from './payment-history-item.component';

describe('PaymentHistoryItemViewComponent', () => {
  let component: PaymentHistoryItemComponent;
  let fixture: ComponentFixture<PaymentHistoryItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PaymentHistoryItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentHistoryItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
