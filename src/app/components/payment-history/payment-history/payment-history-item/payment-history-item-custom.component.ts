import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { PaymentHistoryItemView } from 'shared/models/viewModel';

const firstGroup = [
  'CUSTOMER_TABLE_GUID',
  'CREDIT_APP_TABLE_GUID',
  'PRODUCT_TYPE',
  'INVOICE_TABLE_GUID',
  'PAYMENT_DATE',
  'DUE_DATE',
  'CURRENCY_GUID',
  'EXCHANGE_RATE',
  'RECEIVED_DATE',
  'RECEIVED_FROM',
  'PAYMENT_BASE_AMOUNT',
  'PAYMENT_TAX_AMOUNT',
  'PAYMENT_AMOUNT',
  'WHT_AMOUNT',
  'PAYMENT_BASE_AMOUNT_MST',
  'PAYMENT_TAX_AMOUNT_MST',
  'PAYMENT_AMOUNT_MST',
  'WHT_AMOUNT_MST',
  'WITHHOLDING_TAX_TABLE_GUID',
  'PAYMENT_TAX_BASE_AMOUNT',
  'PAYMENT_TAX_BASE_AMOUNT_MST',
  'CANCEL',
  'RECEIPT_TABLE_GUID',
  'REF_TYPE',
  'REF_ID',
  'REF_GUID',
  'CUST_TRANS_GUID',
  'PAYMENT_HISTORY_GUID',
  'WHT_SLIP_RECEIVED_BY_CUSTOMER',
  'INVOICE_SETTLEMENT_DETAIL',
  'ORIGINAL_TAX_CODE_GUID'
];

export class PaymentHistoryItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: PaymentHistoryItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: PaymentHistoryItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(): Observable<PaymentHistoryItemView> {
    let model = new PaymentHistoryItemView();
    model.paymentHistoryGUID = EmptyGuid;
    return of(model);
  }
}
