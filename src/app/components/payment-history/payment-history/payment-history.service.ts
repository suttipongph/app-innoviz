import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PaymentHistoryItemView, PaymentHistoryListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class PaymentHistoryService {
  serviceKey = 'paymentHistoryGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getPaymentHistoryToList(search: SearchParameter): Observable<SearchResult<PaymentHistoryListView>> {
    const url = `${this.servicePath}/GetPaymentHistoryList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deletePaymentHistory(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeletePaymentHistory`;
    return this.dataGateway.delete(url, row);
  }
  getPaymentHistoryById(id: string): Observable<PaymentHistoryItemView> {
    const url = `${this.servicePath}/GetPaymentHistoryById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createPaymentHistory(vmModel: PaymentHistoryItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreatePaymentHistory`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updatePaymentHistory(vmModel: PaymentHistoryItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdatePaymentHistory`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
