import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { PaymentHistoryListView } from 'shared/models/viewModel';
import { PaymentHistoryService } from '../payment-history.service';
import { PaymentHistoryListCustomComponent } from './payment-history-list-custom.component';

@Component({
  selector: 'payment-history-list',
  templateUrl: './payment-history-list.component.html',
  styleUrls: ['./payment-history-list.component.scss']
})
export class PaymentHistoryListComponent extends BaseListComponent<PaymentHistoryListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  productTypeOptions: SelectItems[] = [];
  receivedFromOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  defualtBitOption: SelectItems[] = [];
  customerTableOption: SelectItems[] = [];
  creditApplicationOption: SelectItems[] = [];

  invoiceNumberOption: SelectItems[] = [];

  constructor(public custom: PaymentHistoryListCustomComponent, private service: PaymentHistoryService) {
    super();
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getReceivedFromEnumDropDown(this.receivedFromOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
    this.baseDropdown.getDefaultBitDropDown(this.defualtBitOption);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOption);
    this.baseDropdown.getCreditAppTableDropDown(this.creditApplicationOption);
    this.baseDropdown.getInvoiceTableDropDown(this.invoiceNumberOption);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'paymentHistoryGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOption,
        sortingKey: 'customerTable_CustomerId',
        searchingKey: 'customerTableGUID'
      },
      {
        label: 'LABEL.CREDIT_APPLICATION_ID',
        textKey: 'creditAppTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditApplicationOption,
        sortingKey: 'creditAppTable_CreditAppTableId',
        searchingKey: 'creditAppTableGUID'
      },
      {
        label: 'LABEL.PRODUCT_TYPE',
        textKey: 'productType',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: 'LABEL.INVOICE_ID',
        textKey: 'invoiceTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.invoiceNumberOption,
        sortingKey: 'invoiceTable_InvoiceTableId',
        searchingKey: 'invoiceTableGUID'
      },
      {
        label: 'LABEL.PAYMENT_DATE',
        textKey: 'paymentDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.RECEIVED_DATE',
        textKey: 'receivedDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.RECEIVED_FROM',
        textKey: 'receivedFrom',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.receivedFromOptions
      },
      {
        label: 'LABEL.PAYMENT_BASE_AMOUNT',
        textKey: 'paymentBaseAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PAYMENT_TAX_AMOUNT',
        textKey: 'paymentTaxAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.PAYMENT_AMOUNT',
        textKey: 'paymentAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.WITHHOLDING_TAX_AMOUNT',
        textKey: 'whtAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.WHT_SLIP_RECEIVED_BY_CUSTOMER',
        textKey: 'whtSlipReceivedByCustomer',
        type: ColumnType.BOOLEAN,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.defualtBitOption
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<PaymentHistoryListView>> {
    return this.service.getPaymentHistoryToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deletePaymentHistory(row));
  }
}
