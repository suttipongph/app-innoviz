import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaymentHistoryService } from './payment-history.service';
import { PaymentHistoryListComponent } from './payment-history-list/payment-history-list.component';
import { PaymentHistoryItemComponent } from './payment-history-item/payment-history-item.component';
import { PaymentHistoryItemCustomComponent } from './payment-history-item/payment-history-item-custom.component';
import { PaymentHistoryListCustomComponent } from './payment-history-list/payment-history-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PaymentHistoryListComponent, PaymentHistoryItemComponent],
  providers: [PaymentHistoryService, PaymentHistoryItemCustomComponent, PaymentHistoryListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PaymentHistoryListComponent, PaymentHistoryItemComponent]
})
export class PaymentHistoryComponentModule {}
