import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { CollectionFollowUpListView } from 'shared/models/viewModel/collectionFollowUpListView';
import { CollectionFollowUpItemView } from 'shared/models/viewModel/collectionFollowUpItemView';
@Injectable()
export class CollectionFollowUpService {
  serviceKey = 'collectionFollowUpGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCollectionFollowUpToList(search: SearchParameter): Observable<SearchResult<CollectionFollowUpListView>> {
    const url = `${this.servicePath}/GetCollectionFollowUpList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCollectionFollowUp(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCollectionFollowUp`;
    return this.dataGateway.delete(url, row);
  }
  getCollectionFollowUpById(id: string): Observable<CollectionFollowUpItemView> {
    const url = `${this.servicePath}/GetCollectionFollowUpById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCollectionFollowUp(vmModel: CollectionFollowUpItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCollectionFollowUp`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCollectionFollowUp(vmModel: CollectionFollowUpItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCollectionFollowUp`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<CollectionFollowUpItemView> {
    const url = `${this.servicePath}/GetCollectionFollowUpInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
}
