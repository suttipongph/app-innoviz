import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { AccessMode, CollectionFollowUpResult, ColumnType, SortType } from 'shared/constants';
import { isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CollectionFollowUpListView } from 'shared/models/viewModel/collectionFollowUpListView';
import { CollectionFollowUpService } from '../collection-follow-up.service';
import { CollectionFollowUpListCustomComponent } from './collection-follow-up-list-custom.component';

@Component({
  selector: 'collection-follow-up-list',
  templateUrl: './collection-follow-up-list.component.html',
  styleUrls: ['./collection-follow-up-list.component.scss']
})
export class CollectionFollowUpListComponent extends BaseListComponent<CollectionFollowUpListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  collectionFollowUpResultOptions: SelectItems[] = [];
  methodOfPaymentOptions: SelectItems[] = [];
  receivedFromOptions: SelectItems[] = [];
  resultOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];

  constructor(
    public custom: CollectionFollowUpListCustomComponent,
    private service: CollectionFollowUpService,
    private currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
  }
  ngOnInit(): void {
    let passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setPassParameter(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getCollectionFollowUpResultEnumDropDown(this.collectionFollowUpResultOptions);
    this.baseDropdown.getReceivedFromEnumDropDown(this.receivedFromOptions);

    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getMethodOfPaymentDropDown(this.methodOfPaymentOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.getCanView();
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'collectionFollowUpGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.COLLECTION_DATE',
        textKey: 'collectionDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: 'LABEL.DESCRIPTION',
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.RECEIVED_FROM',
        textKey: 'receivedFrom',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.receivedFromOptions
      },
      {
        label: 'LABEL.METHOD_OF_PAYMENT_ID',
        textKey: 'methodOfPayment_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.methodOfPaymentOptions,
        searchingKey: 'methodOfPaymentGUID',
        sortingKey: 'methodOfPayment_MethodOfPaymentId'
      },
      {
        label: 'LABEL.COLLECTION_AMOUNT',
        textKey: 'collectionAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2_POS
      },
      {
        label: 'LABEL.FOLLOW_UP_RESULT',
        textKey: 'collectionFollowUpResult',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.collectionFollowUpResultOptions
      },
      {
        label: 'LABEL.NEW_COLLECTION_DATE',
        textKey: 'newCollectionDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId'
      },
      {
        label: 'LABEL.BUYER_ID',
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerTableOptions,
        searchingKey: 'buyerTableGUID',
        sortingKey: 'buyerTable_BuyerId'
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: !isNullOrUndefOrEmptyGUID(this.parentId) ? this.parentId : null
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CollectionFollowUpListView>> {
    return this.service.getCollectionFollowUpToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCollectionFollowUp(row));
  }
  getRowAuthorize(row: CollectionFollowUpListView[]): void {
    row.forEach((item) => {
      let accessMode: AccessMode = item.collectionFollowUpResult === CollectionFollowUpResult.None ? AccessMode.full : AccessMode.creator;
      item.rowAuthorize = this.getSingleRowAuthorize(accessMode, item);
    });
  }
}
