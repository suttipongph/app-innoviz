import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CollectionFollowUpListComponent } from './collection-follow-up-list.component';

describe('CollectionFollowUpListViewComponent', () => {
  let component: CollectionFollowUpListComponent;
  let fixture: ComponentFixture<CollectionFollowUpListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CollectionFollowUpListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectionFollowUpListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
