import { Observable, of } from 'rxjs';
import { isNullOrUndefined, isNullOrUndefOrEmpty } from 'shared/functions/value.function';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView } from 'shared/models/viewModel';
import { CollectionFollowUpListView } from 'shared/models/viewModel/collectionFollowUpListView';

export class CollectionFollowUpListCustomComponent {
  baseService: BaseServiceModel<any>;
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();

  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setPassParameter(model: CollectionFollowUpListView) {
    if (!isNullOrUndefOrEmpty(model) && !isNullOrUndefOrEmpty(model.accessModeView)) {
      this.accessModeView = model.accessModeView;
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
}
