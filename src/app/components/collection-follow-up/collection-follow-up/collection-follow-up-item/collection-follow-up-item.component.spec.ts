import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollectionFollowUpItemComponent } from './collection-follow-up-item.component';

describe('CollectionFollowUpItemViewComponent', () => {
  let component: CollectionFollowUpItemComponent;
  let fixture: ComponentFixture<CollectionFollowUpItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CollectionFollowUpItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectionFollowUpItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
