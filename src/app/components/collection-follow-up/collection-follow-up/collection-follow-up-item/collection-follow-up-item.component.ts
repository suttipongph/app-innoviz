import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, RefType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmpty, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, BuyerReceiptTableListView } from 'shared/models/viewModel';
import { CollectionFollowUpItemView } from 'shared/models/viewModel/collectionFollowUpItemView';
import { CollectionFollowUpService } from '../collection-follow-up.service';
import { CollectionFollowUpItemCustomComponent } from './collection-follow-up-item-custom.component';
@Component({
  selector: 'collection-follow-up-item',
  templateUrl: './collection-follow-up-item.component.html',
  styleUrls: ['./collection-follow-up-item.component.scss']
})
export class CollectionFollowUpItemComponent extends BaseItemComponent<CollectionFollowUpItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  bankGroupOptions: SelectItems[] = [];
  collectionFollowUpResultOptions: SelectItems[] = [];
  methodOfPaymentOptions: SelectItems[] = [];
  receivedFromOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];
  resultOptions: SelectItems[] = [];

  constructor(
    private service: CollectionFollowUpService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CollectionFollowUpItemCustomComponent,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
  }
  ngOnInit(): void {
    this.custom.passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setPassParameter(this.custom.passingObj);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getResultEnumDropDown(this.resultOptions);
    this.baseDropdown.getCollectionFollowUpResultEnumDropDown(this.collectionFollowUpResultOptions);
    this.baseDropdown.getReceivedFromEnumDropDown(this.receivedFromOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<CollectionFollowUpItemView> {
    return this.service.getCollectionFollowUpById(this.id);
  }
  getInitialData(): Observable<CollectionFollowUpItemView> {
    return this.service.getInitialData(this.parentId);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions(result);
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.custom.clearValueToDefault(this.model);
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getBankGroupDropDown(), this.baseDropdown.getMethodOfPaymentDropDown()).subscribe(
      ([bankGroup, methodOfPayment]) => {
        this.bankGroupOptions = bankGroup;
        this.methodOfPaymentOptions = methodOfPayment;

        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(model: CollectionFollowUpItemView): void {
    const getbuyerreceiptdisible = this.custom.getBuyerReceiptDisible(model);
    const getbuyerreceiptVisible = this.custom.getBuyerReceiptVisible(model);
    const accessModeView = !isNullOrUndefOrEmpty(this.custom.passingObj) ? this.custom.passingObj.accessModeView : new AccessModeView();
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: {
              refGUID: model.collectionFollowUpGUID,
              refType: RefType.CollectionFollowUp
            }
          })
      },
      {
        label: 'LABEL.BUYER_RECEIPT_TABLE',
        command: () =>
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.BUYER_RECEIPT_TABLE,
            parameters: {
              accessModeView: accessModeView
            }
          }),
        disabled: getbuyerreceiptdisible,
        visible: getbuyerreceiptVisible
      },
      {
        label: 'LABEL.MEMO',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.MEMO })
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateCollectionFollowUp(this.model), isColsing);
    } else {
      super.onCreate(this.service.createCollectionFollowUp(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onCollectionFollowUpResultChange() {
    super.setBaseFieldAccessing(this.custom.setFieldAccessingByCollectionFollowUpResult(this.model));
  }
}
