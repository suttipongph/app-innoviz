import { Observable, of } from 'rxjs';
import { AppConst, CollectionFollowUpResult, EmptyGuid, ReceivedFrom, Result } from 'shared/constants';
import { isNullOrUndefined, isNullOrUndefOrEmpty, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, TranslateModel } from 'shared/models/systemModel';
import { AccessModeView } from 'shared/models/viewModel';
import { CollectionFollowUpItemView } from 'shared/models/viewModel/collectionFollowUpItemView';

const firstGroup = [
  'COLLECTION_DATE',
  'CUSTOMER_TABLE_GUID',
  'BUYER_TABLE_GUID',
  'DOCUMENT_ID',
  'NEW_COLLECTION_DATE',
  'REF_TYPE',
  'REF_ID',
  'REF_GUID',
  'COLLECTION_FOLLOW_UP_GUID',
  'CREDIT_APP_LINE_GUID'
];
const condition1 = ['REMARK', 'CHEQUE_COLLECTION_RESULT', 'COLLECTION_FOLLOW_UP_RESULT'];
const condition2 = ['REMARK', 'COLLECTION_FOLLOW_UP_RESULT'];
const condition3 = ['COLLECTION_FOLLOW_UP_RESULT'];

export class CollectionFollowUpItemCustomComponent {
  baseService: BaseServiceModel<any>;
  parentName = null;
  isRequiredChequeCollectionResult: boolean = false;
  accessModeView: AccessModeView = new AccessModeView();

  passingObj: CollectionFollowUpItemView;
  getFieldAccessing(model: CollectionFollowUpItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    switch (model.collectionFollowUpResult) {
      case CollectionFollowUpResult.Confirmed:
        fieldAccessing.push({ filedIds: [AppConst.ALL_ID], readonly: true }, { filedIds: condition1, readonly: false });
        this.isRequiredChequeCollectionResult = true;
        break;
      case CollectionFollowUpResult.Postpone:
      case CollectionFollowUpResult.Extend:
        fieldAccessing.push({ filedIds: [AppConst.ALL_ID], readonly: true }, { filedIds: condition2, readonly: false });
        this.setDefaultEnumValue(model);
        this.isRequiredChequeCollectionResult = false;
        break;
      default:
        fieldAccessing.push({ filedIds: [AppConst.ALL_ID], readonly: false }, { filedIds: firstGroup, readonly: true });
        this.isRequiredChequeCollectionResult = true;
        break;
    }
    if (!isUpdateMode(model.collectionFollowUpGUID)) {
      fieldAccessing.push({ filedIds: condition3, readonly: true });
    }
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CollectionFollowUpItemView): Observable<boolean> {
    return of(
      !isUpdateMode(model.collectionFollowUpGUID) ? !(canCreate && this.accessModeView.canCreate) : !(canUpdate && this.accessModeView.canView)
    );
  }
  setPassParameter(model: any) {
    if (!isNullOrUndefOrEmpty(model) && !isNullOrUndefOrEmpty(model.accessModeView)) {
      this.accessModeView = model.accessModeView;
    }
  }
  clearValueToDefault(model: CollectionFollowUpItemView) {
    model.receivedFrom = null;
  }
  setDefaultEnumValue(model: CollectionFollowUpItemView) {
    if (isNullOrUndefined(model.chequeCollectionResult)) {
      model.chequeCollectionResult = Result.None;
    }
  }
  setFieldAccessingByCollectionFollowUpResult(model: CollectionFollowUpItemView): FieldAccessing[] {
    switch (model.collectionFollowUpResult) {
      case CollectionFollowUpResult.Confirmed:
        this.isRequiredChequeCollectionResult = true;
        return [
          { filedIds: [AppConst.ALL_ID], readonly: true },
          { filedIds: condition1, readonly: false }
        ];
      case CollectionFollowUpResult.Postpone:
      case CollectionFollowUpResult.Extend:
        this.setDefaultEnumValue(model);
        this.isRequiredChequeCollectionResult = false;
        return [
          { filedIds: [AppConst.ALL_ID], readonly: true },
          { filedIds: condition2, readonly: false }
        ];
      default:
        this.isRequiredChequeCollectionResult = true;
        return [
          { filedIds: [AppConst.ALL_ID], readonly: false },
          { filedIds: firstGroup, readonly: true }
        ];
    }
  }
  getBuyerReceiptDisible(model: CollectionFollowUpItemView): boolean {
    switch (model.receivedFrom) {
      case ReceivedFrom.Buyer:
        return false;
      case ReceivedFrom.Customer:
        return true;
    }
  }
  getBuyerReceiptVisible(model: CollectionFollowUpItemView): boolean {
    switch (model.receivedFrom) {
      case ReceivedFrom.Buyer:
        return true;
      case ReceivedFrom.Customer:
        return true;
    }
  }
  onValidateMandatory(value: any): TranslateModel {
    if (isNullOrUndefined(value)) {
      return {
        code: 'ERROR.MANDATORY_FIELD',
        parameters: []
      };
    } else {
      return null;
    }
  }
  onValidateMandatoryText(value: any): TranslateModel {
    if (isNullOrUndefOrEmpty(value)) {
      return {
        code: 'ERROR.MANDATORY_FIELD',
        parameters: []
      };
    } else {
      return null;
    }
  }
}
