import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CollectionFollowUpService } from './collection-follow-up.service';
import { CollectionFollowUpListComponent } from './collection-follow-up-list/collection-follow-up-list.component';
import { CollectionFollowUpItemComponent } from './collection-follow-up-item/collection-follow-up-item.component';
import { CollectionFollowUpItemCustomComponent } from './collection-follow-up-item/collection-follow-up-item-custom.component';
import { CollectionFollowUpListCustomComponent } from './collection-follow-up-list/collection-follow-up-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CollectionFollowUpListComponent, CollectionFollowUpItemComponent],
  providers: [CollectionFollowUpService, CollectionFollowUpItemCustomComponent, CollectionFollowUpListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CollectionFollowUpListComponent, CollectionFollowUpItemComponent]
})
export class CollectionFollowUpComponentModule {}
