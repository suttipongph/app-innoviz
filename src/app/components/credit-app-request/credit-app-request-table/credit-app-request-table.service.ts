import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import {
  AccessModeView,
  CreditAppRequestTableItemView,
  CreditAppRequestTableListView,
  InterestTypeItemView,
  InterestTypeValueItemView
} from 'shared/models/viewModel';
@Injectable()
export class CreditAppRequestTableService {
  serviceKey = 'creditAppRequestTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCreditAppRequestTableToList(search: SearchParameter): Observable<SearchResult<CreditAppRequestTableListView>> {
    const url = `${this.servicePath}/GetCreditAppRequestTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCreditAppRequestTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCreditAppRequestTable`;
    return this.dataGateway.delete(url, row);
  }
  getCreditAppRequestTableById(id: string): Observable<CreditAppRequestTableItemView> {
    const url = `${this.servicePath}/GetCreditAppRequestTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCreditAppRequestTable(vmModel: CreditAppRequestTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCreditAppRequestTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCreditAppRequestTable(vmModel: CreditAppRequestTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCreditAppRequestTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(productType: number): Observable<CreditAppRequestTableItemView> {
    const url = `${this.servicePath}/GetCreditAppRequestTableInitialData/productType=${productType}`;
    return this.dataGateway.get(url);
  }
  getLoanRequestInitialData(creditAppId: string): Observable<CreditAppRequestTableItemView> {
    const url = `${this.servicePath}/GetLoanRequestCreditAppRequestTableInitialData/CreditAppId=${creditAppId}`;
    return this.dataGateway.get(url);
  }
  getBuyerMatchingInitialData(creditAppId: string): Observable<CreditAppRequestTableItemView> {
    const url = `${this.servicePath}/GetBuyerMatchingCreditAppRequestTableInitialData/CreditAppId=${creditAppId}`;
    return this.dataGateway.get(url);
  }
  validateIsManualNumberSeq(productType: number): Observable<boolean> {
    const url = `${this.servicePath}/ValidateIsManualNumberSeq/productType=${productType}`;
    return this.dataGateway.get(url);
  }
  getInterestTypeValueByCreditAppRequest(vmModel: CreditAppRequestTableItemView): Observable<number> {
    const url = `${this.servicePath}/GetInterestTypeValueByCreditAppRequest`;
    return this.dataGateway.post(url, vmModel);
  }
  getAccessModeByCreditAppTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppTable/id=${id}`;
    return this.dataGateway.get(url);
  }
}
