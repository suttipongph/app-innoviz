import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditAppRequestTableListComponent } from './credit-app-request-table-list.component';

describe('CreditAppRequestTableListViewComponent', () => {
  let component: CreditAppRequestTableListComponent;
  let fixture: ComponentFixture<CreditAppRequestTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditAppRequestTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppRequestTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
