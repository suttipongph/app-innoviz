import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CreditAppRequestTableListView } from 'shared/models/viewModel';
import { CreditAppRequestTableService } from '../credit-app-request-table.service';
import { CreditAppRequestTableListCustomComponent } from './credit-app-request-table-list-custom.component';

@Component({
  selector: 'credit-app-request-table-list',
  templateUrl: './credit-app-request-table-list.component.html',
  styleUrls: ['./credit-app-request-table-list.component.scss']
})
export class CreditAppRequestTableListComponent extends BaseListComponent<CreditAppRequestTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  creditAppRequestTypeOptions: SelectItems[] = [];
  creditLimitExpirationOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  documentReasonOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  purchaseFeeCalculateBaseOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];
  creditLimitTypeOptions: SelectItems[] = [];
  constructor(public custom: CreditAppRequestTableListCustomComponent, private service: CreditAppRequestTableService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.setInitialListData(this.isWorkFlowMode()).subscribe((result) => {
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getCreditAppRequestTypeEnumDropDown(this.creditAppRequestTypeOptions);
    this.baseDropdown.getCreditLimitExpirationEnumDropDown(this.creditLimitExpirationOptions);
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getPurchaseFeeCalculateBaseEnumDropDown(this.purchaseFeeCalculateBaseOptions);
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCreditLimitTypeDropDown(this.creditLimitTypeOptions);
    this.baseDropdown.getCustomerTableDropDown(this.customerTableOptions);
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'creditAppRequestTableGUID';
    const columns: ColumnModel[] = [
      {
        label: this.translate.instant('LABEL.CREDIT_APPLICATION_REQUEST_ID'),
        textKey: 'creditAppRequestId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.DESC
      },
      {
        label: this.translate.instant('LABEL.CUSTOMER_ID'),
        textKey: 'customerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.customerTableOptions,
        searchingKey: 'customerTableGUID',
        sortingKey: 'customerTable_CustomerId'
      },
      {
        label: this.translate.instant('LABEL.PRODUCT_TYPE'),
        textKey: 'productType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.productTypeOptions
      },
      {
        label: this.translate.instant('LABEL.CREDIT_APPLICATION_REQUEST_TYPE'),
        textKey: 'creditAppRequestType',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditAppRequestTypeOptions
      },
      {
        label: this.translate.instant('LABEL.CREDIT_LIMIT_TYPE_ID'),
        textKey: 'creditLimitType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.creditLimitTypeOptions,
        searchingKey: 'creditLimitTypeGUID',
        sortingKey: 'creditLimitType_CreditLimitTypeId'
      },
      {
        label: this.translate.instant('LABEL.REQUEST_DATE'),
        textKey: 'requestDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: this.translate.instant('LABEL.CREDIT_LIMIT_REQUEST'),
        textKey: 'creditLimitRequest',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: this.translate.instant('LABEL.APPROVED_CREDIT_LIMIT_REQUEST'),
        textKey: 'approvedCreditLimitRequest',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: this.translate.instant('LABEL.STATUS'),
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        searchingKey: 'documentStatusGUID',
        sortingKey: 'documentStatus_StatusId',
        masterList: this.documentStatusOptions
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CreditAppRequestTableListView>> {
    return this.service.getCreditAppRequestTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCreditAppRequestTable(row));
  }
}
