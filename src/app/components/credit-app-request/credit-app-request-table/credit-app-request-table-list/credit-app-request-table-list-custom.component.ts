import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CreditAppRequestType } from 'shared/constants';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView, CreditAppRequestTableListView } from 'shared/models/viewModel';
import { CreditAppRequestTableService } from '../credit-app-request-table.service';
const CREDIT_APP_TABLE = 'creditapptable';
const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';
export class CreditAppRequestTableListCustomComponent {
  baseService: BaseServiceModel<any>;
  parentId: string = null;
  parentName: string = null;
  activeName: string = null;
  model: CreditAppRequestTableListView = new CreditAppRequestTableListView();
  buyerMatchingOrLoanReq: boolean = false;
  isWorkFlowMode: boolean = false;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setInitialListData(isWorkFlowMode: boolean): Observable<AccessModeView> {
    this.model.accessModeView = new AccessModeView();
    this.parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
    this.parentName = this.baseService.uiService.getRelatedInfoOriginTableName();
    this.activeName = this.baseService.uiService.getRelatedInfoActiveTableName();
    this.buyerMatchingOrLoanReq = this.activeName === BUYER_MATCHING || this.activeName === LOAN_REQUEST;
    switch (this.parentName) {
      case CREDIT_APP_TABLE:
        return this.getAccessModeByCreditAppTable();
      case null: // from left menu
        this.model.accessModeView.canCreate = true;
        this.model.creditAppRequestType = CreditAppRequestType.MainCreditLimit;
        return of(this.model.accessModeView);
      default:
        return of(this.model.accessModeView);
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.model.accessModeView.canCreate;
  }
  getAccessModeByCreditAppTable(): Observable<AccessModeView> {
    return (this.baseService.service as CreditAppRequestTableService).getAccessModeByCreditAppTable(this.parentId).pipe(
      tap((result) => {
        this.model.creditAppRequestType = CreditAppRequestType.LoanRequest;
        this.model.accessModeView = result;
        this.model.accessModeView.canCreate = true;
        this.model.accessModeView.canView = true;
      })
    );
  }
}
