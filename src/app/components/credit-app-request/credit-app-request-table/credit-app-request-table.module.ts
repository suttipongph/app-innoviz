import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreditAppRequestTableService } from './credit-app-request-table.service';
import { CreditAppRequestTableListComponent } from './credit-app-request-table-list/credit-app-request-table-list.component';
import { CreditAppRequestTableItemComponent } from './credit-app-request-table-item/credit-app-request-table-item.component';
import { CreditAppRequestTableItemCustomComponent } from './credit-app-request-table-item/credit-app-request-table-item-custom.component';
import { CreditAppRequestTableListCustomComponent } from './credit-app-request-table-list/credit-app-request-table-list-custom.component';
import { CreditAppRequestLineComponentModule } from '../credit-app-request-line/credit-app-request-line.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, CreditAppRequestLineComponentModule],
  declarations: [CreditAppRequestTableListComponent, CreditAppRequestTableItemComponent],
  providers: [CreditAppRequestTableService, CreditAppRequestTableItemCustomComponent, CreditAppRequestTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CreditAppRequestTableListComponent, CreditAppRequestTableItemComponent]
})
export class CreditAppRequestTableComponentModule {}
