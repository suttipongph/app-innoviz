import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditAppRequestTableItemComponent } from './credit-app-request-table-item.component';

describe('CreditAppRequestTableItemViewComponent', () => {
  let component: CreditAppRequestTableItemComponent;
  let fixture: ComponentFixture<CreditAppRequestTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditAppRequestTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppRequestTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
