import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { Dimension, ProductType, RefType, ROUTE_FUNCTION_GEN, ROUTE_RELATED_GEN } from 'shared/constants';
import { getMaxDate, getMinDate, setDate, setDateString } from 'shared/functions/date.function';
import { modelRegister } from 'shared/functions/model.function';
import {
  getDropDownLabel,
  getRelatedItemConditionDateRange,
  getRelatedItemConditionMultipleValue,
  isNullOrUndefined,
  isNullOrUndefOrEmpty,
  isNullOrUndefOrEmptyGUID,
  isUpdateMode
} from 'shared/functions/value.function';
import { MenuItem } from 'shared/models/primeModel';
import { FormValidationModel, IvzDropdownOnFocusModel, PageInformationModel, SearchCondition, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, CreditAppRequestTableItemView, LedgerDimensionItemView, RefIdParm } from 'shared/models/viewModel';
import { PrintSetBookDocTransView } from 'shared/models/viewModel/printSetBookDocTransView';
import { CreditAppRequestTableService } from '../credit-app-request-table.service';
import { CreditAppRequestTableItemCustomComponent } from './credit-app-request-table-item-custom.component';

const BUYER_MATCHING = 'buyermatching';

@Component({
  selector: 'credit-app-request-table-item',
  templateUrl: './credit-app-request-table-item.component.html',
  styleUrls: ['./credit-app-request-table-item.component.scss']
})
export class CreditAppRequestTableItemComponent extends BaseItemComponent<CreditAppRequestTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  isBuyerMatching: boolean = false;
  addressTransOptions: SelectItems[] = [];
  applicationTableOptions: SelectItems[] = [];
  blacklistStatusOptions: SelectItems[] = [];
  consortiumTableOptions: SelectItems[] = [];
  billingContactPersonTransOptions: SelectItems[] = [];
  receiptContactPersonTransOptions: SelectItems[] = [];
  creditAppRequestTypeOptions: SelectItems[] = [];
  creditAppTableOptions: SelectItems[] = [];
  creditLimitExpirationOptions: SelectItems[] = [];
  creditLimitTypeOptions: SelectItems[] = [];
  creditScoringOptions: SelectItems[] = [];
  creditTermOptions: SelectItems[] = [];
  custBankBankAccountControlOptions: SelectItems[] = [];
  custBankPDCOptions: SelectItems[] = [];
  customerTableOptions: SelectItems[] = [];
  documentReasonOptions: SelectItems[] = [];
  interestTypeOptions: SelectItems[] = [];
  kycOptions: SelectItems[] = [];
  ledgerDimensionOptions1: SelectItems[] = [];
  ledgerDimensionOptions2: SelectItems[] = [];
  ledgerDimensionOptions3: SelectItems[] = [];
  ledgerDimensionOptions4: SelectItems[] = [];
  ledgerDimensionOptions5: SelectItems[] = [];
  productSubTypeOptions: SelectItems[] = [];
  productTypeOptions: SelectItems[] = [];
  purchaseFeeCalculateBaseOptions: SelectItems[] = [];
  serviceFeeCondTemplateTableOptions: SelectItems[] = [];
  assignmentMethodOptions: SelectItems[] = [];

  constructor(
    public service: CreditAppRequestTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CreditAppRequestTableItemCustomComponent
  ) {
    super();
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {
    this.custom.setInitailData();
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {
    this.baseDropdown.getCreditAppRequestTypeEnumDropDown(this.creditAppRequestTypeOptions);
    this.baseDropdown.getCreditLimitExpirationEnumDropDown(this.creditLimitExpirationOptions);
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
    this.baseDropdown.getPurchaseFeeCalculateBaseEnumDropDown(this.purchaseFeeCalculateBaseOptions);
  }
  getById(): Observable<CreditAppRequestTableItemView> {
    return this.service.getCreditAppRequestTableById(this.id);
  }
  getInitialData(): Observable<CreditAppRequestTableItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions(result);
        this.setFunctionOptions(result);
        this.setWorkflowOption(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe(
      (result) => {
        this.model = result;
        this.onAsyncRunner();
        super.setDefaultValueSystemFields();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  onAsyncRunner(model?: CreditAppRequestTableItemView): void {
    const productType = isNullOrUndefOrEmpty(model) ? this.model.productType : model.productType;
    forkJoin(
      this.baseDropdown.getBlacklistStatusDropDown(),
      this.baseDropdown.getConsortiumTableDropDown(),
      this.baseDropdown.getCreditScoringDropDown(),
      this.baseDropdown.getCreditTermDropDown(),
      this.baseDropdown.getCustomerTableByDropDown(),
      this.baseDropdown.getDocumentReasonDropDown(RefType.CreditAppRequestTable.toString()),
      this.baseDropdown.getInterestTypeDropDown(),
      this.baseDropdown.getKYCSetupDropDown(),
      this.baseDropdown.getLedgerDimensionDropDown(),
      this.baseDropdown.getProductSubTypeByProductTypeDropDown(productType.toString()),
      this.baseDropdown.getCreditLimitTypeByProductTypeDropDown(productType.toString()),
      this.baseDropdown.getAssignmentMethodDropDown()
    ).subscribe(
      ([
        blacklistStatus,
        consortiumTable,
        creditScoring,
        creditTerm,
        customerTable,
        documentReason,
        interestType,
        kyc,
        ledgerDimension,
        productSubType,
        creditLimitType,
        assignmentMethod
      ]) => {
        this.blacklistStatusOptions = blacklistStatus;
        this.consortiumTableOptions = consortiumTable;
        this.creditScoringOptions = creditScoring;
        this.creditTermOptions = creditTerm;
        this.customerTableOptions = customerTable;
        this.documentReasonOptions = documentReason;
        this.interestTypeOptions = interestType;
        this.kycOptions = kyc;
        this.productSubTypeOptions = productSubType;
        this.creditLimitTypeOptions = creditLimitType;
        this.setLedgerDimension(ledgerDimension);
        this.assignmentMethodOptions = assignmentMethod;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model, this.isWorkFlowMode()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model, this.isWorkFlowMode()).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(model: CreditAppRequestTableItemView): void {
    const tempModel = isNullOrUndefOrEmpty(model) ? this.model : model;
    this.custom.getDisableRelelated(tempModel);
    this.custom.setInitailData();
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        command: () => {
          const refIdParm: RefIdParm = { refType: RefType.CreditAppRequestTable, refGUID: this.model.creditAppRequestTableGUID };
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: refIdParm
          });
        }
      },
      { label: 'LABEL.BOOKMARK_DOCUMENT', command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.BOOKMARK_DOCUMENT_TRANS }) },
      {
        label: 'LABEL.CUSTOMER_VISITING',
        visible: !this.custom.isLoanerOrBuyerMaching,
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.CUST_VISITING_TRANS })
      },
      {
        label: 'LABEL.INQUIRY',
        visible: isNullOrUndefOrEmptyGUID(tempModel.creditlimitType_ParentCreditLimitTypeGUID),
        items: [
          {
            label: 'LABEL.ASSIGNMENT_AGREEMENT_OUTSTANDING',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.ASSIGNMENT_AGREEMENT_OUTSTANDING })
          },
          {
            label: 'LABEL.CA_BUYER_CREDIT_OUTSTANDING',
            visible: !(this.custom.hasParentCreditLimitType || isNullOrUndefOrEmptyGUID(tempModel.refCreditAppTableGUID)),
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.CA_BUYER_CREDIT_OUTSTANDING,
                parameters: { creditAppRequestTableGUID: this.model.creditAppRequestTableGUID }
              })
          },
          {
            label: 'LABEL.CREDIT_OUTSTANDING',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.CREDIT_OUTSTANDING })
          },
          {
            label: 'LABEL.PARENT_COMPANY',
            visible: !this.custom.isLoanerOrBuyerMaching,
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.PARENT_COMPANY })
          },
          {
            label: 'LABEL.RETENTION_OUTSTANDING',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.RETENTION_TRANSACTION })
          }
          // 0022711: LIT1086_Manage Credit application request_R11
          // {
          //   label: 'LABEL.CREDIT_APPICATION',
          //   visible: !this.custom.isLoanerOrBuyerMaching,
          //   command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.CREDIT_APP_TABLE })
          // }
        ]
      },
      {
        label: 'LABEL.FINANCIAL',
        items: [
          {
            label: 'LABEL.FINANCIAL_CREDIT',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.FINANCIAL_CREDIT,
                parameters: { refGUID: this.model.creditAppRequestTableGUID, refType: RefType.CreditAppRequestTable }
              })
          },
          {
            label: 'LABEL.FINANCIAL_STATEMENT',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.FINANCIAL_STATEMENT_TRANS,
                parameters: { refGUID: this.model.creditAppRequestTableGUID, refType: RefType.CreditAppRequestTable }
              })
          },
          {
            label: 'LABEL.NCB',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.NCB_TRANS,
                parameters: { refGUID: this.model.creditAppRequestTableGUID, refType: RefType.CreditAppRequestTable }
              })
          },
          {
            label: 'LABEL.TAX_REPORT',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.TAX_REPORT_TRANS,
                parameters: { refGUID: this.model.creditAppRequestTableGUID, refType: RefType.CreditAppRequestTable }
              })
          }
        ]
      },
      {
        label: 'LABEL.MEMO',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.MEMO })
      },
      {
        label: 'LABEL.MANAGE',
        items: [
          {
            label: 'LABEL.ASSIGNMENT',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.ASSIGNMENT,
                parameters: {
                  creditAppRequestTable_Values: getDropDownLabel(model.creditAppRequestId, model.description),
                  customerTable_Values: this.custom.getCustomerValue(model.customerTableGUID, this.customerTableOptions),
                  customerTableGUID: model.customerTableGUID,
                  creditAppRequestTableGUID: model.creditAppRequestTableGUID
                }
              })
          },
          {
            label: 'LABEL.BUSINESS_COLLATERAL',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.BUSINESS_COLLATERAL,
                parameters: {
                  customerTableGUID: model.customerTableGUID,
                  creditAppRequestTableGUID: model.creditAppRequestTableGUID,
                  creditAppRequestTable_Values: getDropDownLabel(model.creditAppRequestId, model.description),
                  customerTable_Values: this.custom.getCustomerValue(model.customerTableGUID, this.customerTableOptions)
                }
              })
          },
          {
            label: 'LABEL.RETENTION_CONDITION',
            visible: !this.custom.isLoanerOrBuyerMaching,
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.RETENTION_CONDITION_TRANS })
          },
          {
            label: 'LABEL.SERVICE_FEE_CONDITION',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.SERVICE_FEE_CONDITION_TRANS })
          }
        ]
      },
      {
        label: 'LABEL.RELATED_PERSON',
        visible: !this.custom.isLoanerOrBuyerMaching,
        items: [
          {
            label: 'LABEL.AUTHORIZED_PERSON',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.AUTHORIZED_PERSON_TRANS })
          },
          {
            label: 'LABEL.GUARANTOR',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.GUARANTOR_TRANS })
          },
          {
            label: 'LABEL.OWNER',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.OWNER_TRANS })
          }
        ]
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }
  setFunctionOptions(model: CreditAppRequestTableItemView): void {
    this.functionItems = [
      {
        label: 'LABEL.CANCEL_CREDIT_APPLICATION_REQUEST',
        disabled: !this.custom.statusIsDraft(model),
        command: () =>
          this.toFunction({
            path: ROUTE_FUNCTION_GEN.CANCEL_CREDIT_APPLICATION_REQUEST,
            parameters: { creditAppRequestGUID: model.creditAppRequestTableGUID }
          })
      },
      {
        label: 'LABEL.PRINT',
        items: [
          {
            label: 'LABEL.BOOKMARK_DOCUMENT',
            command: () =>
              this.toFunction({ path: ROUTE_FUNCTION_GEN.PRINT_BOOKMARK_DOCUMENT, parameters: { model: this.setPrintSetBookDocTransData() } })
          }
        ]
      }
    ];
    const enableFunctionsWFMode: MenuItem[] = [{ label: 'LABEL.PRINT', items: [{ label: 'LABEL.BOOKMARK_DOCUMENT' }] }];
    super.setFunctionOptionsMapping(enableFunctionsWFMode);
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateCreditAppRequestTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createCreditAppRequestTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onCreditLimitExpirationChange(): void {
    this.custom.onCreditLimitExpirationChange(this.model);
    this.onCreditLimitExpirationBinding();
  }
  onCreditLimitExpirationBinding(): void {
    super.setBaseFieldAccessing(this.custom.setFieldAccessingByCreditLimitExpiration(this.model));
  }
  onCreditLimitTypeChange(): void {
    this.custom.onCreditLimitTypeChange(this.model, this.creditLimitTypeOptions);
    this.onCreditLimitExpirationChange();
    this.onCreditLimitTypeBinding();
  }
  onCreditLimitTypeBinding(): void {
    super.setBaseFieldAccessing(this.custom.onCreditLimitTypeBinding(this.model, this.creditLimitTypeOptions));
    this.getCreditAppTableByCreditAppRequestTableDropDown();
  }

  onProductTypeChange(): void {
    this.getServiceFeeCondTemplateTableByProductTypeDropDown();
  }
  onCustomerChange(): void {
    this.custom.onCustomerChange(this.model, this.customerTableOptions);
    this.onCustomerBinding();
  }
  onCustomerBinding(): void {
    this.getAddressTransByCustomerDropDown();
    this.getCustBankByBankAccountControlDropDown();
    this.getCustBankByPDCDropDown();
    this.getContactPersonTransByCustomerDropDown();
    this.getNotCancelApplicationTableByCustomerDropDown();
    this.getCreditAppTableByCreditAppRequestTableDropDown();
  }
  //#region GetDropdownByCondition
  getAddressTransByCustomerDropDown(): void {
    if (!isNullOrUndefOrEmptyGUID(this.model.customerTableGUID)) {
      this.baseDropdown.getAddressTransByCustomerDropDown(this.model.customerTableGUID).subscribe((result) => {
        this.addressTransOptions = result;
        this.custom.setRegisteredAddress(this.model, this.addressTransOptions);
        this.custom.allAddressChange(this.model, this.addressTransOptions);
      });
    } else {
      this.addressTransOptions.length = 0;
      this.custom.setRegisteredAddress(this.model, this.addressTransOptions);
      this.custom.allAddressChange(this.model, this.addressTransOptions);
    }
  }
  getCustBankByBankAccountControlDropDown(): void {
    if (!isNullOrUndefOrEmptyGUID(this.model.customerTableGUID)) {
      this.baseDropdown
        .getCustBankByBankAccountControlDropDown(this.model.customerTableGUID, this.model.bankAccountControlGUID)
        .subscribe((result) => {
          this.custBankBankAccountControlOptions = result;
        });
    } else {
      this.custBankBankAccountControlOptions.length = 0;
    }
  }
  onCustBankByBankAccountControlDropdownFocus(e: IvzDropdownOnFocusModel): void {
    super.setDropdownOptionOnFocus(e, this.model, this.baseDropdown.getCustBankByPDCDropDown(this.model.customerTableGUID)).subscribe((result) => {
      this.custBankBankAccountControlOptions = result;
    });
  }
  getCustBankByPDCDropDown(): void {
    if (!isNullOrUndefOrEmptyGUID(this.model.customerTableGUID)) {
      this.baseDropdown.getCustBankByPDCDropDown(this.model.customerTableGUID, this.model.pdcBankGUID).subscribe((result) => {
        this.custBankPDCOptions = result;
      });
    } else {
      this.custBankPDCOptions.length = 0;
    }
  }
  onCustBankByPDCDropdownFocus(e: IvzDropdownOnFocusModel): void {
    super.setDropdownOptionOnFocus(e, this.model, this.baseDropdown.getCustBankByPDCDropDown(this.model.customerTableGUID)).subscribe((result) => {
      this.custBankPDCOptions = result;
    });
  }
  getContactPersonTransByCustomerDropDown(): void {
    if (!isNullOrUndefOrEmptyGUID(this.model.customerTableGUID)) {
      this.baseService.baseDropdown
        .getContactPersonTransByCustomerDropDown(this.model.customerTableGUID, this.model.billingContactPersonGUID)
        .subscribe((result) => {
          this.billingContactPersonTransOptions = result;
        });
      this.baseService.baseDropdown
        .getContactPersonTransByCustomerDropDown(this.model.customerTableGUID, this.model.receiptContactPersonGUID)
        .subscribe((result) => {
          this.receiptContactPersonTransOptions = result;
        });
    } else {
      this.billingContactPersonTransOptions.length = 0;
      this.receiptContactPersonTransOptions.length = 0;
    }
  }
  onBillingContactPersonTransDropdownFocus(e: IvzDropdownOnFocusModel): void {
    super
      .setDropdownOptionOnFocus(e, this.model, this.baseDropdown.getContactPersonTransByCustomerDropDown(this.model.customerTableGUID))
      .subscribe((result) => {
        this.billingContactPersonTransOptions = result;
      });
  }
  onReceiptContactPersonTransDropdownFocus(e: IvzDropdownOnFocusModel): void {
    super
      .setDropdownOptionOnFocus(e, this.model, this.baseDropdown.getContactPersonTransByCustomerDropDown(this.model.customerTableGUID))
      .subscribe((result) => {
        this.receiptContactPersonTransOptions = result;
      });
  }
  getNotCancelApplicationTableByCustomerDropDown(): void {
    if (
      !isNullOrUndefOrEmptyGUID(this.model.customerTableGUID) &&
      (this.model.productType === ProductType.Leasing || this.model.productType === ProductType.HirePurchase)
    ) {
      this.baseService.baseDropdown.getNotCancelApplicationTableByCustomerDropDown(this.model.customerTableGUID).subscribe((result) => {
        this.applicationTableOptions = result;
      });
    } else {
      this.applicationTableOptions.length = 0;
    }
  }
  getServiceFeeCondTemplateTableByProductTypeDropDown(): void {
    if (!isNullOrUndefOrEmpty(this.model.productType)) {
      if (this.model.productType === ProductType.ProjectFinance) {
        this.baseService.baseDropdown
          .getServiceFeeCondTemplateTableByProductTypeDropDown(this.model.productType.toString(), this.model.extensionServiceFeeCondTemplateGUID)
          .subscribe((result) => {
            this.serviceFeeCondTemplateTableOptions = result;
          });
      } else {
        this.serviceFeeCondTemplateTableOptions.length = 0;
      }
    }
  }
  onServiceFeeCondTemplateTableDropdownFocus(e: IvzDropdownOnFocusModel): void {
    super
      .setDropdownOptionOnFocus(
        e,
        this.model,
        this.baseDropdown.getServiceFeeCondTemplateTableByProductTypeDropDown(this.model.productType.toString())
      )
      .subscribe((result) => {
        this.serviceFeeCondTemplateTableOptions = result;
      });
  }

  getCreditAppTableByCreditAppRequestTableDropDown(): void {
    if (
      this.custom.isLoanerRequest ||
      this.custom.isBuyerMatching ||
      (!isNullOrUndefOrEmpty(this.model.customerTableGUID) && this.custom.hasParentCreditLimitType && !isNullOrUndefined(this.model.requestDate))
    ) {
      this.baseService.baseDropdown
        .getCreditAppTableByCreditAppRequestTableDropDown(
          [this.model.customerTableGUID, this.custom.parentCreditLimitTypeGUID],
          [setDateString(this.model.requestDate, 1), getMaxDate()],
          this.custom.isLoanerRequest || this.custom.isBuyerMatching
        )
        .subscribe((result) => {
          this.creditAppTableOptions = result;
        });
    } else {
      this.creditAppTableOptions.length = 0;
    }
  }

  //#endregion
  onRegisteredAddressChange(): void {
    this.custom.onRegisteredAddressChange(this.model, this.addressTransOptions);
  }
  onBillingedAddressChange(): void {
    this.custom.onBillingedAddressChange(this.model, this.addressTransOptions);
  }
  onReceiptAddressChange(): void {
    this.custom.onReceiptAddressChange(this.model, this.addressTransOptions);
  }
  onInvoiceAddressChange(): void {
    this.custom.onInvoiceAddressChange(this.model, this.addressTransOptions);
  }
  onMailingReceiptAddressChange(): void {
    this.custom.onMailingReceiptAddressChange(this.model, this.addressTransOptions);
  }

  onCreditLimitRequestChange(): void {
    this.custom.onCreditLimitRequestChange(this.model);
  }
  onApprovedCreditLimitRequestChange(): void {
    this.custom.onApprovedCreditLimitRequestChange(this.model);
  }
  onCreditRequestFeePctChange(): void {
    this.custom.onCreditRequestFeePctChange(this.model);
  }
  onMaxRetentionAmountChange(): void {
    this.custom.onMaxRetentionAmountChange(this.model);
  }
  onInterestTypeChange(): void {
    this.custom.onInterestTypeChange(this.model);
  }
  onRequestDateChange(): void {
    this.custom.onRequestDateChange(this.model);
    this.getCreditAppTableByCreditAppRequestTableDropDown();
  }
  setLedgerDimension(ledgerDimension: SelectItems[]): void {
    this.ledgerDimensionOptions1 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension1);
    this.ledgerDimensionOptions2 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension2);
    this.ledgerDimensionOptions3 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension3);
    this.ledgerDimensionOptions4 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension4);
    this.ledgerDimensionOptions5 = ledgerDimension.filter((f) => (f.rowData as LedgerDimensionItemView).dimension === Dimension.Dimension5);
  }
  onInterestAdjustmentChange(): void {
    this.custom.onInterestAdjustmentChange(this.model);
  }
  //#endregion onCreditLimitExpirationChange
  //#region WF
  setWorkflowOption(model: CreditAppRequestTableItemView): void {
    super.setK2WorkflowOption(model, this.custom.getWorkFlowPath(model), this.custom.getDisabledWorkFlow(model));
  }
  //#endregion WF
  setPrintSetBookDocTransData(): PrintSetBookDocTransView {
    return this.custom.setPrintSetBookDocTransData(this.model);
  }
}
