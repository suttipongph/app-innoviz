import { Observable, of } from 'rxjs';
import { logging } from 'selenium-webdriver';
import {
  CreditAppRequestStatus,
  CreditAppRequestType,
  CreditLimitExpiration,
  IdentificationType,
  ProductType,
  RecordType,
  RefType
} from 'shared/constants';
import { toMapModel } from 'shared/functions/model.function';
import {
  getDropDownLabel,
  getProductType,
  isNullOrUndefined,
  isNullOrUndefOrEmpty,
  isNullOrUndefOrEmptyGUID,
  isUpdateMode
} from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, PathParamModel, SelectItems } from 'shared/models/systemModel';
import { AddressTransItemView, CreditAppRequestTableItemView, CreditLimitTypeItemView, CustomerTableItemView } from 'shared/models/viewModel';
import { PrintSetBookDocTransView } from 'shared/models/viewModel/printSetBookDocTransView';
import { CreditAppRequestTableService } from '../credit-app-request-table.service';

const NUMBER_SEQ = ['CREDIT_APP_REQUEST_ID'];
const CONDITION1_1 = ['MAX_PURCHASE_PCT', 'MAX_RETENTION_PCT', 'MAX_RETENTION_AMOUNT', 'PURCHASE_FEE_PCT', 'PURCHASE_FEE_CALCULATE_BASE'];
const CONDITION1_2 = ['CREDIT_TERM_GUID', 'EXTENSION_SERVICE_FEE_COND_TEMPLATE_GUID'];
const CONDITION1_3 = ['APPLICATION_TABLE_GUID'];
const CONDITION1_4 = ['START_DATE', 'EXPIRY_DATE'];
const CONDITION1_5 = ['REF_CREDIT_APP_TABLE_GUID'];
const CONDITION2 = ['ALL_ID'];
const CONDITION3 = ['CREDIT_COMMENT'];
const CONDITION4 = ['CREDIT_COMMENT', 'APPROVER_COMMENT', 'APPROVED_DATE'];
const CONDITION4_AND5_MAIN = ['CREDIT_REQUEST_FEE_PCT', 'CREDIT_REQUEST_FEE_AMOUNT'];
const CONDITION5 = ['APPROVED_DATE', 'APPROVER_COMMENT'];
const CONDITION4_1AND5_1_MAIN = ['MAX_RETENTION_PCT', 'MAX_RETENTION_AMOUNT'];
const READONLY_WHEN_CREATE = [
  'PRODUCT_TYPE',
  'CREDIT_APP_REQUEST_TYPE',
  'DOCUMENT_STATUS_GUID',
  'REF_CREDIT_APP_TABLE_GUID',
  'TAX_IDENTIFICATION_ID',
  'DATE_OF_ESTABLISH',
  'LINE_OF_BUSINESS_ID',
  'BUSINESS_TYPE_ID',
  'INTRODUCED_BY_ID',
  'UNBOUND_REGISTERED_ADDRESS',
  'CREDIT_LIMIT_EXPIRATION',
  'START_DATE',
  'EXPIRY_DATE',
  'TOTAL_INTEREST_PCT',
  'BLACKLIST_STATUS_GUID',
  'UNBOUND_BILLING_ADDRESS',
  'UNBOUND_RECEIPT_ADDRESS',
  'UNBOUND_INVOICE_ADDRESS',
  'UNBOUND_MAILING_RECEIPT_ADDRESS',
  'MAX_PURCHASE_PCT',
  'MAX_RETENTION_PCT',
  'MAX_RETENTION_AMOUNT',
  'PURCHASE_FEE_PCT',
  'PURCHASE_FEE_CALCULATE_BASE',
  'NUMBER_OF_REGISTERED_BUYER',
  'CREDIT_TERM_GUID',
  'EXTENSION_SERVICE_FEE_COND_TEMPLATE_GUID',
  'APPROVED_CREDIT_LIMIT_REQUEST',
  'APPROVED_DATE',
  'CREDIT_COMMENT',
  'APPROVER_COMMENT',
  'CUSTOMER_CREDIT_LIMIT',
  'CUSTOMER_ALL_BUYER_OUTSTANDING'
];
const READONLY_WHEN_UPDATE = ['CREDIT_APP_REQUEST_ID', 'CREDIT_LIMIT_TYPE_GUID', 'CUSTOMER_TABLE_GUID'];
const LOAN_REQUEST = 'loanrequest';
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const LOAN_REQUEST_AND_BUYER_MATCHING_GROUP = [
  'DESCRIPTION',
  'PRODUCT_SUB_TYPE_GUID',
  'REQUEST_DATE',
  'DOCUMENT_REASON_GUID',
  'REMARK',
  'MARKETING_COMMENT'
];
const BUYER_MATCHING = 'buyermatching';
const APPROVED_CREDIT_LIMIT_REQUEST = ['APPROVED_CREDIT_LIMIT_REQUEST'];
export class CreditAppRequestTableItemCustomComponent {
  baseService: BaseServiceModel<CreditAppRequestTableService>;
  isManual: boolean = false;
  bySpecific: boolean = false;
  hasParentCreditLimitType: boolean = false;
  parentCreditLimitTypeGUID: string = null;
  interestTypeValue: number = 0;
  parentId: string = null;
  parentName: string = null;
  activeName: string = null;
  masterRoute: string = null;
  isLoanerRequest: boolean = false;
  isBuyerMatching: boolean = false;
  isCreditAppRequest: boolean = false;
  isDraft: boolean = false;
  OriginTableName: string = null;
  //#region  FieldAccessing
  isLoanerOrBuyerMaching: boolean = false;
  canActiveByStatus(status: string): boolean {
    return (
      status === CreditAppRequestStatus.WaitingForMarketingStaff ||
      status === CreditAppRequestStatus.WaitingForCreditStaff ||
      status === CreditAppRequestStatus.WaitingForCreditHead ||
      status === CreditAppRequestStatus.WaitingForAssistMD
    );
  }
  getFieldAccessing(model: CreditAppRequestTableItemView, isWorkFlowMode: boolean): Observable<FieldAccessing[]> {
    let fieldAccessing: FieldAccessing[] = [];
    this.getDisableRelelated(model);
    return new Observable((observer) => {
      if (!this.canActiveByStatus(model.documentStatus_StatusId) && isWorkFlowMode && !this.statusIsDraft(model)) {
        observer.next(fieldAccessing);
        return;
      }

      fieldAccessing.push(...this.setFieldAccessingByStatus(model));
      fieldAccessing.push(...this.setFieldAccessingByCreditLimitExpiration(model));
      if (!isUpdateMode(model.creditAppRequestTableGUID)) {
        this.validateIsManualNumberSeq(model.productType).subscribe(
          (result) => {
            this.isManual = result;
            fieldAccessing.push({ filedIds: NUMBER_SEQ, readonly: !this.isManual });
            observer.next(fieldAccessing);
          },
          (error) => {
            observer.next(fieldAccessing);
          }
        );
      } else {
        fieldAccessing.push({ filedIds: READONLY_WHEN_UPDATE, readonly: true });
        observer.next(fieldAccessing);
      }
    });
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CreditAppRequestTableItemView, isWorkFlowMode: boolean): Observable<boolean> {
    const accessright = isNullOrUndefOrEmptyGUID(model.creditAppRequestTableGUID) ? canCreate : canUpdate;
    const accessLogic = this.statusIsDraft(model) ? true : this.canActiveByStatus(model.documentStatus_StatusId) && isWorkFlowMode;
    return of(!(accessLogic && accessright));
  }
  validateIsManualNumberSeq(productType: number): Observable<boolean> {
    return this.baseService.service.validateIsManualNumberSeq(productType);
  }
  setFieldAccessingByStatus(model: CreditAppRequestTableItemView): FieldAccessing[] {
    let fieldAccessing: FieldAccessing[] = []; // all field
    // <= 110
    if (model.documentStatus_StatusId <= CreditAppRequestStatus.WaitingForMarketingStaff) {
      fieldAccessing.push(...this.setFieldAccessingByProductType(model));
      return fieldAccessing;
    }
    if (model.documentStatus_StatusId === CreditAppRequestStatus.WaitingForCreditStaff) {
      // 130
      fieldAccessing.push(
        ...[
          { filedIds: CONDITION2, readonly: true },
          { filedIds: CONDITION3, readonly: false }
        ]
      );
      return fieldAccessing;
    } else if (model.documentStatus_StatusId === CreditAppRequestStatus.WaitingForCreditHead) {
      // 140
      fieldAccessing.push(
        ...[
          { filedIds: CONDITION2, readonly: true },
          { filedIds: CONDITION4, readonly: false }
        ]
      );
      if (!this.isLoanerOrBuyerMaching) {
        fieldAccessing.push({ filedIds: APPROVED_CREDIT_LIMIT_REQUEST, readonly: false });
      }
      if (model.creditAppRequestType === CreditAppRequestType.MainCreditLimit) {
        fieldAccessing.push({ filedIds: CONDITION4_AND5_MAIN, readonly: false });
        if (model.productType === ProductType.Factoring) {
          fieldAccessing.push({ filedIds: CONDITION4_1AND5_1_MAIN, readonly: false });
        }
      }
      return fieldAccessing;
    } else if (model.documentStatus_StatusId === CreditAppRequestStatus.WaitingForAssistMD) {
      // 150
      fieldAccessing.push(
        ...[
          { filedIds: CONDITION2, readonly: true },
          { filedIds: CONDITION5, readonly: false }
        ]
      );
      if (!this.isLoanerOrBuyerMaching) {
        fieldAccessing.push({ filedIds: APPROVED_CREDIT_LIMIT_REQUEST, readonly: false });
      }
      if (model.creditAppRequestType === CreditAppRequestType.MainCreditLimit) {
        fieldAccessing.push({ filedIds: CONDITION4_AND5_MAIN, readonly: false });
        if (model.productType === ProductType.Factoring) {
          fieldAccessing.push({ filedIds: CONDITION4_1AND5_1_MAIN, readonly: false });
        }
      }
      return fieldAccessing;
    }
    return fieldAccessing;
  }
  setFieldAccessingByProductType(model: CreditAppRequestTableItemView): FieldAccessing[] {
    const fieldAccessing: FieldAccessing[] = [{ filedIds: READONLY_WHEN_CREATE, readonly: true }];
    if (this.isLoanerRequest || this.isBuyerMatching) {
      fieldAccessing.push(
        ...[
          { filedIds: CONDITION2, readonly: true },
          { filedIds: LOAN_REQUEST_AND_BUYER_MATCHING_GROUP, readonly: false }
        ]
      );
      return fieldAccessing;
    }
    switch (model.productType) {
      case ProductType.Factoring:
        fieldAccessing.push({ filedIds: CONDITION1_1, readonly: false });
        fieldAccessing.push({ filedIds: CONDITION1_2, readonly: true });
        fieldAccessing.push({ filedIds: CONDITION1_3, readonly: true });
        break;
      case ProductType.ProjectFinance:
        fieldAccessing.push({ filedIds: CONDITION1_1, readonly: true });
        fieldAccessing.push({ filedIds: CONDITION1_2, readonly: false });
        fieldAccessing.push({ filedIds: CONDITION1_3, readonly: true });
        break;
      case ProductType.Leasing:
        fieldAccessing.push({ filedIds: CONDITION1_1, readonly: true });
        fieldAccessing.push({ filedIds: CONDITION1_2, readonly: true });
        fieldAccessing.push({ filedIds: CONDITION1_3, readonly: false });
        break;
      case ProductType.HirePurchase:
        fieldAccessing.push({ filedIds: CONDITION1_1, readonly: true });
        fieldAccessing.push({ filedIds: CONDITION1_2, readonly: true });
        fieldAccessing.push({ filedIds: CONDITION1_3, readonly: false });
        break;
      default:
        fieldAccessing.push({ filedIds: CONDITION1_3, readonly: true });
        break;
    }
    return fieldAccessing;
  }
  //#endregion FieldAccessing
  //#region  onChange

  //#region  onCustomerChange
  onCustomerChange(model: CreditAppRequestTableItemView, customerTableOptions: SelectItems[]): void {
    this.clearValueByCustomer(model);
    this.setValueByCustomer(model, customerTableOptions);
  }
  clearValueByCustomer(model: CreditAppRequestTableItemView): void {
    model.billingContactPersonGUID = null;
    model.receiptContactPersonGUID = null;
    model.bankAccountControlGUID = null;
    model.pdcBankGUID = null;
    model.billingAddressGUID = null;
    model.invoiceAddressGUID = null;
    model.mailingReceiptAddressGUID = null;
    model.receiptAddressGUID = null;
    model.registeredAddressGUID = null;
    model.applicationTableGUID = null;
    if (!this.isLoanerOrBuyerMaching) {
      model.refCreditAppTableGUID = null;
    }
  }
  setValueByCustomer(model: CreditAppRequestTableItemView, customerTableOptions: SelectItems[]): void {
    const rowData = isNullOrUndefOrEmptyGUID(model.customerTableGUID)
      ? new CustomerTableItemView()
      : (customerTableOptions.find((o) => o.value === model.customerTableGUID).rowData as CustomerTableItemView);
    const isTaxId = rowData.identificationType === IdentificationType.TaxId ? true : false;
    const isPerson = rowData.recordType === RecordType.Person ? true : false;
    model.signingCondition = rowData.signingCondition;
    model.blacklistStatusGUID = rowData.blacklistStatusGUID;
    model.kycguid = rowData.kycSetupGUID;
    model.dimension1GUID = rowData.dimension1GUID;
    model.dimension2GUID = rowData.dimension2GUID;
    model.dimension3GUID = rowData.dimension3GUID;
    model.dimension4GUID = rowData.dimension4GUID;
    model.dimension5GUID = rowData.dimension5GUID;
    model.creditScoringGUID = rowData.creditScoringGUID;
    model.lineOfBusinessId = rowData.lineOfBusiness_Values;
    model.introducedById = rowData.introducedBy_Values;
    model.businessTypeId = rowData.businessType_Values;
    model.taxIdentificationId = isTaxId ? rowData.taxID : rowData.passportID;
    model.dateOfEstablish = isPerson ? rowData.dateOfBirth : rowData.dateOfEstablish;
  }
  setRegisteredAddress(model: CreditAppRequestTableItemView, addressTransOptions: SelectItems[]): void {
    if (!isUpdateMode(model.creditAppRequestTableGUID)) {
      const isTax = addressTransOptions.find((o) => (o.rowData as AddressTransItemView).isTax === true);
      if (!this.isLoanerOrBuyerMaching) {
        model.registeredAddressGUID = !isNullOrUndefined(isTax) ? isTax.value : null;
      }
      this.onRegisteredAddressChange(model, addressTransOptions);
    }
  }

  //#endregion onCustomerChange
  //#region onCreditLimitExpirationChange
  onCreditLimitExpirationChange(model: CreditAppRequestTableItemView): void {
    if (!isNullOrUndefOrEmpty(model.creditLimitExpiration)) {
      this.bySpecific = model.creditLimitExpiration === CreditLimitExpiration.BySpecific;
    } else {
      this.bySpecific = false;
    }
    if (!this.bySpecific) {
      model.startDate = null;
      model.expiryDate = null;
    }
  }
  setFieldAccessingByCreditLimitExpiration(model: CreditAppRequestTableItemView): FieldAccessing[] {
    if (!(model.documentStatus_StatusId <= CreditAppRequestStatus.WaitingForMarketingStaff)) {
      return [];
    }
    return [{ filedIds: CONDITION1_4, readonly: !this.bySpecific }];
  }
  //#endregion onCreditLimitExpirationChange
  //#region onCreditLimitTypeChange
  onCreditLimitTypeChange(model: CreditAppRequestTableItemView, creditLimitTypeOptions: SelectItems[]): void {
    const rowData = isNullOrUndefOrEmptyGUID(model.creditLimitTypeGUID)
      ? new CreditLimitTypeItemView()
      : (creditLimitTypeOptions.find((o) => o.value === model.creditLimitTypeGUID).rowData as CreditLimitTypeItemView);
    model.creditLimitExpiration = rowData.creditLimitExpiration;
    model.creditLimitRemark = rowData.creditLimitRemark;
    if (!this.isLoanerOrBuyerMaching) {
      model.refCreditAppTableGUID = null;
    }
  }
  onCreditLimitTypeBinding(model: CreditAppRequestTableItemView, creditLimitTypeOptions: SelectItems[]): FieldAccessing[] {
    const rowData = isNullOrUndefOrEmptyGUID(model.creditLimitTypeGUID)
      ? new CreditLimitTypeItemView()
      : (creditLimitTypeOptions.find((o) => o.value === model.creditLimitTypeGUID).rowData as CreditLimitTypeItemView);
    this.hasParentCreditLimitType = !isNullOrUndefOrEmptyGUID(rowData.parentCreditLimitTypeGUID);
    this.parentCreditLimitTypeGUID = rowData.parentCreditLimitTypeGUID;
    return [{ filedIds: CONDITION1_5, readonly: !this.hasParentCreditLimitType }];
  }
  //#endregion onCreditLimitTypeChange
  //#region  onAddressChange
  getAddressTrans(addressTransGUID: string, addressTransOptions: SelectItems[]): AddressTransItemView {
    const rowData = isNullOrUndefOrEmptyGUID(addressTransGUID)
      ? new AddressTransItemView()
      : (addressTransOptions.find((o) => o.value === addressTransGUID).rowData as AddressTransItemView);
    return rowData;
  }
  onRegisteredAddressChange(model: CreditAppRequestTableItemView, addressTransOptions: SelectItems[]): void {
    const address = this.getAddressTrans(model.registeredAddressGUID, addressTransOptions);
    model.unboundRegisteredAddress = address.unboundAddress;
  }
  onBillingedAddressChange(model: CreditAppRequestTableItemView, addressTransOptions: SelectItems[]): void {
    const address = this.getAddressTrans(model.billingAddressGUID, addressTransOptions);
    model.unboundBillingAddress = address.unboundAddress;
  }
  onReceiptAddressChange(model: CreditAppRequestTableItemView, addressTransOptions: SelectItems[]): void {
    const address = this.getAddressTrans(model.receiptAddressGUID, addressTransOptions);
    model.unboundReceiptAddress = address.unboundAddress;
  }
  onInvoiceAddressChange(model: CreditAppRequestTableItemView, addressTransOptions: SelectItems[]): void {
    const address = this.getAddressTrans(model.invoiceAddressGUID, addressTransOptions);
    model.unboundInvoiceAddress = address.unboundAddress;
  }
  onMailingReceiptAddressChange(model: CreditAppRequestTableItemView, addressTransOptions: SelectItems[]): void {
    const address = this.getAddressTrans(model.mailingReceiptAddressGUID, addressTransOptions);
    model.unboundMailingReceiptAddress = address.unboundAddress;
  }
  allAddressChange(model: CreditAppRequestTableItemView, addressTransOptions: SelectItems[]): void {
    this.onBillingedAddressChange(model, addressTransOptions);
    this.onReceiptAddressChange(model, addressTransOptions);
    this.onInvoiceAddressChange(model, addressTransOptions);
    this.onMailingReceiptAddressChange(model, addressTransOptions);
  }
  //#endregion onAddressChange
  //#region  onCreditLimitRequestChange
  onCreditLimitRequestChange(model: CreditAppRequestTableItemView): void {
    this.calcCreditRequestFeeAmount(model);
    this.calcMaxRetentionAmount(model);
  }
  onApprovedCreditLimitRequestChange(model: CreditAppRequestTableItemView): void {
    this.calcCreditRequestFeeAmount(model);
    this.calcMaxRetentionAmount(model);
  }

  onCreditRequestFeePctChange(model: CreditAppRequestTableItemView): void {
    this.calcCreditRequestFeeAmount(model);
  }
  onMaxRetentionAmountChange(model: CreditAppRequestTableItemView): void {
    this.calcMaxRetentionAmount(model);
  }
  onInterestTypeChange(model: CreditAppRequestTableItemView): void {
    this.getInterestTypeValueByCreditAppRequest(model);
  }
  onRequestDateChange(model: CreditAppRequestTableItemView): void {
    if (!this.isLoanerOrBuyerMaching) {
      model.refCreditAppTableGUID = null;
    }
    this.getInterestTypeValueByCreditAppRequest(model);
  }
  onInterestAdjustmentChange(model: CreditAppRequestTableItemView): void {
    this.getInterestTypeValueByCreditAppRequest(model);
  }
  getInterestTypeValueByCreditAppRequest(model: CreditAppRequestTableItemView): void {
    if (isNullOrUndefOrEmptyGUID(model.interestTypeGUID) || isNullOrUndefOrEmptyGUID(model.requestDate)) {
      this.interestTypeValue = 0;
      this.calcTotalInterestPct(model);
      return;
    }
    this.baseService.service.getInterestTypeValueByCreditAppRequest(model).subscribe(
      (result) => {
        this.interestTypeValue = result;
        this.calcTotalInterestPct(model);
      },
      (error) => {
        model.interestTypeGUID = null;
        this.calcTotalInterestPct(model);
      }
    );
  }
  //#endregion onCreditLimitRequestChange
  //#endregion onChange
  //#region  Calculation
  calcCreditRequestFeeAmount(model: CreditAppRequestTableItemView): void {
    const value = model.approvedCreditLimitRequest === 0 ? model.creditLimitRequest : model.approvedCreditLimitRequest;
    model.creditRequestFeeAmount = (value * model.creditRequestFeePct) / 100;
  }
  calcMaxRetentionAmount(model: CreditAppRequestTableItemView): void {
    const value = model.approvedCreditLimitRequest === 0 ? model.creditLimitRequest : model.approvedCreditLimitRequest;
    model.maxRetentionAmount = value * (model.maxRetentionPct / 100);
  }
  calcTotalInterestPct(model: CreditAppRequestTableItemView): void {
    model.totalInterestPct = model.interestAdjustment + this.interestTypeValue;
  }

  //#endregion Calculation
  getProductType(): number {
    const masterRoute = this.baseService.uiService.getMasterRoute();
    return Number(getProductType(masterRoute));
  }
  statusIsDraft(model: CreditAppRequestTableItemView): boolean {
    return model.documentStatus_StatusId === CreditAppRequestStatus.Draft;
  }
  setInitailData(): void {
    this.parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
    this.parentName = this.baseService.uiService.getRelatedInfoOriginTableName();
    this.activeName = this.baseService.uiService.getRelatedInfoActiveTableName();
    this.masterRoute = this.baseService.uiService.getMasterRoute();
    switch (this.activeName) {
      case LOAN_REQUEST:
        this.isLoanerRequest = true;
        break;
      case BUYER_MATCHING:
        this.isBuyerMatching = true;
        break;
      default:
        break;
    }
  }
  getInitialData(): Observable<CreditAppRequestTableItemView> {
    switch (this.activeName) {
      case LOAN_REQUEST:
        return this.baseService.service.getLoanRequestInitialData(this.parentId);
      case BUYER_MATCHING:
        return this.baseService.service.getBuyerMatchingInitialData(this.parentId);
      default:
        return this.baseService.service.getInitialData(this.getProductType());
    }
  }
  getWorkFlowPath(model: CreditAppRequestTableItemView): PathParamModel {
    const ACTION_WORKFLOW = 'actionworkflow';
    const START_WORKFLOW = 'startworkflow';
    return model.documentStatus_StatusId === CreditAppRequestStatus.Draft ? { path: START_WORKFLOW } : { path: ACTION_WORKFLOW };
  }
  getDisabledWorkFlow(model: CreditAppRequestTableItemView): boolean {
    return !(
      model.documentStatus_StatusId >= CreditAppRequestStatus.Draft && model.documentStatus_StatusId <= CreditAppRequestStatus.WaitingForRetry
    );
  }
  isFactoring(model: CreditAppRequestTableItemView): boolean {
    return !(model.productType === ProductType.Factoring); // value for disabled
  }

  getDisableRelelated(model: CreditAppRequestTableItemView): boolean {
    this.OriginTableName = this.baseService.uiService.getMasterRoute(1);
    if (!this.isLoanerOrBuyerMaching) {
      this.isLoanerOrBuyerMaching =
        model.creditAppRequestType === CreditAppRequestType.BuyerMatching || model.creditAppRequestType === CreditAppRequestType.LoanRequest;
    }
    switch (this.activeName) {
      case LOAN_REQUEST:
      case BUYER_MATCHING:
        this.isLoanerOrBuyerMaching = true;
        break;
      case null:
        switch (this.OriginTableName) {
          case CREDIT_APP_REQUEST_TABLE:
            this.isCreditAppRequest = true;
            break;
          default:
            return false;
        }
      default:
        return false;
    }
  }
  setPrintSetBookDocTransData(model: CreditAppRequestTableItemView): PrintSetBookDocTransView {
    const paramModel = new PrintSetBookDocTransView();
    toMapModel(model, paramModel);

    paramModel.refType = RefType.CreditAppRequestTable;
    paramModel.refGUID = model.creditAppRequestTableGUID;
    paramModel.internalAgreementId = '';
    paramModel.agreementId = model.creditAppRequestId;
    paramModel.agreementDescription = model.description;
    paramModel.creditLimitTypeId = model.creditLimitType_CreditLimitTypeId;
    paramModel.creditAppRequestDescription = model.description;
    return paramModel;
  }
  getCustomerValue(customerTableGUID: string, customerTableOptions: SelectItems[]): string {
    const customer = customerTableOptions.find((f) => f.value === customerTableGUID).rowData as CustomerTableItemView;
    return getDropDownLabel(customer.customerId, customer.name);
  }
}
