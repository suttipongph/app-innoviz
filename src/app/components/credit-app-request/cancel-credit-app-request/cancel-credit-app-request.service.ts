import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CancelCreditAppRequestParamView, CancelCreditAppRequestResultView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CancelCreditAppRequestService {
  serviceKey = 'cancelCreditAppRequestGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCancelCreditAppRequestById(id: string): Observable<CancelCreditAppRequestResultView> {
    const url = `${this.servicePath}/GetCancelCreditAppRequestById/id=${id}`;
    return this.dataGateway.get(url);
  }
  cancelCreditAppRequest(vmModel: CancelCreditAppRequestParamView): Observable<CancelCreditAppRequestResultView> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vmModel);
  }
}
