import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CancelCreditAppRequestService } from './cancel-credit-app-request.service';
import { CancelCreditAppRequestComponent } from './cancel-credit-app-request/cancel-credit-app-request.component';
import { CancelCreditAppRequestCustomComponent } from './cancel-credit-app-request/cancel-credit-app-request-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CancelCreditAppRequestComponent],
  providers: [CancelCreditAppRequestService, CancelCreditAppRequestCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CancelCreditAppRequestComponent]
})
export class CancelCreditAppRequestComponentModule {}
