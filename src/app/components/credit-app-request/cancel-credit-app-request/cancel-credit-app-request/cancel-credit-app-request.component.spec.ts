import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelCreditAppRequestComponent } from './cancel-credit-app-request.component';

describe('CancelCreditAppRequestViewComponent', () => {
  let component: CancelCreditAppRequestComponent;
  let fixture: ComponentFixture<CancelCreditAppRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CancelCreditAppRequestComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelCreditAppRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
