import { Observable, of } from 'rxjs';
import { CreditAppRequestStatus } from 'shared/constants';
import { Confirmation } from 'shared/models/primeModel';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { CancelCreditAppRequestParamView, CancelCreditAppRequestResultView } from 'shared/models/viewModel';

const firstGroup = ['CREDIT_APP_REQUEST_ID', 'DESCRIPTION', 'CUSTOMER_ID', 'CUSTOMER_NAME'];

export class CancelCreditAppRequestCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getModelParam(model: CancelCreditAppRequestResultView): Observable<CancelCreditAppRequestParamView> {
    const param: CancelCreditAppRequestParamView = new CancelCreditAppRequestParamView();
    param.creditAppRequestGUID = model.creditAppRequestGUID;
    param.refLabel = 'LABEL.CANCEL_CREDIT_APPLICATION_REQUEST';
    return of(param);
  }
  getRemarkReason(id: string, options: SelectItems[]): Observable<SelectItems> {
    const item = options.find((f) => f.value === id);
    return of(item);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  showConfirmDialog(model: CancelCreditAppRequestResultView): Observable<any> {
    const confirmMessage: Confirmation = {
      header: this.baseService.translate.instant('CONFIRM.CONFIRM'),
      message: this.baseService.translate.instant('CONFIRM.90011', [model.documentStatus_Description, 'Cancelled'])
    };
    return new Observable((observ) => {
      this.baseService.notificationService.showConfirmDialog(confirmMessage);
      this.baseService.notificationService.isAccept.subscribe((isConfrim) => {
        observ.next(isConfrim);
      });
    });
  }
  getCancelCreditAppRequestById(passingObject: CancelCreditAppRequestResultView): Observable<CancelCreditAppRequestResultView> {
    return this.baseService.service.getCancelCreditAppRequestById(passingObject.creditAppRequestGUID);
  }
}
