import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class WorkFlowCreditAppRequestService {
  serviceKey = 'workFlowCreditAppRequestGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getWorkFlowCreditAppRequestToList(search: SearchParameter): Observable<SearchResult<any>> {
    const url = `${this.servicePath}/GetWorkFlowCreditAppRequestList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteWorkFlowCreditAppRequest(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteWorkFlowCreditAppRequest`;
    return this.dataGateway.delete(url, row);
  }
  getWorkFlowCreditAppRequestById(id: string): Observable<any> {
    const url = `${this.servicePath}/GetWorkFlowCreditAppRequestById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createWorkFlowCreditAppRequest(vmModel: any): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateWorkFlowCreditAppRequest`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateWorkFlowCreditAppRequest(vmModel: any): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateWorkFlowCreditAppRequest`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  startWorkflow(vmModel: any): Observable<any> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vmModel);
  }
  actionWorkflow(vmModel: any): Observable<any> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vmModel);
  }
}
