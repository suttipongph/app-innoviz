import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WorkFlowCreditAppRequestService } from './work-flow-credit-app-request.service';
import { WorkFlowCreditAppRequestComponent } from './work-flow-credit-app-request/work-flow-credit-app-request.component';
import { WorkFlowCreditAppRequestCustomComponent } from './work-flow-credit-app-request/work-flow-credit-app-request-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [WorkFlowCreditAppRequestComponent],
  providers: [WorkFlowCreditAppRequestService, WorkFlowCreditAppRequestCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [WorkFlowCreditAppRequestComponent]
})
export class WorkFlowCreditAppRequestComponentModule {}
