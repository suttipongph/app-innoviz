import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkFlowCreditAppRequestComponent } from './work-flow-credit-app-request.component';

describe('WorkFlowCreditAppRequestViewComponent', () => {
  let component: WorkFlowCreditAppRequestComponent;
  let fixture: ComponentFixture<WorkFlowCreditAppRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WorkFlowCreditAppRequestComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkFlowCreditAppRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
