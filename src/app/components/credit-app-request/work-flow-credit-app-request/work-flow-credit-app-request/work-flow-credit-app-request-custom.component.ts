import { Observable, of } from 'rxjs';
import { CreditAppRequestStatus, EmptyGuid } from 'shared/constants';
import { isNullOrUndefOrEmptyGUID } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { EmployeeTableItemView, TaskItemView, WorkFlowCreditAppRequestView } from 'shared/models/viewModel';
import { WorkFlowCreditAppRequestService } from '../work-flow-credit-app-request.service';

const firstGroup = ['CREDIT_APP_REQUEST_ID', 'DESCRIPTION', 'CREDIT_APP_REQUEST_TYPE', 'CUSTOMER_ID'];
const CREDIT_APP_TABLE = 'creditapptable';
const CREDIT_APP_LINE = 'creditappline-child';
const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';
const AMEND_CA = 'amendca';
const AMEND_CA_LINE = 'amendcaline';
export class WorkFlowCreditAppRequestCustomComponent {
  baseService: BaseServiceModel<any>;
  showActionGroup: boolean = false;
  showAssistMDGroup: boolean = false;
  activityName: string = 'Submit workflow';
  requireAssistMD: boolean = false;
  parentName: string = null;
  parentId: string = null;
  activeName: string = null;
  refCreditAppLineGUID: string = null;
  getFieldAccessing(model: WorkFlowCreditAppRequestView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    this.setFieldGroup(model);
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(getCanAction: boolean, model: WorkFlowCreditAppRequestView): Observable<boolean> {
    return of(!getCanAction);
  }
  getInitialData(): Observable<WorkFlowCreditAppRequestView> {
    let model = new WorkFlowCreditAppRequestView();
    model.workFlowCreditAppRequestGUID = EmptyGuid;
    return of(model);
  }
  setFieldGroup(model: WorkFlowCreditAppRequestView): void {
    this.showActionGroup = model.documentStatus_StatusId > CreditAppRequestStatus.Draft;
    this.showAssistMDGroup = model.documentStatus_StatusId === CreditAppRequestStatus.WaitingForCreditHead;
  }
  setActivityName(taskItem: TaskItemView): void {
    this.activityName = taskItem.actInstDestDisplayName;
  }
  setParam(model: WorkFlowCreditAppRequestView, workflowName: string, employeeTableOptions: SelectItems[], comment: string): object {
    let ob = new Object();

    ob['ParmDocGUID'] = model.creditAppRequestTableGUID;
    if (model.documentStatus_StatusId === CreditAppRequestStatus.Draft) {
      ob['ParmFolio'] = model.parmFolio;
      ob['ProcName'] = workflowName;
      ob['ParmCompany'] = model.companyGUID;
      ob['ParmDocID'] = model.creditAppRequestId;
      ob['ParmCreditAppRequestType'] = model.creditAppRequestType;
      ob['ParmMarketingHead'] = model.marketingHead;
      ob['ParmOwner'] = model.owner;
      ob['ParmOwnerBusinessUnitGUID'] = model.ownerBusinessUnitGUID;
      ob['ParmProductType'] = model.parmProductType;
      ob['ParmRefCreditAppTableGUID'] = model.refCreditAppTableGUID;
      ob['ParmCreditAppRequestTableAmendGUID'] = this.activeName === AMEND_CA ? this.baseService.uiService.getRelatedInfoActiveTableKey() : null;
      ob['ParmRefCreditAppLineGUID'] = this.refCreditAppLineGUID;
      ob['ParmCreditAppRequestLineAmendGUID'] = this.activeName === AMEND_CA_LINE ? this.baseService.uiService.getRelatedInfoActiveTableKey() : null;
      ob['ParmCustomerEmail'] = model.customerEmail;
      ob['ParmMarketingUser'] = this.baseService.userDataService.getUsernameFromToken();
    } else if (
      model.documentStatus_StatusId >= CreditAppRequestStatus.WaitingForMarketingStaff &&
      model.documentStatus_StatusId <= CreditAppRequestStatus.WaitingForRetry
    ) {
      ob['ActionName'] = model.actionName;
      ob['Comment'] = comment;
      ob['ProcessInstanceId'] = model.processInstanceId;
    }
    if (model.documentStatus_StatusId === CreditAppRequestStatus.WaitingForCreditHead) {
      const row = !isNullOrUndefOrEmptyGUID(model.assistMD)
        ? (employeeTableOptions.find((f) => f.value === model.assistMD).rowData as EmployeeTableItemView)
        : new EmployeeTableItemView();
      ob['ParmAssistMD'] = row.sysuserTable_UserName;
    }
    return ob;
  }
  getEmployeeTableWithUserDropDown(model: WorkFlowCreditAppRequestView): Observable<SelectItems[]> {
    if (model.documentStatus_StatusId === CreditAppRequestStatus.WaitingForCreditHead) {
      return this.baseService.baseDropdown.getEmployeeTableWithUserDropDown();
    }
    return of(null);
  }
  onActionChange(action: string): void {
    this.requireAssistMD = action === 'Submit to assist MD';
  }
  getWorkFlowCreditAppRequestById(passObject: WorkFlowCreditAppRequestView): Observable<any> {
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    this.activeName = this.baseService.uiService.getRelatedInfoActiveTableName();

    switch (this.parentName) {
      case CREDIT_APP_TABLE:
        this.getParentIdByCreditAppTable(passObject);
        break;
      case CREDIT_APP_LINE:
        this.parentId = passObject.creditAppRequestTableGUID;
        this.refCreditAppLineGUID = passObject.refCreditAppLineGUID;
        break;
      default:
        this.parentId = this.baseService.uiService.getWorkflowActiveTableKey();
        break;
    }

    return (this.baseService.service as WorkFlowCreditAppRequestService).getWorkFlowCreditAppRequestById(this.parentId);
  }
  getParentIdByCreditAppTable(passObject: WorkFlowCreditAppRequestView): void {
    switch (this.activeName) {
      case BUYER_MATCHING:
        this.parentId = this.baseService.uiService.getWorkflowActiveTableKey();
        break;
      case LOAN_REQUEST:
        this.parentId = this.baseService.uiService.getWorkflowActiveTableKey();
        break;
      default:
        this.parentId = passObject.creditAppRequestTableGUID;
        break;
    }
  }
}
