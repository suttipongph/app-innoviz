import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseWorkflowComponent } from 'core/components/base-workflow/base-workflow.component';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { WorkFlowCreditAppRequestView } from 'shared/models/viewModel';
import { WorkFlowCreditAppRequestService } from '../work-flow-credit-app-request.service';
import { WorkFlowCreditAppRequestCustomComponent } from './work-flow-credit-app-request-custom.component';
import { getCreditAppRequestWorkflowName } from 'shared/config/globalvar.config';

@Component({
  selector: 'work-flow-credit-app-request',
  templateUrl: './work-flow-credit-app-request.component.html',
  styleUrls: ['./work-flow-credit-app-request.component.scss']
})
export class WorkFlowCreditAppRequestComponent extends BaseWorkflowComponent<WorkFlowCreditAppRequestView> {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  actionNameOptions: SelectItems[] = [];
  creditAppRequestTypeOptions: SelectItems[] = [];
  employeeTableOptions: SelectItems[] = [];
  isStartWorkflow: boolean;
  passObject: any;
  constructor(
    private service: WorkFlowCreditAppRequestService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: WorkFlowCreditAppRequestCustomComponent,
    public uiService: UIControllerService
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.workflowName = getCreditAppRequestWorkflowName();
    this.isStartWorkflow = uiService.getWorkflowActiveTableName() === 'startworkflow';
  }
  ngOnInit(): void {
    this.passObject = this.getPassingObject(this.currentActivatedRoute);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    if (!this.isStartWorkflow) {
      super.workflowGetTaskBySerialNumber().subscribe(
        (result) => {
          this.custom.setActivityName(result);
          this.checkAccessMode();
          this.checkPageMode();
        },
        (error) => {
          throw error;
        }
      );
    } else {
      this.checkAccessMode();
      this.checkPageMode();
    }
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getCreditAppRequestTypeEnumDropDown(this.creditAppRequestTypeOptions);
  }
  getById(): Observable<WorkFlowCreditAppRequestView> {
    return this.custom.getWorkFlowCreditAppRequestById(this.passObject);
  }
  getInitialData(): Observable<WorkFlowCreditAppRequestView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    const tempModel = !isNullOrUndefined(model) ? model : this.model;
    forkJoin(this.custom.getEmployeeTableWithUserDropDown(tempModel)).subscribe(
      ([employeeTable]) => {
        this.employeeTableOptions = employeeTable;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {}
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true, this.model);
      }
    });
  }
  onSubmit(isColsing: boolean, model: WorkFlowCreditAppRequestView): void {
    const param = this.custom.setParam(this.model, this.workflowName, this.employeeTableOptions, this.comment);
    if (this.isStartWorkflow) {
      model.workflowInstance = super.setStartWorkflowParm(param);

      super.onExecuteFunction(this.service.startWorkflow(model));
    } else {
      model.workflowInstance = super.setActionWorkflowParm(param);
      super.onExecuteFunction(this.service.actionWorkflow(model));
    }
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onActionChange(): void {
    this.custom.onActionChange(this.workflow.action);
  }
}
