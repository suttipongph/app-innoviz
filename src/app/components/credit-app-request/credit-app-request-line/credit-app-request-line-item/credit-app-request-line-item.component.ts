import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { RefType, ROUTE_RELATED_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, IvzDropdownOnFocusModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { CreditAppRequestLineItemView, RefIdParm } from 'shared/models/viewModel';
import { CreditAppRequestLineService } from '../credit-app-request-line.service';
import { CreditAppRequestLineItemCustomComponent } from './credit-app-request-line-item-custom.component';
@Component({
  selector: 'credit-app-request-line-item',
  templateUrl: './credit-app-request-line-item.component.html',
  styleUrls: ['./credit-app-request-line-item.component.scss']
})
export class CreditAppRequestLineItemComponent extends BaseItemComponent<CreditAppRequestLineItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  addressTransOptions: SelectItems[] = [];
  approvalDecisionOptions: SelectItems[] = [];
  assignmentAgreementTableOptions: SelectItems[] = [];
  assignmentMethodOptions: SelectItems[] = [];
  billingDayOptions: SelectItems[] = [];
  billingResponsibleByOptions: SelectItems[] = [];
  blacklistStatusOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  billingContactPersonTransOptions: SelectItems[] = [];
  receiptContactPersonTransOptions: SelectItems[] = [];
  creditScoringOptions: SelectItems[] = [];
  creditTermOptions: SelectItems[] = [];
  kycOptions: SelectItems[] = [];
  methodOfBillingOptions: SelectItems[] = [];
  methodOfPaymentOptions: SelectItems[] = [];
  purchaseFeeCalculateBaseOptions: SelectItems[] = [];
  receiptDayOptions: SelectItems[] = [];
  dayOfMonthOptions: SelectItems[] = [];
  constructor(
    public service: CreditAppRequestLineService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: CreditAppRequestLineItemCustomComponent
  ) {
    super();
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.baseService.service = this.service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {
    this.custom.setInitailData();
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getApprovalDecisionEnumDropDown(this.approvalDecisionOptions);
    this.baseDropdown.getDayOfMonthEnumDropDown(this.dayOfMonthOptions);
    this.baseDropdown.getMethodOfBillingEnumDropDown(this.methodOfBillingOptions);
    this.baseDropdown.getPurchaseFeeCalculateBaseEnumDropDown(this.purchaseFeeCalculateBaseOptions);
  }
  getById(): Observable<CreditAppRequestLineItemView> {
    return this.service.getCreditAppRequestLineById(this.id);
  }
  getInitialData(): Observable<CreditAppRequestLineItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions(result);
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }
  onAsyncRunner(model?: CreditAppRequestLineItemView): void {
    forkJoin(
      this.baseDropdown.getAssignmentMethodDropDown(),
      this.baseDropdown.getBillingResponsibleByDropDown(),
      this.baseDropdown.getBlacklistStatusDropDown(),
      this.baseDropdown.GetBuyerTableByCreditAppRequestDropDown(),
      this.baseDropdown.getCreditScoringDropDown(),
      this.baseDropdown.getCreditTermDropDown(),
      this.baseDropdown.getKYCSetupDropDown(),
      this.baseDropdown.getMethodOfPaymentDropDown()
    ).subscribe(
      ([assignmentMethod, billingResponsibleBy, blacklistStatus, buyerTable, creditScoring, creditTerm, kyc, methodOfPayment]) => {
        this.assignmentMethodOptions = assignmentMethod;
        this.billingResponsibleByOptions = billingResponsibleBy;
        this.blacklistStatusOptions = blacklistStatus;
        this.buyerTableOptions = buyerTable;
        this.creditScoringOptions = creditScoring;
        this.creditTermOptions = creditTerm;
        this.kycOptions = kyc;
        this.methodOfPaymentOptions = methodOfPayment;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(this.getCanCreate(), this.getCanUpdate(), this.model, this.isWorkFlowMode()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(model: CreditAppRequestLineItemView): void {
    this.custom.getDisableRelelated();
    this.relatedInfoItems = [
      {
        label: 'LABEL.ATTACHMENT',
        command: () => {
          const refIdParm: RefIdParm = { refType: RefType.CreditAppRequestLine, refGUID: this.model.creditAppRequestLineGUID };
          this.toRelatedInfo({
            path: ROUTE_RELATED_GEN.ATTACHMENT,
            parameters: refIdParm
          });
        }
      },
      {
        label: 'LABEL.BUYER_AGREEMENT_TRANSACTION',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.BUYER_AGREEMENT_TRANS })
      },
      {
        label: 'LABEL.FINANCIAL',
        items: [
          {
            label: 'LABEL.FINANCIAL_STATEMENT_TRANSACTION',
            command: () =>
              this.toRelatedInfo({
                path: ROUTE_RELATED_GEN.FINANCIAL_STATEMENT_TRANS,
                parameters: { refGUID: this.model.creditAppRequestLineGUID, refType: RefType.CreditAppRequestLine }
              })
          }
        ]
      },
      {
        label: 'LABEL.MANAGE',
        items: [
          {
            label: 'LABEL.DOCUMENT_CONDITION',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.DOCUMENT_CONDITION_INFO })
          },
          {
            label: 'LABEL.SERVICE_FEE_CONDITION',
            command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.SERVICE_FEE_CONDITION_TRANS })
          }
        ]
      },
      {
        label: 'LABEL.MEMO',
        command: () => this.toRelatedInfo({ path: ROUTE_RELATED_GEN.MEMO })
      }
    ];
    super.setRelatedinfoOptionsMapping();
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateCreditAppRequestLine(this.model), isColsing);
    } else {
      super.onCreate(this.service.createCreditAppRequestLine(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onBuyerChange(): void {
    this.custom.onBuyerChange(this.model);
    this.custom.setValueByBuyer(this.model, this.buyerTableOptions);
    this.onBuyerBinding();
  }
  onBuyerBinding(): void {
    this.getAddressTransByBuyerDropDown();
    this.getContactPersonTransByBuyerDropDown();
    this.getAssignmentAgreementTableByBuyerDropDown();
  }
  //#region Field Change
  onBillingedAddressChange(): void {
    this.custom.onBillingedAddressChange(this.model, this.addressTransOptions);
  }
  onReceiptAddressChange(): void {
    this.custom.onReceiptAddressChange(this.model, this.addressTransOptions);
  }
  onInvoiceAddressChange(): void {
    this.custom.onInvoiceAddressChange(this.model, this.addressTransOptions);
  }
  onMailingReceiptAddressChange(): void {
    this.custom.onMailingReceiptAddressChange(this.model, this.addressTransOptions);
  }
  onBuyerCreditTermChange(): void {
    this.custom.onBuyerCreditTermChange(this.model, this.creditTermOptions);
  }
  onMethodOfPaymentChange(): void {
    this.custom.onMethodOfPaymentChange(this.model, this.methodOfPaymentOptions);
  }
  onAssignmentMethodChange(): void {
    this.custom.onAssignmentMethodChange(this.model, this.assignmentMethodOptions);
  }
  //#endregion
  //#region GetDropdownByCondition
  getAddressTransByBuyerDropDown(): void {
    if (!isNullOrUndefOrEmptyGUID(this.model.buyerTableGUID)) {
      this.baseDropdown.getAddressTransByBuyerDropDown(this.model.buyerTableGUID).subscribe((result) => {
        this.addressTransOptions = result;
      });
    } else {
      this.addressTransOptions.length = 0;
    }
  }
  getContactPersonTransByBuyerDropDown(): void {
    if (!isNullOrUndefOrEmptyGUID(this.model.buyerTableGUID)) {
      this.baseService.baseDropdown
        .getContactPersonTransByBuyerDropDown(this.model.buyerTableGUID, this.model.billingContactPersonGUID)
        .subscribe((result) => {
          this.billingContactPersonTransOptions = result;
        });
      this.baseService.baseDropdown
        .getContactPersonTransByBuyerDropDown(this.model.buyerTableGUID, this.model.receiptContactPersonGUID)
        .subscribe((result) => {
          this.receiptContactPersonTransOptions = result;
        });
    } else {
      this.billingContactPersonTransOptions.length = 0;
      this.receiptContactPersonTransOptions.length = 0;
    }
  }
  onBillingContactPersonTransDropdownFocus(e: IvzDropdownOnFocusModel): void {
    super
      .setDropdownOptionOnFocus(e, this.model, this.baseDropdown.getContactPersonTransByBuyerDropDown(this.model.buyerTableGUID))
      .subscribe((result) => {
        this.billingContactPersonTransOptions = result;
      });
  }
  onReceiptContactPersonTransDropdownFocus(e: IvzDropdownOnFocusModel): void {
    super
      .setDropdownOptionOnFocus(e, this.model, this.baseDropdown.getContactPersonTransByBuyerDropDown(this.model.buyerTableGUID))
      .subscribe((result) => {
        this.receiptContactPersonTransOptions = result;
      });
  }
  getAssignmentAgreementTableByBuyerDropDown(): void {
    if (!isNullOrUndefOrEmptyGUID(this.model.buyerTableGUID)) {
      this.baseDropdown
        .getAssignmentAgreementTableByBuyerDropDown(this.model.buyerTableGUID, this.model.assignmentAgreementTableGUID)
        .subscribe((result) => {
          this.assignmentAgreementTableOptions = result;
        });
    } else {
      this.assignmentAgreementTableOptions.length = 0;
    }
  }
  onAssignmentAgreementTableDropdownFocus(e: IvzDropdownOnFocusModel): void {
    super
      .setDropdownOptionOnFocus(e, this.model, this.baseDropdown.getContactPersonTransByBuyerDropDown(this.model.buyerTableGUID))
      .subscribe((result) => {
        this.assignmentAgreementTableOptions = result;
      });
  }
  //#endregion
}
