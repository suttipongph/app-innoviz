import { Observable, of } from 'rxjs';
import { CreditAppRequestStatus, ProductType } from 'shared/constants';
import { isNullOrUndefined, isNullOrUndefOrEmptyGUID, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import {
  AddressTransItemView,
  AssignmentMethodItemView,
  BuyerTableItemView,
  CreditAppRequestLineItemView,
  CreditTermItemView,
  MethodOfPaymentItemView
} from 'shared/models/viewModel';
import { CreditAppRequestLineService } from '../credit-app-request-line.service';

const firstGroup = [
  'CREDIT_APP_REQUEST_TABLE_GUID',
  'LINE_NUM',
  'REF_CREDIT_APP_TABLE_ID',
  'REF_CREDIT_APP_LINE_GUID',
  'BUSINESS_SEGMENT_ID',
  'LINE_OF_BUSINESS_ID',
  'BUSINESS_TYPE_ID',
  'DATE_OF_ESTABLISH',
  'UNBOUND_BILLING_ADDRESS',
  'UNBOUND_RECEIPT_ADDRESS',
  'UNBOUND_INVOICE_ADDRESS',
  'UNBOUND_MAILING_RECEIPT_ADDRESS',
  'BUYER_CREDIT_LIMIT',
  'REMAINING_CREDIT_LOAN_REQUEST',
  'ALL_CUSTOMER_BUYER_OUTSTANDING',
  'CUSTOMER_BUYER_OUTSTANDING',
  'BLACKLIST_STATUS_GUID',
  'MAX_PURCHASE_PCT',
  'PURCHASE_FEE_PCT',
  'PURCHASE_FEE_CALCULATE_BASE',
  'ASSIGNMENT_AGREEMENT_TABLE_GUID',
  'APPROVAL_DECISION',
  'APPROVED_CREDIT_LIMIT_LINE_REQUEST',
  'CREDIT_COMMENT',
  'APPROVER_COMMENT'
];
const CONSITION1_1 = ['MAX_PURCHASE_PCT', 'PURCHASE_FEE_PCT', 'PURCHASE_FEE_CALCULATE_BASE'];
const CONSITION1_2 = ['ASSIGNMENT_AGREEMENT_TABLE_GUID'];
const CONDITION2 = ['ALL_ID'];
const CONDITION3 = ['CREDIT_COMMENT'];
const CONDITION4 = ['CREDIT_COMMENT', 'APPROVER_COMMENT', 'APPROVED_CREDIT_LIMIT_LINE_REQUEST', 'APPROVAL_DECISION'];
const CONDITION5 = ['APPROVAL_DECISION', 'APPROVER_COMMENT', 'APPROVED_CREDIT_LIMIT_LINE_REQUEST'];
const LOAN_REQUEST = 'loanrequest';
const LOAN_REQUEST_OR_BUYER_MATCHING = [
  'CUSTOMER_CONTACT_BUYER_PERIOD',
  'BUYER_CREDIT_TERM_GUID',
  'CREDIT_TERM_DESCRIPTION',
  'BUYER_PRODUCT',
  'BILLING_DAY',
  'BILLING_DESCRIPTION',
  'BILLING_ADDRESS_GUID',
  'BILLING_CONTACT_PERSON_GUID',
  'BILLING_RESPONSIBLE_BY_GUID',
  'METHOD_OF_BILLING',
  'BILLING_REMARK',
  'RECEIPT_DAY',
  'RECEIPT_DESCRIPTION',
  'RECEIPT_ADDRESS_GUID',
  'RECEIPT_CONTACT_PERSON_GUID',
  'METHOD_OF_PAYMENT_GUID',
  'RECEIPT_REMARK',
  'INVOICE_ADDRESS_GUID',
  'MAILING_RECEIPT_ADDRESS_GUID',
  'CUSTOMER_REQUEST',
  'CREDIT_LIMIT_LINE_REQUEST',
  'ASSIGNMENT_METHOD_GUID',
  'ACCEPTANCE_DOCUMENT',
  'ACCEPTANCE_DOCUMENT_DESCRIPTION',
  'ASSIGNMENT_METHOD_REMARK',
  'PAYMENT_CONDITION',
  'LINE_CONDITION',
  'INSURANCE_CREDIT_LIMIT',
  'KYCGUID',
  'CREDIT_SCORING_GUID',
  'MARKETING_COMMENT'
];
const READONLY_WHEN_UPDATE = ['BUYER_TABLE_GUID'];
const BUYER_MATCHING = 'buyermatching';
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
export class CreditAppRequestLineItemCustomComponent {
  baseService: BaseServiceModel<CreditAppRequestLineService>;
  parentId: string = null;
  parentName: string = null;
  activeId: string = null;
  activeName: string = null;
  masterRoute: string = null;
  isLoanerRequest: boolean = false;
  isBuyerMatching: boolean = false;
  originTableName: string = null;
  isCreditAppRequest: boolean = false;
  isLoanerAndBuyerMaching: boolean = false;
  getFieldAccessing(model: CreditAppRequestLineItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if (this.canActiveByStatus(model.creditAppRequestTable_StatusId) || this.statusIsDraft(model)) {
      fieldAccessing.push(...this.setFieldAccessingByStatus(model));
      fieldAccessing.push({ filedIds: READONLY_WHEN_UPDATE, readonly: isUpdateMode(model.creditAppRequestLineGUID) });
    }
    return of(fieldAccessing);
  }
  canActiveByStatus(status: string): boolean {
    return (
      status === CreditAppRequestStatus.WaitingForMarketingStaff ||
      status === CreditAppRequestStatus.WaitingForCreditStaff ||
      status === CreditAppRequestStatus.WaitingForCreditHead ||
      status === CreditAppRequestStatus.WaitingForAssistMD
    );
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: CreditAppRequestLineItemView, isWorkFlowMode: boolean): Observable<boolean> {
    const accessright = isNullOrUndefOrEmptyGUID(model.creditAppRequestLineGUID) ? canCreate : canUpdate;
    const accessLogic = this.statusIsDraft(model) ? true : this.canActiveByStatus(model.creditAppRequestTable_StatusId) && isWorkFlowMode;
    return of(!(accessLogic && accessright));
  }
  setFieldAccessingByStatus(model: CreditAppRequestLineItemView): FieldAccessing[] {
    let fieldAccessing: FieldAccessing[] = []; // all field
    // <= 110
    if (model.creditAppRequestTable_StatusId <= CreditAppRequestStatus.WaitingForMarketingStaff) {
      fieldAccessing.push(...this.setFieldAccessingByProductType(model));
      return fieldAccessing;
    }
    // 130
    else if (model.creditAppRequestTable_StatusId === CreditAppRequestStatus.WaitingForCreditStaff) {
      fieldAccessing.push(
        ...[
          { filedIds: CONDITION2, readonly: true },
          { filedIds: CONDITION3, readonly: false }
        ]
      );
      return fieldAccessing;
    }
    // 140
    else if (model.creditAppRequestTable_StatusId === CreditAppRequestStatus.WaitingForCreditHead) {
      fieldAccessing.push(
        ...[
          { filedIds: CONDITION2, readonly: true },
          { filedIds: CONDITION4, readonly: false }
        ]
      );
      return fieldAccessing;
    } else if (model.creditAppRequestTable_StatusId === CreditAppRequestStatus.WaitingForAssistMD) {
      // 150
      fieldAccessing.push(
        ...[
          { filedIds: CONDITION2, readonly: true },
          { filedIds: CONDITION5, readonly: false }
        ]
      );
      return fieldAccessing;
    }
    return fieldAccessing;
  }
  setFieldAccessingByProductType(model: CreditAppRequestLineItemView): FieldAccessing[] {
    const fieldAccessing: FieldAccessing[] = [];
    if (this.isLoanerRequest || this.isBuyerMatching) {
      fieldAccessing.push(
        ...[
          { filedIds: CONDITION2, readonly: true },
          { filedIds: LOAN_REQUEST_OR_BUYER_MATCHING, readonly: false }
        ]
      );
    } else {
      fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    }
    if (model.creditAppRequestTable_ProductType === ProductType.Factoring) {
      fieldAccessing.push({ filedIds: CONSITION1_1, readonly: false });
    } else if (model.creditAppRequestTable_ProductType === ProductType.Leasing) {
      fieldAccessing.push({ filedIds: CONSITION1_2, readonly: false });
    }
    return fieldAccessing;
  }
  onBuyerChange(model: CreditAppRequestLineItemView): void {
    this.clearValueByBuyer(model);
    this.getCreditLimitByProductByCreditAppRequest(model);
  }
  clearValueByBuyer(model: CreditAppRequestLineItemView): void {
    model.billingAddressGUID = null;
    model.invoiceAddressGUID = null;
    model.mailingReceiptAddressGUID = null;
    model.receiptAddressGUID = null;
    model.billingContactPersonGUID = null;
    model.receiptContactPersonGUID = null;
    model.assignmentAgreementTableGUID = null;
    model.unboundBillingAddress = null;
    model.unboundReceiptAddress = null;
    model.unboundInvoiceAddress = null;
    model.unboundMailingReceiptAddress = null;
  }
  setValueByBuyer(model: CreditAppRequestLineItemView, buyerTableOptions: SelectItems[]): void {
    const rowData = isNullOrUndefOrEmptyGUID(model.buyerTableGUID)
      ? new BuyerTableItemView()
      : (buyerTableOptions.find((o) => o.value === model.buyerTableGUID).rowData as BuyerTableItemView);

    model.dateOfEstablish = rowData.dateOfEstablish;
    model.blacklistStatusGUID = rowData.blacklistStatusGUID;
    model.blacklistStatus_Values = rowData.blacklistStatus_Values;
    model.businessSegment_Values = rowData.businessSegment_Values;
    model.businessType_Values = rowData.businessType_Values;
    model.lineOfBusiness_Values = rowData.lineOfBusiness_Values;
    model.kycSetupGUID = rowData.kycSetupGUID;
    model.creditScoringGUID = rowData.creditScoringGUID;
  }
  onBuyerBinding(model: CreditAppRequestLineItemView, buyerTableOptions: SelectItems[]): void {
    if (!isUpdateMode(model.creditAppRequestLineGUID) || this.isLoanerRequest || this.isBuyerMatching) {
      this.onBuyerChange(model);
      this.setValueByBuyer(model, buyerTableOptions);
    }
  }
  onBillingedAddressChange(model: CreditAppRequestLineItemView, addressTransOptions: SelectItems[]): void {
    const address = this.getAddressTrans(model.billingAddressGUID, addressTransOptions);
    model.unboundBillingAddress = address.unboundAddress;
  }
  onReceiptAddressChange(model: CreditAppRequestLineItemView, addressTransOptions: SelectItems[]): void {
    const address = this.getAddressTrans(model.receiptAddressGUID, addressTransOptions);
    model.unboundReceiptAddress = address.unboundAddress;
  }
  onInvoiceAddressChange(model: CreditAppRequestLineItemView, addressTransOptions: SelectItems[]): void {
    const address = this.getAddressTrans(model.invoiceAddressGUID, addressTransOptions);
    model.unboundInvoiceAddress = address.unboundAddress;
  }
  onMailingReceiptAddressChange(model: CreditAppRequestLineItemView, addressTransOptions: SelectItems[]): void {
    const address = this.getAddressTrans(model.mailingReceiptAddressGUID, addressTransOptions);
    model.unboundMailingReceiptAddress = address.unboundAddress;
  }
  getAddressTrans(addressTransGUID: string, addressTransOptions: SelectItems[]): AddressTransItemView {
    const rowData = isNullOrUndefOrEmptyGUID(addressTransGUID)
      ? new AddressTransItemView()
      : (addressTransOptions.find((o) => o.value === addressTransGUID).rowData as AddressTransItemView);
    return rowData;
  }

  getCreditLimitByProductByCreditAppRequest(model: CreditAppRequestLineItemView): void {
    if (isNullOrUndefOrEmptyGUID(model.buyerTableGUID)) {
      model.buyerCreditLimit = 0;
      return;
    }
    this.baseService.service.getCreditLimitByProductByCreditAppRequest(model).subscribe((result) => {
      model.buyerCreditLimit = !isNullOrUndefined(result) ? result.creditLimit : 0;
    });
  }
  onBuyerCreditTermChange(model: CreditAppRequestLineItemView, creditTermOptions: SelectItems[]): void {
    const rowData = isNullOrUndefOrEmptyGUID(model.buyerCreditTermGUID)
      ? new CreditTermItemView()
      : (creditTermOptions.find((o) => o.value === model.buyerCreditTermGUID).rowData as CreditTermItemView);
    model.creditTermDescription = rowData.description;
  }
  onMethodOfPaymentChange(model: CreditAppRequestLineItemView, methodOfPaymentOptions: SelectItems[]): void {
    const rowData = isNullOrUndefOrEmptyGUID(model.methodOfPaymentGUID)
      ? new MethodOfPaymentItemView()
      : (methodOfPaymentOptions.find((o) => o.value === model.methodOfPaymentGUID).rowData as MethodOfPaymentItemView);
    model.receiptRemark = rowData.paymentRemark;
  }
  onAssignmentMethodChange(model: CreditAppRequestLineItemView, assignmentMethodOptions: SelectItems[]): void {
    const rowData = isNullOrUndefOrEmptyGUID(model.assignmentMethodGUID)
      ? new AssignmentMethodItemView()
      : (assignmentMethodOptions.find((o) => o.value === model.assignmentMethodGUID).rowData as AssignmentMethodItemView);
    model.assignmentMethodRemark = rowData.description;
  }
  setInitailData(): void {
    this.parentId = this.baseService.uiService.getKey('creditapprequesttable');
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableKey();
    this.activeId = this.baseService.uiService.getRelatedInfoActiveTableKey();
    this.activeName = this.baseService.uiService.getRelatedInfoActiveTableName();
    this.masterRoute = this.baseService.uiService.getMasterRoute();
    switch (this.activeName) {
      case LOAN_REQUEST:
        this.isLoanerRequest = true;
        break;
      case BUYER_MATCHING:
        this.isBuyerMatching = true;
        break;
      default:
        break;
    }
  }
  getInitialData(): Observable<CreditAppRequestLineItemView> {
    switch (this.activeName) {
      case LOAN_REQUEST:
        return this.baseService.service.getLoanRequestOrBuyerMatchingInitialData(this.activeId);
      case BUYER_MATCHING:
        return this.baseService.service.getLoanRequestOrBuyerMatchingInitialData(this.activeId);
      default:
        return this.baseService.service.getInitialData(this.parentId);
    }
  }
  statusIsDraft(model: CreditAppRequestLineItemView): boolean {
    return model.creditAppRequestTable_StatusId === CreditAppRequestStatus.Draft;
  }
  getDisableRelelated(): boolean {
    this.originTableName = this.baseService.uiService.getMasterRoute(1);
    switch (this.activeName) {
      case LOAN_REQUEST:
      case BUYER_MATCHING:
        this.isLoanerAndBuyerMaching = true;
        break;
      case null:
        switch (this.originTableName) {
          case CREDIT_APP_REQUEST_TABLE:
            this.isCreditAppRequest = true;
            break;
          default:
            return false;
        }
      default:
        return true;
        break;
    }
  }
}
