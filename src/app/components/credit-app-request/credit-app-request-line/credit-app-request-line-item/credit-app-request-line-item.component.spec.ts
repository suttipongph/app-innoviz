import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditAppRequestLineItemComponent } from './credit-app-request-line-item.component';

describe('CreditAppRequestLineItemViewComponent', () => {
  let component: CreditAppRequestLineItemComponent;
  let fixture: ComponentFixture<CreditAppRequestLineItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditAppRequestLineItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppRequestLineItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
