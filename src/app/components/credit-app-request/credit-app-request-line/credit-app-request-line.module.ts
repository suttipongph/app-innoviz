import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreditAppRequestLineService } from './credit-app-request-line.service';
import { CreditAppRequestLineListComponent } from './credit-app-request-line-list/credit-app-request-line-list.component';
import { CreditAppRequestLineItemComponent } from './credit-app-request-line-item/credit-app-request-line-item.component';
import { CreditAppRequestLineItemCustomComponent } from './credit-app-request-line-item/credit-app-request-line-item-custom.component';
import { CreditAppRequestLineListCustomComponent } from './credit-app-request-line-list/credit-app-request-line-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CreditAppRequestLineListComponent, CreditAppRequestLineItemComponent],
  providers: [CreditAppRequestLineService, CreditAppRequestLineItemCustomComponent, CreditAppRequestLineListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CreditAppRequestLineListComponent, CreditAppRequestLineItemComponent]
})
export class CreditAppRequestLineComponentModule {}
