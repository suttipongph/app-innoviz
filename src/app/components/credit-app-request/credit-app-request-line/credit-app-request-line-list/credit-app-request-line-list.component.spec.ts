import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreditAppRequestLineListComponent } from './credit-app-request-line-list.component';

describe('CreditAppRequestLineListViewComponent', () => {
  let component: CreditAppRequestLineListComponent;
  let fixture: ComponentFixture<CreditAppRequestLineListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditAppRequestLineListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditAppRequestLineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
