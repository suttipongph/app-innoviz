import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CreditAppRequestStatus } from 'shared/constants';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView } from 'shared/models/viewModel/accessModeView';
import { CreditAppRequestLineService } from '../credit-app-request-line.service';
const LOAN_REQUEST = 'loanrequest';
const BUYER_MATCHING = 'buyermatching';
export class CreditAppRequestLineListCustomComponent {
  parentId = null;
  accessModeView: AccessModeView = new AccessModeView();
  baseService: BaseServiceModel<any>;
  parentName: string = null;
  activeId: string = null;
  activeName: string = null;
  isWorkFlowMode: boolean = false;
  statusIsDraft: boolean = false;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  getAccessModeByParent(isWorkFlowMode: boolean): Observable<any> {
    this.isWorkFlowMode = isWorkFlowMode;
    this.parentName = this.baseService.uiService.getRelatedInfoParentTableKey();
    this.activeName = this.baseService.uiService.getRelatedInfoActiveTableName();
    switch (this.activeName) {
      case LOAN_REQUEST:
      case BUYER_MATCHING:
        this.parentId = this.baseService.uiService.getRelatedInfoActiveTableKey();
        return this.baseService.service.getAccessModeByCreditAppRequestTable(this.parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
            this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
            this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
            this.accessModeView.canView = true;
          })
        );
      default:
        this.parentId = this.baseService.uiService.getKey('creditapprequesttable');
        return this.baseService.service.getAccessModeByCreditAppRequestTableCreditLimitType(this.parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
            this.accessModeView.canCreate = isDraftStatus ? this.accessModeView.canCreate : this.accessModeView.canCreate && isWorkFlowMode;
            this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
            this.accessModeView.canView = true;
          })
        );
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
}
