import { Component, Input } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, ROUTE_MASTER_GEN, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { CreditAppRequestLineListView } from 'shared/models/viewModel';
import { CreditAppRequestLineService } from '../credit-app-request-line.service';
import { CreditAppRequestLineListCustomComponent } from './credit-app-request-line-list-custom.component';

@Component({
  selector: 'credit-app-request-line-list',
  templateUrl: './credit-app-request-line-list.component.html',
  styleUrls: ['./credit-app-request-line-list.component.scss']
})
export class CreditAppRequestLineListComponent extends BaseListComponent<CreditAppRequestLineListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  addressTransOptions: SelectItems[] = [];
  approvalDecisionOptions: SelectItems[] = [];
  assignmentAgreementTableOptions: SelectItems[] = [];
  assignmentMethodOptions: SelectItems[] = [];
  billingDayOptions: SelectItems[] = [];
  billingResponsibleByOptions: SelectItems[] = [];
  blacklistStatusOptions: SelectItems[] = [];
  buyerTableOptions: SelectItems[] = [];
  businessSegmentOptions: SelectItems[] = [];
  contactPersonTransOptions: SelectItems[] = [];
  creditAppRequestTableOptions: SelectItems[] = [];
  creditScoringOptions: SelectItems[] = [];
  creditTermOptions: SelectItems[] = [];
  kycOptions: SelectItems[] = [];
  methodOfBillingOptions: SelectItems[] = [];
  methodOfPaymentOptions: SelectItems[] = [];
  purchaseFeeCalculateBaseOptions: SelectItems[] = [];
  receiptDayOptions: SelectItems[] = [];
  dayOfMonthOptions: SelectItems[] = [];

  constructor(public custom: CreditAppRequestLineListCustomComponent, private service: CreditAppRequestLineService) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getKey('creditapprequesttable');
  }
  ngOnInit(): void {
    this.accessRightChildPage = 'CreditAppRequestLineListPage';
    this.redirectPath = ROUTE_MASTER_GEN.CREDIT_APP_REQUEST_LINE;
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.getAccessModeByParent(this.isWorkFlowMode()).subscribe((result) => {
      this.setDataGridOption();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getApprovalDecisionEnumDropDown(this.approvalDecisionOptions);
    this.baseDropdown.getDayOfMonthEnumDropDown(this.dayOfMonthOptions);
    this.baseDropdown.getMethodOfBillingEnumDropDown(this.methodOfBillingOptions);
    this.baseDropdown.getPurchaseFeeCalculateBaseEnumDropDown(this.purchaseFeeCalculateBaseOptions);
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(true, this.accessRightChildPage));
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getBuyerTableDropDown(this.buyerTableOptions);
    this.baseDropdown.getBusinessSegmentDropDown(this.businessSegmentOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'creditAppRequestLineGUID';
    const columns: ColumnModel[] = [
      {
        label: this.translate.instant('LABEL.LINE'),
        textKey: 'lineNum',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: this.translate.instant('LABEL.BUYER_ID'),
        textKey: 'buyerTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.buyerTableOptions,
        searchingKey: 'buyerTableGUID',
        sortingKey: 'buyerTable_BuyerId'
      },
      {
        label: this.translate.instant('LABEL.BUSINESS_SEGMENT_ID'),
        textKey: 'businessSegment_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.businessSegmentOptions,
        searchingKey: 'businessSegmentGUID',
        sortingKey: 'businessSegment_BusinessSegmentId'
      },
      {
        label: this.translate.instant('LABEL.CREDIT_LIMIT_LINE_REQUEST'),
        textKey: 'creditLimitLineRequest',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: this.translate.instant('LABEL.APPROVED_CREDIT_LIMIT_LINE_REQUEST'),
        textKey: 'approvedCreditLimitLineRequest',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: this.translate.instant('LABEL.APPROVAL_DECISION'),
        textKey: 'approvalDecision',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.approvalDecisionOptions
      },
      {
        label: null,
        textKey: 'creditapprequesttableGUID',
        type: ColumnType.MASTER,
        visibility: false,
        sorting: SortType.NONE,
        parentKey: this.custom.parentId
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<CreditAppRequestLineListView>> {
    return this.service.getCreditAppRequestLineToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteCreditAppRequestLine(row));
  }
}
