import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  AccessModeView,
  BuyerCreditLimitByProductItemView,
  CreditAppRequestLineItemView,
  CreditAppRequestLineListView
} from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CreditAppRequestLineService {
  serviceKey = 'creditAppRequestLineGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getCreditAppRequestLineToList(search: SearchParameter): Observable<SearchResult<CreditAppRequestLineListView>> {
    const url = `${this.servicePath}/GetCreditAppRequestLineList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteCreditAppRequestLine(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteCreditAppRequestLine`;
    return this.dataGateway.delete(url, row);
  }
  getCreditAppRequestLineById(id: string): Observable<CreditAppRequestLineItemView> {
    const url = `${this.servicePath}/GetCreditAppRequestLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createCreditAppRequestLine(vmModel: CreditAppRequestLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateCreditAppRequestLine`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateCreditAppRequestLine(vmModel: CreditAppRequestLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateCreditAppRequestLine`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(parentId: string): Observable<CreditAppRequestLineItemView> {
    const url = `${this.servicePath}/GetCreditAppRequestLineInitialData/creditAppRequestTableGUID=${parentId}`;
    return this.dataGateway.get(url);
  }
  getLoanRequestInitialData(parentId: string): Observable<CreditAppRequestLineItemView> {
    const url = `${this.servicePath}/GetLoanRequestCreditAppRequestLineInitialData/creditAppRequestTableGUID=${parentId}`;
    return this.dataGateway.get(url);
  }
  getLoanRequestOrBuyerMatchingInitialData(parentId: string): Observable<CreditAppRequestLineItemView> {
    const url = `${this.servicePath}/GetLoanRequestOrBuyerMatchingCreditAppRequestLineInitialData/creditAppRequestTableGUID=${parentId}`;
    return this.dataGateway.get(url);
  }
  getCreditLimitByProductByCreditAppRequest(vmModel: CreditAppRequestLineItemView): Observable<BuyerCreditLimitByProductItemView> {
    const url = `${this.servicePath}/GetCreditLimitByProductByCreditAppRequest`;
    return this.dataGateway.post(url, vmModel);
  }
  getAccessModeByCreditAppRequestTableCreditLimitType(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppRequestTableCreditLimitType/CreditAppRequestLine/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByCreditAppRequestTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppRequestTable/CreditAppRequestLine/id=${id}`;
    return this.dataGateway.get(url);
  }
}
