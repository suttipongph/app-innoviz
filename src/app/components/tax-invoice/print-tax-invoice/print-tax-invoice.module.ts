import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrintTaxInvoiceService } from './print-tax-invoice.service';
import { PrintTaxInvoiceReportCustomComponent } from './print-tax-invoice/print-tax-invoice-report-custom.component';
import { PrintTaxInvoiceReportComponent } from './print-tax-invoice/print-tax-invoice-report.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [PrintTaxInvoiceReportComponent],
  providers: [PrintTaxInvoiceService, PrintTaxInvoiceReportCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PrintTaxInvoiceReportComponent]
})
export class PrintTaxInvoiceComponentModule {}
