import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { PrintTaxInvoiceReportView } from 'shared/models/viewModel';
@Injectable()
export class PrintTaxInvoiceService {
  serviceKey = 'printTaxInvoiceGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getPrintTaxInvoiceById(id: string): Observable<PrintTaxInvoiceReportView> {
    const url = `${this.servicePath}/GetPrintTaxInvoiceById/id=${id}`;
    return this.dataGateway.get(url);
  }
}
