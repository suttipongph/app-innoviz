import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintTaxInvoiceReportComponent } from './print-tax-invoice-report.component';

describe('PrintTaxInvoiceReportViewComponent', () => {
  let component: PrintTaxInvoiceReportComponent;
  let fixture: ComponentFixture<PrintTaxInvoiceReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrintTaxInvoiceReportComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintTaxInvoiceReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
