import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { PrintTaxInvoiceReportView } from 'shared/models/viewModel';

const firstGroup = [
  'TAX_INVOICE_ID',
  'ISSUED_DATE',
  'DUE_DATE',
  'CUSTOMER_ID',
  'INVOICE_ID',
  'INVOICE_TYPE_ID',
  'DOCUMENT_ID',
  'INVOICE_AMOUNT_BEFORE_TAX',
  'TAX_AMOUNT',
  'INVOICE_AMOUNT'
];

export class PrintTaxInvoiceReportCustomComponent {
  baseService: BaseServiceModel<any>;
  isCopy: boolean = false;
  isOriginal: boolean = false;
  getFieldAccessing(model: PrintTaxInvoiceReportView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<PrintTaxInvoiceReportView> {
    const model = new PrintTaxInvoiceReportView();
    model.printTaxInvoiceGUID = EmptyGuid;
    return of(model);
  }
  getLabel(pageLabel: string): any {
    switch (pageLabel) {
      case 'copy':
        this.isCopy = true;
        break;
      case 'original':
        this.isOriginal = true;
        break;
      default:
        break;
    }
  }
}
