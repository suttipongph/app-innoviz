import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TaxInvoiceLineService } from './tax-invoice-line.service';
import { TaxInvoiceLineListComponent } from './tax-invoice-line-list/tax-invoice-line-list.component';
import { TaxInvoiceLineItemComponent } from './tax-invoice-line-item/tax-invoice-line-item.component';
import { TaxInvoiceLineItemCustomComponent } from './tax-invoice-line-item/tax-invoice-line-item-custom.component';
import { TaxInvoiceLineListCustomComponent } from './tax-invoice-line-list/tax-invoice-line-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [TaxInvoiceLineListComponent, TaxInvoiceLineItemComponent],
  providers: [TaxInvoiceLineService, TaxInvoiceLineItemCustomComponent, TaxInvoiceLineListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [TaxInvoiceLineListComponent, TaxInvoiceLineItemComponent]
})
export class TaxInvoiceLineComponentModule {}
