import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { TaxInvoiceLineListView } from 'shared/models/viewModel';
import { TaxInvoiceLineService } from '../tax-invoice-line.service';
import { TaxInvoiceLineListCustomComponent } from './tax-invoice-line-list-custom.component';

@Component({
  selector: 'tax-invoice-line-list',
  templateUrl: './tax-invoice-line-list.component.html',
  styleUrls: ['./tax-invoice-line-list.component.scss']
})
export class TaxInvoiceLineListComponent extends BaseListComponent<TaxInvoiceLineListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  taxCodeOptions: SelectItems[] = [];
  serviceFeeOptions: SelectItems[] = [];
  constructor(
    public custom: TaxInvoiceLineListCustomComponent,
    public uiControllerService: UIControllerService,
    private service: TaxInvoiceLineService
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.parentId = this.uiControllerService.getKey('taxinvoice');
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    // super.checkAccessMode(this.accessService.getAccessRight());
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getTaxTableDropDown(this.taxCodeOptions);
    this.baseDropdown.getInvoiceRevenueTypeDropDown(this.serviceFeeOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'taxInvoiceLineGUID';
    const columns: ColumnModel[] = [
      {
        label: null,
        textKey: 'taxInvoiceTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.parentId
      },
      {
        label: 'LABEL.LINE',
        textKey: 'lineNum',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.SERVICE_FEE_TYPE_ID',
        textKey: 'invoiceRevenueType_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        sortingKey: 'invoiceRevenueType_InvoiceRevenueTypeId',
        searchingKey: 'invoiceRevenueTypeGUID',
        masterList: this.serviceFeeOptions
      },
      {
        label: 'LABEL.TOTAL_AMOUNT_BEFORE_TAX',
        textKey: 'totalAmountBeforeTax',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.TAX_AMOUNT',
        textKey: 'taxAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.TOTAL_AMOUNT',
        textKey: 'totalAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.TAX_CODE',
        textKey: 'taxTable_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        sortingKey: 'taxTable_TaxId',
        searchingKey: 'taxTableGUID',
        masterList: this.taxCodeOptions
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<TaxInvoiceLineListView>> {
    return this.service.getTaxInvoiceLineToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteTaxInvoiceLine(row));
  }
}
