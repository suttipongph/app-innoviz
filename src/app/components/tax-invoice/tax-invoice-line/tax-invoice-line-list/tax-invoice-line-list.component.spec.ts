import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TaxInvoiceLineListComponent } from './tax-invoice-line-list.component';

describe('TaxInvoiceLineListViewComponent', () => {
  let component: TaxInvoiceLineListComponent;
  let fixture: ComponentFixture<TaxInvoiceLineListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaxInvoiceLineListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxInvoiceLineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
