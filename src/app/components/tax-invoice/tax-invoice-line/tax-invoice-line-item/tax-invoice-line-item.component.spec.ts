import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxInvoiceLineItemComponent } from './tax-invoice-line-item.component';

describe('TaxInvoiceLineItemViewComponent', () => {
  let component: TaxInvoiceLineItemComponent;
  let fixture: ComponentFixture<TaxInvoiceLineItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaxInvoiceLineItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxInvoiceLineItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
