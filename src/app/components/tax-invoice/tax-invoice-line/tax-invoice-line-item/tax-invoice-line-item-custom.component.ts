import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { TaxInvoiceLineItemView } from 'shared/models/viewModel';

const firstGroup = [
  'TAX_INVOICE_TABLE_GUID',
  'LINE_NUM',
  'INVOICE_TEXT',
  'INVOICE_REVENUE_TYPE_GUID',
  'QTY',
  'UNIT_PRICE',
  'TOTAL_AMOUNT_BEFORE_TAX',
  'TAX_AMOUNT',
  'TOTAL_AMOUNT',
  'PROD_UNIT_GUID',
  'TAX_TABLE_GUID',
  'DIMENSION1GUID',
  'DIMENSION2GUID',
  'DIMENSION3GUID',
  'DIMENSION4GUID',
  'DIMENSION5GUID',
  'TAX_INVOICE_LINE_GUID'
];

export class TaxInvoiceLineItemCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: TaxInvoiceLineItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: TaxInvoiceLineItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(): Observable<TaxInvoiceLineItemView> {
    let model = new TaxInvoiceLineItemView();
    model.taxInvoiceLineGUID = EmptyGuid;
    return of(model);
  }
}
