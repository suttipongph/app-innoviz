import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { TaxInvoiceLineItemView, TaxInvoiceLineListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class TaxInvoiceLineService {
  serviceKey = 'taxInvoiceLineGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getTaxInvoiceLineToList(search: SearchParameter): Observable<SearchResult<TaxInvoiceLineListView>> {
    const url = `${this.servicePath}/GetTaxInvoiceLineList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteTaxInvoiceLine(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteTaxInvoiceLine`;
    return this.dataGateway.delete(url, row);
  }
  getTaxInvoiceLineById(id: string): Observable<TaxInvoiceLineItemView> {
    const url = `${this.servicePath}/GetTaxInvoiceLineById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createTaxInvoiceLine(vmModel: TaxInvoiceLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateTaxInvoiceLine`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateTaxInvoiceLine(vmModel: TaxInvoiceLineItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateTaxInvoiceLine`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
