import { Observable, of } from 'rxjs';
import { EmptyGuid, TaxInvoiceStatus } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { TaxInvoiceTableItemView } from 'shared/models/viewModel';

const firstGroup = [
  'TAX_INVOICE_ID',
  'TAX_BRANCH_ID',
  'TAX_BRANCH_NAME',
  'ISSUED_DATE',
  'DUE_DATE',
  'CUSTOMER_TABLE_GUID',
  'CUSTOMER_NAME',
  'DOCUMENT_STATUS_GUID',
  'INVOICE_TABLE_GUID',
  'INVOICE_TYPE_GUID',
  'DOCUMENT_ID',
  'TAX_INVOICE_REF_TYPE',
  'TAX_INVOICE_REF_GUID',
  'REMARK',
  'CURRENCY_GUID',
  'EXCHANGE_RATE',
  'METHOD_OF_PAYMENT_GUID',
  'INVOICE_AMOUNT_BEFORE_TAX',
  'TAX_AMOUNT',
  'INVOICE_AMOUNT',
  'INVOICE_AMOUNT_BEFORE_TAX_MST',
  'TAX_AMOUNT_MST',
  'INVOICE_AMOUNT_MST',
  'INVOICE_ADDRESS1',
  'INVOICE_ADDRESS2',
  'MAILING_INVOICE_ADDRESS1',
  'MAILING_INVOICE_ADDRESS2',
  'CN_REASON_GUID',
  'ORIG_TAX_INVOICE_ID',
  'ORIG_TAX_INVOICE_AMOUNT',
  'DIMENSION1GUID',
  'DIMENSION2GUID',
  'DIMENSION3GUID',
  'DIMENSION4GUID',
  'DIMENSION5GUID',
  'REF_TAX_INVOICE_GUID',
  'TAX_INVOICE_TABLE_GUID'
];

export class TaxInvoiceTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  parentName: string;
  getFieldAccessing(model: TaxInvoiceTableItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: TaxInvoiceTableItemView): Observable<boolean> {
    return of(true);
  }
  getInitialData(): Observable<TaxInvoiceTableItemView> {
    let model = new TaxInvoiceTableItemView();
    model.taxInvoiceTableGUID = EmptyGuid;
    return of(model);
  }
  checkIsCancelled(model: TaxInvoiceTableItemView): boolean {
    return TaxInvoiceStatus[model.documentStatus_Values] === TaxInvoiceStatus.Cancelled.toString();
  }
}
