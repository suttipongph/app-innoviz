import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { UIControllerService } from 'core/services/uiController.service';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, ROUTE_REPORT_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { TaxInvoiceTableItemView } from 'shared/models/viewModel';
import { TaxInvoiceTableService } from '../tax-invoice-table.service';
import { TaxInvoiceTableItemCustomComponent } from './tax-invoice-table-item-custom.component';
@Component({
  selector: 'tax-invoice-table-item',
  templateUrl: './tax-invoice-table-item.component.html',
  styleUrls: ['./tax-invoice-table-item.component.scss']
})
export class TaxInvoiceTableItemComponent extends BaseItemComponent<TaxInvoiceTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  taxInvoiceRefTypeOptions: SelectItems[] = [];

  constructor(
    private service: TaxInvoiceTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: TaxInvoiceTableItemCustomComponent,
    public uiControllerService: UIControllerService
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.parentId = this.uiControllerService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    // super.checkAccessMode(this.accessService.getAccessRight());
    super.checkAccessMode(this.accessService.getNestedComponentAccessRight(false));
  }
  onEnumLoader(): void {
    this.baseDropdown.getTaxInvoiceRefTypeEnumDropDown(this.taxInvoiceRefTypeOptions);
  }
  getById(): Observable<TaxInvoiceTableItemView> {
    return this.service.getTaxInvoiceTableById(this.id);
  }
  getInitialData(): Observable<TaxInvoiceTableItemView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions(result);
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateTaxInvoiceTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createTaxInvoiceTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  setFunctionOptions(model: TaxInvoiceTableItemView): void {
    const isCancelled = this.custom.checkIsCancelled(model);
    this.functionItems = [
      {
        label: 'LABEL.PRINT',
        items: [
          {
            label: 'LABEL.COPY',
            command: () =>
              this.toReport({
                path: ROUTE_REPORT_GEN.PRINT_TAX_INVOICE_COPY
              }),
            disabled: isCancelled,
            visible: true
          },
          {
            label: 'LABEL.ORIGINAL',
            command: () =>
              this.toReport({
                path: ROUTE_REPORT_GEN.PRINT_TAX_INVOICE_ORIGINAL
              }),
            disabled: isCancelled,
            visible: true
          }
        ]
      }
    ];
    super.setFunctionOptionsMapping();
  }
}
