import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxInvoiceTableItemComponent } from './tax-invoice-table-item.component';

describe('TaxInvoiceTableItemViewComponent', () => {
  let component: TaxInvoiceTableItemComponent;
  let fixture: ComponentFixture<TaxInvoiceTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaxInvoiceTableItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxInvoiceTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
