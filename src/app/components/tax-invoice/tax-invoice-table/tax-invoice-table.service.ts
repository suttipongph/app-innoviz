import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { InvoiceTableItemView, TaxInvoiceTableItemView, TaxInvoiceTableListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class TaxInvoiceTableService {
  serviceKey = 'taxInvoiceTableGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getTaxInvoiceTableToList(search: SearchParameter): Observable<SearchResult<TaxInvoiceTableListView>> {
    const url = `${this.servicePath}/GetTaxInvoiceTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteTaxInvoiceTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteTaxInvoiceTable`;
    return this.dataGateway.delete(url, row);
  }
  getTaxInvoiceTableById(id: string): Observable<TaxInvoiceTableItemView> {
    const url = `${this.servicePath}/GetTaxInvoiceTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  getInvoiceTableByFreeTextInvoice(id: string): Observable<InvoiceTableItemView> {
    const url = `${this.servicePath}/GetTaxInvoiceTableByByFreeTextInvoice/id=${id}`;
    console.log(url);
    return this.dataGateway.get(url);
  }
  createTaxInvoiceTable(vmModel: TaxInvoiceTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateTaxInvoiceTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateTaxInvoiceTable(vmModel: TaxInvoiceTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateTaxInvoiceTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
