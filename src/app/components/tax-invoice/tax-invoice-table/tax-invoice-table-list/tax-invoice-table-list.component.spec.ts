import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TaxInvoiceTableListComponent } from './tax-invoice-table-list.component';

describe('TaxInvoiceTableListViewComponent', () => {
  let component: TaxInvoiceTableListComponent;
  let fixture: ComponentFixture<TaxInvoiceTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaxInvoiceTableListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxInvoiceTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
