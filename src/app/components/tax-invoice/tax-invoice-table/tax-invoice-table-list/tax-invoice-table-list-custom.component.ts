import { InvoiceTableItemView } from 'shared/models/viewModel';
import { ColumnType, ROUTE_RELATED_GEN, SortType } from 'shared/constants';
import { Observable, of, pipe } from 'rxjs';
import { BaseServiceModel, ColumnModel, OptionModel } from 'shared/models/systemModel';
import { map } from 'rxjs/operators';

export class TaxInvoiceTableListCustomComponent {
  baseService: BaseServiceModel<any>;
  invoiceTableGUID = '';
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  getConditionFreeTextInvoice(option: OptionModel): void {
    const parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    if (parentName === ROUTE_RELATED_GEN.FREE_TEXT_INVOICE_TABLE) {
      option.columns.push({
        label: null,
        textKey: 'invoiceTableGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: this.invoiceTableGUID
      });
    }
  }
  getConditionInvoiceTableWithFreeTextInvoice(): Observable<boolean> {
    const parentName = this.baseService.uiService.getRelatedInfoParentTableName();
    if (parentName === ROUTE_RELATED_GEN.FREE_TEXT_INVOICE_TABLE) {
      const parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
      return new Observable((observer) => {
        this.baseService.service.getInvoiceTableByFreeTextInvoice(parentId).subscribe((invoiceModel: InvoiceTableItemView) => {
          this.invoiceTableGUID = invoiceModel.invoiceTableGUID;
          observer.next(true);
        });
      });
    } else {
      return of(true);
    }
  }
}
