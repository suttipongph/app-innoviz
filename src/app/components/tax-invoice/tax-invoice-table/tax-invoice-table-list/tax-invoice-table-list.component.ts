import { Component, Input, OnInit } from '@angular/core';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { TaxInvoiceTableListView } from 'shared/models/viewModel';
import { TaxInvoiceTableService } from '../tax-invoice-table.service';
import { TaxInvoiceTableListCustomComponent } from './tax-invoice-table-list-custom.component';

@Component({
  selector: 'tax-invoice-table-list',
  templateUrl: './tax-invoice-table-list.component.html',
  styleUrls: ['./tax-invoice-table-list.component.scss']
})
export class TaxInvoiceTableListComponent extends BaseListComponent<TaxInvoiceTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  taxInvoiceRefTypeOptions: SelectItems[] = [];
  documentStatusOptions: SelectItems[] = [];

  constructor(public custom: TaxInvoiceTableListCustomComponent, private service: TaxInvoiceTableService) {
    super();
    this.custom.baseService = this.baseService;
    this.custom.baseService.service = this.service;
    this.custom.baseService.uiService = this.uiService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.custom.getConditionInvoiceTableWithFreeTextInvoice().subscribe((res) => {
      this.onEnumLoader();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getTaxInvoiceRefTypeEnumDropDown(this.taxInvoiceRefTypeOptions);
    this.setDataGridOption();
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getDocumentStatusDropDown(this.documentStatusOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = false;
    this.option.canView = this.getCanView();
    this.option.canDelete = false;
    this.option.key = 'taxInvoiceTableGUID';
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.TAX_INVOICE_ID',
        textKey: 'taxInvoiceId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.ISSUED_DATE',
        textKey: 'issuedDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.DUE_DATE',
        textKey: 'dueDate',
        type: ColumnType.DATERANGE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CUSTOMER_ID',
        textKey: 'customerTable_Values',
        type: ColumnType.STRING,
        visibility: true,
        sortingKey: 'customerTable_CustomerId',
        searchingKey: 'customerTable_CustomerId',
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CUSTOMER_NAME',
        textKey: 'customerName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.INVOICE_AMOUNT_BEFORE_TAX',
        textKey: 'invoiceAmountBeforeTax',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.TAX_AMOUNT',
        textKey: 'taxAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.INVOICE_AMOUNT',
        textKey: 'invoiceAmount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.STATUS',
        textKey: 'documentStatus_Values',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        masterList: this.documentStatusOptions,
        sortingKey: 'documentStatus_StatusId',
        searchingKey: 'documentStatusGUID'
      }
    ];
    this.option.columns = columns;
    this.custom.getConditionFreeTextInvoice(this.option);

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<TaxInvoiceTableListView>> {
    return this.service.getTaxInvoiceTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteTaxInvoiceTable(row));
  }
}
