import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TaxInvoiceTableService } from './tax-invoice-table.service';
import { TaxInvoiceTableListComponent } from './tax-invoice-table-list/tax-invoice-table-list.component';
import { TaxInvoiceTableItemComponent } from './tax-invoice-table-item/tax-invoice-table-item.component';
import { TaxInvoiceTableItemCustomComponent } from './tax-invoice-table-item/tax-invoice-table-item-custom.component';
import { TaxInvoiceTableListCustomComponent } from './tax-invoice-table-list/tax-invoice-table-list-custom.component';
import { TaxInvoiceLineComponentModule } from '../tax-invoice-line/tax-invoice-line.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, TaxInvoiceLineComponentModule],
  declarations: [TaxInvoiceTableListComponent, TaxInvoiceTableItemComponent],
  providers: [TaxInvoiceTableService, TaxInvoiceTableItemCustomComponent, TaxInvoiceTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [TaxInvoiceTableListComponent, TaxInvoiceTableItemComponent]
})
export class TaxInvoiceTableComponentModule {}
