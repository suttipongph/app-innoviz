import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SysUserLogService } from './sys-user-log.service';
import { SysUserLogListComponent } from './sys-user-log-list/sys-user-log-list.component';
import { SysUserLogListCustomComponent } from './sys-user-log-list/sys-user-log-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [SysUserLogListComponent],
  providers: [SysUserLogService, SysUserLogListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [SysUserLogListComponent]
})
export class SysUserLogComponentModule {}
