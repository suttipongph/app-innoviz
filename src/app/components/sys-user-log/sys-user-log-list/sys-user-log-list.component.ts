import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { ColumnType, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { SysUserLogView } from 'shared/models/viewModel';
import { SysUserLogService } from '../sys-user-log.service';
import { SysUserLogListCustomComponent } from './sys-user-log-list-custom.component';

@Component({
  selector: 'sys-user-log-list',
  templateUrl: './sys-user-log-list.component.html',
  styleUrls: ['./sys-user-log-list.component.scss']
})
export class SysUserLogListComponent extends BaseListComponent<SysUserLogView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  sysLanguageOptions: SelectItems[] = [];
  defaultBitOptions: SelectItems[] = [];
  constructor(private service: SysUserLogService, public custom: SysUserLogListCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getSysLanguageEnumDropDown(this.sysLanguageOptions);
    this.baseDropdown.getDefaultBitDropDown(this.defaultBitOptions);
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'sessionId';
    this.option.canCreate = false;
    this.option.canView = false;
    this.option.canDelete = false;
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.USER_NAME',
        textKey: 'userName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.IP_ADDRESS',
        textKey: 'ipAddress',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.COMPUTER_NAME',
        textKey: 'computerName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.CREATED_DATE_TIME',
        textKey: 'createdDateTime',
        type: ColumnType.DATE,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.SESSION_ID',
        textKey: 'sessionId',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: 'LABEL.LOG_OFF_TIME',
        textKey: 'logOffTime',
        type: ColumnType.DATE,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<SysUserLogView>> {
    return this.service.getSysUserLogToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    //super.onDelete(row, this.service.deleteSysUserLog(row));
  }
}
