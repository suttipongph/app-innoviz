import { Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { SysUserLogView } from 'shared/models/viewModel';

@Injectable()
export class SysUserLogService {
  serviceKey = 'id';
  servicePath = '';

  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getSysUserLogToList(search: SearchParameter): Observable<SearchResult<SysUserLogView>> {
    const url = `${this.servicePath}/GetSysUserLogList`;
    return this.dataGateway.getList(url, search);
  }
}
