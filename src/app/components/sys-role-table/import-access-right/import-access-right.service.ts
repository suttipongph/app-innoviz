import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { ImportAccessRightView } from 'shared/models/viewModel/importAccessRightView';
@Injectable()
export class ImportAccessRightService {
  serviceKey = 'importAccessRightGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getImportAccessRightToList(search: SearchParameter): Observable<SearchResult<ImportAccessRightView>> {
    const url = `${this.servicePath}/GetImportAccessRightList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteImportAccessRight(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteImportAccessRight`;
    return this.dataGateway.delete(url, row);
  }
  getImportAccessRightById(id: string): Observable<ImportAccessRightView> {
    const url = `${this.servicePath}/GetImportAccessRightById/id=${id}`;
    return this.dataGateway.get(url);
  }

  importAccessRight(vwModel: ImportAccessRightView): Observable<boolean> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vwModel);
  }
}
