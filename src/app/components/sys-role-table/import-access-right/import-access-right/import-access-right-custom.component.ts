import { CustomerTableService } from 'components/customer-table/customer-table/customer-table.service';
import { Observable, of } from 'rxjs';
import { EmptyGuid, RefType } from 'shared/constants';
import { getRefTypeFromRoute, isUndefinedOrZeroLength, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, FileInformation } from 'shared/models/systemModel';
import { CustomerTableItemView, ImportAccessRightView, RefIdParm } from 'shared/models/viewModel';
import { ImportAccessRightService } from '../import-access-right.service';

const firstGroup = ['NAME', 'FILE'];

export class ImportAccessRightCustomComponent {
  baseService: BaseServiceModel<CustomerTableService>;
  service: ImportAccessRightService;
  displayName: string = '';
  parentId: string = '';
  parentName: string = '';

  getFieldAccessing(): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }

  getDataValidation(): Observable<boolean> {
    return of(true);
  }

  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }

  getInitialData(): Observable<ImportAccessRightView> {
    let model = new ImportAccessRightView();
    return of(model);
  }

  setDisplayName(passingObj: any): void {
    if (!isUndefinedOrZeroLength(passingObj.displayName)) {
      this.displayName = passingObj.displayName;
    }
  }
  getDisplayName(): string {
    return this.displayName;
  }
}
