 import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportAccessRightComponent } from './import-access-right.component';

describe('ImportAccessRightComponent', () => {
  let component: ImportAccessRightComponent;
  let fixture: ComponentFixture<ImportAccessRightComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImportAccessRightComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportAccessRightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
 