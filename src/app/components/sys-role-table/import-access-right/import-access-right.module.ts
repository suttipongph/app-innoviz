import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ImportAccessRightService } from './import-access-right.service';
import { ImportAccessRightComponent } from './import-access-right/import-access-right.component';
import { ImportAccessRightCustomComponent } from './import-access-right/import-access-right-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ImportAccessRightComponent],
  providers: [ImportAccessRightService , ImportAccessRightCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ImportAccessRightComponent]
})
export class ImportAccessRightComponentModule {}