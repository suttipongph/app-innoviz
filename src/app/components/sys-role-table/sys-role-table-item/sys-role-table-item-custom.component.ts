import { Observable, of, Subject } from 'rxjs';
import { FieldAccessing, BaseServiceModel, ButtonConfigModel } from 'shared/models/systemModel';
import { AccessLevelModel, SysRoleTableItemView } from 'shared/models/viewModel';
import { isUpdateMode } from 'shared/functions/value.function';
import { AppConst } from 'shared/constants';
import { dateTimetoStringLong } from 'shared/functions/date.function';

const firstGroup = ['NAME'];
const systemGroup = ['SITE_LOGIN_TYPE'];
const allFields = ['NAME', 'SITE_LOGIN_TYPE'];

export class SysRoleTableItemCustomComponent {
  baseService: BaseServiceModel<any>;
  roleFormAsyncRunnerDoneSubject = new Subject<boolean>();
  roleAccessRightSubject = new Subject<AccessLevelModel>();
  getFieldAccessing(model: SysRoleTableItemView, isView: boolean): Observable<FieldAccessing[]> {
    if (isView) {
      return of([{ filedIds: allFields, readonly: true }]);
    } else {
      const fieldAccessing: FieldAccessing[] = [];
      fieldAccessing.push({ filedIds: systemGroup, readonly: true });
      if (isUpdateMode(model.id) && model.displayName === AppConst.ADMINROLE) {
        fieldAccessing.push({ filedIds: firstGroup, readonly: true });
      } else {
        fieldAccessing.push({ filedIds: firstGroup, readonly: false });
      }
      return of(fieldAccessing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: SysRoleTableItemView): Observable<boolean> {
    return of(!canCreate);
  }
  getAccessLevelButtonStyleConfig(): ButtonConfigModel[] {
    return [
      {
        className: 'ui-button-no-label inline-grid-button ui-button-rounded ui-button-danger ui-button-outlined-medium',
        // label: '.'
        icon: 'inline-grid-button-icon pi pi-circle-off'
      },
      {
        className: 'ui-button-rounded inline-grid-button ui-button-warning ui-button-outlined-medium',
        // label: '.',
        icon: 'inline-grid-button-icon pi pi-user'
      },
      {
        className: 'ui-button-no-label inline-grid-button ui-button-rounded ui-button-warning',
        // label: '.',
        icon: 'inline-grid-button-icon pi pi-circle-off'
      },
      {
        className: 'ui-button-rounded inline-grid-button ui-button-success ui-button-outlined-medium',
        icon: 'inline-grid-button-icon pi pi-users'
        //label: '4'
      },
      {
        className: 'ui-button-no-label inline-grid-button ui-button-rounded ui-button-success',
        // label: '.',
        icon: 'inline-grid-button-icon pi pi-circle-off'
      }
    ];
  }
  getFunctionAccessLevelButtonStyleConfig(): ButtonConfigModel[] {
    return [
      {
        className: 'ui-button-no-label inline-grid-button ui-button-rounded ui-button-danger ui-button-outlined-medium',
        // label: '.'
        icon: 'inline-grid-button-icon pi pi-circle-off'
      },

      {
        className: 'ui-button-no-label inline-grid-button ui-button-rounded ui-button-success',
        // label: '.',
        icon: 'inline-grid-button-icon pi pi-circle-off'
      }
    ];
  }
  getDisableImport(model: SysRoleTableItemView, canUpdate: boolean): boolean {
    return model.displayName === AppConst.ADMINROLE || !canUpdate;
  }
  getExportRoleFileName(displayName: string): string {
    return `${displayName}_${dateTimetoStringLong(new Date(), 'yyyyMMddTHHmmss')}`;
  }
}
