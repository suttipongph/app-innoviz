import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { AccessLevel, AccessMode, EmptyGuid, ROUTE_ADMIN, ROUTE_FUNCTIONS, ROUTE_FUNCTION_GEN, SysFeatureType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUndefinedOrZeroLength, isUpdateMode, transformLabel } from 'shared/functions/value.function';
import { ButtonConfigModel, FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { SysRoleTableItemView, SysFeatureRoleView, SysFeatureGroupRoleView, AccessLevelModel } from 'shared/models/viewModel';
import { SysRoleTableService } from '../sys-role-table.service';
import { SysRoleTableItemCustomComponent } from './sys-role-table-item-custom.component';
import { SysRoleTableFormComponent } from '../sys-role-table-form/sys-role-table-form.component';
import { MenuItem, TreeNode } from 'primeng/api';
import { Steps } from 'primeng/steps';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'sys-role-table-item',
  templateUrl: './sys-role-table-item.component.html',
  styleUrls: ['./sys-role-table-item.component.scss']
})
export class SysRoleTableItemComponent extends BaseItemComponent<SysRoleTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  @ViewChild('roleForm') roleForm: SysRoleTableFormComponent;
  isUndefinedOrZeroLength = isUndefinedOrZeroLength;
  stepItems: MenuItem[];

  companyOption: SelectItems[] = [];

  id: string;
  dateFormat: string;

  activeIndex: number = 0;
  currentIndex: number = 0;

  @ViewChild('stepper') stepper: Steps;
  selectedIndex: number = 0;

  defaultFeatures: SysFeatureGroupRoleView[] = [];
  displayFeatures: SysFeatureGroupRoleView[] = [];

  isSearch: boolean = true;
  treeDataSource: TreeNode[] = [];

  isTreeLoaded: boolean = false;

  displayTree: TreeNode[] = [];
  searchParam: string;

  features: SysFeatureRoleView[] = [];

  readonlyTree: boolean = false;

  cascadeInput: boolean = false;
  selectedFilterGroup: any;
  filterGroup: any[] = [];

  validateStepOneFlag: boolean = false;
  accessRightTreeDirty: boolean = false;
  roleFormAsyncRunnerDone: boolean = false;
  accessLevelBtnStyles: ButtonConfigModel[];
  functionAccessLevelBtnStyles: ButtonConfigModel[];
  SysFeatureType = SysFeatureType;
  constructor(private service: SysRoleTableService, private currentActivatedRoute: ActivatedRoute, public custom: SysRoleTableItemCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.stepItems = transformLabel([{ label: 'LABEL.ROLE_INFO' }, { label: 'LABEL.MAP_ACCESS_RIGHT_TO_ROLE' }]);

    this.accessLevelBtnStyles = this.custom.getAccessLevelButtonStyleConfig();
    this.functionAccessLevelBtnStyles = this.custom.getFunctionAccessLevelButtonStyleConfig();
    this.custom.roleFormAsyncRunnerDoneSubject.subscribe((value) => {
      this.roleFormAsyncRunnerDone = value;
      if (value && this.roleFormAsyncRunnerDone) {
        // this.userForm.getUserFormValidation();
        this.initRoleFormValues();
      }
    });
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {}
  getById(): Observable<SysRoleTableItemView> {
    return this.service.getSysRoleTableById(this.id);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions(result);
        if (!isUndefinedOrZeroLength(this.roleForm)) {
          this.roleForm.vwModel = result;
          this.roleForm.parentAccessRight = this.accessRight;
          this.roleForm.setInitialUpdatingData();
        }
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.onAsyncRunner();
    this.model.id = EmptyGuid;
    super.setDefaultValueSystemFields();
  }
  onAsyncRunner(model?: any): void {
    // forkJoin(this.service.getSysFeatureGroupRoleViewList()).subscribe(
    forkJoin(this.service.getSysFeatureGroupRoleViewList()).subscribe(
      // ([sysFeatureGroupRoles]) => {
      ([sysFeatureGroupRoles]) => {
        this.defaultFeatures = sysFeatureGroupRoles;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.initRoleFormValues();
        // this.initSysFeatureRoleTreeSource(sysFeatureGroupRoles);
        this.initSysFeatureRoleViewList();
        this.setFieldAccessing();
      },
      (error) => {
        this.accessRight = new AccessLevelModel();
        this.custom.roleAccessRightSubject.next(this.accessRight);
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model, this.isView).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {}
  setFunctionOptions(model: SysRoleTableItemView): void {
    const disableImport = this.custom.getDisableImport(model, super.getCanUpdate());
    this.functionItems = [
      {
        label: 'LABEL.IMPORT',
        disabled: disableImport,
        command: () => this.toFunction({ path: ROUTE_FUNCTION_GEN.IMPORT_ACCESS_RIGHT, parameters: { displayName: model.displayName } })
      },
      {
        label: 'LABEL.EXPORT',
        command: () => this.exportRoleTemplate(model.displayName),
        visible: true
      }
    ];
    super.setFunctionOptionsMapping();
  }
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    this.model.sysFeatureGroupRoleViewList = this.prepareSysFeatureRolesForSubmit();
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateSysRoleTable(this.model), isColsing);
    } else {
      super.onCreate(this.service.createSysRoleTable(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  exportRoleTemplate(displayName: string): void {
    this.service.getRoleDataForExport(this.id).subscribe((result) => {
      this.fileService.exportAsExcelFile(result, this.custom.getExportRoleFileName(displayName));
    });
  }
  // ======== stepper ============
  initRoleFormValues(): void {
    // if (!isUndefinedOrZeroLength(this.roleForm)) {
    //   this.roleForm.accessRight = this.accessRight;
    //   // this.userForm.adUserNameOptions = this.adUserOptions;
    //   this.roleForm.setFieldAccessing();
    // }
    if (this.isView) {
      this.custom.roleAccessRightSubject.next(new AccessLevelModel());
    } else {
      if (!isUndefinedOrZeroLength(this.accessRight)) {
        this.custom.roleAccessRightSubject.next(this.accessRight);
      }
    }
  }
  initSysFeatureRoleViewList(): void {
    // deep copy
    this.displayFeatures = JSON.parse(JSON.stringify(this.defaultFeatures));
    const modelSysFeatureRole = this.model.sysFeatureGroupRoleViewList;
    if (!isUndefinedOrZeroLength(modelSysFeatureRole)) {
      for (let i = 0; i < modelSysFeatureRole.length; i++) {
        const modelItem = modelSysFeatureRole[i];
        const index = this.displayFeatures.findIndex((item) => item.sysFeatureGroupGUID === modelItem.sysFeatureGroupGUID);
        if (index !== -1) {
          this.displayFeatures[index] = modelItem;
        }
      }
    }
  }
  initSysFeatureRoleTreeSource(tree: TreeNode[]): void {
    // this.displayTree =
    this.initSysFeatureRoleViewList();
    const treeSource = this.populateSysFeatureRoleViewDataToGroupedTree(tree, this.displayFeatures);
    this.treeDataSource = JSON.parse(JSON.stringify(treeSource));
    this.displayTree = JSON.parse(JSON.stringify(treeSource));
    for (let i = 0; i < this.displayTree.length; i++) {
      if (!isUndefinedOrZeroLength(this.displayTree[i])) {
        this.setParentNodeVal(this.displayTree[i]);
      }
    }
  }
  getTreeDataSource(): Observable<TreeNode[]> {
    //this.setNodeLabels(result);
    // this.getFilterGroupOption(result);
    // this.treeDataSource = JSON.parse(JSON.stringify(result));

    // return result;
    return this.service.getSysFeatureGroupStructure().pipe(
      tap((result) => {
        // set label
        //this.setNodeLabels(result);
        this.getFilterGroupOption(result);
        this.treeDataSource = JSON.parse(JSON.stringify(result));
      })
    );
  }
  initStepTwo() {
    const getTreeIdentifier: string = 'getSysFeatureRoleTree';
    this.uiService.setIncreaseReqCounter(getTreeIdentifier);

    //const result = this.getTreeDataSource();
    this.getTreeDataSource().subscribe(
      (result) => {
        this.initSysFeatureRoleTreeSource(result);
        this.uiService.setDecreaseReqCounter(getTreeIdentifier);
        this.isTreeLoaded = true;
        this.activeIndex = 1;
      },
      (error) => {
        this.uiService.setDecreaseReqCounter(getTreeIdentifier);
        this.isTreeLoaded = true;
        this.activeIndex = 1;
      }
    );
  }
  setParentNodeVal(node: TreeNode): void {
    if (!node.data.hasSysFeatureTable) {
      node.data['action'] = 0;
      node.data['create'] = 0;
      node.data['read'] = 0;
      node.data['update'] = 0;
      node.data['delete'] = 0;
    }
    if (!isUndefinedOrZeroLength(node.children)) {
      for (let i = 0; i < node.children.length; i++) {
        this.setParentNodeVal(node.children[i]);
      }
    }
  }
  onRoleFormChange(formModel: SysRoleTableItemView) {
    this.model = formModel;
  }
  onGroupFilterChange(e) {
    this.updateTreeDataSourceWithTreeNodes(this.displayTree);

    if (isUndefinedOrZeroLength(e)) {
      this.displayTree = this.treeDataSource.slice();
    } else {
      const filterBy = e;
      this.displayTree = this.treeDataSource.filter((item) => item['data'].sysFeatureGroup_GroupId === filterBy);
    }
  }

  updateTreeDataSourceWithTreeNodes(nodes: TreeNode[]) {
    if (!isUndefinedOrZeroLength(nodes)) {
      for (let i = 0; i < nodes.length; i++) {
        let target = this.findTreeNode(this.treeDataSource, nodes[i].data);
        if (!isUndefinedOrZeroLength(target)) {
          target.data['action'] = nodes[i].data['action'];
          target.data['create'] = nodes[i].data['create'];
          target.data['read'] = nodes[i].data['read'];
          target.data['update'] = nodes[i].data['update'];
          target.data['delete'] = nodes[i].data['delete'];
        }
        if (!isUndefinedOrZeroLength(nodes[i].children)) {
          this.updateTreeDataSourceWithTreeNodes(nodes[i].children);
        }
      }
    }
  }
  onStepsChange(index: number) {
    this.selectedIndex = index;
  }
  onStepperClick(event) {
    if (this.currentIndex === 0) {
      if (this.selectedIndex > this.currentIndex) {
        let isRoleFormValid = this.validateRoleForm();
        if (!isRoleFormValid) {
          this.validateStepOneFlag = true;
          this.forceClickToIndex(event, 0);
        } else {
          if (!this.isTreeLoaded) {
            this.initStepTwo();
          }
          this.currentIndex = this.selectedIndex;
          this.activeIndex = this.selectedIndex;
        }
      }
    } else if (this.selectedIndex < this.currentIndex) {
      this.currentIndex = this.selectedIndex;
      this.activeIndex = this.selectedIndex;
      this.roleForm.model = this.model;
    }
  }
  next() {
    let isRoleFormValid = this.validateRoleForm();
    if (!isRoleFormValid) {
      this.validateStepOneFlag = true;
      return;
    }
    if (!this.isTreeLoaded) {
      this.initStepTwo();
    }
    this.activeIndex = 1;
    this.currentIndex = this.currentIndex < 1 ? this.currentIndex + 1 : 0;
  }
  prev() {
    this.activeIndex = 0;
    this.currentIndex = this.currentIndex > 0 ? this.currentIndex - 1 : 1;
  }
  forceClickToIndex(event, index: number) {
    this.currentIndex = index;
    this.activeIndex = index;
    this.stepper.itemClick(event, this.stepItems[index], index);
  }
  validateRoleForm() {
    let displayName = this.model.displayName;
    if (isUndefinedOrZeroLength(displayName)) {
      return false;
    }
    return true;
  }

  populateSysFeatureRoleViewDataToGroupedTree(tree: TreeNode[], featureRoles: SysFeatureGroupRoleView[]): TreeNode[] {
    let fr: SysFeatureGroupRoleView[] = featureRoles.slice();
    for (let i = 0; i < featureRoles.length; i++) {
      const sysFeatureRole = featureRoles[i];
      this.populateTreeData(sysFeatureRole, tree);
    }
    return tree;
  }
  populateTreeData(sysFeatureRole: SysFeatureGroupRoleView, treeNodes: TreeNode[]): void {
    for (let i = 0; i < treeNodes.length; i++) {
      const nodeSysFeatureTable: string = treeNodes[i].data['sysFeatureGroup_GroupId'];

      // found
      if (!isUndefinedOrZeroLength(nodeSysFeatureTable) && nodeSysFeatureTable === sysFeatureRole.groupId) {
        treeNodes[i].data['action'] = sysFeatureRole.action;
        treeNodes[i].data['create'] = sysFeatureRole.create;
        treeNodes[i].data['read'] = sysFeatureRole.read;
        treeNodes[i].data['update'] = sysFeatureRole.update;
        treeNodes[i].data['delete'] = sysFeatureRole.delete;
        return;
      }
      if (!isUndefinedOrZeroLength(treeNodes[i].children)) {
        this.populateTreeData(sysFeatureRole, treeNodes[i].children);
      }
    }
  }
  convertDataFromTreeToSysFeatureRoleViewList(tree: TreeNode[]): void {
    for (let i = 0; i < tree.length; i++) {
      let node: TreeNode = tree[i];
      let item: SysFeatureGroupRoleView = new SysFeatureGroupRoleView();
      // if (!isUndefinedOrZeroLength(node.data['sysFeatureTableGUID'])) {
      if (node.data['hasSysFeatureTable']) {
        // let featureTables = node.data['sysFeatureTableGUID'];
        // for (let j = 0; j < featureTables.length; j++) {
        //   let sysFeatureTable = featureTables[j];
        //   let index = this.displayFeatures.findIndex(
        //     (x) =>
        //       x.sysFeatureTable_FeatureId === sysFeatureTable.featureId &&
        //       x.sysFeatureTable_ParentFeatureId === sysFeatureTable.parentFeatureId &&
        //       x.sysFeatureTable_Path === sysFeatureTable.path
        //   );
        //   if (index !== -1) {
        //     this.displayFeatures[index].accessRight = node.data['accessRight'];
        //   }
        // }
        const featureGroup = node.data['sysFeatureGroup_GroupId'];
        let index = this.displayFeatures.findIndex((x) => x.groupId === featureGroup);
        if (index !== -1) {
          this.displayFeatures[index].action = node['data'].action;
          this.displayFeatures[index].create = node['data'].create;
          this.displayFeatures[index].read = node['data'].read;
          this.displayFeatures[index].update = node['data'].update;
          this.displayFeatures[index].delete = node['data'].delete;
        }
      }
      if (!isUndefinedOrZeroLength(node.children)) {
        this.convertDataFromTreeToSysFeatureRoleViewList(node.children);
      }
    }
  }

  prepareSysFeatureRolesForSubmit(): SysFeatureGroupRoleView[] {
    this.updateTreeDataSourceWithTreeNodes(this.displayTree);
    this.convertDataFromTreeToSysFeatureRoleViewList(this.treeDataSource);

    let result: SysFeatureGroupRoleView[] = [];
    for (let i = 0; i < this.displayFeatures.length; i++) {
      let displayItem = this.displayFeatures[i];
      if (
        displayItem.sysFeatureGroup_FeatureType !== SysFeatureType.function &&
        displayItem.sysFeatureGroup_FeatureType !== SysFeatureType.report &&
        displayItem.sysFeatureGroup_FeatureType !== SysFeatureType.workflow &&
        displayItem.sysFeatureGroup_FeatureType !== SysFeatureType.workflowActionHistory
      ) {
        if (
          displayItem.create !== AccessLevel.None ||
          displayItem.read !== AccessLevel.None ||
          displayItem.update !== AccessLevel.None ||
          displayItem.delete !== AccessLevel.None
        ) {
          displayItem.sysFeatureGroupRoleGUID = EmptyGuid;
          displayItem.roleGUID = this.id;
          result.push(displayItem);
        }
      } else if (displayItem.sysFeatureGroup_FeatureType === SysFeatureType.workflowActionHistory) {
        if (displayItem.read !== AccessLevel.None) {
          displayItem.sysFeatureGroupRoleGUID = EmptyGuid;
          displayItem.roleGUID = this.id;
          result.push(displayItem);
        }
      } else {
        if (displayItem.action !== AccessLevel.None) {
          displayItem.sysFeatureGroupRoleGUID = EmptyGuid;
          displayItem.roleGUID = this.id;
          result.push(displayItem);
        }
      }
    }
    return result;
  }
  resetWizard() {
    this.activeIndex = 0;
    this.currentIndex = 0;
    this.selectedIndex = 0;

    this.displayFeatures = [];
    this.treeDataSource = [];
    this.displayTree = [];

    this.isTreeLoaded = false;
  }

  onAccessRightChange(e, data, fieldName): void {
    this.accessRightTreeDirty = true;
    let nodeFromTree: TreeNode = this.findTreeNode(this.displayTree, data);
    this.setAccessRightValue(e, nodeFromTree, fieldName);
    //this.setTreeNodeAccessRightByGroup(this.displayTree, data['group'], data['type'], e);

    if (this.cascadeInput) {
      for (let i = 0; i < nodeFromTree.children.length; i++) {
        this.cascadeData(e, nodeFromTree.children[i], fieldName);
      }
    }
  }
  setAccessRightValue(e, node: TreeNode, fieldName): void {
    let value = e;
    const nodeLabel = node.data.nodeLabel;
    if (
      node.data.featureType === SysFeatureType.function ||
      (!isUndefinedOrZeroLength(nodeLabel) && nodeLabel.includes('Function')) ||
      node.data.featureType === SysFeatureType.report ||
      (node.data.featureType === SysFeatureType.workflow && fieldName === 'action')
    ) {
      if (e > AccessMode.noAccess) {
        value = AccessMode.full;
      } else {
        value = AccessMode.noAccess;
      }
      node.data['action'] = value;
    } else if (node.data.featureType === SysFeatureType.workflow && fieldName !== 'action') {
      if (!node.data.hasSysFeatureTable && fieldName === 'read') {
        node.data[fieldName] = value;
      }
    } else {
      if (node.data.featureType === SysFeatureType.workflowActionHistory) {
        if (fieldName === 'read') {
          node.data[fieldName] = value;
        }
      } else {
        node.data[fieldName] = value;
      }
    }
  }
  cascadeData(value, node: TreeNode, fieldName): void {
    this.setAccessRightValue(value, node, fieldName);
    //this.setTreeNodeAccessRightByGroup(this.displayTree, node.data['group'], node.data['type'], value);
    if (isUndefinedOrZeroLength(node.children)) {
      return;
    } else {
      for (let i = 0; i < node.children.length; i++) {
        const current: TreeNode = node.children[i];
        if (!isUndefinedOrZeroLength(current)) {
          this.cascadeData(value, current, fieldName);
        }
      }
    }
  }
  findTreeNode(tree: TreeNode[], data: any): TreeNode {
    for (let i = 0; i < tree.length; i++) {
      if (!isUndefinedOrZeroLength(tree[i])) {
        if (tree[i].data.nodeId === data.nodeId) {
          return tree[i];
        } else {
          if (!isUndefinedOrZeroLength(tree[i].children)) {
            const childNode = this.findTreeNode(tree[i].children, data);
            if (!isUndefinedOrZeroLength(childNode)) {
              return childNode;
            }
          }
        }
      }
    }
    return null;
  }
  setTreeNodeAccessRightByGroup(nodes: TreeNode[], group: string, nodeType: string, accessRight: number, fieldName?) {
    for (let i = 0; i < nodes.length; i++) {
      if (!isUndefinedOrZeroLength(nodes[i])) {
        let nodeGroup = nodes[i].data['group'];
        let type = nodes[i].data['type'];
        let nodeAccessRight = nodes[i].data['accessRight'];
        if (!isUndefinedOrZeroLength(nodeGroup)) {
          if (nodeGroup === group && nodeType === type && nodeAccessRight !== accessRight) {
            // nodeAccessRight = accessRight;
            this.setAccessRightValue(accessRight, nodes[i], fieldName);
          }
        }
        if (!isUndefinedOrZeroLength(nodes[i].children)) {
          this.setTreeNodeAccessRightByGroup(nodes[i].children, group, nodeType, accessRight);
        }
      }
    }
  }

  setNodeLabels(treeNodes: TreeNode[]) {
    if (!isUndefinedOrZeroLength(treeNodes)) {
      treeNodes.map((item) => {
        item.label = item.data.nodeLabel;
        return item;
      });
      for (let i = 0; i < treeNodes.length; i++) {
        if (!isUndefinedOrZeroLength(treeNodes[i].children)) {
          this.setNodeLabels(treeNodes[i].children);
        }
      }
    }
  }

  getFilterGroupOption(result): void {
    const filtergroup = [];
    for (let i = 0; i < result.length; i++) {
      const group = result[i]['data'];
      group.label = group.sysFeatureGroup_GroupName;
      group.value = group.sysFeatureGroup_GroupId;
      filtergroup.push(group);
    }
    this.filterGroup = filtergroup;
  }
}
