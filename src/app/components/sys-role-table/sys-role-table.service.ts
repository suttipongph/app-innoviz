import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { SysFeatureGroupRoleView, SysRoleTableItemView, SysRoleTableListView } from 'shared/models/viewModel';
import { TreeNode } from 'shared/models/primeModel';

@Injectable()
export class SysRoleTableService {
  serviceKey = 'id';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getSysRoleTableToList(search: SearchParameter): Observable<SearchResult<SysRoleTableListView>> {
    const url = `${this.servicePath}/GetSysRoleTableList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteSysRoleTable(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteSysRoleTable`;
    return this.dataGateway.delete(url, row);
  }
  getSysRoleTableById(id: string): Observable<SysRoleTableItemView> {
    const url = `${this.servicePath}/GetSysRoleTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createSysRoleTable(vmModel: SysRoleTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateSysRoleTable`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateSysRoleTable(vmModel: SysRoleTableItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateSysRoleTable`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getSysFeatureGroupRoleViewList(): Observable<SysFeatureGroupRoleView[]> {
    const url = `${this.servicePath}/GetSysFeatureGroupRoleViewList`;
    return this.dataGateway.get(url);
  }
  getRoleDataForExport(id: string): Observable<any> {
    const url = `${this.servicePath}/GetExportRoleData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getSysFeatureGroupStructure(): Observable<TreeNode[]> {
    const url = `${this.servicePath}/GetSysFeatureGroupStructure`;
    return this.dataGateway.get(url);
  }
}
