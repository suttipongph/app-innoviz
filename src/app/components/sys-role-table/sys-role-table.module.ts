import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SysRoleTableService } from './sys-role-table.service';
import { SysRoleTableListComponent } from './sys-role-table-list/sys-role-table-list.component';
import { SysRoleTableItemComponent } from './sys-role-table-item/sys-role-table-item.component';
import { SysRoleTableItemCustomComponent } from './sys-role-table-item/sys-role-table-item-custom.component';
import { SysRoleTableListCustomComponent } from './sys-role-table-list/sys-role-table-list-custom.component';
import { SysRoleTableFormComponent } from './sys-role-table-form/sys-role-table-form.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [SysRoleTableListComponent, SysRoleTableItemComponent, SysRoleTableFormComponent],
  providers: [SysRoleTableService, SysRoleTableItemCustomComponent, SysRoleTableListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [SysRoleTableListComponent, SysRoleTableItemComponent]
})
export class SysRoleTableComponentModule {}
