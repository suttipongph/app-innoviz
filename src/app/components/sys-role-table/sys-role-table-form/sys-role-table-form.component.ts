import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode, isUndefinedOrZeroLength, siteToNum } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { SysRoleTableItemView, AccessLevelModel } from 'shared/models/viewModel';
import { SysRoleTableService } from '../sys-role-table.service';
import { SysRoleTableItemCustomComponent } from '../sys-role-table-item/sys-role-table-item-custom.component';
import { ValidationService } from 'shared/services/validation.service';
@Component({
  selector: 'sys-role-table-form',
  templateUrl: './sys-role-table-form.component.html',
  styleUrls: ['./sys-role-table-form.component.scss']
})
export class SysRoleTableFormComponent extends BaseItemComponent<SysRoleTableItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  @Input() vwModel: SysRoleTableItemView = new SysRoleTableItemView();
  @Input() parentAccessRight: AccessLevelModel;
  @Input() id: string;
  @Output() formModelChange = new EventEmitter<SysRoleTableItemView>();
  @Input()
  set validateStepOneFlag(value: boolean) {
    this.validateRoleFormFlag = value;
    if (this.validateRoleFormFlag) {
      this.getRoleFormValidation();
    }
  }
  // companyOptions: SelectItems[] = [];
  validateRoleFormFlag: boolean = false;
  siteLoginTypeOptions: SelectItems[] = [];
  constructor(
    private service: SysRoleTableService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: SysRoleTableItemCustomComponent,
    private validationService: ValidationService
  ) {
    super();
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.custom.roleAccessRightSubject.subscribe((res) => {
      this.accessRight = res;
      this.setFieldAccessing();
    });
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    //super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getSiteLoginTypeEnumDropDown(this.siteLoginTypeOptions);
  }
  getById(): Observable<SysRoleTableItemView> {
    return of(this.vwModel);
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.model = result;
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.model = this.vwModel;
    this.model.id = EmptyGuid;
    this.model.siteLoginType = siteToNum(this.custom.baseService.userDataService.getSiteLogin());
    super.setDefaultValueSystemFields();
    this.onAsyncRunner();
  }
  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.custom.roleFormAsyncRunnerDoneSubject.next(true);
        if (!isNullOrUndefined(this.parentAccessRight)) {
          this.accessRight = this.parentAccessRight;
        }

        this.setFieldAccessing();
        if (this.validateRoleFormFlag) {
          this.getRoleFormValidation();
        }
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    if (!isUndefinedOrZeroLength(this.accessRight)) {
      this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
        this.isView = res;
      });
      this.custom.getFieldAccessing(this.model, this.isView).subscribe((res) => {
        super.setBaseFieldAccessing(res);
      });
    }
  }
  setRelatedInfoOptions(): void {}
  setFunctionOptions(): void {}
  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {}
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  getRoleFormValidation(): boolean {
    return this.validationService.getFormValidation();
  }
  // field change ##############################################################
  onModelChange(e): void {
    this.formModelChange.emit(this.model);
  }
}
