import { Observable } from 'rxjs';
import { BaseServiceModel } from 'shared/models/systemModel';
export class SysRoleTableListCustomComponent {
  baseService: BaseServiceModel<any>;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }

  // ngOnInit() {
  //   this.checkAccessMode();
  //   this.setLoadPageData();
  // }

  // checkAccessMode() {
  //   this.accessMode = this.accessService.getAccessRight();
  // }

  // setOption() {
  //   this.option = new OptionModel();
  //   this.option.authorization = this.accessMode;
  //   this.option.key = 'id';
  //   const columns: ColumnModel[] = [
  //     {
  //       label: this.translate.get('LABEL.COMPANY_ID')['value'],
  //       textKey: 'companyGUID',
  //       type: ColumnType.MASTER,
  //       visibility: true,
  //       sorting: SortType.NONE,
  //       masterList: this.companyList,
  //       sortingKey: 'companyId'
  //     },
  //     {
  //       label: this.translate.get('LABEL.DISPLAY_NAME')['value'],
  //       textKey: 'displayName',
  //       type: ColumnType.STRING,
  //       visibility: true,
  //       sorting: SortType.NONE
  //     },
  //     {
  //       label: null,
  //       textKey: 'SysRoleTable',
  //       type: ColumnType.CONTROLLER,
  //       visibility: false,
  //       sorting: SortType.NONE
  //     }
  //   ];
  //   this.option.columns = columns;
  // }

  // setLoadPageData() {
  //   this.getCompanyToDropDownItem();
  // }

  // onSearch(e) {
  //   this.onBaseSearch(e);
  //   this.getSysRoleTableToList(e);
  // }
  // onCreate(e) {
  //   this.toItem(e);
  // }
  // onReload(e) {
  //   this.getSysRoleTableToList(e);
  // }
  // onView(e) {
  //   this.toItem(e);
  // }

  // onDelete(e: Deletion) {
  //   this.notificationService.showDeletionDialog();
  //   this.notificationService.isAccept.subscribe((isConfirm) => {
  //     if (isConfirm) {
  //       this.adminService.deleteRole(e).subscribe(
  //         (result) => {
  //           this.notificationService.showDeletedSuccess();
  //         },
  //         (error) => {
  //           this.notificationService.showErrorMessageFromResponse(error);
  //         },
  //         () => {
  //           this.onReload(e.searchParams);
  //         }
  //       );
  //     }
  //     this.notificationService.isAccept.observers = [];
  //   });
  // }
  // onSort(e) {
  //   this.getSysRoleTableToList(e);
  // }
  // onPaging(e) {
  //   this.getSysRoleTableToList(e);
  // }

  // toItem(parameter) {
  //   this.uiService.getNavigator(`${ROUTE_ADMIN.SYSROLETABLE}/${parameter}`);
  // }

  // getSysRoleTableToList(e) {
  //   this.sysListViewService.getSysRoleTableToList(e).subscribe(
  //     (result) => {
  //       this.searchResult = result;
  //     },
  //     (error) => {
  //       this.notificationService.showErrorMessageFromResponse(error);
  //     },
  //     () => {}
  //   );
  // }

  // getCompanyToDropDownItem() {
  //   this.dropDownService
  //     .getCompanyToDropDownItem()
  //     .finally(() => {
  //       this.setOption();
  //     })
  //     .subscribe(
  //       (result) => {
  //         this.companyList = result;
  //       },
  //       (error) => {},
  //       () => {}
  //     );
  // }
}
