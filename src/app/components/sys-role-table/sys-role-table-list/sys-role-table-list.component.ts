import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { AccessMode, AppConst, ColumnType, SortType } from 'shared/constants';
import {
  SearchParameter,
  RowIdentity,
  OptionModel,
  ColumnModel,
  SearchResult,
  GridFilterModel,
  PageInformationModel,
  SelectItems
} from 'shared/models/systemModel';
import { SysRoleTableListView } from 'shared/models/viewModel';
import { SysRoleTableService } from '../sys-role-table.service';
import { SysRoleTableListCustomComponent } from './sys-role-table-list-custom.component';

@Component({
  selector: 'sys-role-table-list',
  templateUrl: './sys-role-table-list.component.html',
  styleUrls: ['./sys-role-table-list.component.scss']
})
export class SysRoleTableListComponent extends BaseListComponent<SysRoleTableListView> implements BaseListInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  companyOptions: SelectItems[] = [];
  constructor(private service: SysRoleTableService, public custom: SysRoleTableListCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.setDataGridOption();
  }
  onFilter(param: GridFilterModel): void {
    this.baseDropdown.getCompanyDropDown(this.companyOptions);
  }
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.key = 'id';
    this.option.canCreate = this.getCanCreate();
    this.option.canView = this.getCanView();
    this.option.canDelete = this.getCanDelete();
    const columns: ColumnModel[] = [
      {
        label: 'LABEL.DISPLAY_NAME',
        textKey: 'displayName',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      }
    ];
    this.option.columns = columns;

    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<SysRoleTableListView>> {
    return this.service.getSysRoleTableToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteSysRoleTable(row));
  }
  getRowAuthorize(row: SysRoleTableListView[]): void {
    row.forEach((item) => {
      const businessRowAuth = item.displayName === AppConst.ADMINROLE ? AccessMode.editor : AccessMode.full;
      item.rowAuthorize = this.getSingleRowAuthorize(businessRowAuth, item);
    });
  }
}
