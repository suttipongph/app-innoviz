import { CustomerTableService } from 'components/customer-table/customer-table/customer-table.service';
import { Observable, of } from 'rxjs';
import { EmptyGuid, RefType } from 'shared/constants';
import { getRefTypeFromRoute, isUndefinedOrZeroLength, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, FileInformation } from 'shared/models/systemModel';
import { CustomerTableItemView, RefIdParm } from 'shared/models/viewModel';
import { ImportTaxReportItemView } from 'shared/models/viewModel/importTaxReportItemView';
import { ImportTaxReportService } from '../import-tax-report.service';

const firstGroup = ['REF_TYPE', 'REF_ID', 'NAME', 'FILE'];

export class ImportTaxReportCustomComponent {
  baseService: BaseServiceModel<CustomerTableService>;
  service: ImportTaxReportService;
  refType: string = '';
  refGUID: string = '';
  parentId: string = '';
  parentName: string = '';
  /*   getFieldAccessing(model: ImportTaxReportItemView): Observable<FieldAccessing[]> {
      const fieldAccessing: FieldAccessing[] = [];
      if (isUpdateMode(model.importTaxReportGUID)) {
        fieldAccessing.push({ filedIds: firstGroup, readonly: true });
        return of(fieldAccessing);
      }
    } */

  getFieldAccessing(): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }

  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  /*   getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: ImportTaxReportItemView): Observable<boolean> {
      return of(!canCreate);
    } */
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  setRefTypeRefGUID(passingObj: RefIdParm): void {
    this.refType = passingObj.refType.toString();
    this.refGUID = passingObj.refGUID;

    if (isUndefinedOrZeroLength(this.refType) || isUndefinedOrZeroLength(this.refGUID)) {
      const parentName = this.baseService.uiService.getRelatedInfoParentTableName();
      if (isUndefinedOrZeroLength(this.refType)) {
        this.refType = getRefTypeFromRoute(parentName).toString();
      }
      if (isUndefinedOrZeroLength(this.refGUID)) {
        this.refGUID = this.baseService.uiService.getKey(parentName);
      }
    }
  }
  getInitialData(): Observable<ImportTaxReportItemView> {
    let model = new ImportTaxReportItemView();
    model.importTaxReportGUID = EmptyGuid;
    this.parentId = this.refGUID;
    return this.service.getImportTaxReportByRefType(this.parentId, RefType.Customer);
  }

  getFileInformationFromModel(model: ImportTaxReportItemView): FileInformation {
    const document = new FileInformation();
    const contentType = this.baseService.fileService.getDataMimeHeaderFromFileName(model.fileName);
    document.base64 = contentType + model.base64Data;
    document.isRemovable = false;
    document.isPreviewable = true;
    document.contentType = contentType;
    document.fileName = model.fileName;
    document.fileDisplayName = model.fileName;
    return document;
  }
}