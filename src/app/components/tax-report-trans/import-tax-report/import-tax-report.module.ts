import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ImportTaxReportService } from './import-tax-report.service';
import { ImportTaxReportComponent } from './import-tax-report/import-tax-report.component';
import { ImportTaxReportCustomComponent } from './import-tax-report/import-tax-report-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ImportTaxReportComponent],
  providers: [ImportTaxReportService , ImportTaxReportCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ImportTaxReportComponent]
})
export class ImportTaxReportComponentModule {}