import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { ImportTaxReportItemView } from 'shared/models/viewModel/importTaxReportItemView';
@Injectable()
export class ImportTaxReportService {
  serviceKey = 'importTaxReportGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getImportTaxReportToList(search: SearchParameter): Observable<SearchResult<ImportTaxReportItemView>> {
    const url = `${this.servicePath}/GetImportTaxReportList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteImportTaxReport(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteImportTaxReport`;
    return this.dataGateway.delete(url, row);
  }
  getImportTaxReportById(id: string): Observable<ImportTaxReportItemView> {
    const url = `${this.servicePath}/GetImportTaxReportById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createImportTaxReport(vmModel: ImportTaxReportItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateImportTaxReport`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateImportTaxReport(vmModel: ImportTaxReportItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateImportTaxReport`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getCustomerTableById(id: string): Observable<ImportTaxReportItemView> {
    const url = `${this.servicePath}/GetCustomerTableById/id=${id}`;
    return this.dataGateway.get(url);
  }
  getImportTaxReportByRefType(id: string, refType: number): Observable<ImportTaxReportItemView> {
    const url = `${this.servicePath}/GetImportTaxReportByRefType/id=${id}/refType=${refType}`;
    return this.dataGateway.get(url);
  }
}