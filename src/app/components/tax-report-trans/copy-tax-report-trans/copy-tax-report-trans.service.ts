import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CopyTaxReportTransView } from 'shared/models/viewModel';
import { PageInformationModel, RefTypeModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class CopyTaxReportTransService {
  serviceKey = 'copyTaxReportTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  GetCopyTaxReportTransByRefType(data: RefTypeModel): Observable<CopyTaxReportTransView> {
    const url = `${this.servicePath}/GetCopyTaxReportTransByRefType`;
    return this.dataGateway.post(url, data);
  }
  getCopyTaxReportTransByRefTypeValidation(data: CopyTaxReportTransView): Observable<boolean> {
    const url = `${this.servicePath}/GetCopyTaxReportTransByRefTypeValidation`;
    return this.dataGateway.post(url, data);
  }
  copyTaxReportTrans(vmModel: CopyTaxReportTransView): Observable<CopyTaxReportTransView> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vmModel);
  }
}
