import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CopyTaxReportTransService } from './copy-tax-report-trans.service';
import { CopyTaxReportTransComponent } from './copy-tax-report-trans/copy-tax-report-trans.component';
import { CopyTaxReportTransCustomComponent } from './copy-tax-report-trans/copy-tax-report-trans-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [CopyTaxReportTransComponent],
  providers: [CopyTaxReportTransService, CopyTaxReportTransCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CopyTaxReportTransComponent]
})
export class CopyTaxReportTransComponentModule {}
