import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopyTaxReportTransComponent } from './copy-tax-report-trans.component';

describe('CopyTaxReportTransViewComponent', () => {
  let component: CopyTaxReportTransComponent;
  let fixture: ComponentFixture<CopyTaxReportTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CopyTaxReportTransComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyTaxReportTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
