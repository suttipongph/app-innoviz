import { Observable, of } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { EmptyGuid, RefType, ROUTE_MASTER_GEN } from 'shared/constants';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, RefTypeModel, TranslateModel } from 'shared/models/systemModel';
import { CopyTaxReportTransView } from 'shared/models/viewModel';
import { CopyTaxReportTransService } from '../copy-tax-report-trans.service';

const firstGroup = [];

export class CopyTaxReportTransCustomComponent {
  refGUID: string;
  refType: RefType;
  baseService: BaseServiceModel<CopyTaxReportTransService>;
  getFieldAccessing(): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(model: CopyTaxReportTransView): Observable<CopyTaxReportTransView> {
    const parentRoute = this.baseService.uiService.getRelatedInfoParentTableName();
    const parentKey = this.baseService.uiService.getRelatedInfoParentTableKey();
    const refType: RefTypeModel = new RefTypeModel();

    if(this.refType === RefType.CreditAppRequestTable){
      refType.refType = this.refType;
      refType.refGUID = this.refGUID;
    }
    return this.baseService.service.GetCopyTaxReportTransByRefType(refType).pipe(
      tap((result) => {
        model = result;
      })
    );
  }
  getDataValidation(model: CopyTaxReportTransView): Observable<boolean> {
    return this.baseService.service.getCopyTaxReportTransByRefTypeValidation(model).pipe(
      switchMap((result) => {
        if (result) {
          return of(result);
        } else {
          const translate: TranslateModel = {
            code: 'CONFIRM.90002',
            parameters: ['LABEL.COPY_TAX_REPORT_TRANS']
          };
          this.baseService.notificationService.showDeletionDialogManual(translate);
          return this.baseService.notificationService.isAccept.asObservable();
        }
      })
    );
  }
  getModelParam(model: CopyTaxReportTransView): Observable<CopyTaxReportTransView> {
    model.resultLabel = 'LABEL.TAX_REPORT_TRANSACTION';
    return of(model);
  }
  setRefTypeRefGUID(passingObj: RefTypeModel): void {
    if(!isNullOrUndefined(passingObj)){
      this.refType = passingObj.refType;
      this.refGUID = passingObj.refGUID;
    }    
  }
}
