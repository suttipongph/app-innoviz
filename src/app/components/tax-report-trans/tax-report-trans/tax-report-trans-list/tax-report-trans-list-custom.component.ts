import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CreditAppRequestStatus, RefType } from 'shared/constants';
import { BaseServiceModel, RefTypeModel } from 'shared/models/systemModel';
import { AccessModeView } from 'shared/models/viewModel';
import { isNullOrUndefined } from 'shared/functions/value.function';
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const CUSTOMER_TABLE = 'customertable';
const AMEND_CA = 'amendca';
const BUYER_MATCHING = 'buyermatching';
const LOAN_REQUEST = 'loanrequest';
export class TaxReportTransListCustomComponent {
  refGUID: string;
  refType: RefType;
  baseService: BaseServiceModel<any>;
  parentName = null;

  filterKey = null;
  isCustomerParent = false;
  accessModeView: AccessModeView = new AccessModeView();
  canImportTaxReport = true;
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setAccessModeByParentStatus(parentId: string, isWorkFlowMode: boolean): Observable<AccessModeView> {
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
        this.canImportTaxReport = false;
        this.filterKey = parentId;
        return this.baseService.service.getAccessModeByCreditAppRequestTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
            this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
            this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
            this.accessModeView.canView = true;
          })
        );
      case AMEND_CA:
        this.canImportTaxReport = false;
        this.filterKey = this.refGUID;
        return this.baseService.service.getAccessModeByCreditAppRequestTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
            this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
            this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
            this.accessModeView.canView = true;
          })
        );
      case BUYER_MATCHING:
      case LOAN_REQUEST:
        this.filterKey = this.refGUID;
        return this.baseService.service.getAccessModeByCreditAppRequestTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
            this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
            this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
            this.accessModeView.canView = true;
          })
        );
      case CUSTOMER_TABLE:
        this.isCustomerParent = true;
        this.filterKey = parentId;
        this.accessModeView.canCreate = true;
        this.accessModeView.canDelete = true;
        this.accessModeView.canView = true;
        return of(this.accessModeView);
      default:
        return of(this.accessModeView);
    }
  }
  getCanCreateByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canCreate;
  }
  getCanViewByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canView;
  }
  getCanDeleteByParent(accessRight: boolean): boolean {
    return accessRight && this.accessModeView.canDelete;
  }
  setRefTypeRefGUID(passingObj: RefTypeModel): void {
    if (!isNullOrUndefined(passingObj)) {
      this.refType = passingObj.refType;
      this.refGUID = passingObj.refGUID;
    }
  }
}
