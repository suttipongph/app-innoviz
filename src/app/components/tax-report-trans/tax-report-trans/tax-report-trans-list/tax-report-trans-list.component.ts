import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseListComponent } from 'core/components/base-list/base-list.component';
import { BaseListInterface } from 'core/interfaces/base-list/base-list.interface';
import { Observable } from 'rxjs';
import { ColumnType, RefType, ROUTE_FUNCTION_GEN, SortType } from 'shared/constants';
import {
  ColumnModel,
  GridFilterModel,
  OptionModel,
  PageInformationModel,
  RowIdentity,
  SearchParameter,
  SearchResult,
  SelectItems
} from 'shared/models/systemModel';
import { TaxReportTransListView } from 'shared/models/viewModel';
import { TaxReportTransService } from '../tax-report-trans.service';
import { TaxReportTransListCustomComponent } from './tax-report-trans-list-custom.component';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { EmptyGuid } from './../../../../shared/constants/constant';

@Component({
  selector: 'tax-report-trans-list',
  templateUrl: './tax-report-trans-list.component.html',
  styleUrls: ['./tax-report-trans-list.component.scss']
})
export class TaxReportTransListComponent extends BaseListComponent<TaxReportTransListView> implements BaseListInterface {
  id: any;
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }

  monthOfYearOptions: SelectItems[] = [];
  refTypeOptions: SelectItems[] = [];

  constructor(
    public custom: TaxReportTransListCustomComponent,
    public service: TaxReportTransService,
    private currentActivatedRoute: ActivatedRoute
  ) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.parentId = this.uiService.getRelatedInfoParentTableKey();
    this.custom.parentName = this.uiService.getRelatedInfoParentTableName();
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setRefTypeRefGUID(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.onEnumLoader();
    this.custom.setAccessModeByParentStatus(this.parentId, this.isWorkFlowMode()).subscribe((result) => {
      this.setDataGridOption();
      this.setFunctionOptions();
    });
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {}
  onEnumLoader(): void {
    this.baseDropdown.getMonthOfYearEnumDropDown(this.monthOfYearOptions);
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }

  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onFilter(param: GridFilterModel): void {}
  setDataGridOption(): void {
    this.option = new OptionModel();
    this.option.canCreate = this.custom.getCanCreateByParent(this.getCanCreate());
    this.option.canView = this.custom.getCanViewByParent(this.getCanView());
    this.option.canDelete = this.custom.getCanDeleteByParent(this.getCanDelete());
    this.option.key = 'taxReportTransGUID';
    const columns: ColumnModel[] = [
      {
        label: this.translate.instant('LABEL.YEAR'),
        textKey: 'year',
        type: ColumnType.INT,
        visibility: true,
        sorting: SortType.DESC,
        format: this.YEAR
      },
      {
        label: this.translate.instant('LABEL.MONTH'),
        textKey: 'month',
        type: ColumnType.ENUM,
        visibility: true,
        sorting: SortType.ASC,
        masterList: this.monthOfYearOptions
      },
      {
        label: this.translate.instant('LABEL.DESCRIPTION'),
        textKey: 'description',
        type: ColumnType.STRING,
        visibility: true,
        sorting: SortType.NONE
      },
      {
        label: this.translate.instant('LABEL.AMOUNT'),
        textKey: 'amount',
        type: ColumnType.DECIMAL,
        visibility: true,
        sorting: SortType.NONE,
        format: this.CURRENCY_13_2
      },
      {
        label: null,
        textKey: 'refGUID',
        type: ColumnType.MASTER,
        visibility: true,
        sorting: SortType.NONE,
        parentKey: isNullOrUndefined(this.custom.filterKey) ? EmptyGuid:this.custom.filterKey
      }
    ];
    this.option.columns = columns;
    super.setGridOptionMapping();
  }
  getList(search: SearchParameter): Observable<SearchResult<TaxReportTransListView>> {
    return this.service.getTaxReportTransToList(search);
  }
  onCreate(row: RowIdentity): void {
    super.onCreate(row);
  }
  onView(row: RowIdentity): void {
    super.onView(row);
  }
  onDelete(row: RowIdentity): void {
    super.onDelete(row, this.service.deleteTaxReportTrans(row));
  }
  setFunctionOptions(): void {
    this.functionItems = [
      {
        label: 'LABEL.COPY_TAX_REPORT_TRANS',
        command: () =>
          this.toFunction({
            path: ROUTE_FUNCTION_GEN.COPY_TAX_REPORT_TRANS,
            parameters: { refGUID: this.custom.refGUID, refType: this.custom.refType }
          }),
        disabled: !this.custom.accessModeView.canCreate,
        visible: !this.custom.isCustomerParent
      },
      {
        label: 'LABEL.IMPORT_TAX_REPORT',
        command: () =>
          this.toFunction({
            path: ROUTE_FUNCTION_GEN.IMPORT_TAX_REPORT,
            parameters: {
              refType: this.custom.refType,
              refGUID: this.custom.refGUID
            }
          }),
        disabled: !this.custom.accessModeView.canCreate,
        visible: this.custom.canImportTaxReport
      }
    ];

    super.setFunctionOptionsMapping();
  }
}
