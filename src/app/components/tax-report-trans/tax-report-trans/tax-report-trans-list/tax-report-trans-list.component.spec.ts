import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TaxReportTransListComponent } from './tax-report-trans-list.component';

describe('TaxReportTransListViewComponent', () => {
  let component: TaxReportTransListComponent;
  let fixture: ComponentFixture<TaxReportTransListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaxReportTransListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxReportTransListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
