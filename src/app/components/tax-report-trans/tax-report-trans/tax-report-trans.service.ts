import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { AccessModeView, TaxReportTransItemView, TaxReportTransListView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class TaxReportTransService {
  serviceKey = 'taxReportTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getTaxReportTransToList(search: SearchParameter): Observable<SearchResult<TaxReportTransListView>> {
    const url = `${this.servicePath}/GetTaxReportTransList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteTaxReportTrans(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteTaxReportTrans`;
    return this.dataGateway.delete(url, row);
  }
  getTaxReportTransById(id: string): Observable<TaxReportTransItemView> {
    const url = `${this.servicePath}/GetTaxReportTransById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createTaxReportTrans(vmModel: TaxReportTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateTaxReportTrans`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateTaxReportTrans(vmModel: TaxReportTransItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateTaxReportTrans`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getInitialData(id: string): Observable<TaxReportTransItemView> {
    const url = `${this.servicePath}/GetTaxReportTransInitialData/id=${id}`;
    return this.dataGateway.get(url);
  }
  getAccessModeByCreditAppRequestTable(id: string): Observable<AccessModeView> {
    const url = `${this.servicePath}/GetAccessModeByCreditAppRequestTable/id=${id}`;
    return this.dataGateway.get(url);
  }
}
