import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TaxReportTransService } from './tax-report-trans.service';
import { TaxReportTransListComponent } from './tax-report-trans-list/tax-report-trans-list.component';
import { TaxReportTransItemComponent } from './tax-report-trans-item/tax-report-trans-item.component';
import { TaxReportTransItemCustomComponent } from './tax-report-trans-item/tax-report-trans-item-custom.component';
import { TaxReportTransListCustomComponent } from './tax-report-trans-list/tax-report-trans-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [TaxReportTransListComponent, TaxReportTransItemComponent],
  providers: [TaxReportTransService, TaxReportTransItemCustomComponent, TaxReportTransListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [TaxReportTransListComponent, TaxReportTransItemComponent]
})
export class TaxReportTransComponentModule {}
