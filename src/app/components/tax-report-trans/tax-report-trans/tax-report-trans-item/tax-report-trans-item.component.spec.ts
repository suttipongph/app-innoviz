import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxReportTransItemComponent } from './tax-report-trans-item.component';

describe('TaxReportTransItemViewComponent', () => {
  let component: TaxReportTransItemComponent;
  let fixture: ComponentFixture<TaxReportTransItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaxReportTransItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxReportTransItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
