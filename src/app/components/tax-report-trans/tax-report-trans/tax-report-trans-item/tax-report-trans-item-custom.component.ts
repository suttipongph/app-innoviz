import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { CreditAppRequestStatus, RefType } from 'shared/constants';
import { isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, RefTypeModel } from 'shared/models/systemModel';
import { AccessModeView, TaxReportTransItemView } from 'shared/models/viewModel';

const firstGroup = ['REF_TYPE', 'REF_ID'];
const CREDIT_APP_REQUEST_TABLE = 'creditapprequesttable';
const CUSTOMER_TABLE = 'customertable';
const AMEND_CA = 'amendca';
const BUYER_MATCHING = 'buyermatching';
const LOAN_REQUEST = 'loanrequest';
export class TaxReportTransItemCustomComponent {
  refGUID: string;
  refType: RefType;
  baseService: BaseServiceModel<any>;
  parentName = null;
  accessModeView: AccessModeView = new AccessModeView();
  getFieldAccessing(model: TaxReportTransItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: TaxReportTransItemView, isWorkFlowMode: boolean): Observable<boolean> {
    return this.setAccessModeByParentStatus(model, isWorkFlowMode).pipe(
      map((result) => (isUpdateMode(model.taxReportTransGUID) ? !(result.canView && canUpdate) : !(result.canCreate && canCreate)))
    );
  }
  setAccessModeByParentStatus(model: TaxReportTransItemView, isWorkFlowMode: boolean): Observable<AccessModeView> {
    let parentId = this.baseService.uiService.getRelatedInfoParentTableKey();
    switch (this.parentName) {
      case CREDIT_APP_REQUEST_TABLE:
      case AMEND_CA:
      case BUYER_MATCHING:
      case LOAN_REQUEST:
        return this.baseService.service.getAccessModeByCreditAppRequestTable(parentId).pipe(
          tap((result) => {
            this.accessModeView = result as AccessModeView;
            const isDraftStatus = this.accessModeView.accessStatus === CreditAppRequestStatus.Draft;
            this.accessModeView.canCreate = isDraftStatus ? true : this.accessModeView.canCreate && isWorkFlowMode;
            this.accessModeView.canDelete = isDraftStatus ? true : this.accessModeView.canDelete && isWorkFlowMode;
            this.accessModeView.canView = isDraftStatus ? true : this.accessModeView.canView && isWorkFlowMode;
          })
        );
      case CUSTOMER_TABLE:
        this.accessModeView.canCreate = true;
        this.accessModeView.canDelete = true;
        this.accessModeView.canView = true;
        return of(this.accessModeView);
      default:
        return of(this.accessModeView);
    }
  }
  getInnitialDataCustom(id: string): Observable<TaxReportTransItemView> {
    switch (this.parentName) {
      case AMEND_CA:
        return this.baseService.service.getInitialData(this.refGUID);
      default:
        return this.baseService.service.getInitialData(id);
    }
  }
  setRefTypeRefGUID(passingObj: RefTypeModel): void {
    this.refType = passingObj.refType;
    this.refGUID = passingObj.refGUID;
  }
}
