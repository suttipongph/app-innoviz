import { Observable, of } from 'rxjs';
import { EmptyGuid, ProcessTransType, PROCESS_TRANS_TYPE, StagingBatchStatus } from 'shared/constants';
import { Confirmation } from 'shared/models/primeModel';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { GenerateAccountingStagingView } from 'shared/models/viewModel';

const firstGroup = ['LIST_PROCESS_TRANS_TYPE', 'STAGING_BATCH_STATUS'];

export class GenerateAccountingStagingCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: GenerateAccountingStagingView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<GenerateAccountingStagingView> {
    let model = new GenerateAccountingStagingView();
    model.generateAccountingStagingGUID = EmptyGuid;
    model.listProcessTransType = [
      ProcessTransType.Invoice,
      ProcessTransType.IncomeRealization,
      ProcessTransType.Payment,
      ProcessTransType.PaymentDetail
    ];
    model.stagingBatchStatus = [StagingBatchStatus.None, StagingBatchStatus.Failed];
    return of(model);
  }
  showConfirmDialog(model: GenerateAccountingStagingView, option: SelectItems[]): Observable<any> {
    let listProcessTransTypeString = '';
    model.listProcessTransType.forEach((f, idx) => {
      listProcessTransTypeString += this.baseService.translate.instant(option.find((fi) => fi.value === f).label);
      if (idx < model.listProcessTransType.length - 1) {
        listProcessTransTypeString += ', ';
      }
    });
    const confirmation: Confirmation = {
      header: this.baseService.translate.instant('CONFIRM.CONFIRM'),
      message: this.baseService.translate.instant('CONFIRM.90010', [listProcessTransTypeString])
    };
    return new Observable((observer) => {
      this.baseService.notificationService.showConfirmDialog(confirmation);
      this.baseService.notificationService.isAccept.subscribe((isConfirm) => {
        this.baseService.notificationService.isAccept.observers = [];
        observer.next(isConfirm);
      });
    });
  }
}
