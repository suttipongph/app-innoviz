import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateAccountingStagingComponent } from './generate-accounting-staging.component';

describe('GenerateAccountingStagingViewComponent', () => {
  let component: GenerateAccountingStagingComponent;
  let fixture: ComponentFixture<GenerateAccountingStagingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GenerateAccountingStagingComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateAccountingStagingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
