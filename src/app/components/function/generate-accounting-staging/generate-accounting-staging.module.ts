import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GenerateAccountingStagingService } from './generate-accounting-staging.service';
import { GenerateAccountingStagingComponent } from './generate-accounting-staging/generate-accounting-staging.component';
import { GenerateAccountingStagingCustomComponent } from './generate-accounting-staging/generate-accounting-staging-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [GenerateAccountingStagingComponent],
  providers: [GenerateAccountingStagingService, GenerateAccountingStagingCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [GenerateAccountingStagingComponent]
})
export class GenerateAccountingStagingComponentModule {}
