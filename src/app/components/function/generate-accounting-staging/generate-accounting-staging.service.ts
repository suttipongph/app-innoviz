import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { GenerateAccountingStagingView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class GenerateAccountingStagingService {
  serviceKey = 'generateAccountingStagingGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getGenerateAccountingStagingToList(search: SearchParameter): Observable<SearchResult<GenerateAccountingStagingView>> {
    const url = `${this.servicePath}/GetGenerateAccountingStagingList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteGenerateAccountingStaging(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteGenerateAccountingStaging`;
    return this.dataGateway.delete(url, row);
  }
  getGenerateAccountingStagingById(id: string): Observable<GenerateAccountingStagingView> {
    const url = `${this.servicePath}/GetGenerateAccountingStagingById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createGenerateAccountingStaging(vmModel: GenerateAccountingStagingView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateGenerateAccountingStaging`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateGenerateAccountingStaging(vmModel: GenerateAccountingStagingView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateGenerateAccountingStaging`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
