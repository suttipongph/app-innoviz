import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GenInterestRealizeProcessTransService } from './gen-interest-realize-process-trans.service';
import { GenInterestRealizeProcessTransComponent } from './gen-interest-realize-process-trans/gen-interest-realize-process-trans.component';
import { GenInterestRealizeProcessTransCustomComponent } from './gen-interest-realize-process-trans/gen-interest-realize-process-trans-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [GenInterestRealizeProcessTransComponent],
  providers: [GenInterestRealizeProcessTransService, GenInterestRealizeProcessTransCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [GenInterestRealizeProcessTransComponent]
})
export class GenInterestRealizeProcessTransComponentModule {}
