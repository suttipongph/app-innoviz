import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { GenInterestRealizeProcessTransView } from 'shared/models/viewModel/genInterestRealizeProcessTransView';
@Injectable()
export class GenInterestRealizeProcessTransService {
  serviceKey = 'genInterestRealizeProcessTransGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getGenInterestRealizeProcessTrans(vmModel: GenInterestRealizeProcessTransView): Observable<GenInterestRealizeProcessTransView> {
    const url = `${this.servicePath}/GetGenInterestRealizeProcessTrans`;
    return this.dataGateway.post(url, vmModel);
  }
  getGenInterestRealizeProcessTransValidation(vmModel: GenInterestRealizeProcessTransView): Observable<boolean> {
    const url = `${this.servicePath}/GetGenInterestRealizeProcessTransValidation`;
    return this.dataGateway.post(url, vmModel);
  }
}
