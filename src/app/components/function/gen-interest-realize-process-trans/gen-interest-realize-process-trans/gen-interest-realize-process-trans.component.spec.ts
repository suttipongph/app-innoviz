import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenInterestRealizeProcessTransComponent } from './gen-interest-realize-process-trans.component';

describe('GenInterestRealizeProcessTransViewComponent', () => {
  let component: GenInterestRealizeProcessTransComponent;
  let fixture: ComponentFixture<GenInterestRealizeProcessTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GenInterestRealizeProcessTransComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenInterestRealizeProcessTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
