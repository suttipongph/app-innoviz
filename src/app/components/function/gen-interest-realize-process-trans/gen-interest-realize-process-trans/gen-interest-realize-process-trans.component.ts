import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { GenInterestRealizeProcessTransView } from 'shared/models/viewModel/genInterestRealizeProcessTransView';
import { GenInterestRealizeProcessTransService } from '../gen-interest-realize-process-trans.service';
import { GenInterestRealizeProcessTransCustomComponent } from './gen-interest-realize-process-trans-custom.component';
@Component({
  selector: 'gen-interest-realize-process-trans',
  templateUrl: './gen-interest-realize-process-trans.component.html',
  styleUrls: ['./gen-interest-realize-process-trans.component.scss']
})
export class GenInterestRealizeProcessTransComponent extends BaseItemComponent<GenInterestRealizeProcessTransView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  productTypeOptions: SelectItems[] = [];

  constructor(
    private service: GenInterestRealizeProcessTransService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: GenInterestRealizeProcessTransCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialCreatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getProductTypeEnumDropDown(this.productTypeOptions);
  }
  getById(): Observable<GenInterestRealizeProcessTransView> {
    return null;
  }
  getInitialData(): Observable<GenInterestRealizeProcessTransView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.service.getGenInterestRealizeProcessTransValidation(this.model).subscribe((isValid) => {
          if (isValid) {
            const header: TranslateModel = { code: 'CONFIRM.CONFIRM' };
            const message: TranslateModel = { code: 'CONFIRM.90014', parameters: [this.model.transDate] };
            this.notificationService.showManualConfirmDialog(header, message);
            this.notificationService.isAccept.subscribe((isConfirm) => {
              if (isConfirm) {
                this.onSubmit(true);
              }
              this.notificationService.isAccept.observers = [];
            });
          }
        });
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    super.onExecuteFunction(this.service.getGenInterestRealizeProcessTrans(this.model));
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onClose(): void {
    super.onBack();
  }
}
