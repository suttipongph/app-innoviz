import { Observable, of } from 'rxjs';
import { EmptyGuid } from 'shared/constants';
import { datetoString, getLastDayOfMonth } from 'shared/functions/date.function';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { GenInterestRealizeProcessTransView } from 'shared/models/viewModel/genInterestRealizeProcessTransView';

const firstGroup = [];

export class GenInterestRealizeProcessTransCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: GenInterestRealizeProcessTransView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<GenInterestRealizeProcessTransView> {
    let model = new GenInterestRealizeProcessTransView();
    var lastDayOfMonth = datetoString(getLastDayOfMonth(new Date()));
    model.asofDate = lastDayOfMonth;
    model.transDate = lastDayOfMonth;
    return of(model);
  }
}
