import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InterfaceIntercoInvSettlementService } from './interface-interco-inv-settlement.service';
import { InterfaceIntercoInvSettlementComponent } from './interface-interco-inv-settlement/interface-interco-inv-settlement.component';
import { InterfaceIntercoInvSettlementCustomComponent } from './interface-interco-inv-settlement/interface-interco-inv-settlement-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [InterfaceIntercoInvSettlementComponent],
  providers: [InterfaceIntercoInvSettlementService, InterfaceIntercoInvSettlementCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [InterfaceIntercoInvSettlementComponent]
})
export class InterfaceIntercoInvSettlementComponentModule {}
