import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { InterfaceIntercoInvSettlementView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class InterfaceIntercoInvSettlementService {
  serviceKey = 'interfaceIntercoInvSettlementGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getInterfaceIntercoInvSettlementToList(search: SearchParameter): Observable<SearchResult<InterfaceIntercoInvSettlementView>> {
    const url = `${this.servicePath}/GetInterfaceIntercoInvSettlementList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteInterfaceIntercoInvSettlement(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteInterfaceIntercoInvSettlement`;
    return this.dataGateway.delete(url, row);
  }
  getInterfaceIntercoInvSettlementById(id: string): Observable<InterfaceIntercoInvSettlementView> {
    const url = `${this.servicePath}/GetInterfaceIntercoInvSettlementById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createInterfaceIntercoInvSettlement(vmModel: InterfaceIntercoInvSettlementView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateInterfaceIntercoInvSettlement`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateInterfaceIntercoInvSettlement(vmModel: InterfaceIntercoInvSettlementView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateInterfaceIntercoInvSettlement`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
