import { Observable, of } from 'rxjs';
import { EmptyGuid, InterfaceStatus } from 'shared/constants';
import { Confirmation } from 'shared/models/primeModel';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { InterfaceIntercoInvSettlementView } from 'shared/models/viewModel';

const firstGroup = ['INTERFACE_STATUS'];

export class InterfaceIntercoInvSettlementCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: InterfaceIntercoInvSettlementView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<InterfaceIntercoInvSettlementView> {
    let model = new InterfaceIntercoInvSettlementView();
    model.interfaceIntercoInvSettlementGUID = EmptyGuid;
    model.interfaceStatus = [InterfaceStatus.None, InterfaceStatus.Failed];
    return of(model);
  }
  showConfirmDialog(model: InterfaceIntercoInvSettlementView): Observable<any> {
    const confirmation: Confirmation = {
      header: this.baseService.translate.instant('CONFIRM.CONFIRM'),
      message: this.baseService.translate.instant('CONFIRM.90016', [
        this.baseService.translate.instant('LABEL.INTERFACE_INTERCOMPANY_INVOICE_SETTLEMENT_STAGING')
      ])
    };
    return new Observable((observer) => {
      this.baseService.notificationService.showConfirmDialog(confirmation);
      this.baseService.notificationService.isAccept.subscribe((isConfirm) => {
        this.baseService.notificationService.isAccept.observers = [];
        observer.next(isConfirm);
      });
    });
  }
}
