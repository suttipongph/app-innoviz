import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterfaceIntercoInvSettlementComponent } from './interface-interco-inv-settlement.component';

describe('InterfaceIntercoInvSettlementViewComponent', () => {
  let component: InterfaceIntercoInvSettlementComponent;
  let fixture: ComponentFixture<InterfaceIntercoInvSettlementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InterfaceIntercoInvSettlementComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterfaceIntercoInvSettlementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
