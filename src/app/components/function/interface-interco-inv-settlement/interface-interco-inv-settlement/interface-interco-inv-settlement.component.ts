import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseBatchFunctionComponent } from 'core/components/base-batch-function/base-batch-function.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { BATCH_FN_MAPPING_KEY } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { InterfaceIntercoInvSettlementView } from 'shared/models/viewModel';
import { InterfaceIntercoInvSettlementService } from '../interface-interco-inv-settlement.service';
import { InterfaceIntercoInvSettlementCustomComponent } from './interface-interco-inv-settlement-custom.component';

@Component({
  selector: 'interface-interco-inv-settlement',
  templateUrl: './interface-interco-inv-settlement.component.html',
  styleUrls: ['./interface-interco-inv-settlement.component.scss']
})
export class InterfaceIntercoInvSettlementComponent
  extends BaseBatchFunctionComponent<InterfaceIntercoInvSettlementView>
  implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  interfaceStatusOptions: SelectItems[] = [];

  constructor(
    private service: InterfaceIntercoInvSettlementService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: InterfaceIntercoInvSettlementCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.batchApiFunctionMappingKey = BATCH_FN_MAPPING_KEY.INTERFACE_INTERCO_INV_SETTLEMENT;
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialCreatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getInterfaceStatusEnumDropDown(this.interfaceStatusOptions);
  }
  getById(): Observable<InterfaceIntercoInvSettlementView> {
    return this.service.getInterfaceIntercoInvSettlementById(this.id);
  }
  getInitialData(): Observable<InterfaceIntercoInvSettlementView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    this.custom.showConfirmDialog(this.model).subscribe((res) => {
      if (res) {
        super.submitBatchFunctionParameters(this.model);
      }
    });
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onClose(): void {
    super.onBack();
  }
}
