import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ManageAgreementView, ManageAssignAgreementValidationResult } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class ManageAgreementService {
  serviceKey = 'manageAgreementGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  // getManageAgreementToList(search: SearchParameter): Observable<SearchResult<ManageAgreementView>> {
  //   const url = `${this.servicePath}/GetManageAgreementList/${search.branchFilterMode}`;
  //   return this.dataGateway.getList(url, search);
  // }
  // deleteManageAgreement(row: RowIdentity): Observable<boolean> {
  //   const url = `${this.servicePath}/DeleteManageAgreement`;
  //   return this.dataGateway.delete(url, row);
  // }
  // getAssignmentManageAgreementById(id: string): Observable<ManageAgreementView> {
  //   const url = `${this.servicePath}/GetAssignmentManageAgreementById/id=${id}`;
  //   return this.dataGateway.get(url);
  // }
  // createManageAgreement(vmModel: ManageAgreementView): Observable<ResponseModel> {
  //   const url = `${this.servicePath}/CreateManageAgreement`;
  //   return this.dataGateway.create(url, vmModel, this.serviceKey);
  // }
  manageAgreement(vmModel: ManageAgreementView): Observable<ManageAgreementView> {
    const url = `${this.servicePath}/ManageAgreement`;
    return this.dataGateway.post(url, vmModel);
  }
  getManageAgreementValidation(vmModel: ManageAgreementView): Observable<boolean> {
    const url = `${this.servicePath}/GetManageAgreementValidation`;
    return this.dataGateway.post(url, vmModel);
  }
  getManageAgreementByRefType(vmModel: ManageAgreementView): Observable<ManageAgreementView> {
    const url = `${this.servicePath}/getManageAgreementByRefType`;
    return this.dataGateway.post(url, vmModel);
  }
  getManageAssignAgreementValidation(vmModel: ManageAgreementView): Observable<ManageAssignAgreementValidationResult> {
    const url = `${this.servicePath}/GetManageAssignmentAgreementValidation`;
    return this.dataGateway.post(url, vmModel);
  }
}
