import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmptyGuid, ROUTE_RELATED_GEN, ROUTE_FUNCTION_GEN, ManageAgreementAction, RefType } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { ManageAgreementView } from 'shared/models/viewModel';
import { ManageAgreementService } from '../manage-agreement.service';
import { ManageAgreementCustomComponent } from './manage-agreement-custom.component';
@Component({
  selector: 'manage-agreement',
  templateUrl: './manage-agreement.component.html',
  styleUrls: ['./manage-agreement.component.scss']
})
export class ManageAgreementComponent extends BaseItemComponent<ManageAgreementView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  agreementDocTypeOptions: SelectItems[] = [];
  documentReasonOptions: SelectItems[] = [];
  action: number;

  constructor(private service: ManageAgreementService, private currentActivatedRoute: ActivatedRoute, public custom: ManageAgreementCustomComponent) {
    super();
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.refType = passingObj['refType'];
    this.refGuid = passingObj['refGuid'];
    this.action = passingObj['action'];
    this.custom.setTitle(this.action);
    this.custom.getModelParam(this.model, this.refType, this.refGuid, this.action);
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialUpdatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getAgreementDocTypeEnumDropDown(this.agreementDocTypeOptions);
  }
  getById(): Observable<ManageAgreementView> {
    return this.service.getManageAgreementByRefType(this.model);
  }
  getInitialData(): Observable<ManageAgreementView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {}
  onAsyncRunner(model?: any): void {
    forkJoin(this.baseDropdown.getDocumentReasonDropDown(this.refType.toString())).subscribe(
      ([documentReason]) => {
        this.documentReasonOptions = documentReason;
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model, this.action).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
    this.custom.getVisibleFieldSet(this.refType, this.action).subscribe((res) => {
      super.setVisibilityFieldSet(res);
    });
  }
  onSave(validation: FormValidationModel): void {}
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        if (this.refType == RefType.AssignmentAgreement) {
          this.service.getManageAssignAgreementValidation(this.model).subscribe((result) => {
            if (result.isValid) {
              if (this.action === ManageAgreementAction.Post) {
                const header: TranslateModel = { code: 'CONFIRM.CONFIRM' };
                let message: TranslateModel = { code: 'CONFIRM.90073', parameters: [this.model.label] };
                if (!result.hasLine) {
                  message = { code: 'CONFIRM.90019', parameters: ['LABEL.ASSIGNMENT_AGREEMENT_LINE'] };
                }
                this.notificationService.showManualConfirmDialog(header, message);
                this.notificationService.isAccept.subscribe((isConfirm) => {
                  if (isConfirm) {
                    this.onSubmit(true, this.model);
                  }
                  this.notificationService.isAccept.observers = [];
                });
              } else {
                this.onSubmit(true, this.model);
              }
            }
          });
        } else {
          this.service.getManageAgreementValidation(this.model).subscribe((isValid) => {
            if (isValid) {
              if (this.action === ManageAgreementAction.Post) {
                const header: TranslateModel = { code: 'CONFIRM.CONFIRM' };
                const message: TranslateModel = { code: 'CONFIRM.90073', parameters: [this.model.label] };
                this.notificationService.showManualConfirmDialog(header, message);
                this.notificationService.isAccept.subscribe((isConfirm) => {
                  if (isConfirm) {
                    this.onSubmit(true, this.model);
                  }
                  this.notificationService.isAccept.observers = [];
                });
              } else {
                this.onSubmit(true, this.model);
              }
            }
          });
        }
      }
    });
  }
  onSubmit(isColsing: boolean, model: ManageAgreementView): void {
    super.onExecuteFunction(this.service.manageAgreement(model));
  }
  onClose(): void {
    super.onBack();
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
}
