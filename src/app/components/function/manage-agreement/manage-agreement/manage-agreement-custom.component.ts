import { Observable, of } from 'rxjs';
import { EmptyGuid, ManageAgreementAction, RefType } from 'shared/constants';
import { toMapModel } from 'shared/functions/model.function';
import { getProductType } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { ManageAgreementView } from 'shared/models/viewModel';
import { ManageAgreementService } from '../manage-agreement.service';

const firstGroup = [
  'INTERNAL_AGREEMENT_ID',
  'AGREEMENT_ID',
  'DESCRIPTION',
  'AGREEMENT_DOC_TYPE',
  'AGREEMENT_DATE',
  'CUSTOMER_ID',
  'CUSTOMER_NAME',
  'BUYER_ID',
  'BUYER_NAME',
  'ASSIGNMENT_METHOD_ID',
  'AMOUNT',
  'EXPIRY_DATE',
  'AFFILIATE'
];

export class ManageAgreementCustomComponent {
  title: string;
  baseService: BaseServiceModel<ManageAgreementService>;
  isReasonRequire: boolean = false;
  isSigningDateRequire: boolean = false;
  getFieldAccessing(model: ManageAgreementView, action: number): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    if ([ManageAgreementAction.Cancel, ManageAgreementAction.Close].some((s) => s == action)) {
      this.isReasonRequire = true;
    } else if ([ManageAgreementAction.Sign].some((s) => s == action)) {
      this.isSigningDateRequire = true;
    }
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<ManageAgreementView> {
    let model = new ManageAgreementView();
    model.manageAgreementGUID = EmptyGuid;
    return of(model);
  }
  getVisibleFieldSet(refType: number, action: number): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: ['BUYER', 'ASSIGNMENT_AGREEMENT', 'GUARANTOR', 'SIGN', 'REASON'], invisible: true });
    if (action === ManageAgreementAction.Sign) {
      fieldAccessing.push({ filedIds: ['SIGN'], invisible: false });
    }
    if (action === ManageAgreementAction.Cancel || action === ManageAgreementAction.Close) {
      fieldAccessing.push({ filedIds: ['REASON'], invisible: false });
    }
    switch (refType) {
      case RefType.AssignmentAgreement:
        fieldAccessing.push({ filedIds: ['BUYER', 'ASSIGNMENT_AGREEMENT'], invisible: false });
        break;
      case RefType.MainAgreement:
        fieldAccessing.push({ filedIds: ['BUYER'], invisible: false });
        break;
      case RefType.GuarantorAgreement:
        fieldAccessing.push({ filedIds: ['GUARANTOR'], invisible: false });
        break;
    }

    return of(fieldAccessing);
  }
  setTitle(action: any) {
    switch (action) {
      case ManageAgreementAction.Post:
        this.title = 'LABEL.POST_AGREEMENT';
        break;
      case ManageAgreementAction.Send:
        this.title = 'LABEL.SEND_AGREEMENT';
        break;
      case ManageAgreementAction.Sign:
        this.title = 'LABEL.SIGN_AGREEMENT';
        break;
      case ManageAgreementAction.Cancel:
        this.title = 'LABEL.CANCEL_AGREEMENT';
        break;
      case ManageAgreementAction.Close:
        this.title = 'LABEL.CLOSE_AGREEMENT';
        break;
      default:
        break;
    }
  }
  getModelParam(model: ManageAgreementView, refType: number, refGUID: string, action: number): Observable<ManageAgreementView> {
    model.label = this.title;
    model.refType = refType;
    model.refGUID = refGUID;
    model.manageAgreementAction = action;
    switch (refType) {
      case RefType.MainAgreement:
      case RefType.GuarantorAgreement:
      case RefType.BusinessCollateralAgreement:
        const masterRoute = this.baseService.uiService.getMasterRoute();
        model.productType = Number(getProductType(masterRoute));
    }
    return of(model);
  }
}
