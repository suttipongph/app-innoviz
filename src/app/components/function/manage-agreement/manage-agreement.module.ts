import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ManageAgreementService } from './manage-agreement.service';
import { ManageAgreementComponent } from './manage-agreement/manage-agreement.component';
import { ManageAgreementCustomComponent } from './manage-agreement/manage-agreement-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [ManageAgreementComponent],
  providers: [ManageAgreementService, ManageAgreementCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ManageAgreementComponent]
})
export class ManageAgreementComponentModule {}
