import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseBatchFunctionComponent } from 'core/components/base-batch-function/base-batch-function.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { BATCH_FN_MAPPING_KEY } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { InterfaceVendorInvoiceView } from 'shared/models/viewModel';
import { InterfaceVendorInvoiceService } from '../interface-vendor-invoice.service';
import { InterfaceVendorInvoiceCustomComponent } from './interface-vendor-invoice-custom.component';

@Component({
  selector: 'interface-vendor-invoice',
  templateUrl: './interface-vendor-invoice.component.html',
  styleUrls: ['./interface-vendor-invoice.component.scss']
})
export class InterfaceVendorInvoiceComponent extends BaseBatchFunctionComponent<InterfaceVendorInvoiceView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  interfaceStatusOptions: SelectItems[] = [];
  processTransTypeOptions: SelectItems[] = [];

  constructor(
    private service: InterfaceVendorInvoiceService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: InterfaceVendorInvoiceCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.batchApiFunctionMappingKey = BATCH_FN_MAPPING_KEY.INTERFACE_VENDOR_INVOICE;
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialCreatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getInterfaceStatusEnumDropDown(this.interfaceStatusOptions);
    this.baseDropdown.getProcessTransTypeEnumDropDown(this.processTransTypeOptions);
  }
  getById(): Observable<InterfaceVendorInvoiceView> {
    return this.service.getInterfaceVendorInvoiceById(this.id);
  }
  getInitialData(): Observable<InterfaceVendorInvoiceView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        const processTransType = this.translate.instant('ENUM.VENDOR_PAYMENT');
        const header: TranslateModel = { code: 'CONFIRM.CONFIRM' };
        const message: TranslateModel = { code: 'CONFIRM.90013', parameters: [processTransType] };
        this.notificationService.showManualConfirmDialog(header, message);
        this.notificationService.isAccept.subscribe((isConfirm) => {
          if (isConfirm) {
            this.onSubmit(true);
          }
          this.notificationService.isAccept.observers = [];
        });
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    super.submitBatchFunctionParameters(this.model);
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onClose(): void {
    super.onBack();
  }
}
