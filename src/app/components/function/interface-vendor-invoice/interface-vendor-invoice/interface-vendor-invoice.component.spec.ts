import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterfaceVendorInvoiceComponent } from './interface-vendor-invoice.component';

describe('InterfaceVendorInvoiceViewComponent', () => {
  let component: InterfaceVendorInvoiceComponent;
  let fixture: ComponentFixture<InterfaceVendorInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InterfaceVendorInvoiceComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterfaceVendorInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
