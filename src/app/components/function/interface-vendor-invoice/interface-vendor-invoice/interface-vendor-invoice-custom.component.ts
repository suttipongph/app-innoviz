import { Observable, of } from 'rxjs';
import { EmptyGuid, InterfaceStatus, ProcessTransType } from 'shared/constants';
import { BaseServiceModel, FieldAccessing } from 'shared/models/systemModel';
import { InterfaceVendorInvoiceView } from 'shared/models/viewModel';

const firstGroup = ['PROCESS_TRANS_TYPE', 'INTERFACE_STATUS'];

export class InterfaceVendorInvoiceCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: InterfaceVendorInvoiceView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<InterfaceVendorInvoiceView> {
    let model = new InterfaceVendorInvoiceView();
    model.interfaceStatus = [InterfaceStatus.None, InterfaceStatus.Failed];
    model.processTransType = [ProcessTransType.VendorPayment];
    return of(model);
  }
}
