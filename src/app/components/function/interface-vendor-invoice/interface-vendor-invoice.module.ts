import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InterfaceVendorInvoiceService } from './interface-vendor-invoice.service';
import { InterfaceVendorInvoiceComponent } from './interface-vendor-invoice/interface-vendor-invoice.component';
import { InterfaceVendorInvoiceCustomComponent } from './interface-vendor-invoice/interface-vendor-invoice-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [InterfaceVendorInvoiceComponent],
  providers: [InterfaceVendorInvoiceService, InterfaceVendorInvoiceCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [InterfaceVendorInvoiceComponent]
})
export class InterfaceVendorInvoiceStagingComponentModule {}
