import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { InterfaceVendorInvoiceView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class InterfaceVendorInvoiceService {
  serviceKey = 'interfaceVendorInvoiceGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getInterfaceVendorInvoiceToList(search: SearchParameter): Observable<SearchResult<InterfaceVendorInvoiceView>> {
    const url = `${this.servicePath}/GetInterfaceVendorInvoiceList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteInterfaceVendorInvoice(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteInterfaceVendorInvoice`;
    return this.dataGateway.delete(url, row);
  }
  getInterfaceVendorInvoiceById(id: string): Observable<InterfaceVendorInvoiceView> {
    const url = `${this.servicePath}/GetInterfaceVendorInvoiceById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createInterfaceVendorInvoice(vmModel: InterfaceVendorInvoiceView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateInterfaceVendorInvoice`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateInterfaceVendorInvoice(vmModel: InterfaceVendorInvoiceView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateInterfaceVendorInvoice`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
