import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterfaceAccountingStagingComponent } from './interface-accounting-staging.component';

describe('InterfaceAccountingStagingViewComponent', () => {
  let component: InterfaceAccountingStagingComponent;
  let fixture: ComponentFixture<InterfaceAccountingStagingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InterfaceAccountingStagingComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterfaceAccountingStagingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
