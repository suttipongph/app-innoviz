import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseBatchFunctionComponent } from 'core/components/base-batch-function/base-batch-function.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { BATCH_FN_MAPPING_KEY } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { InterfaceAccountingStagingView } from 'shared/models/viewModel';
import { InterfaceAccountingStagingService } from '../interface-accounting-staging.service';
import { InterfaceAccountingStagingCustomComponent } from './interface-accounting-staging-custom.component';
@Component({
  selector: 'interface-accounting-staging',
  templateUrl: './interface-accounting-staging.component.html',
  styleUrls: ['./interface-accounting-staging.component.scss']
})
export class InterfaceAccountingStagingComponent extends BaseBatchFunctionComponent<InterfaceAccountingStagingView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  interfaceStatusOptions: SelectItems[] = [];
  processTransTypeOptions: SelectItems[] = [];

  constructor(
    private service: InterfaceAccountingStagingService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: InterfaceAccountingStagingCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.batchApiFunctionMappingKey = BATCH_FN_MAPPING_KEY.INTERFACE_ACCOUNTING_STAGING;
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    this.setInitialCreatingData();
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getInterfaceStatusEnumDropDown(this.interfaceStatusOptions);
    this.baseDropdown.getProcessTransTypeEnumDropDown(this.processTransTypeOptions);
  }
  getById(): Observable<InterfaceAccountingStagingView> {
    return this.service.getInterfaceAccountingStagingById(this.id);
  }
  getInitialData(): Observable<InterfaceAccountingStagingView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        const processTransTypes = this.custom.getTranslateProcessTransTypeLabels(this.processTransTypeOptions, this.model.processTransType);
        const header: TranslateModel = { code: 'CONFIRM.CONFIRM' };
        const message: TranslateModel = { code: 'CONFIRM.90013', parameters: [processTransTypes] };
        this.notificationService.showManualConfirmDialog(header, message);
        this.notificationService.isAccept.subscribe((isConfirm) => {
          if (isConfirm) {
            this.onSubmit(true);
          }
          this.notificationService.isAccept.observers = [];
        });
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    super.submitBatchFunctionParameters(this.model);
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onClose(): void {
    super.onBack();
  }
}
