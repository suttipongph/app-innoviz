import { Observable, of } from 'rxjs';
import { EmptyGuid, InterfaceStatus, ProcessTransType } from 'shared/constants';
import { isNullOrUndefined, isUndefinedOrZeroLength } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { InterfaceAccountingStagingView } from 'shared/models/viewModel';

const firstGroup = ['PROCESS_TRANS_TYPE', 'INTERFACE_STATUS'];

export class InterfaceAccountingStagingCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: InterfaceAccountingStagingView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<InterfaceAccountingStagingView> {
    let model = new InterfaceAccountingStagingView();
    model.interfaceStatus = [InterfaceStatus.None, InterfaceStatus.Failed];
    model.processTransType = [ProcessTransType.Invoice, ProcessTransType.IncomeRealization, ProcessTransType.Payment, ProcessTransType.PaymentDetail];
    return of(model);
  }
  getTranslateProcessTransTypeLabels(processTransTypeOptions: SelectItems[], processTransTypes: number[]): string {
    if (!isUndefinedOrZeroLength(processTransTypeOptions) && !isUndefinedOrZeroLength(processTransTypes)) {
      let result = '';
      for (let i = 0; i < processTransTypes.length; i++) {
        const value = processTransTypes[i];
        const findLabel = processTransTypeOptions.find((f) => f.value === value);
        result += isNullOrUndefined(findLabel) ? '' : this.baseService.translate.instant(findLabel.label);
        if (i < processTransTypes.length - 1) {
          result += ', ';
        }
      }
      return result;
    } else {
      return '';
    }
  }
}
