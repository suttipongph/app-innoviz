import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { InterfaceAccountingStagingView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class InterfaceAccountingStagingService {
  serviceKey = 'interfaceAccountingStagingGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getInterfaceAccountingStagingToList(search: SearchParameter): Observable<SearchResult<InterfaceAccountingStagingView>> {
    const url = `${this.servicePath}/GetInterfaceAccountingStagingList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteInterfaceAccountingStaging(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteInterfaceAccountingStaging`;
    return this.dataGateway.delete(url, row);
  }
  getInterfaceAccountingStagingById(id: string): Observable<InterfaceAccountingStagingView> {
    const url = `${this.servicePath}/GetInterfaceAccountingStagingById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createInterfaceAccountingStaging(vmModel: InterfaceAccountingStagingView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateInterfaceAccountingStaging`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateInterfaceAccountingStaging(vmModel: InterfaceAccountingStagingView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateInterfaceAccountingStaging`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
