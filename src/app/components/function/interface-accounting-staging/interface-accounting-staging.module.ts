import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InterfaceAccountingStagingService } from './interface-accounting-staging.service';
import { InterfaceAccountingStagingComponent } from './interface-accounting-staging/interface-accounting-staging.component';
import { InterfaceAccountingStagingCustomComponent } from './interface-accounting-staging/interface-accounting-staging-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [InterfaceAccountingStagingComponent],
  providers: [InterfaceAccountingStagingService, InterfaceAccountingStagingCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [InterfaceAccountingStagingComponent]
})
export class InterfaceAccountingStagingComponentModule {}
