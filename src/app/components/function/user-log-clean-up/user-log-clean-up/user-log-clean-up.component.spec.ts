import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserLogCleanUpComponent } from './user-log-clean-up.component';

describe('UserLogCleanUpViewComponent', () => {
  let component: UserLogCleanUpComponent;
  let fixture: ComponentFixture<UserLogCleanUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserLogCleanUpComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserLogCleanUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
