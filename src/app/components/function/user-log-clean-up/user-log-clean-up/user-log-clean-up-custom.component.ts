import { Observable, of } from 'rxjs';
import { EmptyGuid, ProcessTransType, PROCESS_TRANS_TYPE, StagingBatchStatus } from 'shared/constants';
import { Confirmation } from 'shared/models/primeModel';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { SysUserLogCleanUpParm } from 'shared/models/viewModel';

const firstGroup = [];

export class UserLogCleanUpCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: SysUserLogCleanUpParm): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<SysUserLogCleanUpParm> {
    let model = new SysUserLogCleanUpParm();
    model.historyLimitDays = 100;
    return of(model);
  }
}
