import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserLogCleanUpService } from './user-log-clean-up.service';
import { UserLogCleanUpComponent } from './user-log-clean-up/user-log-clean-up.component';
import { UserLogCleanUpCustomComponent } from './user-log-clean-up/user-log-clean-up-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [UserLogCleanUpComponent],
  providers: [UserLogCleanUpService, UserLogCleanUpCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [UserLogCleanUpComponent]
})
export class UserLogCleanUpComponentModule {}
