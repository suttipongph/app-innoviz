import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { SysUserLogCleanUpParm } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class UserLogCleanUpService {
  serviceKey = 'generateAccountingStagingGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getUserLogCleanUpToList(search: SearchParameter): Observable<SearchResult<SysUserLogCleanUpParm>> {
    const url = `${this.servicePath}/GetUserLogCleanUpList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteUserLogCleanUp(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteUserLogCleanUp`;
    return this.dataGateway.delete(url, row);
  }
  getUserLogCleanUpById(id: string): Observable<SysUserLogCleanUpParm> {
    const url = `${this.servicePath}/GetUserLogCleanUpById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createUserLogCleanUp(vmModel: SysUserLogCleanUpParm): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateUserLogCleanUp`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateUserLogCleanUp(vmModel: SysUserLogCleanUpParm): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateUserLogCleanUp`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  cleanupUserLogs(vwModel: SysUserLogCleanUpParm): Observable<boolean> {
    const url = `${this.servicePath}`;
    return this.dataGateway.post(url, vwModel);
  }
}
