import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GenerateVendorInvoiceStagingService } from './generate-vendor-invoice-staging.service';
import { GenerateVendorInvoiceStagingComponent } from './generate-vendor-invoice-staging/generate-vendor-invoice-staging.component';
import { GenerateVendorInvoiceStagingCustomComponent } from './generate-vendor-invoice-staging/generate-vendor-invoice-staging-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [GenerateVendorInvoiceStagingComponent],
  providers: [GenerateVendorInvoiceStagingService, GenerateVendorInvoiceStagingCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [GenerateVendorInvoiceStagingComponent]
})
export class GenerateVendorInvoiceStagingComponentModule {}
