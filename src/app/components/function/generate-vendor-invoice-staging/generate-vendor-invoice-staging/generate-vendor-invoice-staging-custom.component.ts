import { Observable, of } from 'rxjs';
import { EmptyGuid, ProcessTransType, StagingBatchStatus } from 'shared/constants';
import { Confirmation } from 'shared/models/primeModel';
import { BaseServiceModel, FieldAccessing, SelectItems } from 'shared/models/systemModel';
import { GenerateVendorInvoiceStagingView } from 'shared/models/viewModel';

const firstGroup = ['LIST_PROCESS_TRANS_TYPE', 'STAGING_BATCH_STATUS'];

export class GenerateVendorInvoiceStagingCustomComponent {
  baseService: BaseServiceModel<any>;
  getFieldAccessing(model: GenerateVendorInvoiceStagingView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(): Observable<boolean> {
    return of(true);
  }
  getPageAccessRight(canAction: boolean): Observable<boolean> {
    return of(!canAction);
  }
  getInitialData(): Observable<GenerateVendorInvoiceStagingView> {
    let model = new GenerateVendorInvoiceStagingView();
    model.generateVendorInvoiceStagingGUID = EmptyGuid;
    model.listProcessTransType = [ProcessTransType.VendorPayment];
    model.stagingBatchStatus = [StagingBatchStatus.None, StagingBatchStatus.Failed];
    return of(model);
  }
  showConfirmDialog(model: GenerateVendorInvoiceStagingView, option: SelectItems[]): Observable<any> {
    let listProcessTransTypeString = '';
    model.listProcessTransType.forEach((f, idx) => {
      listProcessTransTypeString += this.baseService.translate.instant(option.find((fi) => fi.value === f).label);
      if (idx < model.listProcessTransType.length - 1) {
        listProcessTransTypeString += ', ';
      }
    });
    const confirmation: Confirmation = {
      header: this.baseService.translate.instant('CONFIRM.CONFIRM'),
      message: this.baseService.translate.instant('CONFIRM.90010', [listProcessTransTypeString])
    };
    return new Observable((observer) => {
      this.baseService.notificationService.showConfirmDialog(confirmation);
      this.baseService.notificationService.isAccept.subscribe((isConfirm) => {
        this.baseService.notificationService.isAccept.observers = [];
        observer.next(isConfirm);
      });
    });
  }
}
