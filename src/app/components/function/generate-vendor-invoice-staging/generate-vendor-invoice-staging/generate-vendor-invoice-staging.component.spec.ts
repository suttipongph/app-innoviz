import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateVendorInvoiceStagingComponent } from './generate-vendor-invoice-staging.component';

describe('GenerateVendorInvoiceStagingViewComponent', () => {
  let component: GenerateVendorInvoiceStagingComponent;
  let fixture: ComponentFixture<GenerateVendorInvoiceStagingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GenerateVendorInvoiceStagingComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateVendorInvoiceStagingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
