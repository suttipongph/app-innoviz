import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseBatchFunctionComponent } from 'core/components/base-batch-function/base-batch-function.component';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { BATCH_FN_MAPPING_KEY } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUpdateMode } from 'shared/functions/value.function';
import { FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { GenerateVendorInvoiceStagingView } from 'shared/models/viewModel';
import { GenerateVendorInvoiceStagingService } from '../generate-vendor-invoice-staging.service';
import { GenerateVendorInvoiceStagingCustomComponent } from './generate-vendor-invoice-staging-custom.component';
@Component({
  selector: 'generate-vendor-invoice-staging',
  templateUrl: './generate-vendor-invoice-staging.component.html',
  styleUrls: ['./generate-vendor-invoice-staging.component.scss']
})
export class GenerateVendorInvoiceStagingComponent extends BaseBatchFunctionComponent<GenerateVendorInvoiceStagingView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  processTransTypeOptions: SelectItems[] = [];
  stagingBatchStatusOptions: SelectItems[] = [];

  constructor(
    private service: GenerateVendorInvoiceStagingService,
    private currentActivatedRoute: ActivatedRoute,
    public custom: GenerateVendorInvoiceStagingCustomComponent
  ) {
    super();
    this.custom.baseService = this.baseService;
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.batchApiFunctionMappingKey = BATCH_FN_MAPPING_KEY.GENERATE_VENDOR_INVOICE_STAGING;
  }
  ngOnInit(): void {}
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  checkPageMode(): void {
    // if (isUpdateMode(this.id)) {
    //   this.isUpdateMode = true;
    //   this.setInitialUpdatingData();
    // } else {
    //   this.isUpdateMode = false;
    this.setInitialCreatingData();
    // }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getProcessTransTypeEnumDropDown(this.processTransTypeOptions);
    this.baseDropdown.getStagingBatchStatusEnumDropDown(this.stagingBatchStatusOptions);
  }
  getById(): Observable<GenerateVendorInvoiceStagingView> {
    return this.service.getGenerateVendorInvoiceStagingById(this.id);
  }
  getInitialData(): Observable<GenerateVendorInvoiceStagingView> {
    return this.custom.getInitialData();
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model = result;
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }

  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanAction()).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    // super.onExecuteFunction(of(true));
    this.custom.showConfirmDialog(this.model, this.processTransTypeOptions).subscribe((res) => {
      if (res) {
        super.submitBatchFunctionParameters(this.model);
      }
    });
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation();
  }
  onClose(): void {
    super.onBack();
  }
}
