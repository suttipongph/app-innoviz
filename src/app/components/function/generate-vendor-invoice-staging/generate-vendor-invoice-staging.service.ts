import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { GenerateVendorInvoiceStagingView } from 'shared/models/viewModel';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
@Injectable()
export class GenerateVendorInvoiceStagingService {
  serviceKey = 'generateVendorInvoiceStagingGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getGenerateVendorInvoiceStagingToList(search: SearchParameter): Observable<SearchResult<GenerateVendorInvoiceStagingView>> {
    const url = `${this.servicePath}/GetGenerateVendorInvoiceStagingList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteGenerateVendorInvoiceStaging(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteGenerateVendorInvoiceStaging`;
    return this.dataGateway.delete(url, row);
  }
  getGenerateVendorInvoiceStagingById(id: string): Observable<GenerateVendorInvoiceStagingView> {
    const url = `${this.servicePath}/GetGenerateVendorInvoiceStagingById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createGenerateVendorInvoiceStaging(vmModel: GenerateVendorInvoiceStagingView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateGenerateVendorInvoiceStaging`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateGenerateVendorInvoiceStaging(vmModel: GenerateVendorInvoiceStagingView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateGenerateVendorInvoiceStaging`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
}
