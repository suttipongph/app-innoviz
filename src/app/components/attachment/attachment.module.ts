import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AttachmentService } from './attachment.service';
import { AttachmentListComponent } from './attachment-list/attachment-list.component';
import { AttachmentItemComponent } from './attachment-item/attachment-item.component';
import { AttachmentItemCustomComponent } from './attachment-item/attachment-item-custom.component';
import { AttachmentListCustomComponent } from './attachment-list/attachment-list-custom.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [AttachmentListComponent, AttachmentItemComponent],
  providers: [AttachmentService, AttachmentItemCustomComponent, AttachmentListCustomComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [AttachmentListComponent, AttachmentItemComponent]
})
export class AttachmentComponentModule {}
