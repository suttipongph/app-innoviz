import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PageInformationModel, ResponseModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { AttachmentItemView, AttachmentListView, RefIdParm } from 'shared/models/viewModel';

@Injectable()
export class AttachmentService {
  serviceKey = 'attachmentGUID';
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.dataGateway.serviceKey = this.serviceKey;
    this.servicePath = param.servicePath;
  }
  getAttachmentToList(search: SearchParameter): Observable<SearchResult<AttachmentListView>> {
    const url = `${this.servicePath}/GetAttachmentList/${search.branchFilterMode}`;
    return this.dataGateway.getList(url, search);
  }
  deleteAttachment(row: RowIdentity): Observable<boolean> {
    const url = `${this.servicePath}/DeleteAttachment`;
    return this.dataGateway.delete(url, row);
  }
  getAttachmentById(id: string): Observable<AttachmentItemView> {
    const url = `${this.servicePath}/GetAttachmentById/id=${id}`;
    return this.dataGateway.get(url);
  }
  createAttachment(vmModel: AttachmentItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/CreateAttachment`;
    return this.dataGateway.create(url, vmModel, this.serviceKey);
  }
  updateAttachment(vmModel: AttachmentItemView): Observable<ResponseModel> {
    const url = `${this.servicePath}/UpdateAttachment`;
    return this.dataGateway.update(url, vmModel, this.serviceKey);
  }
  getAttachmentRefId(parm: RefIdParm): Observable<string> {
    const url = `${this.servicePath}/GetAttachmentRefId`;
    return this.dataGateway.post(url, parm);
  }
}
