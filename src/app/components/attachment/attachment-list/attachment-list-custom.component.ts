import { Observable } from 'rxjs';
import { getRefTypeFromRoute, isUndefinedOrZeroLength } from 'shared/functions/value.function';
import { BaseServiceModel } from 'shared/models/systemModel';
import { AccessModeView, RefIdParm } from 'shared/models/viewModel';

const CUSTOMER_TABLE = 'customertable';
export class AttachmentListCustomComponent {
  baseService: BaseServiceModel<any>;
  accessModeView: AccessModeView = new AccessModeView();
  refType: string = '';
  refGUID: string = '';
  getPageAccessRight(): Observable<any> {
    return new Observable((observer) => {});
  }
  setRefTypeRefGUID(passingObj: RefIdParm): void {
    this.refType = passingObj.refType.toString();
    this.refGUID = passingObj.refGUID;

    if (isUndefinedOrZeroLength(this.refType) || isUndefinedOrZeroLength(this.refGUID)) {
      const parentName = this.baseService.uiService.getRelatedInfoParentTableName();
      if (isUndefinedOrZeroLength(this.refType)) {
        this.refType = getRefTypeFromRoute(parentName).toString();
      }
      if (isUndefinedOrZeroLength(this.refGUID)) {
        this.refGUID = this.baseService.uiService.getKey(parentName);
      }
    }
  }
}
