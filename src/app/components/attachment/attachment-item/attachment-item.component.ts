import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseItemComponent } from 'core/components/base-item/base-item.component';
import { BaseItemInterface } from 'core/interfaces/base-item/base-item.interface';
import { Observable, of } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { IVZFileUploadComponent } from 'shared/components/ivz-file-upload/ivz-file-upload.component';
import { EmptyGuid } from 'shared/constants';
import { modelRegister } from 'shared/functions/model.function';
import { isNullOrUndefined, isUndefinedOrZeroLength, isUpdateMode } from 'shared/functions/value.function';
import { BaseServiceModel, FileInformation, FileUpload, FormValidationModel, PageInformationModel, SelectItems } from 'shared/models/systemModel';
import { AccessModeView, AttachmentItemView } from 'shared/models/viewModel';
import { AttachmentService } from '../attachment.service';
import { AttachmentItemCustomComponent } from './attachment-item-custom.component';
@Component({
  selector: 'attachment-item',
  templateUrl: './attachment-item.component.html',
  styleUrls: ['./attachment-item.component.scss']
})
export class AttachmentItemComponent extends BaseItemComponent<AttachmentItemView> implements BaseItemInterface {
  @Input() set pageInfo(param: PageInformationModel) {
    super.setPath(param);
    this.service.setPath(param);
  }
  refTypeOptions: SelectItems[] = [];
  fileUpload: FileUpload = new FileUpload();
  @ViewChild('fileUploadCmpnt') fileComponent: IVZFileUploadComponent;

  constructor(private service: AttachmentService, private currentActivatedRoute: ActivatedRoute, public custom: AttachmentItemCustomComponent) {
    super();
    this.id = this.currentActivatedRoute.snapshot.params['id'];
    this.baseService.service = service;
    this.custom.baseService = this.baseService;
  }
  ngOnInit(): void {
    const passingObj = this.getPassingObject(this.currentActivatedRoute);
    this.custom.setRefTypeRefGUID(passingObj);
  }
  ngAfterViewInit(): void {
    this.checkAccessMode();
    this.checkPageMode();
  }
  ngOnDestroy(): void {}
  checkPageMode(): void {
    this.fileDirty = false;
    if (isUpdateMode(this.id)) {
      this.isUpdateMode = true;
      this.setInitialUpdatingData();
    } else {
      this.isUpdateMode = false;
      this.setInitialCreatingData();
    }
    this.onEnumLoader();
    this.getGroups();
  }
  checkAccessMode(): void {
    super.checkAccessMode(this.accessService.getAccessRight());
  }
  onEnumLoader(): void {
    this.baseDropdown.getRefTypeEnumDropDown(this.refTypeOptions);
  }
  getById(): Observable<AttachmentItemView> {
    return this.service.getAttachmentById(this.id);
  }
  getInitialData(): Observable<string> {
    return this.service.getAttachmentRefId({ refType: Number(this.custom.refType), refGUID: this.custom.refGUID });
  }
  setInitialUpdatingData(): void {
    this.getById().subscribe(
      (result) => {
        this.onAsyncRunner(result);
        this.setRelatedInfoOptions();
        this.setFunctionOptions();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setInitialCreatingData(): void {
    this.getInitialData().subscribe((result) => {
      this.model.refId = result;
      this.model.attachmentGUID = EmptyGuid;
      this.model.refGUID = this.custom.refGUID;
      this.model.refType = Number(this.custom.refType);
      this.onAsyncRunner();
      super.setDefaultValueSystemFields();
    });
  }
  onAsyncRunner(model?: any): void {
    forkJoin(of(true)).subscribe(
      ([]) => {
        if (!isNullOrUndefined(model)) {
          this.model = model;
          this.getFileUpload();
          modelRegister(this.model);
        }
        this.setFieldAccessing();
      },
      (error) => {
        this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  setFieldAccessing(): void {
    this.custom.getPageAccessRight(super.getCanCreate(), super.getCanUpdate(), this.model).subscribe((res) => {
      this.isView = res;
    });
    this.custom.getFieldAccessing(this.model).subscribe((res) => {
      super.setBaseFieldAccessing(res);
    });
  }
  setRelatedInfoOptions(): void {}
  setFunctionOptions(): void {}

  onSave(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(false);
      }
    });
  }
  onSaveAndClose(validation: FormValidationModel): void {
    this.getDataValidation().subscribe((res) => {
      if (res && validation.isValid) {
        this.onSubmit(true);
      }
    });
  }
  onSubmit(isColsing: boolean): void {
    this.setFileUpload();
    if (this.isUpdateMode) {
      super.onUpdate(this.service.updateAttachment(this.model), isColsing);
    } else {
      super.onCreate(this.service.createAttachment(this.model), isColsing);
    }
  }
  getDataValidation(): Observable<boolean> {
    return this.custom.getDataValidation(this.fileUpload, this.isUpdateMode);
  }
  onFileChange(e) {
    this.fileUpload = e;
    this.model.fileDescription = this.fileUpload.fileInfos[0].fileName.split('.')[0];
    // clear file description if file is removed
    if (isUndefinedOrZeroLength(this.fileUpload) || isUndefinedOrZeroLength(this.fileUpload.fileInfos)) {
      this.model.fileDescription = null;
    }
    this.fileDirty = true;
  }
  getFileUpload(): void {
    if (!isUndefinedOrZeroLength(this.model.base64Data)) {
      this.fileUpload.fileInfos = [];
      const attachment = this.custom.getFileInformationFromModel(this.model);
      this.fileUpload.fileInfos.push(attachment);

      this.fileComponent.setFileFromExternal(attachment);
    }
  }

  setFileUpload() {
    if (isUndefinedOrZeroLength(this.fileUpload.fileInfos) || isUndefinedOrZeroLength(this.fileUpload.fileInfos[0])) {
      return;
    }
    if (!this.isUpdateMode) {
      let file = this.fileUpload.fileInfos[0].base64.split(',');
      this.model.base64Data = file[file.length - 1];
      this.model.contentType = file[0];
      this.model.fileName = this.fileUpload.fileInfos[0].fileName;
    }
  }
}
