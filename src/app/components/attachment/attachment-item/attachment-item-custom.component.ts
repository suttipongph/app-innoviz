import { Observable, of } from 'rxjs';
import { getRefTypeFromRoute, getRefTypeLabelFromRefType, isUndefinedOrZeroLength } from 'shared/functions/value.function';
import { BaseServiceModel, FieldAccessing, FileInformation, FileUpload, TranslateModel } from 'shared/models/systemModel';
import { AccessModeView, AttachmentItemView, RefIdParm } from 'shared/models/viewModel';

const firstGroup = ['REFERENCE_TYPE', 'REFERENCE_ID'];
export class AttachmentItemCustomComponent {
  baseService: BaseServiceModel<any>;
  accessModeView: AccessModeView = new AccessModeView();
  refType: string = '';
  refGUID: string = '';
  getFieldAccessing(model: AttachmentItemView): Observable<FieldAccessing[]> {
    const fieldAccessing: FieldAccessing[] = [];
    fieldAccessing.push({ filedIds: firstGroup, readonly: true });
    return of(fieldAccessing);
  }
  getDataValidation(fileUpload: FileUpload, isUpdateMode: boolean): Observable<boolean> {
    if (!isUpdateMode) {
      if (isUndefinedOrZeroLength(fileUpload.fileInfos) || isUndefinedOrZeroLength(fileUpload.fileInfos[0])) {
        const topic: TranslateModel = { code: 'ERROR.00159' };
        const contents: TranslateModel[] = [{ code: 'ERROR.00160' }];
        this.baseService.notificationService.showErrorMessageFromManual(topic, contents);
        return of(false);
      }
    }
    return of(true);
  }
  getPageAccessRight(canCreate: boolean, canUpdate: boolean, model: AttachmentItemView): Observable<boolean> {
    return of(!canCreate);
  }

  getFileInformationFromModel(model: AttachmentItemView): FileInformation {
    const attachment = new FileInformation();
    const contentType = this.baseService.fileService.getDataMimeHeaderFromFileName(model.fileName);
    attachment.base64 = contentType + model.base64Data;
    attachment.isRemovable = false;
    attachment.isPreviewable = this.baseService.fileService.getPreviewable(contentType);
    attachment.contentType = contentType;
    attachment.fileName = model.fileName;
    attachment.fileDisplayName = model.fileName;
    return attachment;
  }
  setRefTypeRefGUID(passingObj: RefIdParm): void {
    this.refType = passingObj.refType.toString();
    this.refGUID = passingObj.refGUID;

    if (isUndefinedOrZeroLength(this.refType) || isUndefinedOrZeroLength(this.refGUID)) {
      const parentName = this.baseService.uiService.getRelatedInfoParentTableName();
      if (isUndefinedOrZeroLength(this.refType)) {
        this.refType = getRefTypeFromRoute(parentName).toString();
      }
      if (isUndefinedOrZeroLength(this.refGUID)) {
        this.refGUID = this.baseService.uiService.getKey(parentName);
      }
    }
  }
}
