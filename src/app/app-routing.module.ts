import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions, PreloadAllModules } from '@angular/router';
import { SiteGuardService } from 'core/services/site-guard.service';
import { FallbackComponent } from './core/auth/fallback.component';
import { LoginComponent } from './core/auth/login.component';
import { LogOutComponent } from './core/auth/logout.component';
import { SmartAppNotFoundComponent } from './core/auth/not-found.component';
import { SiteSelectComponent } from './core/auth/site-select/site-select.component';
import { SmartAppUnAuthorizedComponent } from './core/auth/unauthorized.component';
import { LoginGuardService } from './core/services/login-guard.service';
import { CustomPreloadingStrategy } from './customPreloadingStrategy';

const routes: Routes = [
  { path: '', component: FallbackComponent, pathMatch: 'full' },
  { path: 'Login', component: LoginComponent },
  { path: 'Logout', component: LogOutComponent },
  { path: 'siteselect', component: SiteSelectComponent },
  { path: 'initdata', loadChildren: () => import('./pages/create-company/create-company.module').then((m) => m.CreateCompanyModule) },
  { path: ':site/401', canActivate: [LoginGuardService], component: SmartAppUnAuthorizedComponent, pathMatch: 'full' },
  { path: ':site/404', canActivate: [LoginGuardService], component: SmartAppNotFoundComponent, pathMatch: 'full' },
  {
    path: ':site/home',
    canActivate: [LoginGuardService],
    loadChildren: () => import('./pages/home/home.module').then((m) => m.HomeModule)
  },
  {
    path: ':site/usersettings',
    canActivate: [LoginGuardService],
    loadChildren: () => import('./pages/user-settings/user-settings.module').then((m) => m.UserSettingsModule)
  },
  {
    path: ':site/setup',
    canActivate: [LoginGuardService],
    loadChildren: () => import('./pages/setup/setup.module').then((m) => m.SetupModule)
  },
  {
    path: ':site/organization',
    canActivate: [LoginGuardService],
    loadChildren: () => import('./pages/organization/organization.module').then((m) => m.OrganizationModule)
  },
  {
    path: ':site/masterinformation',
    canActivate: [LoginGuardService],
    loadChildren: () => import('./pages/master-information/master-information.module').then((m) => m.MasterInformationModule)
  },
  // {
  //   path: ':site/demo',
  //   loadChildren: () => import('./pages/demo/demo.module').then((m) => m.DemoModule)
  // },
  // {
  //   path: ':site/demo2',
  //   loadChildren: () => import('./pages/demo-credit-app/credit-app-table/credit-app-table.module').then((m) => m.DemoCreditAppTableModule)
  // },
  {
    path: ':site/factoring',
    canActivate: [LoginGuardService],
    loadChildren: () => import('./pages/factoring/factoring.module').then((m) => m.FactoringModule)
  },
  {
    path: ':site/bond',
    canActivate: [LoginGuardService],
    loadChildren: () => import('./pages/bond/bond.module').then((m) => m.BondModule)
  },
  {
    path: ':site/hirepurchase',
    canActivate: [LoginGuardService],
    loadChildren: () => import('./pages/hire-purchase/hire-purchase.module').then((m) => m.HirePurchaseModule)
  },
  {
    path: ':site/lcdlc',
    canActivate: [LoginGuardService],
    loadChildren: () => import('./pages/lc-dlc/lc-dlc.module').then((m) => m.LCDLCModule)
  },
  {
    path: ':site/leasing',
    canActivate: [LoginGuardService],
    loadChildren: () => import('./pages/leasing/leasing.module').then((m) => m.LeasingModule)
  },
  {
    path: ':site/projectfinance',
    canActivate: [LoginGuardService],
    loadChildren: () => import('./pages/project-finance/project-finance.module').then((m) => m.ProjectFinanceModule)
  },
  {
    path: ':site/assignmentagreement',
    canActivate: [LoginGuardService],
    loadChildren: () => import('./pages/assignment-agreement/assignment-agreement.module').then((m) => m.AssignmentAgreementModule)
  },
  {
    path: ':site/invoiceandcollection',
    canActivate: [LoginGuardService],
    loadChildren: () => import('./pages/invoice-and-collection/invoice-and-collection.module').then((m) => m.InvoiceAndCollectionModule)
  },
  {
    path: ':site/accounting',
    canActivate: [LoginGuardService],
    loadChildren: () => import('./pages/accouting/accouting.module').then((m) => m.AccoutingModule)
  },
  {
    path: ':site/admin',
    canActivate: [LoginGuardService],
    loadChildren: () => import('./pages/administration/admin.module').then((m) => m.AdminModule)
  },
  {
    path: ':site/framework',
    canActivate: [LoginGuardService],
    loadChildren: () => import('./pages/ob-framework/ob-framework.module').then((m) => m.OBFrameworkModule)
  },
  {
    path: ':site/function',
    canActivate: [LoginGuardService],
    loadChildren: () => import('./pages/function/function.module').then((m) => m.FunctionModule)
  },
  {
    path: ':site/report',
    canActivate: [LoginGuardService],
    loadChildren: () => import('./pages/report/report.module').then((m) => m.ReportModule)
  },
  { path: '**', canActivate: [SiteGuardService], component: SmartAppNotFoundComponent }
];

const config: ExtraOptions = {
  useHash: false
};

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: CustomPreloadingStrategy })],
  exports: [RouterModule],
  providers: [CustomPreloadingStrategy]
})
export class AppRoutingModule {}
