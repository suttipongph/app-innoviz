import { AfterViewInit, OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { FormValidationModel, PathParamModel } from 'shared/models/systemModel';
import { AccessLevelModel } from 'shared/models/viewModel';
import { BaseInterface } from '../base/base.interface';

export declare abstract class BaseItemInterface implements BaseInterface {
  ngOnInit(): void;
  ngOnDestroy(): void;
  ngAfterViewInit(): void;

  checkPageMode(): void;
  checkAccessMode(): void;
  onEnumLoader(): void;
  getById(): Observable<any>;
  setFieldAccessing(): void;
  setInitialUpdatingData(): void;
  onAsyncRunner(model?: any): void;
  setInitialCreatingData(): void;
  setRelatedInfoOptions(model?: any): void;
  setFunctionOptions(model?: any): void;
  //#endregion
  //#region page event
  onSave(validation: FormValidationModel): void;
  onSaveAndClose(validation: FormValidationModel): void;
  onSubmit(isColsing: boolean, model?: any): void;
  getDataValidation(): Observable<boolean>;
  // toReload(param?: PathParamModel): void;
  toRelatedInfo(param?: PathParamModel): void;
  toFunction(param?: PathParamModel): void;
  //#endregion
}
