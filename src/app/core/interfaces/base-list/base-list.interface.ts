import { AfterViewInit, OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { GridFilterModel, PathParamModel, RowIdentity, SearchParameter, SearchResult } from 'shared/models/systemModel';
import { AccessLevelModel } from 'shared/models/viewModel';
import { BaseInterface } from '../base/base.interface';

export declare abstract class BaseListInterface implements BaseInterface {
  ngOnInit(): void;
  ngAfterViewInit(): void;
  ngOnDestroy(): void;
  checkPageMode(): void;
  checkAccessMode(): void;
  onEnumLoader(): void;
  onFilter(param: GridFilterModel): void;
  setDataGridOption(): void;
  getList(search: SearchParameter): Observable<SearchResult<any>>;
  onCreate(row: RowIdentity): void;
  onView(row: RowIdentity): void;
  onDelete(row: RowIdentity): void;
}
