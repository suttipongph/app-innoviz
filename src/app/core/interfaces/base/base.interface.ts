import { AfterViewInit, OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AccessLevelModel } from 'shared/models/viewModel';

export declare abstract class BaseInterface implements OnInit, AfterViewInit, OnDestroy {
  ngOnInit(): void;
  ngOnDestroy(): void;
  ngAfterViewInit(): void;
  checkPageMode(): void;
  checkAccessMode(): void;
}
