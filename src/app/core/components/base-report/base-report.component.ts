import { Component, OnInit, ValueProvider } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { ColumnType, WORKFLOW } from 'shared/constants';
import { cloneObject, getQueryStringValue, isSelectAll, isUndefinedOrZeroLength } from 'shared/functions/value.function';
import { NotificationResponse, PageInformationModel, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { CommentView, DataField, TaskItemView, WorkflowInstanceView } from 'shared/models/viewModel';
import { BaseItemComponent } from '../base-item/base-item.component';

@Component({
  selector: 'app-base-report',
  template: `<p>base report works!</p> `
})
export class BaseReportComponent<T> extends BaseItemComponent<T> {
  isViewBody: boolean = true;
  parameters: any = undefined;
  reportName: string;
  servicePath: string;
  constructor() {
    super();
  }

  ngOnInit(): void {}
  setFieldAccessing(): void {}
  onReportReady(e: boolean): void {
    this.isViewBody = !e;
    this.setFieldAccessing();
  }
  submitReportParameters(parm: any): void {
    let param = {};
    if (!isUndefinedOrZeroLength(parm)) {
      param = cloneObject(parm);
      param['reportName'] = this.reportName;
      param['companyGUID'] = this.baseService.userDataService.getCurrentCompanyGUID();
      // param['owner'] = this.baseService.userDataService.getUsernameFromToken();
      // param['ownerBusinessUnitGUID'] = this.baseService.accessService.getUserBusinessUnitGUID();
      // param['parentChildBusinessUnitGUID'] = this.baseService.accessService.getUserParentChildBU();

      param['servicePath'] = this.servicePath;
    }
    this.parameters = param;
  }
  setPath(param: PageInformationModel): void {
    super.setPath(param);
    this.servicePath = param.servicePath;
  }
}
