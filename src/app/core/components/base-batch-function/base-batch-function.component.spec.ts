import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseBatchFunctionComponent } from './base-batch-function.component';

describe('BaseBatchFunctionComponent', () => {
  let component: BaseBatchFunctionComponent;
  let fixture: ComponentFixture<BaseBatchFunctionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BaseBatchFunctionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseBatchFunctionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
