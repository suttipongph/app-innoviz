import { Component, OnInit } from '@angular/core';
import { isNullOrUndefined, isUndefinedOrZeroLength } from 'shared/functions/value.function';
import { PageInformationModel } from 'shared/models/systemModel';
import { BaseItemComponent } from '../base-item/base-item.component';

@Component({
  selector: 'app-base-batch-function',
  template: `<p>base-batch-function works!</p> `
})
export class BaseBatchFunctionComponent<T> extends BaseItemComponent<T> {
  parameters: any = undefined;
  batchApiFunctionMappingKey: string; // sheet name
  servicePath: string;
  constructor() {
    super();
  }

  ngOnInit(): void {}
  setPath(param: PageInformationModel): void {
    super.setPath(param);
    this.servicePath = param.servicePath;
  }
  submitBatchFunctionParameters(param: any): void {
    if (!isUndefinedOrZeroLength(param)) {
      this.setBatchParamDefaultData(param);
    } else {
      param = {};
    }
    param['hasCompanyGUID'] = !isNullOrUndefined(param['companyGUID']);
    param['batchApiFunctionMappingKey'] = this.batchApiFunctionMappingKey;
    param['servicePath'] = this.servicePath;
    param['companyGUID'] = this.baseService.userDataService.getCurrentCompanyGUID();
    this.parameters = param;
  }
  onCreateBatchSuccess(val: boolean): void {
    if (val === true) {
      this.onBack();
    }
  }
}
