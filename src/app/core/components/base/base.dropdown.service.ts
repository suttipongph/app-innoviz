import { Injectable } from '@angular/core';
import { DataGatewayService } from 'core/services/data-gateway.service';
import {
  AGREEMENT_DOC_TYPE,
  BILLING_BY,
  CONTACT_TYPE,
  CREDIT_LIMIT_CONDITION_TYPE,
  CREDIT_LIMIT_EXPIRATION,
  DOC_CON_VERIFY_TYPE,
  IDENTIFICATION_TYPE,
  METHOD_OF_BILLING,
  PRODUCT_TYPE,
  PURCHASE_FEE_CALCULATE_BASE,
  RECORD_TYPE,
  REF_TYPE,
  RETENTION_CALCULATE_BASE,
  RETENTION_DEDUCTION_METHOD,
  ASSET_FEE_TYPE,
  SERVICE_FEE_CATEGORY,
  DEFAULT_BIT,
  PAYMENT_TYPE,
  DIMENSION,
  BOOKMARK_DOCUMENT_REF_TYPE,
  DOCUMENT_TEMPLATE_TYPE,
  CREDIT_APP_REQUEST_TYPE,
  NUMBER_SEQ_SEGMENT_TYPE,
  MONTH_OF_YEAR,
  APPROVAL_DECISION,
  DAY_OF_MONTH,
  WORKING_DAY,
  HOLIDAY_TYPE,
  CALC_INTEREST_METHOD,
  CALC_INTEREST_DAY_METHOD,
  COMPANY_SIGNATURE_REF_TYPE,
  INVOICE_ISSUING,
  PAYM_STRUCTURE_REF_NUM,
  RECEIPT_GEN_BY,
  PENALTY_BASE,
  COLLECTION_FEE_METHOD,
  NPL_METHOD,
  PROVISION_METHOD,
  PROVISION_RATE_BY,
  PENALTY_CALCULATION_METHOD,
  OVERDUE_TYPE,
  LEDGER_FISCAL_PERIOD_UNIT,
  LEDGER_FISCAL_PERIOD_STATUS,
  SETTLE_INVOICE_CRITERIA,
  SUSPENSE_INVOICE_TYPE,
  SYS_LANGUAGE,
  CUST_TRANS_STATUS,
  PAID_TO_TYPE,
  STAGING_BATCH_STATUS,
  PROCESS_TRANS_TYPE,
  RECEIVED_FROM,
  RESULT,
  COLLECTION_FOLLOW_UP_RESULT,
  TAX_INVOICE_REF_TYPE,
  CONTACT_TO,
  PRIORITY,
  CHEQUE_SOURCE,
  INTEREST_CALCULATION_METHOD,
  WITHDRAWAL_LINE_TYPE,
  RECEIPT_TEMP_REF_TYPE,
  VERIFY_TYPE,
  ACCOUNT_TYPE,
  BUSINESS_SEGMENT_TYPE,
  INTERFACE_STATUS,
  SITE_LOGIN_TYPE,
  BATCH_INTERVAL_TYPE,
  BATCH_DAY_OF_WEEK,
  BATCH_JOB_STATUS,
  BATCH_INSTANCE_STATE,
  BATCH_RESULT_STATUS,
  SHOW_DOC_CON_VERIFY_TYPE
} from 'shared/constants';
import { transformLabel } from 'shared/functions/value.function';
import { DataServiceModel, PageInformationModel, SearchCondition, SelectItems } from 'shared/models/systemModel';

@Injectable({
  providedIn: 'root'
})
export class BaseDropdownService {
  servicePath = '';
  constructor(private dataGateway: DataGatewayService) {}
  setPath(param: PageInformationModel): void {
    this.servicePath = param.servicePath;
  }
  //#region DB

  getRaceDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetRaceDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getRegistrationTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetRegistrationTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getRelatedPersonTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetRelatedPersonTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getRelatedPersonTableByCreditAppRequestTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetRelatedPersonTableByCreditAppRequestTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getRelatedPersonTableByCreditAppRequestTableGuarantorDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetRelatedPersonTableByCreditAppRequestTableGuarantorDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getRelatedPersonTableByCreditAppRequestTableOwnerTransDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetRelatedPersonTableByCreditAppRequestTableOwnerTransDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getRelatedPersonTableByCreditAppRequestTableAuthorizedPersonTransDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetRelatedPersonTableByCreditAppRequestTableAuthorizedPersonTransDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getRelatedPersonTableByAmendCADropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetRelatedPersonTableByAmendCADropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getAddressCountryDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAddressCountryDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getAddressDistrictDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAddressDistrictDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getAddressPostalCodeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAddressPostalCodeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getAddressProvinceDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAddressProvinceDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getAddressSubDistrictDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAddressSubDistrictDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getAddressTransDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAddressTransDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getApplicationTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetApplicationTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getAssignmentAgreementSettleDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAssignmentAgreementSettleDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getAssignmentAgreementTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAssignmentAgreementTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }

  getAssignmentAgreementTableByCustomerDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/getAssignmentAgreementTableByCustomerDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getAssignmentMethodDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAssignmentMethodDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getAuthorizedPersonTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAuthorizedPersonTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBillingResponsibleByDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBillingResponsibleByDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBlacklistStatusDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBlacklistStatusDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBusinessSegmentDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBusinessSegmentDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBusinessSizeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBusinessSizeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBusinessTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBusinessTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBuyerAgreementTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBuyerAgreementTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBuyerAgreementTablePurchaseLineValidateBuyerDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBuyerAgreementTablePurchaseLineValidateBuyerDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBuyerAgeementTableByCustomerAndBuyerDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBuyerAgeementTableByCustomerAndBuyerDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBuyerAgreementLineDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBuyerAgreementLineDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBuyerTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBuyerTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBuyerTableDropDownByPurchaseTable(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBuyerTableDropDownByPurchaseTable`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getConsortiumTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetConsortiumTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getContactPersonTransDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetContactPersonTransDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCreditAppRequestTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCreditAppRequestTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }

  getCreditAppRequestTableByCreditAppTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCreditAppRequestTableByCreditAppTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCreditAppTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCreditAppTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCreditAppTableDropDownByMainAgreement(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCreditAppTableDropDownByMainAgreement`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCreditLimitTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCreditLimitTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCreditScoringDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCreditScoringDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCreditTermDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCreditTermDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCurrencyDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCurrencyDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCustBankDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCustBankDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCustGroupDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCustGroupDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCustomerTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCustomerTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCreditAppRequestTableByAssignmentAgreementTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCreditAppRequestTableByAssignmentAgreementTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCreditAppReqAssignmentByCreditAppRequestTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCreditAppReqAssignmentByCreditAppRequestTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }

  getDocumentConditionTemplateTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetDocumentConditionTemplateTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getDocumentReasonDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetDocumentReasonDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getDocumentStatusDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetDocumentStatusDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getDocumentTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetDocumentTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getEmployeeTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetEmployeeTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getDocumentReturnMethodDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetDocumentReturnMethodDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getEmployeeTableByNotCurrentEmpDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetEmployeeTableByNotCurrentEmpDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getExposureGroupDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetExposureGroupDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getGenderDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetGenderDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getGradeClassificationDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetGradeClassificationDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getGuarantorTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetGuarantorTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getInterestTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetInterestTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getIntroducedByDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetIntroducedByDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getKYCSetupDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetKYCSetupDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getLedgerDimensionDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetLedgerDimensionDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getLineOfBusinessDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetLineOfBusinessDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getMainAgreementTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetMainAgreementTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getMaritalStatusDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetMaritalStatusDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getMethodOfPaymentDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetMethodOfPaymentDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getNationalityDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetNationalityDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getNCBAccountStatusDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetNCBAccountStatusDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getOccupationDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetOccupationDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getOwnershipDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetOwnershipDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getParentCompanyDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetParentCompanyDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getProductSubTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetProductSubTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getPropertyTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetPropertyTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getServiceFeeCondTemplateTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetServiceFeeCondTemplateTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getTerritoryDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetTerritoryDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getVendorTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetVendorTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getWithholdingTaxGroupDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetWithholdingTaxGroupDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getTaxTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetTaxTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getTaxBranchDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetTaxBranchDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getTaxInvoiceTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetTaxInvoiceTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getIntercompanyTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetIntercompanyTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getInvoiceRevenueTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetInvoiceRevenueTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getWithholdingTaxTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetWithholdingTaxTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getProdUnitTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetProdUnitTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCompanyBankDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCompanyBankDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBankGroupDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBankGroupDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBankTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBankTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getAssetFeeTypeEnumDropDown(): SelectItems[] {
    return transformLabel(ASSET_FEE_TYPE);
  }
  getServiceFeeCategoryEnumDropDown(): SelectItems[] {
    return transformLabel(SERVICE_FEE_CATEGORY);
  }
  getPaymentTypeEnumDropDown(): SelectItems[] {
    return transformLabel(PAYMENT_TYPE);
  }

  getVendGroupDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetVendGroupDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getLanguageDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetLanguageDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getDocumentProcessDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetDocumentProcessDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getAgreementDocTypeEnumDropDown(): SelectItems[] {
    return transformLabel(AGREEMENT_DOC_TYPE);
  }
  getBillingByEnumDropDown(): SelectItems[] {
    return transformLabel(BILLING_BY);
  }
  getContactTypeEnumDropDown(): SelectItems[] {
    return transformLabel(CONTACT_TYPE);
  }
  getCreditLimitConditionTypeEnumDropDown(): SelectItems[] {
    return transformLabel(CREDIT_LIMIT_CONDITION_TYPE);
  }
  getDocumentTemplateTypeEnumDropDown(): SelectItems[] {
    return transformLabel(DOCUMENT_TEMPLATE_TYPE);
  }
  getBookmarkDocumentRefTypeEnumDropDown(): SelectItems[] {
    return transformLabel(BOOKMARK_DOCUMENT_REF_TYPE);
  }

  getCreditLimitExpirationEnumDropDown(): SelectItems[] {
    return transformLabel(CREDIT_LIMIT_EXPIRATION);
  }

  getCustTransEnumDropDown(): SelectItems[] {
    return transformLabel(CUST_TRANS_STATUS);
  }

  getDocConVerifyTypeEnumDropDown(): SelectItems[] {
    return transformLabel(DOC_CON_VERIFY_TYPE);
  }
  getIdentificationTypeEnumDropDown(): SelectItems[] {
    return transformLabel(IDENTIFICATION_TYPE);
  }
  getNumberSeqTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetNumberSeqTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getNumberSeqSegmentTypeDropDown(): SelectItems[] {
    return transformLabel(NUMBER_SEQ_SEGMENT_TYPE);
  }
  getMethodOfBillingEnumDropDown(): SelectItems[] {
    return transformLabel(METHOD_OF_BILLING);
  }
  getProductTypeEnumDropDown(): SelectItems[] {
    return transformLabel(PRODUCT_TYPE);
  }
  getPurchaseFeeCalculateBaseEnumDropDown(): SelectItems[] {
    return transformLabel(PURCHASE_FEE_CALCULATE_BASE);
  }
  getRecordTypeEnumDropDown(): SelectItems[] {
    return transformLabel(RECORD_TYPE);
  }
  getRefTypeEnumDropDown(): SelectItems[] {
    return transformLabel(REF_TYPE);
  }
  getRetentionCalculateBaseEnumDropDown(): SelectItems[] {
    return transformLabel(RETENTION_CALCULATE_BASE);
  }
  getRetentionDeductionMethodEnumDropDown(): SelectItems[] {
    return transformLabel(RETENTION_DEDUCTION_METHOD);
  }
  getDefaultBitDropDown(): SelectItems[] {
    return transformLabel(DEFAULT_BIT);
  }
  getDimensionEnumDropDown(): SelectItems[] {
    return transformLabel(DIMENSION);
  }
  getCreditAppRequestTypeToEnumDropDown(): SelectItems[] {
    return transformLabel(CREDIT_APP_REQUEST_TYPE);
  }
  getCreditLimitExpirationToEnumDropDown(): SelectItems[] {
    return transformLabel(CREDIT_LIMIT_EXPIRATION);
  }
  getProductTypeToEnumDropDown(): SelectItems[] {
    return transformLabel(PRODUCT_TYPE);
  }
  getPurchaseFeeCalculateBaseToEnumDropDown(): SelectItems[] {
    return transformLabel(PURCHASE_FEE_CALCULATE_BASE);
  }
  getMonthOfYearEnumDropDown(): SelectItems[] {
    return transformLabel(MONTH_OF_YEAR);
  }
  getReceivedFromEnumDropDown(): SelectItems[] {
    return transformLabel(RECEIVED_FROM);
  }
  getShowDocConVerifyTypeEnumDropDown(): SelectItems[] {
    return transformLabel(SHOW_DOC_CON_VERIFY_TYPE);
  }
  //#region DEMO
  getDocumentProcessDropdown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetDocumentProcessDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getDocumentStatusByDocumentProcessDropdown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetDocumentStatusDocumentProcessDropdown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getEmployeeFilterActiveStatusDropdown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetEmployeeFilterActiveStatusDropdown`;
    return this.dataGateway.getDropdown(url, conditions);
  }

  //#endregion
  getRelatedInfoAddressTransAddressCountryDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAddressCountryDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getRelatedInfoAddressTransAddressPostalCodeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAddressPostalCodeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getRelatedInfoAddressTransOwnershipDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetOwnershipDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getRelatedInfoAddressTransPropertyTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetPropertyTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getDepartmentDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetDepartmentDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getEmplTeamDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetEmplTeamDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCreditLimitTypeByProductTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCreditLimitTypeByProductTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getProductSubTypeByProductTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetProductSubTypeByProductTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getServiceFeeCondTemplateTableByProductTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetServiceFeeCondTemplateTableByProductTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getSuspenseInvoiceTypeEnumDropDown(): SelectItems[] {
    return transformLabel(SUSPENSE_INVOICE_TYPE);
  }
  getAddressTransByCustomerDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAddressTransByCustomerDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getContactPersonTransByCustomerDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetContactPersonTransByCustomerDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCustBankByBankAccountControlDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCustBankByBankAccountControlDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCustBankByPDCDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCustBankByPDCDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCustomerTableByDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCustomerTableByDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getNotCancelApplicationTableByCustomerDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetNotCancelApplicationTableByCustomerDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getInvoiceRevenueTypeByProductTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetInvoiceRevenueTypeByProductTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBranchDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBranchDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBusinessUnitDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBusinessUnitDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCreditTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCreditTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBusinessUnitFilterExcludeBusinessUnitDropdown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBusinessUnitFilterExcludeBusinessUnitDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getApprovalDecisionEnumDropDown(): SelectItems[] {
    return transformLabel(APPROVAL_DECISION);
  }
  getDayOfMonthEnumDropDown(): SelectItems[] {
    return transformLabel(DAY_OF_MONTH);
  }
  getContactPersonTransByBuyerDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetContactPersonTransByBuyerDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getAddressTransByBuyerDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAddressTransByBuyerDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBuyerTableByCreditAppRequestDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBuyerTableByCreditAppRequestDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBuyerTableByCreditAppTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBuyerTableByCreditAppTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBuyerInvoiceTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBuyerInvoiceTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getAssignmentAgreementTableByBuyerDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAssignmentAgreementTableByBuyerDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getWorkingDayEnumDropDown(): SelectItems[] {
    return transformLabel(WORKING_DAY);
  }
  getHolidayTypeEnumDropDown(): SelectItems[] {
    return transformLabel(HOLIDAY_TYPE);
  }
  getCalendarGroupDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCalendarGroupDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCalcInterestMethodEnumDropDown(): SelectItems[] {
    return transformLabel(CALC_INTEREST_METHOD);
  }
  getCalcInterestDayMethodEnumDropDown(): SelectItems[] {
    return transformLabel(CALC_INTEREST_DAY_METHOD);
  }
  getBusinessCollateralTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBusinessCollateralTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCompanySignatureRefTypeEnumDropDown(): SelectItems[] {
    return transformLabel(COMPANY_SIGNATURE_REF_TYPE);
  }
  getInvoiceTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetInvoiceTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getInvoiceTableByStatusInvoiceDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetDocumentStatusByInvoiceTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getRefInvoiceTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetRefInvoiceTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getInvoiceTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetInvoiceTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  GetCusttransDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCusttransDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getInvoiceIssuingEnumDropDown(): SelectItems[] {
    return transformLabel(INVOICE_ISSUING);
  }
  getPaymStructureRefNumEnumDropDown(): SelectItems[] {
    return transformLabel(PAYM_STRUCTURE_REF_NUM);
  }
  getReceiptGenByEnumDropDown(): SelectItems[] {
    return transformLabel(RECEIPT_GEN_BY);
  }
  getPenaltyBaseEnumDropDown(): SelectItems[] {
    return transformLabel(PENALTY_BASE);
  }
  getCollectionFeeMethodEnumDropDown(): SelectItems[] {
    return transformLabel(COLLECTION_FEE_METHOD);
  }
  getNPLMethodEnumDropDown(): SelectItems[] {
    return transformLabel(NPL_METHOD);
  }
  getProvisionMethodEnumDropDown(): SelectItems[] {
    return transformLabel(PROVISION_METHOD);
  }
  getProvisionRateByEnumDropDown(): SelectItems[] {
    return transformLabel(PROVISION_RATE_BY);
  }
  getPenaltyCalculationMethodEnumDropDown(): SelectItems[] {
    return transformLabel(PENALTY_CALCULATION_METHOD);
  }
  getOverdueTypeEnumDropDown(): SelectItems[] {
    return transformLabel(OVERDUE_TYPE);
  }
  getBusinessCollateralStatusDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBusinessCollateralStatusDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBusinessCollateralSubTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBusinessCollateralSubTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBookmarkDocumentDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBookmarkDocumentDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getDocumentTemplateTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetDocumentTemplateTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getbookmarkDocumentTemplateTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetbookmarkDocumentTemplateTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getDocumentTemplateTableByDocumentTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetDocumentTemplateTableByDocumentTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getWithdrawalTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetWithdrawalTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCustBusinessCollateralDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCustBusinessCollateralDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCustomervisitingTransDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCustomerVisitingTransDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCustTransStatusEnumDropDown(): SelectItems[] {
    return transformLabel(CUST_TRANS_STATUS);
  }
  getLedgerFiscalPeriodUnitDropDown(): SelectItems[] {
    return transformLabel(LEDGER_FISCAL_PERIOD_UNIT);
  }
  getLedgerFiscalPeriodStatusDropDown(): SelectItems[] {
    return transformLabel(LEDGER_FISCAL_PERIOD_STATUS);
  }
  getLedgerFiscalYearDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetLedgerFiscalYearDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getNotCancelCustBusinessCollateralByCustomerDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetNotCancelCustBusinessCollateralByCustomerDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBusinessCollateralSubTypeByBusinessCollateralTypDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getAuthorizedPersonTransDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAuthorizedPersonTransDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getAuthorizedPersonTransByCustomerDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAuthorizedPersonTransByCustomerDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getAuthorizedPersonTransByBuyerDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAuthorizedPersonTransByBuyerDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getAuthorizedPersonTransByCreditAppDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAuthorizedPersonTransByCreditAppDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getAuthorizedPersonTransByAgreementTableInfoDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAuthorizedPersonTransByAgreementTableInfoDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCompanySignatureDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCompanySignatureDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCompanySignatureByBranchDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCompanySignatureByBranchDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCustBankByCustomerDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCustBankByCustomerDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getSettleInvoiceCriteriaDropDown(): SelectItems[] {
    return transformLabel(SETTLE_INVOICE_CRITERIA);
  }
  getSuspenseinvoicetypeDropdown(): SelectItems[] {
    return transformLabel(SUSPENSE_INVOICE_TYPE);
  }
  getConsortiumLineDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetConsortiumLineDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getConsortiumLineDropDownByConsortiumTable(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetConsortiumLineByConsortiumTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getSysLanguageEnumDropDown(): SelectItems[] {
    return transformLabel(SYS_LANGUAGE);
  }
  getAdUserDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAdUserDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCompanyDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCompanyDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getSysRoleTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetSysRoleTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getGuarantorTransDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/getGuarantorTransDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCreditAppTableByCreditAppRequestTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCreditAppTableByCreditAppRequestTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCustomerTableByMainAgreementDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCustomerTableByMainAgreementDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }

  getCustomerTableByGuarantorIdDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/getCustomerTableByGuarantorIdDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getChequeTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetChequeTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getPurchaseLineDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetPurchaseLineDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getMainAgreementTableByProductDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetMainAgreementTableByGuarantorDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }

  //#region  purchaseLine
  getChequeTableCustomerDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetChequeTableCustomerDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getChequeTableBuyerDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetChequeTableBuyerDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  //#endregion

  getBuyerAgreementTransDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBuyerAgreementTransDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCreditAppDropdownByProductMainAgreementDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCreditAppDropdownByProductMainAgreementDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  GetVerificationTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetVerificationTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getDocumentStatusByVerificationTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/getDocumentStatusByVerificationTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getPaidToTypeEnumDropDown(): SelectItems[] {
    return transformLabel(PAID_TO_TYPE);
  }
  getInvoiceTypeBySuspenseInvoiceTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetInvoiceTypeBySuspenseInvoiceTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getStagingBatchStatusEnumDropDown(): SelectItems[] {
    return transformLabel(STAGING_BATCH_STATUS);
  }
  getProcessTransTypeEnumDropDown(): SelectItems[] {
    return transformLabel(PROCESS_TRANS_TYPE);
  }
  getProcessTransDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetProcessTransDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }

  getChequeTableByStatusDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetChequeTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getChequeRefPDCDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetRefPDCDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getVerificationTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetVerificationTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getVerificationTableByVerificationTransDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetVerificationTableByVerificationTransDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getPurchaseTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetPurchaseTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCreditAppRequestTableByBusinessCollateralAgmTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getResultEnumDropDown(): SelectItems[] {
    return transformLabel(RESULT);
  }
  getCollectionFollowUpResultEnumDropDown(): SelectItems[] {
    return transformLabel(COLLECTION_FOLLOW_UP_RESULT);
  }
  getTaxInvoiceRefTypeEnumDropDown(): SelectItems[] {
    return transformLabel(TAX_INVOICE_REF_TYPE);
  }
  getWithdrawalLineTypeEnumDropDown(): SelectItems[] {
    return transformLabel(WITHDRAWAL_LINE_TYPE);
  }
  getCreditApplicationRequestTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCreditApplicationRequestTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getEmployeeTableWithUserDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetEmployeeTableWithUserDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getEmployeeTableWithCompanySignatureDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetEmployeeTableWithCompanySignature`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getContactToEnumDropDown(): SelectItems[] {
    return transformLabel(CONTACT_TO);
  }
  getPriorityEnumDropDown(): SelectItems[] {
    return transformLabel(PRIORITY);
  }
  getJobTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetJobTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getMessengerTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetMessengerTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }

  getMessengerJobTableByCustomerDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetMessengerTableByCustomerDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCreditAppLineDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCreditAppLineDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getReceiptTempTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetReceiptTempTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getMessengerJobTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetMessengerJobTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getContactPersonDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetContactPersonDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getAddressTransByDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAddressTransByDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getChequeSourceEnumDropDown(): SelectItems[] {
    return transformLabel(CHEQUE_SOURCE);
  }
  getCollectionFollowUpDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCollectionFollowUpDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getChequeTableByCustomerDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetChequeTableByCustomerDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getChequeTableByCustomerAndBuyerDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetChequeTableByCustomerAndBuyerDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }

  getChequeTableByCustomerBuyerReplacedDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetChequeTableByCustomerBuyerReplacedDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }

  getCollectionFollowUpByRefDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCollectionFollowUpByRefDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getInterestCalculationMethodEnumDropDown(): SelectItems[] {
    return transformLabel(INTEREST_CALCULATION_METHOD);
  }
  getAccounTypeEnumDropDown(): SelectItems[] {
    return transformLabel(ACCOUNT_TYPE);
  }
  getReceiptTempRefTypeEnumDropDown(): SelectItems[] {
    return transformLabel(RECEIPT_TEMP_REF_TYPE);
  }
  getBuyerReceiptTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBuyerReceiptTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getInvoiceTypeByReceiptTempTableNoneProductTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetInvoiceTypeDropDownByReceiptTempTableNoneProductType`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getInvoiceTypeByReceiptTempTableProductTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetInvoiceTypeDropDownByReceiptTempTableProductType`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCreditAppTableDropDownByReceiptTempTable(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCreditAppTableDropDownByReceiptTempTable`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getChequeTableByReceiptTempPaymDetailDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetChequeTableByReceiptTempPaymDetailDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getInvoiceSettlementDetailDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetInvoiceSettlementDetailDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBusinessCollateralAgmLineDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBusinessCollateralAgmLineDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBusinessCollateralAgmTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetBusinessCollateralAgmTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getMigrationTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetMigrationTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getCreditAppTableByCustomerRefundTableDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/getCreditAppTableByCustomerRefundTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getAssignmentAgreementTableByCustomerAndStatusDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetAssignmentAgreementTableByCustomerAndStatusDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getVerifyTypeEnumDropDown(): SelectItems[] {
    return transformLabel(VERIFY_TYPE);
  }
  getCreditAppReqAssignmentDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/getCreditAppReqAssignmentDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBusinessSegmentTypeEnumDropDown(): SelectItems[] {
    return transformLabel(BUSINESS_SEGMENT_TYPE);
  }
  getbookmarkDocumentTemplateTableDropDownByRefType(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetbookmarkDocumentTemplateTableDropDownByRefType`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getAccountTypeEnumDropDown(): SelectItems[] {
    return transformLabel(ACCOUNT_TYPE);
  }
  getInterfaceStatusEnumDropDown(): SelectItems[] {
    return transformLabel(INTERFACE_STATUS);
  }
  getSiteLoginTypeEnumDropDown(): SelectItems[] {
    return transformLabel(SITE_LOGIN_TYPE);
  }
  getBatchIntervalTypeEnumDropDown(): SelectItems[] {
    return transformLabel(BATCH_INTERVAL_TYPE);
  }
  getBatchDayOfWeekEnumDropDown(): SelectItems[] {
    return transformLabel(BATCH_DAY_OF_WEEK);
  }
  getOriginalInvoiceDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetOriginalInvoiceDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getWithdrawalTableDropDownByMainAgreement(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetWithdrawalTableDropDownByMainAgreement`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getBatchJobStatusEnumDropDown(): SelectItems[] {
    return transformLabel(BATCH_JOB_STATUS);
  }
  getBatchInstanceStateEnumDropDown(): SelectItems[] {
    return transformLabel(BATCH_INSTANCE_STATE);
  }
  getBatchResultStatusEnumDropDown(): SelectItems[] {
    return transformLabel(BATCH_RESULT_STATUS);
  }
  getSysUserTableDropDownUserNameValue(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetSysUserTableDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
  getInvoiceTypeByProductTypeDropDown(conditions?: SearchCondition[]): DataServiceModel {
    const url: string = `${this.servicePath}/GetInvoiceTypeByProductTypeDropDown`;
    return this.dataGateway.getDropdown(url, conditions);
  }
}
