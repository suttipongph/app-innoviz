import { Injectable } from '@angular/core';
import { AppInjector } from 'app-injector';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { interval, Observable, of } from 'rxjs';
import { AppConst, ColumnType, Operators, RETENTION_CALCULATE_BASE } from 'shared/constants';
import {
  getBindingItemCondition,
  getRelatedItemCondition,
  getRelatedItemConditionDateRange,
  getRelatedItemConditionMultipleValue,
  getSingleBindingItemCondition,
  isNullOrUndefined,
  isNullOrUndefOrEmpty,
  isNullOrUndefOrEmptyGUID,
  isUndefinedOrZeroLength,
  transformLabel
} from 'shared/functions/value.function';
import { DataServiceModel, PageInformationModel, Paginator, SearchCondition, SearchParameter, SelectItems } from 'shared/models/systemModel';
import { BaseComponent } from './base.component';
import { BaseDropdownService } from './base.dropdown.service';
@Injectable({
  providedIn: 'root'
})
export class BaseDropdownComponent {
  getVerificationTableDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getVerificationTableDropDown(conditions), options);
  }
  private dataGateway: DataGatewayService;
  private service: BaseDropdownService;
  constructor() {
    this.dataGateway = AppInjector.get(DataGatewayService);
    this.service = AppInjector.get(BaseDropdownService);
  }
  setDropdownServicePath(param: PageInformationModel): void {
    this.service.setPath(param);
  }
  private getDropdownOption($service: DataServiceModel, options?: SelectItems[]): Observable<SelectItems[]> {
    const pageLimit = 3000;
    if (isNullOrUndefined(options)) {
      const paginator: Paginator = {
        page: 0,
        first: 0,
        rows: -1,
        pageCount: 0
      };
      const search: SearchParameter = {
        conditions: isNullOrUndefined($service.conditions) ? [] : $service.conditions,
        paginator
      };
      return new Observable((observer) => {
        this.dataGateway.getHttpDropdown($service.url, search).subscribe(
          (result) => {
            observer.next(result);
          },
          (error) => {
            observer.error(error);
          },
          () => {
            observer.complete();
          }
        );
      });
    } else {
      let dataResult: SelectItems[] = [];
      let pageIndex = pageLimit * -1;
      let canReq = true;
      options.length = 0;
      const itemKeyConditionIndex = isNullOrUndefined($service.conditions)
        ? -1
        : $service.conditions.findIndex((s) => s.columnName === AppConst.BINDING_ITEM);
      if (itemKeyConditionIndex > -1) {
        const paginator: Paginator = {
          page: pageIndex,
          first: 0,
          rows: 1,
          pageCount: 0
        };
        const searchItem: SearchParameter = {
          conditions: [$service.conditions[itemKeyConditionIndex]],
          paginator,
          excludeRowData: true
        };
        $service.conditions.splice(itemKeyConditionIndex, 1);
        this.dataGateway.getHttpDropdown($service.url, searchItem).subscribe(
          (resultItem) => {
            resultItem.forEach((item) => {
              options.push(item);
            });
            const paginator: Paginator = {
              page: pageIndex,
              first: 0,
              rows: pageLimit,
              pageCount: 0
            };
            const search: SearchParameter = {
              conditions: $service.conditions,
              paginator
            };
            const $interval = interval(500).subscribe((val) => {
              if (canReq) {
                paginator.page = paginator.page + pageLimit;
                dataResult = [];
                canReq = false;
                this.dataGateway.getHttpDropdown($service.url, search).subscribe(
                  (result) => {
                    dataResult = result;
                    if (dataResult.length < pageLimit) {
                      $interval.unsubscribe();
                    }
                    result.forEach((item) => {
                      options.push(item);
                    });
                    canReq = true;
                    pageIndex++;
                  },
                  (error) => {
                    canReq = false;
                    $interval.unsubscribe();
                    // this.notificationService.showErrorMessageFromResponse(error);
                  }
                );
              }
            });
          },
          (error) => {
            // this.notificationService.showErrorMessageFromResponse(error);
          }
        );
      } else {
        const paginator: Paginator = {
          page: pageIndex,
          first: 0,
          rows: pageLimit,
          pageCount: 0
        };
        const search: SearchParameter = {
          conditions: $service.conditions,
          paginator,
          excludeRowData: true
        };
        const $interval = interval(1500).subscribe((val) => {
          if (canReq) {
            paginator.page = paginator.page + pageLimit;
            dataResult = [];
            canReq = false;
            this.dataGateway.getHttpDropdown($service.url, search).subscribe(
              (result) => {
                dataResult = result;
                if (dataResult.length < pageLimit) {
                  $interval.unsubscribe();
                }
                result.forEach((item) => {
                  options.push(item);
                });
                canReq = true;
                pageIndex++;
              },
              (error) => {
                canReq = false;
                $interval.unsubscribe();
                // this.notificationService.showErrorMessageFromResponse(error);
              }
            );
          }
        });
      }
    }
  }
  private getEnumDropdownOption($getDropdownList: SelectItems[], options: SelectItems[], filterBy?: number[]): SelectItems[] {
    options.length = 0;
    if (!isUndefinedOrZeroLength(filterBy)) {
      $getDropdownList
        .filter((f) => filterBy.includes(f.value))
        .forEach((item) => {
          options.push(item);
        });
    } else {
      $getDropdownList.forEach((item) => {
        options.push(item);
      });
    }
    return options;
  }

  getAddressCountryDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getAddressCountryDropDown(), options);
  }
  getAddressDistrictDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getAddressDistrictDropDown(), options);
  }
  getAddressPostalCodeDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getAddressPostalCodeDropDown(conditions), options);
  }
  getAddressProvinceDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getAddressProvinceDropDown(), options);
  }
  getAddressSubDistrictDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getAddressSubDistrictDropDown(), options);
  }
  getAddressTransDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getAddressTransDropDown(), options);
  }
  getApplicationTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getApplicationTableDropDown(), options);
  }
  getAssignmentAgreementSettleDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getAssignmentAgreementSettleDropDown(), options);
  }
  getAssignmentAgreementTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getAssignmentAgreementTableDropDown(), options);
  }
  getAssignmentAgreementTableByWithdrawalDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getAssignmentAgreementTableDropDown(conditions), options);
  }
  getAssignmentAgreementTableByCustomerDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getAssignmentAgreementTableByCustomerDropDown(), options);
  }

  getAssignmentMethodDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getAssignmentMethodDropDown(), options);
  }
  getAuthorizedPersonTypeDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getAuthorizedPersonTypeDropDown(), options);
  }
  getBillingResponsibleByDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getBillingResponsibleByDropDown(), options);
  }
  getBlacklistStatusDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getBlacklistStatusDropDown(), options);
  }
  getBusinessSegmentDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getBusinessSegmentDropDown(), options);
  }
  getBuyerAgreementTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getBuyerAgreementTableDropDown(), options);
  }
  getBuyerAgeementTableByCustomerAndBuyerDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getBuyerAgeementTableByCustomerAndBuyerDropDown(conditions), options);
  }
  getBuyerAgreementLineDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getBuyerAgreementLineDropDown(), options);
  }
  getBuyerTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getBuyerTableDropDown(), options);
  }
  getBuyerInvoiceTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getBuyerInvoiceTableDropDown(), options);
  }
  getConsortiumTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getConsortiumTableDropDown(), options);
  }
  getContactPersonTransDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getContactPersonTransDropDown(), options);
  }
  getCreditAppRequestTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getCreditAppRequestTableDropDown(), options);
  }
  getCreditAppReqAssignmentDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getCreditAppReqAssignmentDropDown(), options);
  }
  getCreditAppTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getCreditAppTableDropDown(), options);
  }
  getCreditAppTableByDropDown(values: string[], date: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditionsDate: SearchCondition[] = getRelatedItemConditionDateRange(date);
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    conditions.push(...conditionsDate);
    return this.getDropdownOption(this.service.getCreditAppTableDropDown(conditions), options);
  }
  getCreditLimitTypeDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getCreditLimitTypeDropDown(), options);
  }
  getCreditScoringDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getCreditScoringDropDown(), options);
  }
  getCreditTermDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getCreditTermDropDown(), options);
  }
  getCurrencyDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getCurrencyDropDown(), options);
  }
  getCustBankDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getCustBankDropDown(), options);
  }
  getCustGroupDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getCustGroupDropDown(), options);
  }
  getCustomerTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getCustomerTableDropDown(), options);
  }
  getCreditAppRequestTableByAssignmentAgreementTableDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getCreditAppRequestTableByAssignmentAgreementTableDropDown(conditions), options);
  }
  getCreditAppReqAssignmentByCreditAppRequestTableDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getCreditAppReqAssignmentByCreditAppRequestTableDropDown(conditions), options);
  }
  getDocumentConditionTemplateTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getDocumentConditionTemplateTableDropDown(), options);
  }
  getDocumentReasonDropDown(refType: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(refType);
    return this.getDropdownOption(this.service.getDocumentReasonDropDown(conditions), options);
  }

  getDocumentReasonNoneConditionDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getDocumentReasonDropDown(), options);
  }
  //   getBuyerAgreementTransByCreditAppLineDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
  //   const conditions: SearchCondition[] = getRelatedItemCondition(id);
  //   return this.getDropdownOption(this.service.getBuyerAgreementTransDropDown(conditions), options);
  // }

  getDocumentStatusDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getDocumentStatusDropDown(), options);
  }
  getDocumentStatusByVerificationTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getDocumentStatusByVerificationTableDropDown(), options);
  }
  getDocumentStatusByInvoiceTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getInvoiceTableByStatusInvoiceDropDown(), options);
  }
  getDocumentTypeDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getDocumentTypeDropDown(), options);
  }
  getEmployeeTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getEmployeeTableDropDown(), options);
  }
  getDocumentReturnMethodDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getDocumentReturnMethodDropDown(), options);
  }
  getExposureGroupDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getExposureGroupDropDown(), options);
  }
  getGenderDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getGenderDropDown(), options);
  }
  getGradeClassificationDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getGradeClassificationDropDown(), options);
  }
  getGuarantorTypeDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getGuarantorTypeDropDown(), options);
  }
  getInterestTypeDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getInterestTypeDropDown(), options);
  }
  getIntroducedByDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getIntroducedByDropDown(), options);
  }
  getKYCSetupDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getKYCSetupDropDown(), options);
  }
  getLedgerDimensionDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getLedgerDimensionDropDown(), options);
  }
  getLineOfBusinessDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getLineOfBusinessDropDown(), options);
  }
  getMainAgreementTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getMainAgreementTableDropDown(), options);
  }
  getMaritalStatusDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getMaritalStatusDropDown(), options);
  }
  getMethodOfPaymentDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getMethodOfPaymentDropDown(), options);
  }
  getNationalityDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getNationalityDropDown(), options);
  }
  getNCBAccountStatusDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getNCBAccountStatusDropDown(), options);
  }
  getNumberSeqTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getNumberSeqTableDropDown(), options);
  }
  getNumberSeqSegmentTypeDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getNumberSeqSegmentTypeDropDown(), options);
  }
  getBusinessSizeDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getBusinessSizeDropDown(), options);
  }
  getBusinessTypeDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getBusinessTypeDropDown(), options);
  }
  getInvoiceRevenueTypeDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getInvoiceRevenueTypeDropDown(), options);
  }
  getInvoiceRevenueTypeByDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getInvoiceRevenueTypeDropDown(conditions), options);
  }
  getInvoiceReferenceDropDown(refType: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(refType);
    return this.getDropdownOption(this.service.getDocumentReasonDropDown(conditions), options);
  }
  getOccupationDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getOccupationDropDown(), options);
  }
  getOwnershipDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getOwnershipDropDown(), options);
  }
  getParentCompanyDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getParentCompanyDropDown(), options);
  }
  getProductSubTypeDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getProductSubTypeDropDown(), options);
  }
  getPropertyTypeDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getPropertyTypeDropDown(), options);
  }
  getRaceDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getRaceDropDown(), options);
  }
  getRegistrationTypeDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getRegistrationTypeDropDown(), options);
  }
  getRelatedPersonTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getRelatedPersonTableDropDown(), options);
  }
  getRelatedPersonTableByCreditAppTableDropDown(value: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(value);
    return this.getDropdownOption(this.service.getRelatedPersonTableByCreditAppRequestTableDropDown(conditions), options);
  }
  getRelatedPersonTableByCreditAppRequestTableGuarantorDropDown(value: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(value);
    return this.getDropdownOption(this.service.getRelatedPersonTableByCreditAppRequestTableGuarantorDropDown(conditions), options);
  }
  getRelatedPersonTableByCreditAppRequestTableOwnerTransDropDown(value: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(value);
    return this.getDropdownOption(this.service.getRelatedPersonTableByCreditAppRequestTableOwnerTransDropDown(conditions), options);
  }
  getRelatedPersonTableByCreditAppRequestTableAuthorizedPersonTransDropDown(value: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(value);
    return this.getDropdownOption(this.service.getRelatedPersonTableByCreditAppRequestTableAuthorizedPersonTransDropDown(conditions), options);
  }

  getRelatedPersonTableByAmendCADropDown(value: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(value);
    return this.getDropdownOption(this.service.getRelatedPersonTableByAmendCADropDown(conditions), options);
  }
  getServiceFeeCondTemplateTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getServiceFeeCondTemplateTableDropDown(), options);
  }
  getTaxBranchDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getTaxBranchDropDown(), options);
  }
  getTaxTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getTaxTableDropDown(), options);
  }
  getTaxInvoiceTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getTaxInvoiceTableDropDown(), options);
  }
  getTerritoryDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getTerritoryDropDown(), options);
  }
  getVendorTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getVendorTableDropDown(), options);
  }
  getWithholdingTaxGroupDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getWithholdingTaxGroupDropDown(), options);
  }
  getWithholdingTaxTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getWithholdingTaxTableDropDown(), options);
  }
  geProdUnitTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getProdUnitTableDropDown(), options);
  }
  getAgreementDocTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getAgreementDocTypeEnumDropDown(), options);
  }
  getAssetFeeTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getAssetFeeTypeEnumDropDown(), options);
  }
  getBillingByEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getBillingByEnumDropDown(), options);
  }
  getContactTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getContactTypeEnumDropDown(), options);
  }
  getProductTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getProductTypeEnumDropDown(), options);
  }
  getCreditLimitConditionTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getCreditLimitConditionTypeEnumDropDown(), options);
  }
  getBookmarkDocumentRefTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getBookmarkDocumentRefTypeEnumDropDown(), options);
  }
  getDocumentTemplateTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getDocumentTemplateTypeEnumDropDown(), options);
  }
  getCreditLimitExpirationEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getCreditLimitExpirationEnumDropDown(), options);
  }
  getCustTransEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getCustTransEnumDropDown(), options);
  }
  getDocConVerifyTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getDocConVerifyTypeEnumDropDown(), options);
  }
  getIdentificationTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getIdentificationTypeEnumDropDown(), options);
  }
  getMethodOfBillingEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getMethodOfBillingEnumDropDown(), options);
  }
  getPurchaseFeeCalculateBaseEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getPurchaseFeeCalculateBaseEnumDropDown(), options);
  }
  getRecordTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getRecordTypeEnumDropDown(), options);
  }
  getRefTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getRefTypeEnumDropDown(), options);
  }
  getRetentionCalculateBaseEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getRetentionCalculateBaseEnumDropDown(), options);
  }
  getRetentionDeductionMethodEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getRetentionDeductionMethodEnumDropDown(), options);
  }
  getDefaultBitDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getDefaultBitDropDown(), options);
  }
  getDimensionEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getDimensionEnumDropDown(), options);
  }
  getCreditAppRequestTypeEnumDropDown(options: SelectItems[], filterBy?: number[]): void {
    this.getEnumDropdownOption(this.service.getCreditAppRequestTypeToEnumDropDown(), options, filterBy);
  }
  getMonthOfYearEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getMonthOfYearEnumDropDown(), options);
  }
  getCompanyBankDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getCompanyBankDropDown(), options);
    return this.getDropdownOption(this.service.getCompanyBankDropDown(), options);
  }
  //#region DEMO
  getDocumentProcessDropdown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getDocumentProcessDropdown(), options);
  }
  getDocumentStatusByDocumentProcessDropdown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getDocumentStatusByDocumentProcessDropdown(conditions), options);
  }
  getEmployeeFilterActiveStatusDropdown(id?: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getSingleBindingItemCondition(id);
    return this.getDropdownOption(this.service.getEmployeeFilterActiveStatusDropdown(conditions), options);
  }
  //#endregion
  getServiceFeeCategoryEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getServiceFeeCategoryEnumDropDown(), options);
  }
  getSuspenseInvoiceTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getSuspenseInvoiceTypeEnumDropDown(), options);
  }
  getVendGroupDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getVendGroupDropDown(), options);
  }
  getLanguageDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getLanguageDropDown(), options);
  }
  getDocumentProcessDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getDocumentProcessDropDown(), options);
  }
  getBankGroupDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getBankGroupDropDown(), options);
  }
  getBankTypeDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getBankTypeDropDown(), options);
  }
  getPaymentTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getPaymentTypeEnumDropDown(), options);
  }
  getRelatedInfoAddressTransAddressCountryDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getRelatedInfoAddressTransAddressCountryDropDown(), options);
  }
  getAddressDistrictByAddressProvinceDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getAddressDistrictDropDown(conditions), options);
  }
  getRelatedInfoAddressTransAddressPostalCodeDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getRelatedInfoAddressTransAddressPostalCodeDropDown(), options);
  }
  getAddressProvinceByAddressCountryDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getAddressProvinceDropDown(conditions), options);
  }
  getAddressSubDistrictByAddressDistrictDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getAddressSubDistrictDropDown(conditions), options);
  }
  getRelatedInfoAddressTransOwnershipDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getRelatedInfoAddressTransOwnershipDropDown(), options);
  }
  getRelatedInfoAddressTransPropertyTypeDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getRelatedInfoAddressTransPropertyTypeDropDown(), options);
  }
  getDepartmentToDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getDepartmentDropDown(), options);
  }
  getEmplTeamToDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getEmplTeamDropDown(), options);
  }
  getLedgerDimensionToDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getLedgerDimensionDropDown(), options);
  }
  getCreditLimitTypeByProductTypeDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getCreditLimitTypeByProductTypeDropDown(conditions), options);
  }
  getProductSubTypeByProductTypeDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getProductSubTypeByProductTypeDropDown(conditions), options);
  }
  getServiceFeeCondTemplateTableByProductTypeDropDown(productType: string, id?: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditionsSingleBindingItem: SearchCondition[] = getSingleBindingItemCondition(id);
    const conditions: SearchCondition[] = getRelatedItemCondition(productType);
    conditions.push(...conditionsSingleBindingItem);
    return this.getDropdownOption(this.service.getServiceFeeCondTemplateTableByProductTypeDropDown(conditions), options);
  }
  getAddressTransByCustomerDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getAddressTransByCustomerDropDown(conditions), options);
  }
  getContactPersonTransByCustomerDropDown(customerId: string, id?: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditionsSingleBindingItem: SearchCondition[] = getSingleBindingItemCondition(id);
    const conditions: SearchCondition[] = getRelatedItemCondition(customerId);
    conditions.push(...conditionsSingleBindingItem);
    return this.getDropdownOption(this.service.getContactPersonTransByCustomerDropDown(conditions), options);
  }
  getCustBankByBankAccountControlDropDown(customerId: string, id?: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditionsSingleBindingItem: SearchCondition[] = getSingleBindingItemCondition(id);
    const conditions: SearchCondition[] = getRelatedItemCondition(customerId);
    conditions.push(...conditionsSingleBindingItem);
    return this.getDropdownOption(this.service.getCustBankByBankAccountControlDropDown(conditions), options);
  }
  getCustBankByPDCDropDown(customerId: string, id?: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditionsSingleBindingItem: SearchCondition[] = getSingleBindingItemCondition(id);
    const conditions: SearchCondition[] = getRelatedItemCondition(customerId);
    conditions.push(...conditionsSingleBindingItem);
    return this.getDropdownOption(this.service.getCustBankByPDCDropDown(conditions), options);
  }
  getCustomerTableByDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getCustomerTableByDropDown(), options);
  }
  getNotCancelApplicationTableByCustomerDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getNotCancelApplicationTableByCustomerDropDown(conditions), options);
  }
  getInvoiceRevenueTypeByProductTypeDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getInvoiceRevenueTypeByProductTypeDropDown(conditions), options);
  }
  getBranchToDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getBranchDropDown(), options);
  }
  getBusinessUnitToDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getBusinessUnitDropDown(), options);
  }
  getBusinessUnitFilterExcludeBusinessUnitDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getBusinessUnitFilterExcludeBusinessUnitDropdown(conditions), options);
  }
  getCreditTypeDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getCreditTypeDropDown(), options);
  }
  getIntercompanyTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getIntercompanyTableDropDown(), options);
  }
  getApprovalDecisionEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getApprovalDecisionEnumDropDown(), options);
  }
  getDayOfMonthEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getDayOfMonthEnumDropDown(), options);
  }
  getContactPersonTransByBuyerDropDown(buyerId: string, id?: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditionsSingleBindingItem: SearchCondition[] = getSingleBindingItemCondition(id);
    const conditions: SearchCondition[] = getRelatedItemCondition(buyerId);
    conditions.push(...conditionsSingleBindingItem);
    return this.getDropdownOption(this.service.getContactPersonTransByBuyerDropDown(conditions), options);
  }
  getAddressTransByBuyerDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getAddressTransByBuyerDropDown(conditions), options);
  }
  GetBuyerTableByCreditAppRequestDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getBuyerTableByCreditAppRequestDropDown(), options);
  }
  GetBuyerTableByCreditAppTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getBuyerTableByCreditAppTableDropDown(), options);
  }
  getAssignmentAgreementTableByBuyerDropDown(assignAgreementId: string, id?: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditionsSingleBindingItem: SearchCondition[] = getSingleBindingItemCondition(id);
    const conditions: SearchCondition[] = getRelatedItemCondition(assignAgreementId);
    conditions.push(...conditionsSingleBindingItem);
    return this.getDropdownOption(this.service.getAssignmentAgreementTableByBuyerDropDown(conditions), options);
  }
  getWorkingDayEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getWorkingDayEnumDropDown(), options);
  }
  getHolidayTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getHolidayTypeEnumDropDown(), options);
  }
  getCalendarGroupDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getCalendarGroupDropDown(), options);
  }
  getCalcInterestMethodEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getCalcInterestMethodEnumDropDown(), options);
  }
  getCalcInterestDayMethodEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getCalcInterestDayMethodEnumDropDown(), options);
  }
  getBusinessCollateralTypeDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getBusinessCollateralTypeDropDown(), options);
  }
  getCompanySignatureRefTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getCompanySignatureRefTypeEnumDropDown(), options);
  }
  getCusttransDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.GetCusttransDropDown(), options);
  }
  getInvoiceTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getInvoiceTableDropDown(), options);
  }
  getInvoiceTableByDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getInvoiceTableDropDown(conditions), options);
  }
  getRefInvoiceTableDropDown(refInvoiceGUID: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(refInvoiceGUID);
    return this.getDropdownOption(this.service.getRefInvoiceTableDropDown(conditions), options);
  }
  getInvoiceTypeDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getInvoiceTypeDropDown(), options);
  }
  getInvoiceTypeByProductTypeDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getInvoiceTypeByProductTypeDropDown(conditions), options);
  }
  getInvoiceIssuingToDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getInvoiceIssuingEnumDropDown(), options);
  }
  getPaymStructureRefNumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getPaymStructureRefNumEnumDropDown(), options);
  }
  getReceiptGenByDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getReceiptGenByEnumDropDown(), options);
  }
  getPenaltyBaseDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getPenaltyBaseEnumDropDown(), options);
  }
  getCollectionFeeMethodDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getCollectionFeeMethodEnumDropDown(), options);
  }
  getNPLMethodDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getNPLMethodEnumDropDown(), options);
  }
  getProvisionMethodDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getProvisionMethodEnumDropDown(), options);
  }
  getProvisionRateByDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getProvisionRateByEnumDropDown(), options);
  }
  getPenaltyCalculationMethodDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getPenaltyCalculationMethodEnumDropDown(), options);
  }
  getOverdueTypeDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getOverdueTypeEnumDropDown(), options);
  }
  // BuyerAgreementTrans
  getBuyerAgeementTableByBuyerAgreementTransDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getBuyerAgreementTableDropDown(conditions), options);
  }
  getBuyerAgreementLineByBuyerAgreementTransDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);

    return this.getDropdownOption(this.service.getBuyerAgreementLineDropDown(conditions), options);
  }
  getBusinessCollateralStatusDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getBusinessCollateralStatusDropDown(), options);
  }
  getBusinessCollateralSubTypeDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getBusinessCollateralSubTypeDropDown(), options);
  }
  getBookmarkDocumentDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getBookmarkDocumentDropDown(), options);
  }
  getDocumentTemplateTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getDocumentTemplateTableDropDown(), options);
  }
  getBookmarkDocumentTemplateTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getbookmarkDocumentTemplateTableDropDown(), options);
  }
  getDocumentTemplateTableDropDownByDocumenttemplatetype(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getDocumentTemplateTableByDocumentTypeDropDown(conditions), options);
  }
  getWithdrawalTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getWithdrawalTableDropDown(), options);
  }
  getCreditAppTableByMainAgreementTableDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getCreditAppTableDropDownByMainAgreement(conditions), options);
  }
  getBuyerTableByMainAgreementTableDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getBuyerTableDropDown(conditions), options);
  }
  getCustBusinessCollateralDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getCustBusinessCollateralDropDown(), options);
  }
  getCustomerVisitingTransByRefType(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getCustomervisitingTransDropDown(conditions), options);
  }
  getCustTransStatusEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getCustTransStatusEnumDropDown(), options);
  }
  getLedgerFiscalPeriodUnitDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getLedgerFiscalPeriodUnitDropDown(), options);
  }
  getLedgerFiscalPeriodStatusDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getLedgerFiscalPeriodStatusDropDown(), options);
  }
  getLedgerFiscalYearDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getLedgerFiscalYearDropDown(), options);
  }
  getBuyerAgreementTableByBuyerAndCustomerDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getBuyerAgreementTableDropDown(conditions), options);
  }
  getNotCancelCustBusinessCollateralByCustomerDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getNotCancelCustBusinessCollateralByCustomerDropDown(conditions), options);
  }
  getBusinessCollateralSubTypeByBusinessCollateralTypDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getBusinessCollateralSubTypeByBusinessCollateralTypDropDown(conditions), options);
  }
  getAuthorizedPersonTransDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getAuthorizedPersonTransDropDown(), options);
  }
  getAuthorizedPersonTransByCustomerDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getAuthorizedPersonTransByCustomerDropDown(conditions), options);
  }
  getAuthorizedPersonTransByBuyerDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getAuthorizedPersonTransByBuyerDropDown(conditions), options);
  }
  getAuthorizedPersonTransByCreditAppDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getAuthorizedPersonTransByCreditAppDropDown(conditions), options);
  }
  getAuthorizedPersonTransByAgreementTableInfoDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getAuthorizedPersonTransByAgreementTableInfoDropDown(conditions), options);
  }
  getCompanySignatureDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getCompanySignatureDropDown(), options);
  }
  getCompanySignatureByBranchDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getCompanySignatureByBranchDropDown(), options);
  }
  getCustBankByCustomerDropDown(value: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(value);
    return this.getDropdownOption(this.service.getCustBankByCustomerDropDown(conditions), options);
  }
  getSettleInvoiceCriteriaDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getSettleInvoiceCriteriaDropDown(), options);
  }
  getSuspenseinvoicetypeDropdown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getSuspenseinvoicetypeDropdown(), options);
  }
  getConsortiumLineDropDownByConsortiumTable(value: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(value);
    return this.getDropdownOption(this.service.getConsortiumLineDropDownByConsortiumTable(conditions), options);
  }
  getSysLanguageEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getSysLanguageEnumDropDown(), options);
  }
  getAdUserDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getAdUserDropDown(), options);
  }
  getCompanyDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getCompanyDropDown(), options);
  }
  getEmployeeTableByCompanyDropDown(companyGUID: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(companyGUID);
    return this.getDropdownOption(this.service.getEmployeeTableDropDown(conditions), options);
  }
  getEmployeeTableByNotCurrentEmpDropDown(currentEmpGUID: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(currentEmpGUID);
    return this.getDropdownOption(this.service.getEmployeeTableByNotCurrentEmpDropDown(conditions), options);
  }
  getSysRoleTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getSysRoleTableDropDown(), options);
  }
  getGuarantorTransDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getGuarantorTransDropDown(conditions), options);
  }
  getChequeTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getChequeTableDropDown(), options);
  }
  getPurchaseLineDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getPurchaseLineDropDown(), options);
  }
  getCreditAppTableByCreditAppRequestTableDropDown(
    values: string[],
    date: string[],
    ignoreFilter: boolean,
    options?: SelectItems[]
  ): Observable<SelectItems[]> {
    const conditionsDate: SearchCondition[] = getRelatedItemConditionDateRange(date);
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    conditions.push(...conditionsDate);
    return this.getDropdownOption(this.service.getCreditAppTableByCreditAppRequestTableDropDown(ignoreFilter ? undefined : conditions), options);
  }
  getChequeTableByPurchaseLineCustomerDropDown(values: string[], id?: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditionsSingleBindingItem: SearchCondition[] = getSingleBindingItemCondition(id);
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    conditions.push(...conditionsSingleBindingItem);
    return this.getDropdownOption(this.service.getChequeTableCustomerDropDown(conditions), options);
  }
  getChequeTableByPurchaseLineBuyerDropDown(values: string[], id?: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditionsSingleBindingItem: SearchCondition[] = getSingleBindingItemCondition(id);
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    conditions.push(...conditionsSingleBindingItem);
    return this.getDropdownOption(this.service.getChequeTableBuyerDropDown(conditions), options);
  }
  getBuyerTableByPurchaseLineDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getBuyerTableDropDownByPurchaseTable(conditions), options);
  }
  getBuyerInvoiceTableByCreditAppLineDropDown(value: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(value);
    return this.getDropdownOption(this.service.getBuyerInvoiceTableDropDown(conditions), options);
  }
  getAssignmentAgreementTableByCustomerTableDropDown(value: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(value);
    return this.getDropdownOption(this.service.getAssignmentAgreementTableByCustomerDropDown(conditions), options);
  }
  getAssignmentAgreementTableByPurchaseLineDropDown(values: string[], id?: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditionsSingleBindingItem: SearchCondition[] = getSingleBindingItemCondition(id);
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    conditions.push(...conditionsSingleBindingItem);
    return this.getDropdownOption(this.service.getAssignmentAgreementTableDropDown(conditions), options);
  }
  getBuyerAgeementTableByPurchaseLineDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getBuyerAgreementTableDropDown(conditions), options);
  }
  getBuyerAgeementTableByPurchaseLineValidateBuyerDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getBuyerAgreementTablePurchaseLineValidateBuyerDropDown(conditions), options);
  }
  getVerificationTypeDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.GetVerificationTypeDropDown(), options);
  }
  getPaidToTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getPaidToTypeEnumDropDown(), options);
  }
  getStagingBatchStatusEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getStagingBatchStatusEnumDropDown(), options);
  }
  getProcessTransTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getProcessTransTypeEnumDropDown(), options);
  }
  getProcessTransDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getProcessTransDropDown(), options);
  }
  getChequeTableByStatusDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getChequeTableByStatusDropDown(conditions), options);
  }
  getReceivedFromEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getReceivedFromEnumDropDown(), options);
  }
  getRefPDCDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getChequeRefPDCDropDown(), options);
  }
  getCustomerTableByMainAgreementDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getCustomerTableByMainAgreementDropDown(conditions), options);
  }

  getCustomerTableByGuarantorIdDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getCustomerTableByGuarantorIdDropDown(conditions), options);
  }
  getMainAgreementTableByProductDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getMainAgreementTableByProductDropDown(conditions), options);
  }
  getVerificationTableByVerificationTransListViewDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getVerificationTableDropDown(), options);
  }
  getVerificationTableByVerificationTransItemViewDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getVerificationTableByVerificationTransDropDown(conditions), options);
  }
  getPurchaseTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getPurchaseTableDropDown(), options);
  }
  getBuyerTableByCreditAppTableDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getBuyerTableByCreditAppTableDropDown(conditions), options);
  }
  getCreditApplicationRequestTableDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getCreditApplicationRequestTableDropDown(conditions), options);
  }
  getResultEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getResultEnumDropDown(), options);
  }
  getCollectionFollowUpResultEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getCollectionFollowUpResultEnumDropDown(), options);
  }
  getCreditAppRequestTableDropDownByBusinessCollateralAgmTable(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getCreditAppRequestTableByBusinessCollateralAgmTableDropDown(conditions), options);
  }
  getTaxInvoiceRefTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getTaxInvoiceRefTypeEnumDropDown(), options);
  }
  getWithdrawalLineTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getWithdrawalLineTypeEnumDropDown(), options);
  }
  getEmployeeTableWithUserDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getEmployeeTableWithUserDropDown(), options);
  }
  getEmployeeTableWithCompanySignatureDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getEmployeeTableWithCompanySignatureDropDown(), options);
  }
  getCreditAppLineByWithdrawalDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getCreditAppLineDropDown(conditions), options);
  }
  getBuyerAgreementTransByCreditAppLineDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getBuyerAgreementTransDropDown(conditions), options);
  }
  getCreditAppDropdownByProductMainAgreementDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getCreditAppDropdownByProductMainAgreementDropDown(conditions), options);
  }
  getReceiptTempTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getReceiptTempTableDropDown(), options);
  }
  getChequeTableByWithdrawalLineCustomerDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getChequeTableCustomerDropDown(conditions), options);
  }
  getChequeTableByWithdrawalLineBuyerDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getChequeTableBuyerDropDown(conditions), options);
  }
  getContactToEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getContactToEnumDropDown(), options);
  }
  getPriorityEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getPriorityEnumDropDown(), options);
  }
  getJobTypeDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getJobTypeDropDown(), options);
  }
  getMessengerTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getMessengerTableDropDown(), options);
  }
  getMessengerJobTableByCustomerDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getMessengerJobTableByCustomerDropDown(conditions), options);
  }
  getCreditAppLineDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);

    return this.getDropdownOption(this.service.getCreditAppLineDropDown(conditions), options);
  }
  getMessengerJobTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getMessengerJobTableDropDown(), options);
  }
  getContactPersonDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getContactPersonDropDown(conditions), options);
  }
  getAddressTransByDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getAddressTransByDropDown(conditions), options);
  }
  getChequeSourceEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getChequeSourceEnumDropDown(), options);
  }
  getCollectionFollowUpDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getCollectionFollowUpDropDown(), options);
  }
  getChequeTableByCustomerDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getChequeTableByCustomerDropDown(conditions), options);
  }
  getChequeTableByCustomerAndBuyerDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getChequeTableByCustomerAndBuyerDropDown(conditions), options);
  }
  getChequeTableByCustomerBuyerReplacedDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getChequeTableByCustomerBuyerReplacedDropDown(conditions), options);
  }

  getCollectionFollowUpByRefDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getCollectionFollowUpByRefDropDown(conditions), options);
  }
  getAccounTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getAccounTypeEnumDropDown(), options);
  }

  getCreditAppRequestTableByCreditAppTableDropDown(value: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(value);
    return this.getDropdownOption(this.service.getCreditAppRequestTableByCreditAppTableDropDown(conditions), options);
  }
  getInterestCalculationMethodEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getInterestCalculationMethodEnumDropDown(), options);
  }
  getReceiptTempRefTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getReceiptTempRefTypeEnumDropDown(), options);
  }
  getBuyerReceiptTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getBuyerReceiptTableDropDown(), options);
  }
  getBuyerReceiptTableByReceiptTempTableDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getBuyerReceiptTableDropDown(conditions), options);
  }
  getInvoiceTypeByReceiptTempTableNoneProductTypeDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getInvoiceTypeByReceiptTempTableNoneProductTypeDropDown(), options);
  }
  getInvoiceTypeByReceiptTempTableProductTypeDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getInvoiceTypeByReceiptTempTableProductTypeDropDown(conditions), options);
  }

  getCreditAppTableByReceiptTempTableDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getCreditAppTableDropDownByReceiptTempTable(conditions), options);
  }
  getChequeTableByReceiptTempPaymDetailDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getChequeTableByReceiptTempPaymDetailDropDown(conditions), options);
  }
  getInvoiceSettlementDetaileDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getInvoiceSettlementDetailDropDown(), options);
  }
  getCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown(conditions), options);
  }
  getBusinessCollateralAgmLineDropDown(
    originalBusinessCollateralAgreementTableId: string,
    originalBusinessCollateralAgreementLineId?: string,
    options?: SelectItems[]
  ): Observable<SelectItems[]> {
    const conditionsSingleBindingItem: SearchCondition[] = getSingleBindingItemCondition(originalBusinessCollateralAgreementLineId);
    const conditions: SearchCondition[] = getRelatedItemCondition(originalBusinessCollateralAgreementTableId);
    conditions.push(...conditionsSingleBindingItem);
    return this.getDropdownOption(this.service.getBusinessCollateralAgmLineDropDown(conditions), options);
  }
  getBusinessCollateralAgmTableDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getBusinessCollateralAgmTableDropDown(conditions), options);
  }
  getMigrationTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getMigrationTableDropDown(), options);
  }
  getCreditAppTableByCustomerRefundTableDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getCreditAppTableByCustomerRefundTableDropDown(conditions), options);
  }
  getAssignmentAgreementTableByCustomerAndStatusDropDown(values: string[], options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemConditionMultipleValue(values);
    return this.getDropdownOption(this.service.getAssignmentAgreementTableByCustomerAndStatusDropDown(conditions), options);
  }
  getVerifyTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getVerifyTypeEnumDropDown(), options);
  }
  getBusinessSegmentTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getBusinessSegmentTypeEnumDropDown(), options);
  }
  getbookmarkDocumentTemplateTableDropDown(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getbookmarkDocumentTemplateTableDropDown(), options);
  }
  getbookmarkDocumentTemplateTableDropDownByReftype(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getbookmarkDocumentTemplateTableDropDownByRefType(), options);
  }
  getAccountTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getAccountTypeEnumDropDown(), options);
  }
  getInterfaceStatusEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getInterfaceStatusEnumDropDown(), options);
  }
  getEmployeeTableByInActiveDropDown(inActive: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(inActive);
    return this.getDropdownOption(this.service.getEmployeeTableDropDown(conditions), options);
  }
  getSiteLoginTypeEnumDropDown(options?: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getSiteLoginTypeEnumDropDown(), options);
  }
  getBatchIntervalTypeEnumDropDown(options?: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getBatchIntervalTypeEnumDropDown(), options);
  }
  getBatchDayOfWeekEnumDropDown(options?: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getBatchDayOfWeekEnumDropDown(), options);
  }
  getBuyerAgeementTableByWithdrawalTableDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getBuyerAgreementTableDropDown(conditions), options);
  }
  getOriginalInvoiceDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getOriginalInvoiceDropDown(conditions), options);
  }
  getWithdrawalTableByMainAgreementTableDropDown(id: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditions: SearchCondition[] = getRelatedItemCondition(id);
    return this.getDropdownOption(this.service.getWithdrawalTableDropDownByMainAgreement(conditions), options);
  }
  getBatchJobStatusEnumDropDown(options?: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getBatchJobStatusEnumDropDown(), options);
  }
  getBatchInstanceStateEnumDropDown(options?: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getBatchInstanceStateEnumDropDown(), options);
  }
  getBatchResultStatusEnumDropDown(options?: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getBatchResultStatusEnumDropDown(), options);
  }
  getGuarantorTransDropDownByCreditAppTable(creditAppTableGUID: string, id?: string, options?: SelectItems[]): Observable<SelectItems[]> {
    const conditionsSingleBindingItem: SearchCondition[] = getSingleBindingItemCondition(id);
    const conditions: SearchCondition[] = getRelatedItemCondition(creditAppTableGUID);
    conditions.push(...conditionsSingleBindingItem);
    return this.getDropdownOption(this.service.getGuarantorTransDropDown(conditions), options);
  }
  getBusinessCollateralAgmTableDropDownByBusinessCollateralAgmTable(
    originalBusinessCollateralAgreementTableGUID: string,
    id?: string,
    options?: SelectItems[]
  ): Observable<SelectItems[]> {
    const conditionsSingleBindingItem: SearchCondition[] = getSingleBindingItemCondition(id);
    const conditions: SearchCondition[] = getRelatedItemCondition(originalBusinessCollateralAgreementTableGUID);
    conditions.push(...conditionsSingleBindingItem);
    return this.getDropdownOption(this.service.getBusinessCollateralAgmTableDropDown(conditions), options);
  }
  getShowDocConVerifyTypeEnumDropDown(options: SelectItems[]): void {
    this.getEnumDropdownOption(this.service.getShowDocConVerifyTypeEnumDropDown(), options);
  }
  getSysUserTableDropDownUserNameValue(options?: SelectItems[]): Observable<SelectItems[]> {
    return this.getDropdownOption(this.service.getSysUserTableDropDownUserNameValue(), options);
  }
}
