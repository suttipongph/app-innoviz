import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { AppInjector } from 'app-injector';
import { DataGatewayService } from 'core/services/data-gateway.service';
import { DropDownService } from 'core/services/dropdown.service';
import {
  BatchFormConfig,
  ButtonConfig,
  CalendarConfig,
  DropdownConfig,
  FormatConfig,
  InputSwitchConfig,
  InputTextConfig,
  MultiSelectConfig,
  TextareaConfig
} from 'shared/config/format.config';
import { ACTION_PATH, AppConst, ColumnType, RELATED_SPLITTER, ROUTE_RELATED_GEN, WORKFLOW } from 'shared/constants';
import { getModelRegistered, modelGetDirty } from 'shared/functions/model.function';
import {
  isMatchGuid,
  isNullOrUndefined,
  isRedirectPath,
  isSiteWithParam,
  removeIdPath,
  removeLastPath,
  removeRelatedPath,
  setPathToParam,
  removeIdPathMatch,
  switchClass,
  removeAllQueryParams,
  removeQueryParamsByPath,
  moveUrlParamToObjParameter,
  enCodePassingObj,
  deCodePassingObj,
  isNullOrUndefOrEmpty,
  switchAttribute,
  isUndefinedOrZeroLength,
  getQueryStringValue,
  replaceAll,
  getQueryParamObject,
  removeLatestPath,
  setFormReady,
  getPanelContent,
  isSelectAll,
  cloneObject
} from 'shared/functions/value.function';
import { MenuItem } from 'shared/models/primeModel';
import {
  BaseServiceModel,
  FieldAccessing,
  IvzDropdownOnFocusModel,
  PageInformationModel,
  PathParamModel,
  RelatedKeyModel,
  SelectItems
} from 'shared/models/systemModel';
import { AccessLevelModel, AccessModeView, UserAccessLevelParm } from 'shared/models/viewModel';
import { ValidationService } from 'shared/services/validation.service';
import { AccessRightService } from '../../services/access-right.service';
import { NotificationService } from '../../services/notification.service';
import { UIControllerService } from '../../services/uiController.service';
import { UserDataService } from '../../services/user-data.service';
import { BaseDropdownComponent } from './base.dropdown.component';
import { FileService } from 'core/services/file.service';
import { K2Service } from 'core/services/k2.service';
import { isInterfacedWithK2 } from 'shared/config/globalvar.config';
import { isWorkFlowMode, setBodyFunctionMode, setRoutingGateway } from 'shared/functions/routing.function';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-base',
  template: ` <p>base works!</p> `
})
export class BaseComponent {
  public translate: TranslateService;
  public dropdownService: DropDownService;
  public notificationService: NotificationService;
  public accessService: AccessRightService;
  public accessRight: AccessLevelModel;

  public validateService: ValidationService;
  public fileService: FileService;
  public k2Service: K2Service;
  public router: Router;
  public path: string;
  public childPaths: PageInformationModel[] = [];
  public skipingPath: string;
  public relatedInfoItems: MenuItem[];
  public functionItems: MenuItem[];
  public itemsGroup: MenuItem[];
  public buttonConfig = ButtonConfig.SAVE;

  public uiService: UIControllerService;
  public userDataService: UserDataService;
  public relatedInfoCustomItems: MenuItem[];
  public functionCustomItems: MenuItem[];

  public MONTH = FormatConfig.MONTH;
  public YEAR = FormatConfig.YEAR;
  public INTEGER = FormatConfig.INTEGER;
  public INTEGER_POS = FormatConfig.INTEGER_POS;
  public CURRENCY_13_2 = FormatConfig.CURRENCY_13_2;
  public CURRENCY_13_2_POS = FormatConfig.CURRENCY_13_2_POS;
  public CURRENCY_16_5 = FormatConfig.CURRENCY_16_5;
  public CURRENCY_16_5_POS = FormatConfig.CURRENCY_16_5_POS;
  public PERCENT_13_10 = FormatConfig.PERCENT_13_10;
  public PERCENT_13_10_POS = FormatConfig.PERCENT_13_10_POS;
  public PERCENT_5_2 = FormatConfig.PERCENT_5_2;
  public PERCENT_5_2_POS = FormatConfig.PERCENT_5_2_POS;
  public PERCENT_7_4 = FormatConfig.PERCENT_7_4;
  public PERCENT_7_4_POS = FormatConfig.PERCENT_7_4_POS;
  public PERCENT_10_7 = FormatConfig.PERCENT_10_7;
  public PERCENT_10_7_POS = FormatConfig.PERCENT_10_7_POS;
  public ID_INPUTTEXT = InputTextConfig.ID_INPUTTEXT;
  public DEFAULT_INPUTTEXT = InputTextConfig.DEFAULT_INPUTTEXT;
  public TAX_ID_INPUTTEXT = InputTextConfig.TAX_ID_INPUTTEXT;
  public DEFAULT_DROPDOWN = DropdownConfig.DEFAULT_DROPDOWN;
  public DEFAULT_MULTISELECT = MultiSelectConfig.DEFAULT_MULTISELECT;
  public DATETIME = CalendarConfig.DATETIME;
  public DATE = CalendarConfig.DATE;
  public DATERANGE = CalendarConfig.DATERANGE;
  public TIME = CalendarConfig.TIME;
  public DEFAULT_INPUTSWITCH = InputSwitchConfig.DEFAULT_INPUTSWITCH;
  public SAVE = ButtonConfig.SAVE;
  public SAVE_AND_CLOSE = ButtonConfig.SAVE_AND_CLOSE;
  public CLOSE = ButtonConfig.CLOSE;
  public DEFAULT_TEXTAREA = TextareaConfig.DEFAULT_TEXTAREA;
  public DEFAULT_BATCHFORM = BatchFormConfig.DEFAULT;

  private dataGateway: DataGatewayService;
  public baseDropdown: BaseDropdownComponent;
  public refType: number;
  public refGuid: string;
  public passingModel: any;

  fields: FieldAccessing[] = [];
  //#endregion
  userAccessLevelParm: UserAccessLevelParm;
  baseService: BaseServiceModel<any>;
  parentId: string;
  isRelatedMode = false;
  isFunctionMode = false;
  redirectPath: string;

  itemPageMode = [ROUTE_RELATED_GEN.AGREEMENT_TABLE_INFO, ROUTE_RELATED_GEN.COMPANY_BRANCH];
  public workflowItems: MenuItem[];
  constructor() {
    this.translate = AppInjector.get(TranslateService);
    this.dropdownService = AppInjector.get(DropDownService);
    this.router = AppInjector.get(Router);
    this.uiService = AppInjector.get(UIControllerService);
    this.userDataService = AppInjector.get(UserDataService);
    this.notificationService = AppInjector.get(NotificationService);
    this.accessService = AppInjector.get(AccessRightService);
    this.dataGateway = AppInjector.get(DataGatewayService);
    this.baseDropdown = AppInjector.get(BaseDropdownComponent);
    this.validateService = AppInjector.get(ValidationService);
    this.fileService = AppInjector.get(FileService);
    this.k2Service = AppInjector.get(K2Service);
    this.baseService = {
      translate: this.translate,
      dropdownService: this.dropdownService,
      router: this.router,
      uiService: this.uiService,
      userDataService: this.userDataService,
      notificationService: this.notificationService,
      accessService: this.accessService,
      dataGateway: this.dataGateway,
      baseDropdown: this.baseDropdown,
      validateService: this.validateService,
      fileService: this.fileService,
      k2Service: this.k2Service
    };
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.setRelatedInfoOptions();
      this.setFunctionOptions();
      this.userDataService.languageChangeSubject.next(true);
    });
    this.accessService.getUserParentChildBU().subscribe((result) => {
      this.userAccessLevelParm = new UserAccessLevelParm();
      this.userAccessLevelParm.parentChildBU = result;
      this.userAccessLevelParm.owner = this.userDataService.getUsernameFromToken();
      this.userAccessLevelParm.businessUnitGUID = this.accessService.getUserBusinessUnitGUID();
    });
    this.uiService = AppInjector.get(UIControllerService);
    if (this.uiService.finishSpinnerItemSubject.observers.length > 1) {
      this.uiService.finishSpinnerItemSubject.observers.splice(1, this.uiService.finishSpinnerItemSubject.observers.length - 1);
    }
    this.isRelatedMode = this.router.url.indexOf('relatedinfo') > -1;
    this.isFunctionMode = !isNullOrUndefined(this.uiService.skipedPath);
  }
  setRelatedInfoOptions(model?: any): void {}
  setFunctionOptions(model?: any): void {}
  setRoutingGateway(param: PathParamModel, removeSN: boolean = false): void {
    setRoutingGateway(param, this.path, this.itemPageMode, this.redirectPath, removeSN);
    this.redirectPath = null;
  }
  onBack(removeSerialNumber: boolean = false): void {
    const param: PathParamModel = { action: ACTION_PATH.GOBACK };
    this.setRoutingGateway(param, removeSerialNumber);
  }
  setPath(param: PageInformationModel): void {
    this.path = param.pagePath;
    this.childPaths = param.childPaths;
    this.skipingPath = param.skipingPath;
    this.baseDropdown.setDropdownServicePath(param);
    this.k2Service.setPath(param);
  }
  public getPassingObject(route?: ActivatedRoute): any {
    if (this.isFunctionMode) {
      const obj = Object.assign({}, this.uiService.functionObject);
      this.uiService.functionObject = null;
      return obj;
    } else {
      if (!isNullOrUndefined(route) && !isNullOrUndefined(route.snapshot.queryParams)) {
        const splitedPath = Object.keys(route.snapshot.queryParams);
        const obj = route.snapshot.queryParams[splitedPath[splitedPath.length - 1]];
        return isNullOrUndefined(obj) || splitedPath[splitedPath.length - 1] === 'SN' ? null : JSON.parse(deCodePassingObj(obj));
      } else {
        return null;
      }
    }
  }
  setBaseFieldAccessing(fields: FieldAccessing[]): void {
    if (isNullOrUndefined(fields)) {
      fields = [];
    }
    // fields.push(this.getSystemFieldReadOnly());
    if (!isNullOrUndefined(fields)) {
      this.uiService.setUIFieldsAccessing(fields);
    }
    setFormReady(getPanelContent(), true);
  }
  setVisibilityFieldSet(fields: FieldAccessing[]): void {
    fields.forEach((field) => {
      field.filedIds.forEach((item) => {
        const selector = document.getElementById(item);
        if (!isNullOrUndefined(selector)) {
          switchClass(selector, 'display-none', field.invisible);
        }
      });
    });
  }
  baseModelGetDirty(model: any): string[] {
    return modelGetDirty(model);
  }
  setRelatedinfoOptionsMapping(): void {
    if (isNullOrUndefined(this.relatedInfoItems)) {
      this.relatedInfoItems = [];
    } else {
      this.relatedInfoItems.forEach((item) => {
        item.label = this.translate.instant(item.label);
        if (!isNullOrUndefined(item.items)) {
          item.items.forEach((sub) => {
            sub.label = this.translate.instant(sub.label);
          });
        }
      });
    }
    if (isNullOrUndefined(this.relatedInfoCustomItems)) {
      this.relatedInfoCustomItems = [];
    } else {
      this.relatedInfoCustomItems.forEach((item) => {
        item.label = this.translate.instant(item.label);
        item.fragment = 'custom';
        if (!isNullOrUndefined(item.items)) {
          item.items.forEach((sub) => {
            sub.label = this.translate.instant(sub.label);
            sub.fragment = 'custom';
          });
        }
      });
    }
    if (this.relatedInfoItems.length === 0) {
      this.relatedInfoCustomItems.forEach((customItem) => {
        this.relatedInfoItems.push(this.setRelatedinfoCustomCommand(customItem));
      });
    } else {
      this.relatedInfoCustomItems.forEach((customItem) => {
        const index = this.relatedInfoItems.findIndex((f) => f.label === customItem.label);
        if (index > -1) {
          this.relatedInfoItems[index] = this.setRelatedinfoCustomCommand(customItem);
        } else {
          this.relatedInfoItems.push(this.setRelatedinfoCustomCommand(customItem));
        }
      });
    }
    this.relatedInfoItems.forEach((item) => {
      if (!isNullOrUndefined(item.items)) {
        for (let i = 0; i < item.items.length; i++) {
          item.items[i] = this.setRelatedinfoCustomCommand(item.items[i]);
        }
      }
    });

    this.relatedInfoItems.forEach((item) => {
      if (isNullOrUndefined(item['tabindex'])) {
        item['tabindex'] = '999';
      }
    });
    this.relatedInfoItems.sort(this.compare);
    this.setMenuVisibilty();
  }
  setFunctionOptionsMapping(enableFunctionsWFMode: MenuItem[] = null): void {
    if (isNullOrUndefined(this.functionItems)) {
      this.functionItems = [];
    } else {
      this.functionItems.forEach((item) => {
        item.label = this.translate.instant(item.label);
        item.fragment = 'custom';
        if (!isNullOrUndefined(item.items)) {
          item.items.forEach((sub) => {
            sub.label = this.translate.instant(sub.label);
            sub.fragment = 'custom';
          });
        }
      });
    }
    if (isNullOrUndefined(this.functionCustomItems)) {
      this.functionCustomItems = [];
    } else {
      this.functionCustomItems.forEach((item) => {
        item.label = this.translate.instant(item.label);
        if (!isNullOrUndefined(item.items)) {
          item.items.forEach((sub) => {
            sub.label = this.translate.instant(sub.label);
          });
        }
      });
    }
    if (this.functionItems.length === 0) {
      this.functionCustomItems.forEach((customItem) => {
        this.functionItems.push(this.setFunctionCustomCommand(customItem));
      });
    } else {
      this.functionCustomItems.forEach((customItem) => {
        const index = this.functionItems.findIndex((f) => f.label === customItem.label);
        if (index > -1) {
          this.functionItems[index] = this.setFunctionCustomCommand(customItem);
        } else {
          this.functionItems.push(this.setFunctionCustomCommand(customItem));
        }
      });
    }
    this.functionItems.forEach((item) => {
      if (!isNullOrUndefined(item.items)) {
        for (let i = 0; i < item.items.length; i++) {
          item.items[i] = this.setFunctionCustomCommand(item.items[i]);
        }
      }
    });

    // case workflow
    const isWorkflowMode = isWorkFlowMode();
    const hasOverrides = !isUndefinedOrZeroLength(enableFunctionsWFMode);
    if (isWorkflowMode) {
      // => set all visible false if no overrides, else set all disabled
      this.functionItems.forEach((item) => {
        if (hasOverrides) {
          item.disabled = true;
        } else {
          item.visible = false;
        }
        if (!isUndefinedOrZeroLength(item.items)) {
          item.items.forEach((it) => {
            if (hasOverrides) {
              it.disabled = true;
            } else {
              it.visible = false;
            }
          });
        }
      });
      // set disable by overrides
      if (hasOverrides) {
        enableFunctionsWFMode.forEach((override) => {
          override.label = this.translate.instant(override.label);
          const index = this.functionItems.findIndex((f) => f.label === override.label);
          if (index > -1) {
            this.functionItems[index].disabled = override.disabled;
            // sub-level
            if (!isUndefinedOrZeroLength(override.items) && !isUndefinedOrZeroLength(this.functionItems[index].items)) {
              override.items.forEach((ovit) => {
                ovit.label = this.translate.instant(ovit.label);
                const itemsIndex = this.functionItems[index].items.findIndex((f) => f.label === ovit.label);
                if (itemsIndex > -1) {
                  this.functionItems[index].items[itemsIndex].disabled = ovit.disabled;
                }
              });
            }
          }
        });
      }
    }
    this.functionItems.forEach((item) => {
      if (isNullOrUndefined(item['tabindex'])) {
        item['tabindex'] = '999';
      }
    });
    this.functionItems.forEach((item) => {
      if (!isNullOrUndefined(item.items)) {
        item.disabled = this.isAllVisibleFalse(item.items);
      }
    });
    this.functionItems.sort(this.compare);
    this.setMenuVisibilty();
  }
  private setRelatedinfoCustomCommand(custom: MenuItem): MenuItem {
    const keys = Object.keys(custom);
    const item: MenuItem = {};
    keys.forEach((key) => {
      item[key] = custom[key];
    });

    if (!isNullOrUndefined(custom.command)) {
      if (item.fragment === 'custom') {
        item.command = () => this.toRelatedInfo(custom.command() as any);
      }
    }
    return item;
  }
  private setFunctionCustomCommand(custom: MenuItem): MenuItem {
    const keys = Object.keys(custom);
    const item: MenuItem = {};
    keys.forEach((key) => {
      item[key] = custom[key];
    });
    if (!isNullOrUndefined(custom.command)) {
      if (item.fragment === 'custom') {
        item.command = () => this.toRelatedInfo(custom.command() as any);
      }
    }
    return item;
  }
  setMenuVisibilty(): void {
    const relatedBtn = document.querySelector('#MENURELATED');
    const functionBtn = document.querySelector('#MENUFUNCTION');
    const groupBtn = document.querySelector('#MENUGROUP');
    const workflowBtn = document.querySelector('#MENUWORKFLOW');

    if (!isNullOrUndefined(relatedBtn) && !isNullOrUndefined(relatedBtn.nextSibling)) {
      const allVisibleFalseRelated: boolean = this.isAllVisibleFalse(this.relatedInfoItems);
      switchClass(
        relatedBtn.nextSibling,
        'disabled',
        isNullOrUndefined(this.relatedInfoItems) || this.relatedInfoItems.length === 0 || allVisibleFalseRelated
      );
    }
    if (!isNullOrUndefined(functionBtn) && !isNullOrUndefined(functionBtn.nextSibling)) {
      const allVisibleFalseFn: boolean = this.isAllVisibleFalse(this.functionItems);
      switchClass(functionBtn.nextSibling, 'disabled', isNullOrUndefined(this.functionItems) || this.functionItems.length === 0 || allVisibleFalseFn);
    }
    if (!isNullOrUndefined(groupBtn) && !isNullOrUndefined(groupBtn.nextSibling)) {
      switchClass(groupBtn.nextSibling, 'disabled', isNullOrUndefined(this.itemsGroup) || isNullOrUndefined(this.itemsGroup));
    }
    if (!isNullOrUndefined(workflowBtn) && !isNullOrUndefined(workflowBtn.nextSibling)) {
      switchClass(workflowBtn.nextSibling, 'disabled', !isInterfacedWithK2() || isUndefinedOrZeroLength(this.workflowItems));
    }
  }
  toRelatedInfo(param: PathParamModel): void {}
  toFunction(param: PathParamModel): void {
    param.action = ACTION_PATH.TOLISTFUN;
    this.setRoutingGateway(param);
  }
  private compare(a: MenuItem, b: MenuItem) {
    if (Number(a.tabindex) < Number(b.tabindex)) {
      return -1;
    }
    if (Number(a.tabindex) > Number(b.tabindex)) {
      return 1;
    }
    return 0;
  }
  setDropdownOptionOnFocus(event: IvzDropdownOnFocusModel, model: any, $loadDropdown: Observable<SelectItems[]>): Observable<SelectItems[]> {
    if (event.readonly === true || event.isLoadedOnFirstFocus === true) {
      return of(event.options);
    }
    if (event.isLoadedOnFirstFocus === false) {
      const guidStamp = model.guidStamp;
      // update mode
      if (!isUndefinedOrZeroLength(guidStamp)) {
        const origModel = getModelRegistered(guidStamp);
        const key = replaceAll(event.inputId, '_', '').toUpperCase();
        if (!isUndefinedOrZeroLength(origModel)) {
          const modelKeys = Object.keys(origModel);
          const index = modelKeys.findIndex((f) => f.toUpperCase() === key);
          if (index > -1 && !isUndefinedOrZeroLength(event.options) && event.options.length === 1) {
            const fieldName = modelKeys[index];
            if (event.options[0].value === origModel[fieldName]) {
              event.isLoadedOnFirstFocus = true;
              this.uiService.ddlLoadFirstFocusSubject.next([event]);
              return $loadDropdown;
            }
          }
        }
      }
    }

    return of(event.options);
  }
  resetDropdownIsLoadFirstFocus(model: any): void {
    const fieldsToReset: IvzDropdownOnFocusModel[] = [];
    if (!isUndefinedOrZeroLength(model)) {
      const modelKeys = Object.keys(model);
      for (let i = 0; i < modelKeys.length; i++) {
        const ddLoadFocus: IvzDropdownOnFocusModel = {
          inputId: modelKeys[i],
          isLoadedOnFirstFocus: false
        };
        fieldsToReset.push(ddLoadFocus);
      }
      this.uiService.ddlLoadFirstFocusSubject.next(fieldsToReset);
    }
  }
  //#region K2
  isWorkFlowMode(): boolean {
    return isWorkFlowMode();
  }
  //#endregion
  private isAllVisibleFalse(options: MenuItem[]): boolean {
    if (isUndefinedOrZeroLength(options)) {
      return true;
    } else {
      for (let i = 0; i < options.length; i++) {
        if (options[i].visible !== false) {
          return false;
        }
      }
      return true;
    }
  }
  private isAllDisableTrue(options: MenuItem[]): boolean {
    if (isUndefinedOrZeroLength(options)) {
      return true;
    } else {
      for (let i = 0; i < options.length; i++) {
        if (options[i].visible !== false && options[i].disabled !== true) {
          return false;
        }
      }
      return true;
    }
  }
  setBatchParamDefaultData(param: any): void {
    this.dataGateway.setBatchParamDefaultData(param);
  }
}
