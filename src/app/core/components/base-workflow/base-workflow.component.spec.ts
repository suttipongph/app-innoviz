import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseWorkflowComponent } from './base-workflow.component';

describe('BaseWorkflowComponent', () => {
  let component: BaseWorkflowComponent;
  let fixture: ComponentFixture<BaseWorkflowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BaseWorkflowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseWorkflowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
