import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { WORKFLOW } from 'shared/constants';
import { getQueryStringValue, isUndefinedOrZeroLength } from 'shared/functions/value.function';
import { NotificationResponse, PageInformationModel, SelectItems, TranslateModel } from 'shared/models/systemModel';
import { CommentView, DataField, TaskItemView, WorkflowInstanceView } from 'shared/models/viewModel';
import { BaseItemComponent } from '../base-item/base-item.component';

@Component({
  selector: 'app-base-workflow',
  template: `<p>base workflow works!</p> `
})
export class BaseWorkflowComponent<T> extends BaseItemComponent<T> {
  parmDocGUID: string;
  parmCompanyGUID: string;
  comment: string;
  actionOptions: SelectItems[];
  workflowName: string;
  workflow: WorkflowInstanceView = new WorkflowInstanceView();
  parmWorkflow: any = {};
  taskItemView: TaskItemView;
  constructor() {
    super();
    this.parmCompanyGUID = this.baseService.userDataService.getCurrentCompanyGUID();
    this.workflow.impersonateUser = this.baseService.userDataService.getUsernameFromToken();
    const sn = getQueryStringValue(WORKFLOW.SN, this.router.url);
    this.workflow.serialNumber = !isUndefinedOrZeroLength(sn) ? sn[sn.length - 1] : null;
  }

  ngOnInit(): void {}
  workflowGetTaskBySerialNumber(): Observable<TaskItemView> {
    if (isUndefinedOrZeroLength(this.workflow.serialNumber)) {
      const sn = getQueryStringValue(WORKFLOW.SN, this.router.url);
      this.workflow.serialNumber = !isUndefinedOrZeroLength(sn) ? sn[sn.length - 1] : null;
    }
    return this.k2Service.getTaskBySerialNumber(this.workflow).pipe(
      tap((result) => {
        this.taskItemView = result;
        this.actionOptions = this.k2Service.getBatchableActionToDropDownItem(result.actions);
      }),
      catchError((error) => {
        throw error;
      })
    );
  }
  setStartWorkflowParm(param: any): WorkflowInstanceView {
    this.parmDocGUID = !isUndefinedOrZeroLength(param['parmDocGUID']) ? param['parmDocGUID'] : param['ParmDocGUID'];
    param[WORKFLOW.PARM_COMPANY] = this.parmCompanyGUID;
    this.workflow.impersonateUser = this.baseService.userDataService.getUsernameFromToken();
    this.workflow.name = this.workflowName;
    this.workflow.dataFields = this.buildDataFieldList(param);
    return this.workflow;
  }
  startWorkflow(param: any): void {
    this.workflow = this.setStartWorkflowParm(param);
    this.baseService.k2Service
      .startWorkflow(this.workflow)
      .pipe(
        catchError((error) => {
          throw error;
        }),
        tap((result) => {
          super.onBack();
        }),
        catchError((error) => {
          throw error;
        })
      )
      .subscribe();
  }
  setActionWorkflowParm(param: any): WorkflowInstanceView {
    this.parmDocGUID = !isUndefinedOrZeroLength(param['parmDocGUID']) ? param['parmDocGUID'] : param['ParmDocGUID'];
    param[WORKFLOW.PARM_COMPANY] = this.parmCompanyGUID;
    this.workflow.impersonateUser = this.baseService.userDataService.getUsernameFromToken();
    this.workflow.name = this.workflowName;
    this.workflow.dataFields = this.buildDataFieldList(param);
    this.workflow.comments = this.getComments(this.comment);
    this.workflow.actInstDestName = this.taskItemView.actInstDestName;
    this.workflow.actInstDestDisplayName = this.taskItemView.actInstDestDisplayName;
    this.workflow.allocatedUser = this.taskItemView.allocatedUser;
    return this.workflow;
  }
  actionWorkflow(param: any): void {
    this.workflow = this.setActionWorkflowParm(param);
    this.baseService.k2Service
      .actionWorkflow(this.workflow)
      .pipe(
        catchError((error) => {
          throw error;
        }),
        tap((result) => {
          // remove SN query string
          super.onBack(this.isWorkflowActionSuccess(result));
        }),
        catchError((error) => {
          throw error;
        })
      )
      .subscribe();
  }

  buildDataFieldList(param: any): DataField[] {
    let result: DataField[] = [];
    if (!isUndefinedOrZeroLength(param)) {
      let keys = Object.keys(param);
      if (!isUndefinedOrZeroLength(keys)) {
        for (let i = 0; i < keys.length; i++) {
          if (!isUndefinedOrZeroLength(keys[i])) {
            let dataField: DataField = new DataField();
            dataField.name = keys[i];
            dataField.value = param[keys[i]];
            result.push(dataField);
          }
        }
      }
    }

    return result;
  }
  getComments(comment: string): CommentView[] {
    let result: CommentView[] = [];
    let commentObj: CommentView = new CommentView();
    commentObj.message = comment;
    result.push(commentObj);
    return result;
  }
}
