import { Component, Input } from '@angular/core';
import { BaseComponent } from '../base/base.component';
import { MenuItem } from 'shared/models/primeModel';
import {
  BaseOption,
  DataServiceModel,
  FieldAccessing,
  FormValidationModel,
  NotificationResponse,
  OptionModel,
  Paginator,
  PathParamModel,
  ResponseModel,
  SearchParameter,
  SelectItems,
  TranslateModel
} from 'shared/models/systemModel';
import { ButtonConfig, CalendarConfig, DropdownConfig, FormatConfig, InputSwitchConfig, InputTextConfig } from 'shared/config/format.config';
import { ACTION_PATH, Ordinal, ROUTE_MASTER_GEN, ROUTE_WORKFLOW, WORKFLOW } from 'shared/constants';
import { AccessLevelModel, AccessModeView, TaskItemView, WorkflowInstanceView } from 'shared/models/viewModel';
import { AccessLevel } from 'shared/constants';
import { Observable, of } from 'rxjs';
import { focusInValidated, modelGetDirty, modelRegister } from 'shared/functions/model.function';
import { DataGatewayService } from '../../services/data-gateway.service';
import { AppInjector } from 'app-injector';
import { getQueryStringValue, isNullOrUndefined, isUndefinedOrZeroLength, switchAttribute, switchClass } from 'shared/functions/value.function';
import { isInterfacedWithK2 } from 'shared/config/globalvar.config';
import { catchError, finalize, tap } from 'rxjs/operators';

@Component({
  selector: 'app-base-item',
  template: ` <p>base-item works!</p> `,
  styleUrls: ['./base-item.component.scss']
})
export class BaseItemComponent<T> extends BaseComponent {
  public baseOption: BaseOption = {};
  intervalChecking: boolean;
  model: T = {} as T;
  id: string;
  pathName: string;
  isView: boolean = false;
  pageCustomOptions: OptionModel[];
  fileDirty: boolean = false;

  // K2 workflow
  public isViewWorkflowBtn: boolean;
  public ordinal = Ordinal;
  public isUpdateMode: boolean;
  @Input() set gridOptions(param: OptionModel[]) {
    this.pageCustomOptions = param;
  }
  @Input() set relatedInfoOption(param: MenuItem[]) {
    this.relatedInfoCustomItems = param;
  }
  @Input() set functionOption(param: MenuItem[]) {
    this.functionCustomItems = param;
  }
  constructor() {
    super();
  }
  getGroups(): void {
    this.uiService.getGroupOptions().subscribe((data) => {
      this.itemsGroup = data;
      super.setMenuVisibilty();
    });
  }

  checkPageMode(): void {}
  getCanCreate(): boolean {
    return this.accessRight.create !== AccessLevel.None;
  }
  getCanUpdate(): boolean {
    return this.accessRight.update !== AccessLevel.None;
  }
  getCanAction(): boolean {
    return this.accessRight.action !== AccessLevel.None;
  }
  // private getSystemFieldReadOnly(): FieldAccessing[] {
  //   const fields: FieldAccessing[] = [];
  //   const systemGroup = ['CREATED_BY', 'CREATED_DATE_TIME', 'MODIFIED_BY', 'MODIFIED_DATE_TIME', 'COMPANY_ID'];
  //   fields.push({ filedIds: systemGroup, readonly: true });
  //   return fields;
  // }
  checkAccessMode($getAccessRight: Observable<AccessLevelModel>): void {
    // setTimeout(() => {
    // super.setBaseFieldAccessing(this.getSystemFieldReadOnly());
    // }, 2000);

    if (isNullOrUndefined(this.accessRight)) {
      $getAccessRight.subscribe((result) => {
        this.accessRight = result;
      });
    }
  }
  toRelatedInfo(param: PathParamModel): void {
    const conflicts = modelGetDirty(this.model);
    param.action = ACTION_PATH.TOREL;
    if (conflicts.length === 0 && !this.fileDirty) {
      this.setRoutingGateway(param);
      // this.uiService.getRelatedMode2(`${param.path}`);
      // this.toNav(param);
    } else {
      const formValid = this.getFormValidation();
      if (formValid.invalidId.length === 0) {
        this.notificationService.showConfirmRedirect();
        this.notificationService.isAccept.subscribe((isConfirm) => {
          if (isConfirm) {
            this.setRoutingGateway(param);
          }
        });
      } else {
        focusInValidated(formValid.invalidId);
      }
    }
  }
  toFunction(param: PathParamModel, isWorkflow: boolean = false): void {
    const conflicts = modelGetDirty(this.model);
    param.action = isWorkflow ? ACTION_PATH.TOWORKFLOW : !isUndefinedOrZeroLength(param.action) ? ACTION_PATH.TOITEMREPORT : ACTION_PATH.TOITEMFUN;
    if (conflicts.length === 0 && !this.fileDirty) {
      if (!isNullOrUndefined(param.parameters)) {
        this.uiService.functionObject = param.parameters;
        param.parameters = null;
      }
      this.setRoutingGateway(param);
    } else {
      const formValid = this.getFormValidation();
      if (formValid.invalidId.length === 0) {
        this.notificationService.showConfirmRedirect();
        this.notificationService.isAccept.subscribe((isConfirm) => {
          if (isConfirm) {
            if (!isNullOrUndefined(param.parameters)) {
              this.uiService.functionObject = param.parameters;
              param.parameters = null;
            }
            this.setRoutingGateway(param);
          }
        });
      } else {
        focusInValidated(formValid.invalidId);
      }
    }
  }
  toReport(param: PathParamModel): void {
    param.action = ACTION_PATH.TOITEMREPORT;
    this.toFunction(param);
  }
  toList(): void {
    const param: PathParamModel = { action: ACTION_PATH.TOLIST };
    if (!isNullOrUndefined(this.redirectPath)) {
      param.path = this.redirectPath;
    }
    if (this.isUpdateMode) {
      const conflicts = modelGetDirty(this.model);
      if (conflicts.length === 0 && !this.fileDirty) {
        this.setRoutingGateway(param);
      } else {
        // const formValid = this.getFormValidation();
        // if (formValid.invalidId.length === 0) {
        this.notificationService.showConfirmRedirect();
        this.notificationService.isAccept.subscribe((isConfirm) => {
          if (isConfirm) {
            this.setRoutingGateway(param);
          }
        });
        // }
      }
    } else {
      this.setRoutingGateway(param);
    }
  }
  onClose(): void {
    this.toList();
  }
  onCreate($create: Observable<ResponseModel>, isClose: boolean): void {
    $create.subscribe(
      (result) => {
        this.id = result.key;
        result.model.guidStamp = this.model['guidStamp'];
        this.model = result.model;
        modelRegister(this.model);
        this.resetDropdownIsLoadFirstFocus(this.model);
        this.notificationService.showSaveSuccess();
        isClose ? this.toList() : this.toReload();
      },
      (error) => {
        // this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }

  onUpdate($update: Observable<ResponseModel>, isClose: boolean): void {
    $update.subscribe(
      (result) => {
        this.id = result.key;
        result.model.guidStamp = this.model['guidStamp'];
        this.model = result.model;
        modelRegister(this.model);
        this.resetDropdownIsLoadFirstFocus(this.model);
        this.notificationService.showSaveSuccess();
        isClose ? this.toList() : this.toReload();
      },
      (error) => {
        // this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  onExecuteFunction($function: Observable<any>) {
    $function.subscribe(
      (result) => {
        this.onBack(this.isWorkflowActionSuccess(result));
      },
      (error) => {
        // this.notificationService.showErrorMessageFromResponse(error);
      }
    );
  }
  toReload(): void {
    if (this.isUpdateMode) {
      this.checkPageMode();
    } else {
      const param: PathParamModel = { action: ACTION_PATH.RELOAD, pageId: this.id };
      this.setRoutingGateway(param);
      this.checkPageMode();
    }
  }

  setDefaultValueSystemFields(): void {
    // setTimeout(() => {
    if (isUndefinedOrZeroLength(this.model['companyGUID'])) {
      this.model['companyGUID'] = this.userDataService.getCurrentCompanyGUID();
    }
    if (isUndefinedOrZeroLength(this.model['companyId'])) {
      this.model['companyId'] = this.userDataService.getCurrentCompanyId();
    }
    if (isUndefinedOrZeroLength(this.model['branchGUID'])) {
      this.model['branchGUID'] = this.userDataService.getCurrentBranchGUID();
    }
    if (isUndefinedOrZeroLength(this.model['branchId'])) {
      this.model['branchId'] = this.userDataService.getCurrentBranchId();
    }
    if (isUndefinedOrZeroLength(this.model['owner'])) {
      this.model['owner'] = this.userDataService.getUsernameFromToken();
    }
    if (isUndefinedOrZeroLength(this.model['ownerBusinessUnitGUID'])) {
      this.model['ownerBusinessUnitGUID'] = this.accessService.getUserBusinessUnitGUID();
    }
    if (isUndefinedOrZeroLength(this.model['ownerBusinessUnitId'])) {
      this.model['ownerBusinessUnitId'] = this.accessService.getUserBusinessUnitId();
    }

    // }, 0);
  }
  getFormValidation(): FormValidationModel {
    this.uiService.setIncreaseReqCounter('formvalidate');
    this.validateService.clearInvalidFiled();
    this.validateService.$formValidate.emit({ isValid: true });
    this.uiService.setDecreaseReqCounter('formvalidate');
    return {
      isValid: this.validateService.invalidFields.length === 0,
      invalidId: this.validateService.invalidFields
    };
  }

  //#region K2
  setK2WorkflowOption(model: any, workflowActionPathParam: PathParamModel, disableAction: boolean = false) {
    if (isInterfacedWithK2()) {
      this.isViewWorkflowBtn = true;
      this.setMenuWorkflow(model, workflowActionPathParam, disableAction);
    } else {
      this.isViewWorkflowBtn = false;
    }
  }
  private setMenuWorkflow(model: any, workflowActionPathParam: PathParamModel, disableAction: boolean) {
    const serialNumber = getQueryStringValue(WORKFLOW.SN, this.router.url);
    const processInstanceId = model[WORKFLOW.PROCESSINSTANCEID];
    if (isUndefinedOrZeroLength(serialNumber)) {
      if (!isUndefinedOrZeroLength(processInstanceId) && Number(processInstanceId) !== 0) {
        this.setWorkflowItems(model, workflowActionPathParam, serialNumber, false, disableAction);
      } else {
        this.setWorkflowItems(model, workflowActionPathParam, serialNumber, false, disableAction);
      }
    } else {
      let viewflowURL: string = '';
      let errorGetSN: boolean = false;
      let worklistItemId: number = -1;
      this.getTaskBySerialNumber(serialNumber[serialNumber.length - 1])
        .pipe(
          tap((result) => {
            viewflowURL = isUndefinedOrZeroLength(result) ? '' : result.viewflow;
            worklistItemId = result.worklistItemId;
          }),
          catchError((error) => {
            console.error('error get task by serial number', error);
            errorGetSN = true;
            throw error;
          }),
          finalize(() => {
            this.setWorkflowItems(model, workflowActionPathParam, serialNumber, errorGetSN, disableAction, viewflowURL, worklistItemId);
          })
        )
        .subscribe();
    }
  }
  setWorkflowItems(
    model: any,
    workflowActionPathParam: PathParamModel,
    serialNumber: string[],
    errorGetSN: boolean,
    disableAction: boolean,
    viewFlowURL: string = '',
    worklistItemId: number = -1
  ): void {
    const processInstanceId: number = isUndefinedOrZeroLength(model) ? 0 : Number(model[WORKFLOW.PROCESSINSTANCEID]);
    this.workflowItems = [
      {
        label: this.translate.instant('LABEL.ACTION'),
        // command: () => this.baseSetOpenFunction(workflowActionRoute, vwModel, serialNumberParm),
        command: () => this.toFunction(workflowActionPathParam, true),
        disabled: (isUndefinedOrZeroLength(serialNumber) && processInstanceId !== 0) || errorGetSN || disableAction
      },
      {
        label: this.translate.instant('LABEL.VIEW_ACTIVE_FLOW'),
        command: () => window.open(viewFlowURL, '_blank'),
        disabled: isUndefinedOrZeroLength(serialNumber) || isUndefinedOrZeroLength(viewFlowURL)
      },
      {
        label: this.translate.instant('LABEL.ACTION_HISTORY'),
        command: () => this.toFunction({ path: ROUTE_WORKFLOW.ACTIONHISTORY, parameters: workflowActionPathParam.parameters }, true),
        disabled: false
      },
      {
        label: this.translate.instant('LABEL.RELEASE'),
        command: () => this.releaseTaskK2(worklistItemId),
        disabled: isUndefinedOrZeroLength(serialNumber) || errorGetSN
      }
    ];
    this.setMenuVisibilty();
  }

  private getTaskBySerialNumber(serialNumber: string): Observable<TaskItemView> {
    if (isUndefinedOrZeroLength(serialNumber)) {
      return of(null);
    }
    let instance: WorkflowInstanceView = new WorkflowInstanceView();
    instance.impersonateUser = this.userDataService.getUsernameFromToken();
    instance.serialNumber = serialNumber;
    return this.k2Service.getTaskBySerialNumber(instance);
  }
  isWorkflowActionSuccess(result: any): boolean {
    const notification: NotificationResponse = result['notification'];
    if (!isUndefinedOrZeroLength(notification)) {
      const notificationMessage: any[] = notification['notificationMessage'];
      if (!isUndefinedOrZeroLength(notificationMessage)) {
        const keys = Object.keys(notificationMessage);
        if (keys.some((k) => k.toUpperCase() === 'SUCCESS.00034')) {
          return true;
        }
      }
    }
    return false;
  }
  private releaseTaskK2(worklistItemId: number): void {
    const header: TranslateModel = { code: 'CONFIRM.CONFIRM' };
    const message: TranslateModel = { code: 'CONFIRM.90012' };
    this.notificationService.showManualConfirmDialog(header, message);
    this.notificationService.isAccept.subscribe((isConfirm) => {
      if (isConfirm) {
        const instance: WorkflowInstanceView = new WorkflowInstanceView();
        instance.worklistItemId = worklistItemId;
        this.k2Service
          .releaseTask(instance)
          .pipe(
            finalize(() => {
              const param: PathParamModel = { path: ROUTE_MASTER_GEN.HOME, action: ACTION_PATH.MENUTOLIST };
              this.setRoutingGateway(param, true);
            })
          )
          .subscribe();
      }
      this.notificationService.isAccept.observers = [];
    });
  }
  //#endregion
}
