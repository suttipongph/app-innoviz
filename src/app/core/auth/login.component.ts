import { OnInit, Component } from '@angular/core';
import { Router } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { AuthService } from 'core/services/auth.service';
import { UIControllerService } from 'core/services/uiController.service';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'smart-app-login',
  template: ''
})
export class LoginComponent implements OnInit {
  site: string;

  silentRefreshed: boolean = false;
  accessRightLoaded: boolean = false;
  isTokenExpired: boolean = false;
  constructor(private router: Router, private oAuthService: OAuthService, private authService: AuthService, private uiService: UIControllerService) {
    this.authService.isTokenExpired.subscribe((val) => {
      if (val) {
        this.isTokenExpired = val;
      }
    });
    this.oAuthService.events.pipe(filter((e) => ['token_received'].includes(e.type))).subscribe((e) => {
      if (!this.isTokenExpired) {
        this.router.navigate(['/']);
      }
    });
  }
  ngOnInit(): void {
    if (!this.oAuthService.hasValidIdToken() || !this.oAuthService.hasValidAccessToken()) {
      this.uiService.setIncreaseReqCounter('login');
      this.oAuthService
        .loadDiscoveryDocument()
        .finally(() => {
          this.uiService.setDecreaseReqCounter('login');
        })
        .then(() => {
          this.oAuthService.tryLogin();
        })
        .then(() => {
          if (!this.oAuthService.hasValidIdToken() || !this.oAuthService.hasValidAccessToken()) {
            return this.oAuthService
              .silentRefresh()
              .finally(() => {})
              .then(() => {
                this.silentRefreshed = true;
                Promise.resolve();
              })
              .catch((result) => {
                const errorResponsesRequiringUserInteraction = [
                  'interaction_required',
                  'login_required',
                  'account_selection_required',
                  'consent_required'
                ];

                if (result && result.reason && errorResponsesRequiringUserInteraction.indexOf(result.reason.error) >= 0) {
                  // console.warn('User interaction is needed to log in, we will wait for the user to manually log in.');
                  // console.warn('Waiting for user to log in.')
                  // Could also call this.oauthService.initImplicitFlow() here...
                  return Promise.resolve();
                }
                return Promise.reject(result);
              });
          } else {
            return Promise.resolve();
          }
        })
        .then(() => {
          this.authService.isLoggedInSubject.next(true);
        })
        .catch((err) => {
          console.error('error in login component', err);
          this.authService.onInvalidToken();
          this.router.navigate(['/Logout']);
        });
    } else {
      console.log('else 1');
    }
  }
}
