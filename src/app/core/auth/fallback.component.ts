import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { UserDataService } from 'core/services/user-data.service';
import { UIControllerService } from 'core/services/uiController.service';
import { isUndefinedOrZeroLength } from 'shared/functions/value.function';
import { AppConst } from 'shared/constants';
import { AccessRightService } from '../services/access-right.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { Subscription } from 'rxjs';

@Component({
  selector: 'smart-app-fallback',
  template: ''
})
export class FallbackComponent implements OnInit {
  site: string;
  accessRightLoadedSubscription: Subscription;
  siteAndCompanyBranchSelectedSubScription: Subscription;
  isSiteAndCompanyBranchSelected: boolean = false;
  constructor(
    private router: Router,
    private authService: AuthService,
    private userDataService: UserDataService,
    private accessService: AccessRightService,
    private oAuthService: OAuthService,
    private uiService: UIControllerService
  ) {
    this.accessRightLoadedSubscription = this.accessService.accessRightLoadedSubject.subscribe((val) => {
      if (val) {
        if (!this.accessService.isPermittedByAccessRight()) {
          this.router.navigate(['/siteselect'], { skipLocationChange: true });
        }
      }
    });
    this.siteAndCompanyBranchSelectedSubScription = this.accessService.siteAndCompanyBranchSelected.subscribe((val) => {
      if (val) {
        this.isSiteAndCompanyBranchSelected = val;
      }
    });
  }

  ngOnInit(): void {
    if (this.authService.hasToken() && !this.authService.isLoggedIn()) {
      this.uiService.setIncreaseReqCounter('silentRefresh');
      this.oAuthService
        .silentRefresh()
        .finally(() => {
          this.uiService.setDecreaseReqCounter('silentRefresh');
        })
        .then(() => {
          this.authService.isLoggedInSubject.next(true);
          Promise.resolve();
          this.router.navigate(['/']);
        })
        .catch((result) => {
          const errorResponsesRequiringUserInteraction = ['interaction_required', 'login_required', 'account_selection_required', 'consent_required'];

          if (result && result.reason && errorResponsesRequiringUserInteraction.indexOf(result.reason.error) >= 0) {
            // console.warn('User interaction is needed to log in, we will wait for the user to manually log in.');
            // console.warn('Waiting for user to log in.')
            // Could also call this.oauthService.initImplicitFlow() here...
            Promise.resolve();
          }
          Promise.reject();
        });
    } else if (!this.authService.isLoggedIn()) {
      this.authService.login();
    }
    // no site selected/ CASHIER with no selected company
    else if (
      isUndefinedOrZeroLength(this.userDataService.getSiteLogin()) ||
      (this.userDataService.getSiteLogin() === AppConst.CASHIER && isUndefinedOrZeroLength(this.userDataService.getCurrentCompanyGUID()))
    ) {
      this.router.navigate(['/siteselect'], { skipLocationChange: true });
    } else {
      // to fix router navigation
      // upon coming back from login
      let that = this;
      setTimeout(() => {
        that.site = that.userDataService.getSiteLogin();
        if (!isUndefinedOrZeroLength(that.site)) {
          if (that.accessService.isPermittedByAccessRight()) {
            that.accessService.siteAndCompanyBranchSelected.next(true);
            const redirect = sessionStorage.getItem('redirect');
            if (!isUndefinedOrZeroLength(redirect)) {
              that.router.navigate([redirect]);
            } else {
              that.router.navigate([`/${that.site}/home`]);
            }
          } else {
            if (that.isSiteAndCompanyBranchSelected) {
              that.accessService.setErrorCode('ERROR.00808');
              that.router.navigate([`/${that.site}/401`]);
            } else {
              that.router.navigate(['/siteselect'], { skipLocationChange: true });
            }
          }
        } else {
          that.accessService.setErrorCode('ERROR.00809');
          that.router.navigate([`/${that.site}/401`]);
        }
      }, 100);
    }
  }
}
