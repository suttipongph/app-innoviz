import { Component } from '@angular/core';

@Component({
  selector: 'smart-app-not-found',
  template: "<h1>We can't find what you're looking for... (404)</h1>"
})
export class SmartAppNotFoundComponent {}
