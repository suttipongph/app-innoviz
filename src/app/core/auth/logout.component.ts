import { OnInit, Component } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'smart-app-logout',
  template: ''
})
export class LogOutComponent implements OnInit {
  constructor(private authService: AuthService, private router: Router) {}
  ngOnInit(): void {
    if (this.authService.isLoggedIn()) {
      console.log('go to logout');
      this.authService.logOut();
    } else {
      console.log('go to /');

      this.router.navigate(['/']);
    }
  }
}
