import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { AccessRightService } from '../../services/access-right.service';
import { Router } from '@angular/router';
import { UserDataService } from 'core/services/user-data.service';
import { AuthService } from 'core/services/auth.service';
import { isUndefinedOrZeroLength, getQueryParamObject } from 'shared/functions/value.function';
import { AppConst } from 'shared/constants';
import { CompanyBranchSelectorComponent } from 'shared/components/company-branch-selector/company-branch-selector.component';
import { Subscription } from 'rxjs';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { NotificationService } from 'core/services/notification.service';

@Component({
  selector: 'app-site-select',
  templateUrl: './site-select.component.html',
  styleUrls: ['./site-select.component.css'],
  animations: [
    trigger('flip', [
      state(
        'front',
        style({
          transform: 'rotateY(0deg)'
        })
      ),
      state(
        'back',
        style({
          transform: 'rotateY(180deg)'
        })
      ),
      transition('front => back', [
        animate(
          '1s 0s ease-out',
          keyframes([
            style({
              transform: 'perspective(20rem) rotateY(0deg)',
              offset: 0
            }),
            style({
              transform: 'perspective(20rem) scale3d(1.0, 1.0, 1.0) rotateY(80deg)',
              offset: 0.4
            }),
            style({
              transform: 'perspective(20rem) scale3d(1.0, 1.0, 1.0) rotateY(100deg)',
              offset: 0.5
            }),
            style({
              transform: 'perspective(20rem) scale3d(0.95, 0.95, 0.95) rotateY(180deg)',
              offset: 0.8
            }),
            style({
              transform: 'perspective(20rem) rotateY(180deg)',
              offset: 1
            })
          ])
        )
      ]),
      transition('back => front', [
        animate(
          '1s 0s ease-in',
          keyframes([
            style({
              transform: 'perspective(20rem) rotateY(-180deg)',
              offset: 0
            }),
            style({
              transform: 'perspective(20rem) scale3d(1.0, 1.0, 1.0) rotateY(100deg)',
              offset: 0.4
            }),
            style({
              transform: 'perspective(20rem) scale3d(1.0, 1.0, 1.0) rotateY(80deg)',
              offset: 0.5
            }),
            style({
              transform: 'perspective(20rem) scale3d(0.95, 0.95, 0.95) rotateY(0deg)',
              offset: 0.8
            }),
            style({
              transform: 'perspective(20rem) rotateY(0deg)',
              offset: 1
            })
          ])
        )
      ])
    ])
  ]
})
export class SiteSelectComponent implements OnInit, AfterViewInit, OnDestroy {
  isUndefinedOrZeroLength = isUndefinedOrZeroLength;
  AppConst = AppConst;

  loginSite: string;
  siteSelected: boolean = false;

  currentCompanyGUID: string = '';
  visibleCompanyBranchSelector: boolean = false;

  accessRightLoadedSubscription: Subscription;
  isAuthNavigationDoneSubscription: Subscription;
  isAuthNavigationDone: boolean = false;
  @ViewChild('companyBranchSelector') companyBranchSelector: CompanyBranchSelectorComponent;

  flipState = 'front';
  hasUser: boolean = true;
  constructor(
    private accessService: AccessRightService,
    private router: Router,
    private userDataService: UserDataService,
    private authService: AuthService,
    private notificationService: NotificationService
  ) {
    this.isAuthNavigationDoneSubscription = this.authService.isAuthNavigationDone.subscribe((val) => {
      if (val) {
        this.isAuthNavigationDone = val;
      }
    });
    this.accessRightLoadedSubscription = this.accessService.accessRightLoadedSubject.subscribe((data) => {
      this.currentCompanyGUID = this.userDataService.getCurrentCompanyGUID();
      if (data === true) {
        const siteLogin = this.userDataService.getSiteLogin();
        const currentCompany = this.userDataService.getCurrentCompanyGUID();
        const currentBranch = this.userDataService.getCurrentBranchGUID();
        if (isUndefinedOrZeroLength(currentCompany)) {
          this.visibleCompanyBranchSelector = true;
          this.flipState = 'back';
        } else if (!isUndefinedOrZeroLength(currentCompany) && isUndefinedOrZeroLength(currentBranch) && siteLogin !== AppConst.BACK) {
          this.visibleCompanyBranchSelector = true;
          this.flipState = 'back';
        } else {
          // redirect to page before login
          const redirect = sessionStorage.getItem('redirect');
          if (!isUndefinedOrZeroLength(redirect)) {
            if (redirect.includes('siteselect')) {
              this.router.navigate([redirect], { skipLocationChange: true });
            } else if (redirect.includes('/wf:')) {
              this.handleWorkflowRedirect(redirect);
            } else {
              this.handleQueryStringRedirect(redirect);
            }
          } else {
            if (!isUndefinedOrZeroLength(this.userDataService.getSiteLogin())) {
              this.loginSite = this.userDataService.getSiteLogin();
              if (!this.accessService.isPermittedByAccessRight()) {
                this.accessService.setErrorCode('ERROR.00808');
                this.router.navigate([`/${this.loginSite}/401`]);
              } else {
                this.router.navigate([`/${this.loginSite}/home`]);
              }
            } else {
              this.router.navigate([`/Logout`]);
            }
          }
        }
      } else {
        // error occurred loading access rights
        // or not loading default access right
        const siteLogin = this.userDataService.getSiteLogin();
        if (!isUndefinedOrZeroLength(siteLogin)) {
          this.accessService.setErrorCode('ERROR.00810');
          this.router.navigate([`/${this.loginSite}/401`]);
        } else {
          this.router.navigate(['/Logout']);
        }
      }
    });
  }

  ngOnInit(): void {
    const userName = this.userDataService.getUsernameFromToken();
    const userId = this.userDataService.getUserIdFromToken();
    this.accessService.checkExistingUser(userId, userName).subscribe(
      (result) => {
        if (!result) {
          this.accessService.setErrorCode('ERROR.00841', [userName]);
          this.hasUser = false;
        } else {
          this.hasUser = true;
        }
      },
      (err) => {
        this.notificationService.showErrorMessageFromResponse(err);
      }
    );
  }

  ngAfterViewInit(): void {
    if (this.isAuthNavigationDone) {
      this.onSiteSelect(AppConst.BACK);
    }
  }
  onInitCompanyBranchSelector(): void {
    this.companyBranchSelector.setCompanyBranchSelectorData(this.loginSite);
  }
  onSiteSelect(site: string): void {
    this.loginSite = site;
    this.siteSelected = true;
    this.userDataService.setSiteLogin(this.loginSite);

    if (this.loginSite === AppConst.CASHIER) {
      if (this.visibleCompanyBranchSelector) {
        this.companyBranchSelector.setCompanyBranchSelectorData(this.loginSite);
      } else {
        this.visibleCompanyBranchSelector = true;
      }
      this.flipState = 'back';
    } else {
      this.visibleCompanyBranchSelector = false;
      const userId = this.userDataService.getUserIdFromToken();

      const redirect = sessionStorage.getItem('redirect');
      if (!isUndefinedOrZeroLength(redirect) && redirect.includes('/wf:')) {
        const splits = redirect.split('/');
        const companyGUID = splits.find((item) => item.includes('wf:')).split(':')[1];
        this.accessService.loadUserAccessRightAtLogin(userId, this.loginSite, companyGUID).subscribe();
      } else {
        this.accessService.loadUserAccessRightAtLogin(userId, this.loginSite).subscribe();
      }
    }
  }
  onCompanyBranchSelected(oldCompany): void {
    const selectedCompany = this.userDataService.getCurrentCompanyGUID();
    if (oldCompany === selectedCompany) {
      this.accessService.siteAndCompanyBranchSelected.next(true);
      this.router.navigate(['/']);
    }
  }

  isLoggedIn(): boolean {
    return this.authService.hasToken();
  }

  onBackClick(): void {
    this.flipState = 'front';
  }

  ngOnDestroy(): void {
    if (!isUndefinedOrZeroLength(this.accessRightLoadedSubscription)) {
      this.accessRightLoadedSubscription.unsubscribe();
    }
  }

  private handleQueryStringRedirect(redirect: string): void {
    const path_decoded = decodeURIComponent(redirect);

    const splits = path_decoded.split('?');
    if (splits.length > 1) {
      this.router.navigate([splits[0]], {
        queryParams: getQueryParamObject(path_decoded)
      });
    } else {
      this.router.navigate([path_decoded]);
    }
  }
  private handleWorkflowRedirect(redirect: string): void {
    this.handleQueryStringRedirect(redirect);
  }
}
