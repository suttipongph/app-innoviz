import { Component, OnInit } from '@angular/core';
import { AccessRightService } from '../services/access-right.service';
import { isUndefinedOrZeroLength } from 'shared/functions/value.function';
@Component({
  selector: 'smart-app-unauthorized',
  template: `
    <h1>You're not allowed in this section (401).</h1>
    <h3 *ngIf="!isUndefinedOrZeroLength(errorMsg)">{{ errorMsg }}</h3>
  `
})
export class SmartAppUnAuthorizedComponent implements OnInit {
  errorMsg: string = '';
  isUndefinedOrZeroLength = isUndefinedOrZeroLength;
  constructor(private accessService: AccessRightService) {}
  ngOnInit(): void {
    this.errorMsg = this.accessService.getTranslatedUnauthorizedError();
  }
}
