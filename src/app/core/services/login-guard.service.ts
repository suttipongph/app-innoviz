import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthService } from './auth.service';
import { AppConst, WORKFLOW } from 'shared/constants';
import { isUndefinedOrZeroLength, isSiteValid, guidValidation, isNullOrUndefined, getSiteFormParam } from 'shared/functions/value.function';
import { UserDataService } from './user-data.service';
import { AccessRightService } from './access-right.service';
import { finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginGuardService implements CanActivate {
  constructor(
    private authService: AuthService,
    private accessService: AccessRightService,
    private userDataService: UserDataService,
    private router: Router
  ) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (!this.authService.isLoggedIn()) {
      let redirecturl = state.url;
      sessionStorage.setItem('redirect', redirecturl);
      this.authService.initImplicitFlow();
      return false;
    } else {
      let siteParam = next.params['site'];
      let mainParam = next.params['main'];
      // siteParam = getSiteFormParam(siteParam);
      let siteFromRoute = isUndefinedOrZeroLength(siteParam)
        ? isUndefinedOrZeroLength(mainParam)
          ? ''
          : mainParam.split(':')[0] + ':' + mainParam.split(':')[1]
        : siteParam;
      let siteLogin = this.userDataService.getSiteLogin();
      if (!isUndefinedOrZeroLength(siteFromRoute)) {
        // check site value
        if (isSiteValid(siteLogin)) {
          const isWfSite = siteFromRoute.split(':')[0] === WORKFLOW.WF;
          if (isWfSite) {
            this.accessService.siteAndCompanyBranchSelected.next(false);
            this.authService.setDisplaySubject.next(true);
            const companyGUID = siteFromRoute.split(':')[1];

            if (!isUndefinedOrZeroLength(companyGUID) && guidValidation(companyGUID)) {
              return true;
            }
          } else {
            siteFromRoute = siteFromRoute.split(':')[0];
          }
          this.authService.setDisplaySubject.next(true);
          if (isSiteValid(siteFromRoute) && siteFromRoute === siteLogin) {
            return true;
          } else if (siteFromRoute === AppConst.OB && siteFromRoute !== siteLogin) {
            const userId = this.userDataService.getUserIdFromToken();
            this.accessService
              .loadUserAccessRightForOB(userId)
              .pipe(
                finalize(() => {
                  return false;
                })
              )
              .subscribe(
                () => {
                  const newCurrentCompany = this.userDataService.getCurrentCompanyGUID();
                  if (!isUndefinedOrZeroLength(newCurrentCompany)) {
                    this.router.navigate([`/${AppConst.OB}/home`]);
                  } else {
                    console.error('User not mapped to company');
                    this.accessService.setErrorCode('ERROR.00812');
                    this.router.navigate([`/${AppConst.OB}/401`]);
                  }
                },
                (error) => {
                  console.error('failed loading access right', error);
                  this.accessService.setErrorCode('ERROR.00810');
                  this.router.navigate([`/${AppConst.OB}/401`]);
                }
              );
          } else {
            this.router.navigate(['/Logout']);
            return false;
          }
        } else {
          this.router.navigate(['/Logout']);
          return false;
        }
      }
      return true;
    }
  }
}
