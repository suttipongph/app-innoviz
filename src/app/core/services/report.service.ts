import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { FileInformation } from 'shared/models/systemModel';
import { DataGatewayService } from './data-gateway.service';
import { HttpClientService } from './httpclient.service';

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  constructor(private dataGateway: DataGatewayService, private http: HttpClientService) {}
  private getServicePath(param: any): string {
    const result: string = param['servicePath'];
    return !isNullOrUndefined(result) ? result : '';
  }
  renderReport(parm: any): Observable<FileInformation> {
    const url: string = `${this.getServicePath(parm)}/RenderReport`;
    console.log(url);
    const result = this.dataGateway.post(url, parm);
    return result;
  }

  exportReport(parm: any): Observable<FileInformation> {
    const url: string = `${this.getServicePath(parm)}/ExportReport`;
    const result = this.dataGateway.post(url, parm);
    return result;
  }
}
