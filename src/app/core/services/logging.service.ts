import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpRequest } from '@angular/common/http';
import { formatDate } from '@angular/common';
import { Message } from 'primeng/api';
import { Subject } from 'rxjs';
import {
  isUndefinedOrZeroLength,
  encryptObject,
  getErrorToTopic,
  getTranslateMessage,
  getTranslateMessages,
  getErrorToTranslate
} from 'shared/functions/value.function';
import { LoggingModel, SystemLogModel } from 'shared/models/systemModel';
import { getEnvironment, getEncryptionKey, getSystemLogLevel, isEncryptionEnabled } from 'shared/config/globalvar.config';
import { LogType, MessageType, AppConst } from 'shared/constants';
import { UserDataService } from './user-data.service';

@Injectable({
  providedIn: 'root'
})
export class LoggingService {
  unseenNotiCountSubject = new Subject<number>();

  constructor(private userDataService: UserDataService) {
    const logs = sessionStorage.getItem('appLogs');

    // reset logs
    if (!isUndefinedOrZeroLength(logs)) {
      sessionStorage.removeItem('appLogs');
    }
  }

  getAppLogs(): [] {
    const appLogs = sessionStorage.getItem('appLogs');
    if (!isUndefinedOrZeroLength(appLogs)) {
      return JSON.parse(appLogs);
    } else {
      return [];
    }
  }
  setAppLogsToStorage(appLogs: LoggingModel[]): void {
    const item = JSON.stringify(appLogs);
    sessionStorage.setItem('appLogs', item);

    let unseenCount: number = 0;
    const userEnabledSystemLog: boolean = this.userDataService.getShowSystemLog() === AppConst.TRUE;
    if (userEnabledSystemLog) {
      const result = appLogs.filter((item) => item.seen === false);
      unseenCount = isUndefinedOrZeroLength(result) ? 0 : result.length;
    } else {
      const result = appLogs.filter((item) => item.logType === LogType.NORMAL && item.seen === false);
      unseenCount = isUndefinedOrZeroLength(result) ? 0 : result.length;
    }
    this.unseenNotiCountSubject.next(unseenCount);
  }
  addItemToAppLogs(item: LoggingModel): void {
    const appLogs: LoggingModel[] = this.getAppLogs();

    if (!isUndefinedOrZeroLength(appLogs)) {
      appLogs.push(item);
      this.setAppLogsToStorage(appLogs);
    } else {
      const loggedItems = [];
      loggedItems.push(item);
      this.setAppLogsToStorage(loggedItems);
    }
  }

  // from notification service
  createNormalAppLog(item: Message, origin: string, msgArr?: string[]): LoggingModel {
    const result = new LoggingModel();
    result.systemLogDetail = null;

    const now = new Date();
    result.timestamp = formatDate(now, 'yyyy-MM-dd HH:mm:ss', 'EN-US');
    result.topic = item.summary;
    result.message = item.detail;
    result.messages = msgArr;

    result.origin = origin;
    result.logType = LogType.NORMAL;
    result.messageType = item.severity;
    result.seen = false;

    this.addItemToAppLogs(result);

    return result;
  }

  // from http interceptor
  createSystemHttpAppLog(req: HttpRequest<any>, res, started: number, now: number): LoggingModel {
    const result = new LoggingModel();
    result.systemLogDetail = new SystemLogModel();
    // set http result
    result.systemLogDetail.isError = !res.ok;
    result.systemLogDetail.startTime = started;
    result.systemLogDetail.finishTime = now;
    result.systemLogDetail.elapsedTime = now - started;
    result.systemLogDetail.httpMethod = req.method;
    result.systemLogDetail.reqUrl = this.getRequestUrl(req);

    result.systemLogDetail.statusCode = res.status.toString();
    result.systemLogDetail.statusText = res.statusText;

    result.systemLogDetail.requestId = req.headers.get('RequestHeader');

    if (res.ok) {
      result.messageType = MessageType.SUCCESS;
    } else {
      result.messageType = MessageType.ERROR;
    }

    if (res instanceof HttpErrorResponse) {
      if (!isUndefinedOrZeroLength(res.error)) {
        const encSaltIV = res.error.encSaltIV;
        result.systemLogDetail.reqBody = this.getRequestBody(req, encSaltIV);
        result.systemLogDetail.stackTrace = res.error.stackTrace;

        const errorMessages: string[] = [];
        errorMessages.push(getTranslateMessage(getErrorToTopic(res)));
        errorMessages.push(...getTranslateMessages(getErrorToTranslate(res)));

        if (!isUndefinedOrZeroLength(errorMessages)) {
          result.messages = errorMessages;
        }
      }
    } else {
      result.systemLogDetail.reqBody = this.getRequestBody(req);
    }

    result.message = this.getMessageDetail(result);

    result.topic = AppConst.SYSTEM;
    result.origin = AppConst.SYSTEM;
    result.logType = LogType.SYSTEM;
    result.timestamp = formatDate(now, 'yyyy-MM-dd HH:mm:ss', 'EN-US');
    result.seen = false;

    const logLevel = getSystemLogLevel();
    if (logLevel === AppConst.ERROR) {
      if (result.messageType === MessageType.ERROR) {
        this.addItemToAppLogs(result);
      }
    } else if (logLevel === AppConst.ALL) {
      this.addItemToAppLogs(result);
    }

    return result;
  }

  getMessageDetail(item: LoggingModel): string {
    let msg: string = '';
    const sysDetail = item.systemLogDetail;

    msg = `Request responded with status: ${sysDetail.statusCode} (${sysDetail.statusText})`;

    return msg;
  }

  getRequestUrl(req: HttpRequest<any>): string {
    if (!isUndefinedOrZeroLength(req.url)) {
      try {
        const env = getEnvironment();
        const baseUrl = env.baseURL;
        const userUrl = env.userURL;
        if (req.url.includes(baseUrl)) {
          return req.url.replace(baseUrl, '/');
        } else if (req.url.includes(userUrl)) {
          return req.url.replace(userUrl, '/');
        } else {
          return req.url;
        }
      } catch (error) {
        return req.url;
      }
    } else {
      return null;
    }
  }
  getRequestBody(req: HttpRequest<any>, encSaltIV?: string): string {
    if (!isUndefinedOrZeroLength(req.body)) {
      const stringReqBody = JSON.stringify(req.body);
      let reqBody = JSON.parse(stringReqBody);
      reqBody = this.truncateReqBody(reqBody);

      const enabledEncryption = isEncryptionEnabled();
      if (enabledEncryption) {
        const encKey = getEncryptionKey();
        return encryptObject(encKey, JSON.stringify(reqBody), encSaltIV);
      } else {
        return JSON.stringify(reqBody, null, 3);
      }
    }
  }
  // cut request body length to save space
  truncateReqBody(reqBody: any): any {
    if (!isUndefinedOrZeroLength(reqBody)) {
      if (typeof reqBody === 'object') {
        const keys = Object.keys(reqBody);
        // check each field in reqBody to truncate as needed
        if (!isUndefinedOrZeroLength(keys)) {
          for (let i = 0; i < keys.length; i++) {
            let value = reqBody[keys[i]];
            reqBody[keys[i]] = this.truncateReqBody(value);
          }
          return reqBody;
        } else {
          return reqBody;
        }
      } else {
        // truncate
        if (!isUndefinedOrZeroLength(reqBody.length) && reqBody.length > 300) {
          return '(omitted)';
        } else {
          return reqBody;
        }
      }
    } else {
      return reqBody;
    }
  }
}
