import { Injectable } from '@angular/core';
import { HttpClientService } from './httpclient.service';
import { Observable } from 'rxjs';
import { CustomerTableListView } from 'shared/models/viewModel';
import { SearchParameter, SearchResult } from 'shared/models/systemModel';
import { UIControllerService } from './uiController.service';

@Injectable({
  providedIn: 'root'
})
export class SysListViewService {
  constructor(private http: HttpClientService, private uiService: UIControllerService) {}
  // getAgreementTableToList(search: SearchParameter): Observable<SearchResult<AgreementTableView>> {
  //   const url = `SysListView/AgreementTable/${search.branchFilterMode}`;
  //   const result = this.http.Post(url, search);
  //   return result;
  // }
}
