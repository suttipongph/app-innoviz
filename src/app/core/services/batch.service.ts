import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { isNullOrUndefined } from 'shared/functions/value.function';
import { Deletion, PageInformationModel, RowIdentity, SearchParameter } from 'shared/models/systemModel';
import { BatchHistoryView, BatchObjectView } from 'shared/models/viewModel';
import { DataGatewayService } from './data-gateway.service';
import { HttpClientService } from './httpclient.service';
@Injectable({
  providedIn: 'root'
})
export class BatchService {
  servicePath = '';
  setPath(param: PageInformationModel): void {
    this.servicePath = param.servicePath;
  }
  setPathFromPathString(parm: string): void {
    this.servicePath = !isNullOrUndefined(parm) ? parm : '';
  }
  constructor(private dataGateway: DataGatewayService, private http: HttpClientService) {}
  private getServicePath(param: any): string {
    const result: string = param['servicePath'];
    return !isNullOrUndefined(result) ? result : '';
  }
  createScheduledJob(param: BatchObjectView): Observable<boolean> {
    // const url: string = 'BatchObject/create';
    // const url: string = `${this.getServicePath(parm)}/RenderReport`;
    const url: string = `${this.servicePath}/ScheduleBatchJob`;
    return this.dataGateway.post(url, param);
  }
  getBatchViewList(search: SearchParameter) {
    const url: string = `BatchObject/GetBatchView/List`;
    // const url: string = `${this.servicePath}/GetReleaseTask`;
    return this.dataGateway.post(url, search);
  }
  getBatchViewByJobId(jobId: string) {
    const url: string = `BatchObject/Getbatchview/${jobId}`;
    // const url: string = `${this.servicePath}/GetReleaseTask`;
    // return this.dataGateway.post(url, param);
    return this.dataGateway.get(url);
  }
  getBatchInstanceHistoryList(jobId: string, search: SearchParameter) {
    const url: string = `BatchObject/GetBatchHistory/List/${jobId}`;
    // const url: string = `${this.servicePath}/GetReleaseTask`;
    return this.dataGateway.post(url, search);
    // return this.dataGateway.get(url);
  }
  getBatchInstanceHistoryByKey(id: string) {
    const url: string = `BatchObject/Getbatchhistory/${id}`;
    // const url: string = `${this.servicePath}/GetReleaseTask`;
    // return this.dataGateway.post(url, param);
    return this.dataGateway.get(url);
  }
  getBatchInstanceLogList(instanceId: string, search: SearchParameter) {
    const url: string = `BatchObject/GetBatchLog/List/${instanceId}`;
    // const url: string = `${this.servicePath}/GetReleaseTask`;
    return this.dataGateway.post(url, search);
  }
  getBatchInstanceLogByKey(id: number) {
    const url: string = `BatchObject/Getbatchlog/${id}`;
    // const url: string = `${this.servicePath}/GetReleaseTask`;
    // return this.dataGateway.post(url, param);
    return this.dataGateway.get(url);
  }
  deleteBatchJob(row: RowIdentity) {
    const url: string = 'BatchObject/DeleteBatchJob';
    //return this.dataGateway.post(url, model);
    return this.dataGateway.delete(url, row);
  }
  validateRetryFunction(model: BatchHistoryView): Observable<boolean> {
    const url: string = 'BatchObject/ValidateRetryInstanceFunction';
    return this.dataGateway.post(url, model);
  }
}
