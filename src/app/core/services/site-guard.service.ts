import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';
import { UserDataService } from './user-data.service';
import { AuthService } from './auth.service';
import { isSiteValid, isUndefinedOrZeroLength } from 'shared/functions/value.function';
import { setBodyFunctionMode } from 'shared/functions/routing.function';

@Injectable({
  providedIn: 'root'
})
export class SiteGuardService implements CanActivate {
  constructor(private router: Router, private userDataService: UserDataService, private authService: AuthService) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let path = next.routeConfig.path;
    // check url segments
    let urlSegments = next.url;
    if (isUndefinedOrZeroLength(urlSegments)) {
      this.router.navigate(['/Logout']);
      return false;
    } else {
      // at least 1 segment
      // get site value from url segment
      const firstSeg = urlSegments[0].path;
      let siteParam = '';
      if (firstSeg.includes(':')) {
        siteParam = firstSeg.split(':')[0];
      } else {
        siteParam = firstSeg;
      }

      let siteLogin = this.userDataService.getSiteLogin();
      console.log('siteParam', siteParam, isSiteValid(siteParam));
      console.log('siteLogin', siteLogin, isSiteValid(siteLogin));
      setBodyFunctionMode(false);
      return true;
    }
  }
}
