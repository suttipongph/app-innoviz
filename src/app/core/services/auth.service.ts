import { Injectable } from '@angular/core';
import jwt_decode from 'jwt-decode';
import { OAuthService, OAuthStorage } from 'angular-oauth2-oidc';
import { Router } from '@angular/router';
import { Subject, BehaviorSubject } from 'rxjs';
import { isNullOrUndefined, isUndefinedOrZeroLength } from 'shared/functions/value.function';
import { StorageKey } from 'shared/constants';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  token: any;
  tokenPayload: any;

  isLoggedInSubject = new Subject<boolean>();
  setDisplaySubject = new Subject<boolean>();

  isAuthNavigationDoneSubject = new BehaviorSubject<boolean>(false);
  isAuthNavigationDone = this.isAuthNavigationDoneSubject.asObservable();
  isTokenExpiredSubject = new BehaviorSubject<boolean>(false);
  isTokenExpired = this.isTokenExpiredSubject.asObservable();
  constructor(private oAuthService: OAuthService, private oAuthStorage: OAuthStorage, private router: Router) {}

  /*
   * Authentication
   */
  isLoggedIn(): boolean {
    return this.oAuthService.hasValidAccessToken();
  }

  login(): void {
    this.router.navigate(['/Login']);
  }

  logOut(): void {
    if (this.isLoggedIn()) {
      const redirecturl = this.router.routerState.snapshot.url;
      this.onLogOut();
      this.oAuthService.logOut(false);
      this.router.navigate([redirecturl]);
    } else {
      this.router.navigate(['/']);
    }
  }

  initImplicitFlow(): void {
    this.oAuthService.initImplicitFlow();
  }

  onLogOut(): void {
    this.resetStorage();
  }

  hasToken(): boolean {
    const token = this.oAuthService.getAccessToken();
    return !isUndefinedOrZeroLength(token);
  }

  onInvalidToken(): void {
    this.resetStorage();
  }

  resetStorage(): void {
    this.oAuthStorage.removeItem(StorageKey.BRANCHGUID);
    this.oAuthStorage.removeItem(StorageKey.BRANCH_ID);
    this.oAuthStorage.removeItem(StorageKey.COMPANYGUID);
    this.oAuthStorage.removeItem(StorageKey.COMPANY_ID);

    this.oAuthStorage.removeItem(StorageKey.ACCESSRIGHT);

    this.oAuthStorage.removeItem(StorageKey.USER_LANGUAGE);

    localStorage.removeItem(StorageKey.SHOW_LOG);
    localStorage.removeItem(StorageKey.SITE_LOGIN);
    localStorage.removeItem(StorageKey.USER_IMAGE);
    localStorage.removeItem(StorageKey.INACTIVE_USER);
    localStorage.removeItem(StorageKey.USER_NAME);
    localStorage.removeItem(StorageKey.USER_FULLNAME);

    localStorage.removeItem(StorageKey.DROPDOWN_CACHE);

    localStorage.removeItem(StorageKey.BUSINESSUNITGUID);
    localStorage.removeItem(StorageKey.BUSINESSUNIT_ID);
    localStorage.removeItem(StorageKey.PARENTCHILDBU);

    sessionStorage.clear();
  }

  getDisplayName(): string {
    return localStorage.getItem(StorageKey.USER_NAME);
  }
  // total length <= 25 characters
  getValidatedLengthDisplayName(name: string, username: string): string {
    if (isUndefinedOrZeroLength(name)) {
      return username;
    }
    const splitName = name.split(' ');
    if (splitName.length > 2) {
      return username;
    }
    if (name.length > 25) {
      if (splitName[0].length <= 25) {
        return splitName[0];
      }
      return username;
    }
    return name;
  }
}
