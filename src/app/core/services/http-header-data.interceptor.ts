import { Injectable, Optional } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { OAuthStorage, OAuthModuleConfig } from 'angular-oauth2-oidc';
import { Observable } from 'rxjs';

import { AuthService } from './auth.service';
import { UserDataService } from './user-data.service';
import { getEnvironment, isInterfacedWithK2, isInterfacedWithAX } from 'shared/config/globalvar.config';
import { isUndefinedOrZeroLength, Guid, isNullOrUndefined } from 'shared/functions/value.function';
import { UIControllerService } from './uiController.service';

@Injectable()
export class HttpHeaderDataInterceptor implements HttpInterceptor {
  constructor(
    private userDataService: UserDataService,
    private oAuthStorage: OAuthStorage,
    private authService: AuthService,
    private uiService: UIControllerService,
    @Optional() private moduleConfig: OAuthModuleConfig
  ) {}

  private checkUrl(url: string): boolean {
    const found = this.moduleConfig.resourceServer.allowedUrls.find((u) => url.startsWith(u));
    return !!found;
  }

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.uiService.setIncreaseReqCounter(req.url);
    const env = getEnvironment();
    if (!isNullOrUndefined(env)) {
      if (!this.moduleConfig.resourceServer.allowedUrls.some((s) => s === env.baseURL)) {
        this.moduleConfig.resourceServer.allowedUrls.push(env.baseURL);
      }
      if (!this.moduleConfig.resourceServer.allowedUrls.some((s) => s === env.userURL)) {
        this.moduleConfig.resourceServer.allowedUrls.push(env.userURL);
      }
      if (isInterfacedWithK2()) {
        if (!this.moduleConfig.resourceServer.allowedUrls.some((s) => s === env.baseK2URL)) {
          this.moduleConfig.resourceServer.allowedUrls.push(env.baseK2URL);
        }
      }
      if (isInterfacedWithAX()) {
        if (!this.moduleConfig.resourceServer.allowedUrls.some((s) => s === env.baseAxURL)) {
          this.moduleConfig.resourceServer.allowedUrls.push(env.baseAxURL);
        }
      }
    }
    const url = req.url;

    const requestId = Guid.newGuid();
    const headers = req.headers.set('RequestHeader', requestId);
    req = req.clone({ headers });

    if (!this.moduleConfig) {
      return next.handle(req);
    }
    if (!this.moduleConfig.resourceServer) {
      return next.handle(req);
    }
    if (!this.moduleConfig.resourceServer.allowedUrls) {
      return next.handle(req);
    }
    if (!this.checkUrl(url)) {
      return next.handle(req);
    }

    if (!this.authService.isLoggedIn()) {
      this.authService.initImplicitFlow();
    }
    const sendAccessToken = this.moduleConfig.resourceServer.sendAccessToken;

    if (sendAccessToken) {
      const token = this.oAuthStorage.getItem('access_token');
      const header = 'Bearer ' + token;

      const headers = req.headers.set('Authorization', header);

      req = req.clone({ headers });
    }

    const companyHeader = this.userDataService.getCurrentCompanyGUID();
    const branchHeader = this.userDataService.getCurrentBranchGUID();

    // site
    const siteHeader = this.userDataService.getSiteLogin();

    if (!isUndefinedOrZeroLength(companyHeader)) {
      let headers = req.headers.set('CompanyHeader', companyHeader);
      if (!isUndefinedOrZeroLength(branchHeader)) {
        headers = headers.set('BranchHeader', branchHeader);
      }
      if (!isUndefinedOrZeroLength(siteHeader) && isUndefinedOrZeroLength(headers.get('SiteHeader'))) {
        headers = headers.set('SiteHeader', siteHeader);
      }
      req = req.clone({ headers });
    }
    return next.handle(req);
  }
}
