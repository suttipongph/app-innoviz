import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { getEnvironment } from 'shared/config/globalvar.config';
import { isNullOrUndefined, isNullOrUndefOrEmpty } from 'shared/functions/value.function';
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {
  api: string;
  jsonApi: string;
  constructor(private http: HttpClient, private messageService: MessageService) {}

  public Post(url: string, data: any, defualt?: any, option?: any): Observable<any> {
    const env = getEnvironment();
    if (!isNullOrUndefined(env)) {
      this.api = env.baseURL;
    }
    // this.UrlDetecting(url, false);
    return new Observable((observer) => {
      this.http.post(this.api.concat(url), data, option).subscribe(
        (res) => {
          observer.next(res);
        },
        (err) => {
          observer.error(err);
        },
        () => {
          observer.complete();
          // this.UrlDetecting(url, true);
        }
      );
    });
  }
  public PostOld(url: string, data: any, defualt?: any, option?: any): Observable<any> {
    if (!isNullOrUndefOrEmpty(defualt)) {
      // data = compareDefault(defualt, data);
    }

    const env = getEnvironment();
    if (!isNullOrUndefined(env)) {
      this.api = env.baseURL;
    }
    // this.UrlDetecting(url, false);
    return this.http.post(this.api.concat(url), data, option).pipe(
      finalize(() => {
        // this.UrlDetecting(url, true);
      })
    );
  }
  public Get(url: string, option?: any): Observable<any> {
    const env = getEnvironment();
    if (!isNullOrUndefined(env)) {
      this.api = env.baseURL;
    }

    return new Observable((observer) => {
      this.http.get(this.api.concat(url), option).subscribe(
        (res) => {
          observer.next(res);
        },
        (err) => {
          observer.error(err);
        },
        () => {
          observer.complete();
        }
      );
    });
  }
  public GetOld(url: string, option?: any): Observable<any> {
    const env = getEnvironment();
    if (!isNullOrUndefined(env)) {
      this.api = env.baseURL;
    }
    return this.http.get(this.api.concat(url), option).pipe(finalize(() => {}));
  }
  public Getxml(url: string): Observable<any> {
    const option: any = { responseType: 'text', contentType: 'application/x-www-form-urlencoded' };
    return this.http.get<any>(this.api.concat(url), option);
  }
  public Postxml(url: string, data: any): Observable<any> {
    const option: any = { responseType: 'text', contentType: 'application/x-www-form-urlencoded' };
    return this.http.post<any>(this.api.concat(url), data, option);
  }
  public GetOri(url: string): Observable<any> {
    const option: any = { responseType: 'text', contentType: 'application/x-www-form-urlencoded' };
    return this.http.get<any>(url, option);
  }
  public GetAuth(url: string, option?: any): Observable<any> {
    const env = getEnvironment();
    if (!isNullOrUndefined(env)) {
      this.api = env.userURL;
    }
    return this.http.get(this.api.concat(url), option).pipe(finalize(() => {}));
  }

  // private UrlDetecting(url: string, responsed: boolean) {
  //     if (url.includes(`SysListView/`) || url.includes(`Attachment/AttachmentView`)) {
  //         this.uiService.finishSpinnerListSubject.next({ action: ACTIONTYPE.HTTP_GETLIST, state: responsed });
  //     }
  // }
}
