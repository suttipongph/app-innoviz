import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PageInformationModel, SearchResult, SelectItems } from 'shared/models/systemModel';
import { Action, ActionHistoryListView, TaskItemView, TaskListParm, WorkflowInstanceView } from 'shared/models/viewModel';
import { DataGatewayService } from './data-gateway.service';
import { HttpClientService } from './httpclient.service';

@Injectable({
  providedIn: 'root'
})
export class K2Service {
  servicePath = '';
  constructor(private dataGateway: DataGatewayService, private http: HttpClientService) {}
  setPath(param: PageInformationModel): void {
    this.servicePath = param.servicePath;
  }
  getBatchableActionToDropDownItem(actions: Action[]): SelectItems[] {
    return actions.map((val) => {
      return {
        value: val.name,
        label: val.name
      };
    });
  }
  getTaskBySerialNumber(param: WorkflowInstanceView): Observable<TaskItemView> {
    const url: string = `${this.servicePath}/GetTaskBySerialNumber`;
    return this.dataGateway.post(url, param);
  }
  startWorkflow(param: WorkflowInstanceView): Observable<number> {
    const url: string = `${this.servicePath}`;
    return this.dataGateway.post(url, param);
  }
  actionWorkflow(param: WorkflowInstanceView): Observable<boolean> {
    const url: string = `${this.servicePath}`;
    return this.dataGateway.post(url, param);
  }
  getActionHistoryByRefGUID(id: string): Observable<ActionHistoryListView[]> {
    const url: string = `${this.servicePath}/GetActionHistoryByRefGUID/id=${id}`;
    return this.dataGateway.get(url);
  }
  getTaskList(param: TaskListParm): Observable<SearchResult<TaskItemView>> {
    const url: string = `${this.servicePath}/GetTaskList`;
    return this.http.Post(url, param);
  }
  releaseTask(param: WorkflowInstanceView): Observable<boolean> {
    const url: string = `${this.servicePath}/GetReleaseTask`;
    return this.dataGateway.post(url, param);
  }
}
