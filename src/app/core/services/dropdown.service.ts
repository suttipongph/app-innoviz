import { Injectable } from '@angular/core';
import { HttpClientService } from './httpclient.service';
import { of, Observable } from 'rxjs';
// // import {
//   ACCOUNT_TYPE,
//   AGREEMENT_REVISION_TYPE,
//   AGREEMENT_TRANS_TYPE,
//   ASSET_FEE_TYPE,
//   CALCULATION_TYPE,
//   COLLATERAL_MOVEMENT_TYPE,
//   COLLECTION_FEE_METHOD,
//   CONTACT_TYPE,
//   IRR_CALCULATION_TYPE,
//   CUST_TRANS_STATUS,
//   DIMENSION,
//   DISCOUNT_BASE,
//   FV_FORMULA,
//   INCOME_REALIZE_GEN_METHOD,
//   INVOICE_ISSUING,
//   LEDGER_FISCAL_PERIOD_STATUS,
//   LEDGER_FISCAL_PERIOD_UNIT,
//   NPL_METHOD,
//   NUMBER_SEQ_SEGMENT_TYPE,
//   PAYMENT_TYPE,
//   PAYM_OVERRULE_TYPE,
//   PAYM_SCHEDULE_LINE_TYPE,
//   PAYM_STRUCTURE_REF_NUM,
//   PENALTY_BASE,
//   PROVISION_METHOD,
//   PROVISION_RATE_BY,
//   PV_FORMULA,
//   RECEIPT_GEN_BY,
//   RECEIPT_TEMP_REF_TYPE,
//   RECORD_TYPE,
//   REF_TYPE,
//   RESPONSIBLE_BY,
//   SYS_ACCESS_LEVEL,
//   SYS_ACCESS_RIGHT,
//   SYS_FEATURE_TYPE,
//   SYS_SESSION_TYPE,
//   TAX_INVOICE_REF_TYPE,
//   WHT_BASE,
//   BATCH_JOB_STATUS,
//   BATCH_INSTANCE_STATE,
//   BATCH_INTERVAL_TYPE,
//   BATCH_RESULT_STATUS,
//   INTERESTRATETYPE,
//   ROUNDINGTYPE,
//   SYS_LANGUAGE,
//   LETTER_OF_EXPIRATION_GEN_METHOD,
//   OVERDUE_TYPE,
//   ASSET_FEE_GEN_METHOD,
//   LETTER_OF_RENEWAL_GEN_METHOD,
//   AGENT_JOB_GEN_METHOD,
//   GENERATE_PENALTY_GEN_METHOD,
//   DEFAULT_BIT,
//   GENERATE_INSTALLMENT_INVOICE,
//   GEN_RENEW_AGREEMENT_ASSET_FEE_METHOD,
//   STAGING_BATCH_STATUS,
//   SITE_LOGIN_TYPE,
//   FILTER_BY,
//   GEN_INQUIRY_EXPECTED_CLOSE_AGREEMENT_METHOD,
//   LETTER_OF_OVERDUE_GEN_METHOD,
//   AGENT_ACTION_RESULT,
//   PENALTY_CALCULATION_METHOD,
//   SETTLE_INVOICE_CRITERIA,
//   PER_AGREEMENT_OR_ASSET,
//   REPOSSESSED_ASSET,
//   TYPE_OF_DATE,
//   DOCUMENT,
//   PAYMENT_STATUS,
//   DOCUMENT_FORMAT,
//   INTERFACE_STATUS,
//   BRANCH_CRITERIA,
//   FLEX_TYPE,
//   ASSET_FLEX_TYPE,
//   DOCUMENT_FORMAT_TAX,
//   STOP_TAX_REALIZATION_AGREEMENT,
//   ROUNDINGMETHOD,
//   TEMP_RECEIPT_GROUPBY,
//   NEW_DUE_DATE,
//   AGREEMENT_REPOSSESSION_STATUS,
//   BATCH_DAY_OF_WEEK,
//   ASSET_CONDITION,
//   REVERT_REPOSSESS_TYPE,
//   COMPANY_SIGNATURE_REF_TYPE,
//   OPEN_AND_CLOSE,
//   DOCUMENT_TEMPLATE_TYPE,
//   DAY_OR_MONTH,
//   PRINTING_DIRECTION,
//   CRITERIA,
//   DETAIL_LEVEL
// } from 'shared/constants';
import { SelectItems, JoinCondtion, SearchCondition, SearchParameter } from 'shared/models/systemModel';
import { UIControllerService } from './uiController.service';
import { BranchFilterType } from 'shared/constants';
import { isNullOrUndefOrEmpty, transformLabel } from 'shared/functions/value.function';
import { DataGatewayService } from './data-gateway.service';

@Injectable({
  providedIn: 'root'
})
export class DropDownService {
  constructor(private http: HttpClientService, private uiService: UIControllerService, private dataGateway: DataGatewayService) {}

  // #region ENUM

  // getDefaultBitDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(DEFAULT_BIT);
  //     return of(result);
  // }
  // genRenewAgreementAssetFeeMethod(): Observable<SelectItems[]> {
  //     const result = transformLabel(GEN_RENEW_AGREEMENT_ASSET_FEE_METHOD);
  //     return of(result);
  // }
  // getGeneratePenaltyGenMethodDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(GENERATE_PENALTY_GEN_METHOD);
  //     return of(result);
  // }
  // getGenerateInstallmentInvoiceDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(GENERATE_INSTALLMENT_INVOICE);
  //     return of(result);
  // }
  // getAccountTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(ACCOUNT_TYPE);
  //     return of(result);
  // }
  // getAgreementRevisionTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(AGREEMENT_REVISION_TYPE);
  //     return of(result);
  // }
  // getAgreementTransTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(AGREEMENT_TRANS_TYPE);
  //     return of(result);
  // }
  // getAssetFeeTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(ASSET_FEE_TYPE);
  //     return of(result);
  // }
  // getBatchDayOfWeekDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(BATCH_DAY_OF_WEEK);
  //     return of(result);
  // }
  // getBatchInstanceStateDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(BATCH_INSTANCE_STATE);
  //     return of(result);
  // }
  // getBatchIntervalTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(BATCH_INTERVAL_TYPE);
  //     return of(result);
  // }
  // getBatchJobStatusDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(BATCH_JOB_STATUS);
  //     return of(result);
  // }
  // getBatchResultStatusDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(BATCH_RESULT_STATUS);
  //     return of(result);
  // }
  // getCalculationTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(CALCULATION_TYPE);
  //     return of(result);
  // }
  // getIRRCalculationTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(IRR_CALCULATION_TYPE);
  //     return of(result);
  // }
  // getCollateralMovementTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(COLLATERAL_MOVEMENT_TYPE);
  //     return of(result);
  // }
  // getCollectionFeeMethodDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(COLLECTION_FEE_METHOD);
  //     return of(result);
  // }
  // getContactTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(CONTACT_TYPE);
  //     return of(result);
  // }
  // getCustTransStatusDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(CUST_TRANS_STATUS);
  //     return of(result);
  // }
  // getDimensionDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(DIMENSION);
  //     return of(result);
  // }
  // getDiscountBaseDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(DISCOUNT_BASE);
  //     return of(result);
  // }
  // getFVFormulaDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(FV_FORMULA);
  //     return of(result);
  // }
  // getIncomeRealizeGenMethodDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(INCOME_REALIZE_GEN_METHOD);
  //     return of(result);
  // }
  // getInterfaceStatusDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(INTERFACE_STATUS);
  //     return of(result);
  // }
  // getInvoiceIssuingDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(INVOICE_ISSUING);
  //     return of(result);
  // }
  // getLedgerFiscalPeriodStatusDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(LEDGER_FISCAL_PERIOD_STATUS);
  //     return of(result);
  // }
  // getLedgerFiscalPeriodUnitDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(LEDGER_FISCAL_PERIOD_UNIT);
  //     return of(result);
  // }
  // getNPLMethodDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(NPL_METHOD);
  //     return of(result);
  // }
  // getNumberSeqSegmentTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(NUMBER_SEQ_SEGMENT_TYPE);
  //     return of(result);
  // }
  // getPaymentTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(PAYMENT_TYPE);
  //     return of(result);
  // }
  // getPaymOverruleTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(PAYM_OVERRULE_TYPE);
  //     return of(result);
  // }
  // getPaymScheduleLineTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(PAYM_SCHEDULE_LINE_TYPE);
  //     return of(result);
  // }
  // getPaymStructureRefNumDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(PAYM_STRUCTURE_REF_NUM);
  //     return of(result);
  // }
  // getPenaltyBaseDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(PENALTY_BASE);
  //     return of(result);
  // }
  // getProvisionMethodDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(PROVISION_METHOD);
  //     return of(result);
  // }
  // getProvisionRateByDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(PROVISION_RATE_BY);
  //     return of(result);
  // }
  // getPVFormulaDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(PV_FORMULA);
  //     return of(result);
  // }
  // getReceiptGenByDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(RECEIPT_GEN_BY);
  //     return of(result);
  // }
  // getReceiptTempRefTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(RECEIPT_TEMP_REF_TYPE);
  //     return of(result);
  // }
  // getRecordTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(RECORD_TYPE);
  //     return of(result);
  // }
  // getRefTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(REF_TYPE);
  //     return of(result);
  // }
  // getResponsibleByDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(RESPONSIBLE_BY);
  //     return of(result);
  // }
  // getSiteLoginOptionDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(SITE_LOGIN_TYPE);
  //     return of(result);
  // }
  // getSysAccessLevelDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(SYS_ACCESS_LEVEL);
  //     return of(result);
  // }
  // getSysAccessRightDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(SYS_ACCESS_RIGHT);
  //     return of(result);
  // }
  // getSysFeatureTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(SYS_FEATURE_TYPE);
  //     return of(result);
  // }
  // getSysLanguageDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(SYS_LANGUAGE);
  //     return of(result);
  // }
  // getSysSessionTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(SYS_SESSION_TYPE);
  //     return of(result);
  // }
  // getTaxInvoiceRefTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(TAX_INVOICE_REF_TYPE);
  //     return of(result);
  // }
  // getWHTBaseDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(WHT_BASE);
  //     return of(result);
  // }

  // getCalcSheet_InterestRateTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(INTERESTRATETYPE);
  //     return of(result);
  // }
  // getCalcSheet_RoundingTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(ROUNDINGTYPE);
  //     return of(result);
  // }
  // getCalcSheet_RoundingMethodDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(ROUNDINGMETHOD);
  //     return of(result);
  // }
  // getLetterOfExpirationGenMethodDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(LETTER_OF_EXPIRATION_GEN_METHOD);
  //     return of(result);
  // }
  // getOverdueTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(OVERDUE_TYPE);
  //     return of(result);
  // }
  // getAssetFeeGenMethodDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(ASSET_FEE_GEN_METHOD);
  //     return of(result);
  // }
  // getLetterOfRenewalGenMethodDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(LETTER_OF_RENEWAL_GEN_METHOD);
  //     return of(result);
  // }
  // getAgentJobGenMethodDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(AGENT_JOB_GEN_METHOD);
  //     return of(result);
  // }
  // getStagingBatchStatusDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(STAGING_BATCH_STATUS);
  //     return of(result);
  // }
  // getFilterByDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(FILTER_BY);
  //     return of(result);
  // }
  // getGenInquiryExpectedCloseAgreementMethodDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(GEN_INQUIRY_EXPECTED_CLOSE_AGREEMENT_METHOD);
  //     return of(result);
  // }
  // getLetterOfOverdueGenMethodDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(LETTER_OF_OVERDUE_GEN_METHOD);
  //     return of(result);
  // }

  // getAgentActionResultDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(AGENT_ACTION_RESULT);
  //     return of(result);
  // }
  // getPenaltyCalculationMethodDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(PENALTY_CALCULATION_METHOD);
  //     return of(result);
  // }
  // getSettleInvoiceCriteriaDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(SETTLE_INVOICE_CRITERIA);
  //     return of(result);
  // }
  // getPerAgreementOrAssetDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(PER_AGREEMENT_OR_ASSET);
  //     return of(result);
  // }
  // getRepossessedAssetDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(REPOSSESSED_ASSET);
  //     return of(result);
  // }
  // getTypeOfDateDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(TYPE_OF_DATE);
  //     return of(result);
  // }
  // getDocumenDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(DOCUMENT);
  //     return of(result);
  // }
  // getDocumentFormatDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(DOCUMENT_FORMAT);
  //     return of(result);
  // }
  // getDocumentFormatTaxDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(DOCUMENT_FORMAT_TAX);
  //     return of(result);
  // }
  // getPaymentStatusDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(PAYMENT_STATUS);
  //     return of(result);
  // }
  // getBranchCriteriaDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(BRANCH_CRITERIA);
  //     return of(result);
  // }
  // getFlexTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(FLEX_TYPE);
  //     return of(result);
  // }
  // getAssetFlexTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(ASSET_FLEX_TYPE);
  //     return of(result);
  // }
  // getStopTaxRealizationAgreement(): Observable<SelectItems[]> {
  //     const result = transformLabel(STOP_TAX_REALIZATION_AGREEMENT);
  //     return of(result);
  // }
  // getTemporaryReceiptCriteriaDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(TEMP_RECEIPT_GROUPBY);
  //     return of(result);
  // }
  // getNewDueDateOfAgreementPaymentScheduleDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(NEW_DUE_DATE);
  //     return of(result);
  // }
  // getAgreementRepossessionStatusDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(AGREEMENT_REPOSSESSION_STATUS);
  //     return of(result);
  // }
  // getAssetConditionDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(ASSET_CONDITION);
  //     return of(result);
  // }
  // getRevertRepossessTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(REVERT_REPOSSESS_TYPE);
  //     return of(result);
  // }
  // getCompanySignatureRefTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(COMPANY_SIGNATURE_REF_TYPE);
  //     return of(result);
  // }
  // getOpenAndCloseDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(OPEN_AND_CLOSE);
  //     return of(result);
  // }
  // getDocumentTemplateTypeDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(DOCUMENT_TEMPLATE_TYPE);
  //     return of(result);
  // }
  // getDayOrMonthDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(DAY_OR_MONTH);
  //     return of(result);
  // }
  // getPrintingDirectionDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(PRINTING_DIRECTION);
  //     return of(result);
  // }
  // getCriteriaDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(CRITERIA);
  //     return of(result);
  // }
  // getDetailLevelDropDown(): Observable<SelectItems[]> {
  //     const result = transformLabel(DETAIL_LEVEL);
  //     return of(result);
  // }
  // //#endregion

  // // #region New Dropdown - Normal case
  // getPurchAgreementDeductionSetupDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/PurchAgreementDeductionSetup/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getDocumentProcessDropDown(): Observable<SelectItems[]> {
  //     const url: string = 'SysDropDown/DocumentProcess';
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getAddressCountryDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AddressCountry/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getAddressDistrictDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AddressDistrict/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAddressPostalCodeDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AddressPostalCode/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getAddressProvinceDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AddressProvince/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAddressSubDistrictDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AddressSubDistrict/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getAddressTransDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AddressTrans/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAddressTransByRefIdDropDown(refId: string): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AddressTrans/RefId=${refId}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAgentJobJournalDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AgentJobJournal/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAgentJobTransDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AgentJobTrans/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAgreementAssetDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AgreementAsset/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAgreementAssetFeesDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AgreementAssetFees/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAgreementCalcTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AgreementCalcTable/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAgreementClassificationJournalDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AgreementClassificationJournal/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAgreementClassificationTransDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AgreementClassificationTrans/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAgreementEarlyPayoffDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AgreementEarlyPayoff/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAgreementODDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AgreementOD/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAgreementOverrulePaymStructureDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AgreementOverrulePaymStructure/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAgreementRevisionHistoryDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AgreementRevisionHistory/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAgreementTableDropDown(branch?: string): Observable<SelectItems[]> {
  //     const branchType = isNullOrUndefOrEmpty(branch) ? this.uiService.branchType : branch;
  //     const url: string = `SysDropDown/AgreementTable/${branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAgreementTableDropDownById(id: string): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AgreementTable/${id}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getAgreementTypeDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AgreementType/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getApplicationCalcCompulsoryDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ApplicationCalcCompulsory/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getApplicationCalcInsuranceDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ApplicationCalcInsurance/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getApplicationCalcMaintenanceDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ApplicationCalcMaintenance/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getApplicationCalcOtherCostDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ApplicationCalcOtherCost/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getApplicationCalcTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ApplicationCalcTable/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getApplicationCalcVehicleTaxDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ApplicationCalcVehicleTax/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getApplicationLineDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ApplicationLine/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getApplicationPaymScheduleDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ApplicationPaymSchedule/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getApplicationTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ApplicationTable/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAssetGroupDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AssetGroup/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAuthorizedPersonTransDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AuthorizedPersonTrans/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getBankGroupDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/BankGroup/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getBillingNotesTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/BillingNotesTable/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getBranchDropDown(conditions: SearchCondition[], branch?: string): Observable<SelectItems[]> {
  //     const branchType = isNullOrUndefOrEmpty(branch) ? this.uiService.branchType : branch;
  //     const url: string = `SysDropDown/Branch/${branchType}`;
  //     const result = this.http.Post(url, conditions);
  //     return result;
  // }

  // getBranchByTaxRealizationReportDropDown(branch?: string): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/Branch/ByTaxRealizationReport`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getBranchGetAllDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/Branch/GetAll`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCampaignTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CampaignTable/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCollateralLocationDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CollateralLocation/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCollateralMovementJournalDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CollateralMovementJournal/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCollateralMovementTransDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CollateralMovementTrans/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCollateralTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CollateralTable/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCollateralTypeDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CollateralType/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCollectionActivityDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CollectionActivity/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCollectionActivityResultDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CollectionActivityResult/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCollectionActivityTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CollectionActivityTable/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCollectionActivityTransDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CollectionActivityTrans/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getCollectionFeeGroupDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CollectionFeeGroup/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCollectionFeeSetupDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CollectionFeeSetup/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCollectionFeeTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CollectionFeeTable/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCollectionGroupDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CollectionGroup/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCombinationSetupDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CombinationSetup/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCompanyDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/Company/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCompanyGetAllDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/Company/GetAll`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCompanyParameterDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CompanyParameter/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getContactTransDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ContactTrans/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCostTypeDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CostType/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCreditAppTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CreditAppTable/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCreditResultDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CreditResult/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getCurrencyDropDown(conditions: SearchCondition[]): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/Currency/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Post(url, conditions);
  //     return result;
  // }
  // getCustBankDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CustBank/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCustCreditGroupDropDown(conditions: SearchCondition[]): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CustCreditGroup/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Post(url, conditions);
  //     return result;
  // }
  // getCustCreditRatingDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CustCreditRating/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCustGroupDropDown(conditions: SearchCondition[]): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CustGroup/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Post(url, conditions);
  //     return result;
  // }
  // getCustomerNameHistoryDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CustomerNameHistory/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCustomerTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CustomerTable/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCustTransDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CustTrans/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getDocumentCheckListTemplateLineDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/DocumentCheckListTemplateLine/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getDocumentCheckListTemplateTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/DocumentCheckListTemplateTable/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getDocumentCheckListTypeDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/DocumentCheckListType/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getDocumentReasonDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/DocumentReason/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getEmployeeTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/EmployeeTable/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getEmployeeTableActiveDropDown(conditions: SearchCondition[]): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/EmployeeTable/Active`;
  //     const result = this.http.Post(url, conditions);
  //     return result;
  // }
  // getEmployeeTableByCollectionActivityDropDown(id: string): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/EmployeeTableByCollectionActivity/id=${id}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getEmplTeamDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/EmplTeam/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getExchangeRateDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ExchangeRate/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getGuarantorTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/GuarantorTable/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getInstituteDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/Institute/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getInsuranceDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/Insurance/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getIntroducedByDropDown(conditions: SearchCondition[]): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/IntroducedBy/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Post(url, conditions);
  //     return result;
  // }
  // getInvoiceLineDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/InvoiceLine/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getInvoiceNumberSeqSetupDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/InvoiceNumberSeqSetup/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getInvoiceRevenueTypeDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/InvoiceRevenueType/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getInvoiceTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/InvoiceTable/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getInvoiceTableDropDownById(id: string): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/InvoiceTable/${id}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getInvoiceTypeDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/InvoiceType/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getLanguageDropDown(conditions: SearchCondition[]): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/Language/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Post(url, conditions);
  //     return result;
  // }
  // getLeaseSubTypeDropDown(conditions: SearchCondition[]): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/LeaseSubType/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Post(url, conditions);
  //     return result;
  // }

  // getLeaseTypeDropDown(conditions: SearchCondition[]): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/LeaseType/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Post(url, conditions);
  //     return result;
  // }
  // getLedgerDimensionDropDown(conditions: SearchCondition[]): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/LedgerDimension/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Post(url, conditions);
  //     return result;
  // }
  // getLedgerFiscalPeriodDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/LedgerFiscalPeriod/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getLedgerFiscalYearDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/LedgerFiscalYear/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getLetterOfExpirationJournalDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/LetterOfExpirationJournal/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getLetterOfExpirationTransDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/LetterOfExpirationTrans/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getLetterOfOverdueJournalDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/LetterOfOverdueJournal/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getLetterOfOverdueSetupDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/LetterOfOverdueSetup/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getLetterOfOverdueTransDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/LetterOfOverdueTrans/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getLetterOfOverdueTypeDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/LetterOfOverdueType/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getLetterOfRenewalJournalDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/LetterOfRenewalJournal/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getLetterOfRenewalTransDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/LetterOfRenewalTrans/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getLineOfBusinessDropDown(conditions: SearchCondition[]): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/LineOfBusiness/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Post(url, conditions);
  //     return result;
  // }
  // getMethodOfClosingDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/MethodOfClosing/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getMethodOfPaymentDropDown(conditions: SearchCondition[]): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/MethodOfPayment/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Post(url, conditions);
  //     return result;
  // }
  // getMethodOfPaymentByAgreementInvoiceDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/MethodOfPayment/ByAgreementInvoice`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getMethodOfPaymentByTemporaryReceiptDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/MethodOfPayment/TemporaryReceipt`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getNCBAccountStatusDropDown(conditions: SearchCondition[]): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/NCBAccountStatus/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Post(url, conditions);
  //     return result;
  // }

  // getNPLRateDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/NPLRate/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getNumberSeqTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/NumberSeqTable/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getPaymentFrequencyDropDown(conditions: SearchCondition[]): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/PaymentFrequency/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Post(url, conditions);
  //     return result;
  // }
  // getPaymentHistoryDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/PaymentHistory/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getPaymentStructureLineDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/PaymentStructureLine/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getPaymentStructureTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/PaymentStructureTable/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getPenaltyRateDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/PenaltyRate/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getPenaltySetupDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/PenaltySetup/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getPenaltyTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/PenaltyTable/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getProdCampaignDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ProdCampaign/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getProdGroupDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ProdGroup/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getProductTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ProductTable/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getProdUnitDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ProdUnit/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getProspectTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ProspectTable/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getProvisionJournalDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ProvisionJournal/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getProvisionRateDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ProvisionRate/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getProvisionTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ProvisionTable/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getProvisionTransDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ProvisionTrans/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getPurchAgreementConfirmTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/PurchAgreementConfirmTable/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getPurchAgreementLineDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/PurchAgreementLine/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getPurchAgreementTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/PurchAgreementTable/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getReceiptLineDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ReceiptLine/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getReceiptTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ReceiptTable/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getReceiptTempTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ReceiptTempTable/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getRepossessionStatusDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/RepossessionStatus/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getStagingTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/StagingTable/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getStagingTransTextDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/StagingTransText/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getTaxInvoiceLineDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/TaxInvoiceLine/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getTaxInvoiceTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/TaxInvoiceTable/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getTaxTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/TaxTable/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getTaxValueDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/TaxValue/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getTermDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/Term/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getTerritoryDropDown(conditions: SearchCondition[]): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/Territory/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Post(url, conditions);
  //     return result;
  // }
  // getVehicleEngineSizeDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/VehicleEngineSize/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getVehicleSubTypeDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/VehicleSubType/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getVehicleTypeDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/VehicleType/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getVendGroupDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/VendGroup/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getVendorTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/VendorTable/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getWaiveTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/WaiveTable/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getWithholdingTaxGroupDropDown(conditions: SearchCondition[]): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/WithholdingTaxGroup/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Post(url, conditions);
  //     return result;
  // }

  // getWithholdingTaxTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/WithholdingTaxTable/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getWitnessTransDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/WitnessTrans/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getImportReceiptHeaderDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ImportReceiptHeader/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getImportReceiptLineDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/ImportReceiptLine/${this.uiService.branchType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getFlexSetupDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/FlexSetup/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAssetFlexSetupDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AssetFlexSetup/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getSellingTypeDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/sellingType/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // //#endregion

  // //#region  NEW DropDown - Special case
  // getDocumentStatusDropDown(conditions: SearchCondition[]): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/DocumentStatus/ProcessId`;
  //     const result = this.http.Post(url, conditions);
  //     return result;
  // }
  // getDropDownDocumentStatusByStatusId(id: string[]): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/DocumentStatus/GetDropDownDocumentStatusByStatusId`;
  //     const result = this.http.Post(url, id);
  //     return result;
  // }
  // getAddressDistrictByProvinceDropDown(id: string): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AddressDistrictByProvince/id=${id}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAddressProvinceByCountryDropDown(id: string): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AddressProvinceByCountry/id=${id}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAddressSubDistrictByDistrictDropDown(id: string): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AddressSubDistrictByDistrict/id=${id}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getAddressTransByInvoiceTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = 'SysDropDown/AddressTransByInvoiceTable';
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAddressTransByGuarantorTrans(id: string): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AddressTrans/ByGuarantorTrans/id=${id}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAddressTransByGuarantor(id: string): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AddressTrans/ByGuarantor/id=${id}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAgreementAssetByAgreementDropDown(agreementId: string): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AgreementAssetByAgreement/Id=${agreementId}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAgreementTableByGenerateLetterOfOverdueTransaction(): Observable<SelectItems[]> {
  //     const url: string = 'SysDropDown/AgreementTableByGenerateLetterOfOverdueTransaction';
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAgreementTableByGenerateAgentJobTransaction(): Observable<SelectItems[]> {
  //     const url: string = 'SysDropDown/AgreementTableByGenerateAgentJobTransaction';
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAgreementStatusEqualDropDown(statusId: string[], branch?: string): Observable<SelectItems[]> {
  //     const branchType = isNullOrUndefOrEmpty(branch) ? this.uiService.branchType : branch;
  //     const url: string = `SysDropDown/GetAgreementStatusEqual/${branchType}`;
  //     const result = this.http.Post(url, statusId);
  //     return result;
  // }
  // getAgreementWithStausAndCustomerNameDropDown(joinCondtion: JoinCondtion): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AgreementWithStausAndCustomerName`;
  //     const result = this.http.Post(url, joinCondtion);
  //     return result;
  // }
  // // getBranchByUserCompanyDropDown_Filter(keyword: string): Observable<SelectItems[]> {
  // //     const userId = this.userDataService.getUserIdFromToken();
  // //     const companyGUID = this.userDataService.getCurrentCompanyGUID();
  // //     const url: string = `SysDropDown/BranchByUserCompany/${keyword}/${userId}/${companyGUID}`;
  // //     const result = this.http.Get(url);
  // //     return result;
  // // }
  // getCampaignTableByAppTableDateDropDown(id: string): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CampaignTableByAppTableDate/id=${id}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getActiveCustomersDropDown(): Observable<SelectItems[]> {
  //     const url: string = 'SysDropDown/ActiveCustomers';
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getActiveCustomersDropDown_Filter(keyword: string, isBypass: boolean): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CustomerTableFiltered/${keyword}/${isBypass}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getDocumentReasonByRefTypeDropDown(refType: number): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/DocumentReasonByRefType/${refType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getInvoiceRevenueTypeByAssetFeeTypeDropDown(assetFeeType: number): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/InvoiceRevenueTypeByAssetFeeType/AssetType=${assetFeeType}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getInvoiceRevenueTypeByInvoiceTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = 'SysDropDown/InvoiceRevenueTypeByInvoiceTable';
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // // getInvoiceTableByOverrulePaymentDropDown(model: AgreementOverrulePaymStructureView): Observable<SelectItems[]> {
  // //     const url: string = 'SysDropDown/InvoiceTableByOverrulePayment';
  // //     const result = this.http.Post(url, model);
  // //     return result;
  // // }
  // getInvoiceTableByCompanyParameterDropDown(): Observable<SelectItems[]> {
  //     const url: string = 'SysDropDown/InvoiceTableByCompanyParameter';
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getInvoiceTypeByOverrulePaymentDropDown(): Observable<SelectItems[]> {
  //     const url: string = 'SysDropDown/InvoiceTypeByOverrulePayment';
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getPaymentStructureTableByActiveStatusDropDown(): Observable<SelectItems[]> {
  //     const url: string = 'SysDropDown/PaymentStructureTableByActiveStatus';
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getTaxInvoiceTableForGenDepositReturningDropDown(id: string): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/GetTaxInvoiceTableForGenDepositReturning/id=${id}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAgreementTableByOriginalAgreement(): Observable<SelectItems[]> {
  //     const url: string = 'SysDropDown/AgreementTableByOriginalAgreement';
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCompanySignatureForAGM_SignatureDropDown(branchId: string): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CompanySignatureForAGM_Signature/branchId=${branchId}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // // use SysUserTable.UserName as key
  // getSysUserTableDropDownByUserName(): Observable<SelectItems[]> {
  //     const url: string = `SysUser/SysUserTableDropDownUserName`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getDocumentCheckTransLineByHeaderDropDown(id: string): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/DocumentCheckTransLineByHeader/${id}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // // all employee
  // getAllEmployeeTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/EmployeeTable/All`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // //#endregion Special case

  // getDutyStampSetupDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/DutyStampSetup/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getOwnershipDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/Ownership/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }

  // getDutyStampJournalDropDown(conditions: SearchCondition[]): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/DutyStampJournal/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Post(url, conditions);
  //     return result;
  // }
  // getGenderDropDown(conditions: SearchCondition[]): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/Gender/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Post(url, conditions);
  //     return result;
  // }
  // // getMaritalStatusDropDown(conditions: SearchCondition[]): Observable<SelectItems[]> {
  // //     const search: SearchParameter = { urlPath: `SysDropDown/MaritalStatus/${BranchFilterType.BYCOMPANY}`, conditions };
  // //     return this.dataGateway.getDropdown('', conditions);
  // // }
  // getOccupationDropDown(conditions: SearchCondition[]): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/Occupation/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Post(url, conditions);
  //     return result;
  // }
  // getPositionDropDown(conditions: SearchCondition[]): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/Position/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Post(url, conditions);
  //     return result;
  // }
  // getPropertyTypeDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/PropertyType/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getCustomerTableOnlyIdDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/CustomerTable/${BranchFilterType.BYCOMPANY}OnlyId`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getDocumentTemplateTableDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/DocumentTemplateTable/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getStagingBatchIdDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/StagingBatchId/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getInterfaceStagingBatchIdDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/InterfaceStagingBatchId/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getRepossessionLocationDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/RepossessionLocation/${BranchFilterType.BYCOMPANY}`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getMigrationDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/Migration`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
  // getAdUserDropDown(): Observable<SelectItems[]> {
  //     const url: string = `SysDropDown/AdUserForCreate`;
  //     const result = this.http.Get(url);
  //     return result;
  // }
}
