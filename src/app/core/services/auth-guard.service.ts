import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, CanActivate } from '@angular/router';
import { AccessRightService } from './access-right.service';
import { EmptyGuid, AppConst, AccessLevel, WORKFLOW } from 'shared/constants';
import {
  isUndefinedOrZeroLength,
  getNewRouteFromWorkFlowUrl,
  getQueryParamObject,
  isZero,
  isNullOrUndefined,
  getSiteFormParam
} from 'shared/functions/value.function';
import { NotificationService } from './notification.service';
import { AuthService } from './auth.service';
import { UserDataService } from './user-data.service';
import { finalize } from 'rxjs/operators';
import { UIControllerService } from './uiController.service';
import { AppInjector } from 'app-injector';
import { setBodyFunctionMode } from 'shared/functions/routing.function';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  uiService: UIControllerService;
  constructor(
    private authService: AuthService,
    private userDataService: UserDataService,
    private accessService: AccessRightService,
    private notificationService: NotificationService,
    private router: Router // private uiService: UIControllerService
  ) {
    this.uiService = AppInjector.get(UIControllerService);
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (!this.authService.isLoggedIn()) {
      this.router.navigate(['/']);
      return false;
    }
    const currentCompany: string = this.userDataService.getCurrentCompanyGUID();
    const siteLogin = this.userDataService.getSiteLogin();
    if (isUndefinedOrZeroLength(currentCompany)) {
      this.notificationService.showBranchCompanySelector();
      return false;
    }
    // check 'wf' route
    let siteParam: string = next.params['site'];
    // siteParam = getSiteFormParam(siteParam);
    let mainParam = next.params['main'];
    let siteFromRoute = isUndefinedOrZeroLength(siteParam)
      ? isUndefinedOrZeroLength(mainParam)
        ? ''
        : mainParam.split(':')[0] + ':' + mainParam.split(':')[1]
      : siteParam;
    const isWfSite = isUndefinedOrZeroLength(siteFromRoute) ? false : siteFromRoute.split(':')[0] === WORKFLOW.WF;
    if (isWfSite) {
      if (siteLogin === AppConst.CASHIER) {
        sessionStorage.removeItem('redirect');
        console.error('Workflow tasks not available in site: Cashier.');
        this.accessService.setErrorCode('ERROR.00811');
        setBodyFunctionMode(false);
        this.router.navigate([`/${siteLogin}/401`]);
        return false;
      } else {
        const companyGUID = siteFromRoute.split(':')[1];
        if (!isUndefinedOrZeroLength(companyGUID)) {
          let newRoute = getNewRouteFromWorkFlowUrl(state.url, siteLogin);
          this.uiService.initialSerialNumberUrl = newRoute;
          if (companyGUID === currentCompany) {
            this.accessService.siteAndCompanyBranchSelected.next(true);
            sessionStorage.removeItem('redirect');
            this.navigateToQueryStringRoute(newRoute);
            return false;
          } else {
            const userId = this.userDataService.getUserIdFromToken();
            this.accessService
              .loadUserAccessRightByCompany(userId, companyGUID)
              .pipe(
                finalize(() => {
                  sessionStorage.removeItem('redirect');
                  return false;
                })
              )
              .subscribe(
                () => {
                  const newCurrentCompany = this.userDataService.getCurrentCompanyGUID();
                  if (!isUndefinedOrZeroLength(newCurrentCompany)) {
                    this.navigateToQueryStringRoute(newRoute);
                  } else {
                    console.error('User not mapped to company');
                    this.accessService.setErrorCode('ERROR.00812');
                    setBodyFunctionMode(false);
                    this.router.navigate([`/${siteLogin}/401`]);
                  }
                },
                (error) => {
                  console.error('failed loading access right', error);
                  this.accessService.setErrorCode('ERROR.00810');
                  setBodyFunctionMode(false);
                  this.router.navigate([`/${siteLogin}/401`]);
                }
              );
          }
        } else {
          // invalid companyGUID value
          sessionStorage.removeItem('redirect');
          this.accessService.setErrorCode('ERROR.00813');
          setBodyFunctionMode(false);
          this.router.navigate([`/${siteLogin}/401`]);
          return false;
        }
      }
    } else {
      if (!this.accessService.isPermittedBySite()) {
        this.accessService.setErrorCode('ERROR.00809');
        this.router.navigate(['/Logout']);
        return false;
      }
      if (!this.accessService.isPermittedByAccessRight()) {
        this.accessService.setErrorCode('ERROR.00808');
        setBodyFunctionMode(false);
        this.router.navigate([`/${siteLogin}/401`]);
        return false;
      }
      // set activated sysFeatureRoles to sessionStorage
      const activatedPath: string = this.accessService.getActivatedRoutePath(next);
      let activatedFeatureRoles = this.accessService.getFeatureRoleFromActivatedPath(activatedPath);
      if (activatedPath === '/:site/dummy/dummy/:id') {
        // dummy
        return true;
      }

      if (isUndefinedOrZeroLength(activatedFeatureRoles)) {
        this.accessService.setErrorCode('ERROR.00814');
        setBodyFunctionMode(false);
        this.router.navigate([`/${siteLogin}/401`]);
        return false;
      } else {
        const parentIdx: number = activatedFeatureRoles.findIndex((item) => isUndefinedOrZeroLength(item.sysFeatureTable_ParentFeatureId));
        // primary component of route AccessMode = noAccess
        if (parentIdx === -1) {
          this.accessService.setErrorCode('ERROR.00814');
          setBodyFunctionMode(false);
          this.router.navigate([`/${siteLogin}/401`]);
          return false;
        } else {
          let userAccess = activatedFeatureRoles[parentIdx].accessLevels;
          if (this.accessService.isNoAccess(userAccess, activatedPath)) {
            this.accessService.setErrorCode('ERROR.00814');
            setBodyFunctionMode(false);
            this.router.navigate([`/${siteLogin}/401`]);
            return false;
          }
          // create company
          if (activatedPath.includes('createcompany')) {
            if (userAccess.create < AccessLevel.User) {
              this.accessService.setErrorCode('ERROR.00814');
              setBodyFunctionMode(false);
              this.router.navigate([`/${siteLogin}/401`]);
              return false;
            }
          }
          // delete company
          else if (activatedPath.includes('deletecompany')) {
            if (userAccess.delete < AccessLevel.User) {
              this.accessService.setErrorCode('ERROR.00814');
              this.router.navigate([`/${siteLogin}/401`]);
              return false;
            }
          }
          // check create mode
          let idParam = next.params['id'];
          if (!isUndefinedOrZeroLength(idParam)) {
            if (idParam === EmptyGuid) {
              if (userAccess.create < AccessLevel.User) {
                this.accessService.setErrorCode('ERROR.00814');
                setBodyFunctionMode(false);
                this.router.navigate([`/${siteLogin}/401`]);
                return false;
              }
            }
          }

          return true;
        }
      }
    }
  }

  private navigateToQueryStringRoute(path: string) {
    const path_decoded = decodeURIComponent(path);
    const splits = path_decoded.split('?');

    if (splits.length > 1) {
      let routePath = splits[0];
      if (splits.length > 2) {
        for (let i = 1; i < splits.slice(splits.length - 2).length; i++) {
          routePath = routePath.concat('?', splits[i]);
        }
      }

      this.router.navigate([routePath], {
        queryParams: getQueryParamObject(splits.slice(-1).pop())
      });
    } else {
      this.router.navigate([path_decoded]);
    }
  }
}
