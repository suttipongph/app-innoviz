import { Subject, Observable } from 'rxjs';
import jwt_decode from 'jwt-decode';
import { OAuthStorage } from 'angular-oauth2-oidc';
import { Injectable } from '@angular/core';
import { HttpClientService } from './httpclient.service';
import { isNullOrUndefined, isUndefinedOrZeroLength, decryptObject, encryptObject } from 'shared/functions/value.function';
import { getEncryptionKey } from 'shared/config/globalvar.config';
import { StorageKey, AppConst } from 'shared/constants';
import { SysUserSettingsItemView, SysUserTableView } from 'shared/models/viewModel';

export const userAPI = {
  sysusertableUrl: 'sysusertable/',
  getUserDataUrl: 'SysUser/SysUserTableView/Get/',
  getcompaniesbyuserUrl: 'sysusercompanymapping/companies/',
  getUserSettingsUrl: 'SysUser/UserSettings/',
  updateusersettingsUrl: 'SysUser/UpdateSettings/'
};

@Injectable({
  providedIn: 'root'
})
export class UserDataService {
  getDisplayNameSubject = new Subject<string>();
  getCompanyMappingSubject = new Subject<any>();
  getUserDataSubject = new Subject<any>();
  getFeatureRoleSubject = new Subject<any>();
  languageChangeSubject = new Subject<any>();

  staticCompanyBranchSelectorLoadedSubject = new Subject<any>();
  showCompanyBranchSelectorSubject = new Subject<any>();
  companyChangeSubject = new Subject<any>();

  userCompanyMappingUrl: string = userAPI.getcompaniesbyuserUrl;
  userDataUrl: string = userAPI.getUserDataUrl;
  userKey: any = ['id'];

  getUserSettingsUrl: string = userAPI.getUserSettingsUrl;
  updateUserDataUrl: string = userAPI.updateusersettingsUrl;

  constructor(private oAuthStorage: OAuthStorage, private httpService: HttpClientService) {}

  /*
   * Get/Set UserData
   */

  loadCompanyMapping(): Observable<SysUserSettingsItemView> {
    const userId = this.getUserIdFromToken();
    const url = `SysAccessRight/UserDefaultBranchCompanyMappings/${userId}`;
    return this.httpService.Get(url);
  }

  // loadUserData(username, site?): Observable<SysUserTableView> {
  //   const url = `${this.userDataUrl + username}`;

  //   if (isUndefinedOrZeroLength(site)) {
  //     site = this.getSiteLogin();
  //   }

  //   return this.httpService.Get(url, { headers: { SiteHeader: site } });
  // }

  loadUserSettings(username): Observable<SysUserSettingsItemView> {
    const url = `${this.getUserSettingsUrl + username}`;
    return this.httpService.Get(url);
  }

  getCompanyBranchByCurrentUser(): Observable<any> {
    const userId = this.getUserIdFromToken();
    const url = `SysUser/GetUserSettingMappedBranchCompanyByUserName/${userId}`;
    return this.httpService.Get(url);
  }
  /*
   *  Update User Data (user-settings)
   */
  updateUserData(data: any): Observable<any> {
    const url = `${this.updateUserDataUrl}`;
    return this.httpService.Post(url, data);
  }

  /*
   * Get/Set user-company mapping (current company, branch)
   */

  getCurrentCompanyGUID(): string {
    return this.oAuthStorage.getItem(StorageKey.COMPANYGUID);
  }

  getCurrentCompanyId(): string {
    return this.oAuthStorage.getItem(StorageKey.COMPANY_ID);
  }

  setCurrentCompany(data: any): any {
    if (isUndefinedOrZeroLength(data)) {
      return;
    }
    const currentCompany = {
      companyGUID: data.companyGUID,
      companyId: data.companyId
    };
    if (!isUndefinedOrZeroLength(currentCompany.companyGUID)) {
      this.oAuthStorage.setItem(StorageKey.COMPANYGUID, currentCompany.companyGUID);
    }
    if (!isUndefinedOrZeroLength(currentCompany.companyId)) {
      this.oAuthStorage.setItem(StorageKey.COMPANY_ID, currentCompany.companyId);
    }
    this.setCurrentBranch(data);
  }

  getCurrentBranchGUID(): string {
    return this.oAuthStorage.getItem(StorageKey.BRANCHGUID);
  }

  getCurrentBranchId(): string {
    return this.oAuthStorage.getItem(StorageKey.BRANCH_ID);
  }

  setCurrentBranch(data: any): any {
    if (isUndefinedOrZeroLength(data)) {
      return;
    }
    const currentBranch = {
      branchGUID: data.branchGUID,
      branchId: data.branchId
    };
    if (!isUndefinedOrZeroLength(currentBranch.branchGUID)) {
      this.oAuthStorage.setItem(StorageKey.BRANCHGUID, currentBranch.branchGUID);
    }
    if (!isUndefinedOrZeroLength(currentBranch.branchId)) {
      this.oAuthStorage.setItem(StorageKey.BRANCH_ID, currentBranch.branchId);
    }
  }

  /*
   * Get/Set App language
   */
  getAppLanguage(): string {
    return localStorage.getItem(StorageKey.USER_LANGUAGE);
  }
  setAppLanguage(languageId: string): void {
    localStorage.setItem(StorageKey.USER_LANGUAGE, languageId);
  }

  /*
   * Get/Set show System toast
   */
  getShowSystemLog(): string {
    return localStorage.getItem(StorageKey.SHOW_LOG);
  }
  setShowSystemLog(showLog: string): void {
    localStorage.setItem(StorageKey.SHOW_LOG, showLog);
  }

  /*
   * Helper: HeaderComponent
   */
  getDisplayName(): any {
    const token = this.oAuthStorage.getItem(StorageKey.ACCESS_TOKEN);

    const tokenPayload: any = !isNullOrUndefined(token) ? jwt_decode(token) : null;

    const name = tokenPayload !== null ? tokenPayload.name : null;
    const username = tokenPayload !== null ? tokenPayload.username : null;
    return name != null ? name : username;
  }
  /*
   * Get/Set user image
   */
  getUserImageContent(): string {
    return localStorage.getItem(StorageKey.USER_IMAGE);
  }
  // base64 content with data mime type
  setUserImageContent(img: string): void {
    localStorage.setItem(StorageKey.USER_IMAGE, img);
  }

  /*
   * Get/Set site login
   */
  getSiteLogin(): string {
    const fromStorage = localStorage.getItem(StorageKey.SITE_LOGIN);
    if (isUndefinedOrZeroLength(fromStorage)) {
      return null;
    } else {
      const key = getEncryptionKey();
      let site = decryptObject(key, fromStorage);
      if (!isUndefinedOrZeroLength(site)) {
        return site;
      } else {
        return null;
      }
    }
  }
  setSiteLogin(site: string): void {
    const key = getEncryptionKey();
    const item = encryptObject(key, site);
    localStorage.setItem(StorageKey.SITE_LOGIN, item);
  }

  /*
   * Get/Set inactive user
   */
  getInactiveUser(): boolean {
    const fromStorage = localStorage.getItem(StorageKey.INACTIVE_USER);
    if (isUndefinedOrZeroLength(fromStorage)) {
      return true;
    } else {
      const key = getEncryptionKey();
      const inactiveUser = decryptObject(key, fromStorage);
      if (!isUndefinedOrZeroLength(inactiveUser)) {
        return inactiveUser === AppConst.TRUE ? true : false;
      } else {
        return false;
      }
    }
  }
  setInactiveUser(inactiveUser: string): void {
    const key = getEncryptionKey();
    const item = encryptObject(key, inactiveUser);
    localStorage.setItem(StorageKey.INACTIVE_USER, item);
  }
  /*
   * reset dropdown cache
   */
  clearUsageData(): void {
    localStorage.removeItem(StorageKey.DROPDOWN_CACHE);
  }

  /*
   * Helper: Others
   */
  getUsernameFromToken(): string {
    const tokenPayload: any = jwt_decode(this.oAuthStorage.getItem(StorageKey.ACCESS_TOKEN));
    if (!isNullOrUndefined(tokenPayload)) {
      return tokenPayload.username;
    }
    return null;
  }

  getUserIdFromToken(): any {
    const tokenPayload: any = jwt_decode(this.oAuthStorage.getItem(StorageKey.ACCESS_TOKEN));
    if (!isNullOrUndefined(tokenPayload)) {
      return tokenPayload.usubj;
    }
    return null;
  }
  setUserFullName(fullname: string): void {
    const key = getEncryptionKey();
    const item = encryptObject(key, fullname);
    localStorage.setItem(StorageKey.USER_FULLNAME, item);
  }
  getUserFullName(): any {
    const fromStorage = localStorage.getItem(StorageKey.USER_FULLNAME);
    if (isUndefinedOrZeroLength(fromStorage)) {
      return '';
    } else {
      const key = getEncryptionKey();
      const result = decryptObject(key, fromStorage);
      if (!isNullOrUndefined(result)) {
        return result;
      } else {
        return '';
      }
    }
  }
}
