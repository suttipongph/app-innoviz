import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpResponse } from '@angular/common/http';
import { LoggingService } from './logging.service';
import { tap, finalize } from 'rxjs/operators';
import { NotificationService } from './notification.service';
import { UserDataService } from './user-data.service';
import { AppConst, Ordinal } from 'shared/constants';
import { LoggingModel, NotificationResponse } from 'shared/models/systemModel';
import { UIControllerService } from './uiController.service';
import { isUndefinedOrZeroLength, isNullOrUndefined, getWarningToTranslate } from 'shared/functions/value.function';

@Injectable()
export class LoggingInterceptor implements HttpInterceptor {
  counter = 0;
  constructor(
    private logger: LoggingService,
    private notificationService: NotificationService,
    private userDataService: UserDataService,
    private uiService: UIControllerService
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): any {
    const started = Date.now();
    let result;

    return next.handle(req).pipe(
      tap(
        (event) => {
          if (event instanceof HttpResponse) {
            result = event;
            this.interceptResponseBody(result.body);
            this.notificationDetecting(result.body);
          }
        },
        (err) => {
          result = err;
          this.getErrorDetecting(result);
          console.error(result);
        }
      ),
      finalize(() => {
        this.uiService.setDecreaseReqCounter(req.url);
        const now = Date.now();

        const logItem: LoggingModel = this.logger.createSystemHttpAppLog(req, result, started, now);
        if (this.userDataService.getShowSystemLog() === AppConst.TRUE) {
          // show toast
          this.notificationService.showHttpLogNotification(logItem);
        }
      })
    );
  }

  interceptResponseBody(respBody: any): void {
    if (!(respBody instanceof Array)) {
      if (!isUndefinedOrZeroLength(respBody)) {
        const properties = Object.keys(respBody);
        if (properties.indexOf('companyGUID') > -1) {
          if (properties.indexOf('companyLogo') > -1) {
            //check if companyView model
            return;
          } else {
            const siteLogin = this.userDataService.getSiteLogin();
            if (siteLogin !== AppConst.OB) {
              // check companyGUID from returned data
              // whether it matched current companyGUID
              const companyFromData = respBody['companyGUID'];
              const currentCompany = this.userDataService.getCurrentCompanyGUID();

              if (!isUndefinedOrZeroLength(companyFromData) && companyFromData !== currentCompany) {
                sessionStorage.setItem('companyFromResponse', companyFromData);
                this.notificationService.showBranchCompanySelector();
              }
            }
          }
        }
      }
    }
  }
  private notificationDetecting(result: any): void {
    if (!isNullOrUndefined(result)) {
      const notification: NotificationResponse = result['notification'];
      if (!isNullOrUndefined(notification) && !isNullOrUndefined(notification.notificationMessage)) {
        this.notificationService.showNotificationFromResponse({
          topic: null,
          contents: getWarningToTranslate(notification)
        });
      } else {
        const notificationMessage: any[] = result['notificationMessage'];
        const notificationParameter: any[] = result['notificationParameter'];
        const notificationType: string = result['notificationType'];
        if (!isNullOrUndefined(notificationMessage)) {
          this.notificationService.showNotificationFromResponse({
            topic: null,
            contents: getWarningToTranslate({
              notificationMessage: notificationMessage,
              notificationParameter: notificationParameter,
              notificationType: notificationType
            })
          });
        }
      }
    }
  }
  private getErrorDetecting(result: any): void {
    if (!isNullOrUndefined(result.error) && !isNullOrUndefined(result.error.errorMessage)) {
      const keys = Object.keys(result.error.errorMessage);
      keys.forEach((key) => {
        if (AppConst.VIEWMODE_ERROR_CODE.some((s) => s.toUpperCase() === key.toUpperCase())) {
          this.uiService.setUIFieldsAccessing([{ filedIds: [AppConst.VIEWMODE_ID], readonly: true }]);
        }
      });
    }
  }
}
