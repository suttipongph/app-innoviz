import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { FileInformation, IExportParam, IFileName, SearchParameter, OptionModel, OptionExportModel, ColumnModel } from 'shared/models/systemModel';
import { isUndefinedOrZeroLength, base64ToByteArray, trimEnd, isNullOrUndefined } from 'shared/functions/value.function';
import {
  CONTENT_TYPE,
  TXT_TYPE,
  PDF_TYPE,
  PNG_TYPE,
  JPG_TYPE,
  JPEG_TYPE,
  GIF_TYPE,
  XLSX_TYPE,
  XLS_TYPE,
  XLSX_EXTENSION
} from 'shared/constants/filetypeConstant';
import { AppInjector } from 'app-injector';
import { SysListViewService } from './sys-list-view.service';
import { dateTimetoString, dateTimetoStringLong } from 'shared/functions/date.function';
import { setOrderingJson } from 'shared/functions/model.function';
import { getExcelfileNameFormat } from 'shared/config/globalvar.config';
import { ColumnType } from 'shared/constants/enumsSystem';
import { FormatConfig } from 'shared/config/format.config';

@Injectable({
  providedIn: 'root'
})
export class FileService {
  public searchParam: SearchParameter;
  // ####### export excel ########
  public exportAsExcelFile(json: any[], excelFileName: string, option?: OptionModel): void {
    if (json.length > 0) {
      const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json, { cellDates: true });
      if (!isNullOrUndefined(option)) {
        const keys = Object.keys(worksheet);
        keys.forEach((k) => {
          if (typeof worksheet[k].v === 'number') {
            if (this.getExcelColumnType(worksheet, k, option) === ColumnType.INT) {
              worksheet[k].z = '0';
              worksheet[k].t = 'n';
            } else {
              if (this.getExcelColumnType(worksheet, k, option) === ColumnType.DECIMAL) {
                if (
                  this.getExcelFormatConfig(worksheet, k, option) === FormatConfig.CURRENCY_16_5 ||
                  this.getExcelFormatConfig(worksheet, k, option) === FormatConfig.CURRENCY_16_5_POS
                ) {
                  worksheet[k].z = '0.00000';
                  worksheet[k].t = 'n';
                } else if (
                  this.getExcelFormatConfig(worksheet, k, option) === FormatConfig.PERCENT_13_10 ||
                  this.getExcelFormatConfig(worksheet, k, option) === FormatConfig.PERCENT_13_10_POS
                ) {
                  worksheet[k].z = '0.0000000000';
                  worksheet[k].t = 'n';
                } else if (
                  this.getExcelFormatConfig(worksheet, k, option) === FormatConfig.PERCENT_10_7 ||
                  this.getExcelFormatConfig(worksheet, k, option) === FormatConfig.PERCENT_10_7_POS
                ) {
                  worksheet[k].z = '0.0000000';
                  worksheet[k].t = 'n';
                } else if (
                  this.getExcelFormatConfig(worksheet, k, option) === FormatConfig.PERCENT_7_4 ||
                  this.getExcelFormatConfig(worksheet, k, option) === FormatConfig.PERCENT_7_4_POS
                ) {
                  worksheet[k].z = '0.0000';
                  worksheet[k].t = 'n';
                } else {
                  worksheet[k].z = '0.00';
                  worksheet[k].t = 'n';
                }
              }
            }
          }
        });
      }
      const workbook: XLSX.WorkBook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = XLSX.write(workbook, { bookType: XLSX_TYPE, type: 'array' });
      this.saveAsExcelFile(excelBuffer, excelFileName);
    }
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: CONTENT_TYPE[XLS_TYPE] });
    const config: IFileName = getExcelfileNameFormat();
    FileSaver.saveAs(data, this.getExcelFileName(fileName, config) + XLSX_EXTENSION);
  }

  // input buffer => base64string
  // input fileName => fileName with extension
  saveAsFile(buffer: any, fileName: string): void {
    let contentType: string;
    let base64data: string;
    if (this.hasMimeHeader(buffer)) {
      const dataArray: string[] = buffer.split(',');
      contentType = this.getMimeHeader(dataArray[0] + ',');
      base64data = dataArray.pop();
    } else {
      base64data = buffer;
      contentType = this.getContentTypeFromFileName(fileName);
    }
    const data: Blob = this.base64toBlob(base64data, contentType);
    FileSaver.saveAs(data, fileName);
  }
  // input file name with extension
  getContentTypeFromFileName(fileName: string): string {
    const extension: string = fileName.split('.').pop().toLowerCase();
    return CONTENT_TYPE[extension];
  }
  // input base64Data without 'data:[content-type];base64,'
  base64toBlob(base64Data, contentType): Blob {
    contentType = contentType || '';
    const byteArrays = base64ToByteArray(base64Data);
    return new Blob(byteArrays, { type: contentType });
  }

  getPreviewData(file: FileInformation): string {
    const base64Data = file.base64;
    if (this.hasMimeHeader(base64Data)) {
      return base64Data;
    }
    let contentType: string = this.getMimeHeader(file.contentType);
    if (isUndefinedOrZeroLength(contentType)) {
      contentType = this.getContentTypeFromFileName(file.fileName);
    }
    return 'data:' + contentType + ';base64, ' + encodeURI(base64Data);
  }

  getPreviewBlob(file: FileInformation): Blob {
    let base64Data = file.base64;
    let contentType;
    if (this.hasMimeHeader(file.base64)) {
      const dataArray: string[] = file.base64.split(',');
      contentType = this.getMimeHeader(dataArray[0] + ',');
      base64Data = dataArray.pop();
    } else {
      contentType = this.getContentTypeFromFileName(file.fileName);
    }
    const data: Blob = this.base64toBlob(base64Data, contentType);
    return data;
  }
  getPreviewable(contentType: string): boolean {
    contentType = this.getMimeHeader(contentType);
    const fileType: string = this.getKeyFromValue(CONTENT_TYPE, contentType);

    switch (fileType) {
      case PDF_TYPE:
      case PNG_TYPE:
      case JPG_TYPE:
      case JPEG_TYPE:
      case GIF_TYPE:
        return true;

      default:
        return false;
    }
  }

  isSupportedFileType(contentType: string): boolean {
    contentType = !isUndefinedOrZeroLength(contentType) && contentType.split(':').length > 1 ? this.getMimeHeader(contentType) : contentType;

    const fileType: string = this.getKeyFromValue(CONTENT_TYPE, contentType);
    return fileType !== undefined;
  }

  isPictureType(contentType: string): boolean {
    const type = this.getMimeHeader(contentType);
    const fileType: string = this.getKeyFromValue(CONTENT_TYPE, type);
    switch (fileType) {
      case PNG_TYPE:
      case JPG_TYPE:
      case JPEG_TYPE:
      case GIF_TYPE:
        return true;

      default:
        return false;
    }
  }

  isPreviewableDocumentType(contentType: string): boolean {
    const type = this.getMimeHeader(contentType);
    const fileType: string = this.getKeyFromValue(CONTENT_TYPE, type);
    switch (fileType) {
      case TXT_TYPE:
      case PDF_TYPE:
        return true;

      default:
        return false;
    }
  }
  isFileSizeValid(fileSize: number, fileSizeLimit: number): boolean {
    // 30 MB
    return fileSize < fileSizeLimit * 1000 * 1000;
  }
  public getDisplayFileName(nameWithExt: string, description: string): string {
    if (isUndefinedOrZeroLength(description)) {
      return nameWithExt;
    }
    const splitName: string[] = nameWithExt.split('.');
    if (splitName.length > 1) {
      return description + '.' + splitName.pop();
    } else {
      return nameWithExt;
    }
  }
  isDataUrl(contentType: string): boolean {
    const array = contentType.split(':');
    if (!isUndefinedOrZeroLength(array) && array.length > 1) {
      return array[0] === 'data';
    }
    return false;
  }
  private hasMimeHeader(data: string): boolean {
    const mimeHeader = this.getMimeHeader(data);
    if (isUndefinedOrZeroLength(mimeHeader)) {
      return false;
    }
    return true;
  }

  private getKeyFromValue(object: any, value: any): string {
    return Object.keys(object).find((key) => object[key] === value);
  }

  private getMimeHeader(data: string): string {
    if (typeof data !== 'string') {
      return null;
    }
    const regArray = data.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);
    if (regArray && regArray.length) {
      return regArray[1];
    }
    return '';
  }
  //   public getExportToExcel(param: IExportParam): any {
  //     const syslistViewService = AppInjector.get(SysListViewService);
  //     const column = param.option.columns.find((f) => f.type === ColumnType.CONTROLLER);
  //     const table = isNullOrUndefined(column) ? trimEnd(param.option.key, 'GUID') : column.textKey;
  //     const isFilter = !param.option.key.includes('attachment');
  //     const controller = isFilter ? 'SysListView' : 'Attachment';
  //     syslistViewService
  //       .getExportingList(param.searchParam, controller, table, isFilter)
  //       .finally(() => {})
  //       .subscribe(
  //         (result) => {
  //           this.getResultDataToExcel(result, param.option);
  //         },
  //         (error) => {}
  //       );
  //   }
  getResultDataToExcel(result: any, option: OptionModel): any {
    const orderedJson = setOrderingJson(result.results, option);
    this.exportAsExcelFile(orderedJson, !isNullOrUndefined(option.exportFileName) ? option.exportFileName : trimEnd(option.key, 'GUID'), option);
  }
  saveTextContentAsFile(content: string, fileName: string): void {
    const data = new Blob([content], { type: 'text/plain;charset=utf-8' });
    FileSaver.saveAs(data, fileName);
  }
  getExcelFileName(tableName: string, config: IFileName): string {
    let naming = config.nameFormat;
    naming = naming.replace('{{tableName}}', tableName);
    naming = naming.replace('{{date}}', dateTimetoStringLong(new Date(), config.dateFormat));
    return naming;
  }
  getDataMimeHeaderFromFileName(fileName: string): string {
    const contentType = this.getContentTypeFromFileName(fileName);
    return 'data:' + contentType + ';base64,';
  }
  getReportToExcel(results: any, option: OptionExportModel): any {
    let optionModel = new OptionModel();
    optionModel.key = option.exportFileName;
    let columnModels: ColumnModel[] = [];
    option.columns.forEach((c) => {
      const columnModel: ColumnModel = new ColumnModel();
      columnModel.label = c.label;
      columnModel.textKey = c.textKey;
      columnModel.type = c.type != null ? c.type : ColumnType.NONE;
      columnModel.format = c.format;
      columnModel.visibility = true;
      columnModels.push(columnModel);
    });
    optionModel.columns = columnModels;
    const orderedJson = setOrderingJson(results, optionModel);
    this.exportAsExcelFile(orderedJson, option.exportFileName, optionModel);
  }
  getExcelColumnType(worksheet: XLSX.WorkSheet, cell: string, options: OptionModel): ColumnType {
    let result = ColumnType.STRING;
    cell = cell.replace(/\d/g, '');
    const colName = worksheet[`${cell}1`].v;
    if (!isNullOrUndefined(options)) {
      options.columns.forEach((col) => {
        if (col.label === colName) {
          result = col.type;
        }
      });
    }
    return result;
  }

  getExcelFormatConfig(worksheet: XLSX.WorkSheet, cell: string, options: OptionModel): Object {
    let result = null;
    cell = cell.replace(/\d/g, '');
    const colName = worksheet[`${cell}1`].v;
    if (!isNullOrUndefined(options)) {
      options.columns.forEach((col) => {
        if (col.label === colName) {
          result = col.format;
        }
      });
    }
    return result;
  }
}
