import { Injectable } from '@angular/core';
import { HttpClientService } from './httpclient.service';
import { Observable, Subject } from 'rxjs';
import { DataServiceModel, ResponseModel, SearchCondition, SearchParameter, SearchResult, SelectItems } from 'shared/models/systemModel';
import { UIControllerService } from './uiController.service';
import {
  cloneObject,
  isNullOrUndefined,
  isNullOrUndefOrEmpty,
  isSelectAll,
  isUndefinedOrZeroLength,
  replaceAll
} from 'shared/functions/value.function';
import { ColumnType } from 'shared/constants';
import { NotificationService } from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class DataGatewayService {
  public getDropdownSub = new Subject<SelectItems[]>();
  public types = [];
  public multiSelectInfo = [];
  serviceKey: string;
  constructor(private http: HttpClientService, private uiService: UIControllerService, private notificationService: NotificationService) {}

  get(url: string): Observable<any> {
    this.getFilterUrlToRelatedKey(url);
    const $result = this.http.Get(url);
    return new Observable((observer) => {
      $result.subscribe(
        (res) => {
          observer.next(res);
        },
        (err) => {
          this.notificationService.showErrorMessageFromResponse(err);
          observer.error(err);
        },
        () => {
          observer.complete();
        }
      );
    });
  }
  post(url: string, data: any): Observable<any> {
    const $result = this.http.Post(url, this.setParamMutiSelect(data));
    return new Observable((observer) => {
      $result.subscribe(
        (res) => {
          observer.next(res);
        },
        (err) => {
          this.notificationService.showErrorMessageFromResponse(err);
          observer.error(err);
        },
        () => {
          observer.complete();
        }
      );
    });
  }
  create(url: string, data: any, key?: string): Observable<ResponseModel> {
    this.setDefaultData(data);
    const $result = this.http.Post(url, data);
    if (isNullOrUndefOrEmpty(key)) {
      return $result;
    } else {
      return new Observable((observer) => {
        $result.subscribe(
          (res) => {
            observer.next({ key: res[key], model: res });
          },
          (err) => {
            this.notificationService.showErrorMessageFromResponse(err);
            observer.error(err);
          },
          () => {
            observer.complete();
          }
        );
      });
    }
  }
  update(url: string, data: any, key?: string): Observable<ResponseModel> {
    this.setDefaultData(data);
    const $result = this.http.Post(url, data);
    if (isNullOrUndefOrEmpty(key)) {
      return $result;
    } else {
      return new Observable((observer) => {
        $result.subscribe(
          (res) => {
            observer.next({ key: res[key], model: res });
          },
          (err) => {
            this.notificationService.showErrorMessageFromResponse(err);
            observer.error(err);
          },
          () => {
            observer.complete();
          }
        );
      });
    }
  }
  delete(url: string, data: any): Observable<any> {
    return this.http.Post(url, data);
  }
  getDropdown(url: string, conditions?: SearchCondition[]): DataServiceModel {
    return { url, conditions };
    // const search: SearchParameter = new SearchParameter();
    // for (let index = 0; index < 5; index++) {
    //     console.log(index);

    // }
    // search.conditions = conditions;
    // for (let index = 0; index < 5; index++) {
    //     let c = 0;
    //     while (c < 100000) {
    //         const ff = 'dd';
    //         c++;
    //     }
    //     console.log(new Date());

    //     return new Observable((observer) => {
    //         this.http.Post(url, search)
    //             .subscribe(
    //                 res => {
    //                     console.log('res', res);

    //                     observer.next(res);
    //                 },
    //                 err => {
    //                     observer.error(err);
    //                 },
    //                 () => {
    //                     observer.complete();
    //                     // this.UrlDetecting(url, true);
    //                 });
    //     });

    // }
  }
  getHttpDropdown(url: string, search: SearchParameter): Observable<SelectItems[]> {
    return this.http.Post(url, search);
  }
  getList(url: string, search: SearchParameter): Observable<any> {
    return this.http.Post(url, search);
  }

  getListStore(selectStore$: Observable<any>): Observable<any> {
    return new Observable((observer) => {
      selectStore$.subscribe(
        (res) => {
          observer.next(res.list);
        },
        (err) => {
          observer.error(err);
        }
      );
    });
  }
  getItemStore(selectStore$: Observable<any>): Observable<any> {
    return new Observable((observer) => {
      selectStore$.subscribe(
        (res) => {
          observer.next(res.item);
        },
        (err) => {
          observer.error(err);
        }
      );
    });
  }
  updateStore(selectStore$: Observable<any>): Observable<any> {
    return new Observable((observer) => {
      selectStore$.subscribe(
        (res) => {
          observer.next(res.item);
        },
        (err) => {
          observer.error(err);
        }
      );
    });
  }
  createStore(selectStore$: Observable<any>): Observable<any> {
    return new Observable((observer) => {
      selectStore$.subscribe(
        (res) => {
          observer.next(res.item);
        },
        (err) => {
          observer.error(err);
        }
      );
    });
  }
  deleteStore(selectStore$: Observable<any>): Observable<any> {
    return new Observable((observer) => {
      selectStore$.subscribe(
        (res) => {
          observer.next(true);
        },
        (err) => {
          observer.error(err);
        }
      );
    });
  }

  private setDefaultData(model: any): void {
    if (!isNullOrUndefined(model)) {
      const keys = Object.keys(model);
      keys.forEach((key) => {
        // check unbound field
        if (key.indexOf('_') === -1) {
          const field = this.types.find((f) => f.id === key.toUpperCase());
          if (!isNullOrUndefined(field)) {
            if (isNullOrUndefined(model[key])) {
              switch (field.type) {
                case ColumnType.STRING:
                  model[key] = '';
                  break;
                case ColumnType.INT:
                  model[key] = 0;
                  break;
                default:
                  break;
              }
            }
          }
        }
      });
    }
  }
  setDataType(id: string, type?: ColumnType): void {
    id = replaceAll(id, '_', '').toUpperCase();
    const index = this.types.findIndex((f) => f.id === id);
    if (isNullOrUndefined(type)) {
      if (index > -1) {
        this.types.splice(index, 1);
      }
    } else {
      if (index === -1) {
        this.types.push({ id, type });
      }
    }
  }
  private getTableName(param: string): string {
    if (!isNullOrUndefined(param)) {
      const pUpper = param.toUpperCase();
      if (/(GET\w+BYID)/g.test(pUpper)) {
        return pUpper.replace('GET', '').replace('BYID', '');
      } else if (/(\w+GUID)/g.test(pUpper)) {
        return pUpper.replace('GUID', '');
      } else {
        return pUpper;
      }
    } else {
      return null;
    }
  }
  private getTableUrlSegment(param: string): any {
    const segments = param.split('/');
    const index = segments.findIndex((f) => /(GET\w+BYID)/g.test(f.toUpperCase()));
    if (index > -1 && !isNullOrUndefined(segments[index + 1])) {
      return { table: segments[index], key: segments[index + 1].replace('id=', '') };
    } else {
      return null;
    }
  }
  private getFilterUrlToRelatedKey(url: string): void {
    if (!isNullOrUndefined(url)) {
      const urlSegment = this.getTableUrlSegment(url);
      if (!isNullOrUndefined(urlSegment)) {
        if (this.getTableName(urlSegment.table) === this.getTableName(this.serviceKey)) {
          this.uiService.relatedKey = { key: urlSegment.key, textKey: this.serviceKey };
        } else {
          this.uiService.relatedKey = null;
        }
      } else {
        this.uiService.relatedKey = null;
      }
    } else {
      this.uiService.relatedKey = null;
    }
  }
  setBindingMultiSelectDataInfo(id: string, type: string, optionsLength?: number): void {
    id = replaceAll(id, '_', '').toUpperCase();
    const index = this.multiSelectInfo.findIndex((f) => f.id === id);
    if (isNullOrUndefined(optionsLength)) {
      if (index > -1) {
        this.multiSelectInfo.splice(index, 1);
      }
    } else {
      if (index === -1) {
        this.multiSelectInfo.push({ id, type, optionsLength });
      } else {
        this.multiSelectInfo.splice(index, 1);
        this.multiSelectInfo.push({ id, type, optionsLength });
      }
    }
  }
  resetBindingMultiSelectDataInfo(): void {
    this.multiSelectInfo = [];
  }
  setBatchParamDefaultData(param: any): void {
    this.setDefaultData(param);
  }
  setParamMutiSelect(parm: any): any {
    let param = cloneObject(parm);
    const keys = Object.keys(parm);
    if (!isUndefinedOrZeroLength(this.multiSelectInfo) && !isUndefinedOrZeroLength(keys)) {
      this.multiSelectInfo.forEach((multiSelect) => {
        const index = keys.findIndex((f) => f.toUpperCase() === multiSelect.id.toUpperCase());
        if (index > -1) {
          const itemKey = keys[index];
          const isEnum: boolean = multiSelect.type === ColumnType.INT;
          param[itemKey] = isSelectAll(param[itemKey], multiSelect.optionsLength, isEnum);
        }
      });
    }
    return param;
  }
}
