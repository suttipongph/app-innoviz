import { Action, props } from '@ngrx/store';
import { CustomerTableItemView } from 'shared/models/viewModel';
import { createAction } from '@ngrx/store';

// export const addDummy = createAction('[DUMMY] Add', props<{ item: CustomerTableItemView }>());
export const saveDummy = createAction('[DUMMY] Save', props<{ index: number; item: CustomerTableItemView }>());
export const deleteDummy = createAction('[DUMMY] Delete', props<{ index: number }>());
export const viewDummy = createAction('[DUMMY] VIEW');
export const viewDummyItem = createAction('[DUMMY] VIEW_ITEM', props<{ index: number }>());
