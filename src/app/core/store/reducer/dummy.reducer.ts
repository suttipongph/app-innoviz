import { CustomerTableItemView, CustomerTableListView } from 'shared/models/viewModel';
import * as DummyActions from 'core/store/actions/dummy.action';
import { Action, createReducer, on, State } from '@ngrx/store';
import { CustomerTableStoreModel, SearchResult } from 'shared/models/systemModel';

export const initialState: CustomerTableStoreModel = new CustomerTableStoreModel();

const dummyReducer = createReducer(
  initialState,
  on(DummyActions.saveDummy, (state, { index, item }) => saveDummy(state, { index, item })),
  on(DummyActions.deleteDummy, (state, { index }) => deleteDummy(state, index)),
  on(DummyActions.viewDummy, (state) => viewDummy(state)),
  on(DummyActions.viewDummyItem, (state, { index }) => viewDummyItem(state, index))
);
export function reducer(state: CustomerTableStoreModel | undefined, action: Action): CustomerTableStoreModel {
  return dummyReducer(state, action);
}
function saveDummy(state: CustomerTableStoreModel, action): CustomerTableStoreModel {
  if (action.index === -1) {
    state.items.push(action.item);
    state.list.results.push(ItemToList(action.item));
  } else {
    state.items[action.index] = action.item;
    state.list.results[action.index] = ItemToList(action.item);
  }
  state.item = action.item;
  return state;
}
function deleteDummy(state: CustomerTableStoreModel, action): CustomerTableStoreModel {
  state.items.splice(action.index, 1);
  state.list.results.splice(action.index, 1);
  return state;
}
function viewDummy(state: CustomerTableStoreModel): CustomerTableStoreModel {
  return state;
}
function viewDummyItem(state: CustomerTableStoreModel, action): CustomerTableStoreModel {
  state.item = state.items[action.index];
  return state;
}
//#endregion
function ItemToList(item: CustomerTableItemView): CustomerTableListView {
  const result: CustomerTableListView = new CustomerTableListView();
  return result;
}
